#
# FOL ProstBuildScript 
#	
	
# Enable -Verbose option
[CmdletBinding()]
	
param
(
[switch] $Disable,
[switch] $Deploy,
[switch] $ZipSrc,
[switch] $CleanBin
)
 
Write-Host "Running ProstBuildScript"
 
if ($PSBoundParameters.ContainsKey('Disable'))
{
	Write-Host "Script disabled; no actions will be taken on the files."
}

# If this script is not running on a build server, remind user to 
# set environment variables so that this script can be debugged
if(-not $Env:TF_BUILD -and -not ($Env:TF_BUILD_SOURCESDIRECTORY -and $Env:TF_BUILD_BINARIESDIRECTORY))
{
	Write-Error "You must set the following environment variables"
	Write-Host "Example:"
	Write-Host '$Env:TF_BUILD_SOURCESDIRECTORY = "C:\Builds\1\Vizelia\EO_Daily_dev\src"'
	Write-Host '$Env:TF_BUILD_BINARIESDIRECTORY = "C:\Builds\1\Vizelia\EO_Daily_dev\bin"'
	exit 1
}
	
# Make sure path to source code directory is available
if (-not $Env:TF_BUILD_SOURCESDIRECTORY)
{
	Write-Error ("TF_BUILD_SOURCESDIRECTORY environment variable is missing.")
	exit 1
}
elseif (-not (Test-Path $Env:TF_BUILD_SOURCESDIRECTORY))
{
	Write-Error "TF_BUILD_SOURCESDIRECTORY does not exist: $Env:TF_BUILD_SOURCESDIRECTORY"
	exit 1
}
Write-Host "TF_BUILD_SOURCESDIRECTORY: $Env:TF_BUILD_SOURCESDIRECTORY"
	
# Make sure path to binary output directory is available
if (-not $Env:TF_BUILD_BINARIESDIRECTORY)
{
	Write-Error ("TF_BUILD_BINARIESDIRECTORY environment variable is missing.")
	exit 1
}
if ([IO.File]::Exists($Env:TF_BUILD_BINARIESDIRECTORY))
{
	Write-Error "Cannot create output directory."
    Write-Error "File with name $Env:TF_BUILD_BINARIESDIRECTORY already exists."
	exit 1
}
Write-Host "TF_BUILD_BINARIESDIRECTORY: $Env:TF_BUILD_BINARIESDIRECTORY"

# Tell user what script is about to do
Write-Host "Create package"
if(-not $Disable)
{
	# Create version file with build number and build date
	$verInfo=Join-Path $Env:TF_BUILD_BINARIESDIRECTORY "$Env:TF_BUILD_BUILDNUMBER.txt"
	$curDate = Get-Date
	Write-Verbose "New-Item $verInfo -ItemType file -force -value ""$Env:TF_BUILD_BUILDNUMBER [$curDate]"""
	New-Item $verInfo -ItemType file -force -value "$Env:TF_BUILD_BUILDNUMBER [$curDate]" | Out-Null

	if($Deploy)
	{
		# Create deploy folders
		$deployPath=Join-Path $Env:TF_BUILD_BINARIESDIRECTORY "deploy"
		if  (-not (Test-Path $deployPath)) 
		{
		  Write-Verbose "New-Item -ItemType directory -Path $deployPath"
		  New-Item -ItemType directory -Path $deployPath | Out-Null
		}
	
		$array = @("utilities", "sql", "iis")
		foreach ($element in $array) {
			$newItem=Join-Path $deployPath $element
			if  (-not (Test-Path $newItem)) 
			{
			  Write-Verbose "New-Item -ItemType directory -Path $newItem"
			  New-Item -ItemType directory -Path $newItem | Out-Null
			}
		}

		# Copy items to deploy folder
		$srcItem=Join-Path $Env:TF_BUILD_BINARIESDIRECTORY "Vizelia.FOL.Database\*"
		$destItem=Join-Path $deployPath "sql\"
		Write-Verbose "Copy-Item -Path $srcItem -Destination $destItem -recurse -container -force"
		Copy-Item -Path $srcItem -Destination $destItem -recurse -container -force

		$removeItem=Join-Path $deployPath "sql\SqlPackage"
		Write-Verbose "Remove-Item $removeItem -recurse"
 		Remove-Item $removeItem -recurse

		$srcItem=Join-Path $Env:TF_BUILD_BINARIESDIRECTORY "Vizelia.FOL\_PublishedWebsites\*"
		$destItem=Join-Path $deployPath "iis\"
		Write-Verbose "Copy-Item -Path $srcItem -Destination $destItem -recurse -container -force"
		Copy-Item -Path $srcItem -Destination $destItem -recurse -container -force

		$srcItem=Join-Path $Env:TF_BUILD_SOURCESDIRECTORY "Vizelia.FOL.Database\DatabaseDeployment\*.*"
		$destItem=Join-Path $deployPath "utilities\*.*"
		Write-Verbose "xcopy ""$srcItem"" ""$destItem"" /s /e /v"
		xcopy "$srcItem" "$destItem" /s /e /v  | Out-Null

		$srcItem=Join-Path $Env:TF_BUILD_SOURCESDIRECTORY "ExternalProviders\EWS RSP Mapping Provider\*.*"
		$destItem=Join-Path $deployPath "iis\Vizelia.FOL.Web\bin\*.*"
		Write-Verbose "xcopy ""$srcItem"" ""$destItem"" /s /e /v"
		xcopy "$srcItem" "$destItem" /s /e /v  | Out-Null

		$srcItem=Join-Path $Env:TF_BUILD_BINARIESDIRECTORY "Vizelia.FOL\Support Files\*.*"
		$destItem=Join-Path $deployPath "utilities\Support Files\*.*"
		Write-Verbose "xcopy ""$srcItem"" ""$destItem"" /s /e /v"
		xcopy "$srcItem" "$destItem" /s /e /v  | Out-Null

		$srcpath=Join-Path $Env:TF_BUILD_BINARIESDIRECTORY "Vizelia.FOL"
		$destPath=Join-Path $deployPath "utilities"
		$array = @("Vizelia.FOL.Utilities.IISInstaller.exe", "Vizelia.FOL.Utilities.AzmanUpdateTool.exe", "Vizelia.FOL.Utilities.LogInstaller.exe")
		foreach ($element in $array) {
			$cpItem=Join-Path $srcpath $element
  			Write-Verbose "Copy-Item -Path $cpItem -Destination $destPath"
     		Copy-Item -Path $cpItem -Destination $destPath
		}
	}
	
	if($ZipSrc)
	{
		# ZipFile
		[Reflection.Assembly]::LoadWithPartialName( "System.IO.Compression.FileSystem" )
		$compressionLevel = [System.IO.Compression.CompressionLevel]::Optimal

		# Create Vizelia.FOL source code package (zip)
		$sourcePath=Join-Path $Env:TF_BUILD_SOURCESDIRECTORY "Vizelia.FOL"
		$zipfilePath = Join-Path $Env:TF_BUILD_BINARIESDIRECTORY "Vizelia.FOL_source_$Env:TF_BUILD_BUILDNUMBER.zip"
		Write-Verbose "ZipFile $zipfilePath from $sourcePath"
		[System.IO.Compression.ZipFile]::CreateFromDirectory(  $sourcePath, $zipfilePath, $compressionLevel, $false )

		# Create Vizelia.FOL.Database source code package (zip)
		$sourcePath=Join-Path $Env:TF_BUILD_SOURCESDIRECTORY "Vizelia.FOL.Database"
		$zipfilePath = Join-Path $Env:TF_BUILD_BINARIESDIRECTORY "Vizelia.FOL.Database_source_$Env:TF_BUILD_BUILDNUMBER.zip"
		Write-Verbose "ZipFile $zipfilePath from $sourcePath"
		[System.IO.Compression.ZipFile]::CreateFromDirectory(  $sourcePath, $zipfilePath, $compressionLevel, $false )

		#ftp zip to ftp server
		$File = $zipfilePath
		$ftp = "ftp://Vizelia:Vizelia1@ec2-54-77-240-156.eu-west-1.compute.amazonaws.com/$Env:TF_BUILD_BUILDNUMBER.zip"



		$webclient = New-Object -TypeName System.Net.WebClient
		$uri = New-Object -TypeName System.Uri -ArgumentList $ftp

		Write-Verbose  "Uploading $File..."

		$webclient.UploadFile($uri, $File)





	}

	if($CleanBin)
	{
		# Clean up folders
		$array = @("Vizelia.FOL", "Vizelia.FOL.Database")
		foreach ($element in $array) {
			$removeItem=Join-Path $Env:TF_BUILD_BINARIESDIRECTORY $element
			Write-Verbose "Remove-Item $removeItem -recurse"
 			Remove-Item $removeItem -recurse
		}
	}

	Write-Host "ProstBuildScript completed."
}

#
# FOL PreBuildScript 
#	
	
# Enable -Verbose option
[CmdletBinding()]
	
param
(
[string] $srcPath,
[string] $assemblyVersion,
[string] $fileAssemblyVersion,
[switch] $Disable
)
 
Write-Host "Running PreBuildScript"
 
if ($PSBoundParameters.ContainsKey('Disable'))
{
	Write-Host "Script disabled; no actions will be taken on the files."
}

if ($assemblyVersion -eq "")
{
    $assemblyVersion = "1.0.J.B"
}

 if ($fileAssemblyVersion -eq "")
{
    $fileAssemblyVersion = $assemblyVersion
}

function Update-FileVersion
{
  Param ([string]$assemblyVersion, [string]$fileAssemblyVersion)
  $NewVersion = 'AssemblyVersion("' + $assemblyVersion + '")';
  $NewFileVersion = 'AssemblyFileVersion("' + $fileAssemblyVersion + '")';

  foreach ($o in $input) 
  {
    Write-Host $o.FullName
	
	if(-not $Disable)
	{
		$TmpFile = $o.FullName + ".tmp"

		 get-content $o.FullName | 
			%{$_ -replace 'AssemblyVersion\("[0-9]+(\.([0-9]+|\*)){1,3}"\)', $NewVersion } |
			%{$_ -replace 'AssemblyFileVersion\("[0-9]+(\.([0-9]+|\*)){1,3}"\)', $NewFileVersion }  > $TmpFile

		 move-item $TmpFile $o.FullName -force
	}
  }
}

function Update-SourceVersion
{
  Param
  (
    [string]$SrcPath,
    [string]$assemblyVersion,
    [string]$fileAssemblyVersion
  )
     
    $buildNumber = $env:TF_BUILD_BUILDNUMBER
    if ($buildNumber -eq $null)
    {
        $buildIncrementalNumber = 1
    }
    else
    {
        $splitted = $buildNumber.Split('.')
        $buildIncrementalNumber = $splitted[$splitted.Length - 1]
    }
 
    if ($fileAssemblyVersion -eq "")
    {
        $fileAssemblyVersion = $assemblyVersion
    }
     
    Write-Verbose "Executing Update-SourceVersion in path $SrcPath, Version is $assemblyVersion and File Version is $fileAssemblyVersion"
 
	$Year1 = (get-date -format yy).Substring(1)
	$Year2 = get-date -format yy
	$DayOfYear = (Get-Date).DayofYear
	$jdate = $Year2 + "{0:D3}" -f $DayOfYear
	$buildNum1 = $buildIncrementalNumber.ToString()
	$buildNum1 = $buildNum1.Substring($buildNum1.length - 1, 1)
	
    $assemblyVersion = $assemblyVersion.Replace("J", $jdate).Replace("[B1]", $buildNum1).Replace("B", $buildIncrementalNumber).Replace("[DayOfYear]", $DayOfYear).Replace("YY", $Year2).Replace("Y", $Year1)
    $fileAssemblyVersion = $fileAssemblyVersion.Replace("J", $jdate).Replace("[B1]", $buildNum1).Replace("B", $buildIncrementalNumber).Replace("[DayOfYear]", $DayOfYear).Replace("YY", $Year2).Replace("Y", $Year1)
     
    Write-Verbose "Transformed Version is $assemblyVersion and Transformed File Version is $fileAssemblyVersion"
	
    Write-Host "Update Assembly Version to $assemblyVersion and File Version to $fileAssemblyVersion"
 
    foreach ($file in "SharedAssemblyInfo.cs" ) 
    {
      get-childitem $SrcPath -recurse |? {$_.Name -eq $file} | Update-FileVersion $assemblyVersion $fileAssemblyVersion;
    }
}

Update-SourceVersion $srcPath $assemblyVersion $fileAssemblyVersion


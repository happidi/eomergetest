﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Build.Utilities;
using NetSqlAzMan.Interfaces;
using NetSqlAzMan;

namespace Vizelia.FOL.Utilities.Builder {
	/// <summary>
	/// This class will generate a list of operations in the AzMan store.
	/// </summary>
	public class AzManGenerator : IDisposable {

		private const string WCFAzmanOperationPrefix = "Vizelia.FOL.WCFService";

		private IAzManApplication m_Application;
		private IAzManStorage m_Storage;
		private IList<IAzManItem> m_PreExistingOperations;
		private bool m_RemoveObsoleteAzmanOperations;
		private TaskLoggingHelper m_Log;

		/// <summary>
		/// Public ctor.
		/// </summary>
		/// <param name="connectionString">The connection string of the azman database.</param>
		/// <param name="storeName">The name of the store.</param>
		/// <param name="applicationName">The name of the application.</param>
		/// <param name="removeObsoleteAzmanOperations">if set to <c>true</c> [remove obsolete azman operations].</param>
		/// <param name="log">The log.</param>
		public AzManGenerator(string connectionString, string storeName, string applicationName, bool removeObsoleteAzmanOperations,
			TaskLoggingHelper log) {
			m_Storage = new SqlAzManStorage(connectionString);

			if (!m_Storage.Stores.ContainsKey(storeName))
				throw new ApplicationException(string.Format("AzMan store {0} does not exist", storeName));

			m_Application = m_Storage.Stores[storeName].Applications[applicationName];
			m_PreExistingOperations = GetPreExistingWCFOperations();
			m_RemoveObsoleteAzmanOperations = removeObsoleteAzmanOperations;
			m_Log = log;
		}



		/// <summary>
		/// Gets the preexisting WCF operations.
		/// </summary>
		/// <returns></returns>
		private IList<IAzManItem> GetPreExistingWCFOperations() {
			var operations = m_Application.GetItems(ItemType.Operation).Where(item => item.Name.StartsWith(WCFAzmanOperationPrefix)).ToList();
			return operations;
		}

		/// <summary>
		/// Adds an operation to the store.
		/// </summary>
		/// <param name="name">The name of the operation.</param>
		/// <param name="description">The description of the operation.</param>
		public void AddOperation(string name, string description) {
			try {
				IAzManItem item = this.GetItem(name);
				if (item == null)
					m_Application.CreateItem(name, description, ItemType.Operation);
				else {
					if (item.Description != description) {
						item.Update(description);
					}

					IAzManItem azManItem = m_PreExistingOperations.FirstOrDefault(op => op.Name == item.Name);
					if (azManItem != null) {
						m_PreExistingOperations.Remove(azManItem);
					}
				}

			}
			catch { ;}
		}

		/// <summary>
		/// Gets an item an catch the error in case the item does not exist.
		/// </summary>
		/// <param name="name">The name of the item.</param>
		/// <returns></returns>
		private IAzManItem GetItem(string name) {
			IAzManItem item = default(IAzManItem);
			try {
				item = this.m_Application.GetItem(name);
			}
			catch { ;}
			return item;
		}

		/// <summary>
		/// Removes the defunct operations.
		/// Any operation that was in the DB but is now no longer on the WCF should be removed
		/// </summary>
		public void RemoveObsoleteOperations() {
			foreach (var preExistingOperation in m_PreExistingOperations) {
				if (m_RemoveObsoleteAzmanOperations) {
					preExistingOperation.Delete();
				}
				//else {
				//	m_Log.LogWarning("The azman operation {0} is obselete and should be deleted", preExistingOperation.Name);
				//}
			}
		}


		/// <summary>
		/// Disposes the resources.
		/// </summary>
		public void Dispose() {
			m_Storage.Dispose();
		}

		/// <summary>
		/// Warns the about orphaned operations.
		/// </summary>
		public void WarnAboutOrphanedOperations() {

			var oprhans = m_Application.Items.Select(item => item.Value).Where(val => val.ItemsWhereIAmAMember.Count == 0 && val.ItemType == ItemType.Operation);
			var operationNames = oprhans.Select(o => o.Name).Where(o=>!o.Contains("TestWCF"));
			foreach (var operationName in operationNames) {
				m_Log.LogWarning("The following operation is not associated to any tasks " + operationName);	
			}
			

		}
	}
}

﻿/**
 * A base class for submitting long-running operations
 * @class Viz.Services.LongRunningOperationSubmitter
 * @extends Ext.util.Observable Base class - extended by StreamLongRunningOperationSubmitter and ObjectLongRunningOperationSubmitter - should not be instantiated directly
 */
Viz.Services.LongRunningOperationSubmitter = Ext.extend(Ext.util.Observable, {
            /**
             * @cfg {Function} serviceState The service method to submit the data to, which is a long-running operation.
             */
            serviceStart                      : null,
            /**
             * @cfg {Boolean} isFileResult Determines whether the result of the long-running operation is a file that should be downloaded once the result is available. This is done by calling the serviceResult.
             */
            isFileResult                      : false,
            /**
             * @cfg {Function} serviceResult The service method to call to get the long-running operation result when it is available. It will be called using an HTTP GET with operationId=[GUID]. If isFileResult is true but no serviceResult is specified, then the default service for downloading result files will be used.
             */
            serviceResult                     : null,
            /**
             * @cfg {Boolean} showProgress Determines whether progress is shown in UI as a progressbar window or not.
             */
            showProgress                      : true,
            /**
             * @cfg {Boolean} showProgressPercentage Determines whether progress is shown as percentage or not. If not, a general wait progress bar is shown.
             */
            showProgressPercentage            : false,
            /**
             * @cfg {Boolean} modal Determines whether the progress window is modal or not.
             */
            modal                             : true,
            /**
             * @cfg {String} progressWindowTitle The title of the progress window
             */
            progressWindowTitle               : $lang('msg_polling_status'),
            /**
             * @cfg {Object} serviceParams The parameters for calling the serviceStart. If an MS AJAX proxy call - these will be method parameters. If a Form post - these will be form fields.
             */
            serviceParams                     : {},
            /**
             * @cfg {Boolean} checkProgressBeforeServerResponse Determines whether progress will be checked even before the server responds to the serviceStart original request or not.
             */
            checkProgressBeforeServerResponse : false,
            /**
             * @cfg {Function} checkProgressMethod The service to call to query for progress.
             */
            checkProgressMethod               : null,
            /**
             * @cfg {Number} pollingInterval The interval for polling progress.
             */
            pollingInterval                   : 2000,
            /**
             * @cfg {Number} maxPollingTime The maximum wait time for long-running operation completion.
             */
            maxPollingTime                    : 30 * 60 * 1000, // 30 minutes
            /**
             * @cfg {Function} progressHandler The handler for progress. The consumer can supply this method and write his own code to handle progress, such as displaying information about it in the UI.
             */
            progressHandler                   : null,
            /**
             * @cfg {Function} completionHandler The handler for completion. The consumer can supply this method and write his own code to handle completion, such as displaying information about it in the UI.
             */
            completionHandler                 : null,
            /**
             * @cfg {Function} resultHandler The handler for an available result. It will be called after a result is retreived from the serviceResult call. It will only be called if isFileResult=false.
             */
            resultHandler                     : null,
            /**
             * @cfg {Function} failureHandler The handler for errors in the long-running operation. This function can be called in two cases: 1. When either an error occured in calling services: The response and options parameters will be populated. 2. When the long-running service experienced an error, and we got its information when querying for progress: The exceptionMessage parameter will be populated.
             */
            failureHandler                    : null,
            /**
             * @cfg {Boolean} allowCancel Determines whether the operation can be canceled or not
             */
            allowCancel                       : false,
            /**
             * @cfg {Boolean} getResultIfCanceled Determines whether to get the operation result even if cancelled (for example, the output until cancelled.)
             */
            getResultIfCanceled               : false,
            /**
             * @cfg {Boolean} allowEmailResult Determines whether the operation can done completely in the background and email the result
             */
            allowEmailResult                  : false,
            /**
             * @cfg {Function} updateMethod The service to call to update the operation with a client request.
             */
            updateMethod                      : null,
            /**
             * @cfg {Function} cancelationHandler The handler for cancelation.
             */
            cancelationHandler                : function() {
                // Do nothing
            },
            /**
             * @cfg {Object} scope The scope in which to call the handler methods.
             */
            scope                             : this,

            /**
             * @private {Object} task The task object for this submitter. A submitter can only submit once, and this is when the task is created.
             */
            task                              : null,
            /**
             * @private {Boolean} isAborted Determines whether the submitting has aborted or not. This can be either because of failure or user-cancelation.
             */
            isAborted                         : false,

            // CTor
            constructor                       : function(config) {
                config = config || {};

                var defaultConfig = {
                    serviceResult       : (config.isFileResult === true) ? Viz.Services.CoreWCF.GetLongRunningOperationResultFile : Viz.Services.CoreWCF.GetLongRunningOperationResultEntity,
                    checkProgressMethod : Viz.Services.CoreWCF.GetLongRunningOperationProgress,
                    updateMethod        : Viz.Services.CoreWCF.UpdateLongRunningOperation,
                    progressHandler     : function(progressObject) {
                        // Do nothing
                    },
                    completionHandler   : function(progressObject) {
                        // Do nothing
                    },
                    resultHandler       : function(result) {
                        // Do nothing
                    },
                    failureHandler      : function(response, options, exceptionMessage) {
                        if (response) {
                            Ext.MessageBox.alert('Error', response);
                        }
                        else if (exceptionMessage) {
                            Ext.MessageBox.alert('Error', exceptionMessage);
                        }
                    },
                    cancelationHandler  : function() {
                        // Do nothing
                    }
                };
                Ext.applyIf(config, defaultConfig);

                var minimalTimeToWait = 2000;
                var forcedConfig = {
                    // We set the value of operation ID to make sure the consumer doesn't give us a fixed ID
                    // and not a random GUID by mistake, as this may cause conflicting progress reports.
                    operationId                 : new Ext.ux.GUID().id,
                    // Start polling only after a minimal time
                    timeToWaitUntilFirstPolling : config.pollingInterval > minimalTimeToWait ? config.pollingInterval : minimalTimeToWait
                };

                Ext.apply(config, forcedConfig);

                // This is usually done by Ext.Component, but we don't inherit from it.
                Ext.apply(this, config);

                Viz.Services.LongRunningOperationSubmitter.superclass.constructor.call(this, config);
            },

            /**
             * @private Shows the MessageBox for the progress using the given config
             * @param showConfig
             */
            showProgressMessageBox            : function(showConfig) {
                var defaultConfig = {
                    title    : this.progressWindowTitle,
                    minWidth : Ext.MessageBox.minProgressWidth,
                    modal    : this.modal,
                    wait     : !this.showProgressPercentage,
                    progress : this.showProgressPercentage === true,
                    buttons  : {
                        cancel : (this.allowCancel === true && this.isAborted === false) ? $lang('msg_cancel') : null,
                        ok     : (this.allowEmailResult === true && this.isAborted === false) ? $lang('msg_long_running_operation_email_result') : null
                    },
                    fn       : (this.allowCancel === true || this.allowEmailResult === true) ? this.handleCancelation.createDelegate(this) : null
                };

                var msgboxConfig = Ext.applyIf(showConfig, defaultConfig);

                Ext.MessageBox.show(msgboxConfig);
            },

            /**
             * @private Starts the long-running operation by calling the serviceStart and performing a recurring progress check.
             */
            start                             : function() {
                if (this.task) {
                    // we can't do this, already started one task.
                    return;
                }

                try {
                    this.isAborted = false;
                    if (this.showProgress == true) {
                        Ext.MessageBox.hide();
                        this.showProgressMessageBox({
                                    msg          : $lang('msg_polling_start'),
                                    progressText : this.showProgressPercentage === true ? '0%' : ''
                                });
                    }

                    // Call the server start method. This request may take a while if we do a file upload.
                    // This method must be overriden in subclasses.
                    this.callStartMethod(this.operationId, function() {
                                // If this is not a streaming upload, then the server code will run only after the request
                                // has been fully sent. We should receive response quickly as the service method should be OneWay.
                                if (this.checkProgressBeforeServerResponse === false) {
                                    this.startPolling.defer(this.timeToWaitUntilFirstPolling, this);
                                }
                            }.createDelegate(this), this.handleFailure.createDelegate(this));

                    if (this.checkProgressBeforeServerResponse === true) {
                        // If this is a ing upload, the server will not wait for the whole file
                        // to be received to start running the method, so it will register the long running process soon.
                        // Start the newly created task after we submitted the request, without waiting for a response.
                        this.startPolling.defer(this.timeToWaitUntilFirstPolling, this);
                    }
                }
                catch (e) {
                    this.handleFailure(e, null, null);
                }
            },

            /**
             * @private Starts the progress polling recurring task.
             */
            startPolling                      : function() {
                this.task = {
                    interval : this.pollingInterval,
                    scope    : this,
                    duration : this.maxPollingTime,
                    run      : this.checkProgress
                };

                // Check that in the second between sending the first request and starting to check
                // for progress, the first request hasn't failed.
                if (this.isAborted === false) {
                    Ext.TaskMgr.start(this.task);
                }
            },

            /**
             * @private Checks for progress by calling the server and examining the returned progress object. This is the logic that runs in the recurring task.
             */
            checkProgress                     : function() {
                this.checkProgressMethod({
                            operationId : this.operationId,
                            success     : function(progressObject) {
                                if (this.isAborted !== false) {
                                    return;
                                }

                                if (progressObject.Status == Vizelia.FOL.BusinessEntities.LongRunningOperationStatus.Initialized || progressObject.Status == Vizelia.FOL.BusinessEntities.LongRunningOperationStatus.InProgress) {
                                    this.handleProgress(progressObject);
                                }
                                else if (progressObject.Status == Vizelia.FOL.BusinessEntities.LongRunningOperationStatus.Completed || progressObject.Status == Vizelia.FOL.BusinessEntities.LongRunningOperationStatus.ResultAvailable) {
                                    this.handleCompletion(progressObject);
                                }
                                else if (progressObject.Status == Vizelia.FOL.BusinessEntities.LongRunningOperationStatus.Exception) {
                                    this.handleFailure(null, null, progressObject.Label);
                                }
                                // Ignore the cancel status, as this may occur if a checkProgressMethod service call was performed while
                                // cancelling the operation. We handle cancelation in the handleCancelation method.

                            }.createDelegate(this),
                            failure     : this.handleFailure.createDelegate(this)
                        });
            },

            /**
             * @private Handles progress in the server side.
             */
            handleProgress                    : function(progressObject) {
                // There may be a case the user cancelled or there was an error, and the progress task was already initiated.
                if (this.isAborted === true) {
                    return;
                }

                if (this.showProgress) {
                    if (this.showProgressPercentage === true) {
                        // Update the message box text and the progress bar.
                        Ext.MessageBox.updateProgress(progressObject.PercentDone / 100.0, progressObject.PercentDone + '%', progressObject.Label);
                    }
                    else {
                        // Update only the message box text.
                        Ext.MessageBox.updateText(progressObject.Label);
                    }
                }

                this.progressHandler.apply(this.scope, arguments);
            },

            /**
             * @private Handles the completion of the long-running operation.
             */
            handleCompletion                  : function(progressObject) {
                this.stopPolling();
                // Notify the UI of completion
                if (this.showProgress === true) {
                    Ext.MessageBox.updateProgress(1, '', $lang('msg_polling_end'));
                    Ext.MessageBox.hide();
                }

                // Do we need to get the result from the server?
                if (progressObject.Status == Vizelia.FOL.BusinessEntities.LongRunningOperationStatus.ResultAvailable) {
                    // If configured to do so, download the resulting file. The browser will open it.
                    if (this.isFileResult === true) {
                        // We are using the generated wrappers for the MS AJAX proxies in order
                        // to get the URL we need to submit the form to.

                        var info = this.serviceResult.getInfo();
                        var url = info.url + '?operationId=' + this.operationId;
                        // If this is IE8, then downloading without user interaction causes the annoying strip at the top.
                        // This is why in IE8 we create a window with a download button to trigger the download.
                        if (!Ext.isIE8) {
                            Viz.downloadFile(url);
                            this.completionHandler.apply(this.scope, arguments);
                        }
                        else {
                            Ext.MessageBox.show({
                                        title    : this.progressWindowTitle,
                                        msg      : $lang('msg_polling_end'),
                                        minWidth : Ext.MessageBox.minProgressWidth,
                                        modal    : this.modal,
                                        buttons  : {
                                            ok     : $lang('msg_document_download'),
                                            cancel : $lang('msg_cancel')
                                        },
                                        fn       : function(btn) {
                                            if (btn == 'ok') {
                                                Viz.downloadFile(url);
                                            }

                                            this.completionHandler.apply(this.scope, arguments);
                                        }.createDelegate(this)
                                    });
                        }
                    }
                    // If configured to do so, get the result from the result web service and call the result handler.
                    else if (this.resultHandler) {
                        // Call the result service with the operation ID and the other service params
                        this.serviceResult(Ext.applyIf({
                                    operationId : this.operationId,
                                    success     : function(result) {
                                        this.resultHandler.call(this.scope, result);
                                    }.createDelegate(this),
                                    failure     : this.handleFailure.createDelegate(this)
                                }, this.serviceParams));
                    }
                }

                // Is the operation complete and has no return value we need to query for?
                if (progressObject.Status == Vizelia.FOL.BusinessEntities.LongRunningOperationStatus.Completed) {
                    this.completionHandler.apply(this.scope, arguments);
                }

            },

            /**
             * @private Handles the failure of the long-running operation.
             * @param response Populated only if an error occured while calling any service (serviceStart, checkProgressMethod, serviceResult).
             * @param options Populated only if an error occured while calling any service (serviceStart, checkProgressMethod, serviceResult).
             * @param exceptionMessage Populated if the server has experienced an exception in the long-running operation.
             */
            handleFailure                     : function(response, options, exceptionMessage) {
                this.isAborted = true;
                // Stop the Task if it's already running.
                // TODO: Need to put a lock on this and the task starting.
                if (this.task != null) {
                    this.stopPolling();
                }

                if (this.showProgress === true) {
                    Ext.MessageBox.hide();
                }

                this.failureHandler.apply(this.scope, arguments);
            },

            /**
             * @private Handles cancellation
             */
            handleCancelation                 : function(btn) {
                if (this.isAborted === true) {
                    // The user might have clicked twice fast.
                    return;
                }

                this.isAborted = true;

                var clientRequest = null;
                if (btn == 'ok') {
                    clientRequest = Vizelia.FOL.BusinessEntities.LongRunningOperationClientRequest.EmailResult;
                }
                if (btn == 'cancel') {
                    clientRequest = Vizelia.FOL.BusinessEntities.LongRunningOperationClientRequest.Cancel;
                }

                if (this.getResultIfCanceled !== true) {
                    // We are not interested in the result, stop polling.
                    Ext.MessageBox.hide();
                    this.stopPolling();
                }

                // Call server-side method to notify about client-update
                this.updateMethod({
                            operationId   : this.operationId,
                            clientRequest : clientRequest,
                            failure       : this.handleFailure.createDelegate(this),
                            success       : function() {
                                this.cancelationHandler.call(this.scope);
                            }.createDelegate(this)
                        });
            },

            /**
             * @private Stops the progress polling recurring task.
             */
            stopPolling                       : function() {
                this.isAborted = true;
                if (this.task) {
                    Ext.TaskMgr.stop(this.task);
                }
            }

        });

/**
 * A class responsible for submitting data to long-running operations on the server by calling the service method and sending objects.
 * @class Viz.Services.ObjectLongRunningOperationSubmitter
 * @extends Viz.Services.LongRunningOperationSubmitter
 */
Viz.Services.ObjectLongRunningOperationSubmitter = Ext.extend(Viz.Services.LongRunningOperationSubmitter, {
            constructor     : function(config) {
                Viz.Services.ObjectLongRunningOperationSubmitter.superclass.constructor.call(this, config);
            },

            /**
             * Calls the service method to start the long-running operation.
             */
            callStartMethod : function(operationId, onSuccess, onFail) {
                var serviceConfig = {
                    operationId : operationId,
                    success     : onSuccess,
                    failure     : onFail
                };
                Ext.applyIf(serviceConfig, this.serviceParams);
                this.serviceStart(serviceConfig);
            }

        });

/**
 * A class responsible for submitting data to long-running operations on the server by sending a form to the service URL.
 * @class Viz.Services.StreamLongRunningOperationSubmitter
 * @extends Viz.Services.LongRunningOperationSubmitter
 */
Viz.Services.StreamLongRunningOperationSubmitter = Ext.extend(Viz.Services.LongRunningOperationSubmitter, {
            /**
             * @private {Boolean} isFormAutoCreated Holds information of whether a form was automatically created by this class since it was not specified in the config, or not. If it was auto-created, it will be destroyed when the long-running operation finishes for any reason (completion, result or error).
             */
            isFormAutoCreated : false,

            constructor       : function(config) {

                config = config || {};
                var defaultConfig = {
                    checkProgressBeforeServerResponse : true,
                    serviceParams                     : {}
                };
                Ext.applyIf(config, defaultConfig);

                if (!config.form) {
                    // We create a blank form since we need to call the server method
                    // using a Stream for input. The parameters will be added to the form when submitting.
                    config.form = new Ext.FormPanel({
                                renderTo : Ext.getBody(),
                                hidden   : true
                            });
                    this.isFormAutoCreated = true;
                }

                // If the parameters contain column objects, we need to take only specific
                // properties of them, in order to avoid infinite recursion when seriazling them.
                // Serializing the ExtJS column objects will cause an exception since they cyclicly refer themselves.
                for (key in config.serviceParams) {
                    if (key == 'columns') {
                        var columns = [];
                        Ext.each(config.serviceParams[key], function(col, index) {
                                    columns.push({
                                                dataIndex : col.dataIndex,
                                                header    : col.headerText || col.header,
                                                width     : col.width
                                            });
                                });
                        config.serviceParams[key] = columns;
                    }
                }

                // Since this is form submittal, parameters will be added as inputs to the form.
                // However if they are objects, we must first represent them in JSON so the server can deserialize them.
                for (key in config.serviceParams) {
                    if (typeof config.serviceParams[key] == "object")
                        config.serviceParams[key] = Ext.encode(config.serviceParams[key]);
                }

                Viz.Services.StreamLongRunningOperationSubmitter.superclass.constructor.call(this, config);
            },

            /**
             * @private An override for that destroys an auto-created form if needed before the superclass executes its own implementation for the function
             */
            handleCompletion  : function(progressObject) {
                if (this.isFormAutoCreated === true) {
                    this.form.destroy();
                }
                Viz.Services.StreamLongRunningOperationSubmitter.superclass.handleCompletion.apply(this, arguments);
            },

            /**
             * @private An override for that destroys an auto-created form if needed before the superclass executes its own implementation for the function
             */
            handleFailure     : function(response, options, exceptionMessage) {
                if (this.isFormAutoCreated === true) {
                    this.form.destroy();
                }
                Viz.Services.StreamLongRunningOperationSubmitter.superclass.handleFailure.apply(this, arguments);
            },

            /**
             * @private Calls the service method to start the long-running operation by submitting the form. Will be called by superclass.
             */
            callStartMethod   : function(operationId, onSuccess, onFail) {
                // We are using the generated wrappers for the MS AJAX proxies in order
                // to get the URL we need to submit the form to.
                var info = this.serviceStart.getInfo();
                var url = info.url;
                url += (url.indexOf('?') != -1 ? '&' : '?') + "PAGEID" + '=' + Viz.pageId;

                // If we got a FormPanel, then get its internal HTML form using the getForm() method.
                var formToSubmit = this.form.getForm ? this.form.getForm() : this.form;

                Ext.Ajax.request({
                            id       : this.operationId,
                            form     : formToSubmit.getEl().dom,
                            url      : url,
                            method   : 'POST',
                            isUpload : true,
                            success  : onSuccess,
                            failure  : onFail,
                            params   : Ext.applyIf({
                                        operationId : this.operationId
                                    }, this.serviceParams)
                        });
            }

        });

/**
 * @namespace Viz.Services
 * @class Viz.Services
 * @method BuildUrlStream
 * @static
 * <p>
 * Build a url for receiving a stream from a WCF service.
 * </p>
 * @param {Object} AjaxServiceInterface the fully qualified ajax interface (www.vizelia.com.wcf._2011._01.IEnergyWCF).
 * @param {String} methodName The method name returning a stream.
 * @param {Object} params The parameters object of the service.
 * @param {Boolean} (optional) disableCaching true to indicate that caching should be disabled, false otherwise.
 */
Viz.Services.BuildUrlStream = function(ajaxServiceInterface, methodName, params, disableCaching) {
    params.PAGEID = Viz.pageId;
    var servicePath = ajaxServiceInterface;
    var url = Sys.Net.WebRequest._createUrl(methodName ? (servicePath + "/" + encodeURIComponent(methodName)) : servicePath, params, null);
    if (disableCaching == true)
        return url;
    else {
        var dcp = "_dc";
        url += (url.indexOf('?') != -1 ? '&' : '?') + dcp + '=' + (new Date().getTime());
    }
    return url;
};
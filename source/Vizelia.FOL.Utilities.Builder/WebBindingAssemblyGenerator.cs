﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Linq;

using System.Xml.Linq;
using System.Configuration;
using System.Web.Configuration;
using System.Security.Permissions;
using Vizelia.FOL.Infrastructure;

namespace Vizelia.FOL.Utilities.Builder {
	/// <summary>
	/// A Build Task for Web Assembly Binding.
	/// </summary>
	public class WebBindingAssemblyGenerator : Task {

        /// <summary>
        /// The Assembly names to process
        /// </summary>
        [Required]
        public string AssemblyNames { get; set; }

        /// <summary>
		/// The path for bin directory.
		/// </summary>
		[Required]
		public string AssemblyDirectory { get; set; }

        /// <summary>
		/// The path for the config file
		/// </summary>
		[Required]
		public string ConfigFile { get; set; }

		/// <summary>
		/// Main function.
		/// </summary>
		/// <returns></returns>
		[EnvironmentPermissionAttribute(SecurityAction.Demand, Unrestricted = true)]
		public override bool Execute() {
			// Debugger.Launch();
			try {
				AppDomain.CurrentDomain.AssemblyResolve += CurrentDomainAssemblyResolve;
				BuildAssemblyBindingVersion();
				return true;
			}
			catch (Exception ex) {
				Log.LogWarning("An error occured when processing task {0}. The error is : {1}.", GetType().Name, ex.Message);
				return true;
			}
		}

        /// <summary>
		/// Builds the assembly binding section.
		/// </summary>
        private void BuildAssemblyBindingVersion()
        {
            var binDirectory = new DirectoryInfo(AssemblyDirectory);

            var nameList = AssemblyBindingNameList();

            var configPath  =  Path.Combine(Environment.CurrentDirectory, ConfigFile);

            var doc = XDocument.Load(configPath);
            if (doc != null)
            {
                foreach (XElement xe in doc.Descendants("{urn:schemas-microsoft-com:asm.v1}dependentAssembly"))
                {
                    var assemblyIdentity = xe.Element("{urn:schemas-microsoft-com:asm.v1}assemblyIdentity");
                    if (assemblyIdentity != null && assemblyIdentity.Attribute("name") != null && nameList.Contains(assemblyIdentity.Attribute("name").Value))
                    {
                        // Update Assembly Binding Version Info
                        var assemblyname = assemblyIdentity.Attribute("name").Value;
                        var ass = Assembly.LoadFrom(binDirectory.FullName + "\\" + assemblyname + ".dll");
                        if (ass != null)
                        {
                            string version = ass.GetName().Version.ToString();

                            var bindingRedirect = xe.Element("{urn:schemas-microsoft-com:asm.v1}bindingRedirect");
                            if (bindingRedirect != null)
                            {
                                bindingRedirect.Attribute("oldVersion").Value = "1.0.0.0-" + version;
                                bindingRedirect.Attribute("newVersion").Value = version;
                            }
                        }
                    }
                }

                doc.Save(configPath, SaveOptions.None);
            }
        }

        /// <summary>
        /// Handler for resolving assembly reference.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private Assembly CurrentDomainAssemblyResolve(object sender, ResolveEventArgs args)
        {
            var parts = args.Name.Split(',');
            var file = AssemblyDirectory + "\\" + parts[0].Trim() + ".dll";
            if (File.Exists(file))
            {
                var asm = Assembly.LoadFrom(file);
                return asm;
            }
            return null;
        }

        /// <summary>
        /// AssemblyBindingNameList
        /// </summary>
        /// <returns></returns>
        private List<string> AssemblyBindingNameList()
        {
            var nameList = new List<string>();
            if (!String.IsNullOrEmpty(AssemblyNames))
            {
                var names = AssemblyNames.Split(',');
                foreach (var assembly in names)
                {
                    var name = assembly.Trim();
                    if (!String.IsNullOrEmpty(name))
                    {
                        nameList.Add(name);
                    }
                }
            }
            return (nameList);
        }
	}
}

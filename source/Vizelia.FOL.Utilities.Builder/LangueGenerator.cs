﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System.Diagnostics;
using System.Resources;
using System.Linq;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Permissions;
using Vizelia.FOL.Infrastructure;

namespace Vizelia.FOL.Utilities.Builder {
	/// <summary>
	/// A Build Task for generating langue resources files.
	/// Will translate any language message expect those starting with 'langue_'
	/// </summary>
	public class LangueGenerator : Task {

		/// <summary>
		/// The path of the web project.
		/// </summary>
		[Required]
		public string WebProjectPath { get; set; }

		/// <summary>
		/// The path for bin directory.
		/// </summary>
		[Required]
		public string AssemblyDirectory { get; set; }

		/// <summary>
		/// A comma separated list of 2 or 4 chars supported language.
		/// </summary>
		[Required]
		public string SupportedLanguages { get; set; }

		/// <summary>
		/// Handler for resolving assembly reference.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args) {
			string[] parts = args.Name.Split(',');
			string file = this.AssemblyDirectory + "\\" + parts[0].Trim() + ".dll";
			if (File.Exists(file)) {
				Assembly asm = Assembly.LoadFrom(file);
				return asm;
			}
			else
				return null;
		}

		/// <summary>
		/// Main function.
		/// </summary>
		/// <returns></returns>
		[EnvironmentPermissionAttribute(SecurityAction.Demand, Unrestricted = true)]
		public override bool Execute() {
			//Debugger.Launch();
			try {
				AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
				if (!CheckTranslationService())
					return true;
				DirectoryInfo d = new DirectoryInfo(Environment.CurrentDirectory);
				string[] languages = SupportedLanguages.Split(',');
				foreach (string langue in languages) {
					FileInfo[] resxFiles = d.GetFiles("Langue." + langue + ".resx", SearchOption.AllDirectories);
					if (resxFiles.Count() == 0) {
						GenerateResourceFile(langue);
					}
					else
						CompareResourceFile(langue);
				}
				return true;
			}
			catch (Exception ex) {
				this.Log.LogWarning("An error occured when processing task {0}. The error is : {1}.", this.GetType().Name, ex.Message);
				return true;
			}
		}

		/// <summary>
		/// Checks the status of the translation service.
		/// </summary>
		/// <returns>True if service is available, false otherwise.</returns>
		private bool CheckTranslationService() {
			string result;
			try {
				result = TranslationHelper.TranslateText("hello", "en", "fr");
				if (!String.IsNullOrEmpty(result)) {
					this.Log.LogMessage("LangueGenerator : Translation service is available");
					return true;
				}
				else
					return false;
			}
			catch (Exception ex) {
				this.Log.LogWarning("LangueGenerator : An error occured when accessing translation service. The error is : {0}.", ex.Message);
				return false;
			}
		}

		/// <summary>
		/// Generates a resource file for a specific langue.
		/// </summary>
		/// <param name="langue">The langue (2 chars).</param>
		private void GenerateResourceFile(string langue) {
			this.Log.LogMessage("LangueGenerator : Generating langue for " + langue);

			string resxFilePath = Environment.CurrentDirectory + "\\Langue.resx";
			string outputFilePath = String.Format(Environment.CurrentDirectory + "\\{0}.{1}{2}", Path.GetFileNameWithoutExtension(resxFilePath), langue, Path.GetExtension(resxFilePath));
			string langue_direction = GetTextDirection(langue);

			XmlDocument doc = new XmlDocument();
			doc.Load(resxFilePath);
			XmlNodeList dataNodes = doc.SelectNodes("//root/data");
			int totalKeys = dataNodes.Count;
			using (ResXResourceReader rr = new ResXResourceReader(resxFilePath)) {
				using (ResXResourceWriter rw = new ResXResourceWriter(outputFilePath)) {
					IDictionaryEnumerator di = rr.GetEnumerator();
					foreach (DictionaryEntry de in rr) {
						//Debug.Print(de.Key.ToString());
						string key = de.Key as string;
						string value = de.Value as string;

						if (!String.IsNullOrEmpty(key) && !String.IsNullOrEmpty(value)) {
							string translatedValue = TranslationHelper.TranslateText(value, "en", langue);

							if (key == "langue_direction")
								translatedValue = langue_direction;
							else if (key == "msg_application_title" || key.StartsWith("langue_")) {
								translatedValue = value;
							}

							rw.AddResource(key, translatedValue);
						}
					}
					rw.Generate();
				}
			}
		}

		/// <summary>
		/// Updates an existing resource file for a specific langue.
		/// </summary>
		/// <param name="langue">The langue (2 chars).</param>
		private void CompareResourceFile(string langue) {
			this.Log.LogMessage("LangueGenerator : Comparing langue for " + langue);

			string resxFilePath = Environment.CurrentDirectory + "\\Langue.resx";
			string outputFilePath = String.Format(Environment.CurrentDirectory + "\\{0}.{1}{2}", Path.GetFileNameWithoutExtension(resxFilePath), langue, Path.GetExtension(resxFilePath));
			string langue_direction = GetTextDirection(langue);
			XmlDocument doc = new XmlDocument();
			doc.Load(resxFilePath);
			XmlNodeList dataNodes = doc.SelectNodes("//root/data");
			int totalKeys = dataNodes.Count;
			Dictionary<string, string> keys = GetKeysFromResource(outputFilePath);
			using (ResXResourceReader rr = new ResXResourceReader(resxFilePath)) {
				using (ResXResourceWriter rw = new ResXResourceWriter(outputFilePath)) {
					IDictionaryEnumerator di = rr.GetEnumerator();
					foreach (DictionaryEntry de in rr) {
						string key = de.Key as string;
						string value = de.Value as string;
						
						if (!String.IsNullOrEmpty(key) && !String.IsNullOrEmpty(value)) {
							if (key == "langue_direction") {
								rw.AddResource(key, langue_direction);
							}
							else if (!keys.Keys.Contains(key)) {
								string translatedValue;

								if (key == "msg_application_title" || key.StartsWith("langue_")) {
									translatedValue = value;
								}
								else {
									translatedValue = TranslationHelper.TranslateText(value, "en", langue);
								}

								rw.AddResource(key, translatedValue);
							}
							else {
								rw.AddResource(key, keys[key]);
							}
						}
					}
					rw.Generate();
				}

			}
		}

		/// <summary>
		/// Gets the dictionary of keys defined in an existing resource file.
		/// </summary>
		/// <param name="resxFilePath">The full file name for the resource file.</param>
		/// <returns>The dictionary of keys as string.</returns>
		private Dictionary<string, string> GetKeysFromResource(string resxFilePath) {
			Dictionary<string, string> keys = new Dictionary<string, string>();
			using (ResXResourceReader rr = new ResXResourceReader(resxFilePath)) {
				IDictionaryEnumerator di = rr.GetEnumerator();
				foreach (DictionaryEntry de in rr) {
					keys.Add(de.Key.ToString(), de.Value.ToString());
				}
			}
			return keys;
		}

		/// <summary>
		/// Retreives the text direction of a specific langue.
		/// </summary>
		/// <param name="langue">The langue (2 chars).</param>
		/// <returns>The direction of the text (rtl or ltr).</returns>
		private string GetTextDirection(string langue) {
			var direction = "ltr";
			CultureInfo[] aCultures = CultureInfo.GetCultures(CultureTypes.NeutralCultures);
			var query = from p in aCultures
						where p.TwoLetterISOLanguageName == langue
						select p;
			if (query.ToList().Count <= 0)
				return direction;
			return query.ToList().First().TextInfo.IsRightToLeft ? "rtl" : "ltr";
		}

	}
}

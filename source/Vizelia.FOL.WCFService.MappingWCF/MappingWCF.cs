﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.WCFService {

	/// <summary>
	/// Implementation for Mapping Services.
	/// </summary>
	//[PolicyInjectionBehavior]
	public class MappingWCF : BaseModuleWCF, IMappingWCF {

		private readonly IMappingBusinessLayer m_MappingBusinessLayer;

		/// <summary>
		/// Public ctor.
		/// </summary>
		public MappingWCF() {
			// Policy injection on the business layer instance
			m_MappingBusinessLayer = Helper.CreateInstance<MappingBusinessLayer, IMappingBusinessLayer>();
		}

		/// <summary>
		/// Starts the mapping process.
		/// </summary>
		/// <param name="item">The mapping request item.</param>
		/// <param name="operationId">The operation id.</param>
		public void BeginMapping(FileUploadRequest item, Guid operationId) {
			Document document = DocumentHelper.RetrieveDocument(item.InputFile);
			var documentStream = new MemoryStream(document.DocumentContents);

			m_MappingBusinessLayer.BeginMapping(operationId, document.DocumentName, documentStream);
		}

		/// <summary>
		/// Starts the mapping process.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="contents">The contents.</param>
		public Guid BeginMapping(string filename, Stream contents) {
			Guid operationId = Guid.NewGuid();
			// We fear the contents stream will be disposed when the request is gone, so we copy it to a different stream.
			Stream localStream = new MemoryStream();
			contents.CopyTo(localStream);

			ThreadPool.QueueUserWorkItem(state => {
				try {
					m_MappingBusinessLayer.BeginMapping(operationId, filename, localStream);
				}
				finally {
					localStream.Dispose();
				}
			});

			return operationId;
		}

		/// <summary>
		/// Gets all mappings.
		/// </summary>
		/// <returns></returns>
		public string GetAllMappings() {
			string allMappings = m_MappingBusinessLayer.GetAllMappings();
			return allMappings;
		}
		
		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		public IEnumerable<string> GetAllowedMappingFileExtensions() {
			var allowedExtensions = m_MappingBusinessLayer.GetAllowedMappingFileExtensions();

			return allowedExtensions;
		}

		#region MappingTask

		/// <summary>
		/// Gets a json store for the business entity MappingTask.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<MappingTask> MappingTask_GetStore(PagingParameter paging, PagingLocation location) {
			return m_MappingBusinessLayer.MappingTask_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity MappingTask.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<MappingTask> MappingTask_SaveStore(CrudStore<MappingTask> store) {
			return m_MappingBusinessLayer.MappingTask_SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity MappingTask. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<MappingTask> MappingTask_GetAll(PagingParameter paging, PagingLocation location) {
			return m_MappingBusinessLayer.MappingTask_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity MappingTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public MappingTask MappingTask_GetItem(string Key) {
			return m_MappingBusinessLayer.MappingTask_GetItem(Key);
		}

		/// <summary>
		/// Loads a specific item for the business entity MappingTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse MappingTask_FormLoad(string Key) {
			return m_MappingBusinessLayer.MappingTask_FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity MappingTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse MappingTask_FormCreate(MappingTask item) {
			return m_MappingBusinessLayer.MappingTask_FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity MappingTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse MappingTask_FormUpdate(MappingTask item) {
			return m_MappingBusinessLayer.MappingTask_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse MappingTask_FormUpdateBatch(string[] keys, MappingTask item) {
			return m_MappingBusinessLayer.MappingTask_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity MappingTask.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool MappingTask_Delete(MappingTask item) {
			return m_MappingBusinessLayer.MappingTask_Delete(item);
		}

		#endregion

		#region MeterDataExportTask
		/// <summary>
		/// Gets a json store for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<MeterDataExportTask> MeterDataExportTask_GetStore(PagingParameter paging, PagingLocation location) {
			return m_MappingBusinessLayer.MeterDataExportTask_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<MeterDataExportTask> MeterDataExportTask_SaveStore(CrudStore<MeterDataExportTask> store) {
			return m_MappingBusinessLayer.MeterDataExportTask_SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity MeterDataExportTask. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<MeterDataExportTask> MeterDataExportTask_GetAll(PagingParameter paging, PagingLocation location) {
			return m_MappingBusinessLayer.MeterDataExportTask_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public MeterDataExportTask MeterDataExportTask_GetItem(string Key) {
			return m_MappingBusinessLayer.MeterDataExportTask_GetItem(Key);
		}

		/// <summary>
		/// Loads a specific item for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse MeterDataExportTask_FormLoad(string Key) {
			return m_MappingBusinessLayer.MeterDataExportTask_FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity MeterDataExportTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		public FormResponse MeterDataExportTask_FormCreate(MeterDataExportTask item, CrudStore<Meter> meters) {
			return m_MappingBusinessLayer.MeterDataExportTask_FormCreate(item, meters);
		}

		/// <summary>
		/// Updates an existing business entity MeterDataExportTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		public FormResponse MeterDataExportTask_FormUpdate(MeterDataExportTask item, CrudStore<Meter> meters) {
			return m_MappingBusinessLayer.MeterDataExportTask_FormUpdate(item, meters);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse MeterDataExportTask_FormUpdateBatch(string[] keys, MeterDataExportTask item) {
			return m_MappingBusinessLayer.MeterDataExportTask_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity MeterDataExportTask.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool MeterDataExportTask_Delete(MeterDataExportTask item) {
			return m_MappingBusinessLayer.MeterDataExportTask_Delete(item);
		}

		#endregion

		/// <summary>
		/// Tests the connection to the file source.
		/// This is a long running operation that returns a business object.
		/// </summary>
		/// <param name="keyMappingTask">The key of the mapping task.</param>
		/// <param name="operationId">The operation id.</param>
		public void BeginTestConnection(string keyMappingTask, Guid operationId) {
			m_MappingBusinessLayer.BeginTestConnection(keyMappingTask, operationId);
		}

		/// <summary>
		/// Gets a list of the given business entity, ready for export.
		/// </summary>
		/// <param name="entityType">Type of the entity, expressed as the class name.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public List<BaseBusinessEntity> GetEntitiesForExport(string entityType, PagingParameter paging) {
			List<BaseBusinessEntity> entitiesForExport = m_MappingBusinessLayer.GetEntitiesForExport(entityType, paging);
			return entitiesForExport;
		}
		/// <summary>
		/// Maps the excel to XML.
		/// </summary>
		/// <param name="fileStream">The file stream.</param>
		/// <returns></returns>
		public Guid MapExcelToXml(Stream fileStream) {
			var operationId = m_MappingBusinessLayer.MapExcelToXml(fileStream);
			return operationId;
		}

		/// <summary>
		/// Starts the mapping process.
		/// </summary>
		/// <param name="item">The mapping request item.</param>
		/// <param name="operationId">The operation id.</param>
		public void BeginMappingFileConversion(FileUploadRequest item, Guid operationId) {
			Document document = DocumentHelper.RetrieveDocument(item.InputFile);
			var documentStream = new MemoryStream(document.DocumentContents);

			m_MappingBusinessLayer.MapExcelToXml(documentStream, operationId);
		}
	}
}

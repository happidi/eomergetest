﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Transactions;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Providers;


namespace Vizelia.FOL.Security {

	/// <summary>
	/// The FOL MembershipProvider
	/// Derives from SqlMembershipProvider
	/// </summary>
	public class FOLSqlMembershipProvider : SqlMembershipProvider, IFOLSqlMembershipProvider {
		private const string const_attribute_passwordhistorycount = "passwordHistoryCount";
		private const string const_attribute_autounlocktimeout = "autoUnlockTimeout";
		private const string const_attribute_autolocktimeout = "autoLockTimeout";
		private const string const_attribute_passwordexpiration = "passwordExpiration";
		private const string const_attribute_passwordexpirationafterforgotpassword = "passwordExpirationAfterForgotPassword";
		private const string const_attribute_changepasswordwindow = "changePasswordWindow";

		private int _passwordHistoryCount = 5; // defaults to 5 
		private int _autoUnlockTimeout = 60; // defaults to 60 mins (O indicates no autoUnlock)
		private int _changePasswordWindow = 0; // defaults to 0, disabled.
		private int _autoLockTimeout = 60 * 24 * 60; // default to 2 months (0 indicates no autoLock)
		private int _passwordExpiration = 60 * 24 * 60; // default to 2 months (0 indicates that the passowrd never expires)
		private int _passwordExpirationAfterForgotPassword = 24; // default to 24 hours (0 indicates that the passowrd never expires)




		/// <summary>
		/// The name of the stored procedure that logs the user in LoginHistory.
		/// </summary>
		private const string const_proc_addloginhistory = AppModule.Membership + "_AddLoginHistory";

		/// <summary>	
		/// The name of the stored procedure that deletes a user.
		/// </summary>
		private const string const_proc_deleteuser = AppModule.Membership + "_DeleteUser";

		/// <summary>	
		/// The name of the stored procedure that gets FOL MembershipUser from a standard MembershipUser.
		/// </summary>
		private const string const_proc_getuserbyuserid = AppModule.Membership + "_GetUserByUserId";

		/// <summary>	
		/// The name of the stored procedure that changes the username of user.
		/// </summary>
		private const string const_proc_changeusername = AppModule.Membership + "_ChangeUserName";

		/// <summary>	
		/// The name of the stored procedure that gets the password salt for a user.
		/// </summary>
		private const string const_proc_getpasswordsalt = AppModule.Membership + "_GetPasswordSalt";

		/// <summary>	
		/// The name of the stored procedure that gets the password history for a user.
		/// </summary>
		private const string const_proc_getpasswordhistory = AppModule.Membership + "_GetPasswordHistory";

		/// <summary>	
		/// The name of the stored procedure that updates a user.
		/// </summary>
		private const string const_proc_updateuser = AppModule.Membership + "_UpdateUser";

		/// <summary>	
		/// The name of the stored procedure that create a new user.
		/// </summary>
		private const string const_proc_createuser = AppModule.Membership + "_CreateUser";

		/// <summary>	
		/// The name of the stored procedure that gets a password for an existing user.
		/// </summary>
		private const string const_proc_aspnet_getpasswordwithformat = "aspnet_Membership_GetPasswordWithFormat";

		/// <summary>	
		/// The name of the stored procedure that change a password for an existing user.
		/// </summary>
		private const string const_proc_aspnet_resetpassword = "aspnet_Membership_ResetPassword";

		/// <summary>	
		/// The name of the stored procedure that add a last known password to the history.
		/// </summary>
		private const string const_proc_addpasswordhistory = AppModule.Membership + "_AddPasswordHistory";

		/// <summary>	
		/// The name of the stored procedure that locks a user account.
		/// </summary>
		private const string const_proc_lockuser = AppModule.Membership + "_LockUser";



		private IDataAccess _da;



		/// <summary>
		/// Public ctor.
		/// </summary>
		public FOLSqlMembershipProvider() {
			_da = new DataAccess();

		}

		/// <summary>
		/// Gets or sets the name of the application to store and retrieve membership information for.
		/// </summary>
		/// <returns>The name of the application to store and retrieve membership information for. The default is the <see cref="P:System.Web.HttpRequest.ApplicationPath"/> property value for the current <see cref="P:System.Web.HttpContext.Request"/>.</returns>
		///   
		/// <exception cref="T:System.ArgumentException">An attempt was made to set the <see cref="P:System.Web.Security.SqlMembershipProvider.ApplicationName"/> property to an empty string or null.</exception>
		///   
		/// <exception cref="T:System.Configuration.Provider.ProviderException">An attempt was made to set the <see cref="P:System.Web.Security.SqlMembershipProvider.ApplicationName"/> property to a string that is longer than 256 characters.</exception>
		public override string ApplicationName {
			get {
				string retval = ContextHelper.ApplicationName;
				base.ApplicationName = retval;
				return retval;
			}
			set {

			}
		}

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {

			// retrieve PasswordHistoryCount
			if (!String.IsNullOrEmpty(config[const_attribute_passwordhistorycount])) {
				string s = config[const_attribute_passwordhistorycount];
				int num;
				if (!int.TryParse(s, out num))
					throw new ProviderException(const_attribute_passwordhistorycount + " must be an integer");
				if (num < 0)
					throw new ProviderException(const_attribute_passwordhistorycount + "must be greater than or equal to 0");

				PasswordHistoryCount = num;
				config.Remove(const_attribute_passwordhistorycount);
			}

			// retreive AutoUnlockTimeout
			if (!String.IsNullOrEmpty(config[const_attribute_autounlocktimeout])) {
				string s = config[const_attribute_autounlocktimeout];
				int num;
				if (!int.TryParse(s, out num))
					throw new ProviderException(const_attribute_autounlocktimeout + " must be an integer");
				if (num < 0)
					throw new ProviderException(const_attribute_autounlocktimeout + "must be greater than or equal to 0");

				AutoUnlockTimeout = num;
				config.Remove(const_attribute_autounlocktimeout);
			}



			if (!String.IsNullOrEmpty(config[const_attribute_changepasswordwindow])) {
				string s = config[const_attribute_changepasswordwindow];
				int changePasswordWindow;
				if (!int.TryParse(s, out changePasswordWindow))
					throw new ProviderException(const_attribute_changepasswordwindow + " must be an integer");
				if (changePasswordWindow < 0)
					throw new ProviderException(const_attribute_changepasswordwindow + "must be greater than or equal to 0");

				ChangePasswordWindow = changePasswordWindow;
				config.Remove(const_attribute_changepasswordwindow);
			}



			// retreive AutoUnlockTimeout
			if (!String.IsNullOrEmpty(config[const_attribute_autolocktimeout])) {
				string s = config[const_attribute_autolocktimeout];
				int num;
				if (!int.TryParse(s, out num))
					throw new ProviderException(const_attribute_autolocktimeout + " must be an integer");
				if (num < 0)
					throw new ProviderException(const_attribute_autolocktimeout + "must be greater than or equal to 0");

				AutoLockTimeout = num;
				config.Remove(const_attribute_autolocktimeout);
			}

			// retreive PasswordExpiration
			if (!String.IsNullOrEmpty(config[const_attribute_passwordexpiration])) {
				string s = config[const_attribute_passwordexpiration];
				int num;
				if (!int.TryParse(s, out num))
					throw new ProviderException(const_attribute_passwordexpiration + " must be an integer");
				if (num < 0)
					throw new ProviderException(const_attribute_passwordexpiration + "must be greater than or equal to 0");

				PasswordExpiration = num;
				config.Remove(const_attribute_passwordexpiration);
			}

			// retreive PasswordExpirationAfterForgotPassword
			if (!String.IsNullOrEmpty(config[const_attribute_passwordexpirationafterforgotpassword])) {
				string s = config[const_attribute_passwordexpirationafterforgotpassword];
				int num;
				if (!int.TryParse(s, out num))
					throw new ProviderException(const_attribute_passwordexpirationafterforgotpassword + " must be an integer");
				if (num < 0)
					throw new ProviderException(const_attribute_passwordexpirationafterforgotpassword + "must be greater than or equal to 0");

				PasswordExpirationAfterForgotPassword = num;
				config.Remove(const_attribute_passwordexpirationafterforgotpassword);
			}

			base.Initialize(name, config);

		}

		/// <summary>
		/// The time in minutes that should cause an automatic unlock.
		/// A value of 0 indicates that the behavior is disabled and there should not be any automatic unlock.
		/// </summary>
		public int AutoUnlockTimeout {
			get {
				return _autoUnlockTimeout;
			}
			set {
				_autoUnlockTimeout = value;
			}
		}


		/// <summary>
		/// The time in minutes that should pass before a user can change their password after they have changed it.
		/// </summary>
		public int ChangePasswordWindow {
			get {
				return _changePasswordWindow;
			}
			set {
				_changePasswordWindow = value;
			}
		}


		/// <summary>
		/// The time in minutes that should cause an automatic lock.
		/// A value of 0 indicates that the behavior is disabled and there should not be any automatic lock.
		/// </summary>
		public int AutoLockTimeout {
			get {
				return _autoLockTimeout;
			}
			set {
				_autoLockTimeout = value;
			}
		}

		/// <summary>
		/// The number of last passwords that should be check when assigning a new password.
		/// </summary>
		public int PasswordHistoryCount {
			get {
				return _passwordHistoryCount;
			}
			set {
				_passwordHistoryCount = value;
			}
		}

		/// <summary>
		/// The time in minutes after which password should expire.
		/// </summary>
		public int PasswordExpiration {
			get {
				return _passwordExpiration;
			}
			set {
				_passwordExpiration = value;
			}
		}

		/// <summary>
		/// The time in hours after which password should expire (after forgot password procces).
		/// </summary>
		public int PasswordExpirationAfterForgotPassword {
			get {
				return _passwordExpirationAfterForgotPassword;
			}
			set {
				_passwordExpirationAfterForgotPassword = value;
			}
		}

		/// <summary>
		/// Auto unlocking of locked accounts.
		/// </summary>
		/// <param name="username"></param>
		/// <returns></returns>
		private bool AutoUnlockUser(string username) {
			MembershipUser mu = GetUser(username, false);
			if ((mu != null) &&
			 (mu.IsLockedOut) &&
			 (mu.LastLockoutDate.ToUniversalTime().AddMinutes(
				AutoUnlockTimeout)
					 < DateTime.UtcNow)
				) {
				bool retval = mu.UnlockUser();
				return retval;
			}
			return false; //not locked out in the first place or still in lockout period
		}


		/// <summary>
		/// Auto locking of unused accounts.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns></returns>
		private bool AutoLockUser(string username) {
			MembershipUser mu = GetUser(username, false);
			if ((mu != null) && (!mu.IsLockedOut) && (mu.LastActivityDate.ToUniversalTime().AddMinutes(this.AutoLockTimeout) < DateTime.UtcNow)) {
				bool retval = LockUser(username);
				return retval;
			}
			return false; //not locked or still in available period
		}

		/// <summary>
		/// Validates the credential of a user. 
		/// </summary>
		/// <param name="username">Name of the user to validate.</param>
		/// <param name="password">Password of the user to validate.</param>
		/// <returns>True if successfull, false otherwise.</returns>
		public override bool ValidateUser(string username, string password) {
			// automatically lock the account
			if (AutoLockTimeout > 0) {
				AutoLockUser(username);
			}

			MembershipUser membershipUser = GetUser(username, false);

			if (membershipUser == null) {
				return false;
			}
			//Because validating the user can change the last login date.
			DateTime lastLoginDate = membershipUser.LastLoginDate;

			// Calls SqlMembershipProvier's ValidateUser method to check with the DB.
			bool isUserValid = base.ValidateUser(username, password);

			// checks if the user must change its password.
			if (isUserValid) {
				CheckForgotPasswordTimeFrame(membershipUser);
				var folProviderUserKey = (FOLProviderUserKey)membershipUser.ProviderUserKey;
				if (folProviderUserKey != null && (!folProviderUserKey.NeverExpires || folProviderUserKey.IsForgotPassword)) {
					bool mustChangePassword = PasswordExpired(membershipUser, lastLoginDate);
					if (mustChangePassword) {
						membershipUser.LastLoginDate = lastLoginDate;
						UpdateUser(membershipUser);
						throw new VizeliaException(Langue.error_password_expired);
					}
				}
			}

			bool result = isUserValid;

			// the account may be locked out at this point
			if (AutoUnlockTimeout > 0 && !isUserValid) {
				bool successfulUnlock = AutoUnlockUser(username);
				if (successfulUnlock)
					//re-attempt the login
					result = base.ValidateUser(username, password);
				else
					return false;

			}
			return result;
		}

		private void CheckForgotPasswordTimeFrame(MembershipUser membershipUser) {
			var folProviderUserKey = (FOLProviderUserKey)membershipUser.ProviderUserKey;
			if (folProviderUserKey != null) {
				if (folProviderUserKey.IsForgotPassword && membershipUser.LastPasswordChangedDate.ToUniversalTime().AddHours(PasswordExpirationAfterForgotPassword) < DateTime.UtcNow) {
					throw new VizeliaException(string.Format(Langue.error_msg_forgotpassword_passwordexpired, PasswordExpirationAfterForgotPassword));
				}
			}
		}

		/// <summary>
		/// Logs the user in LoginHistory.
		/// </summary>
		/// <param name="username"></param>
		/// <param name="sessionID"></param>
		/// <param name="loginDate"></param>
		/// <param name="logoutDate"></param>
		public void AddLoginHistory(string username, string sessionID, DateTime? loginDate, DateTime? logoutDate) {
			DbCommand cm = _da.GetStoredProcCommand(const_proc_addloginhistory);
			FOLProfileBase profile = null;
			try {
				profile = FOLProfileBase.GetProfile(username);
			}
			catch { }
			string ip = null;
			if (profile != null && profile.entity != null) {
				ip = profile.entity.IP;
			}
			_da.AddInParameterString(cm, "ApplicationName", ContextHelper.ApplicationName);
			_da.AddInParameterString(cm, "UserName", username);
			_da.AddInParameterString(cm, "SessionID", sessionID);
			_da.AddInParameterString(cm, "IP", ip);
			_da.AddInParameterDateTime(cm, "LoginDate", loginDate);
			_da.AddInParameterDateTime(cm, "LogoutDate", logoutDate);
			_da.ExecuteNonQuery(cm);
		}

		/// <summary>
		/// Gets the user list filtered.
		/// </summary>
		/// <param name="locations">The locations.</param>
		/// <param name="roles">The roles.</param>
		/// <returns></returns>
		public List<FOLMembershipUser> GetUserListFiltered(List<string> locations, List<string> roles) {
			//IDataAccess db = new DataAccess();
			DbCommand cm = _da.GetStoredProcCommand("Role_GetUsersByRoles");
			_da.AddInParameterString(cm, "Application", ContextHelper.ApplicationName);
			_da.AddInParameterXml(cm, "Locations", locations == null ? null : Helper.BuildXml(locations.ToArray()));
			_da.AddInParameterXml(cm, "Roles", roles == null ? null : Helper.BuildXml(roles.ToArray()));
			DataSet ds = _da.ExecuteDataSet(cm);
			return (from DataRow row in ds.Tables[0].Rows
					select new FOLMembershipUser {
						UserName = row.Field<string>("Login"),
						Email = row.Field<string>("ElectronicMailAddresses"),
						LastLoginDate = DateTime.UtcNow,
						ProviderUserKey = new FOLProviderUserKey {
							KeyOccupant = row.Field<string>("KeyOccupant"),
							KeyUser = row.Field<string>("KeyUser"),
							Occupant = new Occupant {
								KeyOccupant = row.Field<string>("KeyOccupant"),
								FirstName = row.Field<string>("FirstName"),
								LastName = row.Field<string>("LastName"),
								Id = row.Field<string>("Id"),
								JobTitle = row.Field<string>("JobTitle"),
								KeyOrganization = row.Field<string>("KeyOrganization")
							}
						},
						ApplicationName = ContextHelper.ApplicationName
					}).ToList();
		}

		/// <summary>
		/// Checks if the password has expired.
		/// </summary>
		/// <param name="membershipUser">The membership user.</param>
		/// <param name="lastLoginDate">The last login date, because the validate user causes the user to login.</param>
		/// <returns>
		/// True if the password has expired, false otherwise.
		/// </returns>
		//private bool PasswordExpired(string username) {
		private bool PasswordExpired(MembershipUser membershipUser, DateTime lastLoginDate) {
			//MembershipUser membershipUser = GetUser(username, false);
			if (membershipUser == null)
				return false;

			if (membershipUser.IsLockedOut || !membershipUser.IsApproved)
				return false;
			if ((PasswordExpiration > 0) &&
				(membershipUser.LastPasswordChangedDate.ToUniversalTime().AddMinutes(PasswordExpiration) < DateTime.UtcNow)) {
				return true;
			}
			var folProviderUserKey = (FOLProviderUserKey)membershipUser.ProviderUserKey;
			if (folProviderUserKey != null && folProviderUserKey.IsForgotPassword) {
				return true;
			}

			// in case the users identifies after a reset of its password.
			var retVal = lastLoginDate < membershipUser.LastPasswordChangedDate;
			return retVal;
		}

		/// <summary>
		/// Deletes a user.
		/// </summary>
		/// <param name="username">Name of the user.</param>
		/// <param name="deleteAllRelatedData">True to delete related data, false otherwise.</param>
		/// <returns>True if operation successfull, false otherwise.</returns>
		public override bool DeleteUser(string username, bool deleteAllRelatedData) {
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				bool result = base.DeleteUser(username, deleteAllRelatedData);
				if (!result)
					return false;
				DbCommand cm = _da.GetStoredProcCommand(const_proc_deleteuser);
				_da.AddInParameterString(cm, "ApplicationName", ContextHelper.ApplicationName);
				_da.AddInParameterString(cm, "UserName", username);
				_da.AddOutParameterInt(cm, "NumTablesDeletedFrom");
				_da.ExecuteNonQuery(cm);
				result = Convert.ToBoolean(_da.GetParameterValue(cm, "NumTablesDeletedFrom"));
				t.Complete();
				return result;
			}
		}

		/// <summary>
		/// Gets the FOL MembershipUser from a standard MembershipUser through it's ProviderUserKey (Guid).
		/// </summary>
		/// <param name="user">The standard MembershipUser.</param>
		/// <returns>The FOL MembershipUser.</returns>
		private MembershipUser GetFolUser(MembershipUser user) {
			if ((user.ProviderUserKey == null) || (user.ProviderUserKey != null && !(user.ProviderUserKey is Guid))) {
				throw new MembershipCreateUserException(MembershipCreateStatus.InvalidProviderUserKey);
			}

			var userKey = new FOLProviderUserKey {
				Guid = (Guid)user.ProviderUserKey
			};
			string username;
			using (var readerUser = _da.ExecuteReader(const_proc_getuserbyuserid, userKey.Guid.ToString())) {
				if (!readerUser.Read()) return null;

				userKey.KeyUser = readerUser.GetValue<string>("KeyUser");
				userKey.KeyOccupant = readerUser.GetValue<string>("KeyOccupant");
				userKey.NeverExpires = readerUser.GetValue<bool>("NeverExpires");
				userKey.IsForgotPassword = readerUser.GetValue<bool>("IsForgotPassword");
				userKey.KeyLocation = readerUser.GetValue<string>("UserKeyLocation");
				userKey.UserLocationLongPath = readerUser.GetValue<string>("UserLocationLongPath");
 				userKey.FirstName = readerUser.GetValue<string>("FirstName");
				userKey.LastName = readerUser.GetValue<string>("LastName");
				username = readerUser.GetValue<string>("UserName");
				var broker = new OccupantBrokerDB();
				userKey.Occupant = broker.FillObjectFromUser(readerUser, "");
			}
			username = username ?? user.UserName;
			userKey.Profile = FOLProfileBase.GetProfile(username).entity;
			var membershipUser = new MembershipUser(user.ProviderName, username, userKey, user.Email, user.PasswordQuestion,
													user.Comment, user.IsApproved, user.IsLockedOut, user.CreationDate,
													user.LastLoginDate, user.LastActivityDate, user.LastPasswordChangedDate,
													user.LastLockoutDate);

			return membershipUser;
		}

		/// <summary>
		/// Gets a user.
		/// </summary>
		/// <param name="providerUserKey">The provider user key.</param>
		/// <param name="userIsOnline">True to update last date connection, false otherwise.</param>
		/// <returns>The membership user.</returns>
		public override MembershipUser GetUser(object providerUserKey, bool userIsOnline) {
			// special case for sliding the user window with better performance.
			var specialArg = providerUserKey as UpdateLastActivityArgument;
			if (specialArg != null)
				return base.GetUser(specialArg.UserName, userIsOnline);

			MembershipUser user = base.GetUser(providerUserKey, userIsOnline);
			if (user == null)
				return null;
			return GetFolUser(user);
		}

		/// <summary>
		/// Gets a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="userIsOnline">True top update last date connection, false otherwise.</param>
		/// <returns>The membership user.</returns>
		public override MembershipUser GetUser(string username, bool userIsOnline) {
			MembershipUser user = base.GetUser(username, userIsOnline);
			if (user == null)
				return null;
			return GetFolUser(user);
		}


		/// <summary>
		/// Updates the UserName of a user.
		/// </summary>
		/// <param name="oldUserName">The old UserName.</param>
		/// <param name="newUserName">The new UserName.</param>
		/// <returns>True if the action was successfull, false otherwise.</returns>
		public virtual bool ChangeUserName(string oldUserName, string newUserName) {
			IDataAccess da = new DataAccess();
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				DbCommand cm = da.GetStoredProcCommand(const_proc_changeusername);
				da.AddInParameterString(cm, "ApplicationName", ContextHelper.ApplicationName);
				da.AddInParameterString(cm, "OldUserName", oldUserName);
				da.AddInParameterString(cm, "NewUserName", newUserName);
				da.AddReturnParameter(cm);
				da.ExecuteNonQuery(cm);
				int result = da.GetReturnParameter(cm);
				t.Complete();
				if (result == 0)
					return true;
				if (result == -2)
					throw new MembershipCreateUserException(MembershipCreateStatus.DuplicateUserName);
				throw new MembershipCreateUserException(MembershipCreateStatus.InvalidUserName);
			}
		}

		/// <summary>
		/// Overrides the OnValidationPassword function to check for conformance with password history.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnValidatingPassword(ValidatePasswordEventArgs e) {
			// disable the check for password history in the case of a mapping (IsAsynchronousMode)
			if (!e.IsNewUser && PasswordHistoryCount > 0 && !ExternalCrudNotificationService.IsAsynchronousMode && PasswordAlreadyUsed(e.UserName, e.Password)) {
				e.Cancel = true;
				e.FailureInformation = new Exception(String.Format(Langue.error_msg_password_history, PasswordHistoryCount));
			}
			base.OnValidatingPassword(e);
		}

		/// <summary>
		/// Checks if the password was already in used.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="password">The password.</param>
		/// <returns></returns>
		private bool PasswordAlreadyUsed(string username, string password) {
			string thepasswd;
			if (PasswordFormat == MembershipPasswordFormat.Clear) {
				thepasswd = password;
			}
			else {
				byte[] bytes = Encoding.Unicode.GetBytes(password);
				byte[] src = Convert.FromBase64String(GetPasswordSalt(username));
				byte[] dst = new byte[src.Length + bytes.Length];
				byte[] inArray;
				Buffer.BlockCopy(src, 0, dst, 0, src.Length);
				Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
				if (PasswordFormat == MembershipPasswordFormat.Hashed) {
					HashAlgorithm algorithm = HashAlgorithm.Create(Membership.HashAlgorithmType);
					inArray = algorithm.ComputeHash(dst);
				}
				else {
					inArray = EncryptPassword(dst);
				}
				thepasswd = Convert.ToBase64String(inArray);
			}

			//Compare old passwords
			DataTable dtPasswordHistory = GetPasswordHistory(username);
			List<string> arPasswordHistory = (from DataRow dr in dtPasswordHistory.Rows select dr.Field<string>("Password")).ToList();

			return arPasswordHistory.Contains(thepasswd);
		}

		/// <summary>
		/// Gets the password salt for a user.
		/// </summary>
		/// <param name="username"></param>
		/// <returns></returns>
		private string GetPasswordSalt(string username) {
			DbCommand command = _da.GetStoredProcCommand(const_proc_getpasswordsalt);
			_da.AddInParameterString(command, "ApplicationName", ContextHelper.ApplicationName);
			_da.AddInParameterString(command, "UserName", username);
			string passwordSalt = null;
			using (IDataReader readerGetPasswordSalt = _da.ExecuteReader(command)) {
				while (readerGetPasswordSalt.Read()) {
					passwordSalt = readerGetPasswordSalt.GetValue<string>("PasswordSalt");
				}
			}
			return passwordSalt;

		}

		/// <summary>
		/// Gets the password history for a user.
		/// </summary>
		/// <param name="username"></param>
		/// <returns></returns>
		private DataTable GetPasswordHistory(string username) {
			DbCommand command = _da.GetStoredProcCommand(const_proc_getpasswordhistory);
			_da.AddInParameterString(command, "ApplicationName", ContextHelper.ApplicationName);
			_da.AddInParameterString(command, "UserName", username);
			_da.AddInParameterInt(command, "PasswordHistoryCount", PasswordHistoryCount);
			DataSet ds = _da.ExecuteDataSet(command);
			return ds.Tables[0];
		}

		/// <summary>
		/// Updates a user.
		/// </summary>
		/// <param name="user">The user.</param>
		public override void UpdateUser(MembershipUser user) {
			var folProviderUserKey = (FOLProviderUserKey)user.ProviderUserKey;
			if (folProviderUserKey == null) {
				throw new MembershipCreateUserException(MembershipCreateStatus.InvalidProviderUserKey);
			}

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				MembershipUser sqlUser = new MembershipUser(user.ProviderName, user.UserName, folProviderUserKey.Guid, user.Email,
															user.PasswordQuestion, user.Comment, user.IsApproved, user.IsLockedOut,
															user.CreationDate, user.LastLoginDate, user.LastActivityDate,
															user.LastPasswordChangedDate, user.LastLockoutDate);
				base.UpdateUser(sqlUser);

				DbCommand command = _da.GetStoredProcCommand(const_proc_updateuser);
				_da.AddInParameterString(command, "ApplicationName", ContextHelper.ApplicationName);
				_da.AddInParameterString(command, "UserId", folProviderUserKey.Guid.ToString());
				_da.AddInParameterString(command, "UserName", user.UserName);
				_da.AddInParameterString(command, "KeyOccupant", folProviderUserKey.KeyOccupant);
				_da.AddInParameterBool(command, "NeverExpires", folProviderUserKey.NeverExpires);
				_da.AddInParameterBool(command, "IsForgotPassword", folProviderUserKey.IsForgotPassword);
                _da.AddInParameterString(command, "KeyLocation", folProviderUserKey.KeyLocation);
				_da.AddInParameterString(command, "FirstName", folProviderUserKey.FirstName);
				_da.AddInParameterString(command, "LastName", folProviderUserKey.LastName);
				_da.ExecuteNonQuery(command);
				t.Complete();
			}
		}



		/// <summary>
		/// Creates the user.
		/// </summary>
		/// <param name="item">The user.</param>
		/// <param name="status">The status.</param>
		/// <returns></returns>
		public virtual MembershipUser CreateUser(FOLMembershipUser item, out MembershipCreateStatus status) {
			var user = CreateUser(item.UserName, item.Password, item.Email, null, null, item.IsApproved, item.ProviderUserKey, out status);

			if (status != MembershipCreateStatus.Success) {
				throw new MembershipCreateUserException(status);
			}

			// Since createuser doesn't save the comment we need to update the user with the comment immediately. 
			user.Comment = item.Comment;
			UpdateUser(user);

			if (!item.UserMustChangePasswordAtNextLogon) {
				// In this case we need to reset the password for the same password, to trick the provider.
				bool changePasswordResult = ChangePassword(item.UserName, item.Password);

				if (!changePasswordResult) {
					throw new MembershipCreateUserException(String.Format(Langue.error_membership_cannotchangepassword, item.UserName));
				}
			}

			//if (item.UserMustChangePasswordAtNextLogon) {
			//    // In this case we need to reset the password for the same password, to trick the provider.
			//    bool changePasswordResult = ChangePassword(item.UserName, item.Password);

			//    if (!changePasswordResult) {
			//        throw new MembershipCreateUserException(String.Format(Langue.error_membership_cannotchangepassword, item.UserName));
			//    }

			//    user.LastLoginDate = user.LastLoginDate.AddSeconds(-1);
			//    base.UpdateUser(user);
			//}

			return user;
		}

		/// <summary>
		/// Creates a new user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="password">The password</param>
		/// <param name="email">The email address.</param>
		/// <param name="passwordQuestion">The password question.</param>
		/// <param name="passwordAnswer">The password answer.</param>
		/// <param name="isApproved">True if user is approved, false otherwise.</param>
		/// <param name="providerUserKey">The provider user key.</param>
		/// <param name="status">The membership status of the operation.</param>
		/// <returns>The membership user.</returns>
		public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status) {
			if (providerUserKey != null && !(providerUserKey is FOLProviderUserKey)) {
				status = MembershipCreateStatus.InvalidProviderUserKey;
				return null;
			}

			if (providerUserKey == null) {
				status = MembershipCreateStatus.InvalidProviderUserKey;
				return null;
			}

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				FOLProviderUserKey userKey = (FOLProviderUserKey)providerUserKey;

				MembershipUser membershipUser = base.CreateUser(username, password, email, passwordQuestion, passwordAnswer, isApproved, userKey.Guid, out status);
				if (membershipUser == null)
					throw new MembershipCreateUserException(status);

				DbCommand command = _da.GetStoredProcCommand(const_proc_createuser);
				_da.AddInParameterString(command, "ApplicationName", ContextHelper.ApplicationName);
				
				if (membershipUser.ProviderUserKey != null)
					_da.AddInParameterString(command, "UserId", ((Guid)membershipUser.ProviderUserKey).ToString());

				_da.AddInParameterString(command, "UserName", membershipUser.UserName);
				_da.AddInParameterString(command, "FirstName", userKey.FirstName);
				_da.AddInParameterString(command, "LastName", userKey.LastName);
				_da.AddInParameterString(command, "KeyOccupant", userKey.KeyOccupant);
				_da.AddInParameterBool(command, "NeverExpires", userKey.NeverExpires);
				_da.AddInParameterBool(command, "IsForgotPassword", userKey.IsForgotPassword);
                _da.AddInParameterString(command, "KeyLocation", userKey.KeyLocation);
				_da.AddOutParameterString(command, "KeyUser");
				_da.ExecuteNonQuery(command);
				t.Complete();
				return new MembershipUser(membershipUser.ProviderName, membershipUser.UserName,
					new FOLProviderUserKey {
						Guid = (Guid)membershipUser.ProviderUserKey,
						KeyOccupant = userKey.KeyOccupant,
						NeverExpires = userKey.NeverExpires,
                        KeyLocation = userKey.KeyLocation,
						KeyUser = (string)_da.GetParameterValue(command, "KeyUser"),
						FirstName = userKey.FirstName,
						LastName = userKey.LastName
					}, membershipUser.Email, membershipUser.PasswordQuestion, membershipUser.Comment, membershipUser.IsApproved, membershipUser.IsLockedOut, membershipUser.CreationDate, membershipUser.LastLoginDate, membershipUser.LastActivityDate, membershipUser.LastPasswordChangedDate, membershipUser.LastLockoutDate);
			}
		}

		/// <summary>
		/// Changes the password format for an existing user.
		/// This is necessary when modifying web.config because existing password are not updated when modifying passwordFormat.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public virtual bool ChangePasswordFormat(string username) {
			DataAccess db = new DataAccess("dsn_vizelia_membership");
			DbCommand commandGetPassword = db.GetStoredProcCommand(const_proc_aspnet_getpasswordwithformat);
			db.AddInParameterString(commandGetPassword, "ApplicationName", ContextHelper.ApplicationName);
			db.AddInParameterString(commandGetPassword, "UserName", username);
			db.AddInParameterBool(commandGetPassword, "UpdateLastLoginActivityDate", false);
			db.AddInParameterDateTime(commandGetPassword, "CurrentTimeUtc", DateTime.UtcNow);
			string passwordSalt = null;
			string password = null;
			int passwordFormat = 0;
			using (IDataReader readerGetPassword = db.ExecuteReader(commandGetPassword)) {
				while (readerGetPassword.Read()) {
					password = readerGetPassword.GetString(0);
					passwordFormat = readerGetPassword.GetInt32(1);
					passwordSalt = readerGetPassword.GetString(2);
				}
			}

			DbCommand commandResetPassword = db.GetStoredProcCommand(const_proc_aspnet_resetpassword);
			db.AddInParameterString(commandResetPassword, "ApplicationName", ContextHelper.ApplicationName);
			db.AddInParameterString(commandResetPassword, "UserName", username);
			db.AddInParameterString(commandResetPassword, "NewPassword", String.Empty);
			db.AddInParameterInt(commandResetPassword, "MaxInvalidPasswordAttempts", Membership.MaxInvalidPasswordAttempts);
			db.AddInParameterInt(commandResetPassword, "PasswordAttemptWindow", Membership.PasswordAttemptWindow);
			db.AddInParameterString(commandResetPassword, "PasswordSalt", passwordSalt);
			db.AddInParameterDateTime(commandResetPassword, "CurrentTimeUtc", DateTime.Now);
			db.AddInParameterInt(commandResetPassword, "PasswordFormat", (int)Membership.Provider.PasswordFormat);
			db.ExecuteNonQuery(commandResetPassword);
			return ChangePassword(username, password);
		}

		/// <summary>
		/// Changes the password for a membership user.
		/// This method works regardless of the password format (clear, encrypted, hashed).
		/// It requires that requiresPasswordAndQuestion is set to false.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="newPassword">The new password.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public virtual bool ChangePassword(string username, string newPassword) {
			string resetPassword = ResetPassword(username);
			return ChangePassword(username, resetPassword, newPassword);
		}

		/// <summary>
		/// Changes the password for a membership user.
		/// </summary>
		/// <param name="username"></param>
		/// <param name="oldPassword"></param>
		/// <param name="newPassword"></param>
		/// <returns></returns>
		public override bool ChangePassword(string username, string oldPassword, string newPassword) {
			var user = base.GetUser(username, false);

			//Want to allow in reset password scenario, otherwise make sure the user is not changing passwords too oftenly.
			if (!(user.LastLoginDate <= user.LastPasswordChangedDate) && ((DateTime.Now - user.LastPasswordChangedDate).TotalMinutes < ChangePasswordWindow)) {
				throw new MembershipException(string.Format(Langue.error_msg_password_window_closed, new TimeSpan(0, ChangePasswordWindow, 0).ToPrettyTimeSpanString()));
			}
			try {
				bool result = base.ChangePassword(username, oldPassword, newPassword);

				if (result) {
					AddPasswordHistory(username);
					// to force update LastLoginDate after a password change. The sleep is necessary to get a difference.
					//Thread.Sleep(100);
					//base.ValidateUser(username, newPassword);

					// Thread.Sleep cause exception when entering here from the Mapping.


					//user.LastLoginDate = user.LastLoginDate.AddMilliseconds(100);
					user.LastLoginDate = DateTime.UtcNow;
					base.UpdateUser(user);
				}
				return result;
			}
			catch (ArgumentException e) {
				if (e.Message == "The parameter 'newPassword' does not match the regular expression specified in config file.") {
					throw new ArgumentException(Langue.error_msg_password_regex_config);
				}
				throw e;
			}
		}

		/// <summary>
		/// Adds the last known password to the history.
		/// </summary>
		/// <param name="username"></param>
		private void AddPasswordHistory(string username) {
			DbCommand command = _da.GetStoredProcCommand(const_proc_addpasswordhistory);
			_da.AddInParameterString(command, "ApplicationName", ContextHelper.ApplicationName);
			_da.AddInParameterString(command, "UserName", username);
			_da.ExecuteNonQuery(command);
		}

		/// <summary>
		/// Resets the password for a membership user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="isForgotPassword">The is forgot password.</param>
		/// <returns>
		/// The reseted password.
		/// </returns>
		public virtual string ResetPassword(string username, bool? isForgotPassword = null) {
			string newPassword = null;
			MembershipUser user = GetUser(username, false);
			if (user != null) {
				if (user.IsLockedOut)
					user.UnlockUser();

				if (isForgotPassword.HasValue) {
					var folProviderUserKey = (FOLProviderUserKey)user.ProviderUserKey;
					if (folProviderUserKey != null) {
						folProviderUserKey.IsForgotPassword = isForgotPassword.Value;
						UpdateUser(user);
					}
				}
				newPassword = ResetPassword(user.UserName, String.Empty);
			}
			return newPassword;
		}

		/// <summary>
		/// Locks a user account.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>True if successfull, false otherwise.</returns>
		public bool LockUser(string username) {
			DbCommand command = _da.GetStoredProcCommand(const_proc_lockuser);
			_da.AddInParameterString(command, "ApplicationName", ContextHelper.ApplicationName);
			_da.AddInParameterString(command, "UserName", username);
			_da.AddInParameterDateTime(command, "CurrentTimeUtc", DateTime.UtcNow);
			_da.AddReturnParameter(command);
			_da.ExecuteNonQuery(command);
			int returnValue = _da.GetReturnParameter(command);
			return returnValue == 0;
		}

		/// <summary>
		/// Switches the is forgot password off.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <returns></returns>
		public bool SwitchIsForgotPasswordOff(string username) {
			var switchSuccess = false;
			var user = GetUser(username, false);
			if (user != null) {
				var folProviderUserKey = (FOLProviderUserKey)user.ProviderUserKey;
				if (folProviderUserKey != null) {
					folProviderUserKey.IsForgotPassword = false;
				}
				UpdateUser(user);
				switchSuccess = true;
			}
			return switchSuccess;
		}

		/// <summary>
		/// Gets the users store.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="fields">The fields</param>
		/// <returns></returns>
		public virtual JsonStore<FOLMembershipUser> GetUsersStore(PagingParameter paging, params string[] fields) {
			// Gets the users from the Users table in the Vizelia Database.
			var broker = Helper.CreateInstance<FOLMembershipUserBroker>();
			JsonStore<FOLMembershipUser> usersStore = broker.GetStore(paging, PagingLocation.Database, fields: fields);

			return usersStore;
		}
	}
}

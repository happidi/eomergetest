﻿using System;
using System.IdentityModel.Claims;
using System.IdentityModel.Policy;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.Security;
using NetSqlAzMan;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Security {
	/// <summary>
	/// Defines a set of rules for authorizing a user, given a set of claims.
	/// </summary>
	public class AuthorizationPolicy : IAuthorizationPolicy {


		/// <summary>
		/// Evaluates the specified business layer name.
		/// </summary>
		/// <param name="businessLayerName">Name of the business layer.</param>
		/// <param name="methodName">Name of the method.</param>
		/// <returns></returns>
		/// <exception cref="Vizelia.FOL.Common.VizeliaSecurityException">Access Denied</exception>
		public static bool Evaluate(string businessLayerName, string methodName) {
			string operationServiceName = string.Format("Vizelia.FOL.WCFService.{0}WCF.{1}", businessLayerName, methodName);
			CookieData cookieData;
			SessionService.SetApplicationName(out cookieData);
			// in case the user is authenticated
			if (HttpContext.Current.User.Identity.IsAuthenticated) {
				//CheckPageId(cookieData.PageId);
				bool isAllowed = IsAllowed(operationServiceName, false);

				if (!isAllowed) {
					throw new VizeliaSecurityException("Access Denied");
				}

				return true;
			}

			SessionService.RaiseTimeoutException();
			return false;
		}

		/// <summary>
		/// Evaluates whether a user meets the requirements for this authorization policy.
		/// This function is called for every WCF request.
		/// </summary>
		/// <param name="evaluationContext">A System.IdentityModel.Policy.EvaluationContext that contains the claim set that the authorization policy evaluates.</param>
		/// <param name="state"> A System.Object, passed by reference that represents the custom state for this authorization policy.</param>
		/// <returns></returns>
		public bool Evaluate(EvaluationContext evaluationContext, ref object state) {
			CopyPrincipal(evaluationContext);
			CookieData cookieData;
			SessionService.SetApplicationName(out cookieData);

			// if the call is to a publicly accessible resource on the server
			if (SessionService.IsPubliclyAccessible()) {
				return true;
			}



			// in case the user is authenticated
			if (HttpContext.Current.User.Identity.IsAuthenticated) {
				//if (!(SessionService.GetActiveSessions().Contains(cookieData.SessionId))) {
				//    SessionService.RaiseTimeoutException();
				//}
				CheckPageId(cookieData.PageId);
				bool isAllowed = IsAllowed();

				if (!isAllowed) {
					throw new VizeliaSecurityException("Access Denied");
				}

				return true;
			}

			// in order to signal an internal call the client has to send a valid forms authentication encrypted token with key SessionService.const_internal_request_header_name
			if (IsInternalCall(evaluationContext)) {
				return true;
			}

			SessionService.RaiseTimeoutException();
			return false;
		}

		/// <summary>
		/// Determines whether this WCF request is allowed or not.
		/// </summary>
		/// <returns>
		///   <c>true</c> if this WCF request is allowed; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsAllowed(string operationServiceName = null, bool checkIsTenancyService = true) {
			IPrincipal principal = HttpContext.Current.User;

			operationServiceName = operationServiceName ?? GetOperationServiceName();
			bool isTenancyService = checkIsTenancyService && IsTenancyService();

			// The DisablePermissions flag doesn't apply on the tenancy service.
			if (VizeliaConfiguration.Instance.Authentication.DisablePermissions && !isTenancyService) {
				return true;
			}

			//Proven to be much faster than IsInRole
			bool isAllowed = ((FOLRoleProvider)Roles.Provider).CheckAccess(principal.Identity.Name, operationServiceName);
			//bool isAllowed = principal.IsInRole(operationServiceName); - OLD VERSION if something is wrong with CheckAccess
			if (!isAllowed) {
				TracingService.Write(TraceEntrySeverity.Error, string.Format("Access Denied for user {0}: Not authorized to operation {1}.", principal.Identity.Name, operationServiceName), "Security", string.Empty);
				return false;
			}

			return true;
		}

		/// <summary>
		/// Determines whether the current WCF request is for the tenancy service or not.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the current WCF request is for the tenancy service; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsTenancyService() {
			// We may want to move this to checking an interface if another service has this behavior later.
			bool isTenancyService = OperationContext.Current.Host.Description.ServiceType.Name.Equals("TenancyWCF");

			return isTenancyService;
		}

		/// <summary>
		/// Determines whether this call is an internal call.
		/// </summary>
		/// <param name="evaluationContext"> </param>
		/// <returns>
		///   <c>true</c> if [is internal call]; otherwise, <c>false</c>.
		/// </returns>
		private bool IsInternalCall(EvaluationContext evaluationContext) {

			try {
				var formsValue = HttpContext.Current.Request.Headers[SessionService.const_internal_request_header_name];
				if (string.IsNullOrEmpty(formsValue))
					return false;

				FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(formsValue);
				if (ticket.Expired)
					return false;
				else {
					FormsIdentity fi = new FormsIdentity(ticket);
					GenericPrincipal gp = new GenericPrincipal(fi, new string[0]);


					var result = typeof(HttpContext).InvokeMember("SetPrincipalNoDemand",
																   BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Instance,
																   Type.DefaultBinder, HttpContext.Current, new object[] { gp });
					CopyPrincipal(evaluationContext);
					ContextHelper.ApplicationName = new CookieData(ticket.UserData).ApplicationName;
					ContextHelper.Add("SessionID", new CookieData(ticket.UserData).SessionId);
					HttpContext.Current.Request.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, formsValue));
					return true;
				}
			}
			catch {
			}
			return false;
		}


		/// <summary>
		/// Checks the page id.
		/// </summary>
		/// <param name="pageId">The page id.</param>
		private static void CheckPageId(string pageId) {
			var pageIdFromRequest = GetPageIdFromRequest();
			if (pageId != pageIdFromRequest) {
				throw new VizeliaSecurityException("Access Denied");
			}
		}

		/// <summary>
		/// Gets the page id from request.
		/// </summary>
		/// <returns></returns>
		private static string GetPageIdFromRequest() {
			string pageId = null;
			if (HttpContext.Current.Request.Headers != null) {
				try {
					pageId = HttpContext.Current.Request.Headers[SessionService.const_page_id];
				}
				catch (Exception) {
					pageId = null;

				}
			}
			if (string.IsNullOrWhiteSpace(pageId)) {
				try {
					pageId = HttpContext.Current.Request[SessionService.const_page_id].Trim('\"');
				}
				catch (Exception) {
					pageId = null;
				}

			}
			return pageId;
		}


		/// <summary>
		/// Gets the full name of the operation, including service.
		/// </summary>
		/// <returns></returns>
		private static string GetOperationServiceName() {
			var operationCtx = OperationContext.Current;
			MessageProperties incomingMessageProperties = operationCtx.IncomingMessageProperties;
			var serviceName = operationCtx.Host.Description.ServiceType.FullName;
			string operationName = null;

			if (incomingMessageProperties.ContainsKey("HttpOperationName")) {
				operationName = (string)incomingMessageProperties["HttpOperationName"];
			}
			else if (operationCtx.IncomingMessageHeaders != null && (operationCtx.IncomingMessageHeaders).Action != null) {
				operationName = (operationCtx.IncomingMessageHeaders).Action.Split('/').Last();
			}

			if (operationName == null) {
				//	SessionService.Abandon();
				TracingService.Write(TraceEntrySeverity.Error, string.Format("Access Denied for user {0}: Unknown operation service name.", HttpContext.Current.User.Identity.Name), "Security", string.Empty);
				throw new VizeliaSecurityException("Access Denied");
			}

			string operationServiceName = String.Format("{0}.{1}", serviceName, operationName);
			return operationServiceName;
		}

		/// <summary>
		/// Copies the principal.
		/// </summary>
		/// <param name="evaluationContext">The evaluation context.</param>
		/// <returns></returns>
		private void CopyPrincipal(EvaluationContext evaluationContext) {
			// Copy IPrincipal so it is available for WCF.
			if (HttpContext.Current != null && HttpContext.Current.User != null) {
				evaluationContext.Properties["Principal"] = HttpContext.Current.User;
			}
			else {
				evaluationContext.Properties["Principal"] = new RolePrincipal(new GenericIdentity(""));
			}
		}


		/// <summary>
		/// Checks if the WCF Operation is one way.
		/// </summary>
		/// <returns></returns>
		private bool OperationIsOneWay() {
			bool isOneWay = false;
			try {
				var operationCtx = OperationContext.Current;
				if (operationCtx.IncomingMessageProperties.ContainsKey("HttpOperationName")) {
					string operationName = (string)operationCtx.IncomingMessageProperties["HttpOperationName"];
					isOneWay = operationCtx.EndpointDispatcher.DispatchRuntime.Operations[operationName].IsOneWay;
				}
			}
			catch { }
			return isOneWay;
		}

		/// <summary>
		/// Gets a claim set that represents the issuer of the authorization policy.
		/// </summary>
		public ClaimSet Issuer {
			get { throw new NotImplementedException(); }
		}

		/// <summary>
		/// Gets a string that identifies this authorization component.
		/// </summary>
		public string Id {
			get { throw new NotImplementedException(); }
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSqlAzMan.Cache;
using NetSqlAzMan.Interfaces;

namespace Vizelia.FOL.Security {
    class ExtendedAzManStorage : StorageCache {

        /// <summary>
        /// Items by key dictionary.
        /// This dictionary initialized as tenant specific collection, therefore we save it in the storage level and not in the application level as 'Itmes' does.
        /// We create and cache it for performance matter.
        /// </summary>
        private Dictionary<string, IAzManItem> ItemsByKey;
        /// <summary>
        /// Application groups by key dictionary.
        /// This dictionary initialized as tenant specific collection, therefore we save it in the storage level and not in the application level as 'ApplicationGroups' does.
        /// We create and cache it for performance matter.
        /// </summary>
        private Dictionary<string, IAzManApplicationGroup> ApplicationGroupsByKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtendedAzManStorage"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        public ExtendedAzManStorage(string connectionString)
            : base(connectionString) {            
        }

        /// <summary>
        /// Builds the storage cache.
        /// </summary>
        /// <param name="storeNameFilter">The store name filter.</param>
        /// <param name="applicationNameFilter">The application name filter.</param>
        public new void BuildStorageCache(string storeNameFilter, string applicationNameFilter) {
            base.BuildStorageCache(storeNameFilter, applicationNameFilter);
            InitializeItemsByKeyDictionary(storeNameFilter, applicationNameFilter);
            InitializeApplicationGroupsByKeyDictionary(storeNameFilter, applicationNameFilter);
        }

        /// <summary>
        /// Initializes the items by key dictionary.
        /// </summary>
        /// <param name="storeNameFilter">The store name filter.</param>
        /// <param name="applicationNameFilter">The application name filter.</param>
        private void InitializeItemsByKeyDictionary(string storeNameFilter, string applicationNameFilter) {
            ItemsByKey = new Dictionary<string, IAzManItem>();
            foreach (var item in this.storage.Stores[storeNameFilter].Applications[applicationNameFilter].Items.Values) {
                ItemsByKey.Add(item.ItemId.ToString(), item);
            }
        }

        /// <summary>
        /// Initializes the application groups by key dictionary.
        /// </summary>
        /// <param name="storeNameFilter">The store name filter.</param>
        /// <param name="applicationNameFilter">The application name filter.</param>
        private void InitializeApplicationGroupsByKeyDictionary(string storeNameFilter, string applicationNameFilter) {
            ApplicationGroupsByKey = new Dictionary<string, IAzManApplicationGroup>();
            foreach (var appGroup in this.storage.Stores[storeNameFilter].Applications[applicationNameFilter].ApplicationGroups.Values) {
                ApplicationGroupsByKey.Add(appGroup.SID.ToString(), appGroup);
            }
        }

        /// <summary>
        /// Gets the item by identifier.
        /// </summary>
        /// <param name="ItemId">The item identifier.</param>
        /// <returns></returns>
        public IAzManItem GetItemById(string ItemId) {
            IAzManItem retItem = null;
            if (ItemId != null && ItemsByKey.ContainsKey(ItemId)) {
                retItem = ItemsByKey[ItemId]; 
            }

            return retItem;
        }

        /// <summary>
        /// Gets the application group by identifier.
        /// </summary>
        /// <param name="applicationGroupId">The application group identifier.</param>
        /// <returns></returns>
        public IAzManApplicationGroup GetApplicationGroupById(string applicationGroupId) {
            IAzManApplicationGroup retAppGroup = null;
            if (applicationGroupId != null && ApplicationGroupsByKey.ContainsKey(applicationGroupId)) {
                retAppGroup = ApplicationGroupsByKey[applicationGroupId]; 
            }

            return retAppGroup;
        }
    }
}

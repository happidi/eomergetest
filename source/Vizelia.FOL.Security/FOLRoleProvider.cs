﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading;
using System.Transactions;
using System.Web.Security;
using System.Xml;
using NetSqlAzMan;
using NetSqlAzMan.Cache;
using NetSqlAzMan.ENS;
using NetSqlAzMan.Interfaces;
using NetSqlAzMan.Providers;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Helpers;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Security {
	/// <summary>
	/// The FOL Role Provider
	/// Derives from NetSqlRoleProvider
	/// </summary>
	public class FOLRoleProvider : NetSqlAzManRoleProvider, IFOLRoleProvider {

		private string _name;
		private const string const_attribute_filter = "Filter";
		private const string const_attribute_type = "Type";
		private const string const_attribute_locationfilter = "Vizelia.FOL.BusinessEntities.LocationFilter";
		private const string const_azman_attribute_to_require_administration_tenant = "RequireAdministrationTenant";
        private Dictionary<string, KeyValuePair<Guid, ExtendedAzManStorage>> m_AzmanCache = new Dictionary<string, KeyValuePair<Guid, ExtendedAzManStorage>>();
		private object azmanLock = new object();


		/// <summary>	
		/// The name of the stored procedure that returns the roles for a user.
		/// </summary>
		private const string const_proc_azman_getrolesforuser = "Role_GetRolesForUser";

		/// <summary>	
		/// The name of the stored procedure that returns the azman items.
		/// </summary>
		private const string const_proc_azman_item_getall = "Role_AzManItem_GetAll";


		/// <summary>
		/// The name of the stored procedure that returns the azman roles for a specific Chart KPI.
		/// </summary>
		private const string const_proc_azman_role_getallbykeychart = "Role_GetAllByKeyChart";

		/// <summary>
		/// The name of the stored procedure that removes a user from azman store.
		/// </summary>
		private const string const_proc_azman_removeuser = "Role_RemoveUser";

		/// <summary>
		/// The user name for the admin user
		/// </summary>
		private const string AdminUserName = "Admin";
		/// <summary>
		/// The name of the admins application group
		/// </summary>
		private const string AdminsApplicationGroupName = "Admins";

		/// <summary>
		/// The name of the Schedulers application group
		/// </summary>
		private const string SchedulersApplicationGroupName = "Schedulers";

		/// <summary>
		/// The user name for the scheduler user
		/// </summary>
		private readonly string SchedulerUserName = VizeliaConfiguration.Instance.Authentication.SchedulerUsername;


		/// <summary>
		/// The name of the azman store
		/// </summary>
		private const string m_StoreName = "Vizelia";


		/// <summary>
		/// Gets a brief, friendly description suitable for display in administrative tools or other user interfaces (UIs).
		/// </summary>
		public override string Description {
			get {
				return "FOL Role Provider";
			}
		}

		/// <summary>
		/// Gets the friendly name used to refer to the provider during configuration.
		/// </summary>
		public override string Name {
			get {
				return _name;
			}
		}

		private delegate void OperateMember(IAzManItem obj, IAzManItem member);

		/// <summary>
		/// Gets or sets the name of the application to store and retrieve role information for.
		/// </summary>
		/// <returns>The name of the application to store and retrieve role information for.</returns>
		public override string ApplicationName {
			get {
				string retval = ContextHelper.ApplicationName;
				base.ApplicationName = retval;
				return retval;
			}
			set {

			}
		}

		/// <summary>
        /// Gets the chached application.
        /// </summary>
        private IAzManApplication CachedApplication {
            get {
                IAzManApplication retVal;

                var cachedStorage = GetCachedAzmanStorage();
                if (cachedStorage != null) {
                    retVal = cachedStorage.Storage.Stores[m_StoreName].Applications[ContextHelper.ApplicationName];
                }
                else {
                    retVal = GetStore().GetApplication(ApplicationName);
                }
                return retVal;
            }
        }

        /// <summary>
		/// Gets the list of authorizations for a specific application group.
		/// </summary>
		/// <param name="applicationGroupId">The application group id to retreive authorizations.</param>
		/// <returns></returns>
		public List<FOLMembershipUser> Authorization_GetListByApplicationGroupId(string applicationGroupId) {
			List<FOLMembershipUser> result = new List<FOLMembershipUser>();
			var isDirty = false;
			if (applicationGroupId == null)
				return result;
			try {
                // IAzManApplicationGroup applicationGroup = CachedApplication.GetApplicationGroupById(applicationGroupId); 
                var storage = GetCachedAzmanStorage();
                IAzManApplicationGroup applicationGroup = storage.GetApplicationGroupById(applicationGroupId);

                if (applicationGroup != null) {
                    foreach (var auth in applicationGroup.Members.Values) {

					try {
                            var user = new FOLMembershipUser(Membership.GetUser(CachedApplication.GetDBUser(auth.SID).UserName));
						result.Add(user);
					}
					catch (SqlAzManException) {

						//applicationGroup.Members.Remove(auth.SID);
						foreach (var member in applicationGroup.Members) {
							if (member.Value.SID.StringValue == auth.SID.StringValue) {
								member.Value.Delete();
								isDirty = true;
							}
						}
					}
				}
                }

				if (isDirty) {
					InvalidateStorageCache();
				}

				return result;
			}
			catch {
				return result;
			}
		}

		/// <summary>
		/// Gets the list of authorizations for a specific application group.
		/// </summary>
		/// <param name="applicationGroupId">The application group id to retreive authorizations.</param>
		/// <returns></returns>
		public List<AuthorizationItem> AzManRole_GetListByApplicationGroupId(string applicationGroupId) {
			List<AuthorizationItem> result = new List<AuthorizationItem>();
			if (applicationGroupId == null)
				return result;
            // IAzManApplicationGroup applicationGroup = CachedApplication.GetApplicationGroupById(applicationGroupId);
            var storage = GetCachedAzmanStorage();
            IAzManApplicationGroup applicationGroup = storage.GetApplicationGroupById(applicationGroupId);
            List<IAzManItem> items = AzManItem_GetList(null, const_attribute_type, const_attribute_filter, false);

			IEnumerable<IAzManItem> childrenRoles = items.SelectMany(azmanItem => azmanItem.Members)
														 .Select(azmanItem => azmanItem.Value)
														 .Where(azmanItem => azmanItem.ItemType == ItemType.Role);
			while (childrenRoles.Count() != 0) {
				foreach (IAzManItem item in childrenRoles) {
					if (item.Authorizations.Any(p => p.SID.StringValue == applicationGroup.SID.StringValue))
						result.Add(new AuthorizationItem(item));
					childrenRoles = childrenRoles.SelectMany(azmanItem => azmanItem.Members)
												 .Select(azmanItem => azmanItem.Value)
												 .Where(azmanItem => azmanItem.ItemType == ItemType.Role);
				}
			}

			result.AddRange(from item in items
							where item.Authorizations.Any(p => p.SID.StringValue == applicationGroup.SID.StringValue)
							select new AuthorizationItem(item));
			return result;
		}

		/// <summary>
		/// Creates a new ApplicationGroup.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public ApplicationGroup ApplicationGroup_Create(ApplicationGroup item) {
			item.Description = item.Description ?? ""; // AzMan database will not accept null values for Description
			ValidationHelper.Validate(item);
            IAzManApplicationGroup applicationGroup = CachedApplication.CreateApplicationGroup(SqlAzManSID.NewSqlAzManSid(), item.Name, item.Description, null, GroupType.Basic);
			return new ApplicationGroup(applicationGroup);
		}

		/// <summary>
		/// Deletes an ApplicationGroup entity.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ApplicationGroup_Delete(ApplicationGroup item) {
            // IAzManApplicationGroup applicationGroup = CachedApplication.GetApplicationGroupById(item.Id);
            var storage = GetCachedAzmanStorage();
            IAzManApplicationGroup applicationGroup = storage.GetApplicationGroupById(item.Id);
			if (applicationGroup == null)
				return false;
			applicationGroup.Delete();
			return true;
		}

		/// <summary>
		/// Gets all the application groups.
		/// </summary>
		/// <returns></returns>
		public List<IAzManApplicationGroup> ApplicationGroup_GetAll() {
            return CachedApplication.ApplicationGroups.Values.OrderBy(p => p.Name).ToList();
		}

		/// <summary>
		/// Gets an ApplicationGroup by its key.
		/// </summary>
		/// <param name="Key">The Key of the ApplicationGroup.</param>
		/// <returns></returns>
		public ApplicationGroup ApplicationGroup_GetItem(string Key) {
            // IAzManApplicationGroup applicationGroup = CachedApplication.GetApplicationGroupById(Key);
            var storage = GetCachedAzmanStorage();
            IAzManApplicationGroup applicationGroup = storage.GetApplicationGroupById(Key);
            return new ApplicationGroup(applicationGroup);
		}

		/// <summary>
		/// Gets an ApplicationGroup by its name.
		/// </summary>
		/// <param name="name">The name of the ApplicationGroup.</param>
		/// <returns></returns>
		public ApplicationGroup ApplicationGroup_GetItemByName(string name) {
			try {
                return new ApplicationGroup(CachedApplication.ApplicationGroups[name]);
			}
			catch {
				return null;
			}
		}

		/// <summary>
		/// Generates the json structure of the ApplicationGroup treeview.
		/// </summary>
		/// <returns></returns>
		public List<TreeNode> ApplicationGroup_GetTree() {
			return ApplicationGroup_GetAll().Select(p => new ApplicationGroup(p).GetTree()).ToList();
		}

		/// <summary>
		/// Updates an ApplicationGroup.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public ApplicationGroup ApplicationGroup_Update(ApplicationGroup item) {
            // IAzManApplicationGroup applicationGroup = CachedApplication.GetApplicationGroupById(item.Id);
            var storage = GetCachedAzmanStorage();
            IAzManApplicationGroup applicationGroup = storage.GetApplicationGroupById(item.Id);

			if (applicationGroup.Name != item.Name)
				applicationGroup.Rename(item.Name);
			item.Description = item.Description ?? ""; // AzMan database will not accept null values for Description
			applicationGroup.Update(item.Description, applicationGroup.GroupType);

			return new ApplicationGroup(applicationGroup);
		}

		/// <summary>
		/// Gets all authorizations items of a specified user.
		/// </summary>
		/// <param name="username">The user name. If null string is passed (or an empty string) then the current user is retreived.</param>
		/// <param name="filterTypeOnly">if set to <c>true</c> [filter type only].</param>
		/// <returns></returns>
		public List<AuthorizationItem> AuthorizationItem_GetAll(string username, bool filterTypeOnly = false) {

			if (username == "")
				return new List<AuthorizationItem>();
			if (username == null) {
				MembershipUser user = Membership.GetUser();
				if (user == null)
					throw new Exception("No current user. You should login before execute this function.");
				username = user.UserName;

			}
			List<AuthorizationItem> list = GetAuthorizationItemsFromDB(username, filterTypeOnly);
			return list;
		}

		/// <summary>
		/// Gets the authorization items from DB.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="filterTypeOnly">if set to <c>true</c> [filter type only].</param>
		/// <returns></returns>
		private List<AuthorizationItem> GetAuthorizationItemsFromDB(string username, bool filterTypeOnly) {
			try {
				var tuple = GetCachedAzmanStorageAndUserSID(username);
				var storage = tuple.Item1;
				string userCustomSid = tuple.Item2;

                var innerCacheEntry = string.Format("{0}#{1}#{2}", m_AzmanCache[ContextHelper.ApplicationName].Key, ContextHelper.ApplicationName, userCustomSid);

				IEnumerable<AuthorizationItem> items = CacheService.Get(innerCacheEntry, () => {
					var azmanItems = storage.GetAuthorizedItems(m_StoreName, ContextHelper.ApplicationName, userCustomSid, DateTime.Now).Where(item => item.Authorization == AuthorizationType.Allow || item.Authorization == AuthorizationType.AllowWithDelegation);
                    IAzManApplication azManApplication = storage.Storage.Stores[m_StoreName].Applications[CachedApplication.Name];
					var retVal =
                        azmanItems.Where(item => IsUserInProject(item.Name, username)).Select(
							item =>
							new AuthorizationItem {
								Name = item.Name,
								ItemType = item.Type.ParseAsString(),
								IsTenancyTask = IsTenancyTask(item),
                                IsFilterOnly = item.Attributes.Count(attrib => attrib.Value == const_attribute_filter && attrib.Key == const_attribute_type) > 0,
                                Id = azManApplication.Items[item.Name].ItemId.ToString(),
                                Description = azManApplication.Items[item.Name].Description
							});
					return retVal.ToList();
				}, SessionService.Timeout.Minutes);

				if (filterTypeOnly)
					items = items.Where(item => item.IsFilterOnly);

				// We only allow requests to the tenancy service from within the administration tenant.
				bool isInAdministrationApplication = IsInAdministrationApplication();
				items = items.Where(item => isInAdministrationApplication || !(item.IsTenancyTask || IsTenancyOperationName(item.Name)));

				var list = items.ToList();
				return list;
			}
			catch (Exception) {
				TracingService.Write(TraceEntrySeverity.Warning, "BuildStorageCache on Azman storage cache failed - getting data through stored procedure");
				var list = new List<AuthorizationItem>();
                IAzManDBUser dBUser = CachedApplication.GetDBUser(username);
				IDataAccess db = new DataAccess();
				DataSet rolesDataset = db.ExecuteDataSet(const_proc_azman_getrolesforuser, dBUser.CustomSid.BinaryValue, DateTime.Now, "RootDSE", filterTypeOnly);

				list.AddRange(from DataRow row in rolesDataset.Tables[0].Rows
							  select new AuthorizationItem {
								  Id = row.Field<int>("ItemId").ToString(CultureInfo.InvariantCulture),
								  Name = row.Field<string>("Name"),
								  Description = row.Field<string>("Description"),
                                  ItemType = AzManExtensions.GetItemType(Convert.ToInt32(row.Field<object>("ItemType")))
							  });
				return list;
			}

		}

		/// <summary>
		/// Determines whether a user is in project or not (direct or indirect).
		/// </summary>
		/// <param name="projectName">Name of the project.</param>
		/// <param name="userName">Name of the user.</param>
		/// <returns>
		/// true if the user is in the project (direct or via an application group) or if it's not a project
		/// </returns>
        private bool IsUserInProject(string projectName, string userName) {
            var isUserInProject = false;
	        var authItem = CachedApplication.Items[projectName];
            // if it's a project
            if (IsAzManItemIsFilter(authItem)) {
                if (IsUserInProjectDirect(authItem, userName) || IsUserInProjectIndirect(authItem, userName)) {
                    isUserInProject = true;
                }
            }
            else {
                // it's not a project
                isUserInProject = true;
            }
	        return isUserInProject;
	    }

		/// <summary>
		/// Determines whether a user is an indirect member of a project (through application group).
		/// </summary>
		/// <param name="authItem">The authentication item.</param>
		/// <param name="userName">Name of the user.</param>
		/// <returns></returns>
	    private bool IsUserInProjectIndirect(IAzManItem authItem, string userName) {
            var userAppGroups = User_ApplicationGroup_GetList(userName).Select(app => app.Id);
            var projectAppGroups = AzManFilter_ApplicationGroupsAuthorization_GetList(authItem).Select(app => app.Id);
            var intersect =  userAppGroups.Intersect(projectAppGroups).Any();
            return intersect;
        }

	    /// <summary>
        /// Determines whether a user is a direct member of a project.
        /// </summary>
        /// <param name="project">The project.</param>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
	    private bool IsUserInProjectDirect(IAzManItem project, string userName) {
	        var isInProject = false;
            var usersList = AzManFilter_UserAuthorization_GetList(project.ItemId.ToString());

            foreach (var user in usersList) {
                if (user.UserName == userName) {
                    isInProject = true;
                    break;
                }
            }

	        return isInProject;
	    }


	    /// <summary>
		/// Determines whether the specified item is a tenancy task or not.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>
		///   <c>true</c> if the specified item is a tenancy task; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsTenancyTask(AuthorizedItem item) {
			bool isTenancyTask =
				item.Type == ItemType.Task &&
				item.Attributes != null &&
					item.Attributes.Any(
						kvp =>
						kvp.Key.Equals(const_azman_attribute_to_require_administration_tenant) &&
						kvp.Value.ToLower().Equals(bool.TrueString.ToLower()));

			return isTenancyTask;
		}

		/// <summary>
		/// Clears the cache username_UserSSID cache value.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <returns></returns>
		public void ClearCache(string username) {
			CacheService.Remove(username + CacheKey.const_cache_usercustomsid);
		}

	    /// <summary>
		/// Checks the access.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="itemName">Name of the item.</param>
		/// <returns></returns>
		public bool CheckAccess(string username, string itemName) {
			bool isTenancyService = IsTenancyOperationName(itemName);

			// We only allow requests to the tenancy service from within the administration tenant.
			if (isTenancyService && !IsInAdministrationApplication()) {
				return false;
			}

			var tuple = GetCachedAzmanStorageAndUserSID(username);
			var storage = tuple.Item1;
			string userCustomSid = tuple.Item2;

			//var userCustomSid = CacheService.Get(username + CacheKey.const_cache_userCustomSid,
			//    () => Application.GetDBUser(username).CustomSid.ToString());
			AuthorizationType access;
			try {
				List<KeyValuePair<string, string>> attributes;
				access = storage.CheckAccess(m_StoreName, ContextHelper.ApplicationName, itemName, userCustomSid, DateTime.Now, false, out attributes);
			}
			catch {
                access = this.GetStore().GetApplication(ContextHelper.ApplicationName).GetItem(itemName).CheckAccess(CachedApplication.GetDBUser(username), DateTime.Now, null);
			}
			return (access == AuthorizationType.Allow || access == AuthorizationType.AllowWithDelegation);
		}

		/// <summary>
		/// Determines whether [is tenancy operation name] [the specified item name].
		/// </summary>
		/// <param name="itemName">Name of the item.</param>
		/// <returns>
		///   <c>true</c> if [is tenancy operation name] [the specified item name]; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsTenancyOperationName(string itemName) {
			return itemName.StartsWith("Vizelia.FOL.WCFService.TenancyWCF", true, CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Determines whether [is in administration application].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is in administration application]; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsInAdministrationApplication() {
			return ContextHelper.ApplicationName.Equals(VizeliaConfiguration.Instance.Deployment.ApplicationName);
		}

		/// <summary>
		/// Gets an AuthorizationItem by its name.
		/// </summary>
		/// <param name="name">The name of the AuthorizationItem.</param>
		/// <returns></returns>
		public AuthorizationItem AuthorizationItem_GetByName(string name) {
			try {
                IAzManItem item = CachedApplication.Items[name];
				if (item == null)
					return null;
				return new AuthorizationItem(item);
			}
			catch {
				return null;
			}
		}

		/// <summary>
		/// Gets a store of auhorization for a specific application groups.
		/// </summary>
		/// <param name="id">The AzMan filter id for which to retreive the authorizations.</param>
		/// <returns></returns>
		public List<ApplicationGroup> AzManFilter_Authorization_GetList(string id) {
			var result = new List<ApplicationGroup>();
			if (id == null)
				return result;
            // AzManItem item = CachedApplication.GetItemById(id);
            var storage = GetCachedAzmanStorage();
		    IAzManItem item = storage.GetItemById(id);
            
			if (item == null)
				return result;
            var auths = item.Authorizations;

            var query = from auth in auths
						where (auth.AuthorizationType == AuthorizationType.Allow || auth.AuthorizationType == AuthorizationType.AllowWithDelegation)
						&& auth.SidWhereDefined == WhereDefined.Application
                        select new ApplicationGroup(storage.GetApplicationGroupById(auth.SID.StringValue));
			return query.ToList();
		}
		/// <summary>
		/// Gets a list of user authorized for a specific project.
		/// </summary>
		/// <param name="filterId">The AzMan filter id for which to retreive the authorizations.</param>
		/// <returns></returns>
		public List<FOLMembershipUser> AzManFilter_UserAuthorization_GetList(string filterId) {
			var users = new List<FOLMembershipUser>();
			if (filterId == null)
				return users;
            var storage = GetCachedAzmanStorage();
            IAzManItem item = storage.GetItemById(filterId);
			if (item == null)
				return users;
            var auths = item.Authorizations;

			users = auths.Where(auth => (auth.AuthorizationType == AuthorizationType.Allow || auth.AuthorizationType == AuthorizationType.AllowWithDelegation) && auth.SidWhereDefined == WhereDefined.Database)
							 .Select(auth => new FOLMembershipUser(Membership.GetUser(CachedApplication.GetDBUser(auth.SID).UserName))).ToList();

			return users;
		}

        /// <summary>
        /// Gets a list of application groups authorized for a specific project.
        /// </summary>
        /// <param name="azManItem">The azman item.</param>
        /// <returns></returns>
        public List<ApplicationGroup> AzManFilter_ApplicationGroupsAuthorization_GetList(IAzManItem azManItem) {
            var retAppGroups = new List<ApplicationGroup>();
            if (azManItem == null)
                return retAppGroups;

            var auths = azManItem.Authorizations.Where(auth => (auth.AuthorizationType == AuthorizationType.Allow || auth.AuthorizationType == AuthorizationType.AllowWithDelegation) && auth.SidWhereDefined == WhereDefined.Application)
                .Select(auth => new ApplicationGroup(auth.SID, azManItem));
            retAppGroups.AddRange(auths);
            return retAppGroups;
        }

		/// <summary>
		/// Gets user name for azman filter -project from predifined user cache.
        /// Gets direct users and users from application group 
		/// </summary>
		/// <param name="item">Azman item</param>
		/// <param name="dicUsers">User cache</param>
		/// <returns></returns>
		public List<string> AzManFilter_UserName_GetList(IAzManItem item, Dictionary<IAzManSid, IAzManDBUser> dicUsers) {
            var userNames = new Dictionary<string, string>();
            if (item == null)
                return userNames.Keys.ToList();
			var auths = item.Authorizations;
            var azmanStorage = GetCachedAzmanStorage();
            foreach (var auth in auths) {
                // Get direct users
                if (dicUsers.ContainsKey(auth.SID)) {
                    userNames[dicUsers[auth.SID].UserName] = string.Empty;
                } 
                // Get application group users
                else {
                    var applicationGroup = azmanStorage.GetApplicationGroupById(auth.SID.StringValue);
                    if (applicationGroup == null) continue;
                    foreach (var appGroupUserKeyPair in applicationGroup.Members) {
                        userNames[dicUsers[appGroupUserKeyPair.Key].UserName] = string.Empty;
                    }
                }
            }
            var listUserNames = userNames.Keys.ToList();
            listUserNames.Sort();

            return listUserNames;
		}
        
		/// <summary>
		/// Creates a new filter item.
		/// </summary>
		/// <param name="parent">The id of the parent of the item.</param>
		/// <param name="name">The name of the item.</param>
		/// <param name="description">The description of the item.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="filterLink"> </param>
		/// <returns>
		/// The authorization item created. Null if the creation failed.
		/// </returns>
		public AuthorizationItem AzManFilter_Create(string parent, string name, string description, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, List<SecurableEntity> filterLink) {
			AuthorizationItem azManFilterCreate =
				AzManItem_Create(parent, name, description, ItemType.Role, filterSpatial, filterReport, filterChart, filterAlarmDefinition, filterDynamicDisplay, filterPlaylist,
				filterChartScheduler, filterPortalTemplate, filterPortalWindow, filterClassificationItem, filterLink, const_attribute_type, const_attribute_filter);
			return azManFilterCreate;
		}

		/// <summary>
		/// Generates the json structure of the AzMan Filter treeview.
		/// </summary>
		/// <param name="itemId">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		public List<TreeNode> AzManFilter_GetTree(string itemId) {

			List<TreeNode> retVal = new List<TreeNode>();

			// query azman for permissions
			List<ApplicationGroup> authorization = AzManFilter_Authorization_GetList(itemId);

			List<TreeNode> childrenList = authorization.GetTree(true);

			if (childrenList.Count > 0) {
				TreeNode nodeAuthorization = new TreeNode {
					iconCls = "viz-icon-small-applicationgroup",
					text = Langue.msg_applicationgroup,
					children = childrenList
				};
				retVal.Add(nodeAuthorization);
			}

			// get all the securable types
			List<Type> securableTypes = Helper.GetSecurableTypes();

            // When we delete a securable entity, this entity as filter will be deleted by a database trigger. 
            // We have no control in server on this deletion and we can't know when to invalidate the azman cache.
            // Therefore we get the item from the DB and not from the azman cache.
            // TODO: Move filter deletion from the DB trigger into the server and work with the cached storage instead of the azman DB
            //var storage = GetCachedAzmanStorage();
            var storage = GetStore().GetApplication(ApplicationName);
            IAzManItem item = storage.GetItemById(itemId);
			if (item != null) {
                var attributes = item.Attributes.Values;

				foreach (Type securableType in securableTypes) {
					IAzManAttribute<IAzManItem> attribute = attributes.SingleOrDefault(p => p.Key == securableType.FullName + const_attribute_filter); // GetAttribute(item, securableType.FullName + const_attribute_filter);

					if (attribute != null) {
						var entityFilter = Helper.DeserializeXml<List<SecurableEntity>>(attribute.Value).OrderBy(p => p.SecurableName).ToList();
						List<TreeNode> childrenEntityFilterList = entityFilter.GetTree(true);
						if (childrenEntityFilterList.Count > 0) {
							TreeNode nodeFilter = new TreeNode {
								iconCls = Helper.GetAttributeValue<IconClsAttribute>(securableType),
								text = Helper.GetLocalizedText(securableType),
								children = childrenEntityFilterList
							};

							retVal.Add(nodeFilter);

						}
					}
				}
			}

			List<FOLMembershipUser> users = AzManFilter_UserAuthorization_GetList(itemId);
			if (users.Count > 0) {
				var nodeUsers = new TreeNode {
					iconCls = "viz-icon-small-user",
					text = Langue.msg_user_management,
					children = users.Select(user => new TreeNode {
						iconCls = "viz-icon-small-user",
						text = user.UserName,
						leaf = true
					}).ToList()
				};
				retVal.Add(nodeUsers);
			}

			List<TreeNode> nodes = this.AzManItem_GetTree(itemId, const_attribute_type, const_attribute_filter, true);

			retVal.AddRange(nodes);

			return retVal;
		}

		/// <summary>
		/// Gets the filter for an item.
		/// </summary>
		/// <param name="id">The id of the item.</param>
		/// <param name="searchByName">True to search by name instead of by the id.</param>
		/// <param name="typeName">Name of the type.</param>
		/// <returns>
		/// The list of filter elements.
		/// </returns>
		public List<SecurableEntity> AzManFilter_Filter_GetList(string id, bool searchByName, string typeName) {
			var retVal = new List<SecurableEntity>();
			if (id != null) {
                // When we delete a securable entity, this entity as filter will be deleted by a database trigger. 
                // We have no control in server on this deletion and we can't know when to invalidate the azman cache.
                // Therefore we get the item from the DB and not from the azman cache.
                // TODO: Move filter deletion from the DB trigger into the server and work with the cached storage instead of the azman DB
                //var storage = GetCachedAzmanStorage();
                var storage = GetStore().GetApplication(ApplicationName);
                IAzManItem item = searchByName ? CachedApplication.Items[id] : storage.GetItemById(id);
				if (item != null) {
					IAzManAttribute<IAzManItem> attributeSpatialFilter = this.GetAttribute(item, typeName + const_attribute_filter);
					if (attributeSpatialFilter != null) {
						retVal = Helper.DeserializeXml<List<SecurableEntity>>(attributeSpatialFilter.Value);
						retVal = retVal.OrderBy(p => p.SecurableName).ToList();
					}
				}
			}
			return retVal;
		}

		/// <summary>
		/// Updates a filter item.
		/// </summary>
		/// <param name="id">The id of the item to update.</param>
		/// <param name="name">The name of the item to update.</param>
		/// <param name="description">The description of the item to update.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="filterLink">The filter link.</param>
		/// <returns>
		/// True if operation was successfull, false otherwise.
		/// </returns>
		public IAzManItem AzManFilter_Update(string id, string name, string description, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, List<SecurableEntity> filterLink) {
			description = description ?? ""; // AzMan database will not accept null values for Description

            // IAzManItem item = this.CachedApplication.GetItemById(id);
            var storage = GetCachedAzmanStorage();
            IAzManItem item = storage.GetItemById(id);
			if (item == null)
				throw new ArgumentNullException("name");

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				if (item.Name != name)
					item.Rename(name);
				if (item.Description != description)
					item.Update(description);

				CreateAttributeFilter(item, filterSpatial, typeof(Location).FullName);
				CreateAttributeFilter(item, filterChart, typeof(Chart).FullName);
				CreateAttributeFilter(item, filterAlarmDefinition, typeof(AlarmDefinition).FullName);
				CreateAttributeFilter(item, filterDynamicDisplay, typeof(DynamicDisplay).FullName);
				CreateAttributeFilter(item, filterPlaylist, typeof(Playlist).FullName);
				CreateAttributeFilter(item, filterLink, typeof(Link).FullName);
				CreateAttributeFilter(item, filterChartScheduler, typeof(ChartScheduler).FullName);
				CreateAttributeFilter(item, filterPortalTemplate, typeof(PortalTemplate).FullName);
				CreateAttributeFilter(item, filterReport, typeof(Report).FullName);
				CreateAttributeFilter(item, filterPortalWindow, typeof(PortalWindow).FullName);
				CreateAttributeFilter(item, filterClassificationItem, typeof(ClassificationItem).FullName);

				t.Complete();
				return item;
			}
		}

		/// <summary>
		/// Updates a azman task.
		/// </summary>
		/// <param name="id">The id of the item to update.</param>
		/// <param name="name">The name of the item to update.</param>
		/// <param name="description">The description of the item to update.</param>
		/// <returns>
		/// True if operation was successfull, false otherwise.
		/// </returns>
		public IAzManItem AzManTask_Update(string id, string name, string description) {
			description = description ?? ""; // AzMan database will not accept null values for Description

            // IAzManItem item = CachedApplication.GetItemById(id);
            var storage = GetCachedAzmanStorage();
            IAzManItem item = storage.GetItemById(id);
			if (item == null)
				throw new ArgumentNullException("name");
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				if (item.Name != name)
					item.Rename(name);
				if (item.Description != description)
					item.Update(description);

				t.Complete();
				return item;
			}
		}

		/// <summary>
		/// Add members to an AzMan item.
		/// </summary>
		/// <param name="parent">The id of the AzMan item.</param>
		/// <param name="children">The list of AzManItem to add as members.</param>
		/// <returns></returns>
		private bool AzManItem_AddMembers(string parent, IEnumerable<string> children) {
            return AzManItem_OperateMembers(parent, children, AzManExtensions.AddMemberCheckExist);
		}

		/// <summary>
		/// Creates a new AzMan item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		private AuthorizationItem AzManItem_Create(AuthorizationItem item) {
            var result = CachedApplication.CreateItem(item.Name, item.Description, item.ItemType.ParseAsEnum<ItemType>());
			return new AuthorizationItem(result);
		}


		/// <summary>
		/// Copies an azman role.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>True if the copy worked, false otherwise.</returns>
		public bool AzManRole_Copy(AuthorizationItem item) {
            IAzManItem originalItem = CachedApplication.Items[item.Name];
            IAzManItem newItem = CachedApplication.CreateItem("copy_" + item.Name, item.Description, ItemType.Role);
			foreach (IAzManItem member in originalItem.Members.Values) {
				newItem.AddMember(member);
			}
			return true;
		}

		/// <summary>
		/// Creates a new azman item.
		/// </summary>
		/// <param name="parent">The id of the parent of the item.</param>
		/// <param name="name">The name of the item.</param>
		/// <param name="description">The description of the item.</param>
		/// <param name="itemType">The item type.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="filterLink">The filter link.</param>
		/// <param name="attributeFilterName">the attribute filter name</param>
		/// <param name="attributeFilterValue">the attribute filter value</param>
		/// <returns>
		/// The authorization item created. Null if the creation failed.
		/// </returns>
		private AuthorizationItem AzManItem_Create(string parent, string name, string description, ItemType itemType, List<SecurableEntity> filterSpatial,
			List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay,
			List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate,
			List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, List<SecurableEntity> filterLink, string attributeFilterName, string attributeFilterValue) {
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				IAzManItem itemParent = null;
				try {
                    //itemParent = CachedApplication.GetItemById(parent);
                    var storage = GetCachedAzmanStorage();
                    itemParent = storage.GetItemById(parent);
				}
				catch { ; }
				// the azman table does not accept null in column description
				description = String.IsNullOrEmpty(description) ? "" : description;
				IAzManItem item;
				try {
                    item = CachedApplication.CreateItem(name, description, itemType);
				}
				catch (ArgumentException) {
					throw new VizeliaDatabaseException(101);
				}

				AuthorizationItem result = new AuthorizationItem(item);

				if (item != null) {
					if (itemParent != null)
						itemParent.AddMember(item);
					if (!string.IsNullOrEmpty(attributeFilterName))
						item.CreateAttribute(attributeFilterName, attributeFilterValue);

					CreateAttributeFilter(item, filterSpatial, typeof(Location).FullName);
					CreateAttributeFilter(item, filterReport, typeof(Report).FullName);
					CreateAttributeFilter(item, filterChart, typeof(Chart).FullName);
					CreateAttributeFilter(item, filterAlarmDefinition, typeof(AlarmDefinition).FullName);
					CreateAttributeFilter(item, filterDynamicDisplay, typeof(DynamicDisplay).FullName);
					CreateAttributeFilter(item, filterPlaylist, typeof(Playlist).FullName);
					CreateAttributeFilter(item, filterLink, typeof(Link).FullName);
					CreateAttributeFilter(item, filterChartScheduler, typeof(ChartScheduler).FullName);
					CreateAttributeFilter(item, filterPortalTemplate, typeof(PortalTemplate).FullName);
					CreateAttributeFilter(item, filterPortalWindow, typeof(PortalWindow).FullName);
					CreateAttributeFilter(item, filterClassificationItem, typeof(ClassificationItem).FullName);
				}
				t.Complete();

				return result;
			}
		}

		/// <summary>
		/// Deletes an azman item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public bool AzManItem_Delete(AuthorizationItem item) {
            // IAzManItem azmanItem = CachedApplication.GetItemById(item.Id);
            var storage = GetCachedAzmanStorage();
            IAzManItem azmanItem = storage.GetItemById(item.Id);
			if (azmanItem != null) {
				azmanItem.Delete();
				return true;
			}
			return false;
		}

		/// <summary>
		/// Gets all the AzMan items.
		/// </summary>
		/// <returns></returns>
		private List<AuthorizationItem> AzManItem_GetAll(ItemType itemType) {

			IDataAccess db = new DataAccess();
			DataSet ds = db.ExecuteDataSet(const_proc_azman_item_getall, ContextHelper.ApplicationName, itemType);
			var retVal = (from DataRow row in ds.Tables[0].Rows
						  select new AuthorizationItem {
							  Id = row.Field<int>("ItemId").ToString(CultureInfo.InvariantCulture),
							  Name = row.Field<string>("Name"),
							  Description = row.Field<string>("Description"),
                              ItemType = AzManExtensions.GetItemType(Convert.ToInt32(row.Field<object>("ItemType")))
						  }).ToList();
			return retVal;


			//var query = from item in Application.GetItems(itemType)
			//            select item;

			//return query.OrderBy(p => p.Name).Select(p => new AuthorizationItem(p)).ToList();
		}



		/// <summary>
		/// Gtes an AzMan item.
		/// </summary>
		/// <param name="Key">The Key.</param>
		/// <returns></returns>
		public AuthorizationItem AzManItem_GetItem(string Key) {
            // IAzManItem item = CachedApplication.GetItemById(Key);
            var storage = GetCachedAzmanStorage();
            IAzManItem item = storage.GetItemById(Key);
			if (item == null)
				throw new ArgumentNullException();
			return new AuthorizationItem(item);
		}

		/// <summary>
		/// Generates a list of IAzMan item.
		/// </summary>
		/// <param name="itemId">The Key of the parent node. If null will return the root role</param>
		/// <param name="attributeFilterName">The name of the attribute for filtering (Can be null).</param>
		/// <param name="attributeFilterValue">The value of the attribute for filtering (Can be null).</param>
		/// <param name="normalCheck">If false, reverse the check to allow getting all nodes except the ones corresponding to the check.</param>
		/// <returns>a list of IAzManItem.</returns>
		private List<IAzManItem> AzManItem_GetList(string itemId, string attributeFilterName, string attributeFilterValue, bool normalCheck) {
            var result = new List<IAzManItem>();           
            var azmanItems = CachedApplication.Items.Values.Where(item => item.ItemType == ItemType.Role).ToList();

			if (!String.IsNullOrEmpty(itemId))
				itemId = itemId.Trim();
			if (String.IsNullOrEmpty(itemId)) {                
                foreach (var item in azmanItems) {
                    // We're using IsRoot extension method to avoid all members of all items enumeration occurs while accessing IAzManItem.ItemsWhereIAmAMember.
                    if (item.IsRoot(azmanItems)) {
                        if (AzManItem_HasAttributeFilter(item, attributeFilterName, attributeFilterValue, normalCheck)) {
                            result.Add(item);
                        } 
                    }
				}
			}
			else {
                // IAzManItem item = CachedApplication.GetItemById(itemId);
                var storage = GetCachedAzmanStorage();
                IAzManItem item = storage.GetItemById(itemId);

				if (item == null)
					throw new ArgumentNullException("itemId");
                foreach (var role in item.Members.Values) {
                    if (AzManItem_HasAttributeFilter(role, attributeFilterName, attributeFilterValue, normalCheck)) {
						result.Add(role);
				}
			}
            }
			return result;
		}

		/// <summary>
		/// Generates the json structure of the AzMan item treeview.
		/// </summary>
		/// <param name="itemId">The Key of the parent node.</param>
		/// <param name="attributeFilterName">The name of the attribute for filtering (Can be null).</param>
		/// <param name="attributeFilterValue">The value of the attribute for filtering (Can be null).</param>
		/// <param name="normalCheck">If false, reverse the check to allow getting all nodes except the ones corresponding to the check.</param>
		/// <returns>a list of treenodes in json.</returns>
		private List<TreeNode> AzManItem_GetTree(string itemId, string attributeFilterName, string attributeFilterValue, bool normalCheck) {
			return this.AzManItem_GetList(itemId, attributeFilterName, attributeFilterValue, normalCheck).Select(p => new AuthorizationItem(p).GetTree()).OrderBy(p => p.text).ToList();
		}



		/// <summary>
		/// Gets the children of an AzMan item.
		/// </summary>
		/// <param name="itemId">The Key of the parent node.</param>
		/// <returns></returns>
		public List<AuthorizationItem> AzManItem_GetChildren(string itemId) {
			return this.AzManItem_GetList(itemId, null, null, true).Select(p => new AuthorizationItem(p)).ToList();
		}

		/// <summary>
		/// Checks if the attributeFilterName exists, and is equals to the attributeFilterValue if this parameter is not null.
		/// </summary>
		/// <param name="item">The item on which to perform the check.</param>
		/// <param name="attributeFilterName">The name of the attribute.</param>
		/// <param name="attributeFilterValue">The value of the attribute (if null then only the function will only check if the attributeFilterName exists on the item).</param>
		/// <param name="normalCheck">If false, reverse the check to allow getting all nodes except the ones corresponding to the check.</param>
		/// <returns></returns>
		private bool AzManItem_HasAttributeFilter(IAzManItem item, string attributeFilterName, string attributeFilterValue, bool normalCheck) {
			bool result;
			if (String.IsNullOrEmpty(attributeFilterName))
				return normalCheck == true;
			result = item.Attributes.ContainsKey(attributeFilterName);
			if (!result || String.IsNullOrEmpty(attributeFilterValue))
				return normalCheck == result;
            return normalCheck == (item.Attributes[attributeFilterName].Value == attributeFilterValue);
		}

		/// <summary>
		/// Add or Remove members to an AzMan item.
		/// </summary>
		/// <param name="parent">The id of the AzMan item.</param>
		/// <param name="children">The list of AzManItem to operation as members.</param>
		/// <param name="operateMember">The operation to execute.</param>
		/// <returns></returns>
		private bool AzManItem_OperateMembers(string parent, IEnumerable<string> children, OperateMember operateMember) {
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				IAzManItem itemParent = null;
				try {
                    // itemParent = CachedApplication.GetItemById(parent);
                    var storage = GetCachedAzmanStorage();
                    itemParent = storage.GetItemById(parent);
				}
				catch { }
				if (itemParent == null)
					return false;
				foreach (string id in children) {
					IAzManItem item = null;
					try {
                        // item = CachedApplication.GetItemById(id);
                        var storage = GetCachedAzmanStorage();
                        item = storage.GetItemById(id);
					}
					catch { }
					if (item == null)
						return false;
					operateMember(itemParent, item);
				}

				t.Complete();
				return true;
			}
		}

		/// <summary>
		/// Removes members from an AzManItem. 
		/// </summary>
		/// <param name="parent">The id of AzManItem parent.</param>
		/// <param name="children">The list of ids of AzManItem to remove as members.</param>
		/// <returns></returns>
		private bool AzManItem_RemoveMembers(string parent, IEnumerable<string> children) {
            return AzManItem_OperateMembers(parent, children, AzManExtensions.RemoveMemberCheckExist);
		}

		/// <summary>
		/// Creates a new Operation item.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="description">The description.</param>
		/// <returns></returns>
		public AuthorizationItem AzManOperation_Create(string name, string description) {
			return AzManItem_Create(null, name, description, ItemType.Operation, null, null, null, null, null, null, null, null, null, null, null, null, null);
		}

		/// <summary>
		/// Gets all the AzMan operations.
		/// </summary>
		/// <returns></returns>
		public List<AuthorizationItem> AzManOperation_GetAll() {
			return this.AzManItem_GetAll(ItemType.Operation);
		}

		/// <summary>
		/// Creates a new Role item.
		/// </summary>
		/// <param name="parent">The id of the parent of the item.</param>
		/// <param name="name">The name of the item.</param>
		/// <param name="description">The description of the item.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="filterLink"> </param>
		/// <returns>
		/// True if operation was successfull, false otherwise.
		/// </returns>
		public AuthorizationItem AzManRole_Create(string parent, string name, string description, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, List<SecurableEntity> filterLink) {
			AuthorizationItem retVal = AzManItem_Create(parent, name, description, ItemType.Role, filterSpatial, filterReport, filterChart, filterAlarmDefinition,
				filterDynamicDisplay, filterPlaylist, filterChartScheduler, filterPortalTemplate, filterPortalWindow, filterClassificationItem, filterLink, null, null);
			return retVal;
		}



		/// <summary>
		/// Generates the json structure of the AzMan Role treeview.
		/// </summary>
		/// <param name="itemId">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		public List<TreeNode> AzManRole_GetTree(string itemId) {
			return this.AzManItem_GetTree(itemId, const_attribute_type, const_attribute_filter, false);
		}

		/// <summary>
		/// Gets the azman tree starting from the given item.
		/// </summary>
		/// <param name="itemId">The item id</param>
		/// <returns>
		/// A list of tree nodes
		/// </returns>
		public List<TreeNode> AzManItem_GetAncestorTree(string itemId) {
			List<IAzManItem> result = new List<IAzManItem>();
			if (!String.IsNullOrEmpty(itemId))
				itemId = itemId.Trim();
            // IAzManItem item = CachedApplication.GetItemById(itemId);
            var storage = GetCachedAzmanStorage();
            IAzManItem item = storage.GetItemById(itemId);
			if (item == null)
				throw new ArgumentNullException("itemId");
			foreach (var role in item.ItemsWhereIAmAMember) {
				result.Add(role.Value);
			}

			return result.Select(p => new AuthorizationItem(p).GetTree()).OrderBy(p => p.text).ToList();
		}


		/// <summary>
		/// Get the store of saved AzmanRoles for a specific Chart KPI.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> AzManRole_GetStoreByKeyChartKPI(PagingParameter paging, string KeyChart) {
			IDataAccess db = new DataAccess();
			DataSet ds = db.ExecuteDataSet(const_proc_azman_role_getallbykeychart, ContextHelper.ApplicationName, KeyChart);
			var retVal = (from DataRow row in ds.Tables[0].Rows
						  select new AuthorizationItem {
							  Id = row.Field<int>("ItemId").ToString(),
							  Name = row.Field<string>("ItemName"),
							  Description = row.Field<string>("ItemDescription"),
                              ItemType = AzManExtensions.GetItemType(Convert.ToInt32(row.Field<object>("ItemType")))
						  }).ToList();
			return retVal.ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Creates a new Task item.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="description">The description.</param>
		/// <returns></returns>
		public AuthorizationItem AzManTask_Create(string name, string description) {
			var retVal = AzManItem_Create(null, name, description, ItemType.Task, null, null, null, null, null, null, null, null, null, null, null, null, null);
			return retVal;
		}

		/// <summary>
		/// Adds and removes operations.
		/// </summary>
		/// <param name="authorizationItems">The operations.</param>
		/// <param name="authorizationItem">The ret val.</param>
		public bool AddRemoveAuthorizationItems(AuthorizationItem authorizationItem, CrudStore<AuthorizationItem> authorizationItems) {
			var retval = true;
			if (authorizationItems != null && authorizationItems.destroy != null && authorizationItems.destroy.Count > 0)
				retval = AzManItem_RemoveMembers(authorizationItem.Id, authorizationItems.destroy.Select(p => p.Id).ToList());
			if (authorizationItems != null && authorizationItems.create != null && authorizationItems.create.Count > 0)
				retval = AzManItem_AddMembers(authorizationItem.Id, authorizationItems.create.Select(p => p.Id).ToList());
			return retval;
		}

		/// <summary>
		/// Gets all the AzMan tasks.
		/// </summary>
		/// <returns></returns>
		public List<AuthorizationItem> AzManTask_GetAll() {
			return this.AzManItem_GetAll(ItemType.Task);
		}

		private string GetFilterNameByTypeName(string typeName) {
			return typeName + const_attribute_filter;
		}

		/// <summary>
		/// Creates (or recreates) the filter spatial attribute.
		/// </summary>
		/// <param name="item">The item to create the attribute on.</param>
		/// <param name="filters">The filters.</param>
		/// <param name="typeName">Name of the type.</param>
		/// <returns>
		/// The created attribute.
		/// </returns>
		public IAzManAttribute<IAzManItem> CreateAttributeFilter(IAzManItem item, List<SecurableEntity> filters, string typeName) {
			IAzManAttribute<IAzManItem> result = null;

			if (filters != null) {
				IAzManAttribute<IAzManItem> attributeFilter = this.GetAttribute(item, GetFilterNameByTypeName(typeName));
				if (attributeFilter != null)
					attributeFilter.Delete();
				result = item.CreateAttribute(GetFilterNameByTypeName(typeName), Helper.SerializeXml(filters));
			}
			return result;
		}

		/// <summary>
		/// Creates the list of authorizations for the specified item.
		/// </summary>
		/// <param name="item">The item to create the authorizations for.</param>
		/// <param name="authorizations">The list of users authorized.</param>
		private void CreateAuthorizations(IAzManItem item, IEnumerable<FOLMembershipUser> authorizations) {
			if (authorizations == null)
				return;
			IAzManSid ownerSid;
			WhereDefined ownerLocation;
			GetOwnerInfo(out ownerSid, out ownerLocation);
            var previousAuthorizations = item.Authorizations;
			foreach (var auth in previousAuthorizations) {
				auth.Delete();
			}
			foreach (var auth in authorizations) {
                item.CreateAuthorization(ownerSid, ownerLocation, CachedApplication.GetDBUser(auth.UserName).CustomSid, WhereDefined.Database, AuthorizationType.Allow, null, null);
			}
		}

		/// <summary>
		/// Creates the list of authorizations for the specified item.
		/// </summary>
		/// <param name="authorizationItem">The authorization item.</param>
		/// <param name="applicationGroups">The list of application groups authorized.</param>
		public void CreateAzManItemApplicationGroups(AuthorizationItem authorizationItem, CrudStore<ApplicationGroup> applicationGroups) {
			if (applicationGroups != null && (applicationGroups.create != null || applicationGroups.destroy != null)) {
				IAzManSid ownerSid;
				WhereDefined ownerLocation;
				GetOwnerInfo(out ownerSid, out ownerLocation);
				var item = authorizationItem.Item;

                var currAuthorizations = item.Authorizations;
				IEnumerable<ApplicationGroup> joinedList = applicationGroups.create ?? new List<ApplicationGroup>();
				if (applicationGroups.destroy != null) {
					joinedList = joinedList.Concat(applicationGroups.destroy);
				}
				var joinedIdList = joinedList.Select(x => x.Id);
				foreach (var currAuth in currAuthorizations.Where(x => joinedIdList.Contains(x.SID.StringValue))) {
					currAuth.Delete();
				}

				if (applicationGroups.create != null) {
					foreach (var authorization in applicationGroups.create) {
						item.CreateAuthorization(ownerSid, ownerLocation, new SqlAzManSID(authorization.Id), WhereDefined.Application,
												 AuthorizationType.Allow, null, null);
					}
				}
			}
		}

		/// <summary>
		/// Creates the list of authorizations for the specified item.
		/// </summary>
		/// <param name="authorizationItem">The authorization item.</param>
		/// <param name="users">The users.</param>
		public void CreateAzManItemMembers(AuthorizationItem authorizationItem, CrudStore<FOLMembershipUser> users) {
			if (users != null) {
				IAzManSid ownerSid;
				WhereDefined ownerLocation;
				GetOwnerInfo(out ownerSid, out ownerLocation);
				var item = authorizationItem.Item;

				if (users.destroy != null) {
					var usersToDestroy = (from user in users.destroy
                                          let sid = CachedApplication.GetDBUser(user.UserName).CustomSid
										  select new {
											  sid,
											  user
										  }).ToList();

                    var previousAuthorizations = item.Authorizations;
					foreach (var previousAuthorization in previousAuthorizations) {
						if (usersToDestroy.Exists(user => user.sid.ToString() == previousAuthorization.SID.ToString())) {
							//if (users.destroy.Exists(user => (previousAuthorization.SID.ToString() == Application.GetDBUser(user.UserName).CustomSid.ToString()))) {
							previousAuthorization.Delete();
						}
					}

				}
				if (users.create != null) {
                    var previousAuthorizations = item.Authorizations;
					foreach (var user in users.create) {
                        var userSid = CachedApplication.GetDBUser(user.UserName).CustomSid;
                        if (previousAuthorizations.All(x => x.SID.ToString() != userSid.ToString())) {
							item.CreateAuthorization(ownerSid, ownerLocation, userSid, WhereDefined.Database, AuthorizationType.Allow, null, null);
						}
					}
				}
			}
		}

		/// <summary>
		/// Create members for an application group.
		/// </summary>
		/// <param name="applicationGroup">The application group.</param>
		/// <param name="members">The list of users to add as members of the application group.</param>
		public void CreateGroupMembers(ApplicationGroup applicationGroup, CrudStore<FOLMembershipUser> members) {
            // IAzManApplicationGroup appGroup = CachedApplication.GetApplicationGroupById(applicationGroup.Id);
            var storage = GetCachedAzmanStorage();
            IAzManApplicationGroup appGroup = storage.GetApplicationGroupById(applicationGroup.Id);
			if (members != null) {

				if (members.destroy != null) {
					foreach (var member in members.destroy) {
						IAzManApplicationGroupMember memberToDestroy;
                        appGroup.Members.TryGetValue(CachedApplication.GetDBUser(member.UserName).CustomSid, out memberToDestroy);
						if (memberToDestroy != null) {
							memberToDestroy.Delete();
						}
					}
				}
				if (members.create != null) {
					foreach (var member in members.create) {
						IAzManApplicationGroupMember currentMember;
                        appGroup.Members.TryGetValue(CachedApplication.GetDBUser(member.UserName).CustomSid, out currentMember);
						if (currentMember == null) {
                            appGroup.CreateApplicationGroupMember(CachedApplication.GetDBUser(member.UserName).CustomSid, WhereDefined.Database, true);
						}
					}
				}
			}
		}

		/// <summary>
		/// Modify the list of authorizations for an application group.
		/// </summary>
		/// <param name="applicationGroup">The application group.</param>
		/// <param name="roles">The roles.</param>
		/// <param name="userName">Name of the user.</param>
		public void CreateGroupAuthorizations(ApplicationGroup applicationGroup, CrudStore<AuthorizationItem> roles, string userName = null) {
			if (roles == null)
				return;
			IAzManSid ownerSid;
			WhereDefined ownerLocation;
			GetOwnerInfo(out ownerSid, out ownerLocation, userName);
			if (roles.destroy != null) {
				foreach (AuthorizationItem role in roles.destroy) {
                    // IAzManItem item = CachedApplication.GetItemById(role.Id);
                    var storage = GetCachedAzmanStorage();
                    IAzManItem item = storage.GetItemById(role.Id);
                    var previousAuthorizations = item.Authorizations;
					foreach (var auth in previousAuthorizations) {
						if (auth.SID.StringValue == applicationGroup.Id)
							auth.Delete();
					}
				}
			}
			if (roles.create != null) {
				foreach (AuthorizationItem role in roles.create) {
                    // IAzManItem item = CachedApplication.GetItemById(role.Id);
                    var storage = GetCachedAzmanStorage();
                    IAzManItem item = storage.GetItemById(role.Id);
					if (item != null && (!item.Authorizations.Select(auth => auth.SID.StringValue).Contains(applicationGroup.Id)))
						item.CreateAuthorization(ownerSid, ownerLocation, new SqlAzManSID(applicationGroup.Id), WhereDefined.Application, AuthorizationType.Allow, null, null);
				}
			}
		}

		/// <summary>
		/// Exports the AzMan configuration as a xml stream file.
		/// </summary>
		/// <returns>The xml stream file.</returns>
		public MemoryStream Export() {
			var stream = new MemoryStream();
			using (XmlWriter writer = XmlTextWriter.Create(stream)) {
                // CachedApplication.Export(writer, true, true, true, null);
                // using CachedApplication.Export produces an error : "ExecuteReader requires an open and available Connection. The connection's current state is open."
                this.GetStore().GetApplication(Membership.ApplicationName).Export(writer, true, true, true, null);
				writer.Flush();
			}

			stream.Seek(0, SeekOrigin.Begin);
			TextReader textReader = new StreamReader(stream);
			string azmanXmlString = textReader.ReadToEnd();
			stream.Dispose();
			//Due to a bug in the azman - the export from the code does not produce the same xml as the export from the mmc
			//Therefore we need to add <NetSqlAzMan> around the result
			int actualXMLStart = azmanXmlString.IndexOf('>') + 1;
			azmanXmlString = azmanXmlString.Insert(actualXMLStart, "<NetSqlAzMan><Store Name=\"Vizelia\" Description=\"\"><Attributes /><Permissions><Managers /><Users /><Readers /></Permissions><StoreGroups /><Applications>");
			azmanXmlString = azmanXmlString + "</Applications></Store></NetSqlAzMan>";
			// create XmlDocument object 
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(azmanXmlString);

			StringWriter sw = new StringWriter();
			XmlTextWriter xw = new XmlTextWriter(sw) { Formatting = Formatting.Indented };
			xmlDoc.WriteTo(xw);
			azmanXmlString = sw.ToString();

			//Recreate the stream.
			MemoryStream memoryStream = new MemoryStream(UTF8Encoding.Default.GetBytes(azmanXmlString));
			memoryStream.Seek(0, SeekOrigin.Begin);
			return memoryStream;
		}

		/// <summary>
		/// Imports the specified XML node.
		/// </summary>
		/// <param name="xmlNode">The XML node.</param>
		/// <param name="importOptions">The import options.</param>
		/// <param name="userName">Name of the user.</param>
		public void Import(XmlNode xmlNode, AzmanImportOptions importOptions, string userName = null) {
			Helper.RetryIfFailed(3, 100, () => ImportInternal(xmlNode, importOptions, userName));
		}

		/// <summary>
		/// Imports the specified XML node.
		/// </summary>
		/// <param name="xmlNode">The XML node.</param>
		/// <param name="importOptions">The import options.</param>
		/// <param name="userName">Name of the user.</param>
		private void ImportInternal(XmlNode xmlNode, AzmanImportOptions importOptions, string userName = null) {

			var nodeApplication = xmlNode.SelectSingleNode("//Store/Applications/Application") as XmlElement;

			// make sure store always exist.
			IAzManStore store = null;
			try {
				store = GetStore();
			}
			catch { }
			if (store == null)
				store = GetStorage().CreateStore(StoreName, StoreName);

			if (nodeApplication != null)
				nodeApplication.SetAttribute("Name", ContextHelper.ApplicationName);

			SqlAzManMergeOptions azManMergeOptions = SqlAzManMergeOptions.CreatesNewItems;
			if (importOptions == AzmanImportOptions.CreateOnly) {
				azManMergeOptions = SqlAzManMergeOptions.CreatesNewItems;
			}
			if (importOptions == AzmanImportOptions.CreateAndOverwrite) {
				azManMergeOptions = SqlAzManMergeOptions.CreatesNewItems | SqlAzManMergeOptions.OverwritesExistingItems;
			}
			if (importOptions == AzmanImportOptions.FullImport) {
				// We delete exisiting application and recreate it.
				IAzManApplication app = null;
				try {
                    app = store.Applications[ContextHelper.ApplicationName];
				}
				catch { }
				if (app != null)
					app.Delete();
				// azManMergeOptions = SqlAzManMergeOptions.CreatesNewItems | SqlAzManMergeOptions.OverwritesExistingItems | SqlAzManMergeOptions.DeleteMissingItems;
				azManMergeOptions = SqlAzManMergeOptions.CreatesNewItems; // A lot faster than previous settings. 
			}
			//	GetStore().ImportChildren(xmlNode, false, false, false, azManMergeOptions);
			GetStorage().ImportChildren(xmlNode, false, false, false, azManMergeOptions);

			AssociateRolesToApplicationGroups(importOptions, userName);
			AssociateSystemUsersToApplicationGroups();
			InvalidateStorageCache();
		}

		/// <summary>
		/// Associates the system users to application groups.
		/// These are the Admin user to Admins group and Scheduler to Schedulers group.
		/// </summary>
		private void AssociateSystemUsersToApplicationGroups() {
			AssociateUserToGroup(AdminUserName, AdminsApplicationGroupName);
			AssociateUserToGroup(SchedulerUserName, SchedulersApplicationGroupName);
		}

		/// <summary>
		/// Associates the user to an application group.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="applicationGroupName">Name of the application group.</param>
		private void AssociateUserToGroup(string userName, string applicationGroupName) {
			var adminsGroup = ApplicationGroup_GetAll().FirstOrDefault(appGroup => appGroup.Name == applicationGroupName);

			if (adminsGroup != null) {
                if (!adminsGroup.Members.Values.Select(member => member.SID).Contains(CachedApplication.GetDBUser(userName).CustomSid)) {
                    adminsGroup.CreateApplicationGroupMember(CachedApplication.GetDBUser(userName).CustomSid, WhereDefined.Database, true);
				}
			}
		}


		/// <summary>
		/// Associates the roles to application groups.
		/// </summary>
		/// <param name="importOptions">The import options.</param>
		/// <param name="userName">Name of the user.</param>
		private void AssociateRolesToApplicationGroups(AzmanImportOptions importOptions, string userName = null) {
			var allApplicationGroups = ApplicationGroup_GetAll().Select(appGroup => new ApplicationGroup(appGroup));
			List<AuthorizationItem> allRoles = AzManRole_GetAll();

			foreach (var applicationGroup in allApplicationGroups) {
				var role = allRoles.FirstOrDefault(rl => rl.Name == applicationGroup.Name);
				if (role != null) {
					CreateGroupAuthorizations(applicationGroup, new CrudStore<AuthorizationItem> {
						create = new List<AuthorizationItem> { role }
					}, userName);
				}
				else {
					if (importOptions == AzmanImportOptions.FullImport) {
						ApplicationGroup_Delete(applicationGroup);
					}
				}
			}
		}

		/// <summary>
		/// Gets all the azman roles
		/// </summary>
		/// <returns>A list of all the azman items that are roles.</returns>
		private List<AuthorizationItem> AzManRole_GetAll() {
            return CachedApplication.Items.Values.Where(i => i.ItemType == ItemType.Role).Select(role => new AuthorizationItem(role)).ToList();
		}

		/// <summary>
		/// Imports the specified XML .
		/// </summary>
		/// <param name="xmlStream">The XML stream.</param>
		/// <param name="importOptions">The import options.</param>
		public void Import(Stream xmlStream, AzmanImportOptions importOptions) {
			//for security reasons we need to use the settings to prohibit dtd
			var settings = new XmlReaderSettings { IgnoreComments = true, IgnoreWhitespace = true, IgnoreProcessingInstructions = true, DtdProcessing = DtdProcessing.Prohibit };
			XmlReader reader = XmlReader.Create(xmlStream, settings);

			XmlDocument xmlDoc = new XmlDocument();

			xmlDoc.Load(reader);

			Import(xmlDoc.DocumentElement, importOptions);
		}


		/// <summary>
		/// Gets an attribute from an item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="key">The attribute key.</param>
		/// <returns>The attribute. Returns null if does not exist.</returns>
		private IAzManAttribute<IAzManItem> GetAttribute(IAzManItem item, string key) {
			IAzManAttribute<IAzManItem> result = null;
			if (item.Attributes.ContainsKey(key)) {
                result = item.Attributes[key];
			}
			return result;
		}

		/// <summary>
		/// Gets the authorizations for an item.
		/// </summary>
		/// <param name="id">The id of the item.</param>
		/// <returns>The list of spatial filter elements.</returns>
		public List<FOLMembershipUser> GetAuthorization(string id) {
			List<FOLMembershipUser> result = new List<FOLMembershipUser>();
			if (id == null)
				return result;
            // IAzManItem item = CachedApplication.GetItemById(id);
            var storage = GetCachedAzmanStorage();
            IAzManItem item = storage.GetItemById(id);
			if (item == null)
				throw new ArgumentNullException("id");
            var auths = item.Authorizations;
			var query = from auth in auths
						where (auth.AuthorizationType == AuthorizationType.Allow || auth.AuthorizationType == AuthorizationType.AllowWithDelegation)
						&& auth.SidWhereDefined == WhereDefined.Database
                        select new FOLMembershipUser(Membership.GetUser(CachedApplication.GetDBUser(auth.SID).UserName));
			return query.ToList();
		}

		/// <summary>
		/// Gets the complete list of spatial elements associated to a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="filterType">Type of the filter.</param>
		/// <returns>
		/// The list of spatial elements associated to the user.
		/// </returns>
		public List<SecurableEntity> GetFilterForUser(string username, Type filterType) {
			List<SecurableEntity> result = new List<SecurableEntity>();
			string[] roles = GetFilterRolesForUser(username);
			foreach (string role in roles) {
				result.AddRange(AzManFilter_Filter_GetList(role, true, filterType.FullName));
			}
			var retVal = result.Distinct().ToList();
			return retVal;
		}

		/// <summary>
		/// Gets all azman filters with paging and search keyword
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="totalRecordCount"></param>
		/// <returns></returns>
		public List<AuthorizationItem> AzManFilter_GetAll(PagingParameter paging, out int totalRecordCount) {
            var items = CachedApplication.Items.Values;
			var list = items.
				Where(item => IsAzManItemIsFilter(item) && IsAzManItemContainsQueryString(item, paging.query)).
				Select(item => new AuthorizationItem(item, true)).AsQueryable().Filter(paging).ToList();
			totalRecordCount = list.Count;
			list = list.Rank(paging.sort, paging.dir, paging.start, paging.limit);

			return list;
		}

		/// <summary>
		/// Gets azman filters by key location
		/// </summary>
		/// <param name="keyLocations"></param>
		public List<AuthorizationItem> AzManFilter_GetByLocation(string[] keyLocations) {
            var items = CachedApplication.Items.Values;
			
			var dicUsers = CachedApplication.GetDBUsers().ToDictionary(user => user.CustomSid);
			var list = items.
				Where(item => IsAzManItemIsFilter(item) && IsAzManItemAuthorizedForLocation(item, keyLocations)).
				Select(item => {
					var authItem = new AuthorizationItem(item, true);
					authItem.UserNames = AzManFilter_GetUserNames(authItem, dicUsers);
					return authItem;
				}).ToList();
			
			return list;
		}

		/// <summary>
		/// Gets a concatinated string of user names for AzManFilter
		/// </summary>
		/// <param name="authorizationItem"></param>
		/// <param name="dicUsers"></param>
		private string AzManFilter_GetUserNames(AuthorizationItem authorizationItem, Dictionary<IAzManSid, IAzManDBUser> dicUsers) {
			var userNamesList = AzManFilter_UserName_GetList(authorizationItem.Item, dicUsers);
			var userNames = userNamesList.Aggregate(string.Empty, (total, name2) => total + name2 + ", ").TrimEnd(new[] { ',', ' ' });
			
			return userNames;
		}

		/// <summary>
		/// Check if azman item name contains query, if query is empty they true
		/// </summary>
		/// <param name="item"></param>
		/// <param name="query"></param>
		/// <returns></returns>
		private bool IsAzManItemContainsQueryString(IAzManItem item, string query) {
			var result = string.IsNullOrEmpty(query) || item.Name.ToUpper().Contains(query.ToUpper());
			return result;
		}

		/// <summary>
		/// Check if azman item is a filter project. Optionally check if it is authorized for location key
		/// </summary>
		/// <param name="item"></param>
		/// <param name="locationKeys"></param>
		/// <returns></returns>
		private bool IsAzManItemAuthorizedForLocation(IAzManItem item, string[] locationKeys) {
			if (locationKeys == null) return true;
			var locationKeysWrapped = locationKeys.Select(s => "<KeySecurable>" + s + "</KeySecurable>").ToArray();
			
			var result = item.Attributes.ContainsKey(const_attribute_locationfilter) &&
			             item.Attributes[const_attribute_locationfilter].Value.ContainsAny(locationKeysWrapped);
			
			return result;
		}
		
		/// <summary>
		/// Check if azman item is a filter project.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		private bool IsAzManItemIsFilter(IAzManItem item) {
			return item.Attributes.ContainsKey(const_attribute_type) && item.Attributes[const_attribute_type].Value == const_attribute_filter;
		}
		
		/// <summary>
		/// Gets the complete list of securable elements associated to a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="filterTypes">The filter types.</param>
		/// <returns>
		/// The list of securable elements associated to the user.
		/// </returns>
		public Dictionary<string, List<SecurableEntity>> GetFiltersDictionaryForUser(string username, List<Type> filterTypes) {
			Dictionary<string, List<SecurableEntity>> retVal = new Dictionary<string, List<SecurableEntity>>();
			string[] roles = GetFilterRolesForUser(username);
			foreach (var filterType in filterTypes) {
				List<SecurableEntity> result = new List<SecurableEntity>();
				foreach (string role in roles) {
					result.AddRange(AzManFilter_Filter_GetList(role, true, filterType.FullName));
				}
				retVal.Add(filterType.FullName, result.Distinct().ToList());
			}
			return retVal;
		}

		/// <summary>
		/// Retreives the owner informations needed when saving an authorization.
		/// </summary>
		/// <param name="ownerSid">The owner Sid.</param>
		/// <param name="ownerLocation">The owner location.</param>
		/// <param name="ownerUserName">Name of the owner user. Default null for current user.</param>
		private void GetOwnerInfo(out IAzManSid ownerSid, out WhereDefined ownerLocation, string ownerUserName = null) {
			if ((Thread.CurrentPrincipal.Identity as FormsIdentity) != null) {
				if (ownerUserName == null) {
                    ownerSid = CachedApplication.GetDBUser((Thread.CurrentPrincipal.Identity as FormsIdentity).Name).CustomSid;
				}
				else {
                    ownerSid = CachedApplication.GetDBUser(ownerUserName).CustomSid;
				}

				ownerLocation = WhereDefined.Database;
			}
			else {
				ownerSid = new SqlAzManSID((WindowsIdentity.GetCurrent().User));
				ownerLocation = WhereDefined.Local;
			}
		}

		/// <summary>
		///  Gets a list of the roles that a specified user is in for the configured applicationName.
		/// </summary>
		/// <param name="username">The user to return a list of roles for.</param>
		/// <returns>A string array containing the names of all the roles that the specified user is in for the configured applicationName.</returns>
		public override string[] GetRolesForUser(string username) {
			// We don't change the behavior for the LDAP mode as we won't use it
			if (UserLookupType == "LDAP")
				return base.GetRolesForUser(username);

			string[] strRoles = AuthorizationItem_GetAll(username).Select(p => p.Name).ToArray();
			return strRoles;
		}

		/// <summary>
		/// Gets the filter roles for user.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <returns></returns>
		private string[] GetFilterRolesForUser(string username) {
			return AuthorizationItem_GetAll(username, true).Select(p => p.Name).ToArray();
		}

		// We need to override this to allow a custom name for the provider in web.config.
		// The parent class NetSqlAzManRoleProvider does not respect the name attribute of the configuration section and search for the specific entry "NetSqlAzManRoleProvider"
		/// <summary>
		///  We need to override this to allow a custom name for the provider in web.config.
		///  The parent class NetSqlAzManRoleProvider does not respect the name attribute of the configuration section and search for the specific entry "NetSqlAzManRoleProvider"
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="config">The config.</param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			_name = name;
			config["applicationName"] = ContextHelper.ApplicationName;
			try {
				base.Initialize(name, config);
			}
			catch { }
		}

		/// <summary>
		/// Gets a value indicating whether the specified user is in the specified role for the configured applicationName.
		/// </summary>
		/// <param name="username">The user name to search for.</param>
		/// <param name="roleName">The role to search in.</param>
		/// <returns>true if the specified user is in the specified role for the configured applicationName, false otherwise.</returns>
		public override bool IsUserInRole(string username, string roleName) {
			// We don't change the behavior for the LDAP mode as we won't use it
			if (UserLookupType == "LDAP")
				return base.IsUserInRole(username, roleName);

            IAzManItem item = CachedApplication[roleName];
            IAzManDBUser dBUser = CachedApplication.GetDBUser(username);

			AuthorizationType type2 = item.CheckAccess(dBUser, DateTime.Now, new KeyValuePair<string, object>[0]);
			if (type2 != AuthorizationType.Allow) {
				return (type2 == AuthorizationType.AllowWithDelegation);
			}
			return true;
		}

        /// <summary>
        /// Retrieves a list of application groups.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, List<string>> Users_ApplicationGroups_GetAll() {
            var applicationGroups = CachedApplication.ApplicationGroups.Values;
            var usersApplicationGroupsDictionary = new Dictionary<string, List<string>>();

            foreach (var azManApplicationGroup in applicationGroups) {
                foreach (var azManApplicationGroupMember in azManApplicationGroup.Members) {
                    // we're using GetKeyUserFromSID method to avoid translation of user sid to userKey through the azman cache or database. 
                    string userKey = GetKeyUserFromSID(azManApplicationGroupMember.Value.SID);
                    if (!String.IsNullOrEmpty(userKey)) {
                        if (usersApplicationGroupsDictionary.ContainsKey(userKey)) {
                            usersApplicationGroupsDictionary[userKey].Add(azManApplicationGroup.Name);
                        }
                        else {
                            usersApplicationGroupsDictionary.Add(userKey, new List<string> { azManApplicationGroup.Name });
                        } 
                    }
                }
            }

            return usersApplicationGroupsDictionary;
        }

        /// <summary>
        /// Gets the key user from sid.
        /// </summary>
        /// <param name="sid">The sid.</param>
        /// <returns></returns>
	    private string GetKeyUserFromSID(IAzManSid sid) {
	        return Encoding.UTF8.GetString(sid.BinaryValue);
	    }

        /// <summary>
        /// Gets the cached azman storage.
        /// </summary>
		/// <param name="pAzmanCacheStateGuid">The Guid that is used for cache sync flag.</param>
        /// <returns></returns>
        private ExtendedAzManStorage GetCachedAzmanStorage(Guid? pAzmanCacheStateGuid) {
            Guid azmanCacheStateGuid;
			if (pAzmanCacheStateGuid.HasValue) {
				azmanCacheStateGuid = pAzmanCacheStateGuid.Value;
            }
            else {
                azmanCacheStateGuid = Guid.NewGuid();
                CacheService.Add(CacheKey.const_cache_azmancacheguid, azmanCacheStateGuid);
            }

            if (!m_AzmanCache.ContainsKey(ContextHelper.ApplicationName) || m_AzmanCache[ContextHelper.ApplicationName].Key.CompareTo(azmanCacheStateGuid) != 0) {
                lock (azmanLock) {
                    var localStorageCache = new ExtendedAzManStorage(this.storageCache.ConnectionString);
                    localStorageCache.BuildStorageCache(m_StoreName, ContextHelper.ApplicationName);
                    m_AzmanCache.Remove(ContextHelper.ApplicationName);
                    m_AzmanCache.Add(ContextHelper.ApplicationName, new KeyValuePair<Guid, ExtendedAzManStorage>(azmanCacheStateGuid, localStorageCache));
                }
            }

            return m_AzmanCache[ContextHelper.ApplicationName].Value;
	    }

	    /// <summary>
		/// Gets the cached azman storage.
		/// </summary>
		/// <returns></returns>
		private ExtendedAzManStorage GetCachedAzmanStorage() {
			var cacheKeys = new List<string> { CacheKey.const_cache_azmancacheguid };
			var cacheData = CacheService.GetData(cacheKeys);
			var azmanCacheGuid = GetAzmanCacheGuid(cacheData);

			return GetCachedAzmanStorage(azmanCacheGuid);
		}

		/// <summary>
		/// Extracts azman cache guid from dictionary
		/// </summary>
		/// <param name="cacheData"></param>
		/// <returns></returns>
		private static Guid? GetAzmanCacheGuid(IDictionary<string, object> cacheData) {
			Guid? azmanCacheGuid = null;
			if (cacheData.ContainsKey(CacheKey.const_cache_azmancacheguid)) {
                azmanCacheGuid = (Guid)cacheData[CacheKey.const_cache_azmancacheguid];
			}
			return azmanCacheGuid;
		}

		/// <summary>
		/// Gets cached azman storage with user Sid.
		/// Getting two keys together for performance.
		/// </summary>
		/// <param name="username"></param>
		/// <returns></returns>
		private Tuple<StorageCache, string> GetCachedAzmanStorageAndUserSID(string username) {
			var userKey = username + CacheKey.const_cache_usercustomsid;
			var cacheKeys = new List<string> { CacheKey.const_cache_azmancacheguid, userKey };
			var cacheData = CacheService.GetData(cacheKeys);
			var azmanCacheGuid = GetAzmanCacheGuid(cacheData);
			var storage = GetCachedAzmanStorage(azmanCacheGuid);

			string userCustomSid;
			if (cacheData.ContainsKey(userKey)) {
				userCustomSid = cacheData[userKey].ToString();
			}
			else {
                userCustomSid = CachedApplication.GetDBUser(username).CustomSid.ToString();
				CacheService.Add(userKey, userCustomSid);
			}

			var result = new Tuple<StorageCache, string>(storage, userCustomSid);
			return result;
		}

		/// <summary>
		/// Gets the list of application group for a specific user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns></returns>
		public List<ApplicationGroup> User_ApplicationGroup_GetList(string username) {
			var result = new List<ApplicationGroup>();
			if (username == "")
				return result;
		    var user = new FOLMembershipUser(Membership.GetUser(username));
            var userSID = CreateSqlAzmanSID(user.KeyUser);
            var applicationGroups = CachedApplication.ApplicationGroups.Values;
			result.AddRange(from applicationGroup in applicationGroups
                            where  applicationGroup.Members.ContainsKey(userSID)
							select new ApplicationGroup(applicationGroup));
			return result;
		}


        /// <summary>
        /// Creates the SQL azman sid from the user's key.
        /// </summary>
        /// <param name="stringToConvert">The string to convert.</param>
        /// <returns></returns>
        private static SqlAzManSID CreateSqlAzmanSID(string stringToConvert) {
            var bytes =  Encoding.UTF8.GetBytes(stringToConvert);
            return new SqlAzManSID(bytes, true);
        }


	    /// <summary>
		/// Removes reference to a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		public void User_Remove(string username) {
            IAzManDBUser user = CachedApplication.GetDBUser(username);
            var applicationGroups = CachedApplication.ApplicationGroups.Values;
			foreach (IAzManApplicationGroup applicationGroup in applicationGroups) {
				if (applicationGroup.IsInGroup(user))
					foreach (var member in applicationGroup.Members) {
						if (member.Value.SID.StringValue == user.CustomSid.StringValue)
							member.Value.Delete();
					}
			}
			//If this user is recreated with the same username it will not have the same Sid
			CacheService.Remove(username + CacheKey.const_cache_usercustomsid);
		}


		/// <summary>
		/// Invalidates the storage cache.
		/// Also, creates a new guid for azman cache state - this is done so that if we run this on multiple servers we can know cross server if we need to refresh the cache
		/// </summary>
		public void InvalidateStorageCache() {
			Guid newGuid = Guid.NewGuid();
			try {
				if (!m_AzmanCache.ContainsKey(ContextHelper.ApplicationName)) {
					lock (azmanLock) {
                        var localStorageCache = new ExtendedAzManStorage(storageCache.ConnectionString);
						localStorageCache.BuildStorageCache(m_StoreName, ContextHelper.ApplicationName);
                        m_AzmanCache.Add(ContextHelper.ApplicationName, new KeyValuePair<Guid, ExtendedAzManStorage>(newGuid, localStorageCache));
					}
				}
				else {
					m_AzmanCache[ContextHelper.ApplicationName].Value.BuildStorageCache(m_StoreName, ContextHelper.ApplicationName);
				}
			}
			catch {
				//TracingService.Write(TraceEntrySeverity.Warning, "InvalidateStorageCache on Azman storage cache failed");
				//TracingService.Write(e);
			}
			CacheService.Remove(CacheKey.const_cache_azmancacheguid);
			CacheService.Add(CacheKey.const_cache_azmancacheguid, newGuid);
		}

		/// <summary>
		/// Removes the authorizations for a user.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		public void RemoveAuthorizationForUser(string userName) {
			IDataAccess db = new DataAccess();
            var user = CachedApplication.GetDBUser(userName);
			if (user == null) return;
			db.ExecuteNonQuery(const_proc_azman_removeuser, user.CustomSid.BinaryValue);
		}
	}
}

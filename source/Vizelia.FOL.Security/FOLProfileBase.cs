﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Profile;
using Vizelia.FOL.BusinessEntities;
using System.Web.Configuration;
using System.Configuration;
using System.Web.Security;
using System.Reflection;
using System.Web.Hosting;
using System.Web;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Vizelia.FOL.Security {

	/// <summary>
	/// The specific Profile.
	/// Derives from ProfileBase and contains a business entity profile.
	/// Any specific profile properties should be added exclusively to the business entity, and this class can remain unchanged.
	/// </summary>
	public class FOLProfileBase : ProfileBase {
		private static bool s_Initialized;
		private static object s_InitializeLock;
		private static Exception s_InitializeException;

		private FOLProfileEntity _entity;

		/// <summary>
		/// Private static ctor.
		/// </summary>
		static FOLProfileBase() {
			s_InitializeLock = new object();
			s_Initialized = false;
			s_InitializeException = null;
		}

		/// <summary>
		/// Public ctor.
		/// We execute the static initialization (just once) and we instanciate the entity member.
		/// </summary>
		public FOLProfileBase() {
			if (!s_Initialized) {
				InitializeStatic();
			}
			_entity = new FOLProfileEntity(this);
		}

		/// <summary>
		/// Gets the entity member.
		/// </summary>
		public FOLProfileEntity entity {
			get {
				return _entity;
			}
		}

		/// <summary>
		/// Gets the profile for the specified username.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <returns>The profile of the user.</returns>
		public static FOLProfileBase GetProfile(string username) {
			return (FOLProfileBase)ProfileBase.Create(username);
		}

		/// <summary>
		/// Gets the current user's profile.
		/// </summary>
		public static FOLProfileBase Current {
			get {
				return (FOLProfileBase)System.Web.HttpContext.Current.Profile;
			}
		}

		/// <summary>
		/// Private static initialization module.
		/// The code is an adaptation of ProfileBase.InitializeStatic.
		/// we've added reflection in order to get to private members and fields.
		/// </summary>
		static void InitializeStatic() {
			if (!ProfileManager.Enabled || s_Initialized) {
				if (s_InitializeException != null) {
					throw s_InitializeException;
				}
			}
			else {
				lock (s_InitializeLock) {
					if (s_Initialized) {
						if (s_InitializeException != null) {
							throw s_InitializeException;
						}
						return;
					}
					try {
						// gets the profile section.
						ProfileSection profile = (ProfileSection)ConfigurationManager.GetSection("system.web/profile");

						bool fAnonEnabled = HostingEnvironment.IsHosted ? AnonymousIdentificationModule.Enabled : true;
						PropertyInfo property_InheritsFromType = typeof(ProfileBase).GetProperty("InheritsFromType", BindingFlags.NonPublic | BindingFlags.Static);

						// gets the inherited type for Profile.
						Type inheritsFromType = (Type)property_InheritsFromType.GetValue(null, null);

						bool hasLowTrust = (bool)typeof(HttpRuntime).GetMethod("HasAspNetHostingPermission", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, new object[] { AspNetHostingPermissionLevel.Low });

						// Properties collection was previously set to readonly by ProfileBase.
						// we make it writable in order to add the public properties of business entity representing a profile.
						FieldInfo field_s_Properties = typeof(ProfileBase).GetField("s_Properties", BindingFlags.NonPublic | BindingFlags.Static);
						SettingsPropertyCollection s_Properties = (SettingsPropertyCollection)field_s_Properties.GetValue(null);
						FieldInfo field_ReadOnly = s_Properties.GetType().GetField("_ReadOnly", BindingFlags.NonPublic | BindingFlags.Instance);
						field_ReadOnly.SetValue(s_Properties, false);

						if (inheritsFromType != typeof(ProfileBase)) {
							NameValueCollection values = new NameValueCollection(); //properties.Length);

							foreach (PropertyInfo info2 in typeof(FOLProfileEntity).GetProperties(BindingFlags.Public | BindingFlags.Instance)) {
								if (values[info2.Name] == null) {
									ProfileProvider provider = hasLowTrust ? ProfileManager.Provider : null;
									bool isReadOnly = false;
									SettingsSerializeAs providerSpecific = SettingsSerializeAs.ProviderSpecific;
									string defaultValue = string.Empty;
									bool allow = false;
									string customProviderData = null;
									foreach (Attribute attribute in Attribute.GetCustomAttributes(info2, true)) {
										if (attribute is SettingsSerializeAsAttribute) {
											providerSpecific = ((SettingsSerializeAsAttribute)attribute).SerializeAs;
										}
										else if (attribute is SettingsAllowAnonymousAttribute) {
											allow = ((SettingsAllowAnonymousAttribute)attribute).Allow;
											if (!fAnonEnabled && allow) {
												throw new ConfigurationErrorsException(String.Format("The Profile property '{0}' allows anonymous users to store data. This requires that the AnonymousIdentification feature be enabled.", info2.Name), profile.ElementInformation.Properties["inherits"].Source, profile.ElementInformation.Properties["inherits"].LineNumber);
											}
										}
										else if (attribute is ReadOnlyAttribute) {
											isReadOnly = ((ReadOnlyAttribute)attribute).IsReadOnly;
										}
										else if (attribute is DefaultSettingValueAttribute) {
											defaultValue = ((DefaultSettingValueAttribute)attribute).Value;
										}
										else if (attribute is CustomProviderDataAttribute) {
											customProviderData = ((CustomProviderDataAttribute)attribute).CustomProviderData;
										}
										else if (hasLowTrust && (attribute is ProfileProviderAttribute)) {
											provider = ProfileManager.Providers[((ProfileProviderAttribute)attribute).ProviderName];
											if (provider == null) {
												throw new ConfigurationErrorsException(String.Format("The profile provider was not found '{0}'", ((ProfileProviderAttribute)attribute).ProviderName), profile.ElementInformation.Properties["inherits"].Source, profile.ElementInformation.Properties["inherits"].LineNumber);
											}
										}
									}
									SettingsAttributeDictionary attributes = new SettingsAttributeDictionary();
									attributes.Add("AllowAnonymous", allow);
									if (!string.IsNullOrEmpty(customProviderData)) {
										attributes.Add("CustomProviderData", customProviderData);
									}
									SettingsProperty property = new SettingsProperty(info2.Name, info2.PropertyType, provider, isReadOnly, defaultValue, providerSpecific, attributes, false, true);
									s_Properties.Add(property);
								}
							}
						}
					}
					catch (Exception exception) {
						if (s_InitializeException == null) {
							s_InitializeException = exception;
						}
					}
					ProfileBase.Properties.SetReadOnly();
					s_Initialized = true;
				}
				if (s_InitializeException != null) {
					throw s_InitializeException;
				}
			}
		}
	}

}

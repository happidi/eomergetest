﻿//------------------------------------------------------------------------------
// <copyright file="BasicDigestAuthenticationModule.cs" company="Schneider Electric">
//     Copyright (c) Schneider Electric.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Net;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Security;

using Vizelia.FOL.Providers;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Security {
	/// <summary>
	/// BasicDigestAuthenticationModule
	/// </summary>
	public class BasicDigestAuthenticationModule : IHttpModule {
		private const int AccessDeniedStatusCode = 401;
		private const string AccessDeniedStatusText = "Access Denied";
		private const string AuthenticateHeaderName = "WWW-Authenticate";
		private const string BasicAuthenticationMethodName = "Basic";
		private const string DigestAuthenticationMethodName = "Digest";
		private const string AuthorizationHeaderName = "Authorization";

        /// <summary>
        /// Check if Basic Authentication is enabled (web.config appSettings - "BasicAuthentication")
        /// </summary>
        private bool BasicAuthentication { 
            get {return CheckConfig("BasicAuthentication");}
        }

        /// <summary>
        /// Check if Digest Authentication is enabled (web.config appSettings - "DigestAuthentication")
        /// </summary>
        private bool DigestAuthentication
        {
            get {return CheckConfig("DigestAuthentication");} 
        }

		private string Realm { get; set; }

		private HttpApplication Application { get; set; }

		/// <summary>
		/// Disposes of the resources (other than memory) used by the module that implements <see cref="T:System.Web.IHttpModule"/>.
		/// </summary>
		public void Dispose() { }

		/// <summary>
		/// Inits the specified module.
		/// </summary>
		/// <param name="application">The HTTP application containing the module.</param>
		public void Init(HttpApplication application) {
			Application = application;

			Realm = GetConfig("Realm");
			if (String.IsNullOrEmpty(Realm)) {
				Realm = Membership.ApplicationName;
			}

			application.AuthenticateRequest += OnAuthenticateRequest;
			application.EndRequest += OnEndRequest;
		}

		/// <summary>
		/// Handles the AuthenticateRequest event of the application control.
		/// </summary>
		/// <param name="sender">The sender of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		/// <remarks>
		/// The main work is done here. We parse incoming headers, get user name and password from them.
		/// Then we verify the user name and password against configured membership provider.
		/// </remarks>
		public virtual void OnAuthenticateRequest(object sender, EventArgs e) {
			var app = sender as HttpApplication;
			if ((app != null) && ((app.Context.User == null) || !app.Context.User.Identity.IsAuthenticated)) {
                if (BasicAuthentication || DigestAuthentication)
                {
                    // Get authentication data
                    string authString = app.Request.Headers[AuthorizationHeaderName];

                    if (!String.IsNullOrEmpty(authString))
                    {
                        authString = authString.Trim();

                        if (BasicAuthentication && authString.StartsWith(BasicAuthenticationMethodName, StringComparison.OrdinalIgnoreCase))
                        {
                            DoBasicAuthentication(app, authString);
                        }
                        else
                            if (DigestAuthentication && authString.StartsWith(DigestAuthenticationMethodName, StringComparison.OrdinalIgnoreCase))
                            {
                                DoDigestAuthentication(app, authString);
                            }
                    }

                    if ((app.Context.User == null) || !app.Context.User.Identity.IsAuthenticated)
                    {
                        HttpContext.Current.Items["authcode"] = 1;
                    }
                }
			}
		}

		/// <summary>
		/// Handles the EndRequest event of the application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		/// <remarks>
		/// This methods adds the "WWW-Authenticate" challenge header to all requests ending with the
		/// HTTP status code 401 (Access Denied), set either in this module's application_AuthenticateRequest
		/// or in any other place in application (most likely the authorization module).
		/// </remarks>
		public virtual void OnEndRequest(object sender, EventArgs e) {
			// Add WWW-Authenticate header if access denied
			var app = sender as HttpApplication;
			if (app != null) {
				if ((HttpContext.Current.Items["authcode"] != null) && (app.Response.StatusCode == (int)HttpStatusCode.InternalServerError)) {
					//app.Response.Clear();
					if (BasicAuthentication) {
						app.Response.AppendHeader(AuthenticateHeaderName, BasicAuthenticateHeader());
					}

					if (DigestAuthentication) {
						app.Response.AppendHeader(AuthenticateHeaderName, DigestAuthenticateHeader());
					}

					// 401 Access Denied
					AccessDenied(app);
				}
			}
		}

		private void DoBasicAuthentication(HttpApplication app, string authString) {
			// Get username and password
			string username;
			string password;
			try {
				authString = Encoding.UTF8.GetString(Convert.FromBase64String(authString.Substring(6)));
				string[] authParts = authString.Split(new[] { ':' }, 2);
				username = authParts[0];
				password = authParts[1];
			}
			catch (Exception ex) {
				throw new System.Security.SecurityException("Invalid format of '" + AuthorizationHeaderName + "' HTTP header.", ex);
			}

			string parsedUsername;
			if (ValidateUser(username, password, out parsedUsername)) {
				string[] roles = GetRoles(parsedUsername);

				// Success - set user
				app.Context.User = new GenericPrincipal(new GenericIdentity(parsedUsername, BasicAuthenticationMethodName), roles);

				// Login User
				LoginValidatedUser(username, password);
			}
		}

		private void DoDigestAuthentication(HttpApplication app, string authString) {
			// get Header parts
			// write them to the ListDictionary object
			var dictAuthHeaderContents = GetHeaderParts(authString);

			var username = (string)dictAuthHeaderContents["username"];
			string password;
			if (!GetPassword(username, out password)) {
				AccessDenied(app);
				return;
			}

			// A1 = unq(username-value) ":" unq(realm-value) ":" passwd
			string a1 = String.Format("{0}:{1}:{2}",
									  dictAuthHeaderContents["username"],
									  Realm,
									  password);

			// HA1 = MD5(A1)
			string ha1 = CvtHex(a1);

			// A2 = HTTP Method ":" digest-uri-value
			string a2 = String.Format("{0}:{1}",
									  app.Request.HttpMethod,
									  dictAuthHeaderContents["uri"]);

			// HA2 = MD5(A2)
			string ha2 = CvtHex(a2);

			// genresponse = 
			// HA1 ":" nonce ":" nc ":" cnonce ":" qop ":" HA2
			string genresponse;
			if (dictAuthHeaderContents["qop"] != null) {
				genresponse = String.Format("{0}:{1}:{2}:{3}:{4}:{5}",
											ha1,
											(string)dictAuthHeaderContents["nonce"],
											(string)dictAuthHeaderContents["nc"],
											(string)dictAuthHeaderContents["cnonce"],
											(string)dictAuthHeaderContents["qop"],
											ha2);
			}
			else {
				genresponse = String.Format("{0}:{1}:{2}",
											ha1,
											dictAuthHeaderContents["nonce"],
											ha2);
			}

			string hgenresponse = CvtHex(genresponse);


			// Check the nonce
			bool isNonceStale = !IsValidNonce((string)dictAuthHeaderContents["nonce"]);
			app.Context.Items["staleNonce"] = isNonceStale;

			// Check HGENRESPONSE 
			// against the response of the Authorization header
			// Check the nonce
			// if everything is ok - 
			// create GenericPrincipal instance, which contains roles
			string testResponse = dictAuthHeaderContents["response"].ToString();
			if ((testResponse == hgenresponse) && (!isNonceStale)) {
				string[] roles = GetRoles(username);

				// Success - set user
				app.Context.User = new GenericPrincipal(new GenericIdentity(username, DigestAuthenticationMethodName), roles);

				// Login User
				LoginValidatedUser(username, password);
			}
		}

		/// <summary>
		/// LoginValidatedUser
		/// </summary>
		/// <param name="username"></param>
		/// <param name="password"></param>
		private void LoginValidatedUser(string username, string password) {
			// Need to Insure the Current User is Authenticated for Service layer
			string pageId;

			// Need to create EO Cookie
			var module = Helper.Resolve<IAuthenticationBusinessLayer>();
			var retVal = module.Login(username, password, false, null, out pageId, null);

			// PageID on Response
			HttpContext.Current.Response.Headers.Add(SessionService.const_page_id, pageId);

			// PageID for Request
			// HttpContext.Current.Request.Headers.Add(SessionService.const_page_id, pageId);
		}

		private string BasicAuthenticateHeader() {
			var authHeader = new StringBuilder(BasicAuthenticationMethodName);

			// build WWW-Authenticate server response header
			authHeader.Append(" realm=\"");
			authHeader.Append(Realm);
			authHeader.Append("\"");

			return authHeader.ToString();
		}

		private string DigestAuthenticateHeader() {
			var authHeader = new StringBuilder(DigestAuthenticationMethodName);

			string lOpaque = GetConfig("AuthDigest_Opaque");
			string lAlgorithm = GetConfig("AuthDigest_Algorithm");
			string lQop = GetConfig("AuthDigest_Qop");

			// generate
			string lNonce = GenerateNonce();

			bool isNonceStale = false;
			object staleObj = Application.Context.Items["staleNonce"];
			if (staleObj != null) {
				isNonceStale = (bool)staleObj;
			}

			// build WWW-Authenticate server response header
			authHeader.Append(" realm=\"");
			authHeader.Append(Realm);
			authHeader.Append("\"");
			authHeader.Append(", nonce=\"");
			authHeader.Append(lNonce);
			authHeader.Append("\"");
			authHeader.Append(", opaque=\"");
			authHeader.Append(lOpaque);
			authHeader.Append("\"");
			authHeader.Append(", stale=");
			authHeader.Append(isNonceStale ? "true" : "false");
			authHeader.Append(", algorithm=\"");
			authHeader.Append(lAlgorithm);
			authHeader.Append("\"");
			authHeader.Append(", qop=\"");
			authHeader.Append(lQop);
			authHeader.Append("\"");

			return authHeader.ToString();
		}

		private static string GenerateNonce() {
			// Create a unique nonce - 
			// the simpliest version
			// Now + 3 minutes, encoded base64
			// The nonce validity check 
			// will be performed also against the time
			// More strong example of nonce - 
			// use additionally ETag and unique key, which is
			// known by the server
			DateTime nonceTime = DateTime.Now + TimeSpan.FromMinutes(3);
			string expireStr = nonceTime.ToString("G");

			Encoding enc = new ASCIIEncoding();
			byte[] expireBytes = enc.GetBytes(expireStr);
			string nonce = Convert.ToBase64String(expireBytes);

			// base64 adds "=" 
			// sign, which is forbidden by the server
			// cut it
			nonce = nonce.TrimEnd(new[] { '=' });
			return nonce;
		}

		/// <summary>
		/// IsValidNonce
		/// </summary>
		/// <param name="nonce">nonce value</param>
		/// <returns></returns>
		protected bool IsValidNonce(string nonce) {
			// Check nonce validity
			// decode from base64 and check
			// This implementation uses a simple version - 
			// thats why the check is simple also -
			// check against the time

			DateTime expireTime;
			int numPadChars = nonce.Length % 4;
			if (numPadChars > 0)
				numPadChars = 4 - numPadChars;
			string newNonce =
			   nonce.PadRight(nonce.Length + numPadChars, '=');

			try {
				byte[] decodedBytes =
				   Convert.FromBase64String(newNonce);
				string preNonce =
				   new ASCIIEncoding().GetString(decodedBytes);
				expireTime = DateTime.Parse(preNonce);
			}
			catch (FormatException) {
				return false;
			}
			return (expireTime >= DateTime.Now);
		}

		private static string CvtHex(string sToConvert) {
			// Hashing
			Encoding enc = new ASCIIEncoding();
			MD5 md5 = new MD5CryptoServiceProvider();
			byte[] bToConvert =
			   md5.ComputeHash(enc.GetBytes(sToConvert));
			string sConverted = "";
			for (int i = 0; i < 16; i++)
				sConverted +=
				   String.Format("{0:x02}", bToConvert[i]);
			return sConverted;
		}

		private static ListDictionary GetHeaderParts(string authorization) {
			// A method, that converts 
			// HTTP header string with all its contents
			// to the ListDictionary object
			var dict = new ListDictionary();
			string[] parts = authorization.Substring(7).Split(new[] { ',' });
			foreach (string part in parts) {
				string[] subParts = part.Split(new[] { '=' }, 2);
				string key = subParts[0].Trim(new[] { ' ', '\"' });
				string val = subParts[1].Trim(new[] { ' ', '\"' });
				dict.Add(key, val);
			}
			return dict;
		}

		private static bool CheckConfig(string cfg) {
			bool retval;
			Boolean.TryParse(ConfigurationManager.AppSettings[cfg], out retval);
			return retval;
		}

		private static string GetConfig(string cfg) {
			return ConfigurationManager.AppSettings[cfg];
		}

		/// <summary>
		/// AccessDenied
		/// </summary>
		/// <param name="app">HTTP Application</param>
		protected static void AccessDenied(HttpApplication app) {
			// Access denied
			app.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
			app.Response.StatusDescription = AccessDeniedStatusText;
			// app.Response.Write("401 Access Denied");
			app.CompleteRequest();
		}

		private static bool ValidateUser(string username, string password, out string parsedUsername) {
			bool retVal;

			try {
				//var membershipProvider = (IFOLSqlMembershipProvider)Membership.Provider;
				//retVal = membershipProvider.ValidateUser(username, password);
				var authenticationBL = Helper.Resolve<IAuthenticationBusinessLayer>();
				string parsedApplicationName;
				authenticationBL.ParseApplicationAndUsername(username, out parsedApplicationName, out parsedUsername);
				retVal = Membership.ValidateUser(parsedUsername, password);
			}
			catch (Exception) {
				parsedUsername = null;
				retVal = false;
			}

			return retVal;
		}

		private static bool GetPassword(string username, out string password) {
			bool retVal = false;

			password = null;

			try {
				var user = Membership.GetUser(username);
				if (user != null) {
					password = user.GetPassword();
					retVal = true;
				}
			}
			catch (Exception) {
				retVal = false;
			}

			return retVal;
		}

		private static string[] GetRoles(string username) {
			string[] roles;

			try {
				roles = Roles.GetRolesForUser(username);
			}
			catch (Exception) {
				roles = null;
			}

			return roles;
		}
	}
}



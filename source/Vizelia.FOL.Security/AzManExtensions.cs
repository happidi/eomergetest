﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NetSqlAzMan.Interfaces;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Security {
	/// <summary>
	/// Encapsulates extension for SqlAzMan.
	/// </summary>
	public static class AzManExtensions {

		/// <summary>
		/// Gets the item by it's id.
		/// </summary>
		/// <param name="application"></param>
		/// <param name="itemId">Id of the item</param>
		/// <returns></returns>
        [Obsolete("This method is obsolete. Use ExtendedAzManStorage.GetItemById instead")]
		public static IAzManItem GetItemById(this IAzManApplication application, string itemId) {
			return application.Items.Values.FirstOrDefault(p => p.ItemId.ToString(CultureInfo.InvariantCulture) == itemId);
		}

	    /// <summary>
	    /// Determines whether an item is root or not.
	    /// </summary>
	    /// <param name="item">The item.</param>
	    /// <param name="items"></param>
	    /// <returns></returns>
	    public static bool IsRoot(this IAzManItem item, IEnumerable<IAzManItem> items) {
	        return !items.Any(i => i.Members.ContainsKey(item.Name));
	    }

		/// <summary>
		/// Adds a member if not already exist.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="member">The member to add.</param>
		public static void AddMemberCheckExist(this IAzManItem obj, IAzManItem member) {
			if (!obj.Members.ContainsKey(member.Name))
				obj.AddMember(member);
		}

		/// <summary>
		/// Removes a member if exist.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="member">The member to remove.</param>
		public static void RemoveMemberCheckExist(this IAzManItem obj, IAzManItem member) {
			if (obj.Members.ContainsKey(member.Name))
				obj.RemoveMember(member);
		}

		/// <summary>
		/// Returns the ItemType as a string.
		/// </summary>
		/// <param name="itemTypeValue"></param>
		/// <returns></returns>
		public static string GetItemType(int itemTypeValue) {
			switch (itemTypeValue) {
				case 100:
					return "Filter";
				default:
					return ((ItemType)itemTypeValue).ParseAsString();
			}
		}
	}
}

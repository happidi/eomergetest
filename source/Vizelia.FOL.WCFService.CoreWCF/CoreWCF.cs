﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Threading;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;
using Vizelia.FOL.WCFService.Contracts;


namespace Vizelia.FOL.WCFService {
	/// <summary>
	/// Implementation for Core Services.
	/// </summary>
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
	public class CoreWCF : BaseModuleWCF, ICoreWCF {

		private readonly ICoreBusinessLayer m_CoreBusinessLayer;



		/// <summary>
		/// Public ctor.
		/// </summary>
		public CoreWCF() {
			// Policy injection on the business layer instance.
			m_CoreBusinessLayer = Helper.CreateInstance<CoreBusinessLayer, ICoreBusinessLayer>();
		}



		/// <summary>
		/// Gets the tree of assemblies.
		/// </summary>
		/// <returns></returns>
		public List<TreeNode> Assembly_GetTree() {
			return m_CoreBusinessLayer.Assembly_GetTree();
		}

		/// <summary>
		/// Begins the importing of the azman data.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="operationId">The operation id.</param>
		public void BeginImporting(FileUploadRequest item, Guid operationId) {
			Document document = DocumentHelper.RetrieveDocument(item.InputFile);
			var documentStream = new MemoryStream(document.DocumentContents);

			m_CoreBusinessLayer.BeginImporting(operationId, document.DocumentName, documentStream, item.ImportOptions);
		}

		/// <summary>
		/// Deletes an existing business entity Building.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Building_Delete(Building item) {
			return m_CoreBusinessLayer.Building_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Building and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Building_FormCreate(Building item) {
			var retVal = m_CoreBusinessLayer.Building_FormCreate(item);
			return retVal;
		}

		/// <summary>
		/// Loads a specific item for the business entity Building.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Building_FormLoad(string Key) {
			var retVal = m_CoreBusinessLayer.Building_FormLoad(Key);
			return retVal;
		}

		/// <summary>
		/// Updates an existing business entity Building and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Building_FormUpdate(Building item) {
			return m_CoreBusinessLayer.Building_FormUpdate(item);
		}

		/// <summary>
		/// Gets a json store for the business entity Building.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Building> Building_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Building_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Building.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Building> Building_SaveStore(CrudStore<Building> store) {
			return m_CoreBusinessLayer.Building_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity BuildingStorey.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool BuildingStorey_Delete(BuildingStorey item) {
			return m_CoreBusinessLayer.BuildingStorey_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity BuildingStorey and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse BuildingStorey_FormCreate(BuildingStorey item) {
			return m_CoreBusinessLayer.BuildingStorey_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity BuildingStorey.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse BuildingStorey_FormLoad(string Key) {
			return m_CoreBusinessLayer.BuildingStorey_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity BuildingStorey and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse BuildingStorey_FormUpdate(BuildingStorey item) {
			return m_CoreBusinessLayer.BuildingStorey_FormUpdate(item);
		}

		/// <summary>
		/// Gets a json store for the business entity BuildingStorey.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<BuildingStorey> BuildingStorey_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.BuildingStorey_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity BuildingStorey.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<BuildingStorey> BuildingStorey_SaveStore(CrudStore<BuildingStorey> store) {
			return m_CoreBusinessLayer.BuildingStorey_SaveStore(store);
		}

		/// <summary>
		/// Gets the store of calendar day names.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> CalendarDayName_GetStore() {
			return m_CoreBusinessLayer.CalendarDayName_GetStore();
		}

		/// <summary>
		/// Copy existing CalendarEvent.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy.</param>
		/// <returns></returns>
		public FormResponse CalendarEvent_Copy(string[][] keys) {
			return m_CoreBusinessLayer.CalendarEvent_Copy(keys);
		}
		/// <summary>
		/// Deletes an existing business entity CalendarEvent.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="KeyLocation">The location key.</param>
		/// <returns></returns>
		public bool CalendarEvent_Delete(CalendarEvent item, string KeyLocation) {
			return m_CoreBusinessLayer.CalendarEvent_Delete(item, KeyLocation);
		}

		/// <summary>
		/// Create a business entity CalendarEvent.
		/// </summary>
		/// <param name="item">The entity CalendarEvent.</param>
		/// <param name="KeyLocation">The location Key</param>
		/// <returns></returns>
		public FormResponse CalendarEvent_FormCreate(CalendarEvent item, string KeyLocation) {
			return m_CoreBusinessLayer.CalendarEvent_FormCreate(item, KeyLocation);

		}

		/// <summary>
		/// Loads a specific item for the business entity CalendarEvent.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="KeyLocation">The location Key</param>
		/// <returns></returns>
		public FormResponse CalendarEvent_FormLoad(string Key, string KeyLocation) {
			return m_CoreBusinessLayer.CalendarEvent_FormLoad(Key, KeyLocation);
		}

		/// <summary>
		/// Create a business entity CalendarEvent.
		/// </summary>
		/// <param name="item">The entity CalendarEvent.</param>
		/// <param name="KeyLocation">The location Key</param>
		/// <returns></returns>
		public FormResponse CalendarEvent_FormUpdate(CalendarEvent item, string KeyLocation) {
			return m_CoreBusinessLayer.CalendarEvent_FormUpdate(item, KeyLocation);
		}

		/// <summary>
		/// Gets the CalendarEvent store for a location.
		/// </summary>
		/// <param name="KeyLocation">The key of the location.</param>
		/// <param name="category">The category.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>A json store of the result.</returns>
		public JsonStore<CalendarEvent> CalendarEvent_GetStoreFromLocation(string KeyLocation, string category, PagingParameter paging) {
			return m_CoreBusinessLayer.CalendarEvent_GetStoreFromLocation(KeyLocation, category, paging);
		}

		/// <summary>
		/// Gets the CalendarEvent store of occurrences for a location.
		/// </summary>
		/// <param name="KeyLocation">The key location.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="start">The start.</param>
		/// <param name="end">The end.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		public JsonStore<CalendarEvent> CalendarEvent_GetStoreOccurence(string KeyLocation, PagingParameter paging, DateTime start, DateTime end, string category) {
			return m_CoreBusinessLayer.CalendarEvent_GetStoreOccurence(KeyLocation, paging, start, end, category);
		}

		/// <summary>
		/// Saves a crud store for the business entity CalendarEvent.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <param name="KeyLocation">The loation Key.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<CalendarEvent> CalendarEvent_SaveStore(CrudStore<CalendarEvent> store, string KeyLocation) {
			return m_CoreBusinessLayer.CalendarEvent_SaveStore(store, KeyLocation);
		}

		/// <summary>
		/// Deletes an existing business entity CalendarEventCategory.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool CalendarEventCategory_Delete(CalendarEventCategory item) {
			return m_CoreBusinessLayer.CalendarEventCategory_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity CalendarEventCategory and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse CalendarEventCategory_FormCreate(CalendarEventCategory item) {
			return m_CoreBusinessLayer.CalendarEventCategory_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse CalendarEventCategory_FormLoad(string Key) {
			return m_CoreBusinessLayer.CalendarEventCategory_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity CalendarEventCategory and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse CalendarEventCategory_FormUpdate(CalendarEventCategory item) {
			return m_CoreBusinessLayer.CalendarEventCategory_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse CalendarEventCategory_FormUpdateBatch(string[] keys, CalendarEventCategory item) {
			return m_CoreBusinessLayer.CalendarEventCategory_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity CalendarEventCategory. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<CalendarEventCategory> CalendarEventCategory_GetAll(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.CalendarEventCategory_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public CalendarEventCategory CalendarEventCategory_GetItem(string Key) {
			return m_CoreBusinessLayer.CalendarEventCategory_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<CalendarEventCategory> CalendarEventCategory_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.CalendarEventCategory_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<CalendarEventCategory> CalendarEventCategory_SaveStore(CrudStore<CalendarEventCategory> store) {
			return m_CoreBusinessLayer.CalendarEventCategory_SaveStore(store);
		}

		/// <summary>
		/// Gets the store of calendar frequency occurences.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> CalendarFrequencyOccurrence_GetStore() {
			return m_CoreBusinessLayer.CalendarFrequencyOccurrence_GetStore();
		}

		/// <summary>
		/// Gets the store of calendar month names.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> CalendarMonthName_GetStore() {
			return m_CoreBusinessLayer.CalendarMonthName_GetStore();
		}

		/// <summary>
		/// Gets all the classes that inherit from the type given.
		/// </summary>
		/// <param name="typeName">Name of the type.</param>
		/// <returns></returns>
		public JsonStore<ClassDefinition> ClassDefinition_GetStoreByTypeName(string typeName) {
			return m_CoreBusinessLayer.ClassDefinition_GetStoreByTypeName(typeName);
		}

		/// <summary>
		/// Deletes an existing business entity ClassificationDefinition.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ClassificationDefinition_Delete(ClassificationDefinition item) {
			return m_CoreBusinessLayer.ClassificationDefinition_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ClassificationDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ClassificationDefinition_FormCreate(ClassificationDefinition item) {
			return m_CoreBusinessLayer.ClassificationDefinition_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ClassificationDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ClassificationDefinition_FormLoad(string Key) {
			return m_CoreBusinessLayer.ClassificationDefinition_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ClassificationDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ClassificationDefinition_FormUpdate(ClassificationDefinition item) {
			return m_CoreBusinessLayer.ClassificationDefinition_FormUpdate(item);
		}

		/// <summary>
		/// Gets a list for the business entity ClassificationDefinition. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<ClassificationDefinition> ClassificationDefinition_GetAll(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.ClassificationDefinition_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity ClassificationDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public ClassificationDefinition ClassificationDefinition_GetItem(string Key) {
			return m_CoreBusinessLayer.ClassificationDefinition_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the ClassificationDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ClassificationDefinition> ClassificationDefinition_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.ClassificationDefinition_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity ClassificationDefinition.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ClassificationDefinition> ClassificationDefinition_SaveStore(CrudStore<ClassificationDefinition> store) {
			return m_CoreBusinessLayer.ClassificationDefinition_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity ClassificationItem.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ClassificationItem_Delete(ClassificationItem item) {
			return m_CoreBusinessLayer.ClassificationItem_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ClassificationItem and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ClassificationItem_FormCreate(ClassificationItem item) {
			return m_CoreBusinessLayer.ClassificationItem_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ClassificationItem.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ClassificationItem_FormLoad(string Key) {
			return m_CoreBusinessLayer.ClassificationItem_FormLoad(Key);
		}

		/// <summary>
		/// Get the count of all entities for a tenant.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElementGeneric<long>> EntityCount_GetStore() {
			return m_CoreBusinessLayer.EntityCount_GetStore();
		}

		/// <summary>
		/// Updates an existing business entity ClassificationItem and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ClassificationItem_FormUpdate(ClassificationItem item) {
			return m_CoreBusinessLayer.ClassificationItem_FormUpdate(item);
		}

		/// <summary>
		/// Gets a json store for the business entity ClassificationItem.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ClassificationItem> ClassificationItem_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.ClassificationItem_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity ClassificationItem.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<ClassificationItem> ClassificationItem_GetStoreByKeyword(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.ClassificationItem_GetStoreByKeyword(paging, location);
		}
		/// <summary>
		/// Gets a json store of ClassificationItem based on related object Key.
		/// </summary>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<ClassificationItem> ClassificationItem_GetStoreByKeyObject(string KeyObject, PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.ClassificationItem_GetStoreByKeyObject(KeyObject, paging, location);
		}

		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The parent entity.</param>
		/// <param name="excludePset">True to exclude pset nodes.</param>
		/// <param name="excludeRelationship">True to exclude non direct children nodes.</param>
		/// <param name="flgFilter">if set to <c>true</c> Flag filter.</param>
		/// <returns>
		/// A list of treenodes in json.
		/// </returns>
		public List<TreeNode> ClassificationItem_GetTree(string Key, ClassificationItem entity, bool excludePset, bool excludeRelationship, bool flgFilter) {
			return m_CoreBusinessLayer.ClassificationItem_GetTree(Key, entity, excludePset, excludeRelationship, flgFilter);
		}

		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview from a related object.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The parent entity.</param>
		/// <param name="KeyRelatedObject">The key object.</param>
		/// <returns>
		/// A list of treenodes in json.
		/// </returns>
		public List<TreeNode> ClassificationItem_GetTreeByRelatedObject(string Key, ClassificationItem entity, string KeyRelatedObject) {
			return m_CoreBusinessLayer.ClassificationItem_GetTreeByRelatedObject(Key, entity, KeyRelatedObject);
		}

		/// <summary>
		/// Gets a json store of ClassificationItem based on ClassificationDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ClassificationItem> ClassificationItemDefinition_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.ClassificationItemDefinition_GetStore(paging, location);
		}

		/// <summary>
		/// Creates an association between a ClassificationItem and a Priority.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <param name="KeyPriority">The Key of the priority.</param>
		/// <returns></returns>
		public bool ClassificationItemPriority_Add(string KeyClassificationChildren, string KeyClassificationParent, string KeyPriority) {
			return m_CoreBusinessLayer.ClassificationItemPriority_Add(KeyClassificationChildren, KeyClassificationParent, KeyPriority);
		}

		/// <summary>
		/// Deletes an association between a ClassificationItem and a Priority.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <returns></returns>
		public bool ClassificationItemPriority_Delete(string KeyClassificationChildren, string KeyClassificationParent) {
			return m_CoreBusinessLayer.ClassificationItemPriority_Delete(KeyClassificationChildren, KeyClassificationParent);
		}

		/// <summary>
		/// Creates an Association beetween a list of Pset's Attributes and a ClassificationItem.
		/// </summary>
		/// <param name="Key">the Key of the ClassificationItem.</param>
		/// <param name="psetDefinitions">The list pset definitions.</param>
		/// <param name="psetAttributeDefinitions">The list of pset attribute definitions.</param>
		/// <remarks>Any attributes of a the pset definitions provided will be included, only the specified attributes definition of the last argument will be included.</remarks>
		/// <returns></returns>
		public bool ClassificationItemPset_Add(string Key, List<PsetDefinition> psetDefinitions, List<PsetAttributeDefinition> psetAttributeDefinitions) {
			return m_CoreBusinessLayer.ClassificationItemPset_Add(Key, psetDefinitions, psetAttributeDefinitions);
		}

		/// <summary>
		/// Cleans all the pset values for the classification item
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public bool ClassificationItemPset_CleanAllPsetValues(string key) {
			m_CoreBusinessLayer.ClassificationItemPset_CleanAllPsetValues(key);
			return true;
		}

		/// <summary>
		/// Deletes an Association beetween a list of Pset's Attributes and a ClassificationItem.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="psetDefinitions">The pset definitions.</param>
		/// <param name="psetAttributeDefinitions">The pset attribute definitions.</param>
		/// <param name="shouldClean">if set to <c>true</c> [should clean].</param>
		/// <returns></returns>
		public bool ClassificationItemPset_Delete(string Key, List<PsetDefinition> psetDefinitions, List<PsetAttributeDefinition> psetAttributeDefinitions, bool shouldClean) {
			return m_CoreBusinessLayer.ClassificationItemPset_Delete(Key, psetDefinitions, psetAttributeDefinitions, shouldClean);
		}

		/// <summary>
		/// Creates an association between a ClassificationItem and a WorkflowAssemblyDefinition.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <param name="WorkflowAssemblyDefinitionName">Name of the workflow assembly definition.</param>
		/// <param name="WorkflowAssemblyDefinitionAssemblyQualifiedName">Name of the workflow assembly definition assembly qualified.</param>
		/// <returns></returns>
		public bool ClassificationItemWorkflowAssemblyDefinition_Add(string KeyClassificationChildren, string KeyClassificationParent, string WorkflowAssemblyDefinitionName, string WorkflowAssemblyDefinitionAssemblyQualifiedName) {
			return m_CoreBusinessLayer.ClassificationItemWorkflowAssemblyDefinition_Add(KeyClassificationChildren, KeyClassificationParent, WorkflowAssemblyDefinitionName, WorkflowAssemblyDefinitionAssemblyQualifiedName);
		}

		/// <summary>
		/// Deletes an association between a ClassificationItem and a WorkflowAssemblyDefinition.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <returns></returns>
		public bool ClassificationItemWorkflowAssemblyDefinition_Delete(string KeyClassificationChildren, string KeyClassificationParent) {
			return m_CoreBusinessLayer.ClassificationItemWorkflowAssemblyDefinition_Delete(KeyClassificationChildren, KeyClassificationParent);
		}

		/// <summary>
		/// Gets all the Colors.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<ListElement> Color_GetStore(PagingParameter paging) {
			return m_CoreBusinessLayer.Color_GetStore(paging);
		}

		/// <summary>
		/// Gets the specific data models tree.
		/// </summary>
		/// <param name="modelTypeNames">The model type names.</param>
		/// <param name="classificationItemLocalIdPath">The classification item local id path.</param>
		/// <returns></returns>
		public List<TreeNode> DataModel_GetTree(List<string> modelTypeNames, string classificationItemLocalIdPath) {
			return m_CoreBusinessLayer.GetGeneralTypeTree(modelTypeNames, classificationItemLocalIdPath);
		}

		/// <summary>
		/// Gets the store of desktop background.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<ListElement> DesktopBackground_GetStore(PagingParameter paging) {
			return m_CoreBusinessLayer.DesktopBackground_GetStore(paging);
		}

		/// <summary>
		/// Audit the get store by entity key - for the template view.
		/// </summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="entityKey">The entity key.</param>
		/// <returns></returns>
		public JsonStore<DetailedAudit> DetailedAudit_GetStoreAuditByKey(string entityName, PagingParameter paging, string entityKey) {
			return m_CoreBusinessLayer.DetailedAudit_GetStoreByKey(entityName, paging, entityKey);
		}

		/// <summary>
		/// Deletes the document.
		/// </summary>
		/// <param name="item">The document to delete.</param>
		/// <returns></returns>
		public bool Document_Delete(Document item) {
			return m_CoreBusinessLayer.Document_Delete(item);
		}

		/// <summary>
		/// Gets a stream of a document that is an image.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public Stream Document_GetImageStream(string key) {
			StreamResult imageStream = m_CoreBusinessLayer.Document_GetImageStream(key);
			return imageStream.ContentStream;
		}

		/// <summary>
		/// Gets a document by key
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public Document Document_GetItem(string key) {
			var retVal = m_CoreBusinessLayer.Document_GetItem(key);
			return retVal;
		}

		/// <summary>
		/// Gets a document
		/// </summary>
		/// <param name="documentId">The document id.</param>
		/// <returns></returns>
		public Stream Document_GetStream(string documentId) {

			var document = m_CoreBusinessLayer.Document_GetItem(documentId);
			PrepareFileAttachment(document.DocumentName, document.DocumentType);

			MemoryStream stream = new MemoryStream(document.DocumentContents);

			return stream;
		}

		/// <summary>
		/// Return the metadata of the document (without its contents)
		/// </summary>
		/// <param name="documentId">The document id.</param>
		/// <returns></returns>
		public DocumentMetadata DocumentMetadata_GetItem(string documentId) {
			DocumentMetadata retVal = m_CoreBusinessLayer.DocumentMetadata_GetItem(documentId);
			return retVal;
		}

		/// <summary>
		/// Gets the updatable batch properties from an entity type.
		/// </summary>
		/// <param name="entityType">The type of the entity.</param>
		/// <returns></returns>
		public List<string> Entity_GetUpdatableBatchProperties(string entityType) {
			return m_CoreBusinessLayer.Entity_GetUpdatableBatchProperties(entityType);
		}

		/// <summary>
		/// Creates a new document
		/// </summary>
		/// <param name="file">The stream with the document contents - this parameter is not used.</param>
		/// <returns></returns>
		public FormResponse EntityDocument_FormPrepare(Stream file) {
			if (WebOperationContext.Current != null) WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
			try {
				HttpFileCollection files = HttpContext.Current.Request.Files;
				var fieldsAndDocuments = new Dictionary<string, Document>();
				for (int i = 0; i < files.Count; i++) {
					if (files[i].InputStream.Length > 0) {
						var inputFile = files[i].InputStream;
						var fileName = Path.GetFileName(files[i].FileName);
						var contentType = files[i].ContentType;

						var document = new Document(inputFile, fileName, contentType);
						if (!FileProtectionService.Validate(document)) {
							throw new Exception(Langue.msg_document_type_exception);
						}
						fieldsAndDocuments.Add(files.AllKeys[i], document);
					}
				}

				FormResponse entityDocumentFormPrepare = m_CoreBusinessLayer.EntityDocument_FormPrepare(fieldsAndDocuments);
				return entityDocumentFormPrepare;
			}

			catch (Exception e) {
				return new FormResponse { success = false, msg = e.Message };
			}
		}

		/// <summary>
		/// Gets all elements from an enum. 
		/// </summary>
		/// <param name="enumTypeName">The type name of the enum.</param>
		/// <returns></returns>
		public JsonStore<ListElementGeneric<int>> Enum_GetStore(string enumTypeName) {
			return m_CoreBusinessLayer.Enum_GetStore(enumTypeName);
		}

		/// <summary>
		/// Gets all the enums from the application.
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, JsonStore<ListElementGeneric<int>>> Enum_GetStores() {
			return m_CoreBusinessLayer.Enum_GetStores();
		}

		/// <summary>
		/// Gets all the installed Font Family.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<ListElement> FontFamily_GetStore(PagingParameter paging) {
			return m_CoreBusinessLayer.FontFamily_GetStore(paging);
		}

		/// <summary>
		/// Deletes an existing business entity Furniture.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Furniture_Delete(Furniture item) {
			return m_CoreBusinessLayer.Furniture_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Furniture and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Furniture_FormCreate(Furniture item) {
			return m_CoreBusinessLayer.Furniture_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Furniture.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Furniture_FormLoad(string Key) {
			return m_CoreBusinessLayer.Furniture_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Furniture and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Furniture_FormUpdate(Furniture item) {
			return m_CoreBusinessLayer.Furniture_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Furniture_FormUpdateBatch(string[] keys, Furniture item) {
			return m_CoreBusinessLayer.Furniture_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Furniture. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Furniture> Furniture_GetAll(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Furniture_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Furniture.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Furniture Furniture_GetItem(string Key) {
			return m_CoreBusinessLayer.Furniture_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Furniture.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Furniture> Furniture_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Furniture_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Furniture.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Furniture> Furniture_SaveStore(CrudStore<Furniture> store) {
			return m_CoreBusinessLayer.Furniture_SaveStore(store);
		}

		/// <summary>
		/// Returns the progress of operationId.
		/// This function is called by a timer on a client, usually when downloading a file, and the long processing function is responsible for updating Session[operationId] with the status of the operation.
		/// </summary>
		/// <param name="operationId">The id of the iframe generated on client side for calling the long process function</param>
		/// <returns>The progress of the operation</returns>
		public LongRunningOperationState GetLongRunningOperationProgress(Guid operationId) {
			var state = m_CoreBusinessLayer.GetLongRunningOperationProgress(operationId);
			return state;
		}

		/// <summary>
		/// Gets the long running operation result entity.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <returns>A file result for the given operation ID</returns>
		public BaseBusinessEntity GetLongRunningOperationResultEntity(Guid operationId) {
			var entity = m_CoreBusinessLayer.GetLongRunningOperationResultEntity(operationId);
			return entity;
		}

		/// <summary>
		/// Gets the long running operation result file.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <returns>A file result for the given operation ID</returns>
		public Stream GetLongRunningOperationResultFile(Guid operationId) {
			StreamResult file = m_CoreBusinessLayer.GetLongRunningOperationResultFile(operationId);
			PrepareFileAttachment(file.FullFileName, file.MimeType);

			return file.ContentStream;
		}

		/// <summary>
		/// Audit the get store by entity key - for the grid view.
		/// </summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<Audit> Audit_GetStore(string entityName, PagingParameter paging) {
			return m_CoreBusinessLayer.Audit_GetStore(entityName, paging);
		}

		/// <summary>
		/// Deletes an existing business entity Job.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Job_Delete(Job item) {
			return m_CoreBusinessLayer.Job_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Job_FormCreate(Job item) {
			return m_CoreBusinessLayer.Job_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Job.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Job_FormLoad(string Key) {
			return m_CoreBusinessLayer.Job_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Job_FormUpdate(Job item) {
			return m_CoreBusinessLayer.Job_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Job_FormUpdateBatch(string[] keys, Job item) {
			return m_CoreBusinessLayer.Job_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Job. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Job> Job_GetAll(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Job_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Job.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Job Job_GetItem(string Key) {
			return m_CoreBusinessLayer.Job_GetItem(Key);
		}



		/// <summary>
		/// Executes the selected job
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Job Job_ExecuteItem(string Key) {
			return m_CoreBusinessLayer.Job_ExecuteItem(Key);
		}


		/// <summary>
		/// Pauses the selected job
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Job Job_PauseItem(string Key) {
			return m_CoreBusinessLayer.Job_PauseItem(Key);
		}


		/// <summary>
		/// Resumes the selected job
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Job Job_ResumeItem(string Key) {
			return m_CoreBusinessLayer.Job_ResumeItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Job.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Job> Job_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Job_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Job.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Job> Job_SaveStore(CrudStore<Job> store) {
			return m_CoreBusinessLayer.Job_SaveStore(store);
		}

		/// <summary>
		/// Gets all elements from a list. 
		/// </summary>
		/// <param name="listName">The name of the list.</param>
		/// <returns></returns>
		public JsonStore<ListElement> ListElement_GetStore(string listName) {
			return m_CoreBusinessLayer.ListElement_GetStore(listName);
		}

		/// <summary>
		/// Returns a javascript file containing security attributes, that is parsed dynamically regarding the user logged.
		/// </summary>
		/// <param name="filename">The name of the file to parse</param>
		/// <returns>The content of the file parsed.</returns>
		public string LoadConfiguration(string filename) {
			// It seems that this method was never consumed. We throws exception instead of simply deleting it so we won't break other WCF clients.
			throw new NotImplementedException();
		}

		/// <summary>
		/// Gets a json store of exposed Cultures.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<LocalizationCulture> LocalizationCulture_GetStore(PagingParameter paging) {
			return m_CoreBusinessLayer.LocalizationCulture_GetStore(paging);
		}

		/// <summary>
		/// Creates a new CreateLocalizationResource.
		/// </summary>
		/// <param name="item">The localization resource item.</param>
		/// <returns>The FormResponse object.</returns>
		public FormResponse LocalizationResource_FormCreate(LocalizationResource item) {
			return m_CoreBusinessLayer.LocalizationResource_FormCreate(item);
		}

		/// <summary>
		/// Updates a CreateLocalizationResource.
		/// </summary>
		/// <param name="item">The localization resource item.</param>
		/// <returns>The FormResponse object.</returns>
		public FormResponse LocalizationResource_FormUpdate(LocalizationResource item) {
			return m_CoreBusinessLayer.LocalizationResource_FormUpdate(item);
		}

		/// <summary>
		/// Gets a json store of LocalizationResource.
		/// </summary>
		/// <param name="language">The language.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<LocalizationResource> LocalizationResource_GetStore(string language, PagingParameter paging) {
			return m_CoreBusinessLayer.LocalizationResource_GetStore(language, paging);
		}

		/// <summary>
		/// Saves a LocalizationResource store.
		/// </summary>
		/// <param name="language">The base language.</param>
		/// <param name="store">The crud store.</param>
		/// <returns></returns>
		public JsonStore<LocalizationResource> LocalizationResource_SaveStore(string language, CrudStore<LocalizationResource> store) {
			return m_CoreBusinessLayer.LocalizationResource_SaveStore(language, store);
		}

		/// <summary>
		/// Translate Text using Google Translate.
		/// </summary>
		/// <param name="input">The string to translate.</param>
		/// <param name="languageFrom">2 letters Language source. e.g. "en , da" language pair means to translate from English to Danish</param>
		/// <param name="languageTo">2 letters language destination. e.g. "en , da" language pair means to translate from English to Danish</param>
		/// <returns>Translated to String</returns>
		public string LocalizationResource_TranslateText(string input, string languageFrom, string languageTo) {
			return m_CoreBusinessLayer.LocalizationResource_TranslateText(input, languageFrom, languageTo);
		}

		/// <summary>
		/// Gets the store of logging for a specific method.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="taskType">Name of the method.</param>
		/// <returns></returns>
		public JsonStore<Logging> Logging_GetStoreByMethodName(PagingParameter paging, string taskType) {
			return m_CoreBusinessLayer.Logging_GetStoreByMethodName(paging, taskType);
		}

		/// <summary>
		/// Saves a crud store for the business entity Logging.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Logging> Logging_SaveStore(CrudStore<Logging> store) {
			return m_CoreBusinessLayer.Logging_SaveStore(store);
		}

		/// <summary>
		/// Gets all the methods on classes that inherit from the type given that have the display attribute.
		/// </summary>
		/// <param name="typeName">Name of the type.</param>
		/// <returns></returns>
		public JsonStore<MethodDefinition> MethodDefinition_GetStoreByTypeName(string typeName) {
			return m_CoreBusinessLayer.MethodDefinition_GetStoreByTypeName(typeName);
		}

		/// <summary>
		/// Saves a crud store for the business entity ObjectClassification.
		/// </summary>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="store">The crud store.</param>
		/// <returns>
		/// A json store containing the value after the save, or a message for each error.
		/// </returns>
		public JsonStore<ObjectClassification> ObjectClassification_SaveStoreByKeyObject(string KeyObject, CrudStore<ClassificationItem> store) {
			return m_CoreBusinessLayer.ObjectClassification_SaveStoreByKeyObject(KeyObject, store);
		}

		/// <summary>
		/// Deletes an existing business entity Occupant.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Occupant_Delete(Occupant item) {
			return m_CoreBusinessLayer.Occupant_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Occupant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Occupant_FormCreate(Occupant item) {
			return m_CoreBusinessLayer.Occupant_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Occupant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Occupant_FormLoad(string Key) {
			return m_CoreBusinessLayer.Occupant_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Occupant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Occupant_FormUpdate(Occupant item) {
			return m_CoreBusinessLayer.Occupant_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of items based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Occupant_FormUpdateBatch(string[] keys, Occupant item) {
			return m_CoreBusinessLayer.Occupant_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Occupant. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Occupant> Occupant_GetAll(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Occupant_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Occupant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Occupant Occupant_GetItem(string Key) {
			return m_CoreBusinessLayer.Occupant_GetItem(Key);
		}

		/// <summary>
		/// Gets the occupant pset.
		/// </summary>
		/// <param name="PsetName">Name of the pset.</param>
		/// <param name="KeyOccupant">The key occupant.</param>
		/// <returns></returns>
		public Dictionary<string, string> Occupant_GetPset(string PsetName, string KeyOccupant) {
			return m_CoreBusinessLayer.Occupant_GetPset(PsetName, KeyOccupant);
		}

		/// <summary>
		/// Gets the Occupant store.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns>A json store of the result.</returns>
		public JsonStore<Occupant> Occupant_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Occupant_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Occupant.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Occupant> Occupant_SaveStore(CrudStore<Occupant> store) {
			return m_CoreBusinessLayer.Occupant_SaveStore(store);
		}



		/// <summary>
		/// Deletes an existing business entity Organization.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Organization_Delete(Organization item) {
			return m_CoreBusinessLayer.Organization_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Organization and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Organization_FormCreate(Organization item) {
			return m_CoreBusinessLayer.Organization_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Organization.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Organization_FormLoad(string Key) {
			return m_CoreBusinessLayer.Organization_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Organization and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Organization_FormUpdate(Organization item) {
			return m_CoreBusinessLayer.Organization_FormUpdate(item);
		}

		/// <summary>
		/// Gets a list for the business entity Organization. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Organization> Organization_GetAll(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Organization_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Organization.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Organization Organization_GetItem(string Key) {
			return m_CoreBusinessLayer.Organization_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Organization.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Organization> Organization_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Organization_GetStore(paging, location);
		}

		/// <summary>
		/// Generates the json structure of the Organization treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		public List<TreeNode> Organization_GetTree(string Key) {
			return m_CoreBusinessLayer.Organization_GetTree(Key);
		}

		/// <summary>
		/// Saves a crud store for the business entity Organization.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Organization> Organization_SaveStore(CrudStore<Organization> store) {
			return m_CoreBusinessLayer.Organization_SaveStore(store);
		}

		/// <summary>
		/// Gets the priority assigned to a ClassificationItem.
		/// </summary>
		/// <param name="KeyClassificationChildren">The key classification children.</param>
		/// <param name="KeyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public Priority Priority_GetItemByClassification(string KeyClassificationChildren, string KeyClassificationParent) {
			return m_CoreBusinessLayer.Priority_GetItemByClassification(KeyClassificationChildren, KeyClassificationParent);
		}

		/// <summary>
		/// Gets the priority assigned to a any upper level of ClassificationItem Hierarchy.
		/// </summary>
		/// <param name="KeyClassificationChildren">The key classification children.</param>
		/// <param name="KeyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		public Priority Priority_GetItemByClassificationAscendant(string KeyClassificationChildren, string KeyClassificationParent) {
			return m_CoreBusinessLayer.Priority_GetItemByClassificationAscendant(KeyClassificationChildren, KeyClassificationParent);
		}

		/// <summary>
		/// Gets the pset for an object.
		/// </summary>
		/// <param name="KeyObject">The key of the object.</param>
		/// <returns></returns>
		public List<Pset> Pset_GetList(string KeyObject) {
			return m_CoreBusinessLayer.Pset_GetList(new[] { KeyObject });
		}

		/// <summary>
		/// Gets the pset for an object.
		/// </summary>
		/// <param name="KeyObjectArray">The key object array.</param>
		/// <returns></returns>
		public List<Pset> Pset_GetListByKeysArray(string[] KeyObjectArray) {
			return m_CoreBusinessLayer.Pset_GetList(KeyObjectArray);
		}

		/// <summary>
		/// Generates the json structure of the Pset treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="usageName">The usageName (IfcBuilding, IfcActionRequest etc...) for filtering the results. If null no filter should occur.</param>
		/// <returns>a list of treenodes in json.</returns>
		public List<TreeNode> Pset_GetTree(string Key, string usageName) {
			return m_CoreBusinessLayer.Pset_GetTree(Key, usageName);
		}

		#region PsetAttributeDefinition
		/// <summary>
		/// Deletes a PsetAttributeDefinition
		/// </summary>
		/// <param name="item">The pset attribute  definition item to delete</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool PsetAttributeDefinition_Delete(PsetAttributeDefinition item) {
			return m_CoreBusinessLayer.PsetAttributeDefinition_Delete(item);
		}

		/// <summary>
		/// Creates a new PsetAttributeDefinition.
		/// </summary>
		/// <param name="item">The pset attribute  definition item.</param>
		/// <returns>The FormResponse object.</returns>
		public FormResponse PsetAttributeDefinition_FormCreate(PsetAttributeDefinition item) {
			return m_CoreBusinessLayer.PsetAttributeDefinition_FormCreate(item);
		}

		/// <summary>
		/// Updates a PsetAttributeDefinition.
		/// </summary>
		/// <param name="item">The pset attribute definition item.</param>
		/// <returns>The FormResponse object.</returns>
		public FormResponse PsetAttributeDefinition_FormUpdate(PsetAttributeDefinition item) {
			return m_CoreBusinessLayer.PsetAttributeDefinition_FormUpdate(item);
		}

		/// <summary>
		/// Get All PsetAttributes(s) definitions.
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="location"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public List<PsetAttributeDefinition> PsetAttributeDefinition_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			return m_CoreBusinessLayer.PsetAttributeDefinition_GetAll(paging, location, fields);
		}

		/// <summary>
		/// Get PseAttributeDefinition Item by key.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public PsetAttributeDefinition PsetAttributeDefinition_GetItem(string key, params string[] fields) {
			return m_CoreBusinessLayer.PsetAttributeDefinition_GetItem(key, fields);
		}
		#endregion

		/// <summary>
		/// Deletes an existing business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool PsetAttributeHistorical_Delete(PsetAttributeHistorical item) {
			return m_CoreBusinessLayer.PsetAttributeHistorical_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity PsetAttributeHistorical and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistorical_FormCreate(PsetAttributeHistorical item) {
			return m_CoreBusinessLayer.PsetAttributeHistorical_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistorical_FormLoad(string Key) {
			return m_CoreBusinessLayer.PsetAttributeHistorical_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PsetAttributeHistorical and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistorical_FormUpdate(PsetAttributeHistorical item) {
			return m_CoreBusinessLayer.PsetAttributeHistorical_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistorical_FormUpdateBatch(string[] keys, PsetAttributeHistorical item) {
			return m_CoreBusinessLayer.PsetAttributeHistorical_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PsetAttributeHistorical. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<PsetAttributeHistorical> PsetAttributeHistorical_GetAll(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.PsetAttributeHistorical_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public PsetAttributeHistorical PsetAttributeHistorical_GetItem(string Key) {
			return m_CoreBusinessLayer.PsetAttributeHistorical_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PsetAttributeHistorical> PsetAttributeHistorical_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.PsetAttributeHistorical_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of PsetAttributeHistorical for a specific Object.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="KeyPropertySingleValue">The key property single value.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		public JsonStore<PsetAttributeHistorical> PsetAttributeHistorical_GetStoreByKeyObject(PagingParameter paging, string KeyObject, string KeyPropertySingleValue) {
			return m_CoreBusinessLayer.PsetAttributeHistorical_GetStoreByKeyObject(paging, KeyObject, KeyPropertySingleValue);
		}

		/// <summary>
		/// Saves a crud store for the business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PsetAttributeHistorical> PsetAttributeHistorical_SaveStore(CrudStore<PsetAttributeHistorical> store) {
			return m_CoreBusinessLayer.PsetAttributeHistorical_SaveStore(store);
		}

		/// <summary>
		/// Deletes a PsetDefinition
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool PsetDefinition_Delete(PsetDefinition item) {
			return m_CoreBusinessLayer.PsetDefinition_Delete(item);
		}

		/// <summary>
		/// Creates a new PsetDefinition.
		/// </summary>
		/// <param name="item">The pset definition item.</param>
		/// <returns>The FormResponse object.</returns>
		public FormResponse PsetDefinition_FormCreate(PsetDefinition item) {
			return m_CoreBusinessLayer.PsetDefinition_FormCreate(item);
		}

		/// <summary>
		/// Updates a PsetDefinition.
		/// </summary>
		/// <param name="item">The pset definition item.</param>
		/// <returns>The FormResponse object.</returns>
		public FormResponse PsetDefinition_FormUpdate(PsetDefinition item) {
			return m_CoreBusinessLayer.PsetDefinition_FormUpdate(item);
		}

		/// <summary>
		/// Gets the pset definition list corresponding to a specific usage name.
		/// </summary>
		/// <param name="UsageName">Name of the usage.</param>
		/// <param name="KeyClassificationItem">The KeyClassificationItem (optional).</param>
		/// <returns></returns>
		public List<PsetDefinition> PsetDefinition_GetListByUsageName(string UsageName, string KeyClassificationItem) {
			return m_CoreBusinessLayer.PsetDefinition_GetListByUsageName(UsageName, KeyClassificationItem);
		}

		/// <summary>
		/// Gets the pset definition store corresponding to a specific usage name.
		/// </summary>
		/// <param name="UsageName">Name of the usage.</param>
		/// <param name="KeyClassificationItem">The KeyClassificationItem (optional).</param>
		/// <returns></returns>
		public JsonStore<PsetDefinition> PsetDefinition_GetStoreByUsageName(string UsageName, string KeyClassificationItem) {
			return m_CoreBusinessLayer.PsetDefinition_GetStoreByUsageName(UsageName, KeyClassificationItem);
		}

		/// <summary>
		/// Get all the PsetDefinition(s).
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="location"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public List<PsetDefinition> PsetDefinition_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			return m_CoreBusinessLayer.PsetDefinition_GetAll(paging, location, fields);
		}

		/// <summary>
		/// Get PsetDefinitionItem by key.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public PsetDefinition PsetDefinition_GetItem(string key, params string[] fields) {
			return m_CoreBusinessLayer.PsetDefinition_GetItem(key, fields);
		}

		/// <summary>
		/// Gets the pset list specified.
		/// </summary>
		/// <param name="listName">The name of the psetlist to retreive.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<ListElement> PsetListElement_GetStore(string listName, PagingParameter paging) {
			return m_CoreBusinessLayer.PsetListElement_GetStore(listName, paging);
		}

		/// <summary>
		/// Saves a Pset List.
		/// </summary>
		/// <param name="listName">The name of the list to save.</param>
		/// <param name="store">The crud store of the list.</param>
		/// <returns></returns>
		public JsonStore<ListElement> PsetListElement_SaveStore(string listName, CrudStore<ListElement> store) {
			return m_CoreBusinessLayer.PsetListElement_SaveStore(listName, store);
		}

		/// <summary>
		/// Deletes an existing business entity Site.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Site_Delete(Site item) {
			return m_CoreBusinessLayer.Site_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Site and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Site_FormCreate(Site item) {
			return m_CoreBusinessLayer.Site_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Site.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Site_FormLoad(string Key) {
			return m_CoreBusinessLayer.Site_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Site and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Site_FormUpdate(Site item) {
			return m_CoreBusinessLayer.Site_FormUpdate(item);
		}

		/// <summary>
		/// Gets a json store for the business entity Site.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Site> Site_GetStore(PagingParameter paging, PagingLocation location) {
			JsonStore<Site> result = m_CoreBusinessLayer.Site_GetStore(paging, location);
			return result;
		}

		/// <summary>
		/// Saves a crud store for the business entity Site.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Site> Site_SaveStore(CrudStore<Site> store) {
			return m_CoreBusinessLayer.Site_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity Space.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Space_Delete(Space item) {
			return m_CoreBusinessLayer.Space_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Space and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Space_FormCreate(Space item) {
			return m_CoreBusinessLayer.Space_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Space.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Space_FormLoad(string Key) {
			return m_CoreBusinessLayer.Space_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Space and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Space_FormUpdate(Space item) {
			return m_CoreBusinessLayer.Space_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Space_FormUpdateBatch(string[] keys, Space item) {
			return m_CoreBusinessLayer.Space_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Space. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Space> Space_GetAll(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Space_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Space.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Space Space_GetItem(string Key) {
			return m_CoreBusinessLayer.Space_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Space.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Space> Space_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Space_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Space.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Space> Space_SaveStore(CrudStore<Space> store) {
			return m_CoreBusinessLayer.Space_SaveStore(store);
		}

        /// <summary>
        /// Generates the json structure of the Spatial treeview.
        /// </summary>
        /// <param name="Key">The Key of the parent node.</param>
        /// <param name="flgFilter">True to filter the result, false otherwise.</param>
        /// <param name="ExcludeMetersAndAlarms">if set to <c>true</c> [filter meters and alarms out of the response tree].</param>
        /// <returns>
        /// a list of treenodes in json.
        /// </returns>
        public List<TreeNode> Spatial_GetTree(string Key, bool flgFilter, bool ExcludeMetersAndAlarms = false) {
            return m_CoreBusinessLayer.Spatial_GetTree(Key, flgFilter, ExcludeMetersAndAlarms);
		}

		/// <summary>
		/// Gets all the locations for a specific key word.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="keyword">The keyword.</param>		
		public JsonStore<Location> Spatial_GetStoreByKeyword(PagingParameter paging, string keyword) {
			return m_CoreBusinessLayer.Spatial_GetStoreByKeyword(paging, keyword);
		}

		/// <summary>
		/// Creates a new business entity TaskScheduler and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse TaskScheduler_FormCreate(TaskScheduler item) {
			return m_CoreBusinessLayer.TaskScheduler_FormCreate(item);
		}

		/// <summary>
		/// Gets the store of time zones.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<ListElement> TimeZone_GetStore(PagingParameter paging) {
			return m_CoreBusinessLayer.TimeZone_GetStore(paging);
		}

		/// <summary>
		/// Deletes an existing business entity TraceEntry.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool TraceEntry_Delete(TraceEntry item) {
			return m_CoreBusinessLayer.TraceEntry_Delete(item);
		}

		/// <summary>
		/// Gets a json store for the business entity TraceEntry.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<TraceEntry> TraceEntry_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.TraceEntry_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity TraceSummary.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<TraceSummary> TraceSummary_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.TraceSummary_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of TraceEntries for a specific request id.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="requestId">The request id.</param>
		/// <returns></returns>
		public JsonStore<TraceEntry> TraceEntry_GetStoreByRequestId(PagingParameter paging, string requestId) {
			return m_CoreBusinessLayer.TraceEntry_GetStoreByRequestId(paging, requestId);
		}

		/// <summary>
		/// Get a store of TraceEntries for a specific job run instance request id.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="jobRunInstanceRequestId">The job run instance request id.</param>
		/// <returns></returns>
		public JsonStore<TraceEntry> TraceEntry_GetStoreByJobRunInstanceRequestId(PagingParameter paging, string jobRunInstanceRequestId) {
			return m_CoreBusinessLayer.TraceEntry_GetStoreByJobRunInstanceRequestId(paging, jobRunInstanceRequestId);
		}

		/// <summary>
		/// Jobs the instance_ get store by job id.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="keyJob">The key job.</param>
		/// <returns></returns>
		public JsonStore<JobInstance> JobInstance_GetStoreByJobId(PagingParameter paging, string keyJob) {
			return m_CoreBusinessLayer.JobInstance_GetStoreByJobId(paging, keyJob);
		}

		/// <summary>
		/// Gets the TraceEntry for an exception that occured during the request specified by the request id.
		/// </summary>
		/// <param name="requestId">The request id.</param>
		/// <returns></returns>
		public TraceEntry TraceEntry_GetErrorEntryByRequestId(string requestId) {
			return m_CoreBusinessLayer.TraceEntry_GetErrorEntryByRequestId(requestId);
		}

		/// <summary>
		/// Saves a crud store for the business entity TraceEntry.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<TraceEntry> TraceEntry_SaveStore(CrudStore<TraceEntry> store) {
			return m_CoreBusinessLayer.TraceEntry_SaveStore(store);
		}

		/// <summary>
		/// Updates the long running operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="clientRequest">The client request.</param>
		public void UpdateLongRunningOperation(Guid operationId, LongRunningOperationClientRequest clientRequest) {
			m_CoreBusinessLayer.UpdateLongRunningOperation(operationId, clientRequest);
		}

		/// <summary>
		/// Get list of UserProfilePreference for the current user.
		/// </summary>
		/// <returns></returns>
		public List<UserProfilePreference> UserProfilePreference_GetListByUser() {
			return m_CoreBusinessLayer.UserProfilePreference_GetListByUser();
		}

		/// <summary>
		/// Get list of UserProfilePreference for the a specific user (Used in the PrintScreen for example).
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <returns></returns>
		public List<UserProfilePreference> UserProfilePreference_GetListImpersonateUser(string userName) {
			return m_CoreBusinessLayer.UserProfilePreference_GetListByUser(userName);
		}

		/// <summary>
		/// Sets the preference key and value for the user
		/// </summary>
		/// <param name="preferenceKey">The preference key.</param>
		/// <param name="preferenceValue">The preference value.</param>
		public void UserProfilePreference_Update(string preferenceKey, string preferenceValue) {
			m_CoreBusinessLayer.UserProfilePreference_Update(preferenceKey, preferenceValue);
		}

		/// <summary>
		/// Sets the preference key and value for the tenant
		/// </summary>
		/// <param name="preferenceKey">The preference key.</param>
		/// <param name="preferenceValue">The preference value.</param>
		public void UserProfilePreference_UpdateGlobal(string preferenceKey, string preferenceValue) {
			m_CoreBusinessLayer.UserProfilePreference_UpdateGlobal(preferenceKey, preferenceValue);
		}

		/// <summary>
		/// Deletes all UserProfilePreference data for the current user.
		/// </summary>
		public bool UserProfilePreference_DeleteAllData() {
			return m_CoreBusinessLayer.UserProfilePreference_DeleteAllData();
		}

		/// <summary>
		/// Creates the user settings entity
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public FormResponse UserPreferences_FormCreate(UserPreferences item) {
			return m_CoreBusinessLayer.UserPreferences_FormCreate(item);
		}

		/// <summary>
		/// Updates the user settings entity
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public FormResponse UserPreferences_FormUpdate(UserPreferences item) {
			return m_CoreBusinessLayer.UserPreferences_FormUpdate(item);
		}

		/// <summary>
		/// Gets a specific item for the business entity UserPreferences.
		/// </summary>
		/// <param name="key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse UserPreferences_FormLoad(string key) {
			return m_CoreBusinessLayer.UserPreferences_FormLoad(key);
		}

		/// <summary>
		/// Gets the differents versions of the frameworks used encapsulated in a business entity.
		/// </summary>
		/// <returns></returns>
		public Versions Versions_GetItem() {
			Versions versions = m_CoreBusinessLayer.Versions_GetItem();
			versions.CLR += " " + Thread.CurrentThread.ManagedThreadId.ToString(CultureInfo.InvariantCulture);
			return versions;
		}

		/// <summary>
		/// Deletes an existing business entity AuditEntity.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool AuditEntity_Delete(AuditEntity item) {
			return m_CoreBusinessLayer.AuditEntity_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity AuditEntity and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse AuditEntity_FormCreate(AuditEntity item) {
			return m_CoreBusinessLayer.AuditEntity_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity AuditEntity.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse AuditEntity_FormLoad(string Key) {
			return m_CoreBusinessLayer.AuditEntity_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity AuditEntity and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse AuditEntity_FormUpdate(AuditEntity item) {
			return m_CoreBusinessLayer.AuditEntity_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse AuditEntity_FormUpdateBatch(string[] keys, AuditEntity item) {
			return m_CoreBusinessLayer.AuditEntity_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity AuditEntity. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<AuditEntity> AuditEntity_GetAll(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.AuditEntity_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity AuditEntity.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public AuditEntity AuditEntity_GetItem(string Key) {
			return m_CoreBusinessLayer.AuditEntity_GetItem(Key);
		}

		/// <summary>
		/// Get store of AuditEntity.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		public JsonStore<AuditEntity> AuditEntity_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.AuditEntity_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity AuditEntity.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AuditEntity> AuditEntity_SaveStore(CrudStore<AuditEntity> store) {
			return m_CoreBusinessLayer.AuditEntity_SaveStore(store);
		}

		/// <summary>
		/// Get all AuditEntity aggregated.
		/// </summary>
		/// <returns></returns>
		public JsonStore<AuditEntity> AuditEntity_GetAllAggregated() {
			return m_CoreBusinessLayer.AuditEntity_GetAllAggregated();
		}

		/// <summary>
		/// Get all Auditable entities.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> AuditableEntity_GetStore() {
			return m_CoreBusinessLayer.AuditableEntity_GetStore();
		}

		/// <summary>
		/// Deletes an existing business entity Logo.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Logo_Delete(Logo item) {
			return m_CoreBusinessLayer.Logo_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Logo and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Logo_FormCreate(Logo item) {
			return m_CoreBusinessLayer.Logo_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Logo.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Logo_FormLoad(string Key) {
			return m_CoreBusinessLayer.Logo_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Logo and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Logo_FormUpdate(Logo item) {
			return m_CoreBusinessLayer.Logo_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Logo_FormUpdateBatch(string[] keys, Logo item) {
			return m_CoreBusinessLayer.Logo_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Logo. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Logo> Logo_GetAll(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Logo_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Logo.
		/// </summary>
		/// <returns></returns>
		public Logo Logo_GetItem() {
			// key value doesn't matter. logo is unique, so at this moment db sp will 
			// return the only row it has. maybe in the future key will be used.
			return m_CoreBusinessLayer.Logo_GetItem("1");
		}

		/// <summary>
		/// Gets a json store for the business entity Logo.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Logo> Logo_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Logo_GetStore(paging, location);
		}

		/// <summary>
		/// Returns the stream of the image  from a Logo.
		/// </summary>
		/// <returns></returns>
		public Stream Logo_GetStreamImage() {
			// key value doesn't matter. logo is unique, so at this moment db sp will 
			// return the only row it has. maybe in the future key will be used.
			var retVal = m_CoreBusinessLayer.Logo_GetStreamImage("1");
			WebOperationContext.Current.OutgoingResponse.ContentType = retVal.MimeType;
			return retVal.ContentStream;
		}

		/// <summary>
		/// Saves a crud store for the business entity Logo.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Logo> Logo_SaveStore(CrudStore<Logo> store) {
			return m_CoreBusinessLayer.Logo_SaveStore(store);
		}



		/// <summary>
		/// Gets a json store for the business entity Link.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Link> Link_GetStore(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Link_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity Link that are roots.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Link> Link_GetStoreRootOnly(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Link_GetStoreRootOnly(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Link.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Link> Link_SaveStore(CrudStore<Link> store) {
			return m_CoreBusinessLayer.Link_SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity Link. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Link> Link_GetAll(PagingParameter paging, PagingLocation location) {
			return m_CoreBusinessLayer.Link_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Link.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Link Link_GetItem(string Key) {
			return m_CoreBusinessLayer.Link_GetItem(Key);
		}

		/// <summary>
		/// Loads a specific item for the business entity Link.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Link_FormLoad(string Key) {
			return m_CoreBusinessLayer.Link_FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity Link and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Link_FormCreate(Link item) {
			return m_CoreBusinessLayer.Link_FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity Link and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Link_FormUpdate(Link item) {
			return m_CoreBusinessLayer.Link_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Link_FormUpdateBatch(string[] keys, Link item) {
			return m_CoreBusinessLayer.Link_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity Link.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Link_Delete(Link item) {
			return m_CoreBusinessLayer.Link_Delete(item);
		}


		/// <summary>
		/// Gets a specific item for the business entity Location.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Location Location_GetItem(string Key) {
			return m_CoreBusinessLayer.Location_GetItem(Key);
		}

		/// <summary>
		/// Deletes an existing business entity CustomData.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool CustomData_Delete(CustomData item) {
			return m_CoreBusinessLayer.CustomData_Delete(item);
		}

		/// <summary>
		/// Deletes all custom data by container name and key user.
		/// </summary>
		/// <param name="containerName">Name of the container.</param>
		public void CustomData_DeleteAll(string containerName) {
			m_CoreBusinessLayer.CustomData_DeleteAll(containerName);
		}

		/// <summary>
		/// Creates a new business entity CustomData and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse CustomData_FormCreate(CustomData item) {
			return m_CoreBusinessLayer.CustomData_FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity CustomData and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse CustomData_FormUpdate(CustomData item) {
			return m_CoreBusinessLayer.CustomData_FormUpdate(item);
		}

		/// <summary>
		/// Gets a list for the business entity CustomData. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<CustomData> CustomData_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			return m_CoreBusinessLayer.CustomData_GetAll(paging, location, fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity CustomData.
		/// </summary>
		/// <param name="key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public CustomData CustomData_GetItem(string key, params string[] fields) {
			return m_CoreBusinessLayer.CustomData_GetItem(key, fields);
		}

		#region PsetValue

		/// <summary>
		/// Get All PsetValue(s).
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="location"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public List<PsetValue> PsetValue_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			return m_CoreBusinessLayer.PsetValue_GetAll(paging, location, fields);
		}

		/// <summary>
		/// Get PsetValue by key.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public PsetValue PsetValue_GetItem(string key, params string[] fields) {
			return m_CoreBusinessLayer.PsetValue_GetItem(key, fields);
		}

		/// <summary>
		/// Updates PsetValue item.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public FormResponse PsetValue_FormUpdate(PsetValue item) {
			return m_CoreBusinessLayer.PsetValue_FormUpdate(item);
		}

		/// <summary>
		/// Creates PsetValue item.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public FormResponse PsetValue_FormCreate(PsetValue item) {
			return m_CoreBusinessLayer.PsetValue_FormCreate(item);
		}

		/// <summary>
		/// Deletes the PsetValue item.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public bool PsetValue_Delete(PsetValue item) {
			return m_CoreBusinessLayer.PsetValue_Delete(item);
		}
		#endregion
	}
}

﻿using System.Data;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.DataLayer {

	/// <summary>
	/// Exposes DataAccess for ServiceDesk
	/// </summary>
	public class ServiceDeskDataAccess : IServiceDeskDataAccess {
		private IDataAccess m_DataAccess;

		/// <summary>
		/// Ctor.
		/// </summary>
		public ServiceDeskDataAccess() {
			m_DataAccess = new DataAccess();
		}

		/// <summary>
		/// Retreives all the requests created by an occupant
		/// </summary>
		/// <param name="KeyOccupant">The Key of the occupant</param>
		/// <returns></returns>
		public DataSet GetRequestorActionRequest(string KeyOccupant) {
			return m_DataAccess.ExecuteDataSet(CommandType.Text, "SELECT * FROM obj.ActionRequest (NOEXPAND) WHERE Requestor = '" + KeyOccupant + "'");
		}


		

	}
}

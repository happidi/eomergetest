﻿using System;
using System.Collections.Concurrent;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Vizelia.FOL.Utilities.WCFClient.CookieManager {

	/// <summary>
	/// Adds the singleton instance of <see cref="CookieManagerMessageInspector"/> to an endpoint on the client.
	/// </summary>
	public class CookieManagerEndpointBehavior : IEndpointBehavior {
		// The singleton members.
		private static readonly object m_LockObject = new object();
		private static volatile CookieManagerEndpointBehavior m_Instance;
		
		// A thread-safe dictionary of inspectors by domain, meaning cookies are saved per-domain, like in the browser.
		private readonly ConcurrentDictionary<string, CookieManagerMessageInspector> m_Inspectors;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="CookieManagerEndpointBehavior"/> class.
		/// </summary>
		private CookieManagerEndpointBehavior() {
			m_Inspectors = new ConcurrentDictionary<string, CookieManagerMessageInspector>();
		}

		/// <summary>
		/// Create a new non-singleton instance.
		/// </summary>
		public static CookieManagerEndpointBehavior CreateNew() {
			return new CookieManagerEndpointBehavior();
		}

		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static CookieManagerEndpointBehavior Shared {
			get {
				if (m_Instance == null) { // check
					lock (m_LockObject) {
						if (m_Instance == null) { // double check, volatile ensures that the value is re-read
							m_Instance = new CookieManagerEndpointBehavior();
						}
					}
				}

				return m_Instance;
			}
		}


		/// <summary>
		/// Implement to pass data at runtime to bindings to support custom behavior.
		/// </summary>
		/// <param name="endpoint">The endpoint to modify.</param>
		/// <param name="bindingParameters">The objects that binding elements require to support the behavior.</param>
		public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) {
			// Do nothing.
		}

		/// <summary>
		/// Adds the singleton of the message inspector class to the client endpoint's message inspectors.
		/// </summary>
		/// <param name="endpoint">The endpoint that is to be customized.</param>
		/// <param name="clientRuntime">The client runtime to be customized.</param>
		public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime) {
			var inspector = m_Inspectors.GetOrAdd(endpoint.Address.Uri.Host.ToLower(), k => new CookieManagerMessageInspector());
			clientRuntime.MessageInspectors.Add(inspector);
		}

		/// <summary>
		/// Implements a modification or extension of the service across an endpoint.
		/// </summary>
		/// <param name="endpoint">The endpoint that exposes the contract.</param>
		/// <param name="endpointDispatcher">The endpoint dispatcher to be modified or extended.</param>
		public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) {
			// Do nothing.
		}

		/// <summary>
		/// Implement to confirm that the endpoint meets some intended criteria.
		/// </summary>
		/// <param name="endpoint">The endpoint to validate.</param>
		public void Validate(ServiceEndpoint endpoint) {
			// Do nothing.
		}

	    /// <summary>
	    /// Injects a given cookie into the simulated cookie container represented by this behavior.
	    /// </summary>
	    /// <param name="host">The site that should receive the cookie</param>
	    /// <param name="cookieName">The cookie name</param>
	    /// <param name="cookieValue">The cookie value</param>
	    public void InjectCookie(string host, string cookieName, string cookieValue)
	    {
            var inspector = m_Inspectors.GetOrAdd(host.ToLower(), k => new CookieManagerMessageInspector());
	        inspector.InjectCookie(cookieName, cookieValue);
	    }

        /// <summary>
        /// Sets a page id maintained by this cookie manager.
        /// </summary>
        /// <param name="host">The EO site that should have this page id</param>
        /// <param name="pageId">The page id value</param>
	    public void SetPageId(string host, string pageId)
	    {
	        var inspector = m_Inspectors.GetOrAdd(host.ToLower(), k => new CookieManagerMessageInspector());
            inspector.SetPageId(pageId);
	    }
	}
}




﻿using System;
using System.ServiceModel.Configuration;

namespace Vizelia.FOL.Utilities.WCFClient.CookieManager {
	/// <summary>
	/// A behavior extension for automatically sending / receiving cookies from WCF different proxies client.
	/// Usage : the following section should be added to web.config or app.config under system.serviceModel
	/// <code>
	/// 
	///  <behaviors>
    ///  <endpointBehaviors>
    ///    <behavior name="EnableCookieManager">
    ///      <cookieManager />
    ///    </behavior>
    ///  </endpointBehaviors>
    ///  </behaviors>
    ///  <extensions>
    ///  <behaviorExtensions>
    ///    <add name="cookieManager" type="ConsoleApplication7.CookieManagerBehaviorExtension, ConsoleApplication7" />
    ///  </behaviorExtensions>
    ///  </extensions>
	///  
	/// </code>
	/// Then an attribute behaviorConfiguration="EnableCookieManager" should be added to each endpoint.
	/// </summary>
	public class CookieManagerBehaviorExtension : BehaviorExtensionElement {
		/// <summary>
		/// Gets the type of behavior.
		/// </summary>
		public override Type BehaviorType {
			get { return typeof(CookieManagerEndpointBehavior); }
		}

		/// <summary>
		/// Creates a behavior extension based on the current configuration settings.
		/// </summary>
		protected override object CreateBehavior() {
			return CookieManagerEndpointBehavior.Shared;
		}
	}
}

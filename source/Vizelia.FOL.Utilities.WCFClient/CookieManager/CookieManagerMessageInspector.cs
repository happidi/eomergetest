﻿using System;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace Vizelia.FOL.Utilities.WCFClient.CookieManager {
	/// <summary>
	/// Maintains a copy of the cookies contained in the incoming HTTP response received from any service
	/// and appends it to all outgoing HTTP requests.
	/// </summary>
	/// <remarks>
	/// This class effectively allows to send any received HTTP cookies to different services,
	/// reproducing the same functionality available in ASMX Web Services proxies with the <see cref="System.Net.CookieContainer"/> class.
	/// </remarks>
	public class CookieManagerMessageInspector : IClientMessageInspector {
		private const string const_page_id = "PAGEID";
		private string m_SharedCookie;
		private string m_PageID;

		/// <summary>
		/// Initializes a new instance of the <see cref="CookieManagerMessageInspector"/> class.
		/// </summary>
		internal CookieManagerMessageInspector() {
		}

		/// <summary>
		/// Formats the cookie.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <returns></returns>
		private static string FormatCookie(string input) {
			string[] cookies = input.Split(new char[] { ',', ';' });
			StringBuilder buffer = new StringBuilder(input.Length * 10);
			foreach (string entry in cookies) {
				if (entry.IndexOf("=") > 0 && !entry.Trim().StartsWith("path") && !entry.Trim().StartsWith("expires")) {
					buffer.Append(entry).Append("; ");
				}
			}
			if (buffer.Length > 0) {
				buffer.Remove(buffer.Length - 2, 2);
			}
			return buffer.ToString();
		}

		/// <summary>
		/// Inspects a message after a reply message is received but prior to passing it back to the client application.
		/// </summary>
		/// <param name="reply">The message to be transformed into types and handed back to the client application.</param>
		/// <param name="correlationState">Correlation state data.</param>
		public void AfterReceiveReply(ref Message reply, object correlationState) {
			HttpResponseMessageProperty httpResponse = reply.Properties[HttpResponseMessageProperty.Name] as HttpResponseMessageProperty;
			if (httpResponse != null) {
				string cookie = httpResponse.Headers[HttpResponseHeader.SetCookie];
				if (!string.IsNullOrEmpty(cookie)) {
					m_SharedCookie = FormatCookie(cookie);
				}
				string pageId = httpResponse.Headers[const_page_id];
				if (!string.IsNullOrEmpty(pageId)) {
					m_PageID = pageId;
				}
			}
		}

		/// <summary>
		/// Inspects a message before a request message is sent to a service.
		/// </summary>
		/// <param name="request">The message to be sent to the service.</param>
		/// <param name="channel">The client object channel.</param>
		/// <returns>
		/// <strong>Null</strong> since no message correlation is used.
		/// </returns>
		public object BeforeSendRequest(ref Message request, IClientChannel channel) {
			if (m_SharedCookie == null) return null;

			HttpRequestMessageProperty httpRequest;

			// The HTTP request object is made available in the outgoing message only when
			// the Visual Studio Debugger is attacched to the running process
			if (!request.Properties.ContainsKey(HttpRequestMessageProperty.Name)) {
				request.Properties.Add(HttpRequestMessageProperty.Name, new HttpRequestMessageProperty());
			}

			httpRequest = (HttpRequestMessageProperty)request.Properties[HttpRequestMessageProperty.Name];
			httpRequest.Headers.Add(HttpRequestHeader.Cookie, m_SharedCookie);
			if (!String.IsNullOrWhiteSpace(m_PageID)) {
				httpRequest.Headers.Add(const_page_id,m_PageID);
			}
			return null;
		}

        internal void InjectCookie(string cookieName, string cookieValue)
        {
            // split the m_SharedCookie string into a dictionary of cookie names/values
            var cookies = 
                // m_SharedCookie might not be initialized
                (m_SharedCookie ?? string.Empty)
                    // split into sequence of "name=value", "name=value"
                    .Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries)
                    // split each "name=value" into {name, value}
                    .Select(c => c.Split('='))
                    // assemble into dictionary
                    .ToDictionary(p => p[0], p => p[1]);
            cookies[cookieName] = cookieValue;
            // reassemble dictionary into m_SharedCookie
            m_SharedCookie = string.Join("; ", cookies.Select(e => e.Key + "=" + e.Value));
        }

	    internal void SetPageId(string pageId)
	    {
	        m_PageID = pageId;
	    }
    }
}

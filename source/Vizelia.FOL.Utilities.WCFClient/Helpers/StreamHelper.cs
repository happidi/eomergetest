﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Utilities.WCFClient {
	/// <summary>
	/// Helper with Stream objects
	/// </summary>
	public static class WCFClientHelper {
		const string const_output_file_header = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><!--Mapping document--><XML><action type=\"update\">";
		const string const_output_file_footer = "</action></XML>";

        /// <summary>
        /// Writes stream to file.
        /// </summary>
        /// <param name="inputStream">The input stream.</param>
        /// <param name="outputFilename">The output filename.</param>
		public static void WriteStreamToFile(Stream inputStream, string outputFilename)
		{
			using (var streamReader = new StreamReader(inputStream)) {
				using (var outputFile = new FileStream(outputFilename, FileMode.Create, FileAccess.Write)) {
					using (var streamWriter = new StreamWriter(outputFile)) {
						streamWriter.Write(const_output_file_header + streamReader.ReadToEnd() + const_output_file_footer);
					}
				}
			}
		}
	}
}

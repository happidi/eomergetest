﻿using System.Web.UI;

// Those 2 instructions are necessary each time we need to expose a resource as a javascript object
// This creates several localized js files called "Langue.js". Script manager of Asp.Net knows to serve the localized file
// based on the client requesting the resource. The files created from the .Net resources files(.resx) with the same name. 
[assembly: WebResource("Vizelia.FOL.Common.Localization.Langue.js" , "text/javascript")]
[assembly: ScriptResource("Vizelia.FOL.Common.Localization.Langue.js" , "Vizelia.FOL.Common.Localization.Langue" , "Viz.Localization.Langue")]
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.ServiceModel;
using System.Data;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.WCFService.Contracts {
	/// <summary>
	/// Service contract for ServiceDesk service.
	/// </summary>
	[ServiceContract(Namespace = "http://www.vizelia.com/wcf/2011/01/")]
	public interface IServiceDeskWCF : IBaseModuleWCF {

		/// <summary>
		/// Deletes an existing business entity ActionRequest.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool ActionRequest_Delete(ActionRequest item);

		/// <summary>
		/// Creates a new business entity ActionRequest and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ActionRequest_FormCreate(ActionRequest item);

		/// <summary>
		/// Loads a specific item for the business entity ActionRequest.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ActionRequest_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity ActionRequest and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ActionRequest_FormUpdate(ActionRequest item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ActionRequest_FormUpdateBatch(string[] keys, ActionRequest item);

		/// <summary>
		/// Gets a list for the business entity ActionRequest. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<ActionRequest> ActionRequest_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity ActionRequest.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ActionRequest ActionRequest_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity ActionRequest.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ActionRequest> ActionRequest_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity ActionRequest.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ActionRequest> ActionRequest_SaveStore(CrudStore<ActionRequest> store);

		/// <summary>
		/// Deletes an existing business entity Approval.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool Approval_Delete(Approval item);

		/// <summary>
		/// Creates a new business entity Approval and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Approval_FormCreate(Approval item);

		/// <summary>
		/// Loads a specific item for the business entity Approval.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Approval_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Approval and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Approval_FormUpdate(Approval item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Approval_FormUpdateBatch(string[] keys, Approval item);

		/// <summary>
		/// Gets a list for the business entity Approval. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Approval> Approval_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Approval.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Approval Approval_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity Approval.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Approval> Approval_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store for the business entity Approval.
		/// </summary>
		/// <param name="KeyActionRequest">The Key of the action request.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Approval> Approval_GetStoreFromActionRequest(string KeyActionRequest, PagingParameter paging,
															   PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Approval.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Approval> Approval_SaveStore(CrudStore<Approval> store);

		/// <summary>
		/// Calculates the next schedule date based on a location.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="minutes"></param>
		/// <param name="KeyLocation"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DateTime? Calendar_CalculateScheduleDate(DateTime start, double minutes, string KeyLocation);

		/// <summary>
		/// Gets a json store of ClassificationItem for a specific mail
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ClassificationItem> ClassificationItem_GetStoreByKeyMail(PagingParameter paging, string KeyMail);

		/// <summary>
		/// Gets a json store of locations for a specific mail
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Location> Location_GetStoreByKeyMail(PagingParameter paging, string KeyMail);

		/// <summary>
		/// Deletes an existing business entity Mail.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool Mail_Delete(Mail item);

		/// <summary>
		/// Creates a new business entity Mail and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="priorities">The store of the priorities for the mail.</param>
		/// <param name="classifications">The store of the classifications for the mail.</param>
		/// <param name="locations">The store of the locations for the mail.</param>
		/// <param name="workflowActionElements">The workflow action elements.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Mail_FormCreate(Mail item, CrudStore<Priority> priorities, CrudStore<ClassificationItem> classifications, CrudStore<Location> locations, CrudStore<WorkflowActionElement> workflowActionElements);

		/// <summary>
		/// Loads a specific item for the business entity Mail.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Mail_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Mail and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="priorities">The store of the priorities for the mail.</param>
		/// <param name="classifications">The store of the classifications for the mail.</param>
		/// <param name="locations">The store of the locations for the mail.</param>
		/// <param name="workflowActionElements">The workflow action elements.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Mail_FormUpdate(Mail item, CrudStore<Priority> priorities, CrudStore<ClassificationItem> classifications, CrudStore<Location> locations, CrudStore<WorkflowActionElement> workflowActionElements);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Mail_FormUpdateBatch(string[] keys, Mail item);

		/// <summary>
		/// Gets a list for the business entity Mail. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Mail> Mail_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Mail.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Mail Mail_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity Mail.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Mail> Mail_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Mail.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Mail> Mail_SaveStore(CrudStore<Mail> store);

		/// <summary>
		/// Deletes an existing business entity Priority.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool Priority_Delete(Priority item);

		/// <summary>
		/// Creates a new business entity Priority and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Priority_FormCreate(Priority item);

		/// <summary>
		/// Loads a specific item for the business entity Priority.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Priority_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Priority and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Priority_FormUpdate(Priority item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Priority_FormUpdateBatch(string[] keys, Priority item);

		/// <summary>
		/// Gets a list for the business entity Priority. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Priority> Priority_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Priority.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Priority Priority_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity Priority.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Priority> Priority_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store of priorities for a specific mail
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Priority> Priority_GetStoreByKeyMail(PagingParameter paging, string KeyMail);

		/// <summary>
		/// Saves a crud store for the business entity Priority.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Priority> Priority_SaveStore(CrudStore<Priority> store);

		/// <summary>
		/// Deletes an existing business entity Task.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool Task_Delete(Task item);

		/// <summary>
		/// Creates a new business entity Task and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Task_FormCreate(Task item);

		/// <summary>
		/// Loads a specific item for the business entity Task.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Task_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Task and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Task_FormUpdate(Task item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Task_FormUpdateBatch(string[] keys, Task item);

		/// <summary>
		/// Gets a list for the business entity Task. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Task> Task_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Task.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Task Task_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity Task.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Task> Task_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store for the business entity Task from an ActionRequest.
		/// </summary>
		/// <param name="KeyActionRequest">The Key of the action request.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Task> Task_GetStoreFromActionRequest(string KeyActionRequest, PagingParameter paging,
													   PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Task.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Task> Task_SaveStore(CrudStore<Task> store);

		/// <summary>
		/// Builds the image of the workflow.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ImageMap Workflow_BuildImage(Guid instanceId, int width, int height);

		/// <summary>
		/// Gets all the workflows from bins.
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<WorkflowAssemblyDefinition> Workflow_GetAllWorkflows();

		/// <summary>
		/// Gets the possible events that a state machine workflow instance can listen to.
		/// </summary>
		/// <param name="entity">The workflow entity.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		[ServiceKnownType(typeof(ActionRequest))]
		[ServiceKnownType(typeof(Task))]
		List<WorkflowActionElement> Workflow_GetStateMachineEventsTransitions(IWorkflowEntity entity);

		/// <summary>
		/// Gets all possible events from an assembly.
		/// </summary>
		/// <param name="assemblyName">The full name of the assembly.
		/// <example>Vizelia.FOL.Workflow.Services.Machine, Vizelia.FOL.WF.Workflows, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null</example>
		/// </param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<WorkflowActionElement> Workflow_GetStateMachineEventsTransitionsFromAssembly(string assemblyName);

		/// <summary>
		/// Gets a distinct store of possible WorkflowActionElement given a list of KeyClassificationItem.
		/// </summary>
		/// <param name="keys">An array of KeyClassificationItem.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<WorkflowActionElement> Workflow_GetStateMachineEventsTransitionsFromClassificationItems(string[] keys);

		/// <summary>
		///  Gets a store of WorkflowActionElement for a specified Mail.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<WorkflowActionElement> Workflow_GetStoreWorkflowActionElementByKeyMail(PagingParameter paging, string KeyMail);

		/// <summary>
		/// Returns the stream of the image for a worflow.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream Workflow_GetStreamImage(Guid instanceId, int width, int height);

		/// <summary>
		/// Returns the stream of the image for a worflow.
		/// </summary>
		/// <param name="guid">The guid of the image.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream Workflow_GetStreamImageFromCache(string guid);

		/// <summary>
		/// Runs workflow when raising an event on a IWorkflowEnity instance.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="eventName">Name of the event.</param>
		/// <param name="workflowQualifiedName">Name of the workflow qualified.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		[ServiceKnownType(typeof(ActionRequest))]
		[ServiceKnownType(typeof(Task))]
		FormResponse Workflow_RaiseEvent(IWorkflowEntity entity, string eventName, string workflowQualifiedName);

		/// <summary>
		/// Restarts the workflow.
		/// This will generate a new instanceId and also update the workflow definition.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		[ServiceKnownType(typeof(ActionRequest))]
		[ServiceKnownType(typeof(Task))]
		FormResponse Workflow_Restart(IWorkflowEntity entity);

		/// <summary>
		/// Resumes all the pending workflows.
		/// </summary>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void Workflow_ResumePending();

		/// <summary>
		/// Update the workflow definition with current state.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		[ServiceKnownType(typeof(ActionRequest))]
		[ServiceKnownType(typeof(Task))]
		FormResponse Workflow_UpdateInstanceDefinition(IWorkflowEntity entity);
	}
}

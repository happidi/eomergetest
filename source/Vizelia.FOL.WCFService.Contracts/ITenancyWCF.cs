﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.WCFService.Contracts {
	/// <summary>
	/// Service contract for Tenancy service.
	/// </summary>
	[ServiceContract(Namespace = "http://www.vizelia.com/wcf/2011/01/")]
	public interface ITenancyWCF : IBaseModuleWCF {

		/// <summary>
		/// Deletes an existing business entity Tenant.
		/// </summary>
		/// <param name="keyTenant">The key tenant.</param>
		/// <param name="operationId">The operation id.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void Tenant_Delete(string keyTenant, Guid operationId);

		/// <summary>
		/// Creates a new business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="operationId">The operation id.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void Tenant_FormCreate(Tenant item, Guid operationId);

		/// <summary>
		/// Loads a specific item for the business entity Tenant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Tenant_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="operationId">The operation id.</param>
		/// <returns></returns>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void Tenant_FormUpdate(Tenant item, Guid operationId);
		
		/// <summary>
		/// Gets a list for the business entity Tenant. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Tenant> Tenant_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Tenant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Tenant Tenant_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity Tenant.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Tenant> Tenant_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Tenant.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Tenant> Tenant_SaveStore(CrudStore<Tenant> store);

		/// <summary>
		/// Resets the current tenant.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="adminPassword">The admin password.</param>
		[OperationContract]
		[WebGet]
		void ResetTenant(Guid operationId, string adminPassword);

	}
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
namespace Vizelia.FOL.WCFService.Contracts {

	/// <summary>
	/// Service contract for SpaceManagement service.
	/// </summary>
	[ServiceContract(Namespace = "http://www.vizelia.com/wcf/2011/01/")]
	public interface ISpaceManagementWCF : IBaseModuleWCF {

		/// <summary>
		/// Gets the zoning 
		/// </summary>
		/// <param name="KeyBuildingStorey"></param>
		/// <param name="KeyClassificationItem">The filter for Classifications (optional)</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Space> BuildingStorey_GetZoning(string KeyBuildingStorey, string KeyClassificationItem);

	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.WCFService.Contracts {

	/// <summary>
	/// Service contract for Mapping service.
	/// </summary>
	[ServiceContract(Namespace = "http://www.vizelia.com/wcf/2011/01/")]
	public interface IMappingWCF : IBaseModuleWCF {

		/// <summary>
		/// Starts the mapping process.
		/// </summary>
		/// <param name="item">The mapping request item.</param>
		/// <param name="operationId">The operation id.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void BeginMapping(FileUploadRequest item, Guid operationId);

		/// <summary>
		/// Gets all mappings.
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebGet]
		string GetAllMappings();

		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		[OperationContract]
		[WebGet]
		IEnumerable<string> GetAllowedMappingFileExtensions();

		#region MappingTask

		/// <summary>
		/// Gets a json store for the business entity MappingTask.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MappingTask> MappingTask_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity MappingTask.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MappingTask> MappingTask_SaveStore(CrudStore<MappingTask> store);

		/// <summary>
		/// Gets a list for the business entity MappingTask. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<MappingTask> MappingTask_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity MappingTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		MappingTask MappingTask_GetItem(string Key);

		/// <summary>
		/// Loads a specific item for the business entity MappingTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MappingTask_FormLoad(string Key);

		/// <summary>
		/// Creates a new business entity MappingTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MappingTask_FormCreate(MappingTask item);

		/// <summary>
		/// Updates an existing business entity MappingTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MappingTask_FormUpdate(MappingTask item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MappingTask_FormUpdateBatch(string[] keys, MappingTask item);

		/// <summary>
		/// Deletes an existing business entity MappingTask.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool MappingTask_Delete(MappingTask item);

		#endregion

		#region MeterDataExportTask
		/// <summary>
		/// Gets a json store for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MeterDataExportTask> MeterDataExportTask_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MeterDataExportTask> MeterDataExportTask_SaveStore(CrudStore<MeterDataExportTask> store);

		/// <summary>
		/// Gets a list for the business entity MeterDataExportTask. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<MeterDataExportTask> MeterDataExportTask_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		MeterDataExportTask MeterDataExportTask_GetItem(string Key);

		/// <summary>
		/// Loads a specific item for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterDataExportTask_FormLoad(string Key);

		/// <summary>
		/// Creates a new business entity MeterDataExportTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterDataExportTask_FormCreate(MeterDataExportTask item, CrudStore<Meter> meters);

		/// <summary>
		/// Updates an existing business entity MeterDataExportTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterDataExportTask_FormUpdate(MeterDataExportTask item, CrudStore<Meter> meters);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterDataExportTask_FormUpdateBatch(string[] keys, MeterDataExportTask item);

		/// <summary>
		/// Deletes an existing business entity MeterDataExportTask.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool MeterDataExportTask_Delete(MeterDataExportTask item);


		#endregion

		/// <summary>
		/// Tests the connection to the file source.
		/// This is a long running operation that returns a business object.
		/// </summary>
		/// <param name="keyMappingTask">The key of the mapping task.</param>
		/// <param name="operationId">The operation id.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void BeginTestConnection(string keyMappingTask, Guid operationId);

		/// <summary>
		/// Gets a list of the given business entity, ready for export.
		/// </summary>
		/// <param name="entityType">Type of the entity, expressed as the class name.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<BaseBusinessEntity> GetEntitiesForExport(string entityType, PagingParameter paging);

		/// <summary>
		/// Maps the excel to XML.
		/// </summary>
		/// <param name="fileStream">The file stream.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Guid MapExcelToXml(Stream fileStream);

		/// <summary>
		/// Begins the mapping file conversion.
		/// </summary>
		/// <param name="item">The mapping request item.</param>
		/// <param name="operationId">The operation id.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void BeginMappingFileConversion(FileUploadRequest item, Guid operationId);
	}
}

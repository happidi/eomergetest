﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.ServiceModel.Description;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using System.Threading;

namespace Vizelia.FOL.WCFService.Contracts {
	/// <summary>
	/// This attributes extends WCF operation behavior.
	/// It inspects the parameter of the operation and if EnergyAggregatorContext is found, it sets the ContextHelper.
	/// It is primarely used in Energy Aggregator WCF.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method)]
	public class SetEnergyAggregatorContextAttribute : Attribute, IOperationBehavior {
		private int _energyAggregatorContextParamIndex;

		private bool _requireInitializationCheck = false;

		void IOperationBehavior.Validate(OperationDescription description) {

			_requireInitializationCheck = description.DeclaringContract.ContractType == typeof (IEnergyAggregatorWCF);

			int index = description.SyncMethod.GetParameters().ToList().Single(prm =>prm.ParameterType == typeof(EnergyAggregatorContext)).Position;
			_energyAggregatorContextParamIndex = index;
		}

		void IOperationBehavior.AddBindingParameters(OperationDescription description, BindingParameterCollection parameters) {
		}


		void IOperationBehavior.ApplyClientBehavior(OperationDescription description, ClientOperation proxy) {
		}

		void IOperationBehavior.ApplyDispatchBehavior(OperationDescription description, DispatchOperation dispatch) {
			dispatch.ParameterInspectors.Add(new SetApplicationNameBehavior(_energyAggregatorContextParamIndex, _requireInitializationCheck));
		}
	}

	/// <summary>
	/// The implementation of the parameter inspection
	/// </summary>
	internal class SetApplicationNameBehavior : IParameterInspector {

		readonly int _energyAggregatorContextParamIndex;
		readonly bool _requireInitializationCheck;

		/// <summary>
		/// Initializes a new instance of the <see cref="SetApplicationNameBehavior"/> class.
		/// </summary>
		/// <param name="energyAggregatorContextParamIndex">Index of the energy aggregator context param.</param>
		/// <param name="requireInitializationCheck">if set to <c>true</c> [require initialization check].</param>
		public SetApplicationNameBehavior(int energyAggregatorContextParamIndex, bool requireInitializationCheck) {
			_energyAggregatorContextParamIndex = energyAggregatorContextParamIndex;
			_requireInitializationCheck = requireInitializationCheck;
		}

		/// <summary>
		/// Called after client calls are returned and before service responses are sent.
		/// </summary>
		/// <param name="operationName">The name of the invoked operation.</param>
		/// <param name="outputs">Any output objects.</param>
		/// <param name="returnValue">The return value of the operation.</param>
		/// <param name="correlationState">Any correlation state returned from the <see cref="M:System.ServiceModel.Dispatcher.IParameterInspector.BeforeCall(System.String,System.Object[])"/> method, or null.</param>
		public void AfterCall(string operationName, object[] outputs, object returnValue, object correlationState) {
		}


		/// <summary>
		/// Called before client calls are sent and after service responses are returned.
		/// </summary>
		/// <param name="operationName">The name of the operation.</param>
		/// <param name="inputs">The objects passed to the method by the client.</param>
		/// <returns>
		/// The correlation state that is returned as the parameter in <see cref="M:System.ServiceModel.Dispatcher.IParameterInspector.AfterCall(System.String,System.Object[],System.Object,System.Object)"/>. Return null if you do not intend to use correlation state.
		/// </returns>
		public object BeforeCall(string operationName, object[] inputs) {
			// validate parameters before call
			EnergyAggregatorContext context = inputs[_energyAggregatorContextParamIndex] as EnergyAggregatorContext;
			if (context == null)
				throw new FaultException("Parameter should be application name");

			ContextHelper.ApplicationName = context.ApplicationName;
			
			if (context.Culture != null) {
				Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(context.Culture);
			}

			if ( _requireInitializationCheck ) {
				IEnergyAggregatorWCF localProxy = Helper.Resolve<IEnergyAggregatorWCF>();
				if (localProxy.IsInitialized(context) == false) {
					localProxy.Initialize(new List<EnergyAggregatorContext>() {context});
				}
			}
			
			return null;
		}
	}
}

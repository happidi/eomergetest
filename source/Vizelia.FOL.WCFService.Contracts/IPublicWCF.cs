﻿using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Configuration;

namespace Vizelia.FOL.WCFService.Contracts {

	/// <summary>
	/// Service contract for Public service.
	/// </summary>
	[ServiceContract(Namespace = "http://www.vizelia.com/wcf/2011/01/")]
	public interface IPublicWCF : IBaseModuleWCF {

		/// <summary>
		/// Gets the store of authentication providers declared in vizelia section of web.config.
		/// The returned store is used to build a domain combobox in login form.
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ListElement> AuthenticationProvider_GetStore();

		/// <summary>
		/// Returns the stream of the image (with map) from a chart stored in the cache
		/// (used by printscreen.aspx because cuttycapt doesnt send cookies).
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="guid">The guid of the image.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream Chart_GetStreamImageFromCache(string applicationName, string guid);


		/// <summary>
		/// Returns the stream of the image (without map) from a chart.
		/// (used by printscreen.aspx because cuttycapt doesnt send cookies).
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="Key">The key of the Chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream Chart_GetStreamImage(string applicationName, string Key, int width, int height);


		/// <summary>
		/// Returns the stream of the image  from a dynamicdisplayimage.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="Key">The key of the dynamicdisplayimage.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream DynamicDisplayImage_GetStreamImage(string applicationName, string Key, int width, int height);

		/// <summary>
		/// Returns the stream of the image from a playlist page (corresponding to the latest generation of the playlist).
		/// </summary>
		/// <param name="Key">The key of the playlist page.</param>
		/// <param name="applicationName">Name of the application.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream PlaylistPage_GetStreamImage(string Key, string applicationName);


		/// <summary>
		/// Initializes the database.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="adminPassword">The admin password.</param>
		/// <param name="tenantName">Name of the tenant.</param>
		[OperationContract(IsOneWay = true)]
		[WebGet]
		void InitializeTenant(Guid operationId, string adminPassword, string tenantName);



		/// <summary>
		/// Checks user credentials and creates an authentication ticket (cookie) if the credentials are valid.
		/// Returns a detail message with the reason for not login the user.
		/// </summary>
		/// <param name="userName">The user name to be validated.</param>
		/// <param name="password">The password for the specified user.</param>
		/// <param name="createPersistentCookie">A value that indicates whether the authentication ticket remains valid across sessions.</param>
		/// <param name="provider">The provider used to check credentials</param>
		/// <returns>true if user credentials are valid; otherwise, false.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Login_Response(string userName, string password, bool createPersistentCookie, string provider);

		/// <summary>
		/// Resets the password for a membership user that forgot the password.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="email">The email.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse UserForgotPassword(string username, string email);

		/// <summary>
		/// Starts an sso login process
		/// </summary>
		/// <param name="providerName">The key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void SSOLogin(string providerName);

		/// <summary>
		/// Clears the authentication ticket (cookie) in the browser.
		/// </summary>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void Logout();

		/// <summary>
		/// Resumes the pending workflows.
		/// </summary>
		[OperationContract]
		[WebGet]
		void ResumePendingWorkflows();

		/// <summary>
		/// Changes the password for a membership user through a form with old password and new password.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="username">The user name.</param>
		/// <param name="oldPassword">The old password.</param>
		/// <param name="newPassword">The new password.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse User_ChangePasswordForm(string applicationName, string username, string oldPassword, string newPassword);

		/// <summary>
		/// Return a stream from a VirtualFile Path.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream VirtualFile_GetStreamImage(string applicationName, string value);

		/// <summary>
		/// Determines whether the current tenant is initialized.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the current tenant is initialized; otherwise, <c>false</c>.
		/// </returns>
		[OperationContract]
		[WebGet]
		bool IsTenantInitialized();


		/// <summary>
		/// Determines whether the current user is user authenticated.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the current user is authenticated otherwise, <c>false</c>.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		UserAuthenticationStatus IsUserAuthenticated();


		/// <summary>
		/// Determines whether the client and the server have the same time.
		/// </summary>
		/// <param name="clientTime">The client time.</param>
		/// <returns>
		///   <c>true</c> if the client and the server have the same time. otherwise, <c>false</c>.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool IsTimeSynchronized(DateTime clientTime);

		/// <summary>
		/// Gets the application settings.
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ApplicationSettings GetApplicationSettings();
	}
}

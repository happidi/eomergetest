﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;
using System.ServiceModel;
using System.Reflection;

namespace Vizelia.FOL.WCFService.Contracts {
	/// <summary>
	///	Implementation of the base class for all the WCF services.
	///	This class has a static ctor used to run initialization (Init).
	/// </summary>
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
	public class BaseWCF: IBaseWCF {
		/// <summary>
		/// Returns the version of the assembly.
		/// </summary>
		/// <returns>
		/// The version of the service.
		/// </returns>
		public virtual string GetVersion() {
			Assembly asm = this.GetType().Assembly;
			AssemblyName asmName = asm.GetName();
			return asmName.Name + " " + asmName.Version.ToString();
		}
	}
}

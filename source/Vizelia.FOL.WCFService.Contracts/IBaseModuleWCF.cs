﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Vizelia.FOL.WCFService.Contracts {
	/// <summary>
	/// Base implementation for all regular  WCF services.
	/// Do not implement any public OperationContract on this interface
	/// </summary>
	[ServiceContract(Namespace = "http://www.vizelia.com/wcf/2011/01/")]
	public interface IBaseModuleWCF : IBaseWCF{
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.WCFService.Contracts {

	/// <summary>
	/// Service contract for Energy service.
	/// </summary>
	[ServiceContract(Namespace = "http://www.vizelia.com/wcf/2011/01/")]
	public interface IEnergyWCF : IBaseModuleWCF {

		/// <summary>
		/// Deletes an existing business entity AlarmDefinition.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool AlarmDefinition_Delete(AlarmDefinition item);

		/// <summary>
		/// Creates a new business entity AlarmDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <param name="charts">The charts.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlarmDefinition_FormCreate(AlarmDefinition item, CrudStore<Meter> meters, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification, CrudStore<Chart> charts);

		/// <summary>
		/// Loads a specific item for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlarmDefinition_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity AlarmDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <param name="filterSpatialPset">The filter spatial pset.</param>
		/// <param name="charts">The charts.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlarmDefinition_FormUpdate(AlarmDefinition item, CrudStore<Meter> meters, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification, CrudStore<FilterSpatialPset> filterSpatialPset, CrudStore<Chart> charts);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlarmDefinition_FormUpdateBatch(string[] keys, AlarmDefinition item);

		/// <summary>
		/// Gets a list for the business entity AlarmDefinition. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<AlarmDefinition> AlarmDefinition_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		AlarmDefinition AlarmDefinition_GetItem(string Key);

		/// <summary>
		/// Return a json store of the complete list of Meters associated with a AlarmDefinition.
		/// including meters associated via SpatialFilter cross SpatialFilterPset cross MeterClassification
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key of the AlarmDefinition.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Meter> AlarmDefinition_GetMetersStore(string KeyAlarmDefinition, PagingParameter paging);

		/// <summary>
		/// Gets a json store for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmDefinition> AlarmDefinition_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets the AlarmDefinition store by a job and a step.
		/// </summary>
		/// <param name="keyJob">The key job.</param>
		/// <param name="keyStep">The key step.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmDefinition> AlarmDefinition_GetStoreByJobStep(string keyJob, string keyStep, PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store of AlarmDefinition for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmTable">The Key of the AlarmTable.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmDefinition> AlarmDefinition_GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable);

		/// <summary>
		/// Gets all the enabled AlarmDefinition and process them to generate the AlarmInstances
		/// </summary>
		/// <param name="Keys">The keys.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void AlarmDefinition_Process(string[] Keys);

		/// <summary>
		/// Gets all the enabled AlarmDefinition and process them to generate the AlarmInstanceswith a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void AlarmDefinition_ProcessBegin(Guid operationId, string[] Keys);

		/// <summary>
		/// Saves a crud store for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmDefinition> AlarmDefinition_SaveStore(CrudStore<AlarmDefinition> store);

		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview for the AlarmDefinition Classification..
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The parent entity.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<TreeNode> AlarmDefinitionClassification_GetTree(string Key, ClassificationItem entity);

		/// <summary>
		/// Deletes an existing business entity AlarmInstance.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool AlarmInstance_Delete(AlarmInstance item);


		/// <summary>
		/// Deletes all existing alarm instances for a specific AlarmDefinition or MeterValidationRule.
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool AlarmInstance_DeleteAll(string KeyAlarmDefinition, string KeyMeterValidationRule);

        /// <summary>
        /// Begins long running operation to deletes all existing alarm instances for a specific AlarmDefinition or MeterValidationRule.
        /// </summary>
        /// <param name="operationId">The operation identifier.</param>
        /// <param name="KeyAlarmDefinition">The key alarm definition.</param>
        /// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
        [OperationContract(IsOneWay = true)]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void AlarmInstance_BeginDeleteAll(Guid operationId, string KeyAlarmDefinition, string KeyMeterValidationRule);


		/// <summary>
		/// Creates a new business entity AlarmInstance and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlarmInstance_FormCreate(AlarmInstance item);

		/// <summary>
		/// Loads a specific item for the business entity AlarmInstance.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlarmInstance_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity AlarmInstance and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlarmInstance_FormUpdate(AlarmInstance item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlarmInstance_FormUpdateBatch(string[] keys, AlarmInstance item);

		/// <summary>
		/// Gets a list for the business entity AlarmInstance. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<AlarmInstance> AlarmInstance_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity AlarmInstance.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		AlarmInstance AlarmInstance_GetItem(string Key);

		/// <summary>
		/// Get the latest AlarmInstances with an InstanceDatetime newer than Now - seconds.
		/// </summary>
		/// <param name="seconds">The seconds.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<AlarmInstance> AlarmInstance_GetLatest(int seconds);

		/// <summary>
		/// Gets a json store for the business entity AlarmInstance.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmInstance> AlarmInstance_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets all the AlarmInstance for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition);

		/// <summary>
		/// Gets all the AlarmInstance for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmTable">The Key of the AlarmTable.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable);

		/// <summary>
		/// Gets all the AlarmInstance for a specific Meter.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeter">The key meter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyMeter(PagingParameter paging, string KeyMeter);

		/// <summary>
		/// Gets all the AlarmInstance for a specific MeterData.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterData">The key meter data.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyMeterData(PagingParameter paging, string KeyMeterData);

		/// <summary>
		/// Gets all the AlarmInstance for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule);

		/// <summary>
		/// Saves a crud store for the business entity AlarmInstance.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmInstance> AlarmInstance_SaveStore(CrudStore<AlarmInstance> store);

		/// <summary>
		/// Deletes an existing business entity AlarmTable.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool AlarmTable_Delete(AlarmTable item);

		/// <summary>
		/// Creates a new business entity AlarmTable and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="alarmdefinitions">The alarmdefinitions.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlarmTable_FormCreate(AlarmTable item, CrudStore<AlarmDefinition> alarmdefinitions, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterAlarmDefinitionClassification);

		/// <summary>
		/// Loads a specific item for the business entity AlarmTable.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlarmTable_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity AlarmTable and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="alarmdefinitions">The alarmdefinitions.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlarmTable_FormUpdate(AlarmTable item, CrudStore<AlarmDefinition> alarmdefinitions, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterAlarmDefinitionClassification);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlarmTable_FormUpdateBatch(string[] keys, AlarmTable item);

		/// <summary>
		/// Return a json store of the complete list of AlarmDefinitions associated with a AlarmTable
		/// including AlarmDefinitions associated via SpatialFilter  cross AlarmDefinitionClassification.
		/// </summary>
		/// <param name="KeyAlarmTable">The key alarm table.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmDefinition> AlarmTable_GetAlarmDefinitionsStore(string KeyAlarmTable, PagingParameter paging);

		/// <summary>
		/// Gets a list for the business entity AlarmTable. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<AlarmTable> AlarmTable_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity AlarmTable.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		AlarmTable AlarmTable_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmTable> AlarmTable_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity AlarmTable.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlarmTable> AlarmTable_SaveStore(CrudStore<AlarmTable> store);

		/// <summary>
		/// Checks if the formula is valid.
		/// </summary>
		///<param name="KeyChart">The Key of the Chart the calculatedseries belong to.</param>
		/// <param name="field">the name of the field.</param>
		/// <param name="value">the value of the field.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse CalculatedSerie_CheckFormula(string KeyChart, string field, string value);

		/// <summary>
		/// Deletes an existing business entity CalculatedSerie.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool CalculatedSerie_Delete(CalculatedSerie item);

		/// <summary>
		/// Creates a new business entity CalculatedSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse CalculatedSerie_FormCreate(CalculatedSerie item);

		/// <summary>
		/// Loads a specific item for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse CalculatedSerie_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity CalculatedSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse CalculatedSerie_FormUpdate(CalculatedSerie item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse CalculatedSerie_FormUpdateBatch(string[] keys, CalculatedSerie item);

		/// <summary>
		/// Gets a list for the business entity CalculatedSerie. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<CalculatedSerie> CalculatedSerie_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		CalculatedSerie CalculatedSerie_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<CalculatedSerie> CalculatedSerie_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of calculatedSeries for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<CalculatedSerie> CalculatedSerie_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Saves a crud store for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<CalculatedSerie> CalculatedSerie_SaveStore(CrudStore<CalculatedSerie> store);

		/// <summary>
		/// Add a classification item to an existing Chart(used in dragdrop of classification tree to chart).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="Key">The key of the entity.</param>
		/// <param name="type">The type of entity.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Chart Chart_AddClassificationItemEntity(string KeyChart, string Key, string type);

		/// <summary>
		/// Add a spatial entity (Site, Building, Meter) to an existing Chart(used in dragdrop of spatial tree to chart).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="Key">The key of the entity.</param>
		/// <param name="type">The type of entity.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Chart Chart_AddSpatialEntity(string KeyChart, string Key, string type);


		/// <summary>
		/// Apply ModernUI color to a chart.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="color">The color.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Chart Chart_ApplyModernUIColor(string Key, ModernUIColor color);

		/// <summary>
		/// Builds the image (with map) from a chart.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ImageMap Chart_BuildImage(string Key, int width, int height);

		/// <summary>
		/// Clear the cache of any information related to this Charts.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="KeyDynamicDisplay">The key dynamic display.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void Chart_ClearCache(string Key, string KeyDynamicDisplay = null);

		/// <summary>
		/// Copy existing business entity Chart.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy </param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Chart_Copy(string[] keys);

		/// <summary>
		/// Remove a point from the Correlation analysis.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="xValue">The x value.</param>
		/// <param name="yValue">The y value.</param>
		/// <param name="LocalId">The local id.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Chart Chart_CorrelationRemovePoint(string KeyChart, double xValue, double yValue, string LocalId);

		/// <summary>
		/// Deletes an existing business entity Chart.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool Chart_Delete(Chart item);

		/// <summary>
		/// Creates a new business entity Chart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <param name="filterEventLogClassification">The filter event log classification.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <param name="filterAzManRoles">The filter KPI az man roles.</param>
		/// <param name="meters">The store of the meters of the chart.</param>
		/// <param name="dataseries">The store of the dataseries of the Chart.</param>
		/// <param name="calculatedseries">The store of the calculatedseries of the Chart.</param>
		/// <param name="statisticalseries">The statisticalseries.</param>
		/// <param name="chartaxis">The store of chartaxis of the Chart.</param>
		/// <param name="chartmarker">The store of chartmarker of the Chart.</param>
		/// <param name="charthistoricalanalysis">the store of ChartHistoricalAnalysis of the Chart.</param>
		/// <param name="historicals">The historicals.</param>
		/// <param name="Algorithms">The Algorithms.</param>
		/// <param name="KeyChartToCopy">The key chart to copy (coming from the Chart wizard KPI).</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Chart_FormCreate(Chart item, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification, CrudStore<ClassificationItem> filterEventLogClassification, CrudStore<ClassificationItem> filterAlarmDefinitionClassification, CrudStore<AuthorizationItem> filterAzManRoles, CrudStore<Meter> meters, CrudStore<DataSerie> dataseries, CrudStore<CalculatedSerie> calculatedseries, CrudStore<StatisticalSerie> statisticalseries, CrudStore<ChartAxis> chartaxis, CrudStore<ChartMarker> chartmarker, CrudStore<ChartHistoricalAnalysis> charthistoricalanalysis, CrudStore<ChartPsetAttributeHistorical> historicals, CrudStore<ChartAlgorithm> Algorithms, string KeyChartToCopy);

		/// <summary>
		/// Loads a specific item for the business entity Chart.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Chart_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Chart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <param name="filterEventLogClassification">The filter event log classification.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <param name="filterAzManRoles">The filter KPI az man roles.</param>
		/// <param name="meters">The store of the meters of the Chart.</param>
		/// <param name="dataseries">The store of the dataseries of the Chart.</param>
		/// <param name="calculatedseries">The store of the calculatedseries of the Chart.</param>
		/// <param name="statisticalseries">The statisticalseries.</param>
		/// <param name="chartaxis">The store of chartaxis of the Chart.</param>
		/// <param name="chartmarker">The store of chartmarker of the Chart.</param>
		/// <param name="charthistoricalanalysis">the store of ChartHistoricalAnalysis of the Chart.</param>
		/// <param name="chartfilterpset">The pset filters associated to the chart.</param>
		/// <param name="historicals">The historicals.</param>
		/// <param name="Algorithms">The Algorithms.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Chart_FormUpdate(Chart item, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification, CrudStore<ClassificationItem> filterEventLogClassification, CrudStore<ClassificationItem> filterAlarmDefinitionClassification, CrudStore<AuthorizationItem> filterAzManRoles, CrudStore<Meter> meters, CrudStore<DataSerie> dataseries, CrudStore<CalculatedSerie> calculatedseries, CrudStore<StatisticalSerie> statisticalseries, CrudStore<ChartAxis> chartaxis, CrudStore<ChartMarker> chartmarker, CrudStore<ChartHistoricalAnalysis> charthistoricalanalysis, CrudStore<FilterSpatialPset> chartfilterpset, CrudStore<ChartPsetAttributeHistorical> historicals, CrudStore<ChartAlgorithm> Algorithms);

		/// <summary>
		/// Returns the requested chart. Unlike UpdateChart method, which updates all the Chart additional information(meters, spatial hierarchy, classification),
		/// this methos updates only the core Chart entity.
		/// </summary>
		/// <param name="item">The item to update.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Chart_ChangeView(Chart item);



		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Chart_FormUpdateBatch(string[] keys, Chart item);

		/// <summary>
		/// Gets a list for the business entity Chart. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Chart> Chart_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Return a store of available Series for a specific Chart.
		/// </summary>
		/// <param name="Key">The Key of the Chart.</param>
		/// <param name="includeExisting">True to include existing serie, false to return only series to be created.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ListElement> Chart_GetAvailableDataSerieLocalId(string Key, bool includeExisting);

		/// <summary>
		/// Return the Chart Classification level store.
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<SimpleListElementGeneric<int>> Chart_GetClassificationLevelStore();

		/// <summary>
		/// Gets a specific item for the business entity Chart.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Chart Chart_GetItem(string Key);

		/// <summary>
		/// Gets a specific item for the business entity Chart and fetch data to create the series .
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns>The Chart Item</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Chart Chart_GetItemWithData(string Key);

		/// <summary>
		/// Gets the store of possible grouping localisation for Charts. 
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ListElementGeneric<int>> Chart_GetLocalisationStore();

		/// <summary>
		/// Return a json store of the complete list of Meters associated with a Chart.
		/// including meters associated via SpatialFilter cross SpatialFilterPset cross MeterClassification
		/// </summary>
		/// <param name="KeyChart">The key of the Chart.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Meter> Chart_GetMetersStore(string KeyChart, PagingParameter paging);

		/// <summary>
		/// Gets a json store for the business entity Chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Chart> Chart_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store for the business entity Chart for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmDefinition">The key AlarmDefinition.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Chart> Chart_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition);

		/// <summary>
		/// Gets a json store for the business entity Chart for a specific ChartScheduler.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChartScheduler">The key chart scheduler.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Chart> Chart_GetStoreByKeyChartScheduler(PagingParameter paging, string KeyChartScheduler);

		/// <summary>
		/// Gets a json store for the business entity Chart KPI.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="KeyClassificationItemKPI">The key classification item KPI.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Chart> Chart_GetStoreKPI(PagingParameter paging, PagingLocation location, string KeyClassificationItemKPI);

		/// <summary>
		/// Returns the stream of the excel chart from a chart.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream Chart_GetStreamExcel(string Key);

		/// <summary>
		/// Exports the Chart as an Excel File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the Chart.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void Chart_GetStreamExcelBegin(Guid operationId, string Key);

		/// <summary>
		/// Returns the stream of the image (without map) from a chart.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream Chart_GetStreamImage(string Key, int width, int height);

		/// <summary>
		/// Returns the stream of the image (with map) from a chart stored in the cache.
		/// </summary>
		/// <param name="guid">The guid of the image.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream Chart_GetStreamImageFromCache(string guid);

		/// <summary>
		/// Returns the chart as a stream of javascript code (by default using hightcharts.com).
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream Chart_GetStreamJavascript(string Key, int width, int height);

		/// <summary>
		/// Returns the chart as a stream of html.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream Chart_GetStreamHtml(string Key);

		/// <summary>
		/// Returns the stream of the pdf chart from a chart.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream Chart_GetStreamPdf(string Key);

		/// <summary>
		/// Exports the Chart as an Pdf File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the Chart.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void Chart_GetStreamPdfBegin(Guid operationId, string Key);

		/// <summary>
		/// Returns the stream of the image of a Micro chart.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="value">The value.</param>
		/// <param name="targetValue">The target value.</param>
		/// <param name="minValue">The min value.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream Chart_MicroChartGetStreamImage(string text, double value, double targetValue, string minValue, int width, int height);


		/// <summary>
		/// Preload in cache all the Charts that are used inside a PortalWindow.
		/// </summary>
		/// <param name="clearCache">if set to <c>true</c> [clear cache].</param>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <param name="KeyPortalWindow">The key portal window.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		PortalTab Chart_PreLoadAll(bool clearCache = false, string KeyPortalTab = null, string KeyPortalWindow = null);


		/// <summary>
		/// Preload in cache all the Charts that are used inside a PortalWindow using a long running operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="clearCache">if set to <c>true</c> [clear cache].</param>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <param name="KeyPortalWindow">The key portal window.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void Chart_PreLoadAllBegin(Guid operationId, bool clearCache = false, string KeyPortalTab = null, string KeyPortalWindow = null);

		/// <summary>
		/// Saves a crud store for the business entity Chart.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Chart> Chart_SaveStore(CrudStore<Chart> store);

		/// <summary>
		/// Deletes an existing business entity ChartAxis.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool ChartAxis_Delete(ChartAxis item);

		/// <summary>
		/// Creates a new business entity ChartAxis and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartAxis_FormCreate(ChartAxis item);

		/// <summary>
		/// Loads a specific item for the business entity ChartAxis.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartAxis_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity ChartAxis and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartAxis_FormUpdate(ChartAxis item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartAxis_FormUpdateBatch(string[] keys, ChartAxis item);

		/// <summary>
		/// Gets a list for the business entity ChartAxis. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<ChartAxis> ChartAxis_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity ChartAxis.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ChartAxis ChartAxis_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity ChartAxis.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartAxis> ChartAxis_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store of chartaxis for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartAxis> ChartAxis_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Saves a crud store for the business entity ChartAxis.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartAxis> ChartAxis_SaveStore(CrudStore<ChartAxis> store);

		/// <summary>
		/// Add a new selection to Calendar View.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void ChartCalendarViewSelection_Add(string KeyChart, string startDate, string endDate);

		/// <summary>
		/// Delete a Chart Calendar View Selection.
		/// </summary>
		/// <param name="KeyChartCalendarViewSelection">The key chart calendar view selection.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool ChartCalendarViewSelection_Delete(string KeyChartCalendarViewSelection);

		/// <summary>
		/// Reset the Chart Calendar View Selection.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void ChartCalendarViewSelection_Reset(string KeyChart);

		/// <summary>
		/// Deletes an existing business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool ChartCustomGridCell_Delete(ChartCustomGridCell item);

		/// <summary>
		/// Creates a new business entity ChartCustomGridCell and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartCustomGridCell_FormCreate(ChartCustomGridCell item);

		/// <summary>
		/// Loads a specific item for the business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartCustomGridCell_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity ChartCustomGridCell and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartCustomGridCell_FormUpdate(ChartCustomGridCell item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartCustomGridCell_FormUpdateBatch(string[] keys, ChartCustomGridCell item);

		/// <summary>
		/// Gets a list for the business entity ChartCustomGridCell. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<ChartCustomGridCell> ChartCustomGridCell_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ChartCustomGridCell ChartCustomGridCell_GetItem(string Key);

		/// <summary>
		/// Gets a list of ChartCustomGridCell for a specific chart.
		/// </summary>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <param name="populateValues">if set to <c>true</c> we fetch the chart with data and we populate the values.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<ChartCustomGridCell> ChartCustomGridCell_GetListByKeyChart(string KeyChart, bool populateValues);

		/// <summary>
		/// Gets a json store for the business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartCustomGridCell> ChartCustomGridCell_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Save a list of ChartCustomGridCell for a specific chart.
		/// </summary>
		/// <param name="cells">The cells.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool ChartCustomGridCell_SaveList(List<ChartCustomGridCell> cells);

		/// <summary>
		/// Saves a crud store for the business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartCustomGridCell> ChartCustomGridCell_SaveStore(CrudStore<ChartCustomGridCell> store);

		/// <summary>
		/// Deletes an existing business entity ChartDrillDown.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool ChartDrillDown_Delete(ChartDrillDown item);

		/// <summary>
		/// Creates a new business entity ChartDrillDown and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartDrillDown_FormCreate(ChartDrillDown item);

		/// <summary>
		/// Loads a specific item for the business entity ChartDrillDown.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartDrillDown_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity ChartDrillDown and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartDrillDown_FormUpdate(ChartDrillDown item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartDrillDown_FormUpdateBatch(string[] keys, ChartDrillDown item);

		/// <summary>
		/// Gets a list for the business entity ChartDrillDown. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<ChartDrillDown> ChartDrillDown_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity ChartDrillDown.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ChartDrillDown ChartDrillDown_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity ChartDrillDown.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartDrillDown> ChartDrillDown_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Reset the Chart drill down.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void ChartDrillDown_Reset(string KeyChart);

		/// <summary>
		/// Saves a crud store for the business entity ChartDrillDown.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartDrillDown> ChartDrillDown_SaveStore(CrudStore<ChartDrillDown> store);

		/// <summary>
		/// Update (or Create) the ChartDrillDown after a user click on the Chart.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="XDateTime">The X date time.</param>
		/// <param name="KeyLocation">The key location.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <param name="Type">Type of the drill down.</param>
		/// <param name="KeyPortalWindow">The key portal window.</param>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<ChartDrillDown> ChartDrillDown_Update(string KeyChart, string XDateTime, string KeyLocation, string KeyClassificationItem, DrillDownType Type, string KeyPortalWindow, string KeyPortalTab);

		/// <summary>
		/// Deletes an existing business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool ChartHistoricalAnalysis_Delete(ChartHistoricalAnalysis item);

		/// <summary>
		/// Creates a new business entity ChartHistoricalAnalysis and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartHistoricalAnalysis_FormCreate(ChartHistoricalAnalysis item);

		/// <summary>
		/// Loads a specific item for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartHistoricalAnalysis_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity ChartHistoricalAnalysis and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartHistoricalAnalysis_FormUpdate(ChartHistoricalAnalysis item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartHistoricalAnalysis_FormUpdateBatch(string[] keys, ChartHistoricalAnalysis item);

		/// <summary>
		/// Gets a list for the business entity ChartHistoricalAnalysis. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<ChartHistoricalAnalysis> ChartHistoricalAnalysis_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ChartHistoricalAnalysis ChartHistoricalAnalysis_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartHistoricalAnalysis> ChartHistoricalAnalysis_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of ChartHistoricalAnalysis for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartHistoricalAnalysis> ChartHistoricalAnalysis_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Saves a crud store for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartHistoricalAnalysis> ChartHistoricalAnalysis_SaveStore(CrudStore<ChartHistoricalAnalysis> store);

		/// <summary>
		/// Deletes an existing business entity ChartMarker.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool ChartMarker_Delete(ChartMarker item);

		/// <summary>
		/// Creates a new business entity ChartMarker and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartMarker_FormCreate(ChartMarker item);

		/// <summary>
		/// Loads a specific item for the business entity ChartMarker.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartMarker_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity ChartMarker and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartMarker_FormUpdate(ChartMarker item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartMarker_FormUpdateBatch(string[] keys, ChartMarker item);

		/// <summary>
		/// Gets a list for the business entity ChartMarker. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<ChartMarker> ChartMarker_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity ChartMarker.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ChartMarker ChartMarker_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity ChartMarker.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartMarker> ChartMarker_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store of ChartMarker for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartMarker> ChartMarker_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Saves a crud store for the business entity ChartMarker.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartMarker> ChartMarker_SaveStore(CrudStore<ChartMarker> store);

		/// <summary>
		/// gets the Chart model tree.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<TreeNode> ChartModel_GetMenuTree(string KeyChart);

		/// <summary>
		/// Deletes an existing business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool ChartPsetAttributeHistorical_Delete(ChartPsetAttributeHistorical item);

		/// <summary>
		/// Creates a new business entity ChartPsetAttributeHistorical and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartPsetAttributeHistorical_FormCreate(ChartPsetAttributeHistorical item);

		/// <summary>
		/// Loads a specific item for the business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartPsetAttributeHistorical_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity ChartPsetAttributeHistorical and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartPsetAttributeHistorical_FormUpdate(ChartPsetAttributeHistorical item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartPsetAttributeHistorical_FormUpdateBatch(string[] keys, ChartPsetAttributeHistorical item);

		/// <summary>
		/// Gets a list for the business entity ChartPsetAttributeHistorical. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<ChartPsetAttributeHistorical> ChartPsetAttributeHistorical_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ChartPsetAttributeHistorical ChartPsetAttributeHistorical_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartPsetAttributeHistorical> ChartPsetAttributeHistorical_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of ChartPsetAttributeHistorical for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartPsetAttributeHistorical> ChartPsetAttributeHistorical_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Saves a crud store for the business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartPsetAttributeHistorical> ChartPsetAttributeHistorical_SaveStore(CrudStore<ChartPsetAttributeHistorical> store);

		/// <summary>
		/// Deletes an existing business entity ChartScheduler.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool ChartScheduler_Delete(ChartScheduler item);

		/// <summary>
		/// Creates a new business entity ChartScheduler and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="charts">The charts.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartScheduler_FormCreate(ChartScheduler item, CrudStore<Chart> charts, CrudStore<PortalWindow> portalwindows);

		/// <summary>
		/// Loads a specific item for the business entity ChartScheduler.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartScheduler_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity ChartScheduler and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="charts">The charts.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartScheduler_FormUpdate(ChartScheduler item, CrudStore<Chart> charts, CrudStore<PortalWindow> portalwindows);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartScheduler_FormUpdateBatch(string[] keys, ChartScheduler item);

		/// <summary>
		/// Generates the Scheduled Reports and sends them via email.
		/// </summary>
		/// <param name="KeyChartScheduler">the ChartScheduler Key.</param>
		/// <returns></returns>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void ChartScheduler_Generate(string KeyChartScheduler);

		/// <summary>
		/// Generates the Scheduled Reports and sends them via email via a long running operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="KeyChartScheduler">The key chart scheduler.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void ChartScheduler_GenerateBegin(Guid operationId, string KeyChartScheduler);

		/// <summary>
		/// Gets a list for the business entity ChartScheduler. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<ChartScheduler> ChartScheduler_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity ChartScheduler.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ChartScheduler ChartScheduler_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity ChartScheduler.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartScheduler> ChartScheduler_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity ChartScheduler.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartScheduler> ChartScheduler_SaveStore(CrudStore<ChartScheduler> store);

		/// <summary>
		/// Get the tree of DataAcquisition instances and endpoints.
		/// </summary>
		/// <param name="entity">The parent entity.</param>
		/// <param name="entityContainer">The entity container.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<TreeNode> DataAcquisition_GetTree(DataAcquisitionItem entity, DataAcquisitionContainer entityContainer);

		/// <summary>
		/// Get the stream image of	a data acquisition endpoint.
		/// </summary>
		/// <param name="EndpointType">Type of the endpoint.</param>
		/// <param name="KeyEndpoint">The key endpoint.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ImageMap DataAcquisitionEndpoint_BuildImage(string EndpointType, string KeyEndpoint, int width, int height);

		/// <summary>
		/// Gets a json store for the business entity DataAcquisitionEndpoint.
		/// paging is done in memory because of the multiples brokers.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DataAcquisitionEndpoint> DataAcquisitionEndpoint_GetStore(PagingParameter paging);


		/// <summary>
		/// Gets ths store of datapoint from a chart.
		/// </summary>
		/// <param name="KeyChart">The KeyChart.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DataPoint> DataPoint_GetStoreFromChart(string KeyChart, PagingParameter paging);


		/// <summary>
		/// Deletes an existing business entity DataSerie.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool DataSerie_Delete(DataSerie item);

		/// <summary>
		/// Creates a new business entity DataSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DataSerie_FormCreate(DataSerie item);

		/// <summary>
		/// Loads a specific item for the business entity DataSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DataSerie_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity DataSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="ratios">The ratios.</param>
		/// <param name="colorelements">The colorelements.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DataSerie_FormUpdate(DataSerie item, CrudStore<DataSeriePsetRatio> ratios, CrudStore<DataSerieColorElement> colorelements);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DataSerie_FormUpdateBatch(string[] keys, DataSerie item);

		/// <summary>
		/// Gets a list for the business entity DataSerie. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<DataSerie> DataSerie_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity DataSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DataSerie DataSerie_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DataSerie> DataSerie_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store of dataserie for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DataSerie> DataSerie_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Saves a crud store for the business entity DataSerie.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DataSerie> DataSerie_SaveStore(CrudStore<DataSerie> store);

		/// <summary>
		/// Toggle the visibility of a specific dataserie (and creates it of its not instanciated yet).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyDataSerie">The key data serie.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="Visible">if set to <c>true</c> [visible].</param>
		/// <param name="serieName">Name of the serie as optional parameter. If not suplied it will default to the localId</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DataSerie DataSerie_ToggleVisibility(string KeyChart, string KeyDataSerie, string localId, bool Visible,string serieName = null);

		/// <summary>
		/// Toggle the visibility of a specific dataserie (and creates it of its not instanciated yet).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyDataSerie">The key data serie.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="type">The type.</param>
		/// <param name="serieName">Name of the serie as optional parameter. If not suplied it will default to the localId</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DataSerie DataSerie_TypeUpdate(string KeyChart, string KeyDataSerie, string localId, DataSerieType type,string serieName = null);

		/// <summary>
		/// Assign a secondary axis to a specific dataserie (and creates it of its not instanciated yet).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyDataSerie">The key data serie.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="serieName">Name of the serie as optional parameter. If not suplied it will default to the localId</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DataSerie DataSerie_UseSecondaryAxis(string KeyChart, string KeyDataSerie, string localId, string serieName = null);

		/// <summary>
		/// Deletes an existing business entity DataSerieColorElement.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool DataSerieColorElement_Delete(DataSerieColorElement item);

		/// <summary>
		/// Creates a new business entity DataSerieColorElement and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DataSerieColorElement_FormCreate(DataSerieColorElement item);

		/// <summary>
		/// Loads a specific item for the business entity DataSerieColorElement.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DataSerieColorElement_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity DataSerieColorElement and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DataSerieColorElement_FormUpdate(DataSerieColorElement item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DataSerieColorElement_FormUpdateBatch(string[] keys, DataSerieColorElement item);

		/// <summary>
		/// Gets a list for the business entity DataSerieColorElement. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<DataSerieColorElement> DataSerieColorElement_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity DataSerieColorElement.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DataSerieColorElement DataSerieColorElement_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity DataSerieColorElement.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DataSerieColorElement> DataSerieColorElement_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of DataSerieColorElement for a specific DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyDataSerie">The key of the dataserie.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DataSerieColorElement> DataSerieColorElement_GetStoreByKeyDataSerie(PagingParameter paging, string KeyDataSerie);

		/// <summary>
		/// Saves a crud store for the business entity DataSerieColorElement.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DataSerieColorElement> DataSerieColorElement_SaveStore(CrudStore<DataSerieColorElement> store);

		/// <summary>
		/// Deletes an existing business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool DataSeriePsetRatio_Delete(DataSeriePsetRatio item);

		/// <summary>
		/// Creates a new business entity DataSeriePsetRatio and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DataSeriePsetRatio_FormCreate(DataSeriePsetRatio item);

		/// <summary>
		/// Loads a specific item for the business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DataSeriePsetRatio_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity DataSeriePsetRatio and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DataSeriePsetRatio_FormUpdate(DataSeriePsetRatio item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DataSeriePsetRatio_FormUpdateBatch(string[] keys, DataSeriePsetRatio item);

		/// <summary>
		/// Gets a list for the business entity DataSeriePsetRatio. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<DataSeriePsetRatio> DataSeriePsetRatio_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DataSeriePsetRatio DataSeriePsetRatio_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DataSeriePsetRatio> DataSeriePsetRatio_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of DataSeriePsetRatio for a specific DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyDataSerie">The key of the dataserie.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DataSeriePsetRatio> DataSeriePsetRatio_GetStoreByKeyDataSerie(PagingParameter paging, string KeyDataSerie);

		/// <summary>
		/// Saves a crud store for the business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DataSeriePsetRatio> DataSeriePsetRatio_SaveStore(CrudStore<DataSeriePsetRatio> store);

		/// <summary>
		/// Deletes an existing business entity DrawingCanvas.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool DrawingCanvas_Delete(DrawingCanvas item);

		/// <summary>
		/// Creates a new business entity DrawingCanvas and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DrawingCanvas_FormCreate(DrawingCanvas item);

		/// <summary>
		/// Loads a specific item for the business entity DrawingCanvas.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DrawingCanvas_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity DrawingCanvas and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="charts">The charts.</param>
		/// <param name="images">The images.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DrawingCanvas_FormUpdate(DrawingCanvas item, CrudStore<DrawingCanvasChart> charts, CrudStore<DrawingCanvasImage> images);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DrawingCanvas_FormUpdateBatch(string[] keys, DrawingCanvas item);

		/// <summary>
		/// Gets a list for the business entity DrawingCanvas. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<DrawingCanvas> DrawingCanvas_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity DrawingCanvas.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DrawingCanvas DrawingCanvas_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DrawingCanvas> DrawingCanvas_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity DrawingCanvas.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DrawingCanvas> DrawingCanvas_SaveStore(CrudStore<DrawingCanvas> store);

		/// <summary>
		/// Deletes an existing business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool DrawingCanvasChart_Delete(DrawingCanvasChart item);

		/// <summary>
		/// Creates a new business entity DrawingCanvasChart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DrawingCanvasChart_FormCreate(DrawingCanvasChart item);

		/// <summary>
		/// Loads a specific item for the business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DrawingCanvasChart_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity DrawingCanvasChart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DrawingCanvasChart_FormUpdate(DrawingCanvasChart item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DrawingCanvasChart_FormUpdateBatch(string[] keys, DrawingCanvasChart item);

		/// <summary>
		/// Gets a list for the business entity DrawingCanvasChart. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<DrawingCanvasChart> DrawingCanvasChart_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DrawingCanvasChart DrawingCanvasChart_GetItem(string Key);

		/// <summary>
		/// Gets a list of DrawingCanvasChart for a specific DrawingCanvas.
		/// </summary>
		/// <param name="KeyDrawingCanvas">The key of the DrawingCanvas.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<DrawingCanvasChart> DrawingCanvasChart_GetListByKeyDrawingCanvas(string KeyDrawingCanvas);

		/// <summary>
		/// Gets a json store for the business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DrawingCanvasChart> DrawingCanvasChart_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of DrawingCanvasChart for a specific DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyDrawingCanvas">The key of the DrawingCanvas.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DrawingCanvasChart> DrawingCanvasChart_GetStoreByKeyDrawingCanvas(PagingParameter paging, string KeyDrawingCanvas);

		/// <summary>
		/// Saves a crud store for the business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DrawingCanvasChart> DrawingCanvasChart_SaveStore(CrudStore<DrawingCanvasChart> store);

		/// <summary>
		/// Deletes an existing business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool DrawingCanvasImage_Delete(DrawingCanvasImage item);

		/// <summary>
		/// Creates a new business entity DrawingCanvasImage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DrawingCanvasImage_FormCreate(DrawingCanvasImage item);

		/// <summary>
		/// Loads a specific item for the business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DrawingCanvasImage_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity DrawingCanvasImage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DrawingCanvasImage_FormUpdate(DrawingCanvasImage item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DrawingCanvasImage_FormUpdateBatch(string[] keys, DrawingCanvasImage item);

		/// <summary>
		/// Gets a list for the business entity DrawingCanvasImage. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<DrawingCanvasImage> DrawingCanvasImage_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DrawingCanvasImage DrawingCanvasImage_GetItem(string Key);

		/// <summary>
		/// Gets a list of DrawingCanvasImage for a specific DrawingCanvas.
		/// </summary>
		/// <param name="KeyDrawingCanvas">The key of the DrawingCanvas.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<DrawingCanvasImage> DrawingCanvasImage_GetListByKeyDrawingCanvas(string KeyDrawingCanvas);

		/// <summary>
		/// Gets a json store for the business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DrawingCanvasImage> DrawingCanvasImage_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of DrawingCanvasImage for a specific DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyDrawingCanvas">The key of the DrawingCanvas.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DrawingCanvasImage> DrawingCanvasImage_GetStoreByKeyDrawingCanvas(PagingParameter paging, string KeyDrawingCanvas);

		/// <summary>
		/// Saves a crud store for the business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DrawingCanvasImage> DrawingCanvasImage_SaveStore(CrudStore<DrawingCanvasImage> store);

		/// <summary>
		/// Deletes an existing business entity DynamicDisplay.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool DynamicDisplay_Delete(DynamicDisplay item);

		/// <summary>
		/// Creates a new business entity DynamicDisplay and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DynamicDisplay_FormCreate(DynamicDisplay item);

		/// <summary>
		/// Generate modern UI dynamic displays.
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool DynamicDisplay_GenerateModernUI();

		/// <summary>
		/// Loads a specific item for the business entity DynamicDisplay.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DynamicDisplay_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity DynamicDisplay and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DynamicDisplay_FormUpdate(DynamicDisplay item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DynamicDisplay_FormUpdateBatch(string[] keys, DynamicDisplay item);

		/// <summary>
		/// Gets a list for the business entity DynamicDisplay. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<DynamicDisplay> DynamicDisplay_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity DynamicDisplay.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DynamicDisplay DynamicDisplay_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity DynamicDisplay.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DynamicDisplay> DynamicDisplay_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Returns the stream of the image  from a dynamicdisplay.
		/// </summary>
		/// <param name="Key">The key of the dynamicdisplay.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="displayBogusChart">if set to <c>true</c> [display bogus chart].</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream DynamicDisplay_GetStreamImage(string Key, int width, int height, bool displayBogusChart = true);

		/// <summary>
		/// Saves a crud store for the business entity DynamicDisplay.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DynamicDisplay> DynamicDisplay_SaveStore(CrudStore<DynamicDisplay> store);

		/// <summary>
		/// Deletes an existing business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool DynamicDisplayColorTemplate_Delete(DynamicDisplayColorTemplate item);

		/// <summary>
		/// Creates a new business entity DynamicDisplayColorTemplate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DynamicDisplayColorTemplate_FormCreate(DynamicDisplayColorTemplate item);

		/// <summary>
		/// Loads a specific item for the business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DynamicDisplayColorTemplate_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity DynamicDisplayColorTemplate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DynamicDisplayColorTemplate_FormUpdate(DynamicDisplayColorTemplate item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DynamicDisplayColorTemplate_FormUpdateBatch(string[] keys, DynamicDisplayColorTemplate item);

		/// <summary>
		/// Gets a list for the business entity DynamicDisplayColorTemplate. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<DynamicDisplayColorTemplate> DynamicDisplayColorTemplate_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DynamicDisplayColorTemplate DynamicDisplayColorTemplate_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DynamicDisplayColorTemplate> DynamicDisplayColorTemplate_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DynamicDisplayColorTemplate> DynamicDisplayColorTemplate_SaveStore(CrudStore<DynamicDisplayColorTemplate> store);

		/// <summary>
		/// Deletes an existing business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool DynamicDisplayImage_Delete(DynamicDisplayImage item);

		/// <summary>
		/// Creates a new business entity DynamicDisplayImage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DynamicDisplayImage_FormCreate(DynamicDisplayImage item);

		/// <summary>
		/// Loads a specific item for the business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DynamicDisplayImage_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity DynamicDisplayImage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DynamicDisplayImage_FormUpdate(DynamicDisplayImage item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse DynamicDisplayImage_FormUpdateBatch(string[] keys, DynamicDisplayImage item);

		/// <summary>
		/// Gets a list for the business entity DynamicDisplayImage. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<DynamicDisplayImage> DynamicDisplayImage_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		DynamicDisplayImage DynamicDisplayImage_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="type">The type.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DynamicDisplayImage> DynamicDisplayImage_GetStore(PagingParameter paging, PagingLocation location, DynamicDisplayImageType type);

		/// <summary>
		/// Returns the stream of the image  from a dynamicdisplayimage.
		/// </summary>
		/// <param name="Key">The key of the dynamicdisplayimage.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream DynamicDisplayImage_GetStreamImage(string Key, int width, int height);

		/// <summary>
		/// Saves a crud store for the business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<DynamicDisplayImage> DynamicDisplayImage_SaveStore(CrudStore<DynamicDisplayImage> store);

		/// <summary>
		/// Copy existing business entity Chart.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy </param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EnergyCertificate_Copy(string[] keys);

		/// <summary>
		/// Deletes an existing business entity EnergyCertificate.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool EnergyCertificate_Delete(EnergyCertificate item);

		/// <summary>
		/// Creates a new business entity EnergyCertificate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="categories">The categories.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EnergyCertificate_FormCreate(EnergyCertificate item, CrudStore<EnergyCertificateCategory> categories);

		/// <summary>
		/// Loads a specific item for the business entity EnergyCertificate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EnergyCertificate_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity EnergyCertificate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="categories">The categories.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EnergyCertificate_FormUpdate(EnergyCertificate item, CrudStore<EnergyCertificateCategory> categories);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EnergyCertificate_FormUpdateBatch(string[] keys, EnergyCertificate item);

		/// <summary>
		/// Gets a list for the business entity EnergyCertificate. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<EnergyCertificate> EnergyCertificate_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity EnergyCertificate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		EnergyCertificate EnergyCertificate_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity EnergyCertificate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<EnergyCertificate> EnergyCertificate_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity EnergyCertificate.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<EnergyCertificate> EnergyCertificate_SaveStore(CrudStore<EnergyCertificate> store);

		/// <summary>
		/// Deletes an existing business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool EnergyCertificateCategory_Delete(EnergyCertificateCategory item);

		/// <summary>
		/// Creates a new business entity EnergyCertificateCategory and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EnergyCertificateCategory_FormCreate(EnergyCertificateCategory item);

		/// <summary>
		/// Loads a specific item for the business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EnergyCertificateCategory_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity EnergyCertificateCategory and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EnergyCertificateCategory_FormUpdate(EnergyCertificateCategory item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EnergyCertificateCategory_FormUpdateBatch(string[] keys, EnergyCertificateCategory item);

		/// <summary>
		/// Gets a list for the business entity EnergyCertificateCategory. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<EnergyCertificateCategory> EnergyCertificateCategory_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		EnergyCertificateCategory EnergyCertificateCategory_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<EnergyCertificateCategory> EnergyCertificateCategory_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of  EnergyCertificateCategory for a specific EnergyCertificate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyEnergyCertificate">The key energy certificate.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<EnergyCertificateCategory> EnergyCertificateCategory_GetStoreByKeyEnergyCertificate(PagingParameter paging, string KeyEnergyCertificate);

		/// <summary>
		/// Saves a crud store for the business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<EnergyCertificateCategory> EnergyCertificateCategory_SaveStore(CrudStore<EnergyCertificateCategory> store);

		/// <summary>
		/// Deletes an existing business entity EventLog.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool EventLog_Delete(EventLog item);

		/// <summary>
		/// Creates a new business entity EventLog and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EventLog_FormCreate(EventLog item);

		/// <summary>
		/// Loads a specific item for the business entity EventLog.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EventLog_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity EventLog and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EventLog_FormUpdate(EventLog item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EventLog_FormUpdateBatch(string[] keys, EventLog item);

		/// <summary>
		/// Gets a list for the business entity EventLog. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<EventLog> EventLog_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity EventLog.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		EventLog EventLog_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity EventLog.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<EventLog> EventLog_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity EventLog.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<EventLog> EventLog_SaveStore(CrudStore<EventLog> store);

		/// <summary>
		/// Deletes an existing business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool EWSDataAcquisitionContainer_Delete(EWSDataAcquisitionContainer item);

		/// <summary>
		/// Creates a new business entity EWSDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EWSDataAcquisitionContainer_FormCreate(EWSDataAcquisitionContainer item);

		/// <summary>
		/// Loads a specific item for the business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EWSDataAcquisitionContainer_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity EWSDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EWSDataAcquisitionContainer_FormUpdate(EWSDataAcquisitionContainer item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EWSDataAcquisitionContainer_FormUpdateBatch(string[] keys, EWSDataAcquisitionContainer item);

		/// <summary>
		/// Gets a list for the business entity EWSDataAcquisitionContainer. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<EWSDataAcquisitionContainer> EWSDataAcquisitionContainer_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		EWSDataAcquisitionContainer EWSDataAcquisitionContainer_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<EWSDataAcquisitionContainer> EWSDataAcquisitionContainer_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<EWSDataAcquisitionContainer> EWSDataAcquisitionContainer_SaveStore(CrudStore<EWSDataAcquisitionContainer> store);

		/// <summary>
		/// Deletes an existing business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool EWSDataAcquisitionEndpoint_Delete(EWSDataAcquisitionEndpoint item);

		/// <summary>
		/// Creates a new business entity EWSDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EWSDataAcquisitionEndpoint_FormCreate(EWSDataAcquisitionEndpoint item);

		/// <summary>
		/// Loads a specific item for the business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EWSDataAcquisitionEndpoint_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity EWSDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EWSDataAcquisitionEndpoint_FormUpdate(EWSDataAcquisitionEndpoint item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse EWSDataAcquisitionEndpoint_FormUpdateBatch(string[] keys, EWSDataAcquisitionEndpoint item);

		/// <summary>
		/// Gets a list for the business entity EWSDataAcquisitionEndpoint. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<EWSDataAcquisitionEndpoint> EWSDataAcquisitionEndpoint_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		EWSDataAcquisitionEndpoint EWSDataAcquisitionEndpoint_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<EWSDataAcquisitionEndpoint> EWSDataAcquisitionEndpoint_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of EWSDataAcquisitionEndpoint by key of the container.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<EWSDataAcquisitionEndpoint> EWSDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer(PagingParameter paging, string KeyDataAcquisitionContainer);

		/// <summary>
		/// Saves a crud store for the business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<EWSDataAcquisitionEndpoint> EWSDataAcquisitionEndpoint_SaveStore(CrudStore<EWSDataAcquisitionEndpoint> store);

		/// <summary>
		/// Gets a json store of the Filter AlarmDefinition Classification for a specific KeyAlarmTable.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyAlarmTable">The key KeyAlarmTable.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ClassificationItem> FilterAlarmDefinitionClassification_GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable);

		/// <summary>
		/// Gets a json store of the Filter AlarmDefinition Classification for a specific Chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ClassificationItem> FilterAlarmDefinitionClassification_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Gets a json store of the Filter EventLog Classification for a specific chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ClassificationItem> FilterEventLogClassification_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Gets a json store of the Filter Meter Classification for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyAlarmDefinition">The key AlarmDefinition.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ClassificationItem> FilterMeterClassification_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition);

		/// <summary>
		/// Gets a json store of the Filter Meter Classification for a specific chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ClassificationItem> FilterMeterClassification_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Gets a json store of the Filter Meter Classification for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyMeterValidationRule">The key MeterValidationRule.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ClassificationItem> FilterMeterClassification_GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule);

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyAlarmDefinition">The key AlarmDefinition.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Location> FilterSpatial_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition);

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyAlarmTable">The key alarm table.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Location> FilterSpatial_GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable);

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Location> FilterSpatial_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyMeterValidationRule">The key MeterValidationRule.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Location> FilterSpatial_GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule);

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyPortalTemplate">The key PortalTemplate.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Location> FilterSpatial_GetStoreByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate);

		/// <summary>
		/// Deletes an existing business entity FilterSpatialPset.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool FilterSpatialPset_Delete(FilterSpatialPset item);

		/// <summary>
		/// Creates a new business entity FilterSpatialPset and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse FilterSpatialPset_FormCreate(FilterSpatialPset item);

		/// <summary>
		/// Loads a specific item for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse FilterSpatialPset_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity FilterSpatialPset and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse FilterSpatialPset_FormUpdate(FilterSpatialPset item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse FilterSpatialPset_FormUpdateBatch(string[] keys, FilterSpatialPset item);

		/// <summary>
		/// Gets a list for the business entity FilterSpatialPset. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<FilterSpatialPset> FilterSpatialPset_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FilterSpatialPset FilterSpatialPset_GetItem(string Key);

		/// <summary>
		/// Gets a store of Location for a specific AlarmDefinition based on the FilterSpatialPset attached to it.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmDefinition">The key of the AlarmDefinition.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Location> FilterSpatialPset_GetLocationStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition);

		/// <summary>
		/// Gets a store of Location for a specific chart based on the FilterSpatialPset attached to it.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Location> FilterSpatialPset_GetLocationStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Gets a json store for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<FilterSpatialPset> FilterSpatialPset_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of FilterSpatialPset for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmDefinition">The key of the AlarmDefinition.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<FilterSpatialPset> FilterSpatialPset_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition);

		/// <summary>
		/// Gets a store of FilterSpatialPset for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<FilterSpatialPset> FilterSpatialPset_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Saves a crud store for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<FilterSpatialPset> FilterSpatialPset_SaveStore(CrudStore<FilterSpatialPset> store);

		/// <summary>
		/// Gets a json store for the business entity FOLMembershipUser for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalTemplate">The Key PortalTemplate.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<FOLMembershipUser> FOLMembershipUser_GetStoreByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate);

		/// <summary>
		/// Initializes the energy aggregator service agent.
		/// </summary>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void InitializeEnergyAggregatorServiceAgent();

		/// <summary>
		/// Deletes an existing business entity MachineInstance.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool MachineInstance_Delete(MachineInstance item);

		/// <summary>
		/// Create a new Machine Instance
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MachineInstance_FormCreate(MachineInstance item);

		/// <summary>
		/// Gets a json store for the business entity MachineInstance.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MachineInstance> MachineInstance_GetStore(PagingParameter paging);

		/// <summary>
		/// Build the Chart of the Machines memory usage.
		/// </summary>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ImageMap MachineInstance_MemoryUsageBuildImage(int width, int height);

		/// <summary>
		///Get stream image of Machines memory usage from cache.
		/// </summary>
		/// <param name="guid">The GUID.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream MachineInstance_MemoryUsageGetStreamImageFromCache(string guid);

		/// <summary>
		/// Restart the passed machine instance.
		/// </summary>
		/// <param name="item">The item.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void MachineInstance_Restart(MachineInstance item);

		/// <summary>
		/// Returns if the MachineInstance service is enabled or not.
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool MachineInstance_ServiceIsEnabled();

		/// <summary>
		/// Deletes an existing business entity Map.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool Map_Delete(Map item);

		/// <summary>
		/// Creates a new business entity Map and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Map_FormCreate(Map item);

		/// <summary>
		/// Loads a specific item for the business entity Map.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Map_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Map and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Map_FormUpdate(Map item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Map_FormUpdateBatch(string[] keys, Map item);

		/// <summary>
		/// Gets a list for the business entity Map. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Map> Map_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Map.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Map Map_GetItem(string Key);

		/// <summary>
		///Return the Map Key to be used when instanciating a new Key client side.
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		string Map_GetKey();

		/// <summary>
		/// Get the Map Pushpins.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<MapPushpin> Map_GetPushpins(string Key);

		/// <summary>
		/// Gets a json store for the business entity Map.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Map> Map_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Map.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Map> Map_SaveStore(CrudStore<Map> store);

		/// <summary>
		/// Builds the image (with map) from a single meter.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ImageMap Meter_BuildImage(string Key, int width, int height);

		/// <summary>
		/// Deletes an existing business entity Meter.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool Meter_Delete(Meter item);

		/// <summary>
		/// Creates a new business entity Meter and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meterOperationsWizard">The meter operations wizard.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Meter_FormCreate(Meter item, CrudStore<Meter> meterOperationsWizard);

		/// <summary>
		/// Loads a specific item for the business entity Meter.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Meter_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Meter and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meterOperations">The meter operations.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Meter_FormUpdate(Meter item, CrudStore<MeterOperation> meterOperations);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Meter_FormUpdateBatch(string[] keys, Meter item);

		/// <summary>
		/// Gets a list for the business entity Meter. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Meter> Meter_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Meter.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Meter Meter_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity Meter.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Meter> Meter_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store of meters for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Meter> Meter_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition);

		/// <summary>
		/// Gets a json store of meters for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The Key of the chart.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Meter> Meter_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Gets a json store of meters for a specific meter data export task.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMeterDataExportTask">The key meter data export task.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Meter> Meter_GetStoreByKeyMeterDataExportTask(PagingParameter paging, string KeyMeterDataExportTask);

		/// <summary>
		/// Gets a store of meters for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMeterValidationRule">The key of the MeterValidationRule.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Meter> Meter_GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule);

		/// <summary>
		/// Gets a store of meters for a specific location and classification item.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyLocation">The key of the Location.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Meter> Meter_GetStoreByLocationAndClassification(PagingParameter paging, string KeyLocation, string KeyClassificationItem);

		/// <summary>
		/// Save the current end point value of a meter.
		/// </summary>
		/// <param name="KeyMeter">The key meter.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool Meter_SaveEndpointValue(string KeyMeter);

		/// <summary>
		/// Saves a crud store for the business entity Meter.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Meter> Meter_SaveStore(CrudStore<Meter> store);

		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview for the Meter Classification filtered by Chart or AlarmDefinition.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The parent entity.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <param name="existing">if set to <c>true</c>
		/// the method will only return MeterClassifcation for meter that are attached to the chart  or AlarmDefinition,
		/// else it will return all available MeterClassification in the spatial filter of the chart or AlarmDefinition.</param>
		/// <param name="filterSpatial">The current filter spatial of the Chart or AlarmDefinition.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<TreeNode> MeterClassification_GetTree(string Key, ClassificationItem entity, string KeyChart, string KeyAlarmDefinition, string KeyMeterValidationRule, bool existing, CrudStore<Location> filterSpatial);

		/// <summary>
		/// Deletes an existing business entity MeterData.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool MeterData_Delete(MeterData item);

		/// <summary>
		/// Delete MeterData by Date Range (long running start point).
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="KeyMeter">The key of the meter.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void MeterData_DeleteByDateRangeBegin(Guid operationId, string KeyMeter, DateTime StartDate, DateTime EndDate);

		/// <summary>
		/// Creates a new business entity MeterData and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterData_FormCreate(MeterData item);

		/// <summary>
		/// Loads a specific item for the business entity MeterData.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterData_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity MeterData and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterData_FormUpdate(MeterData item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterData_FormUpdateBatch(string[] keys, MeterData item);

		/// <summary>
		/// Generate Random MeterData.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="KeyMeter">The key of the meter.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="FrequencyNumber">The frequency number.</param>
		/// <param name="Frequency">The frequency.</param>
		/// <param name="Minimum">The minimum.</param>
		/// <param name="Maximum">The maximum.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void MeterData_GenerateBegin(Guid operationId, string KeyMeter, DateTime StartDate, DateTime EndDate, int FrequencyNumber, AxisTimeInterval Frequency, double Minimum, double Maximum);


		/// <summary>
		/// Export all data for a list of Meters
		/// </summary>
		/// <param name="localIds">The meter local ids.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="zip">if set to <c>true</c> [zip].</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream MeterData_GetCSVExport(List<string> localIds, DateTime? startDate, DateTime? endDate, bool zip);

		/// <summary>
		/// Gets a specific item for the business entity MeterData.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		MeterData MeterData_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity MeterData.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MeterData> MeterData_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store for the business entity MeterData.
		/// </summary>
		/// <param name="KeyMeter">The Key of the parent Meter.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="timeZoneId">The time zone id.</param>
		/// <param name="rawData">if set to <c>true</c> [raw data].</param>
		/// <param name="datetimeFormat">The datetime format.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MeterData> MeterData_GetStoreFromMeter(string KeyMeter, PagingParameter paging, PagingLocation location, string timeZoneId, bool rawData, string datetimeFormat);

		/// <summary>
		/// Offset MeterData by Date Range (long running start point) (md.Value * Factor - Offset).
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="KeyMeter">The key of the meter.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="Factor">The factor.</param>
		/// <param name="Offset">The offset.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void MeterData_OffsetByDateRangeBegin(Guid operationId, string KeyMeter, DateTime StartDate, DateTime EndDate, double Factor, double Offset);

		/// <summary>
		/// Saves a crud store for the business entity MeterData.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MeterData> MeterData_SaveStore(CrudStore<MeterData> store);

		/// <summary>
		/// Validate a new MeterData (used in the MeterData form to validate a meter data when inputted).
		/// </summary>
		/// <param name="item">The Meter Data.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<AlarmInstance> MeterData_Validate(MeterData item);

		/// <summary>
		/// Deletes an existing business entity MeterOperation.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool MeterOperation_Delete(MeterOperation item);

		/// <summary>
		/// Creates a new business entity MeterOperation and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterOperation_FormCreate(MeterOperation item);

		/// <summary>
		/// Loads a specific item for the business entity MeterOperation.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterOperation_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity MeterOperation and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterOperation_FormUpdate(MeterOperation item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterOperation_FormUpdateBatch(string[] keys, MeterOperation item);

		/// <summary>
		/// Gets a list for the business entity MeterOperation. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<MeterOperation> MeterOperation_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity MeterOperation.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		MeterOperation MeterOperation_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity MeterOperation.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MeterOperation> MeterOperation_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of MeterOperations for a specific Meter.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMeter">The key of the Meter.</param>
		/// <param name="KeyMeterOperation">The key meter operation to exclude.</param>
		/// <param name="order">The order if the current meter operation in order to filter available results. 0 to disable filtering.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MeterOperation> MeterOperation_GetStoreByKeyMeter(PagingParameter paging, string KeyMeter, string KeyMeterOperation, int order);

		/// <summary>
		/// Saves a crud store for the business entity MeterOperation.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MeterOperation> MeterOperation_SaveStore(CrudStore<MeterOperation> store);

		/// <summary>
		/// Deletes an existing business entity MeterValidationRule.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool MeterValidationRule_Delete(MeterValidationRule item);

		/// <summary>
		/// Creates a new business entity MeterValidationRule and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterValidationRule_FormCreate(MeterValidationRule item, CrudStore<Meter> meters, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification);

		/// <summary>
		/// Loads a specific item for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterValidationRule_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity MeterValidationRule and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterValidationRule_FormUpdate(MeterValidationRule item, CrudStore<Meter> meters, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse MeterValidationRule_FormUpdateBatch(string[] keys, MeterValidationRule item);

		/// <summary>
		/// Gets a list for the business entity MeterValidationRule. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<MeterValidationRule> MeterValidationRule_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		MeterValidationRule MeterValidationRule_GetItem(string Key);

		/// <summary>
		/// Return a json store of the complete list of Meters associated with a MeterValidationRule.
		/// including meters associated via SpatialFilter cross SpatialFilterPset cross MeterClassification
		/// </summary>
		/// <param name="KeyMeterValidationRule">The key of the MeterValidationRule.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Meter> MeterValidationRule_GetMetersStore(string KeyMeterValidationRule, PagingParameter paging);

		/// <summary>
		/// Gets a json store for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MeterValidationRule> MeterValidationRule_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets all the enabled MeterValidationRule and process them to generate the AlarmInstances
		/// </summary>
		/// <param name="Keys">The keys.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void MeterValidationRule_Process(string[] Keys);

		/// <summary>
		/// Gets all the enabled MeterValidationRule and process them to generate the AlarmInstances with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void MeterValidationRule_ProcessBegin(Guid operationId, string[] Keys);

		/// <summary>
		/// Saves a crud store for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<MeterValidationRule> MeterValidationRule_SaveStore(CrudStore<MeterValidationRule> store);


		/// <summary>
		/// Copy the specified palettes and colors..
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Palette_Copy(string[] keys);


		/// <summary>
		/// Deletes an existing business entity Palette.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool Palette_Delete(Palette item);

		/// <summary>
		/// Creates a new business entity Palette and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="palettecolor">The palettecolor crudstore.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Palette_FormCreate(Palette item, CrudStore<PaletteColor> palettecolor);

		/// <summary>
		/// Loads a specific item for the business entity Palette.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Palette_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Palette and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="palettecolor">The palettecolor crudstore.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Palette_FormUpdate(Palette item, CrudStore<PaletteColor> palettecolor);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Palette_FormUpdateBatch(string[] keys, Palette item);

		/// <summary>
		/// Gets a list for the business entity Palette. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Palette> Palette_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Palette.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Palette Palette_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity Palette.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Palette> Palette_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Returns the stream of the image that represent a palette.
		/// </summary>
		/// <param name="Key">The key of the Palette.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream Palette_GetStreamImage(string Key, int height, int width);

		/// <summary>
		/// Saves a crud store for the business entity Palette.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Palette> Palette_SaveStore(CrudStore<Palette> store);

		/// <summary>
		/// Deletes an existing business entity PaletteColor.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool PaletteColor_Delete(PaletteColor item);

		/// <summary>
		/// Creates a new business entity PaletteColor and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PaletteColor_FormCreate(PaletteColor item);

		/// <summary>
		/// Loads a specific item for the business entity PaletteColor.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PaletteColor_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity PaletteColor and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PaletteColor_FormUpdate(PaletteColor item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PaletteColor_FormUpdateBatch(string[] keys, PaletteColor item);

		/// <summary>
		/// Gets a list for the business entity PaletteColor. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<PaletteColor> PaletteColor_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity PaletteColor.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		PaletteColor PaletteColor_GetItem(string Key);

		/// <summary>
		/// Gets a list of PaletteColor for a specific Palette.
		/// </summary>
		/// <param name="KeyPalette">The key of the Palette.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<PaletteColor> PaletteColor_GetListByKeyPalette(string KeyPalette);

		/// <summary>
		/// Gets a json store for the business entity PaletteColor.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PaletteColor> PaletteColor_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of PaletteColor for a specific Palette.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPalette">The key of the Palette.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PaletteColor> PaletteColor_GetStoreByKeyPalette(PagingParameter paging, string KeyPalette);

		/// <summary>
		/// Saves a crud store for the business entity PaletteColor.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PaletteColor> PaletteColor_SaveStore(CrudStore<PaletteColor> store);

		/// <summary>
		/// Deletes an existing business entity Playlist.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool Playlist_Delete(Playlist item);

		/// <summary>
		/// Creates a new business entity Playlist and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="pages">The pages.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Playlist_FormCreate(Playlist item, CrudStore<PlaylistPage> pages);

		/// <summary>
		/// Loads a specific item for the business entity Playlist.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Playlist_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Playlist and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="pages">The pages.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Playlist_FormUpdate(Playlist item, CrudStore<PlaylistPage> pages);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Playlist_FormUpdateBatch(string[] keys, Playlist item);


		/// <summary>
		/// Generates the differents playlist as images.
		/// </summary>
		/// <param name="Keys">The keys.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void Playlist_Generate(string[] Keys);

		/// <summary>
		/// Generates the differents playlist as images. with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void Playlist_GenerateBegin(Guid operationId, string[] Keys);

		/// <summary>
		/// Gets a list for the business entity Playlist. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Playlist> Playlist_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Playlist.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Playlist Playlist_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity Playlist.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Playlist> Playlist_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets the Playlist store by the job step.
		/// </summary>
		/// <param name="keyJob">The key job.</param>
		/// <param name="keyStep">The key step.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Playlist> Playlist_GetStoreByJobStep(string keyJob, string keyStep, PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Playlist.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Playlist> Playlist_SaveStore(CrudStore<Playlist> store);

		/// <summary>
		/// Deletes an existing business entity PlaylistPage.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool PlaylistPage_Delete(PlaylistPage item);

		/// <summary>
		/// Creates a new business entity PlaylistPage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PlaylistPage_FormCreate(PlaylistPage item);

		/// <summary>
		/// Loads a specific item for the business entity PlaylistPage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PlaylistPage_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity PlaylistPage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PlaylistPage_FormUpdate(PlaylistPage item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PlaylistPage_FormUpdateBatch(string[] keys, PlaylistPage item);

		/// <summary>
		/// Gets a list for the business entity PlaylistPage. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<PlaylistPage> PlaylistPage_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity PlaylistPage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		PlaylistPage PlaylistPage_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity PlaylistPage.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PlaylistPage> PlaylistPage_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of  PlaylistPage for a specific Playlist.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPlaylist">The key of the Playlist.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PlaylistPage> PlaylistPage_GetStoreByKeyPlaylist(PagingParameter paging, string KeyPlaylist);

		/// <summary>
		/// Returns the stream of the image from a playlist page (corresponding to the latest generation of the playlist).
		/// </summary>
		/// <param name="Key">The key of the playlist page.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream PlaylistPage_GetStreamImage(string Key);

		/// <summary>
		/// Saves a crud store for the business entity PlaylistPage.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PlaylistPage> PlaylistPage_SaveStore(CrudStore<PlaylistPage> store);

		/// <summary>
		/// Deletes an existing business entity PortalColumn.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="deleteContent">if set to <c>true</c> [delete content].</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool PortalColumn_Delete(PortalColumn item, bool deleteContent = false);

		/// <summary>
		/// Creates a new business entity PortalColumn and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="portlet">the portlet crud store attached to this portal column.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalColumn_FormCreate(PortalColumn item, CrudStore<Portlet> portlet);

		/// <summary>
		/// Loads a specific item for the business entity PortalColumn.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalColumn_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity PortalColumn and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="portlet">the portlet crud store attached to this portal column.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalColumn_FormUpdate(PortalColumn item, CrudStore<Portlet> portlet);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalColumn_FormUpdateBatch(string[] keys, PortalColumn item);

		/// <summary>
		/// Gets a list for the business entity PortalColumn. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<PortalColumn> PortalColumn_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity PortalColumn.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		PortalColumn PortalColumn_GetItem(string Key);

		/// <summary>
		/// Gets a list of portalcolumn for a specific portal tab.
		/// </summary>
		/// <param name="KeyPortalTab">The key of the portal tab.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<PortalColumn> PortalColumn_GetListByKeyPortalTab(string KeyPortalTab);

		/// <summary>
		/// Gets a json store for the business entity PortalColumn.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalColumn> PortalColumn_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store of portalcolumn for a specific portal tab.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalTab">The Key of the portal.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalColumn> PortalColumn_GetStoreByKeyPortalTab(PagingParameter paging, string KeyPortalTab);

		/// <summary>
		/// Saves a crud store for the business entity PortalColumn.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalColumn> PortalColumn_SaveStore(CrudStore<PortalColumn> store);

		/// <summary>
		/// Apply the filter Spatial to all the Charts contained in the portal tab.
		/// </summary>
		/// <param name="Key">The portaltab Key.</param>
		/// <param name="filterSpatial">The spatial filter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalTab_ApplySpatialFilter(string Key, List<BaseBusinessEntity> filterSpatial);

		/// <summary>
		/// Apply the filter Spatial to all the Charts contained in all tabs of a portal window.
		/// </summary>
		/// <param name="Key">The portaltab Key.</param>
		/// <param name="filterSpatial">The spatial filter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalWindow_ApplySpatialFilter(string Key, List<BaseBusinessEntity> filterSpatial);

		/// <summary>
		/// Copy existing business entity PortalTab and it s content.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy </param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalTab_Copy(string[] keys);

		/// <summary>
		/// Deletes an existing business entity PortalTab.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="deleteContent">if set to <c>true</c> [delete content].</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool PortalTab_Delete(PortalTab item, bool deleteContent = false);

		/// <summary>
		/// Creates a new business entity PortalTab and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="portalcolumn">the portalcolumn crud store attached to this portal tab.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalTab_FormCreate(PortalTab item, CrudStore<PortalColumn> portalcolumn);

		/// <summary>
		/// Loads a specific item for the business entity PortalTab.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalTab_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity PortalTab and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="portalcolumn">the portalcolumn crud store attached to this portal tab.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalTab_FormUpdate(PortalTab item, CrudStore<PortalColumn> portalcolumn);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalTab_FormUpdateBatch(string[] keys, PortalTab item);

		/// <summary>
		/// Gets a list for the business entity PortalTab. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<PortalTab> PortalTab_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Get the PortalTab total flex based on the columns type (Main,Header, Footer).
		/// </summary>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <param name="type">The type of column.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		int PortalTab_GetFlex(string KeyPortalTab, PortalColumnType type);

		/// <summary>
		/// Gets a specific item for the business entity PortalTab.
		/// </summary>
		/// <param name="fields">The entity Key.</param>
		/// <param name="Key">The fields.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		PortalTab PortalTab_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets a list of portaltab for a specific portal.
		/// </summary>
		/// <param name="KeyPortalWindow">The key of the portal window.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<PortalTab> PortalTab_GetListByKeyPortalWindow(string KeyPortalWindow);

		/// <summary>
		/// Gets a json store for the business entity PortalTab.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalTab> PortalTab_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store of portaltab for a specific portal.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalWindow">The Key of the portal window.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalTab> PortalTab_GetStoreByKeyPortalWindow(PagingParameter paging, string KeyPortalWindow);

		/// <summary>
		/// Returns the stream of the excel file from a portaltab.
		/// </summary>
		/// <param name="Key">The key of the PortalTab.</param>
		/// <returns></returns>
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream PortalTab_GetStreamExcel(string Key);

		/// <summary>
		/// Exports the PortalTab as an Excel File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the Chart.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void PortalTab_GetStreamExcelBegin(Guid operationId, string Key);

		/// <summary>
		/// Returns the stream of the image  from a portaltab.
		/// </summary>
		/// <param name="Key">The key of the portaltab.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream PortalTab_GetStreamImage(string Key, int width, int height);

		/// <summary>
		/// Returns the stream of the pdf file from a portaltab.
		/// </summary>
		/// <param name="Key">The key of the PortalTab.</param>
		/// <returns></returns>
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream PortalTab_GetStreamPdf(string Key);

		/// <summary>
		/// Exports the PortalTab as an Pdf File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the Chart.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void PortalTab_GetStreamPdfBegin(Guid operationId, string Key);

		/// <summary>
		/// Saves a crud store for the business entity PortalTab.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalTab> PortalTab_SaveStore(CrudStore<PortalTab> store);

		/// <summary>
		/// Deletes an existing business entity PortalTemplate.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool PortalTemplate_Delete(PortalTemplate item);

		/// <summary>
		/// Creates a new business entity PortalTemplate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <param name="users">The users.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalTemplate_FormCreate(PortalTemplate item, CrudStore<PortalWindow> portalwindows, CrudStore<FOLMembershipUser> users, CrudStore<Location> filterSpatial);

		/// <summary>
		/// Loads a specific item for the business entity PortalTemplate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalTemplate_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity PortalTemplate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <param name="users">The users.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalTemplate_FormUpdate(PortalTemplate item, CrudStore<PortalWindow> portalwindows, CrudStore<FOLMembershipUser> users, CrudStore<Location> filterSpatial);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalTemplate_FormUpdateBatch(string[] keys, PortalTemplate item);

		/// <summary>
		/// Gets a list for the business entity PortalTemplate. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<PortalTemplate> PortalTemplate_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity PortalTemplate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		PortalTemplate PortalTemplate_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalTemplate> PortalTemplate_GetStore(PagingParameter paging, PagingLocation location);


		/// <summary>
		/// Gets the PortalTemplate store by the job step.
		/// </summary>
		/// <param name="keyJob">The key job.</param>
		/// <param name="keyStep">The key step.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalTemplate> PortalTemplate_GetStoreByJobStep(string keyJob, string keyStep, PagingParameter paging, PagingLocation location);


		/// <summary>
		/// Push the modification made to the associated portalwindows to the associated users of the PortalTemplate.
		/// </summary>
		/// <param name="KeyPortalTemplates">The key portal templates.</param>
        /// <param name="userKeys">The user keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool PortalTemplate_PushUpdates(string[] KeyPortalTemplates, string[] userKeys = null);

		/// <summary>
		///  Push the modification made to the associated portalwindows to the associated users of the PortalTemplaten with long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
        /// <param name="userKeys">The user keys.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void PortalTemplate_PushUpdatesBegin(Guid operationId, string[] Keys, string[] userKeys = null);


		/// <summary>
		///  Delete a portaltemplate and the portalwindows to the associated users with long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void PortalTemplate_DeleteBegin(Guid operationId, string[] Keys);


		/// <summary>
		/// Saves a crud store for the business entity PortalTemplate.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalTemplate> PortalTemplate_SaveStore(CrudStore<PortalTemplate> store);

        /// <summary>
        /// Gets a list of new user keys by key portal template.
        /// </summary>
        /// <param name="keyPortalTemplate">The keyPortalTemplate.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        List<string> FOLMembershipUser_GetNewUserKeysByKeyPortalTemplate(int keyPortalTemplate);

		/// <summary>
		/// Copy existing business entity PortalWindow.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy </param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalWindow_Copy(string[] keys);

		/// <summary>
		/// Deletes an existing business entity Portal.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="deleteContent">if set to <c>true</c> [delete content].</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool PortalWindow_Delete(PortalWindow item, bool deleteContent = false);

		/// <summary>
		/// Creates a new business entity Portal and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="portaltab">the portaltab crud store attached to this portal.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalWindow_FormCreate(PortalWindow item, CrudStore<PortalTab> portaltab);

		/// <summary>
		/// Loads a specific item for the business entity Portal.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalWindow_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Portal and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="portaltab">the portaltab crud store attached to this portal.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalWindow_FormUpdate(PortalWindow item, CrudStore<PortalTab> portaltab);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalWindow_FormUpdateBatch(string[] keys, PortalWindow item);

		/// <summary>
		/// Gets a list for the business entity Portal. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<PortalWindow> PortalWindow_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Portal.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		PortalWindow PortalWindow_GetItem(string Key);

		/// <summary>
		/// Portals the window_ get portal.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		PortalWindow PortalWindow_GetPortal(string Key, params string[] fields);

		/// <summary>
		/// Gets a json store for the business entity Portal.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalWindow> PortalWindow_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store for the business entity PortalWindow_ for a specific ChartScheduler.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChartScheduler">The key chart scheduler.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalWindow> PortalWindow_GetStoreByKeyChartScheduler(PagingParameter paging, string KeyChartScheduler);

		/// <summary>
		/// Gets a json store for the business entity PortalWindow for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalTemplate">The Key PortalTemplate.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalWindow> PortalWindow_GetStoreByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate);

		/// <summary>
		/// Returns the stream of the excel file from a portalWindow.
		/// </summary>
		/// <param name="Key">The key of the PortalWindow.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream PortalWindow_GetStreamExcel(string Key);

		/// <summary>
		/// Exports the PortalWindow as an Excel File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the PortalWindow.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void PortalWindow_GetStreamExcelBegin(Guid operationId, string Key);

		/// <summary>
		/// Returns the stream of the pdf file from a portalWindow.
		/// </summary>
		/// <param name="Key">The key of the PortalWindow.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream PortalWindow_GetStreamPdf(string Key);

		/// <summary>
		/// Exports the PortalWindow as an Pdf File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the PortalWindow.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void PortalWindow_GetStreamPdfBegin(Guid operationId, string Key);

		/// <summary>
		/// Saves a crud store for the business entity Portal.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PortalWindow> PortalWindow_SaveStore(CrudStore<PortalWindow> store);

		/// <summary>
		/// Update the PortalWindow desktop shortcut position.
		/// </summary>
		/// <param name="KeyPortalWindow">The PortalWindow key.</param>
		/// <param name="desktopShortcutIconX">The desktop shortcut icon X.</param>
		/// <param name="desktopShortcutIconY">The desktop shortcut icon Y.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PortalWindow_UpdateDesktopShortcut(string KeyPortalWindow, int desktopShortcutIconX, int desktopShortcutIconY);

		/// <summary>
		/// Copy an existing Portlet.
		/// </summary>
		/// <param name="keys">The item to copy.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Portlet_Copy(string[] keys);

		/// <summary>
		/// Create a Portlet from a ClassificationItem (used in the drag drop of a ClassificationItem in a portal column).
		/// </summary>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <param name="KeyPortalColumn">The key portal column.</param>
		/// <param name="Order">The order.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="KeyChartToCopy">The key chart to copy.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Portlet Portlet_CreateFromClassification(string KeyClassificationItem, string KeyPortalColumn, int Order, string TimeZoneId, string KeyChartToCopy);

		/// <summary>
		/// Create a Portlet from a Location (used in the drag drop of a Location in a portal column).
		/// </summary>
		/// <param name="KeyLocation">The key location.</param>
		/// <param name="KeyPortalColumn">The key portal column.</param>
		/// <param name="Order">The order.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="KeyChartToCopy">The key chart to copy.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Portlet Portlet_CreateFromLocation(string KeyLocation, string KeyPortalColumn, int Order, string TimeZoneId, string KeyChartToCopy);

		/// <summary>
		/// Create a Portlet from a Meter (used in the drag drop of a meter in a portal column).
		/// </summary>
		/// <param name="KeyMeter">the Key Meter.</param>
		/// <param name="KeyPortalColumn">The key portal column.</param>
		/// <param name="Order">The order.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="KeyChartToCopy">The key chart to copy.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Portlet Portlet_CreateFromMeter(string KeyMeter, string KeyPortalColumn, int Order, string TimeZoneId, string KeyChartToCopy);

		/// <summary>
		/// Deletes an existing business entity Portlet.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="deleteContent">if set to <c>true</c> [delete content].</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool Portlet_Delete(Portlet item, bool deleteContent = false);

		/// <summary>
		/// Creates a new business entity Portlet and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Portlet_FormCreate(Portlet item);

		/// <summary>
		/// Loads a specific item for the business entity Portlet.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Portlet_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Portlet and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Portlet_FormUpdate(Portlet item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Portlet_FormUpdateBatch(string[] keys, Portlet item);

		/// <summary>
		/// Gets a list for the business entity Portlet. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Portlet> Portlet_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		///Gets all the open on start up portets.
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<BaseBusinessEntity> Portlet_GetAllOpenOnStartup();

		/// <summary>
		/// Gets a specific item for the business entity Portlet.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Portlet Portlet_GetItem(string Key);

		/// <summary>
		/// Gets a list of portlet for a specific portal column.
		/// </summary>
		/// <param name="KeyPortalColumn">The key of the portal column.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Portlet> Portlet_GetListByKeyPortalColumn(string KeyPortalColumn);

		/// <summary>
		/// Gets a json store for the business entity Portlet.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Portlet> Portlet_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of portlet for a specific entity.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyEntity">The key entity.</param>
		/// <param name="TypeEntity">The type entity.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Portlet> Portlet_GetStoreByKeyEntity(PagingParameter paging, string KeyEntity, string TypeEntity);

		/// <summary>
		/// Gets a json store of portlet for a specific portal column.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalColumn">The Key of the portal column.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Portlet> Portlet_GetStoreByKeyPortalColumn(PagingParameter paging, string KeyPortalColumn);

		/// <summary>
		/// Generates the json structure of the Energy available portlets.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The entity.</param>
		/// <returns>
		/// a list of treenodes in json.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<TreeNode> Portlet_GetTree(string Key, BaseBusinessEntity entity);

		/// <summary>
		/// Saves a crud store for the business entity Portlet.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Portlet> Portlet_SaveStore(CrudStore<Portlet> store);

		/// <summary>
		/// Deletes an existing business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool PsetAttributeHistoricalEndpoint_Delete(PsetAttributeHistoricalEndpoint item);

		/// <summary>
		/// Creates a new business entity PsetAttributeHistoricalEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PsetAttributeHistoricalEndpoint_FormCreate(PsetAttributeHistoricalEndpoint item);

		/// <summary>
		/// Loads a specific item for the business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PsetAttributeHistoricalEndpoint_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity PsetAttributeHistoricalEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PsetAttributeHistoricalEndpoint_FormUpdate(PsetAttributeHistoricalEndpoint item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse PsetAttributeHistoricalEndpoint_FormUpdateBatch(string[] keys, PsetAttributeHistoricalEndpoint item);

		/// <summary>
		/// Gets a list for the business entity PsetAttributeHistoricalEndpoint. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<PsetAttributeHistoricalEndpoint> PsetAttributeHistoricalEndpoint_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		PsetAttributeHistoricalEndpoint PsetAttributeHistoricalEndpoint_GetItem(string Key);

		/// <summary>
		///  Get the  PsetAttributeHistoricalEndpoint for a specific Object.
		/// </summary>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="KeyPropertySingleValue">The key property single value.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		PsetAttributeHistoricalEndpoint PsetAttributeHistoricalEndpoint_GetItemByKeyObject(string KeyObject, string KeyPropertySingleValue);

		/// <summary>
		/// Gets a json store for the business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PsetAttributeHistoricalEndpoint> PsetAttributeHistoricalEndpoint_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<PsetAttributeHistoricalEndpoint> PsetAttributeHistoricalEndpoint_SaveStore(CrudStore<PsetAttributeHistoricalEndpoint> store);

		/// <summary>
		/// Deletes an existing business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool RESTDataAcquisitionContainer_Delete(RESTDataAcquisitionContainer item);

		/// <summary>
		/// Creates a new business entity RESTDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse RESTDataAcquisitionContainer_FormCreate(RESTDataAcquisitionContainer item);

		/// <summary>
		/// Loads a specific item for the business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse RESTDataAcquisitionContainer_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity RESTDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse RESTDataAcquisitionContainer_FormUpdate(RESTDataAcquisitionContainer item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse RESTDataAcquisitionContainer_FormUpdateBatch(string[] keys, RESTDataAcquisitionContainer item);

		/// <summary>
		/// Gets a list for the business entity RESTDataAcquisitionContainer. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<RESTDataAcquisitionContainer> RESTDataAcquisitionContainer_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		RESTDataAcquisitionContainer RESTDataAcquisitionContainer_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<RESTDataAcquisitionContainer> RESTDataAcquisitionContainer_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<RESTDataAcquisitionContainer> RESTDataAcquisitionContainer_SaveStore(CrudStore<RESTDataAcquisitionContainer> store);

		/// <summary>
		/// Deletes an existing business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool RESTDataAcquisitionEndpoint_Delete(RESTDataAcquisitionEndpoint item);

		/// <summary>
		/// Creates a new business entity RESTDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse RESTDataAcquisitionEndpoint_FormCreate(RESTDataAcquisitionEndpoint item);

		/// <summary>
		/// Loads a specific item for the business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse RESTDataAcquisitionEndpoint_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity RESTDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse RESTDataAcquisitionEndpoint_FormUpdate(RESTDataAcquisitionEndpoint item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse RESTDataAcquisitionEndpoint_FormUpdateBatch(string[] keys, RESTDataAcquisitionEndpoint item);

		/// <summary>
		/// Gets a list for the business entity RESTDataAcquisitionEndpoint. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<RESTDataAcquisitionEndpoint> RESTDataAcquisitionEndpoint_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		RESTDataAcquisitionEndpoint RESTDataAcquisitionEndpoint_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<RESTDataAcquisitionEndpoint> RESTDataAcquisitionEndpoint_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of  RESTDataAcquisitionEndpoint by key of the container.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<RESTDataAcquisitionEndpoint> RESTDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer(PagingParameter paging, string KeyDataAcquisitionContainer);

		/// <summary>
		/// Saves a crud store for the business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<RESTDataAcquisitionEndpoint> RESTDataAcquisitionEndpoint_SaveStore(CrudStore<RESTDataAcquisitionEndpoint> store);

		/// <summary>
		/// Return a store of available REST DataAcquisition Provider.
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ListElement> RESTDataAcquisitionProvider_GetStore();

		/// <summary>
		/// Deletes an existing business entity StatisticalSerie.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool StatisticalSerie_Delete(StatisticalSerie item);

		/// <summary>
		/// Creates a new business entity StatisticalSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse StatisticalSerie_FormCreate(StatisticalSerie item);

		/// <summary>
		/// Loads a specific item for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse StatisticalSerie_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity StatisticalSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse StatisticalSerie_FormUpdate(StatisticalSerie item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse StatisticalSerie_FormUpdateBatch(string[] keys, StatisticalSerie item);

		/// <summary>
		/// Gets a list for the business entity StatisticalSerie. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<StatisticalSerie> StatisticalSerie_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		StatisticalSerie StatisticalSerie_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<StatisticalSerie> StatisticalSerie_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of StatisticalSeries for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<StatisticalSerie> StatisticalSerie_GetStoreByKeyChart(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Saves a crud store for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<StatisticalSerie> StatisticalSerie_SaveStore(CrudStore<StatisticalSerie> store);

		/// <summary>
		/// Deletes an existing business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool StruxureWareDataAcquisitionContainer_Delete(StruxureWareDataAcquisitionContainer item);

		/// <summary>
		/// Creates a new business entity StruxureWareDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse StruxureWareDataAcquisitionContainer_FormCreate(StruxureWareDataAcquisitionContainer item);

		/// <summary>
		/// Loads a specific item for the business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse StruxureWareDataAcquisitionContainer_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity StruxureWareDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse StruxureWareDataAcquisitionContainer_FormUpdate(StruxureWareDataAcquisitionContainer item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse StruxureWareDataAcquisitionContainer_FormUpdateBatch(string[] keys, StruxureWareDataAcquisitionContainer item);

		/// <summary>
		/// Gets a list for the business entity StruxureWareDataAcquisitionContainer. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<StruxureWareDataAcquisitionContainer> StruxureWareDataAcquisitionContainer_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		StruxureWareDataAcquisitionContainer StruxureWareDataAcquisitionContainer_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<StruxureWareDataAcquisitionContainer> StruxureWareDataAcquisitionContainer_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<StruxureWareDataAcquisitionContainer> StruxureWareDataAcquisitionContainer_SaveStore(CrudStore<StruxureWareDataAcquisitionContainer> store);

		/// <summary>
		/// Deletes an existing business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool StruxureWareDataAcquisitionEndpoint_Delete(StruxureWareDataAcquisitionEndpoint item);

		/// <summary>
		/// Creates a new business entity StruxureWareDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse StruxureWareDataAcquisitionEndpoint_FormCreate(StruxureWareDataAcquisitionEndpoint item);

		/// <summary>
		/// Loads a specific item for the business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse StruxureWareDataAcquisitionEndpoint_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity StruxureWareDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse StruxureWareDataAcquisitionEndpoint_FormUpdate(StruxureWareDataAcquisitionEndpoint item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse StruxureWareDataAcquisitionEndpoint_FormUpdateBatch(string[] keys, StruxureWareDataAcquisitionEndpoint item);

		/// <summary>
		/// Gets a list for the business entity StruxureWareDataAcquisitionEndpoint. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<StruxureWareDataAcquisitionEndpoint> StruxureWareDataAcquisitionEndpoint_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		StruxureWareDataAcquisitionEndpoint StruxureWareDataAcquisitionEndpoint_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<StruxureWareDataAcquisitionEndpoint> StruxureWareDataAcquisitionEndpoint_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of  StruxureWareDataAcquisitionEndpoint by key of the container.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<StruxureWareDataAcquisitionEndpoint> StruxureWareDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer(PagingParameter paging, string KeyDataAcquisitionContainer);

		/// <summary>
		/// Saves a crud store for the business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<StruxureWareDataAcquisitionEndpoint> StruxureWareDataAcquisitionEndpoint_SaveStore(CrudStore<StruxureWareDataAcquisitionEndpoint> store);

		/// <summary>
		/// Deletes an existing business entity VirtualFile.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool VirtualFile_Delete(VirtualFile item);

		/// <summary>
		/// Creates a new business entity VirtualFile and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse VirtualFile_FormCreate(VirtualFile item);

		/// <summary>
		/// Loads a specific item for the business entity VirtualFile.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse VirtualFile_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity VirtualFile and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse VirtualFile_FormUpdate(VirtualFile item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse VirtualFile_FormUpdateBatch(string[] keys, VirtualFile item);

		/// <summary>
		/// Gets a list for the business entity VirtualFile. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<VirtualFile> VirtualFile_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity VirtualFile.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		VirtualFile VirtualFile_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity VirtualFile.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<VirtualFile> VirtualFile_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Return a stream from a VirtualFile Path.
		/// </summary>
		/// <param name="direct">the path.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Stream VirtualFile_GetStreamImage(string direct);

		/// <summary>
		/// Saves a crud store for the business entity VirtualFile.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<VirtualFile> VirtualFile_SaveStore(CrudStore<VirtualFile> store);

		/// <summary>
		/// Deletes an existing business entity WeatherLocation.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool WeatherLocation_Delete(WeatherLocation item);

		/// <summary>
		/// Creates a new business entity WeatherLocation and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse WeatherLocation_FormCreate(WeatherLocation item);

		/// <summary>
		/// Loads a specific item for the business entity WeatherLocation.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse WeatherLocation_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity WeatherLocation and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse WeatherLocation_FormUpdate(WeatherLocation item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse WeatherLocation_FormUpdateBatch(string[] keys, WeatherLocation item);

		/// <summary>
		/// Gets a list for the business entity WeatherLocation. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<WeatherLocation> WeatherLocation_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Returns the 5 day Forecast for this Weather Location.
		/// </summary>
		/// <param name="KeyWeatherLocation">The WeatherLocation key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<WeatherForecast> WeatherLocation_GetFiveDayForecast(string KeyWeatherLocation);

		/// <summary>
		/// Gets a specific item for the business entity WeatherLocation.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		WeatherLocation WeatherLocation_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity WeatherLocation.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<WeatherLocation> WeatherLocation_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity WeatherLocation.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<WeatherLocation> WeatherLocation_SaveStore(CrudStore<WeatherLocation> store);

		/// <summary>
		/// Gets a json store of WeatherLocation based on a specific search.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<WeatherLocation> WeatherLocationSearch_GetStore(PagingParameter paging);

		/// <summary>
		/// Deletes an existing business entity WebFrame.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool WebFrame_Delete(WebFrame item);

		/// <summary>
		/// Creates a new business entity WebFrame and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse WebFrame_FormCreate(WebFrame item);

		/// <summary>
		/// Loads a specific item for the business entity WebFrame.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse WebFrame_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity WebFrame and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse WebFrame_FormUpdate(WebFrame item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse WebFrame_FormUpdateBatch(string[] keys, WebFrame item);

		/// <summary>
		/// Gets a list for the business entity WebFrame. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<WebFrame> WebFrame_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity WebFrame.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		WebFrame WebFrame_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity WebFrame.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<WebFrame> WebFrame_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity WebFrame.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<WebFrame> WebFrame_SaveStore(CrudStore<WebFrame> store);

		/// <summary>
		/// Gets a json store for the business entity Algorithm.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Algorithm> Algorithm_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Algorithm.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<Algorithm> Algorithm_SaveStore(CrudStore<Algorithm> store);

		/// <summary>
		/// Gets a list for the business entity Algorithm. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<Algorithm> Algorithm_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Algorithm.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		Algorithm Algorithm_GetItem(string Key);

		/// <summary>
		/// Loads a specific item for the business entity Algorithm.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Algorithm_FormLoad(string Key);

		/// <summary>
		/// Creates a new business entity Algorithm and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Algorithm_FormCreate(Algorithm item, CrudStore<AlgorithmInputDefinition> inputs);

		/// <summary>
		/// Updates an existing business entity Algorithm and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Algorithm_FormUpdate(Algorithm item, CrudStore<AlgorithmInputDefinition> inputs);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse Algorithm_FormUpdateBatch(string[] keys, Algorithm item);

		/// <summary>
		/// Deletes an existing business entity Algorithm.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool Algorithm_Delete(Algorithm item);

		/// <summary>
		/// Gets a json store for the business entity ChartAlgorithm.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartAlgorithm> ChartAlgorithm_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity ChartAlgorithm.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartAlgorithm> ChartAlgorithm_SaveStore(CrudStore<ChartAlgorithm> store);

		/// <summary>
		/// Gets a list for the business entity ChartAlgorithm. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<ChartAlgorithm> ChartAlgorithm_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity ChartAlgorithm.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		ChartAlgorithm ChartAlgorithm_GetItem(string Key);

		/// <summary>
		/// Loads a specific item for the business entity ChartAlgorithm.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartAlgorithm_FormLoad(string Key);

		/// <summary>
		/// Creates a new business entity ChartAlgorithm and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartAlgorithm_FormCreate(ChartAlgorithm item, CrudStore<AlgorithmInputValue> inputs);

		/// <summary>
		/// Updates an existing business entity ChartAlgorithm and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartAlgorithm_FormUpdate(ChartAlgorithm item, CrudStore<AlgorithmInputValue> inputs);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ChartAlgorithm_FormUpdateBatch(string[] keys, ChartAlgorithm item);

		/// <summary>
		/// Deletes an existing business entity ChartAlgorithm.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool ChartAlgorithm_Delete(ChartAlgorithm item);

		/// <summary>
		/// Get a store of Algorithm for a specific chart.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ChartAlgorithm> ChartAlgorithm_GetStoreByKeyChart(string KeyChart, PagingParameter paging);

		/// <summary>
		/// Returns the list of existing events title from a specific calendar category.
		/// </summary>
		/// <param name="KeyCalendarEventCategory">The key calendar event category.</param>
		/// <param name="KeyMeter">The key meter.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<SimpleListElement> Calendar_GetEventsTitle(string KeyCalendarEventCategory, string KeyMeter, PagingParameter paging);





		/// <summary>
		/// Gets a json store for the business entity FlipCard.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<FlipCard> FlipCard_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity FlipCard.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<FlipCard> FlipCard_SaveStore(CrudStore<FlipCard> store);

		/// <summary>
		/// Gets a list for the business entity FlipCard. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<FlipCard> FlipCard_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity FlipCard.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FlipCard FlipCard_GetItem(string Key);

		/// <summary>
		/// Loads a specific item for the business entity FlipCard.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse FlipCard_FormLoad(string Key);

		/// <summary>
		/// Creates a new business entity FlipCard and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="cards">The cards.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse FlipCard_FormCreate(FlipCard item, CrudStore<FlipCardEntry> cards);

		/// <summary>
		/// Updates an existing business entity FlipCard and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="cards">The cards.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse FlipCard_FormUpdate(FlipCard item, CrudStore<FlipCardEntry> cards);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse FlipCard_FormUpdateBatch(string[] keys, FlipCard item);

		/// <summary>
		/// Deletes an existing business entity FlipCard.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool FlipCard_Delete(FlipCard item);



		/// <summary>
		/// Gets a json store for the business entity FlipCardEntry.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<FlipCardEntry> FlipCardEntry_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity FlipCardEntry.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<FlipCardEntry> FlipCardEntry_SaveStore(CrudStore<FlipCardEntry> store);

		/// <summary>
		/// Gets a list for the business entity FlipCardEntry. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<FlipCardEntry> FlipCardEntry_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity FlipCardEntry.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FlipCardEntry FlipCardEntry_GetItem(string Key);

		/// <summary>
		/// Loads a specific item for the business entity FlipCardEntry.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse FlipCardEntry_FormLoad(string Key);

		/// <summary>
		/// Creates a new business entity FlipCardEntry and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse FlipCardEntry_FormCreate(FlipCardEntry item);

		/// <summary>
		/// Updates an existing business entity FlipCardEntry and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse FlipCardEntry_FormUpdate(FlipCardEntry item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse FlipCardEntry_FormUpdateBatch(string[] keys, FlipCardEntry item);

		/// <summary>
		/// Deletes an existing business entity FlipCardEntry.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool FlipCardEntry_Delete(FlipCardEntry item);

		/// <summary>
		/// Gets a store of  FlipCardEntry for a specific FlipCard.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyFlipCard">The key flip card.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<FlipCardEntry> FlipCardEntry_GetStoreByKeyFlipCard(PagingParameter paging, string KeyFlipCard);


		/// <summary>
		/// Determines whether [is energy aggregator crud notification in progress].
		/// </summary>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool IsEnergyAggregatorCrudNotificationInProgress();




		/// <summary>
		/// Gets a json store for the business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlgorithmInputDefinition> AlgorithmInputDefinition_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlgorithmInputDefinition> AlgorithmInputDefinition_SaveStore(CrudStore<AlgorithmInputDefinition> store);

		/// <summary>
		/// Gets a list for the business entity AlgorithmInputDefinition. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<AlgorithmInputDefinition> AlgorithmInputDefinition_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		AlgorithmInputDefinition AlgorithmInputDefinition_GetItem(string Key);

		/// <summary>
		/// Loads a specific item for the business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlgorithmInputDefinition_FormLoad(string Key);

		/// <summary>
		/// Creates a new business entity AlgorithmInputDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlgorithmInputDefinition_FormCreate(AlgorithmInputDefinition item);

		/// <summary>
		/// Updates an existing business entity AlgorithmInputDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlgorithmInputDefinition_FormUpdate(AlgorithmInputDefinition item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlgorithmInputDefinition_FormUpdateBatch(string[] keys, AlgorithmInputDefinition item);

		/// <summary>
		/// Deletes an existing business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool AlgorithmInputDefinition_Delete(AlgorithmInputDefinition item);





		/// <summary>
		/// Gets a json store for the business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlgorithmInputValue> AlgorithmInputValue_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlgorithmInputValue> AlgorithmInputValue_SaveStore(CrudStore<AlgorithmInputValue> store);

		/// <summary>
		/// Gets a list for the business entity AlgorithmInputValue. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<AlgorithmInputValue> AlgorithmInputValue_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		AlgorithmInputValue AlgorithmInputValue_GetItem(string Key);

		/// <summary>
		/// Loads a specific item for the business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlgorithmInputValue_FormLoad(string Key);

		/// <summary>
		/// Creates a new business entity AlgorithmInputValue and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlgorithmInputValue_FormCreate(AlgorithmInputValue item);

		/// <summary>
		/// Updates an existing business entity AlgorithmInputValue and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlgorithmInputValue_FormUpdate(AlgorithmInputValue item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse AlgorithmInputValue_FormUpdateBatch(string[] keys, AlgorithmInputValue item);

		/// <summary>
		/// Deletes an existing business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool AlgorithmInputValue_Delete(AlgorithmInputValue item);



		/// <summary>
		/// Gets a json store of AlgorithmInputDefinition for a specific Algorithm.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlgorithm">The key script.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlgorithmInputDefinition> AlgorithmInputDefinition_GetStoreByKeyAlgorithm(PagingParameter paging, string KeyAlgorithm);


		/// <summary>
		/// Gets a json store of AlgorithmInputValue for a specific Algorithm/Chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyAlgorithm">The key algorithm.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<AlgorithmInputValue> AlgorithmInputValue_GetStoreByKeyChart(PagingParameter paging, string KeyChart, string KeyAlgorithm);


		/// <summary>
		/// Gets the specific data models tree.
		/// </summary>
		/// <param name="KeyScript">The key script.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<TreeNode> ScriptModel_GetTree(string KeyScript);


		/// <summary>
		/// Get the C# class squeleton for Analytics.
		/// </summary>
		/// <param name="className">Name of the class.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		string Algorithm_GetCode(string className);

		/// <summary>
		/// Get data points of Analytics.
		/// </summary>
		/// <param name="keyChart">The key chart.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="timeInterval">The time interval.</param>
		/// <param name="localisation">The localisation.</param>
		/// <param name="classificationLevel">The classification level.</param>
		/// <param name="dynamicTimeScaleEnabled">if set to <c>true</c> [dynamic time scale enabled].</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="filterSpatialKeys">The filter spatial keys.</param>
		/// <param name="metersClassificationsKeys">The meters classifications keys.</param>
		/// <param name="metersKeys">The meters keys.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<DataPoint> Analytics_GetDataPoints(string keyChart, PagingParameter paging, AxisTimeInterval timeInterval,
			ChartLocalisation localisation, int classificationLevel, bool dynamicTimeScaleEnabled, DateTime startDate,
			DateTime endDate, List<string> filterSpatialKeys, List<string> metersClassificationsKeys, List<string> metersKeys);

        /// <summary>
        /// Begins long running operation to Save a crud store for the business entity MeterValidationRule.
        /// </summary>
        /// <param name="operationId">The operation identifier.</param>
        /// <param name="store">The store.</param>
        [OperationContract(IsOneWay = true)]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void MeterValidationRule_BeginSaveStore(Guid operationId, CrudStore<MeterValidationRule> store);

        /// <summary>
        /// Begins long running operation to Save a crud store for the business entity AlarmDefinition.
        /// </summary>
        /// <param name="operationId">The operation identifier.</param>
        /// <param name="store">The store.</param>
        [OperationContract(IsOneWay = true)]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        void AlarmDefinition_BeginSaveStore(Guid operationId, CrudStore<AlarmDefinition> store);

	    /// <summary>
	    /// Begins long running operation to get map pushpins
	    /// </summary>
	    /// <param name="operationId"></param>
	    /// <param name="key"></param>
	    [OperationContract(IsOneWay = true)]
	    [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
	    void Map_BeginGetPushpins(Guid operationId, string key);

        /// <summary>
        /// Convert date to time zone
        /// </summary>
        /// <param name="date"></param>
        /// <param name="timeZoneId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        DateTime ConvertDateToTimeZone(DateTime date, string timeZoneId);

        /// <summary>
        /// Convert Date to Utc from a date in a given time zone
        /// </summary>
        /// <param name="date"></param>
        /// <param name="timeZoneId"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        DateTime ConvertDateToUtcFromTimeZone(DateTime date, string timeZoneId);

	}
}


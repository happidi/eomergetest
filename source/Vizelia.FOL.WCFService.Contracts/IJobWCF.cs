﻿using System.ServiceModel;
using System.ServiceModel.Web;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.WCFService.Contracts {

	/// <summary>
	/// Service contract for Job service.
	/// </summary>
	[ServiceContract(Namespace = "http://www.vizelia.com/wcf/2011/01/")]
	public interface IJobWCF : IBaseModuleWCF {

		/// <summary>
		/// Resumes the pending workflows.
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void ResumePendingWorkflows(JobContext context);

		/// <summary>
		/// Tests the job exception.
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void TestJobException(JobContext context);

		/// <summary>
		/// Performs the scheduled mapping.
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void PerformScheduledMapping(JobContext context);

		/// <summary>
		/// Generate the different playlist as images.
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void GeneratePlaylist(JobContext context);

		/// <summary>
		/// Process all the enabled AlarmDefinition and generates the corresponding alarm instances.
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void AlarmDefinitionProcess(JobContext context);

		/// <summary>
		/// Process all the Meter that have an endpoint and store the corresponding real time value.
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void MeterEndpointProcess(JobContext context);

		/// <summary>
		/// Process all the ChartScheduler and sends the one(s) that needs to be sent.
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void ChartSchedulerProcess(JobContext context);


		/// <summary>
		/// Process all the enabled MeterValidation and generates the corresponding alarm instances.
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void MeterValidationRuleProcess(JobContext context);

		/// <summary>
		/// Purges the server logs.
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void PurgeLogs(JobContext context);

		/// <summary>
		/// Synchronizes the external authentication provider.
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void SynchronizeExternalAuthenticationProvider(JobContext context);

		/// <summary>
		/// Preload charts for specific users.
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void ChartPreLoadAll(JobContext context);

		/// <summary>
		/// Exports meter data
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void ExportMeterData(JobContext context);


		/// <summary>
		/// Push the different portal templates.
		/// </summary>
		/// <param name="context">The context.</param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void PortalTemplatePushUpdates(JobContext context);

	}
}

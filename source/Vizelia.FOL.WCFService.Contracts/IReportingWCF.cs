﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.IO;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.WCFService.Contracts {
	/// <summary>
	/// Service contract for reporting service
	/// </summary>
	[ServiceContract(Namespace = "http://www.vizelia.com/wcf/2011/01/")]
	public interface IReportingWCF : IBaseModuleWCF {
		/// <summary>
		/// Renders a report as a stream. This is the start function.
		/// Input data are sent via a form encoded as multipart/form-data.
		/// </summary>
		/// <param name="input">
		/// Contains the differents parameters needed by the service.
		/// Each parameter is read via Form collection
		/// </param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void BeginRenderReport(Stream input);


		/// <summary>
		/// Renders an ActionRequest report as a stream. This is the start function
		/// Input data are sent via a form encoded as multipart/form-data.
		/// </summary>
		///<param name="input"></param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void BeginRenderReportActionRequest(Stream input);


		/// <summary>
		/// Renders an Task report as a stream. This is the start function
		/// Input data are sent via a form encoded as multipart/form-data.
		/// </summary>
		///<param name="input"></param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void BeginRenderReportTask(Stream input);


		/// <summary>
		/// Renders a custom report as a stream. This is the start function
		/// Input data are sent via a form encoded as multipart/form-data.
		/// </summary>
		///<param name="input"></param>
		[OperationContract(IsOneWay = true)]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void BeginRenderReportCustom(Stream input);

		/// <summary>
		/// Generates the json structure of the Reporting treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<TreeNode> Reporting_GetTree(string Key);

		/// <summary>
		/// Gets the report parameters.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		List<ReportParameter> GetReportParameters(string key);

		/// <summary>
		/// Gets the report parameter values.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ReportParameterValue> GetReportParameterValues(string key, string parameterName);

		/// <summary>
		/// Creates the folder with the given name under the specified path.
		/// </summary>
		/// <param name="parentPath">The parent path.</param>
		/// <param name="folderName">Name of the folder.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void CreateFolder(string parentPath, string folderName);

		/// <summary>
		/// Deletes the folder.
		/// </summary>
		/// <param name="path">The path.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void DeleteFolder(string path);

		/// <summary>
		/// Renames or moves the item.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="newPath">The new path.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void MoveItem(string path, string newPath);

		/// <summary>
		/// Deletes the report.
		/// </summary>
		/// <param name="path">The path.</param>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		void DeleteReport(string path);

		/// <summary>
		/// Saves a crud store for the business entity ReportSubscription.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ReportSubscription> ReportSubscription_SaveStore(CrudStore<ReportSubscription> store);

		/// <summary>
		/// Creates a new business entity ReportSubscription and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		FormResponse ReportSubscription_FormCreate(ReportSubscription item);

		/// <summary>
		/// Deletes an existing business entity ReportSubscription.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		bool ReportSubscription_Delete(ReportSubscription item);

		/// <summary>
		/// Reports the subscription get all by report.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
		JsonStore<ReportSubscription> ReportSubscription_GetAllByReport(string reportPath, PagingParameter paging);
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Vizelia.FOL.WCFService.Contracts {

	/// <summary>
	/// Base implementation for all standalone  WCF services ( ie: EnergyAggregatorWCF..)
	/// Do not implement any public OperationContract on this interface
	/// </summary>
	[ServiceContract(Namespace = "http://www.vizelia.com/wcf/2011/01/")]
	public interface IBaseWCF {
		/// <summary>
		/// Gets the version of the service.
		/// </summary>
		/// <returns>
		/// The version of the service.
		/// </returns>
		string GetVersion();
	}
}

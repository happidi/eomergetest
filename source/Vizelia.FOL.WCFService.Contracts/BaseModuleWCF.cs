﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ServiceModel.Activation;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Collections.Specialized;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;
using System.Web;
using System.ServiceModel.Web;
using System.ServiceModel;

namespace Vizelia.FOL.WCFService.Contracts {

	/// <summary>
	///	Implementation of the base class for all the WCF services.
	///	This class has a static ctor used to run initialization (Init).
	/// </summary>
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall)]
	public abstract class BaseModuleWCF :BaseWCF {

		/// <summary>
		/// Static ctor executed only once.
		/// </summary>
		static BaseModuleWCF() {
			//Init();
		}

		/// <summary>
		/// Initialization of the application.
		/// </summary>
		public static void Init() {
			//CoreBusinessLayer.Init();
		}


		/// <summary>
		/// Prepares the response for a file attachment.
		/// </summary>
		/// <param name="streamResult">The stream result.</param>
		protected void PrepareFileAttachment(StreamResult streamResult) {
			PrepareFileAttachment(streamResult.FileName, streamResult.MimeType);
		}


		/// <summary>
		/// Prepares the response for a file attachment.
		/// </summary>
		/// <param name="filename">The name of the file (with the extension).</param>
		/// <param name="mimeType">The mime type as a string.</param>
		protected void PrepareFileAttachment(string filename, string mimeType) {
			HttpContext.Current.Response.ContentType = mimeType;
			WebOperationContext.Current.OutgoingResponse.ContentType = mimeType;
			if (!string.IsNullOrEmpty(filename))
				HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename=\"{0}\"", filename));
			HttpContext.Current.Response.AddHeader("X-Content-Type-Options","nosniff");

		}


	}
}

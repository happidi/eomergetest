﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.WCFService.Contracts {

	/// <summary>
	/// Service contract for graph service.
	/// </summary>
	[ServiceContract(Namespace = "http://www.vizelia.com/wcf/2011/01/")]
	public interface IGraphWCF : IBaseModuleWCF {

		

		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <param name="viewName">Name of the view.</param>
		/// <returns></returns>
		[OperationContract]
		[WebGet]
		DataSet GetView(string viewName);

	}


}

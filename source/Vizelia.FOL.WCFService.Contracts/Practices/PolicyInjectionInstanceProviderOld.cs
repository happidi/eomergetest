﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Dispatcher;
using System.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace Vizelia.FOL.WCFService.Contracts.Practices {
	/// <summary>
	/// Replaces the standard mecanism of creation instance for a WCF service, and use PolicInjection instead.
	/// </summary>
	public class PolicyInjectionInstanceProviderOld : IInstanceProvider {
		private Type serviceContractType;
		private static PolicyInjectionHelper defaultHelper;
		//private static readonly object singletonLock = new object();


		/// <summary>
		/// Public ctor.
		/// </summary>
		/// <param name="t">The service contract type.</param>
		public PolicyInjectionInstanceProviderOld(Type t) {
			if (defaultHelper == null)
				defaultHelper = new PolicyInjectionHelper(ConfigurationSourceFactory.Create());
			this.serviceContractType = t;
		}


		//private static PolicyInjectionHelper DefaultHelper {
		//    get {
		//        if (defaultHelper == null) {
		//            lock (singletonLock) {
		//                if (defaultHelper == null) {
		//                    defaultHelper = GetHelperFromConfig(ConfigurationSourceFactory.Create());
		//                }
		//            }
		//        }
		//        return defaultHelper;
		//    }
		//}

		/// <summary>
		/// Returns an instance of the service via PolicyInjection.
		/// </summary>
		/// <param name="instanceContext"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		public object GetInstance(InstanceContext instanceContext, System.ServiceModel.Channels.Message message) {
			Type type = instanceContext.Host.Description.ServiceType;
			if (serviceContractType != null)
				return defaultHelper.Create(type, serviceContractType);
			else {
				return defaultHelper.Create(type);
			}
		}

		/// <summary>
		/// Returns an instance of the service via PolicyInjection.
		/// </summary>
		/// <param name="instanceContext"></param>
		/// <returns></returns>
		public object GetInstance(InstanceContext instanceContext) {
			return GetInstance(instanceContext, null);
		}

		/// <summary>
		/// Releases the instance.
		/// </summary>
		/// <param name="instanceContext"></param>
		/// <param name="instance"></param>
		public void ReleaseInstance(InstanceContext instanceContext, object instance) {
			IDisposable disposable = instance as IDisposable;
			if (disposable != null) {
				disposable.Dispose();
			}
		}
	}
}

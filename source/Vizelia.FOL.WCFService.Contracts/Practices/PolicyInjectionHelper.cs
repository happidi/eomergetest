﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;

namespace Vizelia.FOL.WCFService.Contracts.Practices {
	/// <summary>
	/// The static PolicyInjection static class, uses generics.
	/// We need this helper so we can pass to create or wrap type instead of generics.
	/// This class is inspired from PolicyInjectionHelper found in assembly Microsoft.Practices.EnterpriseLibrary.PolicyInjection.
	/// </summary>
	class PolicyInjectionHelper : IDisposable {
		// Fields
		private readonly IUnityContainer container = new UnityContainer();
		private static readonly TransparentProxyInterceptor injector = new TransparentProxyInterceptor();

		/// <summary>
		/// Public ctor.
		/// </summary>
		/// <param name="configurationSource"></param>
		public PolicyInjectionHelper(IConfigurationSource configurationSource) {
			this.container.AddNewExtension<Interception>();
			PolicyInjectionSettings section = (PolicyInjectionSettings)configurationSource.GetSection("policyInjection");
			if (section != null) {
				section.ConfigureContainer(this.container, configurationSource);
			}
		}
	
		/// <summary>
		/// Equivalent to PolicyInjection.Create but uses Type instead of Generic.
		/// </summary>
		/// <param name="typeToCreate"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public object Create(Type typeToCreate, params object[] args) {
			return this.Create(typeToCreate, typeToCreate, args);
		}

		/// <summary>
		/// Equivalent to PolicyInjection.Create but uses Type instead of Generic.
		/// </summary>
		/// <param name="typeToCreate"></param>
		/// <param name="typeToReturn"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public object Create(Type typeToCreate, Type typeToReturn, params object[] args) {
			return this.DoCreate(typeToCreate, typeToReturn, args);
		}

		private object DoCreate(Type typeToCreate, Type typeToReturn, object[] arguments) {
			object instance = Activator.CreateInstance(typeToCreate, arguments);
			return this.DoWrap(instance, typeToReturn);
		}

		private object DoWrap(object instance, Type typeToReturn) {
			this.container.Configure<Interception>().SetDefaultInterceptorFor(typeToReturn, injector);
			return this.container.BuildUp(typeToReturn, instance);
		}

		/// <summary>
		/// Equivalent to PolicyInjection.Wrap but uses Type instead of Generic.
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="typeToReturn"></param>
		/// <returns></returns>
		public object Wrap(object instance, Type typeToReturn) {
			return this.DoWrap(instance, typeToReturn);
		}

		void IDisposable.Dispose() {
			if (this.container != null) {
				this.container.Dispose();
			}
		}

	}

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Description;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.ServiceModel;

namespace Vizelia.FOL.WCFService.Contracts.Practices {

	/// <summary>
	/// Custom attribute that should be applied to implementation of a WCF service for which we would like PolicyInjection.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class PolicyInjectionBehaviorAttribute : Attribute, IContractBehavior, IContractBehaviorAttribute {

		/// <summary>
		/// Target contract is null so we apply to all contracts.
		/// </summary>
		public Type TargetContract {
			get {
				return null; //return null so we apply to all contracts
			}
		}

		/// <summary>
		/// Not implemented.
		/// </summary>
		/// <param name="description"></param>
		/// <param name="endpoint"></param>
		/// <param name="parameters"></param>
		public void AddBindingParameters(ContractDescription description, ServiceEndpoint endpoint, BindingParameterCollection parameters) {
		}

		/// <summary>
		/// Not implemented.
		/// </summary>
		/// <param name="description"></param>
		/// <param name="endpoint"></param>
		/// <param name="clientRuntime"></param>
		public void ApplyClientBehavior(ContractDescription description, ServiceEndpoint endpoint, ClientRuntime clientRuntime) {
		}

		/// <summary>
		/// Not implemented.
		/// </summary>
		/// <param name="description"></param>
		/// <param name="endpoint"></param>
		public void Validate(ContractDescription description, ServiceEndpoint endpoint) {
		}

		/// <summary>
		/// Apply the behavior of PolicyInjectionInstanceProvider.
		/// </summary>
		/// <param name="description"></param>
		/// <param name="endpoint"></param>
		/// <param name="dispatch"></param>
		public void ApplyDispatchBehavior(ContractDescription description, ServiceEndpoint endpoint, DispatchRuntime dispatch) {
			Type contractType = description.ContractType;
			dispatch.InstanceProvider = new PolicyInjectionInstanceProvider(contractType);
		}


	}



}

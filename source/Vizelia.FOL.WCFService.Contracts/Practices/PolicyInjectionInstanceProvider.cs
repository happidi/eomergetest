﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity.InterceptionExtension;
using Microsoft.Practices.Unity;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection.Configuration;
using System.ServiceModel.Dispatcher;

namespace Vizelia.FOL.WCFService.Contracts.Practices {
	/// <summary>
	/// Replaces the standard mecanism of instance creation for a WCF service, and uses PolicyInjection instead.
	/// </summary>
	public class PolicyInjectionInstanceProvider : IInstanceProvider {

		private Type serviceContractType {
			get;
			set;
		}
		private readonly object _sync = new object();
		private static readonly IUnityContainer container;
		private static readonly TransparentProxyInterceptor injector = new TransparentProxyInterceptor();
		

		/// <summary>
		/// Static ctor.
		/// </summary>
		static PolicyInjectionInstanceProvider() {
		
			container = new UnityContainer()
				.AddNewExtension<Interception>();

			IConfigurationSource configSource = ConfigurationSourceFactory.Create();
			PolicyInjectionSettings settings = (PolicyInjectionSettings)configSource.GetSection(PolicyInjectionSettings.SectionName);
			if (settings != null) {
				settings.ConfigureContainer(container, configSource);
			}
		}

		/// <summary>
		/// Public ctor.
		/// </summary>
		/// <param name="type"></param>
		public PolicyInjectionInstanceProvider(Type type) {
			
			if (type != null && !type.IsInterface) {
				throw new ArgumentException("Specified Type must be an interface");
			}

			serviceContractType = type;
		}

		/// <summary>
		/// Returns an instance of the service via PolicyInjection.
		/// </summary>
		/// <param name="instanceContext"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		public object GetInstance(System.ServiceModel.InstanceContext instanceContext, System.ServiceModel.Channels.Message message) {
			
			Type type = instanceContext.Host.Description.ServiceType;
			if (serviceContractType != null) {
				lock (_sync) {
					container.Configure<Interception>()
						.SetDefaultInterceptorFor(serviceContractType, injector);
					container.RegisterType(serviceContractType, type);
					return container.Resolve(serviceContractType);
				}
			}
			else {
				if (!type.IsMarshalByRef) {
					throw new ArgumentException("Type Must inherit MarshalByRefObject if no ServiceInterface is specified");
				}
				lock (_sync) {
					container.Configure<Interception>()
						.SetDefaultInterceptorFor(type, injector);
					return container.Resolve(type);
				}
			}
		}

		/// <summary>
		/// Returns an instance of the service via PolicyInjection.
		/// </summary>
		/// <param name="instanceContext"></param>
		/// <returns></returns>
		public object GetInstance(System.ServiceModel.InstanceContext instanceContext) {
			return GetInstance(instanceContext, null);
		}

		/// <summary>
		/// Releases the instance.
		/// </summary>
		/// <param name="instanceContext"></param>
		/// <param name="instance"></param>
		public void ReleaseInstance(System.ServiceModel.InstanceContext instanceContext, object instance) {
			IDisposable disposable = instance as IDisposable;
			if (disposable != null) {
				disposable.Dispose();
			}
		}
	}
}

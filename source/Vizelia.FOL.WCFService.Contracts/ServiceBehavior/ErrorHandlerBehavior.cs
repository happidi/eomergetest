﻿using System;
using System.ServiceModel.Configuration;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.WCFService.Contracts {
	/// <summary>
	/// A behavior for the error handler
	/// </summary>
	public class ErrorHandlerBehavior : BehaviorExtensionElement {

		/// <summary>
		/// Gets the type of behavior.
		/// </summary>
		/// <returns>A <see cref="T:System.Type"/>.</returns>
		public override Type BehaviorType {

			get { return typeof (ErrorHandler); }

		}

		/// <summary>
		/// Creates a behavior extension based on the current configuration settings.
		/// </summary>
		/// <returns>
		/// The behavior extension.
		/// </returns>
		protected override object CreateBehavior() {

			return new ErrorHandler();

		}

	}
}


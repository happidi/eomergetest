﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.DataLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer {
	/// <summary>
	/// Business Layer implementation for Core. 
	/// </summary>
	public class SpaceManagementBusinessLayer : ISpaceManagementBusinessLayer {
		private ISpaceManagementDataAccess m_SpaceManagementDataAccess;

		/// <summary>
		/// Public Ctor.
		/// </summary>
		public SpaceManagementBusinessLayer() {
			m_SpaceManagementDataAccess = Helper.CreateInstance<SpaceManagementDataAccess, ISpaceManagementDataAccess>();
		}

		/// <summary>
		/// Gets the zoning 
		/// </summary>
		/// <param name="KeyBuildingStorey"></param>
		/// <param name="KeyClassificationItem">The filter for Classifications (optional)</param>
		/// <returns></returns>
		public JsonStore<Space> BuildingStorey_GetZoning(string KeyBuildingStorey, string KeyClassificationItem) {
			return m_SpaceManagementDataAccess.BuildingStorey_GetZoning(KeyBuildingStorey,KeyClassificationItem);
		}
	}
}

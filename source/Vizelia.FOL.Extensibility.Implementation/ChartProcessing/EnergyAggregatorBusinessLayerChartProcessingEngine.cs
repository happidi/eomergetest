﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Extensibility.Contracts.ChartProcessing;

namespace Vizelia.FOL.Extensibility.Implementation.ChartProcessing
{
	/// <summary>
	/// An analytics engine implementation that uses the IEnergyAggregatorBusinessLayer.
	/// </summary>
    public class EnergyAggregatorBusinessLayerChartProcessingEngine : IChartProcessingEngine {
		private readonly IEnergyAggregatorBusinessLayer m_EnergyAggregatorBusinessLayer;
		private readonly IEnergyBusinessLayer m_EnergyBusinessLayer;

		/// <summary>
		/// Initializes a new instance of the <see cref="EnergyAggregatorBusinessLayerChartProcessingEngine" /> class.
		/// </summary>
		public EnergyAggregatorBusinessLayerChartProcessingEngine() {
			m_EnergyAggregatorBusinessLayer = Helper.Resolve<IEnergyAggregatorBusinessLayer>();
			m_EnergyBusinessLayer = Helper.Resolve<IEnergyBusinessLayer>();
		}

		/// <summary>
		/// Processes the chart.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	    public ProcessChartResponse ProcessChart(ProcessChartRequest request) {
			string currentApplicationName = ContextHelper.ApplicationName;

			try {
				ContextHelper.ApplicationName = request.Context.ApplicationName;
#if true
				var locationFilter = new List<Filter>();
#else
			var locationFilter = ContextHelper.Get("LocationFilter") as List<Filter>;

			if (locationFilter == null) {
				throw new VizeliaException("Cannot fetch LocationFilter value from ContextHelper for a nested chart processing.");
			}
#endif
				var meterKeys = new List<string>();

				if (request.MeterKeys != null) {
					meterKeys.AddRange(request.MeterKeys.Select(key => string.Format("#{0}", key)));
				}

				request.Chart.Meters = m_EnergyBusinessLayer.Chart_GetMeters(request.Chart.KeyChart, true,
					request.LocationKeys, request.MeterClassificationItemKeys, meterKeys);


				Chart resultingChart = m_EnergyAggregatorBusinessLayer.Chart_Process(request.Context, request.Chart.KeyChart,
					locationFilter, request.Culture, request.Chart);

				return new ProcessChartResponse(resultingChart);
			}
			finally {
				ContextHelper.ApplicationName = currentApplicationName;
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Activities.Presentation.Metadata;
using System.ComponentModel;

namespace Vizelia.FOL.WF.Activities.Design {

	/// <summary>
	/// The designer for EventHandlerActivity.
	/// </summary>
	public partial class EventHandlerActivityDesigner {


		/// <summary>
		/// Initializes a new instance of the <see cref="EventHandlerActivityDesigner"/> class.
		/// </summary>
		public EventHandlerActivityDesigner() {
			InitializeComponent();
		}

		/// <summary>
		/// Registers the metadata.
		/// </summary>
		/// <param name="builder">The builder.</param>
		public static void RegisterMetadata(AttributeTableBuilder builder) {
			//builder.AddCustomAttributes(typeof(EventHandlerActivity), new DesignerAttribute(typeof(EventHandlerActivityDesigner)));
			//builder.AddCustomAttributes(typeof(EventHandlerActivity), new DescriptionAttribute("A state machine EventHandler activity"));
		}
	}
}

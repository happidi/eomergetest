﻿using System;
using System.Activities.Presentation;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Activities;
using System.Text.RegularExpressions;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;
using Vizelia.FOL.WF.Activities.Design;
using Vizelia.FOL.WF.Common;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessLayer.Broker;
using System.Web;
using System.Net.Configuration;
using System.Configuration;
using System.Threading;

namespace Vizelia.FOL.WF.Activities {

	/// <summary>
	/// A template for an async activity.
	/// </summary>
	//[ToolboxBitmap(typeof(EventHandlerActivity), "async.png")]
	//[Designer(typeof(AsyncActivityDesigner))]
	public sealed class AsyncActivity : AsyncCodeActivity {

		/// <summary>
		/// Ctor.
		/// </summary>
		public AsyncActivity() {
			this.DisplayName = "AsyncActivity";
		}

		/// <summary>
		/// When implemented in a derived class and using the specified execution context, callback method, and user state, enqueues an asynchronous activity in a run-time workflow.
		/// </summary>
		/// <param name="context">Information that defines the execution environment for the <see cref="T:System.Activities.AsyncCodeActivity"/>.</param>
		/// <param name="callback">The method to be called after the asynchronous activity and completion notification have occurred.</param>
		/// <param name="state">An object that saves variable information for an instance of an asynchronous activity.</param>
		/// <returns>
		/// The object that saves variable information for an instance of an asynchronous activity.
		/// </returns>
		protected override IAsyncResult BeginExecute(AsyncCodeActivityContext context, AsyncCallback callback, object state) {
			//BaseBusinessEntity entity = WorkflowHelper.GetArgumentEntity(context.DataContext);
			var param1 = "";
			Action<string> longActionDelegate = LongAction;
			context.UserState = longActionDelegate;
			return longActionDelegate.BeginInvoke(param1, callback, state);


		}

		/// <summary>
		/// When implemented in a derived class and using the specified execution environment information, notifies the workflow runtime that the associated asynchronous activity operation has completed.
		/// </summary>
		/// <param name="context">Information that defines the execution environment for the <see cref="T:System.Activities.AsyncCodeActivity"/>.</param>
		/// <param name="result">The implemented <see cref="T:System.IAsyncResult"/> that returns the status of an asynchronous activity when execution ends.</param>
		protected override void EndExecute(AsyncCodeActivityContext context, IAsyncResult result) {
			Action<string> longActionDelegate = (Action<string>)context.UserState;
			longActionDelegate.EndInvoke(result);
		}

		/// <summary>
		/// The long action.
		/// </summary>
		/// <param name="param1">The param1.</param>
		private void LongAction(string param1) {
			Thread.Sleep(5000);
		}
		
	}
}

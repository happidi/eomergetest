﻿using System;
using System.Activities.Presentation;
using System.Activities.Validation;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Activities;
using System.Text.RegularExpressions;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Providers;
using Vizelia.FOL.WF.Activities.Design;
using Vizelia.FOL.WF.Common;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessLayer.Broker;
using System.Web;
using System.Net.Configuration;
using System.Configuration;

namespace Vizelia.FOL.WF.Activities {

	/// <summary>
	/// An activity for raising a specific event on the parent ActionRequest of a task in case all tasks state equal a specific value.
	/// </summary>
	[ToolboxBitmap(typeof(EventHandlerActivity), "autocomplete.png")]
	[Designer(typeof(AutoRaiseEventActionRequestActivityDesigner))]
	public sealed class AutoRaiseEventActionRequestActivity : NativeActivity {

		/// <summary>
		/// Initializes a new instance of the <see cref="AutoRaiseEventActionRequestActivity"/> class.
		/// </summary>
		public AutoRaiseEventActionRequestActivity() {
			this.DisplayName = "AutoRaiseEventActionRequest";
		}

		/// <summary>
		/// Gets or sets the event name that should be raised on the action request.
		/// </summary>
		public string ActionRequestEventName { get; set; }

		/// <summary>
		/// Gets or sets the state that all tasks should verify before raising the event on action request.
		/// </summary>
		public string TaskState { get; set; }


		/// <summary>
		/// Creates and validates a description of the activity’s arguments, variables, child activities, and activity delegates.
		/// </summary>
		/// <param name="metadata">The activity’s metadata that encapsulates the activity’s arguments, variables, child activities, and activity delegates.</param>
		protected override void CacheMetadata(NativeActivityMetadata metadata) {
			base.CacheMetadata(metadata);

			if (ActionRequestEventName == null) {
				metadata.AddValidationError(new ValidationError("ActionRequestEventName must be set", false, "ActionRequestEventName"));
			}
			if (TaskState == null) {
				metadata.AddValidationError(new ValidationError("TaskState must be set", false, "TaskState"));
			}

		}

		/// <summary>
		/// When implemented in a derived class, performs the execution of the activity.
		/// </summary>
		/// <param name="context">The execution context under which the activity executes.</param>
		protected override void Execute(NativeActivityContext context) {
			try {

				BaseBusinessEntity entity = WorkflowHelper.GetArgumentEntity(context.DataContext);
				Task task = entity as Task;
				ActionRequest actionRequest = ResolveActionRequest(task);
				List<Task> tasks = ResolveTasks(actionRequest);
			
				// we check that all tasks have state set to TaskState.
				if (tasks.Find(p => p.State != TaskState) == null) {
					try {
						// we raise the event on action request. We try catch because action request could be in a state where it does not support the event.
						WorkflowExtensionService.RaiseEvent(actionRequest, ActionRequestEventName);
						
					}
					catch {}

				}
			}
			catch { }

		}


		/// <summary>
		/// Resolves the action request for a child task.
		/// </summary>
		/// <param name="task">The task.</param>
		/// <returns></returns>
		private ActionRequest ResolveActionRequest(Task task) {
			var actionRequestBroker = Helper.CreateInstance<ActionRequestBroker>();
			var retVal = actionRequestBroker.GetItem(task.KeyActionRequest);
			return retVal;
		}

		/// <summary>
		/// Resolves the list of tasks for an ActionRequest.
		/// </summary>
		/// <param name="actionRequest">The action request.</param>
		/// <returns></returns>
		private List<Task> ResolveTasks(ActionRequest actionRequest) {
			var taskBroker = Helper.CreateInstance<TaskBroker>();
			var retVal = taskBroker.GetListByKeyActionRequest(actionRequest.KeyActionRequest);
			return retVal.records;
		}
	}
}

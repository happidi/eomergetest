﻿using System;
using System.Activities.Presentation;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Activities;
using System.Text.RegularExpressions;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Providers;
using Vizelia.FOL.WF.Activities.Design;
using Vizelia.FOL.WF.Common;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessLayer.Broker;
using System.Web;
using System.Net.Configuration;
using System.Configuration;

namespace Vizelia.FOL.WF.Activities {

	/// <summary>
	/// An activity for creating the workplan of an ActionRequest.
	/// </summary>
	[ToolboxBitmap(typeof(EventHandlerActivity), "workplan.png")]
	[Designer(typeof(CreateWorkplanActivityDesigner))]
	public sealed class CreateWorkplanActivity : NativeActivity {

		/// <summary>
		/// Initializes a new instance of the <see cref="CreateWorkplanActivity"/> class.
		/// </summary>
		public CreateWorkplanActivity() {
			this.DisplayName = "CreateWorkplan";
		}


		/// <summary>
		/// When implemented in a derived class, performs the execution of the activity.
		/// </summary>
		/// <param name="context">The execution context under which the activity executes.</param>
		protected override void Execute(NativeActivityContext context) {
			try {

				BaseBusinessEntity entity = WorkflowHelper.GetArgumentEntity(context.DataContext);
				ActionRequest actionRequest = entity as ActionRequest;
				if (actionRequest == null)
					return;
				var tasks = ResolveWorkplan(actionRequest);
				foreach (Task task in tasks) {
					WorkflowExtensionService.RaiseEvent(task, "");
				}

			}
			catch { }

		}



		/// <summary>
		/// Resolves the mails.
		/// </summary>
		/// <param name="actionRequest">The action request.</param>
		/// <returns></returns>
		private List<Task> ResolveWorkplan(ActionRequest actionRequest) {
			var keyClassificationItemParent = actionRequest.ClassificationKeyParent;
			var keyClassificationItemChildren = actionRequest.ClassificationKeyChildren;
			var classificationItemBroker = Helper.CreateInstance<ClassificationItemBroker>();
			var classificationItemTasks = classificationItemBroker.GetChildren(keyClassificationItemChildren, keyClassificationItemParent + "," + keyClassificationItemChildren);
			var taskBroker = Helper.CreateInstance<TaskBroker>();

			var existingTasks = taskBroker.GetListByKeyActionRequest(actionRequest.KeyActionRequest);
			int countExistingTasks = existingTasks != null ? existingTasks.recordCount : 0;

			var query = classificationItemTasks.OrderBy(p => p.Relation.RelationOrder).ThenBy(p => p.Title);

			return query.Select(classificationItemTask => new Task {
				Name = classificationItemTask.Title,
				Priority = (++countExistingTasks),
				KeyClassificationItem = classificationItemTask.KeyClassificationItem,
				ActualStart = DateTime.Now,
				KeyActionRequest = actionRequest.KeyActionRequest
			}).ToList();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.WF.Common;
using System.Web;
using System.Threading;
using System.Drawing;
using Vizelia.FOL.WF.Activities.Design;
using System.ComponentModel;
using System.Activities.Validation;
using System.Reflection;
using System.Activities.Statements;

namespace Vizelia.FOL.WF.Activities {


	/// <summary>
	/// An event handler activity.
	/// </summary>
	[ToolboxBitmap(typeof(EventHandlerActivity), "event.png")]
	[Designer(typeof(EventHandlerActivityDesigner))]
	public class EventHandlerActivity : NativeActivity {

		/// <summary>
		/// Gets or sets the name of the event.
		/// </summary>
		/// <value>The name of the event.</value>
		public string EventName { get; set; }

		/// <summary>
		/// Gets or sets the IconCls.
		/// </summary>
		/// <value>The IconCls.</value>
		public string IconCls { get; set; }

		/// <summary>
		/// Gets or sets the MsgCode.
		/// </summary>
		/// <value>The MsgCode.</value>
		public string MsgCode { get; set; }

		/// <summary>
		/// Gets or sets the pre action.
		/// </summary>
		public string PreAction { get; set; }

		/// <summary>
		/// Creates and validates a description of the activity’s arguments, variables, child activities, and activity delegates.
		/// </summary>
		/// <param name="metadata">The activity’s metadata that encapsulates the activity’s arguments, variables, child activities, and activity delegates.</param>
		protected override void CacheMetadata(NativeActivityMetadata metadata) {
			base.CacheMetadata(metadata);

			if (EventName == null) {
				metadata.AddValidationError(new ValidationError("EventName must be set", false, "EventName"));
			}

		}

		/// <summary>
		/// This method will:
		///  (1) set the state of the BaseBusinessEntity to be the state of the container State Machine State of this activity
		///  (2) will set the specified bookmark
		/// </summary>
		/// <param name="context">The execution context in which the activity executes.</param>
		protected override void Execute(NativeActivityContext context) {
			BaseBusinessEntity entity = WorkflowHelper.GetArgumentEntity(context.DataContext);
			if (entity is IWorkflowEntity) {
				((IWorkflowEntity)entity).State = WorkflowHelper.GetCurrentState(this);
				((IWorkflowEntity)entity).InstanceId = context.WorkflowInstanceId;
				WorkflowHelper.SetArgumentEntity(entity, context.DataContext);
			}

			context.CreateBookmark(EventName, Callback);

			context.Track(new BusinessEntityTrackingRecord { Entity = entity });
		}

		/// <summary>
		/// The callback is raised from the host; the value should be a BaseBusinessEntity, 
		/// if it is we'll set the main entity to this value;
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="bookmark">The bookmark.</param>
		/// <param name="value">The value.</param>
		private void Callback(NativeActivityContext context, Bookmark bookmark, object value) {
			WorkflowResumeArgument argument = value as WorkflowResumeArgument;
			
			if (argument == null) return;
			// copying the context between the threads.
			HttpContext.Current = argument.CurrentHttpContext;
			Thread.CurrentPrincipal = argument.CurrentHttpContext.User;

			// setting the entity.
			if (!(argument.Entity is BaseBusinessEntity)) return;

			// It may have more steps with the same bookmark name, so we have to add the InstanceId.
			HttpContext.Current.Items.Add("bookmark" + argument.Entity.InstanceId, bookmark.Name);


			WorkflowHelper.SetArgumentEntity(argument.Entity as BaseBusinessEntity, context.DataContext);
			context.Track(new BusinessEntityTrackingRecord { Entity = (BaseBusinessEntity)argument.Entity });
		}

		/// <summary>
		/// Mandatory since we use bookmarks.
		/// </summary>
		/// <returns>true if the activity can cause the workflow to become idle. This value is false by default.</returns>
		protected override bool CanInduceIdle {
			get {
				return true;
			}
		}
	}


}

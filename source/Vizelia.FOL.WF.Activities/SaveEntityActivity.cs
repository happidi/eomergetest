﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Activities;
using Vizelia.FOL.WF.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.WF.Activities.Design;
using System.Drawing;
using System.Activities.Presentation;

namespace Vizelia.FOL.WF.Activities {
	/// <summary>
	/// A custom activity for saving the entity argument of the workflow in database. 
	/// </summary>
	[ToolboxBitmap(typeof(EventHandlerActivity), "save.png")]
	[Designer(typeof(SaveEntityActivityDesigner))]
	public class SaveEntityActivity : CodeActivity {

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveEntityActivity"/> class.
		/// </summary>
		public SaveEntityActivity() {
			this.DisplayName = "SaveEntity";
		}

		/// <summary>
		/// When implemented in a derived class, performs the execution of the activity.
		/// </summary>
		/// <param name="context">The execution context under which the activity executes.</param>
		protected override void Execute(CodeActivityContext context) {
			BaseBusinessEntity entity = WorkflowHelper.GetArgumentEntity(context.DataContext);
			context.Track(new BusinessEntityTrackingRecord { Entity = entity });
		}

	}
}


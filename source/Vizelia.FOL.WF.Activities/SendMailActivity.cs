﻿using System.Activities;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using Vizelia.FOL.WF.Activities.Design;
using Vizelia.FOL.WF.Common;

namespace Vizelia.FOL.WF.Activities {

	/// <summary>
	/// A send mail activity
	/// </summary>
	[ToolboxBitmap(typeof(EventHandlerActivity), "mail.png")]
	[Designer(typeof(SendMailActivityDesigner))]
	public sealed class SendMailActivity : CodeActivity {

		/// <summary>
		/// Initializes a new instance of the <see cref="SendMailActivity"/> class.
		/// </summary>
		public SendMailActivity() {
			this.DisplayName = "SendMail";
		}

		/// <summary>
		/// When implemented in a derived class, performs the execution of the activity.
		/// </summary>
		/// <param name="context">The execution context under which the activity executes.</param>
		protected override void Execute(CodeActivityContext context) {
			try {

				BaseBusinessEntity entity = WorkflowHelper.GetArgumentEntity(context.DataContext);
				ActionRequest actionRequest = entity as ActionRequest;
				if (actionRequest == null)
					return;
				string actionId = (string)HttpContext.Current.Items["bookmark" + actionRequest.InstanceId];

				// fire and forget call
				ThreadPool.QueueUserWorkItem(delegate { SendMails(actionRequest, actionId); });

			}
			catch { }

		}

		/// <summary>
		/// Sends the mails.
		/// </summary>
		/// <param name="actionRequest">The action request.</param>
		/// <param name="actionId">The action id.</param>
		private static void SendMails(ActionRequest actionRequest, string actionId) {
			try {
				PsetBroker psetBroker = Helper.CreateInstance<PsetBroker>();
				OccupantBroker occupantBroker = Helper.CreateInstance<OccupantBroker>();

				// load the pset.
				actionRequest.PropertySetList = psetBroker.GetList(new string[] { actionRequest.KeyActionRequest });
				// load the tasks
				// load the equiments

				// synchronize the state label
				actionRequest.StateLabel = Helper.LocalizeText("msg_workflow_state_" + actionRequest.State.ToLower(), true);

				// build the data model
				var modelData = new ModelDataServiceDesk {
					ActionRequest = actionRequest,
					Owner = occupantBroker.GetItemByKeyActor(actionRequest.KeyApprover) ?? new Occupant(),
					Requestor = occupantBroker.GetItemByKeyActor(actionRequest.Requestor) ?? new Occupant()
				};

				IEnumerable<Mail> mails = ResolveMails(actionRequest, actionId);
				var mailMessages = MailService.ConvertMailsToMailMessages(modelData, mails);
				foreach (var mailMessage in mailMessages) {
					MailService.Send(mailMessage);
				}
			}
			catch { }
		}

		/// <summary>
		/// Resolves the mails.
		/// </summary>
		/// <param name="actionRequest">The action request.</param>
		/// <param name="actionId">The action id.</param>
		/// <returns></returns>
		private static IEnumerable<Mail> ResolveMails(ActionRequest actionRequest, string actionId) {
			var retVal = new List<Mail>();
			var workflowInstanceDefinitionBroker = Helper.CreateInstance<WorkflowInstanceDefinitionBroker>();
			WorkflowInstanceDefinition workflowInstanceDefinition = workflowInstanceDefinitionBroker.GetItemByInstanceId(actionRequest.InstanceId);
			if (workflowInstanceDefinition == null)
				return retVal;
			var mailBroker = Helper.CreateInstance<MailBroker>();
			retVal = mailBroker.GetListFromWorkflow(actionRequest.ClassificationKeyParent, actionRequest.ClassificationKeyChildren, actionRequest.PriorityID, actionRequest.KeyLocation, actionId, workflowInstanceDefinition.WorkflowAssemblyQualifiedName);
			return retVal;
		}
	}
}

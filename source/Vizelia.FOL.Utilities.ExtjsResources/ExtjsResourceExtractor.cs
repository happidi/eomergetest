﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Vizelia.FOL.Utilities.CommandLine;

namespace Vizelia.FOL.Utilities.ExtjsResources {

	/// <summary>
	/// Private Helper for storing the assembly declaration and avoid duplicates
	/// </summary>
	class Helper {
		private static Dictionary<string, string> _assemblyDeclarations;
		private static string _nameSpace;

		public static Dictionary<string, string> AssemblyDeclarations {
			get {
				if (_assemblyDeclarations == null) {
					_assemblyDeclarations = new Dictionary<string, string>();
				}
				return _assemblyDeclarations;
			}
		}

		public static string NameSpace {
			get {
				return _nameSpace;
			}
			set {
				_nameSpace = value;
			}
		}
	};

	/// <summary>
	/// Provides a rewritten version of an css file containing url declaration
	/// and exposes the list of assembly declaration that should be added to the resource assembly
	/// You have to change the css embedded file with the rewritten one, add the assembly declaration, and make sure the resources are marked as embedded into the assembly
	/// Usage : ExtjsResourceExtractor [/S source] [/D destination] [/N namespace]
	/// </summary>
	class ExtjsResourceExtractor {

		/// <summary>
		/// Message which describes how to use the program
		/// </summary>
		static private void ShowUsage() {
			Console.WriteLine();
			Console.WriteLine("ExtjsResources.exe [/S:source] [/D:destination] [/N:namespace]");
			Console.WriteLine("");
			Console.WriteLine("    /S\t\tName of the source css file");
			Console.WriteLine("    source\tExemple : D:\\source\\ext-all.css");
			Console.WriteLine("");
			Console.WriteLine("    /D\t\tName of the destination folder");
			Console.WriteLine("    destination\tExample : D:\\temp");
			Console.WriteLine("");
			Console.WriteLine("    /N\t\tName of the namespace");
			Console.WriteLine("    namespace\tExample : Vizelia.FOL.Web.Extensions.Extjs.v2_2_1");
            Console.WriteLine("");
            Console.WriteLine("ExtjsResources.exe /S:D:\\source\\ext-all.css /D:d:\\temp /N:Vizelia.FOL.Web.Extensions.Extjs.v2_2_1");
            Console.WriteLine("");
            Console.WriteLine("After execution css file should be replaced and the declaration in .cs file for images should be added to the assembly declarations");
        }

		/// <summary>
		/// Main function
		/// </summary>
		/// <param name="args"></param>
		static void Main(string[] args) {

			if (args.Length != 3) {
				ShowUsage();
				return;
			}
			if ((args.Length != 0) && (args[0].Contains("?"))) {
				ShowUsage();
				return;
			}
			Arguments CommandLine = new Arguments(args);
			bool flgArgumentValid = true;
			if (CommandLine["S"] == null) {
				Console.WriteLine("Argument S not supplied");
				flgArgumentValid = false;
			}
			if (CommandLine["D"] == null) {
				Console.WriteLine("Argument D not supplied");
				flgArgumentValid = false;
			}
			if (CommandLine["N"] == null) {
				flgArgumentValid = false;
				Console.WriteLine("Argument N not supplied");
			}
			if (!flgArgumentValid)
				return;

			CreateCssFile(CommandLine["S"], CommandLine["D"], CommandLine["N"]);
		}

		/// <summary>
		/// Creates the css file, and the assembly declaration file
		/// </summary>
		/// <param name="sourceCssPath">Name of the css file</param>
		/// <param name="destinationFolder">Destination folder</param>
		/// <param name="nameSpace">Namespace for rewritten urls</param>
		static void CreateCssFile(string sourceCssPath, string destinationFolder, string nameSpace) {
			if (!File.Exists(sourceCssPath))
				throw new FileNotFoundException();

			Helper.NameSpace = nameSpace;

			FileInfo fileSourceCss = new FileInfo(sourceCssPath);
			StreamWriter destinationCss = new StreamWriter(destinationFolder + @"\" + fileSourceCss.Name);
			StreamWriter destinationAssembly = new StreamWriter(destinationFolder + @"\" + fileSourceCss.Name.Replace(fileSourceCss.Extension, ".cs"));
			StreamReader source = new StreamReader(sourceCssPath);

			string line;
			while ((line = source.ReadLine()) != null) {
				if (line.Contains("url"))
					WriteChangeUrl(line, destinationCss, destinationAssembly);
				else
					destinationCss.WriteLine(line);
			}
			destinationCss.Close();
			destinationAssembly.Close();
		}

		static void WriteChangeUrl(string line, StreamWriter cssFile, StreamWriter assemblyFile) {
			int startIndex = line.IndexOf("url");
			int lastIndex = line.IndexOf(")", startIndex);

			string url = line.Substring(startIndex, lastIndex - startIndex + 1);
			url = url.Replace(" ", "");
			url = url.Replace(@"../", "");
			url = url.Replace("'", "");
			url = url.Replace("/", ".");
			url = url.Trim();
			string urlAssembly = url.Substring(url.IndexOf("(") + 1, url.IndexOf(")") - url.IndexOf("(") - 1);

			urlAssembly = String.Format("[assembly: WebResource(\"{0}.resources.{1}\",\"image/{2}\")]", Helper.NameSpace, urlAssembly, urlAssembly.Substring(urlAssembly.Length - 3, 3));
			url = url.Replace("url(", String.Format("url(<%=WebResource(\"{0}.resources.", Helper.NameSpace));
			url = url.Replace(")", "\")%>");
			line = line.Substring(0, startIndex) + url + line.Substring(lastIndex);
			cssFile.WriteLine(line);

			if (!Helper.AssemblyDeclarations.ContainsKey(urlAssembly)) {
				Helper.AssemblyDeclarations.Add(urlAssembly, urlAssembly);
				assemblyFile.WriteLine(urlAssembly);
			}
		}
	}
}

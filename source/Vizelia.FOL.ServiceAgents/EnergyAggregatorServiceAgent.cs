﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.ServiceAgents {
	/// <summary>
	/// ReportingServices static service agent to abstract the communication with ReportingServices webservices.
	/// </summary>
	public class EnergyAggregatorServiceAgent : MapReduceServiceAgent<IEnergyAggregatorWCF> {
		/// <summary>
		/// constructor.
		/// </summary>
		public EnergyAggregatorServiceAgent(IEnumerable<MachineInstance> machines)
			: base("VizeliaEnergyAggregatorSection", machines) {
			if (Agents == null || Agents.Count == 0) {
				throw new NullReferenceException("EnergyAggregator agents cannot be null or empty");
			}
		}

		/// <summary>
		/// Determines whether the service agent is ready to receive notifications.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is ready for notifications; otherwise, <c>false</c>.
		/// </value>
		public bool IsReadyForNotifications {
			get {
				bool isReady =
					Agents.Any() &&
					Agents.All(a => a.IsInitialized(ConstructEnergyAggregatorContext()));

				return isReady;
			}
		}

		/// <summary>
		/// Determines whether crud notifications are in progress
		/// </summary>
		public bool IsCrudNotificationInProgress {
			get {
				bool isReady =
					Agents.Any() &&
					Agents.All(a => a.IsCrudNotificationInProgress(ConstructEnergyAggregatorContext()));

				return isReady;
			}
		}


		/// <summary>
		/// Process all the enabled AlarmDefinition to generate the AlarmInstances.
		/// </summary>
		/// <param name="alarms">The alarms.</param>
		/// <returns></returns>
		public Dictionary<AlarmDefinition, List<AlarmInstance>> AlarmDefinition_Process(List<AlarmDefinition> alarms) {
			Task<Dictionary<AlarmDefinition, List<AlarmInstance>>> task = null;
			var culture = Helper.GetCurrentUICulture();
			try {
				task = MapReduceHelper.Start(
					aggregator => {
						var retVal = aggregator.AlarmDefinition_Process(ConstructEnergyAggregatorContext(), alarms, culture);
						return retVal;
					},
						AlarmDefinitions_ProcessReduce,
					Agents.ToArray());
				task.Wait();
			}
			catch {
			}
			if (task == null)
				return null;
			if (task.Exception != null) {
				throw task.Exception;
			}
			return task.Result;
		}

		/// <summary>
		/// Reduce the passed array of List of AlarmInstance.
		/// </summary>
		/// <param name="array">The array.</param>
		/// <returns></returns>
		private Dictionary<AlarmDefinition, List<AlarmInstance>> AlarmDefinitions_ProcessReduce(Dictionary<AlarmDefinition, List<AlarmInstance>>[] array) {
			var retVal = new Dictionary<AlarmDefinition, List<AlarmInstance>>();
			if (array != null) {
				foreach (var t in array) {
					if (t != null) {
						foreach (var kvp in t) {
							List<AlarmInstance> list;
							if (retVal.TryGetValue(kvp.Key, out list) == false) {
								retVal.Add(kvp.Key, kvp.Value);
							}
							else {
								list.AddRange(kvp.Value);
							}
						}
					}
				}
			}
			return retVal;
		}

		/// <summary>
		/// Process all the enabled MeterValidationRule to generate the AlarmInstances.
		/// </summary>
		/// <param name="rules">The rules.</param>
		/// <returns></returns>
		public Dictionary<MeterValidationRule, List<AlarmInstance>> MeterValidationRule_Process(List<MeterValidationRule> rules) {
			Task<Dictionary<MeterValidationRule, List<AlarmInstance>>> task = null;
			var culture = Helper.GetCurrentUICulture();
			try {
				task = MapReduceHelper.Start(
					aggregator => {
						var retVal = aggregator.MeterValidationRule_Process(ConstructEnergyAggregatorContext(), rules, culture);
						return retVal;
					},
						MeterValidationRules_ProcessReduce,
					Agents.ToArray());
				task.Wait();
			}
			catch {
			}
			if (task == null)
				return null;
			if (task.Exception != null) {
				throw task.Exception;
			}
			return task.Result;
		}

		/// <summary>
		/// Reduce the passed array of List of AlarmInstance.
		/// </summary>
		/// <param name="array">The array.</param>
		/// <returns></returns>
		private Dictionary<MeterValidationRule, List<AlarmInstance>> MeterValidationRules_ProcessReduce(Dictionary<MeterValidationRule, List<AlarmInstance>>[] array) {
			var retVal = new Dictionary<MeterValidationRule, List<AlarmInstance>>();
			if (array != null) {
				foreach (var t in array) {
					if (t != null) {
						foreach (var kvp in t) {
							List<AlarmInstance> list;
							if (retVal.TryGetValue(kvp.Key, out list) == false) {
								retVal.Add(kvp.Key, kvp.Value);
							}
							else {
								list.AddRange(kvp.Value);
							}
						}
					}
				}
			}
			return retVal;
		}


		// <summary>
		// Check the different agents to see if one needs to be initialized.
		// </summary>
		//public bool AreInitializedRemoteAgents() {
		//    var retVal = true;
		//    int total = 0;
		//    for (var i = 0; i < Agents.Count; i++) {
		//        var agent = Agents[i];
		//        try {
		//            if (!agent.IsInitialized(ConstructEnergyAggregatorContext()))
		//                retVal = false;
		//            else
		//                total += agent.GetMeterDataCount(ConstructEnergyAggregatorContext());
		//        }
		//        catch (Exception ex) {
		//            retVal = false;
		//            TracingService.Write(ex);
		//        }
		//    }
		//    return retVal;
		//}

		/// <summary>
		/// Sends a crud notification when a base business entity that is stored in memory needs to be updated, create or deleted.
		/// </summary>
		/// <param name="notifications">The notifications.</param>
		public void CrudNotification(List<CrudInformation> notifications) {
			if (!IsReadyForNotifications) {
				throw new VizeliaException(Langue.error_chart_energyaggregatorloadingmeterdata);
			}

			foreach (var agent in Agents) {
				agent.CrudNotification(ConstructEnergyAggregatorContext(), notifications);
			}
		}



		///// <summary>
		///// Initialiazes the remote agents data.
		///// </summary>
		//public void InitializeRemoteAgents() {
		//    var dateRanges = GetAgentsDateRange();
		//    var tenantList = Helper.Resolve<ICoreWCF>().Tenant_GetAll(new PagingParameter() {start = 0, limit = 0}, PagingLocation.Database);
		//    List<string> applicationList = tenantList.Select(tenant => tenant.Name).ToList();

		//    for (var i = 0; i < Agents.Count; i++) {
		//        var agent = Agents[i];
		//        var dateRange = dateRanges[i];
		//        var list = new List<EnergyAggregatorContext>();
		//        foreach (var applicationName in applicationList) {
		//            //if (!agent.IsInitialized(applicationName)) { // no need to do this (12/04/2012) as the initialization is performed on the server
		//                list.Add(new EnergyAggregatorContext() {ApplicationName = applicationName, StartDate = dateRange.Item1, EndDate = dateRange.Item2});
		//            //}
		//        }

		//        agent.Initialize(list);
		//    }
		//}


		/// <summary>
		/// Initialiazes the remote agents data.
		/// </summary>
		public void InitializeRemoteAgents() {
			List<string> tenants;
			// get all tenants
			if (TenantProvisioningService.Mode == MultitenantHostingMode.Shared) {
				var tenantList = Helper.Resolve<ITenancyWCF>().Tenant_GetAll(new PagingParameter { start = 0, limit = 0 }, PagingLocation.Database);
				tenants = tenantList.Select(tenant => tenant.Name).ToList();
			}
			else {
				// if this is isolated mode we get only the current tenant.
				tenants = new List<string> { ContextHelper.ApplicationName };
			}

			InitializeRemoteAgents(tenants);
		}

		/// <summary>
		/// Initializes the remote agents.
		/// </summary>
		/// <param name="tenants">The tenants.</param>
		public void InitializeRemoteAgents(IEnumerable<string> tenants) {
			// endpoint address is the the url of the proxy
			var dictionary = new Dictionary<IClientChannel, List<EnergyAggregatorContext>>();

			foreach (var tenant in tenants) {
				TenantHelper.RunInDifferentTenantContext(tenant, () => {
					var dateRanges = GetAgentsDateRange();
					for (var i = 0; i < Agents.Count; i++) {
						var agent = Agents[i];
						var dateRange = dateRanges[i];
						var icc = (IClientChannel)agent;
						if (!dictionary.ContainsKey(icc)) {
							dictionary.Add(icc, new List<EnergyAggregatorContext>());
						}
						dictionary[icc].Add(new EnergyAggregatorContext {
							ApplicationName = ContextHelper.ApplicationName,
							StartDate = dateRange.Item1,
							EndDate = dateRange.Item2
						});
					}
				});
			}

			foreach (IClientChannel channel in dictionary.Keys) {
				var energyAggregatorProxy = (IEnergyAggregatorWCF)channel;
				energyAggregatorProxy.Initialize(dictionary[channel]);
			}
		}


		/// <summary>
		/// Gets the agents date range.
		/// </summary>
		/// <returns></returns>
		private Tuple<DateTime, DateTime>[] GetAgentsDateRange() {
			//TODO : Get this from MeterData Storage
			var retVal = new Tuple<DateTime, DateTime>[Agents.Count];

			var meterDataStartDate = new DateTime(VizeliaConfiguration.Instance.Deployment.MeterStartYear, 1, 1);
			var meterDataEndDate = new DateTime(DateTime.Today.Year + VizeliaConfiguration.Instance.Deployment.MeterDataEndYearOffsetFromCurrentDate, 1, 1);

			var totalNumberOfYears = meterDataEndDate.Year - meterDataStartDate.Year;
			int instanceNumberOfYears = (int)Math.Truncate((double)(totalNumberOfYears / Agents.Count)) + 1;

			var startDate = new DateTime(meterDataStartDate.Year, 1, 1);
			var endDate = startDate.AddYears(instanceNumberOfYears).AddSeconds(-1);//AddMilliseconds(-1);

			for (var i = 0; i < Agents.Count; i++) {
				retVal[i] = new Tuple<DateTime, DateTime>(startDate, endDate);
				startDate = startDate.AddYears(instanceNumberOfYears);
				endDate = startDate.AddYears(instanceNumberOfYears);
			}

			return retVal;
		}

		/// <summary>
		/// Calls all the EnergyAggregator instances to process the Chart (partitioned by Year) and adds the differents results to the Series collecion.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="meterKeys">The meter keys.</param>
		/// <param name="locationKeys">The location keys.</param>
		/// <returns></returns>
		public Chart Chart_Process(Chart chart, List<int> meterKeys, List<string> locationKeys) {
			return Chart_Process(chart, meterKeys, locationKeys, null);
		}

		/// <summary>
		/// Calls all the EnergyAggregator instances to process the Chart (partitioned by Year) and adds the differents results to the Series collecion.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="meterKeys">The meter keys.</param>
		/// <param name="locationKeys">The location keys.</param>
		/// <param name="analysis">The historical analysis.</param>
		/// <returns></returns>
		public Chart Chart_Process(Chart chart, List<int> meterKeys, List<string> locationKeys, ChartHistoricalAnalysis analysis) {
			Task<Chart> task = null;
			var locationFilter = FilterHelper.GetFilter<Location>();
			var culture = Helper.GetCurrentUICulture();
			try {
				task = MapReduceHelper.Start(
					aggregator => {
						var retVal = aggregator.Chart_Process(ConstructEnergyAggregatorContext(), chart, meterKeys, locationKeys, analysis, locationFilter, culture);
						return retVal;
					},
					Chart_ProcessReduce,
					Agents.ToArray());
				task.Wait();
			}
			catch {
			}

			if (task == null)
				return null;
			if (task.Exception != null) {
				throw task.Exception;
			}

			var taskResult = task.Result;
			return taskResult;
		}

		/// <summary>
		/// Reduces the specified array.
		/// </summary>
		/// <param name="array">The array.</param>
		/// <returns></returns>
		private Chart Chart_ProcessReduce(Chart[] array) {
			if (array.Length == 1)
				return array[0];

			DataSerieCollection retVal = new DataSerieCollection();
			Chart chart = null;
			if (array.Length > 0) {
				var dictionary = new Dictionary<string, DataSerie>();
				foreach (var chartTmp in array) {
					if (chart == null)
						chart = chartTmp;
					var serieCollection = chartTmp.Series;
					if (serieCollection != null) {
						#region We concatenate the data
						foreach (var s in serieCollection.AsEnumerable()) {
							DataSerie existingSerie;
							if (dictionary.TryGetValue(s.LocalId, out existingSerie)) {
								existingSerie.Points.AddRange(s.Points);
							}
							else {
								dictionary.Add(s.LocalId, s);
							}
						}
						#endregion
					}
				}
				#region Global Time Interval (we need to recalculate for Max, Min, Avg)
				if (chart.TimeInterval == AxisTimeInterval.Global) {
					foreach (var s in dictionary.Values) {
						switch (s.GroupOperation) {
							case MathematicOperator.AVG:
								retVal.Add(DataSerieHelper.DataSerieProjection(s, DataSerieProjection.Average));
								break;

							case MathematicOperator.MAX:
								retVal.Add(DataSerieHelper.DataSerieProjection(s, DataSerieProjection.Maximum));
								break;

							case MathematicOperator.MIN:
								retVal.Add(DataSerieHelper.DataSerieProjection(s, DataSerieProjection.Minimum));
								break;

							case MathematicOperator.SUM:
								retVal.Add(DataSerieHelper.DataSerieProjection(s, DataSerieProjection.Sum));
								break;
						}
					}
				}
				#endregion
				else {
					retVal.AddRange(dictionary.Values.ToList());
				}
			}
			chart.Series = retVal;
			return chart;
		}


		/// <summary>
		/// Calls all the EnergyAggregator instances to process the Chart.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public Chart Chart_Process(string key, Chart chart = null) {
			var culture = Helper.GetCurrentUICulture();
			var locationFilter = FilterHelper.GetFilter<Location>();
			var retVal = Agents.First().Chart_ProcessFromKey(ConstructEnergyAggregatorContext(), key, locationFilter, culture, chart);
			return retVal;
		}

		/// <summary>
		/// Calls all the EnergyAggregator instances to process the Chart.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public List<Chart> Chart_Process(List<string> keys) {
			var culture = Helper.GetCurrentUICulture();
			var locationFilter = FilterHelper.GetFilter<Location>();
			var retVal = Agents.First().Chart_ProcessList(ConstructEnergyAggregatorContext(), keys, locationFilter, culture);
			return retVal;
		}

		/// <summary>
		/// Return a list of available series localid for a specific Chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="meterKeys">The meter keys.</param>
		/// <param name="locationKeys">The location keys.</param>
		/// <param name="analysis">The historical analysis.</param>
		/// <param name="includeExisting">True to include existing serie, false to return only series to be created.</param>
		/// <returns></returns>
		public List<ListElement> Chart_GetDataSerieLocalId(Chart chart, List<int> meterKeys, List<string> locationKeys, ChartHistoricalAnalysis analysis, bool includeExisting) {
			var culture = Helper.GetCurrentUICulture();
			var locationFilter = FilterHelper.GetFilter<Location>();
			return Agents.First().Chart_GetDataSerieLocalId(ConstructEnergyAggregatorContext(), chart, meterKeys, locationKeys, analysis, locationFilter, includeExisting, culture);
		}


		/// <summary>
		/// Gets a json store for the business entity MeterData.
		/// </summary>
		/// <param name="meter">The meter.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="rawData">if set to <c>true</c> [raw data].</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<MeterData> MeterData_GetStoreFromMeter(Meter meter, PagingParameter paging, DateTime? startDate, DateTime? endDate, bool rawData) {
			JsonStore<MeterData> retVal = new JsonStore<MeterData>();

			if (Agents.Count == 1) {
				retVal = Agents[0].MeterData_GetStoreFromMeter(ConstructEnergyAggregatorContext(), meter, paging, startDate, endDate, rawData);
			}
			else {
				var data = new List<MeterData>();
				foreach (var agent in Agents) {
					var store = agent.MeterData_GetStoreFromMeter(ConstructEnergyAggregatorContext(), meter,
						//if we have more than one agent we need to page on the front end side
					 new PagingParameter {
						 limit = 0,
						 start = 0
					 },
					startDate,
					endDate,
					rawData);
					if (store != null && store.recordCount > 0)
						data.AddRange(store.records);
				}
				retVal = data.ToJsonStoreWithFilter(paging);
			}
			return retVal;
		}

		/// <summary>
		/// Get the last meter data for a specific Meter (sorted by AcquisitionDateTime).
		/// </summary>
		/// <param name="meter">The meter.</param>
		/// <returns></returns>
		public MeterData MeterData_GetLastFromMeter(Meter meter) {
			MeterData retVal = null;
			foreach (var agent in Agents) {
				var md = agent.MeterData_GetLastFromMeter(ConstructEnergyAggregatorContext(), meter);
				if (retVal == null || md.AcquisitionDateTime > retVal.AcquisitionDateTime)
					retVal = md;
			}

			return retVal;
		}


		/// <summary>
		/// Returns the list of existing events title from a specific calendar category.
		/// </summary>
		/// <param name="category">The category.</param>
		/// <param name="meter">The meter.</param>
		/// <returns></returns>
		public List<string> Calendar_GetEventsTitle(CalendarEventCategory category, Meter meter) {
			List<string> retVal = new List<string>();
			foreach (var agent in Agents) {
				var list = agent.Calendar_GetEventsTitle(ConstructEnergyAggregatorContext(), category, meter);
				retVal.AddRange(list);
			}
			retVal = retVal.Distinct().ToList();
			return retVal;
		}


		/// <summary>
		/// Update the machine instance status and memory
		/// </summary>
		/// <returns></returns>
		public void UpdateMachineInstance(MachineInstance instance) {
			var agent = MachineInstances[instance];
			if (agent != null) {
				try {
					//TODO: Dan needs to replace this with an enum
					if (instance.Status.StartsWith("Ready")) {
						if (agent.IsLoadingMeterData(ConstructEnergyAggregatorContext())) {
							instance.Status = string.Format("{0} - {1}", "Ready", Langue.msg_machineinstance_loadingmeterdata);
						}
						else {
							instance.InMemoryObjectCount = agent.GetMeterDataCount(ConstructEnergyAggregatorContext());
						}
						if (!agent.IsInitialized(ConstructEnergyAggregatorContext())) {
							instance.Status = string.Format("{0} - {1}", "Ready", Langue.msg_machineinstance_uninitialized);
						}
					}
				}
				catch (SystemException e) {
					instance.Status = e.ToString();
				}
			}
		}

		/// <summary>
		/// Constructs the energy aggregator context.
		/// </summary>
		/// <returns></returns>
		EnergyAggregatorContext ConstructEnergyAggregatorContext() {
			var dateRange = CacheService.Get(CacheKey.const_cache_ea_context, GetAgentsDateRange, location: CacheLocation.Memory);
            HttpCookie authCookie = HttpContext.Current == null ? null : HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            var retval = new EnergyAggregatorContext
            {
                ApplicationName = ContextHelper.ApplicationName,
                StartDate = dateRange[0].Item1,
                EndDate = dateRange[0].Item2,
                Culture = Thread.CurrentThread.CurrentCulture.ToString(),
                Username = Helper.GetCurrentUserName(),
                TenantUrl = ContextHelper.Url,
                LoginCookie = authCookie == null ? null : authCookie.Value,
                PageId = HttpContext.Current == null ? null : HttpContext.Current.Request.Headers[SessionService.const_page_id]
            };
            return retval;
		}

	}
}


﻿using System;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Web;
using System.Xml.Linq;
namespace Vizelia.FOL.ServiceAgents {


	/// <summary>
	/// Service Agent to fetch MapLocation from Address using Bing.
	/// </summary>
	public class BingServiceAgent : MarshalByRefObject {

		private const string const_url = "http://dev.virtualearth.net/REST/v1/Locations?q={0}&key={1}&output=json";

		/// <summary>
		/// Returns a list of WeatherLocation based on a search string.
		/// </summary>
		/// <param name="address">the address to search for.</param>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public XDocument GetLocationFromAddress(string address, string key) {
			WebRequest request = WebRequest.Create(string.Format(const_url, HttpUtility.UrlEncode(address), key));
			WebResponse response = request.GetResponse();
			StreamReader reader = new StreamReader(response.GetResponseStream());

			XDocument xmlDoc = XDocument.Load(reader);
			return xmlDoc;
		}

		public dynamic GetLocationFromAddressJson(string address, string key) {
			WebRequest request = WebRequest.Create(string.Format(const_url, HttpUtility.UrlEncode(address), key));
			WebResponse response = request.GetResponse();
			StreamReader reader = new StreamReader(response.GetResponseStream());
			return JsonConvert.DeserializeObject(reader.ReadToEnd());
		}
	}
}


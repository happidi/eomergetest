﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using Vizelia.FOL.ServiceAgents.EWS;
using ValueItemType = Vizelia.FOL.ServiceAgents.EWS.ValueItemType;

namespace Vizelia.FOL.ServiceAgents {
	/// <summary>
	/// EWS Service Agent.
	/// </summary>
	public class EWSServiceAgent : MarshalByRefObject, IDisposable {
		private readonly string m_Url;
		private readonly string m_Username;
		private readonly string m_Password;
		private readonly AuthenticationScheme m_Authentication;

		/// <summary>
		/// Initializes a new instance of the <see cref="EWSServiceAgent"/> class.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <param name="username">The username.</param>
		/// <param name="password">The password.</param>
		/// <param name="authentication">The authentication Scheme.</param>
		public EWSServiceAgent(string url, string username, string password, AuthenticationScheme authentication) {
			m_Url = url;
			m_Username = username;
			m_Password = password;
			m_Authentication = authentication;
		}

		/// <summary>
		/// Creates the client.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <param name="username">The username.</param>
		/// <param name="password">The password.</param>
		/// <param name="authentication">The authentication.</param>
		/// <returns></returns>
		private DataExchangeInterfaceClient CreateClient(string url, string username, string password, AuthenticationScheme authentication) {
			// Trust all certificates 
			ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

			// Build the endpoint uri
			var urlBuilder = new UriBuilder(url);

			// Configure HTTP transport security 
			var transport = urlBuilder.Uri.Scheme.Equals(Uri.UriSchemeHttps) ? new HttpsTransportBindingElement() : new HttpTransportBindingElement();
			transport.MaxBufferSize = int.MaxValue;
			transport.MaxReceivedMessageSize = transport.MaxBufferSize;
			transport.AllowCookies = true;

			// Configure Authentication Scheme
			switch (authentication) {
				case AuthenticationScheme.Anonymous:
					transport.AuthenticationScheme = AuthenticationSchemes.Anonymous;
					break;
				case AuthenticationScheme.Basic:
					transport.AuthenticationScheme = AuthenticationSchemes.Basic;
					break;
				case AuthenticationScheme.Digest:
					transport.AuthenticationScheme = AuthenticationSchemes.Digest;
					break;
			}

			// Configure Message Encoding
			var textelement = new TextMessageEncodingBindingElement { MessageVersion = MessageVersion.Soap12 };

			// Create the binding.
			var myBinding = new CustomBinding();
			myBinding.Elements.Add(textelement);
			myBinding.Elements.Add(transport);

			// Create the client. 
			var client = new DataExchangeInterfaceClient(myBinding, new EndpointAddress(urlBuilder.Uri));

			// Configure the client credentials 
			if (client.ClientCredentials != null) {
				client.ClientCredentials.HttpDigest.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

				// Set the client credentials. 
				if (!String.IsNullOrEmpty(username)) {
					client.ClientCredentials.HttpDigest.ClientCredential.UserName = username;
					client.ClientCredentials.UserName.UserName = username;
				}
				if (!String.IsNullOrEmpty(password)) {
					client.ClientCredentials.HttpDigest.ClientCredential.Password = password;
					client.ClientCredentials.UserName.Password = password;
				}
			}


			return client;
		}

		/// <summary>
		/// Does the action with the client.
		/// </summary>
		/// <param name="action">The action.</param>
		private void DoWithClient(Action<DataExchangeInterfaceClient> action) {
			string key = string.Format("DataExchangeInterfaceClient:{0}@{1}", m_Username, m_Url);
			var client = CacheService.Get(key, () => CreateClient(m_Url, m_Username, m_Password, m_Authentication), 120, CacheLocation.Memory);

			try {
				action(client);
			}
			catch (Exception) {
				// Lets retry since authentication may have expired.
				// Actually, an exception due to "401 Unauthorized" error should rarely happen, as in a case of an 
				// expired authentication cookie, WCF performs the request again on its own and authenticates.
				client = CreateClient(m_Url, m_Username, m_Password, m_Authentication);
				CacheService.MemoryCacheAdd(key, client, 120);
				action(client);
			}
		}

		/// <summary>
		/// Gets the containers.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<ContainerItemType> GetHierarchy(string parentContainerKey = null) {
			string id = parentContainerKey ?? string.Empty;
			var request = new GetContainerItemsRequest { GetContainerItemsIds = new[] { id } };
			GetContainerItemsResponse response = null;

			DoWithClient(client => {
				response = client.GetContainerItems(request);
			});

			return response.GetContainerItemsItems;
		}


		/// <summary>
		/// Gets the endpoints.
		/// </summary>
		/// <param name="endpointIds">The endpoint ids.</param>
		/// <returns></returns>
		public IEnumerable<ValueItemType> GetEndpoints(IEnumerable<string> endpointIds) {
			var request = new GetItemsRequest { GetItemsIds = endpointIds.ToArray() };
			GetItemsResponse response = null;

			DoWithClient(client => {
				response = client.GetItems(request);
			});


			if (response.GetItemsErrorResults != null && response.GetItemsErrorResults.Any()) {
				string errors = string.Join("\r\n", response.GetItemsErrorResults.Select(e => e.Id + ": " + e.Message).ToArray());
				throw new VizeliaException(errors);
			}

			if (response.GetItemsItems == null || response.GetItemsItems.ValueItems == null) {
				throw new VizeliaException("Received null items list.");
			}

			return response.GetItemsItems.ValueItems;
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public void Dispose() {
			// Do nothing. We store the client in the memory cache.
		}

	}
}
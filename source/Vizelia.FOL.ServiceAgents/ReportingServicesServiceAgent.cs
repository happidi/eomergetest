﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.ServiceAgents.ReportServicesProxy;
using Vizelia.FOL.BusinessEntities;
using Schedule = Vizelia.FOL.BusinessEntities.Schedule;

namespace Vizelia.FOL.ServiceAgents {

	/// <summary>
	/// ReportingServices static service agent to abstract the communication with ReportingServices webservices.
	/// </summary>
	public class ReportingServicesServiceAgent : MarshalByRefObject {
		private const string const_timed_subscription = "TimedSubscription";

		/// <summary>
		/// private static proxy to access the reporting services.
		/// </summary>
		private readonly ReportingService2010 m_Proxy;

		private readonly List<Tuple<ExportType, string>> m_FormatConversion;
		
		/// <summary>
		/// static constructor.
		/// </summary>
		public ReportingServicesServiceAgent() {
			m_Proxy = new ReportingService2010 { Credentials = System.Net.CredentialCache.DefaultNetworkCredentials };

			m_FormatConversion = new List<Tuple<ExportType, string>> {
			                                                         	new Tuple<ExportType, string>(ExportType.Pdf, "PDF"),
			                                                         	new Tuple<ExportType, string>(ExportType.Html, "MHTML"),
			                                                         	new Tuple<ExportType, string>(ExportType.CSV, "CSV"),
			                                                         	new Tuple<ExportType, string>(ExportType.ImageTIFF, "IMAGE"),
			                                                         	new Tuple<ExportType, string>(ExportType.ExcelBasic, "EXCEL"),
			                                                         	new Tuple<ExportType, string>(ExportType.XML, "XML"),
			                                                         	new Tuple<ExportType, string>(ExportType.Word, "WORD")
			                                                         };
		}

		/// <summary>
		/// The proxy Reporting Service URL.
		/// </summary>
		public string ReportingServiceUrl {
			get {
				return m_Proxy.Url;
			}
			set {
				m_Proxy.Url = value;
			}
		}

		/// <summary>
		/// Retrieve a list of CatalogItem Children based on an existing path.  Root path is "/".
		/// </summary>
		/// <param name="path">the parent path.</param>
		/// <param name="recursive"><t>true</t> to make the search recursive, <t>false</t> to get only the direct children.</param>
		/// <returns></returns>
		public List<Report> GetChildren(string path, bool recursive) {
			CatalogItem[] items = m_Proxy.ListChildren(path, recursive);
			var retVal = (from item in items
						  select new Report {
							  Id = item.ID,
							  Name = item.Name,
							  Path = item.Path,
							  TypeName = item.TypeName,
							  Description = item.Description
						  }).ToList();

			return retVal;
		}

		/// <summary>
		/// Gets the report parameters.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		public List<ReportParameter> GetReportParameters(string reportPath) {
			ItemParameter[] parameters = m_Proxy.GetItemParameters(reportPath, null, true, null, null);
			IEnumerable<ItemParameter> parametersToShow = parameters.Where(p => IsExtendedPropertiesParameter(p) || IsQueryInputParameter(p));

			var reportParameters =
				parametersToShow.Select(p =>
					new ReportParameter {
						AllowBlank = p.AllowBlank,
						AllowBlankSpecified = p.AllowBlankSpecified,
						DefaultValues = p.DefaultValues,
						DefaultValuesQueryBased = p.DefaultValuesQueryBased,
						Dependencies = p.Dependencies,
						DefaultValuesQueryBasedSpecified = p.DefaultValuesQueryBasedSpecified,
						ErrorMessage = p.ErrorMessage,
						MultiValue = p.MultiValue,
						MultiValueSpecified = p.MultiValueSpecified,
						Name = p.Name,
						Nullable = p.Nullable,
						NullableSpecified = p.NullableSpecified,
						ParameterStateName = p.ParameterStateName,
						ParameterTypeName = p.ParameterTypeName,
						Prompt = p.Prompt,
						PromptUser = p.PromptUser,
						PromptUserSpecified = p.PromptUserSpecified,
						QueryParameter = p.QueryParameter,
						QueryParameterSpecified = p.QueryParameterSpecified,
						ValidValuesQueryBased = p.ValidValuesQueryBased,
						ValidValuesQueryBasedSpecified = p.ValidValuesQueryBasedSpecified,
						IsSelection = p.ValidValues != null && p.ValidValues.Any()
					}).ToList();

			return reportParameters;
		}

		private static bool IsQueryInputParameter(ItemParameter p) {
			return p.PromptUser && !string.IsNullOrEmpty(p.Prompt);
		}

		private static bool IsExtendedPropertiesParameter(ItemParameter p) {
			return p.Name.Equals("ExtendedProperties") && !p.PromptUser && !p.QueryParameter;
		}

		/// <summary>
		/// Gets the report parameter values.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <returns></returns>
		public List<ReportParameterValue> GetReportParameterValues(string reportPath, string parameterName) {
			ItemParameter[] parameters = m_Proxy.GetItemParameters(reportPath, null, true, null, null);

			ItemParameter parameter = parameters.First(p => p.Name.Equals(parameterName));
			List<ReportParameterValue> values = parameter.ValidValues.Select(v => new ReportParameterValue {Label = v.Label, Value = v.Value}).ToList();

			return values;
		}

		/// <summary>
		/// Renames or moves the item.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="newPath">The new path.</param>
		public void MoveItem(string path, string newPath) {
			m_Proxy.MoveItem(path, newPath);
		}

		/// <summary>
		/// Creates the folder.
		/// </summary>
		/// <param name="path">The path.</param>
		public void CreateFolder(string path) {
			if (DoesFolderExist(path)) return;

			// Remove possible "/" at end, so if we get a path ending with "/" we won't take it.
			string pathWithoutLastChar = path.Substring(0, path.Length - 1);
			int slashIndex = pathWithoutLastChar.LastIndexOf("/");

			string parentPath = path.Substring(0, slashIndex);

			if(!parentPath.StartsWith("/")) {
				parentPath = "/" + parentPath;
			}

			string folderName = path.Substring(slashIndex + 1);

			m_Proxy.CreateFolder(folderName, parentPath, null);
		}

		/// <summary>
		/// Deletes the item.
		/// </summary>
		/// <param name="path">The path.</param>
		public void DeleteItem(string path) {
			m_Proxy.DeleteItem(path.TrimEnd('/'));
		}

		/// <summary>
		/// Deletes the folder. All content will be deleted as well.
		/// </summary>
		/// <param name="path">The path.</param>
		public void DeleteFolder(string path) {
			if (!DoesFolderExist(path)) return;

			m_Proxy.DeleteItem(path.TrimEnd('/'));
		}

		/// <summary>
		/// Determines whether the folder exists or not.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <returns></returns>
		private bool DoesFolderExist(string path) {
			bool folderExists;

			try {
				// This is expected to throw if the folder does not exist.
				m_Proxy.ListChildren(path, false);
				folderExists = true;
			}
			catch(Exception e) {
				if (e.Message.Contains("ItemNotFoundException")) {
					folderExists = false;
				}
				else {
					// Some other kind of error.
					throw;
				}
			}

			return folderExists;
		}


		/// <summary>
		/// Deletes the subscription.
		/// </summary>
		/// <param name="subscriptionId">The subscription id.</param>
		public void DeleteSubscription(string subscriptionId) {
			m_Proxy.DeleteSubscription(subscriptionId);
		}

		/// <summary>
		/// Gets the subscriptions.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="creatorId">The creator id.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="totalCount">The total count.</param>
		/// <returns></returns>
		public List<ReportSubscription> GetSubscriptions(string reportPath, string creatorId, PagingParameter paging, out int totalCount) {
			// We need the paging parameter here because this method works in N+1 web service calls. So we want
			// to minimize the number of calls to just the subscriptions we want to return.

			// Get everything from the web service. Single call that returns all subscriptions.
			Subscription[] listSubscriptions = m_Proxy.ListSubscriptions(reportPath);

			// Extract metadata and compare it with given creator ID (if needs to be compared).
			var subscriptionsWithMetadataForCurrentUser = (from s in listSubscriptions
			                                              let metadata = DeserializeMetadata(s.Description)
			                                              where (creatorId == null || creatorId.Equals(metadata.Creator))
			                                              select new {Subscription = s, Metadata = metadata}).ToList();

			totalCount = subscriptionsWithMetadataForCurrentUser.Count;

			// Do the actual enumeration and paging here. Perform a service call for each item within the paging range only.
			List<ReportSubscription> subscriptions = 
				subscriptionsWithMetadataForCurrentUser
					.Skip(paging.start)
					.Take(paging.limit)
					.Select(pair => GetReportSubscription(pair.Subscription, pair.Metadata))
					.ToList();
			
			return subscriptions;
		}

		private ReportSubscription GetReportSubscription(Subscription s, ReportSubscriptionMetadata metadata) {
			ExtensionSettings extensionSettings;
			string description;
			ActiveState active;
			string status;
			string eventType;
			string matchData;
			ParameterValue[] parameterValues;

			m_Proxy.GetSubscriptionProperties(s.SubscriptionID, out extensionSettings, out description, out active, out status,
			                                  out eventType, out matchData, out parameterValues);

			string recipients = GetParameterValue(extensionSettings, "TO");
			string renderFormat = GetParameterValue(extensionSettings, "RenderFormat");
			ExportType fileFormat = GetExportTypeFromRenderFormat(renderFormat);
			DateTime? lastRun = (s.LastExecutedSpecified && s.LastExecuted != DateTime.MinValue) ? (DateTime?) s.LastExecuted : null;

			var subscription = new ReportSubscription {
			                                          	KeyReportSubscription = s.SubscriptionID,
			                                          	Name = metadata.Name,
			                                          	ReportPath = s.Path,
			                                          	Description = metadata.Description,
			                                          	Recipients = recipients,
			                                          	FileFormat = fileFormat,
			                                          	Creator = metadata.Creator,
														Status = s.Status,
														LastRun = lastRun
			                                          };

			return subscription;
		}

		private static string GetParameterValue(ExtensionSettings extensionSettings, string name) {
			ParameterValue parameterValue = extensionSettings.ParameterValues.OfType<ParameterValue>().First(p => p.Name.Equals(name));
			string value = (parameterValue != null) ? parameterValue.Value : null;

			return value;
		}

		/// <summary>
		/// Creates the subscription.
		/// </summary>
		/// <param name="report">The report.</param>
		/// <param name="subscription">The subscription.</param>
		public void CreateSubscription(string report, ReportSubscription subscription) {
			var metadata = new ReportSubscriptionMetadata {
			                                              	Creator = Helper.GetCurrentActor(),
			                                              	Description = subscription.Description,
			                                              	Name = subscription.Name
			                                              };
			string description = SerializeMetadata(metadata);
			
			string matchData = GetMatchData(subscription.Schedule);
			ExtensionSettings extSettings = GetExtensionSettings(subscription.Recipients, subscription.FileFormat);
			var parameterValues = subscription.ParameterValues.Select(ConvertToParameterValue).ToArray();
			
			m_Proxy.CreateSubscription(report, extSettings, description, const_timed_subscription, matchData, parameterValues);
		}

		private static ParameterValue ConvertToParameterValue(KeyValuePair<string, string> kvp) {
			string value = kvp.Value;

			if(value.StartsWith("/Date(")) {
				// Received form:     "/Date(1349218800000+0200)/"
				// Wanted form:   "\"\\/Date(1351676393889+0200)\\/\""
				value = "\"\\" + value.TrimEnd('/') + "\\/\"";
				var dateTime = Helper.DeserializeJson<DateTime>(value);
				value = dateTime.ToString("o");
			}

			var parameterValue = new ParameterValue {Name = kvp.Key, Value = value};
			return parameterValue;
		}

		private ExtensionSettings GetExtensionSettings(string recipients, ExportType fileFormat) {
			recipients = recipients.Replace(',', ';').Replace(' ',';');
			
			var extensionParams = new List<ParameterValue>();

			extensionParams.Add(new ParameterValue {Name = "TO", Value = recipients});
			//extensionParams.Add(new ParameterValue {Name = "ReplyTo", Value = VizeliaConfiguration.Instance.Mail});
			extensionParams.Add(new ParameterValue {Name = "IncludeReport", Value = "True"});
			extensionParams.Add(new ParameterValue {Name = "RenderFormat", Value = GetRenderFormat(fileFormat)});
			extensionParams.Add(new ParameterValue {Name = "Subject", Value = Langue.msg_report_email_subject + "@ReportName"});
			extensionParams.Add(new ParameterValue {Name = "Comment", Value = string.Empty});
			extensionParams.Add(new ParameterValue {Name = "IncludeLink", Value = "False"});
			extensionParams.Add(new ParameterValue {Name = "Priority", Value = "NORMAL"});

			var extensionSettings = new ExtensionSettings {Extension = "Report Server Email", ParameterValues = extensionParams.ToArray()};

			return extensionSettings;
		}

		private ExportType GetExportTypeFromRenderFormat(string renderFormat) {
			var tuple = m_FormatConversion.FirstOrDefault(t => t.Item2.Equals(renderFormat));

			if (tuple == null) {
				throw new ArgumentOutOfRangeException("renderFormat");
			}

			return tuple.Item1;
		}

		/// <summary>
		/// Gets the file format.
		/// </summary>
		/// <param name="fileFormat">The file format.</param>
		/// <returns></returns>
		private string GetRenderFormat(ExportType fileFormat) {
			var tuple = m_FormatConversion.FirstOrDefault(t => t.Item1.Equals(fileFormat));

			if (tuple == null) {
				throw new ArgumentOutOfRangeException("fileFormat");
			}

			return tuple.Item2;
		}

		private string GetMatchData(Schedule userSchedule) {
			var calendarEvent = (CalendarEvent) userSchedule;
			CalendarHelper.UpdateEventClientDates(calendarEvent);

			var reportSchedule = new ScheduleDefinition {
			                                            	StartDateTime = userSchedule.StartDate.Value,
			                                            	EndDateSpecified = false,
			                                            	Item = GetPattern(userSchedule)
			                                            };

			string xml = Helper.SerializeXml(reportSchedule);
			return xml;
		}

		private RecurrencePattern GetPattern(Schedule schedule) {
			RecurrencePattern pattern;

			switch (schedule.RecurrencePattern.Frequency) {
				case CalendarFrequencyType.None:
				case CalendarFrequencyType.Secondly:
				case CalendarFrequencyType.Yearly:
				case CalendarFrequencyType.Daily:
					throw new ArgumentOutOfRangeException("Frequency disallowed.");
				case CalendarFrequencyType.Minutely:
					pattern = new MinuteRecurrence {MinutesInterval = schedule.RecurrencePattern.Interval};
					break;
				case CalendarFrequencyType.Hourly:
					pattern = new MinuteRecurrence {MinutesInterval = schedule.RecurrencePattern.Interval * 60};
					break;
				case CalendarFrequencyType.Weekly:
					pattern = new WeeklyRecurrence {
					                               	DaysOfWeek = GetDaysOfWeek(schedule),
					                               	WeeksInterval = 1,
					                               	WeeksIntervalSpecified = true
					                               };
					break;
				case CalendarFrequencyType.Monthly:
					if (schedule.RecurrencePattern.BySetPosition != null) {
						pattern = new MonthlyDOWRecurrence {
							MonthsOfYear = GetMonthsOfYear(schedule),
							WhichWeekSpecified = true,
							WhichWeek = GetWeek(schedule.RecurrencePattern.BySetPosition),
							DaysOfWeek = GetDaysOfWeek(schedule)
						};
					}
					else {
						pattern = new MonthlyRecurrence {
							MonthsOfYear = GetMonthsOfYear(schedule),
							Days = string.Join(",", schedule.RecurrencePattern.ByMonthDay.ToArray())
						};
					}
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			return pattern;
		}

		private static DaysOfWeekSelector GetDaysOfWeek(Schedule schedule) {
			return new DaysOfWeekSelector {
			                              	Sunday = schedule.RecurrencePattern.ByDay.Any(d => d.DayOfWeek == DayOfWeek.Sunday),
			                              	Monday = schedule.RecurrencePattern.ByDay.Any(d => d.DayOfWeek == DayOfWeek.Monday),
			                              	Tuesday = schedule.RecurrencePattern.ByDay.Any(d => d.DayOfWeek == DayOfWeek.Tuesday),
			                              	Wednesday = schedule.RecurrencePattern.ByDay.Any(d => d.DayOfWeek == DayOfWeek.Wednesday),
			                              	Thursday = schedule.RecurrencePattern.ByDay.Any(d => d.DayOfWeek == DayOfWeek.Thursday),
			                              	Friday = schedule.RecurrencePattern.ByDay.Any(d => d.DayOfWeek == DayOfWeek.Friday),
			                              	Saturday = schedule.RecurrencePattern.ByDay.Any(d => d.DayOfWeek == DayOfWeek.Saturday)
			                              };
		}

		private WeekNumberEnum GetWeek(IEnumerable<int> bySetPosition) {
			// Gets the month occurance. A number between 5 and -5, 1 is first, -1 is last, etc
			int occuranceInMonth = bySetPosition.FirstOrDefault();
			if (occuranceInMonth < 0) {
				occuranceInMonth = 6 + occuranceInMonth; //occurance is between 5 and -5
			}

			WeekNumberEnum weekNumber;

			switch (occuranceInMonth) {
				case 1:
					weekNumber = WeekNumberEnum.FirstWeek;
					break;
				case 2:
					weekNumber = WeekNumberEnum.SecondWeek;
					break;
				case 3:
					weekNumber = WeekNumberEnum.ThirdWeek;
					break;
				case 4:
					weekNumber = WeekNumberEnum.FourthWeek;
					break;
				case -1:
					weekNumber = WeekNumberEnum.LastWeek;
					break;
				default:
					throw new ArgumentOutOfRangeException("bySetPosition");
			}

			return weekNumber;
		}

		private static MonthsOfYearSelector GetMonthsOfYear(Schedule schedule) {
			return new MonthsOfYearSelector {
			                                	January = schedule.RecurrencePattern.ByMonth.Contains(1),
			                                	February = schedule.RecurrencePattern.ByMonth.Contains(2),
			                                	March = schedule.RecurrencePattern.ByMonth.Contains(3),
			                                	April = schedule.RecurrencePattern.ByMonth.Contains(4),
			                                	May = schedule.RecurrencePattern.ByMonth.Contains(5),
			                                	June = schedule.RecurrencePattern.ByMonth.Contains(6),
			                                	July = schedule.RecurrencePattern.ByMonth.Contains(7),
			                                	August = schedule.RecurrencePattern.ByMonth.Contains(8),
			                                	September = schedule.RecurrencePattern.ByMonth.Contains(9),
			                                	October = schedule.RecurrencePattern.ByMonth.Contains(10),
			                                	November = schedule.RecurrencePattern.ByMonth.Contains(11),
			                                	December = schedule.RecurrencePattern.ByMonth.Contains(12)
			                                };
		}

		[DataContract]
		private class ReportSubscriptionMetadata {
			[DataMember]
			public string Name { get; set; }

			[DataMember]
			public string Description { get; set; }

			[DataMember]
			public string Creator { get; set; }
		}

		private ReportSubscriptionMetadata DeserializeMetadata(string serialized) {
			try {
				var metadata = Helper.DeserializeJson<ReportSubscriptionMetadata>(serialized);
				return metadata;
			}
			catch {
				return new ReportSubscriptionMetadata { Creator = null, Description = string.Empty, Name = string.Empty };
			}
		}

		private string SerializeMetadata(ReportSubscriptionMetadata metadata) {
			string json = Helper.SerializeJson(metadata);
			return json;
		}


	}
}

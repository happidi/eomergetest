﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.IO;
using System.Xml.Linq;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using System.Configuration.Provider;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.ServiceAgents {
	/// <summary>
	/// Service Agent to fetch MapLocation from Address using Bing.
	/// </summary>

	public class AzureServiceAgent : MarshalByRefObject {
		/// <summary>
		/// The azure namespace.
		/// </summary>
		private const string const_azure_namespace = "http://schemas.microsoft.com/windowsazure";

		private const string const_x_ms_version = "2011-06-01";

		/// <summary>
		/// Initializes a new instance of the <see cref="AzureServiceAgent"/> class.
		/// </summary>
		/// <param name="subscriptionId">The subscription id.</param>
		/// <param name="certThumbprint">The cert thumbprint.</param>
		public AzureServiceAgent(string subscriptionId, string certThumbprint) {
			this.SubscriptionId = subscriptionId;

			X509Store certificateStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
			certificateStore.Open(OpenFlags.ReadOnly);
			X509Certificate2Collection certs = certificateStore.Certificates.Find(X509FindType.FindByThumbprint, certThumbprint, false);

			if (certs.Count == 0) {
				throw new ProviderException("Couldn't find the azure certificate with thumbprint:" + certThumbprint);
			}
			this.Certificate = certs[0];
		}

		/// <summary>
		/// Gets or sets the certificate.
		/// </summary>
		/// <value>
		/// The certificate.
		/// </value>
		private X509Certificate Certificate { get; set; }

		/// <summary>
		/// Gets or sets the subscription id.
		/// </summary>
		/// <value>
		/// The subscription id.
		/// </value>
		private string SubscriptionId { get; set; }

		/// <summary>
		/// Creates the deployment.
		/// </summary>
		/// <param name="ServiceName">Name of the service.</param>
		/// <param name="PackageUrlInBlobStorage">The package URL in BLOB storage.</param>
		/// <param name="ServiceConfigurationFileContents">The service configuration file contents.</param>
		/// <param name="Label">The label.</param>
		/// <param name="slot">The slot.</param>
		/// <param name="RequestId">The request id.</param>
		/// <returns></returns>
		public string CreateDeployment(string ServiceName, string PackageUrlInBlobStorage, string ServiceConfigurationFileContents, string Label, DeploymentSlotName slot, out string RequestId) {

			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri("https://management.core.windows.net/" + SubscriptionId + "/services/hostedservices/" + ServiceName + "/deploymentslots/" + slot.ToString()));

			request.Method = "POST";
			request.ClientCertificates.Add(Certificate);
			request.ContentType = "application/xml";
			request.Headers.Add("x-ms-version", const_x_ms_version);
			StringBuilder sbRequestXML = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			sbRequestXML.Append("<CreateDeployment xmlns=\"http://schemas.microsoft.com/windowsazure\">");
			//Required. The name for the deployment. The deployment name must be unique among other deployments for the hosted service
			sbRequestXML.AppendFormat("<Name>{0}</Name>", Guid.NewGuid().ToString());
			//Required. A URL that refers to the location of the service package in the Blob service. The service package must be located in a storage account beneath the same subscription.sbRequestXML.AppendFormat("<Name>{0}</Name>", DeploymentName);
			sbRequestXML.AppendFormat("<PackageUrl>{0}</PackageUrl>", PackageUrlInBlobStorage);
			//Required. A name for the hosted service that is base-64 encoded. The name can be up to 100 characters in length.
			sbRequestXML.AppendFormat("<Label>{0}</Label>", EncodeToBase64String(Label));
			//Required. The base-64 encoded service configuration file for the deployment.
			sbRequestXML.AppendFormat("<Configuration>{0}</Configuration>", EncodeToBase64String(ServiceConfigurationFileContents));
			sbRequestXML.Append("<StartDeployment>true</StartDeployment>");
			sbRequestXML.Append("<TreatWarningsAsError>false</TreatWarningsAsError>");
			sbRequestXML.Append("</CreateDeployment>");


			byte[] formData = UTF8Encoding.UTF8.GetBytes(sbRequestXML.ToString());
			request.ContentLength = formData.Length;

			using (Stream post = request.GetRequestStream()) {
				post.Write(formData, 0, formData.Length);
			}


			var response = (HttpWebResponse)request.GetResponse();
			string retVal = response.StatusCode.ToString();
			RequestId = response.GetResponseHeader("x-ms-request-id");
			return retVal;
		}

		/// <summary>
		/// Creates the hosted service.
		/// </summary>
		/// <param name="ServiceName">Name of the service.</param>
		/// <param name="Label">The label.</param>
		/// <param name="RequestId">The request id.</param>
		/// <returns></returns>
		public string CreateHostedService(string ServiceName, string Label, out string RequestId) {
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri("https://management.core.windows.net/" + SubscriptionId + "/services/hostedservices"));
			request.Method = "POST";
			request.ClientCertificates.Add(Certificate);
			request.ContentType = "application/xml";
			request.Headers.Add("x-ms-version", const_x_ms_version);

			StringBuilder sbRequestXML = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
			sbRequestXML.Append("<CreateHostedService xmlns=\"http://schemas.microsoft.com/windowsazure\">");
			sbRequestXML.AppendFormat("<ServiceName>{0}</ServiceName>", ServiceName);
			sbRequestXML.AppendFormat("<Label>{0}</Label>", EncodeToBase64String(Label));
			sbRequestXML.Append("<Location>North Europe</Location>");
			sbRequestXML.Append("</CreateHostedService>");

			byte[] formData = UTF8Encoding.UTF8.GetBytes(sbRequestXML.ToString());
			request.ContentLength = formData.Length;

			using (Stream post = request.GetRequestStream()) {
				post.Write(formData, 0, formData.Length);
			}

			var response = (HttpWebResponse)request.GetResponse();
			string retVal = response.StatusCode.ToString();
			RequestId = response.GetResponseHeader("x-ms-request-id");

			return retVal;
		}

		/// <summary>
		/// Decodes from base64 string.
		/// </summary>
		/// <param name="base64string">The base64string.</param>
		/// <returns></returns>
		public static string DecodeFromBase64String(string base64string) {
			return Encoding.UTF8.GetString(Convert.FromBase64String(base64string));
		}

		/// <summary>
		/// Encodes to base64 string.
		/// </summary>
		/// <param name="original">The original.</param>
		/// <returns></returns>
		public static string EncodeToBase64String(string original) {
			return Convert.ToBase64String(Encoding.UTF8.GetBytes(original));
		}

		/// <summary>
		/// Gets the deployments.
		/// </summary>
		/// <param name="ServiceName">Name of the service.</param>
		/// <param name="slot">The slot.</param>
		/// <returns></returns>
		public string GetDeployments(string ServiceName, DeploymentSlotName slot) {
			try {
				HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri("https://management.core.windows.net/" + SubscriptionId + "/services/hostedservices/" + ServiceName + "/deploymentslots/" + slot.ToString()));
				request.Method = "GET";
				request.ClientCertificates.Add(Certificate);
				request.Headers.Add("x-ms-version", const_x_ms_version);
				StreamReader sr = new StreamReader(request.GetResponse().GetResponseStream());
				return sr.ReadToEnd();
			}
			catch {
			}
			return null;
		}

		/// <summary>
		/// Gets the operation status.
		/// </summary>
		/// <param name="RequestId">The request id.</param>
		/// <returns></returns>
		public string GetOperationStatus(string RequestId) {
			try {
				HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri("https://management.core.windows.net/" + SubscriptionId + "/operations/" + RequestId));
				request.Method = "GET";
				request.ClientCertificates.Add(Certificate);
				request.Headers.Add("x-ms-version", const_x_ms_version);
				StreamReader sr = new StreamReader(request.GetResponse().GetResponseStream());
				return sr.ReadToEnd();
			}
			catch {
			}
			return null;
		}

		/// <summary>
		/// Gets the operation status.
		/// </summary>
		/// <param name="RequestId">The request id.</param>
		/// <returns></returns>
		public string GetOperationStatusValue(string RequestId) {
			var operationStatus = GetOperationStatus(RequestId);
			var xOperationStatus = XElement.Parse(operationStatus);
			var retVal = xOperationStatus.Element(XName.Get("Status", const_azure_namespace)).Value;
			return retVal;
		}


		/// <summary>
		/// Lists the hosted services.
		/// </summary>
		/// <returns></returns>
		public string ListHostedServices() {
			try {
				HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri("https://management.core.windows.net/" + SubscriptionId + "/services/hostedservices"));
				request.Method = "GET";
				request.ClientCertificates.Add(Certificate);
				request.Headers.Add("x-ms-version", const_x_ms_version);
				StreamReader sr = new StreamReader(request.GetResponse().GetResponseStream());
				return sr.ReadToEnd();
			}
			catch {
			}
			return null;
		}

		/// <summary>
		/// Reboots the specified service name.
		/// </summary>
		/// <param name="ServiceName">Name of the service.</param>
		/// <param name="slot">The slot.</param>
		/// <param name="roleInstanceName">Name of the role instance.</param>
		/// <param name="RequestId">The request id.</param>
		/// <returns></returns>
		public string Reboot(string ServiceName, DeploymentSlotName slot, string roleInstanceName, out string RequestId) {
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri("https://management.core.windows.net/" + SubscriptionId + "/services/hostedservices/" + ServiceName + "/deploymentslots/" + slot.ToString() + "/roleinstances/" + roleInstanceName + "?comp=reboot"));
			request.Method = "POST";
			request.ClientCertificates.Add(Certificate);
			request.ContentType = "application/xml";
			request.Headers.Add("x-ms-version", const_x_ms_version);
			request.ContentLength = 0;
			var response = (HttpWebResponse)request.GetResponse();
			string retVal = response.StatusCode.ToString();
			RequestId = response.GetResponseHeader("x-ms-request-id");
			return retVal;
		}

		/// <summary>
		/// Uploads the file.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="accountName">Name of the account.</param>
		/// <param name="key">The key.</param>
		/// <param name="containerName">Name of the container.</param>
		/// <returns></returns>
		public string UploadFile(string filePath, string accountName, string key, string containerName) {
			var cloud = new CloudStorageAccount(new StorageCredentialsAccountAndKey(accountName, key), true);
			var blobManager = cloud.CreateCloudBlobClient();
			var container = blobManager.GetContainerReference(containerName);
			container.CreateIfNotExist();
			var blob = container.GetBlobReference(Path.GetFileName(filePath));
			blob.UploadFile(filePath);
			string path = blob.Uri.AbsoluteUri;
			return blob.Uri.AbsolutePath;
		}

		/// <summary>
		/// Deletes the hosted service.
		/// </summary>
		/// <param name="ServiceName">Name of the service.</param>
		/// <returns></returns>
		public bool DeleteHostedService(string ServiceName) {
			string Url = string.Format("https://management.core.windows.net/{0}/services/hostedservices/{1}", SubscriptionId, ServiceName);
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(Url));

			request.Method = "DELETE";
			request.ClientCertificates.Add(Certificate);
			request.Headers.Add("x-ms-version", const_x_ms_version);

			var response = (HttpWebResponse)request.GetResponse();

			return response.StatusCode == HttpStatusCode.OK;
		}

		/// <summary>
		/// Deletes the deployment.
		/// </summary>
		/// <param name="ServiceName">Name of the service.</param>
		/// <param name="slot">The slot.</param>
		/// <param name="RequestId">The request id.</param>
		/// <returns></returns>
		public bool DeleteDeployment(string ServiceName, DeploymentSlotName slot, out string RequestId) {

			string Url = string.Format("https://management.core.windows.net/{0}/services/hostedservices/{1}/deploymentslots/{2}", SubscriptionId, ServiceName, slot);

			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(Url));

			request.Method = "DELETE";
			request.ClientCertificates.Add(Certificate);
			request.ContentType = "application/xml";
			request.Headers.Add("x-ms-version", const_x_ms_version);

			var response = (HttpWebResponse)request.GetResponse();

			RequestId = response.GetResponseHeader("x-ms-request-id");

			return response.StatusCode == HttpStatusCode.Accepted;

		}

		/// <summary>
		/// Gets the list of Machine Instances.
		/// </summary>
		/// <returns></returns>
		public List<MachineInstance> GetList() {
			var retVal = new List<MachineInstance>();
			var hostedServicesXml = this.ListHostedServices();
			var xHostedServices = XElement.Parse(hostedServicesXml);

			var hostedServices = xHostedServices.Elements(XName.Get("HostedService", const_azure_namespace)).ToList();
			if (hostedServices != null) {
				foreach (var hostedService in hostedServices) {
					string serviceName = hostedService.Element(XName.Get("ServiceName", const_azure_namespace)).Value;
					string deploymentsXml = this.GetDeployments(serviceName, DeploymentSlotName.production);

					if (deploymentsXml != null)
					{
						var xDeployments = XElement.Parse(deploymentsXml);
						try
						{
							string url = xDeployments.Element(XName.Get("Url", const_azure_namespace)).Value;
							string deploymentLabel =
								DecodeFromBase64String(xDeployments.Element(XName.Get("Label", const_azure_namespace)).Value);

							var roleInstanceListElement = xDeployments.Element(XName.Get("RoleInstanceList", const_azure_namespace));

							foreach (var roleInstance in roleInstanceListElement.Elements(XName.Get("RoleInstance", const_azure_namespace)))
							{
								var roleName = roleInstance.Element(XName.Get("RoleName", const_azure_namespace)).Value;
								var instanceName = roleInstance.Element(XName.Get("InstanceName", const_azure_namespace)).Value;
								var instanceStatus = roleInstance.Element(XName.Get("InstanceStatus", const_azure_namespace)).Value;
								var instanceSize = roleInstance.Element(XName.Get("InstanceSize", const_azure_namespace)).Value;

								retVal.Add(new MachineInstance()
								           	{
								           		KeyMachineInstance =
								           			retVal.Count.ToString() + "|" + deploymentLabel + "|" + roleName + "|" + instanceName,
								           		InstanceName = instanceName,
								           		Status = instanceStatus,
								           		MachinePurpose = roleName,
								           		EnvironmentName = serviceName,
								           		AbsoluteUri = url,
								           		Size = (MachineInstanceSize) Enum.Parse(typeof (MachineInstanceSize), instanceSize)
								           	});
							}
						}
						catch
						{
							;
						}
					}
				}
			}
			return retVal;
		}
	}

	/// <summary>
	/// Azure Deployment Slot Names
	/// </summary>
	public enum DeploymentSlotName {
		/// <summary>
		///  Staging slot (lower case in purpose for azure compatibility)
		/// </summary>
		staging,
		/// <summary>
		///  Production slot (lower case in purpose for azure compatibility)		
		/// </summary>
		production
	}

}

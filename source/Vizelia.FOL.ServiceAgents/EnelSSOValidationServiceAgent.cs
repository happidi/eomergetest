﻿using System;
using System.ServiceModel;
using Vizelia.FOL.ServiceAgents.EnelSSOValidationService;

namespace Vizelia.FOL.ServiceAgents {
	/// <summary>
	/// The RSP Service Agent.
	/// </summary>
	public class EnelSSOValidationServiceAgent : MarshalByRefObject, IDisposable {
		private readonly ValidateTokenClient m_Client;

		/// <summary>
		/// Initializes a new instance of the <see cref="RSPAuthenticationServiceAgent"/> class.
		/// </summary>
		public EnelSSOValidationServiceAgent() {
			m_Client = new EnelSSOValidationService.ValidateTokenClient();
		}


		/// <summary>
		/// Validates the specified user token pair.
		/// </summary>
		/// <param name="token">The token.</param>
		/// <param name="userId">The user id.</param>
		/// <param name="param">The param for additional parameters, currently not used.</param>
		/// <returns></returns>
		public string Validate(string token, string userId, string param) {
			string response = m_Client.validate(token, userId, param);
			return response;
		}


		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public void Dispose() {
			if (m_Client.State == CommunicationState.Faulted) {
				m_Client.Abort();
			}
			else {
				m_Client.Close();
			}
		}
	}
}
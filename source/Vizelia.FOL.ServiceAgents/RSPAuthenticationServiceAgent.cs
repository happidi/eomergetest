﻿using System;
using System.ServiceModel;
using Vizelia.FOL.ServiceAgents.RSPAuthenticationService;

namespace Vizelia.FOL.ServiceAgents {
	/// <summary>
	/// The RSP Service Agent.
	/// </summary>
	public class RSPAuthenticationServiceAgent : MarshalByRefObject, IDisposable {
		private readonly ServiceManagementCallbackPortClient m_Client;

		/// <summary>
		/// Initializes a new instance of the <see cref="RSPAuthenticationServiceAgent"/> class.
		/// </summary>
		public RSPAuthenticationServiceAgent() {
			m_Client = new ServiceManagementCallbackPortClient("RSPAuthentication");
		}

		// IMPORTANT!
		// In case you ever do an Update Reference, note that you must edit the GetUsersResponse class in Reference.cs
		// such that the XmlElementAttribute on the userType array member will have a namespace:
		// [XmlElementAttribute("userType", Namespace = "http://rsp.schneiderelectric.com/user/services")]
		// public userType[] userType;

		/// <summary>
		/// Authenticates the user.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="password">The password.</param>
		/// <param name="tenantName">Name of the tenant.</param>
		/// <returns>
		/// The user if login was successful, null otherwise.
		/// </returns>
		public userType AuthenticateUser(string username, string password, string tenantName) {
			userType user = m_Client.AuthenticateUser(username, password, tenantName);
			return user;
		}

		/// <summary>
		/// Gets the users.
		/// </summary>
		/// <param name="tenantName">Name of the tenant.</param>
		/// <returns>
		/// The list of users for the given tenant.
		/// </returns>
		public userType[] GetUsers(string tenantName) {
			userType[] users = m_Client.GetUsers(tenantName);
			return users;
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public void Dispose() {
			if(m_Client.State == CommunicationState.Faulted) {
				m_Client.Abort();
			}
			else {
				m_Client.Close();
			}
		}
	}
}
﻿using System;
using Vizelia.FOL.BusinessEntities;
using System.Net;
using System.IO;
using System.Xml;

namespace Vizelia.FOL.ServiceAgents {


	/// <summary>
	/// Service Agent to fetch Weather.com Forecast.
	/// </summary>
	public class WeatherDotComServiceAgent : MarshalByRefObject {

		/// <summary>
		/// Forecast Url template.
		/// </summary>
		private const string const_url = "http://gadgets.weather.com/weather/local/{0}?cc=*&dayf=5";


		/// <summary>
		/// Gets the weather five day forecast Xml document.
		/// </summary>
		/// <param name="location">The WeatherLocation.</param>
		/// <returns></returns>
		public XmlDocument GetWeatherFiveDayForecast(WeatherLocation location) {

			WebRequest request = WebRequest.Create(string.Format(const_url, location.LocationId));
			WebResponse response = request.GetResponse();
			StreamReader reader = new StreamReader(response.GetResponseStream());
			XmlDocument responseXML = new XmlDocument();
			responseXML.Load(reader);

			return responseXML;
		}
	}
}

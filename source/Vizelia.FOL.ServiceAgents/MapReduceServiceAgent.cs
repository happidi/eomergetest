﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.ServiceAgents {

	/// <summary>
	/// MapReduce Generic Service Agent.
	/// </summary>
	/// <typeparam name="TInterface">The WCF interface type.</typeparam>
	public class MapReduceServiceAgent<TInterface> : MarshalByRefObject, IDisposable {
		private List<ChannelFactory<TInterface>> m_ChannelFactories;

		/// <summary>
		/// The tenant proxy dictionary.
		/// </summary>
		protected Dictionary<string, List<TInterface>> TenantAndProxyDictionary;

		/// <summary>
		/// Initializes a new instance of the <see cref="MapReduceServiceAgent&lt;TInterface&gt;"/> class.
		/// </summary>
		/// <param name="sectionName">Name of the config section.</param>
		/// <param name="machines">The machines.</param>
		public MapReduceServiceAgent(string sectionName, IEnumerable<MachineInstance> machines) {
			ConfigureAgents(sectionName, machines);
		}

		/// <summary>
		/// Configures the agents.
		/// </summary>
		/// <param name="sectionName">Name of the config section that list the different instances.</param>
		/// <param name="machines">The machines.</param>
		private void ConfigureAgents(string sectionName, IEnumerable<MachineInstance> machines) {
			TenantAndProxyDictionary = new Dictionary<string, List<TInterface>>();

			m_ChannelFactories = new List<ChannelFactory<TInterface>>();
			MachineInstances = new Dictionary<MachineInstance, TInterface>();
			var conf = Helper.GetConfigurationSection<MapReduceSection>(sectionName);
			var proxyList = new List<TInterface>();

			//Fixed machines defined in the Web.config
			for (var i = 0; i < conf.Instances.Count; i++) {

				var aggregator = conf.Instances[i];
				// if no specific tenant configured for this energy aggregator we use the default applicationName

				var tenants = Helper.Resolve<ITenancyWCF>().Tenant_GetAll(new PagingParameter { start = 0, limit = 0 }, PagingLocation.Database);
				var tenantNames = tenants.Select(x => x.Name).ToArray();

				string[] applicationName = string.IsNullOrEmpty(aggregator.ApplicationName) ? tenantNames : new[] { aggregator.ApplicationName };

				if (aggregator.Protocol == ServiceInvocationProtocol.InMemory) {
					if (aggregator.Type != null) {
						TInterface proxy = (TInterface)Activator.CreateInstance(aggregator.Type);
						InternalAddProxy(applicationName, proxy);
					}
				}
				else if (aggregator.Protocol == ServiceInvocationProtocol.WCFHTTP || aggregator.Protocol == ServiceInvocationProtocol.WCFTCP) {
					var channelFactory = new ChannelFactory<TInterface>(aggregator.EndPoint);
					m_ChannelFactories.Add(channelFactory);
					FixChannelFactory(channelFactory);
					TInterface proxy = channelFactory.CreateChannel();
					InternalAddProxy(applicationName, proxy);
				}
			}

			if (proxyList.Count == 0) {
				//Available machines from the cloud.
				foreach (var machine in machines) {
					//TODO : Dan needs to replace this with mex
					var endpointAddress = new EndpointAddress("net.tcp://" + machine.AbsoluteUri.ToLower().Replace("http://", "").Replace("https://", "").TrimEnd('/') + ":555/tcp");
					var channelFactory = new ChannelFactory<TInterface>(machine.MachinePurpose, endpointAddress);
					m_ChannelFactories.Add(channelFactory);
					FixChannelFactory(channelFactory);
					TInterface proxy = channelFactory.CreateChannel();
					proxyList.Add(proxy);
					MachineInstances.Add(machine, proxy);
				}
			}
		}

		/// <summary>
		/// Internallly add the proxy to the right tenant
		/// </summary>
		/// <param name="applicationNames">The application names.</param>
		/// <param name="proxy">The proxy.</param>
		private void InternalAddProxy(IEnumerable<string> applicationNames, TInterface proxy) {
			foreach (var applicationName in applicationNames) {
				List<TInterface> proxies = null;
				if (TenantAndProxyDictionary.ContainsKey(applicationName)) {
					proxies = TenantAndProxyDictionary[applicationName] ?? new List<TInterface>();
					proxies.Add(proxy);
					TenantAndProxyDictionary[applicationName] = proxies;
				}
				else {
					proxies = new List<TInterface> { proxy };
					TenantAndProxyDictionary.Add(applicationName, proxies);
				}
			}
		}

		/// <summary>
		/// Fixes the channel factory.DAN NEEDS TO FIX THIS
		/// </summary>
		/// <param name="channelFactory">The channel factory.</param>
		private void FixChannelFactory(ChannelFactory<TInterface> channelFactory) {
			var operations = channelFactory.Endpoint.Contract.Operations.ToList();
			var behaviors = operations[0].Behaviors;
			foreach (var b in behaviors) {
				//TODO : DAN NEEDS TO FIX THIS
				if (b is DataContractSerializerOperationBehavior)
					((DataContractSerializerOperationBehavior)b).MaxItemsInObjectGraph = Int32.MaxValue;
			}
		}

		/// <summary>
		/// Gets the agents.
		/// </summary>
		protected List<TInterface> Agents {
			get {
				List<TInterface> retval;
				if (TenantAndProxyDictionary.ContainsKey(ContextHelper.ApplicationName)) {
					retval = TenantAndProxyDictionary[ContextHelper.ApplicationName];
				}
				else {
					retval = new List<TInterface>();
					//retval = m_tenantAndProxyDictionary[VizeliaConfiguration.Instance.Deployment.ApplicationName];
				}
				if (retval.Count == 0)
					throw new VizeliaException("no energy aggregator found for " + ContextHelper.ApplicationName);

				return retval;
			}
		}


		/// <summary>
		/// Gets or sets the machine instances.
		/// </summary>
		/// <value>
		/// The machine instances.
		/// </value>
		protected Dictionary<MachineInstance, TInterface> MachineInstances { get; set; }


		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose() {
			if (Agents != null) {
				foreach (var agent in Agents.OfType<IDisposable>()) {


					agent.Dispose();
				}
			}

			if (m_ChannelFactories != null) {
				foreach (var channelFactory in m_ChannelFactories) {
					if (channelFactory.State != CommunicationState.Closed || channelFactory.State != CommunicationState.Closing) {
						channelFactory.Close();
					}
				}
			}
		}
	}
}

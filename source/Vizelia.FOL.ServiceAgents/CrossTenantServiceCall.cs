﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Utilities.WCFClient.CookieManager;
using Vizelia.FOL.WCFService.Contracts;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ServiceAgents {
	/// <summary>
	/// A helper class for making cross-tenant service calls.
	/// </summary>
	public static class CrossTenantServiceCall {
		/// <summary>
		/// Calls the unauthenticated.
		/// </summary>
		/// <typeparam name="TServiceInterface">The type of the service interface.</typeparam>
		/// <param name="tenantUrl">The tenant URL.</param>
		/// <param name="action">The action.</param>
		/// <param name="protocol">The protocol.</param>
		/// <returns></returns>
		public static Exception CallUnauthenticated<TServiceInterface>(string tenantUrl, Action<TServiceInterface> action, string protocol = null) where TServiceInterface : IBaseModuleWCF {
			var result = Call(tenantUrl, null, null, action, protocol);
			return result;
		}

		/// <summary>
		/// Calls the unauthenticated.
		/// </summary>
		/// <typeparam name="TServiceInterface">The type of the service interface.</typeparam>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <param name="tenantUrl">The tenant URL.</param>
		/// <param name="action">The action.</param>
		/// <param name="protocol">The protocol.</param>
		/// <param name="writeFailureToTrace">if set to <c>true</c> write failure to trace.</param>
		/// <returns></returns>
		public static Tuple<TResult, Exception> CallUnauthenticated<TServiceInterface, TResult>(string tenantUrl, Func<TServiceInterface, TResult> action, string protocol = null, bool writeFailureToTrace = true) where TServiceInterface : IBaseModuleWCF {
			var result = Call(tenantUrl, null, null, action, protocol, writeFailureToTrace);
			return result;
		}

		/// <summary>
		/// Makes an unauthenticated service call to the given service.
		/// </summary>
		/// <typeparam name="TServiceInterface">The type of the service interface.</typeparam>
		/// <param name="tenantUrl">The tenant URL.</param>
		/// <param name="username">The username.</param>
		/// <param name="password">The password.</param>
		/// <param name="action">The action to perform on the service proxy.</param>
		/// <param name="protocol">The protocol.</param>
		/// <param name="writeFailureToTrace">if set to <c>true</c> write failure to trace.</param>
		/// <returns></returns>
		public static Exception Call<TServiceInterface>(string tenantUrl, string username, string password, Action<TServiceInterface> action, string protocol = null, bool writeFailureToTrace = true) where TServiceInterface : IBaseModuleWCF {
			var result = Call<TServiceInterface, object>(tenantUrl, username, password, svc => {
				action(svc);
				return new object();
			}, protocol, writeFailureToTrace);
			return result.Item2;
		}

		/// <summary>
		/// Makes an authenticated service call to the given service and returns the result.
		/// </summary>
		/// <typeparam name="TServiceInterface">The type of the service interface to call a method on.</typeparam>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <param name="tenantUrl">The tenant URL.</param>
		/// <param name="username">The username.</param>
		/// <param name="password">The password.</param>
		/// <param name="action">The action to perform on the service proxy.</param>
		/// <param name="protocol">The protocol.</param>
		/// <param name="writeFailureToTrace"> </param>
		/// <returns></returns>
		public static Tuple<TResult, Exception> Call<TServiceInterface, TResult>(string tenantUrl, string username, string password, Func<TServiceInterface, TResult> action, string protocol = null, bool writeFailureToTrace = true) where TServiceInterface : IBaseModuleWCF {
			ChannelFactory<IPublicWCF> publicFactory = null;
			ChannelFactory<TServiceInterface> serviceChannelFactory = null;
			TResult result = default(TResult);
			Exception exceptionResult = null;
			IPublicWCF publicProxy = null;

			bool loginSucess = false;

			try {
				// Resolve Tenant Endpoint
				if (protocol == null) protocol = VizeliaConfiguration.Instance.Deployment.Protocol;
				Uri baseUri = new Uri((tenantUrl.ToLower().StartsWith(protocol + "://") ? string.Empty : protocol + "://") + tenantUrl.TrimEnd('/') + '/');
				string publicServiceName = GetServiceName<IPublicWCF>();
				Uri publicServiceUri = new Uri(baseUri, publicServiceName + ".svc/soap");
				var cookieManager = CookieManagerEndpointBehavior.CreateNew();
				publicFactory = new ChannelFactory<IPublicWCF>("PublicProxy_" + protocol.ToLower(), new EndpointAddress(publicServiceUri));
				publicFactory.Endpoint.Behaviors.Add(cookieManager);

				// Log in to the tenant, if given login details. Otherwise work unauthenticated.
				if (username != null && password != null) {
					publicProxy = publicFactory.CreateChannel(new EndpointAddress(publicServiceUri));
					Login(username, password, publicProxy);
					loginSucess = true;
				}

				// Invoke remote service
				string serviceName = GetServiceName<TServiceInterface>();
				Uri serviceUri = new Uri(baseUri, serviceName + ".svc/soap");
				serviceChannelFactory = new ChannelFactory<TServiceInterface>(serviceName + "Proxy_" + protocol.ToLower(), new EndpointAddress(serviceUri));
				serviceChannelFactory.Endpoint.Behaviors.Add(cookieManager);
				TServiceInterface serviceProxy = serviceChannelFactory.CreateChannel(new EndpointAddress(serviceUri));
				result = action(serviceProxy);
			}
			catch (Exception ex){
				exceptionResult = ex;

				if (writeFailureToTrace) {
					string category = string.Empty;

					if (ex is CommunicationException) {
						category = "Communication";
					}

					using (TracingService.StartTracing(category, string.Empty)) {
						TracingService.Write(ex);
					}
				}
			}
			finally {
				// Log out of tenant
				if (loginSucess && publicProxy != null) {
					try {
						var channel = publicProxy as IChannel;

						if (channel != null && channel.State != CommunicationState.Faulted) {
							publicProxy.Logout();
						}
					}
					catch (Exception exception) {
						exceptionResult = exception;
						
						if (writeFailureToTrace) {
							TracingService.Write(TraceEntrySeverity.Warning, "Could not log out: " + exception);
						}
					}
				}
				
				// Close communication
				CloseChannelFactory(publicFactory);
				CloseChannelFactory(serviceChannelFactory);
			}

			return Tuple.Create(result, exceptionResult);
		}

		/// <summary>
		/// Extracts the name of the service from its interface, for example, if given type ICoreWCF, returns Core.
		/// </summary>
		/// <typeparam name="TServiceInterface">The type of the service interface.</typeparam>
		/// <returns></returns>
		private static string GetServiceName<TServiceInterface>() where TServiceInterface : IBaseModuleWCF {
			string serviceInterfaceName = typeof(TServiceInterface).Name;
			string serviceName = serviceInterfaceName.Substring(1, serviceInterfaceName.IndexOf("WCF") - 1);

			return serviceName;
		}

		/// <summary>
		/// Logins to the tenant using the provided credentials.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="password">The password.</param>
		/// <param name="publicProxy">The public proxy.</param>
		private static void Login(string username, string password, IPublicWCF publicProxy) {
			FormResponse loginResponse = publicProxy.Login_Response(username, password, true, null);

			if (!loginResponse.success) {
				throw new VizeliaException("Could not login to tenant");
			}
		}


		/// <summary>
		/// Closes the channel factory safely.
		/// </summary>
		/// <param name="channelFactory">The channel factory.</param>
		private static void CloseChannelFactory(ChannelFactory channelFactory) {
			if (channelFactory != null && (channelFactory.State != CommunicationState.Closed || channelFactory.State != CommunicationState.Closing))
				channelFactory.Close();

		}

	}
}

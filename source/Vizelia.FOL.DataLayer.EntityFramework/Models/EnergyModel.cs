using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Vizelia.FOL.DataLayer.EntityFramework.Entities;

namespace Vizelia.FOL.DataLayer.EntityFramework.Models {

	public partial class EnergyModel : DbContext {
		public EnergyModel()
			: base("name=EnergyModel") {
			((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 360;
		}

		public virtual DbSet<MeterDataEF> MeterDatas { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder) {
			BuildMeterData(modelBuilder);
		}

		private static void BuildMeterData(DbModelBuilder modelBuilder) {
			var meterData = modelBuilder.Entity<MeterDataEF>();

			meterData.ToTable("MeterData");

			meterData.Property(e => e.App).IsUnicode(false);
			meterData.Property(e => e.KeyMeter).IsUnicode(false);
		}
	}
}
﻿using System;
using System.Linq;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.EntityFramework.Entities;
using Vizelia.FOL.DataLayer.EntityFramework.Models;

namespace Vizelia.FOL.DataLayer.EntityFramework.Repositories {
	/// <summary>
	/// Meter Data Repository
	/// </summary>
	public class MeterDataRepository : IDataRepository<MeterDataEF> {
		private readonly EnergyModel m_Context;

		/// <summary>
		/// Initializes a new instance of the <see cref="MeterDataRepository"/> class.
		/// </summary>
		public MeterDataRepository() : this (new EnergyModel()) {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MeterDataRepository"/> class.
		/// </summary>
		/// <param name="context">The context.</param>
		public MeterDataRepository(EnergyModel context) {
			m_Context = context;
		}

		/// <summary>
		/// Gets all MeterData entities as queryable.
		/// </summary>
		/// <returns></returns>
		public IQueryable<MeterDataEF> GetAll() {
			
			var result = m_Context.MeterDatas
				.Where(meterData => meterData.App == ContextHelper.ApplicationName);
			
			return result;
		}

		private bool m_Disposed;
		protected virtual void Dispose(bool disposing) {
			if (!m_Disposed) {
				if (disposing) {
					m_Context.Dispose();
				}
			}
			m_Disposed = true;
		}

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}

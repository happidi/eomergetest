﻿using System;
using System.Linq;

namespace Vizelia.FOL.DataLayer.EntityFramework.Repositories {
	/// <summary>
	/// Interface for data repositories.
	/// </summary>
	/// <typeparam name="TEntity">The type of the entity.</typeparam>
	public interface IDataRepository<TEntity> : IDisposable{
		/// <summary>
		/// Gets all entities as queryable.
		/// </summary>
		/// <returns></returns>
		IQueryable<TEntity> GetAll();
	}
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vizelia.FOL.DataLayer.EntityFramework.Entities {
	public partial class MeterDataEF {
		[StringLength(40)]
		public string App { get; set; }

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long KeyMeterData { get; set; }

		[StringLength(40)]
		public string KeyMeter { get; set; }

		public DateTime AcquisitionDateTime { get; set; }

		public double Value { get; set; }

		[StringLength(255)]
		public string Source { get; set; }

		public byte? Validity { get; set; }
	}
}

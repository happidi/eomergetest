﻿using System.Linq;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using System.Collections.Generic;
using System.Diagnostics;

namespace Vizelia.FOL.BusinessLayer {
	/// <summary>
	/// Business Layer implementation for Job.
	/// Each method here must have the JobContext in order to have logging of the job steps.
	/// </summary>
	public class JobBusinessLayer : IJobBusinessLayer {

		/// <summary>
		/// Resumes the pending workflows.
		/// </summary>
		/// <param name="context">The context.</param>
		public void ResumePendingWorkflows(JobContext context) {
			IServiceDeskBusinessLayer module = Helper.CreateInstance<ServiceDeskBusinessLayer, IServiceDeskBusinessLayer>();
			module.Workflow_ResumePending();
		}

		/// <summary>
		/// Tests the job exception.
		/// </summary>
		/// <param name="context">The context.</param>
		public void TestJobException(JobContext context) {
			throw new VizeliaException("The job has failed");
		}

		/// <summary>
		/// Performs the scheduled mapping.
		/// </summary>
		/// <param name="context">The context.</param>
		public void PerformScheduledMapping(JobContext context) {
			IMappingBusinessLayer mappingBusinessLayer = Helper.CreateInstance<MappingBusinessLayer, IMappingBusinessLayer>();

			if (context.Keys == null || !context.Keys.Any()) {
				TracingService.Write(TraceEntrySeverity.Warning, "No Mapping Tasks selected for this job. Terminating.", "Scheduled Mapping", context.JobKey);
				return;
			}

			foreach (string mappingTaskKey in context.Keys) {
				mappingBusinessLayer.PerformScheduledMapping(mappingTaskKey);
			}
		}

		/// <summary>
		/// Generate the different playlist as images.
		/// </summary>
		/// <param name="context">The context.</param>
		public void GeneratePlaylist(JobContext context) {
			var module = Helper.CreateInstance<EnergyBusinessLayer, IEnergyBusinessLayer>();
			string[] keys = new string[0];

			if (context.Keys != null && context.Keys.Count > 0) {
				keys = context.Keys.ToArray();
			}

			ScheduledTaskHelper.RunLocked<Playlist>(string.Join(",", keys), () => module.Playlist_Generate(keys));
		}

		/// <summary>
		/// Process all the enabled AlarmDefinition and generates the corresponding alarm instances.
		/// </summary>
		/// <param name="context">The context.</param>
		public void AlarmDefinitionProcess(JobContext context) {
			var module = Helper.CreateInstance<EnergyBusinessLayer, IEnergyBusinessLayer>();
			var moduleAuthentication = Helper.CreateInstance<AuthenticationBusinessLayer, IAuthenticationBusinessLayer>();

			//If the job doesnt have any alarm assigned, we now go and get all alarm that are not manually processed.
			string[] Keys = null;
			Dictionary<string, List<string>> alarmsByCreator = new Dictionary<string, List<string>>();

			if (context.Keys != null && context.Keys.Count > 0) {
				Keys = context.Keys.ToArray();
			}
			else {
				Keys = module.AlarmDefinition_GetAllToProcess();
			}

			//now we organise the alarm by their creator.(to solve filtering issues).
			foreach (var key in Keys) {
				var ad = module.AlarmDefinition_GetItem(key);
				if (ad != null) {
					var user = moduleAuthentication.User_GetItem(ad.Creator);
					if (user != null) {
						List<string> list = null;
						if (!alarmsByCreator.TryGetValue(user.UserName, out list)) {
							list = new List<string>();
							alarmsByCreator.Add(user.UserName, list);
						}
						list.Add(key);
					}
				}
			}


			foreach (var kvp in alarmsByCreator) {
				string lockkey = string.Join(",", kvp.Value.OrderBy(k => k).ToArray());
				moduleAuthentication.User_SetFilters(kvp.Key, SessionService.GetSessionID());
				ScheduledTaskHelper.RunLocked<AlarmDefinition>(lockkey, () => module.AlarmDefinition_Process(kvp.Value.ToArray(), kvp.Key));
			}
		}

		/// <summary>
		/// Process all the Meter that have an endpoint and store the corresponding real time value.
		/// </summary>
		/// <param name="context">The context.</param>
		public void MeterEndpointProcess(JobContext context) {
			var module = Helper.CreateInstance<EnergyBusinessLayer, IEnergyBusinessLayer>();
			ScheduledTaskHelper.RunLocked<Meter>("MeterEndpointProcess", () => {
				module.Meter_EndpointProcess();
				module.PsetAttributeHistorical_EndpointProcess();
			});
		}

		/// <summary>
		/// Process all the ChartScheduler and sends the one(s) that needs to be sent.
		/// </summary>
		/// <param name="context">The context.</param>
		public void ChartSchedulerProcess(JobContext context) {
			var module = Helper.CreateInstance<EnergyBusinessLayer, IEnergyBusinessLayer>();
			ScheduledTaskHelper.RunLocked<Chart>("ChartSchedulerProcess", () => module.ChartScheduler_Process(context));
		}



		/// <summary>
		/// Process all the enabled MeterValidationRule and generates the corresponding alarm instances.
		/// </summary>
		/// <param name="context">The context.</param>
		public void MeterValidationRuleProcess(JobContext context) {
			var module = Helper.CreateInstance<EnergyBusinessLayer, IEnergyBusinessLayer>();
			ScheduledTaskHelper.RunLocked<AlarmDefinition>("MeterValidationRuleProcess",
				() => module.MeterValidationRule_Process(null));
		}

		/// <summary>
		/// Purges the server logs.
		/// </summary>
		/// <param name="context">The context.</param>
		public void PurgeLogs(JobContext context) {
			var module = Helper.CreateInstance<CoreBusinessLayer, ICoreBusinessLayer>();
			module.PurgeLogs();
		}

		/// <summary>
		/// Exports the meter data.
		/// </summary>
		/// <param name="context">The context.</param>
		public void ExportMeterData(JobContext context) {
			IMappingBusinessLayer mappingBusinessLayer = Helper.CreateInstance<MappingBusinessLayer, IMappingBusinessLayer>();

			if (context.Keys == null) {
				TracingService.Write(TraceEntrySeverity.Warning, "No Meter Data Export Tasks selected for this job. Terminating.", "Meter Data Export", context.JobKey);
				return;
			}

			foreach (string meterDataExportTask in context.Keys) {
				ScheduledTaskHelper.RunLocked<MeterDataExportTask>(meterDataExportTask,
																   () => mappingBusinessLayer.ExportMeterData(meterDataExportTask));
			}
		}

		/// <summary>
		/// Synchronizes the external authentication provider.
		/// </summary>
		/// <param name="context">The context.</param>
		public void SynchronizeExternalAuthenticationProvider(JobContext context) {
			// The concurrency lock is internal in the provider itself. We don't use ScheduledTaskLocker here.
			var provider = Membership.Provider as IExternalAuthenticationProvider;

			if (provider != null) {
				provider.Synchronize();
			}
			else {
				throw new VizeliaException(string.Format("The membership provider with name '{0}' is not an IExternalAuthenticationProvider. Cannot sync with external authentication source.", Membership.Provider.Name));
			}
		}

		/// <summary>
		/// Preload charts for specific users.
		/// </summary>
		/// <param name="context">The context.</param>
		public void ChartPreLoadAll(JobContext context) {
			if (context.Keys != null && context.Keys.Count > 0) {
				context.Keys.ForEach(username => {
					var moduleEnergy = Helper.CreateInstance<EnergyBusinessLayer, IEnergyBusinessLayer>();
					var moduleAuthentication = Helper.CreateInstance<AuthenticationBusinessLayer, IAuthenticationBusinessLayer>();

					moduleAuthentication.User_SetFilters(username, SessionService.GetSessionID());
					ScheduledTaskHelper.RunLocked<Chart>("Chart_PreLoadAll", () => moduleEnergy.Chart_PreLoadAll(true, username));
				});
			}
		}

		/// <summary>
		/// Push the different portal templates.
		/// </summary>
		/// <param name="context">The context.</param>
		public void PortalTemplatePushUpdates(JobContext context) {
			var module = Helper.CreateInstance<EnergyBusinessLayer, IEnergyBusinessLayer>();
			var moduleAuthentication = Helper.CreateInstance<AuthenticationBusinessLayer, IAuthenticationBusinessLayer>();

			string[] Keys = new string[0];

			if (context.Keys != null && context.Keys.Count > 0) {
				Keys = context.Keys.ToArray();
				Dictionary<string, List<string>> templateByCreator = new Dictionary<string, List<string>>();

				//now we organise the portal template by their creator.(to solve filtering issues).
				foreach (var key in Keys) {
					var pt = module.PortalTemplate_GetItem(key);
					if (pt != null) {
						var user = moduleAuthentication.User_GetItem(pt.Creator);
						if (user != null) {
							List<string> list = null;
							if (!templateByCreator.TryGetValue(user.UserName, out list)) {
								list = new List<string>();
								templateByCreator.Add(user.UserName, list);
							}
							list.Add(key);
						}
					}
				}

				foreach (var kvp in templateByCreator) {
					string lockkey = string.Join(",", kvp.Value.OrderBy(k => k).ToArray());
					moduleAuthentication.User_SetFilters(kvp.Key, SessionService.GetSessionID());
					ScheduledTaskHelper.RunLocked<PortalTemplate>(lockkey, () => module.PortalTemplate_PushUpdates(kvp.Value.ToArray(), kvp.Key));
				}
			}
		}
	}
}

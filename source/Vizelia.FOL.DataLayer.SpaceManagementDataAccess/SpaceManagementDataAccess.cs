﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.DataLayer;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.BusinessEntities;
using System.Data.Common;
using System.Data;

namespace Vizelia.FOL.DataLayer {
	/// <summary>
	/// Data Layer implementation for SpaceManagement. 
	/// </summary>
	public class SpaceManagementDataAccess : ISpaceManagementDataAccess {

		private IDataAccess m_DataAccess;

		/// <summary>
		/// Ctor.
		/// </summary>
		public SpaceManagementDataAccess() {
			m_DataAccess = new DataAccess();
		}


		/// <summary>
		/// Gets the zoning from a BuildingStorey.
		/// </summary>
		/// <param name="KeyBuildingStorey">The Key of the BuildingStorey.</param>
		/// <param name="KeyClassificationItem">The filter for Classifications (optional)</param>
		/// <returns></returns>
		public JsonStore<Space> BuildingStorey_GetZoning(string KeyBuildingStorey , string KeyClassificationItem) {
			DbCommand command = m_DataAccess.GetStoredProcCommand("SpaceManagement_GetZoningFromBuildingStorey");
			m_DataAccess.AddInParameterString(command, "KeyBuildingStorey", KeyBuildingStorey);
			m_DataAccess.AddInParameterString(command, "KeyClassificationItem", KeyClassificationItem);
			DataSet ds = m_DataAccess.ExecuteDataSet(command);
			var query = from row in ds.Tables[0].AsEnumerable()
						select new Space {
							KeySpace = row.Field<string>("KeySpace"),
							KeyBuildingStorey = row.Field<string>("KeyBuildingStorey"),
							Name = row.Field<string>("Name"),
							GlobalId = row.Field<string>("GlobalId"),
							ElevationWithFlooring = row.Field<double>("ElevationWithFlooring"),
							AreaValue = row.Field<double>("AreaValue"),
							Zoning = new ClassificationItem {
								KeyClassificationItem = row.Field<string>("KeyClassificationItem"),
								Code = row.Field<string>("Code"),
								Title = row.Field<string>("Title"),
								ColorR = row.Field<int>("ColorR"),
								ColorG = row.Field<int>("ColorG"),
								ColorB = row.Field<int>("ColorB")
							}
						};
			return query.ToList().ToJsonStore();
		}
	}
}

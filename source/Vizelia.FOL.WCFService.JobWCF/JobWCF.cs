﻿using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.WCFService.Contracts;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessLayer;

namespace Vizelia.FOL.WCFService {
	/// <summary>
	/// Implementation for Core Services.
	/// </summary>
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
	public class JobWCF : BaseModuleWCF, IJobWCF {

		private readonly IJobBusinessLayer m_JobBusinessLayer;

		/// <summary>
		/// Public ctor.
		/// </summary>
		public JobWCF() {
			// Policy injection on the business layer instance.
			m_JobBusinessLayer = Helper.CreateInstanceWithPolicyInjection<JobBusinessLayer, IJobBusinessLayer>();
		}


		/// <summary>
		/// Resumes the pending workflows.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_resumependingworkflows_name", "msg_step_resumependingworkflows_description", null, null)]
		[IconCls("viz-icon-small-step-resumependingworkflows")]
		public void ResumePendingWorkflows(JobContext context) {
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.ResumePendingWorkflows(context));
		}

		/// <summary>
		/// Performs the scheduled mapping.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_scheduled_mapping_name", "msg_step_scheduled_mapping_description", "KeyMappingTask", "vizGridMappingTask")]
		[IconCls("viz-icon-small-xml")]
		public void PerformScheduledMapping(JobContext context) {
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.PerformScheduledMapping(context));
		}

		/// <summary>
		/// Tests the job exception.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_testjobexception_name", "msg_step_testjobexception_description", "KeyOccupant", "vizGridOccupant")]
		[IconCls("viz-icon-small-step")]
		public void TestJobException(JobContext context) {
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.TestJobException(context));
		}


		/// <summary>
		/// Generate the different playlist as images.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_generateplaylist_name", "msg_step_generateplaylist_description", "KeyPlaylist", "vizGridPlaylist", "vizStoreStepPlaylist")]
		[IconCls("viz-icon-small-playlist")]
		public void GeneratePlaylist(JobContext context) {
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.GeneratePlaylist(context));
		}


		/// <summary>
		/// Process all the enabled AlarmDefinition and generates the corresponding alarm instances.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_alarmdefinitionprocess_name", "msg_step_alarmdefinitionprocess_description", "KeyAlarmDefinition", "vizGridAlarmDefinition", "vizStoreStepAlarmDefinition")]
		[IconCls("viz-icon-small-alarm")]
		public void AlarmDefinitionProcess(JobContext context) {
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.AlarmDefinitionProcess(context));
		}

		/// <summary>
		/// Process all the Meter that have an endpoint and store the corresponding real time value.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_meterendpointprocess_name", "msg_step_meterendpointprocess_description", null, null)]
		[IconCls("viz-icon-small-dataacquisitioncontainer")]
		public void MeterEndpointProcess(JobContext context) {
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.MeterEndpointProcess(context));
		}

		/// <summary>
		/// Process all the ChartScheduler and sends the one(s) that needs to be sent.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_chartschedulerprocess_name", "msg_step_chartschedulerprocess_description", null, null)]
		[IconCls("viz-icon-small-chartscheduler")]
		public void ChartSchedulerProcess(JobContext context) {
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.ChartSchedulerProcess(context));
		}

		/// <summary>
		/// Purges the server logs.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_purgelogs_name", "msg_step_purgelogs_description", null, null)]
		[IconCls("viz-icon-small-trace-delete")]
		public void PurgeLogs(JobContext context) {
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.PurgeLogs(context));
		}

		/// <summary>
		/// Process all the enabled AlarmDefinition and generates the corresponding alarm instances.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_metervalidationrule_name", "msg_step_metervalidationrule_description", null, null)]
		[IconCls("viz-icon-small-metervalidationrule")]
		public void MeterValidationRuleProcess(JobContext context) {
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.MeterValidationRuleProcess(context));
		}

		/// <summary>
		/// Synchronizes the external authentication provider.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_externalauthenticationprovidersync_name", "msg_step_externalauthenticationprovidersync_description", null, null)]
		[IconCls("viz-icon-small-sync")]
		public void SynchronizeExternalAuthenticationProvider(JobContext context) {
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.SynchronizeExternalAuthenticationProvider(context));
		}

		/// <summary>
		/// Preload charts for specific users.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_chart_preloadall", "msg_step_chart_preloadall_description", "UserName", "vizGridUser")]
		[IconCls("viz-icon-small-chart")]
		public void ChartPreLoadAll(JobContext context) {
			//var principal = new GenericPrincipal(new GenericIdentity("myuser"), new string[] { "myrole" });
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.ChartPreLoadAll(context));
		}

		/// <summary>
		/// Exports the meter data.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_exportmeterdata", "msg_step_exportmeterdata_description", "KeyMeterDataExportTask", "vizGridMeterDataExportTask")]
		[IconCls("viz-icon-small-exporttask")]
		public void ExportMeterData(JobContext context) {
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.ExportMeterData(context));			
		}

		/// <summary>
		/// Push the different portal templates.
		/// </summary>
		/// <param name="context">The context.</param>
		[StepInformation("msg_step_portaltemplatepush_name", "msg_step_portaltemplatepush_description", "KeyPortalTemplate", "vizGridPortalTemplate", "vizStoreStepPortalTemplate")]
		[IconCls("viz-icon-small-portaltemplate")]
		public void PortalTemplatePushUpdates(JobContext context) {
			ContextHelper.JobRunInstanceRequestId = context.JobRunInstanceRequestId;
			ContextHelper.KeyJob = context.JobKey;
			TenantHelper.RunInDifferentTenantContext(context.ApplicationName, () => m_JobBusinessLayer.PortalTemplatePushUpdates(context));
		}
	}
}

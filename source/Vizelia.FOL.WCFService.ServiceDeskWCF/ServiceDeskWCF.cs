﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.WCFService {
	/// <summary>
	/// Implementation for ServiceDesk Services.
	/// </summary>
	public class ServiceDeskWCF : BaseModuleWCF, IServiceDeskWCF {

		private IServiceDeskBusinessLayer m_ServiceDeskBusinessLayer;



		/// <summary>
		/// Ctor.
		/// </summary>
		public ServiceDeskWCF() {
			m_ServiceDeskBusinessLayer = Helper.CreateInstance<ServiceDeskBusinessLayer, IServiceDeskBusinessLayer>();
		}



		/// <summary>
		/// Deletes an existing business entity ActionRequest.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ActionRequest_Delete(ActionRequest item) {
			return m_ServiceDeskBusinessLayer.ActionRequest_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ActionRequest and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ActionRequest_FormCreate(ActionRequest item) {
			return m_ServiceDeskBusinessLayer.ActionRequest_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ActionRequest.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ActionRequest_FormLoad(string Key) {
			return m_ServiceDeskBusinessLayer.ActionRequest_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ActionRequest and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ActionRequest_FormUpdate(ActionRequest item) {
			return m_ServiceDeskBusinessLayer.ActionRequest_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ActionRequest_FormUpdateBatch(string[] keys, ActionRequest item) {
			return m_ServiceDeskBusinessLayer.ActionRequest_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ActionRequest. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<ActionRequest> ActionRequest_GetAll(PagingParameter paging, PagingLocation location) {
			return m_ServiceDeskBusinessLayer.ActionRequest_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity ActionRequest.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public ActionRequest ActionRequest_GetItem(string Key) {
			return m_ServiceDeskBusinessLayer.ActionRequest_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity ActionRequest.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ActionRequest> ActionRequest_GetStore(PagingParameter paging, PagingLocation location) {
			return m_ServiceDeskBusinessLayer.ActionRequest_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity ActionRequest.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ActionRequest> ActionRequest_SaveStore(CrudStore<ActionRequest> store) {
			return m_ServiceDeskBusinessLayer.ActionRequest_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity Approval.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Approval_Delete(Approval item) {
			return m_ServiceDeskBusinessLayer.Approval_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Approval and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Approval_FormCreate(Approval item) {
			return m_ServiceDeskBusinessLayer.Approval_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Approval.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Approval_FormLoad(string Key) {
			return m_ServiceDeskBusinessLayer.Approval_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Approval and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Approval_FormUpdate(Approval item) {
			return m_ServiceDeskBusinessLayer.Approval_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Approval_FormUpdateBatch(string[] keys, Approval item) {
			return m_ServiceDeskBusinessLayer.Approval_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Approval. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Approval> Approval_GetAll(PagingParameter paging, PagingLocation location) {
			return m_ServiceDeskBusinessLayer.Approval_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Approval.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Approval Approval_GetItem(string Key) {
			return m_ServiceDeskBusinessLayer.Approval_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Approval.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Approval> Approval_GetStore(PagingParameter paging, PagingLocation location) {
			return m_ServiceDeskBusinessLayer.Approval_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity Approval.
		/// </summary>
		/// <param name="KeyActionRequest">The Key of the action request.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Approval> Approval_GetStoreFromActionRequest(string KeyActionRequest, PagingParameter paging,
																	  PagingLocation location) {
			return m_ServiceDeskBusinessLayer.Approval_GetStoreFromActionRequest(KeyActionRequest, paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Approval.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Approval> Approval_SaveStore(CrudStore<Approval> store) {
			return m_ServiceDeskBusinessLayer.Approval_SaveStore(store);
		}

		/// <summary>
		/// Calculates the next schedule date based on a location.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="minutes"></param>
		/// <param name="KeyLocation"></param>
		/// <returns></returns>
		public DateTime? Calendar_CalculateScheduleDate(DateTime start, double minutes, string KeyLocation) {
			return m_ServiceDeskBusinessLayer.Calendar_CalculateScheduleDate(start, minutes, KeyLocation);
		}

		/// <summary>
		/// Gets a json store of ClassificationItem for a specific mail.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ClassificationItem> ClassificationItem_GetStoreByKeyMail(PagingParameter paging, string KeyMail) {
			return m_ServiceDeskBusinessLayer.ClassificationItem_GetStoreByKeyMail(paging, KeyMail);
		}

		/// <summary>
		/// Gets a json store of locations for a specific mail.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Location> Location_GetStoreByKeyMail(PagingParameter paging, string KeyMail) {
			return m_ServiceDeskBusinessLayer.Location_GetStoreByKeyMail(paging, KeyMail);
		}

		/// <summary>
		/// Deletes an existing business entity Mail.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Mail_Delete(Mail item) {
			return m_ServiceDeskBusinessLayer.Mail_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Mail and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="priorities">The store of the priorities for the mail.</param>
		/// <param name="classifications">The store of the classifications for the mail.</param>
		/// <param name="locations">The store of the locations for the mail.</param>
		/// <param name="workflowActionElements">The workflow action elements.</param>
		/// <returns></returns>
		public FormResponse Mail_FormCreate(Mail item, CrudStore<Priority> priorities,
											CrudStore<ClassificationItem> classifications,
											CrudStore<Location> locations, CrudStore<WorkflowActionElement> workflowActionElements) {
			return m_ServiceDeskBusinessLayer.Mail_FormCreate(item, priorities, classifications, locations, workflowActionElements);
		}

		/// <summary>
		/// Loads a specific item for the business entity Mail.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Mail_FormLoad(string Key) {
			return m_ServiceDeskBusinessLayer.Mail_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Mail and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="priorities">The store of the priorities for the mail.</param>
		/// <param name="classifications">The store of the classifications for the mail.</param>
		/// <param name="locations">The store of the locations for the mail.</param>
		/// <param name="workflowActionElements">The workflow action elements.</param>
		/// <returns></returns>
		public FormResponse Mail_FormUpdate(Mail item, CrudStore<Priority> priorities,
											CrudStore<ClassificationItem> classifications,
											CrudStore<Location> locations, CrudStore<WorkflowActionElement> workflowActionElements) {
			return m_ServiceDeskBusinessLayer.Mail_FormUpdate(item, priorities, classifications, locations, workflowActionElements);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Mail_FormUpdateBatch(string[] keys, Mail item) {
			return m_ServiceDeskBusinessLayer.Mail_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Mail. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Mail> Mail_GetAll(PagingParameter paging, PagingLocation location) {
			return m_ServiceDeskBusinessLayer.Mail_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Mail.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Mail Mail_GetItem(string Key) {
			return m_ServiceDeskBusinessLayer.Mail_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Mail.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Mail> Mail_GetStore(PagingParameter paging, PagingLocation location) {
			return m_ServiceDeskBusinessLayer.Mail_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Mail.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Mail> Mail_SaveStore(CrudStore<Mail> store) {
			return m_ServiceDeskBusinessLayer.Mail_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity Priority.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Priority_Delete(Priority item) {
			return m_ServiceDeskBusinessLayer.Priority_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Priority and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Priority_FormCreate(Priority item) {
			return m_ServiceDeskBusinessLayer.Priority_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Priority.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Priority_FormLoad(string Key) {
			return m_ServiceDeskBusinessLayer.Priority_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Priority and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Priority_FormUpdate(Priority item) {
			return m_ServiceDeskBusinessLayer.Priority_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Priority_FormUpdateBatch(string[] keys, Priority item) {
			return m_ServiceDeskBusinessLayer.Priority_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Priority. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Priority> Priority_GetAll(PagingParameter paging, PagingLocation location) {
			return m_ServiceDeskBusinessLayer.Priority_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Priority.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Priority Priority_GetItem(string Key) {
			return m_ServiceDeskBusinessLayer.Priority_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Priority.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Priority> Priority_GetStore(PagingParameter paging, PagingLocation location) {
			return m_ServiceDeskBusinessLayer.Priority_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store of priorities for a specific mail.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Priority> Priority_GetStoreByKeyMail(PagingParameter paging, string KeyMail) {
			return m_ServiceDeskBusinessLayer.Priority_GetStoreByKeyMail(paging, KeyMail);
		}

		/// <summary>
		/// Saves a crud store for the business entity Priority.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Priority> Priority_SaveStore(CrudStore<Priority> store) {
			return m_ServiceDeskBusinessLayer.Priority_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity Task.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Task_Delete(Task item) {
			return m_ServiceDeskBusinessLayer.Task_Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Task and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Task_FormCreate(Task item) {
			return m_ServiceDeskBusinessLayer.Task_FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Task.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Task_FormLoad(string Key) {
			return m_ServiceDeskBusinessLayer.Task_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Task and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Task_FormUpdate(Task item) {
			return m_ServiceDeskBusinessLayer.Task_FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Task_FormUpdateBatch(string[] keys, Task item) {
			return m_ServiceDeskBusinessLayer.Task_FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Task. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Task> Task_GetAll(PagingParameter paging, PagingLocation location) {
			return m_ServiceDeskBusinessLayer.Task_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a specific item for the business entity Task.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Task Task_GetItem(string Key) {
			return m_ServiceDeskBusinessLayer.Task_GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Task.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Task> Task_GetStore(PagingParameter paging, PagingLocation location) {
			return m_ServiceDeskBusinessLayer.Task_GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity Task from an ActionRequest.
		/// </summary>
		/// <param name="KeyActionRequest">The Key of the action request.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Task> Task_GetStoreFromActionRequest(string KeyActionRequest, PagingParameter paging,
															  PagingLocation location) {
			return m_ServiceDeskBusinessLayer.Task_GetStoreFromActionRequest(KeyActionRequest, paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Task.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Task> Task_SaveStore(CrudStore<Task> store) {
			return m_ServiceDeskBusinessLayer.Task_SaveStore(store);
		}

		/// <summary>
		/// Builds the image of the workflow.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public ImageMap Workflow_BuildImage(Guid instanceId, int width, int height) {
			return m_ServiceDeskBusinessLayer.Workflow_BuildImage(instanceId, width, height);
		}

		/// <summary>
		/// Gets all the workflows from bins.
		/// </summary>
		/// <returns></returns>
		public JsonStore<WorkflowAssemblyDefinition> Workflow_GetAllWorkflows() {
			return m_ServiceDeskBusinessLayer.Workflow_GetAllWorkflows();
		}

		/// <summary>
		/// Gets the possible events that a state machine workflow instance can listen to.
		/// </summary>
		/// <param name="entity">The workflow entity.</param>
		/// <returns></returns>
		public List<WorkflowActionElement> Workflow_GetStateMachineEventsTransitions(IWorkflowEntity entity) {
			return m_ServiceDeskBusinessLayer.Workflow_GetStateMachineEventsTransitions(entity);
		}

		/// <summary>
		/// Gets all possible events from an assembly.
		/// </summary>
		/// <param name="assemblyName">The full name of the assembly.
		/// <example>Vizelia.FOL.Workflow.Services.Machine, Vizelia.FOL.WF.Workflows, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null</example>
		/// </param>
		/// <returns></returns>
		public JsonStore<WorkflowActionElement> Workflow_GetStateMachineEventsTransitionsFromAssembly(string assemblyName) {
			return m_ServiceDeskBusinessLayer.Workflow_GetStateMachineEventsTransitionsFromAssembly(assemblyName).ToJsonStore();
		}

		/// <summary>
		/// Gets a distinct store of possible WorkflowActionElement given a list of KeyClassificationItem.
		/// </summary>
		/// <param name="keys">An array of KeyClassificationItem.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<WorkflowActionElement> Workflow_GetStateMachineEventsTransitionsFromClassificationItems(string[] keys) {
			return m_ServiceDeskBusinessLayer.Workflow_GetStateMachineEventsTransitionsFromClassificationItems(keys);
		}

		/// <summary>
		///  Gets a store of WorkflowActionElement for a specified Mail.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<WorkflowActionElement> Workflow_GetStoreWorkflowActionElementByKeyMail(PagingParameter paging, string KeyMail) {
			return m_ServiceDeskBusinessLayer.Workflow_GetStoreWorkflowActionElementByKeyMail(paging, KeyMail);
		}

		/// <summary>
		/// Returns the stream of the image for a worflow.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public Stream Workflow_GetStreamImage(Guid instanceId, int width, int height) {
			var stream = m_ServiceDeskBusinessLayer.Workflow_GetStreamImage(instanceId, width, height);
			if (WebOperationContext.Current != null) WebOperationContext.Current.OutgoingResponse.ContentType = stream.MimeType;
			return stream.ContentStream;
		}

		/// <summary>
		/// Returns the stream of the image for a worflow.
		/// </summary>
		/// <param name="guid">The guid of the image.</param>
		/// <returns></returns>
		public Stream Workflow_GetStreamImageFromCache(string guid) {
			if (WebOperationContext.Current != null) WebOperationContext.Current.OutgoingResponse.ContentType = MimeType.Png;
			return m_ServiceDeskBusinessLayer.Workflow_GetStreamImageFromCache(guid);
		}

		/// <summary>
		/// Gets the possible events that a state machine workflow instance can listen to.
		/// </summary>
		/// <param name="entity">The workflow entity.</param>
		/// <param name="eventName">Name of the event.</param>
		/// <param name="workflowQualifiedName">Name of the workflow qualified.</param>
		/// <returns></returns>
		public FormResponse Workflow_RaiseEvent(IWorkflowEntity entity, string eventName, string workflowQualifiedName) {
			return m_ServiceDeskBusinessLayer.Workflow_RaiseEvent(entity, eventName, workflowQualifiedName);
		}

		/// <summary>
		/// Restarts the workflow.
		/// This will generate a new instanceId and also update the workflow definition.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public FormResponse Workflow_Restart(IWorkflowEntity entity) {
			return m_ServiceDeskBusinessLayer.Workflow_Restart(entity);
		}

		/// <summary>
		/// Resumes all the pending workflows.
		/// </summary>
		public void Workflow_ResumePending() {
			m_ServiceDeskBusinessLayer.Workflow_ResumePending();
		}

		/// <summary>
		/// Update the workflow definition with current state.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public FormResponse Workflow_UpdateInstanceDefinition(IWorkflowEntity entity) {
			return m_ServiceDeskBusinessLayer.Workflow_UpdateInstanceDefinition(entity);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Vizelia.FOL.BusinessEntities;


namespace Vizelia.FOL.BusinessLayer.Interfaces {

	/// <summary>
	/// Business layer interface for Reporting.
	/// </summary>
	public interface IReportingBusinessLayer {
		/// <summary>
		/// Renders the report as a stream.
		/// </summary>
		/// <param name="data">The report data.</param>
		/// <param name="columns">The columns of the data passed to the report.</param>
		/// <param name="title">The report's title.</param>
		/// <param name="exportType">The export type of the report.</param>
		/// <param name="pageFormat">The page format of the report.</param>
		/// <param name="mimeType">The resulting mime type.</param>
		/// <param name="extension">The resulting extension of the file which content is returned as a stream.</param>
		/// <returns>The report rendered as a stream.</returns>
		MemoryStream RenderReport(IList data, IList<Column> columns, string title, ExportType exportType, FormatPageType pageFormat, out string mimeType, out string extension);


		/// <summary>
		/// Renders the report RDLC.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="dataSources">The dictionary of data sources. Each entry in the dictionary should be a List of business entities.</param>
		/// <param name="reportParameters">The dictionary of report parameters. Each entry in the dictionary should be a string.</param>
		/// <param name="exportType">Type of the export.</param>
		/// <param name="pageFormat">The page format.</param>
		/// <param name="mimeType">Type of the MIME.</param>
		/// <param name="extension">The extension.</param>
		/// <returns></returns>
		MemoryStream RenderReportRdlc(string reportPath, Dictionary<string, object> dataSources, Dictionary<string, object> reportParameters, ExportType exportType, FormatPageType pageFormat, out string mimeType, out string extension);

		/// <summary>
		/// Generates the json structure of the Reporting treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		List<TreeNode> Reporting_GetTree(string Key);

		/// <summary>
		/// Populates the model action request.
		/// </summary>
		/// <param name="keyActionRequest">The key action request.</param>
		/// <returns></returns>
		Dictionary<string, object> PopulateModelActionRequest(string keyActionRequest);

		/// <summary>
		/// Gets the report parameters.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		List<ReportParameter> GetReportParameters(string key);

		/// <summary>
		/// Gets the report parameter values.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <returns></returns>
		JsonStore<ReportParameterValue> GetReportParameterValues(string key, string parameterName);

		/// <summary>
		/// Creates the folder with the given name under the specified path.
		/// </summary>
		/// <param name="parentPath">The parent path.</param>
		/// <param name="folderName">Name of the folder.</param>
		void CreateFolder(string parentPath, string folderName);

		/// <summary>
		/// Deletes the folder. All content will be deleted as well.
		/// </summary>
		/// <param name="path">The path.</param>
		void DeleteFolder(string path);

		/// <summary>
		/// Renames or moves the item.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="newPath">The new path.</param>
		void MoveItem(string path, string newPath);

		/// <summary>
		/// Deletes the report.
		/// </summary>
		/// <param name="path">The path.</param>
		void DeleteReport(string path);

		/// <summary>
		/// Saves a crud store for the business entity ReportSubscription.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<ReportSubscription> ReportSubscription_SaveStore(CrudStore<ReportSubscription> store);
		
		/// <summary>
		/// Creates a new business entity ReportSubscription and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse ReportSubscription_FormCreate(ReportSubscription item);

		/// <summary>
		/// Deletes an existing business entity ReportSubscription.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool ReportSubscription_Delete(ReportSubscription item);

		/// <summary>
		/// Reports the subscription_ get all by report.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		JsonStore<ReportSubscription> ReportSubscription_GetAllByReport(string reportPath, PagingParameter paging);
	}
}

﻿using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.Interfaces {

	/// <summary>
	/// Business layer interface for SpaceManagement.
	/// </summary>
	public interface ISpaceManagementBusinessLayer {

		/// <summary>
		/// Gets the zoning 
		/// </summary>
		/// <param name="KeyBuildingStorey"></param>
		/// <param name="KeyClassificationItem">The filter for Classifications (optional)</param>
		/// <returns></returns>
		JsonStore<Space> BuildingStorey_GetZoning(string KeyBuildingStorey, string KeyClassificationItem);

	}
}

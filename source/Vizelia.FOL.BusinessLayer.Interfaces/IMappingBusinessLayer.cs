﻿using System;
using System.Collections.Generic;
using System.IO;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.Interfaces {
	/// <summary>
	/// Business layer interface for Mapping.
	/// </summary>
	public interface IMappingBusinessLayer {
		/// <summary>
		/// Initializes the current tenant with data needed to begin working with it.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="adminPassword">The admin password.</param>
		void InitializeTenant(Guid operationId, string adminPassword);

		/// <summary>
		/// Begins the mapping process.
		/// </summary>
		/// <param name="operationId">The operation unique id.</param>
		/// <param name="fileName">The name of the mapping file.</param>
		/// <param name="mappingFile">The stream mapping file.</param>
		void BeginMapping(Guid operationId, string fileName, Stream mappingFile);

		/// <summary>
		/// Gets all mappings.
		/// </summary>
		/// <returns></returns>
		string GetAllMappings();

		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		/// <returns></returns>
		IEnumerable<string> GetAllowedMappingFileExtensions();

		/// <summary>
		/// Tests the connection to the file source.
		/// This is a long running operation that returns a business object.
		/// </summary>
		/// <param name="keyMappingTask">The key of the mapping task.</param>
		/// <param name="operationId">The operation id.</param>
		void BeginTestConnection(string keyMappingTask, Guid operationId);

		/// <summary>
		/// Performs the scheduled mapping.
		/// </summary>
		/// <param name="keyMappingTask">The key mapping task.</param>
		void PerformScheduledMapping(string keyMappingTask);

		#region MappingTask

		/// <summary>
		/// Gets a json store for the business entity MappingTask.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<MappingTask> MappingTask_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity MappingTask.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<MappingTask> MappingTask_SaveStore(CrudStore<MappingTask> store);

		/// <summary>
		/// Gets a list for the business entity MappingTask. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		List<MappingTask> MappingTask_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity MappingTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		MappingTask MappingTask_GetItem(string Key);


		/// <summary>
		/// Loads a specific item for the business entity MappingTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse MappingTask_FormLoad(string Key);


		/// <summary>
		/// Creates a new business entity MappingTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse MappingTask_FormCreate(MappingTask item);

		/// <summary>
		/// Updates an existing business entity MappingTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse MappingTask_FormUpdate(MappingTask item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		FormResponse MappingTask_FormUpdateBatch(string[] keys, MappingTask item);

		/// <summary>
		/// Deletes an existing business entity MappingTask.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool MappingTask_Delete(MappingTask item);

		#endregion

		#region MeterDataExportTask
		/// <summary>
		/// Gets a json store for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<MeterDataExportTask> MeterDataExportTask_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<MeterDataExportTask> MeterDataExportTask_SaveStore(CrudStore<MeterDataExportTask> store);

		/// <summary>
		/// Gets a list for the business entity MeterDataExportTask. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		List<MeterDataExportTask> MeterDataExportTask_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		MeterDataExportTask MeterDataExportTask_GetItem(string Key);


		/// <summary>
		/// Loads a specific item for the business entity MeterDataExportTask.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse MeterDataExportTask_FormLoad(string Key);


		/// <summary>
		/// Creates a new business entity MeterDataExportTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		FormResponse MeterDataExportTask_FormCreate(MeterDataExportTask item, CrudStore<Meter> meters);

		/// <summary>
		/// Updates an existing business entity MeterDataExportTask and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		FormResponse MeterDataExportTask_FormUpdate(MeterDataExportTask item, CrudStore<Meter> meters);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		FormResponse MeterDataExportTask_FormUpdateBatch(string[] keys, MeterDataExportTask item);

		/// <summary>
		/// Deletes an existing business entity MeterDataExportTask.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool MeterDataExportTask_Delete(MeterDataExportTask item);

	
		#endregion

		/// <summary>
		/// Gets a list of the given business entity, ready for export.
		/// </summary>
		/// <param name="entityType">Type of the entity, expressed as the class name.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		List<BaseBusinessEntity> GetEntitiesForExport(string entityType, PagingParameter paging);

		/// <summary>
		/// Maps the excel to XML.
		/// </summary>
		/// <param name="fileStream">The file stream.</param>
		/// <param name="operationId">The operation id.</param>
		/// <returns></returns>
		Guid MapExcelToXml(Stream fileStream, Guid? operationId = null);

		/// <summary>
		/// Exports the meter data to CSV.
		/// </summary>
		void ExportMeterData(string keyMeterDataExportTask);

		/// <summary>
		/// Cleans the mapping temp folder.
		/// </summary>
		void CleanMappingTempFolder();
	}
}

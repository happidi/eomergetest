﻿using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.Interfaces {

	/// <summary>
	/// Business layer interface for Job.
	/// </summary>
	public interface IJobBusinessLayer {

		/// <summary>
		/// Resumes the pending workflows.
		/// </summary>
		/// <param name="context">The context.</param>
		void ResumePendingWorkflows(JobContext context);

		/// <summary>
		/// Tests the job exception.
		/// </summary>
		/// <param name="context">The context.</param>
		void TestJobException(JobContext context);


		/// <summary>
		/// Performs the scheduled mapping.
		/// </summary>
		/// <param name="context">The context.</param>
		void PerformScheduledMapping(JobContext context);
		/// <summary>
		/// Generate the different playlist as images.
		/// </summary>
		/// <param name="context">The context.</param>
		void GeneratePlaylist(JobContext context);

		/// <summary>
		/// Process all the enabled AlarmDefinition and generates the corresponding alarm instances.
		/// </summary>
		/// <param name="context">The context.</param>
		void AlarmDefinitionProcess(JobContext context);

		/// <summary>
		/// Process all the Meter that have an endpoint and store the corresponding real time value.
		/// </summary>
		/// <param name="context">The context.</param>
		void MeterEndpointProcess(JobContext context);

		/// <summary>
		/// Process all the ChartScheduler and sends the one(s) that needs to be sent.
		/// </summary>
		/// <param name="context">The context.</param>
		void ChartSchedulerProcess(JobContext context);

		/// <summary>
		/// Process all the enabled MeterValidationRule and generates the corresponding alarm instances.
		/// </summary>
		/// <param name="context">The context.</param>
		void MeterValidationRuleProcess(JobContext context);

		/// <summary>
		/// Purges the server logs.
		/// </summary>
		/// <param name="context">The context.</param>
		void PurgeLogs(JobContext context);

		/// <summary>
		/// Synchronizes the external authentication provider.
		/// </summary>
		/// <param name="context">The context.</param>
		void SynchronizeExternalAuthenticationProvider(JobContext context);

		/// <summary>
		/// Preload charts for specific users.
		/// </summary>
		/// <param name="context">The context.</param>
		void ChartPreLoadAll(JobContext context);

		/// <summary>
		/// Exports the meter data.
		/// </summary>
		/// <param name="context">The context.</param>
		void ExportMeterData(JobContext context);

		/// <summary>
		/// Push the different portal templates.
		/// </summary>
		/// <param name="context">The context.</param>
		void PortalTemplatePushUpdates(JobContext context);
	}
}

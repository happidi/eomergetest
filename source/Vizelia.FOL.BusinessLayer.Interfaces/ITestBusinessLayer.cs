﻿using System;
using System.Collections.Generic;
using System.Data;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.Interfaces {

	/// <summary>
	/// Business layer interface for Test.
	/// </summary>
	public interface ITestBusinessLayer {

		/// <summary>
		/// Gets the dataset of langue.
		/// </summary>
		/// <returns></returns>
		DataSet GetLangue();



		/// <summary>
		/// 
		/// </summary>
		/// <param name="actionRequest"></param>
		/// <returns></returns>
		Guid CreateActionRequest(ActionRequest actionRequest);


		#region Site

		/// <summary>
		/// Gets a json store for the business entity Site.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<Site> Site_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Site.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<Site> Site_SaveStore(CrudStore<Site> store);

		/// <summary>
		/// Gets a list for the business entity Site. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		List<Site> Site_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Site.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		Site Site_GetItem(string Key);

		/// <summary>
		/// Creates a new business entity Site and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse Site_FormCreate(Site item);


		/// <summary>
		/// Loads a business entity Site and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="Key">The item to load.</param>
		/// <returns></returns>
		FormResponse Site_FormLoad(string Key);


		/// <summary>
		/// Updates an existing business entity Site and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse Site_FormUpdate(Site item);

		/// <summary>
		/// Deletes an existing business entity Site.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool Site_Delete(Site item);

		#endregion

		/// <summary>
		/// Does the long running operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		void DoLongRunningOperation(Guid operationId);
	}
}

﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;


namespace Vizelia.FOL.BusinessLayer.Interfaces {

	/// <summary>
	/// Business layer interface for EnergyAggregator.
	/// </summary>
	public interface IEnergyAggregatorBusinessLayer {

		/// <summary>
		/// Process all the enabled AlarmDefinition to generate the AlarmInstances.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="alarms">The alarms.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		Dictionary<AlarmDefinition, List<AlarmInstance>> AlarmDefinition_Process(EnergyAggregatorContext context, List<AlarmDefinition> alarms, string culture = null);

		/// <summary>
		/// Process all the enabled MeterValidationRule to generate the AlarmInstances.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="rules">The rules.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		Dictionary<MeterValidationRule, List<AlarmInstance>> MeterValidationRule_Process(EnergyAggregatorContext context, List<MeterValidationRule> rules, string culture = null);

		/// <summary>
		/// Initializes the data.
		/// </summary>
		/// <param name="tenantDateRangeList">The tenant date range list.</param>
		void Initialize(List<EnergyAggregatorContext> tenantDateRangeList);


		/// <summary>
		/// Determines whether this instance is initialized with Data.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns>
		///   <c>true</c> if this instance is initialized; otherwise, <c>false</c>.
		/// </returns>
		bool IsInitialized(EnergyAggregatorContext context);

		/// <summary>
		/// Determines whether the Aggregator is loading meter data.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns>
		///   <c>true</c> if [is loading meter data] [the specified application name]; otherwise, <c>false</c>.
		/// </returns>
		bool IsLoadingMeterData(EnergyAggregatorContext context);


		/// <summary>
		/// Gets the meter data count.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns></returns>
		int GetMeterDataCount(EnergyAggregatorContext context);


		/// <summary>
		/// Process the Chart and adds returns the Series collecion.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="meterKeys">The meter keys.</param>
		/// <param name="locationKeys">The location keys.</param>
		/// <param name="analysis">The historical analysis.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="alarm">The alarm (just in case a chart is assigned to an alarm definition and we need to apply the alarm calendar as well).</param>
		/// <returns></returns>
		Chart Chart_Process(EnergyAggregatorContext context, Chart chart, List<int> meterKeys, List<string> locationKeys, ChartHistoricalAnalysis analysis, List<Filter> locationFilter, string culture = null, AlarmDefinition alarm = null);

		/// <summary>
		/// Process a collection of Charts.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="Keys">The keys.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		List<Chart> Chart_Process(EnergyAggregatorContext context, List<string> Keys, List<Filter> locationFilter, string culture);

		/// <summary>
		/// Process a single Chart.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="Key">The key of the Chart.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		Chart Chart_Process(EnergyAggregatorContext context, string Key, List<Filter> locationFilter, string culture, Chart chart = null);

		/// <summary>
		/// Return a list of available Series name for a specific Chart.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="meterKeys">The meter keys.</param>
		/// <param name="locationKeys">The location keys.</param>
		/// <param name="analysis">The historical analysis.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="includeExisting">True to include existing serie, false to return only series to be created.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		List<ListElement> Chart_GetDataSerieLocalId(EnergyAggregatorContext context, Chart chart, List<int> meterKeys, List<string> locationKeys, ChartHistoricalAnalysis analysis, List<Filter> locationFilter, bool includeExisting, string culture = null);

		/// <summary>
		/// Gets a json store for the business entity MeterData.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="meter">The meter.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="rawData">if set to <c>true</c> [raw data].</param>
		/// <returns>
		/// A json store.
		/// </returns>
		JsonStore<MeterData> MeterData_GetStoreFromMeter(EnergyAggregatorContext context, Meter meter, PagingParameter paging, DateTime? startDate, DateTime? endDate, bool rawData = false);

		/// <summary>
		/// Get the last meter data for a specific Meter (sorted by AcquisitionDateTime).
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="meter">The meter.</param>
		/// <returns></returns>
		MeterData MeterData_GetLastFromMeter(EnergyAggregatorContext context, Meter meter);

		/// <summary>
		/// Update, delete or create the base business entities that are stored in memory needs.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="notifications">The notifications.</param>
		void CrudNotification(EnergyAggregatorContext context, List<CrudInformation> notifications);

		/// <summary>
		/// Returns the list of existing events title from a specific calendar category.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="category">The category.</param>
		/// <param name="meter">The meter.</param>
		/// <returns></returns>
		List<string> Calendar_GetEventsTitle(EnergyAggregatorContext context, CalendarEventCategory category, Meter meter);

		/// <summary>
		/// Determines whether [is crud notification in progress].
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns>
		///   <c>true</c> if [is crud notification in progress]; otherwise, <c>false</c>.
		/// </returns>
		bool IsCrudNotificationInProgress(EnergyAggregatorContext context);
	}
}

﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.Interfaces {

	/// <summary>
	/// Business layer interface for Tenancy.
	/// </summary>
	public interface ITenancyBusinessLayer {
		/// <summary>
		/// Creates the admin tenant.
		/// </summary>
		Tenant Tenant_CreateAdminTenant();

		/// <summary>
		/// Deletes an existing business entity Tenant.
		/// </summary>
		/// <param name="keyTenant">The key tenant.</param>
		/// <param name="operationId">The operation id.</param>
		void Tenant_Delete(string keyTenant, Guid operationId);

		/// <summary>
		/// Creates a new business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="operationId">The operation id.</param>
		void Tenant_FormCreate(Tenant item, Guid operationId);

		/// <summary>
		/// Loads a specific item for the business entity Tenant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse Tenant_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="operationId">The operation id.</param>
		/// <returns></returns>
		void Tenant_FormUpdate(Tenant item, Guid operationId);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		FormResponse Tenant_FormUpdateBatch(string[] keys, Tenant item);

		/// <summary>
		/// Gets a list for the business entity Tenant. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		List<Tenant> Tenant_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a specific item for the business entity Tenant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		Tenant Tenant_GetItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity Tenant.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<Tenant> Tenant_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Tenant.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<Tenant> Tenant_SaveStore(CrudStore<Tenant> store);

		/// <summary>
		/// Resets the current tenant.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="adminPassword">The admin password.</param>
		void ResetTenant(Guid operationId, string adminPassword);

		/// <summary>
		/// Gets the tenants for current request host header.
		/// </summary>
		/// <returns></returns>
		List<Tenant> GetTenantsForCurrentRequestHostHeader();

		/// <summary>
		/// Checks if tenant is initialized.
		/// </summary>
		/// <returns></returns>
		bool IsTenantInitialized();
	}

}
﻿using System;
using System.Collections.Generic;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Configuration;

namespace Vizelia.FOL.BusinessLayer.Interfaces {
	/// <summary>
	/// Business layer interface for Core.
	/// </summary>
	public interface ICoreBusinessLayer {

		/// <summary>
		/// Gets the tree of assemblies.
		/// </summary>
		/// <returns></returns>
		List<TreeNode> Assembly_GetTree();

		/// <summary>
		/// Begins the importing.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="documentName">Name of the document.</param>
		/// <param name="documentStream">The document stream.</param>
		/// <param name="importOptions">The import options.</param>
		void BeginImporting(Guid operationId, string documentName, MemoryStream documentStream, AzmanImportOptions importOptions);

		/// <summary>
		/// Deletes an existing business entity Building.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool Building_Delete(Building item);

		/// <summary>
		/// Creates a new business entity Building and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse Building_FormCreate(Building item);

		/// <summary>
		/// Loads a specific item for the business entity Building.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse Building_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Building and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse Building_FormUpdate(Building item);

		/// <summary>
		/// Gets a json store for the business entity Building.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<Building> Building_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Building.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<Building> Building_SaveStore(CrudStore<Building> store);

		/// <summary>
		/// Deletes an existing business entity BuildingStorey.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool BuildingStorey_Delete(BuildingStorey item);

		/// <summary>
		/// Creates a new business entity BuildingStorey and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse BuildingStorey_FormCreate(BuildingStorey item);

		/// <summary>
		/// Loads a specific item for the business entity BuildingStorey.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse BuildingStorey_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity BuildingStorey and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse BuildingStorey_FormUpdate(BuildingStorey item);

		/// <summary>
		/// Gets a json store for the business entity BuildingStorey.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<BuildingStorey> BuildingStorey_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity BuildingStorey.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<BuildingStorey> BuildingStorey_SaveStore(CrudStore<BuildingStorey> store);

		/// <summary>
		/// Gets the store of calendar day names.
		/// </summary>
		/// <returns></returns>
		JsonStore<ListElement> CalendarDayName_GetStore();

		/// <summary>
		/// Deletes an existing business entity CalendarEvent.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="KeyLocation">The location key.</param>
		/// <returns></returns>
		bool CalendarEvent_Delete(CalendarEvent item, string KeyLocation);

		/// <summary>
		/// Copy existing CalendarEvent.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy.</param>
		/// <returns></returns>
		FormResponse CalendarEvent_Copy(string[][] keys);

		/// <summary>
		/// Creates a business entity CalendarEvent.
		/// </summary>
		/// <param name="item">The entity CalendarEvent.</param>
		/// <param name="KeyLocation">The location Key</param>
		/// <returns></returns>
		FormResponse CalendarEvent_FormCreate(CalendarEvent item, string KeyLocation);

		/// <summary>
		/// Loads a specific item for the business entity CalendarEvent.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="KeyLocation">The location Key</param>
		/// <returns></returns>
		FormResponse CalendarEvent_FormLoad(string Key, string KeyLocation);

		/// <summary>
		/// Updates a business entity CalendarEvent.
		/// </summary>
		/// <param name="item">The entity CalendarEvent.</param>
		/// <param name="KeyLocation">The location Key</param>
		/// <returns></returns>
		FormResponse CalendarEvent_FormUpdate(CalendarEvent item, string KeyLocation);

		/// <summary>
		/// Gets a dictionnary of all the calendar data as an iCal string in a dictionnary group by key object.
		/// </summary>
		/// <returns></returns>
		Dictionary<string, Tuple<string, string>> CalendarEvent_GetAll();

		/// <summary>
		/// Gets the CalendarEvent store for a location.
		/// </summary>
		/// <param name="KeyLocation">The key of the location.</param>
		/// <param name="category">The Category.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>A json store of the result.</returns>
		JsonStore<CalendarEvent> CalendarEvent_GetStoreFromLocation(string KeyLocation, string category, PagingParameter paging);

		/// <summary>
		/// Gets the CalendarEvent store of occurrences for a location.
		/// </summary>
		/// <param name="KeyLocation">The key location.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="start">The start.</param>
		/// <param name="end">The end.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		JsonStore<CalendarEvent> CalendarEvent_GetStoreOccurence(string KeyLocation, PagingParameter paging, DateTime start, DateTime end, string category);

		/// <summary>
		/// Saves a crud store for the business entity CalendarEvent.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <param name="KeyLocation">The loation Key.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<CalendarEvent> CalendarEvent_SaveStore(CrudStore<CalendarEvent> store, string KeyLocation);

		/// <summary>
		/// Deletes an existing business entity CalendarEventCategory.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool CalendarEventCategory_Delete(CalendarEventCategory item);

		/// <summary>
		/// Creates a new business entity CalendarEventCategory and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse CalendarEventCategory_FormCreate(CalendarEventCategory item);

		/// <summary>
		/// Loads a specific item for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse CalendarEventCategory_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity CalendarEventCategory and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse CalendarEventCategory_FormUpdate(CalendarEventCategory item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		FormResponse CalendarEventCategory_FormUpdateBatch(string[] keys, CalendarEventCategory item);

		/// <summary>
		/// Gets a list for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<CalendarEventCategory> CalendarEventCategory_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a specific item for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		CalendarEventCategory CalendarEventCategory_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets a json store for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<CalendarEventCategory> CalendarEventCategory_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity CalendarEventCategory.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<CalendarEventCategory> CalendarEventCategory_SaveStore(CrudStore<CalendarEventCategory> store);

		/// <summary>
		/// Gets the store of calendar frequency occurrences.
		/// </summary>
		/// <returns></returns>
		JsonStore<ListElement> CalendarFrequencyOccurrence_GetStore();

		/// <summary>
		/// Gets the store of calendar month names.
		/// </summary>
		/// <returns></returns>
		JsonStore<ListElement> CalendarMonthName_GetStore();

		/// <summary>
		/// Gets all the classes that inherit from the type given.
		/// </summary>
		/// <param name="typeName">Name of the type.</param>
		/// <returns></returns>
		JsonStore<ClassDefinition> ClassDefinition_GetStoreByTypeName(string typeName);

		/// <summary>
		/// Deletes an existing business entity ClassificationDefinition.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool ClassificationDefinition_Delete(ClassificationDefinition item);

		/// <summary>
		/// Creates a new business entity ClassificationDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse ClassificationDefinition_FormCreate(ClassificationDefinition item);

		/// <summary>
		/// Loads a specific item for the business entity ClassificationDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse ClassificationDefinition_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity ClassificationDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse ClassificationDefinition_FormUpdate(ClassificationDefinition item);

		/// <summary>
		/// Gets a list for the business entity ClassificationDefinition. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<ClassificationDefinition> ClassificationDefinition_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a specific item for the business entity ClassificationDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		ClassificationDefinition ClassificationDefinition_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets a json store for the ClassificationDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<ClassificationDefinition> ClassificationDefinition_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity ClassificationDefinition.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<ClassificationDefinition> ClassificationDefinition_SaveStore(CrudStore<ClassificationDefinition> store);

		/// <summary>
		/// Deletes an existing business entity ClassificationItem.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool ClassificationItem_Delete(ClassificationItem item);

		/// <summary>
		/// Creates a new business entity ClassificationItem and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse ClassificationItem_FormCreate(ClassificationItem item);

		/// <summary>
		/// Loads a specific item for the business entity ClassificationItem.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse ClassificationItem_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity ClassificationItem and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse ClassificationItem_FormUpdate(ClassificationItem item);

		/// <summary>
		/// Gets a dictionnary for the business entity ClassificationItem, by KeyClassificationItem.
		/// </summary>
		/// <param name="KeyClassificationItemPath">The key classification item path.</param>
		/// <returns></returns>
		Dictionary<string, ClassificationItem> ClassificationItem_GetDictionary(string KeyClassificationItemPath);

		/// <summary>
		/// Classifications the item_ get all.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<ClassificationItem> ClassificationItem_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a ClassificationItem.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		ClassificationItem ClassificationItem_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets a json store for the business entity ClassificationItem.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<ClassificationItem> ClassificationItem_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store for the business entity ClassificationItem.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		JsonStore<ClassificationItem> ClassificationItem_GetStoreByKeyword(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store of ClassificationItem based on related object Key.
		/// </summary>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		JsonStore<ClassificationItem> ClassificationItem_GetStoreByKeyObject(string KeyObject, PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The parent entity.</param>
		/// <param name="excludePset">True to exclude pset nodes.</param>
		/// <param name="excludeRelationship">True to exclude non direct children nodes.</param>
		/// <param name="flgFilter">if set to <c>true</c> Flag filter.</param>
		/// <returns>
		/// A list of treenodes in json.
		/// </returns>
		List<TreeNode> ClassificationItem_GetTree(string Key, ClassificationItem entity, bool excludePset, bool excludeRelationship, bool flgFilter);

		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview by key word and tree level.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <param name="familly">The familly code of notion.</param>
		/// <param name="level">The level.</param>
		/// <param name="flgFilter">if set to <c>true</c> [FLG filter].</param>
		/// <returns>
		/// a list of treenodes in json.
		/// </returns>
		List<TreeNode> ClassificationItem_GetTree(string query, string familly, int level, bool flgFilter);

		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview from a related object.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The parent entity.</param>
		/// <param name="KeyRelatedObject">The key object.</param>
		/// <returns>
		/// A list of treenodes in json.
		/// </returns>
		List<TreeNode> ClassificationItem_GetTreeByRelatedObject(string Key, ClassificationItem entity, string KeyRelatedObject);

		/// <summary>
		/// Gets a json store of ClassificationItem based on ClassificationDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<ClassificationItem> ClassificationItemDefinition_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Creates an association between a ClassificationItem and a Priority.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <param name="KeyPriority">The Key of the priority.</param>
		/// <returns></returns>
		bool ClassificationItemPriority_Add(string KeyClassificationChildren, string KeyClassificationParent, string KeyPriority);

		/// <summary>
		/// Deletes an association between a ClassificationItem and a Priority.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <returns></returns>
		bool ClassificationItemPriority_Delete(string KeyClassificationChildren, string KeyClassificationParent);

		/// <summary>
		/// Creates an Association beetween a list of Pset's Attributes and a ClassificationItem.
		/// </summary>
		/// <param name="Key">the Key of the ClassificationItem.</param>
		/// <param name="psetDefinitions">The list pset definitions.</param>
		/// <param name="psetAttributeDefinitions">The list of pset attribute definitions.</param>
		/// <remarks>Any attributes of a the pset definitions provided will be included, only the specified attributes definition of the last argument will be included.</remarks>
		/// <returns></returns>
		bool ClassificationItemPset_Add(string Key, List<PsetDefinition> psetDefinitions, List<PsetAttributeDefinition> psetAttributeDefinitions);

		/// <summary>
		/// Cleans all the pset values for the classification item
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		void ClassificationItemPset_CleanAllPsetValues(string key);

		/// <summary>
		/// Deletes an Association beetween a list of Pset's Attributes and a ClassificationItem.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="psetDefinitions">The pset definitions.</param>
		/// <param name="psetAttributeDefinitions">The pset attribute definitions.</param>
		/// <param name="shouldClean">if set to <c>true</c> [should clean].</param>
		/// <returns></returns>
		bool ClassificationItemPset_Delete(string Key, List<PsetDefinition> psetDefinitions, List<PsetAttributeDefinition> psetAttributeDefinitions, bool shouldClean);

		/// <summary>
		/// Creates an association between a ClassificationItem and a WorkflowAssemblyDefinition.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <param name="WorkflowAssemblyDefinitionName">Name of the workflow assembly definition.</param>
		/// <param name="WorkflowAssemblyDefinitionAssemblyQualifiedName">Name of the workflow assembly definition assembly qualified.</param>
		/// <returns></returns>
		bool ClassificationItemWorkflowAssemblyDefinition_Add(string KeyClassificationChildren, string KeyClassificationParent, string WorkflowAssemblyDefinitionName, string WorkflowAssemblyDefinitionAssemblyQualifiedName);

		/// <summary>
		/// Deletes an association between a ClassificationItem and a WorkflowAssemblyDefinition.
		/// </summary>
		/// <param name="KeyClassificationChildren">The Key of the ClassificationItem.</param>
		/// <param name="KeyClassificationParent">The Key of the parent ClassificationItem in case the priority is added to a relationship.</param>
		/// <returns></returns>
		bool ClassificationItemWorkflowAssemblyDefinition_Delete(string KeyClassificationChildren, string KeyClassificationParent);

		/// <summary>
		/// Gets all Color names.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		JsonStore<ListElement> Color_GetStore(PagingParameter paging);

		/// <summary>
		/// Convertes the business entities.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <returns></returns>
		List<string> ConvertBusinessEntities(string filename);

		/// <summary>
		/// Gets the store of desktop background.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		JsonStore<ListElement> DesktopBackground_GetStore(PagingParameter paging);

		/// <summary>
		/// Audit the get store by entity key - for the template view.
		/// </summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="entityKey">The entity key.</param>
		/// <returns></returns>
		JsonStore<DetailedAudit> DetailedAudit_GetStoreByKey(string entityName, PagingParameter paging, string entityKey);

		/// <summary>
		/// Deletes an existing business entity Document.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool Document_Delete(Document item);

		/// <summary>
		/// Creates a new business entity Document and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse Document_FormCreate(Document item);

		/// <summary>
		/// Loads a specific item for the business entity Document.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse Document_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Document and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse Document_FormUpdate(Document item);

		/// <summary>
		/// Gets a stream of a document that is an image.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		StreamResult Document_GetImageStream(string key);

		/// <summary>
		/// Gets a specific item for the business entity Document.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		Document Document_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets the metadata item for a Document.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		DocumentMetadata DocumentMetadata_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets the updatable batch properties from an entity type.
		/// </summary>
		/// <param name="entityType">The type of the entity.</param>
		/// <returns></returns>
		List<string> Entity_GetUpdatableBatchProperties(string entityType);

		/// <summary>
		/// Get the count of all entities for a tenant.
		/// </summary>
		/// <returns></returns>
		JsonStore<ListElementGeneric<long>> EntityCount_GetStore();

		/// <summary>
		/// Prepares the documents of the entity
		/// </summary>
		/// <param name="fieldsAndDocuments">The fields and corresponding documents.</param>
		/// <returns></returns>
		FormResponse EntityDocument_FormPrepare(Dictionary<string, Document> fieldsAndDocuments);

		/// <summary>
		/// Gets all elements from an enum. 
		/// </summary>
		/// <param name="enumTypeName">The type name of the enum.</param>
		/// <returns></returns>
		JsonStore<ListElementGeneric<int>> Enum_GetStore(string enumTypeName);

		/// <summary>
		/// Gets all the enums from the application.
		/// </summary>
		/// <returns></returns>
		Dictionary<string, JsonStore<ListElementGeneric<int>>> Enum_GetStores();

		/// <summary>
		/// Gets all the installed Font Family.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		JsonStore<ListElement> FontFamily_GetStore(PagingParameter paging);

		/// <summary>
		/// Deletes an existing business entity Furniture.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool Furniture_Delete(Furniture item);

		/// <summary>
		/// Creates a new business entity Furniture and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse Furniture_FormCreate(Furniture item);

		/// <summary>
		/// Loads a specific item for the business entity Furniture.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse Furniture_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Furniture and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse Furniture_FormUpdate(Furniture item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		FormResponse Furniture_FormUpdateBatch(string[] keys, Furniture item);

		/// <summary>
		/// Gets a list for the business entity Furniture. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<Furniture> Furniture_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a specific item for the business entity Furniture.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		Furniture Furniture_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets a json store for the business entity Furniture.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<Furniture> Furniture_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Furniture.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<Furniture> Furniture_SaveStore(CrudStore<Furniture> store);

		/// <summary>
		/// Gets the general type tree.
		/// </summary>
		/// <param name="typeName">Name of the type.</param>
		/// <param name="classificationItemLocalIdPath">The classification item local id path.</param>
		/// <returns></returns>
		List<TreeNode> GetGeneralTypeTree(List<string> typeName, string classificationItemLocalIdPath);

		/// <summary>
		/// Returns the progress of operationId.
		/// This function is called by a timer on a client, usually when downloading a file, and the long processing function is responsible for updating Session[operationId] with the status of the operation.
		/// </summary>
		/// <param name="operationId">The id of the iframe generated on client side for calling the long process function</param>
		/// <returns>The progress of the operation</returns>
		LongRunningOperationState GetLongRunningOperationProgress(Guid operationId);

		/// <summary>
		/// Gets the long running operation result entity.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <returns>A file result for the given operation ID</returns>
		BaseBusinessEntity GetLongRunningOperationResultEntity(Guid operationId);

		/// <summary>
		/// Gets the long running operation result file.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		StreamResult GetLongRunningOperationResultFile(Guid operationId);

		/// <summary>
		/// Gets the TypeName of a record.
		/// </summary>
		/// <param name="Key">The Key of the record.</param>
		/// <returns>The TypeName of the record.</returns>
		string GetTypeNameFromRecord(string Key);

		/// <summary>
		/// Get history store by key.
		/// </summary>
		/// <param name="entityName">Name of the entity type.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		JsonStore<Audit> Audit_GetStore(string entityName, PagingParameter paging);

		/// <summary>
		/// Deletes an existing business entity Job.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool Job_Delete(Job item);

		/// <summary>
		/// Creates a new business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse Job_FormCreate(Job item);

		/// <summary>
		/// Loads a specific item for the business entity Job.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse Job_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse Job_FormUpdate(Job item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		FormResponse Job_FormUpdateBatch(string[] keys, Job item);

		/// <summary>
		/// Gets a list for the business entity Job. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<Job> Job_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a specific item for the business entity Job.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		Job Job_GetItem(string Key, params string[] fields);


		/// <summary>
		/// Executes the selected job
		/// </summary>
		/// <param name="key">The entity Key.</param>
		/// <returns></returns>
		Job Job_ExecuteItem(string key);

		/// <summary>
		/// Pauses the selected job
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		Job Job_PauseItem(string Key);


		/// <summary>
		/// Resumes the selected job
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		Job Job_ResumeItem(string Key);

		/// <summary>
		/// Gets a json store for the business entity Job.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<Job> Job_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Job.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<Job> Job_SaveStore(CrudStore<Job> store);

		/// <summary>
		/// Gets all elements from a list. 
		/// </summary>
		/// <returns></returns>
		JsonStore<ListElement> ListElement_GetStore(string listName);

		/// <summary>
		/// Gets a json store of exposed Cultures.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		JsonStore<LocalizationCulture> LocalizationCulture_GetStore(PagingParameter paging);

		/// <summary>
		/// Creates a new CreateLocalizationResource.
		/// </summary>
		/// <param name="item">The localization resource item.</param>
		/// <returns>The FormResponse object.</returns>
		FormResponse LocalizationResource_FormCreate(LocalizationResource item);

		/// <summary>
		/// Updates a CreateLocalizationResource.
		/// </summary>
		/// <param name="item">The localization resource item.</param>
		/// <returns>The FormResponse object.</returns>
		FormResponse LocalizationResource_FormUpdate(LocalizationResource item);

		/// <summary>
		/// Gets a specific item for the business entity LocalizationResource.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		LocalizationResource LocalizationResource_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets all the localization resource.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<LocalizationResource> LocalizationResource_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets all the localization message for a specified language.
		/// </summary>
		/// <param name="language">2 letters Language source. e.g. "en , da" for English or Danish.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		JsonStore<LocalizationResource> LocalizationResource_GetStore(string language, PagingParameter paging);

		/// <summary>
		/// Saves a LocalizationResource store.
		/// </summary>
		/// <param name="language">The base language.</param>
		/// <param name="store">The crud store.</param>
		/// <returns></returns>
		JsonStore<LocalizationResource> LocalizationResource_SaveStore(string language, CrudStore<LocalizationResource> store);

		/// <summary>
		/// Translate Text using Google Translate.
		/// </summary>
		/// <param name="input">The string to translate.</param>
		/// <param name="languageFrom">2 letters Language source. e.g. "en , da" language pair means to translate from English to Danish</param>
		/// <param name="languageTo">2 letters language destination. e.g. "en , da" language pair means to translate from English to Danish</param>
		/// <returns>Translated to String</returns>
		string LocalizationResource_TranslateText(string input, string languageFrom, string languageTo);

		/// <summary>
		/// Gets a dictionnary for the business entity Location, by KeyLocation. 
		/// </summary>
		/// <returns></returns>
		Dictionary<string, Location> Location_GetDictionary();

		/// <summary>
		/// Gets a specific item for the business entity Location.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		Location Location_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets a list of item for the business entity Location.
		/// </summary>
		/// <param name="Keys">The keys.</param>
		/// <returns></returns>
		List<Location> Location_GetList(string[] Keys);

		/// <summary>
		/// Get the SpatialHierarchy as a list of Location business entity.
		/// </summary>
		/// <param name="Keys">The keys.</param>
		/// <param name="hierarchyDirection">The hierarchy direction.</param>
		/// <returns></returns>
		List<Location> Location_GetSpatialHierarchy(string[] Keys, HierarchyDirection hierarchyDirection = HierarchyDirection.ASC);

		/// <summary>
		/// Get all Location entities.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<Location> Location_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a json store for the business entity Location.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<Location> Location_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Creates Location entity.
		/// </summary>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		FormResponse Location_FormCreate(Location location);

		/// <summary>
		/// Updates Location entity.
		/// </summary>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		FormResponse Location_FormUpdate(Location location);

		/// <summary>
		/// Deletes Location entity.
		/// </summary>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		bool Location_Delete(Location location);

		/// <summary>
		/// Gets the store of logging for a specific method.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="methodName">Name of the method.</param>
		/// <returns></returns>
		JsonStore<Logging> Logging_GetStoreByMethodName(PagingParameter paging, string methodName);

		/// <summary>
		/// Saves a crud store for the business entity Logging.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<Logging> Logging_SaveStore(CrudStore<Logging> store);

		/// <summary>
		/// Gets all the methods on classes that inherit from the type given that have the display attribute.
		/// </summary>
		/// <param name="typeName">Name of the type.</param>
		/// <returns></returns>
		JsonStore<MethodDefinition> MethodDefinition_GetStoreByTypeName(string typeName);

		/// <summary>
		/// Saves a crud store for the business entity ObjectClassification.
		/// </summary>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="store">The crud store.</param>
		/// <returns>
		/// A json store containing the value after the save, or a message for each error.
		/// </returns>
		JsonStore<ObjectClassification> ObjectClassification_SaveStoreByKeyObject(string KeyObject, CrudStore<ClassificationItem> store);

		/// <summary>
		/// Deletes an existing business entity Occupant.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool Occupant_Delete(Occupant item);

		/// <summary>
		/// Creates a new business entity Occupant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse Occupant_FormCreate(Occupant item);

		/// <summary>
		/// Loads a specific item for the business entity Occupant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse Occupant_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Occupant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse Occupant_FormUpdate(Occupant item);

		/// <summary>
		/// Update batch (mass update) a series of items based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		FormResponse Occupant_FormUpdateBatch(string[] keys, Occupant item);

		/// <summary>
		/// Gets a list for the business entity Occupant.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<Occupant> Occupant_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a specific item for the business entity Occupant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		Occupant Occupant_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets the occupant pset.
		/// </summary>
		/// <param name="PsetName">Name of the pset.</param>
		/// <param name="KeyOccupant">The key occupant.</param>
		Dictionary<string, string> Occupant_GetPset(string PsetName, string KeyOccupant);

		/// <summary>
		/// Gets the Occupant store.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns>A json store of the result.</returns>
		JsonStore<Occupant> Occupant_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Occupant.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<Occupant> Occupant_SaveStore(CrudStore<Occupant> store);


		/// <summary>
		/// Deletes an existing business entity Organization.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool Organization_Delete(Organization item);

		/// <summary>
		/// Creates a new business entity Organization and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse Organization_FormCreate(Organization item);

		/// <summary>
		/// Loads a specific item for the business entity Organization.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse Organization_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Organization and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse Organization_FormUpdate(Organization item);

		/// <summary>
		/// Gets a list for the business entity Organization.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<Organization> Organization_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a specific item for the business entity Organization.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		Organization Organization_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets a json store for the business entity Organization.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<Organization> Organization_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Generates the json structure of the Organization treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		List<TreeNode> Organization_GetTree(string Key);

		/// <summary>
		/// Saves a crud store for the business entity Organization.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<Organization> Organization_SaveStore(CrudStore<Organization> store);

		/// <summary>
		/// Gets the priority assigned to a ClassificationItem.
		/// </summary>
		/// <param name="KeyClassificationChildren">The key classification children.</param>
		/// <param name="KeyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		Priority Priority_GetItemByClassification(string KeyClassificationChildren, string KeyClassificationParent);

		/// <summary>
		/// Gets the priority assigned to a any upper level of ClassificationItem Hierarchy.
		/// </summary>
		/// <param name="KeyClassificationChildren">The key classification children.</param>
		/// <param name="KeyClassificationParent">The key classification parent.</param>
		/// <returns></returns>
		Priority Priority_GetItemByClassificationAscendant(string KeyClassificationChildren, string KeyClassificationParent);

		/// <summary>
		/// Gets a list for the business entity Pset. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<Pset> Pset_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a specific item for the business entity Pset.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		Pset Pset_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets the pset for an object.
		/// </summary>
		/// <param name="KeyObjectArray">The key object array.</param>
		/// <returns></returns>
		List<Pset> Pset_GetList(string[] KeyObjectArray);

		/// <summary>
		/// Generates the json structure of the Pset treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="usageName">The usageName (IfcBuilding, IfcActionRequest etc...) for filtering the results. If null no filter should occur.</param>
		/// <returns>a list of treenodes in json.</returns>
		List<TreeNode> Pset_GetTree(string Key, string usageName);

		/// <summary>
		/// Deletes a PsetAttributeDefinition
		/// </summary>
		/// <param name="item">The pset attribute  definition item to delete.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		bool PsetAttributeDefinition_Delete(PsetAttributeDefinition item);

		/// <summary>
		/// Creates a new PsetAttributeDefinition.
		/// </summary>
		/// <param name="item">The pset attribute  definition item.</param>
		/// <returns>The FormResponse object.</returns>
		FormResponse PsetAttributeDefinition_FormCreate(PsetAttributeDefinition item);

		/// <summary>
		/// Updates a PsetAttributeDefinition.
		/// </summary>
		/// <param name="item">The pset attribute definition item.</param>
		/// <returns>The FormResponse object.</returns>
		FormResponse PsetAttributeDefinition_FormUpdate(PsetAttributeDefinition item);

		/// <summary>
		/// Get All PsetAttributes(s) definitions.
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="location"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		List<PsetAttributeDefinition> PsetAttributeDefinition_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Get PseAttributeDefinition Item by key.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		PsetAttributeDefinition PsetAttributeDefinition_GetItem(string key, params string[] fields);

		/// <summary>
		/// Get All PsetValue(s).
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="location"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		List<PsetValue> PsetValue_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Get PsetValue by key.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		PsetValue PsetValue_GetItem(string key, params string[] fields);

		/// <summary>
		/// Updates PsetValue item.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		FormResponse PsetValue_FormUpdate(PsetValue item);
		
		/// <summary>
		/// Creates PsetValue item.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		FormResponse PsetValue_FormCreate(PsetValue item);

		/// <summary>
		/// Deletes the PsetValue item.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		bool PsetValue_Delete(PsetValue item);

		
		/// <summary>
		/// Deletes an existing business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool PsetAttributeHistorical_Delete(PsetAttributeHistorical item);

		/// <summary>
		/// Creates a new business entity PsetAttributeHistorical and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse PsetAttributeHistorical_FormCreate(PsetAttributeHistorical item);

		/// <summary>
		/// Loads a specific item for the business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse PsetAttributeHistorical_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity PsetAttributeHistorical and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse PsetAttributeHistorical_FormUpdate(PsetAttributeHistorical item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		FormResponse PsetAttributeHistorical_FormUpdateBatch(string[] keys, PsetAttributeHistorical item);

		/// <summary>
		/// Gets a list for the business entity PsetAttributeHistorical. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<PsetAttributeHistorical> PsetAttributeHistorical_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a dictionary of all business entities PsetAttributeHistorical. 
		/// </summary>
		/// <returns></returns>
		Dictionary<string, PsetAttributeHistorical> PsetAttributeHistorical_GetDictionary();

		/// <summary>
		/// Gets a specific item for the business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		PsetAttributeHistorical PsetAttributeHistorical_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets a json store for the business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<PsetAttributeHistorical> PsetAttributeHistorical_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of PsetAttributeHistorical for a specific Object.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="KeyPropertySingleValue">The key property single value.</param>
		/// <returns>
		/// A JsonStore.
		/// </returns>
		JsonStore<PsetAttributeHistorical> PsetAttributeHistorical_GetStoreByKeyObject(PagingParameter paging, string KeyObject, string KeyPropertySingleValue);

		/// <summary>
		/// Saves a crud store for the business entity PsetAttributeHistorical.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<PsetAttributeHistorical> PsetAttributeHistorical_SaveStore(CrudStore<PsetAttributeHistorical> store);

		/// <summary>
		/// Deletes a PsetDefinition
		/// </summary>
		/// <param name="item">The pset definition item to delete</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		bool PsetDefinition_Delete(PsetDefinition item);

		/// <summary>
		/// Creates a new PsetDefinition.
		/// </summary>
		/// <param name="item">The pset definition item.</param>
		/// <returns>The FormResponse object.</returns>
		FormResponse PsetDefinition_FormCreate(PsetDefinition item);

		/// <summary>
		/// Updates a PsetDefinition.
		/// </summary>
		/// <param name="item">The pset definition item.</param>
		/// <returns>The FormResponse object.</returns>
		FormResponse PsetDefinition_FormUpdate(PsetDefinition item);

		/// <summary>
		/// Get all the PsetDefinition(s).
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="location"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		List<PsetDefinition> PsetDefinition_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Get PsetDefinitionItem by key.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="fields"></param>
		/// <returns></returns>
		PsetDefinition PsetDefinition_GetItem(string key, params string[] fields);

		/// <summary>
		/// Gets the pset definition list corresponding to a specific usage name.
		/// </summary>
		/// <param name="UsageName">Name of the usage.</param>
		/// <param name="KeyClassificationItem">The KeyClassificationItem (optional).</param>
		/// <returns></returns>
		List<PsetDefinition> PsetDefinition_GetListByUsageName(string UsageName, string KeyClassificationItem);


		/// <summary>
		/// Gets the pset definition store corresponding to a specific usage name.
		/// </summary>
		/// <param name="UsageName">Name of the usage.</param>
		/// <param name="KeyClassificationItem">The KeyClassificationItem (optional).</param>
		/// <returns></returns>
		JsonStore<PsetDefinition> PsetDefinition_GetStoreByUsageName(string UsageName, string KeyClassificationItem);


		/// <summary>
		/// Gets the pset list specified.
		/// </summary>
		/// <param name="listName">The name of the psetlist to retreive.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		JsonStore<ListElement> PsetListElement_GetStore(string listName, PagingParameter paging);

		/// <summary>
		/// Saves a Pset List.
		/// </summary>
		/// <param name="listName">The name of the list to save.</param>
		/// <param name="store">The crud store of the list.</param>
		/// <returns></returns>
		JsonStore<ListElement> PsetListElement_SaveStore(string listName, CrudStore<ListElement> store);

		/// <summary>
		/// Deletes an existing business entity Site.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool Site_Delete(Site item);

		/// <summary>
		/// Creates a new business entity Site and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse Site_FormCreate(Site item);

		/// <summary>
		/// Loads a specific item for the business entity Site.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse Site_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Site and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse Site_FormUpdate(Site item);

		/// <summary>
		/// Return the max level of sites in the spatial hierarchy.
		/// </summary>
		/// <returns></returns>
		int Site_GetMaxLevel();

		/// <summary>
		/// Gets a json store for the business entity Site.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<Site> Site_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Site.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<Site> Site_SaveStore(CrudStore<Site> store);

		/// <summary>
		/// Deletes an existing business entity Space.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool Space_Delete(Space item);

		/// <summary>
		/// Creates a new business entity Space and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse Space_FormCreate(Space item);

		/// <summary>
		/// Loads a specific item for the business entity Space.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse Space_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Space and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse Space_FormUpdate(Space item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		FormResponse Space_FormUpdateBatch(string[] keys, Space item);

		/// <summary>
		/// Gets a list for the business entity Space. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<Space> Space_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a specific item for the business entity Space.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		Space Space_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets a json store for the business entity Space.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<Space> Space_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity Space.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<Space> Space_SaveStore(CrudStore<Space> store);

        /// <summary>
        /// Generates the json structure of the Spatial treeview.
        /// </summary>
        /// <param name="Key">The Key of the parent node.</param>
        /// <param name="flgFilter">True to filter the result, false otherwise.</param>
        /// <param name="ExcludeMetersAndAlarms">if set to <c>true</c> [filter meters and alarms out of the response tree].</param>
        /// <returns>
        /// a list of treenodes in json.
        /// </returns>
        List<TreeNode> Spatial_GetTree(string Key, bool flgFilter, bool ExcludeMetersAndAlarms);

		/// <summary>
		/// Gets a json store of locations found by keyword.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="keyword">The keyword.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		JsonStore<Location> Spatial_GetStoreByKeyword(PagingParameter paging, string keyword);

		/// <summary>
		/// Creates a new business entity TaskScheduler and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse TaskScheduler_FormCreate(TaskScheduler item);

		/// <summary>
		/// Gets the store of time zones.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		JsonStore<ListElement> TimeZone_GetStore(PagingParameter paging);

		/// <summary>
		/// Deletes an existing business entity TraceEntry.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool TraceEntry_Delete(TraceEntry item);

		/// <summary>
		/// Gets a json store for the business entity TraceEntry.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<TraceEntry> TraceEntry_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a store of TraceEntries for a specific request id.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="requestId">The request id.</param>
		/// <returns></returns>
		JsonStore<TraceEntry> TraceEntry_GetStoreByRequestId(PagingParameter paging, string requestId);

		/// <summary>
		/// Gets the TraceEntry for an exception that occured during the request specified by the request id.
		/// </summary>
		/// <param name="requestId">The request id.</param>
		/// <returns></returns>
		TraceEntry TraceEntry_GetErrorEntryByRequestId(string requestId);

		/// <summary>
		/// Saves a crud store for the business entity TraceEntry.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<TraceEntry> TraceEntry_SaveStore(CrudStore<TraceEntry> store);

		/// <summary>
		/// Updates the long running operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="clientRequest">The client request.</param>
		void UpdateLongRunningOperation(Guid operationId, LongRunningOperationClientRequest clientRequest);

		/// <summary>
		/// Creates UserProfilePreference entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		FormResponse UserProfilePreference_FormCreate(UserProfilePreference item);

		/// <summary>
		/// Updates UserProfilePreference entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		FormResponse UserProfilePreference_FormUpdate(UserProfilePreference item);

		/// <summary>
		/// Deletes UserProfilePreference entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		bool UserProfilePreference_Delete(UserProfilePreference item);
	
		/// <summary>
		/// Get list of UserProfilePreference for the current user.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <returns></returns>
		List<UserProfilePreference> UserProfilePreference_GetListByUser(string userName = null);

		/// <summary>
		/// Gets a specific item for the business entity UserProfilePreference.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		UserProfilePreference UserProfilePreference_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Get list of UserProfilePreference for the current user.
		/// Used by OData API with dynamic method invocation therfore using GetAll naming convention and parameters.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<UserProfilePreference> UserProfilePreference_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Sets the preference key and value for the user
		/// </summary>
		/// <param name="preferenceKey">The preference key.</param>
		/// <param name="preferenceValue">The preference value.</param>
		/// <param name="userName">Name of the user.</param>
		void UserProfilePreference_Update(string preferenceKey, string preferenceValue, string userName = null);

		/// <summary>
		/// Sets the preference key and value for the tenant
		/// </summary>
		/// <param name="preferenceKey">The preference key.</param>
		/// <param name="preferenceValue">The preference value.</param>
		void UserProfilePreference_UpdateGlobal(string preferenceKey, string preferenceValue);

		/// <summary>
		/// Deletes all UserProfilePreference for the current user.
		/// </summary>
		bool UserProfilePreference_DeleteAllData();

		/// <summary>
		/// Gets a specific item for the business entity UserPreferences.
		/// </summary>
		/// <param name="key">The entity Key.</param>
		/// <returns></returns>
		FormResponse UserPreferences_FormLoad(string key);

		/// <summary>
		/// Creates UserPreferences entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		FormResponse UserPreferences_FormCreate(UserPreferences item);

		/// <summary>
		/// Updates UserPreferences entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		FormResponse UserPreferences_FormUpdate(UserPreferences item);
		
		/// <summary>
		/// Gets the differents versions of the frameworks used encapsulated in a business entity.
		/// </summary>
		/// <returns></returns>
		Versions Versions_GetItem();

		/// <summary>
		/// Purges the server logs.
		/// </summary>
		void PurgeLogs();

		/// <summary>
		/// Tenant_s the send completion email.
		/// </summary>
		/// <param name="password">The password.</param>
		/// <param name="tenant">The tenant.</param>
		void Tenant_SendCompletionEmail(string password, Tenant tenant);

		/// <summary>
		/// Gets the child entities by parent job step.
		/// </summary>
		/// <typeparam name="TChildEntity">The type of the child entity.</typeparam>
		/// <param name="keyJob">The key job.</param>
		/// <param name="keyStep">The key step.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		JsonStore<TChildEntity> GetChildEntitiesByParentJobStep<TChildEntity>(string keyJob, string keyStep, PagingParameter paging) where TChildEntity : BaseBusinessEntity;

		/// <summary>
		/// Deletes an existing business entity AuditEntity.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool AuditEntity_Delete(AuditEntity item);

		/// <summary>
		/// Creates a new business entity AuditEntity and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse AuditEntity_FormCreate(AuditEntity item);

		/// <summary>
		/// Loads a specific item for the business entity AuditEntity.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse AuditEntity_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity AuditEntity and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse AuditEntity_FormUpdate(AuditEntity item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		FormResponse AuditEntity_FormUpdateBatch(string[] keys, AuditEntity item);

		/// <summary>
		/// Gets a list for the business entity AuditEntity. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<AuditEntity> AuditEntity_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a specific item for the business entity AuditEntity.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		AuditEntity AuditEntity_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Get store of AuditEntity.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		JsonStore<AuditEntity> AuditEntity_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity AuditEntity.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<AuditEntity> AuditEntity_SaveStore(CrudStore<AuditEntity> store);

		/// <summary>
		/// Get all AuditEntity aggregated.
		/// </summary>
		/// <returns>A json store containing the AuditEntity aggregated</returns>
		JsonStore<AuditEntity> AuditEntity_GetAllAggregated();

		/// <summary>
		/// Get all Auditable entities.
		/// </summary>
		/// <returns>A json store containing the AuditableEntityAttribute classes</returns>
		JsonStore<ListElement> AuditableEntity_GetStore();

		/// <summary>
		/// Deletes an existing business entity Logo.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool Logo_Delete(Logo item);

		/// <summary>
		/// Creates a new business entity Logo and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse Logo_FormCreate(Logo item);

		/// <summary>
		/// Loads a specific item for the business entity Logo.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse Logo_FormLoad(string Key);

		/// <summary>
		/// Updates an existing business entity Logo and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse Logo_FormUpdate(Logo item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		FormResponse Logo_FormUpdateBatch(string[] keys, Logo item);

		/// <summary>
		/// Gets a list for the business entity Logo.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<Logo> Logo_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a specific item for the business entity Logo.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		Logo Logo_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets a json store for the business entity Logo.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		JsonStore<Logo> Logo_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Returns the stream of the image  from a Logo.
		/// </summary>
		/// <param name="Key">The key of the logo.</param>
		/// <returns></returns>
		StreamResult Logo_GetStreamImage(string Key);

		/// <summary>
		/// Saves a crud store for the business entity Logo.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>
		/// A json store containing the value after the save, or a message for each error.
		/// </returns>
		JsonStore<Logo> Logo_SaveStore(CrudStore<Logo> store);

	    /// <summary>
	    /// Creates a default occupant from user details.
	    /// </summary>
	    /// <param name="lastName">The last name.</param>
	    /// <param name="firstName">The first name.</param>
	    /// <param name="username">The username.</param>
	    /// <param name="constOccupantLocalIdPrefix">The const occupant local id prefix.</param>
	    /// <param name="updateExisting">Whether to update an existing occupant. Defaults to false (don't update).</param>
	    /// <returns></returns>
	    Occupant Occupant_CreateFromUser(string lastName, string firstName, string username, string constOccupantLocalIdPrefix, bool updateExisting = false);

		/// <summary>
		/// Determines whether the client and the server have the same time.
		/// </summary>
		/// <param name="clientTime">The client time.</param>
		/// <returns>
		///   <c>true</c> if the client and the server have the same time. otherwise, <c>false</c>.
		/// </returns>
		bool IsTimeSynchronized(DateTime clientTime);

		/// <summary>
		/// Get a store of TraceSummaries.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		JsonStore<TraceSummary> TraceSummary_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Get a store of TraceEntries for a specific job run instance request id.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="jobRunInstanceRequestId">The job run instance request id.</param>
		/// <returns></returns>
		JsonStore<TraceEntry> TraceEntry_GetStoreByJobRunInstanceRequestId(PagingParameter paging, string jobRunInstanceRequestId);

		/// <summary>
		/// Jobs the instance_ get store by job id.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="keyJob">The key job.</param>
		/// <returns></returns>
		JsonStore<JobInstance> JobInstance_GetStoreByJobId(PagingParameter paging, string keyJob);

		/// <summary>
		/// Gets the application settings.
		/// </summary>
		/// <returns></returns>
		ApplicationSettings GetApplicationSettings();



		/// <summary>
		/// Gets a json store for the business entity Link.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<Link> Link_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store for the business entity Link that are roots.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<Link> Link_GetStoreRootOnly(PagingParameter paging, PagingLocation location);


		/// <summary>
		/// Saves a crud store for the business entity Link.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<Link> Link_SaveStore(CrudStore<Link> store);

		/// <summary>
		/// Gets a list for the business entity Link. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<Link> Link_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a specific item for the business entity Link.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		Link Link_GetItem(string Key, params string[] fields);


		/// <summary>
		/// Loads a specific item for the business entity Link.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		FormResponse Link_FormLoad(string Key);


		/// <summary>
		/// Creates a new business entity Link and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse Link_FormCreate(Link item);

		/// <summary>
		/// Updates an existing business entity Link and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse Link_FormUpdate(Link item);

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		FormResponse Link_FormUpdateBatch(string[] keys, Link item);

		/// <summary>
		/// Deletes an existing business entity Link.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool Link_Delete(Link item);


		/// <summary>
		/// localize text before login.
		/// </summary>
		/// <param name="msgCode">The message code.</param>
		/// <param name="returnNullIfNotFound">if set to <c>true</c> [return null if not found].</param>
		/// <returns></returns>
		string PreLoginLocalizeText(string msgCode, bool returnNullIfNotFound = false);

		/// <summary>
		/// Pre login localize text.
		/// </summary>
		/// <param name="msgCodes">The message codes.</param>
		/// <param name="returnNullIfNotFound">if set to <c>true</c> [return null if not found].</param>
		/// <returns></returns>
		Dictionary<string, string> PreLoginLocalizeText(List<string> msgCodes, bool returnNullIfNotFound = false);

		/// <summary>
		/// Deletes an existing business entity CustomData.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool CustomData_Delete(CustomData item);

		/// <summary>
		/// Deletes all custom data by container name and key user.
		/// </summary>
		/// <param name="containerName">Name of the container.</param>
		/// <param name="keyUser">The key user.</param>
		bool CustomData_DeleteAll(string containerName, string keyUser = null);

		/// <summary>
		/// Creates a new business entity CustomData and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse CustomData_FormCreate(CustomData item);

		/// <summary>
		/// Updates an existing business entity CustomData and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		FormResponse CustomData_FormUpdate(CustomData item);

		/// <summary>
		/// Gets a list for the business entity CustomData. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<CustomData> CustomData_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets a specific item for the business entity CustomData.
		/// </summary>
		/// <param name="key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		CustomData CustomData_GetItem(string key, params string[] fields);

		/// <summary>
		/// Get the LatestHistoricalPset values by location key
		/// </summary>
		/// <param name="keyLocation">The location key</param>
		/// <returns></returns>
		List<LatestHistoricalPset> GetLatestHistoricalPsetValuesByLocationKey(string keyLocation);

		/// <summary>
		/// Get the LatestHistoricalPset values of locations by Pset
		/// </summary>
		/// <param name="keyPropertySet">The pset key</param>
		/// <returns></returns>
		List<LatestHistoricalPset> GetLatestHistoricalPsetValuesOfLocationsByPset(string keyPropertySet);
	}
}
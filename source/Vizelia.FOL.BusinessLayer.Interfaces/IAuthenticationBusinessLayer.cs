﻿using System;
using System.Collections.Generic;
using System.IO;
using Vizelia.FOL.BusinessEntities;


namespace Vizelia.FOL.BusinessLayer.Interfaces {

	/// <summary>
	/// Business layer interface for Authentication.
	/// </summary>
	public interface IAuthenticationBusinessLayer {

		/// <summary>
		/// Gets a list of auhorization for a specific application group.
		/// </summary>
		/// <param name="applicationGroupId">The application group id for which to retreive the authorizations.</param>
		/// <returns></returns>
		List<FOLMembershipUser> ApplicationGroup_Authorization_GetList(string applicationGroupId);

		/// <summary>
		/// Gets a store of auhorization for a specific application groups.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="applicationGroupId">The application group id for which to retreive the authorizations.</param>
		/// <returns></returns>
		JsonStore<FOLMembershipUser> ApplicationGroup_Authorization_GetStore(PagingParameter paging, string applicationGroupId);

		/// <summary>
		/// Gets a store of roles for a specific application group.
		/// </summary>
		/// <param name="applicationGroupId">The application group id for which to retreive the authorizations.</param>
		/// <returns></returns>
		List<AuthorizationItem> ApplicationGroup_AzManRole_GetList(string applicationGroupId);

		/// <summary>
		/// Gets a store of auhorization for a specific application groups.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="applicationGroupId">The application group id for which to retreive the authorizations.</param>
		/// <returns></returns>
		JsonStore<AuthorizationItem> ApplicationGroup_AzManRole_GetStore(PagingParameter paging, string applicationGroupId);

		/// <summary>
		/// Deletes an ApplicationGroup entity.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool ApplicationGroup_Delete(ApplicationGroup item);

		/// <summary>
		/// Creates a new ApplicationGroup entity.
		/// </summary>
		/// <param name="item">The ApplicationGroup entity to create.</param>
		/// <param name="authorizations">The authorizations.</param>
		/// <param name="roles">The roles.</param>
		/// <returns>A json form response.</returns>
		FormResponse ApplicationGroup_FormCreate(ApplicationGroup item, CrudStore<FOLMembershipUser> authorizations, CrudStore<AuthorizationItem> roles);

		/// <summary>
		/// Loads a ApplicationGroup entity.
		/// </summary>
		/// <param name="Key">The Key of the ApplicationGroup.</param>
		/// <returns></returns>
		FormResponse ApplicationGroup_FormLoad(string Key);

		/// <summary>
		/// Updates an ApplicationGroup entity.
		/// </summary>
		/// <param name="item">The ApplicationGroup entity to update</param>
		/// <param name="authorizations">The authorizations.</param>
		/// <param name="roles">The roles.</param>
		/// <returns>A json form response.</returns>
		FormResponse ApplicationGroup_FormUpdate(ApplicationGroup item, CrudStore<FOLMembershipUser> authorizations, CrudStore<AuthorizationItem> roles);

		/// <summary>
		/// Gets an ApplicationGroup entity by its name.
		/// </summary>
		/// <param name="name">The name of the ApplicationGroup.</param>
		/// <returns></returns>
		ApplicationGroup ApplicationGroup_GetByName(string name);

		/// <summary>
		/// Gets a store of all application groups.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		JsonStore<ApplicationGroup> ApplicationGroup_GetStore(PagingParameter paging);

		/// <summary>
		/// Generates the json structure of the ApplicationGroup treeview.
		/// </summary>
		/// <returns></returns>
		List<TreeNode> ApplicationGroup_GetTree();

		/// <summary>
		/// Saves the store of Application Groups.
		/// </summary>
		/// <param name="store">The crud store to save.</param>
		/// <returns></returns>
		JsonStore<ApplicationGroup> ApplicationGroup_SaveStore(CrudStore<ApplicationGroup> store);

		/// <summary>
		/// Gets all the authentication providers declared in <vizelia></vizelia> section of web.config.
		/// The returned store is used to build a domain combobox in the login form.
		/// </summary>
		/// <returns>The store of authentication providers.</returns>
		JsonStore<ListElement> AuthenticationProvider_GetStore();

		/// <summary>
		/// Gets an AuthorizationItem by its name.
		/// </summary>
		/// <param name="name">The name of the AuthorizationItem.</param>
		/// <returns></returns>
		AuthorizationItem AuthorizationItem_GetByName(string name);

		/// <summary>
		/// Gets a store of all authorizations items of a specific user.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="username">The user name.</param>
		/// <returns></returns>
		JsonStore<AuthorizationItem> AuthorizationItem_GetStore(PagingParameter paging, string username);

		/// <summary>
		/// Exports the AzMan configuration as a xml stream file.
		/// </summary>
		/// <returns>The xml stream file.</returns>
		MemoryStream AzMan_Export();

		/// <summary>
		/// Exports the AzMan configuration as a xml stream file as a long-running operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		void BeginAzManExport(Guid operationId);

		/// <summary>
		/// Gets a store of auhorization for a specific application groups.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="filterId">The AzMan filter id for which to retreive the authorizations.</param>
		/// <returns></returns>
		JsonStore<ApplicationGroup> AzManFilter_Authorization_GetStore(PagingParameter paging, string filterId);


		/// <summary>
		/// Gets a store of user authorization for a specific application groups.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="filterId">The AzMan filter id for which to retreive the authorizations.</param>
		/// <returns></returns>
		JsonStore<FOLMembershipUser> AzManFilter_UserAuthorization_GetStore(PagingParameter paging, string filterId);

		/// <summary>
		/// Get a store of azman filters, projects by keyword
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="keyword"></param>
		/// <returns></returns>
		JsonStore<AuthorizationItem> AzManFilter_GetStoreByKeyword(PagingParameter paging, string keyword);

		/// <summary>
		/// Gets azman filters by location
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="keyLocation"></param>
		/// <returns></returns>
		JsonStore<AuthorizationItem> AzManFilter_GetStoreByLocation(PagingParameter paging, string keyLocation);

		/// <summary>
		/// Deletes a AzMan filter item.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		bool AzManFilter_Delete(AuthorizationItem item);

		/// <summary>
		/// Creates a new AzMan filter item.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="authorizations">The list of application groups.</param>
		/// <param name="users">The list of users</param>
		/// <returns></returns>
		FormResponse AzManFilter_FormCreate(AuthorizationItem item, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, CrudStore<ApplicationGroup> authorizations, CrudStore<FOLMembershipUser> users);

		/// <summary>
		/// Loads an AzMan filter item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns></returns>
		FormResponse AzManFilter_FormLoad(string Key);

		/// <summary>
		/// Updates an existing AzMan filter item.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="filterLink">The filter link item </param>
		/// <param name="authorizations">The list of application groups.</param>
		/// <param name="users">The list of users.</param>
		/// <returns></returns>
		FormResponse AzManFilter_FormUpdate(AuthorizationItem item, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, List<SecurableEntity> filterLink, CrudStore<ApplicationGroup> authorizations, CrudStore<FOLMembershipUser> users);

		/// <summary>
		/// Generates the json structure of the AzMan Filter treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		List<TreeNode> AzManFilter_GetTree(string Key);

		/// <summary>
		/// Gets the store of filters for an AzMan filter element.
		/// </summary>
		/// <param name="paging">The paging parameter</param>
		/// <param name="filterId">The id of the AzMan filter element.</param>
		/// <param name="typeName">Name of the securable business entity.</param>
		/// <returns></returns>
		JsonStore<SecurableEntity> AzManFilter_Filter_GetStore(PagingParameter paging, string filterId, string typeName);

		/// <summary>
		/// Adds members to an AzManItem.
		/// </summary>
		/// <param name="parent">The AzManItem parent.</param>
		/// <param name="children">The list of AzManItem to add as members.</param>
		/// <returns></returns>
		bool AzManItem_AddMembers(AuthorizationItem parent, List<AuthorizationItem> children);

		/// <summary>
		/// Removes members from an AzManItem. 
		/// </summary>
		/// <param name="parent">The AzManItem parent.</param>
		/// <param name="children">The list of AzManItem to remove as members.</param>
		/// <returns></returns>
		bool AzManItem_RemoveMembers(AuthorizationItem parent, List<AuthorizationItem> children);

		/// <summary>
		/// Creates a new AzMan Operation item.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		FormResponse AzManOperation_FormCreate(AuthorizationItem item);

		/// <summary>
		/// Loads a AzMan Operation item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns></returns>
		FormResponse AzManOperation_FormLoad(string Key);

		/// <summary>
		/// Updates an existing AzMan Operation item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		FormResponse AzManOperation_FormUpdate(AuthorizationItem item);

		/// <summary>
		/// Gets a store of all operations.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		JsonStore<AuthorizationItem> AzManOperation_GetStore(PagingParameter paging);

		/// <summary>
		/// Saves a crud store for an AzMan item Operation.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<AuthorizationItem> AzManOperation_SaveStore(CrudStore<AuthorizationItem> store);

		/// <summary>
		/// Creates a new AzMan Role item.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="authorizations">The list of application groups.</param>
		/// <returns></returns>
		FormResponse AzManRole_FormCreate(AuthorizationItem item, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, List<ApplicationGroup> authorizations);

		/// <summary>
		/// Loads a AzMan Role item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns></returns>
		FormResponse AzManRole_FormLoad(string Key);

		/// <summary>
		/// Updates an existing AzMan Role item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		FormResponse AzManRole_FormUpdate(AuthorizationItem item);

		/// <summary>
		/// Copies an azman role.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>True if the copy worked, false otherwise.</returns>
		bool AzManRole_Copy(AuthorizationItem item);

		/// <summary>
		/// Deletes a AzMan Role item.
		/// </summary>
		/// <param name="item">The item of Role.</param>
		/// <returns></returns>
		bool AzManRole_Delete(AuthorizationItem item);

		/// <summary>
		/// Generates the json structure of the AzMan Role treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="onlyRoles">if set to <c>true</c>return [only roles].</param>
		/// <returns>
		/// a list of treenodes in json.
		/// </returns>
		List<TreeNode> AzManRole_GetTree(string Key, bool onlyRoles);

		/// <summary>
		/// Creates a new AzMan Task item.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="operations">The crud store of operations.</param>
		/// <returns></returns>
		FormResponse AzManTask_FormCreate(AuthorizationItem item, CrudStore<AuthorizationItem> operations);

		/// <summary>
		/// Loads an AzMan Task item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns></returns>
		FormResponse AzManTask_FormLoad(string Key);


		/// <summary>
		/// Updates an existing AzMan Task item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="operations">The crud store of operations.</param>
		/// <returns></returns>
		FormResponse AzManTask_FormUpdate(AuthorizationItem item, CrudStore<AuthorizationItem> operations);

		/// <summary>
		/// Gets a store of all tasks.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="onlyUITasks">if set to <c>true</c> [only UI tasks].</param>
		/// <returns></returns>
		JsonStore<AuthorizationItem> AzManTask_GetStore(PagingParameter paging, bool onlyUITasks);

		/// <summary>
		/// Gets a store of all AzMan Operation for a specific AzMan Task.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAzManTask">The Key of the AzMan Task.</param>
		/// <returns></returns>
		JsonStore<AuthorizationItem> AzManOperation_GetStoreByKeyAzManTask(PagingParameter paging, string KeyAzManTask);

		/// <summary>
		/// Saves a crud store for an AzMan item Task.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<AuthorizationItem> AzManTask_SaveStore(CrudStore<AuthorizationItem> store);

		/// <summary>
		/// Checks user credentials and creates an authentication ticket (cookie) if the credentials are valid.
		/// </summary>
		/// <param name="userName">The user name to be validated.</param>
		/// <param name="password">The password for the specified user.</param>
		/// <param name="createPersistentCookie">A value that indicates whether the authentication ticket remains valid across sessions.</param>
		/// <param name="provider">The provider used to check credentials</param>
		/// <param name="pageId">The page id.</param>
		/// <param name="userNameLoginAs">The user name login as.</param>
		/// <returns>
		/// true if user credentials are valid; otherwise, false.
		/// </returns>
		bool Login(string userName, string password, bool createPersistentCookie, string provider,out string pageId, string userNameLoginAs);


		/// <summary>
		/// Starts an sso login process
		/// </summary>
		/// <param name="providerName">The key.</param>
		/// <returns></returns>
		void SSOLogin(string providerName);

		/// <summary>
		/// Deletes an existing business entity LoginHistory.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool LoginHistory_Delete(LoginHistory item);

		/// <summary>
		/// Gets a list for the business entity LoginHistory. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		List<LoginHistory> LoginHistory_GetAll(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Gets a json store for the business entity LoginHistory.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<LoginHistory> LoginHistory_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Saves a crud store for the business entity LoginHistory.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		JsonStore<LoginHistory> LoginHistory_SaveStore(CrudStore<LoginHistory> store);

		/// <summary>
		/// Gets a json store for the business entity LoginHistory active.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		JsonStore<LoginHistory> LoginHistoryActive_GetStore(PagingParameter paging, PagingLocation location);

		/// <summary>
		/// Checks user credentials and creates an authentication ticket (cookie) if the credentials are valid.
		/// Returns a detail message with the reason for not login the user.
		/// </summary>
		/// <param name="userName">The user name to be validated.</param>
		/// <param name="password">The password for the specified user.</param>
		/// <param name="createPersistentCookie">A value that indicates whether the authentication ticket remains valid across sessions.</param>
		/// <param name="provider">The provider used to check credentials</param>
		/// <param name="userNameLoginAs">The user name login as.</param>
		/// <returns>
		/// true if user credentials are valid; otherwise, false.
		/// </returns>
		FormResponse LoginResponse(string userName, string password, bool createPersistentCookie, string provider, string userNameLoginAs);

		/// <summary>
		/// Deletes from browser the authentication cookie.
		/// </summary>
		void Logout();

		/// <summary>
		/// Gets all the membership providers.
		/// </summary>
		/// <returns>The list of membership providers.</returns>
		List<string> MembershipProvider_GetList();

		/// <summary>
		/// Gets the store of application group for a specific user.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="username">The user name.</param>
		/// <returns></returns>
		JsonStore<ApplicationGroup> User_ApplicationGroup_GetStore(PagingParameter paging, string username);

		/// <summary>
		/// Changes the password for a membership user.
		/// This method works regardless of the password format (clear, encrypted, hashed).
		/// It requires that requiresPasswordAndQuestion is set to false.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="newPassword">The new password.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		bool User_ChangePassword(string username, string newPassword);

		/// <summary>
		/// Changes the password for a membership user through a form with old password and new password.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="oldPassword">The old password.</param>
		/// <param name="newPassword">The new password.</param>
		/// <returns></returns>
		FormResponse User_ChangePasswordForm(string username, string oldPassword, string newPassword);

		/// <summary>
		/// Changes the password format for an existing user.
		/// This is necessaty when modifying web.config because existing password are not updated when modifying passwordFormat.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		bool User_ChangePasswordFormat(string username);

		/// <summary>
		/// Deletes a user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		bool User_Delete(FOLMembershipUser user);

		/// <summary>
		/// Deletes a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		bool User_Delete(string username);

		/// <summary>
		/// Deletes an list of users.
		/// </summary>
		/// <param name="usernames">The list of user names.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		bool User_DeleteList(List<string> usernames);

		/// <summary>
		/// Creates a new user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <returns>
		/// A FormResponse with the status of the operation.
		/// </returns>
		FormResponse User_FormCreate(FOLMembershipUser user);

		/// <summary>
		/// Creates a new user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <param name="password">The password.</param>
		/// <param name="UserMustChangePasswordAtNextLogon">if set to <c>true</c> [user must change password at next logon].</param>
		/// <returns>
		/// A FormResponse with the status of the operation.
		/// </returns>
		FormResponse User_FormCreate(FOLMembershipUser user, string password, bool UserMustChangePasswordAtNextLogon = true);

		/// <summary>
		/// Updates an existing user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <returns>A FormResponse whith the status of the operation.</returns>
		FormResponse User_FormUpdate(FOLMembershipUser user);

		/// <summary>
		/// Updates an existing user.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <param name="password">The password. If null the stored password remains unchanged.</param>
		/// <returns>A FormResponse whith the status of the operation.</returns>
		FormResponse User_FormUpdate(FOLMembershipUser user, string password);

		/// <summary>
		/// Gets a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>A FOLMembershipUser.</returns>
		FOLMembershipUser User_GetByUserName(string username);

		/// <summary>
		/// Gets the current user.
		/// </summary>
		/// <returns>A FOLMembershipUser</returns>
		FOLMembershipUser User_GetCurrent();

		/// <summary>
		/// Gets a user.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns>
		/// A FOLMembershipUser
		/// </returns>
		FOLMembershipUser User_GetItem(string Key, params string[] fields);

		/// <summary>
		/// Gets the roles for user.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <returns></returns>
		string[] User_GetRoleList(string username);

		/// <summary>
		/// Gets a list of Users.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		List<FOLMembershipUser> User_GetAll(PagingParameter paging, PagingLocation location, params string[] fields);

		/// <summary>
		/// Gets the store of users.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns>The store of all users.</returns>
		JsonStore<FOLMembershipUser> User_GetStore(PagingParameter paging, params string[] fields);

		/// <summary>
		/// Gets a store of users filtered by locations and roles.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="locations">The list of KeyLocation</param>
		/// <param name="roles">The list of roles.</param>
		/// <returns>The store of users.</returns>
		JsonStore<FOLMembershipUser> User_GetStoreFiltered(PagingParameter paging, List<string> locations, List<string> roles);


		/// <summary>
		/// Generates the json structure of the User treeview.
		/// </summary>
		/// <returns>a list of treenodes in json.</returns>
		List<TreeNode> User_GetTree();

		/// <summary>
		/// Resets the password for a membership user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="notify">True to send a notificiation, false otherwise.</param>
		void User_ResetPassword(string username, bool notify);

		/// <summary>
		/// Resets the password for a membership user that forgot the password.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="email">The email.</param>
		FormResponse User_ForgotPassword(string username, string email);

		/// <summary>
		/// Saves a list of User.
		/// </summary>
		/// <param name="action">The Crud action. <see cref="CrudAction"/></param>
		/// <param name="records">The list of User to save.</param>
		/// <returns>True if the action was successfull, false otherwise.</returns>
		OperationStatusStore<FOLMembershipUser> User_Save(string action, List<FOLMembershipUser> records);

		/// <summary>
		/// Saves the store of users.
		/// </summary>
		/// <param name="store">The crud store to save.</param>
		/// <returns></returns>
		JsonStore<FOLMembershipUser> User_SaveStore(CrudStore<FOLMembershipUser> store);

		/// <summary>
		/// Updates the UserName of a user.
		/// </summary>
		/// <param name="oldUserName">The old UserName.</param>
		/// <param name="newUserName">The new UserName.</param>
		/// <returns>True if the action was successfull, false otherwise.</returns>
		bool User_ChangeUserName(string oldUserName, string newUserName);

		/// <summary>
		/// Returns the Chart's KPI AzMan Role store.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		JsonStore<AuthorizationItem> Chart_AzManRole_GetStore(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Gets the azman tree starting from the given item.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns>
		/// A list of tree nodes
		/// </returns>
		List<TreeNode> AzManItem_GetAncestorTree(string key);


		/// <summary>
		/// Builds and store in session the filter spatial for a specific user.
		/// </summary>
		/// <param name="userName">The user we need to build the filter for.</param>
		/// <param name="sessionID">The session id.</param>
		void User_SetFilters(string userName, string sessionID);

		/// <summary>
		/// Reset license cache.
		/// </summary>
		void License_ResetCache();

		/// <summary>
		/// Parses the application and username.
		/// </summary>
		/// <param name="loginString">The login string.</param>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="userName">Name of the user.</param>
		void ParseApplicationAndUsername(string loginString, out string applicationName, out string userName);
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.Interfaces {

	/// <summary>
	/// Business layer interface for Graph.
	/// </summary>
	public interface IGraphBusinessLayer {

	

		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <param name="viewName">Name of the view.</param>
		/// <returns></returns>
		DataSet GetView(string viewName);

	}
}

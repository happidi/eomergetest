﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.WCFService {

	///<summary>
	/// Implementation for Authentication Services.
	/// </summary>
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
	public class AuthenticationWCF : BaseModuleWCF, IAuthenticationWCF {
		private readonly IAuthenticationBusinessLayer m_AuthenticationBusinessLayer;

		/// <summary>
		/// Public ctor.
		/// </summary>
		public AuthenticationWCF() {
			// Policy injection on the business layer instance.
			m_AuthenticationBusinessLayer = Helper.CreateInstance<AuthenticationBusinessLayer, IAuthenticationBusinessLayer>();
		}

		/// <summary>
		/// Changes the password for a membership user.
		/// This method works regardless of the password format (clear, encrypted, hashed).
		/// It requires that requiresPasswordAndQuestion is set to false.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="newPassword">The new password.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool User_ChangePassword(string username, string newPassword) {
			return m_AuthenticationBusinessLayer.User_ChangePassword(username, newPassword);
		}

		/// <summary>
		/// Gets a store of auhorization for a specific application groups.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="applicationGroupId">The application group id for which to retreive the authorizations.</param>
		/// <returns></returns>
		public JsonStore<FOLMembershipUser> ApplicationGroup_Authorization_GetStore(PagingParameter paging, string applicationGroupId) {
			return m_AuthenticationBusinessLayer.ApplicationGroup_Authorization_GetStore(paging, applicationGroupId);
		}

		/// <summary>
		/// Gets a store of roles for a specific application group.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="applicationGroupId">The application group id for which to retreive the authorizations.</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> ApplicationGroup_AzManRole_GetStore(PagingParameter paging, string applicationGroupId) {
			return m_AuthenticationBusinessLayer.ApplicationGroup_AzManRole_GetStore(paging, applicationGroupId);
		}

		/// <summary>
		/// Deletes an ApplicationGroup entity.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ApplicationGroup_Delete(ApplicationGroup item) {
			return m_AuthenticationBusinessLayer.ApplicationGroup_Delete(item);
		}

		/// <summary>
		/// Creates a new ApplicationGroup entity.
		/// </summary>
		/// <param name="item">The ApplicationGroup entity to create.</param>
		/// <param name="authorizations">The authorizations.</param>
		/// <param name="roles">The roles.</param>
		/// <returns>A json form response.</returns>
		public FormResponse ApplicationGroup_FormCreate(ApplicationGroup item, CrudStore<FOLMembershipUser> authorizations, CrudStore<AuthorizationItem> roles) {
			return m_AuthenticationBusinessLayer.ApplicationGroup_FormCreate(item, authorizations, roles);
		}

		/// <summary>
		/// Loads a ApplicationGroup entity.
		/// </summary>
		/// <param name="Key">The Key of the ApplicationGroup.</param>
		/// <returns></returns>
		public FormResponse ApplicationGroup_FormLoad(string Key) {
			return m_AuthenticationBusinessLayer.ApplicationGroup_FormLoad(Key);
		}

		/// <summary>
		/// Updates an ApplicationGroup entity.
		/// </summary>
		/// <param name="item">The ApplicationGroup entity to update</param>
		/// <param name="authorizations">The authorizations.</param>
		/// <param name="roles">The roles.</param>
		/// <returns>A json form response.</returns>
		public FormResponse ApplicationGroup_FormUpdate(ApplicationGroup item, CrudStore<FOLMembershipUser> authorizations, CrudStore<AuthorizationItem> roles) {
			return m_AuthenticationBusinessLayer.ApplicationGroup_FormUpdate(item, authorizations, roles);
		}

		/// <summary>
		/// Gets a store of all application group.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<ApplicationGroup> ApplicationGroup_GetStore(PagingParameter paging) {
			return m_AuthenticationBusinessLayer.ApplicationGroup_GetStore(paging);
		}

		/// <summary>
		/// Generates the json structure of the ApplicationGroup treeview.
		/// </summary>
		/// <returns></returns>
		public List<TreeNode> ApplicationGroup_GetTree() {
			return m_AuthenticationBusinessLayer.ApplicationGroup_GetTree();
		}

		/// <summary>
		/// Saves the store of Application Groups.
		/// </summary>
		/// <param name="store">The crud store to save.</param>
		/// <returns></returns>
		public JsonStore<ApplicationGroup> ApplicationGroup_SaveStore(CrudStore<ApplicationGroup> store) {
			return m_AuthenticationBusinessLayer.ApplicationGroup_SaveStore(store);
		}

		/// <summary>
		/// Gets the store of authentication providers declared in vizelia section of web.config.
		/// The returned store is used to build a domain combobox in login form.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> AuthenticationProvider_GetStore() {
			return m_AuthenticationBusinessLayer.AuthenticationProvider_GetStore();
		}

		/// <summary>
		/// Gets a store of all authorizations items of a specific user.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="username">The user name.</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> AuthorizationItem_GetStore(PagingParameter paging, string username) {
			return m_AuthenticationBusinessLayer.AuthorizationItem_GetStore(paging, username);
		}

		/// <summary>
		/// Exports the AzMan configuration as a xml stream file.
		/// </summary>
		/// <returns>The xml stream file.</returns>
		public Stream AzMan_Export() {
			return m_AuthenticationBusinessLayer.AzMan_Export();
		}

		/// <summary>
		/// Exports the AzMan configuration as a xml stream file.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		public void BeginAzManExport(Guid operationId) {
			m_AuthenticationBusinessLayer.BeginAzManExport(operationId);
		}

		/// <summary>
		/// Gets a store of auhorization for a specific application groups.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="filterId">The AzMan filter id for which to retreive the authorizations.</param>
		/// <returns></returns>
		public JsonStore<ApplicationGroup> AzManFilter_Authorization_GetStore(PagingParameter paging, string filterId) {
			return m_AuthenticationBusinessLayer.AzManFilter_Authorization_GetStore(paging, filterId);
		}


		/// <summary>
		/// Gets a store of user authorization for a specific application groups.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="filterId">The AzMan filter id for which to retreive the authorizations.</param>
		/// <returns></returns>
		public JsonStore<FOLMembershipUser> AzManFilter_UserAuthorization_GetStore(PagingParameter paging, string filterId) {
			return m_AuthenticationBusinessLayer.AzManFilter_UserAuthorization_GetStore(paging, filterId);
		}


		/// <summary>
		/// Deletes a AzMan filter item.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool AzManFilter_Delete(AuthorizationItem item) {
			return m_AuthenticationBusinessLayer.AzManFilter_Delete(item);
		}

		/// <summary>
		/// Creates a new AzMan filter item.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="authorizations">The list of application groups.</param>
		/// <param name="users">The list of users.</param>
		/// <returns></returns>
		public FormResponse AzManFilter_FormCreate(AuthorizationItem item, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, CrudStore<ApplicationGroup> authorizations, CrudStore<FOLMembershipUser> users) {
			return m_AuthenticationBusinessLayer.AzManFilter_FormCreate(item, filterSpatial, filterReport, filterChart, filterAlarmDefinition, filterDynamicDisplay, filterPlaylist, filterChartScheduler, filterPortalTemplate, filterPortalWindow, filterClassificationItem, authorizations, users);
		}

		/// <summary>
		/// Loads an AzMan filter item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns></returns>
		public FormResponse AzManFilter_FormLoad(string Key) {
			return m_AuthenticationBusinessLayer.AzManFilter_FormLoad(Key);
		}

		/// <summary>
		/// Loads an AzMan Task item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns></returns>
		public FormResponse AzManTask_FormLoad(string Key) {
			return m_AuthenticationBusinessLayer.AzManTask_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing AzMan filter item.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="filterLink">The filter link item </param>
		/// <param name="authorizations">The list of application groups.</param>
		/// <param name="users">The list of users.</param>
		/// <returns></returns>
		public FormResponse AzManFilter_FormUpdate(AuthorizationItem item, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, List<SecurableEntity> filterLink, CrudStore<ApplicationGroup> authorizations, CrudStore<FOLMembershipUser> users) {
			return m_AuthenticationBusinessLayer.AzManFilter_FormUpdate(item, filterSpatial, filterReport, filterChart, filterAlarmDefinition, filterDynamicDisplay, filterPlaylist, filterChartScheduler, filterPortalTemplate, filterPortalWindow, filterClassificationItem, filterLink, authorizations, users);
		}

		/// <summary>
		/// Generates the json structure of the AzMan Filter treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		public List<TreeNode> AzManFilter_GetTree(string Key) {
			return m_AuthenticationBusinessLayer.AzManFilter_GetTree(Key);
		}

		/// <summary>
		/// Gets the store of filters for an AzMan filter element.
		/// </summary>
		/// <param name="paging">The paging parameter</param>
		/// <param name="filterId">The id of the AzMan filter element.</param>
		/// <param name="typeName">Name of the securable object type .</param>
		/// <returns></returns>
		public JsonStore<SecurableEntity> AzManFilter_Filter_GetStore(PagingParameter paging, string filterId, string typeName) {
			return m_AuthenticationBusinessLayer.AzManFilter_Filter_GetStore(paging, filterId, typeName);
		}


		/// <summary>
		/// Adds members to an AzManItem.
		/// </summary>
		/// <param name="parent">The AzManItem parent.</param>
		/// <param name="children">The list of AzManItem to add as members.</param>
		/// <returns></returns>
		public bool AzManItem_AddMembers(AuthorizationItem parent, List<AuthorizationItem> children) {
			return m_AuthenticationBusinessLayer.AzManItem_AddMembers(parent, children);
		}

		/// <summary>
		/// Removes members from an AzManItem. 
		/// </summary>
		/// <param name="parent">The AzManItem parent.</param>
		/// <param name="children">The list of AzManItem to remove as members.</param>
		/// <returns></returns>
		public bool AzManItem_RemoveMembers(AuthorizationItem parent, List<AuthorizationItem> children) {
			return m_AuthenticationBusinessLayer.AzManItem_RemoveMembers(parent, children);

		}

		/// <summary>
		/// Creates a new AzMan Operation item.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse AzManOperation_FormCreate(AuthorizationItem item) {
			return m_AuthenticationBusinessLayer.AzManOperation_FormCreate(item);
		}

		/// <summary>
		/// Loads a AzMan Operation item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns></returns>
		public FormResponse AzManOperation_FormLoad(string Key) {
			return m_AuthenticationBusinessLayer.AzManOperation_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing AzMan Operation item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public FormResponse AzManOperation_FormUpdate(AuthorizationItem item) {
			return m_AuthenticationBusinessLayer.AzManOperation_FormUpdate(item);
		}

		/// <summary>
		/// Gets a store of all operations.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> AzManOperation_GetStore(PagingParameter paging) {
			return m_AuthenticationBusinessLayer.AzManOperation_GetStore(paging);
		}

		/// <summary>
		/// Saves a crud store for an AzMan item Operation.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AuthorizationItem> AzManOperation_SaveStore(CrudStore<AuthorizationItem> store) {
			return m_AuthenticationBusinessLayer.AzManOperation_SaveStore(store);
		}

		/// <summary>
		/// Creates a new AzMan role item.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="authorizations">The list of application groups.</param>
		/// <returns></returns>
		public FormResponse AzManRole_FormCreate(AuthorizationItem item, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, List<ApplicationGroup> authorizations) {
			return m_AuthenticationBusinessLayer.AzManRole_FormCreate(item, filterSpatial, filterReport, filterChart, filterDynamicDisplay, filterPlaylist, filterChartScheduler, filterPortalTemplate, filterPortalWindow, filterClassificationItem, authorizations);
		}

		/// <summary>
		/// Loads a AzMan Role item.
		/// </summary>
		/// <param name="Key">The key of the item.</param>
		/// <returns></returns>
		public FormResponse AzManRole_FormLoad(string Key) {
			return m_AuthenticationBusinessLayer.AzManRole_FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing AzMan Role item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public FormResponse AzManRole_FormUpdate(AuthorizationItem item) {
			return m_AuthenticationBusinessLayer.AzManRole_FormUpdate(item);
		}

		/// <summary>
		/// Copies an azman role.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>True if the copy worked, false otherwise.</returns>
		public bool AzManRole_Copy(AuthorizationItem item) {
			return m_AuthenticationBusinessLayer.AzManRole_Copy(item);
		}

		/// <summary>
		/// Deletes a AzMan Role item.
		/// </summary>
		/// <param name="item">The item of Role.</param>
		/// <returns></returns>
		public bool AzManRole_Delete(AuthorizationItem item) {
			return m_AuthenticationBusinessLayer.AzManRole_Delete(item);
		}


		/// <summary>
		/// Generates the json structure of the AzMan Role treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="onlyRoles">if set to <c>true</c> return [only roles].</param>
		/// <returns>
		/// a list of treenodes in json.
		/// </returns>
		public List<TreeNode> AzManRole_GetTree(string Key, bool onlyRoles) {
			return m_AuthenticationBusinessLayer.AzManRole_GetTree(Key,onlyRoles);
		}

		/// <summary>
		/// Gets azman filter by keyword
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="keyword"></param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> AzManFilter_GetStoreByKeyword(PagingParameter paging, string keyword) {
			return m_AuthenticationBusinessLayer.AzManFilter_GetStoreByKeyword(paging, keyword);
		}

		/// <summary>
		/// Gets azman filters by location
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="keyLocation"></param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> AzManFilter_GetStoreByLocation(PagingParameter paging, string keyLocation) {
			return m_AuthenticationBusinessLayer.AzManFilter_GetStoreByLocation(paging, keyLocation);
		}

		/// <summary>
		/// Gets the azman tree starting from the given item.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns>A list of tree nodes</returns>
		public List<TreeNode> AzManItem_GetAncestorTree(string Key) {
			return m_AuthenticationBusinessLayer.AzManItem_GetAncestorTree(Key);
		}

		/// <summary>
		/// Creates a new AzMan Task item.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="operations">The crud store of operations.</param>
		/// <returns></returns>
		public FormResponse AzManTask_FormCreate(AuthorizationItem item, CrudStore<AuthorizationItem> operations) {
			return m_AuthenticationBusinessLayer.AzManTask_FormCreate(item, operations);
		}

		/// <summary>
		/// Updates an existing AzMan Task item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="operations">The crud store of operations.</param>
		/// <returns></returns>
		public FormResponse AzManTask_FormUpdate(AuthorizationItem item, CrudStore<AuthorizationItem> operations) {
			return m_AuthenticationBusinessLayer.AzManTask_FormUpdate(item, operations);
		}

		/// <summary>
		/// Gets a store of all tasks.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="onlyUITasks">if set to <c>true</c> [only UI tasks].</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> AzManTask_GetStore(PagingParameter paging, bool onlyUITasks) {
			return m_AuthenticationBusinessLayer.AzManTask_GetStore(paging, onlyUITasks);
		}

		/// <summary>
		/// Gets a store of all AzMan Operation for a specific AzMan Task.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAzManTask">The Key of the AzMan Task.</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> AzManOperation_GetStoreByKeyAzManTask(PagingParameter paging, string KeyAzManTask) {
			return m_AuthenticationBusinessLayer.AzManOperation_GetStoreByKeyAzManTask(paging, KeyAzManTask);
		}

		/// <summary>
		/// Saves a crud store for an AzMan item Task.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AuthorizationItem> AzManTask_SaveStore(CrudStore<AuthorizationItem> store) {
			return m_AuthenticationBusinessLayer.AzManTask_SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity LoginHistory.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool LoginHistory_Delete(LoginHistory item) {
			return m_AuthenticationBusinessLayer.LoginHistory_Delete(item);
		}

		/// <summary>
		/// Gets a list for the business entity LoginHistory. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<LoginHistory> LoginHistory_GetAll(PagingParameter paging, PagingLocation location) {
			return m_AuthenticationBusinessLayer.LoginHistory_GetAll(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity LoginHistory.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<LoginHistory> LoginHistory_GetStore(PagingParameter paging, PagingLocation location) {
			return m_AuthenticationBusinessLayer.LoginHistory_GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity LoginHistory.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<LoginHistory> LoginHistory_SaveStore(CrudStore<LoginHistory> store) {
			return m_AuthenticationBusinessLayer.LoginHistory_SaveStore(store);
		}

		/// <summary>
		/// Gets a json store for the business entity LoginHistory active.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<LoginHistory> LoginHistoryActive_GetStore(PagingParameter paging, PagingLocation location) {
			return m_AuthenticationBusinessLayer.LoginHistoryActive_GetStore(paging, location);
		}

		/// <summary>
		/// Clears the authentication ticket (cookie) in the browser.
		/// </summary>
		public void Logout() {
			m_AuthenticationBusinessLayer.Logout();
		}

		/// <summary>
		/// Gets all the membership providers declared in the membership section of web.config.
		/// </summary>
		/// <returns>The list of membership names</returns>
		public List<string> MembershipProvider_GetList() {
			return m_AuthenticationBusinessLayer.MembershipProvider_GetList();
		}

		/// <summary>
		/// Gets the store of application group for a specific user.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="username">The user name.</param>
		/// <returns></returns>
		public JsonStore<ApplicationGroup> User_ApplicationGroup_GetStore(PagingParameter paging, string username) {
			return m_AuthenticationBusinessLayer.User_ApplicationGroup_GetStore(paging, username);
		}

		/// <summary>
		/// Changes the password for a membership user through a form with old password and new password.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="oldPassword">The old password.</param>
		/// <param name="newPassword">The new password.</param>
		/// <returns></returns>
		public FormResponse User_ChangePasswordForm(string username, string oldPassword, string newPassword) {
			return m_AuthenticationBusinessLayer.User_ChangePasswordForm(username, oldPassword, newPassword);
		}

		/// <summary>
		/// Changes the password format for an existing user.
		/// This is necessaty when modifying web.config because existing password are not updated when modifying passwordFormat.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool User_ChangePasswordFormat(string username) {
			return m_AuthenticationBusinessLayer.User_ChangePasswordFormat(username);
		}

		/// <summary>
		/// Deletes a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool User_Delete(string username) {
			return m_AuthenticationBusinessLayer.User_Delete(username);
		}

		/// <summary>
		/// Deletes an list of users.
		/// </summary>
		/// <param name="usernames">The list of user names.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		public bool User_DeleteList(List<string> usernames) {
			return m_AuthenticationBusinessLayer.User_DeleteList(usernames);
		}

		/// <summary>
		/// Creates a new user.
		/// </summary>
		/// <param name="item">The user.</param>
		/// <param name="password">The password.</param>
		/// <returns>A FormResponse with the status of the operation.</returns>
		public FormResponse User_FormCreate(FOLMembershipUser item, string password) {
			return m_AuthenticationBusinessLayer.User_FormCreate(item, password);
		}

		/// <summary>
		/// Gets a user and returns a FormResponse.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>A json FormResponse.</returns>
		public FormResponse User_FormLoad(string username) {
			return m_AuthenticationBusinessLayer.User_GetByUserName(username).ToFormResponse();
		}

		/// <summary>
		/// Updates an existing user.
		/// </summary>
		/// <param name="item">The user.</param>
		/// <param name="password">The password. If null the stored password remains unchanged.</param>
		/// <returns>A FormResponse whith the status of the operation.</returns>
		public FormResponse User_FormUpdate(FOLMembershipUser item, string password) {
			return m_AuthenticationBusinessLayer.User_FormUpdate(item, password);
		}

	    /// <summary>
	    /// Updates the current logged in user time zone.
	    /// </summary>
	    /// <param name="TimeZoneId">The time zone identifier.</param>
	    /// <returns></returns>
	    public FormResponse User_UpdateCurrentUserTimeZone(string TimeZoneId) {
		    throw new NotImplementedException("This method was removed in version 2.9 as part of introducing the UserPreferences entity");
	    }

		/// <summary>
		/// Gets a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>A FOLMembershipUser.</returns>
		public FOLMembershipUser User_GetByUserName(string username) {
			return m_AuthenticationBusinessLayer.User_GetByUserName(username);
		}

		/// <summary>
		/// Gets the current user.
		/// </summary>
		/// <returns>A FOLMembershipUser.</returns>
		public FOLMembershipUser User_GetCurrent() {
			return m_AuthenticationBusinessLayer.User_GetCurrent();
		}

		/// <summary>
		/// Gets the roles for user.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <returns></returns>
		public string[] User_GetRoleList(string username) {
			return m_AuthenticationBusinessLayer.User_GetRoleList(username);
		}

		/// <summary>
		/// Gets the store of users.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns>The store of all users.</returns>
		public JsonStore<FOLMembershipUser> User_GetStore(PagingParameter paging, params string[] fields) {
			return m_AuthenticationBusinessLayer.User_GetStore(paging, fields);
		}


		/// <summary>
		/// Gets a store of users filtered by locations and roles.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="locations">The list of KeyLocation</param>
		/// <param name="roles">The list of roles.</param>
		/// <returns>The store of users.</returns>
		public JsonStore<FOLMembershipUser> User_GetStoreFiltered(PagingParameter paging, List<string> locations, List<string> roles) {
			return m_AuthenticationBusinessLayer.User_GetStoreFiltered(paging, locations, roles);
		}


		/// <summary>
		/// Generates the json structure of the User treeview.
		/// </summary>
		/// <returns>a list of treenodes in json.</returns>
		public List<TreeNode> User_GetTree() {
			return m_AuthenticationBusinessLayer.User_GetTree();
		}

		/// <summary>
		/// Resets the password of an user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="notify">True if a mail should be sent, false otherwise.</param>
		public void User_ResetPassword(string username, bool notify) {
			m_AuthenticationBusinessLayer.User_ResetPassword(username, notify);
		}

		/// <summary>
		/// Saves a list of User.
		/// </summary>
		/// <param name="action">The Crud action. <see cref="CrudAction"/></param>
		/// <param name="records">The list of User to save.</param>
		/// <returns>True if the action was successfull, false otherwise.</returns>
		public OperationStatusStore<FOLMembershipUser> User_Save(string action, List<FOLMembershipUser> records) {
			return m_AuthenticationBusinessLayer.User_Save(action, records);
		}

		/// <summary>
		/// Saves the store of users.
		/// </summary>
		/// <param name="store">The crud store to save.</param>
		/// <returns></returns>
		public JsonStore<FOLMembershipUser> User_SaveStore(CrudStore<FOLMembershipUser> store) {
			return m_AuthenticationBusinessLayer.User_SaveStore(store);
		}


		/// <summary>
		/// Updates the UserName of a user.
		/// </summary>
		/// <param name="oldUserName">The old UserName.</param>
		/// <param name="newUserName">The new UserName.</param>
		/// <returns>True if the action was successfull, false otherwise.</returns>
		public bool User_ChangeUserName(string oldUserName, string newUserName) {
			return m_AuthenticationBusinessLayer.User_ChangeUserName(oldUserName, newUserName);
		}

		/// <summary>
		/// Returns the Chart's KPI AzMan Role store.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public JsonStore<AuthorizationItem> Chart_AzManRole_GetStore(PagingParameter paging, string KeyChart) {
			return m_AuthenticationBusinessLayer.Chart_AzManRole_GetStore(paging, KeyChart);
		}

		/// <summary>
		/// Reset license cache.
		/// </summary>
		public void License_ResetCache() {
			m_AuthenticationBusinessLayer.License_ResetCache();
		}

		/// <summary>
		/// Login_s the login as user.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="password">The password.</param>
		/// <param name="createPersistentCookie">if set to <c>true</c> [create persistent cookie].</param>
		/// <param name="provider">The provider.</param>
		/// <param name="userNameLoginAs">The user name login as.</param>
		/// <returns></returns>
		public FormResponse Login_LoginAsUser(string userName, string password, bool createPersistentCookie, string provider, string userNameLoginAs) {
			return m_AuthenticationBusinessLayer.LoginResponse(userName, password, createPersistentCookie, provider, userNameLoginAs);
		}
	}
}

﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using System.Web.Security;

namespace Vizelia.FOL.Web {
	public partial class PrintScreen : System.Web.UI.Page {
		protected string PageId { get; set; }
		protected string FormsValue { get; set; }

		protected void Page_Load(object sender, EventArgs e) {
			var culture = HttpContext.Current.Request["keyLocalisationCulture"];
			if (!string.IsNullOrEmpty(culture)) {
				UICulture = culture;
				Culture = culture;
			}

			var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
			string encryptedValue = authCookie.Value;

			var ticket = FormsAuthentication.Decrypt(encryptedValue);
			if (!ticket.Expired) {
				var cookieData = new CookieData(ticket.UserData);
				ContextHelper.ApplicationName = cookieData.ApplicationName;
				
				var impersonateUserName = HttpContext.Current.Request["username"];
				string pageid;
				SessionService.SetAuthCookie(impersonateUserName, false, out pageid);
				PageId = pageid;
				FormsValue = encryptedValue;
			}
		}

		protected static string GetLocalizedFile(string resourcePath)
        {
            var culture = CultureInfo.CurrentUICulture;
            string fullCulture = culture.ToString().Replace("-", "_");
            string shortCulture = culture.TwoLetterISOLanguageName;
            string[] resourceSplit = resourcePath.Split('.');
            string localizedResourceName = String.Join(".", resourceSplit.Take(resourceSplit.Length - 1).ToArray()) + "-" + "{0}" +
                                           "." + resourceSplit[resourceSplit.Length - 1];

            if (File.Exists(HttpContext.Current.Server.MapPath(string.Format(localizedResourceName, shortCulture))))
                return string.Format(localizedResourceName, shortCulture);

            if (File.Exists(HttpContext.Current.Server.MapPath(string.Format(localizedResourceName, fullCulture))))
                return string.Format(localizedResourceName, fullCulture);

            //If we don't have the localized file for the client culture falling back to English. 
            // Note that we might do have the file but its name doesn't fit to the two or full culture convetion of .Net
            return string.Format(localizedResourceName, "en");
        }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using System.Web.Security;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.WCFService;

namespace Vizelia.FOL.Web {
	public partial class SSO : System.Web.UI.Page {

		protected void Page_Load(object sender, EventArgs e) {
			PublicWCF service = new PublicWCF();
			service.SSOLogin(HttpContext.Current.Request["Provider"] ?? string.Empty);

		}
	}
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintScreen.aspx.cs" Inherits="Vizelia.FOL.Web.PrintScreen" %>

<%@ Import Namespace="Vizelia.FOL.Common.Localization" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <style type="text/css">
    </style>
</head>
<body dir="<%=Langue.langue_direction%>">
    <form id="form1" runat="server" class="x-hidden">
        <viz:ScriptManager runat="server" ID="ScriptManager" ScriptMode="Auto" EnablePartialRendering="false" LoadScriptsBeforeUI="False" EnableCdn="False" EnableViewState="False">
        </viz:ScriptManager>
    </form>

    <link href="ExtJS/resources/css/ext-sandbox-all.css" rel="stylesheet" />
    <link href="ExtJS/resources/css/ext-all.css" rel="stylesheet" />
    <link href="ExtJS/resources/css/xtheme-gray.css" rel="stylesheet" />
    <link href="resources/css/viz-desktop.css" rel="stylesheet" />
    <link href="resources/css/viz-ext-all.css" rel="stylesheet" />
    <link href="ExtJS/examples/ux/css/ux-all.css" rel="stylesheet" />
    <link href="resources/css/viz-theme-gray.css" rel="stylesheet" />


    <%-- Adding jQuery and jQuery UI from Google's CDN, with fallback to our local jquery file--%>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript">
        if (typeof jQuery == 'undefined') {
            document.write(unescape("%3Cscript src='js/plugins/jquery-1.11.0.min.js' type='text/javascript'%3E%3C/script%3E"));
        }
    </script>

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript">
        if (typeof jQuery.ui == 'undefined') {
            document.write(unescape("%3Cscript src='js/plugins/jquery-ui-1.10.4.min.js' type='text/javascript'%3E%3C/script%3E"));
        }
    </script>


    <script src="ExtJS/adapter/ext/ext-base.js" type="text/javascript"></script>
    <script src="ExtJS/ext-all.js" type="text/javascript"></script>
    <script src="ExtJS/examples/ux/ux-all.js" type="text/javascript"></script>
    <script src="ExtJS/ext-all-sandbox.js" type="text/javascript"></script>

    <% if (Langue.langue_direction == "rtl") { %>
    <script src="ExtJS/rtl/rtl.js" type="text/javascript"></script>
    <link href="ExtJS/rtl/resources/css/extjs-rtl.css" rel="stylesheet" />
    <% } %>

    <!-- custom.css is the clients place to override vizelia css rules. It has after all other css files so it won't be overriden -->
    <link href="css/custom.css" rel="stylesheet" />

    <script src="js/Vizelia/viz-ext-all.js" type="text/javascript"></script>

    <% if (Langue.langue_direction == "rtl") { %>
    <script src="js/Vizelia/source/rtl/rtl.js" type="text/javascript"></script>
    <% } %>

    <script src="<%: GetLocalizedFile("ExtJS/src/locale/ext-lang.js")%>" type="text/javascript"></script>
    <script src="<%: GetLocalizedFile("js/plugins/extensible/extensible-lang.js")%>" type="text/javascript"></script>
    <script src="js/plugins/dotnetcharting_highcharts.js" type="text/javascript"></script>
    <script src="js/printscreen.js"></script>
    <script type="text/javascript">
        function onGetApplicationSettingsSuccess() {
            Ext.Ajax.on("beforerequest", function (con, options) {
                Ext.apply(options.headers, {
                    "<%=Vizelia.FOL.Providers.SessionService.const_page_id%>": '<%=this.PageId%>',
                    "<%=Vizelia.FOL.Providers.SessionService.const_internal_request_header_name %>": '<%=this.FormsValue %>'
                });
            });

            //We need to set up the state provider (useful for custom grid for example);
            Ext.state.Manager.setProvider(new Ext.state.RESTfulProvider({
                serviceHandlerRead: Viz.Services.CoreWCF.UserProfilePreference_GetListImpersonateUser,
                expires: new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 120))
            }));


            Viz.Configuration.ClassificationItemDefinition = new Viz.store.ClassificationItemDefinition();
            Viz.Configuration.ClassificationItemDefinition.on("load", function () {
                Viz.Configuration.Columns.init();


                Viz.Localization.Store = new Viz.store.Localization();
                Viz.Localization.Store.on("load", function () {
                    if ('<%=HttpContext.Current.Request["xtype"] %>' === 'vizPortalTab') {
                        var KeyPortalTab = '<%=HttpContext.Current.Request["keyPropertyValue"]%>';
                        displayPortalTab(KeyPortalTab);
                    };
                }, Viz.App, { single: true });
                Viz.Localization.Store.load();

            }, this, {
                single: true
            });
            Viz.Configuration.ClassificationItemDefinition.load();
        }

        function initPrintScreen() {

            Viz.Services.PublicWCF.GetApplicationSettings({
                success: function (value) {
                    Viz.ApplicationSettings = value;
                    onGetApplicationSettingsSuccess();
                }
            });
        }
    </script>
</body>
</html>

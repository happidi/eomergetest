﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.ServiceModel.Security;
using System.Web;
using Microsoft.Reporting.WebForms;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using Report = Vizelia.FOL.BusinessEntities.Report;
using ReportParameter = Microsoft.Reporting.WebForms.ReportParameter;

namespace Vizelia.FOL.Web.Reports {
	// ASPX Page code-behind implementation after this class, which is used by the ReportViewer control in this page.

	/// <summary>
	/// A Vizelia Custom Reporting Services Connection String Provider. 
	/// Used since there session state is disallowed in the configuration file.
	/// Returns the URL of the Report Server and the user running the IIS AppPool, as well as the report timeout.
	/// </summary>
	public class ReportingServicesConnectionStringProvider : IReportServerConnection2 {
		/// <summary>
		/// Gets or sets the <see cref="T:System.Security.Principal.WindowsIdentity"/> of the user to impersonate when the <see cref="T:Microsoft.Reporting.WebForms.ReportViewer"/> control connects to a report server.
		/// </summary>
		/// <returns>
		/// A <see cref="T:System.Security.Principal.WindowsIdentity"/> object that represents the user to impersonate.
		/// </returns>
		public WindowsIdentity ImpersonationUser {
			get {
				// Return AppPool identity. It should be defined in the Reporting Services 
				// interface as having security access.
				WindowsIdentity identity = WindowsIdentity.GetCurrent();
				return identity;
			}
		}

		#region Connection-string related properties
		/// <summary>
		/// Returns the URL of the report server.
		/// </summary>
		/// <returns>
		/// A Uri object containing the URL of the report server.
		/// </returns>
		public Uri ReportServerUrl {
			get { return ReportingService.ReportServerUrl; }
		}

		/// <summary>
		/// Gets the number of milliseconds to wait for server communications.
		/// </summary>
		/// <returns>
		/// An integer value containing the number of milliseconds to wait for server communications.
		/// </returns>
		public int Timeout {
			get { return ReportingService.Timeout; }
		}
		#endregion

		#region Unsupported authentication methods
		/// <summary>
		/// Provides information that will be used to connect to the report server that is configured for forms authentication.
		/// </summary>
		/// <returns>
		/// true if forms authentication should be used; otherwise, false. 
		/// </returns>
		/// <param name="authCookie">[out] A report server authentication cookie.</param><param name="userName">[out] The name of the user.</param><param name="password">[out] The password of the user.</param><param name="authority">[out] The authority to use when authenticating the user, such as a Microsoft Windows domain. </param>
		public bool GetFormsCredentials(out Cookie authCookie, out string userName, out string password, out string authority) {
			authCookie = null;
			userName = null;
			password = null;
			authority = null;
			return false;
		}


		/// <summary>
		/// Gets or sets the network credentials that are used for authentication with the report server.
		/// </summary>
		/// <returns>
		/// An implementation of <see cref="T:System.Net.ICredentials"/> that contains the credential information for connecting to a report server.
		/// </returns>
		public ICredentials NetworkCredentials {
			get { return null; }
		}

		/// <summary>
		/// Gets a collection of custom cookies to send to the report server.
		/// </summary>
		/// <returns>
		/// A collection of <see cref="T:System.Net.Cookie"/> objects that contain the custom cookies.
		/// </returns>
		public IEnumerable<Cookie> Cookies {
			get { return null; }
		}

		/// <summary>
		/// Gets a collection of custom headers to send to the report server.
		/// </summary>
		/// <returns>
		/// A collection of string objects that contain the custom header values.
		/// </returns>
		public IEnumerable<string> Headers {
			get { return null; }
		}

		#endregion
	}

	public partial class ReportingServices : System.Web.UI.Page {

		protected void Page_Load(object sender, EventArgs e) {
			if(!HttpContext.Current.User.Identity.IsAuthenticated) {
				throw new SecurityAccessDeniedException("Access denied for unauthenticated user");
			}

			string requestedReportPath = Request.QueryString["reportpath"];

			if (!requestedReportPath.StartsWith("/")) {
				throw new VizeliaException("reportpath must begin with /");
			}

			Response.AddHeader("PRAGMA", "NO-CACHE");
			Response.ExpiresAbsolute = DateTime.Now.AddMinutes(-1);
			Response.Expires = 0;
			Response.CacheControl = "no-cache";

			// For shared multi-tenancy environment.
			CookieData cookieData;
			SessionService.SetApplicationName(out cookieData);

			ValidateReportIsAllowed(requestedReportPath);

			if (!IsPostBack) {
				// We seperate reports for tenants by a root folder with the tenant name.
				string reportPath = "/" + ContextHelper.ApplicationName + requestedReportPath;
				ReportViewer1.ServerReport.ReportPath = reportPath;
				

				// Ignore form fields that are not report parameters
				ReportParameterInfoCollection parametersMetadata = ReportViewer1.ServerReport.GetParameters();

				var parameters = new List<ReportParameter>();

				foreach (string parameterName in Request.Form.AllKeys) {
					ReportParameterInfo parameterMetadata = parametersMetadata.FirstOrDefault(p => p.Name.Equals(parameterName));

					if (parameterMetadata == null) {
						continue;
					}

					string parameterValue = Request.Form[parameterName];
					if (parameterMetadata.DataType == ParameterDataType.DateTime) {
						// Format the string of the DateTime for SQL Server.
						DateTime dateTime = DateTime.Parse(parameterValue);
						parameterValue = dateTime.ToString("yyyy'-'MM'-'dd HH':'mm':'ss");
					}

					var parameter = new ReportParameter(parameterName, parameterValue);
					parameters.Add(parameter);
				}

				// Inject Username into parameters if a hidden parameter for it exists.
				var usernameParameter = parametersMetadata.FirstOrDefault(p => p.Name.ToLower().Equals("username") && p.Prompt.Equals(string.Empty));

				if(usernameParameter != null) {
					parameters.Add(new ReportParameter(usernameParameter.Name, Helper.GetCurrentUserName()));
				}

				// Inject User ID into parameters if a hidden parameter for it exists.
				var useridParameter = parametersMetadata.FirstOrDefault(p => p.Name.ToLower().Equals("userid") && p.Prompt.Equals(string.Empty));

				if (useridParameter != null) {
					parameters.Add(new ReportParameter(useridParameter.Name, Helper.GetCurrentActor()));
				}

				// Make sure we didn't miss a VISIBLE parameter or else there would be no output rendered...
				string[] missingParameterNames = parametersMetadata
													.Where(m => m.PromptUser && !string.IsNullOrEmpty(m.Prompt))
													.Select(m => m.Name)
													.Except(parameters.Select(p => p.Name)).ToArray();

				if (missingParameterNames.Length > 0) {
					throw new VizeliaException("These parameters are missing from the POST: " + string.Join(",", missingParameterNames) + ". Please make sure that the parameter name matches the field's getName function output.");
				}


				// Validate the parameter values

				//foreach (ReportParameter incomingParameter in parameters) {
				//    ReportParameterInfo parameterMetadata = reportParameters.First(r => r.Name.Equals(incomingParameter.Name));
				//    // If valid values exist for this field, make sure the incoming values for the field are valid.
				//    if(parameterMetadata.ValidValues.Any() && incomingParameter.Values.OfType<string>().Except(parameterMetadata.ValidValues.Select(v => v.Value)).Any()) {
				//        throw new VizeliaException("Invalid value in field " + incomingParameter.Name);
				//    }
				//}

				ReportViewer1.ServerReport.SetParameters(parameters);
			}
		}

		/// <summary>
		/// Validates that the report is allowed for the current user.
		/// </summary>
		/// <param name="requestedReportPath">The requested report path.</param>
		private static void ValidateReportIsAllowed(string requestedReportPath) {
			// Dummy object we use in order to call the report helper.
			var fakeReport = new Report { Path = requestedReportPath, TypeName = "Report" };

			bool isAllowed = ReportHelper.IsAllowed(fakeReport);

			if (!isAllowed) {
				throw new VizeliaException("Unauthorized to access report.");
			}
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Web
{
    public partial class _Default : System.Web.UI.Page
    {
        private const string const_login_footer_resource_name = "msg_login_footer";
        private const string const_copyright_title_resource_name = "msg_copyright_title";
        private const string const_copyright_smalltext_resource_name = "msg_copyright_smalltext";
        private ICoreBusinessLayer m_CoreBusinessLayer;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                DefaultHead.Title = Langue.msg_application_title;
                DefaultBody.Attributes["dir"] = Langue.langue_direction;
                WaitMessage.InnerText = Langue.msg_wait;
                ApplicationTitle.InnerText = Langue.msg_application_title;
                AuthUsername.Attributes["autocomplete"] = AuthPassword.Attributes["autocomplete"] =
                VizeliaConfiguration.Instance.Authentication.AllowAutoCompleteOnLogin ? "on" : "off";

                SetPageTexts();
            }
            catch (Exception exception)
            {
                // On errors, we do nothing. The home page MUST display.
                try
                {
                    TracingService.Write(exception);
                }
                catch (Exception)
                {
                }
            }

        }

        private void SetPageTexts()
        {
            m_CoreBusinessLayer = Helper.CreateInstance<CoreBusinessLayer, ICoreBusinessLayer>();
            var preLoginLocalizeText = m_CoreBusinessLayer.PreLoginLocalizeText(
                new List<string> {const_login_footer_resource_name, 
								  const_copyright_title_resource_name, 
								  const_copyright_smalltext_resource_name},
                false);

            LoginFooter.InnerHtml = GetTextValue(preLoginLocalizeText, const_login_footer_resource_name);
            CopyrightTitle.InnerHtml = GetTextValue(preLoginLocalizeText, const_copyright_title_resource_name);
            CopyrightSmallText.InnerHtml = GetTextValue(preLoginLocalizeText, const_copyright_smalltext_resource_name);
        }

        private static string GetTextValue(IDictionary<string, string> preLoginLocalizeText, string resourceName)
        {
            var textValue = preLoginLocalizeText[resourceName];
            if (textValue.Equals(resourceName))
            {
                textValue = string.Empty;
            }
            return textValue;
        }

        protected static string GetLocalizedFile(string resourcePath)
        {
            var culture = CultureInfo.CurrentUICulture;
            string fullCulture = culture.ToString().Replace("-", "_");
            string shortCulture = culture.TwoLetterISOLanguageName;
            string[] resourceSplit = resourcePath.Split('.');
            string localizedResourceName = String.Join(".", resourceSplit.Take(resourceSplit.Length - 1).ToArray()) + "-" + "{0}" +
                                           "." + resourceSplit[resourceSplit.Length - 1];

            if (File.Exists(HttpContext.Current.Server.MapPath(string.Format(localizedResourceName, shortCulture))))
                return string.Format(localizedResourceName, shortCulture);

            if (File.Exists(HttpContext.Current.Server.MapPath(string.Format(localizedResourceName, fullCulture))))
                return string.Format(localizedResourceName, fullCulture);

            // If we don't have the localized file for the client culture falling back to English. 
            // Note that we might do have the file but its name doesn't fit to the two or full culture convetion of .Net
            return string.Format(localizedResourceName, "en");
        }
    }
}

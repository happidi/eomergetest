﻿using System;
using System.Web.Http;


namespace Vizelia.FOL.Web {
	public static class WebApiConfig {
		public static void Register(HttpConfiguration config) {
			Vizelia.FOL.OData.WebApiConfig.Register(config);
		}

	}
}

﻿using System;
using System.Web;
using System.Web.Routing;
using System.Web.Http;
using System.Web.SessionState;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Web {
	/// <summary>
	/// Global asa class.
	/// </summary>
	public class Global : HttpApplication {

		private readonly IMappingBusinessLayer m_MappingBusinessLayer = Helper.CreateInstance<MappingBusinessLayer, IMappingBusinessLayer>();

		/// <summary>
		/// Handles the BeginRequest event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Application_BeginRequest(object sender, EventArgs e) {
			if (!HttpContext.Current.Request.Url.LocalPath.ToLower().EndsWith("reportingservices.aspx")) {
				HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.ReadOnly);
			}
			SessionService.PrepareMustRenewSession();
		}

		/// <summary>
		/// Handles the PostAuthenticateRequest event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Application_PostAuthenticateRequest(object sender, EventArgs e) {
			if (HttpContext.Current.User.Identity.IsAuthenticated) {
				SessionService.CreateOrSlideSession(SessionService.GetSessionID(), HttpContext.Current.User.Identity.Name);
			}
		}


		/// <summary>
		/// Handles the PreSendRequestHeaders event of the Application control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Application_PreSendRequestHeaders(object sender, EventArgs e) {
			if (HttpContext.Current != null)
				HttpContext.Current.Response.Headers.Remove("Server");
		}

		//protected void Application_PreRequestHandlerExecute(object sender, EventArgs e) {

		//    string PathAndQuery = HttpContext.Current.Request.Url.PathAndQuery.ToLower();
		//    if (PathAndQuery.EndsWith("/jsdebug") || PathAndQuery.EndsWith("/js")) {
		//        WCFAjaxJavascriptHelper.GenerateAjaxJavascript();
		//    }

		//}
		protected void Application_Start(object sender, EventArgs e) {
			//We need to force the localization provider to init, so we use the VizeliaResourceManager
			LocalizationService.Init();
			// Clean temp mapping folder from files that got stacked in it duo to mapping jobs interuption
			m_MappingBusinessLayer.CleanMappingTempFolder();

			WebApiConfig.Register(GlobalConfiguration.Configuration);
		}
		#region inactive code
		/*
		protected void Session_End(object sender, EventArgs e) {

		}
		
		  
		

		protected void Application_AcquireRequestState(object sender, EventArgs e) {

		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e) {

		}

		protected void Application_Error(object sender, EventArgs e) {

		}

		protected void Session_End(object sender, EventArgs e) {

		}

		protected void Application_End(object sender, EventArgs e) {

		} 
		*/
		#endregion

	}
}
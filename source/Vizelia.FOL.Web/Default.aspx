<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Default.aspx.cs" Inherits="Vizelia.FOL.Web._Default" EnableSessionState="false" %>

<%@ Import Namespace="Vizelia.FOL.Common.Localization" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="DefaultHead" runat="server">
    <title>Energy Operation</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
    <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
</head>
<body id="DefaultBody" runat="server" style="overflow: hidden">
    <div id="loading-msg" style="width: 886px; height: 619px; text-align: center; position: absolute; top: 50%; left: 50%; margin-left: -443px; margin-top: -325px; background-color: Transparent; background: url(images/splashscreen-v2.gif); font-family: Arial, 'Segoe UI', tahoma, helvetica, sans-serif !important;">
        <div id="TextDiv" style="visibility: hidden">
            <div id="WaitMessage" runat="server" style="position: relative; top: 510px; left: 45px; text-align: left; color: #333333; margin-top: 5px; font-size: 13pt !important;">
            </div>
            <div id="ApplicationTitle" runat="server" style="position: relative; top: 100px; left: 170px; color: black; margin-top: 5px; font-size: 36pt !important;">
            </div>
            <div id="ApplicationTexts">
                <div id="CopyrightTitle" runat="server" style="position: relative; left: 44px; text-align: left; top: 474px; color: gray; font-size: 12px">
                </div>
                <div id="CopyrightSmallText" runat="server" style="position: relative; left: 44px; text-align: left; top: 474px; color: gray; font-size: 11px">
                </div>
                <div id="LoginFooter" runat="server" style="position: relative; top: 500px; left: 0px; height: 100px; width: 886px" class="loginfooter"></div>
            </div>
        </div>
    </div>
    <form id="form1" runat="server" class="x-hidden">
        <viz:ScriptManager runat="server" ID="ScriptManager" ScriptMode="Auto" EnablePartialRendering="false" LoadScriptsBeforeUI="False" EnableCdn="False" EnableViewState="False">
        </viz:ScriptManager>
    </form>

    <link href="ExtJS/resources/css/ext-sandbox-all.css" rel="stylesheet" />
    <link href="ExtJS/resources/css/ext-all.css" rel="stylesheet" />
    <link href="ExtJS/resources/css/xtheme-gray.css" rel="stylesheet" />
    <link href="resources/css/viz-desktop.css" rel="stylesheet" />
    <link href="ExtJS/examples/ux/css/ux-all.css" rel="stylesheet" />
    <link href="resources/css/viz-ext-all.css" rel="stylesheet" />
    <link href="resources/css/viz-theme-gray.css" rel="stylesheet" />


    <%-- Adding jQuery and jQuery UI from Google's CDN, with fallback to our local jquery file--%>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript">
        if (typeof jQuery == 'undefined') {
            document.write(unescape("%3Cscript src='js/plugins/jquery-1.11.0.min.js' type='text/javascript'%3E%3C/script%3E"));
        }
    </script>

    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript">
        if (typeof jQuery.ui == 'undefined') {
            document.write(unescape("%3Cscript src='js/plugins/jquery-ui-1.10.4.min.js' type='text/javascript'%3E%3C/script%3E"));
        }
    </script>

    <% #if DEBUG  %>
    <script src="ExtJS/adapter/ext/ext-base-debug.js" type="text/javascript"></script>
    <script src="ExtJS/ext-all-debug.js" type="text/javascript"></script>
    <script src="ExtJS/examples/ux/ux-all-debug-3.4.0.js" type="text/javascript"></script>
    <script src="ExtJS/ext-all-sandbox-debug.js" type="text/javascript"></script>
    <% #else %>
    <script src="ExtJS/adapter/ext/ext-base.js" type="text/javascript"></script>
    <script src="ExtJS/ext-all.js" type="text/javascript"></script>
    <script src="ExtJS/examples/ux/ux-all-3.4.0.js" type="text/javascript"></script>
    <script src="ExtJS/ext-all-sandbox.js" type="text/javascript"></script>
    <% #endif %>

    <% if (Langue.langue_direction == "rtl") { %>
    <script src="ExtJS/rtl/rtl.js" type="text/javascript"></script>
    <link href="ExtJS/rtl/resources/css/extjs-rtl.css" rel="stylesheet" />

    <% } %>
    <!-- custom.css is the clients place to override vizelia css rules. It has after all other css files so it won't be overriden -->
    <link href="css/custom.css" rel="stylesheet" />

    <%-- Those two files are being generated with the jsBuilder in the Web project pre build event.--%>
    <%-- We are not including them in the source control se we get VS errors they don't exist. They do exist.--%>
    <% #if DEBUG  %>
    <script src="js/Vizelia/viz-ext-all-debug.js" type="text/javascript"></script>
    <% #else %>
    <script src="js/Vizelia/viz-ext-all.js" type="text/javascript"></script>
    <% #endif %>

    <% if (Langue.langue_direction == "rtl") { %>
    <script src="js/Vizelia/source/rtl/rtl.js" type="text/javascript"></script>

    <% } %>

    <script src="<%: GetLocalizedFile("ExtJS/src/locale/ext-lang.js")%>" type="text/javascript"></script>
    <script src="<%: GetLocalizedFile("js/plugins/extensible/extensible-lang.js")%>" type="text/javascript"></script>
    <script src="js/plugins/dotnetcharting_highcharts.js" type="text/javascript"></script>
    <script src="js/Vizelia/default.js" type="text/javascript"></script>

    <form class="x-hidden">
        <input id="AuthUsername" runat="server" type="text" name="auth-username" class="x-hidden" />
        <input id="AuthPassword" runat="server" type="password" name="auth-password" class="x-hidden" />
        <input id="auth-submit" type="submit" />
    </form>
</body>
</html>

﻿/*
 * (c)2011 Corporate Web Solutions Ltd. / WebAvail Productions Inc. JavaScript charts for use under license with .netCHARTING. This client side code, or portions thereof, may only be used in conjunction with a valid .netCHARTING license. Portions (c)2011 Torstein Honsi: Highcharts. For additional licensing details please see http://www.dotnetcharting.com/licensing.aspx or consult the .netCHARTING licensing agreement.
 */
(function() {
    function A(a, c) {
        a || (a = {});
        for (var b in c)
            a[b] = c[b];
        return a
    }
    function x(a, c) {
        return parseInt(a, c || 10)
    }
    function Wa(a) {
        return typeof a == "string"
    }
    function X(a) {
        return typeof a == "object"
    }
    function S(a) {
        return typeof a == "number"
    }
    function Za(a, c) {
        for (var b = a.length; b--;)
            if (a[b] == c) {
                a.splice(b, 1);
                break
            }
    }
    function u(a) {
        return a !== ua && a !== null
    }
    function I(a, c, b) {
        var d, f;
        if (Wa(c))
            u(b) ? a.setAttribute(c, b) : a && a.getAttribute && (f = a.getAttribute(c));
        else if (u(c) && X(c))
            for (d in c)
                a.setAttribute(d, c[d]);
        return f
    }
    function Na(a) {
        if (!a || a.constructor != Array)
            a = [a];
        return a
    }
    function p() {
        var a = arguments, c, b, d = a.length;
        for (c = 0; c < d; c++)
            if (b = a[c], typeof b !== "undefined" && b !== null)
                return b
    }
    function Ja(a) {
        var c = "", b;
        for (b in a)
            c += b + ":" + a[b] + ";";
        return c
    }
    function D(a, c) {
        if (Mb && c && c.opacity !== ua)
            c.filter = "alpha(opacity=" + c.opacity * 100 + ")";
        A(a.style, c)
    }
    function Q(a, c, b, d, f) {
        a = U.createElement(a);
        c && A(a, c);
        f && D(a, {
                    padding : 0,
                    border  : Ka,
                    margin  : 0
                });
        b && D(a, b);
        d && d.appendChild(a);
        return a
    }
    function $(a, c) {
        var b = function() {
        };
        b.prototype = new a;
        A(b.prototype, c);
        return b
    }
    function l(a, c, b, d) {
        var f = va.lang, e = isNaN(c = ha(c)) ? 2 : c, c = b === void 0 ? f.decimalPoint : b, d = d === void 0 ? f.thousandsSep : d, f = a < 0 ? "-" : "", b = x(a = ha(+a || 0).toFixed(e)) + "", g = (g = b.length) > 3 ? g % 3 : 0;
        return f + (g ? b.substr(0, g) + d : "") + b.substr(g).replace(/(\d{3})(?=\d)/g, "$1" + d) + (e ? c + ha(a - b).toFixed(e).slice(2) : "")
    }
    function j(a, c) {
        Nb = p(a, c.animation)
    }
    function h() {
        var a = va.global.useUTC;
        Ob = a ? Date.UTC : function(a, b, d, f, e, g) {
            return (new Date(a, b, p(d, 1), p(f, 0), p(e, 0), p(g, 0))).getTime()
        };
        $b = a ? "getUTCMinutes" : "getMinutes";
        ac = a ? "getUTCHours" : "getHours";
        bc = a ? "getUTCDay" : "getDay";
        Db = a ? "getUTCDate" : "getDate";
        Pb = a ? "getUTCMonth" : "getMonth";
        Qb = a ? "getUTCFullYear" : "getFullYear";
        nc = a ? "setUTCMinutes" : "setMinutes";
        oc = a ? "setUTCHours" : "setHours";
        cc = a ? "setUTCDate" : "setDate";
        pc = a ? "setUTCMonth" : "setMonth";
        qc = a ? "setUTCFullYear" : "setFullYear"
    }
    function Ca(a) {
        cb || (cb = Q(jb));
        a && cb.appendChild(a);
        cb.innerHTML = ""
    }
    function oa() {
    }
    function q(a, c) {
        function b(a, b) {
            function c(a, b) {
                this.pos = a;
                this.minor = b;
                this.isNew = !0;
                b || this.addLabel()
            }
            function d(a) {
                if (a)
                    this.options = a, this.id = a.id;
                return this
            }
            function e() {
                var a = [], c = [], d;
                V = La = null;
                xb = [];
                t(ia, function(e) {
                            d = !1;
                            t(["xAxis", "yAxis"], function(a) {
                                        if (e.isCartesian && (a == "xAxis" && r || a == "yAxis" && !r) && (e.options[a] == b.index || e.options[a] === ua && b.index === 0))
                                            e[a] = ja, xb.push(e), d = !0
                                    });
                            !e.visible && s.ignoreHiddenSeries && (d = !1);
                            if (d) {
                                var f, g, k, n, z;
                                if (!r) {
                                    f = e.options.stacking;
                                    la = f == "percent";
                                    if (f)
                                        n = e.type + p(e.options.stack, ""), z = "-" + n, e.stackKey = n, g = a[n] || [], a[n] = g, k = c[z] || [], c[z] = k;
                                    la && (V = 0, La = 99)
                                }
                                e.isCartesian && (t(e.data, function(a) {
                                            var b = a.x, c = a.y, d = c < 0, e = d ? k : g, d = d ? z : n;
                                            V === null && (V = La = a[Rb]);
                                            r ? b > La ? La = b : b < V && (V = b) : u(c) && (f && (e[b] = u(e[b]) ? e[b] + c : c), c = e ? e[b] : c, a = p(a.low, c), la || (c > La ? La = c : a < V && (V = a)), f && (E[d] || (E[d] = {}), E[d][b] = {
                                                total : c,
                                                cum   : c
                                            }))
                                        }), /(area|column|bar)/.test(e.type) && !r && (V >= 0 ? (V = 0, T = !0) : La < 0 && (La = 0, U = !0)))
                            }
                        })
            }
            function f(a, c) {
                var d;
                D = c ? 1 : W.pow(10, Oa(W.log(a) / W.LN10));
                d = a / D;
                if (!c && (c = [1, 2, 2.5, 5, 10], b.allowDecimals === !1 || Eb))
                    D == 1 ? c = [1, 2, 5, 10] : D <= 0.1 && (c = [1 / D]);
                for (var e = 0; e < c.length; e++)
                    if (a = c[e], d <= (c[e] + (c[e + 1] || c[e])) / 2)
                        break;
                a *= D;
                return a
            }
            function g(a) {
                var b;
                b = a;
                D = p(D, W.pow(10, Oa(W.log(C) / W.LN10)));
                D < 1 && (b = J(1 / D) * 10, b = J(a * b) / b);
                return b
            }
            function k() {
                var c, d, e, n, z = b.tickInterval, h = b.tickPixelInterval;
                c = b.maxZoom || (r && !u(b.min) && !u(b.max) ? Pa(a.smallestInterval * 5, La - V) : null);
                q = j ? da : ba;
                L ? (e = a[r ? "xAxis" : "yAxis"][b.linkedTo], n = e.getExtremes(), M = p(n.min, n.dataMin), P = p(n.max, n.dataMax)) : (M = p(Fb, b.min, V), P = p(v, b.max, La));
                Eb && (M = W.log(M) / W.LN10, P = W.log(P) / W.LN10);
                P - M < c && (n = (c - P + M) / 2, M = ea(M - n, p(b.min, M - n), V), P = Pa(M + c, p(b.max, M + c), La));
                if (!Aa && !la && !L && u(M) && u(P)) {
                    c = P - M || 1;
                    if (!u(b.min) && !u(Fb) && vc && (V < 0 || !T))
                        M -= c * vc;
                    if (!u(b.max) && !u(v) && R && (La > 0 || !U))
                        P += c * R
                }
                C = M == P ? 1 : L && !z && h == e.options.tickPixelInterval ? e.tickInterval : p(z, Aa ? 1 : (P - M) * h / q);
                !Sb && !u(b.tickInterval) && (C = f(C));
                ja.tickInterval = C;
                Z = b.minorTickInterval === "auto" && C ? C / 5 : b.minorTickInterval;
                if (Sb) {
                    aa = [];
                    var z = va.global.useUTC, m = 1E3 / Sa, H = 6E4 / Sa, l = 36E5 / Sa, h = 864E5 / Sa;
                    c = 6048E5 / Sa;
                    n = 2592E6 / Sa;
                    var t = 31556952E3 / Sa, s = [["second", m, [1, 2, 5, 10, 15, 30]], ["minute", H, [1, 2, 5, 10, 15, 30]], ["hour", l, [1, 2, 3, 4, 6, 8, 12]], ["day", h, [1, 2]], ["week", c, [1, 2]], ["month", n, [1, 2, 3, 4, 6]], ["year", t, null]], wb = s[6], O = wb[1], o = wb[2];
                    for (e = 0; e < s.length; e++)
                        if (wb = s[e], O = wb[1], o = wb[2], s[e + 1] && C <= (O * o[o.length - 1] + s[e + 1][1]) / 2)
                            break;
                    O == t && C < 5 * O && (o = [1, 2, 5]);
                    s = f(C / O, o);
                    o = new Date(M * Sa);
                    o.setMilliseconds(0);
                    O >= m && o.setSeconds(O >= H ? 0 : s * Oa(o.getSeconds() / s));
                    if (O >= H)
                        o[nc](O >= l ? 0 : s * Oa(o[$b]() / s));
                    if (O >= l)
                        o[oc](O >= h ? 0 : s * Oa(o[ac]() / s));
                    if (O >= h)
                        o[cc](O >= n ? 1 : s * Oa(o[Db]() / s));
                    O >= n && (o[pc](O >= t ? 0 : s * Oa(o[Pb]() / s)), d = o[Qb]());
                    O >= t && (d -= d % s, o[qc](d));
                    if (O == c)
                        o[cc](o[Db]() - o[bc]() + b.startOfWeek);
                    e = 1;
                    d = o[Qb]();
                    m = o.getTime() / Sa;
                    H = o[Pb]();
                    for (l = o[Db](); m < P && e < da;)
                        aa.push(m), O == t ? m = Ob(d + e * s, 0) / Sa : O == n ? m = Ob(d, H + e * s) / Sa : !z && (O == h || O == c) ? m = Ob(d, H, l + e * s * (O == h ? 1 : 7)) : m += O * s, e++;
                    aa.push(m);
                    ma = b.dateTimeLabelFormats[wb[0]]
                }
                else {
                    e = Oa(M / C) * C;
                    d = dc(P / C) * C;
                    aa = [];
                    for (e = g(e); e <= d;)
                        aa.push(e), e = g(e + C)
                }
                if (!L) {
                    if (Aa || r && a.hasColumn) {
                        d = (Aa ? 1 : C) * 0.5;
                        if (Aa || !u(p(b.min, Fb)))
                            M -= d;
                        if (Aa || !u(p(b.max, v)))
                            P += d
                    }
                    d = aa[0];
                    e = aa[aa.length - 1];
                    b.startOnTick ? M = d : M > d && aa.shift();
                    b.endOnTick ? P = e : P < e && aa.pop();
                    db || (db = {
                        x : 0,
                        y : 0
                    });
                    if (!Sb && aa.length > db[Rb])
                        db[Rb] = aa.length
                }
            }
            function n() {
                var a, b;
                Xa = M;
                I = P;
                e();
                k();
                sa = qa;
                qa = q / (P - M || 1);
                if (!r)
                    for (a in E)
                        for (b in E[a])
                            E[a][b].cum = E[a][b].total;
                if (!ja.isDirty)
                    ja.isDirty = M != Xa || P != I
            }
            function z(a) {
                a = (new d(a)).render();
                ta.push(a);
                return a
            }
            function h() {
                var e = b.title, f = b.alternateGridColor, g = b.lineWidth, k, n, z = a.hasRendered, m = z && u(Xa) && !isNaN(Xa);
                k = xb.length && u(M) && u(P);
                q = j ? da : ba;
                qa = q / (P - M || 1);
                Ca = j ? F : Ha;
                if (k || L) {
                    if (Z && !Aa)
                        for (k = M + (aa[0] - M) % Z; k <= P; k += Z)
                            Q[k] || (Q[k] = new c(k, !0)), m && Q[k].isNew && Q[k].render(null, !0), Q[k].isActive = !0, Q[k].render();
                    t(aa, function(a, b) {
                                if (!L || a >= M && a <= P)
                                    m && N[a].isNew && N[a].render(b, !0), N[a].isActive = !0, N[a].render(b)
                            });
                    f && t(aa, function(a, b) {
                                if (b % 2 === 0 && a < P)
                                    ca[a] || (ca[a] = new d), ca[a].options = {
                                        from  : a,
                                        to    : aa[b + 1] !== ua ? aa[b + 1] : P,
                                        color : f
                                    }, ca[a].render(), ca[a].isActive = !0
                            });
                    z || t((b.plotLines || []).concat(b.plotBands || []), function(a) {
                                ta.push((new d(a)).render())
                            })
                }
                t([N, Q, ca], function(a) {
                            for (var b in a)
                                a[b].isActive ? a[b].isActive = !1 : (a[b].destroy(), delete a[b])
                        });
                g && (k = F + (H ? da : 0) + kb, n = wa - Ha - (H ? ba : 0) + kb, k = G.crispLine([y, j ? F : k, j ? n : K, w, j ? xa - Y : k, j ? n : wa - Ha], g), bb ? bb.animate({
                            d : k
                        }) : bb = G.path(k).attr({
                            stroke         : b.lineColor,
                            "stroke-width" : g,
                            zIndex         : 7
                        }).add());
                ja.axisTitle && (k = j ? F : K, g = x(e.style.fontSize || 12), k = {
                    low    : k + (j ? 0 : q),
                    middle : k + q / 2,
                    high   : k + (j ? q : 0)
                }[e.align], g = (j ? K + ba : F) + (j ? 1 : -1) * (H ? -1 : 1) * $ + (o == 2 ? g : 0), ja.axisTitle[z ? "animate" : "attr"]({
                            x : j ? k : g + (H ? da : 0) + kb + (e.x || 0),
                            y : j ? g - (H ? ba : 0) + kb : k + (e.y || 0)
                        }));
                ja.isDirty = !1
            }
            function m(a) {
                for (var b = ta.length; b--;)
                    ta[b].id == a && ta[b].destroy()
            }
            var r = b.isX, H = b.opposite, j = pa ? !r : r, o = j ? H ? 0 : 2 : H ? 1 : 3, E = {}, b = fa(r ? Tb : ec, [yc, zc, rc, Ac][o], b), ja = this, Gb = b.type, Sb = Gb == "datetime", Eb = Gb == "logarithmic", kb = b.offset || 0, Rb = r ? "x" : "y", q, qa, sa, Ca = j ? F : Ha, Fa, Ba, oa, B, bb, V, La, xb, Fb, v, P = null, M = null, Xa, I, vc = b.minPadding, R = b.maxPadding, L = u(b.linkedTo), T, U, la, Gb = b.events, za, ta = [], C, Z, D, aa, N = {}, Q = {}, ca = {}, X, ga, $, ma, Aa = b.categories, ya = b.labels.formatter || function() {
                var a = this.value;
                return ma ? Ub(ma, a) : C % 1E6 === 0 ? a / 1E6 + "M" : C % 1E3 === 0 ? a / 1E3 + "k" : !Aa && a >= 1E3 ? l(a, 0) : a
            }, na = j && b.labels.staggerLines, S = b.reversed, ha = Aa && b.tickmarkPlacement == "between" ? 0.5 : 0;
            c.prototype = {
                addLabel     : function() {
                    var a = this.pos, c = b.labels, d = !(a == M && !p(b.showFirstLabel, 1) || a == P && !p(b.showLastLabel, 0)), e = Aa && j && Aa.length && !c.step && !c.staggerLines && !c.rotation && da / Aa.length || !j && da / 2, f = this.label, g = Aa && u(Aa[a]) ? Aa[a] : a, a = ya.call({
                                isFirst             : a == aa[0],
                                isLast              : a == aa[aa.length - 1],
                                dateTimeLabelFormat : ma,
                                value               : Eb ? W.pow(10, g) : g
                            }), e = e && {
                        width : ea(1, J(e - 2 * (c.padding || 10))) + Da
                    }, e = A(e, c.style);
                    f === ua ? this.label = u(a) && d && c.enabled ? G.text(a, 0, 0).attr({
                                align    : c.align,
                                rotation : c.rotation
                            }).css(e).add(oa) : null : f && f.attr({
                                text : a
                            }).css(e)
                },
                getLabelSize : function() {
                    var a = this.label;
                    return a ? (this.labelBBox = a.getBBox())[j ? "height" : "width"] : 0
                },
                render       : function(a, c) {
                    var d = !this.minor, e = this.label, f = this.pos, g = b.labels, k = this.gridLine, n = d ? b.gridLineWidth : b.minorGridLineWidth, z = d ? b.gridLineColor : b.minorGridLineColor, h = d ? b.gridLineDashStyle : b.minorGridLineDashStyle, m = this.mark, V = d ? b.tickLength : b.minorTickLength, vb = d ? b.tickWidth : b.minorTickWidth || 0, r = d ? b.tickColor : b.minorTickColor, l = d ? b.tickPosition : b.minorTickPosition, d = g.step, s = c && gb || wa, o;
                    o = j ? Fa(f + ha, null, null, c) + Ca : F + kb + (H ? (c && Ea || xa) - Y - F : 0);
                    s = j ? s - Ha + kb - (H ? ba : 0) : s - Fa(f + ha, null, null, c) - Ca;
                    if (n) {
                        f = Ba(f + ha, n, c);
                        if (k === ua) {
                            k = {
                                stroke         : z,
                                "stroke-width" : n
                            };
                            if (h)
                                k.dashstyle = h;
                            this.gridLine = k = n ? G.path(f).attr(k).add(B) : null
                        }
                        k && f && k.animate({
                                    d : f
                                })
                    }
                    if (vb)
                        l == "inside" && (V = -V), H && (V = -V), n = G.crispLine([y, o, s, w, o + (j ? 0 : -V), s + (j ? V : 0)], vb), m ? m.animate({
                                    d : n
                                }) : this.mark = G.path(n).attr({
                                    stroke         : r,
                                    "stroke-width" : vb
                                }).add(oa);
                    if (e && !isNaN(o)) {
                        o = o + g.x - (ha && j ? ha * qa * (S ? -1 : 1) : 0);
                        s = s + g.y - (ha && !j ? ha * qa * (S ? 1 : -1) : 0);
                        u(g.y) || (s += parseInt(e.styles["line-height"]) * 0.9 - e.getBBox().height / 2);
                        na && (s += a / (d || 1) % na * 16);
                        if (d)
                            e[a % d ? "hide" : "show"]();
                        e[this.isNew ? "attr" : "animate"]({
                                    x : o,
                                    y : s
                                })
                    }
                    this.isNew = !1
                },
                destroy      : function() {
                    for (var a in this)
                        this[a] && this[a].destroy && this[a].destroy()
                }
            };
            d.prototype = {
                render  : function() {
                    var a = this, b = a.options, c = b.label, d = a.label, e = b.width, f = b.to, g, k = b.from, n = b.dashStyle, z = a.svgElem, h = [], m, V, vb = b.color;
                    V = b.zIndex;
                    var r = b.events;
                    if (e) {
                        if (h = Ba(b.value, e), b = {
                            stroke         : vb,
                            "stroke-width" : e
                        }, n)
                            b.dashstyle = n
                    }
                    else if (u(k) && u(f))
                        k = ea(k, M), f = Pa(f, P), g = Ba(f), (h = Ba(k)) && g ? h.push(g[4], g[5], g[1], g[2]) : h = null, b = {
                            fill : vb
                        };
                    else
                        return;
                    if (u(V))
                        b.zIndex = V;
                    if (z)
                        h ? z.animate({
                                    d : h
                                }, null, z.onGetPath) : (z.hide(), z.onGetPath = function() {
                            z.show()
                        });
                    else if (h && h.length && (a.svgElem = z = G.path(h).attr(b).add(), r))
                        for (m in n = function(b) {
                            z.on(b, function(c) {
                                        r[b].apply(a, [c])
                                    })
                        }, r)
                            n(m);
                    if (c && u(c.text) && h && h.length && da > 0 && ba > 0) {
                        c = fa({
                                    align         : j && g && "center",
                                    x             : j ? !g && 4 : 10,
                                    verticalAlign : !j && g && "middle",
                                    y             : j ? g ? 16 : 10 : g ? 6 : -4,
                                    rotation      : j && !g && 90
                                }, c);
                        if (!d)
                            a.label = d = G.text(c.text, 0, 0).attr({
                                        align    : c.textAlign || c.align,
                                        rotation : c.rotation,
                                        zIndex   : V
                                    }).css(c.style).add();
                        g = [h[1], h[4], p(h[6], h[1])];
                        h = [h[2], h[5], p(h[7], h[2])];
                        m = Pa.apply(W, g);
                        V = Pa.apply(W, h);
                        d.align(c, !1, {
                                    x      : m,
                                    y      : V,
                                    width  : ea.apply(W, g) - m,
                                    height : ea.apply(W, h) - V
                                });
                        d.show()
                    }
                    else
                        d && d.hide();
                    return a
                },
                destroy : function() {
                    for (var a in this)
                        this[a] && this[a].destroy && this[a].destroy(), delete this[a];
                    Za(ta, this)
                }
            };
            Fa = function(a, b, c, d, e) {
                var f = 1, g = 0, k = d ? sa : qa, d = d ? Xa : M;
                k || (k = qa);
                c && (f *= -1, g = q);
                S && (f *= -1, g -= f * q);
                b ? (S && (a = q - a), a = a / k + d, Eb && e && (a = W.pow(10, a))) : (Eb && e && (a = W.log(a) / W.LN10), a = f * (a - d) * k + g);
                return a
            };
            Ba = function(a, b, c) {
                var d, e, f, a = Fa(a, null, null, c), g = c && gb || wa, k = c && Ea || xa, n, c = e = J(a + Ca);
                d = f = J(g - a - Ca);
                if (isNaN(a))
                    n = !0;
                else if (j) {
                    if (d = K, f = g - Ha, c < F || c > F + da)
                        n = !0
                }
                else if (c = F, e = k - Y, d < K || d > K + ba)
                    n = !0;
                return n ? null : G.crispLine([y, c, d, w, e, f], b || 0)
            };
            pa && r && S === ua && (S = !0);
            A(ja, {
                        addPlotBand       : z,
                        addPlotLine       : z,
                        adjustTickAmount  : function() {
                            if (db && !Sb && !Aa && !L) {
                                var a = X, b = aa.length;
                                X = db[Rb];
                                if (b < X) {
                                    for (; aa.length < X;)
                                        aa.push(g(aa[aa.length - 1] + C));
                                    qa *= (b - 1) / (X - 1);
                                    P = aa[aa.length - 1]
                                }
                                if (u(a) && X != a)
                                    ja.isDirty = !0
                            }
                        },
                        categories        : Aa,
                        getExtremes       : function() {
                            return {
                                min     : M,
                                max     : P,
                                dataMin : V,
                                dataMax : La,
                                userMin : Fb,
                                userMax : v
                            }
                        },
                        getPlotLinePath   : Ba,
                        getThreshold      : function(a) {
                            M > a ? a = M : P < a && (a = P);
                            return Fa(a, 0, 1)
                        },
                        isXAxis           : r,
                        options           : b,
                        plotLinesAndBands : ta,
                        getOffset         : function() {
                            var a = xb.length && u(M) && u(P), d = 0, e = 0, f = b.title, g = b.labels, k = [-1, 1, 1, -1][o];
                            oa || (oa = G.g("axis").attr({
                                        zIndex : 7
                                    }).add(), B = G.g("grid").attr({
                                        zIndex : 1
                                    }).add());
                            ga = 0;
                            if (a || L)
                                t(aa, function(a) {
                                            N[a] ? N[a].addLabel() : N[a] = new c(a);
                                            if (o === 0 || o == 2 || {
                                                1 : "left",
                                                3 : "right"
                                            }[o] == g.align)
                                                ga = ea(N[a].getLabelSize(), ga)
                                        }), na && (ga += (na - 1) * 16);
                            else
                                for (var n in N)
                                    N[n].destroy(), delete N[n];
                            if (f && f.text) {
                                if (!ja.axisTitle)
                                    ja.axisTitle = G.text(f.text, 0, 0).attr({
                                                zIndex   : 7,
                                                rotation : f.rotation || 0,
                                                align    : f.textAlign || {
                                                    low    : "left",
                                                    middle : "center",
                                                    high   : "right"
                                                }[f.align]
                                            }).css(f.style).add();
                                d = ja.axisTitle.getBBox()[j ? "height" : "width"];
                                e = p(f.margin, j ? 5 : 10)
                            }
                            kb = k * (b.offset || Ya[o]);
                            $ = ga + (o != 2 && ga && k * b.labels[j ? "y" : "x"]) + e;
                            Ya[o] = ea(Ya[o], $ + d + k * kb)
                        },
                        render            : h,
                        setCategories     : function(b, c) {
                            ja.categories = Aa = b;
                            t(xb, function(a) {
                                        a.translate();
                                        a.setTooltipPoints(!0)
                                    });
                            ja.isDirty = !0;
                            p(c, !0) && a.redraw()
                        },
                        setExtremes       : function(b, c, d, e) {
                            d = p(d, !0);
                            ka(ja, "setExtremes", {
                                        min : b,
                                        max : c
                                    }, function() {
                                        Fb = b;
                                        v = c;
                                        d && a.redraw(e)
                                    })
                        },
                        setScale          : n,
                        setTickPositions  : k,
                        translate         : Fa,
                        redraw            : function() {
                            yb.resetTracker && yb.resetTracker();
                            h();
                            t(ta, function(a) {
                                        a.render()
                                    });
                            t(xb, function(a) {
                                        a.isDirty = !0
                                    })
                        },
                        removePlotBand    : m,
                        removePlotLine    : m,
                        reversed          : S,
                        stacks            : E
                    });
            for (za in Gb)
                ra(ja, za, Gb[za]);
            n()
        }
        function d() {
            var b = {};
            return {
                add    : function(c, d, e, f) {
                    b[c] || (d = G.text(d, 0, 0).css(a.toolbar.itemStyle).align({
                                align : "right",
                                x     : -Y - 20,
                                y     : K + 30
                            }).on("click", f).attr({
                                align  : "right",
                                zIndex : 20
                            }).add(), b[c] = d)
                },
                remove : function(a) {
                    Ca(b[a].element);
                    b[a] = null
                }
            }
        }
        function f(a) {
            function b() {
                var a = this.points || Na(this), c = a[0].series.xAxis, d = this.x, c = c && c.options.type == "datetime", e = Wa(d) || c, f;
                f = e ? ['<span style="font-size: 10px">' + (c ? Ub("%A, %b %e, %Y", d) : d) + "</span>"] : [];
                t(a, function(a) {
                            f.push(a.point.tooltipFormatter(e))
                        });
                return f.join("<br/>")
            }
            function c(a, b) {
                o = j ? a : (2 * o + a) / 3;
                l = j ? b : (l + b) / 2;
                ja.translate(o, l);
                fc = ha(a - o) > 1 || ha(b - l) > 1 ? function() {
                    c(a, b)
                } : null
            }
            function d() {
                if (!j) {
                    var a = r.hoverPoints;
                    ja.hide();
                    t(k, function(a) {
                                a && a.hide()
                            });
                    a && t(a, function(a) {
                                a.setState()
                            });
                    r.hoverPoints = null;
                    j = !0
                }
            }
            var e, f = a.borderWidth, g = a.crosshairs, k = [], n = a.style, z = a.shared, h = x(n.padding), m = f + h, j = !0, s, H, o = 0, l = 0;
            n.padding = 0;
            var ja = G.g("tooltip").attr({
                        zIndex : 8
                    }).add(), p = G.rect(m, m, 0, 0, a.borderRadius, f).attr({
                        fill           : a.backgroundColor,
                        "stroke-width" : f
                    }).add(ja).shadow(a.shadow), w = G.text("", h + m, x(n.fontSize) + h + m).attr({
                        zIndex : 1
                    }).css(n).add(ja);
            ja.hide();
            return {
                shared  : z,
                refresh : function(f) {
                    var n, o, l, E = 0, u = {}, q = [];
                    l = f.tooltipPos;
                    n = a.formatter || b;
                    u = r.hoverPoints;
                    z ? (u && t(u, function(a) {
                                a.setState()
                            }), r.hoverPoints = f, t(f, function(a) {
                                a.setState(Qa);
                                E += a.plotY;
                                q.push(a.getLabelConfig())
                            }), o = f[0].plotX, E = J(E) / f.length, u = {
                        x : f[0].category
                    }, u.points = q, f = f[0]) : u = f.getLabelConfig();
                    u = n.call(u);
                    e = f.series;
                    o = z ? o : f.plotX;
                    E = z ? E : f.plotY;
                    n = J(l ? l[0] : pa ? da - E : o);
                    o = J(l ? l[1] : pa ? ba - o : E);
                    l = z || !f.series.isCartesian || Ra(n, o);
                    u === !1 || !l ? d() : (j && (ja.show(), j = !1), w.attr({
                                text : u
                            }), l = w.getBBox(), s = l.width + 2 * h, H = l.height + 2 * h, p.attr({
                                width  : s,
                                height : H,
                                stroke : a.borderColor || f.color || e.color || "#606060"
                            }), n = n - s + F - 25, o = o - H + K + 10, p.attr({
                                fill : Bc(f.color || e.color || "#606060", lb, l)
                            }), n < 7 && (n = 7, o -= 30), o < 5 ? o = 5 : o + H > wa && (o = wa - H - 5), c(J(n - m), J(o - m)));
                    if (g) {
                        g = Na(g);
                        o = g.length;
                        for (var y; o--;)
                            if (g[o] && (y = f.series[o ? "yAxis" : "xAxis"]))
                                if (n = y.getPlotLinePath(f[o ? "y" : "x"], 1), k[o])
                                    k[o].attr({
                                                d          : n,
                                                visibility : $a
                                            });
                                else {
                                    l = {
                                        "stroke-width" : g[o].width || 1,
                                        stroke         : g[o].color || "#C0C0C0",
                                        zIndex         : 2
                                    };
                                    if (g[o].dashStyle)
                                        l.dashstyle = g[o].dashStyle;
                                    k[o] = G.path(n).attr(l).add()
                                }
                    }
                },
                hide    : d
            }
        }
        function e(a, b) {
            function c(a) {
                var b, d = sc && U.width / U.documentElement.clientWidth - 1, e, f, g, a = a || ma.event;
                if (!a.target)
                    a.target = a.srcElement;
                b = a.touches ? a.touches.item(0) : a;
                if (a.type != "mousemove" || ma.opera || d) {
                    e = L;
                    for (f = {
                        left : e.offsetLeft,
                        top  : e.offsetTop
                    }; e = e.offsetParent;)
                        f.left += e.offsetLeft, f.top += e.offsetTop, e != U.body && e != U.documentElement && (f.left -= e.scrollLeft, f.top -= e.scrollTop);
                    Hb = f;
                    e = Hb.left;
                    f = Hb.top
                }
                Mb ? (g = a.x, b = a.y) : b.layerX === ua ? (g = b.pageX - e, b = b.pageY - f) : (g = a.layerX, b = a.layerY);
                d && (g += J((d + 1) * e - e), b += J((d + 1) * f - f));
                return A(a, {
                            chartX : g,
                            chartY : b
                        })
            }
            function d(a) {
                var b = {
                    xAxis : [],
                    yAxis : []
                };
                t(ya, function(c) {
                            var d = c.translate, e = c.isXAxis;
                            b[e ? "xAxis" : "yAxis"].push({
                                        axis  : c,
                                        value : d((pa ? !e : e) ? a.chartX - F : ba - a.chartY + K, !0)
                                    })
                        });
                return b
            }
            function e() {
                var b = a.hoverSeries, c = a.hoverPoint;
                if (c)
                    c.onMouseOut();
                if (b)
                    b.onMouseOut();
                Va && Va.hide();
                gc = null
            }
            function g() {
                if (h) {
                    var b = {
                        xAxis : [],
                        yAxis : []
                    }, c = h.getBBox(), d = c.x - F, e = c.y - K;
                    z && (t(ya, function(a) {
                                var f = a.translate, g = a.isXAxis, k = pa ? !g : g, n = f(k ? d : ba - e - c.height, !0, 0, 0, 1), f = f(k ? d + c.width : ba - e, !0, 0, 0, 1);
                                b[g ? "xAxis" : "yAxis"].push({
                                            axis : a,
                                            min  : Pa(n, f),
                                            max  : ea(n, f)
                                        })
                            }), ka(a, "selection", b, hc));
                    h = h.destroy()
                }
                a.mouseIsDown = hb = z = !1;
                ab(U, eb ? "touchend" : "mouseup", g)
            }
            var k, n, z, h, m = s.zoomType, o = /x/.test(m), j = /y/.test(m), l = o && !pa || j && pa, r = j && !pa || o && pa;
            ub = function() {
                cb ? (cb.translate(F, K), pa && cb.attr({
                            width  : a.plotWidth,
                            height : a.plotHeight
                        }).invert()) : a.trackerGroup = cb = G.g("tracker").attr({
                            zIndex : 9
                        }).add()
            };
            ub();
            if (b.enabled)
                a.tooltip = Va = f(b);
            (function() {
                var f = !0;
                L.onmousedown = function(b) {
                    b = c(b);
                    a.mouseIsDown = hb = !0;
                    k = b.chartX;
                    n = b.chartY;
                    ra(U, eb ? "touchend" : "mouseup", g)
                };
                var m = function(d) {
                    if (!d || !(d.touches && d.touches.length > 1)) {
                        d = c(d);
                        if (!eb)
                            d.returnValue = !1;
                        var g = d.chartX, m = d.chartY, s = !Ra(g - F, m - K);
                        eb && d.type == "touchstart" && (I(d.target, "isTracker") ? a.runTrackerClick || d.preventDefault() : !Ja && !s && d.preventDefault());
                        s && (f || e(), g < F ? g = F : g > F + da && (g = F + da), m < K ? m = K : m > K + ba && (m = K + ba));
                        if (hb && d.type != "touchstart") {
                            if (z = Math.sqrt(Math.pow(k - g, 2) + Math.pow(n - m, 2)) > 10) {
                                if (zb && (o || j) && Ra(k - F, n - K))
                                    h || (h = G.rect(F, K, l ? 1 : da, r ? 1 : ba, 0).attr({
                                                fill   : "rgba(69,114,167,0.25)",
                                                zIndex : 7
                                            }).add());
                                h && l && (g -= k, h.attr({
                                            width : ha(g),
                                            x     : (g > 0 ? 0 : g) + k
                                        }));
                                h && r && (m -= n, h.attr({
                                            height : ha(m),
                                            y      : (m > 0 ? 0 : m) + n
                                        }))
                            }
                        }
                        else if (!s) {
                            var H, m = a.hoverPoint, g = a.hoverSeries, t, O, u = xa, p = pa ? d.chartY : d.chartX - F;
                            if (Va && b.shared) {
                                H = [];
                                t = ia.length;
                                for (O = 0; O < t; O++)
                                    if (ia[O].visible && ia[O].tooltipPoints.length)
                                        d = ia[O].tooltipPoints[p], d._dist = ha(p - d.plotX), u = Pa(u, d._dist), H.push(d);
                                for (t = H.length; t--;)
                                    H[t]._dist > u && H.splice(t, 1);
                                if (H.length && H[0].plotX != gc)
                                    Va.refresh(H), gc = H[0].plotX
                            }
                            if (g && g.tracker && (d = g.tooltipPoints[p]) && d != m)
                                d.onMouseOver()
                        }
                        return (f = s) || !zb
                    }
                };
                L.onmousemove = m;
                ra(L, "mouseleave", e);
                L.ontouchstart = function(a) {
                    if (o || j)
                        L.onmousedown(a);
                    m(a)
                };
                L.ontouchmove = m;
                L.ontouchend = function() {
                    z && e()
                };
                L.onclick = function(b) {
                    var e = a.hoverPoint, b = c(b);
                    b.cancelBubble = !0;
                    if (!z)
                        if (e && I(b.target, "isTracker")) {
                            var f = e.plotX, g = e.plotY;
                            A(e, {
                                        pageX : Hb.left + F + (pa ? da - g : f),
                                        pageY : Hb.top + K + (pa ? ba - f : g)
                                    });
                            ka(e.series, "click", A(b, {
                                                point : e
                                            }));
                            e.firePointEvent("click", b)
                        }
                        else
                            A(b, d(b)), Ra(b.chartX - F, b.chartY - K) && ka(a, "click", b);
                    z = !1
                }
            })();
            Kb = setInterval(function() {
                        fc && fc()
                    }, 32);
            A(this, {
                        zoomX        : o,
                        zoomY        : j,
                        resetTracker : e
                    })
        }
        function g(a) {
            var b = a.type || s.type || s.defaultSeriesType, c = Ta[b], d = r.hasRendered;
            if (d)
                if (pa && b == "column")
                    c = Ta.bar;
                else if (!pa && b == "bar")
                    c = Ta.column;
            b = new c;
            b.init(r, a);
            !d && b.inverted && (pa = !0);
            if (b.isCartesian)
                zb = b.isCartesian;
            ia.push(b);
            return b
        }
        function k() {
            s.alignTicks !== !1 && t(ya, function(a) {
                        a.adjustTickAmount()
                    });
            db = null
        }
        function n(a) {
            var b = r.isDirtyLegend, c, d = r.isDirtyBox, e = ia.length, f = e, g = r.clipRect;
            for (j(a, r); f--;)
                if (a = ia[f], a.isDirty && a.options.stacking) {
                    c = !0;
                    break
                }
            if (c)
                for (f = e; f--;)
                    if (a = ia[f], a.options.stacking)
                        a.isDirty = !0;
            t(ia, function(a) {
                        a.isDirty && (a.cleanData(), a.getSegments(), a.options.legendType == "point" && (b = !0))
                    });
            if (b && pb.renderLegend)
                pb.renderLegend(), r.isDirtyLegend = !1;
            zb && (Vb || (db = null, t(ya, function(a) {
                        a.setScale()
                    })), k(), Ib(), t(ya, function(a) {
                        if (a.isDirty || d)
                            a.redraw(), d = !0
                    }));
            d && (ic(), ub(), g && (Ab(g), g.animate({
                        width  : r.plotSizeX,
                        height : r.plotSizeY
                    })));
            t(ia, function(a) {
                        a.isDirty && a.visible && (!a.isCartesian || a.xAxis) && a.redraw()
                    });
            yb && yb.resetTracker && yb.resetTracker();
            ka(r, "redraw")
        }
        function z() {
            var c = a.xAxis || {}, d = a.yAxis || {}, e, c = Na(c);
            t(c, function(a, b) {
                        a.index = b;
                        a.isX = !0
                    });
            d = Na(d);
            t(d, function(a, b) {
                        a.index = b
                    });
            ya = c.concat(d);
            r.xAxis = [];
            r.yAxis = [];
            ya = Bb(ya, function(a) {
                        e = new b(r, a);
                        r[e.isXAxis ? "xAxis" : "yAxis"].push(e);
                        return e
                    });
            k()
        }
        function h(b, c) {
            la = fa(a.title, b);
            C = fa(a.subtitle, c);
            t([["title", b, la], ["subtitle", c, C]], function(a) {
                        var b = a[0], c = r[b], d = a[1], a = a[2];
                        c && d && (c.destroy(), c = null);
                        a && a.text && !c && (r[b] = G.text(a.text, 0, 0).attr({
                                    align   : a.align,
                                    "class" : "jscharting-" + b,
                                    zIndex  : 1
                                }).css(a.style).add().align(a, !1, za))
                    })
        }
        function H() {
            R = s.renderTo;
            $ = mb + Jb++;
            Wa(R) && (R = U.getElementById(R));
            R.innerHTML = "";
            R.offsetWidth || (Z = R.cloneNode(0), D(Z, {
                        position : Cb,
                        top      : "-9999px",
                        display  : ""
                    }), U.body.appendChild(Z));
            ga = (Z || R).offsetWidth;
            ca = (Z || R).offsetHeight;
            r.chartWidth = xa = s.width || ga || 600;
            r.chartHeight = wa = s.height || (ca > 19 ? ca : 400);
            r.container = L = Q(jb, {
                        className : "jscharting-container" + (s.className ? " " + s.className : ""),
                        id        : $
                    }, A({
                                position  : tc,
                                overflow  : Ua,
                                width     : xa + Da,
                                height    : wa + Da,
                                textAlign : "left"
                            }, s.style), Z || R);
            r.renderer = G = s.forExport ? new rb(L, xa, wa, !0) : new Wb(L, xa, wa);
            var a, b;
            uc && L.getBoundingClientRect && (a = function() {
                D(L, {
                            left : 0,
                            top  : 0
                        });
                b = L.getBoundingClientRect();
                D(L, {
                            left : -b.left % 1 + Da,
                            top  : -b.top % 1 + Da
                        })
            }, a(), ra(ma, "resize", a), ra(r, "destroy", function() {
                        ab(ma, "resize", a)
                    }))
        }
        function o() {
            function a() {
                var c = s.width || R.offsetWidth, d = s.height || R.offsetHeight;
                if (c && d) {
                    if (c != ga || d != ca)
                        clearTimeout(b), b = setTimeout(function() {
                                    jc(c, d, !1)
                                }, 100);
                    ga = c;
                    ca = d
                }
            }
            var b;
            ra(ma, "resize", a);
            ra(r, "destroy", function() {
                        ab(ma, "resize", a)
                    })
        }
        function qa() {
            var b = a.labels, c = a.credits, e;
            h();
            pb = r.legend = new Zb(r);
            Ib();
            t(ya, function(a) {
                        a.setTickPositions(!0)
                    });
            k();
            Ib();
            ic();
            zb && t(ya, function(a) {
                        a.render()
                    });
            if (!r.seriesGroup)
                r.seriesGroup = G.g("series-group").attr({
                            zIndex : 3
                        }).add();
            t(ia, function(a) {
                        a.translate();
                        a.setTooltipPoints();
                        a.render()
                    });
            b.items && t(b.items, function() {
                        var a = A(b.style, this.style), c = x(a.left) + F, d = x(a.top) + K + 12;
                        delete a.left;
                        delete a.top;
                        G.text(this.html, c, d).attr({
                                    zIndex : 2
                                }).css(a).add()
                    });
            if (!r.toolbar)
                r.toolbar = d(r);
            if (c.enabled && !r.credits)
                e = c.href, G.text(c.text, 0, 0).on("click", function() {
                            if (e)
                                location.href = e
                        }).attr({
                            align  : c.position.align,
                            zIndex : 8
                        }).css(c.style).add().align(c.position);
            ub();
            r.hasRendered = !0;
            Z && (R.appendChild(L), Ca(Z))
        }
        function q() {
            var a = ia.length, b = L && L.parentNode;
            ka(r, "destroy");
            ab(ma, "unload", q);
            ab(r);
            for (t(ya, function(a) {
                        ab(a)
                    }); a--;)
                ia[a].destroy();
            if (L)
                L.innerHTML = "", ab(L), b && b.removeChild(L), L = null;
            if (G)
                G.alignedObjects = null;
            clearInterval(Kb);
            for (a in r)
                delete r[a]
        }
        function B() {
            !lb && ma == ma.top && U.readyState != "complete" ? U.attachEvent("onreadystatechange", function() {
                        U.detachEvent("onreadystatechange", B);
                        U.readyState == "complete" && B()
                    }) : (Xb = fb = 0, H(), kc(), lc(), t(a.series || [], function(a) {
                        g(a)
                    }), r.inverted = pa = p(pa, a.chart.inverted), z(), r.render = qa, r.tracker = yb = new e(r, a.tooltip), qa(), ka(r, "load"), c && c.apply(r, [r]), t(r.callbacks, function(a) {
                        a.apply(r, [r])
                    }))
        }
        Tb = fa(Tb, va.xAxis);
        ec = fa(ec, va.yAxis);
        va.xAxis = va.yAxis = null;
        var a = fa(va, a), s = a.chart, E = s.margin, E = X(E) ? E : [E, E, E, E], bb = p(s.marginTop, E[0]), Ba = p(s.marginRight, E[1]), Fa = p(s.marginBottom, E[2]), sa = p(s.marginLeft, E[3]), oa = s.spacingTop, v = s.spacingRight, T = s.spacingBottom, ta = s.spacingLeft, za, la, C, K, Y, Ha, F, Ya, R, Z, L, $, ga, ca, xa, wa, Ea, gb, na, tb, Ia, S, r = this, Ja = (E = s.events) && !!E.click, Ma, Ra, Va, hb, sb, qb, ib, ba, da, yb, cb, ub, pb, nb, ob, Hb, zb = s.showAxes, Vb = 0, ya = [], db, ia = [], pa, G, fc, Kb, gc, ic, Ib, kc, lc, jc, hc, Lb, Zb = function(a) {
            function b(a, c) {
                var d = a.legendItem, e = a.legendLine, f = a.legendSymbol, g = r.color, n = c ? k.itemStyle.color : g, h = c ? a.color : g, g = c ? a.pointAttr[Ga] : {
                    stroke : g,
                    fill   : g
                };
                d && d.css({
                            fill : n
                        });
                e && e.attr({
                            stroke : h
                        });
                f && f.attr(g)
            }
            function c(a, b, d) {
                var e = a.legendItem, f = a.legendLine, g = a.legendSymbol, a = a.checkbox;
                e && e.attr({
                            x : b,
                            y : d
                        });
                f && f.translate(b, d - 4);
                g && g.attr({
                            x : b + g.xOff,
                            y : d + g.yOff
                        });
                if (a)
                    a.x = b, a.y = d
            }
            function d() {
                t(m, function(a) {
                            var b = a.checkbox, c = v.alignAttr;
                            b && D(b, {
                                        left : c.translateX + a.legendItemWidth + b.x - 40 + Da,
                                        top  : c.translateY + b.y - 11 + Da
                                    })
                        })
            }
            function e(a) {
                var d, f, g, m, o, s = a.legendItem;
                m = a.series || a;
                if (!s) {
                    o = /^(bar|pie|area|column)$/.test(m.type);
                    a.legendItem = s = G.text(k.labelFormatter.call(a), 0, 0).css(a.visible ? j : r).on("mouseover", function() {
                                a.setState(Qa);
                                s.css(l)
                            }).on("mouseout", function() {
                                s.css(a.visible ? j : r);
                                a.setState()
                            }).on("click", function() {
                                var b = function() {
                                    a.setVisible()
                                };
                                a.firePointEvent ? a.firePointEvent("legendItemClick", null, b) : ka(a, "legendItemClick", null, b)
                            }).attr({
                                zIndex : 2
                            }).add(v);
                    if (!o && a.options && a.options.lineWidth) {
                        var t = a.options;
                        m = {
                            "stroke-width" : t.lineWidth,
                            zIndex         : 2
                        };
                        if (t.dashStyle)
                            m.dashstyle = t.dashStyle;
                        a.legendLine = G.path([y, -h - z, 0, w, -z, 0]).attr(m).add(v)
                    }
                    o ? d = G.rect(f = -h - z, g = -11, h, 12, 2).attr({
                                "stroke-width" : 0,
                                zIndex         : 3
                            }).add(v) : a.options && a.options.marker && a.options.marker.enabled && (d = G.symbol(a.symbol, f = -h / 2 - z, g = -4, a.options.marker.radius).attr({
                                zIndex : 3
                            }).add(v));
                    if (d)
                        d.xOff = f, d.yOff = g;
                    a.legendSymbol = d;
                    b(a, a.visible);
                    if (a.options && a.options.showCheckbox)
                        a.checkbox = Q("input", {
                                    type           : "checkbox",
                                    checked        : a.selected,
                                    defaultChecked : a.selected
                                }, k.itemCheckboxStyle, L), ra(a.checkbox, "click", function(b) {
                                    ka(a, "checkboxClick", {
                                                checked : b.target.checked
                                            }, function() {
                                                a.select()
                                            })
                                })
                }
                d = s.getBBox();
                f = a.legendItemWidth = k.itemWidth || h + z + d.width + u;
                Ba = d.height;
                if (n && q - E + f > (Xa || xa - 2 * H - E))
                    q = E, qa += Ba;
                Fa = qa;
                c(a, q, qa);
                n ? q += f : qa += Ba;
                B = Xa || ea(n ? q - E : f, B)
            }
            function f() {
                q = E;
                qa = p;
                Fa = B = 0;
                v || (v = G.g("legend").attr({
                            zIndex : 7
                        }).add());
                m = [];
                t(bb, function(a) {
                            var b = a.options;
                            b.showInLegend && (m = m.concat(b.legendType == "point" ? a.data : a))
                        });
                m.sort(function(a, b) {
                            return (a.options.legendIndex || 0) - (b.options.legendIndex || 0)
                        });
                F && m.reverse();
                t(m, e);
                nb = Xa || B;
                ob = Fa - p + Ba;
                if (C || oa)
                    nb += 2 * H, ob += 2 * H, sa ? nb > 0 && ob > 0 && sa.animate(sa.crisp(null, null, null, nb, ob)) : sa = G.rect(0, 0, nb, ob, k.borderRadius, C || 0).attr({
                                stroke         : k.borderColor,
                                "stroke-width" : C || 0,
                                fill           : oa || Ka
                            }).add(v).shadow(k.shadow), sa[m.length ? "show" : "hide"]();
                for (var a = ["left", "right", "top", "bottom"], b, c = 4; c--;)
                    b = a[c], o[b] && o[b] != "auto" && (k[c < 2 ? "align" : "verticalAlign"] = b, k[c < 2 ? "x" : "y"] = x(o[b]) * (c % 2 ? -1 : 1));
                v.align(A(k, {
                                    width  : nb,
                                    height : ob
                                }), !0, za);
                Vb || d()
            }
            function b(a, c) {
                var d = a.legendItem, e = a.legendLine, f = a.legendSymbol, g = a.areaSymbol, n = r.color, h = c ? k.itemStyle.color : n, n = c ? a.color : n, z = /^(bar|column)$/.test(a.type);
                /^(areaspline|area|line)$/.test(a.type);
                d && d.css({
                            fill : h
                        });
                e && e.attr({
                            stroke : n
                        });
                f && (d = {}, d[""] = {
                    stroke : n,
                    fill   : n
                }, z && wc(d, lb, s, !0, !1), f.attr(d[""]), z || f.attr({
                            stroke : N(n)
                        }));
                g && (d = {}, d[""] = {
                    stroke : "#111111",
                    fill   : n
                }, g.attr(d[""]), g.attr({
                            stroke         : "#444444",
                            "stroke-width" : 0.8
                        }))
            }
            function g(a, b, c) {
                a.on("mouseover", function() {
                            b.setState(Qa);
                            c.css(l);
                            a.css({
                                        cursor : "pointer"
                                    })
                        }).on("mouseout", function() {
                            c.css(b.visible ? j : r);
                            b.setState()
                        }).on("click", function() {
                            var a = function() {
                                b.setVisible()
                            };
                            b.firePointEvent ? b.firePointEvent("legendItemClick", null, a) : ka(b, "legendItemClick", null, a)
                        }).attr({
                            zIndex : 2
                        }).add(v)
            }
            function e(a) {
                var d, f, m, o, s, t, p = a.legendItem;
                s = a.series || a;
                var O = 0;
                h = 20;
                symbolHeight = 9;
                if (!p) {
                    t = /^(bar|pie|column)$/.test(s.type);
                    m = /^(areaspline|area)$/.test(s.type);
                    a.legendItem = p = G.text(k.labelFormatter.call(a), 0, 0).css(a.visible ? j : r).on("mouseover", function() {
                                a.setState(Qa);
                                p.css(l)
                            }).on("mouseout", function() {
                                p.css(a.visible ? j : r);
                                a.setState()
                            }).on("click", function() {
                                var b = function() {
                                    a.setVisible()
                                };
                                a.firePointEvent ? a.firePointEvent("legendItemClick", null, b) : ka(a, "legendItemClick", null, b)
                            }).attr({
                                zIndex : 2
                            }).add(v);
                    if (m) {
                        var sa = a.options;
                        m = -h - z;
                        s = {
                            "stroke-width" : sa.lineWidth,
                            zIndex         : 2
                        };
                        if (sa.dashStyle)
                            s.dashstyle = sa.dashStyle;
                        m = G.path([y, m, 0, w, m + h / 3, 0 - symbolHeight / 2, m + h * 0.66, 0, m + h, 0 - symbolHeight / 2, m + h, 0 + symbolHeight / 2, m, 0 + symbolHeight / 2, m, 0]).attr(s).add(v);
                        g(m, a, p)
                    }
                    else if (!t && a.options && a.options.lineWidth) {
                        sa = a.options;
                        s = {
                            "stroke-width" : sa.lineWidth,
                            zIndex         : 2
                        };
                        if (sa.dashStyle)
                            s.dashstyle = sa.dashStyle;
                        a.legendLine = G.path([y, -h - z, symbolHeight / 2, w, -z, -symbolHeight / 2]).attr(s).add(v)
                    }
                    t ? (d = G.rect(f = -20 - z, o = -9, 20, 9, 1).attr({
                                "stroke-width" : 1,
                                zIndex         : 3
                            }).add(v), d.attr(d.crisp(1, f, o, 20, 9)), g(d, a, p)) : a.options && a.options.marker && a.options.marker.enabled && (d = G.symbol(a.symbol, f = -h / 2 - z, o = -4, a.options.marker.radius).attr({
                                zIndex : 3
                            }).add(v), g(d, a, p), d.attr({
                                "stroke-width" : 1
                            }));
                    if (d)
                        d.xOff = f, d.yOff = o + 2, m && (d.xOff += 1, d.yOff -= 2);
                    a.legendSymbol = d;
                    a.areaSymbol = m;
                    b(a, a.visible);
                    if (a.options && a.options.showCheckbox)
                        a.checkbox = Q("input", {
                                    type           : "checkbox",
                                    checked        : a.selected,
                                    defaultChecked : a.selected
                                }, k.itemCheckboxStyle, L), ra(a.checkbox, "click", function(b) {
                                    ka(a, "checkboxClick", {
                                                checked : b.target.checked
                                            }, function() {
                                                a.select()
                                            })
                                })
                }
                d = p.getBBox();
                d.height > 16 && (O = (d.height - 16) / 2);
                f = a.legendItemWidth = k.itemWidth || h + z + d.width + u;
                Ba = d.height;
                if (n && q - E + f > (Xa || xa - 2 * H - E))
                    q = E, qa += Ba;
                Fa = qa;
                c(a, q + 5, qa - 2 + O);
                n ? q += f : qa += Ba;
                B = Xa || ea(n ? q - E : f, B)
            }
            function c(a, b, d) {
                var e = a.legendItem, f = a.legendLine, g = a.legendSymbol, k = a.areaSymbol, a = a.checkbox;
                e && (e.getBBox(), e.attr({
                            x : b,
                            y : d
                        }));
                f && f.translate(b, d - 2);
                g && g.attr(g.crisp(1, b + g.xOff, d + g.yOff, 20, 9));
                k && k.translate(b, d - 2);
                if (a)
                    a.x = b, a.y = d
            }
            var k = a.options.legend;
            if (k.enabled) {
                var n = k.layout == "horizontal", h = k.symbolWidth, z = k.symbolPadding, m, o = k.style, j = k.itemStyle, l = k.itemHoverStyle, r = k.itemHiddenStyle, H = x(o.padding), u = 20, p = 18, E = 4 + H + h + z, q, qa, Fa, Ba = 0, sa, C = k.borderWidth, oa = k.backgroundColor, v, B, Xa = k.width, bb = a.series, F = k.reversed;
                f();
                ra(a, "endResize", d);
                return {
                    colorizeItem : b,
                    destroyItem  : function(a) {
                        var b = a.checkbox;
                        t(["legendItem", "legendLine", "legendSymbol"], function(b) {
                                    a[b] && a[b].destroy()
                                });
                        b && Ca(a.checkbox)
                    },
                    renderLegend : f
                }
            }
        };
        Ra = function(a, b) {
            return a >= 0 && a <= da && b >= 0 && b <= ba
        };
        Lb = function() {
            ka(r, "selection", {
                        resetSelection : !0
                    }, hc);
            r.toolbar.remove("zoom")
        };
        hc = function(a) {
            var b = va.lang, c = r.pointCount < 100;
            r.toolbar.add("zoom", b.resetZoom, b.resetZoomTitle, Lb);
            !a || a.resetSelection ? t(ya, function(a) {
                        a.setExtremes(null, null, !1, c)
                    }) : t(a.xAxis.concat(a.yAxis), function(a) {
                        var b = a.axis;
                        r.tracker[b.isXAxis ? "zoomX" : "zoomY"] && b.setExtremes(a.min, a.max, !1, c)
                    });
            n()
        };
        Ib = function() {
            var b = a.legend, c = p(b.margin, 10), d = b.x, e = b.y, f = b.align, g = b.verticalAlign, k;
            kc();
            if ((r.title || r.subtitle) && !u(bb))
                (k = ea(r.title && !la.floating && !la.verticalAlign && la.y || 0, r.subtitle && !C.floating && !C.verticalAlign && C.y || 0)) && (K = ea(K, k + p(la.margin, 15) + oa));
            b.enabled && !b.floating && (f == "right" ? u(Ba) || (Y = ea(Y, nb - d + c + v)) : f == "left" ? u(sa) || (F = ea(F, nb + d + c + ta)) : g == "top" ? u(bb) || (K = ea(K, ob + e + c + oa)) : g == "bottom" && (u(Fa) || (Ha = ea(Ha, ob - e + c + T))));
            zb && t(ya, function(a) {
                        a.getOffset()
                    });
            u(sa) || (F += Ya[3]);
            u(bb) || (K += Ya[0]);
            u(Fa) || (Ha += Ya[2]);
            u(Ba) || (Y += Ya[1]);
            lc()
        };
        jc = function(a, b, c) {
            var d = r.title, e = r.subtitle;
            Vb += 1;
            j(c, r);
            gb = wa;
            Ea = xa;
            r.chartWidth = xa = J(a);
            r.chartHeight = wa = J(b);
            D(L, {
                        width  : xa + Da,
                        height : wa + Da
                    });
            G.setSize(xa, wa, c);
            da = xa - F - Y;
            ba = wa - K - Ha;
            db = null;
            t(ya, function(a) {
                        a.isDirty = !0;
                        a.setScale()
                    });
            t(ia, function(a) {
                        a.isDirty = !0
                    });
            r.isDirtyLegend = !0;
            r.isDirtyBox = !0;
            Ib();
            d && d.align(null, null, za);
            e && e.align(null, null, za);
            n(c);
            gb = null;
            ka(r, "resize");
            setTimeout(function() {
                        ka(r, "endResize", null, function() {
                                    Vb -= 1
                                })
                    }, Nb && Nb.duration || 500)
        };
        lc = function() {
            r.plotLeft = F = J(F);
            r.plotTop = K = J(K);
            r.plotWidth = da = J(xa - F - Y);
            r.plotHeight = ba = J(wa - K - Ha);
            r.plotSizeX = pa ? ba : da;
            r.plotSizeY = pa ? da : ba;
            za = {
                x      : ta,
                y      : oa,
                width  : xa - ta - v,
                height : wa - oa - T
            }
        };
        kc = function() {
            K = p(bb, oa);
            Y = p(Ba, v);
            Ha = p(Fa, T);
            F = p(sa, ta);
            Ya = [0, 0, 0, 0]
        };
        ic = function() {
            var a = s.borderWidth || 0, b = s.backgroundColor, c = s.plotBackgroundColor, d = s.plotBackgroundImage, e, f = {
                x      : F,
                y      : K,
                width  : da,
                height : ba
            };
            e = a + (s.shadow ? 8 : 0);
            if (a || b)
                na ? na.animate(na.crisp(null, null, null, xa - e, wa - e)) : na = G.rect(e / 2, e / 2, xa - e, wa - e, s.borderRadius, a).attr({
                            stroke         : s.borderColor,
                            "stroke-width" : a,
                            fill           : b || Ka
                        }).add().shadow(s.shadow);
            c && (tb ? tb.animate(f) : tb = G.rect(F, K, da, ba, 0).attr({
                        fill : c
                    }).add().shadow(s.plotShadow));
            d && (Ia ? Ia.animate(f) : Ia = G.image(d, F, K, da, ba).add());
            s.plotBorderWidth && (S ? S.animate(S.crisp(null, F, K, da, ba)) : S = G.rect(F, K, da, ba, 0, s.plotBorderWidth).attr({
                        stroke         : s.plotBorderColor,
                        "stroke-width" : s.plotBorderWidth,
                        zIndex         : 4
                    }).add());
            r.isDirtyBox = !1
        };
        ra(ma, "unload", q);
        s.reflow !== !1 && ra(r, "load", o);
        if (E)
            for (Ma in E)
                ra(r, Ma, E[Ma]);
        r.options = a;
        r.series = ia;
        r.addSeries = function(a, b, c) {
            var d;
            a && (j(c, r), b = p(b, !0), ka(r, "addSeries", {
                        options : a
                    }, function() {
                        d = g(a);
                        d.isDirty = !0;
                        r.isDirtyLegend = !0;
                        b && r.redraw()
                    }));
            return d
        };
        r.animation = p(s.animation, !0);
        r.destroy = q;
        r.get = function(a) {
            var b, c, d;
            for (b = 0; b < ya.length; b++)
                if (ya[b].options.id == a)
                    return ya[b];
            for (b = 0; b < ia.length; b++)
                if (ia[b].options.id == a)
                    return ia[b];
            for (b = 0; b < ia.length; b++) {
                d = ia[b].data;
                for (c = 0; c < d.length; c++)
                    if (d[c].id == a)
                        return d[c]
            }
            return null
        };
        r.getSelectedPoints = function() {
            var a = [];
            t(ia, function(b) {
                        a = a.concat(mc(b.data, function(a) {
                                    return a.selected
                                }))
                    });
            return a
        };
        r.getSelectedSeries = function() {
            return mc(ia, function(a) {
                        return a.selected
                    })
        };
        r.hideLoading = function() {
            Yb(sb, {
                        opacity : 0
                    }, {
                        duration : a.loading.hideDuration,
                        complete : function() {
                            D(sb, {
                                        display : Ka
                                    })
                        }
                    });
            ib = !1
        };
        r.isInsidePlot = Ra;
        r.redraw = n;
        r.setSize = jc;
        r.setTitle = h;
        r.showLoading = function(b) {
            var c = a.loading;
            sb || (sb = Q(jb, {
                        className : "jscharting-loading"
                    }, A(c.style, {
                                left    : F + Da,
                                top     : K + Da,
                                width   : da + Da,
                                height  : ba + Da,
                                zIndex  : 10,
                                display : Ka
                            }), L), qb = Q("span", null, c.labelStyle, sb));
            qb.innerHTML = b || a.lang.loading;
            ib || (D(sb, {
                        opacity : 0,
                        display : ""
                    }), Yb(sb, {
                        opacity : c.style.opacity
                    }, {
                        duration : c.showDuration
                    }), ib = !0)
        };
        r.pointCount = 0;
        B()
    }
    function B(a, c, b, d, f) {
        return a + (f - b) / (d - b) * (c - a)
    }
    function x(a, c) {
        return parseInt(a, c || 10)
    }
    function Ha(a, c, b) {
        a /= 255;
        c /= 255;
        b /= 255;
        var d = Math.max(a, c, b), f = Math.min(a, c, b), e, g = (d + f) / 2, k;
        if (d === f)
            e = f = 0;
        else {
            k = d - f;
            f = g > 0.5 ? k / (2 - d - f) : k / (d + f);
            switch (d) {
                case a :
                    e = (c - b) / k + (c < b ? 6 : 0);
                    break;
                case c :
                    e = (b - a) / k + 2;
                    break;
                case b :
                    e = (a - c) / k + 4
            }
            e /= 6
        }
        return [e, f, g]
    }
    function Ya(a, c, b) {
        var d, f;
        if (c === 0)
            b = c = a = b;
        else {
            var e = function(a, b, c) {
                c < 0 && (c += 1);
                c > 1 && (c -= 1);
                if (c < 1 / 6)
                    return a + (b - a) * 6 * c;
                if (c < 0.5)
                    return b;
                if (c < 2 / 3)
                    return a + (b - a) * (2 / 3 - c) * 6;
                return a
            };
            d = b < 0.5 ? b * (1 + c) : b + c - b * c;
            f = 2 * b - d;
            b = e(f, d, a + 1 / 3);
            c = e(f, d, a);
            a = e(f, d, a - 1 / 3)
        }
        return [b * 255, c * 255, a * 255]
    }
    function za(a) {
        var c = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/.exec(a), a = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(a), b;
        c ? b = [x(c[1]), x(c[2]), x(c[3]), parseFloat(c[4], 10)] : a && (b = [x(a[1], 16), x(a[2], 16), x(a[3], 16), 1]);
        return b
    }
    function ta(a) {
        if (a === null)
            return "00";
        a = parseInt(a);
        if (a === 0 || isNaN(a))
            return "00";
        a = Math.max(0, a);
        a = Math.min(a, 255);
        a = Math.round(a);
        return "0123456789ABCDEF".charAt((a - a % 16) / 16) + "0123456789ABCDEF".charAt(a % 16)
    }
    function la(a, c, b) {
        return ta(a) + ta(c) + ta(b)
    }
    function C(a, c) {
        var b = za(a), d, f = b[0];
        d = b[1];
        b = b[2];
        f /= 255;
        d /= 255;
        b /= 255;
        var e = Math.max(f, d, b), g = Math.min(f, d, b), k, n, h = e - g;
        if (e === g)
            k = 0;
        else {
            switch (e) {
                case f :
                    k = (d - b) / h + (d < b ? 6 : 0);
                    break;
                case d :
                    k = (b - f) / h + 2;
                    break;
                case b :
                    k = (f - d) / h + 4
            }
            k /= 6
        }
        d = [k, e === 0 ? 0 : h / e, e];
        b = d[0];
        f = d[1];
        d = d[2] + c;
        var m, j;
        k = Math.floor(b * 6);
        g = b * 6 - k;
        b = d * (1 - f);
        e = d * (1 - g * f);
        f = d * (1 - (1 - g) * f);
        switch (k % 6) {
            case 0 :
                n = d;
                m = f;
                j = b;
                break;
            case 1 :
                n = e;
                m = d;
                j = b;
                break;
            case 2 :
                n = b;
                m = d;
                j = f;
                break;
            case 3 :
                n = b;
                m = e;
                j = d;
                break;
            case 4 :
                n = f;
                m = b;
                j = d;
                break;
            case 5 :
                n = d, m = b, j = e
        }
        n > 1 && (n = 1);
        m > 1 && (m = 1);
        j > 1 && (j = 1);
        return [n * 255, m * 255, j * 255]
    }
    function N(a) {
        a = za(a);
        a = Ha(a[0], a[1], a[2]);
        a[2] -= 0.0070 * Math.pow(a[2] * 100, 2) / 100;
        a = Ya(a[0], a[1], a[2]);
        return "#" + la(a[0], a[1], a[2])
    }
    function R(a) {
        a = za(a);
        a = Ha(a[0], a[1], a[2]);
        a[2] += 0.0070 * Math.pow(a[2] * 100 - 100, 2) / 100;
        a = Ya(a[0], a[1], a[2]);
        return "#" + la(a[0], a[1], a[2])
    }
    function T(a, c, b) {
        var a = za(a), d = za(c), c = x(B(a[0], d[0], 0, 100, b)), f = x(B(a[1], d[1], 0, 100, b)), b = x(B(a[2], d[2], 0, 100, b));
        return "#" + la(c, f, b)
    }
    function tb(a, c, b, d) {
        var f = (c = c ? !0 : !1) ? ["0%", "0%", "0%", "100%"] : ["0%", "0%", "100%", "0%"], c = c ? 0 : 90;
        b && (f = ["100%", "0%", "0%", "0%"], c = 0);
        var e = N(a), a = d ? T(R(a), "#ffffbe", 45) : R(a), d = T(e, a, 0.8), b = T(e, a, 40), g = T(e, "#fefefe", 70.5), k = T(e, d, B(0, 100, 0, 0.5, 0.25)), k = T(k, "#fefefe", 100 / 255 * 100), e = T(e, d, B(0, 100, 0, 0.5, 0.3)), e = T(e, "#fefefe", 100 / 255 * 100), n = T(d, "#fefefe", 110 / 255 * 100);
        return {
            linearGradient : f,
            stops          : [["0%", g], ["25%", k], ["30%", e], ["50%", n], ["50%", d], ["70%", b], ["100%", a]],
            options        : {
                angle    : c,
                focus    : "100%",
                opacity1 : 1,
                opacity2 : 1
            }
        }
    }
    function gb(a, c, b, d) {
        var f = (c = c ? !0 : !1) ? ["0%", "0%", "0%", "100%"] : ["0%", "0%", "100%", "0%"], c = c ? 0 : 90;
        b && (f = ["100%", "0%", "0%", "0%"], c = 0);
        var b = N(a), a = d ? T(R(a), "#ffffbe", 30) : R(a), d = T(b, "#fefefe", B(0, 100, 0, 0.2, 0.05)), e = T(b, a, B(0, 100, 0.05, 0.85, 0.2)), g = T(e, "#fefefe", 160 / 255 * 100), e = T(e, "#fefefe", 140 / 255 * 100), k = T(b, a, B(0, 100, 0.05, 0.85, 0.3)), n = T(k, "#fefefe", 30 / 255 * 100);
        return {
            linearGradient : f,
            stops          : [["0%", b], ["5%", d], ["20%", g], ["20%", e], ["30%", n], ["30%", k], ["85%", a], ["100%", b]],
            options        : {
                angle    : c,
                focus    : "100%",
                opacity1 : 1,
                opacity2 : 1
            }
        }
    }
    function ub(a, c, b) {
        var d = (c = c ? !0 : !1) ? ["0%", "0%", "0%", "100%"] : ["0%", "0%", "100%", "0%"], c = c ? 0 : 90;
        b && (d = ["100%", "0%", "0%", "0%"], c = 0);
        var b = za(a), f = C(a, 0.03), e = C(a, -0.03), a = C(a, -0.06);
        f === b && (a = "#" + la(e[0], e[1], e[2]), f = C(a, -0.03), a = C(a, -0.06));
        f = "#" + la(f[0], f[1], f[2]);
        e = "#" + la(e[0], e[1], e[2]);
        a = "#" + la(a[0], a[1], a[2]);
        b = "#" + la(b[0], b[1], b[2]);
        return {
            linearGradient : d,
            stops          : [["0%", f], ["50%", b], ["90%", e], ["100%", a]],
            options        : {
                angle    : c,
                focus    : "100%",
                opacity1 : 1,
                opacity2 : 1
            }
        }
    }
    function wc(a, c, b, d, f) {
        c = "none";
        if (b.shadingEffectMode)
            c = b.shadingEffectMode;
        if (c === "two") {
            c = a[""];
            c.stroke = N(c.fill);
            c.fill = tb(c.fill, d, f);
            if (c = a.hover)
                c.stroke = T(N(c.fill), c.fill, 50), c.fill = tb(R(R(c.fill)), d, f), c["stroke-width"] = 2;
            if (c = a.select)
                c.stroke = N(c.fill), c.fill = tb(c.fill, d, f, !0)
        }
        else if (c === "three") {
            c = a[""];
            c.stroke = N(c.fill);
            c.fill = gb(c.fill, d, f);
            if (c = a.hover)
                c.stroke = c.fill, c.fill = gb(c.fill, d, f, !0), c["stoke-width"] = 2;
            if (c = a.select)
                c.stroke = N(c.fill), c.fill = gb(c.fill, d, f)
        }
        else if (c === "subtlegradient") {
            c = a[""];
            c.stroke = N(c.fill);
            c.fill = ub(c.fill, d, f);
            if (c = a.hover)
                c.stroke = c.fill, b = c, c = T(R(c.fill), "#ffffbe", 2), c = ub(c, d, f), b.fill = c;
            if (c = a.select)
                c.stroke = N(c.fill), c.fill = gb(c.fill, d, f)
        }
        else {
            c = a[""];
            c.stroke = N(c.fill);
            if (c = a.hover)
                c.stroke = c.fill;
            if (c = a.select)
                c.stroke = N(c.fill)
        }
    }
    function Bc(a, c, b) {
        c = x(3 / b.height * 100);
        return {
            linearGradient : ["0%", "0%", "0%", "100%"],
            stops          : [["0%", a], ["0%", a], [0 + c + "%", "#f0f2fd"], [0 + c + "%", "#ffffff"], [100 - c + "%", "#ffffff"], [100 - c + "%", "#e6e9fd"], ["100%", a], ["100%", a]],
            options        : {
                angle    : 0,
                focus    : "100%",
                opacity1 : 1,
                opacity2 : 1
            }
        }
    }
    var U = document, ma = window, W = Math, J = W.round, Oa = W.floor, dc = W.ceil, ea = W.max, Pa = W.min, ha = W.abs, Y = W.cos, Z = W.sin, Ia = W.PI, Kb = Ia * 2 / 360, Ra = navigator.userAgent, Mb = /msie/i.test(Ra) && !ma.opera, hb = U.documentMode == 8, sc = /AppleWebKit/.test(Ra), uc = /Firefox/.test(Ra), lb = !!U.createElementNS && !!U.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGRect, Wb, eb = "ontouchstart" in U.documentElement, fb, Xb, Lb = {}, Jb = 0, Sa = 1, cb, va, Ub, Nb, pb, ua, jb = "div", Cb = "absolute", tc = "relative", Ua = "hidden", mb = "jscharting-", $a = "visible", Da = "px", Ka = "none", y = "M", w = "L", Zb = "rgba(192,192,192," + (lb ? 1.0E-6 : 0.0020) + ")", Ga = "", Qa = "hover", Ob, $b, ac, bc, Db, Pb, Qb, nc, oc, cc, pc, qc, v = ma.JSChartingAdapter, Ma = v
            || {}, t = Ma.each, mc = Ma.grep, Bb = Ma.map, fa = Ma.merge, xc = Ma.hyphenate, ra = Ma.addEvent, ab = Ma.removeEvent, ka = Ma.fireEvent, Yb = Ma.animate, Ab = Ma.stop, Ta = {};
    Ub = function(a, c, b) {
        function d(a) {
            return a.toString().replace(/^([0-9])$/, "0$1")
        }
        if (!u(c) || isNaN(c))
            return "Invalid date";
        var a = p(a, "%Y-%m-%d %H:%M:%S"), c = new Date(c * Sa), f = c[ac](), e = c[bc](), g = c[Db](), k = c[Pb](), n = c[Qb](), h = va.lang, m = h.weekdays, h = h.months, c = {
            a : m[e].substr(0, 3),
            A : m[e],
            d : d(g),
            e : g,
            b : h[k].substr(0, 3),
            B : h[k],
            m : d(k + 1),
            y : n.toString().substr(2, 2),
            Y : n,
            H : d(f),
            I : d(f % 12 || 12),
            l : f % 12 || 12,
            M : d(c[$b]()),
            p : f < 12 ? "AM" : "PM",
            P : f < 12 ? "am" : "pm",
            S : d(c.getSeconds())
        }, j;
        for (j in c)
            a = a.replace("%" + j, c[j]);
        return b ? a.substr(0, 1).toUpperCase() + a.substr(1) : a
    };
    v && v.init && v.init();
    if (!v && ma.jQuery) {
        var ga = jQuery, t = function(a, c) {
            for (var b = 0, d = a.length; b < d; b++)
                if (c.call(a[b], a[b], b, a) === !1)
                    return b
        }, mc = ga.grep, Bb = function(a, c) {
            for (var b = [], d = 0, f = a.length; d < f; d++)
                b[d] = c.call(a[d], a[d], d, a);
            return b
        }, fa = function() {
            var a = arguments;
            return ga.extend(!0, null, a[0], a[1], a[2], a[3])
        }, xc = function(a) {
            return a.replace(/([A-Z])/g, function(a, b) {
                        return "-" + b.toLowerCase()
                    })
        }, ra = function(a, c, b) {
            ga(a).bind(c, b)
        }, ab = function(a, c, b) {
            var d = U.removeEventListener ? "removeEventListener" : "detachEvent";
            U[d] && !a[d] && (a[d] = function() {
            });
            ga(a).unbind(c, b)
        }, ka = function(a, c, b, d) {
            var f = ga.Event(c), e = "detached" + c;
            A(f, b);
            a[c] && (a[e] = a[c], a[c] = null);
            ga(a).trigger(f);
            a[e] && (a[c] = a[e], a[e] = null);
            d && !f.isDefaultPrevented() && d(f)
        }, Yb = function(a, c, b) {
            var d = ga(a);
            if (c.d)
                a.toD = c.d, c.d = 1;
            d.stop();
            d.animate(c, b)
        }, Ab = function(a) {
            ga(a).stop()
        };
        ga.extend(ga.easing, {
                    easeOutQuad : function(a, c, b, d, f) {
                        return -d * (c /= f) * (c - 2) + b
                    }
                });
        var Cc = jQuery.fx.step._default, Dc = jQuery.fx.prototype.cur;
        ga.fx.step._default = function(a) {
            var c = a.elem;
            c.attr ? c.attr(a.prop, a.now) : Cc.apply(this, arguments)
        };
        ga.fx.step.d = function(a) {
            var c = a.elem;
            if (!a.started) {
                var b = pb.init(c, c.d, c.toD);
                a.start = b[0];
                a.end = b[1];
                a.started = !0
            }
            c.attr("d", pb.step(a.start, a.end, a.pos, c.toD))
        };
        ga.fx.prototype.cur = function() {
            var a = this.elem;
            return a.attr ? a.attr(this.prop) : Dc.apply(this, arguments)
        }
    }
    pb = {
        init : function(a, c, b) {
            var c = c || "", d = a.shift, f = c.indexOf("C") > -1, e = f ? 7 : 3, g, c = c.split(" "), b = [].concat(b), k, n, h = function(a) {
                for (g = a.length; g--;)
                    a[g] == y && a.splice(g + 1, 0, a[g + 1], a[g + 2], a[g + 1], a[g + 2])
            };
            f && (h(c), h(b));
            a.isArea && (k = c.splice(c.length - 6, 6), n = b.splice(b.length - 6, 6));
            if (d)
                b = [].concat(b).splice(0, e).concat(b), a.shift = !1;
            if (c.length)
                for (a = b.length; c.length < a;)
                    d = [].concat(c).splice(c.length - e, e), f && (d[e - 6] = d[e - 2], d[e - 5] = d[e - 1]), c = c.concat(d);
            k && (c = c.concat(k), b = b.concat(n));
            return [c, b]
        },
        step : function(a, c, b, d) {
            var f = [], e = a.length;
            if (b == 1)
                f = d;
            else if (e == c.length && b < 1)
                for (; e--;)
                    d = parseFloat(a[e]), f[e] = isNaN(d) ? a[e] : b * parseFloat(c[e] - d) + d;
            else
                f = c;
            return f
        }
    };
    v = {
        enabled : !0,
        align   : "center",
        x       : 0,
        y       : 15,
        style   : {
            color         : "#666",
            "font-size"   : "11px",
            "line-height" : "14px"
        }
    };
    va = {
        colors      : ["#4572A7", "#AA4643", "#89A54E", "#80699B", "#3D96AE", "#DB843D", "#92A8CD", "#A47D7C", "#B5CA92"],
        symbols     : ["circle", "diamond", "square", "triangle", "triangle-down"],
        lang        : {
            loading        : "Loading...",
            months         : ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            weekdays       : ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            decimalPoint   : ".",
            resetZoom      : "Reset zoom",
            resetZoomTitle : "Reset zoom level 1:1",
            thousandsSep   : ","
        },
        global      : {
            useUTC : !0
        },
        chart       : {
            borderColor        : "#4572A7",
            borderRadius       : 5,
            defaultSeriesType  : "line",
            ignoreHiddenSeries : !0,
            spacingTop         : 10,
            spacingRight       : 10,
            spacingBottom      : 15,
            spacingLeft        : 10,
            style              : {
                fontFamily : '"Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif',
                fontSize   : "12px"
            },
            backgroundColor    : "#FFFFFF",
            plotBorderColor    : "#C0C0C0"
        },
        title       : {
            text  : "Chart title",
            align : "center",
            y     : 15,
            style : {
                color    : "#3E576F",
                fontSize : "16px"
            }
        },
        subtitle    : {
            text  : "",
            align : "center",
            y     : 30,
            style : {
                color : "#6D869F"
            }
        },
        plotOptions : {
            line : {
                allowPointSelect : !1,
                showCheckbox     : !1,
                animation        : {
                    duration : 1E3
                },
                events           : {},
                lineWidth        : 2,
                shadow           : !0,
                marker           : {
                    enabled   : !0,
                    lineWidth : 0,
                    radius    : 4,
                    lineColor : "#FFFFFF",
                    states    : {
                        hover  : {},
                        select : {
                            fillColor : "#FFFFFF",
                            lineColor : "#000000",
                            lineWidth : 2
                        }
                    }
                },
                point            : {
                    events : {}
                },
                dataLabels       : fa(v, {
                            enabled   : !1,
                            y         : -6,
                            formatter : function() {
                                return this.y
                            }
                        }),
                showInLegend     : !0,
                states           : {
                    hover  : {
                        marker : {}
                    },
                    select : {
                        marker : {}
                    }
                },
                stickyTracking   : !0
            }
        },
        labels      : {
            style : {
                position : Cb,
                color    : "#3E576F"
            }
        },
        legend      : {
            enabled           : !0,
            align             : "center",
            layout            : "horizontal",
            labelFormatter    : function() {
                return this.name
            },
            borderWidth       : 1,
            borderColor       : "#909090",
            borderRadius      : 5,
            shadow            : !1,
            style             : {
                padding : "5px"
            },
            itemStyle         : {
                cursor : "pointer",
                color  : "#3E576F"
            },
            itemHoverStyle    : {
                cursor : "pointer",
                color  : "#000000"
            },
            itemHiddenStyle   : {
                color : "#C0C0C0"
            },
            itemCheckboxStyle : {
                position : Cb,
                width    : "13px",
                height   : "13px"
            },
            symbolWidth       : 16,
            symbolPadding     : 5,
            verticalAlign     : "bottom",
            x                 : 0,
            y                 : 0
        },
        loading     : {
            hideDuration : 100,
            labelStyle   : {
                fontWeight : "bold",
                position   : tc,
                top        : "1em"
            },
            showDuration : 100,
            style        : {
                position        : Cb,
                backgroundColor : "white",
                opacity         : 0.5,
                textAlign       : "center"
            }
        },
        tooltip     : {
            enabled         : !0,
            backgroundColor : "rgba(255, 255, 255, .85)",
            borderWidth     : 2,
            borderRadius    : 5,
            shadow          : !0,
            snap            : eb ? 25 : 10,
            style           : {
                color      : "#333333",
                fontSize   : "12px",
                padding    : "5px",
                whiteSpace : "nowrap"
            }
        },
        toolbar     : {
            itemStyle : {
                color  : "#4572A7",
                cursor : "pointer"
            }
        },
        credits     : {
            enabled  : !0,
            text     : "JSCharting.com",
            href     : "http://www.highcharts.com",
            position : {
                align         : "right",
                x             : -10,
                verticalAlign : "bottom",
                y             : -5
            },
            style    : {
                cursor   : "pointer",
                color    : "#909090",
                fontSize : "10px"
            }
        }
    };
    var Tb = {
        dateTimeLabelFormats : {
            second : "%H:%M:%S",
            minute : "%H:%M",
            hour   : "%H:%M",
            day    : "%e. %b",
            week   : "%e. %b",
            month  : "%b '%y",
            year   : "%Y"
        },
        endOnTick            : !1,
        gridLineColor        : "#C0C0C0",
        labels               : v,
        lineColor            : "#C0D0E0",
        lineWidth            : 1,
        max                  : null,
        min                  : null,
        minPadding           : 0.01,
        maxPadding           : 0.01,
        minorGridLineColor   : "#E0E0E0",
        minorGridLineWidth   : 1,
        minorTickColor       : "#A0A0A0",
        minorTickLength      : 2,
        minorTickPosition    : "outside",
        startOfWeek          : 1,
        startOnTick          : !1,
        tickColor            : "#C0D0E0",
        tickLength           : 5,
        tickmarkPlacement    : "between",
        tickPixelInterval    : 100,
        tickPosition         : "outside",
        tickWidth            : 1,
        title                : {
            align : "middle",
            style : {
                color      : "#6D869F",
                fontWeight : "bold"
            }
        },
        type                 : "linear"
    }, ec = fa(Tb, {
                endOnTick         : !0,
                gridLineWidth     : 1,
                tickPixelInterval : 72,
                showLastLabel     : !0,
                labels            : {
                    align : "right",
                    x     : -8,
                    y     : 3
                },
                lineWidth         : 0,
                maxPadding        : 0.05,
                minPadding        : 0.05,
                startOnTick       : !0,
                tickWidth         : 0,
                title             : {
                    rotation : 270,
                    text     : "Y-values"
                }
            }), Ac = {
        labels : {
            align : "right",
            x     : -8,
            y     : null
        },
        title  : {
            rotation : 270
        }
    }, zc = {
        labels : {
            align : "left",
            x     : 8,
            y     : null
        },
        title  : {
            rotation : 90
        }
    }, rc = {
        labels : {
            align : "center",
            x     : 0,
            y     : 14
        },
        title  : {
            rotation : 0
        }
    }, yc = fa(rc, {
                labels : {
                    y : -5
                }
            }), na = va.plotOptions, v = na.line;
    na.spline = fa(v);
    na.scatter = fa(v, {
                lineWidth : 0,
                states    : {
                    hover : {
                        lineWidth : 0
                    }
                }
            });
    na.area = fa(v, {});
    na.areaspline = fa(na.area);
    na.column = fa(v, {
                borderColor    : "#FFFFFF",
                borderWidth    : 1,
                borderRadius   : 0,
                groupPadding   : 0.2,
                marker         : null,
                pointPadding   : 0.1,
                minPointLength : 0,
                states         : {
                    hover  : {
                        brightness : 0.1,
                        shadow     : !1
                    },
                    select : {
                        color       : "#C0C0C0",
                        borderColor : "#000000",
                        shadow      : !1
                    }
                }
            });
    na.bar = fa(na.column, {
                dataLabels : {
                    align : "left",
                    x     : 5,
                    y     : 0
                }
            });
    na.pie = fa(v, {
                borderColor  : "#FFFFFF",
                borderWidth  : 1,
                center       : ["50%", "50%"],
                colorByPoint : !0,
                dataLabels   : {
                    distance  : 30,
                    enabled   : !0,
                    formatter : function() {
                        return this.point.name
                    },
                    y         : 5
                },
                legendType   : "point",
                marker       : null,
                size         : "75%",
                showInLegend : !1,
                slicedOffset : 10,
                states       : {
                    hover : {
                        brightness : 0.1,
                        shadow     : !1
                    }
                }
            });
    h();
    var ca = function(a) {
        var c = [], b;
        (function(a) {
            if (b = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/.exec(a))
                c = [x(b[1]), x(b[2]), x(b[3]), parseFloat(b[4], 10)];
            else if (b = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(a))
                c = [x(b[1], 16), x(b[2], 16), x(b[3], 16), 1]
        })(a);
        return {
            get        : function(b) {
                return c && !isNaN(c[0]) ? b == "rgb" ? "rgb(" + c[0] + "," + c[1] + "," + c[2] + ")" : b == "a" ? c[3] : "rgba(" + c.join(",") + ")" : a
            },
            brighten   : function(a) {
                if (S(a) && a !== 0) {
                    var b;
                    for (b = 0; b < 3; b++)
                        c[b] += x(a * 255), c[b] < 0 && (c[b] = 0), c[b] > 255 && (c[b] = 255)
                }
                return this
            },
            setOpacity : function(a) {
                c[3] = a;
                return this
            }
        }
    };
    oa.prototype = {
        init            : function(a, c) {
            this.element = U.createElementNS("http://www.w3.org/2000/svg", c);
            this.renderer = a
        },
        animate         : function(a, c, b) {
            if (c = p(c, Nb, !0)) {
                c = fa(c);
                if (b)
                    c.complete = b;
                Yb(this, a, c)
            }
            else
                this.attr(a), b && b()
        },
        attr            : function(a, c) {
            var b, d, f, e, g = this.element, k = g.nodeName, n = this.renderer, h, m = this.shadows, j, o = this;
            Wa(a) && u(c) && (b = a, a = {}, a[b] = c);
            if (Wa(a))
                b = a, k == "circle" ? b = {
                    x : "cx",
                    y : "cy"
                }[b] || b : b == "strokeWidth" && (b = "stroke-width"), o = I(g, b) || this[b] || 0, b != "d" && b != "visibility" && (o = parseFloat(o));
            else
                for (b in a) {
                    h = !1;
                    d = a[b];
                    if (b == "d")
                        d && d.join && (d = d.join(" ")), /(NaN| {2}|^$)/.test(d) && (d = "M 0 0"), this.d = d;
                    else if (b == "x" && k == "text") {
                        for (f = 0; f < g.childNodes.length; f++)
                            e = g.childNodes[f], I(e, "x") == I(g, "x") && I(e, "x", d);
                        this.rotation && I(g, "transform", "rotate(" + this.rotation + " " + d + " " + x(a.y || I(g, "y")) + ")")
                    }
                    else if (b == "fill")
                        d = n.color(d, g, b);
                    else if (k == "circle" && (b == "x" || b == "y"))
                        b = {
                            x : "cx",
                            y : "cy"
                        }[b] || b;
                    else if (b == "translateX" || b == "translateY" || b == "rotation" || b == "verticalAlign")
                        this[b] = d, this.updateTransform(), h = !0;
                    else if (b == "stroke")
                        d = n.color(d, g, b);
                    else if (b == "dashstyle")
                        if (b = "stroke-dasharray", d = d && d.toLowerCase(), d == "solid")
                            d = Ka;
                        else {
                            if (d) {
                                d = d.replace("shortdashdotdot", "3,1,1,1,1,1,").replace("shortdashdot", "3,1,1,1").replace("shortdot", "1,1,").replace("shortdash", "3,1,").replace("longdash", "8,3,").replace(/dot/g, "1,3,").replace("dash", "4,3,").replace(/,$/, "").split(",");
                                for (f = d.length; f--;)
                                    d[f] = x(d[f]) * a["stroke-width"];
                                d = d.join(",")
                            }
                        }
                    else
                        b == "isTracker" ? this[b] = d : b == "width" ? d = x(d) : b == "align" && (b = "text-anchor", d = {
                            left   : "start",
                            center : "middle",
                            right  : "end"
                        }[d]);
                    b == "strokeWidth" && (b = "stroke-width");
                    sc && b == "stroke-width" && d === 0 && (d = 1.0E-6);
                    this.symbolName && /^(x|y|r|start|end|innerR)/.test(b) && (j || (this.symbolAttr(a), j = !0), h = !0);
                    if (m && /^(width|height|visibility|x|y|d)$/.test(b))
                        for (f = m.length; f--;)
                            I(m[f], b, d);
                    if ((b == "width" || b == "height") && k == "rect" && d < 0)
                        d = 0;
                    b == "text" ? (this.textStr = d, this.added && n.buildText(this)) : h || I(g, b, d)
                }
            return o
        },
        symbolAttr      : function(a) {
            var c = this;
            t(["x", "y", "r", "start", "end", "width", "height", "innerR"], function(b) {
                        c[b] = p(a[b], c[b])
                    });
            c.attr({
                        d : c.renderer.symbols[c.symbolName](c.x, c.y, c.r, {
                                    start  : c.start,
                                    end    : c.end,
                                    width  : c.width,
                                    height : c.height,
                                    innerR : c.innerR
                                })
                    })
        },
        clip            : function(a) {
            return this.attr("clip-path", "url(" + this.renderer.url + "#" + a.id + ")")
        },
        crisp           : function(a, c, b, d, f) {
            var e, g = {}, k = {}, n, a = a || this.strokeWidth || 0;
            n = a % 2 / 2;
            k.x = Oa(c || this.x || 0) + n;
            k.y = Oa(b || this.y || 0) + n;
            k.width = Oa((d || this.width || 0) - 2 * n);
            k.height = Oa((f || this.height || 0) - 2 * n);
            k.strokeWidth = a;
            for (e in k)
                this[e] != k[e] && (this[e] = g[e] = k[e]);
            return g
        },
        css             : function(a) {
            var c = this.element, c = a && a.width && c.nodeName == "text", b = a, d;
            if (u(a))
                for (d in a = {}, b)
                    a[xc(d)] = b[d];
            if (a && a.color)
                a.fill = a.color;
            this.styles = a = A(this.styles, a);
            Mb && !lb ? (c && delete a.width, D(this.element, a)) : this.attr({
                        style : Ja(a)
                    });
            c && this.added && this.renderer.buildText(this);
            return this
        },
        on              : function(a, c) {
            var b = c;
            eb && a == "click" && (a = "touchstart", b = function(a) {
                a.preventDefault();
                c()
            });
            this.element["on" + a] = b;
            return this
        },
        translate       : function(a, c) {
            return this.attr({
                        translateX : a,
                        translateY : c
                    })
        },
        invert          : function() {
            this.inverted = !0;
            this.updateTransform();
            return this
        },
        updateTransform : function() {
            var a = this.translateX || 0, c = this.translateY || 0, b = this.inverted, d = this.rotation, f = [];
            b && (a += this.attr("width"), c += this.attr("height"));
            (a || c) && f.push("translate(" + a + "," + c + ")");
            b ? f.push("rotate(90) scale(-1,1)") : d && f.push("rotate(" + d + " " + this.x + " " + this.y + ")");
            f.length && I(this.element, "transform", f.join(" "))
        },
        toFront         : function() {
            var a = this.element;
            a.parentNode.appendChild(a);
            return this
        },
        align           : function(a, c, b) {
            a ? (this.alignOptions = a, this.alignByTranslate = c, b || this.renderer.alignedObjects.push(this)) : (a = this.alignOptions, c = this.alignByTranslate);
            var b = p(b, this.renderer), d = a.align, f = a.verticalAlign, e = (b.x || 0) + (a.x || 0), g = (b.y || 0) + (a.y || 0), k = {};
            /^(right|center)$/.test(d) && (e += (b.width - (a.width || 0)) / {
                right  : 1,
                center : 2
            }[d]);
            k[c ? "translateX" : "x"] = J(e);
            /^(bottom|middle)$/.test(f) && (g += (b.height - (a.height || 0)) / ({
                bottom : 1,
                middle : 2
            }[f] || 1));
            k[c ? "translateY" : "y"] = J(g);
            this[this.placed ? "animate" : "attr"](k);
            this.placed = !0;
            this.alignAttr = k;
            return this
        },
        getBBox         : function() {
            var a, c, b, d = this.rotation, f = d * Kb;
            try {
                a = A({}, this.element.getBBox())
            }
            catch (e) {
                a = {
                    width  : 0,
                    height : 0
                }
            }
            c = a.width;
            b = a.height;
            if (d)
                a.width = ha(b * Z(f)) + ha(c * Y(f)), a.height = ha(b * Y(f)) + ha(c * Z(f));
            return a
        },
        show            : function() {
            return this.attr({
                        visibility : $a
                    })
        },
        hide            : function() {
            return this.attr({
                        visibility : Ua
                    })
        },
        add             : function(a) {
            var c = this.renderer, b = a || c, d = b.element || c.box, f = d.childNodes, e = this.element, g = I(e, "zIndex");
            if (c.forExport && e.outerHTML) {
                var k = e.outerHTML.indexOf("/images/brandLogo.png");
                if (k > 0)
                    return
            }
            if (c.forExport && e.href && e.href.baseVal && (k = e.href.baseVal.indexOf("/images/brandLogo.png"), k > 0))
                return;
            this.parentInverted = a && a.inverted;
            this.textStr !== void 0 && c.buildText(this);
            if (g)
                b.handleZ = !0, g = x(g);
            if (b.handleZ)
                for (b = 0; b < f.length; b++)
                    if (a = f[b], c = I(a, "zIndex"), a != e && (x(c) > g || !u(g) && u(c)))
                        return d.insertBefore(e, a), this;
            d.appendChild(e);
            this.added = !0;
            return this
        },
        destroy         : function() {
            var a = this.element || {}, c = this.shadows, b = a.parentNode, d;
            a.onclick = a.onmouseout = a.onmouseover = a.onmousemove = null;
            Ab(this);
            b && b.removeChild(a);
            c && t(c, function(a) {
                        (b = a.parentNode) && b.removeChild(a)
                    });
            Za(this.renderer.alignedObjects, this);
            for (d in this)
                delete this[d];
            return null
        },
        empty           : function() {
            for (var a = this.element, c = a.childNodes, b = c.length; b--;)
                a.removeChild(c[b])
        },
        shadow          : function(a, c) {
            var b = [], d, f, e = this.element, g = this.parentInverted ? "(-1,-1)" : "(1,1)";
            if (a) {
                for (d = 1; d <= 3; d++)
                    f = e.cloneNode(0), I(f, {
                                isShadow         : "true",
                                stroke           : "rgb(0, 0, 0)",
                                "stroke-opacity" : 0.03 * d,
                                "stroke-width"   : 7 - 2 * d,
                                transform        : "translate" + g,
                                fill             : Ka
                            }), c ? c.element.appendChild(f) : e.parentNode.insertBefore(f, e), b.push(f);
                this.shadows = b
            }
            return this
        }
    };
    var rb = function() {
        this.init.apply(this, arguments)
    };
    rb.prototype = {
        Element       : oa,
        init          : function(a, c, b, d) {
            var f = location, e;
            e = this.createElement("svg").attr({
                        xmlns   : "http://www.w3.org/2000/svg",
                        version : "1.1"
                    });
            a.appendChild(e.element);
            this.box = e.element;
            this.boxWrapper = e;
            this.alignedObjects = [];
            this.url = Mb ? "" : f.href.replace(/#.*?$/, "");
            this.defs = this.createElement("defs").add();
            this.forExport = d;
            this.setSize(c, b, !1);
            a = this.createElement("linearGradient").attr({
                        id : mb + Jb++,
                        x1 : "0%",
                        y1 : "0%",
                        x2 : "0%",
                        y2 : "100%"
                    }).add(this.defs);
            this.createElement("stop").attr({
                        offset         : Ab[0],
                        "stop-color"   : "#" + va.dnc_color,
                        "stop-opacity" : "0.33"
                    }).add(a)
        },
        createElement : function(a) {
            var c = new this.Element;
            c.init(this, a);
            return c
        },
        buildText     : function(a) {
            for (var c = a.element, b = p(a.textStr, "").toString().replace(/<(b|strong)>/g, '<span style="font-weight:bold">').replace(/<(i|em)>/g, '<span style="font-style:italic">').replace(/<a/g, "<span").replace(/<\/(b|strong|i|em|a)>/g, "</span>").split(/<br.*?>/g), d = c.childNodes, f = /style="([^"]+)"/, e = /href="([^"]+)"/, g = I(c, "x"), k = a.styles, n = uc && k && k["-hc-direction"] == "rtl" && !this.forExport && x(Ra.split("Firefox/")[1] < 4), h, m = k && x(k.width), j = k && k["line-height"], o, l = d.length; l--;)
                c.removeChild(d[l]);
            m && !a.added && this.box.appendChild(c);
            t(b, function(b, d) {
                        var k, p = 0, u, b = b.replace(/<span/g, "|||<span").replace(/<\/span>/g, "</span>|||");
                        k = b.split("|||");
                        t(k, function(b) {
                                    if (b !== "" || k.length == 1) {
                                        var t = {}, q = U.createElementNS("http://www.w3.org/2000/svg", "tspan");
                                        f.test(b) && I(q, "style", b.match(f)[1].replace(/(;| |^)color([ :])/, "$1fill$2"));
                                        e.test(b) && (I(q, "onclick", 'location.href="' + b.match(e)[1] + '"'), D(q, {
                                                    cursor : "pointer"
                                                }));
                                        b = b.replace(/<(.|\n)*?>/g, "") || " ";
                                        if (n) {
                                            h = [];
                                            for (l = b.length; l--;)
                                                h.push(b.charAt(l));
                                            b = h.join("")
                                        }
                                        q.appendChild(U.createTextNode(b));
                                        p ? t.dx = 3 : t.x = g;
                                        if (!p) {
                                            if (d) {
                                                !lb && a.renderer.forExport && D(q, {
                                                            display : "block"
                                                        });
                                                u = ma.getComputedStyle && ma.getComputedStyle(o, null).getPropertyValue("line-height");
                                                if (!u || isNaN(u))
                                                    u = j || o.offsetHeight || 18;
                                                I(q, "dy", u)
                                            }
                                            o = q
                                        }
                                        I(q, t);
                                        c.appendChild(q);
                                        p++;
                                        if (m)
                                            for (var b = b.replace(/-/g, "- ").split(" "), w, v = []; b.length || v.length;)
                                                w = c.getBBox().width, t = w > m, !t || b.length == 1 ? (b = v, v = [], b.length && (q = U.createElementNS("http://www.w3.org/2000/svg", "tspan"), I(q, {
                                                            dy : j || 16,
                                                            x  : g
                                                        }), c.appendChild(q), w > m && (m = w))) : (q.removeChild(q.firstChild), v.unshift(b.pop())), b.length && q.appendChild(U.createTextNode(b.join(" ").replace(/- /g, "-")))
                                    }
                                })
                    })
        },
        crispLine     : function(a, c) {
            a[1] == a[4] && (a[1] = a[4] = J(a[1]) + c % 2 / 2);
            a[2] == a[5] && (a[2] = a[5] = J(a[2]) + c % 2 / 2);
            return a
        },
        path          : function(a) {
            return this.createElement("path").attr({
                        d    : a,
                        fill : Ka
                    })
        },
        circle        : function(a, c, b) {
            a = X(a) ? a : {
                x : a,
                y : c,
                r : b
            };
            return this.createElement("circle").attr(a)
        },
        arc           : function(a, c, b, d, f, e) {
            if (X(a))
                c = a.y, b = a.r, d = a.innerR, f = a.start, e = a.end, a = a.x;
            return this.symbol("arc", a || 0, c || 0, b || 0, {
                        innerR : d || 0,
                        start  : f || 0,
                        end    : e || 0
                    })
        },
        rect          : function(a, c, b, d, f, e) {
            if (X(a))
                c = a.y, b = a.width, d = a.height, f = a.r, a = a.x;
            f = this.createElement("rect").attr({
                        rx   : f,
                        ry   : f,
                        fill : Ka
                    });
            return f.attr(f.crisp(e, a, c, ea(b, 0), ea(d, 0)))
        },
        setSize       : function(a, c, b) {
            var d = this.alignedObjects, f = d.length;
            this.width = a;
            this.height = c;
            for (this.boxWrapper[p(b, !0) ? "animate" : "attr"]({
                        width  : a,
                        height : c
                    }); f--;)
                d[f].align()
        },
        g             : function(a) {
            return this.createElement("g").attr(u(a) && {
                "class" : mb + a
            })
        },
        image         : function(a, c, b, d, f) {
            var e = {
                preserveAspectRatio : Ka
            };
            arguments.length > 1 && A(e, {
                        x      : c,
                        y      : b,
                        width  : d,
                        height : f
                    });
            e = this.createElement("image").attr(e);
            e.element.setAttributeNS ? e.element.setAttributeNS("http://www.w3.org/1999/xlink", "href", a) : e.element.setAttribute("hc-svg-href", a);
            return e
        },
        symbol        : function(a, c, b, d, f) {
            var e, g = this.symbols[a], g = g && g(c, b, d, f), k = /^url\((.*?)\)$/, n;
            if (g)
                e = this.path(g), A(e, {
                            symbolName : a,
                            x          : c,
                            y          : b,
                            r          : d
                        }), f && A(e, f);
            else if (k.test(a)) {
                var h = function(a, b) {
                    a.attr({
                                width  : b[0],
                                height : b[1]
                            }).translate(-J(b[0] / 2), -J(b[1] / 2))
                };
                n = a.match(k)[1];
                a = Lb[n];
                e = this.image(n).attr({
                            x : c,
                            y : b
                        });
                a ? h(e, a) : (e.attr({
                            width  : 0,
                            height : 0
                        }), Q("img", {
                            onload : function() {
                                h(e, Lb[n] = [this.width, this.height])
                            },
                            src    : n
                        }))
            }
            else
                e = this.circle(c, b, d);
            return e
        },
        symbols       : {
            square          : function(a, c, b) {
                b *= 0.707;
                return [y, a - b, c - b, w, a + b, c - b, a + b, c + b, a - b, c + b, "Z"]
            },
            triangle        : function(a, c, b) {
                return [y, a, c - 1.33 * b, w, a + b, c + 0.67 * b, a - b, c + 0.67 * b, "Z"]
            },
            "triangle-down" : function(a, c, b) {
                return [y, a, c + 1.33 * b, w, a - b, c - 0.67 * b, a + b, c - 0.67 * b, "Z"]
            },
            diamond         : function(a, c, b) {
                return [y, a, c - b, w, a + b, c, a, c + b, a - b, c, "Z"]
            },
            arc             : function(a, c, b, d) {
                var f = d.start, e = d.end - 1.0E-6, g = d.innerR, k = Y(f), n = Z(f), h = Y(e), e = Z(e), d = d.end - f < Ia ? 0 : 1;
                return [y, a + b * k, c + b * n, "A", b, b, 0, d, 1, a + b * h, c + b * e, w, a + g * h, c + g * e, "A", g, g, 0, d, 0, a + g * k, c + g * n, "Z"]
            }
        },
        clipRect      : function(a, c, b, d) {
            var f = mb + Jb++, e = this.createElement("clipPath").attr({
                        id : f
                    }).add(this.defs), a = this.rect(a, c, b, d, 0).add(e);
            a.id = f;
            return a
        },
        color         : function(a, c, b) {
            var d, f = /^rgba/;
            if (a && a.linearGradient) {
                var e = this, c = a.linearGradient, b = mb + Jb++, g, k, n;
                g = e.createElement("linearGradient").attr({
                            id            : b,
                            gradientUnits : "userSpaceOnUse",
                            x1            : c[0],
                            y1            : c[1],
                            x2            : c[2],
                            y2            : c[3]
                        }).add(e.defs);
                t(a.stops, function(a) {
                            f.test(a[1]) ? (d = ca(a[1]), k = d.get("rgb"), n = d.get("a")) : (k = a[1], n = 1);
                            e.createElement("stop").attr({
                                        offset         : a[0],
                                        "stop-color"   : k,
                                        "stop-opacity" : n
                                    }).add(g)
                        });
                return "url(" + this.url + "#" + b + ")"
            }
            else
                return f.test(a) ? (d = ca(a), I(c, b + "-opacity", d.get("a")), d.get("rgb")) : a
        },
        text          : function(a, c, b) {
            var d = va.chart.style, c = J(p(c, 0)), b = J(p(b, 0)), a = this.createElement("text").attr({
                        x    : c,
                        y    : b,
                        text : a
                    }).css({
                        "font-family" : d.fontFamily,
                        "font-size"   : d.fontSize
                    });
            a.x = c;
            a.y = b;
            return a
        }
    };
    Wb = rb;
    var Va;
    if (!lb)
        v = $(oa, {
                    init            : function(a, c) {
                        var b = ["<", c, ' filled="f" stroked="f"'], d = ["position: ", Cb, ";"];
                        (c == "shape" || c == jb) && d.push("left:0;top:0;width:10px;height:10px;");
                        hb && d.push("visibility: ", c == jb ? Ua : $a);
                        b.push(' style="', d.join(""), '"/>');
                        if (c)
                            b = c == jb || c == "span" || c == "img" ? b.join("") : a.prepVML(b), this.element = Q(b);
                        this.renderer = a
                    },
                    add             : function(a) {
                        var c = this.renderer, b = this.element, d = c.box, f = a && a.inverted, a = a ? a.element || a : d;
                        if (!(c.forExport && b.href && b.href.indexOf("/images/brandLogo.png") > 0))
                            return f && c.invertChild(b, a), hb && a.gVis == Ua && D(b, {
                                        visibility : Ua
                                    }), a.appendChild(b), this.added = !0, this.alignOnAdd && this.updateTransform(), this
                    },
                    attr            : function(a, c) {
                        var b, d, f, e = this.element || {}, g = e.style, k = e.nodeName, n = this.renderer, h = this.symbolName, m, j, o = this.shadows, l = this;
                        Wa(a) && u(c) && (b = a, a = {}, a[b] = c);
                        if (Wa(a))
                            b = a, l = b == "strokeWidth" || b == "stroke-width" ? this.strokeweight : this[b];
                        else
                            for (b in a) {
                                d = a[b];
                                m = !1;
                                if (h && /^(x|y|r|start|end|width|height|innerR)/.test(b))
                                    j || (this.symbolAttr(a), j = !0), m = !0;
                                else if (b == "d") {
                                    d = d || [];
                                    this.d = d.join(" ");
                                    f = d.length;
                                    for (m = []; f--;)
                                        m[f] = S(d[f]) ? J(d[f] * 10) - 5 : d[f] == "Z" ? "x" : d[f];
                                    d = m.join(" ") || "x";
                                    e.path = d;
                                    if (o)
                                        for (f = o.length; f--;)
                                            o[f].path = d;
                                    m = !0
                                }
                                else if (b == "zIndex" || b == "visibility") {
                                    if (hb && b == "visibility" && k == "DIV") {
                                        e.gVis = d;
                                        m = e.childNodes;
                                        for (f = m.length; f--;)
                                            D(m[f], {
                                                        visibility : d
                                                    });
                                        d == $a && (d = null)
                                    }
                                    d && (g[b] = d);
                                    m = !0
                                }
                                else if (/^(width|height)$/.test(b))
                                    this.updateClipping ? (this[b] = d, this.updateClipping()) : g[b] = d, m = !0;
                                else if (/^(x|y)$/.test(b))
                                    this[b] = d, e.tagName == "SPAN" ? this.updateTransform() : g[{
                                        x : "left",
                                        y : "top"
                                    }[b]] = d;
                                else if (b == "class")
                                    e.className = d;
                                else if (b == "stroke")
                                    d = n.color(d, e, b), b = "strokecolor";
                                else if (b == "stroke-width" || b == "strokeWidth")
                                    e.stroked = d ? !0 : !1, b = "strokeweight", this[b] = d, S(d) && (d += Da);
                                else if (b == "dashstyle")
                                    (e.getElementsByTagName("stroke")[0] || Q(n.prepVML(["<stroke/>"]), null, null, e))[b] = d || "solid", this.dashstyle = d, m = !0;
                                else if (b == "fill")
                                    k == "SPAN" ? g.color = d : (e.filled = d != Ka ? !0 : !1, d = n.color(d, e, b), b = "fillcolor");
                                else if (b == "translateX" || b == "translateY" || b == "rotation" || b == "align")
                                    b == "align" && (b = "textAlign"), this[b] = d, this.updateTransform(), m = !0;
                                else if (b == "text")
                                    this.bBox = null, e.innerHTML = d, m = !0;
                                if (o && b == "visibility")
                                    for (f = o.length; f--;)
                                        o[f].style[b] = d;
                                m || (hb ? e[b] = d : I(e, b, d))
                            }
                        return l
                    },
                    clip            : function(a) {
                        var c = this, b = a.members;
                        b.push(c);
                        c.destroyClip = function() {
                            Za(b, c)
                        };
                        return c.css(a.getCSS(c.inverted))
                    },
                    css             : function(a) {
                        var c = this.element;
                        if (c = a && c.tagName == "SPAN" && a.width)
                            delete a.width, this.textWidth = c, this.updateTransform();
                        this.styles = A(this.styles, a);
                        D(this.element, a);
                        return this
                    },
                    destroy         : function() {
                        this.destroyClip && this.destroyClip();
                        oa.prototype.destroy.apply(this)
                    },
                    empty           : function() {
                        for (var a = this.element.childNodes, c = a.length, b; c--;)
                            b = a[c], b.parentNode.removeChild(b)
                    },
                    getBBox         : function() {
                        var a = this.element, c = this.bBox;
                        if (!c) {
                            if (a.nodeName == "text")
                                a.style.position = Cb;
                            c = this.bBox = {
                                x      : a.offsetLeft,
                                y      : a.offsetTop,
                                width  : a.offsetWidth,
                                height : a.offsetHeight
                            }
                        }
                        return c
                    },
                    on              : function(a, c) {
                        this.element["on" + a] = function() {
                            var a = ma.event;
                            a.target = a.srcElement;
                            c(a)
                        };
                        return this
                    },
                    updateTransform : function() {
                        if (this.added) {
                            var a = this, c = a.element, b = a.translateX || 0, d = a.translateY || 0, f = a.x || 0, e = a.y || 0, g = a.textAlign || "left", k = {
                                left   : 0,
                                center : 0.5,
                                right  : 1
                            }[g], n = g && g != "left";
                            (b || d) && a.css({
                                        marginLeft : b,
                                        marginTop  : d
                                    });
                            a.inverted && t(c.childNodes, function(b) {
                                        a.renderer.invertChild(b, c)
                                    });
                            if (c.tagName == "SPAN") {
                                var h, m, b = a.rotation, j;
                                h = 0;
                                var d = 1, o = 0, l;
                                j = x(a.textWidth);
                                var p = a.xCorr || 0, q = a.yCorr || 0, s = [b, g, c.innerHTML, a.textWidth].join(",");
                                if (s != a.cTT)
                                    u(b) && (h = b * Kb, d = Y(h), o = Z(h), D(c, {
                                                filter : b ? ["progid:DXImageTransform.Microsoft.Matrix(M11=", d, ", M12=", -o, ", M21=", o, ", M22=", d, ", sizingMethod='auto expand')"].join("") : Ka
                                            })), h = c.offsetWidth, m = c.offsetHeight, h > j && (D(c, {
                                                width      : j + Da,
                                                display    : "block",
                                                whiteSpace : "normal"
                                            }), h = j), j = J((x(c.style.fontSize) || 12) * 1.2), p = d < 0 && -h, q = o < 0 && -m, l = d * o < 0, p += o * j * (l ? 1 - k : k), q -= d * j * (b ? l ? k : 1 - k : 1), n && (p -= h * k * (d < 0 ? -1 : 1), b && (q -= m * k * (o < 0 ? -1 : 1)), D(c, {
                                                textAlign : g
                                            })), a.xCorr = p, a.yCorr = q;
                                D(c, {
                                            left : f + p,
                                            top  : e + q
                                        });
                                a.cTT = s
                            }
                        }
                        else
                            this.alignOnAdd = !0
                    },
                    shadow          : function(a, c) {
                        var b = [], d, f = this.element, e = this.renderer, g, k = f.style, n, h = f.path;
                        "" + f.path === "" && (h = "x");
                        if (a) {
                            for (d = 1; d <= 3; d++)
                                n = ['<shape isShadow="true" strokeweight="', 7 - 2 * d, '" filled="false" path="', h, '" coordsize="100,100" style="', f.style.cssText, '" />'], g = Q(e.prepVML(n), null, {
                                            left : x(k.left) + 1,
                                            top  : x(k.top) + 1
                                        }), n = ['<stroke color="black" opacity="', 0.03 * d, '"/>'], Q(e.prepVML(n), null, null, g), c ? c.element.appendChild(g) : f.parentNode.insertBefore(g, f), b.push(g);
                            this.shadows = b
                        }
                        return this
                    }
                }), Va = function() {
            this.init.apply(this, arguments)
        }, Va.prototype = fa(rb.prototype, {
                    Element     : v,
                    isIE8       : Ra.indexOf("MSIE 8.0") > -1,
                    init        : function(a, c, b) {
                        var d;
                        this.alignedObjects = [];
                        d = this.createElement(jb);
                        a.appendChild(d.element);
                        this.box = d.element;
                        this.boxWrapper = d;
                        this.setSize(c, b, !1);
                        if (!U.namespaces.hcv)
                            U.namespaces.add("hcv", "urn:schemas-microsoft-com:vml"), U.createStyleSheet().cssText = "hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "
                    },
                    clipRect    : function(a, c, b, d) {
                        var f = this.createElement();
                        return A(f, {
                                    members        : [],
                                    left           : a,
                                    top            : c,
                                    width          : b,
                                    height         : d,
                                    getCSS         : function(a) {
                                        var b = this.top, c = this.left, d = c + this.width, f = b + this.height, b = {
                                            clip : "rect(" + J(a ? c : b) + "px," + J(a ? f : d) + "px," + J(a ? d : f) + "px," + J(a ? b : c) + "px)"
                                        };
                                        !a && hb && A(b, {
                                                    width  : d + Da,
                                                    height : f + Da
                                                });
                                        return b
                                    },
                                    updateClipping : function() {
                                        t(f.members, function(a) {
                                                    a.css(f.getCSS(a.inverted))
                                                })
                                    }
                                })
                    },
                    color       : function(a, c, b) {
                        var d, f = /^rgba/;
                        if (a && a.linearGradient) {
                            var e, g, k = a.linearGradient, n, h, m, j;
                            t(a.stops, function(a, b) {
                                        f.test(a[1]) ? (d = ca(a[1]), e = d.get("rgb"), g = d.get("a")) : (e = a[1], g = 1);
                                        b ? (m = e, j = g) : (n = e, h = g)
                                    });
                            a = 90 - W.atan((k[3] - k[1]) / (k[2] - k[0])) * 180 / Ia;
                            b = ["<", b, ' colors="0% ', n, ",100% ", m, '" angle="', a, '" opacity="', j, '" o:opacity2="', h, '" type="gradient" focus="100%" />'];
                            Q(this.prepVML(b), null, null, c)
                        }
                        else
                            return f.test(a) && c.tagName != "IMG" ? (d = ca(a), b = ["<", b, ' opacity="', d.get("a"), '"/>'], Q(this.prepVML(b), null, null, c), d.get("rgb")) : a
                    },
                    prepVML     : function(a) {
                        var c = this.isIE8, a = a.join("");
                        c ? (a = a.replace("/>", ' xmlns="urn:schemas-microsoft-com:vml" />'), a = a.indexOf('style="') == -1 ? a.replace("/>", ' style="display:inline-block;behavior:url(#default#VML);" />') : a.replace('style="', 'style="display:inline-block;behavior:url(#default#VML);')) : a = a.replace("<", "<hcv:");
                        return a
                    },
                    text        : function(a, c, b) {
                        var d = va.chart.style;
                        return this.createElement("span").attr({
                                    text : a,
                                    x    : J(c),
                                    y    : J(b)
                                }).css({
                                    whiteSpace : "nowrap",
                                    fontFamily : d.fontFamily,
                                    fontSize   : d.fontSize
                                })
                    },
                    path        : function(a) {
                        return this.createElement("shape").attr({
                                    coordsize : "100 100",
                                    d         : a
                                })
                    },
                    circle      : function(a, c, b) {
                        return this.symbol("circle").attr({
                                    x : a,
                                    y : c,
                                    r : b
                                })
                    },
                    g           : function(a) {
                        var c;
                        a && (c = {
                            className : mb + a,
                            "class"   : mb + a
                        });
                        return this.createElement(jb).attr(c)
                    },
                    image       : function(a, c, b, d, f) {
                        var e = this.createElement("img").attr({
                                    src : a
                                });
                        arguments.length > 1 && e.css({
                                    left   : c,
                                    top    : b,
                                    width  : d,
                                    height : f
                                });
                        return e
                    },
                    rect        : function(a, c, b, d, f, e) {
                        if (X(a))
                            c = a.y, b = a.width, d = a.height, f = a.r, a = a.x;
                        var g = this.symbol("rect");
                        g.r = f;
                        return g.attr(g.crisp(e, a, c, ea(b, 0), ea(d, 0)))
                    },
                    invertChild : function(a, c) {
                        var b = c.style;
                        D(a, {
                                    flip     : "x",
                                    left     : x(b.width) - 10,
                                    top      : x(b.height) - 10,
                                    rotation : -90
                                })
                    },
                    symbols     : {
                        arc    : function(a, c, b, d) {
                            var f = d.start, e = d.end, g = Y(f), k = Z(f), h = Y(e), j = Z(e), d = d.innerR, m = 0.07 / b, l = d && 0.1 / d || 0;
                            if (e - f === 0)
                                return ["x"];
                            else
                                2 * Ia - e + f < m ? h = -m : e - f < l && (h = Y(f + l));
                            return ["wa", a - b, c - b, a + b, c + b, a + b * g, c + b * k, a + b * h, c + b * j, "at", a - d, c - d, a + d, c + d, a + d * h, c + d * j, a + d * g, c + d * k, "x", "e"]
                        },
                        circle : function(a, c, b) {
                            return ["wa", a - b, c - b, a + b, c + b, a + b, c, a + b, c, "e"]
                        },
                        rect   : function(a, c, b, d) {
                            if (!u(d))
                                return [];
                            var f = d.width, d = d.height, e = a + f, g = c + d, b = Pa(b, f, d);
                            return [y, a + b, c, w, e - b, c, "wa", e - 2 * b, c, e, c + 2 * b, e - b, c, e, c + b, w, e, g - b, "wa", e - 2 * b, g - 2 * b, e, g, e, g - b, e - b, g, w, a + b, g, "wa", a, g - 2 * b, a + 2 * b, g, a + b, g, a, g - b, w, a, c + b, "wa", a, c, a + 2 * b, c + 2 * b, a, c + b, a + b, c, "x", "e"]
                        }
                    }
                }), Wb = Va;
    q.prototype.callbacks = [];
    var qb = function() {
    };
    qb.prototype = {
        init             : function(a, c) {
            var b;
            this.series = a;
            this.applyOptions(c);
            this.pointAttr = {};
            if (a.options.colorByPoint) {
                b = a.chart.options.colors;
                if (!this.options)
                    this.options = {};
                this.color = this.options.color = this.color || b[fb++];
                fb >= b.length && (fb = 0)
            }
            a.chart.pointCount++;
            return this
        },
        applyOptions     : function(a) {
            var c = this.series;
            this.config = a;
            if (S(a) || a === null)
                this.y = a;
            else if (X(a) && !S(a.length))
                A(this, a), this.options = a;
            else if (Wa(a[0]))
                this.name = a[0], this.y = a[1];
            else if (S(a[0]))
                this.x = a[0], this.y = a[1];
            if (this.x === ua)
                this.x = c.autoIncrement()
        },
        destroy          : function() {
            var a = this, c = a.series, b;
            c.chart.pointCount--;
            if (a == c.chart.hoverPoint)
                a.onMouseOut();
            c.chart.hoverPoints = null;
            ab(a);
            t(["graphic", "tracker", "group", "dataLabel", "connector"], function(b) {
                        a[b] && a[b].destroy()
                    });
            a.legendItem && a.series.chart.legend.destroyItem(a);
            for (b in a)
                a[b] = null
        },
        getLabelConfig   : function() {
            return {
                x          : this.category,
                y          : this.y,
                series     : this.series,
                point      : this,
                percentage : this.percentage,
                total      : this.total || this.stackTotal
            }
        },
        select           : function(a, c) {
            var b = this, d = b.series.chart;
            b.selected = a = p(a, !b.selected);
            b.firePointEvent(a ? "select" : "unselect");
            b.setState(a && "select");
            c || t(d.getSelectedPoints(), function(a) {
                        if (a.selected && a != b)
                            a.selected = !1, a.setState(Ga), a.firePointEvent("unselect")
                    })
        },
        onMouseOver      : function() {
            var a = this.series.chart, c = a.tooltip, b = a.hoverPoint;
            if (b && b != this)
                b.onMouseOut();
            this.firePointEvent("mouseOver");
            c && !c.shared && c.refresh(this);
            this.setState(Qa);
            a.hoverPoint = this
        },
        onMouseOut       : function() {
            this.firePointEvent("mouseOut");
            this.setState();
            this.series.chart.hoverPoint = null
        },
        tooltipFormatter : function(a) {
            var c = this.series;
            return ['<span style="color:' + c.color + '">', this.name || c.name, "</span>: ", !a ? "<b>x = " + (this.name || this.x) + ",</b> " : "", "<b>", !a ? "y = " : "", this.y, "</b>"].join("")
        },
        update           : function(a, c, b) {
            var d = this, f = d.series, e = d.graphic, g = f.chart, c = p(c, !0);
            d.firePointEvent("update", {
                        options : a
                    }, function() {
                        d.applyOptions(a);
                        X(a) && (f.getAttribs(), e && e.attr(d.pointAttr[f.state]));
                        f.isDirty = !0;
                        c && g.redraw(b)
                    })
        },
        remove           : function(a, c) {
            var b = this, d = b.series, f = d.chart, e = d.data;
            j(c, f);
            a = p(a, !0);
            b.firePointEvent("remove", null, function() {
                        Za(e, b);
                        b.destroy();
                        d.isDirty = !0;
                        a && f.redraw()
                    })
        },
        firePointEvent   : function(a, c, b) {
            var d = this, f = this.series.options;
            (f.point.events[a] || d.options && d.options.events && d.options.events[a]) && this.importEvents();
            a == "click" && f.allowPointSelect && (b = function(a) {
                d.select(null, a.ctrlKey || a.metaKey || a.shiftKey)
            });
            ka(this, a, c, b)
        },
        importEvents     : function() {
            if (!this.hasImportedEvents) {
                var a = fa(this.series.options.point, this.options).events, c;
                this.events = a;
                for (c in a)
                    ra(this, c, a[c]);
                this.hasImportedEvents = !0
            }
        },
        setState         : function(a) {
            var c = this.series, b = c.options.states, d = na[c.type].marker && c.options.marker, f = d && !d.enabled, e = (d = d && d.states[a]) && d.enabled === !1, g = c.stateMarkerGraphic, k = c.chart, h = this.pointAttr;
            a || (a = Ga);
            if (!(a == this.state || this.selected && a != "select" || b[a] && b[a].enabled === !1 || a && (e || f && !d.enabled))) {
                if (this.graphic)
                    this.graphic.attr(h[a]);
                else {
                    if (a) {
                        if (!g)
                            c.stateMarkerGraphic = g = k.renderer.circle(0, 0, h[a].r).attr(h[a]).add(c.group);
                        g.translate(this.plotX, this.plotY)
                    }
                    if (g)
                        g[a ? "show" : "hide"]()
                }
                this.state = a
            }
        }
    };
    var Ea = function() {
    };
    Ea.prototype = {
        isCartesian        : !0,
        type               : "line",
        pointClass         : qb,
        pointAttrToOptions : {
            stroke         : "lineColor",
            "stroke-width" : "lineWidth",
            fill           : "fillColor",
            r              : "radius"
        },
        init               : function(a, c) {
            var b, d;
            d = a.series.length;
            this.chart = a;
            c = this.setOptions(c);
            A(this, {
                        index     : d,
                        options   : c,
                        name      : c.name || "Series " + (d + 1),
                        state     : Ga,
                        pointAttr : {},
                        visible   : c.visible !== !1,
                        selected  : c.selected === !0
                    });
            d = c.events;
            for (b in d)
                ra(this, b, d[b]);
            if (d && d.click || c.point && c.point.events && c.point.events.click || c.allowPointSelect)
                a.runTrackerClick = !0;
            this.getColor();
            this.getSymbol();
            this.setData(c.data, !1)
        },
        autoIncrement      : function() {
            var a = this.options, c = this.xIncrement, c = p(c, a.pointStart, 0);
            this.pointInterval = p(this.pointInterval, a.pointInterval, 1);
            this.xIncrement = c + this.pointInterval;
            return c
        },
        cleanData          : function() {
            var a = this.chart, c = this.data, b, d, f = a.smallestInterval, e, g;
            c.sort(function(a, b) {
                        return a.x - b.x
                    });
            if (this.options.connectNulls)
                for (g = c.length - 1; g >= 0; g--)
                    c[g].y === null && c[g - 1] && c[g + 1] && c.splice(g, 1);
            for (g = c.length - 1; g >= 0; g--)
                if (c[g - 1] && (e = c[g].x - c[g - 1].x, e > 0 && (d === ua || e < d)))
                    d = e, b = g;
            if (f === ua || d < f)
                a.smallestInterval = d;
            this.closestPoints = b
        },
        getSegments        : function() {
            var a = -1, c = [], b = this.data;
            t(b, function(d, f) {
                        d.y === null ? (f > a + 1 && c.push(b.slice(a + 1, f)), a = f) : f == b.length - 1 && c.push(b.slice(a + 1, f + 1))
                    });
            this.segments = c
        },
        setOptions         : function(a) {
            var c = this.chart.options.plotOptions;
            return fa(c[this.type], c.series, a)
        },
        getColor           : function() {
            var a = this.chart.options.colors;
            this.color = this.options.color || a[fb++] || "#0000ff";
            fb >= a.length && (fb = 0)
        },
        getSymbol          : function() {
            var a = this.chart.options.symbols;
            this.symbol = this.options.marker.symbol || a[Xb++];
            Xb >= a.length && (Xb = 0)
        },
        addPoint           : function(a, c, b, d) {
            var f = this.data, e = this.graph, g = this.area, k = this.chart, a = (new this.pointClass).init(this, a);
            j(d, k);
            if (e && b)
                e.shift = b;
            if (g)
                g.shift = b, g.isArea = !0;
            c = p(c, !0);
            f.push(a);
            b && f[0].remove(!1);
            this.getAttribs();
            this.isDirty = !0;
            c && k.redraw()
        },
        setData            : function(a, c) {
            var b = this, d = b.data, f = b.initialColor, e = b.chart, g = d && d.length || 0;
            b.xIncrement = null;
            u(f) && (fb = f);
            for (a = Bb(Na(a || []), function(a) {
                        return (new b.pointClass).init(b, a)
                    }); g--;)
                d[g].destroy();
            b.data = a;
            b.cleanData();
            b.getSegments();
            b.getAttribs();
            b.isDirty = !0;
            e.isDirtyBox = !0;
            p(c, !0) && e.redraw(!1)
        },
        remove             : function(a, c) {
            var b = this, d = b.chart, a = p(a, !0);
            if (!b.isRemoving)
                b.isRemoving = !0, ka(b, "remove", null, function() {
                            b.destroy();
                            d.isDirtyLegend = d.isDirtyBox = !0;
                            a && d.redraw(c)
                        });
            b.isRemoving = !1
        },
        translate          : function() {
            for (var a = this.chart, c = this.options.stacking, b = this.xAxis.categories, d = this.yAxis, f = this.data, e = f.length; e--;) {
                var g = f[e], k = g.x, h = g.y, j = g.low, m = d.stacks[(h < 0 ? "-" : "") + this.stackKey];
                g.plotX = this.xAxis.translate(k);
                if (c && this.visible && m && m[k])
                    j = m[k], k = j.total, j.cum = j = j.cum - h, h = j + h, c == "percent" && (j = k ? j * 100 / k : 0, h = k ? h * 100 / k : 0), g.percentage = k ? g.y * 100 / k : 0, g.stackTotal = k;
                if (u(j))
                    g.yBottom = d.translate(j, 0, 1, 0, 1);
                if (h !== null)
                    g.plotY = d.translate(h, 0, 1, 0, 1);
                g.clientX = a.inverted ? a.plotHeight - g.plotX : g.plotX;
                g.category = b && b[g.x] !== ua ? b[g.x] : g.x
            }
        },
        setTooltipPoints   : function(a) {
            var c = this.chart, b = c.inverted, d = [], f = J((b ? c.plotTop : c.plotLeft) + c.plotSizeX), e, g, k = [];
            if (a)
                this.tooltipPoints = null;
            t(this.segments, function(a) {
                        d = d.concat(a)
                    });
            this.xAxis && this.xAxis.reversed && (d = d.reverse());
            t(d, function(a, c) {
                        e = d[c - 1] ? d[c - 1]._high + 1 : 0;
                        for (g = a._high = d[c + 1] ? Oa((a.plotX + (d[c + 1] ? d[c + 1].plotX : f)) / 2) : f; e <= g;)
                            k[b ? f - e++ : e++] = a
                    });
            this.tooltipPoints = k
        },
        onMouseOver        : function() {
            var a = this.chart, c = a.hoverSeries;
            if (eb || !a.mouseIsDown) {
                if (c && c != this)
                    c.onMouseOut();
                this.options.events.mouseOver && ka(this, "mouseOver");
                this.tracker && this.tracker.toFront();
                this.setState(Qa);
                a.hoverSeries = this
            }
        },
        onMouseOut         : function() {
            var a = this.options, c = this.chart, b = c.tooltip, d = c.hoverPoint;
            if (d)
                d.onMouseOut();
            this && a.events.mouseOut && ka(this, "mouseOut");
            b && !a.stickyTracking && b.hide();
            this.setState();
            c.hoverSeries = null
        },
        animate            : function(a) {
            var c = this.chart, b = this.clipRect, d = this.options.animation;
            d && !X(d) && (d = {});
            if (a) {
                if (!b.isAnimating)
                    b.attr("width", 0), b.isAnimating = !0
            }
            else
                b.animate({
                            width : c.plotSizeX
                        }, d), this.animate = null
        },
        drawPoints         : function() {
            var a, c = this.data, b = this.chart, d, f, e, g, k, h;
            if (this.options.marker.enabled)
                for (e = c.length; e--;)
                    if (g = c[e], d = g.plotX, f = g.plotY, h = g.graphic, f !== ua && !isNaN(f))
                        a = g.pointAttr[g.selected ? "select" : Ga], k = a.r, h ? h.animate({
                                    x : d,
                                    y : f,
                                    r : k
                                }) : g.graphic = b.renderer.symbol(p(g.marker && g.marker.symbol, this.symbol), d, f, k).attr(a).add(this.group)
        },
        convertAttribs     : function(a, c, b, d) {
            var f = this.pointAttrToOptions, e, g, k = {}, a = a || {}, c = c || {}, b = b || {}, d = d || {};
            for (e in f)
                g = f[e], k[e] = p(a[g], c[e], b[e], d[e]);
            return k
        },
        getAttribs         : function() {
            var a = this, c = na[a.type].marker ? a.options.marker : a.options, b = c.states, d = b[Qa], f, e = a.color, g = {
                stroke : e,
                fill   : e
            }, k = a.data, h = [], j, m = a.pointAttrToOptions;
            a.options.marker ? (d.radius = d.radius || c.radius + 2, d.lineWidth = d.lineWidth || c.lineWidth + 1) : d.color = d.color || ca(d.color || e).brighten(d.brightness).get();
            h[Ga] = a.convertAttribs(c, g);
            t([Qa, "select"], function(c) {
                        h[c] = a.convertAttribs(b[c], h[Ga])
                    });
            a.pointAttr = h;
            for (e = k.length; e--;) {
                g = k[e];
                if ((c = g.options && g.options.marker || g.options) && c.enabled === !1)
                    c.radius = 0;
                f = !1;
                if (g.options)
                    for (var l in m)
                        u(c[m[l]]) && (f = !0);
                if (f) {
                    j = [];
                    b = c.states || {};
                    f = b[Qa] = b[Qa] || {};
                    if (!a.options.marker)
                        f.color = ca(f.color || g.options.color).brighten(f.brightness || d.brightness).get();
                    j[Ga] = a.convertAttribs(c, h[Ga]);
                    j[Qa] = a.convertAttribs(b[Qa], h[Qa], j[Ga]);
                    j.select = a.convertAttribs(b.select, h.select, j[Ga])
                }
                else
                    j = h;
                g.pointAttr = j
            }
        },
        destroy            : function() {
            var a = this, c = a.chart, b = /\/5[0-9\.]+ (Safari|Mobile)\//.test(Ra), d, f;
            ab(a);
            a.legendItem && a.chart.legend.destroyItem(a);
            t(a.data, function(a) {
                        a.destroy()
                    });
            t(["area", "graph", "dataLabelsGroup", "group", "tracker"], function(c) {
                        a[c] && (d = b && c == "group" ? "hide" : "destroy", a[c][d]())
                    });
            if (c.hoverSeries == a)
                c.hoverSeries = null;
            Za(c.series, a);
            for (f in a)
                delete a[f]
        },
        drawDataLabels     : function() {
            if (this.options.dataLabels.enabled) {
                var a, c, b = this.data, d = this.options.dataLabels, f, e = this.dataLabelsGroup, g = this.chart, k = g.inverted, h = this.type, j;
                if (!e)
                    e = this.dataLabelsGroup = g.renderer.g("data-labels").attr({
                                visibility : this.visible ? $a : Ua,
                                zIndex     : 6
                            }).translate(g.plotLeft, g.plotTop).add();
                j = d.color;
                j == "auto" && (j = null);
                d.style.color = p(j, this.color);
                t(b, function(b) {
                            var j = b.barX, j = j && j + b.barW / 2 || b.plotX || -999, o = p(b.plotY, -999), l = b.dataLabel, z = d.align;
                            f = d.formatter.call(b.getLabelConfig());
                            a = (k ? g.plotWidth - o : j) + d.x;
                            c = (k ? g.plotHeight - j : o) + d.y;
                            h == "column" && (a += {
                                left  : -1,
                                right : 1
                            }[z] * b.barW / 2 || 0);
                            if (l)
                                l.attr({
                                            text : f
                                        }).animate({
                                            x : a,
                                            y : c
                                        });
                            else if (u(f))
                                l = b.dataLabel = g.renderer.text(f, a, c).attr({
                                            align    : z,
                                            rotation : d.rotation,
                                            zIndex   : 1
                                        }).css(d.style).add(e);
                            k && !d.y && l.attr({
                                        y : c + parseInt(l.styles["line-height"]) * 0.9 - l.getBBox().height / 2
                                    })
                        })
            }
        },
        drawGraph          : function() {
            var a = this, c = a.options, b = a.graph, d = [], f, e = a.area, g = a.group, k = c.lineColor || a.color, h = c.lineWidth, j = c.dashStyle, m, l = a.chart.renderer, o = a.yAxis.getThreshold(c.threshold || 0), q = /^area/.test(a.type), u = [], v = [];
            t(a.segments, function(b) {
                        m = [];
                        t(b, function(d, e) {
                                    a.getPointSpline ? m.push.apply(m, a.getPointSpline(b, d, e)) : (m.push(e ? w : y), e && c.step && m.push(d.plotX, b[e - 1].plotY), m.push(d.plotX, d.plotY))
                                });
                        b.length > 1 ? d = d.concat(m) : u.push(b[0]);
                        if (q) {
                            var e = [], f, g = m.length;
                            for (f = 0; f < g; f++)
                                e.push(m[f]);
                            g == 3 && e.push(w, m[1], m[2]);
                            if (c.stacking && a.type != "areaspline")
                                for (f = b.length - 1; f >= 0; f--)
                                    e.push(b[f].plotX, b[f].yBottom);
                            else
                                e.push(w, b[b.length - 1].plotX, o, w, b[0].plotX, o);
                            v = v.concat(e)
                        }
                    });
            a.graphPath = d;
            a.singlePoints = u;
            if (q)
                f = p(c.fillColor, ca(a.color).setOpacity(c.fillOpacity || 0.75).get()), e ? e.animate({
                            d : v
                        }) : a.area = a.chart.renderer.path(v).attr({
                            fill : f
                        }).add(g);
            if (b)
                b.animate({
                            d : d
                        });
            else if (h) {
                b = {
                    stroke         : k,
                    "stroke-width" : h
                };
                if (j)
                    b.dashstyle = j;
                a.graph = l.path(d).attr(b).add(g).shadow(c.shadow)
            }
        },
        render             : function() {
            var a = this, c = a.chart, b, d, f = a.options, e = f.animation, g = e && a.animate, e = g ? e && e.duration || 500 : 0, k = a.clipRect;
            d = c.renderer;
            if (!k && (k = a.clipRect = !c.hasRendered && c.clipRect ? c.clipRect : d.clipRect(0, 0, c.plotSizeX, c.plotSizeY), !c.clipRect))
                c.clipRect = k;
            if (!a.group)
                b = a.group = d.g("series"), c.inverted && (d = function() {
                    b.attr({
                                width  : c.plotWidth,
                                height : c.plotHeight
                            }).invert()
                }, d(), ra(c, "resize", d)), b.clip(a.clipRect).attr({
                            visibility : a.visible ? $a : Ua,
                            zIndex     : f.zIndex
                        }).translate(c.plotLeft, c.plotTop).add(c.seriesGroup);
            a.drawDataLabels();
            g && a.animate(!0);
            a.drawGraph && a.drawGraph();
            a.drawPoints();
            a.options.enableMouseTracking !== !1 && a.drawTracker();
            g && a.animate();
            setTimeout(function() {
                        k.isAnimating = !1;
                        if ((b = a.group) && k != c.clipRect && k.renderer)
                            b.clip(a.clipRect = c.clipRect), k.destroy()
                    }, e);
            a.isDirty = !1
        },
        redraw             : function() {
            var a = this.chart, c = this.group;
            c && (a.inverted && c.attr({
                        width  : a.plotWidth,
                        height : a.plotHeight
                    }), c.animate({
                        translateX : a.plotLeft,
                        translateY : a.plotTop
                    }));
            this.translate();
            this.setTooltipPoints(!0);
            this.render()
        },
        setState           : function(a) {
            var c = this.options, b = this.graph, d = c.states, c = c.lineWidth, a = a || Ga;
            if (this.state != a)
                this.state = a, d[a] && d[a].enabled === !1 || (a && (c = d[a].lineWidth || c + 1), b && !b.dashstyle && b.attr({
                            "stroke-width" : c
                        }, a ? 0 : 500))
        },
        setVisible         : function(a, c) {
            var b = this.chart, d = this.legendItem, f = this.group, e = this.tracker, g = this.dataLabelsGroup, k, h = this.data, j = b.options.chart.ignoreHiddenSeries;
            k = this.visible;
            k = (this.visible = a = a === ua ? !k : a) ? "show" : "hide";
            if (f)
                f[k]();
            if (e)
                e[k]();
            else
                for (f = h.length; f--;)
                    if (e = h[f], e.tracker)
                        e.tracker[k]();
            if (g)
                g[k]();
            d && b.legend.colorizeItem(this, a);
            this.isDirty = !0;
            this.options.stacking && t(b.series, function(a) {
                        if (a.options.stacking && a.visible)
                            a.isDirty = !0
                    });
            if (j)
                b.isDirtyBox = !0;
            c !== !1 && b.redraw();
            ka(this, k)
        },
        show               : function() {
            this.setVisible(!0)
        },
        hide               : function() {
            this.setVisible(!1)
        },
        select             : function(a) {
            this.selected = a = a === ua ? !this.selected : a;
            if (this.checkbox)
                this.checkbox.checked = a;
            ka(this, a ? "select" : "unselect")
        },
        drawTracker        : function() {
            var a = this, c = a.options, b = [].concat(a.graphPath), d = b.length, f = a.chart, e = f.options.tooltip.snap, g = a.tracker, k = c.cursor, k = k && {
                cursor : k
            }, h = a.singlePoints, j;
            if (d)
                for (j = d + 1; j--;)
                    b[j] == y && b.splice(j + 1, 0, b[j + 1] - e, b[j + 2], w), (j && b[j] == y || j == d) && b.splice(j, 0, w, b[j - 2] + e, b[j - 1]);
            for (j = 0; j < h.length; j++)
                d = h[j], b.push(y, d.plotX - e, d.plotY, w, d.plotX + e, d.plotY);
            g ? g.attr({
                        d : b
                    }) : a.tracker = f.renderer.path(b).attr({
                        isTracker      : !0,
                        stroke         : Zb,
                        fill           : Ka,
                        "stroke-width" : c.lineWidth + 2 * e,
                        visibility     : a.visible ? $a : Ua,
                        zIndex         : 1
                    }).on(eb ? "touchstart" : "mouseover", function() {
                        if (f.hoverSeries != a)
                            a.onMouseOver()
                    }).on("mouseout", function() {
                        if (!c.stickyTracking)
                            a.onMouseOut()
                    }).css(k).add(f.trackerGroup)
        }
    };
    v = $(Ea);
    Ta.line = v;
    v = $(Ea, {
                type : "area"
            });
    Ta.area = v;
    v = $(Ea, {
                type           : "spline",
                getPointSpline : function(a, c, b) {
                    var d = c.plotX, f = c.plotY, e = a[b - 1], g = a[b + 1], k, h, j, m;
                    if (b && b < a.length - 1) {
                        a = e.plotY;
                        j = g.plotX;
                        var g = g.plotY, l;
                        k = (1.5 * d + e.plotX) / 2.5;
                        h = (1.5 * f + a) / 2.5;
                        j = (1.5 * d + j) / 2.5;
                        m = (1.5 * f + g) / 2.5;
                        l = (m - h) * (j - d) / (j - k) + f - m;
                        h += l;
                        m += l;
                        h > a && h > f ? (h = ea(a, f), m = 2 * f - h) : h < a && h < f && (h = Pa(a, f), m = 2 * f - h);
                        m > g && m > f ? (m = ea(g, f), h = 2 * f - m) : m < g && m < f && (m = Pa(g, f), h = 2 * f - m);
                        c.rightContX = j;
                        c.rightContY = m
                    }
                    b ? (c = ["C", e.rightContX || e.plotX, e.rightContY || e.plotY, k || d, h || f, d, f], e.rightContX = e.rightContY = null) : c = [y, d, f];
                    return c
                }
            });
    Ta.spline = v;
    v = $(v, {
                type : "areaspline"
            });
    Ta.areaspline = v;
    var ib = $(Ea, {
                type               : "column",
                pointAttrToOptions : {
                    stroke         : "borderColor",
                    "stroke-width" : "borderWidth",
                    fill           : "color",
                    r              : "borderRadius"
                },
                init               : function() {
                    Ea.prototype.init.apply(this, arguments);
                    var a = this, c = a.chart;
                    c.hasColumn = !0;
                    c.hasRendered && t(c.series, function(b) {
                                if (b.type == a.type)
                                    b.isDirty = !0
                            })
                },
                translate          : function() {
                    var a = this, c = a.chart, b = 0, d = a.xAxis.reversed, f = a.xAxis.categories, e = {}, g, h;
                    Ea.prototype.translate.apply(a);
                    t(c.series, function(c) {
                                if (c.type == a.type && c.visible)
                                    c.options.stacking ? (g = c.stackKey, e[g] === ua && (e[g] = b++), h = e[g]) : h = b++, c.columnIndex = h
                            });
                    var j = a.options, l = a.data, m = a.closestPoints, c = ha(l[1] ? l[m].plotX - l[m - 1].plotX : c.plotSizeX / (f ? f.length : 1)), f = c * j.groupPadding, m = (c - 2 * f) / b, q = j.pointWidth, o = u(q) ? (m - q) / 2 : m * j.pointPadding, w = ea(p(q, m - 2 * o), 1), v = o + (f + ((d ? b - a.columnIndex : a.columnIndex) || 0) * m - c / 2) * (d ? -1 : 1), y = a.yAxis.getThreshold(j.threshold || 0), s = p(j.minPointLength, 5);
                    t(l, function(a) {
                                var b = a.plotY, c = a.yBottom || y, d = a.plotX + v, e = dc(Pa(b, c)), f = dc(ea(b, c) - e), g;
                                ha(f) < s && (s && (f = s, e = ha(e - y) > s ? c - s : y - (b <= y ? s : 0)), g = e - 3);
                                A(a, {
                                            barX : d,
                                            barY : e,
                                            barW : w,
                                            barH : f
                                        });
                                a.shapeType = "rect";
                                a.shapeArgs = {
                                    x      : d,
                                    y      : e,
                                    width  : w,
                                    height : f,
                                    r      : j.borderRadius
                                };
                                a.trackerArgs = u(g) && fa(a.shapeArgs, {
                                            height : ea(6, f + 3),
                                            y      : g
                                        })
                            })
                },
                getSymbol          : function() {
                },
                drawGraph          : function() {
                },
                drawPoints         : function() {
                    var a = this, c = a.options, b = a.chart.renderer, d, f;
                    t(a.data, function(e) {
                                var g = e.plotY;
                                if (g !== ua && !isNaN(g) && e.y !== null)
                                    d = e.graphic, f = e.shapeArgs, d ? (Ab(d), d.animate(f)) : e.graphic = b[e.shapeType](f).attr(e.pointAttr[e.selected ? "select" : Ga]).add(a.group).shadow(c.shadow)
                            })
                },
                drawTracker        : function() {
                    var a = this, c = a.chart, b = c.renderer, d, f, e = +new Date, g = a.options.cursor, h = g && {
                        cursor : g
                    }, j;
                    t(a.data, function(g) {
                                f = g.tracker;
                                d = g.trackerArgs || g.shapeArgs;
                                if (g.y !== null)
                                    f ? f.attr(d) : g.tracker = b[g.shapeType](d).attr({
                                                isTracker  : e,
                                                fill       : Zb,
                                                visibility : a.visible ? $a : Ua,
                                                zIndex     : 1
                                            }).on(eb ? "touchstart" : "mouseover", function(b) {
                                                j = b.relatedTarget || b.fromElement;
                                                if (c.hoverSeries != a && I(j, "isTracker") != e)
                                                    a.onMouseOver();
                                                g.onMouseOver()
                                            }).on("mouseout", function(b) {
                                                if (!a.options.stickyTracking && (j = b.relatedTarget || b.toElement, I(j, "isTracker") != e))
                                                    a.onMouseOut()
                                            }).css(h).add(g.group || c.trackerGroup)
                            })
                },
                animate            : function(a) {
                    var c = this, b = c.data;
                    if (!a)
                        t(b, function(a) {
                                    var b = a.graphic;
                                    b && (b.attr({
                                                height : 0,
                                                y      : c.yAxis.translate(0, 0, 1)
                                            }), b.animate({
                                                height : a.barH,
                                                y      : a.barY
                                            }, c.options.animation))
                                }), c.animate = null
                },
                remove             : function() {
                    var a = this, c = a.chart;
                    c.hasRendered && t(c.series, function(b) {
                                if (b.type == a.type)
                                    b.isDirty = !0
                            });
                    Ea.prototype.remove.apply(a, arguments)
                }
            });
    Ta.column = ib;
    v = $(ib, {
                type : "bar",
                init : function(a) {
                    a.inverted = this.inverted = !0;
                    ib.prototype.init.apply(this, arguments)
                }
            });
    Ta.bar = v;
    v = $(Ea, {
                type        : "scatter",
                translate   : function() {
                    var a = this;
                    Ea.prototype.translate.apply(a);
                    t(a.data, function(c) {
                                c.shapeType = "circle";
                                c.shapeArgs = {
                                    x : c.plotX,
                                    y : c.plotY,
                                    r : a.chart.options.tooltip.snap
                                }
                            })
                },
                drawTracker : function() {
                    var a = this, c = a.options.cursor, b = c && {
                        cursor : c
                    }, d;
                    t(a.data, function(c) {
                                (d = c.graphic) && d.attr({
                                            isTracker : !0
                                        }).on("mouseover", function() {
                                            a.onMouseOver();
                                            c.onMouseOver()
                                        }).on("mouseout", function() {
                                            if (!a.options.stickyTracking)
                                                a.onMouseOut()
                                        }).css(b)
                            })
                },
                cleanData   : function() {
                }
            });
    Ta.scatter = v;
    v = $(qb, {
                init       : function() {
                    qb.prototype.init.apply(this, arguments);
                    var a = this, c;
                    A(a, {
                                visible : a.visible !== !1,
                                name    : p(a.name, "Slice")
                            });
                    c = function() {
                        a.slice()
                    };
                    ra(a, "select", c);
                    ra(a, "unselect", c);
                    return a
                },
                setVisible : function(a) {
                    var c = this.series.chart, b = this.tracker, d = this.dataLabel, f = this.connector, e;
                    e = (this.visible = a = a === ua ? !this.visible : a) ? "show" : "hide";
                    this.group[e]();
                    if (b)
                        b[e]();
                    if (d)
                        d[e]();
                    if (f)
                        f[e]();
                    this.legendItem && c.legend.colorizeItem(this, a)
                },
                slice      : function(a, c, b) {
                    var d = this.series.chart, f = this.slicedTranslation;
                    j(b, d);
                    p(c, !0);
                    a = this.sliced = u(a) ? a : !this.sliced;
                    a = {
                        translateX : a ? f[0] : d.plotLeft,
                        translateY : a ? f[1] : d.plotTop
                    };
                    this.group.animate(a);
                    this.shadowGroup && this.shadowGroup.animate(a)
                }
            });
    v = $(Ea, {
                type               : "pie",
                isCartesian        : !1,
                pointClass         : v,
                pointAttrToOptions : {
                    stroke         : "borderColor",
                    "stroke-width" : "borderWidth",
                    fill           : "color"
                },
                getColor           : function() {
                    this.initialColor = fb
                },
                animate            : function() {
                    var a = this;
                    t(a.data, function(c) {
                                var b = c.graphic, c = c.shapeArgs, d = -Ia / 2;
                                b && (b.attr({
                                            r     : 0,
                                            start : d,
                                            end   : d
                                        }), b.animate({
                                            r     : c.r,
                                            start : c.start,
                                            end   : c.end
                                        }, a.options.animation))
                            });
                    a.animate = null
                },
                translate          : function() {
                    var a = 0, c = -0.25, b = this.options, d = b.slicedOffset, f = d + b.borderWidth, e = b.center, g = this.chart, h = g.plotWidth, j = g.plotHeight, l, m, q, o = this.data, p = 2 * Ia, u, w = Pa(h, j), s, v, y, B = b.dataLabels.distance;
                    e.push(b.size, b.innerSize || 0);
                    e = Bb(e, function(a, b) {
                                return (s = /%$/.test(a)) ? [h, j, w, w][b] * x(a) / 100 : a
                            });
                    this.getX = function(a, b) {
                        q = W.asin((a - e[1]) / (e[2] / 2 + B));
                        return e[0] + (b ? -1 : 1) * Y(q) * (e[2] / 2 + B)
                    };
                    this.center = e;
                    t(o, function(b) {
                                a += b.y
                            });
                    t(o, function(b) {
                                u = a ? b.y / a : 0;
                                l = J(c * p * 1E3) / 1E3;
                                c += u;
                                m = J(c * p * 1E3) / 1E3;
                                b.shapeType = "arc";
                                b.shapeArgs = {
                                    x      : e[0],
                                    y      : e[1],
                                    r      : e[2] / 2,
                                    innerR : e[3] / 2,
                                    start  : l,
                                    end    : m
                                };
                                q = (m + l) / 2;
                                b.slicedTranslation = Bb([Y(q) * d + g.plotLeft, Z(q) * d + g.plotTop], J);
                                v = Y(q) * e[2] / 2;
                                y = Z(q) * e[2] / 2;
                                b.tooltipPos = [e[0] + v * 0.7, e[1] + y * 0.7];
                                b.labelPos = [e[0] + v + Y(q) * B, e[1] + y + Z(q) * B, e[0] + v + Y(q) * f, e[1] + y + Z(q) * f, e[0] + v, e[1] + y, B < 0 ? "center" : q < p / 4 ? "left" : "right", q];
                                b.percentage = u * 100;
                                b.total = a
                            });
                    this.setTooltipPoints()
                },
                render             : function() {
                    this.drawPoints();
                    this.options.enableMouseTracking !== !1 && this.drawTracker();
                    this.drawDataLabels();
                    this.options.animation && this.animate && this.animate();
                    this.isDirty = !1
                },
                drawPoints         : function() {
                    var a = this.chart, c = a.renderer, b, d, f, e = this.options.shadow, g, h;
                    t(this.data, function(j) {
                                d = j.graphic;
                                h = j.shapeArgs;
                                f = j.group;
                                g = j.shadowGroup;
                                if (e && !g)
                                    g = j.shadowGroup = c.g("shadow").attr({
                                                zIndex : 4
                                            }).add();
                                if (!f)
                                    f = j.group = c.g("point").attr({
                                                zIndex : 5
                                            }).add();
                                b = j.sliced ? j.slicedTranslation : [a.plotLeft, a.plotTop];
                                f.translate(b[0], b[1]);
                                g && g.translate(b[0], b[1]);
                                d ? d.animate(h) : j.graphic = c.arc(h).attr(A(j.pointAttr[Ga], {
                                            "stroke-linejoin" : "round"
                                        })).add(j.group).shadow(e, g);
                                j.visible === !1 && j.setVisible(!1)
                            })
                },
                drawDataLabels     : function() {
                    var a = this.data, c, b = this.chart, d = this.options.dataLabels, f = p(d.connectorPadding, 10), e = p(d.connectorWidth, 1), g, h, j = d.distance > 0, l, m, q = this.center[1], o = [[], [], [], []], u, v, B, s, C, Ca, oa, A = 4, x;
                    Ea.prototype.drawDataLabels.apply(this);
                    t(a, function(a) {
                                var b = a.labelPos[7];
                                o[b < 0 ? 0 : b < Ia / 2 ? 1 : b < Ia ? 2 : 3].push(a)
                            });
                    o[1].reverse();
                    o[3].reverse();
                    for (oa = function(a, b) {
                        return a.y > b.y
                    }; A--;) {
                        a = 0;
                        c = [].concat(o[A]);
                        c.sort(oa);
                        for (x = c.length; x--;)
                            c[x].rank = x;
                        for (s = 0; s < 2; s++) {
                            m = (Ca = A % 3) ? 9999 : -9999;
                            C = Ca ? -1 : 1;
                            for (x = 0; x < o[A].length; x++)
                                if (c = o[A][x], g = c.dataLabel) {
                                    h = c.labelPos;
                                    B = $a;
                                    u = h[0];
                                    v = h[1];
                                    l || (l = g && g.getBBox().height);
                                    if (j)
                                        if (s && c.rank < a)
                                            B = Ua;
                                        else if (!Ca && v < m + l || Ca && v > m - l)
                                            if (v = m + C * l, u = this.getX(v, A > 1), !Ca && v + l > q || Ca && v - l < q)
                                                s ? B = Ua : a++;
                                    c.visible === !1 && (B = Ua);
                                    B == $a && (m = v);
                                    if (s && (g.attr({
                                                visibility : B,
                                                align      : h[6]
                                            })[g.moved ? "animate" : "attr"]({
                                                x : u + d.x + ({
                                                    left  : f,
                                                    right : -f
                                                }[h[6]] || 0),
                                                y : v + d.y
                                            }), g.moved = !0, j && e))
                                        g = c.connector, h = [y, u + (h[6] == "left" ? 5 : -5), v, w, u, v, w, h[2], h[3], w, h[4], h[5]], g ? (g.animate({
                                                    d : h
                                                }), g.attr("visibility", B)) : c.connector = g = this.chart.renderer.path(h).attr({
                                                    "stroke-width" : e,
                                                    stroke         : d.connectorColor || "#606060",
                                                    visibility     : B,
                                                    zIndex         : 3
                                                }).translate(b.plotLeft, b.plotTop).add()
                                }
                        }
                    }
                },
                drawTracker        : ib.prototype.drawTracker,
                getSymbol          : function() {
                }
            });
    Ta.pie = v;
    rb.prototype.symbols = {
        square             : function(a, c, b) {
            b *= 0.707;
            return [y, a - b, c - b, w, a + b, c - b, a + b, c + b, a - b, c + b, "Z"]
        },
        triangle           : function(a, c, b) {
            return [y, a, c - 1.33 * b, w, a + b, c + 0.67 * b, a - b, c + 0.67 * b, "Z"]
        },
        "triangle-down"    : function(a, c, b) {
            return [y, a, c + 1.33 * b, w, a - b, c - 0.67 * b, a + b, c - 0.67 * b, "Z"]
        },
        diamond            : function(a, c, b) {
            return [y, a, c - b, w, a + b, c, a, c + b, a - b, c, "Z"]
        },
        "four-point-star"  : function(a, c, b) {
            return [y, a, c - b, w, a + b * 0.32, c - b * 0.32, a + b, c, a + b * 0.32, c + b * 0.32, a, c + b, a - b * 0.32, c + b * 0.32, a - b, c, a - b * 0.32, c - b * 0.32, "Z"]
        },
        "five-point-star"  : function(a, c, b) {
            return [y, a, c - 0.96 * b, w, a + b * 0.36, c - 0.4 * b, a + b, c - 0.24 * b, a + b * 0.56, c + 0.26 * b, a + b * 0.6, c + 0.94 * b, a, c + 0.65 * b, a - b * 0.6, c + 0.94 * b, a - b * 0.56, c + 0.26 * b, a - b, c - 0.24 * b, a - b * 0.36, c - 0.4 * b, "Z"]
        },
        "six-point-star"   : function(a, c, b) {
            return [y, a, c - b, w, a + b * 0.3, c - b * 0.54, a + b * 0.88, c - b * 0.5, a + b * 0.6, c, a + b * 0.86, c + b * 0.5, a + b * 0.3, c + b * 0.52, a, c + b, a - b * 0.3, c + b * 0.52, a - b * 0.86, c + b * 0.5, a - b * 0.6, c, a - b * 0.88, c - b * 0.5, a - b * 0.3, c - b * 0.54, "Z"]
        },
        "seven-point-star" : function(a, c, b) {
            return [y, a, c - b * 0.98, w, a + b * 0.28, c - b * 0.54, a + b * 0.8, c - b * 0.6, a + b * 0.62, c - b * 0.12, a + b, c + b * 0.16, a + b * 0.5, c + b * 0.44, a + b * 0.44, c + b * 0.96, a, c + b * 0.68, a - b * 0.44, c + b * 0.96, a - b * 0.5, c + b * 0.44, a - b, c + b * 0.16, a - b * 0.62, c - b * 0.12, a - b * 0.8, c - b * 0.6, a - b * 0.28, c - b * 0.54, "Z"]
        },
        "arrow-up"         : function(a, c, b) {
            return [y, a, c - b, w, a + b * 0.9, c - b * 0.08, a + b * 0.36, c - b * 0.08, a + b * 0.36, c + b, a - b * 0.36, c + b, a - b * 0.36, c - b * 0.08, a - b * 0.9, c - b * 0.08, "Z"]
        },
        "arrow-down"       : function(a, c, b) {
            return [y, a, c + b, w, a + b * 0.9, c + b * 0.08, a + b * 0.36, c + b * 0.08, a + b * 0.36, c - b, a - b * 0.36, c - b, a - b * 0.36, c + b * 0.08, a - b * 0.9, c + b * 0.08, "Z"]
        },
        split              : function(a, c, b) {
            return [y, a + b * 0.2, c - b, w, a + b, c, a + b * 0.2, c + b, "Z", y, a - b * 0.2, c + b, w, a - b, c, a - b * 0.2, c - b, "Z"]
        },
        "split-reverse"    : function(a, c, b) {
            return [y, a + b, c - b, w, a + b, c + b, a + b * 0.16, c, "Z", y, a - b, c - b, w, a - b, c + b, a - b * 0.16, c, "Z"]
        },
        spinoff            : function(a, c, b) {
            return [y, a - b, c - b, w, a + b * 0.72, c - b, a + b * 0.72, c, a, c, a, c + b * 0.72, a - b, c + b * 0.72, "Z", y, a + b * 0.24, c + b * 0.24, w, a + b, c + b * 0.24, a + b, c + b, a + b * 0.24, c + b, "Z"]
        },
        merger             : function(a, c, b) {
            return [y, a - b * 0.38, c - b, w, a + b * 0.73, c - b, a + b * 0.73, c + b, a + b * 0.16, c + b, a + b * 0.16, c + b * 0.22, a - b * 0.58, c + b, a - b * 0.94, c + b * 0.6, a - b * 0.38, c + b * 0.02, "Z"]
        },
        arc                : function(a, c, b, d) {
            var f = d.start, e = d.end - 1.0E-6, g = d.innerR, h = Y(f), j = Z(f), l = Y(e), e = Z(e), d = d.end - f < Ia ? 0 : 1;
            return [y, a + b * h, c + b * j, "A", b, b, 0, d, 1, a + b * l, c + b * e, w, a + g * l, c + g * e, "A", g, g, 0, d, 0, a + g * h, c + g * j, "Z"]
        }
    };
    rb.prototype.image = function(a, c, b, d, f) {
        var e = {
            preserveAspectRatio : Ka
        };
        arguments.length > 1 && A(e, {
                    x      : c,
                    y      : b,
                    width  : d,
                    height : f
                });
        e = this.createElement("image").attr(e);
        if (this.boxWrapper.renderer.forExport && a.indexOf("http") < 0) {
            for (var g = window.location.href, h = g.lastIndexOf("/"), g = g.substring(0, h), j = /\.\.\//;;)
                if (j.test(a))
                    a = a.replace(/\.\.\//, ""), h = g.lastIndexOf("/"), g = g.substring(0, h);
                else
                    break;
            a = g + "/" + a
        }
        e.element.setAttributeNS ? e.element.setAttributeNS("http://www.w3.org/1999/xlink", "href", a) : e.element.setAttribute("hc-svg-href", a);
        return e
    };
    rb.prototype.color = function(a, c, b) {
        var d, f = /^rgba/;
        if (a && a.linearGradient) {
            var e = this, c = a.linearGradient, b = mb + Jb++, g, h, j;
            g = e.createElement("linearGradient").attr({
                        id : b,
                        x1 : c[0],
                        y1 : c[1],
                        x2 : c[2],
                        y2 : c[3]
                    }).add(e.defs);
            t(a.stops, function(a) {
                        f.test(a[1]) ? (d = ca(a[1]), h = d.get("rgb"), j = d.get("a")) : (h = a[1], j = 1);
                        e.createElement("stop").attr({
                                    offset         : a[0],
                                    "stop-color"   : h,
                                    "stop-opacity" : j
                                }).add(g)
                    });
            return "url(" + this.url + "#" + b + ")"
        }
        else
            return f.test(a) ? (d = ca(a), I(c, b + "-opacity", d.get("a")), d.get("rgb")) : a
    };
    if (Va)
        Va.prototype.color = function(a, c, b) {
            var d, f = /^rgba/;
            if (a && a.linearGradient) {
                var e, g, h = a.linearGradient, j, l, m, q, o;
                if (a.stops.length > 2) {
                    myColors = [];
                    myColors = [' colors="'];
                    for (i = 0; i < a.stops.length; i++)
                        f.test(a.stops[i][1]) ? (d = ca(a.stops[i][1]), e = d.get("rgb"), g = d.get("a")) : (e = a.stops[i][1], g = 1), myColors = myColors.concat([a.stops[i][0], " ", e, ", "]);
                    myColors.pop()
                }
                t(a.stops, function(a, b) {
                            f.test(a[1]) ? (d = ca(a[1]), e = d.get("rgb"), g = d.get("a")) : (e = a[1], g = 1);
                            b ? (m = e, q = g) : (j = e, l = g)
                        });
                o = "100%";
                h = 90 - W.atan((h[3] - h[1]) / (h[2] - h[0])) * 180 / Ia;
                if (a.options)
                    h = a.options.angle, o = a.options.focus, l = a.options.opacity1, q = a.options.opacity2;
                a.stops.length > 2 ? (a = ["<", b], markup2 = ['" angle="', h, '" opacity="', q, '" o:opacity2="', l, '" type="gradient" focus="', o, '" />'], myMarkup = a.concat(myColors), myMarkup = myMarkup.concat(markup2), Q(this.prepVML(myMarkup), null, null, c)) : (a = ["<", b, ' colors="0% ', j, ",100% ", m, '" angle="', h, '" opacity="', q, '" o:opacity2="', l, '" type="gradient" focus="', o, '" />'], Q(this.prepVML(a), null, null, c))
            }
            else
                return f.test(a) && c.tagName != "IMG" ? (d = ca(a), a = ["<", b, ' opacity="', d.get("a"), '"/>'], Q(this.prepVML(a), null, null, c), d.get("rgb")) : a
        }, Va.prototype.symbols = {
            arc                : function(a, c, b, d) {
                var f = d.start, e = d.end, g = Y(f), h = Z(f), j = Y(e), l = Z(e), d = d.innerR, m = 0.07 / b, q = d && 0.1 / d || 0;
                if (e - f === 0)
                    return ["x"];
                else
                    2 * Ia - e + f < m ? j = -m : e - f < q && (j = Y(f + q));
                return ["wa", a - b, c - b, a + b, c + b, a + b * g, c + b * h, a + b * j, c + b * l, "at", a - d, c - d, a + d, c + d, a + d * j, c + d * l, a + d * g, c + d * h, "x", "e"]
            },
            circle             : function(a, c, b) {
                return ["wa", a - b, c - b, a + b, c + b, a + b, c, a + b, c, "e"]
            },
            rect               : function(a, c, b, d) {
                if (!u(d))
                    return [];
                var f = d.width, d = d.height, e = a + f, g = c + d, b = Pa(b, f, d);
                return [y, a + b, c, w, e - b, c, "wa", e - 2 * b, c, e, c + 2 * b, e - b, c, e, c + b, w, e, g - b, "wa", e - 2 * b, g - 2 * b, e, g, e, g - b, e - b, g, w, a + b, g, "wa", a, g - 2 * b, a + 2 * b, g, a + b, g, a, g - b, w, a, c + b, "wa", a, c, a + 2 * b, c + 2 * b, a, c + b, a + b, c, "x", "e"]
            },
            "four-point-star"  : function(a, c, b) {
                return [y, a, c - b, w, a + b * 0.32, c - b * 0.32, a + b, c, a + b * 0.32, c + b * 0.32, a, c + b, a - b * 0.32, c + b * 0.32, a - b, c, a - b * 0.32, c - b * 0.32, "x", "e"]
            },
            "five-point-star"  : function(a, c, b) {
                return [y, a, c - 0.96 * b, w, a + b * 0.36, c - 0.4 * b, a + b, c - 0.24 * b, a + b * 0.56, c + 0.26 * b, a + b * 0.6, c + 0.94 * b, a, c + 0.65 * b, a - b * 0.6, c + 0.94 * b, a - b * 0.56, c + 0.26 * b, a - b, c - 0.24 * b, a - b * 0.36, c - 0.4 * b, "x", "e"]
            },
            "six-point-star"   : function(a, c, b) {
                return [y, a, c - b, w, a + b * 0.3, c - b * 0.54, a + b * 0.88, c - b * 0.5, a + b * 0.6, c, a + b * 0.86, c + b * 0.5, a + b * 0.3, c + b * 0.52, a, c + b, a - b * 0.3, c + b * 0.52, a - b * 0.86, c + b * 0.5, a - b * 0.6, c, a - b * 0.88, c - b * 0.5, a - b * 0.3, c - b * 0.54, "x", "e"]
            },
            "seven-point-star" : function(a, c, b) {
                return [y, a, c - b * 0.98, w, a + b * 0.28, c - b * 0.54, a + b * 0.8, c - b * 0.6, a + b * 0.62, c - b * 0.12, a + b, c + b * 0.16, a + b * 0.5, c + b * 0.44, a + b * 0.44, c + b * 0.96, a, c + b * 0.68, a - b * 0.44, c + b * 0.96, a - b * 0.5, c + b * 0.44, a - b, c + b * 0.16, a - b * 0.62, c - b * 0.12, a - b * 0.8, c - b * 0.6, a - b * 0.28, c - b * 0.54, "x", "e"]
            },
            "arrow-up"         : function(a, c, b) {
                return [y, a, c - b, w, a + b * 0.9, c - b * 0.08, a + b * 0.36, c - b * 0.08, a + b * 0.36, c + b, a - b * 0.36, c + b, a - b * 0.36, c - b * 0.08, a - b * 0.9, c - b * 0.08, "x", "e"]
            },
            "arrow-down"       : function(a, c, b) {
                return [y, a, c + b, w, a + b * 0.9, c + b * 0.08, a + b * 0.36, c + b * 0.08, a + b * 0.36, c - b, a - b * 0.36, c - b, a - b * 0.36, c + b * 0.08, a - b * 0.9, c + b * 0.08, "x", "e"]
            },
            split              : function(a, c, b) {
                return [y, a + b * 0.2, c - b, w, a + b, c, a + b * 0.2, c + b, "x", y, a - b * 0.2, c + b, w, a - b, c, a - b * 0.2, c - b, "x", "e"]
            },
            "split-reverse"    : function(a, c, b) {
                return [y, a + b, c - b, w, a + b, c + b, a + b * 0.16, c, "x", y, a - b, c - b, w, a - b, c + b, a - b * 0.16, c, "x", "e"]
            },
            spinoff            : function(a, c, b) {
                return [y, a - b, c - b, w, a + b * 0.72, c - b, a + b * 0.72, c, a, c, a, c + b * 0.72, a - b, c + b * 0.72, "x", y, a + b * 0.24, c + b * 0.24, w, a + b, c + b * 0.24, a + b, c + b, a + b * 0.24, c + b, "x", "e"]
            },
            merger             : function(a, c, b) {
                return [y, a - b * 0.38, c - b, w, a + b * 0.73, c - b, a + b * 0.73, c + b, a + b * 0.16, c + b, a + b * 0.16, c + b * 0.22, a - b * 0.58, c + b, a - b * 0.94, c + b * 0.6, a - b * 0.38, c + b * 0.02, "x", "e"]
            },
            square             : function(a, c, b) {
                b *= 0.707;
                return [y, a - b, c - b, w, a + b, c - b, a + b, c + b, a - b, c + b, "x", "e"]
            },
            triangle           : function(a, c, b) {
                return [y, a, c - 1.33 * b, w, a + b, c + 0.67 * b, a - b, c + 0.67 * b, "x", "e"]
            },
            "triangle-down"    : function(a, c, b) {
                return [y, a, c + 1.33 * b, w, a - b, c - 0.67 * b, a + b, c - 0.67 * b, "x", "e"]
            },
            diamond            : function(a, c, b) {
                return [y, a, c - b, w, a + b, c, a, c + b, a - b, c, "x", "e"]
            }
        };
    ib.prototype.drawPoints = function() {
        var a = this, c = a.options, b = a.chart.renderer, d, f;
        t(a.data, function(e) {
                    var g = e.plotY;
                    if (g !== ua && !isNaN(g) && e.y !== null)
                        d = e.graphic, f = e.shapeArgs, e.pointAttr[""].fill.options || wc(e.pointAttr, lb, a.chart.options.chart, null, a.chart.inverted), d ? (Ab(d), d.animate(e.graphic.crisp(1, f.x, f.y, f.width + 1, f.height))) : (e.graphic = b[e.shapeType](f).attr(e.pointAttr[e.selected ? "select" : Ga]).add(a.group).shadow(c.shadow), e.graphic.attr(e.graphic.crisp(1, f.x, f.y, f.width + 1, f.height)))
                })
    };
    ma.JSCharting = {
        Chart          : q,
        dateFormat     : Ub,
        pathAnim       : pb,
        getOptions     : function() {
            return va
        },
        numberFormat   : l,
        Point          : qb,
        Color          : ca,
        Renderer       : Wb,
        seriesTypes    : Ta,
        setOptions     : function(a) {
            va = fa(va, a);
            h();
            return va
        },
        Series         : Ea,
        addEvent       : ra,
        createElement  : Q,
        discardElement : Ca,
        css            : D,
        each           : t,
        extend         : A,
        map            : Bb,
        merge          : fa,
        pick           : p,
        extendClass    : $,
        version        : "2.1.4"
    }
})();
(function() {
    var A = JSCharting, x = A.Chart, Wa = A.addEvent, X = A.createElement, S = A.discardElement, Za = A.css, u = A.merge, I = A.each, Na = A.extend, p = Math.max, Ja = document, D = window, Q = "ontouchstart" in Ja.documentElement, $ = A.setOptions({});
    $.navigation = {
        menuStyle          : {
            border     : "1px solid #A0A0A0",
            background : "#FFFFFF"
        },
        menuItemStyle      : {
            padding    : "0 5px",
            background : "none",
            color      : "#303030",
            fontSize   : Q ? "14px" : "11px"
        },
        menuItemHoverStyle : {
            background : "#4572A5",
            color      : "#FFFFFF"
        },
        buttonOptions      : {
            align             : "right",
            backgroundColor   : {
                linearGradient : [0, 0, 0, 20],
                stops          : [[0.4, "#F7F7F7"], [0.6, "#E3E3E3"]]
            },
            borderColor       : "#B0B0B0",
            borderRadius      : 3,
            borderWidth       : 1,
            height            : 20,
            hoverBorderColor  : "#909090",
            hoverSymbolFill   : "#81A7CF",
            hoverSymbolStroke : "#4572A5",
            symbolFill        : "#E0E0E0",
            symbolStroke      : "#A0A0A0",
            symbolX           : 11.5,
            symbolY           : 10.5,
            verticalAlign     : "top",
            width             : 24,
            y                 : 10
        }
    };
    $.exporting = {
        type    : "image/png",
        url     : "https://export.dotnetcharting.com/download",
        width   : 800,
        buttons : {
            exportButton : {
                symbol          : "exportIcon",
                x               : -10,
                width           : 34,
                height          : 20,
                symbolSize      : 12,
                symbolY         : 10.5,
                symbolX         : 10.5,
                symbolFill      : "#A8BF77",
                hoverSymbolFill : "#768F3E",
                _titleKey       : "exportButtonTitle",
                menuItems       : [{
                            textKey : "downloadPNG",
                            onclick : function() {
                                this.exportChart()
                            }
                        }, {
                            textKey : "downloadJPEG",
                            onclick : function() {
                                this.exportChart({
                                            type : "image/jpeg"
                                        })
                            }
                        }, {
                            textKey : "downloadPDF",
                            onclick : function() {
                                this.exportChart({
                                            type : "application/pdf"
                                        })
                            }
                        }, {
                            textKey : "downloadSVG",
                            onclick : function() {
                                this.exportChart({
                                            type : "image/svg+xml"
                                        })
                            }
                        }]
            },
            printButton  : {
                symbol          : "printIcon",
                x               : -52,
                width           : 21,
                height          : 20,
                symbolSize      : 13,
                symbolY         : 10,
                symbolX         : 11,
                x               : -52,
                symbolFill      : "#B5C9DF",
                hoverSymbolFill : "#779ABF",
                _titleKey       : "printButtonTitle",
                onclick         : function() {
                    this.print()
                }
            }
        }
    };
    Na(x.prototype, {
                getSVG         : function(l) {
                    var j = this, h, p, x, q, B, A, D = u(j.options, l);
                    if (!Ja.createElementNS)
                        Ja.createElementNS = function(h, l) {
                            var q = Ja.createElement(l);
                            q.getBBox = function() {
                                return j.renderer.Element.prototype.getBBox.apply({
                                            element : q
                                        })
                            };
                            return q
                        };
                    h = X("div", null, {
                                position : "absolute",
                                top      : "-9999em",
                                width    : j.chartWidth + "px",
                                height   : j.chartHeight + "px"
                            }, Ja.body);
                    Na(D.chart, {
                                renderTo  : h,
                                forExport : !0
                            });
                    D.exporting.enabled = !1;
                    D.series = [];
                    I(j.series, function(h) {
                                x = h.options;
                                x.animation = !1;
                                x.showCheckbox = !1;
                                if (x && x.marker && /^url\(/.test(x.marker.symbol))
                                    x.marker.symbol = "circle";
                                x.data = [];
                                I(h.data, function(h) {
                                            q = h.config;
                                            B = {
                                                x    : h.x,
                                                y    : h.y,
                                                name : h.name
                                            };
                                            typeof q == "object" && h.config && q.constructor != Array && Na(B, q);
                                            x.data.push(B);
                                            (A = h.config && h.config.marker) && /^url\(/.test(A.symbol) && delete A.symbol
                                        });
                                D.series.push(x)
                            });
                    l = new JSCharting.Chart(D);
                    p = l.container.innerHTML;
                    D = null;
                    l.destroy();
                    S(h);
                    p = p.replace(/zIndex="[^"]+"/g, "").replace(/isShadow="[^"]+"/g, "").replace(/symbolName="[^"]+"/g, "").replace(/jQuery[0-9]+="[^"]+"/g, "").replace(/isTracker="[^"]+"/g, "").replace(/url\([^#]+#/g, "url(#").replace(/id=([^" >]+)/g, 'id="$1"').replace(/class=([^" ]+)/g, 'class="$1"').replace(/ transform /g, " ").replace(/:(path|rect)/g, "$1").replace(/style="([^"]+)"/g, function(h) {
                                return h.toLowerCase()
                            });
                    p = p.replace(/(url\(#jscharting-[0-9]+)&quot;/g, "$1").replace(/&quot;/g, "'");
                    p.match(/ xmlns="/g).length == 2 && (p = p.replace(/xmlns="[^"]+"/, ""));
                    return p = p.replace(/href=/g, ' xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="simple" xlink:href=')
                },
                exportChart    : function(l, j) {
                    var h, p = this.getSVG(j), l = u(this.options.exporting, l);
                    h = X("form", {
                                method : "post",
                                action : l.url
                            }, {
                                display : "none"
                            }, Ja.body);
                    I(["filename", "type", "width", "svg"], function(j) {
                                X("input", {
                                            type  : "hidden",
                                            name  : j,
                                            value : {
                                                filename : l.filename || "chart",
                                                type     : l.type,
                                                width    : l.width,
                                                svg      : p
                                            }[j]
                                        }, null, h)
                            });
                    h.submit();
                    S(h)
                },
                exportChartNew : function(l) {
                    var j, h = this.container.innerHTML, p = h.indexOf("<svg"), h = h.substring(p), p = h.indexOf("svg>"), h = h.substring(0, p + 4), h = h.replace(/zIndex="[^"]+"/g, "").replace(/isShadow="[^"]+"/g, "").replace(/symbolName="[^"]+"/g, "").replace(/jQuery[0-9]+="[^"]+"/g, "").replace(/isTracker="[^"]+"/g, "").replace(/url\([^#]+#/g, "url(#").replace(/id=([^" >]+)/g, 'id="$1"').replace(/class=([^" ]+)/g, 'class="$1"').replace(/ transform /g, " ").replace(/:(path|rect)/g, "$1").replace(/style="([^"]+)"/g, function(h) {
                                return h.toLowerCase()
                            }), h = h.replace(/(url\(#jscharting-[0-9]+)&quot;/g, "$1").replace(/&quot;/g, "'");
                    h.match(/ xmlns="/g).length == 2 && (h = h.replace(/xmlns="[^"]+"/, ""));
                    l = u(this.options.exporting, l);
                    j = X("form", {
                                method : "post",
                                action : l.url
                            }, {
                                display : "none"
                            }, Ja.body);
                    I(["filename", "type", "width", "svg"], function(p) {
                                X("input", {
                                            type  : "hidden",
                                            name  : p,
                                            value : {
                                                filename : l.filename || "chart",
                                                type     : l.type,
                                                width    : l.width,
                                                svg      : h
                                            }[p]
                                        }, null, j)
                            });
                    j.submit();
                    S(j)
                },
                print          : function() {
                    var l = this, j = l.container, h = [], p = j.parentNode, u = Ja.body, q = u.childNodes;
                    if (!l.isPrinting)
                        l.isPrinting = !0, I(q, function(j, l) {
                                    if (j.nodeType == 1)
                                        h[l] = j.style.display, j.style.display = "none"
                                }), u.appendChild(j), D.print(), setTimeout(function() {
                                    p.appendChild(j);
                                    I(q, function(j, l) {
                                                if (j.nodeType == 1)
                                                    j.style.display = h[l]
                                            });
                                    l.isPrinting = !1
                                }, 1E3)
                },
                contextMenu    : function(l, j, h, u, x, q) {
                    var B = this, D = B.options.navigation, S = D.menuItemStyle, za = B.chartWidth, ta = B.chartHeight, la = "cache-" + l, C = B[la], N = p(x, q), R = "3px 3px 10px #888", T, $, R = "1px 1px 1 #888";
                    label = B.options.navigation.label || "Save As...";
                    S.borderLeft = "20px solid #E0E0E0";
                    if (!C)
                        B[la] = C = X("div", {
                                    className : "jscharting-" + l
                                }, {
                                    position : "absolute",
                                    zIndex   : 1E3,
                                    padding  : N + "px"
                                }, B.container), T = X("div", null, Na({
                                            MozBoxShadow    : R,
                                            WebkitBoxShadow : R,
                                            boxShadow       : R
                                        }, D.menuStyle), C), $ = function() {
                            Za(C, {
                                        display : "none"
                                    })
                        }, Wa(C, "mouseleave", $), I(j, function(h) {
                                    h && (X("div", {
                                                onmouseover : function() {
                                                    Za(this, D.menuItemHoverStyle)
                                                },
                                                onmouseout  : function() {
                                                    Za(this, S)
                                                },
                                                innerHTML   : h.text || A.getOptions().lang[h.textKey]
                                            }, Na({
                                                        cursor : "pointer"
                                                    }, S), T)[Q ? "ontouchstart" : "onclick"] = function() {
                                        $();
                                        h.onclick.apply(B, arguments)
                                    })
                                }), B.exportMenuWidth = C.offsetWidth, B.exportMenuHeight = C.offsetHeight;
                    l = {
                        display : "block"
                    };
                    h + B.exportMenuWidth > za ? l.right = za - h - x - N + "px" : l.left = h - N + "px";
                    u + q + B.exportMenuHeight > ta ? l.bottom = ta - u - N + "px" : l.top = u + q - N + "px";
                    Za(C, l)
                },
                addButton      : function(l, j) {
                    function h() {
                        S.attr(T);
                        Q.attr(R)
                    }
                    var p = this, x = p.renderer, q = u(p.options.navigation.buttonOptions, l), B = q.onclick, A = q.menuItems, D = q.width, I = q.height, Q, S, C, N = q.borderWidth, R = {
                        stroke : q.borderColor
                    }, T = {
                        stroke : q.symbolStroke,
                        fill   : q.symbolFill
                    };
                    if (q.enabled !== !1) {
                        var X = {
                            x      : p.plotLeft,
                            y      : p.plotTop,
                            width  : p.plotWidth,
                            height : p.plotHeight
                        };
                        Q = x.rect(0, 0, D, I, q.borderRadius, N).align(q, !0, X).attr(Na({
                                    fill           : q.backgroundColor,
                                    "stroke-width" : N,
                                    zIndex         : 19
                                }, R)).add();
                        C = x.rect(0, 0, D, I, 0).align(q, !1, X).attr({
                                    fill   : "rgba(255, 255, 255, 0.001)",
                                    title  : p.options.lang[q._titleKey] || q._titleKey,
                                    alt    : p.options.lang[q._titleKey] || q._titleKey,
                                    zIndex : 21,
                                    id     : j
                                }).css({
                                    cursor : "pointer"
                                }).on("mouseover", function() {
                                    S.attr({
                                                stroke : q.hoverSymbolStroke,
                                                fill   : q.hoverSymbolFill
                                            });
                                    Q.attr({
                                                stroke : q.hoverBorderColor
                                            })
                                }).on("mouseout", h).on("click", h).add();
                        try {
                            var $ = document.createElementNS("http://www.w3.org/2000/svg", "title"), Ja = document.createTextNode(p.options.lang[q._titleKey] || q._titleKey);
                            $.appendChild(Ja);
                            C.element.appendChild($)
                        }
                        catch (Wa) {
                        }
                        A && (B = function() {
                            h();
                            var j = C.getBBox();
                            p.contextMenu("export-menu", A, j.x, j.y, D, I)
                        });
                        C.on("click", function() {
                                    B.apply(p, arguments)
                                });
                        S = x.symbol(q.symbol, q.symbolX, q.symbolY, (q.symbolSize || 12) / 2).align(q, !0, X).attr(Na(T, {
                                    "stroke-width" : q.symbolStrokeWidth || 1,
                                    zIndex         : 20
                                })).add()
                    }
                }
            });
    A.Renderer.prototype.symbols.exportIcon = function(l, j, h) {
        return ["M", l - h, j + h, "L", l - h * 0.53, j + h * 1, l - h * 0.53, j + h * 0.35, l + h * 0.35, j + h * 0.35, l + h * 0.35, j + h * 1, l + h, j + h, l + h, j - h, l + h * 0.65, j - h * 1, l + h * 0.65, j + h * 0, l - h * 0.65, j + h * 0, l - h * 0.65, j - h * 1, l - h, j - h, "Z", "M", l - h * 0.65, j + h * 0, "L", l + h * 0.65, j + h * 0, l + h * 0.65, j - h * 1, l - h * 0.65, j - h * 1, "Z", "M", l - h * 0.53, j + h * 1, "L", l + h * 0.35, j + h * 1, l + h * 0.35, j + h * 0.35, l - h * 0.53, j + h * 0.35, "Z", "M", l - h * 0.2, j + h * 0.56, "L", l - h * 0.2, j + h * 0.76, "Z", "M", l + h * 1.5, j - h * 1, "L", l + h * 1.5, j + h * 1, "M", l + h * 2, j - h * 0.5, "L", l + h * 3.2, j - h * 0.5, l + h * 2.6, j + h * 0.4, "Z"]
    };
    A.Renderer.prototype.symbols.printIcon = function(l, j, h) {
        return ["M", l + h * 1, j + h * 0.5, "L", l + h * 0.8, j + h * 1, l - h * 0.8, j + h * 1, l - h * 1, j + h * 0.5, l - h, j - h * 0, l - h * 0.7, j - h * 0, l - h * 0.7, j + h * 0.2, l + h * 0.7, j + h * 0.2, l + h * 0.7, j - h * 0, l + h, j - h * 0, "Z", "M", l - h * 0.7, j + h * 0.2, "L", l - h * 0.7, j - h, l + h * 0.1, j - h, l + h * 0.1, j - h * 0.4, l + h * 0.7, j - h * 0.4, l + h * 0.7, j + h * 0.2, "Z", "M", l + h * 0.1, j - h, "L", l + h * 0.1, j - h * 0.4, l + h * 0.7, j - h * 0.4, "Z", "M", l - h * 0.4, j - h * 0.4, "L", l - h * 0.2, j - h * 0.4, "M", l - h * 0.4, j - h * 0.7, "L", l - h * 0.2, j - h * 0.7, "M", l - h * 0.4, j - h * 0.1, "L", l + h * 0.4, j - h * 0.1]
    };
    x.prototype.callbacks.push(function(l) {
                var j, h = l.options.exporting, p = h.buttons;
                if (h.enabled !== !1)
                    for (j in p)
                        l.addButton(p[j], j)
            })
})();

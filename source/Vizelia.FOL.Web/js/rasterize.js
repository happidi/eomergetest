var page = require('webpage').create();
var system = require('system');
var fs = require('fs');

var requests = {};
var lastRequest = Date.now();

if (system.args.length > 8) {
    console.log('Usage: rasterize.js URL filename width height cookie(timeout)');
    phantom.exit(1);
} else {
    var address = system.args[1];
    var output = system.args[2];
    var width = system.args[3];
    var height = system.args[4];
    var authCookie = system.args[5];
    var lang = system.args[6];
    var timeout = system.args.length >= 8 ? system.args[7] : 30000;
    //fs.write("d:\\phantom\\phantom.txt", Date() + ' ' + authCookie + ' ' + address + '\n', 'a');

    page.onResourceRequested = function (requestData) {
        if (!requestData.url.indexOf("http://localhost") == 0 && !requestData.url.indexOf("https://localhost") == 0) {
            requests[requestData.id] = true;
            lastRequest = new Date();
            //fs.write("d:\phantom.txt", 'out ' + lastRequest + ' ' + requestData.url + '\n', 'a');
        }
    };

    page.onResourceReceived = function (response) {
        if (!response.url.indexOf("http://localhost") == 0 && !response.url.indexOf("https://localhost") == 0) {
            delete requests[response.id];
            //fs.write("d:\phantom.txt", 'inn ' + new Date() + ' ' + response.url + '\n', 'a');
        }
    };
    page.customHeaders = {
        'Cookie': '.ASPXAUTH=' + authCookie,
        'Accept-Language': lang
    };

    page.evaluate(function() {
        document.body.bgColor = 'white';
    });

    page.viewportSize = {
        width: width,
        height: height
    };

    page.onError = function(msg, trace) {
        console.log(msg);
        trace.forEach(function(item) {
            console.log('  ', item.file, ':', item.line);
        });
    }

    page.open(address, function (status) {
        //fs.write("d:\phantom.txt", '', 'w');
        var intervalIdTimeout = setTimeout(function () {
            page.render(output);
            phantom.exit(759);
        }, timeout);

        var intervalIdSmart = setInterval(function () {
            // We make sure there are no requests we wait them to come back, 
            // and that at least 2.5 seconds passed since the last request, because there might be requests deferred via Ext.util.DelayedTask's delay function
            //fs.write("d:\phantom.txt", 'end ' + new Date() + ' ' + Object.keys(requests).length + '\n', 'a');
            if (!Object.keys(requests).length && (Date.now() - lastRequest > 5000)) {
                clearInterval(intervalIdSmart);
                page.render(output);
                phantom.exit();
            }
        }, 200);
    });
}

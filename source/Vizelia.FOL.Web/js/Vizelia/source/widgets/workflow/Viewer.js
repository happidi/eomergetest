﻿Ext.namespace('Viz.workflow');

/**
 * @class Viz.workflow.Viewer A viewer of state machine.
 * @extends Viz.viewer.Image.
 */
Viz.workflow.Viewer = Ext.extend(Viz.viewer.Image, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {

                config = config || {};
                var defaultConfig = {
                    stretchImage     : false,
                    serviceInterface : 'ServiceDesk.svc',
                    serviceStream    : "Workflow_GetStreamImageFromCache",
                    serviceHandler   : Viz.Services.ServiceDeskWCF.Workflow_BuildImage,
                    serviceParams    : {
                        instanceId   : config.entity.InstanceId,
                        currentState : config.entity.State
                    }

                };
                var forcedConfig = {

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.workflow.Viewer.superclass.constructor.call(this, config);
            }
        });

Ext.reg('vizWorkflowViewer', Viz.workflow.Viewer);

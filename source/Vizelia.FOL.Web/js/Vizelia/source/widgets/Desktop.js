


Viz.Desktop =Ext.extend(Ext.Viewport,{	
		initComponent:function(){
			
			
			Ext.apply(this,{
				layout:'border',
				renderTo:Ext.getBody(),
				items:[{
					region:'north',
					height:30,
					html:"<div id='viewport-header' ><h1>Vizelia FOL 2.0</h1></div>"
					},{
					layout:'border',
					region:'center',
					border:false,
					items:[{
						region:'north',
						height:122,
						border:false,
						items:this.toolbarTab=new Ext.TabPanel({
							activeTab:0
							})
						},{
						region:'center',
						border:false,
						layout:'fit',
						items:this.mainTab=new Ext.TabPanel({
							xtype:'tabpanel',
							enableTabScroll:true,
							activeTab:0,
							items:[{
								title:'First Tab',
								closable:true
								}]
							})
						},{
						region:'west',
						autoHide:false,
						split:true,
						width:300,
						minSize:175,
						maxSize:400,
						collapsible:true,
						collapsed:true,
						margins:'1 1 1 1',
						layout:'accordion',
						items:[{
							title:'Navigation',
							border:'false' 
							}]
						}],
					bbar:new Ext.StatusBar({border:false,defaultText:'Statut:',busyText:''})					
					}]
				});
			Viz.Desktop.superclass.initComponent.apply(this,arguments);
		},
		
		addTab:function(o){
			this.mainTab.add(o).show();
		}
});

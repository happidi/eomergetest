Ext.namespace("Viz");

/**
 * Global Application Singleton.
 */
Viz.App = {
    /**
     * First function to be called by Ext.onReady
     */
    init: function () {
        if (window == window.top) {

            Ext.get('TextDiv').dom.style.visibility = 'inherit';

            Viz.AjaxNet.init();

            Viz.LoadingIndicator.init();

            // Registering global events and responses.
            Viz.util.MessageBusMgr.subscribe("EndLoading", this.onEndLoading, this);
            Viz.util.MessageBusMgr.subscribe("CheckingCredential", this.onCheckingCredential, this);
            Viz.util.MessageBusMgr.subscribe("LoginSuccess", this.onLoginSuccess, this);
            Viz.util.MessageBusMgr.subscribe("ConfigurationSuccess", this.onConfigurationSuccess, this);

            // this.loadLangue.defer(100);
            this.onAfterLoadLangue.defer(100, Viz.App);
            // Ext.state.Manager.setProvider(new Ext.state.CookieProvider({
            // expires : new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 120))
            // 120 days
            // }));
            this.initMouseTrack();

        }
    },

    /**
     * Track the mouse moves and save them to a global variable. (useful for drill down)
     */
    initMouseTrack: function () {
        function getMouseXY(e) {
            if (IE) {
                Viz.MouseX = event.clientX + document.body.scrollLeft;
                Viz.MouseY = event.clientY + document.body.scrollTop;
            }
            else {
                Viz.MouseX = e.pageX;
                Viz.MouseY = e.pageY;
            }
            if (Viz.MouseX < 0) {
                Viz.MouseX = 0;
            }
            if (Viz.MouseY < 0) {
                Viz.MouseY = 0;
            }
            return true;
        }

        var IE = document.all ? true : false;
        if (!IE)
            document.captureEvents(Event.MOUSEMOVE);
        document.onmousemove = getMouseXY;
        Viz.MouseX = 0;
        Viz.MouseY = 0;
    },

    /**
     * Populates Viz.LocalizeMgr with message from database
     */
    loadLangue: function () {
        Viz.Localization.Store = new Viz.store.Localization();
        Viz.Localization.Store.on("load", function () {
            this.loadClassificationItemDefinition.apply(this);
        }, Viz.App, {
            single: true
        });
        Viz.Localization.Store.load();
    },

    /**
     * Populates Viz.Configuration.ClassificationItemDefinition with definitions for ClassificationItem.
     */
    loadClassificationItemDefinition: function () {
        Viz.Configuration.ClassificationItemDefinition = new Viz.store.ClassificationItemDefinition();
        Viz.Configuration.ClassificationItemDefinition.on("load", function () {
            this.onAfterLoadClassificationItemDefinition.apply(this);
        }, Viz.App, {
            single: true
        });

        Viz.Configuration.ClassificationItemDefinition.load();
    },

    /**
     * Indicates end of messages loading.
     */
    onAfterLoadLangue: function () {
        this.hideLoading();
        // this.loadClassificationItemDefinition();
    },

    /**
     * Indicates end of ClassificationItemDefinition loading.
     */
    onAfterLoadClassificationItemDefinition: function () {
        // this.hideLoading();
        this.loadPortalDefinition();
    },

    /**
     * Hides with an effect Viz.LoadingIndicator
     * @param {Object} Object literal with any of the Fx config options
     */
    hideLoading: function (o) {
        Viz.LoadingIndicator.hide(o);
    },

    /**
     * Indicates that loading indicator was hidden.
     */
    onEndLoading: function () {
        this.doLogin();
    },

    /**
     * Displays a Viz.window.Login instance. The window will not be displayed if the user is already logged in.
     */
    doLogin: function () {
        if (window == window.top) {

            Viz.Services.PublicWCF.GetApplicationSettings({
                success: function (value) {
                    Viz.ApplicationSettings = value;

                    Viz.Services.PublicWCF.IsUserAuthenticated({
                        success: function (value) {
                            if (value.IsAuthenticated != false) {
                                Viz.pageId = value.PageId;
                                Viz.util.MessageBusMgr.publish("CheckingCredential");
                                Viz.HasLogged = true;
                                Viz.Services.AuthenticationWCF.User_GetCurrent({
                                    success: function (user) {
                                        Viz.util.MessageBusMgr.publish("LoginSuccess", user);
                                    },
                                    scope: this
                                });
                            }
                            else

                                Viz.Services.PublicWCF.AuthenticationProvider_GetStore({
                                    success: function (value) {
                                        var oLogin = new Viz.window.Login({
                                            shouldHideAuthenticationCombo: (value.recordCount == 1)
                                        });
                                    }
                                });
                        }
                    });
                }
            });

        }
    },

    /**
     * Displays a wait messagebox before credentials are sent to the server for validation.
     */
    onCheckingCredential: function () {
        Ext.MessageBox.show({
            title: $lang('msg_application_title'),
            iconCls: 'viz-icon-small-main',
            msg: $lang('msg_check_credential'),
            width: 300,
            wait: true,
            waitConfig: {
                animate: true,
                text: '',
                interval: 1000
            },
            progress: true,
            closable: false
        });
    },

    /**
     * Creates the Application viewport.
     */
    onLoginSuccess: function (user) {

        Ext.apply(Viz.App, {
            Global: {
                User: user
            }
        });

        Ext.state.Manager.setProvider(new Ext.state.RESTfulProvider({
            expires: new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 120))
            // 120 days
        }));

        // this.loadPortalDefinition();
        // this.loadClassificationItemDefinition();
        this.loadAuditEntity();
        this.loadLangue();
    },

    /**
     * Populates Viz.Configuration.PortalWindowDefinition with the list of PortalWindows.
     */
    loadPortalDefinition: function (callback) {
        Viz.Configuration.PortalWindowDefinition = new Viz.store.PortalWindow();
        Viz.Configuration.PortalWindowDefinition.on("load", function () {

            if (callback) {
                callback();
            }
            else {
                this.onAfterLoadPortalWindowDefinition.apply(this);
            }

        }, Viz.App, {
            single: true
        });

        Viz.Configuration.PortalWindowDefinition.load();
    },

    /**
     * Indicates end of Viz.Configuration.PortalWindowDefinition loading.
     */
    onAfterLoadPortalWindowDefinition: function () {
        this.loadLinks();
    },

    /**
     * Populates Viz.Configuration.Link with the list of links.
     */
    loadLinks: function (callback) {
        Viz.Configuration.Link = new Viz.store.Link();
        Viz.Configuration.Link.on("load", function () {

            if (callback) {
                callback();
            }
            else {
                this.onAfterLoadLinks.apply(this);
            }

        }, Viz.App, {
            single: true
        });

        Viz.Configuration.Link.load();
    },

    /**
     * Indicates end of Viz.Configuration.links loading.
     */
    onAfterLoadLinks: function () {
        this.loadMachineServiceStatus();
    },

    /**
     * Loads the MachineService Status (enabled or disabled)
     */

    loadMachineServiceStatus: function () {
        Viz.Services.EnergyWCF.MachineInstance_ServiceIsEnabled({
            success: function (response) {
                Viz.Configuration.MachineInstanceEnabled = response;
                this.onAfterMachineServiceStatus();
            },
            scope: this
        });
    },

    /**
     * Indicates end of MachineService Status.
     */
    onAfterMachineServiceStatus: function () {
        this.loadEnumsStore();
        this.loadTimeZoneStore();
    },

    /**
     * Loads all the enums.
     */
    loadEnumsStore: function () {

        var storeChartLocalisation = new Viz.store.ChartLocalisation({
            autoLoad: true
        });
        var storeChartClassificationLevel = new Viz.store.ChartClassificationLevel({
            autoLoad: true
        });

        Viz.Configuration.EnumRecords = {};
        Viz.Services.CoreWCF.Enum_GetStores({
            scope: this,
            success: function (value) {
                var dico = value;
                Ext.each(dico, function (item) {
                    var store = new Ext.data.JsonStore({
                        data: item.Value
                    });
                    Viz.Configuration.EnumRecords[item.Key] = store.getRange(); // get all the records.
                });
                this.loadRendererStore();
            }
        });

    },

    /**
     * Loads all the timezones.
     */
    loadTimeZoneStore: function () {
        (new Viz.store.TimeZone()).load();
    },
    /**
     * Load the stores used in the grid columns renderer if needed.
     */
    loadRendererStore: function () {
        for (var coldef in Viz.Configuration.Columns) {
            var columns = Viz.Configuration.Columns[coldef];
            if (Ext.isArray(columns)) {
                for (var col in columns) {
                    if (columns[col].renderer && columns[col].renderer.loadStore) {
                        columns[col].renderer.loadStore();
                    }
                }
            }
        }
        this.loadConfiguration();
    },

    /**
     * Once the user is authenticated, we load it's configuration file.
     */
    loadConfiguration: function () {
        Viz.Authorization.Store = new Viz.store.AuthorizationItem();
        Viz.Authorization.Store.on("load", function () {
            Viz.util.MessageBusMgr.publish("ConfigurationSuccess");
        }, Viz.App, {
            single: true
        });
        Viz.Authorization.Store.load();

        return;
    },

    /**
     * Indicates that user configuration file was successfully loaded.
     */
    onConfigurationSuccess: function () {
        var viewPort = new Viz.MainViewPort({
            listeners: {
                afterrender: function (viewPort) {
                    this.onConfigurationDesktop.defer(500, this, [viewPort]);
                },
                scope: this
            }
        });
    },

    /*
     * Configure the desktop.
     */
    onConfigurationDesktop: function (viewPort) {
        Viz.App.Global.ViewPort = viewPort;
        this.overrideMessageBox();
        if (Viz.DisplayAsDesktop) {
            Viz.App.Global.Desktop = new Ext.Desktop(Viz.App);
        }
        else {
            Viz.App.Global.IconManager = new Ext.ux.Desktop.IconsManager({
                desktopEl: viewPort.getMainTab().body.id
            });
            var items = Viz.App.Global.ViewPort.getItemsApplicationToolbarPortalWindows();
            Viz.App.Global.IconManager.addPortalWindowIcons(items);
        }
        Viz.App.Global.ViewPort.loadChartsInCache();
        Viz.App.Global.ViewPort.openOnStartUpPortalWindows();
        Ext.MessageBox.hide.defer(1000, Ext.MessageBox);
        Viz.util.MessageBusMgr.publish("UpdateUserInfo", Viz.App.Global.User);
    },

    /**
     * Shows the about box. This handler function has the same parameter as the tool handler function.
     * @param {Ext.EventObject} event The click event.
     * @param {Ext.Element} toolEl The tool element.
     * @param {Ext.Panel} panel The host panel.
     */
    showAboutBox: function (event, toolEl, panel) {
        Viz.Services.CoreWCF.Versions_GetItem({
            success: function (version) {
                new Viz.window.AboutBox({
                    version: version,
                    toolEl: toolEl
                }).show();
            }
        });
    },

    /**
     * Override Messagebox to display (if needed) a fieldset with more details. We do this here because we need $lang to be available.
     */
    overrideMessageBox: function () {
        var diag = Ext.MessageBox.getDialog();
        var fieldSet = new Ext.form.FieldSet({
            renderTo: diag.body.child('div.ext-mb-content').createChild({
                tag: 'div',
                id: 'x-mb-details',
                style: 'margin-top: 5px; display: none;'
            }),
            title: $lang('msg_details'),
            collapsible: true,
            collapsed: true,
            border: false,
            style: 'margin: 0; padding: 0;',
            bodyStyle: 'padding: 5px 0px 0px 5px;',
            listeners: {
                afterrender: function () {
                    var legend = this.getEl().down('legend');
                    legend.setStyle({
                        padding: '0',
                        margin: '0'
                    });
                }
            }
        });
        Ext.MessageBox.show = Ext.MessageBox.show.createSequence(function (config) {
            var details = Ext.get('x-mb-details');
            var disp = Ext.isString(config.details);
            if (disp)
                details.child('div.x-fieldset-body').update(config.details);
            details.setStyle('display', (disp) ? 'block' : 'none');
        });
    },
    /**
     * Populates AuditEntity aggregated for tenant.
     */
    loadAuditEntity: function () {
        Viz.AuditEntityStore = new Viz.data.WCFJsonSimpleStore({
            serviceHandler: Viz.Services.CoreWCF.AuditEntity_GetAllAggregated,
            serviceParams: {}
        });
        Viz.AuditEntityStore.load();
    }

};

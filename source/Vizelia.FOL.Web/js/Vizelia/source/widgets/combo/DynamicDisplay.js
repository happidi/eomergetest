﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.DynamicDisplay
 * @extends Ext.form.ComboBox
 * @xtype vizComboDynamicDisplay
 */
Viz.combo.DynamicDisplay = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                // this.tpl = new Ext.XTemplate(
                /**/
                // '<tpl for=".">',
                /**/
                // '<div class="combo-result-item">',
                /**/
                // '<h3 style="height:120px" >',
                /**/
                // '<img src={Url} />',
                /**/
                // '<div>{Name}</div>',
                /**/
                // '<span>{[xindex]}/{[xcount]}</span>',
                /**/
                // '</h3>',
                /**/
                // '</div>',
                /**/
                // '</tpl>');
                var defaultConfig = {
                    mode         : 'remote',
                    pageSize     : 50,
                    listHeight   : 200,
                    displayField : 'Name',
                    resizable    : true,
                    valueField   : 'KeyDynamicDisplay',
                    store        : {
                        xtype    : 'vizStoreDynamicDisplay',
                        autoLoad : true
                    }
                    /*
                     * itemSelector : 'div.combo-result-item', tpl : this.tpl, lazyInit : false, listeners : { render : { fn : function() { if (this.view) { this.view.prepareData = function(d) { var url = Viz.Services.BuildUrlStream(www.vizelia.com.wcf._2011._01.IEnergyWCF, 'DynamicDisplay_GetStreamImage', { Key : d.KeyDynamicDisplay, width : 80, height : 80 }) d.Url = url; return d; } }; }, single : true, scope : this } }
                     */
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.DynamicDisplay.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboDynamicDisplay", Viz.combo.DynamicDisplay);

/**
 * A factory helper for creating a composite control containing the combo + a button to add new resources messages.
 * @param {Object} config config An object which may contain the following properties:
 * <ul>
 * <li><b>handler</b> : Function <div class="sub-desc">The handler for the button.</div></li>
 * <li><b>scope</b> : Object (Optional)<div class="sub-desc">The scope to pass to the handler function</div></li>
 * </ul>
 * </p>
 */
Viz.combo.DynamicDisplay.factory = function(config) {
    var fieldComboId = Ext.id();
    var preview = function(fieldComboId) {
        var dataviewId = Ext.id();
        var formWindow = Ext.getCmp(fieldComboId).findParentByType('window');

        var win = new Ext.Window({
                    modal   : true,
                    title   : $lang("msg_template"),
                    iconCls : 'viz-icon-small-display',
                    layout  : 'fit',
                    width   : 340,
                    height  : formWindow ? formWindow.getBox().height : 400,
                    x       : formWindow ? formWindow.getBox().x + formWindow.getBox().width : null,
                    y       : formWindow ? formWindow.getBox().y : null,
                    items   : [{
                                xtype    : 'vizDataViewDynamicDisplay',
                                id       : dataviewId,
                                readonly : true,
                                border   : false
                            }],
                    buttons : [{
                                text    : $lang('msg_save'),
                                iconCls : 'viz-icon-small-save',
                                handler : function(button) {
                                    var dvPanel = Ext.getCmp(dataviewId);
                                    var combo = Ext.getCmp(fieldComboId);
                                    var record = dvPanel.getSelectedRecord();
                                    if (record) {
                                        combo.setValue(record.json.KeyDynamicDisplay);
                                        button.findParentByType('window').close();
                                    }
                                },
                                scope   : this
                            }]
                });
        win.show();
    };
    return {
        fieldLabel : config.fieldLabel || $lang('msg_template'),
        xtype      : 'container',
        bodyStyle  : 'background-color: transparent',
        layout     : 'column',
        items      : [{
                    columnWidth : 1,
                    layout      : 'anchor',
                    bodyStyle   : 'background-color: transparent',
                    border      : false,
                    items       : [{
                                id         : fieldComboId,
                                fieldLabel : config.fieldLabel || $lang('msg_chart_dynamicdisplay'),
                                name       : config.name || 'KeyDynamicDisplay',
                                hiddenName : config.hiddenName || 'KeyDynamicDisplay',
                                allowBlank : Ext.isDefined(config.allowBlank) ? config.allowBlank : true,
                                anchor     : '-5',
                                xtype      : 'vizComboDynamicDisplay',
                                store      : config.store,
                                listeners  : config.listeners
                            }]
                }, {
                    xtype   : 'button',
                    iconCls : 'viz-icon-small-display',
                    tooltip : $lang('msg_dynamicdisplay_view'),
                    handler : preview.createDelegate(this, [fieldComboId])
                }, {
                    xtype   : 'button',
                    iconCls : 'viz-icon-small-add',
                    tooltip : $lang('msg_dynamicdisplay_add'),
                    handler : function() {
                        Viz.grid.DynamicDisplay.create();
                    }
                }]
    };
};

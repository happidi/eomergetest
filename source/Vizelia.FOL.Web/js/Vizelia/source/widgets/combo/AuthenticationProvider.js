﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.AuthenticationProvider
 * @extends Ext.form.ComboBox
 */
Viz.combo.AuthenticationProvider = Ext.extend(Viz.form.ComboBox, {

    /**
    * Ctor.
    * @param {Object} config The configuration options
    */
    constructor: function (config) {
        config = config || {};

        var defaultConfig = {
            fieldLabel: $lang('msg_domain'),
            mode: 'remote',
            pageSize: null,
            listHeight: null,
            displayField: 'Label',
            iconClsField: 'IconCls',
            displayIcon: true,
            valueField: 'Id',
            value: "Standard Login",
            store: new Viz.store.AuthenticationProvider({
                listeners: {
                    load: function (thiss, records, options) {
                        Ext.getCmp('newform').items.items[2].hidden = (records.length != 1);
                    }
                },
                scope: this
            }),
            listeners: {
                select: function () {
                    Ext.getCmp(this.authpasswordId).disable();
                    Ext.getCmp(this.authusernameId).disable();
                    Ext.getCmp(this.authbuttontId).disable();
                    if (this.value != "Standard Login") {
                        window.location = './sso/sso.aspx?Provider=' + this.value;
                    }
                 
                },
                scope: this
            }
        };

        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.combo.AuthenticationProvider.superclass.constructor.call(this, config);

    }

});

Ext.reg("vizComboAuthenticationProvider", Viz.combo.AuthenticationProvider);
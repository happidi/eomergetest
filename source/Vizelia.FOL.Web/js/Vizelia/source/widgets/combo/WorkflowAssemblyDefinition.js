﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.ComboBox
 * @extends Ext.form.ComboBox
 * @xtype vizComboWorkflowAssemblyDefinition
 */

Viz.combo.WorkflowAssemblyDefinition = Ext.extend(Ext.form.ComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3>{[Ext.util.Format.htmlEncode(values.AssemblyShortName)]}',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '<div><span class="value">{[Ext.util.Format.htmlEncode(values.AssemblyQualifiedName)]}</span></div>',
                        /**/
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');
                var defaultConfig = {
                    pageSize       : null,
                    loadMask       : true,
                    fieldLabel     : $lang('msg_select_workflowassemblydefinition'),
                    allowBlank     : false,
                    resizable      : false,
                    mode           : 'remote',
                    listHeight     : null,
                    hiddenName     : 'WorkflowAssemblyDefinition',
                    name           : 'WorkflowAssemblyDefinition',
                    triggerAction  : 'all',
                    displayField   : 'AssemblyQualifiedName',
                    valueField     : 'AssemblyShortName',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    tpl            : tpl,
                    itemSelector   : 'div.combo-result-item',
                    lazyRender     : true,
                    store          : new Viz.store.WorkflowAssemblyDefinition()
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.applyIf(config, this);
                Viz.combo.WorkflowAssemblyDefinition.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizComboWorkflowAssemblyDefinition", Viz.combo.WorkflowAssemblyDefinition);

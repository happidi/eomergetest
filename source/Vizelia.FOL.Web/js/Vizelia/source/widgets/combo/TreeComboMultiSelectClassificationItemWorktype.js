﻿Ext.namespace('Viz.combo');
/**
 * @class Viz.combo.TreeComboMultiSelectClassificationItemWorktype
 * @extends Ext.combo.TreeComboMultiSelect Summary.
 * @xtype viztreecombomultiselectClassificationItemWorktype
 */
Viz.combo.TreeComboMultiSelectClassificationItemWorktype = Ext.extend(Viz.combo.TreeComboMultiSelect, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    lazyInit        : true,
                    filterIgnore    : true,
                    serviceHandler  : Viz.Services.CoreWCF.ClassificationItem_GetTree,
                    serviceParams   : {
                        flgFilter   : false,
                        excludePset : true
                    },
                    tools           : null,
                    displayCheckbox : true,
                    rootVisible     : false,
                    root            : {
                        id      : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("worktype"),
                        iconCls : 'viz-icon-small-worktype',
                        text    : ''
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.TreeComboMultiSelectClassificationItemWorktype.superclass.constructor.call(this, config);
            }
        });
Ext.reg('viztreecombomultiselectClassificationItemWorktype', Viz.combo.TreeComboMultiSelectClassificationItemWorktype);

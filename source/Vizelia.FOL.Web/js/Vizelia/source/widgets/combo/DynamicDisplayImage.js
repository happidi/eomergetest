﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.DynamicDisplayImage
 * @extends Ext.form.ComboBox
 * @xtype vizComboDynamicDisplayImage
 */
Viz.combo.DynamicDisplayImage = Ext.extend(Viz.form.ClearableComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                this.tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3 style="height:65px" >',
                        /**/
                        '<img src={Url} />',
                        /**/
                        '<div>{Name}</div>',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');

                var defaultConfig = {
                    mode         : 'remote',
                    pageSize     : 25,
                    listHeight   : 200,
                    displayField : 'Name',
                    valueField   : 'KeyDynamicDisplayImage',
                    resizable    : true,
                    store        : {
                        xtype    : 'vizStoreDynamicDisplayImage',
                        autoLoad : true,
                        type     : config.type
                    },
                    lazyInit     : false,
                    itemSelector : 'div.combo-result-item'

                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                // we do not put the listeners inside the default config, otherwise they get overriden by the instance listeners
                var listeners = {
                    focus  : {
                        fn     : function() {
                            var lw = this.wrap.getWidth();
                            this.list.setSize(lw, 0);
                            this.innerList.setWidth(lw - this.list.getFrameWidth('lr'));
                            this.bufferSize = false;
                        },
                        single : true,
                        scope  : this
                    },
                    render : {
                        fn     : function() {
                            this.view.prepareData = function(d) {
                                var url = Viz.Services.BuildUrlStream('Energy.svc', 'DynamicDisplayImage_GetStreamImage', {
                                            Key    : d.KeyDynamicDisplayImage,
                                            width  : 40,
                                            height : 40
                                        })
                                d.Url = Ext.util.Format.htmlEncode(url);
                                return d;
                            };
                        },
                        single : true,
                        scope  : this
                    }
                };

                if (!Ext.isDefined(config.listeners)) {
                    Ext.apply(config, {
                                listeners : listeners
                            });
                }
                else {
                    Ext.apply(config.listeners, listeners);
                }

                Viz.combo.DynamicDisplayImage.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboDynamicDisplayImage", Viz.combo.DynamicDisplayImage);

/**
 * A factory helper for creating a composite control containing the combo + a button to add new resources messages.
 * @param {Object} config config An object which may contain the following properties:
 * <ul>
 * <li><b>handler</b> : Function <div class="sub-desc">The handler for the button.</div></li>
 * <li><b>scope</b> : Object (Optional)<div class="sub-desc">The scope to pass to the handler function</div></li>
 * </ul>
 * </p>
 */
Viz.combo.DynamicDisplayImage.factory = function(config) {
    var fieldComboId = Ext.id();
    var preview = function(fieldComboId) {
        var dataviewId = Ext.id();
        var formWindow = Ext.getCmp(fieldComboId).findParentByType('window');

        var win = new Ext.Window({
                    modal   : true,
                    title   : $lang("msg_dynamicdisplayimage_view"),
                    iconCls : 'viz-icon-small-displayimage',
                    layout  : 'fit',
                    width   : 260,
                    height  : formWindow ? formWindow.getBox().height : 400,
                    x       : formWindow ? formWindow.getBox().x + formWindow.getBox().width : null,
                    y       : formWindow ? formWindow.getBox().y : null,
                    items   : [{
                                xtype    : 'vizDataViewDynamicDisplayImage',
                                id       : dataviewId,
                                readonly : true,
                                border   : false
                            }],
                    buttons : [{
                                text    : $lang('msg_save'),
                                iconCls : 'viz-icon-small-save',
                                handler : function(button) {
                                    var dvPanel = Ext.getCmp(dataviewId);
                                    var combo = Ext.getCmp(fieldComboId);
                                    var record = dvPanel.getSelectedRecord();
                                    if (record) {
                                        combo.setValue(record.json.KeyDynamicDisplayImage);
                                        button.findParentByType('window').close();
                                    }
                                },
                                scope   : this
                            }]

                });
        win.show();
    };
    return {
        fieldLabel : config.fieldLabel || $lang('msg_dynamicdisplayimage'),
        xtype      : 'container',
        bodyStyle  : 'background-color: transparent',
        layout     : 'column',
        items      : [{
                    columnWidth : 1,
                    layout      : 'anchor',
                    bodyStyle   : 'background-color: transparent',
                    border      : false,
                    items       : [{
                                id         : fieldComboId,
                                fieldLabel : config.fieldLabel || $lang('msg_chart_dynamicdisplayimage'),
                                name       : config.name || 'KeyDynamicDisplayImage',
                                hiddenName : config.hiddenName || config.name || 'KeyDynamicDisplayImage',
                                allowBlank : Ext.isDefined(config.allowBlank) ? config.allowBlank : true,
                                anchor     : '-5',
                                xtype      : 'vizComboDynamicDisplayImage',
                                listeners  : config.listeners,
                                store      : config.store || new Viz.store.DynamicDisplayImage({
                                            type : config.type
                                        })
                            }]
                }, {
                    xtype   : 'button',
                    iconCls : 'viz-icon-small-display',
                    tooltip : $lang('msg_dynamicdisplayimage_view'),
                    handler : preview.createDelegate(this, [fieldComboId])
                }]
    };
};
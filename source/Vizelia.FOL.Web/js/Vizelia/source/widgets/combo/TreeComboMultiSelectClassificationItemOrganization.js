﻿Ext.namespace('Viz.combo');
/**
 * @class Viz.combo.TreeComboMultiSelectClassificationItemOrganization
 * @extends Ext.combo.TreeComboMultiSelect Summary.
 * @xtype viztreecombomultiselectClassificationItemOrganization
 */
Viz.combo.TreeComboMultiSelectClassificationItemOrganization = Ext.extend(Viz.combo.TreeComboMultiSelect, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    lazyInit        : true,
                    filterIgnore    : true,
                    serviceHandler  : Viz.Services.CoreWCF.ClassificationItem_GetTree,
                    serviceParams   : {
                        flgFilter   : false,
                        excludePset : true
                    },
                    tools           : null,
                    displayCheckbox : true,
                    rootVisible     : false,
                    root            : {
                        id   : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("organization"),
                        text : ''
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.TreeComboMultiSelectClassificationItemOrganization.superclass.constructor.call(this, config);
            }
        });
Ext.reg('viztreecombomultiselectClassificationItemOrganization', Viz.combo.TreeComboMultiSelectClassificationItemOrganization);

﻿Ext.namespace('Viz.combo');

/**
 * @class Viz.combo.TreeComboMultiSelectOrganization
 * @extends Ext.combo.TreeComboMultiSelect Summary.
 * @xtype viztreecombomultiselectOrganization
 */
Viz.combo.TreeComboMultiSelectOrganization = Ext.extend(Viz.combo.TreeComboMultiSelect, {
            /**
             * Ctor.
             * @param {Object}
             * config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    lazyInit        : true,
                    filterIgnore    : true,
                    serviceHandler  : Viz.Services.CoreWCF.Organization_GetTree,
                    serviceParams   : {
                        flgFilter : false
                    },
                    tools           : null,
                    displayCheckbox : true,
                    rootVisible     : false,
                    root            : {
                        id      : ' ',
                        iconCls : 'viz-icon-small-organization',
                        text    : $lang('msg_tree_organization_root')
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.TreeComboMultiSelectOrganization.superclass.constructor.call(this, config);
            }
        });

Ext.reg('viztreecombomultiselectOrganization', Viz.combo.TreeComboMultiSelectOrganization);


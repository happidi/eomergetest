﻿Ext.namespace('Viz.combo');
/**
 * @class Viz.combo.TreeComboMultiSelectClassificationItemMeter
 * @extends Ext.combo.TreeComboMultiSelect Summary.
 * @xtype viztreecombomultiselectClassificationItemMeter
 */
Viz.combo.TreeComboMultiSelectClassificationItemMeter = Ext.extend(Viz.combo.TreeComboMultiSelect, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    lazyInit        : true,
                    filterIgnore    : true,                   
                    serviceHandler  : Viz.Services.CoreWCF.ClassificationItem_GetTree,
                    serviceParams   : {
                        flgFilter   : false,
                        excludePset : true
                    },
                    tools           : null,
                    displayCheckbox : true,
                    rootVisible     : false,
                    root            : {
                        id      : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("meter"),
                        iconCls : 'viz-icon-small-meter',
                        text    : ''
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.TreeComboMultiSelectClassificationItemMeter.superclass.constructor.call(this, config);
            }
        });
Ext.reg('viztreecombomultiselectClassificationItemMeter', Viz.combo.TreeComboMultiSelectClassificationItemMeter);

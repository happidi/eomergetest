﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.Css
 * @extends Ext.form.ComboBox
 */
Viz.combo.Css = Ext.extend(Viz.form.ClearableComboBox, {
            /**
             * @cfg {String} selector the selector to retrieve the css class.
             */
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                this.tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3 style="height:36px" >',
                        /**/
                        '<div class="{[Ext.util.Format.htmlEncode(values.Value)]} x-icon-combo-icon-large"></div>',
                        /**/
                        '<div style="margin-left:40px" >{[Ext.util.Format.htmlEncode(values.Display)]}</div>',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');

                var defaultConfig = {
                    mode          : 'local',
                    listHeight    : 300,
                    pageSize      : 0,
                    triggerAction : 'all',
                    displayField  : 'Display',
                    valueField    : 'Value',
                    resizable     : true,
                    itemSelector  : 'div.combo-result-item',
                    anyMatch      : true,
                    caseSensitive : false,
                    tpl           : this.tpl,
                    store         : new Ext.data.ArrayStore({
                                fields   : ['Value', 'Display'],
                                data     : Ext.util.CSS.getArrayMatch(config.selector, config.refreshCache || false),
                                sortInfo : {
                                    field     : 'Display',
                                    direction : 'ASC'
                                }
                            })
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.Css.superclass.constructor.call(this, config);

            }
        });
Ext.reg("vizComboCss", Viz.combo.Css);

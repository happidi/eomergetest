﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.Enum
 * @extends Viz.form.ComboBox
 */
Viz.combo.Enum = Ext.extend(Viz.form.ComboBox, {
    /**
    * @cfg {String} enumTypeName The enum typename.
    */
    enumTypeName: '',

    /**
    * @cfg {bool} sortByValues True to sort by Values, false to sort by MsgCode (default to false).
    */
    sortByValues: false,

    /**
    * @cfg {Array} valuesToRemove List of values to remove from the store.
    */
    valuesToRemove: [],

    /**
    * Ctor.
    * @param {Object} config The configuration options
    */
    constructor: function (config) {
        config = config || {};


        var defaultConfig = {
            mode: 'local',
            pageSize: null,
            listHeight: null,
            displayField: 'Label',
            valueField: 'Value',
            iconClsField: 'IconCls',
            store: new Viz.store.Enum({
                serviceParams: {
                    enumTypeName: config.enumTypeName || this.enumTypeName
                },
                valuesToRemove: config.valuesToRemove,
                valuesToInclude: config.valuesToInclude
            })
        };

        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.combo.Enum.superclass.constructor.call(this, config);

    }

});
Ext.reg("vizComboEnum", Viz.combo.Enum);
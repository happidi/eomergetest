﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.PsetUsageName The list of possible usage names for a pset.
 * @extends Ext.form.ComboBox
 * @xtype vizComboPsetUsageName
 */
Viz.combo.PsetUsageName = Ext.extend(Viz.form.ComboBox, {

            constructor : function(config) {
                config = config || {};

                this.store = new Viz.store.PsetUsageName();
                this.store.load();

                var defaultConfig = {
                    displayIcon    : true,
                    iconClsField   : 'IconCls',
                    loadMask       : true,
                    allowBlank     : false,
                    fieldLabel     : $lang('msg_pset_usagename'),
                    mode           : 'local',
                    triggerAction  : 'all',
                    displayField   : 'Label',
                    valueField     : 'Value',
                    forceSelection : true,
                    typeAhead      : false,
                    store          : this.store,
                    pageSize       : null,
                    listHeight     : null
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.PsetUsageName.superclass.constructor.call(this, config);

            }

        });

Ext.reg("vizComboPsetUsageName", Viz.combo.PsetUsageName);
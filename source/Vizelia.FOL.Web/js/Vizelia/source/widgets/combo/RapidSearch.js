﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.RapidSearch The Location combobox.
 * @extends Ext.form.ComboBox
 * @xtype vizComboLocation
 */
Viz.combo.RapidSearch = Ext.extend(Viz.form.ClearableComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var pathTypeString = config.pathTypeString || $lang('msg_location_path');
                var searchName = config.searchName || '';
                var tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3 style="height:16px;" >',
                        /**/
                        '<div style="padding-right:4px;" class="x-icon-combo-icon {[Ext.util.Format.htmlEncode(values.IconCls)]}"></div>',
                        /**/
                        '<div class="ellipsis">{' + (config['displayField'] || 'Name') + '}</div>',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/

                        /**/
                        '<div class="ellipsis"><span class="title">' + pathTypeString + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.LongPath)]}</span></div>', '</div>',
                        /**/
                        '</tpl>');
                var defaultConfig = {
                    pageSize       : 50,
                    loadMask       : true,
                    fieldLabel     : $lang('msg_select_location'),
                    allowBlank     : false,
                    resizable      : true,
                    mode           : 'remote',
                    listHeight     : 200,
                    listWidth      : 400,
                    hiddenName     : 'KeyChildren',
                    name           : 'KeyChildren',
                    triggerAction  : 'all',
                    displayField   : 'Name',
                    valueField     : 'KeyChildren',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    tpl            : tpl,
                    itemSelector   : 'div.combo-result-item',
                    lazyRender     : true,
                    store          : {
                        xtype : 'vizStoreRapidSearch'
                    },
                    searchName     : searchName,
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.applyIf(config, this);
                Viz.combo.RapidSearch.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizComboRapidSearch", Viz.combo.RapidSearch);
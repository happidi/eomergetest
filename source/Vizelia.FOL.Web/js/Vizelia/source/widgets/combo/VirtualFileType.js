﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.VirtualFileType
 * @extends Ext.form.ComboBox
 */
Viz.combo.VirtualFileType = Ext.extend(Viz.form.ClearableComboBox, {
            /**
             * @cfg {String} selector the selector to retrieve the VirtualFileType class.
             */
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var defaultConfig = {
                    mode          : 'local',
                    listHeight    : 70,
                    pageSize      : 0,
                    triggerAction : 'all',
                    displayField  : 'Display',
                    valueField    : 'Value',
                    displayIcon   : true,
                    iconClsField  : 'IconCls',
                    anyMatch      : true,
                    caseSensitive : false,
                    store         : new Ext.data.ArrayStore({
                                fields   : ['Value', 'Display', 'IconCls'],
                                data     : [['Vizelia.FOL.BusinessEntities.Chart', $lang('msg_chart'), 'viz-icon-small-chart'], ['Vizelia.FOL.BusinessEntities.Playlist', $lang('msg_playlist'), 'viz-icon-small-playlist'], ['Vizelia.FOL.BusinessEntities.Meter', $lang('msg_meter'), 'viz-icon-small-meter']],
                                // ['Vizelia.FOL.BusinessEntities.PortalTab', $lang('msg_portaltab'), 'viz-icon-small-portalwindow']
                                sortInfo : {
                                    field     : 'Display',
                                    direction : 'ASC'
                                }
                            })
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.VirtualFileType.superclass.constructor.call(this, config);

            }
        });
Ext.reg("vizComboVirtualFileType", Viz.combo.VirtualFileType);

﻿Ext.namespace('Viz.combo');
/**
 * @class Viz.combo.TreeComboClassificationItemMeter
 * @extends Ext.combo.TreeCombo Summary.
 * @xtype viztreecomboClassificationItemMeter
 */
Viz.combo.TreeComboClassificationItemMeter = Ext.extend(Viz.combo.TreeCombo, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var rootEntity = Viz.Configuration.GetClassificationItemDefinitionEntityFromCategory("meter");
                rootEntity.Level = 1;
                var defaultConfig = {
                    serviceHandler  : Viz.Services.EnergyWCF.MeterClassification_GetTree,
                    treeHeight      : 200,
                    searchable      : true,
                    serviceParams   : {
                        KeyChart : config.KeyChart,
                        existing : config.existing === true ? true : false,
                        entity   : rootEntity
                    },
                    entityKey       : 'KeyClassificationItem',
                    displayCheckbox : false,
                    rootVisible     : false,
                    root            : {
                        id      : rootEntity.KeyClassificationItem,
                        iconCls : 'viz-icon-small-meter',
                        text    : rootEntity.Title,
                        entity  : rootEntity
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.TreeComboClassificationItemMeter.superclass.constructor.call(this, config);
            }
        });
Ext.reg('viztreecomboClassificationItemMeter', Viz.combo.TreeComboClassificationItemMeter);

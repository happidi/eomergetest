﻿Ext.namespace('Viz.combo');
/**
 * @class Viz.combo.TreeComboClassificationItemEventLog
 * @extends Ext.combo.TreeCombo Summary.
 * @xtype viztreecomboClassificationItemEventLog
 */
Viz.combo.TreeComboClassificationItemEventLog = Ext.extend(Viz.combo.TreeCombo, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler  : Viz.Services.CoreWCF.ClassificationItem_GetTree,
                    treeHeight      : 200,
                    serviceParams   : {
                        flgFilter   : false,
                        excludePset : true
                    },
                    entityKey       : 'KeyClassificationItem',
                    displayCheckbox : false,
                    rootVisible     : false,
                    root            : {
                        id      : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("eventlog"),
                        iconCls : 'viz-icon-small-eventlog',
                        text    : ''
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.TreeComboClassificationItemEventLog.superclass.constructor.call(this, config);
            }
        });
Ext.reg('viztreecomboClassificationItemEventLog', Viz.combo.TreeComboClassificationItemEventLog);

﻿Ext.namespace('Viz.combo');

/**
 * @class Viz.combo.TreeComboMultiSelectSpatial
 * @extends Ext.combo.TreeComboMultiSelect Summary.
 * @xtype viztreecombomultiselectSpatial
 */
Viz.combo.TreeComboMultiSelectSpatial = Ext.extend(Viz.combo.TreeComboMultiSelect, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    lazyInit        : true,
                    filterIgnore    : true,
                    pathAttribute   : null,
                    serviceHandler  : Viz.Services.CoreWCF.Spatial_GetTree,
                    serviceParams   : {
                        flgFilter              : true,
                        ExcludeMetersAndAlarms : true
                    },
                    tools           : null,
                    displayCheckbox : true,
                    rootVisible     : false,
                    stateful        : true,
                    root            : {
                        id      : ' ',
                        iconCls : 'viz-icon-small-site',
                        checked : undefined,
                        text    : $lang("msg_tree_spatial_root")
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.TreeComboMultiSelectSpatial.superclass.constructor.call(this, config);
            }
        });

Ext.reg('viztreecombomultiselectSpatial', Viz.combo.TreeComboMultiSelectSpatial);

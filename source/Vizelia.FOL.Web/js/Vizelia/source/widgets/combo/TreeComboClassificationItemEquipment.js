﻿Ext.namespace('Viz.combo');

/**
 * @class Viz.combo.TreeComboClassificationItemEquipment
 * @extends Ext.combo.TreeCombo Summary.
 */
Viz.combo.TreeComboClassificationItemEquipment = Ext.extend(Viz.combo.TreeCombo, {

			/**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler  : Viz.Services.CoreWCF.ClassificationItem_GetTree,
                    treeHeight      : 200,
                    serviceParams   : {
                        flgFilter   : false,
                        excludePset : true
                    },
                    entityKey       : 'KeyClassificationItem',
                    displayCheckbox : false,
                    rootVisible     : false,
                    root            : {
                        id      : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("equipment"),
                        iconCls : 'viz-icon-small-equipment',
                        text    : ''
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.TreeComboClassificationItemEquipment.superclass.constructor.call(this, config);
            }
        });
Ext.reg('viztreecomboClassificationItemEquipment', Viz.combo.TreeComboClassificationItemEquipment);

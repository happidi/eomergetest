﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.Playlist
 * @extends Ext.form.ComboBox
 * @xtype vizComboPlaylist
 */
Viz.combo.Playlist = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var defaultConfig = {
                    mode         : 'remote',
                    pageSize     : 50,
                    listHeight   : 200,
                    displayField : 'Name',
                    valueField   : 'KeyPlaylist',
                    store        : {
                        xtype    : 'vizStorePlaylist',
                        autoLoad : true
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.Playlist.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboPlaylist", Viz.combo.Playlist);
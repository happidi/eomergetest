﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.Localization The Localization combobox.
 * @extends Ext.form.ComboBox
 * @xtype vizComboLocalization
 */
Viz.combo.Localization = Ext.extend(Ext.form.ComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                // var tpl = new Ext.XTemplate('<tpl for="."><div style ="cursor:hand;padding:2px;" class="combo-result-item"><div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>', '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc" style="font-size:12px"><div style="color:DarkBlue" >{Key}</div><div>' + '{Value}</div></div></div></div>', '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div></div></tpl>');
                var tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3>{Key}',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '<div><span style="font-size:12px" class="value">{[Ext.util.Format.htmlEncode(values.Value)]}</span></div>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');
                var defaultConfig = {
                    pageSize       : 50,
                    loadMask       : true,
                    fieldLabel     : $lang('msg_localization_key'),
                    allowBlank     : false,
                    resizable      : true,
                    mode           : 'remote',
                    listHeight     : 300,
                    hiddenName     : 'Key',
                    name           : 'Localizaton',
                    triggerAction  : 'all',
                    displayField   : 'Key',
                    valueField     : 'Key',
                    // selectedClass : 'x-box-blue',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 1,
                    tpl            : tpl,
                    itemSelector   : 'div.combo-result-item',
                    lazyRender     : true,
                    store          : {
                        xtype : 'vizStoreLocalization'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.applyIf(config, this);
                Viz.combo.Localization.superclass.constructor.call(this, config);
            }
        });
/**
 * A factory helper for creating a composite control containing the combo + a button to add new resources messages.
 * @param {Object} config config An object which may contain the following properties:
 * <ul>
 * <li><b>handler</b> : Function <div class="sub-desc">The handler for the button.</div></li>
 * <li><b>scope</b> : Object (Optional)<div class="sub-desc">The scope to pass to the handler function</div></li>
 * </ul>
 * </p>
 */
Viz.combo.Localization.factory = function(config) {
    var fieldComboId = Ext.id();
    var addLocalization = function() {
        var win = new Ext.Window({
                    modal   : true,
                    title   : $lang("msg_add"),
                    iconCls : 'viz-icon-small-localization-add',
                    layout  : 'fit',
                    width   : 500,
                    height  : 200,
                    items   : [{
                                xtype              : 'vizFormLocalization',
                                mode               : 'create',
                                fieldComboId       : fieldComboId,
                                onSaveSuccessCombo : function(form, action) {
                                    var combo = Ext.getCmp(form.fieldComboId);
                                    if (combo && action.result && action.result.data && action.result.data.Key)
                                        combo.setValue(action.result.data.Key);
                                    return true;
                                }
                            }]
                });
        win.show();
    };
    return {
        fieldLabel : config.fieldLabel || $lang('msg_label'),
        xtype      : 'container',
        bodyStyle  : 'background-color: transparent',
        layout     : 'column',
        items      : [{
                    columnWidth : 1,
                    layout      : 'anchor',
                    bodyStyle   : 'background-color: transparent',
                    border      : false,
                    items       : [{
                                id         : fieldComboId,
                                anchor     : '-20',
                                xtype      : 'vizComboLocalization',
                                name       : config.name || 'MsgCode',
                                hiddenName : config.name || 'MsgCode'
                            }]
                }, {
                    xtype   : 'button',
                    iconCls : 'viz-icon-small-localization',
                    tooltip : $lang('msg_localization_add'),
                    handler : config.handler || addLocalization,
                    scope   : config.scope || this
                }]
    };
};
Ext.reg("vizComboLocalization", Viz.combo.Localization);
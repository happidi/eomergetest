﻿Ext.namespace('Viz.combo');
/**
* @class Viz.combo.TreeComboClassificationItemAlarmDefinition
* @extends Ext.combo.TreeCombo Summary.
* @xtype viztreecomboClassificationItemAlarmDefinition
*/
Viz.combo.TreeComboClassificationItemAlarmDefinition = Ext.extend(Viz.combo.TreeCombo, {
    /**
    * Ctor.
    * @param {Object} config The configuration options
    */
    constructor: function (config) {
        config = config || {};
        var defaultConfig = {
            serviceHandler: Viz.Services.CoreWCF.ClassificationItem_GetTree,
            treeHeight: 200,
            serviceParams: {
                flgFilter: false,
                excludePset: true
            },
            entityKey: 'KeyClassificationItem',
            displayCheckbox: false,
            rootVisible: false,
            root: {
                id: Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("alarm"),
                iconCls: 'viz-icon-small-alarm',
                text: ''
            }
        };
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);
        Viz.combo.TreeComboClassificationItemAlarmDefinition.superclass.constructor.call(this, config);
    }
});
Ext.reg('viztreecomboClassificationItemAlarmDefinition', Viz.combo.TreeComboClassificationItemAlarmDefinition);

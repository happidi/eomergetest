﻿Ext.namespace("Viz.form");
/**
 * @class Viz.form.Combo Standard Vizelia combobox. The following options can be overrided :
 * <ul>
 * <li>pageSize (default to 50)</li>
 * <li>loadMask (default to true)</li>
 * <li>allowBlank (default to true)</li>
 * <li>resizable (default to false)</li>
 * <li>tpl</li>
 * </ul>
 * A specialized combobox should expose the following properties :
 * <ul>
 * <li>displayField </li>
 * <li>valueField</li>
 * <li>hiddenName</li>
 * <li>name</li>
 * </ul>
 * If the combo is in mode 'local', pageSize should be set to null.
 * @extends Ext.form.ComboBox
 * @xtype vizCombo
 */
Viz.form.ComboBox = Ext.extend(Ext.form.ComboBox, {
            /**
             * @cfg {Boolean} insideFilter True if the combo is displayed inside a filter column.
             */
            insideFilter    : false,
            /**
             * @cfg {Boolean} displayIcon True to display icon, false otherwise.
             */
            displayIcon     : false,

            /**
             * @cfg {Boolean} forceLoadStore If this is set to true, then the store will be loaded even in read only mode.
             */
            forceLoadStore  : false,
            /**
             * @cfg {String} iconClsField The field containing the value of the icon cls.
             */
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor     : function(config) {
                config = config || {};
                var defaultConfig = {
                    pageSize       : 50,
                    loadMask       : true,
                    allowBlank     : true,
                    mode           : 'remote',
                    resizable      : false,
                    listHeight     : 300,
                    triggerAction  : 'all',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    lazyRender     : true,
                    anyMatch       : false,
                    caseSensitive  : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                if (config.mode == "local") {
                    if (config.store && Ext.isFunction(config.store.load) && config.store.proxy) {
                        // the store has already by constructed so we have to force the load, applying autoload to the config is too late.
                        this.store = config.store;
                        config.store.load();
                    }
                    else {
                        Ext.apply(config.store, {
                                    autoLoad : true
                                });
                    }
                }
                if (config.configStore) {
                    Ext.apply(config.store, config.configStore);
                }
                Viz.form.ComboBox.superclass.constructor.call(this, config);

                if (this.displayIcon) {

                    var tpl = new Ext.XTemplate('<tpl for="."><div class="x-combo-list-item x-icon-combo-item {[this.customRenderer(values.' + this.iconClsField + ')]} ">{[Ext.util.Format.htmlEncode(values.' + this.displayField + ')]}</div></tpl>', {
                                customRenderer : this.iconClsRenderer
                            });

                    this.tpl = config.tpl || tpl;
                    this.on("setvalue", this.setIconCls, this);
                }
            },

            onRender        : function() {
                Viz.form.ComboBox.superclass.onRender.apply(this, arguments);
                // creates a icon space holder
                if (this.displayIcon) {
                    var wrap = this.el.up('div.x-form-field-wrap');
                    /*
                     * this.wrap.applyStyles({ position : 'relative' });
                     */
                    this.el.addClass('x-icon-combo-input');
                    this.flag = Ext.DomHelper.append(wrap, {
                                tag   : 'div',
                                style : 'position:absolute'
                            });

                    if (this.defaultIconCls) {
                        this.flag.className = 'x-icon-combo-icon ' + this.defaultIconCls;
                    }
                }
            },
            /**
             * Overrides doQuery query allowing the query action to search anyMatch and non caseSensitive
             * @param {String} query The SQL query to execute
             * @param {Boolean} forceAll <tt>true</tt> to force the query to execute even if there are currently fewer characters in the field than the minimum specified by the <tt>{@link #minChars}</tt> config option. It also clears any filter previously saved in the current store (defaults to <tt>false</tt>)
             */
            doQuery         : function(q, forceAll) {
                q = Ext.isEmpty(q) ? '' : q;
                var qe = {
                    query    : q,
                    forceAll : forceAll,
                    combo    : this,
                    cancel   : false
                };
                if (this.fireEvent('beforequery', qe) === false || qe.cancel) {
                    return false;
                }
                q = qe.query;
                forceAll = qe.forceAll;
                if (forceAll === true || (q.length >= this.minChars)) {
                    if (this.lastQuery !== q) {
                        this.lastQuery = q;
                        if (this.mode == 'local') {
                            this.selectedIndex = -1;
                            if (forceAll) {
                                this.store.clearFilter();
                            }
                            else {
                                this.store.filter(this.displayField, q, this.anyMatch, this.caseSensitive);
                            }
                            this.onLoad();
                        }
                        else {
                            this.store.baseParams[this.queryParam] = q;
                            this.store.load({
                                        params : this.getParams(q)
                                    });
                            this.expand();
                        }
                    }
                    else {
                        this.selectedIndex = -1;
                        this.onLoad();
                    }
                }
            },
            setIconCls      : function() {
                if (this.displayIcon) {
                    var rec = this.store.query(this.valueField, this.getValue()).itemAt(0);
                    if (rec && this.flag) {
                        if (this.insideFilter)
                            this.flag.className = 'x-icon-combo-icon x-icon-combo-icon-filter ' + this.iconClsRenderer(rec.get(this.iconClsField));

                        else
                            this.flag.className = 'x-icon-combo-icon ' + this.iconClsRenderer(rec.get(this.iconClsField));
                    }
                }
            },

            /**
             * Overridable function to change the value of the iconCls before it is used.
             * @param {String} value
             * @return {String}
             */
            iconClsRenderer : function(value) {
                return Ext.util.Format.htmlEncode(value);
            }
        });
Ext.reg("vizCombo", Viz.form.ComboBox);
﻿Ext.namespace('Viz.combo');

/**
 * @class Viz.combo.TreeComboSpatial
 * @extends Ext.combo.TreeCombo Summary.
 */
Viz.combo.TreeComboSpatial = Ext.extend(Viz.combo.TreeCombo, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler  : Viz.Services.CoreWCF.Spatial_GetTree,
                    treeHeight      : 200,
                    serviceParams   : {
                        flgFilter              : true,
                        ExcludeMetersAndAlarms : false
                    },
                    searchable      : true,
                    entityKey       : null,
                    displayCheckbox : false,
                    rootVisible     : false,
                    root            : {
                        id      : ' ',
                        iconCls : 'viz-icon-small-site',
                        checked : undefined,
                        text    : $lang("msg_tree_spatial_root")
                    }
                };
                if (Ext.isBoolean(config.ExcludeMetersAndAlarms)) {
                    defaultConfig.serviceParams.ExcludeMetersAndAlarms = config.ExcludeMetersAndAlarms;
                }
                var forcedConfig = {

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.TreeComboSpatial.superclass.constructor.call(this, config);
            }
        });

Ext.reg('viztreecomboSpatial', Viz.combo.TreeComboSpatial);

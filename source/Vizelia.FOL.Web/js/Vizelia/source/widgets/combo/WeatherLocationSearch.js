﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.WeatherLocationSearch
 * @extends Ext.form.ComboBox
 */
Viz.combo.WeatherLocationSearch = Ext.extend(Viz.form.ComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    listHeight    : 200,
                    triggerAction : 'all',
                    displayField : 'Name',
                    valueField   : 'Name',
                    resizable     : true,
                    store         : new Viz.store.WeatherLocationSearch()
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.WeatherLocationSearch.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizComboWeatherLocationSearch", Viz.combo.WeatherLocationSearch);
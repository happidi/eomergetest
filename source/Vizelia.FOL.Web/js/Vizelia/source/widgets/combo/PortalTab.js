﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.PortalTab
 * @extends Ext.form.ComboBox
 * @xtype vizComboPortalTab
 */
Viz.combo.PortalTab = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor         : function(config) {
                config = config || {};
                this.tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3 style="height:36px" >',
                        /**/
                         '<tpl if="Ext.isEmpty(values.PortalWindowIconCls)==false"><div class="{[Ext.util.Format.htmlEncode(values.PortalWindowIconCls)]} x-icon-combo-icon-large"></div></tpl>',
                         /**/
                         '<tpl if="Ext.isEmpty(values.PortalWindowIconCls)==true"><div class="{[Viz.util.Portal.getCssFromImage(values.PortalWindowKeyImage,32,32)]} x-icon-combo-icon-large"></div></tpl>',
                       /**/
                        '<div style="margin-left:40px" >{[Ext.util.Format.htmlEncode(values.FullTitle)]}</div>',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');
                var defaultConfig = {
                    mode         : 'remote',
                    pageSize     : 50,
                    resizable    : true,
                    listHeight   : 200,
                    displayField : 'FullTitle',
                    valueField   : 'KeyPortalTab',
                    itemSelector : 'div.combo-result-item',
                    lazyInit     : false,
                    tpl          : this.tpl,
                    store        : {
                        xtype          : 'vizStorePortalTab',
                        serviceHandler : Viz.Services.EnergyWCF.PortalTab_GetStore,
                        autoLoad       : true
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.PortalTab.superclass.constructor.call(this, config);
                this.on('afterrender', this.onRenderUpdateTitle, this, {
                            single : true
                        })
            },

            onRenderUpdateTitle : function() {
                if (this.view)
                    this.view.prepareData = function(d) {
                        d.FullTitle = Viz.util.Portal.localizeTitle(d.FullTitle);
                        return d;
                    };
            }

        });

Ext.reg("vizComboPortalTab", Viz.combo.PortalTab);

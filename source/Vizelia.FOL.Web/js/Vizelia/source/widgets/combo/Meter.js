﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.Meter The Meter combobox.
 * @extends Ext.form.ComboBox
 * @xtype vizComboMeter
 */
Viz.combo.Meter = Ext.extend(Viz.form.ClearableComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3 style="height:16px;" >',
                        /**/
                        '<div style="padding-right:4px;" class="x-icon-combo-icon {[Ext.util.Format.htmlEncode(values.IconCls)]}"></div>',
                        /**/
                        '<div class="ellipsis">{[Ext.util.Format.htmlEncode(values.Name)]}</div>',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '<div class="ellipsis"><span class="title">' + $lang('msg_location_path') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.LocationLongPath)]}</span></div>',
                        /**/
                        '<div class="ellipsis"><span class="title">' + $lang('msg_meter_classification') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.ClassificationItemLongPath)]}</span></div>',
                        /**/
                        // '<div style="overflow:hidden;"><span class="title">' + $lang('msg_localid') + ':</span><span class="value">{LocalId}</span></div>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');
                var defaultConfig = {
                    pageSize       : 50,
                    loadMask       : true,
                    fieldLabel     : $lang('msg_select_meter'),
                    allowBlank     : false,
                    resizable      : true,
                    mode           : 'remote',
                    listHeight     : 300,
                    hiddenName     : 'KeyMeter',
                    name           : 'KeyMeter',
                    triggerAction  : 'all',
                    displayField   : 'Name',
                    valueField     : 'KeyMeter',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    tpl            : tpl,
                    itemSelector   : 'div.combo-result-item',
                    lazyRender     : true,
                    store          : {
                        xtype : 'vizStoreMeter'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.applyIf(config, this);
                Viz.combo.Meter.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizComboMeter", Viz.combo.Meter);
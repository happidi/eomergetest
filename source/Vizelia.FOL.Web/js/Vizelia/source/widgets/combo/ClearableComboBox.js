﻿Ext.namespace("Viz.form");
/**
 * @class Viz.combo.ClearableComboBox
 * @extends Viz.form.ComboBox
 */

Viz.form.ClearableComboBox = Ext.extend(Viz.form.ComboBox, {

            initComponent   : function() {
                Viz.form.ClearableComboBox.superclass.initComponent.call(this);
                this.addEvents('clear');

                this.triggerConfig = {
                    tag   : 'span',
                    cls   : 'x-form-twin-triggers',
                    style : 'padding-right:2px', // padding needed to prevent IE from clipping 2nd trigger button
                    cn    : [{
                                tag : "img",
                                src : Ext.BLANK_IMAGE_URL,
                                cls : "x-form-trigger"
                            }, {
                                tag : "img",
                                src : Ext.BLANK_IMAGE_URL,
                                cls : this.trigger2Class ? this.trigger2Class : "x-form-trigger x-form-clear-trigger"
                            }]
                };
            },

            getTrigger      : function(index) {
                return this.triggers[index];
            },

            initTrigger     : function() {
                var ts = this.trigger.select('.x-form-trigger', true);
                this.wrap.setStyle('overflow', 'hidden');
                var triggerField = this;
                ts.each(function(t, all, index) {
                            t.hide = function() {
                                var w = triggerField.wrap.getWidth();
                                this.dom.style.display = 'none';
                                triggerField.el.setWidth(w - triggerField.trigger.getWidth());
                            };
                            t.show = function() {
                                var w = triggerField.wrap.getWidth();
                                this.dom.style.display = '';
                                triggerField.el.setWidth(w - triggerField.trigger.getWidth());
                            };
                            var triggerIndex = 'Trigger' + (index + 1);

                            if (this['hide' + triggerIndex]) {
                                t.dom.style.display = 'none';
                            }
                            t.on("click", this['on' + triggerIndex + 'Click'], this, {
                                        preventDefault : true
                                    });
                            t.addClassOnOver('x-form-trigger-over');
                            t.addClassOnClick('x-form-trigger-click');
                        }, this);
                this.triggers = ts.elements;
                if (this.ownerCt instanceof Ext.Container) {
                    this.ownerCt.bubble(function(c) {
                                c.on('expand', this.onBug, this);
                            }, this);
                }
            },

            onBug           : function() {
                this.wrap.setWidth(this.el.getWidth() + this.trigger.getWidth());
            },
            onTrigger1Click : function() {
                this.onTriggerClick()
            },
            onTrigger2Click : function() {
                this.clearValue();
                this.doQuery(this.getRawValue());
                this.fireEvent("clear", this);
            }
        });

Ext.ComponentMgr.registerType('vizClearableCombo', Viz.form.ClearableComboBox);

﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.AzManTask
 * @extends Ext.form.ComboBox
 */
Viz.combo.AzManTask = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var defaultConfig = { 
                    mode         : 'local',
                    pageSize     : null,
                    listHeight   : null,
                    displayField : 'Name',
                    valueField   : 'Id',
                    store        : {
                        xtype: 'vizStoreAzManTask',
                        autoLoad : true
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.AzManTask.superclass.constructor.call(this, config);

            }
        });

        Ext.reg("vizComboAzManTask", Viz.combo.AzManTask);
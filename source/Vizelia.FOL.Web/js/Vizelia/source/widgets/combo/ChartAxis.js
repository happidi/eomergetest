﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.ChartAxis
 * @extends Ext.form.ComboBox
 */
Viz.combo.ChartAxis = Ext.extend(Viz.form.ComboBox, {   
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor  : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode         : 'local',
                    pageSize     : null,
                    listHeight   : null,
                    displayField : 'Name',
                    valueField   : 'KeyChartAxis',
                    store        : {
                        xtype         : 'vizStoreChartAxis',
                        autoLoad      : true,
                        KeyChart : config.KeyChart
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.ChartAxis.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizComboChartAxis", Viz.combo.ChartAxis);
﻿Ext.namespace('Viz.combo');
/**
 * @class Viz.combo.TreeComboClassificationItemKPI
 * @extends Ext.combo.TreeCombo Summary.
 * @xtype viztreecomboClassificationItemKPI
 */
Viz.combo.TreeComboClassificationItemKPI = Ext.extend(Viz.combo.TreeCombo, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var rootEntity = Viz.Configuration.GetClassificationItemDefinitionEntityFromCategory("kpi");
                rootEntity.Level = 1;

                var defaultConfig = {
                    serviceHandler  : Viz.Services.CoreWCF.ClassificationItem_GetTree,
                    treeHeight      : 200,
                    serviceParams   : {
                        flgFilter   : false,
                        excludePset : true
                    },
                    entityKey       : 'KeyClassificationItem',
                    displayCheckbox : false,
                    rootVisible     : true,
                    root            : {
                        id      : rootEntity.KeyClassificationItem,
                        iconCls : 'viz-icon-small-chart-kpi',
                        text    : rootEntity.Title,
                        entity  : rootEntity
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.TreeComboClassificationItemKPI.superclass.constructor.call(this, config);
            }
        });
Ext.reg('viztreecomboClassificationItemKPI', Viz.combo.TreeComboClassificationItemKPI);

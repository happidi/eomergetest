﻿Ext.namespace("Viz.combo"); 

/**
 * @class Viz.combo.Link
 * @extends Ext.form.ComboBox
 */
Viz.combo.Link = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
    
            constructor : function(config) {
                config = config || {};
                
               
                var defaultConfig = { 
                    mode         : 'remote',
                    pageSize     : null,
                    listHeight   : null,
                    displayField: 'LocalId',
                    valueField   : 'KeyLink',
                    store        : {
                        xtype: 'vizStoreLink',
                        autoLoad : true,
                        serviceHandler: config.serviceHandler,
                        currentId: config.currentId
                    }
                };

                var forcedConfig = {};
                
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                
                Viz.combo.Link.superclass.constructor.call(this, config);

            }
        });

        Ext.reg("vizComboLink", Viz.combo.Link);
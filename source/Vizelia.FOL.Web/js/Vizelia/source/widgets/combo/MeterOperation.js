﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.MeterOperation
 * @extends Ext.form.ComboBox
 */
Viz.combo.MeterOperation = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var defaultConfig = {
                    mode         : 'local',
                    pageSize     : null,
                    listHeight   : null,
                    displayField : 'LocalId',
                    valueField   : 'KeyMeterOperation',
                    store        : config.store || {
                        xtype             : 'vizStoreMeterOperation',
                        KeyMeter          : config.KeyMeter,
                        KeyMeterOperation : config.KeyMeterOperation,
                        Order             : config.Order
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.MeterOperation.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboMeterOperation", Viz.combo.MeterOperation);
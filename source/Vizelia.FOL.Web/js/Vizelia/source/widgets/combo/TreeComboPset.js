﻿Ext.namespace('Viz.combo');

/**
 * @class Viz.combo.TreeComboPset
 * @extends Ext.combo.TreeCombo Summary.
 */
Viz.combo.TreeComboPset = Ext.extend(Viz.combo.TreeCombo, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler  : Viz.Services.CoreWCF.Pset_GetTree,
                    treeHeight      : 200,
                    serviceParams   : {
                        flgFilter : true
                    },
                    displayCheckbox : false,
                    rootVisible     : false,
                    root            : {
                        id      : ' ',
                        iconCls : 'viz-icon-small-pset',
                        checked : undefined,
                        text    : $lang("msg_pset_root")
                    }
                };
                var forcedConfig = {

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.TreeComboPset.superclass.constructor.call(this, config);
            }
        });

Ext.reg('viztreecomboPset', Viz.combo.TreeComboPset);

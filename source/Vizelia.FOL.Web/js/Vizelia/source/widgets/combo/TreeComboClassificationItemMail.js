﻿Ext.namespace('Viz.combo');

/**
 * @class Viz.combo.TreeComboClassificationItemMail
 * @extends Ext.combo.TreeCombo Summary.
 */
Viz.combo.TreeComboClassificationItemMail = Ext.extend(Viz.combo.TreeCombo, {

			/**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler  : Viz.Services.CoreWCF.ClassificationItem_GetTree,
                    treeHeight      : 200,
                    serviceParams   : {
                        flgFilter   : false,
                        excludePset : true
                    },
                    entityKey       : 'KeyClassificationItem',
                    displayCheckbox : false,
                    rootVisible     : false,
                    root            : {
                        id      : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("mail"),
                        iconCls : 'viz-icon-small-mail',
                        text    : ''
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.TreeComboClassificationItemMail.superclass.constructor.call(this, config);
            }
        });
Ext.reg('viztreecomboClassificationItemMail', Viz.combo.TreeComboClassificationItemMail);

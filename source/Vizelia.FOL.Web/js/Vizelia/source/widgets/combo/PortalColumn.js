﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.PortalColumn
 * @extends Ext.form.ComboBox
 * @xtype vizComboPortalColumn
 */
Viz.combo.PortalColumn = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                this.tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3 style="height:36px" >',
                        /**/
                        '<div class="{[Ext.util.Format.htmlEncode(values.PortalWindowIconCls)]} x-icon-combo-icon-large"></div>',
                        /**/
                        '<div style="margin-left:40px" >{[Ext.util.Format.htmlEncode(values.FullTitle)]}</div>',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');

                var defaultConfig = {
                    mode         : 'remote',
                    pageSize     : 50,
                    listHeight   : 200,
                    resizable    : true,
                    displayField : 'FullTitle',
                    valueField   : 'KeyPortalColumn',
                    itemSelector : 'div.combo-result-item',
                    tpl          : this.tpl,
                    store        : {
                        xtype          : 'vizStorePortalColumn',
                        serviceHandler : Viz.Services.EnergyWCF.PortalColumn_GetStore,
                        autoLoad       : true
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.PortalColumn.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizComboPortalColumn", Viz.combo.PortalColumn);
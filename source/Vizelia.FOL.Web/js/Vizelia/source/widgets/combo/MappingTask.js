﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.MappingTask The MappingTask combobox.
 * @extends Ext.form.ComboBox
 * @xtype vizComboMappingTask
 */
Viz.combo.MappingTask = Ext.extend(Viz.form.ClearableComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3 style="height:16px;" >',
                        /**/
                        //'<div style="padding-right:4px;" class="x-icon-combo-icon {IconCls}"></div>',
                        /**/
                        '<div class="ellipsis">{[Ext.util.Format.htmlEncode(values.Name)]}</div>',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '<div class="ellipsis"><span class="title">' + $lang('msg_address') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.Address)]}</span></div>',
                        /**/
                        '<div class="ellipsis"><span class="title">' + $lang('msg_username') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.Username)]}</span></div>',
                        /**/
                        '<div class="ellipsis"><span class="title">' + $lang('msg_description') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.Description)]}</span></div>',
                        /**/
                        // '<div style="overflow:hidden;"><span class="title">' + $lang('msg_localid') + ':</span><span class="value">{LocalId}</span></div>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');
                var defaultConfig = {
                    pageSize       : 50,
                    loadMask       : true,
                    fieldLabel     : $lang('msg_select_MappingTask'),
                    allowBlank     : false,
                    resizable      : true,
                    mode           : 'remote',
                    listHeight     : 300,
                    hiddenName     : 'KeyMappingTask',
                    name           : 'KeyMappingTask',
                    triggerAction  : 'all',
                    displayField   : 'Name',
                    valueField     : 'KeyMappingTask',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    tpl            : tpl,
                    itemSelector   : 'div.combo-result-item',
                    lazyRender     : true,
                    store          : {
                        xtype : 'vizStoreMappingTask'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.applyIf(config, this);
                Viz.combo.MappingTask.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizComboMappingTask", Viz.combo.MappingTask);
﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.AlgorithmInputDefinition
 * @extends Ext.form.ComboBox
 * @xtype vizComboAlgorithmInputDefinition
 */
Viz.combo.AlgorithmInputDefinition = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3>{[Ext.util.Format.htmlEncode(values.Name)]}',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '<tpl if="Description"><div><span class="value">{[Ext.util.Format.htmlEncode(values.Description)]}</span></div></tpl>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');
                var defaultConfig = {
                    pageSize       : 50,
                    loadMask       : true,
                    resizable      : true,
                    mode           : 'remote',
                    listHeight     : 300,
                    hiddenName     : 'KeyAlgorithmInputDefinition',
                    name           : 'AlgorithmInputDefinition',
                    triggerAction  : 'all',
                    displayField   : 'Name',
                    valueField     : 'KeyAlgorithmInputDefinition',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    tpl            : tpl,
                    itemSelector   : 'div.combo-result-item',
                    lazyRender     : true,
                    store          : {
                        xtype        : 'vizStoreAlgorithmInputDefinition',
                        KeyAlgorithm : config.KeyAlgorithm,
                        autoLoad     : true
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.AlgorithmInputDefinition.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboAlgorithmInputDefinition", Viz.combo.AlgorithmInputDefinition);
﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.DynamicDisplayColorTemplate
 * @extends Ext.form.ComboBox
 * @xtype vizComboDynamicDisplayColorTemplate
 */
Viz.combo.DynamicDisplayColorTemplate = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var defaultConfig = {
                    mode         : 'remote',
                    pageSize     : 50,
                    listHeight   : 200,
                    displayField : 'Name',
                    valueField   : 'KeyDynamicDisplayColorTemplate',
                    store        : {
                        xtype    : 'vizStoreDynamicDisplayColorTemplate',
                        autoLoad : true
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.DynamicDisplayColorTemplate.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboDynamicDisplayColorTemplate", Viz.combo.DynamicDisplayColorTemplate);
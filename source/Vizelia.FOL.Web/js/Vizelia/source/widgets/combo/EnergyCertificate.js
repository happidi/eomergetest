﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.EnergyCertificate
 * @extends Ext.form.ComboBox
 * @xtype vizComboEnergyCertificate
 */
Viz.combo.EnergyCertificate = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var defaultConfig = {
                    mode         : 'remote',
                    pageSize     : 50,
                    listHeight   : 200,
                    displayField : 'Name',
                    valueField   : 'KeyEnergyCertificate',
                    store        : {
                        xtype    : 'vizStoreEnergyCertificate',
                        autoLoad : true
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.EnergyCertificate.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboEnergyCertificate", Viz.combo.EnergyCertificate);
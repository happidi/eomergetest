﻿Ext.namespace('Viz.combo');
/**
 * @class Viz.combo.TreeComboClassificationItemWork
 * @extends Ext.combo.TreeCombo Summary.
 * @xtype viztreecomboClassificationItemWork
 */
Viz.combo.TreeComboClassificationItemWork = Ext.extend(Viz.combo.TreeCombo, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler  : Viz.Services.CoreWCF.ClassificationItem_GetTree,
                    treeHeight      : 200,
                    searchable      : true,
                    statefull       : true,
                    stateId         : 'viztreecomboClassificationItemWork',
                    serviceParams   : {
                        flgFilter           : false,
                        excludePset         : true,
                        excludeRelationship : true
                    },
                    entityKey       : 'KeyClassificationItem',
                    displayCheckbox : false,
                    rootVisible     : false,
                    root            : {
                        id      : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("work"),
                        iconCls : 'viz-icon-small-work',
                        text    : ''
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.TreeComboClassificationItemWork.superclass.constructor.call(this, config);
            }
        });
Ext.reg('viztreecomboClassificationItemWork', Viz.combo.TreeComboClassificationItemWork);

﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.DesktopBackground
 * @extends Ext.form.ComboBox
 * @xtype vizComboDesktopBackground
 */
Viz.combo.DesktopBackground = Ext.extend(Viz.form.ClearableComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                this.tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3>',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '<img style="height:50px;border:1px gray solid;" src="images/desktop/{[Ext.util.Format.htmlEncode(values.Value)]}" />',
                        /**/
                        '<div>{[Ext.util.Format.htmlEncode(values.Label)]}</div>',
                        /**/
                        '</h3>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');

                var defaultConfig = {
                    mode           : 'remote',
                    pageSize       : 50,
                    listHeight     : 200,
                    displayField   : 'Label',
                    valueField     : 'Value',
                    triggerAction  : 'all',
                    loadMask       : true,
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    mode           : 'remote',
                    autoLoad       : true,
                    lazyRender     : true,
                    lazyInit       : false,
                    itemSelector   : 'div.combo-result-item'
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.apply(config, {

                            store : new Viz.store.DesktopBackground({
                                        autoLoad  : config.autoLoad
                                    })

                        }

                );
                Viz.combo.DesktopBackground.superclass.constructor.call(this, config);
            },
        });

Ext.reg("vizComboDesktopBackground", Viz.combo.DesktopBackground);

﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.Occupant The Algorithm combobox.
 * @extends Ext.form.ComboBox
 * @xtype vizComboAlgorithm
 */
Viz.combo.Algorithm = Ext.extend(Viz.form.ClearableComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor     : function(config) {
                config = config || {};
                var tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3>{[Ext.util.Format.htmlEncode(values.Name)]}',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '<tpl if="Description"><div><span class="value">{[Ext.util.Format.htmlEncode(values.Description)]}</span></div></tpl>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');
                var defaultConfig = {
                    pageSize       : 50,
                    loadMask       : true,
                    fieldLabel: $lang('msg_algorithm'),
                    allowBlank     : false,
                    resizable      : true,
                    mode           : 'remote',
                    listHeight     : 300,
                    hiddenName: 'KeyAlgorithm',
                    name: 'Algorithm',
                    triggerAction  : 'all',
                    displayField   : 'Name',
                    valueField: 'KeyAlgorithm',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    tpl            : tpl,
                    itemSelector   : 'div.combo-result-item',
                    lazyRender     : true,
                    store: new Viz.store.Algorithm(),
                    trigger2Class  : 'x-form-trigger x-form-search-trigger'
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.applyIf(config, this);
                Viz.combo.Algorithm.superclass.constructor.call(this, config);
            },

            onTrigger2Click : function() {
                var formWindow = this.findParentByType('window');

                var KeyAlgorithm = this.getValue();
                if (KeyAlgorithm) {
                    Viz.Services.EnergyWCF.Algorithm_GetItem({
                        Key: KeyAlgorithm,
                                scope   : this,
                                success : function(script) {
                                    if (script) {
                                        var win = new Ext.Window({
                                            title: $lang("msg_algorithm") + ' : ' + script.Name,
                                                    iconCls : 'viz-icon-small-python',
                                                    layout  : 'fit',
                                                    width   : 400,
                                                    height  : formWindow ? formWindow.getBox().height : 400,
                                                    x       : formWindow ? formWindow.getBox().x + formWindow.getBox().width : null,
                                                    y       : formWindow ? formWindow.getBox().y : null,
                                                    items   : [{
                                                                xtype    : 'codemirror',
                                                                border   : false,
                                                                value    : script.Code,
                                                                readOnly : true
                                                            }]
                                                });
                                        win.show();
                                    }
                                }

                            })

                }
            }
        });
                Ext.reg("vizComboAlgorithm", Viz.combo.Algorithm);
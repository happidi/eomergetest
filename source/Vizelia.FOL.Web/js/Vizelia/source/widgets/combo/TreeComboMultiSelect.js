﻿Ext.namespace('Viz.combo');

/**
 * @class Viz.combo.TreeComboMultiSelect
 * @extends Ext.combo.TreeCombo Treecombo showing the multiple nodes that are selected.
 * @xtype viztreecombomultiselect
 */
Viz.combo.TreeComboMultiSelect = Ext.extend(Viz.combo.TreeCombo, {
            pathAttribute      : 'text',
            selectedPath       : [],
            valueDelimiter     : ',',

            /**
             * @cfg {Bool} formatAsArray <tt>true</tt> to get value as an array, <tt>false</tt> to get value as a concatened string of values with the valueDelimiter. Defaults to <tt>false</tt>.
             */
            formatAsArray      : false,
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor        : function(config) {
                config = config || {};
                var defaultConfig = {
                    tools           : {},
                    renderFieldBtns : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Ext.applyIf(config, this);

                Viz.combo.TreeComboMultiSelect.superclass.constructor.call(this, config);
            },

            initComponent      : function() {
                Ext.apply(this, {
                            readonly        : true,
                            items           : new Ext.util.MixedCollection(false),
                            usedRecords     : new Ext.util.MixedCollection(false),
                            addedRecords    : [],
                            remoteLookup    : [],
                            hideTrigger     : true,
                            grow            : false,
                            resizable       : false,
                            multiSelectMode : false,
                            preRenderValue  : null,
                            extraItemCls    : 'viz-indent'
                        });
                Viz.combo.TreeComboMultiSelect.superclass.initComponent.apply(this, arguments);
                if (!this.lazyInit) {
                    var tree = this.getTree();
                    tree.on("checkchange", this.onCheckChange, this);
                    this.on("click", this.onClick, this);
                }
            },

            /**
             * Overrides the superclass event handler. In the case of a multiselect treeview we don't use the node click.
             * @param {Ext.tree.TreeNode} node
             * @param {event} e
             */
            onTreeNodeClick    : function(node, e) {
                // this.setRawValue(node.text);
                // this.value = node.id;
                this.fireEvent('select', this, node);
                this.collapse();
            },

            /**
             * Event handler each time a node is checked / unchecked.
             * @param {Ext.tree.TreeNode} node The node that was checked / unchecked.
             * @param {Boolean} checked The value of the checkbox
             */
            onCheckChange      : function(node, checked) {

                if (checked) {
                    this.addItem(node);
                }
                else {
                    var item = this.items.get(node.id);
                    item.preDestroy(true);
                }
                this.fireEvent('select', this, node);
            },

            onRender           : function(ct, position) {
                Viz.combo.TreeComboMultiSelect.superclass.onRender.call(this, ct, position);
                if (this.lazyInit) {
                    var tree = this.getTree();
                    tree.on("checkchange", this.onCheckChange, this);
                    this.on("click", this.onClick, this);
                }
                this.el.dom.removeAttribute('name');

                var extraClass = (this.stackItems === true) ? 'x-superboxselect-stacked' : '';
                if (this.renderFieldBtns) {
                    extraClass += ' x-superboxselect-display-btns';
                }
                this.el.removeClass('x-form-text').addClass('x-superboxselect-input-field');

                this.wrapEl = this.el.wrap({
                            tag : 'ul'
                        });

                this.outerWrapEl = this.wrapEl.wrap({
                            tag : 'div',
                            cls : 'x-form-text x-superboxselect ' + extraClass
                        });

                this.inputEl = this.el.wrap({
                            tag : 'li',
                            cls : 'x-superboxselect-input'
                        });

                if (this.renderFieldBtns) {
                    this.setupFieldButtons().manageClearBtn();
                }
                this.autoSize();
            },

            setupFieldButtons  : function() {
                this.buttonWrap = this.outerWrapEl.createChild({
                            cls : 'x-superboxselect-btns'
                        });

                this.buttonClear = this.buttonWrap.createChild({
                            tag : 'div',
                            cls : 'x-superboxselect-btn-clear ' + this.clearBtnCls
                        });

                this.buttonExpand = this.buttonWrap.createChild({
                            tag : 'div',
                            cls : 'x-superboxselect-btn-expand ' + this.expandBtnCls
                        });

                this.initButtonEvents();

                return this;
            },
            manageClearBtn     : function() {
                if (!this.renderFieldBtns || !this.rendered) {
                    return this;
                }
                var cls = 'x-superboxselect-btn-hide';
                if (this.items.getCount() === 0) {
                    this.buttonClear.addClass(cls);
                }
                else {
                    this.buttonClear.removeClass(cls);
                }
                return this;
            },
            initButtonEvents   : function() {
                this.buttonClear.addClassOnOver('x-superboxselect-btn-over').on('click', function(e) {
                            e.stopEvent();
                            if (this.disabled) {
                                return;
                            }
                            this.clearValue();
                            this.el.focus();
                        }, this);

                this.buttonExpand.addClassOnOver('x-superboxselect-btn-over').on('click', function(e) {
                            e.stopEvent();
                            if (this.disabled) {
                                return;
                            }
                            if (this.isExpanded()) {
                                this.multiSelectMode = false;
                            }
                            else if (this.pinList) {
                                this.multiSelectMode = true;
                            }
                            this.onTriggerClick();

                        }, this);
            },
            removeButtonEvents : function() {
                this.buttonClear.removeAllListeners();
                this.buttonExpand.removeAllListeners();
                return this;
            },
            clearCurrentFocus  : function() {
                if (this.currentFocus) {
                    this.currentFocus.onLnkBlur();
                    this.currentFocus = null;
                }
                return this;
            },

            onFocus            : function() {
                this.outerWrapEl.addClass(this.focusClass);

                // Ext.ux.form.SuperBoxSelect.superclass.onFocus.call(this);
            },

            onBlur             : function() {
                this.outerWrapEl.removeClass(this.focusClass);
                this.clearCurrentFocus();
                if (this.el.dom.value !== '') {
                    this.applyEmptyText();
                    this.autoSize();
                }

                // Ext.ux.form.SuperBoxSelect.superclass.onBlur.call(this);
            },
            /**
             * Clears all values from the component.
             * @methodOf Ext.ux.form.SuperBoxSelect
             * @name clearValue
             * @param {Boolean} supressRemoveEvent [Optional] When true, the 'removeitem' event will not fire for each item that is removed.
             */
            clearValue         : function(supressRemoveEvent) {
                Ext.ux.form.SuperBoxSelect.superclass.clearValue.call(this);
                this.preventMultipleRemoveEvents = supressRemoveEvent || this.supressClearValueRemoveEvents || false;
                this.removeAllItems();
                this.preventMultipleRemoveEvents = false;
                this.fireEvent('clear', this);
                return this;
            },

            /**
             * Reset the component. Equivalent to clearValue.
             */
            reset              : function() {
                this.clearValue(false);
            },

            /**
             * @private Use clearValue instead
             */
            removeAllItems     : function() {
                Ext.ux.form.SuperBoxSelect.superclass.clearValue.call(this);
                this.items.each(function(item) {
                            item.preDestroy(true);
                        }, this);
                this.manageClearBtn();
                return this;
            },

            /**
             * Adds an array of items to the SuperBoxSelect component if the {@link #Ext.ux.form.SuperBoxSelect-allowAddNewData} config is set to true.
             * @methodOf Ext.ux.form.SuperBoxSelect
             * @name addItem
             * @param {Array} newItemObjects An Array of object literals containing the property names and values for an item. The property names must match those specified in {@link #Ext.ux.form.SuperBoxSelect-displayField}, {@link #Ext.ux.form.SuperBoxSelect-valueField} and {@link #Ext.ux.form.SuperBoxSelect-classField}
             */
            addItems           : function(newItemObjects) {
                if (Ext.isArray(newItemObjects)) {
                    Ext.each(newItemObjects, function(item) {
                                this.addItem(item);
                            }, this);
                }
                else {
                    this.addItem(newItemObjects);
                }
            },
            /**
             * Adds a new non-existing item to the SuperBoxSelect component if the {@link #Ext.ux.form.SuperBoxSelect-allowAddNewData} config is set to true. This method should be used in place of addItem from within the newitem event handler.
             * @methodOf Ext.ux.form.SuperBoxSelect
             * @name addNewItem
             * @param {Object} newItemObject An object literal containing the property names and values for an item. The property names must match those specified in {@link #Ext.ux.form.SuperBoxSelect-displayField}, {@link #Ext.ux.form.SuperBoxSelect-valueField} and {@link #Ext.ux.form.SuperBoxSelect-classField}
             */
            addNewItem         : function(newItemObject) {
                this.addItem(newItemObject, true);
            },
            /**
             * Adds an item to the SuperBoxSelect component if the {@link #Ext.ux.form.SuperBoxSelect-allowAddNewData} config is set to true.
             * @methodOf Ext.ux.form.SuperBoxSelect
             * @name addItem
             * @param {Ext.tree.TreeNode} newItemObject An object literal containing the property names and values for an item. The property names must match those specified in {@link #Ext.ux.form.SuperBoxSelect-displayField}, {@link #Ext.ux.form.SuperBoxSelect-valueField} and {@link #Ext.ux.form.SuperBoxSelect-classField}
             */
            addItem            : function(newItemObject, /* hidden param */forcedAdd) {

                // var val = newItemObject[this.valueField];
                var val = newItemObject.text;

                if (this.disabled) {
                    return false;
                }
                if (this.preventDuplicates && this.hasValue(val)) {
                    return;
                }
                this.addNode(newItemObject);

                return true;
            },

            addItemBox         : function(id, itemVal, itemDisplay, itemCaption, itemClass, itemStyle, node) {
                var parseStyle = function(s) {
                    var ret = '';
                    if (typeof s == 'function') {
                        ret = s.call();
                    }
                    else if (typeof s == 'object') {
                        for (var p in s) {
                            ret += p + ':' + s[p] + ';';
                        }
                    }
                    else if (typeof s == 'string') {
                        ret = s + ';';
                    }
                    return ret;
                };
                var itemKey = Ext.id(null, 'sbx-item');
                var itemKey = id;
                var box = new Ext.ux.form.SuperBoxSelectItem({
                            owner     : this,
                            disabled  : this.disabled,
                            renderTo  : this.wrapEl,// this.wrapEl,
                            cls       : this.extraItemCls + ' ' + itemClass,
                            // style : parseStyle(this.extraItemStyle) + ' ' + itemStyle,
                            caption   : itemCaption,
                            display   : itemDisplay,
                            value     : itemVal,
                            key       : itemKey,
                            path      : node.getPathOriginal(this.pathAttribute),
                            listeners : {
                                'remove'    : function(item) {

                                    if (this.fireEvent('beforeremoveitem', this, item.value) === false) {
                                        return;
                                    }
                                    this.items.removeKey(item.key);
                                    // if (this.removeValuesFromStore) {
                                    // if (this.usedRecords.containsKey(item.value)) {
                                    // this.store.add(this.usedRecords.get(item.value));
                                    // this.usedRecords.removeKey(item.value);
                                    // this.sortStore();
                                    // if (this.view) {
                                    // this.view.render();
                                    // }
                                    // }
                                    // }
                                    if (!this.preventMultipleRemoveEvents) {
                                        this.fireEvent.defer(250, this, ['removeitem', this, item.value]);
                                    }
                                },

                                destroy     : function(item) {
                                    var node = this.getTree().getNodeById(item.key);
                                    if (node.attributes.checked == true) {
                                        node.getUI().checkbox.checked = false;
                                    }
                                    this.autoSize().manageClearBtn();// .validateValue();
                                    if (!this.isExpanded())
                                        this.getTree().hide();
                                },

                                afterremove : function() {
                                    this.autoSize().manageClearBtn();
                                },
                                scope       : this
                            }
                        });
                box.render();

                box.hidden = this.el.insertSibling({
                            tag   : 'input',
                            type  : 'hidden',
                            value : itemVal,
                            name  : (this.hiddenName || this.name)
                        }, 'before');

                this.items.add(itemKey, box);
                this.applyEmptyText().autoSize().manageClearBtn(); // .validateValue();
            },

            applyEmptyText     : function() {
                this.setRawValue('');
                if (this.items.getCount() > 0) {
                    this.el.removeClass(this.emptyClass);
                    this.setRawValue('');
                    return this;
                }
                if (this.rendered && this.emptyText && this.getRawValue().length < 1) {
                    this.setRawValue(this.emptyText);
                    this.el.addClass(this.emptyClass);
                }
                return this;
            },

            onResize           : function(w, h, rw, rh) {
                var reduce = Ext.isIE6 ? 4 : Ext.isIE7 ? 1 : Ext.isIE8 ? 1 : 0;
                if (this.wrapEl) {
                    this._width = w;
                    this.outerWrapEl.setWidth(w - reduce);
                    if (this.renderFieldBtns) {
                        reduce += (this.buttonWrap.getWidth() + 20);
                        this.wrapEl.setWidth(w - reduce);
                    }
                }
                // Viz.combo.TreeComboMultiSelect.superclass.onResize.call(this, w, h, rw, rh);
                this.autoSize();
            },

            autoSize           : function() {
                if (!this.rendered) {
                    return this;
                }
                if (!this.metrics) {
                    this.metrics = Ext.util.TextMetrics.createInstance(this.el);
                }
                var el = this.el, v = el.dom.value, d = document.createElement('div');

                if (v === "" && this.emptyText && this.items.getCount() < 1) {
                    v = this.emptyText;
                }
                d.appendChild(document.createTextNode(v));
                v = d.innerHTML;
                d = null;
                v += "&#160;";
                var w = Math.max(this.metrics.getWidth(v) + 24, 24);
                if (typeof this._width != 'undefined') {
                    w = Math.min(this._width, w);
                }
                this.el.setWidth(w);

                if (Ext.isIE) {
                    this.el.dom.style.top = '0';
                }
                this.getTree().getEl().alignTo(this.getAlignElement(), 'tl-bl?', this.getAlignOffset());
                this.fireEvent("autosize");
                return this;
            },
            /**
             * Adds a new node
             * @param {Ext.tree.TreeNode} node
             */
            addNode            : function(node) {
                var id = node.id;
                var display = node.text;
                var caption = node.text;// this.getCaption(record);
                var val = node.attributes.Key || node.id;
                var cls = node.attributes.iconCls;// this.classField // ? record.data[this.classField] : '';
                var style = ''; // this.styleField // ? record.data[this.styleField] : '';
                this.addItemBox(id, val, display, caption, cls, style, node);
                // this.fireEvent('additem', this, val, record);
            },
            /**
             * Returns an Array of the values of the checked nodes of the treeview.
             * @return {Array} an array of values.
             */
            getValueAsArray    : function() {
                var retVal = [];
                this.items.each(function(item) {
                            retVal.push(item.value);
                        });
                return retVal;
            },
            selectPath         : function(path) {
                var explore = function(bSuccess, oSelNode) {
                    if (!bSuccess)
                        return;
                    oSelNode.checked = true;
                    oSelNode.ui.toggleCheck(true);
                };
                if (!Ext.isEmpty(this.treePanel))
                    this.treePanel.selectPath(path, this.pathAttribute, explore);
            },
            /**
             * Returns an Array of the paths of the checked nodes of the treeview.
             * @return {Array} an array of values.
             */
            getPathAsArray     : function() {
                var retVal = [];
                this.items.each(function(item) {
                            retVal.push(item.path);
                        });
                return retVal;
            },

            /**
             * If the formatAsArray config is <tt>true</tt> returns a String value containing a concatenated list of item values. The list is concatenated with the {@link #Ext.ux.form.SuperBoxSelect-valueDelimiter}. If the formatAsArray config is <tt>false</tt> returns an Array.
             * @return {Object} a String value containing a concatenated list of item values or an Array.
             */
            getValue           : function() {
                var retVal = this.getValueAsArray();
                if (this.formatAsArray)
                    return retVal;
                else
                    return retVal.join(this.valueDelimiter);
            },

            /**
             * Returns the raw data value which may or may not be a valid, defined value. To return a normalized value see {@link #getValue}.
             * @return {Mixed} value The field value
             */
            getRawValue        : function() {
                var v = this.rendered ? this.getValue() : Ext.value(this.value, '');
                if (v === this.emptyText) {
                    v = '';
                }
                return v;
            },

            /**
             * Returns an Array of the checked nodes of the treeview.
             * @return {Array} an array of nodes.
             */
            getValueEx         : function() {
                return this.getTree().getChecked();
            }
        });

Ext.reg('viztreecombomultiselect', Viz.combo.TreeComboMultiSelect);

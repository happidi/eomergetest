﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.FontFamily
 * @extends Ext.form.ComboBox
 */
Viz.combo.FontFamily = Ext.extend(Viz.form.ComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    triggerAction  : 'all',
                    displayField   : 'MsgCode',
                    valueField     : 'Value',
                    resizable      : true,
                    store          : new Viz.store.FontFamily()

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.FontFamily.superclass.constructor.call(this, config);

                var cls = 'x-combo-list';
                this.tpl = '<tpl for="."><div class="' + cls + '-item"><span ext:qtip="{[Ext.util.Format.htmlEncode(values.' + this.valueField + ')]}" style="line-height:1.5;vertical-align:top;font-size:large;font-family:{[Ext.util.Format.htmlEncode(values.' + this.valueField + ')]}">{[Ext.util.Format.htmlEncode(values.' + this.displayField + ')]}</span></div></tpl>';
            }
        });
Ext.reg("vizComboFontFamily", Viz.combo.FontFamily);
﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.CalendarEventCategory
 * @extends Ext.form.ComboBox
 * @xtype vizComboCalendarEventCategory
 */
Viz.combo.CalendarEventCategory = Ext.extend(Viz.form.ClearableComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                this.tpl = new Ext.XTemplate('<tpl for=".">',
                        /**/
                        '<div class="x-combo-list-item">',
                        /**/
                        '<table><tbody><tr>',
                        /**/
                        '<td>',
                        /**/
                        '<div class="zoninglegend-color" style="background:rgb({[Ext.util.Format.htmlEncode(values.ColorR)]},{[Ext.util.Format.htmlEncode(values.ColorG)]},{[Ext.util.Format.htmlEncode(values.ColorB)]});"></div></td>',
                        /**/
                        '<td>{[Ext.util.Format.htmlEncode(values.Label)]}</td>',
                        /**/
                        '</tr></tbody></table>',
                        /**/
                        '</div></tpl>');

                var defaultConfig = {
                    mode         : 'remote',
                    editable     : false,
                    pageSize     : null,
                    listHeight   : null,
                    displayField : 'Label',
                    valueField   : 'LocalId',
                    store        : {
                        xtype : 'vizStoreCalendarEventCategory'
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.CalendarEventCategory.superclass.constructor.call(this, config);
                this.store.autoLoad = false;
                this.on({
                            render : function() {
                                var wrap = this.el.up('div.x-form-field-wrap');
                                this.wrap.applyStyles({
                                            position : 'relative'
                                        });
                                this.el.addClass('x-icon-combo-input');
                                this.color = Ext.DomHelper.append(wrap, {
                                            tag   : 'div',
                                            style : 'position:absolute'
                                        });
                            },
                            clear  : function() {
                                this.color.className = '';
                            },
                            scope  : this
                        });

            },

            setColorCls : function(value) {
                if (this.store) {
                    var rec = this.store.query(this.valueField, value).itemAt(0);
                    if (rec) {
                        this.color.className = 'x-icon-combo-icon zoninglegend-color';
                        this.color.style.background = String.format('rgb({0},{1},{2})', rec.data.ColorR, rec.data.ColorG, rec.data.ColorB);
                    }
                }
            },

            setValue    : function(value) {
                Viz.combo.CalendarEventCategory.superclass.setValue.call(this, value);
                if (!Ext.isEmpty(value) && this.rendered)
                    this.setColorCls.defer(200, this, [value]);
            }
        });

Ext.reg("vizComboCalendarEventCategory", Viz.combo.CalendarEventCategory);
﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.Palette
 * @extends Ext.form.ComboBox
 * @xtype vizComboPalette
 */
Viz.combo.Palette = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                this.tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3 style="height:35px" >',
                        /**/
                        '<img src={[Ext.util.Format.htmlEncode(values.Url)]} />',
                        /**/
                        '<div>{[Ext.util.Format.htmlEncode(values.Label)]}</div>',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');

                var defaultConfig = {
                    mode         : 'remote',
                    pageSize     : 10,
                    resizable    : true,
                    listHeight   : 200,
                    displayField : 'Label',
                    valueField   : 'KeyPalette',
                    store        : {
                        xtype    : 'vizStorePalette',
                        autoLoad : true
                    },
                    lazyInit     : false,
                    itemSelector : 'div.combo-result-item'
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                var listeners = {
                    focus  : {
                        fn     : function() {
                            var lw = this.wrap.getWidth();
                            this.list.setSize(lw, 0);
                            this.innerList.setWidth(lw - this.list.getFrameWidth('lr'));
                            this.bufferSize = false;
                        },
                        single : true,
                        scope  : this
                    },
                    render : {
                        fn     : function() {
                            this.view.prepareData = function(d) {
                                var url = Viz.Services.BuildUrlStream('Energy.svc', 'Palette_GetStreamImage', {
                                            Key    : d.KeyPalette,
                                            height : 12,
                                            width  : 400
                                        })
                                d.Url = url;
                                return d;
                            };
                        },
                        single : true,
                        scope  : this
                    }
                };

                if (!Ext.isDefined(config.listeners)) {
                    Ext.apply(config, {
                                listeners : listeners
                            });
                }
                else {
                    Ext.apply(config.listeners, listeners);
                }

                Viz.combo.Palette.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboPalette", Viz.combo.Palette);

Viz.combo.Palette.factory = function(config) {
    var fieldComboId = Ext.id();
    var preview = function(fieldComboId) {
        var dataviewId = Ext.id();
        var formWindow = Ext.getCmp(fieldComboId).findParentByType('window');

        var win = new Ext.Window({
                    modal   : true,
                    title   : $lang("msg_palette_view"),
                    iconCls : 'viz-icon-small-palette',
                    layout  : 'fit',
                    width   : 260,
                    height  : formWindow ? formWindow.getBox().height : 400,
                    x       : formWindow ? formWindow.getBox().x + formWindow.getBox().width : null,
                    y       : formWindow ? formWindow.getBox().y : null,
                    items   : [{
                                xtype    : 'vizDataViewPalette',
                                id       : dataviewId,
                                readonly : true,
                                border   : false
                            }],
                    buttons : [{
                                text    : $lang('msg_save'),
                                iconCls : 'viz-icon-small-save',
                                handler : function(button) {
                                    var dvPanel = Ext.getCmp(dataviewId);
                                    var combo = Ext.getCmp(fieldComboId);
                                    var record = dvPanel.getSelectedRecord();
                                    if (record) {
                                        combo.setValue(record.json.KeyPalette);
                                        button.findParentByType('window').close();
                                    }
                                },
                                scope   : this
                            }]

                });
        win.show();
    };
    return {
        fieldLabel : config.fieldLabel || $lang('msg_palette'),
        xtype      : 'container',
        bodyStyle  : 'background-color: transparent',
        layout     : 'column',
        items      : [{
                    columnWidth : 1,
                    layout      : 'anchor',
                    bodyStyle   : 'background-color: transparent',
                    border      : false,
                    items       : [{
                                id         : fieldComboId,
                                fieldLabel : config.fieldLabel || $lang('msg_palette'),
                                name       : config.name || 'KeyPalette',
                                hiddenName : config.hiddenName || config.name || 'KeyPalette',
                                allowBlank : Ext.isDefined(config.allowBlank) ? config.allowBlank : true,
                                anchor     : '-5',
                                xtype      : 'vizComboPalette',
                                listeners  : config.listeners
                            }]
                }, {
                    xtype   : 'button',
                    iconCls : 'viz-icon-small-display',
                    tooltip : $lang('msg_palette_view'),
                    handler : preview.createDelegate(this, [fieldComboId])
                }]
    };
};
﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.UserFiltered The UserFiltered combobox.
 * @extends Ext.form.ComboBox
 * @xtype vizComboUserFiltered
 */
Viz.combo.UserFiltered = Ext.extend(Ext.form.ComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var tpl = new Ext.XTemplate(
                       
                        '<tpl for=".">',
                       
                        '<div class="combo-result-item">',

                        '<h3>{[Ext.util.Format.htmlEncode(values[\'ProviderUserKey.Occupant.FullName\'])]} ({[Ext.util.Format.htmlEncode(values[\'ProviderUserKey.Occupant.Id\'])]})',
                       
                        '<span>{[xindex]}/{[xcount]}</span>',
                       
                        '</h3>',

                        '<tpl if="values[\'ProviderUserKey.Occupant.TelephoneNumbers\'] != \'\'"><div><span class="title">' + $lang('msg_occupant_telephone') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values[\'ProviderUserKey.Occupant.TelephoneNumbers\'])]}</span></div></tpl>',

                        '<tpl if="values[\'ProviderUserKey.Occupant.ElectronicMailAddresses\'] != \'\'"><div><span class="title">' + $lang('msg_occupant_email') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values[\'ProviderUserKey.Occupant.ElectronicMailAddresses\'])]}</span></div></tpl>',

                        '<tpl if="values[\'ProviderUserKey.Occupant.LocationShortPath\'] != \'\'"><div><span class="title">' + $lang('msg_occupant_location') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values[\'ProviderUserKey.Occupant.LocationShortPath\'])]}</span></div></tpl>',

                        '<tpl if="values[\'ProviderUserKey.Occupant.ClassificationItemWorktypeLongPath\'] != \'\'"><div><span class="title">' + $lang('msg_occupant_worktype') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values[\'ProviderUserKey.Occupant.ClassificationItemWorktypeLongPath\'])]}</span></div></tpl>',

                        '<tpl if="values[\'ProviderUserKey.Occupant.ClassificationItemOrganizationLongPath\'] != \'\'"><div><span class="title">' + $lang('msg_occupant_organization') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values[\'ProviderUserKey.Occupant.OrganizationName\'])]}</span></div></tpl>',
                      
                        '</div>',
                      
                        '</tpl>');
                var defaultConfig = {
                    pageSize       : 50,
                    loadMask       : true,
                    fieldLabel     : $lang('msg_select_occupant'),
                    allowBlank     : false,
                    resizable      : true,
                    mode           : 'remote',
                    listHeight     : 300,
                    hiddenName     : 'KeyUser',
                    name           : 'User',
                    triggerAction  : 'all',
                    displayField   : 'OccupantFullName',
                    valueField     : 'ProviderUserKey.KeyUser',
                    // selectedClass : 'x-box-blue',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    tpl            : tpl,
                    itemSelector   : 'div.combo-result-item',
                    lazyRender     : true,
                    store : new Viz.store.UserFiltered()
                    //store          : {
                    //    xtype       : 'vizStoreUserFiltered',
                    //    autoDestroy : true
                    //}
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.applyIf(config, this);
                Viz.combo.UserFiltered.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizComboUserFiltered", Viz.combo.UserFiltered);
﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.TimeZone
 * @extends Ext.form.ComboBox
 */
Viz.combo.TimeZone = Ext.extend(Viz.form.ClearableComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */


            constructor         : function(config) {
                config = config || {};

                var defaultConfig = {
                    fieldLabel     : $lang('msg_select_timezone'),
                    pageSize       : null,
                    resizable      : true,
                    listHeight     : 300,
                    triggerAction  : 'all',
                    loadMask       : true,
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    mode           : 'local',
                    autoLoad       : true,
                    lazyRender     : true,
                    displayField   : 'Label',
                    valueField     : 'Id',
                    anyMatch       : true,
                    caseSensitive  : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.apply(config, {
                            store : config.store ? config.store : new Viz.store.TimeZone({
                                        autoLoad  : config.autoLoad,
                                        listeners : {
                                            load   : config.autoLoad ? this.loadDefaultTimeZone : Ext.emptyFn,
                                            single : true,
                                            scope  : this
                                        }
                                    })
                        });

                Viz.combo.TimeZone.superclass.constructor.call(this, config);
            },

            loadDefaultTimeZone: function () {
                var timezoneid = Viz.App.Global.User.Preferences.TimeZoneId;
                var record;
                if (!Ext.isEmpty(timezoneid)) {
                    record = this.findRecord("Id", timezoneid);
                }
                else {
                    var value = "" + (-new Date().getTimezoneOffset());
                    record = this.findRecord("Value", value);
                }
                if (record) {
                    this.setValue(record.data.Id);
                }
            }
});

Ext.reg("vizComboTimeZone", Viz.combo.TimeZone);

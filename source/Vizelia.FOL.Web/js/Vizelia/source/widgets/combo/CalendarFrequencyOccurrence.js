﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.CalendarFrequencyOccurrence
 * @extends Ext.form.ComboBox
 */
Viz.combo.CalendarFrequencyOccurrence = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var defaultConfig = {
                    // fieldLabel : $lang('msg_calendar_dayname'),
                    mode         : 'local',
                    pageSize     : null,
                    listHeight   : null,
                    displayField : 'MsgCode',
                    valueField   : 'Value',
                    store        : {
                        xtype    : 'vizStoreCalendarFrequencyOccurrence',
                        autoLoad : true
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.CalendarFrequencyOccurrence.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboCalendarFrequencyOccurrence", Viz.combo.CalendarFrequencyOccurrence);
﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.CalendarMonthName
 * @extends Ext.form.ComboBox
 */
Viz.combo.CalendarMonthName = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var defaultConfig = { 
                    mode         : 'local',
                    pageSize     : null,
                    listHeight   : null,
                    displayField : 'MsgCode',
                    valueField   : 'Value',
                    store        : {
                        xtype    : 'vizStoreCalendarMonthName',
                        autoLoad : true
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.CalendarMonthName.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboCalendarMonthName", Viz.combo.CalendarMonthName);
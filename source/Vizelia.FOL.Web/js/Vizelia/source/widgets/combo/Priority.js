﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.Priority
 * @extends Viz.form.ComboBox
 * @xtype vizComboPriority
 */
Viz.combo.Priority = Ext.extend(Viz.form.ComboBox, {

            /**
             * @private
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                //var tpl = new Ext.XTemplate('<tpl for="."><div style ="cursor:hand;padding:2px;" class="combo-result-item"><div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>', '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc" style="font-size:10px"><div>{[$lang(values.MsgCode)]}</div></div></div></div>', '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div></div></tpl>');

                var defaultConfig = {
                    mode          : 'local',
                    pageSize      : null,
                    listHeight    : null,
                    // displayField : 'MsgCode',
                    displayField  : 'Label',
                    valueField    : 'KeyPriority',
                    // selectedClass : 'x-box-blue',
                    // tpl : tpl,
                    // itemSelector  : 'div.combo-result-item',
                    store         : {
                        xtype    : 'vizStorePriority',
                        autoLoad : true
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.Priority.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboPriority", Viz.combo.Priority);
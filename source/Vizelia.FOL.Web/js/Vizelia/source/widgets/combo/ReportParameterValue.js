﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.ReportParameterValue
 * @extends Ext.form.ComboBox
 */
Viz.combo.ReportParameterValue = Ext.extend(Viz.form.ComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode           : 'remote',
                    hiddenName     : config.parameterName,
                    name           : config.parameterName + 'Combo',
                    forceSelection : config.allowBlank === false,
                    pageSize       : null,
                    displayField   : 'Label',
                    valueField     : 'Value',
                    store          : {
                        xtype         : 'vizStoreReportParameterValue',
                        autoLoad      : true,
                        key           : config.key,
                        parameterName : config.parameterName
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.ReportParameterValue.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizComboReportParameterValue", Viz.combo.ReportParameterValue);
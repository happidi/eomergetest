﻿Ext.namespace('Viz.combo');
/**
 * @class Viz.combo.TreeComboMultiSelectClassificationItemAlarmDefinition
 * @extends Ext.combo.TreeComboMultiSelect Summary.
 * @xtype viztreecombomultiselectClassificationItemAlarmDefinition
 */
Viz.combo.TreeComboMultiSelectClassificationItemAlarmDefinition = Ext.extend(Viz.combo.TreeComboMultiSelect, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    lazyInit        : true,
                    filterIgnore    : true,
                    serviceHandler  : Viz.Services.CoreWCF.ClassificationItem_GetTree,
                    serviceParams   : {
                        flgFilter   : false,
                        excludePset : true
                    },
                    tools           : null,
                    displayCheckbox : true,
                    rootVisible     : false,
                    root            : {
                        id      : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("alarm"),
                        iconCls : 'viz-icon-small-alarm',
                        text    : ''
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.TreeComboMultiSelectClassificationItemAlarmDefinition.superclass.constructor.call(this, config);
            }
        });
Ext.reg('viztreecombomultiselectClassificationItemAlarmDefinition', Viz.combo.TreeComboMultiSelectClassificationItemAlarmDefinition);

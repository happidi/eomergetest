﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.MethodDefinition
 * @extends Ext.form.ComboBox
 * @xtype vizComboWorkflowAssemblyDefinition
 */

Viz.combo.MethodDefinition = Ext.extend(Viz.form.ComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3 class="x-icon-combo-input {[Ext.util.Format.htmlEncode(values.IconCls)]}">{[Ext.util.Format.htmlEncode(values.Label)]}',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '<div><span class="value">{[Ext.util.Format.htmlEncode(values.LabelDescription)]}</span></div>',
                        /**/
                        '<div><span class="value">{[Ext.util.Format.htmlEncode(values.MethodName)]}</span></div>',
                        /**/
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');
                var defaultConfig = {
                    pageSize       : null,
                    loadMask       : true,
                    fieldLabel     : $lang('msg_select_methoddefinition'),
                    allowBlank     : false,
                    resizable      : false,
                    displayIcon    : true,
                    iconClsField   : 'IconCls',
                    mode           : 'local',
                    listHeight     : null,
                    hiddenName     : 'MethodDefinition',
                    name           : 'MethodDefinition',
                    triggerAction  : 'all',
                    displayField   : 'Label',
                    valueField     : 'KeyMethodDefinition',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    tpl            : tpl,
                    itemSelector   : 'div.combo-result-item',
                    lazyRender     : true,
                    store          : new Viz.store.MethodDefinition({
                                serviceParams : {
                                    typeName : config.typeName
                                }
                            })
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.applyIf(config, this);
                Viz.combo.MethodDefinition.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizComboMethodDefinition", Viz.combo.MethodDefinition);

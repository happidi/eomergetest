﻿Ext.namespace("Viz.combo");
/**
 * A tree combo component.
 * @class Viz.combo.TreeCombo
 * @extends Ext.form.TriggerField
 * @xtype viztreecombo
 * 
 * <pre><code>
 * var win = new Ext.Window({
 *             title     : 'Viz.combo.Tree example',
 *             layout    : 'form',
 *             defaults  : {
 *                 anchor : Viz.Const.UI.Anchor 
 *             },
 *             width     : 400,
 *             bodyStyle : 'padding:5px',
 *             items     : [{
 *                         id              : 'treecombo',
 *                         fieldLabel      : 'Choose value',
 *                         xtype           : 'viztreecombo',
 *                         serviceHandler  : Viz.Services.CoreWCF.Organization_GetTree,
 *                         treeHeight      : 200,
 *                         serviceParams   : {
 *                             flgFilter : false
 *                         },
 *                         displayCheckbox : true,
 *                         rootVisible     : true,
 *                         entityKey       : 'KeyOrganization' 
 *                         root            : {
 *                             id      : ' ',
 *                             iconCls : 'viz-icon-small-organization',
 *                             text    : $lang(&quot;msg_tree_organization_root&quot;)
 *                         }
 *                     }],
 *             buttons   : [{
 *                         text    : 'Get checked',
 *                         handler : function() {
 *                             alert(Ext.getCmp(&quot;treecombo&quot;).getChecked().length);
 *                         }
 *                     }]
 * 
 *         });
 * 
 * win.show();
 * </code></pre>
 */
Viz.combo.TreeCombo = Ext.extend(Ext.form.TwinTriggerField, {
            /**
             * @cfg {Object} configTree The configuration of the treeview
             * 
             * <pre><code>
             * configTree : {
             *     header : false,
             *     xtype  : 'vizTreeSpatial'
             * }
             * </code></pre>
             */

            /**
             * @cfg {Function} serviceHandler The service endpoint to execute.
             */

            /**
             * @cfg {Object} serviceParams The service parameters.
             */

            /**
             * @cfg {Integer} treeHeight The height of the treeview (defaults to 200).
             */

            /**
             * @cfg {Boolean} hasTools <tt>true</tt> to create an emplacement for tools, <tt>false</tt> otherwise. Default to <tt>true</tt>.
             */
            hasTools               : false,

            /**
             * @cfg {Boolean} displayCheckbox True to display checked boxes, false otherwise.
             */

            /**
             * @cfg {Boolean} rootVisible True to display the root, false otherwise.
             */

            /**
             * @cfg {String} entityKey The field name of the Key entity.
             */

            /**
             * @cfg {Boolean} displayIcon True to display icon, false otherwise.
             */
            displayIcon            : true,

            /**
             * @cfg {Boolean} searchable The default searchable.
             */
            searchable             : false,
            /**
             * @cfg {Number} queryDelay The length of time in milliseconds to delay between the start of typing and sending the query to filter the dropdown list (defaults to <tt>500</tt> if <tt>{@link #mode} = 'remote'</tt> or <tt>10</tt> if <tt>{@link #mode} = 'local'</tt>)
             */
            queryDelay             : 500,
            /**
             * *
             * @cfg {Boolean} lazyInit true to prevent the ComboBox from rendering until requested (should always be used when rendering into a menu or a filter defaults to false).
             */

            trigger1Class          : 'x-form-tree-trigger',

            trigger2Class          : 'x-form-trigger x-form-clear-trigger',

            initComponent          : function() {
                this.addEvents(
                        /**
                         * *
                         * @event change * Fires just before the field value has changed. *
                         * @param {Ext.form.Field} this *
                         * @param {Mixed} newValue The new value *
                         * @param {Mixed} node The node selected, null in case the event was raise by setValue
                         */
                        'changevalue',

                        /**
                         * @event clear * Fires when the field value is cleared.
                         * @param {Ext.form.Field} this
                         */
                        'clear');

                this.readOnly = false;
                this.editable = this.searchable;
                if (this.searchable)
                    this.defaultIconCls = 'ux-gridfilter-text-icon';
                Viz.combo.TreeCombo.superclass.initComponent.call(this);
                this.on('specialkey', this.onSpecialKey, this);
                if (!this.lazyInit)
                    this.getTree();

            },

            onSpecialKey           : function(f, e) {
                if (e.getKey() == e.ENTER) {
                    this.onTriggerClick1();
                }
                if (e.getKey() == e.DELETE) {
                    this.onTrigger2Click();
                }
            }, // private
            initEvents             : function() {
                Ext.form.ComboBox.superclass.initEvents.call(this);

                this.dqTask = new Ext.util.DelayedTask(this.initQuery, this);
                if (this.typeAhead) {
                    this.taTask = new Ext.util.DelayedTask(this.onTypeAhead, this);
                }
                if (!this.enableKeyEvents) {
                    this.mon(this.el, 'keyup', this.onKeyUp, this);
                }
            },
            // private
            onKeyUp                : function(e) {
                var k = e.getKey();
                if (this.editable !== false && this.readOnly !== true && (k == e.BACKSPACE || !e.isSpecialKey())) {
                    this.lastKey = k;
                    this.dqTask.delay(this.queryDelay);
                }
                Viz.combo.TreeCombo.superclass.onKeyUp.call(this, e);
            }, // private
            initQuery              : function(p) {
                this.doQuery(this.getRawValue().trim());
            },
            doQuery                : function(q) {
                var tree = this.getTree();
                var node = tree.getRootNode();
                node.removeAll(true);
                node.attributes.Key = q;
                tree.serviceParams.Key = q;
                var expanded = node.expanded;
                tree.getLoader().load(node, function(ev, n) {
                            var p = function(n) {
                                if (n && (n.childrenRendered || (n.attributes.children && (n.attributes.children.length > 0)))) {
                                    n.expand(false);
                                    if (n.firstChild)
                                        p(n.firstChild);
                                }
                            };
                            if (!this.isExpanded())
                                this.onTrigger1Click();
                            p.call(this, n);
                        }, this);

            },
            updateEditState        : function() {
                Viz.combo.TreeCombo.superclass.updateEditState.apply(this, arguments);
                if (this.rendered) {
                    if (this.readOnly) {
                        this.un('specialkey', this.onSpecialKey, this);

                    }
                    else {
                        if (!this.editable) {
                            this.on('specialkey', this.onSpecialKey, this);
                        }
                        else {
                            this.un('specialkey', this.onSpecialKey, this);

                        }

                    }

                }
            },

            onRender               : function(ct, position) {
                Viz.combo.TreeCombo.superclass.onRender.apply(this, arguments);
                if (this.lazyInit == true)
                    this.getTree();

                // creates a icon space holder
                if (this.displayIcon) {
                    var wrap = this.el.up('div.x-form-field-wrap');
                    // removed because if it s used in a composite field, the 2nd combo is not visible
                    // this.wrap.applyStyles({
                    // position : 'relative'
                    // });
                    this.el.addClass('x-icon-combo-input');
                    this.flag = Ext.DomHelper.append(wrap, {
                                tag   : 'div',
                                style : 'position:absolute'
                            });

                    if (this.defaultIconCls) {
                        this.flag.className = 'x-icon-combo-icon ' + this.defaultIconCls;
                    }
                }
                // if the field was mark invalid before beeing rendered, we mark it on render.
                if (this.activeError) {
                    this.markInvalid(this.activeError);
                }

            },

            getListParent          : function() {
                return document.body;
            },

            isExpanded             : function() {
                return this.treePanel && this.treePanel.isVisible();
            },

            getWidthOffset         : function() {
                return 0;
            },

            onTriggerClick         : function() {
                this.onTrigger1Click();
            },

            onTrigger1Click        : function() {
                var tree = this.getTree();
                if (this.isExpanded()) {
                    this.collapse();
                    tree.hide();
                    this.el.focus();
                }
                else {
                    this.onFocus({});
                    tree.show();
                    // tree.getEl().alignTo(this.wrap, 'tl-bl?');
                    tree.getEl().alignTo(this.getAlignElement(), 'tl-bl?', this.getAlignOffset());
                    tree.setWidth(this.getAlignElement().getWidth() + this.getWidthOffset());
                }
            },

            getTree                : function() {
                if (!this.treePanel) {
                    if (!this.treeWidth) {
                        this.treeWidth = Math.max(200, this.width || 200);
                    }
                    if (!this.treeHeight) {
                        this.treeHeight = 200;
                    }

                    /*
                     * if (!this.loader) { this.loaderloader = new Viz.tree.TreeLoaderWCF({ displayCheckbox : this.displayCheckbox ? true : false, serviceHandler : this.serviceHandler, serviceParams : this.serviceParams }); }
                     */
                    if (this.configTree)
                        this.treePanel = Ext.ComponentMgr.create(Ext.apply(this.configTree, {
                                    renderTo      : this.getListParent() || Ext.getBody(),
                                    pathAttribute : this.pathAttribute,
                                    stateId       : this.stateId ? this.stateId + '-' + 'treePanel' : null,
                                    floating      : true,
                                    autoScroll    : true,
                                    minWidth      : 200,
                                    minHeight     : 200,
                                    width         : this.treeWidth,
                                    height        : this.treeHeight,
                                    bodyStyle     : 'padding : 4px',
                                    listeners     : {
                                        hide         : this.onTreeHide,
                                        show         : this.onTreeShow,
                                        click        : this.onTreeNodeClick,
                                        expandnode   : this.onExpandOrCollapseNode,
                                        collapsenode : this.onExpandOrCollapseNode,
                                        resize       : this.onTreeResize,
                                        scope        : this
                                    }
                                }));
                    else
                        this.treePanel = new Viz.tree.TreePanel({
                                    renderTo        : this.getListParent() || Ext.getBody(),
                                    pathAttribute   : this.pathAttribute,
                                    stateId         : this.stateId ? this.stateId + '-' + 'treePanel' : null,
                                    serviceHandler  : this.serviceHandler,
                                    serviceParams   : this.serviceParams,
                                    useArrows       : this.useArrows || true,
                                    sortAttribute   : this.sortAttribute,
                                    displayCheckbox : this.displayCheckbox,
                                    pathSeparator   : ' / ',
                                    /*
                                     * loader : this.loader || new Ext.tree.TreeLoader({ preloadChildren : (typeof this.root == 'undefined'), url : this.dataUrl || this.url }),
                                     */
                                    root            : this.root || new Ext.tree.AsyncTreeNode({
                                                children : this.children
                                            }),
                                    rootVisible     : (typeof this.rootVisible != 'undefined') ? this.rootVisible : (this.root ? true : false),
                                    floating        : true,
                                    autoScroll      : true,
                                    tools           : this.tools,
                                    hasTools        : this.hasTools,
                                    stateEvents     : ['checkchange'],
                                    minWidth        : 200,
                                    minHeight       : 200,
                                    width           : this.treeWidth,
                                    height          : this.treeHeight,
                                    bodyStyle       : 'padding : 4px',
                                    listeners       : {
                                        hide         : this.onTreeHide,
                                        show         : this.onTreeShow,
                                        click        : this.onTreeNodeClick,
                                        expandnode   : this.onExpandOrCollapseNode,
                                        collapsenode : this.onExpandOrCollapseNode,
                                        resize       : this.onTreeResize,
                                        scope        : this
                                    }
                                });
                    var explore = function(bSuccess, oSelNode) {
                        if (!bSuccess)
                            return;
                        oSelNode.checked = true;
                        oSelNode.ui.toggleCheck(true);
                    };
                    if (this.selectedPath) {
                        for (var i = 0; i < this.selectedPath.length; i++) {
                            this.treePanel.selectPath(this.selectedPath[i], this.pathAttribute, explore);
                        }
                    }

                    this.treePanel.show();
                    this.treePanel.hide();
                    this.relayEvents(this.treePanel.loader, ['beforeload', 'load', 'loadexception']);
                    if (this.resizable) {
                        this.resizer = new Ext.Resizable(this.treePanel.getEl(), {
                                    pinned  : true,
                                    handles : 'se'
                                });
                        this.mon(this.resizer, 'resize', function(r, w, h) {
                                    this.treePanel.setSize(w, h);
                                }, this);
                    }
                }
                return this.treePanel;
            },

            getAlignElement        : function() {
                return this.wrap;
            },

            /**
             * Fixes the offset of the list, overridable.
             */
            getAlignOffset         : function() {
                return [0, 2];
            },

            onExpandOrCollapseNode : function() {
                if (!this.maxHeight || this.resizable)
                    return; // -----------------------------> RETURN
                var treeEl = this.treePanel.getTreeEl();
                var heightPadding = treeEl.getHeight() - treeEl.dom.clientHeight;
                var ulEl = treeEl.child('ul'); // Get the underlying tree element
                var heightRequired = ulEl.getHeight() + heightPadding;
                if (heightRequired > this.maxHeight)
                    heightRequired = this.maxHeight;
                this.treePanel.setHeight(heightRequired);
            },

            onTreeResize           : function() {
                if (this.treePanel) {
                    this.treePanel.getEl().alignTo(this.getAlignElement(), 'tl-bl?', this.getAlignOffset());
                }
            },

            onTreeShow             : function() {
                Ext.getDoc().on('mousewheel', this.collapseIf, this);
                Ext.getDoc().on('mousedown', this.collapseIf, this);
            },

            onTreeHide             : function() {
                Ext.getDoc().un('mousewheel', this.collapseIf, this);
                Ext.getDoc().un('mousedown', this.collapseIf, this);
            },

            collapseIf             : function(e) {
                if (!e.within(this.wrap) && !e.within(this.getTree().getEl())) {
                    this.collapse();
                }
            },

            collapse               : function() {
                this.getTree().hide();
                if (this.resizer)
                    this.resizer.resizeTo(this.treeWidth, this.treeHeight);
            },

            // private
            validateBlur           : function() {
                return !this.treePanel || !this.treePanel.isVisible();
            },

            setValue               : function(v, shouldSetTextWithLongPath) {
                var n;
                if (this.treePanel && Ext.isEmpty(v) == false) {
                    n = this.treePanel.getNodeById(v);
                    if (n) {
                        var finalText = n.text;
                        if (shouldSetTextWithLongPath) {
                            var curNode = n.parentNode;
                            while (curNode.parentNode) {
                                finalText = curNode.text + this.treePanel.pathSeparator + finalText;
                                curNode = curNode.parentNode;
                            }
                        }
                        this.setRawValue(finalText);
                        n.select();
                        this.setIconCls(n);
                    }
                }
                if (!n && this.originalDisplayValue && Ext.isEmpty(v) == false) {
                    var form = this.findParentByType('form');
                    if (form) {
                        var entity = form.getForm().entity;
                        if (entity) {
                            var value = entity[this.originalDisplayValue];
                            this.setRawValue(value);
                            var pathElement = this.pathStepField || 'text';
                            if (this.pathStepField)
                                this.treePanel.root.attributes[this.pathStepField] = this.treePanel.root['id'];

                            var path = this.treePanel.pathSeparator + this.treePanel.root.attributes[pathElement] + this.treePanel.pathSeparator;

                            if (this.pathField && entity[this.pathField]) {
                                path = entity[this.pathField].replace(/\s{2,}/g, ' ').trimEnd();
                                var keys = path.split(this.treePanel.pathSeparator);
                                var rootIdex = 0;
                                for (var i = 0; i < keys.length; i++) {
                                    if (keys[i] == this.treePanel.root['id']) {
                                        rootIdex = i;
                                        break;
                                    }
                                }
                                keys.splice(1, rootIdex - 1);
                                path = keys.join(this.treePanel.pathSeparator);
                            }
                            else
                                path = path + value;

                            // we select the node through its path and we make sure the node is visible when viewing the Tree.
                            this.treePanel.selectPath(path, pathElement, (function(success, nodeResult) {
                                        if (success) {
                                            nodeResult.ensureVisible();
                                            this.setIconCls(nodeResult);
                                        }
                                    }).createDelegate(this));
                        }
                    }

                }
                if (this.value != v)
                    this.fireEvent('changevalue', this, v, null);
                this.startValue = this.value = v;
                // this.validate();
            },

            /**
             * Displays the icon of the selected node.
             * @param {Ext.tree.TreeNode} node
             */
            setIconCls             : function(node) {
                if (this.displayIcon && this.flag) {
                    this.flag.style.display = '';
                    var iconCls = node.attributes.iconCls;
                    var icon = node.attributes.icon;

                    if (iconCls) {
                        this.flag.className = 'x-icon-combo-icon ' + iconCls;
                        return;
                    }
                    if (icon) {
                        this.flag.className = 'x-icon-combo-icon';
                        Ext.fly(this.flag).applyStyles("background-image:url('" + icon + "'); background-position:left top;");
                        return;
                    }
                }
            },
            clearValue             : function() {
                this.setValue(null);
                this.setRawValue('');
                if (this.displayIcon && this.flag && !this.searchable)
                    this.flag.style.display = 'none';
                else if (this.searchable && this.flag) {
                    this.flag.className = 'x-icon-combo-icon ' + this.defaultIconCls;
                    this.flag.style.display = '';
                }
                this.getTree().getSelectionModel().clearSelections(true);
                this.fireEvent('clear', this);
            },

            /**
             * Clear the field value and raise the clear event.
             */
            onTrigger2Click        : function() {
                this.clearValue();
            },

            getValue               : function() {
                return this.value;
            },

            /**
             * Retreives the selected node
             */
            getSelectedNode        : function() {
                if (this.getValue() != null)
                    return this.getTree().getSelectionModel().getSelectedNode();
                else
                    return null;
            },

            /**
             * Retrieve an array of checked nodes, or an array of a specific attribute of checked nodes (e.g. "id")
             * @param {String} attribute (optional) Defaults to null (return the actual nodes)
             * @param {TreeNode} startNode (optional) The node to start from, defaults to the root
             * @return {Array}
             */
            getChecked             : function(a, startNode) {
                return this.treePanel.getChecked(a, startNode)
            },

            /**
             * Handler for node click.
             * @private
             * @param {TreeNode} node
             * @param {Object} e
             */
            onTreeNodeClick        : function(node, e) {
                var value;
                this.setRawValue(node.getPath('text'));
                this.setIconCls(node);
                if (this.entityKey && node.attributes.entity)
                    value = node.attributes.entity[this.entityKey];
                else
                    value = node.attributes.Key || node.id;
                var oldvalue = this.value;
                this.value = value;

                if (value != oldvalue)
                    this.fireEvent('changevalue', this, value, node);

                this.validate.defer(100, this);
                this.fireEvent('select', this, node);
                this.collapse();
            }
        });

Ext.reg("viztreecombo", Viz.combo.TreeCombo);

﻿Ext.namespace('Viz.combo');

/**
 * @class Viz.combo.TreeComboPortlet
 * @extends Ext.combo.TreeCombo Summary.
 */
Viz.combo.TreeComboPortlet = Ext.extend(Viz.combo.TreeCombo, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler  : Viz.Services.EnergyWCF.Portlet_GetTree,
                    treeHeight      : 200,
                    serviceParams   : {
                        flgFilter : false
                    },
                    entityKey : null,
                    displayCheckbox : false,
                    rootVisible     : true,
                    root            : {
                        id      : ' ',
                        iconCls : 'viz-icon-small-portlet',
                        checked : undefined,
                        text    : $lang("msg_portlets")
                    }
                };
                var forcedConfig = {

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.TreeComboPortlet.superclass.constructor.call(this, config);
            }
        });

Ext.reg('viztreecomboPortlet', Viz.combo.TreeComboPortlet);

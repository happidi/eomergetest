﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.CalendarEventCategoryTitle
 * @extends Ext.form.ComboBox
 */
Viz.combo.CalendarEventCategoryTitle = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var defaultConfig = {
                    mode         : 'local',
                    pageSize     : null,
                    listHeight   : null,
                    displayField : 'Value',
                    valueField   : 'Id',
                    store        : {
                        xtype                    : 'vizStoreCalendarEventCategoryTitle',
                        KeyCalendarEventCategory : config.KeyCalendarEventCategory,
                        KeyMeter                 : config.KeyMeter
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.CalendarEventCategoryTitle.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboCalendarEventCategoryTitle", Viz.combo.CalendarEventCategoryTitle);
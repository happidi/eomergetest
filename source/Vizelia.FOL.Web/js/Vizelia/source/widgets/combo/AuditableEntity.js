﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.AuditableEntity
 * @extends Ext.form.ComboBox
 */
Viz.combo.AuditableEntity = Ext.extend(Viz.form.ComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    triggerAction : 'all',
                    displayField  : 'MsgCode',
                    valueField    : 'Value',
                    iconClsField  : 'IconCls',
                    displayIcon   : true,
                    resizable     : false,
                    store         : {
                        xtype    : "vizStoreAuditableEntity",
                        autoLoad : false
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.Color.superclass.constructor.call(this, config);

                // this.tpl = '<tpl for="."><div class="x-combo-list-item" style="background-color:{[Ext.util.Format.htmlEncode(values.' + this.valueField + ')]}">{[Ext.util.Format.htmlEncode(values.' + this.displayField + ')]}</div></tpl>';
            }
        });
Ext.reg("vizComboAuditableEntity", Viz.combo.AuditableEntity);
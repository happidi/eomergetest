﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.Chart The Chart combobox.
 * @extends Ext.form.ComboBox
 * @xtype vizComboChart
 */
Viz.combo.Chart = Ext.extend(Viz.form.ClearableComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3>{[Ext.util.Format.htmlEncode(values.Title)]} {[Ext.util.Format.htmlEncode(values.Description)]}',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');
                var defaultConfig = {
                    pageSize       : 10,
                    loadMask       : true,
                    fieldLabel     : $lang('msg_select_chart'),
                    allowBlank     : false,
                    resizable      : true,
                    mode           : 'remote',
                    listHeight     : 300,
                    hiddenName     : 'KeyChart',
                    name           : 'KeyChart',
                    triggerAction  : 'all',
                    displayField   : 'Title',
                    valueField     : 'KeyChart',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    tpl            : tpl,
                    itemSelector   : 'div.combo-result-item',
                    lazyRender     : true,
                    store          : new Viz.store.Chart({
                                isFavorite : true
                            })
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.applyIf(config, this);
                Viz.combo.Chart.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizComboChart", Viz.combo.Chart);

/**
 * A factory helper for creating a composite control containing the combo + a button to display a dataview of the Charts.
 * @param {Object} config config An object which may contain the following properties:
 * <ul>
 * <li><b>handler</b> : Function <div class="sub-desc">The handler for the button.</div></li>
 * <li><b>scope</b> : Object (Optional)<div class="sub-desc">The scope to pass to the handler function</div></li>
 * </ul>
 * </p>
 */
Viz.combo.Chart.factory = function(config) {
    var fieldComboId = config.id || Ext.id();
    var fieldParentId = config.parentid || Ext.id();

    var preview = function(fieldComboId) {
        var dataviewId = Ext.id();
        var formWindow = Ext.getCmp(fieldComboId).findParentByType('window');

        var win = new Ext.Window({
                    modal   : true,
                    title   : $lang("msg_chart"),
                    iconCls : 'viz-icon-small-display',
                    layout  : 'fit',
                    width   : 340,
                    height  : formWindow ? Math.max(formWindow.getBox().height, 330) : 330,
                    x       : formWindow ? formWindow.getBox().x + formWindow.getBox().width : null,
                    y       : formWindow ? formWindow.getBox().y : null,
                    items   : [{
                                xtype    : 'vizDataViewChart',
                                id       : dataviewId,
                                readonly : true,
                                border   : false
                            }],
                    buttons : [{
                                text    : $lang('msg_save'),
                                iconCls : 'viz-icon-small-save',
                                handler : function(button) {
                                    var dvPanel = Ext.getCmp(dataviewId);
                                    var combo = Ext.getCmp(fieldComboId);
                                    var record = dvPanel.getSelectedRecord();
                                    if (record) {
                                        combo.setValue(record.json.KeyChart);
                                        combo.fireEvent('select', combo, record);
                                        button.findParentByType('window').close();
                                    }
                                },
                                scope   : this
                            }]

                });
        win.show();
    };
    return {
        fieldLabel : config.fieldLabel || $lang('msg_template'),
        id         : fieldParentId,
        xtype      : 'container',
        hidden     : config.hidden,
        disable    : config.disable,
        bodyStyle  : 'background-color: transparent',
        layout     : 'column',
        items      : [{
                    columnWidth : 1,
                    layout      : 'anchor',
                    bodyStyle   : 'background-color: transparent',
                    border      : false,
                    items       : [{
                                id         : fieldComboId,
                                fieldLabel : config.fieldLabel || $lang('msg_chart'),
                                name       : config.name || 'KeyChart',
                                hiddenName : config.hiddenName || 'KeyChart',
                                listeners  : config.listeners,
                                store      : config.store || new Viz.store.Chart({
                                            isFavorite : true
                                        }),
                                allowBlank : Ext.isDefined(config.allowBlank) ? config.allowBlank : true,
                                anchor     : '-5',
                                xtype      : 'vizComboChart',
                                value      : config.value
                            }]
                }, {
                    xtype   : 'button',
                    iconCls : 'viz-icon-small-display',
                    tooltip : $lang('msg_chart_view'),
                    handler : preview.createDelegate(this, [fieldComboId])
                }]
    };
};

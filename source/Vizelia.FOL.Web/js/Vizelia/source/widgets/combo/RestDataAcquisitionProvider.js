﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.RESTDataAcquisitionProvider
 * @extends Ext.form.ComboBox
 */
Viz.combo.RESTDataAcquisitionProvider = Ext.extend(Viz.form.ComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode         : 'local',
                    pageSize     : null,
                    listHeight   : null,
                    displayField : 'MsgCode',
                    valueField   : 'Value',
                    store        : new Viz.store.RESTDataAcquisitionProvider()
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.RESTDataAcquisitionProvider.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizComboRESTDataAcquisitionProvider", Viz.combo.RESTDataAcquisitionProvider);
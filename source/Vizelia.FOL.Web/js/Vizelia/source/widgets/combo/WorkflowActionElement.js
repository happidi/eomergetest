﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.WorkflowActionElement
 * @extends Ext.form.ComboBox
 * @xtype vizComboWorkflowActionElement
 */
Viz.combo.WorkflowActionElement = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                // var tpl = new Ext.XTemplate('<tpl for="."><div style ="cursor:hand;padding:2px;" class="combo-result-item"><div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>', '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc" style="font-size:10px"><div>{[$lang(values.MsgCode)]}</div></div></div></div>', '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div></div></tpl>');

                var defaultConfig = {
                    mode         : 'local',
                    pageSize     : null,
                    listHeight   : null,
                    // displayField : 'MsgCode',
                    displayField : 'Label',
                    valueField   : 'MsgCode',
                    // selectedClass : 'x-box-blue',
                    // tpl : tpl,
                    // itemSelector : 'div.combo-result-item',
                    store        : {
                        xtype    : 'vizStoreWorkflowActionElement',
                        autoLoad : true,
                        serviceParams : {
                            keyClassificationItem : config.keyClassificationItem || this.keyClassificationItem
                        }
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.WorkflowActionElement.superclass.constructor.call(this, config);

            }
        });

Ext.reg('vizComboWorkflowActionElement', Viz.combo.WorkflowActionElement);
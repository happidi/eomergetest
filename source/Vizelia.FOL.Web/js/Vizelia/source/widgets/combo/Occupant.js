﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.Occupant The Occupant combobox.
 * @extends Ext.form.ComboBox
 * @xtype vizComboOccupant
 */
Viz.combo.Occupant = Ext.extend(Ext.form.ComboBox, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                // var tpl = new Ext.XTemplate('<tpl for="."><div style ="cursor:hand;padding:2px;" class="combo-result-item"><div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>', '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc" style="font-size:10px"><div style="color:DarkBlue" >{Id} ({[xindex]}/{[xcount]})</div><div>' + $lang('msg_occupant_fullname') + ': {FirstName} {LastName}</div><tpl if="TelephoneNumbers"><div>' + $lang('msg_occupant_telephone') + ': {TelephoneNumbers}</div></tpl><div>' + $lang('msg_occupant_organization') + ': {OrganizationName}</div></div></div></div>', '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div></div></tpl>');
                var tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item">',
                        /**/
                        '<h3>{FullName} ({Id})',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '<tpl if="TelephoneNumbers"><div><span class="title">' + $lang('msg_occupant_telephone') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.TelephoneNumbers)]}</span></div></tpl>',
                        /**/
                        '<div><span class="title">' + $lang('msg_occupant_company') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.OrganizationName)]}</span></div>',
                        /**/
                        '<tpl if="LocationShortPath"><div><span class="title">' + $lang('msg_occupant_location') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.LocationShortPath)]}</span></div></tpl>',
                        /**/
                        '<tpl if="ClassificationItemWorktypeLongPath"><div><span class="title">' + $lang('msg_occupant_worktype') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.ClassificationItemWorktypeLongPath)]}</span></div></tpl>',
                        /**/
                        '<tpl if="ClassificationItemOrganizationLongPath"><div><span class="title">' + $lang('msg_occupant_organization') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.ClassificationItemOrganizationLongPath)]}</span></div></tpl>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>');
                var defaultConfig = {
                    pageSize       : 50,
                    loadMask       : true,
                    fieldLabel     : $lang('msg_select_occupant'),
                    allowBlank     : false,
                    resizable      : true,
                    mode           : 'remote',
                    listHeight     : 300,
                    hiddenName     : 'KeyOccupant',
                    name           : 'Occupant',
                    triggerAction  : 'all',
                    displayField   : 'FullName',
                    valueField     : 'KeyOccupant',
                    // selectedClass : 'x-box-blue',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    tpl            : tpl,
                    itemSelector   : 'div.combo-result-item',
                    lazyRender     : true,
                    store : new Viz.store.Occupant()
                    //store          : {
                    //    xtype       : 'vizStoreOccupant',
                    //    autoDestroy : true
                    //}
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.applyIf(config, this);
                Viz.combo.Occupant.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizComboOccupant", Viz.combo.Occupant);
﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.Culture The Culture combobox. When it loads the combo will automatically selects the culture of the brower. See <tt>onStoreLoad</tt> for more info.
 * @extends Ext.form.ComboBox
 */
Viz.combo.LocalizationCulture = Ext.extend(Viz.form.ClearableComboBox, {

            constructor     : function(config) {
                config = config || {};

                var defaultConfig = {
                    fieldLabel               : $lang('msg_translator_from'),
                    mode                     : 'local',
                    autoLoad                 : true,
                    pageSize                 : null,
                    displayIcon              : true,
                    iconClsField             : 'Key',
                    displayField             : 'Value',
                    valueField               : 'Key',
                    displayFlagForEmptyValue : true
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.apply(config, {
                            store : new Viz.store.LocalizationCulture({
                                        autoLoad  : config.autoLoad,
                                        listeners : {
                                            load  : config.autoLoad ? this.onStoreLoad : Ext.emptyFn,
                                            scope : this
                                        }
                                    })
                        });
                Viz.combo.LocalizationCulture.superclass.constructor.call(this, config);
            },

            /**
             * Handler that set the value of the culture to the current culture used on the client. To get the current culture we ask for the Versions object through an ajax call.
             */
            onStoreLoad     : function(store, records) {
                Viz.Services.CoreWCF.Versions_GetItem({
                            success : function(versions) {
                                var culture = versions.Culture;
                                //if (culture.toLowerCase() == 'en-GB'.toLowerCase())
                                this.setValue(culture);
//                                else if (culture.startsWith('en'))
//                                    this.setValue('en-US');
                                //else
                                  //  this.setValue(versions.Culture.substring(0, 2));
                            },
                            scope   : this
                        });
            },

            iconClsRenderer : function(value) {
                return 'viz-icon-flag-' + value;
            },

            setIconCls: function (value) {
                if (!this.getValue() && !this.displayFlagForEmptyValue) {
                    return;
                }
                Viz.combo.LocalizationCulture.superclass.setIconCls.call(this, value);
            }

});

Ext.reg("vizComboLocalizationCulture", Viz.combo.LocalizationCulture);
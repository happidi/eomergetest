﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.ListElement A combo populated with list elements.
 * @extends Viz.form.ComboBox
 * @xtype vizComboListElement
 */
Viz.combo.ListElement = Ext.extend(Viz.form.ComboBox, {
            /**
             * @cfg {String} listName the list name to retreive from db.
             */
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    loadMask     : true,
                    allowBlank   : false,
                    mode         : 'local',
                    listHeight   : null,
                    pageSize     : null,
                    displayField : 'Label',
                    valueField   : 'Value'
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                if (config.store)
                    this.store = config.store
                else {
                    this.store = new Viz.store.ListElement({
                                listName : config.listName,
                                autoLoad : config.mode == 'local' ? true : config.autoLoad
                            });
                    config.store = this.store;
                }
                Viz.combo.ListElement.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizComboListElement", Viz.combo.ListElement);
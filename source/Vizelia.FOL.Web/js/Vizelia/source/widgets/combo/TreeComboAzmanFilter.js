﻿Ext.namespace('Viz.combo');

/**
 * @class Viz.combo.TreeComboAzManFilter
 * @extends Ext.combo.TreeCombo Summary.
 */
Viz.combo.TreeComboAzManFilter = Ext.extend(Viz.combo.TreeCombo, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler  : Viz.Services.AuthenticationWCF.AzManFilter_GetTree,
                    treeHeight      : 200,
                    serviceParams   : {
                        flgFilter : true
                    },
                    searchable :true,                    
                    entityKey : null,
                    displayCheckbox : false,
                    rootVisible     : false,
                    root            : {
                        id      : ' ',
                        iconCls : 'viz-icon-small-azmanfilter',
                        checked : undefined,
                        text    : $lang("msg_tree_AzManFilter_root")
                    }
                };
                var forcedConfig = {

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.TreeComboAzManFilter.superclass.constructor.call(this, config);
            }
        });

Ext.reg('viztreecomboAzManFilter', Viz.combo.TreeComboAzManFilter);

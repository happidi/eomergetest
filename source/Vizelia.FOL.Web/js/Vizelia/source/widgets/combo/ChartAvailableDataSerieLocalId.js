﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.ChartAvailableDataSerieLocalId
 * @extends Ext.form.ComboBox
 */
Viz.combo.ChartAvailableDataSerieLocalId = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var defaultConfig = {
                    mode         : 'local',
                    pageSize     : null,
                    listHeight   : null,
                    displayField : 'MsgCode',
                    valueField   : 'Value',
                    iconClsField:'IconCls',
                    displayIcon:true,
                    defaultIconCls:'viz-icon-small-dataserie',
                    resizable    : true,
                    store        : {
                        xtype           : 'vizStoreChartAvailableDataSerieLocalId',
                        KeyChart        : config.KeyChart,
                        includeExisting : config.includeExisting === true ? true : false
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.ChartAvailableDataSerieLocalId.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboChartAvailableDataSerieLocalId", Viz.combo.ChartAvailableDataSerieLocalId);
﻿Ext.namespace('Viz.combo');
/**
 * @class Viz.combo.TreeComboMultiSelectClassificationItemEventLog
 * @extends Ext.combo.TreeComboMultiSelect Summary.
 * @xtype viztreecombomultiselectClassificationItemEventLog
 */
Viz.combo.TreeComboMultiSelectClassificationItemEventLog = Ext.extend(Viz.combo.TreeComboMultiSelect, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    lazyInit        : true,
                    filterIgnore    : true,
                    serviceHandler  : Viz.Services.CoreWCF.ClassificationItem_GetTree,
                    serviceParams   : {
                        flgFilter   : false,
                        excludePset : true
                    },
                    tools           : null,
                    displayCheckbox : true,
                    rootVisible     : false,
                    root            : {
                        id      : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("eventlog"),
                        iconCls : 'viz-icon-small-eventlog',
                        text    : ''
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.TreeComboMultiSelectClassificationItemEventLog.superclass.constructor.call(this, config);
            }
        });
Ext.reg('viztreecombomultiselectClassificationItemEventLog', Viz.combo.TreeComboMultiSelectClassificationItemEventLog);

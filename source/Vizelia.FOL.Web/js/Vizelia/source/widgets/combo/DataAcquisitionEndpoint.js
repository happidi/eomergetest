﻿Ext.namespace("Viz.combo");

/**
 * @class Viz.combo.DataAcquisitionEndpoint
 * @extends Ext.form.ComboBox
 */
Viz.combo.DataAcquisitionEndpoint = Ext.extend(Viz.form.ComboBox, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                var tpl = new Ext.XTemplate(
                        /**/
                        '<tpl for=".">',
                        /**/
                        '<div class="combo-result-item" ext:qtip="{[this.getEndpointNameAndPath(values)]}">',
                        /**/
                        '<h3 style="height:16px;" >',
                        /**/
                        // '<div class="x-icon-combo-icon {IconCls}"></div>',
                        /**/
                        '<div class="ellipsis">{[Ext.util.Format.htmlEncode(values.Name)]}</div>',
                        /**/
                        '<span>{[xindex]}/{[xcount]}</span>',
                        /**/
                        '</h3>',
                        /**/
                        '<div class="ellipsis"><span class="title">' + $lang('msg_location_path') + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.Path)]}</span></div>',
                        /**/
                        '<div class="ellipsis"><span class="title">' + $lang('msg_container')     + ':</span><span class="value">{[Ext.util.Format.htmlEncode(values.TitleContainer)]}</span></div>',
                        /**/
                        '</div>',
                        /**/
                        '</tpl>',
                        {
                            getEndpointNameAndPath: function (endpoint) {
                                return endpoint.Path ? Ext.util.Format.htmlEncode(String.format('{0} ({1})', endpoint.Name, endpoint.Path)) : Ext.util.Format.htmlEncode(endpoint.Name);
                            }
                        });

                var defaultConfig = {
                    pageSize       : 50,
                    loadMask       : true,
                    resizable      : true,
                    mode           : 'remote',
                    displayField   : 'Name',
                    valueField     : 'KeyDataAcquisitionEndpoint',
                    listHeight     : 300,
                    hiddenName     : 'KeyDataAcquisitionEndpoint',
                    name           : 'KeyDataAcquisitionEndpoint',
                    triggerAction  : 'all',
                    forceSelection : true,
                    typeAhead      : false,
                    minChars       : 0,
                    lazyRender     : true,
                    tpl            : tpl,
                    itemSelector   : 'div.combo-result-item',
                    store          : {
                        xtype    : 'vizStoreDataAcquisitionEndpoint',
                        autoLoad : true
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.DataAcquisitionEndpoint.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizComboDataAcquisitionEndpoint", Viz.combo.DataAcquisitionEndpoint);
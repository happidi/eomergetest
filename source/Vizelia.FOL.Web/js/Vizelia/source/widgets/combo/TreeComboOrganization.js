﻿Ext.namespace('Viz.combo');

/**
 * @class Viz.combo.TreeComboOrganization
 * @extends Ext.combo.TreeCombo Summary.
 */
Viz.combo.TreeComboOrganization = Ext.extend(Viz.combo.TreeCombo, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler  : Viz.Services.CoreWCF.Organization_GetTree,
                    searchable      : true,
                    treeHeight      : 200,
                    // defaultIconCls : 'viz-icon-small-organization',
                    serviceParams   : {
                        flgFilter : false,
                        entity      : Viz.Configuration.GetClassificationItemDefinitionEntityFromCategory("organization")
                    },
                    entityKey       : 'KeyOrganization',
                    displayCheckbox : false,
                    rootVisible     : false,
                    root            : {
                        id      : ' ',
                        iconCls : 'viz-icon-small-organization',
                        text    : $lang('msg_tree_organization_root')
                    }
                };
                var forcedConfig = {

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.combo.TreeComboOrganization.superclass.constructor.call(this, config);
            }
        });

Ext.reg('viztreecomboOrganization', Viz.combo.TreeComboOrganization);

﻿Ext.namespace("Viz.combo");
/**
 * @class Viz.combo.ChartClassificationLevel
 * @extends Ext.form.ComboBox
 */
Viz.combo.ChartClassificationLevel = Ext.extend(Viz.form.ComboBox, {
    /**
     * Ctor.
     * @param {Object} config The configuration options
     */
    constructor : function(config) {
        config = config || {};
        var defaultConfig = {
            mode         : 'local',
            pageSize     : null,
            listHeight   : null,
            displayField : 'Id',
            valueField   : 'Value',
            store        : new Viz.store.ChartClassificationLevel({
                        // listeners : {
                        // load : this.onStoreLoad,
                        // scope : this
                        // },
                        autoLoad : true
                    })
        };
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);
        Viz.combo.ChartClassificationLevel.superclass.constructor.call(this, config);
    }// ,

        /**
         * Handler to set the va
         * @param {} store
         * @param {} records
         */
        // onStoreLoad : function(store, records) {
        // this.setValue(records[records.length - 1].data.Value);
        // }
    });
Ext.reg("vizComboChartClassificationLevel", Viz.combo.ChartClassificationLevel);
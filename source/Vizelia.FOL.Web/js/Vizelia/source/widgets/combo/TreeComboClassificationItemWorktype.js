﻿Ext.namespace('Viz.combo');
/**
 * @class Viz.combo.TreeComboClassificationItemWorktype
 * @extends Ext.combo.TreeCombo Summary.
 */
Viz.combo.TreeComboClassificationItemWorktype = Ext.extend(Viz.combo.TreeCombo, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var rootEntity = Viz.Configuration.GetClassificationItemDefinitionEntityFromCategory("worktype");
                rootEntity.Level = 1;
                var defaultConfig = {
                    serviceHandler  : Viz.Services.CoreWCF.ClassificationItem_GetTree,
                    treeHeight      : 200,
                    searchable      : true,
                    serviceParams   : {
                        flgFilter   : false,
                        excludePset : true,
                        entity      : rootEntity
                    },
                    entityKey       : 'KeyClassificationItem',
                    displayCheckbox : false,
                    rootVisible     : false,
                    root            : {
                        id      : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("worktype"),
                        iconCls : 'viz-icon-small-worktype',
                        text    : ''
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.combo.TreeComboClassificationItemWorktype.superclass.constructor.call(this, config);
            }
        });
Ext.reg('viztreecomboClassificationItemWorktype', Viz.combo.TreeComboClassificationItemWorktype);

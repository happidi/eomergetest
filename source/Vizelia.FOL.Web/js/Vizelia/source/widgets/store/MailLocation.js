﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.MailLocation
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MailLocation = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.ServiceDeskWCF.Location_GetStoreByKeyMail, 
                    serviceHandlerCrud : null,
                    serviceParams      : {
                    	KeyMail : config.KeyMail, 
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MailLocation.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreMailLocation", Viz.store.MailLocation);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AuthenticationProvider The factory class for the AuthenticationProvider store
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AuthenticationProvider = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.PublicWCF.AuthenticationProvider_GetStore
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.AuthenticationProvider.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreAuthenticationProvider", Viz.store.AuthenticationProvider);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.JobInstance
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.JobInstance = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler: Viz.Services.CoreWCF.JobInstance_GetStoreByJobId,
                    serviceParams      : {
                        location: Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        keyJob: config.KeyJob
                    },
                    sortInfo           : {
                        field     : 'EndDate',
                        direction : 'DESC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'EndDate'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.JobInstance.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreJobInstance", Viz.store.JobInstance);
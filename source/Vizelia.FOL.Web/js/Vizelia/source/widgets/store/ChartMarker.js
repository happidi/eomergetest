﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.ChartMarker
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartMarker = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.ChartMarker_GetStoreByKeyChart,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.ChartMarker.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreChartMarker", Viz.store.ChartMarker);
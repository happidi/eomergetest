﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.DataPoint
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.DataPoint = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.DataPoint_GetStoreFromChart,
                    serviceParams  : {
                        KeyChart : config.KeyChart,
                        paging   : config.paging
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'SeriesName'
                    },
                    sortInfo       : {
                        field     : 'XDateTime',
                        direction : 'ASC'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.DataPoint.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreDataPoint", Viz.store.DataPoint);
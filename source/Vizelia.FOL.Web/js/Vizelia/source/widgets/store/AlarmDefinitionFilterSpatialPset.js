﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.AlarmDefinitionFilterSpatialPset
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmDefinitionFilterSpatialPset = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.FilterSpatialPset_GetStoreByKeyAlarmDefinition,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyAlarmDefinition : config.KeyAlarmDefinition,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'PsetName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.AlarmDefinitionFilterSpatialPset.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreAlarmDefinitionFilterSpatialPset", Viz.store.AlarmDefinitionFilterSpatialPset);
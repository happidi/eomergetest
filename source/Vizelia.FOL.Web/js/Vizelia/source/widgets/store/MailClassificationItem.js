﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.MailClassificationItem
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MailClassificationItem = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.ServiceDeskWCF.ClassificationItem_GetStoreByKeyMail,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyMail  : config.KeyMail,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MailClassificationItem.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreMailClassificationItem", Viz.store.MailClassificationItem);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.CalendarDefinition
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.CalendarDefinition = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.CalendarEvent_GetStoreFromLocation,
                    serviceHandlerCrud : Viz.Services.CoreWCF.CalendarEvent_SaveStore,
                    serviceParams      : {
                        KeyLocation : config.KeyLocation,
                        category    : config.Category
                    },
                    sortInfo           : {
                        field     : 'StartDate',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.CalendarDefinition.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreCalendarDefinition", Viz.store.CalendarDefinition);
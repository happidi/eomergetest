﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.ChartAlgorithm
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartAlgorithm = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler: Viz.Services.EnergyWCF.ChartAlgorithm_GetStoreByKeyChart,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter: 'AlgorithmName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.ChartAlgorithm.superclass.constructor.call(this, config);
            }
        });
        Ext.reg("vizStoreChartAlgorithm", Viz.store.ChartAlgorithm);
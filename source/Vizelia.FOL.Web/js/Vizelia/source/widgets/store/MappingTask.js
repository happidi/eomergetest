﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.MappingTask
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MappingTask = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.MappingWCF.MappingTask_GetStore,
                    serviceHandlerCrud : Viz.Services.MappingWCF.MappingTask_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MappingTask.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreMappingTask", Viz.store.MappingTask);
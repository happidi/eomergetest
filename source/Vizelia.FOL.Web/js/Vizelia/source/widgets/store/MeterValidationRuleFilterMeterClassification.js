﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.MeterValidationRuleFilterMeterClassification
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MeterValidationRuleFilterMeterClassification = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.FilterMeterClassification_GetStoreByKeyMeterValidationRule,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                    	KeyMeterValidationRule : config.KeyMeterValidationRule,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MeterValidationRuleFilterMeterClassification.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreMeterValidationRuleFilterMeterClassification", Viz.store.MeterValidationRuleFilterMeterClassification);

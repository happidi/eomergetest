﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.CalculatedSerie
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MeterOperation = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.MeterOperation_GetStoreByKeyMeter,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyMeter          : config.KeyMeter,
                        KeyMeterOperation : config.KeyMeterOperation,
                        order             : config.Order || 0,
                        location          : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'LocalId'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.MeterOperation.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreMeterOperation", Viz.store.MeterOperation);
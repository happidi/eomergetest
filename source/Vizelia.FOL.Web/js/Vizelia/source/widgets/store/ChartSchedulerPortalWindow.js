﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.ChartSchedulerPortalWindow
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartSchedulerPortalWindow = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.PortalWindow_GetStoreByKeyChartScheduler,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyChartScheduler : config.KeyChartScheduler,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.ChartSchedulerPortalWindow.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreChartSchedulerPortalWindow", Viz.store.ChartSchedulerPortalWindow);
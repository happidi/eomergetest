﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ApplicationGroupAuthorization
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ApplicationGroupAuthorization = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.ApplicationGroup_Authorization_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        applicationGroupId : config.applicationGroupId,
                        location           : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'UserName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ApplicationGroupAuthorization.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreApplicationGroupAuthorization", Viz.store.ApplicationGroupAuthorization);
﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.StatisticalSerie
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.StatisticalSerie = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.StatisticalSerie_GetStoreByKeyChart,
                    serviceHandlerCrud : null,// Viz.Services.EnergyWCF.StatisticalSerie_SaveStore,
                    deleteAllFields    : true,
                    serviceParams      : {
                        KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'ClassificationItemTitle'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.StatisticalSerie.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreStatisticalSerie", Viz.store.StatisticalSerie);
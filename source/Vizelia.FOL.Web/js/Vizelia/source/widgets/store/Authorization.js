﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AuthorizationItem
 * <p>
 * The store containing all the authorizations items for a current user.
 * </p>
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AuthorizationItem = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.AuthorizationItem_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        username : config.username
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    },
                    listeners          : {
                        beforeload : {
                            scope : this,
                            fn    : this.onBeforeLoad
                        }
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AuthorizationItem.superclass.constructor.call(this, config);

            },
            onBeforeLoad: function (options) {
                // we handle the store onbeforeload event because we assume we have the proxy initialized at this point.
                // we're adding listener to the proxy beforeload event so we can add the authorization filters.
                // we can't add the filters in any other point because it gets overriden by grid filters in GridFilter onBeforeLoad (store's beforeload event handler) 
                this.proxy.addListener('beforeload', this.addFiltersToProxyParams, this, {single : true});
            },
            addFiltersToProxyParams : function(proxy, params) {
                if (this.authorizationFilter) {
                    params.filters = params.filters || [];
                    params.filters.push({
                                data  : {
                                    dataIndex : 'ItemType',
                                    type      : 'string',
                                    value     : this.authorizationFilter
                                },
                                field : 'ItemType'
                            });

                    params.filters.push({
                                data  : {
                                    dataIndex : 'IsFilterOnly',
                                    type      : 'boolean',
                                    value     : this.IsFilterOnly || false
                                },
                                field : 'IsFilterOnly'
                            });
                }
            }

        });

Ext.reg("vizStoreAuthorizationItem", Viz.store.AuthorizationItem);
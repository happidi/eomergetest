﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.CalendarEventCategoryTitle
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.CalendarEventCategoryTitle = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.Calendar_GetEventsTitle,
                    serviceParams  : {
                        KeyMeter                 : config.KeyMeter,
                        KeyCalendarEventCategory : config.KeyCalendarEventCategory,
                        location                 : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    },
                    autoLoad       : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.CalendarEventCategoryTitle.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreCalendarEventCategoryTitle", Viz.store.CalendarEventCategoryTitle);
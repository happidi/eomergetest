﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Mail
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Mail = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.ServiceDeskWCF.Mail_GetStore,
                    serviceHandlerCrud : Viz.Services.ServiceDeskWCF.Mail_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Subject'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Mail.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreMail", Viz.store.Mail);
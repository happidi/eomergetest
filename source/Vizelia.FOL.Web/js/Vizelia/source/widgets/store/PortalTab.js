﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.PortalTab
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.PortalTab = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.PortalTab_GetStoreByKeyPortalWindow,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.PortalTab_SaveStore,
                    deleteAllFields    : true,
                    serviceParams      : {
                        KeyPortalWindow : config.KeyPortalWindow,
                        location        : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'PortalWindowTitle, Title',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'PortalWindowTitle'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.PortalTab.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStorePortalTab", Viz.store.PortalTab);
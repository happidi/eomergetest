﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.PortalTemplateFOLMembershipUser
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.PortalTemplateFOLMembershipUser = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.FOLMembershipUser_GetStoreByKeyPortalTemplate,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyPortalTemplate : config.KeyPortalTemplate,
                        location          : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    },
                    deleteAllFields    : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.PortalTemplateFOLMembershipUser.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStorePortalTemplateFOLMembershipUser", Viz.store.PortalTemplateFOLMembershipUser);
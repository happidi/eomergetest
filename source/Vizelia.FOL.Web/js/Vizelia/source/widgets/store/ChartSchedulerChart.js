﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.ChartSchedulerChart
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartSchedulerChart = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Chart_GetStoreByKeyChartScheduler,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyChartScheduler : config.KeyChartScheduler,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.ChartSchedulerChart.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreChartSchedulerChart", Viz.store.ChartSchedulerChart);
﻿Ext.namespace('Viz.store');
Ext.namespace('Viz.Configuration.EnumRecords');
/**
 * @class Viz.store.Enum
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Enum = Ext.extend(Viz.data.WCFJsonSimpleStore, {

    /**
    * @cfg {Array} valuesToRemove List of values to remove from the store.
    */
    valuesToRemove: [],
    /**
    * Ctor.
    * @param {Object} config
    */
    constructor: function (config) {
        config = config || {};
        var defaultConfig = {
            serviceHandler: Viz.Services.CoreWCF.Enum_GetStore,
            baseParams: {
                // used by the combobox to filter results
                filter: 'MsgCode'
            },
            remoteSort: false,
            fields: ['Value', 'Order', 'IconCls', 'MsgCode', 'Id'],
            sortInfo: {
                field: 'Order',
                direction: 'ASC'
            }
        };
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);
        Viz.store.Enum.superclass.constructor.call(this, config);

        this.on('beforeload', this.onBeforeLoad, this);
        this.on('load', this.onLoad, this);

    },

    /**
    * Destroy
    * @private
    */
    onDestroy: function () {
        this.un('beforeload', this.onBeforeLoad, this);
        this.un('load', this.onLoad, this);

        Viz.store.Enum.superclass.onDestroy.apply(this, arguments);
    },

    /**
    * Listener for the beforeload event.
    * @param {Store} store
    * @param {Object} options
    * @return {Boolean}
    */
    onBeforeLoad: function (store, options) {
        var key = this.getHashKey();
        if (!Ext.isEmpty(Viz.Configuration.EnumRecords[key])) {
            var cachedRecords = Viz.copyDataRecords(Viz.Configuration.EnumRecords[key]);
            this.add(cachedRecords);
            this.hasLoaded = true;

            if (options.callback) {
                options.callback.call(options.scope || this);
            }

            this.fireEvent('load', this, cachedRecords, Ext.apply(options, {
                alreadyCached: true
            }));
            return false;
        }
        return true;
    },

    /**
    * Listener for the load event.
    * @param {Store} store
    * @param {Ext.data.Record[]} records
    * @param {Object} options
    */
    onLoad: function (store, records, options) {
        var key = this.getHashKey();

        this.sort(this.sortInfo.field, this.sortInfo.direction);
        records = store.data.items;
        if ((!options) || options.alreadyCached !== true) {
            var cachedRecords = Viz.copyDataRecords(records);
            Viz.Configuration.EnumRecords[key] = cachedRecords;
        }

        if (Ext.isArray(this.valuesToRemove)) {
            Ext.each(this.valuesToRemove, function (valueToRemove) {
                this.remove(this.getById(valueToRemove));
            }, this);
        }

        if (Ext.isArray(this.valuesToInclude)) {
            for (var i = this.data.items.length - 1; i >= 0; i--) {
                if (this.valuesToInclude.indexOf(this.data.items[i].id) == -1) {
                    this.removeAt(i);
                }
            }
        }

        return true;
    },

    /**
    * Returns the Hash Key used to store the records on the prototype, to prevent fetching the data from the server multiple times.
    * @return {String}
    */
    getHashKey: function () {
        return this.serviceParams.enumTypeName;
    }

});
Ext.reg("vizStoreEnum", Viz.store.Enum);

﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ReportSubscription
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ReportSubscription = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.ReportingWCF.ReportSubscription_GetAllByReport,
                    serviceHandlerCrud : Viz.Services.ReportingWCF.ReportSubscription_SaveStore,
                    serviceParams      : {
                        location   : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        reportPath : config.reportPath
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ReportSubscription.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreReportSubscription", Viz.store.ReportSubscription);
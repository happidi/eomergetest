﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.ActionRequest
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ActionRequest = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.ServiceDeskWCF.ActionRequest_GetStore,
                    serviceHandlerCrud : Viz.Services.ServiceDeskWCF.ActionRequest_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'ActualStart',
                        direction : 'DESC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'RequestID'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.ActionRequest.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreActionRequest", Viz.store.ActionRequest);
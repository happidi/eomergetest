﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.UserApplicationGroup
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.UserApplicationGroup = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.User_ApplicationGroup_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        username : config.username
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.UserApplicationGroup.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreUserApplicationGroup", Viz.store.UserApplicationGroup);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.MailPriority
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MailPriority = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.ServiceDeskWCF.Priority_GetStoreByKeyMail, 
                    serviceHandlerCrud : null,
                    serviceParams      : {
                    	KeyMail : config.KeyMail, 
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MailPriority.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreMailPriority", Viz.store.MailPriority);
﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.AlarmDefinitionFilterSpatialPsetLocation
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmDefinitionFilterSpatialPsetLocation = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.FilterSpatialPset_GetLocationStoreByKeyAlarmDefinition,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyAlarmDefinition : config.KeyAlarmDefinition,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'PsetName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.AlarmDefinitionFilterSpatialPsetLocation.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreAlarmDefinitionFilterSpatialPsetLocation", Viz.store.AlarmDefinitionFilterSpatialPsetLocation);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.MeterValidationRuleAlarmInstance
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MeterValidationRuleAlarmInstance = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.AlarmInstance_GetStoreByKeyMeterValidationRule,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.AlarmInstance_SaveStore,
                    serviceParams      : {
                        location      : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        KeyMeterValidationRule : config.KeyMeterValidationRule || ' '
                    },
                    sortInfo           : {
                        field     : 'Title',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MeterValidationRuleAlarmInstance.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreMeterValidationRuleAlarmInstance", Viz.store.MeterValidationRuleAlarmInstance);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Task
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Task = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.ServiceDeskWCF.Task_GetStoreFromActionRequest,
                    serviceHandlerCrud : Viz.Services.ServiceDeskWCF.Task_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Priority',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Task.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreTask", Viz.store.Task);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.DynamicDisplayColorTemplate
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.DynamicDisplayColorTemplate = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.DynamicDisplayColorTemplate_GetStore,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.DynamicDisplayColorTemplate_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Name',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.DynamicDisplayColorTemplate.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreDynamicDisplayColorTemplate", Viz.store.DynamicDisplayColorTemplate);
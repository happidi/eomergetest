﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.CalculatedSerie
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.CalculatedSerie = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.CalculatedSerie_GetStoreByKeyChart,
                    serviceHandlerCrud : null,// Viz.Services.EnergyWCF.CalculatedSerie_SaveStore,
                    deleteAllFields    : true,
                    serviceParams      : {
                        KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.CalculatedSerie.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreCalculatedSerie", Viz.store.CalculatedSerie);
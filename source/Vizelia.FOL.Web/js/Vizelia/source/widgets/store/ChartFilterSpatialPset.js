﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.ChartFilterSpatialPset
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartFilterSpatialPset = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.FilterSpatialPset_GetStoreByKeyChart,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'PsetName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.ChartFilterSpatialPset.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreChartFilterSpatialPset", Viz.store.ChartFilterSpatialPset);
﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.FlipCardEntry
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.FlipCardEntry = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.FlipCardEntry_GetStoreByKeyFlipCard,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyFlipCard : config.KeyFlipCard,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Order'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.FlipCardEntry.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreFlipCardEntry", Viz.store.FlipCardEntry);
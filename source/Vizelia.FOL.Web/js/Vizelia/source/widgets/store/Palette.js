﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Palette
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Palette = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Palette_GetStore,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.Palette_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Memory
                    },
                    sortInfo           : {
                        field     : 'MsgCode',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Label'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Palette.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStorePalette", Viz.store.Palette);
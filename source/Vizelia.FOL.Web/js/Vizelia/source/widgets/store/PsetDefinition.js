﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.PsetDefinition
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.PsetDefinition = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.PsetDefinition_GetStoreByUsageName,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        UsageName             : config.UsageName,
                        KeyClassificationItem : config.KeyClassificationItem
                    },
                    sortInfo           : {
                        field     : 'Label',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Label'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.PsetDefinition.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStorePsetDefinition", Viz.store.PsetDefinition);
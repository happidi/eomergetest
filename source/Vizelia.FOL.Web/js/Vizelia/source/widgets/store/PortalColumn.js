﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.PortalColumn
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.PortalColumn = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.PortalColumn_GetStoreByKeyPortalTab,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.PortalColumn_SaveStore,
                    deleteAllFields    : true,
                    serviceParams      : {
                        KeyPortalTab : config.KeyPortalTab,
                        location     : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'PortalWindowTitle, PortalTabTitle',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'PortalWindowTitle'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.PortalColumn.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStorePortalColumn", Viz.store.PortalColumn);
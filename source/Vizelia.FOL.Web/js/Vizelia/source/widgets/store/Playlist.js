﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Playlist
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Playlist = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Playlist_GetStore,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.Playlist_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Name',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Playlist.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStorePlaylist", Viz.store.Playlist);
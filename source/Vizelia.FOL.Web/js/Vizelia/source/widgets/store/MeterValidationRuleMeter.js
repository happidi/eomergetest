﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.MeterValidationRuleMeter
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MeterValidationRuleMeter = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Meter_GetStoreByKeyMeterValidationRule,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyMeterValidationRule : config.KeyMeterValidationRule,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.MeterValidationRuleMeter.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreMeterValidationRuleMeter", Viz.store.MeterValidationRuleMeter);
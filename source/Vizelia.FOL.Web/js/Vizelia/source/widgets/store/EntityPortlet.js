﻿Ext.namespace('Viz.store');
/**
* @class Viz.store.PortalColumn
* @extends Viz.data.WCFJsonSimpleStore
*/
Viz.store.EntityPortlet = Ext.extend(Viz.data.WCFJsonSimpleStore, {
    /**
    * Ctor.
    * @param {Object} config
    */
    constructor: function (config) {
        config = config || {};
        var defaultConfig = {
            serviceHandler: Viz.Services.EnergyWCF.Portlet_GetStoreByKeyEntity,
            serviceHandlerCrud: null,
            serviceParams: {
                KeyEntity: config.KeyEntity,
                TypeEntity: config.TypeEntity,
                location: Vizelia.FOL.BusinessEntities.PagingLocation.Database
            },
            baseParams: {
                // used by the combobox to filter results
                filter: 'Title, PortalTabTitle, PortalWindowTitle'
            }
        };
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);
        Viz.store.Portlet.superclass.constructor.call(this, config);
    }
});
Ext.reg("vizStoreEntityPortlet", Viz.store.EntityPortlet);
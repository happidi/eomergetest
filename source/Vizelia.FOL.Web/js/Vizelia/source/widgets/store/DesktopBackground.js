﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.DesktopBackground
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.DesktopBackground = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.DesktopBackground_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                       location        : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Label'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.DesktopBackground.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreDesktopBackground", Viz.store.DesktopBackground);
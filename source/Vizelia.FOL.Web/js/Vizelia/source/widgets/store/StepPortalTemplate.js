﻿Ext.namespace('Viz.store');

/**
* @class Viz.store.StepPortalTemplate
* @extends Viz.data.WCFJsonSimpleStore
*/
Viz.store.StepPortalTemplate = Ext.extend(Viz.data.WCFJsonSimpleStore, {
    /**
    * Ctor.
    * @param {Object} config
    */
    constructor: function (config) {
        config = config || {};
        var defaultConfig = {
            serviceHandler: Viz.Services.EnergyWCF.PortalTemplate_GetStoreByJobStep,
            serviceHandlerCrud: null,
            serviceParams: {
                location: Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                keyJob: config.keyJob,
                keyStep: config.keyStep
            },
            baseParams: {
                // used by the combobox to filter results
                filter: 'Name'
            }
        };

        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.store.StepPortalTemplate.superclass.constructor.call(this, config);

    }
});

Ext.reg("vizStoreStepPortalTemplate", Viz.store.StepPortalTemplate);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.DetailedAudit
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.DetailedAudit = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.DetailedAudit_GetStoreAuditByKey,
                    serviceParams  : {
                        entityName : config.entityName,
                        entityKey  : config.entityKey,
                        location   : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.DetailedAudit.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreDetailedAudit", Viz.store.DetailedAudit);
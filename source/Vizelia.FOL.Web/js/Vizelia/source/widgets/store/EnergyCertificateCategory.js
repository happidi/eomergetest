﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.EnergyCertificateCategory
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.EnergyCertificateCategory = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.EnergyCertificateCategory_GetStoreByKeyEnergyCertificate,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyEnergyCertificate : config.KeyEnergyCertificate,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.EnergyCertificateCategory.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreEnergyCertificateCategory", Viz.store.EnergyCertificateCategory);
﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.PortalColumn
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Portlet = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Portlet_GetStoreByKeyPortalColumn,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.Portlet_SaveStore,
                    deleteAllFields    : true,
                    serviceParams      : {
                        KeyPortalColumn : config.KeyPortalColumn,
                        location        : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'KeyPortalTab'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.Portlet.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStorePortlet", Viz.store.Portlet);
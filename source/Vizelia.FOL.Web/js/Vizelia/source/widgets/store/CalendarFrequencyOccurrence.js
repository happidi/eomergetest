﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.CalendarFrequencyOccurrence
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.CalendarFrequencyOccurrence = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.CalendarFrequencyOccurrence_GetStore,
                    serviceParams  : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.CalendarFrequencyOccurrence.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreCalendarFrequencyOccurrence", Viz.store.CalendarFrequencyOccurrence);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Tenant
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Tenant = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler: Viz.Services.TenancyWCF.Tenant_GetStore,
                    serviceHandlerCrud: Viz.Services.TenancyWCF.Tenant_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Tenant.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreTenant", Viz.store.Tenant);
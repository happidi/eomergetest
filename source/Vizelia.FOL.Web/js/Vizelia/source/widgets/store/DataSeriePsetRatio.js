﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.DataSeriePsetRatio
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.DataSeriePsetRatio = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.DataSeriePsetRatio_GetStoreByKeyDataSerie,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyDataSerie : config.KeyDataSerie,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'PsetName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.DataSeriePsetRatio.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreDataSeriePsetRatio", Viz.store.DataSeriePsetRatio);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AlarmTable
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmTable = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.AlarmTable_GetStore,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.AlarmTable_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Title',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AlarmTable.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreAlarmTable", Viz.store.AlarmTable);
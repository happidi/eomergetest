﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Occupant
 * @extends Viz.data.WCFJsonSimpleStore The factory class for the CRUD Occupant store.
 */
Viz.store.Occupant = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.Occupant_GetStore,
                    serviceHandlerCrud : Viz.Services.CoreWCF.Occupant_SaveStore,
                    sortInfo           : {
                        field     : 'LastName',
                        direction : 'ASC'
                    },
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        filter : 'FirstName LastName Id OrganizationName FirstName+LastName LastName+FirstName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Occupant.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreOccupant", Viz.store.Occupant);
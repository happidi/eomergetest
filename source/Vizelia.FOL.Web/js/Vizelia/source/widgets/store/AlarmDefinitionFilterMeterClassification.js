﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AlarmDefinitionFilterMeterClassification
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmDefinitionFilterMeterClassification = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.FilterMeterClassification_GetStoreByKeyAlarmDefinition,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                    	KeyAlarmDefinition : config.KeyAlarmDefinition,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AlarmDefinitionFilterMeterClassification.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAlarmDefinitionFilterMeterClassification", Viz.store.AlarmDefinitionFilterMeterClassification);

﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.DrawingCanvasChart
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.DrawingCanvasChart = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.DrawingCanvasChart_GetStoreByKeyDrawingCanvas,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyDrawingCanvas : config.KeyDrawingCanvas,
                        location        : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.DrawingCanvasChart.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreDrawingCanvasChart", Viz.store.DrawingCanvasChart);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AlarmTableFilterAlarmDefinitionClassification
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmTableFilterAlarmDefinitionClassification = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler: Viz.Services.EnergyWCF.FilterAlarmDefinitionClassification_GetStoreByKeyAlarmTable,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyAlarmTable: config.KeyAlarmTable,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AlarmTableFilterAlarmDefinitionClassification.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAlarmTableFilterAlarmDefinitionClassification", Viz.store.AlarmTableFilterAlarmDefinitionClassification);

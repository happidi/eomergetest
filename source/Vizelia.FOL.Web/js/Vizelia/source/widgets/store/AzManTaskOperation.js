﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AzManTaskOperation
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AzManTaskOperation = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.AuthenticationWCF.AzManOperation_GetStoreByKeyAzManTask, 
                    serviceHandlerCrud : null,
                    serviceParams      : {
                    	KeyAzManTask : config.KeyAzManTask, 
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Memory
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AzManTaskOperation.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAzManTaskOperation", Viz.store.AzManTaskOperation);
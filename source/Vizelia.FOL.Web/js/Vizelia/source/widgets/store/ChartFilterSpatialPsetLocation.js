﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.ChartFilterSpatialPsetLocation
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartFilterSpatialPsetLocation = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.FilterSpatialPset_GetLocationStoreByKeyChart,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'PsetName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.ChartFilterSpatialPsetLocation.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreChartFilterSpatialPsetLocation", Viz.store.ChartFilterSpatialPsetLocation);
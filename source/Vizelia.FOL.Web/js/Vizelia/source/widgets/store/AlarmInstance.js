﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AlarmInstance
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmInstance = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.AlarmInstance_GetStoreByKeyAlarmDefinition,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.AlarmInstance_SaveStore,
                    serviceParams      : {
                        location           : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        KeyAlarmDefinition : config.KeyAlarmDefinition
                    },
                    sortInfo           : {
                        field     : 'InstanceDateTime',
                        direction : 'DESC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AlarmInstance.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAlarmInstance", Viz.store.AlarmInstance);
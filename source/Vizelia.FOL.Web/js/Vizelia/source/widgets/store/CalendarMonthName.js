﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.CalendarMonthName
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.CalendarMonthName = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.CalendarMonthName_GetStore,
                    serviceParams  : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.CalendarMonthName.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreCalendarMonthName", Viz.store.CalendarMonthName);
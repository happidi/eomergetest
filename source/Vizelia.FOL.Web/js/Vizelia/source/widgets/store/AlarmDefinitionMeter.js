﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.AlarmDefinitionMeter
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmDefinitionMeter = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Meter_GetStoreByKeyAlarmDefinition,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyAlarmDefinition : config.KeyAlarmDefinition,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.AlarmDefinitionMeter.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreAlarmDefinitionMeter", Viz.store.AlarmDefinitionMeter);
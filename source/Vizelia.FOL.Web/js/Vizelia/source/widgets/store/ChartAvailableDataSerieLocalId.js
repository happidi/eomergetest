﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ChartAvailableDataSerieLocalId
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartAvailableDataSerieLocalId = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.Chart_GetAvailableDataSerieLocalId,
                    serviceParams  : {
                        Key             : config.KeyChart,
                        includeExisting : config.includeExisting === true ? true : false,
                        location        : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ChartAvailableDataSerieLocalId.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreChartAvailableDataSerieLocalId", Viz.store.ChartAvailableDataSerieLocalId);
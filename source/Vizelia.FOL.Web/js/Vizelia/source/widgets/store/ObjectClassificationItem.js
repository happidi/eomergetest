﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ObjectClassificationItem
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ObjectClassificationItem = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.ClassificationItem_GetStoreByKeyObject,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyObject  : config.KeyObject,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ObjectClassificationItem.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreObjectClassificationItem", Viz.store.ObjectClassificationItem);
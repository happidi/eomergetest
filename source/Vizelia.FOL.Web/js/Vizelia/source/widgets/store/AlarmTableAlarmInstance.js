﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AlarmTableAlarmInstance
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmTableAlarmInstance = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.AlarmInstance_GetStoreByKeyAlarmTable,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.AlarmInstance_SaveStore,
                    serviceParams      : {
                        location      : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        KeyAlarmTable : config.KeyAlarmTable
                    },
                    sortInfo           : {
                        field     : 'Title',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AlarmTableAlarmInstance.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreAlarmTableAlarmInstance", Viz.store.AlarmTableAlarmInstance);
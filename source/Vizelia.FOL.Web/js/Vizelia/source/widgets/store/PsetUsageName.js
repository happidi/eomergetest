﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.UsageName
 * @extends Viz.data.WCFJsonSimpleStore The factory class for the CRUD Occupant store.
 */
Viz.store.PsetUsageName = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.ListElement_GetStore,
                    sortInfo       : {
                        field     : 'Value',
                        direction : 'ASC'
                    },
                    serviceParams  : {
                        listName : 'PsetUsageName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.PsetUsageName.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStorePsetUsageName", Viz.store.PsetUsageName);
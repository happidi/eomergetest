﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ReportParameterValue
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ReportParameterValue = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.ReportingWCF.GetReportParameterValues,
                    //serviceHandlerCrud : Viz.Services.ReportingWCF.ReportParameterValue_SaveStore,
                    serviceParams      : {
                        key           : config.key,
                        parameterName : config.parameterName
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Label'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ReportParameterValue.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreReportParameterValue", Viz.store.ReportParameterValue);
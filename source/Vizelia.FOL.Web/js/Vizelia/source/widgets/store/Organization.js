﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Organization
 * @extends Viz.data.WCFJsonSimpleStore * The factory class for the CRUD Organization store.
 */
Viz.store.Organization = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                   //Doesn't do anything!
                   // serviceHandler     : Viz.Services.TestWCF.GetStoreOrganization,
                    serviceHandlerCrud : null,
                    sortInfo           : {
                        field     : 'Name', // or Id
                        direction : 'ASC'
                    },
                    serviceParams      : {
                        flgServer : true
                    },
                    baseParams         : {
                        filter : 'Id Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Organization.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreOrganization", Viz.store.Organization);

﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.RESTDataAcquisitionProvider
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.RESTDataAcquisitionProvider = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.RESTDataAcquisitionProvider_GetStore,
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.RESTDataAcquisitionProvider.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreRESTDataAcquisitionProvider", Viz.store.RESTDataAcquisitionProvider);

﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.AlarmTableAlarmDefinition
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmTableAlarmDefinition = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler: Viz.Services.EnergyWCF.AlarmDefinition_GetStoreByKeyAlarmTable,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyAlarmTable : config.KeyAlarmTable,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.AlarmTableAlarmDefinition.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreAlarmTableAlarmDefinition", Viz.store.AlarmTableAlarmDefinition);
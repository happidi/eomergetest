﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.ChartMeterAll
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartMeterAll = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Chart_GetMetersStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyChart : config.KeyChart
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.ChartMeterAll.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreChartMeterAll", Viz.store.ChartMeterAll);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AzManFilterReport
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AzManFilterReport = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.AzManFilter_Filter_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        filterId : config.filterId,
                        typeName : 'Vizelia.FOL.BusinessEntities.Report'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AzManFilterReport.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAzManFilterReport", Viz.store.AzManFilterReport);
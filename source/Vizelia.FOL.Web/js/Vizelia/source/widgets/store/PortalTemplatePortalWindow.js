﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.PortalTemplatePortalWindow
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.PortalTemplatePortalWindow = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.PortalWindow_GetStoreByKeyPortalTemplate,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyPortalTemplate : config.KeyPortalTemplate,
                        location          : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.PortalTemplatePortalWindow.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStorePortalTemplatePortalWindow", Viz.store.PortalTemplatePortalWindow);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ApplicationGroupAzManRole
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ApplicationGroupAzManRole = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.ApplicationGroup_AzManRole_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        applicationGroupId : config.applicationGroupId,
                        location           : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ApplicationGroupAzManRole.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreApplicationGroupAzManRole", Viz.store.ApplicationGroupAzManRole);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ChartKPIAzManRole
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartKPIAzManRole = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.Chart_AzManRole_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ChartKPIAzManRole.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreChartKPIAzManRole", Viz.store.ChartKPIAzManRole);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.PsetList
 * @extends Viz.data.WCFJsonSimpleStore The factory class for the PsetList store.
 */
Viz.store.PsetList = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.PsetListElement_GetStore,
                    serviceHandlerCrud : Viz.Services.CoreWCF.PsetListElement_SaveStore,
                    remoteSort         : true,
                    sortInfo           : {
                        field     : 'MsgCode',
                        direction : 'ASC'
                    },
                    serviceParams      : {
                        listName : config.listName
                    }
                };
                var forcedConfig = {
                    isCrud : true
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.PsetList.superclass.constructor.call(this, config);

            }

        });

Ext.reg("vizStorePsetList", Viz.store.PsetList);
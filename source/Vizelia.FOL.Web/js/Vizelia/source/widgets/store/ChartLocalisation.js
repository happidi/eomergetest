﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ChartLocalisation
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartLocalisation = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor  : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Chart_GetLocalisationStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'MsgCode'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ChartLocalisation.superclass.constructor.call(this, config);

                this.on('beforeload', this.onBeforeLoad, this);
                this.on('load', this.onLoad, this);

            },

            /**
             * Destroy
             * @private
             */
            onDestroy   : function() {
                this.un('beforeload', this.onBeforeLoad, this);
                this.un('load', this.onLoad, this);

                Viz.store.ChartLocalisation.superclass.onDestroy.apply(this, arguments);
            },
            
            /**
             * Listener for the beforeload event.
             * @param {Store} store
             * @param {Object} options
             * @return {Boolean}
             */
            onBeforeLoad : function(store, options) {
                return this.beforeLoadWithCache(store, options, "ChartLocalisation");
            },

            /**
             * Listener for the load event.
             * @param {Store} store
             * @param {Ext.data.Record[]} records
             * @param {Object} options
             */
            onLoad       : function(store, records, options) {
                this.loadWithCache(store, records, options, "ChartLocalisation");
            }
        });

Ext.reg("vizStoreChartLocalisation", Viz.store.ChartLocalisation);
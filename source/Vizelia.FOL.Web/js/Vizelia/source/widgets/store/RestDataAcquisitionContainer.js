﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.RESTDataAcquisitionContainer
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.RESTDataAcquisitionContainer = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.RESTDataAcquisitionContainer_GetStore,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.RESTDataAcquisitionContainer_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Title',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.RESTDataAcquisitionContainer.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreRESTDataAcquisitionContainer", Viz.store.RESTDataAcquisitionContainer);
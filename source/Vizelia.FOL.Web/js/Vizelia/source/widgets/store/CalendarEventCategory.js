﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.CalendarEventCategory
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.CalendarEventCategory = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.CalendarEventCategory_GetStore,
                    serviceHandlerCrud : Viz.Services.CoreWCF.CalendarEventCategory_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'LocalId',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Label'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.CalendarEventCategory.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreCalendarEventCategory", Viz.store.CalendarEventCategory);
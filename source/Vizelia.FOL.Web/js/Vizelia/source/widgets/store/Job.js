﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Job
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Job = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.Job_GetStore,
                    serviceHandlerCrud : Viz.Services.CoreWCF.Job_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Memory
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Job.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreJob", Viz.store.Job);
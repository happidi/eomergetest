﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.EntityCount
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.EntityCount = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.EntityCount_GetStore,
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.EntityCount.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreEntityCount", Viz.store.EntityCount);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.MeterDataExportTask
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MeterDataExportTask = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.MappingWCF.MeterDataExportTask_GetStore,
                    serviceHandlerCrud : Viz.Services.MappingWCF.MeterDataExportTask_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MeterDataExportTask.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreMeterDataExportTask", Viz.store.MeterDataExportTask);
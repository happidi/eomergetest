﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.CalendarDayName
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.CalendarDayName = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.CalendarDayName_GetStore,
                    serviceParams  : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.CalendarDayName.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreCalendarDayName", Viz.store.CalendarDayName);
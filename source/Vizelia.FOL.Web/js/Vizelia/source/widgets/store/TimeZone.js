﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.TimeZone
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.TimeZone = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor  : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.TimeZone_GetStore,
                    serviceParams  : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.TimeZone.superclass.constructor.call(this, config);
                this.on('beforeload', this.onBeforeLoad, this);
                this.on('load', this.onLoad, this);

            },
            /**
             * Listener for the beforeload event.
             * @param {Store} store
             * @param {Object} options
             * @return {Boolean}
             */
            onBeforeLoad : function(store, options) {
                return this.beforeLoadWithCache(store, options,"TimeZones");
            },

            /**
             * Listener for the load event.
             * @param {Store} store
             * @param {Ext.data.Record[]} records
             * @param {Object} options
             */
            onLoad       : function(store, records, options) {
                this.loadWithCache(store, records, options, "TimeZones");
            }
        });

Ext.reg("vizStoreTimeZone", Viz.store.TimeZone);
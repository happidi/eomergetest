﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AzManFilterAlarmDefinition
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AzManFilterAlarmDefinition = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.AzManFilter_Filter_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        filterId : config.filterId,
                        typeName : 'Vizelia.FOL.BusinessEntities.AlarmDefinition'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AzManFilterAlarmDefinition.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAzManFilterAlarmDefinition", Viz.store.AzManFilterAlarmDefinition);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Localization
 * @extends Viz.data.WCFJsonSimpleStore The factory class for the Localization store.
 */
Viz.store.Localization = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.LocalizationResource_GetStore,
                    serviceHandlerCrud : Viz.Services.CoreWCF.LocalizationResource_SaveStore,
                    deleteAllFields    : true,
                    sortInfo           : {
                        field     : 'Key',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        filter : 'Key'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Localization.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("LocalizationChange", this.reload, this);
            },

            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("LocalizationChange", this.reload, this);
                Viz.store.Localization.superclass.onDestroy.apply(this, arguments);
            }

        });

Ext.reg("vizStoreLocalization", Viz.store.Localization);
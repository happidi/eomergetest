﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.WeatherLocationSearch
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.WeatherLocationSearch = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.WeatherLocationSearch_GetStore,
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.WeatherLocationSearch.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreWeatherLocationSearch", Viz.store.WeatherLocationSearch);

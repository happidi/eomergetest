﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Meter
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Meter = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.Meter_GetStore,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.Meter_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'LocalId',
                        direction : 'ASC'
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'LocalId Name ClassificationItemLongPath LocationLongPath'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Meter.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreMeter", Viz.store.Meter);
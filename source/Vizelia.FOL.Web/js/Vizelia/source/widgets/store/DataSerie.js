﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.DataSerie
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.DataSerie = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.DataSerie_GetStoreByKeyChart,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.DataSerie.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreDataSerie", Viz.store.DataSerie);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ChartKPI
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartKPI = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Chart_GetStoreKPI,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.Chart_SaveStore,
                    serviceParams      : {
                        location                 : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        KeyClassificationItemKPI : config.KeyClassificationItemKPI
                    },
                    sortInfo           : {
                        field     : 'Title',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    },
                    deleteAllFields    : true
                };

                Ext.apply(defaultConfig, {
                            baseParams : {
                                filters : [{
                                            data  : {
                                                dataIndex : 'IsKPI',
                                                type      : 'innerjoin',
                                                value     : 'true'
                                            },
                                            field : 'IsKPI'
                                        }]
                            }
                        });

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ChartKPI.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreChartKPI", Viz.store.ChartKPI);
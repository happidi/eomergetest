﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AzManFilterPortalTemplate
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AzManFilterPortalTemplate = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.AzManFilter_Filter_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        filterId : config.filterId,
                        typeName : 'Vizelia.FOL.BusinessEntities.PortalTemplate'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AzManFilterPortalTemplate.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAzManFilterPortalTemplate", Viz.store.AzManFilterPortalTemplate);
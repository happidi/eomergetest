﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.DrawingCanvasImage
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.DrawingCanvasImage = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.DrawingCanvasImage_GetStoreByKeyDrawingCanvas,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyDrawingCanvas : config.KeyDrawingCanvas,
                        location        : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.DrawingCanvasImage.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreDrawingCanvasImage", Viz.store.DrawingCanvasImage);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AlarmTableFilterSpatial
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmTableFilterSpatial = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.FilterSpatial_GetStoreByKeyAlarmTable,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                    	KeyAlarmTable : config.KeyAlarmTable,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AlarmTableFilterSpatial.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAlarmTableFilterSpatial", Viz.store.AlarmTableFilterSpatial);

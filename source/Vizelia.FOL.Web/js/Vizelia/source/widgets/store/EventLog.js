﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.EventLog
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.EventLog = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.EventLog_GetStore,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.EventLog_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.EventLog.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreEventLog", Viz.store.EventLog);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.LocalizationCulture
 * <p>
 * The factory class for the Culture store. Exposes a list of cultures supported in the application
 * </p>
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.LocalizationCulture = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.LocalizationCulture_GetStore,
                    baseParams     : {
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.LocalizationCulture.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreLocalizationCulture", Viz.store.LocalizationCulture);
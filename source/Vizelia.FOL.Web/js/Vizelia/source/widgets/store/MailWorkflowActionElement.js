﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.MailWorkflowActionElement
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MailWorkflowActionElement = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    deleteAllFields    : true,
                    serviceHandler     : Viz.Services.ServiceDeskWCF.Workflow_GetStoreWorkflowActionElementByKeyMail,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyMail  : config.KeyMail,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Id'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MailWorkflowActionElement.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreMailWorkflowActionElement", Viz.store.MailWorkflowActionElement);

﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Map
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Map = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Map_GetStore,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.Map_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Title',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Map.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreMap", Viz.store.Map);
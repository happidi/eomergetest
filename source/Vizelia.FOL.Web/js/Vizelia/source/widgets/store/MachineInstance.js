﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Map
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MachineInstance = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.MachineInstance_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {},
                    sortInfo           : {
                        field     : 'InstanceName',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'InstanceName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MachineInstance.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreMachineInstance", Viz.store.MachineInstance);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.UserFiltered
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.UserFiltered = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.User_GetStoreFiltered,
                    serviceHandlerCrud : null,
                    sortInfo           : {
                        field     : 'UserName',
                        direction : 'ASC'
                    },
                    serviceParams      : {
                        roles     : config.roles,
                        locations : config.locations
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.UserFiltered.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreUserFiltered", Viz.store.UserFiltered);

﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AuditEntity
 * @extends Viz.data.WCFJsonSimpleStore The factory class for the AuditEntity store.
 */
Viz.store.AuditEntity = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.AuditEntity_GetStore,
                    serviceHandlerCrud : Viz.Services.CoreWCF.AuditEntity_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {}
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AuditEntity.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAuditEntity", Viz.store.AuditEntity);
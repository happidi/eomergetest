﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Priority
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Priority = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.ServiceDeskWCF.Priority_GetStore,
                    serviceHandlerCrud : Viz.Services.ServiceDeskWCF.Priority_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Value',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Priority.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStorePriority", Viz.store.Priority);
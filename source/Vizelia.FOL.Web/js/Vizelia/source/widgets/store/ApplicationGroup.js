﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ApplicationGroup
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ApplicationGroup = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.ApplicationGroup_GetStore,
                    serviceHandlerCrud : Viz.Services.AuthenticationWCF.ApplicationGroup_SaveStore,
                    serviceParams      : null,
                    baseParams         : {
                        // use by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ApplicationGroup.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreApplicationGroup", Viz.store.ApplicationGroup);
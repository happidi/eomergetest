Ext.namespace('Viz.store');

/**
 * @class Viz.store.AzManOperation
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AzManOperation = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.AzManOperation_GetStore,
                    serviceHandlerCrud : Viz.Services.AuthenticationWCF.AzManOperation_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AzManOperation.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAzManOperation", Viz.store.AzManOperation);
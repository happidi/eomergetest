﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AzManTask
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AzManTask = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.AzManTask_GetStore,
                    serviceHandlerCrud : Viz.Services.AuthenticationWCF.AzManTask_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        onlyUITasks : true
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AzManTask.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAzManTask", Viz.store.AzManTask);
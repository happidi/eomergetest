﻿Ext.namespace('Viz.store');

/**
* @class Viz.store.EWSDataAcquisitionEndpoint
* @extends Viz.data.WCFJsonSimpleStore
*/
Viz.store.EWSDataAcquisitionEndpoint = Ext.extend(Viz.data.WCFJsonSimpleStore, {
    /**
    * Ctor.
    * @param {Object} config
    */
    constructor: function (config) {
        config = config || {};
        var defaultConfig = {
            serviceHandler: Viz.Services.EnergyWCF.EWSDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer,
            serviceHandlerCrud: Viz.Services.EnergyWCF.EWSDataAcquisitionEndpoint_SaveStore,
            serviceParams: {
                KeyDataAcquisitionContainer: config.KeyDataAcquisitionContainer,
                location: Vizelia.FOL.BusinessEntities.PagingLocation.Database
            },
            sortInfo: {
                field: 'Name',
                direction: 'ASC'
            },
            baseParams: {
                // used by the combobox to filter results
                filter: 'Name'
            }
        };
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.store.EWSDataAcquisitionEndpoint.superclass.constructor.call(this, config);
    }
});

Ext.reg("vizStoreEWSDataAcquisitionEndpoint", Viz.store.EWSDataAcquisitionEndpoint);
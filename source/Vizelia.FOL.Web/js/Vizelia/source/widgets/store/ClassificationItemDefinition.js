﻿Ext.namespace('Viz.store');

/**
 * Exposes a list a the definition ClassificationItemDefinition
 * @class Viz.store.ClassificationItemDefinition
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ClassificationItemDefinition = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.ClassificationItemDefinition_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ClassificationItemDefinition.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreClassificationItemDefinition", Viz.store.ClassificationItemDefinition);
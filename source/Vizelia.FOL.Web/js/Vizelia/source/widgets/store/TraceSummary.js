﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.TraceSummary
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.TraceSummary = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.TraceSummary_GetStore,
                    serviceHandlerCrud : Viz.Services.CoreWCF.TraceSummary_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Severity',
                        direction : 'DESC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'TraceCategory'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.TraceSummary.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreTraceSummary", Viz.store.TraceSummary);
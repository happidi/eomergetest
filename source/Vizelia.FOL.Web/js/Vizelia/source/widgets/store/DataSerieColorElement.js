﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.DataSerieColorElement
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.DataSerieColorElement = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.DataSerieColorElement_GetStoreByKeyDataSerie,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyDataSerie : config.KeyDataSerie,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Color'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.DataSerieColorElement.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreDataSerieColorElement", Viz.store.DataSerieColorElement);
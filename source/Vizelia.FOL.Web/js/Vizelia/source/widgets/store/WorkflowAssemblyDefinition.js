﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.WorkflowAssemblyDefinition
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.WorkflowAssemblyDefinition = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.ServiceDeskWCF.Workflow_GetAllWorkflows,
                    serviceParams  : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo       : {
                        field     : 'AssemblyQualifiedName',
                        direction : 'ASC'
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.WorkflowAssemblyDefinition.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreWorkflowAssemblyDefinition", Viz.store.WorkflowAssemblyDefinition);
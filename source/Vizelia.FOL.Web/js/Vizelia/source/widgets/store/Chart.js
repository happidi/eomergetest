﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Chart
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Chart = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Chart_GetStore,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.Chart_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Title',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                if (config.isKPI) {
                    Ext.apply(defaultConfig, {
                                baseParams      : {
                                    filter  : 'Title',
                                    filters : [{
                                                data  : {
                                                    dataIndex : 'IsKPI',
                                                    type      : 'innerjoin',
                                                    value     : 'true'
                                                },
                                                field : 'IsKPI'
                                            }]
                                },
                                deleteAllFields : true
                            });
                }
                else if (config.isFavorite) {
                    Ext.apply(defaultConfig, {
                                baseParams : {
                                    filter  : 'Title',
                                    filters : [{
                                                data  : {
                                                    dataIndex : 'IsFavorite',
                                                    type      : 'innerjoin',
                                                    value     : 'true'
                                                },
                                                field : 'IsFavorite'
                                            }]
                                }
                            });
                }
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Chart.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreChart", Viz.store.Chart);
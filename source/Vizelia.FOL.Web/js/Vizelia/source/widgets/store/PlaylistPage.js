﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.PlaylistPage
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.PlaylistPage = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.PlaylistPage_GetStoreByKeyPlaylist,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyPlaylist : config.KeyPlaylist,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Order'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.PlaylistPage.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStorePlaylistPage", Viz.store.PlaylistPage);
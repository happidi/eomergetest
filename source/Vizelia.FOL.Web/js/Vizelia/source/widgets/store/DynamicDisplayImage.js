﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.DynamicDisplayImage
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.DynamicDisplayImage = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.DynamicDisplayImage_GetStore,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.DynamicDisplayImage_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        type     : Ext.isDefined(config.type) ? config.type : Vizelia.FOL.BusinessEntities.DynamicDisplayImageType.Image
                    },
                    sortInfo           : {
                        field     : 'Name',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.DynamicDisplayImage.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreDynamicDisplayImage", Viz.store.DynamicDisplayImage);
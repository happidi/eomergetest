﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ListElement
 * @extends Viz.data.WCFJsonSimpleStore The factory class for the list element store.
 */
Viz.store.ListElement = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * @cfg {String} listName The list name.
             */

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.ListElement_GetStore,
                    // serviceHandlerCrud : Viz.Services.CoreWCF.ListElement_SaveStore,
                    sortInfo       : {
                        field     : 'Label',
                        direction : 'ASC'
                    },
                    serviceParams  : {
                        listName : config.listName
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ListElement.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreListElement", Viz.store.ListElement);
﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.LoginHistoryActive
 * @extends Viz.store.LoginHistory
 */
Viz.store.LoginHistoryActive = Ext.extend(Viz.store.LoginHistory, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.LoginHistoryActive_GetStore,
                    serviceHandlerCrud : Ext.emptyFn
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.LoginHistoryActive.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreLoginHistoryActive", Viz.store.LoginHistoryActive);
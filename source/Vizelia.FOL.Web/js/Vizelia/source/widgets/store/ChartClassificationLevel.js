﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ChartClassificationLevel
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartClassificationLevel = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor  : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Chart_GetClassificationLevelStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ChartClassificationLevel.superclass.constructor.call(this, config);

                this.on('beforeload', this.onBeforeLoad, this);
                this.on('load', this.onLoad, this);
            },

            /**
             * Listener for the beforeload event.
             * @param {Store} store
             * @param {Object} options
             * @return {Boolean}
             */
            onBeforeLoad : function(store, options) {
                return this.beforeLoadWithCache(store, options, "ChartClassificationLevel");
            },

            /**
             * Listener for the load event.
             * @param {Store} store
             * @param {Ext.data.Record[]} records
             * @param {Object} options
             */
            onLoad       : function(store, records, options) {
                this.loadWithCache(store, records, options, "ChartClassificationLevel");
            }
        });

Ext.reg("vizStoreChartClassificationLevel", Viz.store.ChartClassificationLevel);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ChartFilterSpatial
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartFilterSpatial = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.FilterSpatial_GetStoreByKeyChart,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                    	KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ChartFilterSpatial.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreChartFilterSpatial", Viz.store.ChartFilterSpatial);

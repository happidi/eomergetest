﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.Color
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Color = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.Color_GetStore,
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.Color.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreColor", Viz.store.Color);

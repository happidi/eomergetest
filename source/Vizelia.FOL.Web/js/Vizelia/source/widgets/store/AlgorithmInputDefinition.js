﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.Algorithm
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlgorithmInputDefinition = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.AlgorithmInputDefinition_GetStoreByKeyAlgorithm,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyAlgorithm : config.KeyAlgorithm,
                        location     : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Name',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.AlgorithmInputDefinition.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreAlgorithmInputDefinition", Viz.store.AlgorithmInputDefinition);
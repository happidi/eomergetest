﻿Ext.namespace('Viz.store');

/**
* @class Viz.store.StruxureWareDataAcquisitionEndpoint
* @extends Viz.data.WCFJsonSimpleStore
*/
Viz.store.StruxureWareDataAcquisitionEndpoint = Ext.extend(Viz.data.WCFJsonSimpleStore, {
    /**
    * Ctor.
    * @param {Object} config
    */
    constructor: function (config) {
        config = config || {};
        var defaultConfig = {
            serviceHandler: Viz.Services.EnergyWCF.StruxureWareDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer,
            serviceHandlerCrud: Viz.Services.EnergyWCF.StruxureWareDataAcquisitionEndpoint_SaveStore,
            serviceParams: {
                KeyDataAcquisitionContainer: config.KeyDataAcquisitionContainer,
                location: Vizelia.FOL.BusinessEntities.PagingLocation.Database
            },
            sortInfo: {
                field: 'Name',
                direction: 'ASC'
            },
            baseParams: {
                // used by the combobox to filter results
                filter: 'Name'
            }
        };
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.store.StruxureWareDataAcquisitionEndpoint.superclass.constructor.call(this, config);
    }
});

Ext.reg("vizStoreStruxureWareDataAcquisitionEndpoint", Viz.store.StruxureWareDataAcquisitionEndpoint);
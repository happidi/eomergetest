﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.AlarmDefinitionMeterAll
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmDefinitionMeterAll = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.AlarmDefinition_GetMetersStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyAlarmDefinition : config.KeyAlarmDefinition
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.AlarmDefinitionMeterAll.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreAlarmDefinitionMeterAll", Viz.store.AlarmDefinitionMeterAll);
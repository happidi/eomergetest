﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.LoginHistory
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.LoginHistory = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.LoginHistory_GetStore,
                    serviceHandlerCrud : Viz.Services.AuthenticationWCF.LoginHistory_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'LoginDate',
                        direction : 'DESC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'UserName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.LoginHistory.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreLoginHistory", Viz.store.LoginHistory);
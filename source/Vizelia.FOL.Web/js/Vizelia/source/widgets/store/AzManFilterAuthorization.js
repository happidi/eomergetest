﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AzManFilterAuthorization
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AzManFilterAuthorization = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.AuthenticationWCF.AzManFilter_Authorization_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        filterId : config.filterId
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AzManFilterAuthorization.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAzManFilterAuthorization", Viz.store.AzManFilterAuthorization);
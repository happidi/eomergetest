﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.PsetAttributeHistorical
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.PsetAttributeHistorical = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.PsetAttributeHistorical_GetStoreByKeyObject,
                    serviceHandlerCrud : Viz.Services.CoreWCF.PsetAttributeHistorical_SaveStore,
                    sortInfo           : {
                        field     : 'AttributeAcquisitionDateTime',
                        direction : 'DESC'
                    },
                    serviceParams      : {
                        KeyObject              : config.KeyObject,
                        KeyPropertySingleValue : config.KeyPropertySingleValue,
                        location               : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Order'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.PsetAttributeHistorical.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStorePsetAttributeHistorical", Viz.store.PsetAttributeHistorical);
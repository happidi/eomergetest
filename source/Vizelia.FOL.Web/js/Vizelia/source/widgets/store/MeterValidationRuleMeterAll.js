﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.MeterValidationRuleMeterAll
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MeterValidationRuleMeterAll = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.MeterValidationRule_GetMetersStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyMeterValidationRule : config.KeyMeterValidationRule
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.MeterValidationRuleMeterAll.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreMeterValidationRuleMeterAll", Viz.store.MeterValidationRuleMeterAll);
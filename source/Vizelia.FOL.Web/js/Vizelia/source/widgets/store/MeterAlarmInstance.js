﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.MeterAlarmInstance
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MeterAlarmInstance = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.AlarmInstance_GetStoreByKeyMeter,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.AlarmInstance_SaveStore,
                    serviceParams      : {
                        location      : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        KeyMeter : config.KeyMeter || ' '
                    },
                    sortInfo           : {
                        field     : 'Title',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MeterAlarmInstance.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreMeterAlarmInstance", Viz.store.MeterAlarmInstance);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.VirtualFile
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.VirtualFile = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.VirtualFile_GetStore,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.VirtualFile_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Path'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.VirtualFile.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreVirtualFile", Viz.store.VirtualFile);
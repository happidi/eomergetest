﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Link
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Link = Ext.extend(Viz.data.WCFJsonSimpleStore, {
    /**
    * Ctor.
    * @param {Object} config
    */
    constructor: function (config) {
        config = config || {};
        var defaultConfig = {
            serviceHandler: Viz.Services.CoreWCF.Link_GetStore,
            serviceHandlerCrud: Viz.Services.CoreWCF.Link_SaveStore,
            serviceParams: {
                location: Vizelia.FOL.BusinessEntities.PagingLocation.Database
            },
            baseParams: {
                // used by the combobox to filter results
                filter: 'Subject'
            }

        };



        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        this.currentId = config.currentId;

        Viz.store.Link.superclass.constructor.call(this, config);
        this.on('load', this.onLoad, this);

    },

    /**
    * Listener for the load event.
    * @param {Store} store
    * @param {Ext.data.Record[]} records
    * @param {Object} options
    */
    onLoad: function (store, records, options) {
        if (this.currentId)
            this.remove(this.getById(this.currentId));
        return true;
    }


});

Ext.reg("vizStoreLink", Viz.store.Link);
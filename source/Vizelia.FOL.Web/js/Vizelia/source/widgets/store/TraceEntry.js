﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.TraceEntry
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.TraceEntry = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.TraceEntry_GetStore,
                    serviceHandlerCrud : Viz.Services.CoreWCF.TraceEntry_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Timestamp',
                        direction : 'DESC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter  : 'Message',
                        filters : this.storeFilters || config.storeFilters
                    }
                };
               
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.TraceEntry.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreTraceEntry", Viz.store.TraceEntry);
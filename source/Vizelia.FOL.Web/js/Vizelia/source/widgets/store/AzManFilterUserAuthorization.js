﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AzManFilterUserAuthorization
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AzManFilterUserAuthorization = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.AuthenticationWCF.AzManFilter_UserAuthorization_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        filterId : config.filterId
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AzManFilterUserAuthorization.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAzManFilterUserAuthorization", Viz.store.AzManFilterUserAuthorization);
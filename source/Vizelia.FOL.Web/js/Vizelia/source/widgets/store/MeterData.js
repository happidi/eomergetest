﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.MeterData
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MeterData = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.MeterData_GetStoreFromMeter,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.MeterData_SaveStore,
                    serviceParams      : {
                        location       : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        KeyMeter       : config.KeyMeter ? config.KeyMeter : ' ',
                        datetimeFormat : Viz.getLocalizedDateFormat(true)
                    },
                    sortInfo           : {
                        field     : 'AcquisitionDateTime',
                        direction : 'DESC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'KeyMeterData'
                    },
                    deleteAllFields    : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.MeterData.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreMeterData", Viz.store.MeterData);
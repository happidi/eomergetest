﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.ChartMeter
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartMeter = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Meter_GetStoreByKeyChart,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.ChartMeter.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreChartMeter", Viz.store.ChartMeter);
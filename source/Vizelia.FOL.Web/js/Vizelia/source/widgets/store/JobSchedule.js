﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.JobSchedule
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.JobSchedule = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.Schedule_GetStoreByKeyJob,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyJob   : config.KeyJob,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Memory
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.JobSchedule.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreJobSchedule", Viz.store.JobSchedule);
﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.PortalTemplateFilterSpatial
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.PortalTemplateFilterSpatial = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.FilterSpatial_GetStoreByKeyPortalTemplate,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                    	KeyPortalTemplate : config.KeyPortalTemplate,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.PortalTemplateFilterSpatial.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStorePortalTemplateFilterSpatial", Viz.store.PortalTemplateFilterSpatial);

﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AzManFilterDynamicDisplay
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AzManFilterDynamicDisplay = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.AzManFilter_Filter_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        filterId : config.filterId,
                        typeName : 'Vizelia.FOL.BusinessEntities.DynamicDisplay'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AzManFilterDynamicDisplay.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAzManFilterDynamicDisplay", Viz.store.AzManFilterDynamicDisplay);
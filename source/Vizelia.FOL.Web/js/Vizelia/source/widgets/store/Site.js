﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Site
 * @extends Viz.data.WCFJsonSimpleStore
 */ 
Viz.store.Site = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.Site_GetStore,
                    serviceHandlerCrud : Viz.Services.CoreWCF.Site_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Site.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreSite", Viz.store.Site);



﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.AlarmTableAlarmDefinitionAll
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmTableAlarmDefinitionAll = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.AlarmTable_GetAlarmDefinitionsStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyAlarmTable : config.KeyAlarmTable
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.AlarmTableAlarmDefinitionAll.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreAlarmTableAlarmDefinitionAll", Viz.store.AlarmTableAlarmDefinitionAll);
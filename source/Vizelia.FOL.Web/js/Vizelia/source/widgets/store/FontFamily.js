﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.FontFamily
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.FontFamily = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.FontFamily_GetStore,
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.FontFamily.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreFontFamily", Viz.store.FontFamily);

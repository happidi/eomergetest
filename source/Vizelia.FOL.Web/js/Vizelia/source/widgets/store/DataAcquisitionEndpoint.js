﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.DataAcquisitionEndpoint
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.DataAcquisitionEndpoint = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.DataAcquisitionEndpoint_GetStore,
                    serviceParams  : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Memory
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'FullName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.DataAcquisitionEndpoint.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreDataAcquisitionEndpoint", Viz.store.DataAcquisitionEndpoint);
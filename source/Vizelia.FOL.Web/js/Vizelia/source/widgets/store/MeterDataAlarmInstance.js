﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.MeterDataAlarmInstance
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MeterDataAlarmInstance = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.AlarmInstance_GetStoreByKeyMeterData,
                    serviceHandlerCrud : Viz.Services.EnergyWCF.AlarmInstance_SaveStore,
                    serviceParams      : {
                        location      : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        KeyMeterData : config.KeyMeterData || ' '
                    },
                    sortInfo           : {
                        field     : 'Title',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MeterDataAlarmInstance.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreMeterDataAlarmInstance", Viz.store.MeterDataAlarmInstance);
﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.Algorithm
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlgorithmInputValue = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler: Viz.Services.EnergyWCF.AlgorithmInputValue_GetStoreByKeyChart,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyAlgorithm: config.KeyAlgorithm,
                        KeyChart  : config.KeyChart,
                        location  : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Name',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.AlgorithmInputValue.superclass.constructor.call(this, config);
            }
        });
        Ext.reg("vizStoreAlgorithmInputValue", Viz.store.AlgorithmInputValue);
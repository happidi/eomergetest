﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.Algorithm
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Algorithm = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler: Viz.Services.EnergyWCF.Algorithm_GetStore,
                    serviceHandlerCrud: Viz.Services.EnergyWCF.Algorithm_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Name',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.Algorithm.superclass.constructor.call(this, config);
            }
        });
        Ext.reg("vizStoreAlgorithm", Viz.store.Algorithm);
﻿Ext.namespace('Viz.store');

/**
* @class Viz.store.StepPlaylist
* @extends Viz.data.WCFJsonSimpleStore
*/
Viz.store.StepPlaylist = Ext.extend(Viz.data.WCFJsonSimpleStore, {
    /**
    * Ctor.
    * @param {Object} config
    */
    constructor: function (config) {
        config = config || {};
        var defaultConfig = {
            serviceHandler: Viz.Services.EnergyWCF.Playlist_GetStoreByJobStep,
            serviceHandlerCrud: null,
            serviceParams: {
                location: Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                keyJob: config.keyJob,
                keyStep: config.keyStep
            },
            baseParams: {
                // used by the combobox to filter results
                filter: 'Name'
            }
        };

        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.store.StepPlaylist.superclass.constructor.call(this, config);

    }
});

Ext.reg("vizStoreStepPlaylist", Viz.store.StepPlaylist);
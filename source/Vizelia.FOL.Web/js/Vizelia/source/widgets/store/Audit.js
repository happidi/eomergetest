﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.Audit
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.Audit = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : function(options) {
                        var auditOptions = Ext.applyIf({
                                    success : function(response) {

                                        Ext.each(response.metaData.fields, function(field) {
                                                    if (field.name != 'ChangeTime' && field.name != 'Entity')
                                                        field.type = 'string';
                                                })

                                        Ext.each(response.records, function(row) {
                                                    var entity = {};
                                                    Ext.each(row.Entity, function(field) {
                                                                entity[field.Id] = field.Value;
                                                            })
                                                    row.Entity = entity;
                                                })
                                        options.success.apply(this, arguments);
                                    }
                                }, options);
                        Viz.Services.CoreWCF.Audit_GetStore(auditOptions);
                    },
                    serviceParams  : {
                        entityName : config.entityName,
                        location   : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    groupField     : 'ChangeTime'
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.Audit.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAudit", Viz.store.Audit);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AzManFilterPortalWindow
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AzManFilterPortalWindow = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.AzManFilter_Filter_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        filterId : config.filterId,
                        typeName : 'Vizelia.FOL.BusinessEntities.PortalWindow'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AzManFilterPortalWindow.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAzManFilterPortalWindow", Viz.store.AzManFilterPortalWindow);
﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.PaletteColor
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.PaletteColor = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.PaletteColor_GetStoreByKeyPalette,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyPalette : config.KeyPalette,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Order'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.PaletteColor.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStorePaletteColor", Viz.store.PaletteColor);
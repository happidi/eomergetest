﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.AuditableEntity
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AuditableEntity = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.AuditableEntity_GetStore,
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Value'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.AuditableEntity.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreAuditableEntity", Viz.store.AuditableEntity);

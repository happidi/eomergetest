﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.ChartPsetAttributeHistorical
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartPsetAttributeHistorical = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.ChartPsetAttributeHistorical_GetStoreByKeyChart,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'AttributeName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.ChartPsetAttributeHistorical.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreChartPsetAttributeHistorical", Viz.store.ChartPsetAttributeHistorical);
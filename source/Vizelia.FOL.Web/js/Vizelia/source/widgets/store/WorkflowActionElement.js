﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.WorkflowActionElement
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.WorkflowActionElement = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.ServiceDeskWCF.Workflow_GetStateMachineEventsTransitionsFromClassificationItems,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        keyIfcClassificationItems : config.keyIfcClassificationItems || [],
                        location                  : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'AssemblyShortName'
                    },
                    sortInfo           : {
                        field     : 'AssemblyShortName',
                        direction : 'ASC'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.WorkflowActionElement.superclass.constructor.call(this, config);

            }
        });

Ext.reg('vizStoreWorkflowActionElement', Viz.store.WorkflowActionElement);
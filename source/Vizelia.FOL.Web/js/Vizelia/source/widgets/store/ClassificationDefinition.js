﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ClassificationDefinition
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ClassificationDefinition = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.CoreWCF.ClassificationDefinition_GetStore,
                    serviceHandlerCrud : Viz.Services.CoreWCF.ClassificationDefinition_SaveStore,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Category'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ClassificationDefinition.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreClassificationDefinition", Viz.store.ClassificationDefinition);

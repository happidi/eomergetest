﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.ChartFilterMeterClassification
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartFilterMeterClassification = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.FilterMeterClassification_GetStoreByKeyChart,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                    	KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.ChartFilterMeterClassification.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreChartFilterMeterClassification", Viz.store.ChartFilterMeterClassification);

﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.ChartHistoricalAnalysis
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.ChartHistoricalAnalysis = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.ChartHistoricalAnalysis_GetStoreByKeyChart,
                    serviceHandlerCrud : null,// Viz.Services.EnergyWCF.ChartHistoricalAnalysis_SaveStore,
                    deleteAllFields    : true,
                    serviceParams      : {
                        KeyChart : config.KeyChart,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.ChartHistoricalAnalysis.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreChartHistoricalAnalysis", Viz.store.ChartHistoricalAnalysis);
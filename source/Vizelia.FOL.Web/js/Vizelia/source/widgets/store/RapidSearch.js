﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.RapidSearch
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.RapidSearch = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.Spatial_GetStoreByKeyword, 
                    serviceHandlerCrud : null,
                    serviceParams      : {
                    	keyword : config.keyword, 
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                         filter : 'LocalId Name LongPath ShortPath'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.RapidSearch.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreRapidSearch", Viz.store.RapidSearch);
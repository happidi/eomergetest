﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.User
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.User = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.User_GetStore,
                    serviceHandlerCrud : Viz.Services.AuthenticationWCF.User_SaveStore,
                    serviceParams      : { fields: ['UserPreferences'] },
                    sortInfo           : {
                        field     : 'UserName',
                        direction : 'ASC'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.User.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreUser", Viz.store.User);

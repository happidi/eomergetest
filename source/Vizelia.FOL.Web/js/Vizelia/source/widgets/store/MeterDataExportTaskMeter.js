﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.MeterDataExportTaskMeter
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MeterDataExportTaskMeter = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.Meter_GetStoreByKeyMeterDataExportTask,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        KeyMeterDataExportTask : config.KeyMeterDataExportTask,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.MeterDataExportTaskMeter.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizStoreMeterDataExportTaskMeter", Viz.store.MeterDataExportTaskMeter);
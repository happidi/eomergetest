﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AlarmDefinition
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MeterValidationRule = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.EnergyWCF.MeterValidationRule_GetStore,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    sortInfo           : {
                        field     : 'Title',
                        direction : 'ASC'
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Title'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MeterValidationRule.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreMeterValidationRule", Viz.store.MeterValidationRule);
﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.MethodDefinition
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.MethodDefinition = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.CoreWCF.MethodDefinition_GetStoreByTypeName,
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'MethodName'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.MethodDefinition.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreMethodDefinition", Viz.store.MethodDefinition);
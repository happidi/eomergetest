﻿Ext.namespace('Viz.store');
/**
 * @class Viz.store.AzManFilterByLocation
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AzManFilterByLocation = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler     : Viz.Services.AuthenticationWCF.AzManFilter_GetStoreByLocation,
                    serviceParams      : {
                        location    : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                        keyLocation : config.KeyLocation ? config.KeyLocation : ' ',
                    },
                    baseParams         : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    },
                    deleteAllFields: true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.store.AzManFilterByLocation.superclass.constructor.call(this, config);
            }
        });

Ext.reg("vizStoreAzManFilterByLocation", Viz.store.AzManFilterByLocation);
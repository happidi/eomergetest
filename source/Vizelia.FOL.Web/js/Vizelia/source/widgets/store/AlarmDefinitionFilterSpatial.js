﻿Ext.namespace('Viz.store');

/**
 * @class Viz.store.AlarmDefinitionFilterSpatial
 * @extends Viz.data.WCFJsonSimpleStore
 */
Viz.store.AlarmDefinitionFilterSpatial = Ext.extend(Viz.data.WCFJsonSimpleStore, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler : Viz.Services.EnergyWCF.FilterSpatial_GetStoreByKeyAlarmDefinition,
                    serviceHandlerCrud : null,
                    serviceParams      : {
                    	KeyAlarmDefinition : config.KeyAlarmDefinition,
                        location : Vizelia.FOL.BusinessEntities.PagingLocation.Database
                    },
                    baseParams     : {
                        // used by the combobox to filter results
                        filter : 'Name'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.store.AlarmDefinitionFilterSpatial.superclass.constructor.call(this, config);

            }
        });

Ext.reg("vizStoreAlarmDefinitionFilterSpatial", Viz.store.AlarmDefinitionFilterSpatial);

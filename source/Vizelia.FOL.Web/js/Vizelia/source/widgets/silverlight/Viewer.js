Ext.namespace('Viz.Silverlight');
/**
 * @class Viz.Silverlight.Viewer
 * @extends Viz.Silverlight.SilverlightComponent
 * @constructor
 * @xtype vizSilverLightViewer
 */

Viz.Silverlight.Viewer = Ext.extend(Viz.Silverlight.SilverlightComponent, {
    refreshBuffer          : 100,

    version                : '1',

    initComponent          : function() {
        Viz.Silverlight.Viewer.superclass.initComponent.call(this);
        if (!this.source) {
            this.source = Viz.Silverlight.Viewer.SOURCE;
        }

    },

    /**
     * Sets a drawing to be displayed
     * @param name {drawingSource} Name of the Drawing to Load.
     */
    setDrawingSource       : function(drawingSource) {
        this.silverlight.childNodes[0].Content.MainPage.SetSource(drawingSource);
    },

    /**
     * Display space's assignments
     * @param name {spaces} A silverlight managed List<Space>
     */
    displaySpaceAssignment : function(spaces) {
        this.silverlight.childNodes[0].Content.MainPage.displaySpaceAssignment(spaces);
    },

    /**
     * Hide all space's assignments
     */
    resetSpaceAssignment   : function() {
        this.silverlight.childNodes[0].Content.MainPage.resetSpaceAssignment();
    },

    /**
     * Hide a specific classification based on it's key
     * @param name {KeyClassificationItem} The Key of the Classification Item
     */
    toggleSpaceAssignment  : function(KeyClassificationItem) {
        this.silverlight.childNodes[0].Content.MainPage.toggleSpaceAssignment(KeyClassificationItem);
    }

        /*
         * getLayers: function () { return this.silverlight.childNodes[0].Content.MainPage.GetLayers(); }, updateLayers: function (layers) { this.silverlight.childNodes[0].Content.MainPage.UpdateLayers(layers); }, showLegend: function () { var layers = this.getLayers(); this.updateLayers(layers); var legendWindow = Ext.getCmp(this.id + '-legend'); if (!legendWindow) { var items = []; for (i = 0; i < layers.length; i++) { var layer = layers[i]; items.push({ xtype: 'fieldset', title: '<img src="icons/iconsifc/' + layer.Name + '.gif" /> ' + layer.Name, collapsible: true, collapsed: true, defaults: { anchor: '100%' }, items: [{ xtype: 'checkbox', fieldLabel: 'Visible', checked: layer.Visible }, { fieldLabel: 'FillColor', xtype: 'colorfield', showHexValue: true, allowBlank: true, defaultColor:
         * [layer.FillColor.R, layer.FillColor.G, layer.FillColor.B] }, { fieldLabel: 'StrokeColor', xtype: 'colorfield', showHexValue: true, allowBlank: true, defaultColor: [layer.StrokeColor.R, layer.StrokeColor.G, layer.StrokeColor.B] }] }); } legendWindow = new Ext.Window({ id: this.id + '-legend', width: 280, height: 380, layout: 'fit', cls: 'x-plain', buttonAlign: 'center', closeAction: 'hide', items: { xtype: 'form', layout: 'form', autoScroll: true, border: false, items: items }, buttons: [{ text: 'Apply', handler: function (b) { } }] }); } legendWindow.show(); }, applySpaceList: function (store) { this.silverlight.childNodes[0].Content.MainPage.ApplySpaceList(store); }, test: function (building) { this.silverlight.childNodes[0].Content.MainPage.ApplyStoreTest(); }, test2: function () {
         * return this.silverlight.childNodes[0].Content.MainPage.Test2(); }
         */

    });
Ext.reg('vizSilverlightViewer', Viz.Silverlight.Viewer);

Viz.Silverlight.Viewer.SOURCE = 'clientBin/Vizelia.FOL.Silverlight.Viewer.xap';

Ext.namespace("Viz.Silverlight");
/**
 * @class Viz.Silverlight.SilverlightComponent
 * @extends Ext.BoxComponent
 * @xtype vizSilverlightComponent
 */

Viz.Silverlight.SilverlightComponent = Ext.extend(Ext.BoxComponent, {

            /**
             * @cfg {String} component version to force refresh of the xap in the cache of the browser
             */
            version             : '1',

            /**
             * @cfg {String} silverlightVersion Indicates the version the Silverlight content was published for.
             */
            silverlightVersion  : '4.0.50401',

            /**
             * @cfg {String} backgroundColor The background color of the chart. Defaults to <tt>'#ffffff'</tt>.
             */
            backgroundColor     : '#ffffff',

            /**
             * @cfg {String} isWindowless The isWindowless of the Silverlight object. This can be used to control layering. Defaults to <tt>true</tt>.
             */
            isWindowless        : "true",

            /**
             * @cfg {String} enableHtmlAccess The enableHtmlAccess of the Silverlight object. This can be used to control layering. Defaults to <tt>true</tt>.
             */
            enableHtmlAccess    : "true",
            /**
             * @cfg {Object} silverlightVars A set of key value pairs to be passed to the Silverlight object as Silverlight variables. Defaults to <tt>undefined</tt>.
             */

            silverlightParams   : undefined,

            silverlightId       : undefined,
            silverlightWidth    : '100%',
            silverlightHeight   : '100%',

            /**
             * @cfg {Boolean} expressInstall True to prompt the user to install Silverlight if not installed. Note that this uses Ext.SilverlightComponent.EXPRESS_INSTALL_URL, which should be set to the local resource. Defaults to <tt>false</tt>.
             */
            expressInstall      : true,

            /*
             * Override of the hide function to prevent the compoment from re redering every time
             */
            hide                : function() {
                this.pHeight = this.getHeight();
                this.pWidth = this.getWidth();
                this.setHeight(1);
                this.setWidth(1);
                return false;
            },
            /*
             * Override of the show function to prevent the compoment from re redering every time
             */
            show                : function() {
                if (Ext.isDefined(this.pHeight))
                    this.setHeight(this.pHeight);
                if (Ext.isDefined(this.pWidth))
                    this.setWidth(this.pWidth);
                return false;
            },

            /**
             * Create a managed object from javascript. the class needs to be registered in silverlight using for example : HtmlPage.RegisterCreateableType("Building", typeof(CoreWCF.Building));
             * @param name {scriptAlias} Name of the Class to instanciate
             * @param name {initializationData} Initialization data of the Class
             */
            createObject        : function(scriptAlias, initializationData) {
                if (Ext.isEmpty(initializationData))
                    return this.silverlight.childNodes[0].Content.services.createObject(scriptAlias);
                else
                    return this.silverlight.childNodes[0].Content.services.createObject(scriptAlias, initializationData);

            },

            /**
             * Create a managed object from javascript if the class has been exposed by a ScriptableMember function
             * @param name {scriptAlias} Name of the Class to instanciate
             * @param name {initializationData} Initialization data of the Class
             */
            createManagedObject : function(scriptAlias, initializationData) {
                if (Ext.isEmpty(initializationData))
                    return this.silverlight.childNodes[0].Content.MainPage.createManagedObject(scriptAlias);
                else
                    return this.silverlight.childNodes[0].Content.MainPage.createManagedObject(scriptAlias, initializationData);
            },

            /**
             * onRender Handler to create the silverlight object and pass the initParams
             */
            onRender            : function() {
                Viz.Silverlight.SilverlightComponent.superclass.onRender.apply(this, arguments);
                var obj = Silverlight.createObjectEx({
                            source        : this.source + '?' + this.version,
                            parentElement : document.getElementById(this.getId()),
                            id            : this.getSilverlightId(),
                            properties    : {
                                width             : this.silverlightWidth,
                                height            : this.silverlightHeight,
                                enableHtmlAccess  : this.enableHtmlAccess,
                                minRuntimeVersion : this.silverlightVersion,
                                background        : this.backgroundColor,
                                isWindowless      : this.isWindowless
                            },
                            events        : {},
                            context       : this,
                            initParams    : this.silverlightParams + ",id=" + this.getId()
                        });

                this.silverlight = Ext.getDom(this.id);
                this.el = Ext.get(this.silverlight);
            },

            /**
             * Returns the silverlight component id.
             * @return {String}
             */
            getSilverlightId    : function() {
                return this.silverlightId || (this.silverlightId = "extSilverlight" + (++Ext.Component.AUTO_ID));
            },

            /**
             * Returns the component id.
             * @return {String}
             */
            getId               : function() {
                return this.id || (this.id = "extSilverlightcmp" + (++Ext.Component.AUTO_ID));
            }

        });

Ext.reg('vizSilverlightComponent', Viz.Silverlight.SilverlightComponent);
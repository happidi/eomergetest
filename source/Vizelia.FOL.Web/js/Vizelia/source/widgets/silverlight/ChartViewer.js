Ext.namespace('Viz.Silverlight');
/**
 * @class Viz.Silverlight.Viewer
 * @extends Viz.Silverlight.SilverlightComponent
 * @constructor
 * @xtype vizSilverLightViewer
 */

Viz.Silverlight.ChartViewer = Ext.extend(Viz.Silverlight.SilverlightComponent, {

            version       : '1.0.1.7',

            initComponent : function() {
                Viz.Silverlight.ChartViewer.superclass.initComponent.call(this);
                if (!this.source) {
                    this.source = Viz.Silverlight.ChartViewer.SOURCE;
                }

            },

            /**
             * Fetch newest data and refresh the graph
             */
            reloadChart       : function() {
                if (this.silverlight && this.silverlight.childNodes && this.silverlight.childNodes.length > 0 && this.silverlight.childNodes[0].Content) {
                    this.silverlight.childNodes[0].Content.MainPage.ReloadChart();
                }
            }

        });
Ext.reg('vizSilverlightChartViewer', Viz.Silverlight.ChartViewer);

Viz.Silverlight.ChartViewer.SOURCE = 'clientBin/Vizelia.FOL.Silverlight.ChartViewer.xap';

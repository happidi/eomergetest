﻿Ext.namespace('Viz');

/**
 * @class Viz.TabPanel
 * @extends TabPanel Summary.
 */
Viz.TabPanel = Ext.extend(Ext.TabPanel, {

            constructor : function(config) {

                config = config || {};
                var defaultConfig = {
                    xtype          : 'tabpanel',
                    activeTab      : 0,
                    deferredRender : true,
                    bodyStyle      : 'padding-top:1px;background-color: transparent;',
                    plain          : true,
                    border         : false,
                    defaults       : {
                        layout   : 'fit',
                        hideMode : 'offsets'
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.TabPanel.superclass.constructor.call(this, config);

            }

        });

Ext.reg('vizTabPanel', Viz.TabPanel);

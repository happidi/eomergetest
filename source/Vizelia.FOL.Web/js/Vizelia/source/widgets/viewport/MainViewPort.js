﻿Ext.namespace('Viz.MainViewPort');
/**
 * @class Viz.MainViewPort
 * <p>
 * The main view port of the application
 * </p>
 * @extends Ext.Viewport Summary.
 */
Viz.MainViewPort = Ext.extend(Ext.Viewport, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                             : function(config) {
                config = config || {};
                this._mainTabId = Ext.id();
                this._desktopIconsId = Ext.id();
                this._treeSpatial = Ext.id();
                this._portalToolbarId = Ext.id();
                this._linkToolbarId = Ext.id();
                this._northRegionId = Ext.id();
                this._northSubRegionId = Ext.id();
                this._westRegionId = Ext.id();
                this._northRibbonId = Ext.id();
                this._desktopBackgroundId = Ext.id();
                this._desktopLogoId = Ext.id();
                this._chartPreLoadProgressBarId = Ext.id();

                this._mainMarginLeft = '15';
                this._mainMarginRight = '15';
                this._dashboardName = 'Energy - Dashboard';
                this._regionWestName = 'Left Tree';

                this._unpinButton = Ext.id();
                var authPin = $authorized('@@@@ Portal Icons - Full Control');
                if (authPin)
                    var unpinButton = new Ext.Button({
                                id           : this._unpinButton,
                                iconCls      : 'viz-icon-small-unpin',
                                hidden       : !$authorized('@@@@ Portal Icons - Full Control'),
                                handler      : this.unpinDesktopIcons,
                                enableToggle : true,
                                scope        : this
                            });
                else
                    var unpinButton = {};

                var settingsButton = {
                    text : $lang('msg_settings'),
                    // iconCls : 'viz-icon-small-configuration',
                    menu : new Ext.menu.Menu({
                                items : [{
                                            text    : $lang('msg_application_about') + ' ' + $lang('msg_application_title'),
                                            iconCls : 'viz-icon-small-about',
                                            handler : function(item, event) {
                                                Viz.App.showAboutBox(event);
                                            }
                                        }, {
                                            text    : $lang('msg_changepassword'),
                                            iconCls : 'viz-icon-small-changepassword',
                                            handler : function(item, event) {
                                                new Viz.window.ChangePassword({}).show();
                                            },
                                            hidden  : !$authorized('@@@@ Change password')
                                        }, {
                                            text    : $lang('msg_user_info'),
                                            iconCls : 'viz-icon-small-user',
                                            handler: function() {

                                                Viz.openFormCrud({
                                                    common: {
                                                        width: 550,
                                                        height: 390,
                                                        xtype: 'vizFormUserInfo',
                                                        title: $lang("msg_user_info"),
                                                        iconCls: 'viz-icon-small-user',
                                                        titleEntity: 'title',
                                                        serviceParamsEntity: [
                                                            {
                                                                name: 'key',
                                                                value: 'KeyUser'
                                                            }
                                                        ]
                                                    }
                                                }, 'update', {
                                                    KeyUser: Viz.App.Global.User.KeyUser,
                                                    title: Viz.App.Global.User.UserName
                                                });
                                            }
                                }, {
                                            text    : $lang('msg_logo_change'),
                                            iconCls : 'viz-icon-small-logo',
                                            hidden  : !$authorized('@@@@ Logo - Full Control'),
                                            handler : function() {
                                                // Get the logo entity
                                                var logoConfig = {
                                                    common      : {
                                                        height   : 120,
                                                        width    : 500,
                                                        xtype    : 'vizFormLogo',
                                                        readonly : !$authorized('@@@@ Logo - Full Control')
                                                    },
                                                    create      : {
                                                        title   : $lang("msg_logo_change"),
                                                        iconCls : 'viz-icon-small-add'
                                                    },
                                                    update      : {
                                                        title               : $lang("msg_logo_change"),
                                                        iconCls             : 'viz-icon-small-update',
                                                        titleEntity         : 'Application',
                                                        serviceParamsEntity : [{
                                                                    name  : 'Key',
                                                                    value : 'KeyLogo'
                                                                }]
                                                    },
                                                    updatebatch : {
                                                        title                     : $lang("msg_updatebatch"),
                                                        iconCls                   : 'viz-icon-small-update',
                                                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.DynamicDisplayImage_FormUpdateBatch
                                                    }
                                                };

                                                // Get the logo entity
                                                Viz.Services.CoreWCF.Logo_GetItem({
                                                            Key     : 1,
                                                            success : function(entity) {
                                                                if (entity === null) {
                                                                    Viz.openFormCrud(logoConfig, 'create');
                                                                }
                                                                else {
                                                                    entity.Name = "Logo";
                                                                    Viz.openFormCrud(logoConfig, 'update', entity);
                                                                }
                                                            }
                                                        });
                                            }
                                        }, {
                                            text    : $lang('msg_userprofilepreference_clear'),
                                            iconCls : 'viz-icon-small-clean',
                                            handler : function() {
                                                Ext.MessageBox.confirm($lang('msg_application_title'), $lang('msg_userprofilepreference_clear_confirm'), function(button) {
                                                            if (button == 'yes') {
                                                                Viz.Services.CoreWCF.UserProfilePreference_DeleteAllData({
                                                                            success : function() {
                                                                                Ext.state.Manager.getProvider().state = {};
                                                                            }
                                                                        });
                                                            }
                                                        }, this);
                                            }
                                        }]
                            })
                };

                var modes = Viz.Configuration.EnumRecords['Vizelia.FOL.BusinessEntities.MainViewPortMode,Vizelia.FOL.Common'];
                var modeButton = {
                    text : $lang('msg_enum_mainviewportmode'),
                    // iconCls : 'viz-icon-small-mainviewportmode-minimal',
                    menu : {
                        items : []
                    }
                };
                var currentMode = Ext.state.Manager.get('MainViewPortMode');
                Ext.each(modes, function(mode) {
                            modeButton.menu.items.push({
                                        text           : $img(mode.data.IconCls) + mode.data.Label,
                                        group          : 'mainviewportmode',
                                        checked        : currentMode == mode.data.Value,
                                        mode           : mode.data.Value,
                                        privateHandler : this.changeViewPortMode.createDelegate(this, [mode.data.Value]),
                                        // override to force the checkHandler call even if the menu is already checked.
                                        handleClick    : function(e) {
                                            if (!this.disabled) {
                                                this.checked = false;
                                                this.setChecked(true, true); // the second true is to suppress the event
                                            }
                                            Ext.menu.CheckItem.superclass.handleClick.apply(this, arguments);
                                            this.privateHandler();
                                        }
                                    });
                        }, this);
                if (Viz.util.Fullscreen.isSupported()) {
                    modeButton.menu.items.push('-');
                    modeButton.menu.items.push({
                                text    : $lang('msg_fullscreen_toggle'),
                                handler : function() {
                                    Viz.util.Fullscreen.toggle();
                                }
                            });
                }
                var logoutButton = {
                    // id : 'close',
                    // iconCls : 'viz-icon-small-logout',
                    text    : $lang("msg_logout"),
                    handler : function() {
                        this.onLogOut();
                    },
                    scope   : this
                };
                var helpButton = {
                    // iconCls : 'viz-icon-small-about',
                    text    : $lang("msg_help"),
                    hidden  : !$authorized('@@@@ Help'),
                    handler : function() {
                        if (Viz.ApplicationSettings.HelpUrl != "")
                            window.open(Viz.ApplicationSettings.HelpUrl);
                    },
                    scope   : this
                };

                var userFullName = null;
                if (Viz.App.Global.User.FirstName != null && Viz.App.Global.User.LastName != null) {
                    userFullName = Viz.App.Global.User.FirstName + " " + Viz.App.Global.User.LastName;
                }
                else if (Viz.App.Global.User.FirstName != null) {
                    userFullName = Viz.App.Global.User.FirstName;
                }
                else {
                    userFullName = Viz.App.Global.User.LastName;
                }
                
                var appTitleButtons = [
                        // application title
                        '<span class="appTitle">' + $lang('msg_application_title') + '</span>',
                        // separator
                        // '->',
                        // username
                        String.format('<span class="appSubTitle" ext:qalign="bl" ext:qtip="{1}">{0}</span>', userFullName || Viz.App.Global.User.UserName, Viz.App.Global.User.ApplicationName + '\\' + Viz.App.Global.User.UserName),
                        // separator
                        '-',
                        // logout button
                        logoutButton,
                        // separator
                        '-',
                        // display mode button
                        modeButton,
                        // separator
                        '-',
                        // settings button
                        settingsButton];
                if ($authorized('@@@@ Help')) {
                    appTitleButtons.push('-');
                    appTitleButtons.push(helpButton);
                }
                // separator
                // '-',
                // help button
                // helpButton];
                if (authPin)
                    appTitleButtons = appTitleButtons.concat('-', unpinButton);

                var defaultConfig = {
                    id     : 'viewport',
                    layout : 'border',

                    // items : [this.buildRegionNorth(config), this.buildRegionSouth(config), this.buildRegionCenter(config), this.buildRegionWest(config)]
                    style  : 'background-color:rgb(195,195,195)',
                    items  : [{
                                region    : 'north',
                                bodyStyle : 'border-bottom:none;background-color:transparent;',
                                border    : false,
                                items     : new Ext.ux.StatusBar({
                                            border  : false,
                                            cls     : 'appPanel',
                                            style   : 'border-top: 0px #D0D0D0 solid; border-bottom:0',
                                            height  : 62,
                                            margins : '0 0 0 0',
                                            id      : 'statusbartop',
                                            items   : appTitleButtons
                                        }),
                                height    : 56
                            }, new Ext.Container({
                                        border  : true,
                                        layout  : 'border',
                                        region  : 'center',
                                        frame   : true,
                                        shadow  : true,
                                        style   : 'border: 1px darkgray solid',
                                        margins : '15 ' + this._mainMarginRight + ' 2 ' + this._mainMarginLeft,
                                        items   : [this.buildRegionNorth(config), this.buildRegionCenter(config), this.buildRegionWest(config), {
                                                    region    : 'south',
                                                    height    : 0,
                                                    bodyStyle : 'background-color:gainsboro'
                                                }]
                                    }), this.buildRegionSouth(config)]

                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.MainViewPort.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("PortalWindowChange", this.updateApplicationToolbarPortalWindows, this);
                Viz.util.MessageBusMgr.subscribe("LinkChange", this.updateApplicationToolbarLinks, this);

                this._checkAlarmInstanceNotificationsTask = {
                    run      : this.checkAlarmInstanceNotifications,
                    interval : Ext.max([1, (Viz.ApplicationSettings.SessionAspTimeOutInMinutes / 2) - 1]) * 60 * 1000,
                    scope    : this
                };
                Ext.TaskMgr.start(this._checkAlarmInstanceNotificationsTask);
            },

            /**
             * Builds the toolbar with the icons of the different PortalWindows.
             */
            buildApplicationToolbarLinks            : function() {
                var items = this.getItemsApplicationToolbarLinks();
                var subribbons = {};
                Ext.each(items, function(item) {
                            if (Ext.isEmpty(item.iconCls) && !Ext.isEmpty(item.data.KeyImage)) {
                                item.iconCls = Viz.util.Portal.getCssFromImage(item.data.KeyImage, 32, 32);
                            }
                            if (!subribbons[item.data.GroupNameMsgCode]) {

                                subribbons[item.data.GroupNameMsgCode] = {
                                    title : $lang(item.data.GroupNameMsgCode),
                                    cfg   : {
                                        defaults : {
                                            width  : 80,
                                            height : 70
                                        }
                                    },
                                    items : [item]
                                };
                            }
                            else {
                                subribbons[item.data.GroupNameMsgCode].items.push(item);
                            }
                        }, this);

                var ribbon = {
                    title  : $lang('msg_links'),
                    id     : this._linkToolbarId,
                    ribbon : []
                };

                Ext.iterate(subribbons, function(key, value) {
                            ribbon.ribbon.push(value);
                        });

                return ribbon;
            },

            /**
             * get the array of Button Config to create or update the links toolbar.
             */
            getItemsApplicationToolbarLinks         : function() {
                var store = Viz.Configuration.Link;
                var items = [];
                var array = store.getArray();
                var arrayPart = Ext.partition(array, function(link) {
                            return (link.KeyParent && (link.KeyParent != "") && (link.KeyParent != null));
                        });
                var rootArray = arrayPart[1];
                Ext.each(rootArray, function(data) {

                            var children = Ext.partition(arrayPart[0], function(childLink) {
                                        return childLink.KeyParent === data.KeyLink;
                                    })[0];

                            if (children.length != 0) {
                                var innerItems = [];
                                Ext.each(children, function(child, i) {
                                            innerItems.push({
                                                        iconCls : child.IconCls,
                                                        text    : child.Label,
                                                        // hidden: !$authorized(child.TaskName),
                                                        tooltip : child.LocalId,
                                                        data    : child,
                                                        handler : function() {
                                                            window.open(child.LinkValue);
                                                        }
                                                    }

                                            );
                                        });

                                Ext.each(innerItems, function(item) {
                                            if (Ext.isEmpty(item.iconCls) && !Ext.isEmpty(item.data.KeyImage)) {
                                                item.iconCls = Viz.util.Portal.getCssFromImage(item.data.KeyImage, 16, 16);
                                            }
                                        }, this);

                                items.push({
                                            xtype   : 'splitbutton',
                                            iconCls : data.IconCls,
                                            text    : data.Label,
                                            tooltip : data.LocalId,
                                            data    : data,

                                            width   : 90,
                                            // hidden: !$authorized(data.TaskName),
                                            handler : function() {
                                                window.open(data.LinkValue);
                                            },
                                            menu    : new Ext.menu.Menu({
                                                        items : innerItems
                                                    })
                                        })
                            }

                            else {
                                items.push({
                                            iconCls : data.IconCls,
                                            text    : data.Label,
                                            // hidden: !$authorized(data.TaskName),
                                            tooltip : data.LocalId,
                                            data    : data,
                                            handler : function() {
                                                window.open(data.LinkValue);
                                            }
                                        });
                            }
                        }, this);
                return items;
            },

            /**
             * Update the toolbar with the icons of the different PortalWindows.
             */
            updateApplicationToolbarLinks           : function() {
                var callback = function() {
                    // var items = this.getItemsApplicationToolbarLinks();
                    // if (Viz.App.Global.IconManager)
                    // Viz.App.Global.IconManager.updateApplicationToolbarLinks(items);

                    var ribbon = Ext.getCmp(this._northRibbonId);
                    var previousTab = ribbon.getActiveTab();
                    var existingLinkTab = Ext.getCmp(this._linkToolbarId);
                    var isActive = (previousTab == existingLinkTab);
                    var newLinkTab = this.buildApplicationToolbarLinks();

                    ribbon.remove(existingLinkTab);
                    newPortalTab = ribbon.initRibbon(newLinkTab, ribbon.items.length);
                    ribbon.add(newLinkTab);
                    ribbon.doLayout();

                    if (isActive) {
                        ribbon.setActiveTab(Ext.getCmp(this._linkToolbarId));
                    }
                };
                Viz.App.loadLinks(callback.createDelegate(this));
            },

            /**
             * Destructor.
             */
            onDestroy                               : function() {
                Ext.TaskMgr.stop(this._checkAlarmInstanceNotificationsTask);
                Viz.util.MessageBusMgr.unsubscribe("PortalWindowChange", this.updateApplicationToolbarPortalWindows, this);
                Viz.util.MessageBusMgr.unsubscribe("LinkChange", this.updateApplicationToolbarLinks, this);
                Viz.tree.Portlet.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Builds the north region
             * @return {Object}
             */
            buildRegionNorth                        : function(config) {

                return {
                    region       : 'north',
                    id           : this._northRegionId,
                    // layout : 'fit',
                    stateId      : 'MainViewportRegionNorth',
                    hidden       : Viz.DisplayAsDesktop,
                    // margins : '10 0 20 0',
                    bodyStyle    : 'border-bottom:none;background-color:transparent',
                    // title : $lang('msg_application_title') + ' ' + Viz.AssemblyVersion,
                    // iconCls : 'viz-icon-small-main',

                    items        : this.buildApplicationToolbar(),
                    // tools : this.buildNorthRegionTools(),
                    collapseMode : 'mini',
                    // collapsible : true,
                    height       : 127,
                    listeners    : {
                        expand : function() {
                            var ribbon = Ext.getCmp(this._northRibbonId);
                            var bufferPixels = 4;
                            var widthWithoutMargins = this.getWidth() - this._mainMarginLeft - this._mainMarginRight - bufferPixels;
                            ribbon.setSize(widthWithoutMargins, ribbon.getHeight());
                            ribbon.doLayout();
                            this.initGlobalGrid();
                        },
                        collapse: this.initGlobalGrid,
                        scope  : this
                    }
                };
            },
            /**
             * Builds the north region tools.
             * @return {Object}
             */
            buildNorthRegionTools                   : function() {
                return [{
                            id     : 'pin',
                            qtip   : $lang("msg_pin_desktopicons"),
                            hidden : true,
                            // handler : this.pinDesktopIcons,
                            scope  : this
                        }, {
                            id     : 'unpin',
                            qtip   : $lang("msg_unpin_desktopicons"),
                            hidden : !$authorized('@@@@ Portal Icons - Full Control'),
                            // handler : this.unpinDesktopIcons,
                            scope  : this
                        }, {
                            id      : 'help',
                            // qtip : $lang('msg_help'),
                            scope   : this,
                            handler : function(event, tool) {
                                event.stopEvent();
                                var menu = new Ext.menu.Menu({
                                            items : [{
                                                        text    : $lang('msg_application_about') + ' ' + $lang('msg_application_title'),
                                                        iconCls : 'viz-icon-small-about',
                                                        handler : function(item, event) {
                                                            Viz.App.showAboutBox(event, tool);
                                                        }
                                                    }, {
                                                        text    : $lang('msg_changepassword'),
                                                        iconCls : 'viz-icon-small-changepassword',
                                                        handler : function(item, event) {
                                                            new Viz.window.ChangePassword({
                                                                        animateTarget : tool
                                                                    }).show();
                                                        },
                                                        hidden  : !$authorized('@@@@ Change password')
                                                    }, {
                                                        text    : $lang('msg_user_info'),
                                                        iconCls : 'viz-icon-small-user',
                                                        handler : function() {
                                                            new Ext.Window({
                                                                        title   : $lang('msg_user_info'),
                                                                        iconCls : 'viz-icon-small-user',
                                                                        width   : 550,
                                                                        height  : 320,
                                                                        layout  : 'fit',
                                                                        items   : [{
                                                                                    xtype     : 'vizFormUserInfo',
                                                                                    border    : false,
                                                                                    listeners : {
                                                                                        render : function() {
                                                                                            Viz.util.MessageBusMgr.publish("UpdateUserInfo", Viz.App.Global.User);
                                                                                        }
                                                                                    }
                                                                                }]
                                                                    }).show();
                                                        }
                                                    }, {
                                                        text    : $lang('msg_logo_change'),
                                                        iconCls : 'viz-icon-small-logo',
                                                        hidden  : !$authorized('@@@@ Logo - Full Control'),
                                                        handler : function() {
                                                            // Get the logo entity
                                                            var logoConfig = {
                                                                common      : {
                                                                    height   : 120,
                                                                    width    : 500,
                                                                    xtype    : 'vizFormLogo',
                                                                    readonly : !$authorized('@@@@ Logo - Full Control')
                                                                },
                                                                create      : {
                                                                    title   : $lang("msg_logo_change"),
                                                                    iconCls : 'viz-icon-small-add'
                                                                },
                                                                update      : {
                                                                    title               : $lang("msg_logo_change"),
                                                                    iconCls             : 'viz-icon-small-update',
                                                                    titleEntity         : 'Name',
                                                                    serviceParamsEntity : [{
                                                                                name  : 'Key',
                                                                                value : 'KeyLogo'
                                                                            }]
                                                                },
                                                                updatebatch : {
                                                                    title                     : $lang("msg_updatebatch"),
                                                                    iconCls                   : 'viz-icon-small-update',
                                                                    serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.DynamicDisplayImage_FormUpdateBatch
                                                                }
                                                            };

                                                            Viz.Services.CoreWCF.Logo_GetItem({
                                                                        Key     : 1,
                                                                        success : function(entity) {
                                                                            if (entity === null) {
                                                                                Viz.openFormCrud(logoConfig, 'create');
                                                                            }
                                                                            else {
                                                                                entity.Name = "Logo";
                                                                                Viz.openFormCrud(logoConfig, 'update', entity);
                                                                            }
                                                                        }
                                                                    });
                                                            // Get the logo entity

                                                            /*
                                                             * new Ext.Window(logoConfig{ title: $lang('msg_logo_change'), iconCls: 'viz-icon-small-logo', width: 550, height: 150, layout: 'fit', mode: 'update', items: [{ xtype: 'vizFormLogo', border: false }] }).show();
                                                             */
                                                        }
                                                    }, {
                                                        text    : $lang('msg_userprofilepreference_clear'),
                                                        iconCls : 'viz-icon-small-clean',
                                                        handler : function() {
                                                            Ext.MessageBox.confirm($lang('msg_application_title'), $lang('msg_userprofilepreference_clear_confirm'), function(button) {
                                                                        if (button == 'yes') {
                                                                            Viz.Services.CoreWCF.UserProfilePreference_DeleteAllData({
                                                                                        success : function() {
                                                                                            Ext.state.Manager.getProvider().state = {};
                                                                                        }
                                                                                    });
                                                                        }
                                                                    }, this);
                                                        }
                                                    }]
                                        });
                                menu.on("hide", function(obj) {
                                            obj.destroy();
                                        });
                                var align = 'tr-br?';
                                if ($lang("rtl") == "rtl")
                                    align = 'tl-bl?';
                                menu.show(tool, align);
                            }
                        }, {
                            id      : 'close',
                            qtip    : $lang("msg_logout"),
                            handler : function() {
                                this.onLogOut();
                            },
                            scope   : this
                        }];
            },

            /**
             * @param {Ext.Button} btn
             */
            unpinDesktopIcons                       : function(btn) {
                if ($authorized('@@@@ Portal Icons - Full Control')) {
                    // Ext.getCmp(this._unpinButton).hide();
                    // Ext.getCmp(this._pinButton).show();
                    if (btn.pressed) {
                        btn.setIconClass("viz-icon-small-pin");
                        Viz.App.Global.IconManager.enableIconsManager();
                    }
                    else {
                        btn.setIconClass("viz-icon-small-unpin");
                        Viz.App.Global.IconManager.disableIconsManager();
                    }

                }
            },

            /**
             * Returns an array containing the different toolbar for the modules.
             * @return {}
             */
            getAllToolbars                          : function() {
                return [this.buildApplicationToolbarAdministration(), this.buildApplicationToolbarSpaceManagement(), this.buildApplicationToolbarEnergy(), this.buildApplicationToolbarLinks(), this.buildApplicationToolbarPortalWindows()];
            },

            /**
             * Builds the main application toolbar for modules.
             * @return {Object}
             */
            buildApplicationToolbar                 : function() {
                // return new Viz.TabToolbar(this.getAllToolbars());
                return new Ext.ux.Ribbon({
                            stateId   : 'MainViewPortRibbon',
                            id        : this._northRibbonId,
                            activeTab : 0,
                            items     : this.getAllToolbars(),
                            height    : 125
                        });
            },

            /**
             * Builds the toolbar for the Administration Module.
             * @return {Object}
             */
            buildApplicationToolbarAdministration   : function() {
                return {
                    title  : $lang('msg_module_administration'),
                    // hidden : !$authorized(['@@@@ User management', '@@@@ Change password', '@@@@ Historical logins', '@@@@ Application Groups', '@@@@ Search - Occupants', '@@@@ Role', '@@@@ Job
                    // management', '@@@@ Mail', '@@@@ Import a file', '@@@@ Task management', '@@@@ Operation management', '@@@@ Manage Classification definitions', '@@@@ Language Managment', '@@@@
                    // Translator', '@@@@ Trace Event', '@@@@ Tenant Provisioning']),
                    ribbon : [{
                                title : $lang('msg_module_users'),
                                cfg   : {
                                    defaults : {
                                        width  : 60,
                                        height : 70
                                    }
                                },
                                items : [{
                                            iconCls : 'viz-icon-large-search-occupant',
                                            text    : $lang('msg_search_occupant'),
                                            width   : 80,
                                            handler : this.openModule.createDelegate(this, ["vizPanelOccupant"]),
                                            hidden  : !$authorized('@@@@ Search - Occupants')

                                        }, {
                                            iconCls : 'viz-icon-large-user',
                                            width   : 80,
                                            text    : $lang('msg_user_management'),
                                            handler : this.openModule.createDelegate(this, ["vizPanelUser"]),
                                            hidden  : !$authorized('@@@@ User management')
                                        }, {
                                            iconCls : 'viz-icon-large-changepassword',
                                            width   : 90,
                                            text    : $lang('msg_changepassword'),
                                            handler : function(button) {
                                                new Viz.window.ChangePassword({
                                                            animateTarget : button.getEl()
                                                        }).show();
                                            },
                                            hidden  : !$authorized('@@@@ Change password')
                                        }, {
                                            iconCls : 'viz-icon-large-loginhistory',
                                            width   : 90,
                                            text    : $lang('msg_application_loginhistory'),
                                            handler : this.openModule.createDelegate(this, ["vizPanelLoginHistory"]),
                                            hidden  : !$authorized('@@@@ Historical logins')
                                        }, {
                                            iconCls : 'viz-icon-large-loginhistoryactive',
                                            text    : $lang('msg_application_loginhistoryactive'),
                                            handler : this.openModule.createDelegate(this, ["vizPanelLoginHistoryActive"]),
                                            hidden  : !$authorized('@@@@ Active logins')
                                        }, {
                                            iconCls : 'viz-icon-large-loginasuser',
                                            width   : 90,
                                            text    : $lang('msg_loginasuser'),
                                            handler : function(button) {
                                                new Viz.window.LoginAsUser({
                                                            animateTarget : button.getEl()
                                                        }).show();
                                            },
                                            hidden  : !$authorized('@@@@ Login as User')
                                        }]
                            }, {
                                title : $lang('msg_module_authorization'),
                                cfg   : {
                                    defaults : {
                                        width  : 60,
                                        height : 70
                                    }
                                },
                                items : [{
                                            iconCls : 'viz-icon-large-applicationgroup',
                                            text    : $lang('msg_applicationgroup_root'),
                                            handler : this.openModule.createDelegate(this, ["vizPanelApplicationGroup"]),
                                            width   : 80,
                                            hidden  : !$authorized('@@@@ Application Groups')
                                        }, {
                                            iconCls: 'viz-icon-large-filter',
                                            text    : $lang('msg_azmanfilter_management'),
                                            handler : this.openModule.createDelegate(this, ["vizPanelAzManFilter"]),
                                            hidden  : !$authorized('@@@@ Project Hierarchy')
                                        }, {
                                            iconCls : 'viz-icon-large-role',
                                            text    : $lang('msg_azmanrole_tree'),
                                            handler : this.openModule.createDelegate(this, ["vizPanelAzManRole"]),
                                            hidden  : !$authorized('@@@@ Role')
                                        }, {
                                            iconCls : 'viz-icon-large-azmantask',
                                            text    : $lang('msg_azmantask_management'),
                                            handler : this.openModule.createDelegate(this, ["vizPanelAzManTask"]),
                                            width   : 80,
                                            hidden  : !$authorized('@@@@ Task management')
                                        }, {
                                            iconCls : 'viz-icon-large-azmanoperation',
                                            text    : $lang('msg_azmanoperation_management'),
                                            handler : this.openModule.createDelegate(this, ["vizPanelAzManOperation"]),
                                            width   : 80,
                                            hidden  : !$authorized('@@@@ Operation management')
                                        }, {
                                            xtype   : 'splitbutton',
                                            text    : $lang('msg_azman_import'),
                                            width   : 90,
                                            iconCls : 'viz-icon-large-importazman',
                                            hidden  : !$authorized('@@@@ Import Authorizations'),
                                            // arrowAlign : 'bottom',
                                            handler : this.importAuthorizationsHandler,
                                            menu    : new Ext.menu.Menu({
                                                        items : [{
                                                                    text    : $lang('msg_azman_export'),
                                                                    iconCls : 'viz-icon-small-exportazman',
                                                                    hidden  : !$authorized('@@@@ Import Authorizations'),
                                                                    handler : function() {
                                                                        var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                                                                    pollingInterval : 2000,
                                                                                    serviceStart    : Viz.Services.AuthenticationWCF.BeginAzManExport,
                                                                                    isFileResult    : true,
                                                                                    showProgress    : true,
                                                                                    scope           : this
                                                                                });

                                                                        submitter.start();
                                                                    }
                                                                }]
                                                    })
                                        }]
                            }, {
                                title : $lang('msg_module_mapping'),
                                cfg   : {
                                    defaults : {
                                        width  : 60,
                                        height : 70
                                    }
                                },
                                items : [{
                                            iconCls : 'viz-icon-large-classificationdefinition',
                                            text    : $lang('msg_application_classificationdefinition'),
                                            handler : this.openModule.createDelegate(this, ["vizPanelClassificationDefinition"]),
                                            width   : 90,
                                            hidden  : !$authorized('@@@@ Manage Classification definitions')
                                        }, {
                                            xtype   : 'splitbutton',
                                            text    : $lang('msg_mapping_import_file'),
                                            width   : 90,
                                            iconCls : 'viz-icon-large-xml',
                                            hidden  : !$authorized('@@@@ Import a file'),
                                            // arrowAlign : 'bottom',
                                            handler : function(button) {
                                                new Viz.window.ImportXml({
                                                            animateTarget : button.getEl()
                                                        }).show();
                                            },
                                            menu    : new Ext.menu.Menu({
                                                        items : [{
                                                                    text    : $lang('msg_mapping_download_commissioning'),
                                                                    iconCls : 'viz-icon-small-download-document',
                                                                    // hidden : !$authorized('@@@@ Import commissioning'),
                                                                    handler : function() {
                                                                        Viz.downloadFile('etc/Excel%20Mapping%20Workbook.xlsx');
                                                                    }
                                                                }, {
                                                                    text    : $lang('msg_mapping_download_mapping'),
                                                                    iconCls : 'viz-icon-small-download-document',
                                                                    // hidden : !$authorized('@@@@ Import mapping'),
                                                                    handler : function() {
                                                                        Viz.downloadFile('etc/SchemaBasedXmlMapping.xsd');
                                                                    }
                                                                }]
                                                    })
                                        }, {
                                            iconCls : 'viz-icon-large-excel',
                                            text    : $lang('msg_convert_mapping_file_to_xml'),
                                            width   : 80,
                                            handler : function(button) {
                                                new Viz.window.ImportXml({
                                                            title          : $lang('msg_convert_mapping_file_to_xml'),
                                                            animateTarget  : button.getEl(),
                                                            serviceHandler : Viz.Services.MappingWCF.BeginMappingFileConversion
                                                        }).show();
                                            },
                                            hidden  : !$authorized('@@@@ Import a file')
                                        }, {
                                            iconCls : 'viz-icon-large-auditentity',
                                            text    : $lang('msg_application_auditentity'),
                                            width   : 80,
                                            handler : this.openModule.createDelegate(this, ["vizPanelAuditEntity"]),
                                            hidden  : !$authorized('@@@@ AuditEntity')
                                        }]
                            }, {
                                title : $lang('msg_module_tools'),
                                cfg   : {
                                    defaults : {
                                        width  : 60,
                                        height : 70
                                    }
                                },
                                items : [{
                                    iconCls : 'viz-icon-large-link',
                                    text    : $lang('msg_application_link'),
                                    handler : this.openModule.createDelegate(this, ["vizPanelLink"]),
                                    width   : 80,
                                    hidden  : !$authorized('@@@@ Link')
                                        // Should be link

                                    }, {
                                    iconCls : 'viz-icon-large-mail',
                                    text    : $lang('msg_application_mail'),
                                    handler : this.openModule.createDelegate(this, ["vizPanelMail"]),
                                    width   : 80,
                                    hidden  : !$authorized('@@@@ Mail')

                                }, {
                                    iconCls : 'viz-icon-large-job',
                                    text    : $lang('msg_application_job'),
                                    handler : this.openModule.createDelegate(this, ["vizPanelJob"]),
                                    width   : 80,
                                    hidden  : !$authorized('@@@@ Job management')
                                }, {
                                    iconCls : 'viz-icon-large-trace',
                                    text    : $lang('msg_application_trace_viewer'),
                                    handler : this.openModule.createDelegate(this, ["vizPanelTraceEntry"]),
                                    hidden  : !$authorized('@@@@ Trace Entry')
                                }, {
                                    iconCls : 'viz-icon-large-tracesummary',
                                    text    : $lang('msg_application_trace_summary'),
                                    handler : this.openModule.createDelegate(this, ["vizPanelTraceSummary"]),
                                    hidden  : !$authorized('@@@@ Trace Summary')
                                }, {
                                    iconCls : 'viz-icon-large-tenant',
                                    text    : $lang('msg_application_tenant'),
                                    width   : 80,
                                    handler : this.openModule.createDelegate(this, ["vizPanelTenant"]),
                                    hidden  : !$authorized('@@@@ Tenant Provisioning')
                                }, {
                                    iconCls : 'viz-icon-large-python',
                                    text    : $lang('msg_algorithms'),
                                    width   : 80,
                                    handler : this.openModule.createDelegate(this, ["vizPanelAlgorithm"]),
                                    hidden  : !$authorized('@@@@ Algorithm')
                                }]
                            },

                            {
                                title : $lang('msg_module_localization'),
                                cfg   : {
                                    defaults : {
                                        width  : 60,
                                        height : 70
                                    }
                                },
                                items : [{
                                            iconCls : 'viz-icon-large-localization',
                                            text    : $lang('msg_localization'),
                                            handler : this.openModule.createDelegate(this, ["vizPanelLocalization"]),
                                            width   : 80,
                                            hidden  : !$authorized('@@@@ Language Managment')

                                        }, {
                                            iconCls : 'viz-icon-large-translator',
                                            text    : $lang('msg_application_translator'),
                                            handler : function() {
                                                new Viz.window.Translator().show();
                                            },
                                            hidden  : !$authorized('@@@@ Translator')
                                        }]
                            }]
                };
            },
            /**
             * Builds the toobar for the ServiceDesk module.
             * @return {Object}
             */
            importAuthorizationsHandler             : function(button) {
                new Viz.window.AzmanImportXml({
                            animateTarget  : button.getEl(),
                            title          : $lang('msg_azman_import_file'),
                            serviceHandler : Viz.Services.CoreWCF.BeginImporting,
                            iconCls        : 'viz-icon-small-importazman'
                        }).show();
            },

            buildApplicationToolbarServiceDesk      : function() {
                return {
                    title  : $lang('msg_module_servicedesk'),
                    // hidden : !$authorized(['ActionRequest - Read', 'Task - Read', '@@@@ Manage Priorities']),
                    ribbon : [{
                                title : $lang('msg_wizard'),
                                cfg   : {
                                    defaults : {
                                        width  : 90,
                                        height : 70
                                    }
                                },
                                items : [{
                                            iconCls    : 'viz-icon-large-wizard',
                                            text       : $lang('msg_wizard'),
                                            arrowAlign : 'right',
                                            menu       : [{
                                                iconCls : 'viz-icon-small-actionrequest',
                                                text    : $lang('msg_actionrequest_wizard'),
                                                handler : Viz.grid.ActionRequest.launchWizard
                                                    // hidden : !$authorized('UI_vizPanelMeter')
                                                }],
                                            hidden     : !$authorized('@@@@ Action Request - Full Control')
                                        }]
                            }, {
                                title : $lang('msg_module_servicedesk'),
                                cfg   : {
                                    defaults : {
                                        width  : 90,
                                        height : 70
                                    }
                                },
                                items : [{
                                            iconCls : 'viz-icon-large-servicedeskRequest',
                                            text    : $lang('msg_servicedesk_all_requests'),
                                            handler : this.openModule.createDelegate(this, ["vizPanelActionRequest"]),
                                            hidden  : !$authorized('@@@@ All Action Requests')
                                        }, {
                                            iconCls : 'viz-icon-large-actionrequest',
                                            handler : function() {
                                                Viz.App.Global.ViewPort.openModule({
                                                            title   : $lang('msg_servicedesk_my_requests'),
                                                            filters : ['Requestor'],
                                                            xtype   : 'vizPanelActionRequestSpecific'
                                                        });
                                            },
                                            text    : $lang('msg_servicedesk_my_requests'),
                                            hidden  : !$authorized('@@@@ My Action Request')
                                        }, {
                                            iconCls : 'viz-icon-large-helpdesk',
                                            handler : function() {
                                                Viz.App.Global.ViewPort.openModule({
                                                            title   : $lang('msg_servicedesk_assigned_requests'),
                                                            iconCls : 'viz-icon-small-helpdesk',
                                                            panel   : 'Task',
                                                            filters : ['Approver'],
                                                            xtype   : 'vizPanelActionRequestSpecific'
                                                        });
                                            },
                                            text    : $lang('msg_servicedesk_assigned_requests'),
                                            hidden  : !$authorized('@@@@ Assigned Action Request')
                                        }, {
                                            iconCls : 'viz-icon-large-technician',
                                            handler : function() {
                                                Viz.App.Global.ViewPort.openModule({
                                                            title   : $lang('msg_servicedesk_assigned_tasks'),
                                                            panel   : 'Task',
                                                            iconCls : 'viz-icon-small-task',
                                                            filters : ['Actor'],
                                                            xtype   : 'vizPanelActionRequestSpecific'
                                                        });
                                            },
                                            text    : $lang('msg_servicedesk_assigned_tasks'),
                                            hidden  : !$authorized('@@@@ Assigned Task')
                                        }, {
                                            iconCls : 'viz-icon-large-preventive',
                                            hidden  : true,
                                            handler : function() {
                                                Viz.App.Global.ViewPort.openModule({
                                                            title : $lang('msg_servicedesk_scheduled_requests'),
                                                            xtype : 'vizPanelCalendarTask'
                                                        });
                                            },
                                            text    : $lang('msg_servicedesk_scheduled_requests')
                                        }, {
                                            iconCls : 'viz-icon-large-technician',
                                            hidden  : true,
                                            handler : function() {
                                                Viz.App.Global.ViewPort.openModule({
                                                            title   : $lang('msg_calendar'),
                                                            filters : ['Requestor'],
                                                            xtype   : 'vizPanelCalendarTask'
                                                        });
                                            },
                                            text    : 'Tasks'
                                        }, {
                                            iconCls : 'viz-icon-large-helpdesk',
                                            hidden  : true,
                                            text    : ''
                                        }, {
                                            iconCls : 'viz-icon-large-manager',
                                            hidden  : true,
                                            text    : ''
                                        }, {
                                            iconCls : 'viz-icon-large-technician',
                                            hidden  : true,
                                            text    : ''
                                        }, {
                                            iconCls : 'viz-icon-large-preventive',
                                            hidden  : true,
                                            text    : ''
                                        }, {
                                            iconCls : 'viz-icon-large-template',
                                            hidden  : true,
                                            text    : ''
                                        }, {
                                            iconCls : 'viz-icon-large-searchrequest',
                                            hidden  : true,
                                            text    : ''
                                        }, {
                                            iconCls : 'viz-icon-large-changeUser',
                                            hidden  : true,
                                            text    : ''
                                        }, {
                                            iconCls : 'viz-icon-large-filterClassification',
                                            hidden  : true,
                                            text    : ''
                                        }, {
                                            iconCls : 'viz-icon-large-priority',
                                            text    : $lang('msg_priority_manage'),
                                            handler : this.openModule.createDelegate(this, ["vizPanelPriority"]),
                                            hidden  : !$authorized('@@@@ Manage Priorities')
                                        }]
                            }]
                };
            },
            /**
             * Builds the toolbar for the SpaceManagement module.
             */
            buildApplicationToolbarSpaceManagement  : function() {
                return {
                    title  : $lang('msg_module_spacemanagement'),
                    hidden : true,
                    ribbon : [{
                                title : $lang('msg_module_spacemanagement'),
                                cfg   : {
                                    defaults : {
                                        width  : 60,
                                        height : 70
                                    }
                                },
                                items : [{
                                            iconCls : 'viz-icon-large-manageFloor',
                                            text    : $lang('msg_spacemanagement_managefloor'),
                                            handler : function() {
                                                var treeSpatial = Ext.getCmp(this._treeSpatial);
                                                var node = treeSpatial.getSelectionModel().getSelectedNode();
                                                if (node && node.attributes.entity && node.attributes.entity.IfcType == 'IfcBuildingStorey') {
                                                    this.openModule.call(this, {
                                                                title  : node.parentNode.text + '/' + node.text,
                                                                entity : node.attributes.entity,
                                                                xtype  : 'vizPanelManageBuildingStorey'
                                                            });
                                                }
                                                // else
                                                // treeSpatial.getEl().frame();
                                            },
                                            scope   : this
                                        }, {
                                            iconCls : 'viz-icon-large-viewFloor'
                                        }, {
                                            iconCls : 'viz-icon-large-view3D',
                                            iconCls : 'viz-icon-large-reportSimple'
                                        }, {
                                            iconCls : 'viz-icon-large-reportConsolide'
                                        }, {
                                            iconCls : 'viz-icon-large-reportPeople'
                                        }, {
                                            iconCls : 'viz-icon-large-desktype'
                                        }, {
                                            iconCls : 'viz-icon-large-adminDrawing'
                                        }]
                            }]
                };
            },
            /**
             * Builds the toolbar for the Energy module.
             */
            buildApplicationToolbarEnergy           : function() {
                return {
                    title  : $lang('msg_module_energy'),
                    // hidden : !$authorized(['@@@@ Chart', '@@@@ Alarm Definition', '@@@@ Data Acquistion', '@@@@ Portal']),
                    ribbon : [{
                                title : $lang('msg_wizard'),
                                cfg   : {
                                    defaults : {
                                        width  : 90,
                                        height : 70
                                    }
                                },
                                items : [{
                                            iconCls    : 'viz-icon-large-wizard',
                                            text       : $lang('msg_wizard'),
                                            hidden     : !$authorized(['@@@@ Chart - Full Control', '@@@@ Alarm Definition - Full Control']),
                                            arrowAlign : 'right',
                                            menu       : [{
                                                        iconCls : 'viz-icon-small-portalwindow',
                                                        text    : $lang('msg_portalwindow_wizard'),
                                                        hidden  : !$authorized('@@@@ Portal - Full Control') && !$authorized('@@@@ Portal - Wizard'),
                                                //handler : Viz.grid.PortalWindow.launchWizard
                                                        handler: function () {
                                                            Viz.grid.PortalWindow.launchWizard();
                                                            this.writeEnergyDashBoardEventsToGoogleAnalytics('Portal Wizard');
                                                        }.createDelegate(this)
                                                    }, {
                                                        iconCls : 'viz-icon-small-chart',
                                                        text    : $lang('msg_chart_wizard'),
                                                        //handler : Viz.grid.Chart.launchWizard,
                                                        handler: function () {
                                                            Viz.grid.Chart.launchWizard();
                                                            this.writeEnergyDashBoardEventsToGoogleAnalytics('Chart Wizard');
                                                        }.createDelegate(this),
                                                        hidden  : !$authorized('@@@@ Chart - Full Control') && !$authorized('@@@@ Chart - Wizard')
                                                    }, {
                                                        iconCls : 'viz-icon-small-alarm-meter',
                                                        text    : $lang('msg_alarmdefinition_wizard_meter'),
                                                        //handler : Viz.grid.AlarmDefinition.launchWizard.createDelegate(this, ['Meter']),
                                                        handler: function () {
                                                            Viz.grid.AlarmDefinition.launchWizard(['Meter']);
                                                            this.writeEnergyDashBoardEventsToGoogleAnalytics('Alarm definition wizard on meters');
                                                        }.createDelegate(this),
                                                        hidden  : !$authorized('@@@@ Alarm Definition')
                                                    }, {
                                                        iconCls : 'viz-icon-small-alarm-chart',
                                                        text    : $lang('msg_alarmdefinition_wizard_chart'),
                                                        //handler : Viz.grid.AlarmDefinition.launchWizard.createDelegate(this, ['Chart']),
                                                        handler: function () {
                                                            Viz.grid.AlarmDefinition.launchWizard(['Chart']);
                                                            this.writeEnergyDashBoardEventsToGoogleAnalytics('Alarm definition wizard on charts');
                                                        }.createDelegate(this),
                                                        hidden  : !$authorized('@@@@ Alarm Definition')
                                                    }]
                                        }]
                            }, {
                                title : $lang('msg_meters'),
                                cfg   : {
                                    defaults : {
                                        width  : 60,
                                        height : 70
                                    }
                                },
                                items : [{
                                            iconCls    : 'viz-icon-large-dataacquisitioncontainer',
                                            text       : $lang('msg_dataacquisitioncontainer'),
                                            arrowAlign : 'right',
                                            width      : 90,
                                            hidden     : !$authorized('@@@@ Data Acquistion'),
                                            menu       : [{
                                                        iconCls : 'viz-icon-small-ewsdataacquisitioncontainer',
                                                        text    : $lang('msg_ewsdataacquisitioncontainer'),

                                                        handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelEWSDataAcquisitionContainer", function () {
                                                            return this.writeEnergyDashBoardEventsToGoogleAnalytics('EWS Data Acquisition Container');
                                                        }, this]),
                                                        hidden  : !$authorized('@@@@ Data Acquistion')
                                                    },
                                                    /*
                                                     * { iconCls : 'viz-icon-small-struxurewaredataacquisitioncontainer', text : $lang('msg_struxurewaredataacquisitioncontainer'), handler : this.openModule.createDelegate(this, ["vizPanelStruxurewareDataAcquisitionContainer"]), hidden : !$authorized('@@@@ Data Acquistion') }
                                                     */
                                                    {
                                                        iconCls : 'viz-icon-small-dataacquisitioncontainer',
                                                        text    : $lang('msg_restdataacquisitioncontainer'),
                                                        handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelRESTDataAcquisitionContainer", function () {
                                                                    return this.writeEnergyDashBoardEventsToGoogleAnalytics('REST Data Acquisition Container');
                                                                    }, this]),                                                   
                                                        hidden  : !$authorized('@@@@ Data Acquistion')
                                                    }, {
                                                        iconCls : 'viz-icon-small-xml',
                                                        text    : $lang('msg_mapping_import_file'),
                                                        hidden  : !$authorized('@@@@ Import a file'),
                                                        handler : function(button) {
                                                            new Viz.window.ImportXml({
                                                                        animateTarget : button.getEl()
                                                            }).show();
                                                            this.writeEnergyDashBoardEventsToGoogleAnalytics('Import mapping file');
                                                        },
                                                        scope : this,
                                                    }]
                                        }, {
                                            iconCls : 'viz-icon-large-meter',
                                            text    : $lang('msg_meters'),
                                            handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelMeter",
                                                function () { return this.writeEnergyDashBoardEventsToGoogleAnalytics('Meter(s)');
                                        }, this]),                                
                                            hidden  : !$authorized('@@@@ Meters')
                                        }, {
                                            iconCls    : 'viz-icon-large-meterdata',
                                            text       : $lang('msg_meterdata'),
                                            hidden     : !$authorized('@@@@ Meter Data'),
                                            width      : 90,
                                            arrowAlign : 'right',
                                            menu       : [{
                                                        iconCls : 'viz-icon-small-meterdata',
                                                        text    : $lang('msg_meterdata'),
                                                        handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelMeterData", function () {
                                                            return this.writeEnergyDashBoardEventsToGoogleAnalytics('Meter Data');
                                                        }, this]),                                                    
                                                        hidden  : !$authorized('@@@@ Meter Data')
                                                    }, {
                                                        iconCls : 'viz-icon-small-site',
                                                        text    : $lang('msg_meterdata_batch'),
                                                        handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelMeterDataBatch", function () {
                                                            return this.writeEnergyDashBoardEventsToGoogleAnalytics('Meter Data batch');
                                                        }, this]),                                                        
                                                        hidden  : !$authorized('@@@@ Meter Data')
                                                    }, {
                                                        iconCls : 'viz-icon-small-metervalidationrule',
                                                        text    : $lang('msg_metervalidationrule'),
                                                        handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelMeterValidationRule", function () {
                                                            return this.writeEnergyDashBoardEventsToGoogleAnalytics('Meter Data Validation Rule');
                                                        }, this]),                                                        
                                                        hidden  : !$authorized('@@@@ Meter Validation Rule')
                                                    }
                                            // ,
                                            // {
                                            // iconCls: 'viz-icon-small-apply',
                                            // text: $lang('msg_checkbox'),
                                            // handler:
                                            // function () {
                                            // var link = $lang('msg_checkbox_link');
                                            // if ((link != "") && (link != 'msg_checkbox_link'))
                                            // window.open(link);
                                            // },
                                            // hidden: (!$authorized('@@@@ Checkbox') || ($lang('msg_checkbox_link') == '') || ($lang('msg_checkbox_link') == 'msg_checkbox_link'))
                                            // }
                                            ]
                                        }]
                            }, {
                                title : $lang('msg_module_energy_eventsandcalendar'),
                                cfg   : {
                                    defaults : {
                                        width  : 60,
                                        height : 70
                                    }
                                },
                                items : [{
                                            iconCls : 'viz-icon-large-alarm',
                                            width   : 90,
                                            text    : $lang('msg_alarmdefinition'),
                                            handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelAlarmDefinition", function () {
                                                return this.writeEnergyDashBoardEventsToGoogleAnalytics('Alarm Definition');
                                            }, this]),                                          
                                            hidden  : !$authorized('@@@@ Alarm Definition')
                                        }, {
                                            iconCls : 'viz-icon-large-eventlog',
                                            text    : $lang('msg_eventlog'),
                                            hidden  : !$authorized('@@@@ Event Log'),
                                            handler : this.openModuleWithAnalytics.createDelegate(this, ["vizPanelEventLog", function () {
                                                return this.writeEnergyDashBoardEventsToGoogleAnalytics('Event Log');
                                            }, this])
                                            
                                            // hidden : !$authorized('UI_vizPanelEventLog')
                                    }   , {
                                            iconCls : 'viz-icon-large-calendareventcategory',
                                            width   : 90,
                                            text    : $lang('msg_calendareventcategory'),
                                            handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelCalendarEventCategory", function () {
                                                return this.writeEnergyDashBoardEventsToGoogleAnalytics('Calendar Event Category');
                                                            }, this]),                                            
                                            hidden  : !$authorized('@@@@ Calendar Event')
                                            // hidden : !$authorized('UI_vizPanelCalendarEventCategory')
                                    }]
                            }, {
                                title : $lang('msg_template'),
                                cfg   : {
                                    columns : 4
                                },
                                items : [{
                                    iconCls : 'viz-icon-large-chart-kpi',
                                    text    : $lang('msg_chart_kpi'),
                                    width   : 60,
                                    height  : 70,
                                    rowspan : 3,
                                    hidden  : !$authorized('@@@@ Chart KPI'),
                                    handler : function(button, event) {
                                        this.openModuleWithAnalytics({
                                                    useDataView : true,
                                                    isKPI       : true,
                                                    xtype: 'vizPanelChart'                  
                                        }, function () {
                                            return this.writeEnergyDashBoardEventsToGoogleAnalytics('KPI');
                                        }, this);
                                       // this.writeEnergyDashBoardEventsToGoogleAnalytics('KPI');
                                    }.createDelegate(this)
                                        // hidden : !$authorized('UI_vizPanelChart')
                                    }, {
                                    iconCls : 'viz-icon-large-dynamicdisplay',
                                    text    : $lang('msg_dynamicdisplay'),
                                    width   : 60,
                                    height  : 70,
                                    rowspan : 3,
                                    handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelDynamicDisplay", function () {
                                        return this.writeEnergyDashBoardEventsToGoogleAnalytics('Dynamic Display');
                                    }, this]),                                    
                                    hidden  : !$authorized('@@@@ Dynamic Display')
                                        // hidden : !$authorized('UI_vizPanelDynamicDisplay')
                                    }, {
                                    iconCls : 'viz-icon-large-energycertificate',
                                    text    : $lang('msg_energycertificate'),
                                    width   : 60,
                                    height  : 70,
                                    rowspan : 3,
                                    handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelEnergyCertificate", function () {
                                        return this.writeEnergyDashBoardEventsToGoogleAnalytics('Energy Certificate');
                                    }, this]),                               
                                    hidden  : !$authorized('@@@@ Energy certificate')
                                }, {
                                    iconCls   : 'viz-icon-small-palette',
                                    text      : $lang('msg_palette'),
                                    width     : 130,
                                    scale     : 'small',
                                    iconAlign : 'left',
                                    handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelPalette", function () {
                                        return this.writeEnergyDashBoardEventsToGoogleAnalytics('Palette');
                                    }, this]),                                    
                                    hidden    : !$authorized('@@@@ Palette')

                                        // hidden : !$authorized('UI_vizPanelChart')
                                    }, {
                                    iconCls   : 'viz-icon-small-dynamicdisplaycolortemplate',
                                    text      : $lang('msg_dynamicdisplaycolortemplate'),
                                    scale     : 'small',
                                    width     : 130,
                                    iconAlign : 'left',
                                    handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelDynamicDisplayColorTemplate", function () {
                                        return this.writeEnergyDashBoardEventsToGoogleAnalytics('Color Template');
                                    }, this]),                                    
                                    hidden    : !$authorized('@@@@ Dynamic Display color template')
                                        // hidden : !$authorized('UI_vizPanelDynamicDisplay')
                                    }, {
                                    iconCls   : 'viz-icon-small-dynamicdisplayimage',
                                    scale     : 'small',
                                    width     : 130,
                                    iconAlign : 'left',
                                    text      : $lang('msg_dynamicdisplayimage'),
                                    handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelDynamicDisplayImage", function () {
                                        return this.writeEnergyDashBoardEventsToGoogleAnalytics('Image');
                                    }, this]),                                    
                                    hidden    : !$authorized('@@@@ Dynamic Display Image')
                                        // hidden : !$authorized('UI_vizPanelDynamicDisplay')
                                    }]
                            }, {
                                title : $lang('msg_module_energy_portalandwidgets'),
                                cfg   : {
                                    defaults : {
                                        width  : 60,
                                        height : 70
                                    }
                                },
                                items : [{
                                    iconCls : 'viz-icon-large-chart',
                                    text    : $lang('msg_charts'),
                                    hidden  : !$authorized('@@@@ Chart'),
                                    handler : function(button, event) {
                                        this.openModuleWithAnalytics({
                                                    useDataView : event.ctrlKey,
                                                    xtype       : 'vizPanelChart'                                                  
                                        }, function () {
                                            return this.writeEnergyDashBoardEventsToGoogleAnalytics('Charts');
                                        }, this);                                        
                                    }.createDelegate(this)
                                        // hidden : !$authorized('UI_vizPanelChart')
                                    }, {
                                    iconCls : 'viz-icon-large-weatherlocation',
                                    text    : $lang('msg_weatherlocation'),
                                    handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelWeatherLocation", function () {
                                        return this.writeEnergyDashBoardEventsToGoogleAnalytics('Weather Location');
                                    }, this]),                                    
                                    hidden  : !$authorized('@@@@ Weather Location')
                                }, {
                                    iconCls : 'viz-icon-large-webframe',
                                    text    : $lang('msg_webframe'),
                                    handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelWebFrame", function () {
                                                return this.writeEnergyDashBoardEventsToGoogleAnalytics('Web Frame');
                                                        }, this]),                                    
                                    hidden  : !$authorized('@@@@ Web Frame')
                                }, {
                                    iconCls : 'viz-icon-large-map',
                                    text    : $lang('msg_map'),
                                    handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelMap", function () {
                                        return this.writeEnergyDashBoardEventsToGoogleAnalytics('Map');
                                    }, this]),                                    
                                    hidden  : !$authorized('@@@@ Map')
                                }, {
                                    iconCls : 'viz-icon-large-drawingcanvas',
                                    text    : $lang('msg_drawingcanvas'),
                                    handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelDrawingCanvas", function () {
                                            return this.writeEnergyDashBoardEventsToGoogleAnalytics('Drawing Canvas');
                                            }, this]),                                    
                                    hidden  : !$authorized('@@@@ Drawing Canvas')
                                }, {
                                    iconCls : 'viz-icon-large-alarmtable',
                                    text    : $lang('msg_alarmtable'),
                                    handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelAlarmTable", function () {
                                            return this.writeEnergyDashBoardEventsToGoogleAnalytics('Alarm Table');
                                        }, this]),                                    
                                    hidden  : !$authorized('@@@@ Alarm Table')
                                }, {
                                    iconCls : 'viz-icon-large-flipcard',
                                    text    : $lang('msg_flipcard'),
                                    handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelFlipCard", function () {
                                        return this.writeEnergyDashBoardEventsToGoogleAnalytics('Flip Card');
                                    }, this]),                                    
                                    hidden  : !$authorized('@@@@ FlipCard')
                                }, {
                                    iconCls : 'viz-icon-large-portalwindow',
                                    text    : $lang('msg_portalwindow'),
                                    handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelPortalWindow", function () {
                                        return this.writeEnergyDashBoardEventsToGoogleAnalytics('Portal');
                                    }, this]),                                    
                                    hidden  : !$authorized('@@@@ Portal')
                                }]
                            }, {
                                title : $lang('msg_module_energy_externalaccess'),
                                cfg   : {
                                    defaults : {
                                        width  : 60,
                                        height : 70
                                    }
                                },
                                items : [{
                                            iconCls : 'viz-icon-large-portaltemplate',
                                            width   : 90,
                                            text    : $lang('msg_portaltemplate'),
                                            handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelPortalTemplate", function () {
                                                return this.writeEnergyDashBoardEventsToGoogleAnalytics('Portal Template');
                                            }, this]),                                            
                                            hidden  : !$authorized('@@@@ PortalTemplate')
                                        }, {
                                            iconCls : 'viz-icon-large-playlist',
                                            text    : $lang('msg_playlist'),
                                            handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelPlaylist", function () {
                                                return this.writeEnergyDashBoardEventsToGoogleAnalytics('Playlist');
                                            }, this]),                                            
                                            hidden  : !$authorized('@@@@ Playlist')
                                        }, {
                                            iconCls : 'viz-icon-large-virtualfile',
                                            text    : $lang('msg_virtualfile'),
                                            handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelVirtualFile", function () {
                                                return this.writeEnergyDashBoardEventsToGoogleAnalytics('URL Shortcut');
                                            }, this]),                                            
                                            hidden  : !$authorized('@@@@ Virtual File')
                                        }, {
                                            iconCls : 'viz-icon-large-chartscheduler',
                                            width   : 95,
                                            text    : $lang('msg_chartscheduler'),
                                            handler: this.openModuleWithAnalytics.createDelegate(this, ["vizPanelChartScheduler", function () {
                                                return this.writeEnergyDashBoardEventsToGoogleAnalytics('Chart Scheduler');
                                            }, this]),
                                            hidden  : !$authorized('@@@@ Chart Scheduler')
                                        }]
                            }]
                };

            },
            /**
             * Builds the toolbar with the icons of the different PortalWindows.
             */
            buildApplicationToolbarPortalWindows    : function() {
                var items = this.getItemsApplicationToolbarPortalWindows();

                Ext.each(items, function(item) {
                            if (Ext.isEmpty(item.iconCls) && !Ext.isEmpty(item.data.KeyImage)) {
                                item.iconCls = Viz.util.Portal.getCssFromImage(item.data.KeyImage, 32, 32);
                            }
                        }, this);

                var ribbon = {
                    title  : $lang('msg_portalwindows'),
                    id     : this._portalToolbarId,
                    ribbon : []
                };

                while (items.length > 0) {
                    var subRibbon = {
                        title : $lang('msg_portalwindows'),
                        cfg   : {
                            defaults : {
                                width  : 80,
                                height : 70
                            }
                        },
                        items : []
                    };
                    for (var i = 0; i < 5; i++) {
                        if (items.length > 0) {
                            subRibbon.items.push(items.shift());
                        }
                    }
                    if (subRibbon.items.length > 0) {
                        ribbon.ribbon.push(subRibbon);
                    }
                }

                return ribbon;
            },

            /**
             * Update the toolbar with the icons of the different PortalWindows.
             */
            updateApplicationToolbarPortalWindows   : function() {
                var callback = function() {
                    var items = this.getItemsApplicationToolbarPortalWindows();
                    if (Viz.App.Global.IconManager)
                        Viz.App.Global.IconManager.updateApplicationToolbarPortalWindows(items);

                    var ribbon = Ext.getCmp(this._northRibbonId);
                    var previousTab = ribbon.getActiveTab();
                    var existingPortalTab = Ext.getCmp(this._portalToolbarId);
                    var isActive = (previousTab == existingPortalTab);
                    var newPortalTab = this.buildApplicationToolbarPortalWindows();

                    ribbon.remove(existingPortalTab);
                    newPortalTab = ribbon.initRibbon(newPortalTab, ribbon.items.length);
                    ribbon.add(newPortalTab);
                    ribbon.doLayout();

                    if (isActive) {
                        ribbon.setActiveTab(Ext.getCmp(this._portalToolbarId));
                    }
                };
                Viz.App.loadPortalDefinition(callback.createDelegate(this));
            },

            /**
             * Open the portalWindows that have the OpenOnStartup set to true.
             */
            openOnStartUpPortalWindows              : function() {
                var store = Viz.Configuration.PortalWindowDefinition;
                var items = [];
                Ext.each(store.getArray(), function(data) {
                            if (data.OpenOnStartup) {
                                this.openModule.defer(1000, this, [{
                                                    xtype           : 'vizPortalPanel',
                                                    KeyPortalWindow : data.KeyPortalWindow,
                                                    title           : Viz.util.Portal.localizeTitle(data.Title),
                                                    iconCls         : data.IconCls.replace('-large-', '-small-')
                                                }]);
                            }
                        }, this);

                this.openOnStartUpPortlets();

            },
            /**
             * Loads and open the OpenOnStartup portlets set to true.
             */
            openOnStartUpPortlets                   : function() {
                Viz.Services.EnergyWCF.Portlet_GetAllOpenOnStartup({
                            success : function(portlets) {
                                if (portlets) {
                                    Ext.each(portlets, function(entity) {
                                                Viz.util.Portal.openDisplay(entity);
                                            }, this);
                                }
                            },
                            scope   : this
                        });
            },

            /**
             * get the array of Button Config to create or update the portal window toolbar.
             */
            getItemsApplicationToolbarPortalWindows : function() {
                var store = Viz.Configuration.PortalWindowDefinition;
                var items = [];
                Ext.each(store.getArray(), function(data) {
                            if (data.DisplayInToolbar) {
                                items.push({
                                            iconCls : data.IconCls,
                                            text    : Viz.util.Portal.localizeTitle(data.Title),
                                            tooltip : Viz.util.Portal.localizeTitle(data.Title),
                                            data    : data,
                                            handler : this.openModule.createDelegate(this, [{
                                                                xtype           : 'vizPortalPanel',
                                                                KeyPortalWindow : data.KeyPortalWindow,
                                                                title           : Viz.util.Portal.localizeTitle(data.Title),
                                                                iconCls         : Ext.isEmpty(data.IconCls) ? Viz.util.Portal.getCssFromImage(data.KeyImage, 16, 16) : data.IconCls.replace('-large-', '-small-')
                                                            }, true])
                                        });
                            }
                        }, this);
                return items;
            },

            /**
             * <p>
             * Returns the Element to be used to contain the child Components of this Container.
             * </p>
             * <p>
             * An implementation is provided which returns the Container's {@link #getEl Element}, but if there is a more complex structure to a Container, this may be overridden to return the element into which the {@link #layout layout} renders child Components.
             * </p>
             * @return {Ext.Element} The Element to render child Components into.
             */
            getLayoutTarget                         : function() {
                var target = this.el;

                if (this.el.id && !this.el.dom) {
                    // Fix for Reporting Services messing with the BODY element dom.
                    target = Ext.get(this.el.id);
                }

                return target;
            },

            /**
             * <p>
             * Returns the outermost Element of this Component which defines the Components overall size.
             * </p>
             * <p>
             * <i>Usually</i> this will return the same Element as <code>{@link #getEl}</code>, but in some cases, a Component may have some more wrapping Elements around its main active Element.
             * </p>
             * <p>
             * An example is a ComboBox. It is encased in a <i>wrapping</i> Element which contains both the <code>&lt;input></code> Element (which is what would be returned by its <code>{@link #getEl}</code> method, <i>and</i> the trigger button Element. This Element is returned as the <code>resizeEl</code>.
             * @return {Ext.Element} The Element which is to be resized by size managing layouts.
             */
            getResizeEl                             : function() {
                var resizeEl = this.resizeEl || this.el;

                if (resizeEl.id && !resizeEl.dom) {
                    // Fix for Reporting Services messing with the BODY element dom.
                    resizeEl = Ext.get(resizeEl.id);
                }

                return resizeEl;

            },

            /**
             * Builds the south region
             * @return {Object}
             */
            buildRegionSouth                        : function(config) {
                if (Viz.DisplayAsDesktop) {
                    return {
                        region    : 'south',
                        height    : 25,
                        style     : 'background-color:rgb(195,195,195)',
                        bodyStyle : 'background-color:rgb(195,195,195)',
                        items     : {
                            // taskbar location
                            html : '<div id="ux-taskbar"><div id="ux-taskbar-start"></div><div id="ux-taskbuttons-panel"></div><div class="x-clear"></div></div>'
                        },
                        border    : false
                    };
                }
                else {
                    var modes = Viz.Configuration.EnumRecords['Vizelia.FOL.BusinessEntities.MainViewPortMode,Vizelia.FOL.Common'];
                    var modeButton = {
                        text    : $lang('msg_enum_mainviewportmode'),
                        iconCls : 'viz-icon-small-mainviewportmode-minimal',
                        menu    : {
                            items : []
                        }
                    }
                    var currentMode = Ext.state.Manager.get('MainViewPortMode');
                    Ext.each(modes, function(mode) {
                                modeButton.menu.items.push({
                                            text           : $img(mode.data.IconCls) + mode.data.Label,
                                            group          : 'mainviewportmode',
                                            checked        : currentMode == mode.data.Value,
                                            mode           : mode.data.Value,
                                            privateHandler : this.changeViewPortMode.createDelegate(this, [mode.data.Value]),
                                            // override to force the checkHandler call even if the menu is already checked.
                                            handleClick    : function(e) {
                                                if (!this.disabled) {
                                                    this.checked = false;
                                                    this.setChecked(true, true); // the second true is to suppress the event
                                                }
                                                Ext.menu.CheckItem.superclass.handleClick.apply(this, arguments);
                                                this.privateHandler();
                                            }
                                        });
                            }, this);

                    if (Viz.util.Fullscreen.isSupported()) {
                        modeButton.menu.items.push('-');
                        modeButton.menu.items.push({
                                    text    : $lang('msg_fullscreen_toggle'),
                                    handler : function() {
                                        Viz.util.Fullscreen.toggle();
                                    }
                                });
                    }
                    return new Ext.ux.StatusBar({
                                region  : 'south',
                                border  : false,
                                style   : 'border-top: 1px #D0D0D0 solid;',
                                height  : 30,
                                margins : '4 0 0 0',
                                style   : 'background-color:rgb(195,195,195)',
                                // bodyStyle : 'background-color:rgb(195,195,195);background-image:none !important',

                                id      : 'statusbar',
                                text    : '',
                                items   : [new Ext.ProgressBar({
                                                    id        : this._chartPreLoadProgressBarId,
                                                    width     : 200,
                                                    hidden    : true,
                                                    style     : 'background-color:rgb(195,195,198);background-image:none !important',
                                                    bodyStyle : 'background-color:rgb(195,195,199);background-image:none !important'
                                                }), '<img src="images/logo_se_180.png"  width="150" style="padding-right:14px;margin-top:-4px" />']
                            });
                }
            },

            /**
             * Preload all charts used in portals into cache.
             */
            loadChartsInCache                       : function() {
                if (Viz.App.Global.User.Preferences.PreloadCharts && $authorized('@@@@ Chart - PreLoad')) {
                    var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                pollingInterval : 1000,
                                serviceStart    : Viz.Services.EnergyWCF.Chart_PreLoadAllBegin,
                                serviceParams   : {},
                                showProgress    : false,
                                progressHandler : function(progressObject) {
                                    var pb = Ext.getCmp(this._chartPreLoadProgressBarId);
                                    if (!pb)
                                        return;
                                    if (pb.hidden)
                                        pb.show();
                                    pb.updateProgress(progressObject.PercentDone / 100, $lang('msg_chart_preloadall') + ': ' + progressObject.PercentDone + ' %');

                                },
                                resultHandler   : function() {
                                    var pb = Ext.getCmp(this._chartPreLoadProgressBarId);
                                    if (!pb)
                                        return;
                                    pb.hide();
                                },
                                scope           : this
                            });
                    submitter.start();
                }
            },

            changeViewPortMode                      : function(mode) {
                /*
                 * var mode = 0; var bt = Ext.getCmp(this._modeButtonId); Ext.each(bt.menu.items.items, function(item) { if (item.checked) mode = item.mode; }, this); if (mode < 0 || mode > 3) return;
                 */
                switch (mode) {
                    // Compact
                    case 1 :
                        if (Ext.getCmp(this._northRegionId).collapsed == false)
                            Ext.getCmp(this._northRegionId).collapse();
                        if (Ext.getCmp(this._westRegionId).collapsed == true)
                            Ext.getCmp(this._westRegionId).expand();
                        break;
                    // Extended
                    case 2 :
                        if (Ext.getCmp(this._northRegionId).collapsed == true)
                            Ext.getCmp(this._northRegionId).expand();
                        if (Ext.getCmp(this._westRegionId).collapsed == false)
                            Ext.getCmp(this._westRegionId).collapse();
                        break;
                    // Full
                    case 3 :
                        if (Ext.getCmp(this._northRegionId).collapsed == true)
                            Ext.getCmp(this._northRegionId).expand();
                        if (Ext.getCmp(this._westRegionId).collapsed == true)
                            Ext.getCmp(this._westRegionId).expand();
                        break;
                    // Minimal
                    case 0 :
                    default :
                        if (Ext.getCmp(this._northRegionId).collapsed == false)
                            Ext.getCmp(this._northRegionId).collapse();
                        if (Ext.getCmp(this._westRegionId).collapsed == false)
                            Ext.getCmp(this._westRegionId).collapse();
                        break;
                }
                Ext.state.Manager.set('MainViewPortMode', mode);
            },
            /**
             * Builds the center region.
             * @return {Object}
             */
            buildRegionCenter                       : function(config) {
                if (Viz.DisplayAsDesktop) {
                    return {
                        region : 'center',
                        xtype  : 'container',
                        autoEl : {},
                        layout : 'fit',
                        border : false,
                        html   : '<div id="x-desktop" ><img src="images/desktop.jpg" style="height:100%;width:100%" alt=""> <dl id="x-shortcuts"></dl></div>'
                    };
                }
                else {
                    this.setDesktopShortcutTextColor(Viz.App.Global.User.Preferences.DesktopTextColor);
                    var background = Viz.App.Global.User.Preferences.DesktopBackground || Viz.util.Desktop.getBackgroundUrl(Viz.App.Global.User.Preferences.CustomDesktopBackground) || '';
                    var deskHtml = this.getDesktopHtml(background);
                    return {
                        region : 'center',
                        xtype  : 'container',
                        autoEl : {},
                        layout : 'fit',
                        border : false,
                        items  : {
                            xtype  : 'panel',
                            border : false,
                            layout : 'fit',
                            items  : {
                                xtype           : 'tabpanel',
                                enableTabScroll : true,
                                activeTab       : 0,
                                id              : this.getMainTabId(),
                                items           : [{
                                            title     : $lang('msg_desktop'),
                                            iconCls   : 'viz-icon-small-desktop',
                                            // title : '&nbsp;',
                                            id        : this._desktopIconsId,
                                            html      : deskHtml,
                                            listeners : {
                                                activate   : function(p) {
                                                    p.activated = true;
                                                    if (Viz.App.Global.IconManager) {
                                                        Viz.App.Global.IconManager.initDesktopIconsConfiguration();
                                                        if (Ext.getCmp(this._unpinButton) && Ext.getCmp(this._unpinButton).pressed)
                                                            Viz.App.Global.IconManager.enableIconsManager();
                                                    }
                                                },
                                                deactivate : function(p) {
                                                    p.activated = false;
                                                    if (Viz.App.Global.IconManager) {
                                                        Viz.App.Global.IconManager.disableIconsManager();
                                                    }
                                                },
                                                render     : function(p) {
                                                    Ext.get(p.id).on('contextmenu', function(event) {
                                                                this.displayDesktopMenu(event);
                                                            }, this);
                                                },
                                                scope      : this
                                            }
                                        }],
                                plugins         : [{
                                            ptype : 'vizDragDropTabs'
                                        }],
                                listeners       : {
                                    contextmenu : function(tabpanel, panel, event) {
                                        if (panel.activated && panel.id == this._desktopIconsId) {
                                            this.displayDesktopMenu(event);
                                        }
                                    },
                                    scope       : this
                                }
                            }
                        }
                    };
                }
            },

            /**
             * Sets the Desktop shortcut text color
             */
            setDesktopShortcutTextColor             : function(color) {
                Ext.util.CSS.updateRule('.x-icon-desktop-shortcut-text', 'color', '#' + (color || '000000'));
            },

            /**
             * Sets the Desktop Background
             * @param {string} image : the image name and extension (in the images/desktop folder)
             */
            setDesktopBackground                    : function(image) {
                var deskHtml = this.getDesktopHtml(image);
                Ext.getCmp(Viz.App.Global.ViewPort._desktopIconsId).body.update(deskHtml);
            },

            /**
             * Sets the Desktop Logo
             */
            setDesktopLogo                          : function() {
                Ext.get(Viz.App.Global.ViewPort._desktopLogoId).dom.src = this.getDesktopLogoUrl();
            },

            getDesktopHtml                          : function(background) {
                var desktopImageStyle = Ext.isEmpty(background) ? '' : 'background-image:url(\'images/desktop/' + background + '\');background-size: 100% 100%;';
                var deskHtml = '<div id="' + this._desktopBackgroundId + '" style="width:100%;height:100%;' + desktopImageStyle + '">' + '<table style="height:100%;width:100%;"><tbody>' + '<tr style="height:100%"><td><dl id="x-shortcuts"></dl></td></tr>' + '<tr style="height:150px"><td><center><div>' + '<img id="' + this._desktopLogoId + '" src="' + this.getDesktopLogoUrl() + '"/>' + '</div></center></td></tr>' + '</tbody></table>' + '</div>';
                return deskHtml;
            },

            getDesktopLogoUrl                       : function() {
                var url = Viz.Services.BuildUrlStream("Core.svc", 'Logo_GetStreamImage', {}, false);
                return url;
            },

            /**
             * Display the menu (right click on an empty element of the desktop or on the desktop tab).
             * @param {} event
             */
            displayDesktopMenu                      : function(event) {
                // var unpinVisible = Ext.getCmp(this._northRegionId).tools['unpin'].isVisible();
                var unpinButton = Ext.getCmp(this._unpinButton);  // is null when !$authorized('@@@@ Portal - Full Control')
                var unpinVisible = unpinButton && unpinButton.pressed;
                event.stopEvent();
                var ctxMenu = new Ext.menu.Menu({
                            items : [{
                                        iconCls : 'viz-icon-small-portalwindow',
                                        text    : $lang('msg_portalwindow_wizard'),
                                        hidden  : !$authorized('@@@@ Portal - Full Control') && !$authorized('@@@@ Portal - Wizard'),
                                        handler : Viz.grid.PortalWindow.launchWizard
                                    }, new Ext.menu.Separator({
                                                hidden : !$authorized('@@@@ Portal - Full Control')
                                            }), {
                                        text    : !unpinVisible ? $lang("msg_unpin_desktopicons") : $lang("msg_pin_desktopicons"),
                                        iconCls : !unpinVisible ? 'viz-icon-small-unpin' : 'viz-icon-small-pin',
                                        hidden  : !$authorized('@@@@ Portal Icons - Full Control'),
                                        handler : function() {
                                            var bt = Ext.getCmp(this._unpinButton);
                                            if (bt) {
                                                bt.toggle(undefined, true);
                                                this.unpinDesktopIcons(bt);
                                            }
                                        },
                                        scope   : this
                                    }, {
                                        iconCls : 'viz-icon-small-cancel',
                                        text    : $lang('msg_viewport_closeopentabs'),
                                        handler : this.onCloseOpenTabs,
                                        scope   : this
                                    }, {
                                        iconCls : 'viz-icon-small-refresh',
                                        text    : $lang('msg_refresh'),
                                        handler : function() {
                                            this.updateApplicationToolbarPortalWindows();
                                        },
                                        scope   : this
                                    }]
                        });
                ctxMenu.on("hide", function(menu) {
                            menu.destroy();
                            menu = null;
                        }, this);
                ctxMenu.showAt(event.getXY());
            },

            /**
             * Return the container for all desktop icons.
             * @return {}
             */
            getDesktopIconsContainer                : function() {
                return Ext.getCmp(this._desktopIconsId);
            },

            /**
             * Builds the east region.
             * @return {Object}
             */
            buildRegionEast                         : function(config) {
                return {
                    id           : 'eastpanel',
                    region       : 'east',
                    stateId      : 'MainViewportRegionEast',
                    hidden       : Viz.DisplayAsDesktop,
                    header       : false, // for version > 3.0.0 this is needed to hide the collapse button
                    width        : 300,
                    split        : true,
                    baseCls      : 'x-plain',
                    collapsible  : true,
                    layout       : 'anchor',
                    autoScroll   : true,
                    collapseMode : 'mini',
                    collapsed    : true,
                    defaults     : {
                        style     : 'margin-bottom:3px;',
                        margins   : '5 0 0 0',
                        collapsed : true
                    },
                    items        : [{
                                xtype  : 'vizFormUserInfo',
                                anchor : '-5'
                            }]
                };
            },

            /**
             * Builds the west region
             * @return {Object}
             */
            buildRegionWest                         : function(config) {
                return {
                    region           : 'west',
                    stateId          : 'MainViewportRegionWest',
                    id               : this._westRegionId,
                    width            : 350,
                    hidden           : Viz.DisplayAsDesktop,
                    split            : true,
                    bodyBorder       : true,
                    frame            : false,
                    collapsible      : false,
                    collapseMode     : 'mini',
                    layout           : 'fit',
                    border           : true,
                    // margins : '10 10 10 20',
                    hideCollapseTool : true,
                    collapsed        : true,
                    bodyStyle        : 'padding:0px 0px 0px 0px;border :none;border-top:1px #D0D0D0 solid',
                    xtype            : 'panel',
                    layout           : 'accordion',
                    bodyBorder       : true,
                    minWidth         : 240,
                    defaults         : {
                        // bodyStyle : 'padding:4px',
                        stateEvents : ["collapse", "expand"],
                        getState    : function() {
                            return {
                                collapsed : this.collapsed
                            };
                        }
                    },
                   
                    layoutConfig     : {
                        animate : true
                    },
                    listeners    : {
                        collapse : this.initGlobalGrid,
                        expand : this.initGlobalGrid,
                        scope  : this
                    },
                    items            : [{
                                id                    : this._treeSpatial,
                                xtype                 : 'vizTreeSpatial',
                                hidden                : !$authorized('@@@@ Spatial Hierarchy Tree'),
                                stateId               : 'MainViewPortVizTreeSpatial',
                                enableDD              : $authorized('@@@@ Spatial Hierarchy Tree - Full Control'),
                                enableDrag            : true,
                                containerScroll       : true,
                                autoScroll            : true,
                                ddGroup               : 'SpatialDD',
                                serviceParams         : {
                                    flgFilter : true
                                },
                                displayCheckbox       : true,
                                checkboxExcludedTypes : ['Vizelia.FOL.BusinessEntities.Meter', 'Vizelia.FOL.BusinessEntities.AlarmDefinition'],
                                bbar                  : ['->', {
                                            text    : $lang('msg_portaltab_apply_spatialfilter'),
                                            hidden  : !$authorized('@@@@ Apply Spatial Filter'),
                                            iconCls : 'viz-icon-small-apply',
                                            handler : this.applySpatialFilterFromTree,
                                            scope   : this
                                        }],
                                collapsed             : false,
                                listeners: {
                                    expand: this.writeWestAccordionToGoogleAnalytics,
                                    scope: this
                                },
                            }, {
                                xtype      : 'vizTreeMeterClassification',
                                stateId    : 'MainViewPortVizTreeMeterClassification',
                                title      : $lang('msg_meter_classifications'),
                                collapsed  : true,
                                enableDrag : true,
                                hidden     : !$authorized('@@@@ Chart - Full Control') && !$authorized('@@@@ Meter Classification Tree'),
                                ddGroup: 'ClassificationDD',
                                listeners: {
                                    expand: this.writeWestAccordionToGoogleAnalytics,
                                    scope: this
                                },
                            }, {
                                hidden     : !$authorized('@@@@ Portlets Tree'),
                                stateId    : 'MainViewPortVizTreePortlet',
                                collapsed  : true,
                                enableDrag : true,
                                xtype      : 'vizTreePortlet',
                                ddGroup: 'PortletDD',
                                listeners: {
                                    expand: this.writeWestAccordionToGoogleAnalytics,
                                    scope: this
                                },
                            }, {
                                xtype     : 'vizTreeAzManFilter',
                                stateId   : 'MainViewPortVizTreeAzManFilter',
                                hidden    : !$authorized('@@@@ Project Hierarchy'),
                                collapsed: true,
                                listeners: {
                                    expand: this.writeWestAccordionToGoogleAnalytics,
                                    scope: this
                                },
                            }, /*
                                 * { xtype : 'vizTreeAzManRole', stateId : 'MainViewPortVizTreeAzManRole', hidden : !$authorized('@@@@ Role'), collapsed : true },
                                 */{
                                xtype     : 'vizTreePset',
                                stateId   : 'MainViewPortVizTreePset',
                                hidden    : !$authorized('@@@@ Pset'),
                                collapsed: true,
                                listeners: {
                                    expand: this.writeWestAccordionToGoogleAnalytics,
                                    scope: this
                                },
                            }, {
                                xtype     : 'vizGridPsetList',
                                stateId   : 'MainViewPortVizGridPsetList',
                                border    : true,
                                bodyStyle : 'padding:0px',
                                hidden    : !$authorized('@@@@ Pset'),
                                collapsed: true,
                                listeners: {
                                    expand: this.writeWestAccordionToGoogleAnalytics,
                                    scope: this
                                },
                            }, {
                                xtype     : 'vizTreeClassificationItem',
                                stateId   : 'MainViewPortVizTreeClassificationItem',
                                enableDD  : true,
                                hidden    : !$authorized('@@@@ Classification Item'),
                                collapsed: true,
                                listeners: {
                                    expand: this.writeWestAccordionToGoogleAnalytics,
                                    scope: this
                                },
                            }, {
                                xtype     : 'vizTreeOrganization',
                                stateId   : 'MainViewPortVizTreeOrganization',
                                enableDD  : true,
                                hidden    : !$authorized('@@@@ Organization'),
                                collapsed: true,
                                listeners: {
                                    expand: this.writeWestAccordionToGoogleAnalytics,
                                    scope: this
                                },
                            }, {
                                stateId   : 'MainViewPortVizTreeReport',
                                xtype     : 'vizTreeReport',
                                collapsed : true,
                                hidden: !$authorized('@@@@ Reports'),
                                listeners: {
                                    expand: this.writeWestAccordionToGoogleAnalytics,
                                    scope: this
                                },
                            }, {
                                xtype     : 'vizGridEntityCount',
                                stateId   : "MainViewPortVizGridEntityCount",
                                border    : true,
                                bodyStyle : 'padding:0px',
                                hidden    : !$authorized('@@@@ EntityCount'),
                                collapsed: true,
                                listeners: {
                                    expand: this.writeWestAccordionToGoogleAnalytics,
                                    scope: this
                                },
                            }]

                };
            },
            initGlobalGrid: function() {
                this.updateApplicationToolbarPortalWindows.defer(50, this);
            },

            /**
             * Returns the MainTab
             * @return {}
             */
            getMainTab                              : function() {
                return Ext.getCmp(this.getMainTabId());
            },

            getMainTabId                            : function() {
                return this._mainTabId;
            },
            

            openModuleWithAnalytics                 : function (params, activateAnalytics, scope) {
                this.openModule(params);
                activateAnalytics.apply(scope);
            },

            /**
             * Opens a module and make it active.
             * @param {String/Object} xtype The xtype of the module to open or the configuration of the tab to open.
             * @param {bool] unique (Optional) if <T>True</T> check if another tab with the same config exists and doesnt display it again. We return true if a new tab has been created, false if we juste activated a tab.
             */
            openModule                              : function(xtype, unique, activate) {
                /** @type Ext.TabPanel */
                var retVal = false;
                var mainTab = this.getMainTab();
                var configTab = {};
                if (Ext.isObject(xtype))
                    Ext.apply(configTab, xtype);
                else
                    Ext.apply(configTab, {
                                xtype : xtype
                            });

                if (Viz.DisplayAsDesktop) {
                    var win = Viz.App.Global.Desktop.createWindow(configTab);
                    win.show();
                }
                else {
                    var tab = null;

                    if (unique) {
                        mainTab.items.each(function(item) {
                                    var found = true;
                                    for (var i in configTab) {
                                        found = found && configTab[i] == item.initialConfig[i];
                                    }
                                    if (found)
                                        tab = item;
                                }, this)
                    }

                    if (!tab) {
                        tab = mainTab.add(configTab);
                        retVal = true;
                    }
                    if (activate !== false)
                        mainTab.setActiveTab(tab.id);
                }
                // We return true if a new tab has been created, false if we juste activated a tab.
                return retVal;
            },

            /**
             * open or activate a specific portal tab.s
             * @param {} KeyPortalTab
             */
            openOrActivatePortalTab                 : function(KeyPortalTab) {
                Viz.Services.EnergyWCF.PortalTab_GetItem({
                            Key     : KeyPortalTab,
                            /**
                             * @param {Viz.BusinessEntity.PortalTab} entityPortalTab
                             */
                            success : function(entityPortalTab) {
                                var mainTab = this.getMainTab();
                                var portalPanel = null;
                                var portalTab = null;

                                mainTab.items.each(function(item) {
                                            if (item.getXType() == 'vizPortalPanel' && item.KeyPortalWindow == entityPortalTab.KeyPortalWindow) {
                                                portalPanel = item;
                                            }
                                        }, this);

                                // If the parent portal is already open, we activate the correct Tab.
                                if (portalPanel) {
                                    mainTab.setActiveTab(portalPanel.id);
                                    portalPanel.setActivePortalTab(entityPortalTab.KeyPortalTab);
                                }
                                // else we need to open the portal and set the activetab.
                                else {
                                    this.openModule({
                                                xtype               : 'vizPortalPanel',
                                                KeyPortalWindow     : entityPortalTab.KeyPortalWindow,
                                                title               : entityPortalTab.PortalWindowTitle,
                                                iconCls             : entityPortalTab.PortalWindowIconCls.replace('-large-', '-small-'),
                                                KeyInitialActiveTab : entityPortalTab.KeyPortalTab
                                            });
                                }
                            },
                            scope   : this
                        });
            },

            closePortalWindow                       : function(KeyPortalWindow) {
                var mainTab = this.getMainTab();
                var portalPanel = null;

                mainTab.items.each(function(item) {
                            if (item.getXType() == 'vizPortalPanel' && item.KeyPortalWindow == KeyPortalWindow) {
                                portalPanel = item;
                            }
                        }, this);
                if (portalPanel) {
                    mainTab.remove(portalPanel, true);
                }
            },

            /**
             * Check on the server side if any new alarminstance has been created in the last 30 sec. should be replaced by Comet
             */
            checkAlarmInstanceNotifications         : function() {
                Viz.Services.EnergyWCF.AlarmInstance_GetLatest({
                            seconds : this._checkAlarmInstanceNotificationsTask.interval / 1000,
                            success : function(alarmInstances) {
                                if (Ext.isArray(alarmInstances) && alarmInstances.length > 0) {
                                    new Ext.ux.Notification({
                                                iconCls     : 'viz-icon-small-alarm',
                                                title       : $lang('msg_alarminstance'),
                                                html        : String.format('{0} {1}', alarmInstances.length, $lang('msg_alarminstance_getlatestbody')),
                                                autoDestroy : true,
                                                hideDelay   : 2000
                                            }).show(document);
                                }
                            },
                            failure : function() {
                                Ext.TaskMgr.stop(this._checkAlarmInstanceNotificationsTask);
                            },
                            scope   : this
                        });

            },

            /**
             * Apply the spatial filter selection to the activate Portal Panel.
             */
            applySpatialFilterFromTree          : function () {
                this.writeEventToGoogleAnalytics(this._regionWestName, 'click', 'applySpatialFilter');
                var mainTab = this.getMainTab();
                var activeTab = mainTab.getActiveTab();

                if (activeTab.getXType() == 'vizPortalPanel') {
                    var filterSpatial = this.getCurrentSpatialFilter();
                    if (filterSpatial.length > 0) {
                        activeTab.el.mask();

                        Viz.Services.EnergyWCF.PortalTab_ApplySpatialFilter({
                                    Key           : activeTab.getActiveKeyPortalTab(),
                                    filterSpatial : filterSpatial,
                                    success       : function(response) {
                                        activeTab.el.unmask();
                                        activeTab.refreshActiveTabPortlets();
                                    },
                                    scope         : this
                                });
                    }
                }
            },

            /**
             * Returns an array of the selected locations.
             * @return {Array}
             */
            getCurrentSpatialFilter                 : function() {
                var filterSpatial = new Array();
                var tree = Ext.getCmp(this._treeSpatial);
                var checkedNodes = tree.getChecked();
                if (checkedNodes.length > 0) {
                    Ext.each(checkedNodes, function(node) {
                                filterSpatial.push(node.attributes.entity);
                            }, this);
                }
                return filterSpatial;
            },

            onLogOut                                : function() {
                Ext.MessageBox.confirm($lang('msg_application_title'), $lang("msg_logout_confirmation"), function(id) {
                            if (id == "yes") {
                                Ext.getBody().mask(true);
                                Viz.Services.AuthenticationWCF.Logout({
                                            success : function() {
                                                window.location.reload();
                                            }
                                        });
                            }
                        }, this);
            },
            /**
             * Locks the active tab drop targets.
             */
            activeTabLockDropTargets                : function() {
                var mainTab = this.getMainTab();
                var activeTab = mainTab.getActiveTab();
                if (activeTab.getXType() == 'vizPortalPanel') {
                    activeTab.lockDropTargets();
                }
            },
            /**
             * Unlocks the active tab drop targets.
             */
            activeTabUnlockDropTargets              : function() {
                var mainTab = this.getMainTab();
                var activeTab = mainTab.getActiveTab();
                if (activeTab.getXType() == 'vizPortalPanel') {
                    activeTab.unlockDropTargets();
                }
            },

            /**
             * Close all the open Tabs.
             */
            onCloseOpenTabs                         : function() {
                var mainTab = this.getMainTab();
                mainTab.items.each(function(item) {
                            if (item.closable) {
                                if (item.fireEvent('beforeclose', item) !== false) {
                                    item.fireEvent('close', item);
                                    mainTab.remove(item);
                                }
                            }
                        }, this);

            },
            
            writeWestAccordionToGoogleAnalytics             : function (pnl) {
                this.writeEventToGoogleAnalytics(this._regionWestName, 'click', pnl.title);
            },

            writeEnergyDashBoardEventsToGoogleAnalytics           : function(label) {
                this.writeEventToGoogleAnalytics(this._dashboardName, 'click', label);
            },

            writeEventToGoogleAnalytics             : function (category, action, label) {
                Viz.writeEventToGoogleAnalytics(category, action, label);
            }

        });
Ext.reg('vizMainViewPort', Viz.MainViewPort);

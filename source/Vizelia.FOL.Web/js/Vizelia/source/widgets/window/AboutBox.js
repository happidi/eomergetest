﻿Ext.namespace("Viz.window");

/**
 * The AboutBox window. Usage:
 * 
 * <pre><code>
 * new Viz.window.AboutBox({
 *             version : version,
 *             toolEl  : toolEl
 *         }).show();
 * </code></pre>
 * 
 * @class Viz.window.AboutBox
 * @extends Ext.Window
 * @xtype vizWindowAboutBox
 */
Viz.window.AboutBox = Ext.extend(Ext.Window, {

            /**
             * Constructor.
             * @param {Object} config The configuration contains version and toolEl properties.
             */
            constructor       : function(config) {

                var extIEBrowserVersion = ", Extjs IE: ";

                if (Ext.isIE6) {
                    extIEBrowserVersion += "6,";
                }

                if (Ext.isIE7) {
                    extIEBrowserVersion += "7,";
                }

                if (Ext.isIE8) {
                    extIEBrowserVersion += "8,";
                }

                if (Ext.isIE9) {
                    extIEBrowserVersion += "9,";
                }

                var ieDocumentMode = this.getIEDocumentMode();

                var browserVersion = window.navigator.userAgent + (Ext.isIE ? (extIEBrowserVersion + ' DocumentMode: ' + ieDocumentMode) : '');

                Ext.apply(this, {
                            iconCls       : 'viz-icon-small-about',
                            title         : $lang('msg_application_title') + ' ' + Viz.AssemblyVersion,
                            animateTarget : config.toolEl,
                            width         : 400,
                            height        : 350,
                            closable      : true,
                            resizable     : false,
                            modal         : true,
                            plain         : true,
                            border        : true,
                            layout        : 'border',
                            items         : [{
                                        region  : 'north',
                                        height  : 120,
                                        border  : false,
                                        plain   : true,
                                        baseCls : 'viz-about-logo'
                                    }, {
                                        region    : 'center',
                                        border    : false,
                                        bodyStyle : 'padding: 5px',
                                        // margins : '5 5 5 5',
                                        html      : String.format(($lang('msg_application_about_text_title') + '<div dir="ltr">Culture: {1}<br/>.NET: {2}<br/>Extjs: {3}<br/>Vizjs: {4}<br/>Browser: {5}.<br/></div>'), $lang('msg_application_title'), config.version.Culture, config.version.CLR, Ext.version, Viz.version, browserVersion)
                                    }]
                        });
                Viz.window.AboutBox.superclass.constructor.call(this);
            },

            getIEDocumentMode : function() {
                var engine = null;

                if (window.navigator.appName == "Microsoft Internet Explorer") {
                    // This is an IE browser. What mode is the engine in?
                    if (document.documentMode) // IE8 or later
                        engine = document.documentMode;
                    else // IE 5-7
                    {
                        engine = 5; // Assume quirks mode unless proven otherwise
                        if (document.compatMode) {
                            if (document.compatMode == "CSS1Compat")
                                engine = 7; // standards mode
                        }
                        // There is no test for IE6 standards mode because that mode
                        // was replaced by IE7 standards mode; there is no emulation.
                    }
                }

                return engine;
            }

        });
Ext.reg('vizWindowAboutBox', Viz.window.AboutBox);
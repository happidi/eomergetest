﻿Ext.namespace('Viz.window');

/**
 * @class Viz.window.Occupant
 * @extends Ext.Window A window containing a grid occupant. Uses <tt>Viz.Services.TestWCF.GetOccupant</tt> as a service handler.
 * 
 * <pre><code>
 * new Viz.window.Occupant().show();
 * </code></pre>
 * 
 * @xtype vizWindowOccupant
 */
Viz.window.Occupant = Ext.extend(Ext.Window, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    title  : 'Grid Occupant',
                    width  : 500,
                    height : 300,
                    layout : 'fit',
                    items  : [{
                                xtype   : 'vizGrid',
                                stateId : 'vizWindowOccupant.gridOccupant',
                                store   : new Viz.store.Occupant({
                                           // serviceHandler : Viz.Services.TestWCF.GetOccupant
                                        }),
                                columns : [{
                                            dataIndex : 'Id',
                                            header    : $lang("msg_occupant_id"),
                                            sortable  : true
                                        }, {
                                            dataIndex : 'FirstName',
                                            header    : $lang('msg_occupant_firstname'),
                                            sortable  : true
                                        }, {
                                            dataIndex : 'LastName',
                                            header    : $lang('msg_occupant_lastname'),
                                            sortable  : true
                                        }]
                            }]
                };
                var forcedConfig = {

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.window.Occupant.superclass.constructor.call(this, config);
            }
        });

Ext.reg('vizWindowOccupant', Viz.window.Occupant);

﻿Ext.namespace("Viz.window");

/**
 * @class Viz.window.LoginAsUser
 * @extends Ext.Window The Change password window. Usage:
 * 
 * <pre><code>
 * new Viz.window.LoginAsUser().show();
 * </code></pre>
 * 
 * @xtype vizWindowLoginAsUser
 */
Viz.window.LoginAsUser = Ext.extend(Ext.Window, {

    /**
    * Constructor.
    * @param {Object} config The configuration contains version and toolEl properties.
    */
    constructor: function (config) {
        config = config || {};
        // id of the window.
        this._id = Ext.id();
        // id of the main form.
        this._formid = Ext.id();
        // id of the password field.
        this._fieldpasswordId = Ext.id();
        //id of the username field
        this._formUserId = Ext.id();
        var username = config.username || ((Viz.App && Viz.App.Global && Viz.App.Global.User) ? Viz.App.Global.User.UserName : null);
        var applicationName = config.applicationName || ((Viz.App && Viz.App.Global && Viz.App.Global.User) ? Viz.App.Global.User.ApplicationName : null);

        var defaultConfig = {
            id: this._id,
            title: $lang('msg_loginasuser'),
            iconCls: 'viz-icon-small-loginasuser',
            width: 450,
            height: 120,
            layout: 'fit',
            modal: true,
            xtype: 'window',
            items: [{
                xtype: 'vizForm',
                mode: 'create',
                labelWidth: 200,
                id: this._formid,
                applyEnabled: false,
                closeParentWindow: function (force) {
                    var parentWin = this.getParentWindow();
                    parentWin.close();
                },
                serviceHandler: Viz.Services.AuthenticationWCF.Login_LoginAsUser,
                items: [{
                    fieldLabel: $lang('msg_login'),
                    name: 'userName',
                    id: this._formUserId,
                    value: applicationName + '\\' + username,
                    allowBlank: false,
                    hidden: username ? true : false,
                    hideLabel: username ? true : false
                }, {
                    fieldLabel: $lang('msg_password'),
                    name: 'password',
                    allowBlank: false,
                    inputType: 'password'
                }, {
                    fieldLabel: $lang('msg_loginasuser'),
                    id: this._fieldpasswordId,
                    name: 'userNameLoginAs',
                    allowBlank: false
                }],
                listeners: {
                    savesuccess: function () {
                        window.location.reload();
                    },
                    savefailure: function (result) {
                        Ext.MessageBox.show(result);
                        /*Ext.MessageBox.show({
                        title: $lang('msg_application_title'),
                        msg: $lang("error_msg_invalid_authentication"),
                        width: 350,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                        });
                        return false;*/
                    }
                }
            }]
        };

        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.window.LoginAsUser.superclass.constructor.call(this, config);
    },

    /**
    * @private Redirect the event savesuccess to the window.
    */
    onRender: function () {
        Viz.window.LoginAsUser.superclass.onRender.apply(this, arguments);
        this.relayEvents(Ext.getCmp(this._formid), ['savesuccess']);
    }

});

Ext.reg('vizWindowLoginAsUser', Viz.window.LoginAsUser);

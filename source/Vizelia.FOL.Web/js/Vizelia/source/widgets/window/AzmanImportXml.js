﻿Ext.namespace("Viz.window");

/**
 * @class Viz.window.ImportXml
 * @extends Ext.Window The importXml window. Usage:
 * 
 * <pre><code>
 * new Viz.window.ImportXml().show();
 * </code></pre>
 * 
 * @xtype vizWindowImportXml
 */
Viz.window.AzmanImportXml = Ext.extend(Ext.Window, {

    /**
    * Constructor.
    * @param {Object} config The configuration contains version and toolEl properties.
    */
    constructor: function (config) {
        config = config || {};
        // id of the window.
        this._id = Ext.id();
        // id of the main form.
        this._formid = Ext.id();

        this._fileUploadFieldId = Ext.id();
        this._submitButtonId = Ext.id();
        this._azmanImportOptionsId = Ext.id();

        if (config.serviceHandler) {
            var serviceHandlerForFile = config.serviceHandler;
        }
        else {
            var serviceHandlerForFile = Viz.Services.MappingWCF.BeginMapping;
        }

        var defaultConfig = {
            id: this._id,
            title: $lang('msg_azman_import_file'),
            iconCls: 'viz-icon-small-xml',
            width: 400,
            height: 155,
            layout: 'fit',
            items: [{
                xtype: 'vizForm',
                waitMsgTarget: this._id,
                labelWidth: 100,
                id: this._formid,
                autoScroll: true,
                isUpload: true,
                closeonsave: true,
                isLongRunningSubmit: true,
                longRunningSubmitConfig: {
                    isFileResult: true,
                    progressWindowTitle: $lang('msg_azman_import_file'),
                    maxPollingTime: 60 * 60 * 1000 // one hour
                },
                serviceHandler: serviceHandlerForFile,
                items: [{
                    id: this._fileUploadFieldId,
                    fieldLabel: $lang('msg_filename'),
                    name: 'InputFile',
                    allowBlank: false,
                    xtype: 'fileuploadfield',
                    buttonText: '',
                    buttonCfg: {
                        iconCls: 'viz-icon-small-xml'
                    },
                    regex: /^.*\.(xml|XML)$/,
                    regexText: String.format($lang('error_msg_invalid_file_extension'), 'XML')
                }, {
                    id: this._azmanImportOptionsId,
                    fieldLabel: $lang('msg_azmanimport_options'),
                    name: 'ImportOptions',
                    xtype: 'vizComboEnum',
                    enumTypeName: 'Vizelia.FOL.BusinessEntities.AzmanImportOptions, Vizelia.FOL.Common',
                    allowBlank: false

                }],
                buttons: [{
                    text: $lang('msg_application_upload'),
                    id: this._submitButtonId,
                    scope: this,
                    iconCls: 'viz-icon-small-upload',
                    handler: function () {
                        var options = Ext.getCmp(this._azmanImportOptionsId);
                        if (options.value == 2) {
                            Ext.MessageBox.show({
                                title: $lang('msg_azman_import'),
                                msg: $lang('msg_azman_import_warning'),
                                buttons: Ext.MessageBox.OKCANCEL,
                                fn: function (button) {
                                    if (button !== 'ok') {
                                        return;
                                    }
                                    this.save();
                                },
                                scope: this
                            })
                        }
                        else {
                            this.save();
                        }
                    },
                    scope: this,
                    disabled: false
                }]
            }]
        };

        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        // Create a regex based on the list of extensions.

        // var submitButton = Ext.getCmp(this._submitButtonId);
        // submitButton.enable();

        Viz.window.AzmanImportXml.superclass.constructor.call(this, config);
    },

    save: function () {
        var form = Ext.getCmp(this._formid);
        form.saveItem();
    }
});

Ext.reg('vizWindowAzmanImportXml', Viz.window.AzmanImportXml);
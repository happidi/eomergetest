Ext.namespace("Viz.window");
/**
 * The login window. Usage :
 * 
 * <pre><code>
 * var winLogin = new Viz.window.Login();
 * </code></pre>
 * 
 * @class Viz.window.Login
 * @extends Ext.util.Observable
 * @xtype vizWindowLogin
 */
Viz.window.Login = Ext.extend(Ext.util.Observable, {

            /**
             * Ctor
             */
            constructor         : function(config) {
                this._authenticationProviderId = Ext.id();
                this._rememberMeId = Ext.id();
                this._iframeloginpostbackId = 'iframeloginpostback';
                this._authusernameId = 'AuthUsername';
                this._authpasswordId = 'AuthPassword';
                this._authsubmitId = 'auth-submit';
                this._authbuttontId = 'auth-button';
                this._authforgotpasswordId = 'auth-forgotpassword';

                if ($lang('langue_direction') == 'rtl') {
                    this._propX = 10;
                    this._propDir = 'right';
                    this._propDirReverse = 'left';
                    this._propButtonPadding = "padding-right:690px";
                }
                else {
                    this._propX = 500;
                    this._propDir = 'left';
                    this._propDirReverse = 'right';
                    this._propButtonPadding = "padding-left:685px";

                }
                Viz.window.Login.superclass.constructor.apply(this, arguments);

                // Viz.util.MessageBusMgr.subscribe("DoLogin", this.fnSubmitStandard, this);

                this.addEvents(
                        /**
                         * @event disable Fires after the component is disabled.
                         */
                        'beforelogin',
                        /**
                         * @event success Fires after the component is enabled.
                         */
                        'success',
                        /**
                         * @event failre Fires after the component is enabled.
                         */
                        'failure');

                this.oForm = new Ext.form.FormPanel({
                            id             : 'newform',
                            method         : 'post',
                            labelWidth     : 115,
                            standardSubmit : true,
                            x              : this._propX,
                            y              : 78,
                            width          : 370,
                            bodyStyle      : 'background: transparent;padding:5px;',
                            border         : false,
                            listeners      : {

                                beforerender : function(form) {
                                    form.bodyCfg.action = 'blank.aspx';
                                    form.bodyCfg.target = this._iframeloginpostbackId;
                                },
                                afterrender  : this.addLoginResultFrame,
                                scope        : this
                            },
                            items          : [{
                                        anchor     : '-20',
                                        xtype      : 'textfield',
                                        fieldLabel : $lang("msg_username"),
                                        id         : this._authusernameId,
                                        name       : this._authusernameId,
                                        allowBlank : false,
                                        applyTo    : this._authusernameId,
                                        autoShow   : true
                                    }, {

                                        anchor     : '-20',
                                        xtype      : 'textfield',
                                        allowBlank : false,
                                        fieldLabel : $lang("msg_password"),
                                        name       : this._authpasswordId,
                                        applyTo    : this._authpasswordId,
                                        id         : this._authpasswordId,
                                        allowBlank : false,
                                        autoShow   : true,
                                        minLength  : 1
                                    }, {
                                        id             : this._authenticationProviderId,
                                        hidden         : config.shouldHideAuthenticationCombo,
                                        xtype          : 'vizComboAuthenticationProvider',
                                        anchor         : '-20',
                                        name           : 'provider',
                                        authpasswordId : this._authpasswordId,
                                        authusernameId : this._authusernameId,
                                        authbuttontId  : this._authbuttontId
                                    }, {
                                        xtype      : 'vizcheckbox',
                                        fieldLabel : $lang("msg_remember_me"),
                                        anchor     : '-20',
                                        id         : this._rememberMeId,
                                        value      : false,
                                        checked    : false,
                                        hidden     : !Viz.ApplicationSettings.AllowRememberMe
                                    }, {
                                        id      : this._authforgotpasswordId,
                                        text    : $lang('msg_forgotpassword_linkbutton'),
                                        cls     : 'x-form-item',
                                        xtype   : 'linkbutton',
                                        hidden  : !Viz.ApplicationSettings.AllowForgotPassword,
                                        scope   : this,
                                        handler : function() {
                                            var winForgotPassword = new Viz.window.ForgotPassword();
                                        }
                                    }, {
                                        xtype     : 'component',
                                        applyTo   : this._authsubmitId,
                                        id        : this._authsubmitId,
                                        hideLabel : true,
                                        height    : 1,
                                        cls       : 'x-hidden',
                                        hidden    : true,
                                        anchor    : null
                                    }]
                        });

                var copyrightTitle = Ext.get('CopyrightTitle');
                var copyrightSmallText = Ext.get('CopyrightSmallText');
                var loginFooter = Ext.get('LoginFooter');

                this.oWin = new Ext.Window({
                            showAnimDuration : 1,
                            cls              : 'logindialog',
                            width            : 885,
                            height           : 650,
                            closable         : false,
                            frame            : false,
                            layout           : 'absolute',
                            resizable        : false,
                            bodyStyle        : 'font-size:25px ' + (Ext.isIE && !Ext.isIE9 ? '' : '!important') + ';background:url(images/logindialog-v2.png) top left no-repeat; padding:0px;padding-top:0',
                            items            : [{
                                        xtype     : 'panel',
                                        border    : false,
                                        layout    : 'border',
                                        bodyStyle : 'background:transparent',
                                        x         : 0,
                                        y         : 0,
                                        height    : 620,
                                        width     : 885,
                                        items     : [{
                                                    region    : 'north',
                                                    bodyStyle : 'background:transparent',
                                                    html      : '<div style="font-size:26px;font-family:Arial;padding-top:0px;padding-' + this._propDir + ':33px"><img style="position:relative;top:28px" src="images/main.png" /><span style="padding-' + this._propDir + ':28px">' + $lang('msg_application_title') + '<sup style="font-size:67%;padding-left:10px"></sup></span></div>',
                                                    height    : 162,
                                                    border    : false
                                                }, {
                                                    region    : 'center',
                                                    border    : false,
                                                    layout    : 'absolute',
                                                    bodyStyle : 'background:transparent',
                                                    items     : [this.oForm]
                                                }, {
                                                    region    : 'south',
                                                    height    : 260,
                                                    bodyStyle : 'background:transparent',
                                                    layout    : 'form',
                                                    border    : false,
                                                    items     : [{
                                                                xtype     : 'panel',
                                                                border    : false,
                                                                bodyStyle : 'background:transparent;' + this._propButtonPadding + ';padding-top:20px',
                                                                items     : [{
                                                                            text    : $lang('msg_login_enter'),
                                                                            id      : this._authbuttontId,
                                                                            xtype   : 'button',
                                                                            scope   : this,
                                                                            scale   : 'large',
                                                                            width   : 160,
                                                                            handler : this.fnSubmit
                                                                        }]
                                                            }, {
                                                                xtype     : 'panel',
                                                                anchor    : '-42',
                                                                bodyStyle : 'background:transparent',
                                                                border    : false,
                                                                html      : '<div style="font-size:13px;text-align:' + this._propDirReverse + ';color:gray;">' + $lang('msg_login_all_fields_required') + '</div>'
                                                            }]

                                                }]
                                    }, {

                                        xtype     : 'panel',
                                        x         : 45,
                                        y         : 530,
                                        border    : false,
                                        bodyStyle : 'background:transparent',
                                        html      : '<div style="font-size:13px;text-align:' + this._propDir + ';color:gray;">' + Viz.AssemblyVersion + '</div>'
                                    }, {
                                        xtype     : 'panel',
                                        x         : 43,
                                        y         : 561,
                                        border    : false,
                                        bodyStyle : 'background:transparent;font-size:12px;text-align:' + this._propDir,
                                        html      : copyrightTitle ? copyrightTitle.dom.innerHTML : ''
                                    }, {
                                        xtype     : 'panel',
                                        x         : 43,
                                        y         : 576,
                                        border    : false,
                                        bodyStyle : 'background:transparent;font-size:11px;text-align:' + this._propDir,
                                        html      : copyrightSmallText ? copyrightSmallText.dom.innerHTML : ''
                                    }, {
                                        xtype     : 'panel',
                                        x         : 0,
                                        y         : 620,
                                        width     : 885,
                                        border    : false,
                                        bodyStyle : 'text-align:center;background:transparent;width=100%;font-size:12px',
                                        html      : loginFooter ? loginFooter.dom.innerHTML : ''
                                    }],
                            keys             : {
                                key   : 13,
                                fn    : this.fnSubmit,
                                scope : this
                            }

                        });
                this.oWin.show();
            },

            /**
             * Adds a hidden iframe to the document that serves as the "target" for the form submit. The login response is later read from that iframe.
             * @private
             */
            addLoginResultFrame : function() {
                var frame = document.getElementById(this._iframeloginpostbackId);
                if (frame) {
                    // Ext.EventManager.removeListener(frame, 'load', this.handleFrameLoaded, this);
                    Ext.removeNode(frame);
                }

                frame = document.createElement('iframe');
                Ext.fly(frame).set({
                            id   : this._iframeloginpostbackId,
                            name : this._iframeloginpostbackId,
                            cls  : 'x-hidden',
                            src  : Ext.SSL_SECURE_URL
                        });
                document.body.appendChild(frame);

                // This is required so that IE doesn't pop the response up in a new window.
                if (Ext.isIE) {
                    document.frames['iframeloginpostback'].name = this._iframeloginpostbackId;
                }
                // Ext.EventManager.on(frame, 'load', this.handleFrameLoaded, this);

                this.oForm.getForm().findField(this._authusernameId).focus(true, 1000);
            },

            fnSubmit            : function() {
                if (!this.oForm.getForm().isValid())
                    return;

                // var values = this.oForm.getForm().getValues();
                var values = {
                    userName               : this.oForm.getForm().findField(this._authusernameId).getValue(),
                    password               : this.oForm.getForm().findField(this._authpasswordId).getValue(),
                    createPersistentCookie : Ext.getCmp(this._rememberMeId).getValue(),
                    provider               : 'FOLSqlMembership' // Ext.getCmp(this._authenticationProviderId).getValue()
                };
                var params = Ext.apply(values, {
                            // provider : "FOLSqlMembership",
                            flgFirst     : true,
                            isPersistent : false,
                            // customInfo : 'toto',

                            /**
                             * @param {Viz.BusinessEntity.FormResponse} response
                             */
                            success      : function(response) {
                                // first we check that the couple userName / password is valid
                                if (response.success == true) {
                                    Viz.pageId = response.msg;
                                    Ext.get(this._authsubmitId).dom.click();

                                    Viz.HasLogged = true;
                                    // Viz.DisplayAsDesktop = this.oForm.getForm().findField("DisplayAsDesktop").getValue();
                                    // we add the login/password to a cookie.
                                    // Ext.util.Cookies.set("userName", this.oForm.getForm().findField("userName").getValue(), new Date().add(Date.DAY, 400));
                                    // Ext.util.Cookies.set("password", this.oForm.getForm().findField("password").getValue(), new Date().add(Date.DAY, 400));

                                    // if true we get the user membership
                                    Viz.Services.AuthenticationWCF.User_GetCurrent({
                                                /**
                                                 * @param {Viz.BusinessEntity.FOLMembershipUser ) user
                                                 */
                                                success : function(user) {
                                                    // and we close the login window and raise the event LoginSuccess
                                                    this.oWin.close();
                                                    Viz.util.MessageBusMgr.publish("LoginSuccess", user);
                                                    Viz.activateGoogleAnalytics(user.ApplicationName);
                                                },
                                                scope   : this
                                            });
                                }
                                else
                                // if false we alert the user
                                if (response.msg == $lang('error_password_expired') && params.flgFirst == true) {
                                    // hide the progress box
                                    Ext.MessageBox.hide();
                                    // hide the login box
                                    this.oWin.hide();
                                    var winChangePassord = new Viz.window.ChangePassword({
                                                username        : response.data.Value,
                                                applicationName : response.data.Id,
                                                closable        : false,
                                                listeners       : {
                                                    savesuccess : function(form, action) {
                                                        // redo login
                                                        winChangePassord.close();
                                                        Viz.Services.PublicWCF.Login_Response(Ext.apply(this, {
                                                                    flgFirst : false,
                                                                    password : action.options.extraParams.item.newPassword
                                                                }));
                                                    },
                                                    scope       : params
                                                }
                                            }).show();
                                }
                                else {
                                    Ext.MessageBox.show({
                                                title   : $lang('msg_application_title'),
                                                msg     : response.msg, // $lang("err_msg_invalid_authentication"),
                                                width   : 350,
                                                icon    : Ext.MessageBox.ERROR,
                                                buttons : Ext.MessageBox.OK
                                            });
                                }
                            },
                            scope        : this
                        });
                Viz.util.MessageBusMgr.publish("CheckingCredential");
                Viz.Services.PublicWCF.IsTimeSynchronized({
                            clientTime : new Date()
                        });
                Viz.Services.PublicWCF.Login_Response(params);
            }
});
Ext.reg('vizWindowLogin', Viz.window.Login);

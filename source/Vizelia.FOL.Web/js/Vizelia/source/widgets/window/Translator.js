﻿Ext.namespace("Viz.window");

/**
 * @class Viz.window.Translator
 * @extends Ext.Window The Translator window. Usage:
 * 
 * <pre><code>
 * new Viz.window.Translator().show();
 * </code></pre>
 * 
 * @xtype vizWindowTranslator
 */
Viz.window.Translator = Ext.extend(Ext.Window, {

            /**
             * Constructor.
             * @param {Object} config The configuration contains version and toolEl properties.
             */
            constructor : function(config) {
                config = config || {};
                // id of the window.
                this._id = Ext.id();
                // id of the main form.
                this._formid = Ext.id();
                // id of the output field.
                this._outputid = Ext.id();

                var defaultConfig = {
                    id      : this._id,
                    title   : $lang('msg_application_translator'),
                    iconCls : 'viz-icon-small-translator',
                    width   : 500,
                    height  : 300,
                    layout  : 'fit',
                    items   : [{
                                xtype          : 'vizForm',
                                serviceHandler : Viz.Services.CoreWCF.LocalizationResource_TranslateText,
                                waitMsgTarget  : this._id,
                                id             : this._formid,
                                autoScroll     : true,
                                items          : [{
                                            fieldLabel : $lang('msg_translator_input'),
                                            name       : 'input',
                                            grow       : true,
                                            allowBlank : false,
                                            xtype      : 'textarea'
                                        }, {
                                            fieldLabel : $lang('msg_translator_from'),
                                            name       : 'languageFrom',
                                            hiddenName : 'languageFrom',
                                            allowBlank : false,
                                            xtype      : 'vizComboLocalizationCulture'
                                        }, {
                                            fieldLabel : $lang('msg_translator_to'),
                                            name       : 'languageTo',
                                            hiddenName : 'languageTo',
                                            allowBlank : false,
                                            xtype      : 'vizComboLocalizationCulture'
                                        }, {
                                            fieldLabel : $lang('msg_translator_output'),
                                            name       : 'output',
                                            xtype      : 'textarea',
                                            grow       : true,
                                            id         : this._outputid
                                        }],
                                buttons        : [{
                                            text    : $lang('msg_translator_translate'),
                                            cls     : 'x-toolbar-standardbutton',
                                            scope   : this,
                                            handler : function() {
                                                var form = Ext.getCmp(this._formid).getForm();
                                                form.submitWCF({
                                                            waitMsg : true,
                                                            success : function(form, action) {
                                                                Ext.getCmp(this._outputid).setValue(action.result);
                                                            },
                                                            failure : function(form, action) {
                                                                Ext.getCmp(this._outputid).setValue('');
                                                            },
                                                            scope   : this
                                                        });
                                            }
                                        }]
                            }]
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.window.Translator.superclass.constructor.call(this, config);
            }

        });

Ext.reg('vizWindowTranslator', Viz.window.Translator);
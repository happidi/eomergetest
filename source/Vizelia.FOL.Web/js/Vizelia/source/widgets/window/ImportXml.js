﻿Ext.namespace("Viz.window");

/**
 * @class Viz.window.ImportXml
 * @extends Ext.Window The importXml window. Usage:
 * 
 * <pre><code>
 * new Viz.window.ImportXml().show();
 * </code></pre>
 * 
 * CBL
 * @xtype vizWindowImportXml
 */
Viz.window.ImportXml = Ext.extend(Ext.Window, {

            /**
             * Constructor.
             * @param {Object} config The configuration contains version and toolEl properties.
             */
            constructor : function(config) {
                config = config || {};
                // id of the window.
                this._id = Ext.id();
                // id of the main form.
                this._formid = Ext.id();

                this._fileUploadFieldId = Ext.id();
                this._submitButtonId = Ext.id();

                if (config.serviceHandler) {
                    var serviceHandlerForFile = config.serviceHandler;
                }
                else {
                    var serviceHandlerForFile = Viz.Services.MappingWCF.BeginMapping;
                }

                var defaultConfig = {
                    id      : this._id,
                    title   : $lang('msg_mapping_import_file'),
                    iconCls : 'viz-icon-small-xml',
                    width   : 400,
                    height  : 100,
                    layout  : 'fit',
                    items   : [{
                                xtype                   : 'vizForm',
                                waitMsgTarget           : this._id,
                                labelWidth              : 100,
                                id                      : this._formid,
                                autoScroll              : true,
                                isUpload                : true,
                                isLongRunningSubmit     : true,
                                closeonsave             : true,
                                longRunningSubmitConfig : {
                                    isFileResult           : true,
                                    progressWindowTitle    : $lang('msg_mapping_import_file'),
                                    showProgressPercentage : true,
                                    allowCancel            : true,
                                    maxPollingTime         : 60 * 60 * 1000 // one hour
                                },
                                serviceHandler          : serviceHandlerForFile,
                                items                   : [{
                                            id         : this._fileUploadFieldId,
                                            fieldLabel : $lang('msg_filename'),
                                            name       : 'InputFile',
                                            allowBlank : false,
                                            xtype      : 'fileuploadfield',
                                            buttonText : '',
                                            buttonCfg  : {
                                                iconCls : 'viz-icon-small-xml'
                                            }
                                        }],
                                buttons                 : [{
                                            text     : $lang('msg_application_upload'),
                                            id       : this._submitButtonId,
                                            scope    : this,
                                            iconCls  : 'viz-icon-small-upload',
                                            handler  : this.save,
                                            scope    : this,
                                            disabled : true
                                        }]
                            }]
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.Services.MappingWCF.GetAllowedMappingFileExtensions({
                            success : function(extensionsList) {
                                // Convert the list of extensions we got to two lists of lowercase and uppercase and join them.
                                var lowercaseExtensions = [];
                                var uppercaseExtensions = [];
                                for (var i = 0; i < extensionsList.length; i++) {
                                    lowercaseExtensions[i] = extensionsList[i].toLowerCase();
                                    uppercaseExtensions[i] = extensionsList[i].toUpperCase();
                                }

                                var combinedArray = lowercaseExtensions.concat(uppercaseExtensions);
                                var allowedExtensions = combinedArray.join('|');
                                var allowedExtensionsFriendlyMessage = uppercaseExtensions.join(', ');
                                // Create a regex based on the list of extensions.
                                var regex = '^.*.(' + allowedExtensions + ')$';
                                var fileUploadField = Ext.getCmp(this._fileUploadFieldId);
                                Ext.apply(fileUploadField, {
                                            regex     : new RegExp(regex),
                                            regexText : String.format($lang('error_msg_invalid_file_extension'), allowedExtensionsFriendlyMessage)
                                        });

                                var submitButton = Ext.getCmp(this._submitButtonId);
                                submitButton.enable();
                            },
                            scope   : this
                        });

                Viz.window.ImportXml.superclass.constructor.call(this, config);
            },

            save        : function() {
                var form = Ext.getCmp(this._formid);
                form.longRunningSubmitConfig.showProgressPercentage = this.shouldShowProgressPercentage();
                form.saveItem();
            },
            
            shouldShowProgressPercentage : function() {
                // We do not show percentage on Excel files since they are not read in linear way.
                // Enable/disable percentage by file extension.
                var fileUploadField = Ext.getCmp(this._fileUploadFieldId);
                var isNonProgressFileName = /.xlsx$|.XLS$/.test(fileUploadField.value);
                
                return !isNonProgressFileName;
            }
        });

Ext.reg('vizWindowImportXml', Viz.window.ImportXml);
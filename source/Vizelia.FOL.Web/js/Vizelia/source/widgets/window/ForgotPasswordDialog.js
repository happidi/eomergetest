﻿Ext.namespace("Viz.window");
/**
 * The forgot password window. Usage :
 * 
 * <pre><code>
 * var winForgotPassword = new Viz.window.ForgotPassword();
 * </code></pre>
 * 
 * @class Viz.window.ForgotPassword
 * @extends Ext.util.Observable
 * @xtype vizWindowForgotPassword
 */
Viz.window.ForgotPassword = Ext.extend(Ext.util.Observable, {

            /**
             * Ctor
             */
            constructor                  : function(config) {
                var loginFooterDiv = Ext.get('vizLoginFooter');
                var loginFooterText = $lang('msg_login_footer');

                if (loginFooterDiv && loginFooterDiv.dom && loginFooterDiv.dom.innerHTML) {
                    loginFooterText = loginFooterDiv.dom.innerHTML;
                }

                this._iframeforgotpasswordpostbackId = 'iframeforgotpasswordpostback';
                this._forgotpasswordusernameId = 'forgotpassword-username';
                this._forgotpasswordemailId = 'forgotpassword-email';
                this._forgotpasswordsubmitId = 'forgotpassword-submit';
                this._forgotpasswordcancelId = 'forgotpassword-cancel';

                if ($lang('langue_direction') == 'rtl') {
                    this._propX = 10;
                    this._propDir = 'right';
                    this._propDirReverse = 'left';
                    this._propButtonPadding = "padding-right:630px";
                }
                else {
                    this._propX = 500;
                    this._propDir = 'left';
                    this._propDirReverse = 'right';
                    this._propButtonPadding = "padding-left:625px";

                }
                Viz.window.ForgotPassword.superclass.constructor.apply(this, arguments);

                // Viz.util.MessageBusMgr.subscribe("DoLogin", this.fnSubmitStandard, this);

                this.addEvents(
                        /**
                         * @event disable Fires after the component is disabled.
                         */
                        'beforelogin',
                        /**
                         * @event success Fires after the component is enabled.
                         */
                        'success',
                        /**
                         * @event failre Fires after the component is enabled.
                         */
                        'failure');

                this.forgotPasswordForm = new Ext.form.FormPanel({
                            id             : 'forgotpasswordform',
                            method         : 'post',
                            labelWidth     : 115,
                            standardSubmit : true,
                            x              : this._propX,
                            y              : 78,
                            width          : 370,
                            bodyStyle      : 'background: transparent;padding:5px;',
                            border         : false,
                            listeners      : {

                                beforerender : function(form) {
                                    form.bodyCfg.action = 'blank.aspx';
                                    form.bodyCfg.target = this._iframeforgotpasswordpostbackId;
                                },
                                afterrender  : this.addforgotpasswordResultFrame,
                                scope        : this
                            },
                            items          : [{
                                        anchor     : '-20',
                                        xtype      : 'textfield',
                                        fieldLabel : $lang("msg_username"),
                                        id         : this._forgotpasswordusernameId,
                                        name       : this._forgotpasswordusernameId,
                                        allowBlank : true,
                                        // applyTo : this._forgotpasswordusernameId,
                                        autoShow   : true
                                    }, {
                                        anchor     : '-20',
                                        xtype      : 'textfield',
                                        allowBlank : false,
                                        fieldLabel : $lang("msg_email"),
                                        name       : this._forgotpasswordemailId,
                                        // applyTo : this._forgotpasswordemailId,
                                        id         : this._forgotpasswordemailId,
                                        allowBlank : true,
                                        autoShow   : true,
                                        minLength  : 1
                                    }, {

                                        xtype     : 'component',
                                        // applyTo : this._forgotpasswordsubmitId,
                                        id        : this._forgotpasswordsubmitId,
                                        hideLabel : true,
                                        height    : 1,
                                        cls       : 'x-hidden',
                                        hidden    : true,
                                        anchor    : null
                                    }, {
                                        xtype     : 'component',
                                        // applyTo : this._forgotpasswordcancelId,
                                        id        : this._forgotpasswordcancelId,
                                        hideLabel : true,
                                        height    : 1,
                                        cls       : 'x-hidden',
                                        hidden    : true,
                                        anchor    : null
                                    }]
                        });

                this.forgotPasswordWin = new Ext.Window({
                            showAnimDuration : 1,
                            cls              : 'logindialog',
                            width            : 885,
                            height           : 650,
                            closable         : false,
                            frame            : false,
                            layout           : 'absolute',
                            resizable        : false,
                            bodyStyle        : 'font-size:25px ' + (Ext.isIE && !Ext.isIE9 ? '' : '!important') + ';background:url(images/logindialog-v2.png) top left no-repeat; padding:0px;padding-top:0',
                            items            : [{
                                        xtype     : 'panel',
                                        border    : false,
                                        layout    : 'border',
                                        bodyStyle : 'background:transparent',
                                        x         : 0,
                                        y         : 0,
                                        height    : 620,
                                        width     : 885,
                                        items     : [{
                                                    region    : 'north',
                                                    bodyStyle : 'background:transparent',
                                                    html      : '<div style="font-size:26px;font-family:Arial;padding-top:0px;padding-' + this._propDir + ':33px"><img style="position:relative;top:28px" src="images/main.png" /><span style="padding-' + this._propDir + ':28px">' + $lang('msg_application_title') + '<sup style="font-size:67%;padding-left:10px"></sup></span></div>',
                                                    height    : 162,
                                                    border    : false
                                                }, {
                                                    region    : 'center',
                                                    border    : false,
                                                    layout    : 'absolute',
                                                    bodyStyle : 'background:transparent',
                                                    items     : [{
                                                                xtype : 'label',
                                                                text  : $lang('msg_forgotpassword_title'),
                                                                style : 'font-size:16px;font-family:Arial;',
                                                                x     : 500,
                                                                y     : 0,
                                                                cls   : 'x-form-item'
                                                            }, {
                                                                xtype : 'label',
                                                                text  : $lang('msg_forgotpassword_subtitle'),
                                                                style : 'font-size:16px;font-family:Arial;',
                                                                x     : 500,
                                                                y     : 20,
                                                                cls   : 'x-form-item'
                                                            }, this.forgotPasswordForm]
                                                }, {
                                                    region    : 'south',
                                                    height    : 260,
                                                    bodyStyle : 'background:transparent',
                                                    layout    : 'form',
                                                    border    : false,
                                                    items     : [{
                                                                xtype        : 'panel',
                                                                border       : false,
                                                                bodyStyle    : 'background:transparent;' + this._propButtonPadding + ';padding-top:20px',
                                                                height       : 60,
                                                                autoWidth    : true,
                                                                layout       : 'hbox',
                                                                layoutConfig : {
                                                                    align : 'stretch'
                                                                },
                                                                defaults     : {
                                                                    flex : 1
                                                                },
                                                                items        : [{
                                                                            text    : $lang('msg_submit'),
                                                                            id      : this._forgotpasswordsubmitId,
                                                                            xtype   : 'button',
                                                                            scope   : this,
                                                                            scale   : 'large',
                                                                            width   : 100,
                                                                            handler : this.fnSubmit
                                                                        }, {
                                                                            text    : $lang('msg_cancel'),
                                                                            id      : this._forgotpasswordcancelId,
                                                                            xtype   : 'button',
                                                                            scope   : this,
                                                                            scale   : 'large',
                                                                            width   : 100,
                                                                            style   : 'margin-left:20px',
                                                                            handler : this.fnCancel
                                                                        }]
                                                            }, {
                                                                xtype     : 'panel',
                                                                anchor    : '-42',
                                                                bodyStyle : 'background:transparent',
                                                                border    : false,
                                                                html      : '<div style="font-size:13px;text-align:' + this._propDirReverse + ';color:gray;">' + '</div>'
                                                            }]

                                                }]
                                    }, {
                                        xtype     : 'panel',
                                        x         : 0,
                                        y         : 620,
                                        border    : false,
                                        bodyStyle : 'background:transparent',
                                        html      : '<div class="loginfooter">' + loginFooterText + '</div>'
                                    }],
                            keys             : {
                                key   : 13,
                                fn    : this.fnSubmit,
                                scope : this
                            }

                        });
                this.forgotPasswordWin.show();
            },

            /**
             * Adds a hidden iframe to the document that serves as the "target" for the form submit. The forgot password response is later read from that iframe.
             * @private
             */
            addforgotpasswordResultFrame : function() {
                var frame = document.getElementById(this._iframeforgotpasswordpostbackId);
                if (frame) {
                    // Ext.EventManager.removeListener(frame, 'load', this.handleFrameLoaded, this);
                    Ext.removeNode(frame);
                }

                frame = document.createElement('iframe');
                Ext.fly(frame).set({
                            id   : this._iframeforgotpasswordpostbackId,
                            name : this._iframeforgotpasswordpostbackId,
                            cls  : 'x-hidden',
                            src  : Ext.SSL_SECURE_URL
                        });
                document.body.appendChild(frame);

                // This is required so that IE doesn't pop the response up in a new window.
                if (Ext.isIE) {
                    document.frames['iframeforgotpasswordpostback'].name = this._iframeforgotpasswordpostbackId;
                }
                // Ext.EventManager.on(frame, 'load', this.handleFrameLoaded, this);

                this.forgotPasswordForm.getForm().findField(this._forgotpasswordusernameId).focus(true, 1000);
            },
            fnCancel                     : function() {
                this.forgotPasswordWin.hide();
                this.forgotPasswordWin.destroy();
            },
            fnSubmit                     : function() {
                if (!this.forgotPasswordForm.getForm().isValid())
                    return;

                var username = this.forgotPasswordForm.getForm().findField(this._forgotpasswordusernameId).getValue();
                var email = this.forgotPasswordForm.getForm().findField(this._forgotpasswordemailId).getValue();
                if (username != null && email != null) {
                    Ext.MessageBox.show({
                                title   : $lang('msg_application_title'),
                                msg     : $lang("error_msg_forgotpassword_toomanyparameters"),
                                width   : 350,
                                icon    : Ext.MessageBox.ERROR,
                                buttons : Ext.MessageBox.OK
                            });
                    return;
                }
                if (username == null && email == null) {
                    Ext.MessageBox.show({
                                title   : $lang('msg_application_title'),
                                msg     : $lang("error_msg_forgotpassword_parametersnotsupplied"),
                                width   : 350,
                                icon    : Ext.MessageBox.ERROR,
                                buttons : Ext.MessageBox.OK
                            });
                    return;
                }
                var values = {
                    username : username,
                    email    : email
                };
                var params = Ext.apply(values, {
                            flgFirst     : true,
                            isPersistent : false,

                            /**
                             * @param {Viz.BusinessEntity.FormResponse} response
                             */
                            success      : function(response) {
                                if (response.success == true) {
                                    Ext.get(this._forgotpasswordsubmitId).dom.click();
                                    Ext.MessageBox.show({
                                                title   : $lang('msg_application_title'),
                                                msg     : $lang('Your new password sent to your email, please use it within the next 24 hours'),
                                                width   : 350,
                                                icon    : Ext.MessageBox.INFO,
                                                buttons : Ext.MessageBox.OK,
                                                scope   : this,
                                                fn      : function() {
                                                    this.forgotPasswordWin.hide();
                                                    this.forgotPasswordWin.destroy();
                                                }
                                            });
                                }
                                else
                                    Ext.MessageBox.show({
                                                title   : $lang('msg_application_title'),
                                                msg     : response.msg, // $lang("err_msg_invalid_authentication"),
                                                width   : 350,
                                                icon    : Ext.MessageBox.ERROR,
                                                buttons : Ext.MessageBox.OK
                                            });
                            },
                            scope        : this
                        });
                // Viz.util.MessageBusMgr.publish("CheckingCredential");
                // Viz.Services.PublicWCF.IsTimeSynchronized({
                // clientTime : new Date()
                // });
                Viz.Services.PublicWCF.UserForgotPassword(params);
            }
        });
Ext.reg('vizWindowForgotPassword', Viz.window.ForgotPassword);

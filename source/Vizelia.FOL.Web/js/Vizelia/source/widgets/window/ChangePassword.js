﻿Ext.namespace("Viz.window");

/**
 * @class Viz.window.ChangePassword
 * @extends Ext.Window The Change password window. Usage:
 * 
 * <pre><code>
 * new Viz.window.ChangePassword().show();
 * </code></pre>
 * 
 * @xtype vizWindowChangePassword
 */
Viz.window.ChangePassword = Ext.extend(Ext.Window, {

            /**
             * Constructor.
             * @param {Object} config The configuration contains version and toolEl properties.
             */
            constructor : function(config) {
                config = config || {};
                // id of the window.
                this._id = Ext.id();
                // id of the main form.
                this._formid = Ext.id();
                // id of the password field.
                this._fieldpasswordId = Ext.id();
                // id of the username field
                this._formUserId = Ext.id();
                var username = config.username || ((Viz.App && Viz.App.Global && Viz.App.Global.User) ? Viz.App.Global.User.UserName : null);
                var applicationName = config.applicationName || ((Viz.App && Viz.App.Global && Viz.App.Global.User) ? Viz.App.Global.User.ApplicationName : null);

                var defaultConfig = {
                    id       : this._id,
                    title    : $lang('msg_changepassword'),
                    iconCls  : 'viz-icon-small-changepassword',
                    width    : 450,
                    height   : 190,
                    layout   : 'fit',
                    modal    : true,
                    xtype    : 'window',
                    items    : [{
                                xtype                : 'vizForm',
                                mode                 : 'create',
                                labelWidth           : 200,
                                id                   : this._formid,
                                applyEnabled         : false,
                                cancelable           : false,
                                serviceHandlerCreate : Viz.Services.PublicWCF.User_ChangePasswordForm,
                                items                : [{
                                            fieldLabel : $lang('msg_login'),
                                            name       : 'username',
                                            id         : this._formUserId,
                                            value      : username,
                                            allowBlank : false,
                                            hidden     : username ? true : false,
                                            hideLabel  : username ? true : false
                                        }, {
                                            fieldLabel : $lang('msg_applicationname'),
                                            name       : 'applicationName',
                                            value      : applicationName,
                                            allowBlank : false,
                                            hidden     : applicationName ? true : false,
                                            hideLabel  : applicationName ? true : false
                                        }, {
                                            fieldLabel : $lang('msg_oldpassword'),
                                            name       : 'oldPassword',
                                            allowBlank : false,
                                            inputType  : 'password'
                                        }, {
                                            fieldLabel : $lang('msg_newpassword'),
                                            id         : this._fieldpasswordId,
                                            name       : 'newPassword',
                                            inputType  : 'password',
                                            allowBlank : false
                                        }, {
                                            fieldLabel           : $lang('msg_newpassword_confirm'),
                                            name                 : 'confirmNewPassword',
                                            validator            : Ext.form.VTypes.passwordconfirmValidator,
                                            inputType            : 'password',
                                            userIdField          : this._formUserId,
                                            initialPasswordField : this._fieldpasswordId,
                                            allowBlank           : false
                                        }],
                                listeners            : {
                                    savesuccess : function() {
                                        Ext.MessageBox.show({
                                                    title   : $lang('msg_application_title'),
                                                    msg     : $lang("msg_changepassword_successful"),
                                                    width   : 350,
                                                    icon    : Ext.MessageBox.INFO,
                                                    buttons : Ext.Msg.OK
                                                });
                                    }
                                }

                            }]
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.window.ChangePassword.superclass.constructor.call(this, config);
            },

            /**
             * @private Redirect the event savesuccess to the window.
             */
            onRender    : function() {
                Viz.window.ChangePassword.superclass.onRender.apply(this, arguments);
                this.relayEvents(Ext.getCmp(this._formid), ['savesuccess']);
            }

        });

Ext.reg('vizWindowChangePassword', Viz.window.ChangePassword);
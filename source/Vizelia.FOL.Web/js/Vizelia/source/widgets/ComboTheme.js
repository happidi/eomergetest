Viz.ComboTheme = Ext.extend(Ext.form.ComboBox, {
    
    lazyRender:true,
    lazyInit:true,
    cssPath:'theme/css/',
	
	initComponent:function() {
		if(Ext.state.Manager.getProvider()){
     		var theme=Ext.state.Manager.get('theme');
	   	 	if(theme && theme.length>0)
	   			Ext.util.CSS.swapStyleSheet('theme', this.cssPath + theme);
        }

	 	Ext.apply(this, {
            store: new Ext.data.SimpleStore({
                 fields: ['themeFile', 'themeName'],
                 data: [['xtheme-default.css', $('themeBlueText')],
		     		['xtheme-slate.css', $('themeSlateText')],
		     		['xtheme-gray-extend.css', $('themeGrayText')],
		     		['xtheme-purple.css', $('themePurpleText')],
             		['xtheme-darkgray.css', $('themeDarkGrayText')]]
            }),
            valueField: 'themeFile',
            displayField: 'themeName',
            triggerAction:'all',
            mode: 'local',
            forceSelection:true,
            editable:false,
	    	emptyText:$('selectThemeText'),
            fieldLabel: $('Theme')
        }); 
        Viz.ComboTheme.superclass.initComponent.apply(this, arguments);
    }, 
    
    onSelect:function() {
        Viz.ComboTheme.superclass.onSelect.apply(this, arguments);
        var theme = this.getValue();
        Ext.util.CSS.swapStyleSheet('theme', this.cssPath + theme);
		if(Ext.state.Manager.getProvider()) {
            Ext.state.Manager.set('theme', theme);
        }
    } 
});

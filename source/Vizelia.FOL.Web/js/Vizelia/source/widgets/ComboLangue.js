Viz.ComboLangue = Ext.extend(Ext.CycleButton, {
	languageItems:[
		{language: 'en', text: 'English&nbsp;', iconCls : 'flag_us_BT',checked:true},
		{language: 'fr', text: 'Francais&nbsp;', iconCls : 'flag_fr_BT'}
	],
	languageVar: 'lang',
	initComponent: function() {
		Ext.apply(this, {
		    showText: false,
			prependText: '&nbsp;',
   			items: this.languageItems
		});
		if(Ext.state.Manager){ 
        	var selectedLanguage = Ext.state.Manager.get(this.languageVar); 
            if(selectedLanguage){ 
				for(var i=0; i<this.items.length;i++){
					if (this.items[i].language == selectedLanguage){
						this.items[i].checked = true;
						break;
					}
				}
            } 
        } 
		Viz.ComboLangue.superclass.initComponent.apply(this, arguments);
	},
	
	changeHandler: function(o, i){
        if(Ext.state.Manager.getProvider()) {
            Ext.state.Manager.set(this.languageVar, i.language);
        }
		window.location.href = window.location.protocol + '//' + window.location.host + window.location.pathname + '?l=' + i.language;
	}
	        
});

Ext.reg('vizcombolangue', Viz.ComboLangue);
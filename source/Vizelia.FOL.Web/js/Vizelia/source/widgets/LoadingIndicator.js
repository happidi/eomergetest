Ext.namespace("Viz");

/**
 * Loading box Singleton.
 * <p>
 * The loading box is built from plain HTML to be displayed immediately in the page before loading of scripts.
 * </p>
 * <p>
 * When hidden (hide) it publishes the event EndLoading.
 * </p>
 * <p>
 * The HTML code is:
 * 
 * <pre><code>
 * &lt;div id=&quot;loading-msg&quot; style=&quot;width: 98%; left: 0; top: 45%; text-align: center; margin-left: 2px;
 *             border: 0; background-color: Transparent&quot;&gt;
 *             &lt;div /&gt;
 *             &lt;img style=&quot;text-align: center;&quot; src=&quot;images/logo_vizelia_300.bmp&quot; /&gt;
 * <br />
 *             &lt;div id=&quot;loading-msg-text&quot; style=&quot;color: gray; margin-top: 5px; font-family: 'Segoe UI', tahoma, arial, helvetica, sans-serif !important;
 *                 font-size: 12px !important;&quot;&gt;
 *                 &lt;%=Vizelia.FOL.Common.Localization.Langue.msg_application_title %&gt;
 *                 is loading, please wait...
 *                 &lt;div id=&quot;loading-msg-subtext&quot; &gt;
 *                     &lt;/div&gt;
 *             &lt;/div&gt;
 *             &lt;div style=&quot;margin-top: 20px;&quot;&gt;
 *                 &lt;img src=&quot;images/progress.gif&quot; width=&quot;300&quot; /&gt;&lt;/div&gt;
 *         &lt;/div&gt;
 * 
 * </code></pre>
 * 
 * </p>
 */
Viz.LoadingIndicator = function() {
    var mask;
    var msg;
    var msgtext;
    var msgsubtext;
    var progressBar;

    return {
        /**
         * Initiates the variables.
         */
        init: function() {
            mask = Ext.get('loading-mask');
            msg = Ext.get('loading-msg');
            msgtext = Ext.get('loading-msg-text');
            msgsubtext = Ext.get('loading-msg-subtext');
            if ((msg != null) && (mask != null)) {
                progressBar = new Ext.ProgressBar({
                    renderTo: msg
                });
            }
        },

        /**
         * Shows the loading indicator.
         */
        show: function() {
            msg.fadeIn();
        },

        /**
         * Hides the loading indicator.
         */
        hide: function(o) {
            if (!mask)
                this.init();
            msg.fadeOut({
                scope: this,
                callback: function() {
                    Viz.util.MessageBusMgr.publish("EndLoading");
                }
            });
        },

        /**
         * Updates the messages of the loading indicator.
         * @param {Number} value The value of the progress bar
         * @param {String} text The main text of the indicator.
         * @param {String} (optional) subtext The sub text of the indicator.
         */
        update: function(value, text, subtext) {
            if (text)
                msgtext.update(text);
            if (subtext)
                msgsubtext.update(subtext);
            return;
        }

    };
}();
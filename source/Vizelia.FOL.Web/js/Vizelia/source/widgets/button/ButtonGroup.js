﻿Ext.namespace("Viz");

/**
 * Extends Ext.ButtonGroup to give a Ribbon Microsoft Office 2007 look and feel to the button group.
 * 
 * <pre><code>
 *  new Viz.ButtonGroup({
 *  columns : 2,
 *  title   : 'Presse-papiers',
 *  items   : [{
 *  text       : 'Button1',
 *  rowspan    : '3',
 *  iconAlign  : 'top',
 *  scale      : 'large',
 *  arrowAlign : 'bottom',
 *  iconCls    : 'viz-icon-large-scissors'
 *  }, {
 *  text    : 'Couper',
 *  iconCls : 'viz-icon-small-task'
 *  }, {
 *  text    : 'Copier',
 *  iconCls : 'viz-icon-small-ctiwork'
 *  }, {
 *  text    : 'Reproduire la mise en forme',
 *  iconCls : 'viz-icon-small-occupant'
 *  }]
 * </code></pre>
 * 
 * @class Viz.ButtonGroup
 * @extends Ext.ButtonGroup
 */
Viz.ButtonGroup = Ext.extend(Ext.ButtonGroup, {

            /**
             * @cfg {Boolean} titleAsFooter Defaults to true. Indicates the position of the title (above or under the ButtonGroup).
             */
            titleAsFooter : true,

            /**
             * Ctor
             * @param {Object} config
             */
            constructor   : function(config) {
                config = config || {};
                var defaultConfig = {
                    cls : 'x-btn-group-ribbonstyle'
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Ext.applyIf(config, this);

                if (config.title && config.titleAsFooter) {
                    config.fbar = [config.title];
                    delete config.title;
                }
                Viz.ButtonGroup.superclass.constructor.call(this, config);
            }
        });

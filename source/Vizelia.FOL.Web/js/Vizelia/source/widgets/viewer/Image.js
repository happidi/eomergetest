﻿Ext.namespace('Viz.viewer');

/**
 * @class Viz.viewer.Image
 * @extends Ext.BoxComponent. Abstract class should not be used directly.
 */
Viz.viewer.Image = Ext.extend(Ext.BoxComponent, {

            /**
             * @cfg {Bool} stretchImage <tt>true</tt> to stretch the image to 100%, <tt>false</tt> otherwise, default to <tt>true </tt>
             */

            /**
             * @cfg {Object} serviceInterface The service interface for example www.vizelia.com.wcf._2011._01.IEnergyWCF;
             */
            serviceInterface                   : null,

            /**
             * @cfg {Function) serviceHandler The service handler for building the image and placing it in cache (Optional)
             */
            serviceHandler                     : null,

            /**
             * @cfg {Object} serviceParams The service parameters
             */
            serviceParams                      : null,
            /**
             * @cfg {Object} entity The entity to be displayed.
             */
            entity                             : null,

            /**
             * @cfg {string} serviceStream The service name that is going to return the entity stream.
             */
            serviceStream                      : '',

            /**
             * @cfg {string} entityKey The key of the entity to send to the service.
             */
            entityKey                          : '',

            /**
             * @cfg {string} reloadEventName The MessageBus event Name that can trigger an image refresh.
             */
            reloadEventName                    : '',

            /**
             * @cfg {Number} maskDeferDelay the delay in milliseconds to remove the mask in case where the image is directly build and streamed from the server
             */
            maskDeferDelay                     : 0,

            /**
             * @cfg {Boolean} <T>true</T> to use the overrideHeight and overrideWidth to generate the Image, <T>false</T> to use the parent window size.
             */
            overrideSize                       : false,

            /**
             * @cfg {int} the overriden Height.
             */
            overrideHeight                     : 480,
            /**
             * @cfg {int} the overriden Width.
             */
            overrideWidth                      : 640,

            /**
             * @cfg {string] the loading class
             */
            loadingClass                       : 'viz-chart-loading',

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                        : function(config) {

                if (!this._img)
                    this._img = Ext.id();
                this._lastWidth = 0;
                this._lastHeight = 0;
                this._webRequest = null;
                config = config || {};
                // var stretchImage = this.stretchImage || config.stretchImage;

                var defaultConfig = {
                    stretchImage : true,
                    style        : 'padding:0px;background-color:transparent'
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                if (config.stretchImage === true)
                    config.html = '<img width="100%" height="100%" usemap="" id="' + this._img + '" src="' + Viz.Const.Url.BlankImage + '" >';
                else
                    config.html = '<center><img usemap="" id="' + this._img + '" src="' + Viz.Const.Url.BlankImage + '" ><center>';

                Viz.viewer.Image.superclass.constructor.call(this, config);

                if (!Ext.isEmpty(this.reloadEventName))
                    Viz.util.MessageBusMgr.subscribe(this.reloadEventName, this.onImageChange, this);

                this.on({
                            afterrender : {
                                scope : this,
                                fn    : this.onImageRender
                            },
                            resize      : {
                                scope  : this,
                                fn     : this.onImageResize,
                                buffer : 1000
                            }
                        });
                this.on({
                            resize : {
                                scope : this,
                                fn    : this.onImageStartResize
                            }
                        });
                this.addClass(this.loadingClass);
            },

            cancelLastRequest                  : function() {
                Viz.util.Chart.cancelRequest(this._webRequest);
            },

            /**
             * Destroy
             * @method onDestroy
             * @private
             */
            onDestroy                          : function() {
                this.cancelLastRequest();

                if (!Ext.isEmpty(this.reloadEventName))
                    Viz.util.MessageBusMgr.unsubscribe(this.reloadEventName, this.onImageChange, this);
                Viz.viewer.Image.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Listener for the afterrender event
             * @method onDestroy
             * @private
             */
            onImageRender                      : function() {

            },

            /**
             * Listener when the image start to resize (not buffered).
             * @param {} cmpt
             * @param {} adjWidth
             * @param {} adjHeight
             * @param {} rawWidth
             * @param {} rawHeight
             * @return {Boolean}
             */
            onImageStartResize                 : function(cmpt, adjWidth, adjHeight, rawWidth, rawHeight) {
                this.showLoadingIndicator();
                return true;
            },

            /**
             * Show a loading indicator instead of the image
             */
            showLoadingIndicator               : function() {
                var img = Ext.get(this._img);
                img.dom.src = Viz.Const.Url.BlankImage;
                this.addClass(this.loadingClass);
            },

            /**
             * Listener for the resize event.
             * @method onImageResize
             * @private
             */
            onImageResize                      : function(cmpt, adjWidth, adjHeight, rawWidth, rawHeight) {
                var img = Ext.get(this._img);
                var size = this.getSize();
                this.reloadImage();
            },

            /**
             * Listener for the entity change event.
             * @param {Object} entity
             * @private
             */
            onImageChange                      : function(entity) {
                if (this.entity[this.entityKey] == entity[this.entityKey]) {
                    this.entity = entity;
                    this.reloadImage();
                }
            },

            /**
             * Loads the Image and renders it
             * @method reloadImage
             */
            reloadImage                        : function() {
                if (!Ext.isEmpty(this.serviceStream)) {

                    this.cancelLastRequest();

                    var img = Ext.get(this._img);
                    var size = this.getSize();
                    var animationDirection = this.getParentRefreshAnimationDirection();

                    // fix a bug where the size of the element is 0 if the portal it belongs to is not displayed.
                    if (size.width <= 0)
                        size.width = this._lastWidth;
                    if (size.height <= 0)
                        size.height = this._lastHeight;

                    while (img.next()) {
                        img.next().remove();
                    }
                    // no loading indicator when an animation is set
                    if (!img.dom.src.endsWith(Viz.Const.Url.BlankImage) && animationDirection && animationDirection.length == 1) {
                        if (animationDirection != 'f') {
                            img.stopFx();
                            img.sequenceFx();
                            img.slideOut(animationDirection, {
                                        duration : 1
                                    });
                        }
                    }
                    else {
                        // if we reload an image without resizing it, we need to show the loading indicator here
                        // if (!img.dom.src.endsWith(Viz.Const.Url.BlankImage) && this._lastWidth != size.width && this._lastHeight != size.height) {
                        // this.showLoadingIndicator();
                        // }
                        // else {
                        this.makeParentToolbarBusy();
                        // }
                    }

                    if (this.serviceHandler)
                        // case where the image is pre build on the server, added to the cache and then only retreive by client.
                        this._webRequest = this.serviceHandler(Ext.apply({
                                    scope   : this,
                                    width   : size.width,
                                    height  : size.height,
                                    success : function(value) {
                                        this.removeClass(this.loadingClass);
                                        this.clearParentToolbarStatus();
                                        var newSrc = Viz.Services.BuildUrlStream(this.serviceInterface, this.serviceStream, Ext.apply({
                                                            guid : value.Guid
                                                        }, this.serviceParams));
                                        if (!Ext.isEmpty(value.MapText)) {
                                            img.insertSibling(value.MapText, 'after');
                                        }
                                        if (!Ext.isEmpty(value.MapName)) {
                                            img.set({
                                                        useMap : value.MapName
                                                    });
                                        }

                                        if (!img.dom.src.endsWith(Viz.Const.Url.BlankImage) && animationDirection == 'f') {
                                            $(img.dom).flip({
                                                        direction   : 'tb',
                                                        color       : this.getBackgroundColor(),
                                                        onAnimation : function() {
                                                            img.dom.src = newSrc;
                                                        }
                                                    });
                                        }
                                        else
                                            img.dom.src = newSrc;
                                    },
                                    failure : function() {
                                        this.removeClass(this.loadingClass);
                                    }
                                }, this.serviceParams));
                    else {
                        // simple case where the image is directly build and streamed from the server. Does not allow waiting logo.
                        img.dom.src = Viz.Services.BuildUrlStream(this.serviceInterface, this.serviceStream, Ext.apply({
                                            Key         : this.entity ? this.entity[this.entityKey] : null,
                                            width       : this.overrideSize ? this.overrideWidth : size.width,
                                            height      : this.overrideSize ? this.overrideHeight : size.height,
                                            application : Viz.App.Global.User.ApplicationName

                                        }, this.serviceParams));
                        this.removeClass.defer(this.maskDeferDelay, this, [this.loadingClass]);
                    }
                    if (animationDirection && animationDirection.length == 1 && animationDirection != 'f') {
                        img.slideIn(animationDirection, {
                                    duration : 1
                                });
                    }
                    // we store the last valid dimensions.
                    if (size.width > 0)
                        this._lastWidth = size.width;
                    if (size.height > 0)
                        this._lastHeight = size.height;
                }
            },

            getParentToolbar                   : function() {
                var parent = this.findParentByType('vizChartViewer');
                if (parent && parent.getBottomToolbar) {
                    var tb = parent.getBottomToolbar();
                    if (tb && tb.showBusy)
                        return tb;
                }
                return null;
            },

            getParentRefreshFrequency          : function() {
                var retVal = 0;
                var parent = this.findParentByType('vizChartViewer');
                if (parent && parent.entityPortlet) {
                    retVal = parent.entityPortlet.RefreshFrequency;
                }
                return retVal;
            },

            getParentRefreshAnimationDirection : function() {
                var retVal = '';
                var parent = this.findParentByType('vizChartViewer');
                if (parent && parent.entityPortlet) {
                    retVal = parent.entityPortlet.RefreshAnimationDirection;
                }
                return retVal;
            },

            isParentToolbarVisible             : function() {
                var retVal = true;
                var parent = this.findParentByType('vizChartViewer');
                if (parent && parent.entityPortlet) {
                    retVal = parent.entityPortlet.HeaderAndToolbarVisible;
                }
                return retVal;
            },

            makeParentToolbarBusy              : function() {
                var tb = this.getParentToolbar();
                if (tb && tb.showBusy) {
                    if (this.isParentToolbarVisible())
                        tb.showBusy();
                    else {
                        if (this.getParentRefreshFrequency() > 0)
                            return
                        this.mask(' ', 'x-mask-loading-transparent');
                        /*
                         * var mask = this.el.mask(' ', 'x-mask-loading-transparent'); Ext.each(mask.parent().dom.childNodes, function(node) { node.className = node.className.replace("ext-el-mask-msg ", ""); });
                         */
                    }
                }
            },

            clearParentToolbarStatus           : function() {
                var tb = this.getParentToolbar();
                if (tb && tb.clearStatus)
                    if (this.isParentToolbarVisible())
                        tb.clearStatus();
                    else {
                        if (this.getParentRefreshFrequency() > 0)
                            return
                        this.el.unmask();
                    }
            },

            getBackgroundColor                 : function() {
                var img = Ext.get(this._img);
                if (img && img.dom)
                    return Viz.util.Chart.getBackgroundColor(img.dom);
                return null;
            },

            mask                               : function(msg, msgCls) {
                var me = this.el, dom = me.dom, dh = Ext.DomHelper, el, mask;
                var data = Ext.Element.data;
                if (el = data(dom, 'maskMsg')) {
                    el.remove();
                }
                if (el = data(dom, 'mask')) {
                    el.remove();
                }

                mask = dh.append(dom, {
                            cls : ''// "ext-el-mask"
                        }, true);
                data(dom, 'mask', mask);

                // me.addClass('x-masked');
                mask.setDisplayed(true);

                if (typeof msg == 'string') {
                    var mm = dh.append(dom, {
                                cn : {
                                    tag : 'div'
                                }
                            }, true);
                    data(dom, 'maskMsg', mm);
                    mm.dom.className = msgCls;
                    mm.dom.firstChild.innerHTML = msg;
                    mm.setDisplayed(true);
                    mm.center(me);
                }

                // ie will not expand full height automatically
                if (Ext.isIE && !(Ext.isIE7 && Ext.isStrict) && me.getStyle('height') == 'auto') {
                    mask.setSize(undefined, me.getHeight());
                }

                return mask;
            },

            /**
             * Removes a previously applied mask.
             */
            unmask                             : function() {
                var me = this.el, dom = me.dom, mask = data(dom, 'mask'), maskMsg = data(dom, 'maskMsg');
                var data = Ext.Element.data;

                if (mask) {
                    if (maskMsg) {
                        maskMsg.remove();
                        data(dom, 'maskMsg', undefined);
                    }

                    mask.remove();
                    data(dom, 'mask', undefined);
                    // me.removeClass(['x-masked', 'x-masked-relative']);
                }
            }

        });

Ext.reg('vizViewerImage', Viz.viewer.Image);
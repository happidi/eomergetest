﻿Ext.namespace('Viz.tree');

/**
* Summary.
* @class Viz.tree.AzManRole
* @extends Viz.tree.TreePanel
*/
Viz.tree.AzManRole = Ext.extend(Viz.tree.TreePanel, {

    /**
    * Ctor.
    * @param {Object} config The configuration options
    */
    constructor: function (config) {
        config = config || {};
        if ((config.root) && (config.root.onlyRoles)) {
            config.onlyRoles = true;
        } else {
            config.onlyRoles = false;
        }

        var defaultConfig = {
            title: $lang('msg_azmanrole_tree'),
            iconCls: 'viz-icon-small-role'
        };
        var forcedConfig = {

            serviceHandler: Viz.Services.AuthenticationWCF.AzManRole_GetTree,
            serviceParams: {
                onlyRoles: config.onlyRoles
            },
            root: {
                id: ' ',
                iconCls: 'viz-icon-small-role',
                text: $lang('msg_azmanrole_root')
            },
            menu: [{
                text: $lang("msg_azmanrole_add"),
                iconCls: 'viz-icon-small-role-add',
                handler: this.CreateAzManRole,
                scope: this,
                hidden: !$authorized('@@@@ Role - Full Control')
            }, {
                text: $lang("msg_azmanrole_update"),
                iconCls: 'viz-icon-small-role-update',
                handler: this.UpdateAzManRole,
                scope: this,
                hidden: !$authorized('@@@@ Role - Full Control')
            }, {
                text: $lang("msg_azmanrole_delete"),
                iconCls: 'viz-icon-small-role-delete',
                handler: this.DeleteAzManRole,
                scope: this,
                hidden: !$authorized('@@@@ Role - Full Control')


            }, {
                text: $lang("msg_azmanrole_copy"),
                iconCls: 'viz-icon-small-copy',
                handler: this.CopyAzManRole,
                scope: this,
                hidden: !$authorized('@@@@ Role - Full Control')


            }, '-', {
                text: $lang("msg_azmantask_associate"),
                iconCls: 'viz-icon-small-azmantask-add',
                handler: this.AddAssociationAzManTask,
                scope: this,
                hidden: !$authorized('@@@@ Role - Full Control')
            }],
            menuRoot: [{
                text: $lang("msg_azmanrole_add"),
                iconCls: 'viz-icon-small-role-add',
                handler: this.CreateAzManRole,
                scope: this,
                hidden: !$authorized('@@@@ Role - Full Control')
            }],
            menuTask: [{
                text: $lang("msg_azmantask_deassociate"),
                iconCls: 'viz-icon-small-azmantask-delete',
                handler: this.RemoveAssociationAzManTask,
                scope: this,
                hidden: !$authorized('@@@@ Role - Full Control')
            }],

            formConfig: {
                common: {
                    width: 400,
                    height: 200,
                    xtype: 'vizFormAzManRole',
                    messageSaveStart: 'AzManRoleChangeStart',
                    messageSaveEnd: 'AzManRoleChange',
                    mode_load: 'local',
                    readonly: !$authorized('@@@@ Role - Full Control')
                },
                create: {
                    title: $lang("msg_azmanrole_add"),
                    iconCls: 'viz-icon-small-role-add'

                },
                update: {
                    title: $lang("msg_azmanrole_update"),
                    iconCls: 'viz-icon-small-role-update',
                    titleEntity: 'Name',
                    serviceParamsEntity: [{
                        name: 'Key',
                        value: 'Id'
                    }]
                }
            }

        };
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.tree.AzManRole.superclass.constructor.call(this, config);

        this.tools.push({
            id: 'save',
            qtip: $lang('msg_azman_save'),
            handler: function () {
                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                    pollingInterval: 2000,
                    serviceStart: Viz.Services.AuthenticationWCF.BeginAzManExport,
                    isFileResult: true,
                    showProgress: true
                });

                submitter.start();
            },
            scope: this
        });

        // register to AzManChangeStart and AzManChange.
       // Viz.util.MessageBusMgr.subscribe("AzManRoleChangeStart", this.Mask, this);
        Viz.util.MessageBusMgr.subscribe("AzManRoleChange", this.ReloadNode, this);
        /*
        * // ***** bof multiple nodes with automatic ids.****** // takes care of the fact that the id of nodes task and operation are automatically calculated to enable multiple presence of these nodes. this.on("beforeappend", function(tree, parent, node) { if (node.attributes.entity.ItemType == 'Task' || node.attributes.entity.ItemType == "Operation") node.id = Ext.id(); if (node.attributes.entity.ItemType == 'Task' || node.attributes.entity.ItemType == "Operation") delete node.attributes.checked; return true; }, this); this.on("beforeloadloader", function(obj, node, callback, scope) { var o = { Key : node.attributes.entity ? node.attributes.entity.Id : node.id, success : obj.handleResponse, failure : obj.handleFailure, scope : obj, argument : { node : node, callback : callback, scope :
        * scope || obj } }; Ext.applyIf(o, obj.serviceParams); obj.serviceHandler(o); return false; }, this); // ****** eof multiple nodes with automatic ids.******
        */
    },

    /**
    * @private
    */
    onDestroy: function () {
        //Viz.util.MessageBusMgr.unsubscribe("AzManRoleChangeStart", this.Mask, this);
        Viz.util.MessageBusMgr.unsubscribe("AzManRoleChange", this.ReloadNode, this);
        Viz.tree.AzManRole.superclass.onDestroy.apply(this, arguments);
    },

    /**
    * Menu handler for creating a new AzManRole.
    */
    CreateAzManRole: function () {
        var node = this.getSelectionModel().getSelectedNode();
        var parentId;
        if (node.attributes.entity)
            parentId = node.attributes.entity.Id;
        else
            parentId = node.id;
        var entityParent = {
            ParentId: parentId
        };
        var formConfig = this.formConfig;
        formConfig.common.messageSaveEndParams = node.id;
        this.openFormCrud(formConfig, 'create', null, entityParent);
    },

    /**
    * Menu handler for updating an existing AzManRole.
    */
    UpdateAzManRole: function () {
        var formConfig = this.formConfig;
        this.onUpdateItem(formConfig);
    },

    /**
    * Menu handler for deleting an existing AzManRole.
    */
    DeleteAzManRole: function () {
        this.onDeleteItem(Viz.Services.AuthenticationWCF.AzManRole_Delete);
    },

    /**
    * Menu handler for creating a copy of an existing AzManRole.
    */
    CopyAzManRole: function () {
        var node = this.getSelectionModel().getSelectedNode();
        var role = node.attributes.entity;
        Viz.Services.AuthenticationWCF.AzManRole_Copy({ item: role,
            success: function () {
                if (node.parentNode.parentNode) {
                    node.parentNode.parentNode.reload();
                } else {
                    node.parentNode.reload();
                }

            }
        });

    },

    /**
    * Menu handler for adding tasks to an AzManRole.
    */
    AddAssociationAzManTask: function () {
        var node = this.getSelectionModel().getSelectedNode();
        var item = node.attributes.entity;
        this.store = new Viz.store.AzManTask();
        var componentId = Ext.id();
        var win = new Ext.Window({
            title: $lang("msg_azmantask_associate"),
            iconCls: 'viz-icon-small-azmantask',
            width: 500,
            height: 400,
            bodyStyle: 'padding: 10px;background-color: transparent;',
            layout: 'fit',
            items: [{
                id: componentId,
                xtype: 'vizGrid',
                stateId: 'vizTreeAzManRole.GridAzManTask',
                hasRowEditor: false,
                hasToolbar: false,
                hasPagingToolbar: false,
                border: true,
                stripeRows: false,
                store: this.store,
                columns: Viz.Configuration.Columns.AzManItem,
                tbar: [{
                    text: $lang('msg_refresh'),
                    iconCls: 'x-tbar-loading',
                    handler: function () {
                        store.reload();
                    },
                    scope: this
                }, {
                    xtype: 'checkbox',
                    boxLabel: $lang("msg_only_ui_tasks"),
                    value: true,
                    checked: true,
                    listeners: {
                        check: function (checkbox, checked) {
                            this.store.serviceParams.onlyUITasks = checked;
                            this.store.reload();
                        },
                        scope: this
                    }

                }],
                buttons: [{
                    text: $lang('msg_add'),
                    iconCls: 'viz-icon-small-azmantask-add',
                    handler: function () {
                        var comp = Ext.getCmp(componentId);
                        var s = comp.getSelectionModel().getSelections();
                        var children = [];
                        for (var i = 0, r; r = s[i]; i++) {
                            children.push(r.data);
                        }
                        Viz.Services.AuthenticationWCF.AzManItem_AddMembers({
                            parent: item,
                            children: children,
                            success: function (value) {
                                node.reload();
                                win.close();
                            }
                        });
                    },
                    scope: this
                }]
            }]
        });
        win.show();
    },

    /**
    * Menu handler for removing a task off an AzManRole.
    */
    RemoveAssociationAzManTask: function () {
        var node = this.getSelectionModel().getSelectedNode();
        var children = [].concat(node.attributes.entity);
        var parent = node.parentNode.attributes.entity;
        Viz.Services.AuthenticationWCF.AzManItem_RemoveMembers({
            parent: parent,
            children: children,
            success: function (value) {
                node.parentNode.reload();
            }
        });
    },

    /**
    * Handles the beforecontextmenu event and gives a chance to change the menu
    * @param {Viz.tree.AzManFilter} tree
    * @param {Ext.tree.TreeNode} node
    */
    onBeforeContextMenu: function (tree, node) {
        if (node.isRoot)
            this.ctxMenu = new Ext.menu.Menu({
                items: this.menuRoot
            });
        else {
            if (node.attributes.entity.ItemType == 'Task')
                this.ctxMenu = new Ext.menu.Menu({
                    items: this.menuTask
                });
            else if (node.attributes.entity.ItemType == 'Role')
                this.ctxMenu = new Ext.menu.Menu({
                    items: this.menu
                });
            else
                this.ctxMenu = null;

        }
    }

    /**
    * Handles the beforeappend event and gives a chance to change the nodes
    * @param {Viz.tree.AzManFilter} tree
    * @param {Ext.tree.TreeNode} parent
    * @param {Ext.tree.TreeNode} node
    */
    /*
    * onBeforeAppend : function(tree, parent, node) { if (node.attributes.entity.ItemType == 'Task' || node.attributes.entity.ItemType == "Operation") node.id = Ext.id(); }
    */
});

Ext.reg('vizTreeAzManRole', Viz.tree.AzManRole);
﻿Ext.namespace('Viz.tree');
/**
 * Summary.
 * @class Viz.tree.MeterClassification
 * @extends Viz.tree.TreePanel
 */
Viz.tree.MeterClassification = Ext.extend(Viz.tree.TreePanel, {
            /**
             * @cfg {Object} root the root node
             */
            /**
             * @cfg {string} KeyChart the Key of the Chart in order to filter the list of classification based on the Chart FilterSpatial.
             */
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var rootEntity = Viz.Configuration.GetClassificationItemDefinitionEntityFromCategory("meter");
                var defaultConfig = {
                    title          : $lang('msg_tree_meterclassification'),
                    iconCls        : 'viz-icon-small-meter',
                    rootVisible    : false,
                    pathSeparator  : ' / ',
                    serviceHandler : Viz.Services.EnergyWCF.MeterClassification_GetTree,
                    root           : {
                        id      : rootEntity.KeyClassificationItem,
                        entity  : rootEntity,
                        iconCls : 'viz-icon-small-meter',
                        text    : rootEntity.Title
                    },
                    serviceParams  : {
                        KeyChart               : config.KeyChart || "",
                        KeyAlarmDefinition     : config.KeyAlarmDefinition || "",
                        KeyMeterValidationRule : config.KeyMeterValidationRule || "",
                        filterSpatial          : config.filterSpatial,
                        existing               : config.existing === true ? true : false
                    }
                };
                var forcedConfig = {};
                config.plugins = [new Viz.plugins.RapidSearch({
                            store          : {
                                xtype          : 'vizStoreRapidSearch',
                                serviceHandler : Viz.Services.CoreWCF.ClassificationItem_GetStore,
                                baseParams     : {
                                    filter  : 'LocalId Title Code',
                                    filters : [{
                                                data  : {
                                                    type  : 'string',
                                                    value : Viz.Configuration.GetClassificationItemDefinitionCodeFromCategory('meter')
                                                },
                                                field : 'Code'
                                            }]
                                }
                            },
                            displayField   : 'Title',
                            pathTypeString : $lang("msg_classificationitem_path"),
                            valueField     : 'LocalId',
                            searchName     : 'Meter Classifications'
                        })];
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.tree.MeterClassification.superclass.constructor.call(this, config);
            },
            /**
             * @private OnDestroy.
             */
            onDestroy   : function() {
                Viz.tree.MeterClassification.superclass.onDestroy.apply(this, arguments);
            }
        });
Ext.reg('vizTreeMeterClassification', Viz.tree.MeterClassification);
﻿Ext.namespace('Viz.tree');

/**
 * Summary.
 * @class Viz.tree.Report
 * @extends Viz.tree.TreePanel
 */
Viz.tree.Report = Ext.extend(Viz.tree.TreePanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                 : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title          : $lang('msg_reports'),
                    iconCls        : 'viz-icon-small-report',
                    serviceHandler : Viz.Services.ReportingWCF.Reporting_GetTree,
                    root           : {
                        id      : '/',
                        iconCls : 'viz-icon-small-report',
                        text    : $lang('msg_reports')
                    },
                    enableDD       : true,
                    ddGroup        : 'Report',
                    dropConfig     : {
                        ddGroup            : 'Report',
                        allowContainerDrop : true
                    }
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                this.reportMenu = [{
                            text    : $lang("msg_report_display"),
                            iconCls : 'viz-icon-small-display',
                            handler : this.onDisplayReport,
                            scope   : this
                        }, {
                            text    : $lang("msg_report_manage_my_subscriptions"),
                            iconCls : 'viz-icon-small-settings',
                            handler : this.onManageReportSubscriptions,
                            scope   : this
                        }, {
                            text    : $lang("msg_report_rename"),
                            iconCls : 'viz-icon-small-update',
                            handler : this.onRenameReport,
                            scope   : this
                        }, {
                            text    : $lang("msg_report_delete_menu"),
                            iconCls : 'viz-icon-small-delete',
                            handler : this.onDeleteReport,
                            scope   : this
                        }];

                this.folderMenu = [{
                            text    : $lang("msg_folder_create_menu"),
                            iconCls : 'viz-icon-small-add',
                            handler : this.onCreateFolder,
                            scope   : this
                        }, {
                            text    : $lang("msg_folder_rename_menu"),
                            iconCls : 'viz-icon-small-update',
                            handler : this.onRenameFolder,
                            scope   : this
                        }, {
                            text    : $lang("msg_folder_delete_menu"),
                            iconCls : 'viz-icon-small-delete',
                            handler : this.onDeleteFolder,
                            scope   : this
                        }];

                this.rootMenu = [{
                            text    : $lang("msg_folder_create_menu"),
                            iconCls : 'viz-icon-small-add',
                            handler : this.onCreateFolder,
                            scope   : this
                        }];

                Viz.tree.Report.superclass.constructor.call(this, config);

                // register to ReportChangeStart and ReportChange.
                Viz.util.MessageBusMgr.subscribe("ReportChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.subscribe("ReportChange", this.ReloadNode, this);
            },

            /**
             * @private OnDestroy.
             */
            onDestroy                   : function() {
                Viz.util.MessageBusMgr.unsubscribe("ReportChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.unsubscribe("ReportChange", this.ReloadNode, this);
                Viz.tree.Report.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * @private A function for renaming/moving an item.
             */
            moveItem                    : function(node, newPath) {
                var parentNode = node.parentNode;

                Viz.Services.ReportingWCF.MoveItem({
                            path    : node.attributes.Key,
                            newPath : newPath,
                            success : function() {
                                this.getSelectionModel().select(parentNode);
                                Viz.tree.Report.superclass.doRefresh.apply(this);
                            }.createDelegate(this)
                        });
            },
            /**
             * Menu handler for displaying a Report.
             */
            onDisplayReport             : function() {
                var node = this.getSelectionModel().getSelectedNode();

                Viz.App.Global.ViewPort.openModule({
                            xtype : "vizPanelReportViewer",
                            path  : node.attributes.Key

                        });
            },
            /**
             * Menu handler for managing a Report's subscriptions.
             */
            onManageReportSubscriptions : function() {
                var node = this.getSelectionModel().getSelectedNode();

                var window = new Ext.Window({
                            layout  : 'fit',
                            title    : $lang('msg_report_manage_my_subscriptions'),
                            iconCls : 'viz-icon-small-settings',
                            width   : 740,
                            height  : 250,
                            items   : {
                                xtype      : 'vizGridReportSubscription',
                                reportPath : node.attributes.Key
                            }
                        });
                window.show();

            },
            /**
             * Menu handler for renaming a Report.
             */
            onRenameReport              : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var parentNode = node.parentNode;

                Ext.MessageBox.prompt($lang("msg_report_rename_title"), $lang("msg_report_rename_prompt"), function(btn, text) {
                            if (btn == "ok") {
                                var newPath = (parentNode.isRoot ? '' : parentNode.attributes.Key) + '/' + text;
                                this.moveItem(node, newPath);
                            }
                        }, this, false, node.attributes.text);

            },
            /**
             * Menu handler for deleting a Report.
             */
            onDeleteReport              : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var parentNode = node.parentNode;

                Ext.MessageBox.confirm($lang("msg_report_delete_title"), $lang("msg_report_delete_confirmation_single"), function(btn, text) {
                            if (btn == "yes") {
                                Viz.Services.ReportingWCF.DeleteReport({
                                            path    : node.attributes.Key,
                                            success : function() {
                                                this.getSelectionModel().select(parentNode);
                                                Viz.tree.Report.superclass.doRefresh.apply(this);
                                            }.createDelegate(this)
                                        });
                            }
                        }, this);
            },
            /**
             * Menu handler for renaming a Folder.
             */
            onRenameFolder              : function() {
                var node = this.getSelectionModel().getSelectedNode();
                if (node.isRoot)
                    return;

                var parentNode = node.parentNode;

                Ext.MessageBox.prompt($lang("msg_folder_rename_title"), $lang("msg_folder_rename_prompt"), function(btn, text) {
                            if (btn == "ok") {
                                var newPath = (parentNode.isRoot ? '' : parentNode.attributes.Key) + '/' + text;
                                this.moveItem(node, newPath);
                            }
                        }, this, false, node.attributes.text);
            },
            /**
             * Menu handler for creating a new Folder.
             */
            onCreateFolder              : function() {
                var node = this.getSelectionModel().getSelectedNode();

                Ext.MessageBox.prompt($lang("msg_folder_create_title"), $lang("msg_folder_create_prompt"), function(btn, text) {
                            if (btn == "ok") {
                                Viz.Services.ReportingWCF.CreateFolder({
                                            parentPath : node.isRoot ? '/' : node.attributes.Key,
                                            folderName : text,
                                            success    : function() {
                                                Viz.tree.Report.superclass.doRefresh.apply(this);
                                            }.createDelegate(this)
                                        });
                            }
                        }, this, false);
            },
            /**
             * Menu handler for deleting a Folder.
             */
            onDeleteFolder              : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var parentNode = node.parentNode;

                Ext.MessageBox.confirm($lang("msg_folder_delete_title"), $lang("msg_folder_delete_confirmation_single"), function(btn, text) {
                            if (btn == "yes") {
                                Viz.Services.ReportingWCF.DeleteFolder({
                                            path    : node.attributes.Key,
                                            success : function() {
                                                this.getSelectionModel().select(parentNode);
                                                Viz.tree.Report.superclass.doRefresh.apply(this);
                                            }.createDelegate(this)
                                        });
                            }
                        }, this);
            },
            /**
             * Handles the beforecontextmenu event and gives a chance to change the menu
             * @param {Viz.tree.Report} tree
             * @param {Ext.tree.TreeNode} node
             */
            onBeforeContextMenu         : function(tree, node) {
                if (node.isRoot) {
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.rootMenu
                            });
                }
                else if (node.attributes && node.attributes.entity && node.attributes.entity.TypeName == "Report") {
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.reportMenu
                            });
                }
                else {
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.folderMenu
                            });
                }
            },

            /**
             * Refreshes the tree.
             */
            doRefresh                   : function() {
                var node = this.getSelectionModel().getSelectedNode();

                // Does the current node represent a file?
                if (node && node.leaf === true) {
                    // We can't refresh a file.
                    return;
                }

                Viz.tree.Report.superclass.doRefresh.apply(this);
            },
            /**
             * Called while dragging over.
             * @private
             * @param {Object} e dd event
             */
            onNodeDragOver              : function(e) {
                if (e.target && e.target.attributes && e.target.attributes.entity && e.target.attributes.entity.TypeName == "Folder") {
                    e.cancel = false;
                }
                else {
                    e.cancel = true;
                }
            },
            /**
             * Changes the parent of an item after it was dropped on a node. This is an override although the base class thinks there is no need to override this generic function, as it is only generic for IFC entities. :(
             * @param {Object} nodeSource The source node.
             * @param {Object} nodeTarget The target node.
             * @param {Viz.tree.TreePanel} tree The tree.
             */
            changeParent                : function(nodeSource, nodeTarget, tree) {
                tree.Mask();

                Viz.Services.ReportingWCF.MoveItem({
                            path    : nodeSource.attributes.Key,
                            newPath : nodeTarget.attributes.Key + "/" + nodeSource.attributes.text,
                            success : function() {
                                nodeSource.remove();
                                nodeTarget.reload();
                                tree.UnMask();
                            }.createDelegate(this),
                            failure : function() {
                                tree.UnMask();
                            }.createDelegate(this)
                        });
            }

        });

Ext.reg('vizTreeReport', Viz.tree.Report);

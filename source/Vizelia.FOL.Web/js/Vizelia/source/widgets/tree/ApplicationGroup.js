﻿Ext.namespace('Viz.tree');

/**
 * Summary.
 * @class Viz.tree.ApplicationGroup
 * @extends Viz.tree.TreePanel
 */
Viz.tree.ApplicationGroup = Ext.extend(Viz.tree.TreePanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor            : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title          : $lang('msg_applicationgroup_tree'),
                    iconCls        : 'viz-icon-small-applicationgroup',
                    serviceHandler : Viz.Services.AuthenticationWCF.ApplicationGroup_GetTree,
                    root           : {
                        id      : ' ',
                        iconCls : 'viz-icon-small-applicationgroup',
                        text    : $lang('msg_applicationgroup_root')
                    },
                    menu           : [{
                                text    : $lang("msg_applicationgroup_update"),
                                iconCls : 'viz-icon-small-applicationgroup-update',
                                handler : this.UpdateApplicationGroup,
                                scope   : this
                            }, {
                                text    : $lang("msg_applicationgroup_delete"),
                                iconCls : 'viz-icon-small-applicationgroup-delete',
                                handler : this.DeleteApplicationGroup,
                                scope   : this
                            }],
                    menuRoot       : [{
                                text    : $lang("msg_applicationgroup_add"),
                                iconCls : 'viz-icon-small-applicationgroup-add',
                                handler : this.CreateApplicationGroup,
                                scope   : this
                            }],
                    formConfig     : {
                        common : {
                            width            : 500,
                            height           : 200,
                            xtype            : 'vizFormApplicationGroup',
                            messageSaveStart : 'ApplicationGroupChangeStart',
                            messageSaveEnd   : 'ApplicationGroupChange'
                        },
                        create : {
                            title   : $lang("msg_applicationgroup_add"),
                            iconCls : 'viz-icon-small-applicationgroup-add'

                        },
                        update : {
                            title               : $lang("msg_applicationgroup_update"),
                            iconCls             : 'viz-icon-small-applicationgroup-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'Id'
                                    }]
                        }
                    }

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.tree.ApplicationGroup.superclass.constructor.call(this, config);
                this.tools = this.tools || [];

                // register to ApplicationGroupChangeStart and ApplicationGroupChange.
                Viz.util.MessageBusMgr.subscribe("ApplicationGroupChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.subscribe("ApplicationGroupChange", this.ReloadNode, this);
            },

            /**
             * @private OnDestroy.
             */
            onDestroy              : function() {
                Viz.util.MessageBusMgr.unsubscribe("ApplicationGroupChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.unsubscribe("ApplicationGroupChange", this.ReloadNode, this);
                Viz.tree.ApplicationGroup.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * Menu handler for creating a new ApplicationGroup.
             */
            CreateApplicationGroup : function() {
                this.onAddItem(this.formConfig);
            },

            /**
             * Menu handler for updating an existing ApplicationGroup.
             */
            UpdateApplicationGroup : function() {
                this.onUpdateItem(this.formConfig);
            },

            /**
             * Menu handler for deleting an existing ApplicationGroup.
             */
            DeleteApplicationGroup : function() {
                this.onDeleteItem(Viz.Services.AuthenticationWCF.ApplicationGroup_Delete, 'ApplicationGroupChangeStart', 'ApplicationGroupChange');
            },

            /**
             * Handles the beforecontextmenu event and gives a chance to change the menu
             * @param {Viz.tree.AzManFilter} tree
             * @param {Ext.tree.TreeNode} node
             */
            onBeforeContextMenu    : function(tree, node) {
                if (node.isRoot)
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.menuRoot
                            });
                else
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.menu
                            });
            }
        });

Ext.reg('vizTreeApplicationGroup', Viz.tree.ApplicationGroup);

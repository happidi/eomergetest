Ext.namespace("Viz.tree");

/**
 * @class Viz.tree.TreeLoaderWCF
 * @extends Ext.tree.TreeLoader
 * <p>
 * A TreeLoader that connects to a WCF service
 * </p>
 * @constructor Creates a new TreeloaderWCF.
 * @param {Object} config A config object containing config properties.
 */
Viz.tree.TreeLoaderWCF = Ext.extend(Ext.tree.TreeLoader, {
            /**
             * @cfg {String} serviceHandler The service function from which to request a Json string which specifies an array of node definition objects representing the child nodes to be loaded.
             */
            /**
             * @cfg {String} serviceParams The service parameters to pass to the service handler.
             */
            /**
             * @cfg {Boolean} displayCheckbox True to force display of checkboxes on nodes.
             */
            displayCheckbox       : false,

            /**
             * @cfg {Array} checkboxExcludedTypes an array of types to prevent of having a checkbox. IE ['Vizelia.FOL.BusinessEntities.Meter'].
             */
            checkboxExcludedTypes : [],

            /**
             * Load an {@link Ext.tree.TreeNode} from the URL specified in the constructor. This is called automatically when a node is expanded, but may be used to reload a node (or append new children if the {@link #clearOnLoad} option is false.)
             * @param {Ext.tree.TreeNode} node
             * @param {Function} callback
             * @param {Object} scope
             */
            load                  : function(node, callback, scope) {
                if (this.clearOnLoad) {
                    while (node.firstChild) {
                        node.removeChild(node.firstChild);
                    }
                }
                if (this.doPreload(node)) { // preloaded json children
                    if (typeof callback == "function") {
                        callback.call(scope || node);
                    }
                }
                else if (this.serviceHandler) {
                    this.requestData(node, callback, scope);
                }
            },

            requestData           : function(node, callback, scope) {
                if (this.fireEvent("beforeloadloader", this, node, callback, scope) !== false) {

                    var o = {
                        Key      : node.attributes.Key || node.id,
                        entity   : node.attributes.entity,
                        success  : this.handleResponse,
                        failure  : this.handleFailure,
                        scope    : this,
                        argument : {
                            node     : node,
                            callback : callback,
                            scope    : scope || this
                        }
                    };
                    Ext.applyIf(o, this.serviceParams);
                    // this.serviceHandler(o);

                    this.lastRequest = this.serviceHandler.call(scope || this, o);
                    this.lastRequest.params = o;
                }
                else {
                    // if the load is cancelled, make sure we notify
                    // the node that we are done
                    if (typeof callback == "function") {
                        callback();
                    }
                }
            },

            handleResponse        : function(response, argument) {
                var a = argument;
                this.processResponse(response, a.node, a.callback, a.scope);
                this.fireEvent("load", this, a.node, response);
            },

            processResponse       : function(value, node, callback, scope) {
                try {
                    var o = value;
                    node.beginUpdate();
                    node.ui.onTextChange(node, node.text + " <span style='color:gray;display:inline-block' >(" + o.length + ")</span>", node.text);
                    for (var i = 0, len = o.length; i < len; i++) {
						if (o[i].entity && o[i].entity.LocalId)
						{
							Ext.copyTo(o[i], o[i].entity, 'LocalId');
						}
                        if ((this.displayCheckbox) && (o[i].checked == undefined)) {
                            if (!Ext.isArray(this.checkboxExcludedTypes) || this.checkboxExcludedTypes.length == 0 || !o[i].entity) {
                                o[i].checked = false;
                            }
                            else if (!Array.contains(this.checkboxExcludedTypes, Viz.getEntityType(o[i].entity.__type))) {
                                o[i].checked = false;
                            }
                        }
                        if ((this.disableCheckboxForAncestors) && (o[i].Filtertype == Vizelia.FOL.BusinessEntities.FilterType.Ascendant)) {
                            delete o[i].checked;
                        }

                        var n = this.createNode(o[i]);
                        // n.checked = true;
                        if (n) {
                            node.appendChild(n);
                        }
                    }
                    node.endUpdate();
                    if (typeof callback == "function") {
                        callback.call(scope, this, node);
                    }
                }
                catch (e) {
// handleFailure is overridden in Overrides.js to take a second parameter that can supply the elements shown here
                    this.handleFailure(value, {'node': node, 'callback' : callback, 'scope': scope} );
                }
            },

            doPreload             : function(node) {
                if (node.attributes.children) {
                    if (node.childNodes.length < 1) { // preloaded?
                        var cs = node.attributes.children;
                        node.beginUpdate();
                        for (var i = 0, len = cs.length; i < len; i++) {
                            if (this.displayCheckbox) {
                                if (!Ext.isArray(this.checkboxExcludedTypes) || this.checkboxExcludedTypes.length == 0 || !cs[i].entity) {
                                    cs[i].checked = false;
                                }
                                else if (!Array.contains(this.checkboxExcludedTypes, Viz.getEntityType(cs[i].entity.__type))) {
                                    cs[i].checked = false;
                                }
                            }
                            var cn = this.createNode(cs[i]);
                            var cn = node.appendChild(cn);
                            if (this.preloadChildren) {
                                this.doPreload(cn);
                            }
                        }
                        node.endUpdate();
                    }
                    return true;
                }
                return false;
            },

            /*
             * Template method intended to be overridden by subclasses that need to provide custom attribute processing prior to the creation of each TreeNode. This method will be passed a config object containing existing TreeNode attribute name/value pairs which can be modified as needed directly (no need to return the object).
             */
            processAttributes     : Ext.emptyFn
        });

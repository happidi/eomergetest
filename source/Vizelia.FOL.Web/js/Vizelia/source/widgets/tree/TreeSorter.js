﻿Ext.namespace("Viz.tree");

/**
 * @class Viz.tree.TreeSorter
 * @extends Ext.tree.TreeSorter Extends the {@link Ext.tree.TreeSorter} so we can use property as a json path, much like we do with Ext.data.JsonReader
 */
Viz.tree.TreeSorter = Ext.extend(Ext.tree.TreeSorter, {
            /**
             * Ctor. Makes use of Ext.data.JsonReader.prototype.getJsonAccessor
             * @param {Ext.tree.TreePanel} tree
             * @param {Object} config
             */
            constructor : function(tree, config) {
                Ext.apply(this, config);
                tree.on("beforechildrenrendered", this.doSort, this);
                tree.on("append", this.updateSort, this);
                tree.on("insert", this.updateSort, this);
                tree.on("textchange", this.updateSortParent, this);

                var dsc = this.dir && this.dir.toLowerCase() == "desc";
                var p = this.property || "text";
                var sortType = this.sortType;
                var fs = this.folderSort;
                var cs = this.caseSensitive || true;
                var leafAttr = this.leafAttr || 'leaf';

                this.sortFn = function(n1, n2) {
                    if (fs) {
                        if (n1.attributes[leafAttr] && !n2.attributes[leafAttr]) {
                            return 1;
                        }
                        if (!n1.attributes[leafAttr] && n2.attributes[leafAttr]) {
                            return -1;
                        }
                    }
                    // change here
                    var reader = Ext.data.JsonReader.prototype.getJsonAccessor(p);
                    var v1 = sortType ? sortType(n1) : (cs ? reader(n1.attributes) : reader(n1.attributes).toUpperCase());
                    var v2 = sortType ? sortType(n2) : (cs ? reader(n2.attributes) : reader(n2.attributes).toUpperCase());
                    if (v1 < v2) {
                        return dsc ? +1 : -1;
                    }
                    else if (v1 > v2) {
                        return dsc ? -1 : +1;
                    }
                    else {
                        return 0;
                    }
                };
            }

        });
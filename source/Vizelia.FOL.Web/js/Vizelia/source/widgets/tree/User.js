﻿Ext.namespace("Viz.tree");

Ext.override(Ext.tree.TreePanel, {
            checkAll : function(value, startNode) {
                startNode = startNode || this.root;

                var f = function() {
                    if (!this.attributes.checked == value) {
                        this.getUI().toggleCheck();
                    }
                };
                startNode.cascade(f);
            }

        });

/**
 * @class Viz.tree.User
 * @extends Ext.tree.TreePanel The tree user management.
 */
Viz.tree.User = Ext.extend(Ext.tree.TreePanel, {
            iconCls                     : 'viz-icon-small-user-online',
            rootVisible                 : false,
            useArrows                   : true,
            autoScroll                  : true,
            loader                      : new Viz.tree.TreeLoaderWCF({
                        displayCheckbox : true,
                        serviceHandler  : Viz.Services.AuthenticationWCF.User_GetTree
                    }),
            root                        : {
                id : ''
            },

            /**
             * Ctor
             * @param {Object} config
             */
            constructor                 : function(config) {

                config = config || {};
                var defaultConfig = {
                    title : $lang('msg_user_management')
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, {
                            selModel : new Ext.tree.MultiSelectionModel(),
                            tbar     : [{
                                        iconCls   : 'viz-icon-small-user-add',
                                        listeners : {
                                            render : function(comp) {
                                                this.checkboxToolTip = new Ext.ToolTip({
                                                            target       : comp.getEl(),
                                                            anchor       : 'top',
                                                            anchorOffset : -10,
                                                            // width : 200,
                                                            html         : $lang('msg_user_add')
                                                        });
                                            }
                                        }
                                    }, {
                                        iconCls   : 'viz-icon-small-user-delete',
                                        listeners : {
                                            render : function(comp) {
                                                this.checkboxToolTip = new Ext.ToolTip({
                                                            target       : comp.getEl(),
                                                            anchor       : 'top',
                                                            anchorOffset : -10,
                                                            // width : 200,
                                                            html         : 'Deletes selected nodes'
                                                        });
                                            }
                                        }
                                    }, {
                                        xtype     : 'checkbox',
                                        checked   : false,
                                        listeners : {
                                            check       : function(checkbox, value) {
                                                this.checkAll(value);
                                            },
                                            afterrender : function(comp) {
                                                this.checkboxToolTip = new Ext.ToolTip({
                                                            target       : comp.getEl(),
                                                            anchor       : 'top',
                                                            anchorOffset : -10,
                                                            // width : 200,
                                                            html         : 'Check / uncheck the nodes'
                                                        });
                                            },
                                            scope       : this
                                        }
                                    }],
                            tools    : [{
                                        id      : 'refresh',
                                        handler : this.onUserManagementChange,
                                        scope   : this
                                    }, {
                                        id      : 'up',
                                        handler : function() {
                                            if (this.sorter) {
                                                this.un("beforechildrenrendered", this.sorter.doSort, this);
                                                this.un("append", this.sorter.updateSort, this);
                                                this.un("insert", this.sorter.updateSort, this);
                                                this.un("textchange", this.sorter.updateSortParent, this);
                                            }
                                            // Create new sorter
                                            this.sorter = new Viz.tree.TreeSorter(this, {
                                                        folderSort : true,
                                                        dir        : this.sorter ? this.sorter.dir.toggle("asc", "desc") : "asc",
                                                        property   : "entity.LastLoginDate"
                                                    });
                                            // Resort all nodes with children
                                            this.getRootNode().cascade(function(node) {
                                                        if (node.childrenRendered) {
                                                            this.sorter.doSort(node);
                                                        }
                                                    }, this);
                                        },
                                        scope   : this
                                    }]
                        });
                Viz.tree.User.superclass.constructor.call(this, config);
                // register to UserManagementChangeStart and UserManagementChange
                Viz.util.MessageBusMgr.subscribe("UserManagementChangeStart", this.onUserManagementChangeStart, this);
                Viz.util.MessageBusMgr.subscribe("UserManagementChange", this.onUserManagementChange, this);
            },

            /**
             * Indicates that a user operation is starting. It should be followed by onUserManagementChange.
             */
            onUserManagementChangeStart : function() {
                this.getEl().mask();
            },

            /**
             * Indicates that a user operation occured and user panel should be reload.
             */
            onUserManagementChange      : function() {
                this.getEl().mask();
                this.getLoader().load(this.getRootNode(), function() {
                            this.getEl().unmask();
                        }, this);
            },

            /**
             * Event handler for contextmenu.
             * @param {Ext.tree.TreeNode} node
             * @param {Ext.EventObject} event
             */
            onContextMenu               : function(node, event) {
                event.stopEvent();
                // node.select();
                if (!node.isSelected())
                    node.select();
                if (!this.ctxMenu) {
                    this.ctxMenu = new Ext.menu.Menu({
                                items : [{
                                            text    : $lang('msg_user_update'),
                                            iconCls : 'viz-icon-small-user-update',
                                            handler : this.onUpdateUser,
                                            scope   : this
                                        }, {
                                            text    : $lang('msg_user_add'),
                                            iconCls : 'viz-icon-small-user-add',
                                            handler : this.onCreateUser,
                                            scope   : this
                                        }, {
                                            text    : $lang('msg_user_delete'),
                                            iconCls : 'viz-icon-small-user-delete',
                                            handler : this.onDeleteUser,
                                            scope   : this
                                        }]
                            });
                }
                this.ctxMenu.showAt(event.getXY());
            },

            /**
             * Opens the CreateUser form.
             */
            onCreateUser                : function() {
                var winUser = new Ext.Window({
                            title   : $lang('msg_user_add'),
                            iconCls : 'viz-icon-small-user-add',
                            width   : 450,
                            height  : 400,
                            layout  : 'fit',
                            items   : [{
                                        xtype : 'vizFormUser'
                                    }]
                        });
                winUser.show();

            },

            /**
             * Opens the UpdateUser form.
             */
            onUpdateUser                : function() {
                var nodeUser = this.getSelectionModel().getSelectedNodes()[0];

                nodeUser.select();
                var winUser = new Ext.Window({
                            title   : $lang('msg_user_info') + ' : ' + nodeUser.attributes.entity.UserName,
                            iconCls : 'viz-icon-small-user-update',
                            width   : 450,
                            height  : 400,
                            layout  : 'fit',
                            // animateTarget : nodeUser.getUI().getEl(),
                            items   : [{
                                        xtype : 'vizFormUser',
                                        user  : nodeUser.attributes.entity
                                    }]
                        });
                winUser.show();
            },

            /**
             * Deletes a user.
             */
            onDeleteUser                : function() {
                var nodes = this.getSelectionModel().getSelectedNodes();
                var usernames = [];
                Ext.each(nodes, function(item) {
                            usernames.push(item.attributes.entity.UserName);
                        });

                if (usernames.length == 0)
                    return;

                var msgDelete;
                if (usernames.length > 1)
                    msgDelete = "Are you sure you want to delete the " + usernames.length + " selected users ?";
                else
                    msgDelete = "Are you sure you want to delete the user <b>" + usernames[0] + "</b> ?"

                Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                            if (button == 'yes') {
                                Viz.util.MessageBusMgr.publish('UserManagementChangeStart');
                                Viz.Services.AuthenticationWCF.User_DeleteList({
                                            usernames : usernames,
                                            success   : function() {
                                                Viz.util.MessageBusMgr.publish('UserManagementChange');
                                            }
                                        });
                            }
                        }, this);
            }
        });

Ext.reg("vizTreeUser", Viz.tree.User);

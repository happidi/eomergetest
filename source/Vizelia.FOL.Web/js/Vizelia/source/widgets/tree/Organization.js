﻿Ext.namespace('Viz.tree');

/**
 * Summary.
 * @class Viz.tree.Organization
 * @extends Viz.tree.TreePanel
 */
Viz.tree.Organization = Ext.extend(Viz.tree.TreePanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor         : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title          : $lang('msg_tree_organization'),
                    iconCls        : 'viz-icon-small-organization',
                    serviceHandler : Viz.Services.CoreWCF.Organization_GetTree,
                    pathSeparator  : ' / ',
                    root           : {
                        id      : ' ',
                        iconCls : 'viz-icon-small-organization',
                        text    : $lang('msg_tree_organization_root')
                    },
                    menuRoot       : [{
                                text    : $lang("msg_organization_add"),
                                iconCls : 'viz-icon-small-organization-add',
                                handler : this.CreateOrganization,
                                scope   : this
                            }],
                    menu           : [{
                                text    : $lang("msg_organization_add"),
                                iconCls : 'viz-icon-small-organization-add',
                                handler : this.CreateOrganization,
                                scope   : this
                            }, {
                                text    : $lang("msg_organization_update"),
                                iconCls : 'viz-icon-small-organization-update',
                                handler : this.UpdateOrganization,
                                scope   : this
                            }, {
                                text    : $lang("msg_organization_delete"),
                                iconCls : 'viz-icon-small-organization-delete',
                                handler : this.DeleteOrganization,
                                scope   : this
                            }],
                    formConfig     : {
                        common : {
                            width            : 500,
                            height           : 200,
                            xtype            : 'vizFormOrganization',
                            messageSaveStart : 'OrganizationChangeStart',
                            messageSaveEnd   : 'OrganizationChange'
                        },
                        create : {
                            title   : $lang("msg_organization_add"),
                            iconCls : 'viz-icon-small-organization-add'
                        },
                        update : {
                            title               : $lang("msg_organization_update"),
                            iconCls             : 'viz-icon-small-organization-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyOrganization'
                                    }]
                        }
                    }

                };

                config.plugins = [new Viz.plugins.RapidSearch({
                            store          : {
                                xtype          : 'vizStoreRapidSearch',
                                serviceHandler : Viz.Services.CoreWCF.Organization_GetStore,
                                baseParams     : {
                                    filter : 'Id Name'
                                }
                            },
                            displayField   : 'Name',
                            pathTypeString : $lang("msg_organization_path"),
                            valueField     : 'Id',
                            searchName     : 'Companies'
                        })];

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.tree.Organization.superclass.constructor.call(this, config);

                // register to OrganizationChangeStart and OrganizationChange.
                Viz.util.MessageBusMgr.subscribe("OrganizationChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.subscribe("OrganizationChange", this.ReloadNode, this);
            },

            /**
             * @private OnDestroy.
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("OrganizationChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.unsubscribe("OrganizationChange", this.ReloadNode, this);
                Viz.tree.Organization.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * Menu handler for creating a new Organization.
             */
            CreateOrganization  : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var entityParent = null;
                if (node) {
                    this.formConfig.common.messageSaveEndParams = node.id;
                    entityParent = {
                        KeyParent : node.attributes.Key
                    };
                }
                this.openFormCrud(this.formConfig, 'create', null, entityParent);
            },

            /**
             * Menu handler for updating an existing Organization.
             */
            UpdateOrganization  : function() {
                this.onUpdateItem(this.formConfig);
            },

            /**
             * Menu handler for deleting an existing Organization.
             */
            DeleteOrganization  : function() {
                this.onDeleteItem(Viz.Services.CoreWCF.Organization_Delete, 'OrganizationChangeStart', 'OrganizationChange');
            },

            /**
             * Handles the beforecontextmenu event and gives a chance to change the menu
             * @param {Viz.tree.Organization} tree
             * @param {Ext.tree.TreeNode} node
             */
            onBeforeContextMenu : function(tree, node) {
                if (node.isRoot)
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.menuRoot
                            });
                else
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.menu
                            });
            },

            /**
             * Changes the KeyParent of the entitySource based on the new KeyParent.
             * @param {Object} entitySource
             * @param {string} keyParent
             */
            changeParentKey     : function(entitySource, keyParent) {
                Ext.apply(entitySource, {
                            KeyParent : keyParent
                        });
            }

        });

Ext.reg('vizTreeOrganization', Viz.tree.Organization);

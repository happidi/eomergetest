﻿Ext.namespace('Viz.tree');

/**
* Summary.
* @class Viz.tree.AzManRole
* @extends Viz.tree.TreePanel
*/
Viz.tree.AzManOperation = Ext.extend(Viz.tree.TreePanel, {

    /**
    * Ctor.
    * @param {Object} config The configuration options
    */
    constructor: function (config) {
        config = config || {};
        if ((config.root) && (config.root.onlyRoles)) {
            config.onlyRoles = true;
        } else {
            config.onlyRoles = false;    
        }            
        
        var defaultConfig = {
          
                };
        var forcedConfig = {

            serviceHandler: Viz.Services.AuthenticationWCF.AzManItem_GetAncestorTree,
            serviceParams: {
                onlyRoles: config.onlyRoles
            },
            root: {
                id: config.root.id,
                iconCls: config.root.iconCls,
                text: config.root.text
            }           

        };
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.tree.AzManRole.superclass.constructor.call(this, config);

        

        
    }

    
});

Ext.reg('vizTreeAzManOperation', Viz.tree.AzManOperation);
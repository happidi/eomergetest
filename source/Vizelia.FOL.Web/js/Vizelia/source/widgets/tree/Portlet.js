﻿Ext.namespace('Viz.tree');

/**
 * Summary.
 * @class Viz.tree.Portlet
 * @extends Viz.tree.TreePanel
 */
Viz.tree.Portlet = Ext.extend(Viz.tree.TreePanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor         : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title          : $lang('msg_portlets'),
                    iconCls        : 'viz-icon-small-portlet',
                    serviceHandler : Viz.Services.EnergyWCF.Portlet_GetTree,
                    pathSeparator  : ' / ',
                    rootVisible    : false,
                    root           : {
                        id      : '/',
                        iconCls : 'viz-icon-small-portlet',
                        text    : $lang('msg_portlets')
                    }
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.tree.Portlet.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("DrawingCanvasChange", this.doRefreshPorlet, this);
                Viz.util.MessageBusMgr.subscribe("AlarmTableChange", this.doRefreshPorlet, this);
                Viz.util.MessageBusMgr.subscribe("ChartChange", this.doRefreshPorlet, this);
                Viz.util.MessageBusMgr.subscribe("MapChange", this.doRefreshPorlet, this);
                Viz.util.MessageBusMgr.subscribe("WebFrameChange", this.doRefreshPorlet, this);
                Viz.util.MessageBusMgr.subscribe("WeatherLocationChange", this.doRefreshPorlet, this);
                Viz.util.MessageBusMgr.subscribe("FlipCardChange", this.doRefreshPorlet, this);

            },

            onDestroy           : function() {

                Viz.util.MessageBusMgr.unsubscribe("DrawingCanvasChange", this.doRefreshPorlet, this);
                Viz.util.MessageBusMgr.unsubscribe("AlarmTableChange", this.doRefreshPorlet, this);
                Viz.util.MessageBusMgr.unsubscribe("ChartChange", this.doRefreshPorlet, this);
                Viz.util.MessageBusMgr.unsubscribe("MapChange", this.doRefreshPorlet, this);
                Viz.util.MessageBusMgr.unsubscribe("WebFrameChange", this.doRefreshPorlet, this);
                Viz.util.MessageBusMgr.unsubscribe("WeatherLocationChange", this.doRefreshPorlet, this);
                Viz.util.MessageBusMgr.unsubscribe("FlipCardChange", this.doRefreshPorlet, this);

                Viz.tree.Portlet.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Refresh action
             */
            doRefreshPorlet     : function(entity, mode) {
                if (mode == 'create') {
                    this.doRefresh();
                }
            },

            /**
             * Refresh action
             */
            /*
             * doRefresh : function(entity, mode) { this.getSelectionModel().clearSelections(true); Viz.tree.Portlet.superclass.doRefresh.apply(this, arguments); },
             */
            /**
             * @private OnDestroy.
             */
            onDestroy           : function() {
                Viz.tree.Portlet.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * Handles the beforecontextmenu event and gives a chance to change the menu
             * @param {Viz.tree.Portlet} tree
             * @param {Ext.tree.TreeNode} node
             */
            onBeforeContextMenu : function(tree, node) {

                if (Ext.isObject(node.attributes.entity)) {
                    var type = Viz.getEntityType(node.attributes.entity.__type);

                    if (!$authorized(Viz.util.Portal.getPortletAuthorization(type, true)))
                        return false;

                    if (type == node.attributes.Key) {
                        var items = [{
                                    text    : String.format('{0} {1}', $lang("msg_add"), node.text),
                                    iconCls : node.attributes.iconCls + '-add',
                                    handler : this.onAddPortlet.createDelegate(this, [type]),
                                    scope   : this
                                }];
                        if (type == 'Vizelia.FOL.BusinessEntities.Chart') {
                            items.push('-');
                            items.push({
                                        text    : $lang('msg_chart_wizard'),
                                        iconCls : 'viz-icon-small-wizard',
                                        handler : Viz.grid.Chart.launchWizard
                                    });

                            items.push({
                                        text    : $lang('msg_chart_previewall'),
                                        iconCls : 'viz-icon-small-image',
                                        handler : function() {
                                            Viz.App.Global.ViewPort.openModule({
                                                        useDataView : true,
                                                        xtype       : 'vizPanelChart'
                                                    })
                                        },
                                        scope   : this
                                    });
                        }
                        if (type == 'Vizelia.FOL.BusinessEntities.AlarmTable') {
                            items.push('-');
                            items.push({
                                        iconCls : 'viz-icon-small-alarm',
                                        text    : $lang('msg_alarmdefinition_wizard_meter'),
                                        handler : Viz.grid.AlarmDefinition.launchWizard.createDelegate(this, ['Meter'])
                                    });
                            items.push({
                                        iconCls : 'viz-icon-small-alarm',
                                        text    : $lang('msg_alarmdefinition_wizard_chart'),
                                        handler : Viz.grid.AlarmDefinition.launchWizard.createDelegate(this, ['Chart'])
                                    });
                        }

                    }
                    else {
                        var iconCls = (type == 'Vizelia.FOL.BusinessEntities.Chart') ? 'viz-icon-small-chart' : node.attributes.iconCls;
                        var items = [{
                                    text    : String.format('{0} {1}', $lang("msg_display"), node.text),
                                    iconCls : 'viz-icon-small-display',
                                    handler : this.onDisplayPortlet,
                                    scope   : this
                                }, '-', {
                                    text    : String.format('{0} {1}', $lang("msg_update"), node.text),
                                    iconCls : iconCls + '-update',
                                    handler : this.onUpdatePortlet,
                                    scope   : this
                                }, {
                                    text    : String.format('{0} {1}', $lang("msg_delete"), node.text),
                                    iconCls : iconCls + '-delete',
                                    handler : this.onDeletePortlet,
                                    scope   : this
                                }];
                    }
                    this.ctxMenu = new Ext.menu.Menu({
                                items : items
                            });
                    return true;
                }
                return false;
            },

            /**
             * Menu handler for viewing a Portlet.
             */
            onDisplayPortlet    : function() {
                var node = this.getSelectionModel().getSelectedNode();
                if (Ext.isObject(node.attributes.entity)) {
                    var type = this.getNodeType(node);
                    Viz.grid[type]['open' + type + 'Display'](node.attributes.entity);
                }
            },

            /**
             * Menu handler for creating a new portlet.
             */
            onAddPortlet        : function(type) {
                Viz.grid[type.replace('Vizelia.FOL.BusinessEntities.', '')].create();
            },

            /**
             * Menu handler for updating an existing portlet.
             */
            onUpdatePortlet     : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var type = this.getNodeType(node);
                Viz.grid[type].update(node.attributes.entity);
            },

            /**
             * Return the short type for a node : Chart, Map etc...
             * @param {} node
             * @return {}
             */
            getNodeType         : function(node) {
                var type = Viz.getEntityType(node.attributes.entity.__type).replace('Vizelia.FOL.BusinessEntities.', '');
                return type;
            },

            /**
             * Menu handler for deleting an existing portlet.
             */
            onDeletePortlet     : function() {

                var node = this.getSelectionModel().getSelectedNode();
                if (Ext.isObject(node.attributes.entity)) {
                    var msgDelete = $lang("msg_record_delete_confirmation_single");
                    Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                                if (button == 'yes') {
                                    var type = this.getNodeType(node);
                                    var serviceName = type + '_Delete';
                                    Viz.Services.EnergyWCF[serviceName]({
                                                item    : node.attributes.entity,
                                                success : function(value) {
                                                    if (value == true) {
                                                        Viz.util.MessageBusMgr.fireEvent("PortletDelete", node.attributes.entity);
                                                        node.remove();
                                                    }
                                                }
                                            });
                                }
                            }, this);
                }
            }
        });

Ext.reg('vizTreePortlet', Viz.tree.Portlet);
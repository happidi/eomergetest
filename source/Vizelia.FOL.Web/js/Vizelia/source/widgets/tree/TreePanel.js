﻿Ext.namespace('Viz.tree');
/**
 * @class Viz.tree.TreePanel Base Vizelia TreePanel.
 * @extends Ext.tree.TreePanel
 */
Viz.tree.TreePanel = Ext.extend(Ext.tree.TreePanel, {
            /**
             * @cfg {Boolean} rootVisible <tt>true</tt> if root should be visible, <tt>false</tt> otherwise (defaults to <tt>true</tt>).
             */
            rootVisible          : true,
            /**
             * @cfg {Boolean} useArrows <tt>true</tt> to use arrows, false otherwise (defaults to <tt>true</tt>).
             */
            useArrows            : true,
            /**
             * @cfg {Boolean} autoScroll <tt>true</tt> to autoscroll, false otherwise (defaults to <tt>true</tt>).
             */
            autoScroll           : true,
            /**
             * @cfg {Boolean} containerScroll <tt>true</tt> to register this container with ScrollManager (defaults to <tt>true</tt>).
             */
            containerScroll      : true,
            /**
             * @cfg {String} sortAttribute The name of the property that should be used to sort the nodes.
             */
            sortAttribute        : null,
            /**
             * @cfg {Boolean} hasTools <tt>true</tt> to create an emplacement for tools, <tt>false</tt> otherwise. Default to <tt>true</tt>.
             */
            hasTools             : true,
            /**
             * @cfg {Boolean} stateful True to have a stateful tree.
             */
            stateful             : true,
            /**
             * @cfg {Boolean} hideMenu True to hide the contextual menu.
             */
            /**
             * @cfg {Boolean} displayCheckbox True to display check boxes, false otherwise. Default to <tt>false</tt>.
             */
            /**
             * @cfg {Boolean} disableCheckboxForAncestors True to have a disable checkboxes for ancestors nodes.
             */
            /**
             * @cfg {Array} checkboxExcludedTypes an array of types to prevent of having a checkbox. IE ['Vizelia.FOL.BusinessEntities.Meter'].
             */
            /**
             * @cfg {Function} serviceHandler The service handler for retreiving the nodes.
             */
            /**
             * @cfg {Object} serviceParams The service parameters for retreiving the nodes.
             */
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor          : function(config) {
                config = config || {};
                var defaultConfig = {
                    bodyStyle : 'padding:2px 0px 2px 0;',
                    tools     : [{
                                id      : 'refresh',
                                handler : this.doRefresh,
                                qtip    : $lang('msg_refresh'),
                                scope   : this
                            }, {
                                id      : 'up',
                                handler : this.doOrder,
                                scope   : this,
                                hidden  : config.sortAttribute ? false : true
                            }, {
                                id      : 'minus',
                                handler : this.doDeleteCheckedNodes,
                                qtip    : $lang('msg_tree_uncheckallnodes'),
                                scope   : this,
                                hidden  : config.displayCheckbox ? false : true
                            }, {
                                id      : 'plus',
                                handler : this.doViewCheckedNodes,
                                qtip    : $lang('msg_tree_viewallcheckednodes'),
                                scope   : this,
                                hidden  : config.displayCheckbox ? false : true
                            }]
                };

                if (config.stateful !== false) {
                    config.stateId = config.stateId ? config.stateId : config.xtype;
                    
                    if (Ext.isEmpty(config.plugins)) {
                        config.plugins = [new Ext.ux.state.TreePanel()];
                    }
                    else if (Ext.isArray(config.plugins)) {
                        config.plugins.push(new Ext.ux.state.TreePanel());
                    }
                    else {
                        config.plugins = [new Ext.ux.state.TreePanel()].push(config.plugins);
                    }
                }

                var forcedConfig = {
                    loader : new Viz.tree.TreeLoaderWCF({
                                displayCheckbox             : config.displayCheckbox ? true : false,
                                disableCheckboxForAncestors : config.disableCheckboxForAncestors ? true : false,
                                checkboxExcludedTypes       : config.checkboxExcludedTypes,
                                serviceHandler              : config.serviceHandler,
                                serviceParams               : config.serviceParams
                            })
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                this.addEvents(
                        /**
                         * @event beforecontextmenu Fires before the context menu is shown and gives a chance to change the ctxMenu.
                         * @param {Viz.tree.TreePanel} this
                         * @param {Ext.tree.TreeNode} node The node that is selected.
                         */
                        "beforecontextmenu",
                        /**
                         * @event beforeaddnode Fires before a node is added.
                         * @param {Viz.tree.TreePanel} this
                         * @param {Object} entityParent
                         */
                        "beforeaddnode",
                        /**
                         * @event beforeupdatenode Fires before a node is updated.
                         * @param {Viz.tree.TreePanel} this
                         * @param {Ext.tree.TreeNode} node The node that is updated.
                         */
                        "beforeupdatenode",
                        /**
                         * @event beforedorefresh Fires before a node is refreshed.
                         * @param {Viz.tree.TreePanel} this
                         * @param {Ext.tree.TreeNode} node The node that is refreshed.
                         */
                        "beforedorefresh");
                this.on({
                            beforenodedrop    : {
                                scope : this,
                                fn    : this.onBeforeNodeDrop
                            },
                            nodedrop          : {
                                scope : this,
                                fn    : this.onNodeDrop
                            },
                            nodedragover      : {
                                scope : this,
                                fn    : this.onNodeDragOver
                            },
                            beforecontextmenu : {
                                scope : this,
                                fn    : this.onBeforeContextMenu
                            },
                            beforedorefresh   : {
                                scope : this,
                                fn    : this.onBeforeDoRefresh
                            },
                            contextmenu       : {
                                scope : this,
                                fn    : this.onContextMenu
                            },
                            checkchange       : {
                                scope : this,
                                fn    : this.onCheckChange
                            }
                        });

                // this.on("beforecontextmenu", this.onBeforeContextMenu, this);
                // this.on("contextmenu", this.onContextMenu, this);
                Viz.tree.TreePanel.superclass.constructor.call(this, config);
                this.tools = this.tools || [];
                this.relayEvents(this.loader, ['beforeloadloader']);
            },
            initComponent        : function() {
                if (!this.hasTools)
                    delete this.tools;
                Viz.tree.TreePanel.superclass.initComponent.apply(this, arguments);
                // var node = this.getRootNode();

            },
            /**
             * Refreshes the tree.
             */
            doRefresh            : function() {
                var node = this.getSelectionModel().getSelectedNode() || this.getRootNode();
                if (this.fireEvent("beforedorefresh", this, node)) {
                    this.Mask();
                    var expanded = node.expanded;
                    this.getLoader().load(node, function(ev, n) {
                                if (expanded && n)
                                    n.expand();
                                this.UnMask();
                            }, this);
                }
            },
            /**
             * Reorder the tree using the sortAttribute value provided in configuration.
             */
            doOrder              : function() {
                if (this.sorter) {
                    this.un("beforechildrenrendered", this.sorter.doSort, this);
                    this.un("append", this.sorter.updateSort, this);
                    this.un("insert", this.sorter.updateSort, this);
                    this.un("textchange", this.sorter.updateSortParent, this);
                }
                // Create new sorter
                this.sorter = new Viz.tree.TreeSorter(this, {
                            folderSort : true,
                            dir        : this.sorter ? this.sorter.dir.toggle("asc", "desc") : "asc",
                            property   : this.sortAttribute
                        });
                // Resort all nodes with children
                this.getRootNode().cascade(function(node) {
                            if (node.childrenRendered) {
                                this.sorter.doSort(node);
                            }
                        }, this);
            },
            /**
             * Uncheck all checkednodes
             */
            doDeleteCheckedNodes : function() {
                Ext.MessageBox.confirm($lang('msg_tree_uncheckallnodes'), $lang('msg_tree_uncheckallnodes_confirm'), function(button) {
                            if (button == 'yes') {
                                var nodes = this.getChecked();
                                Ext.each(nodes, function(node) {
                                            node.getUI().toggleCheck(false);
                                        }, this);
                            }
                        }, this);
            },
            /**
             * View all checked nodes
             */
            doViewCheckedNodes   : function() {
                var nodes = this.getChecked();
                var newNodes = [];
                Ext.each(nodes, function(node) {
                            newNodes.push({
                                        text    : node.text,
                                        leaf    : true,
                                        iconCls : node.attributes.iconCls
                                    });
                        }, this);
                var win = new Ext.Window({
                            title   : this.title + ' ' + newNodes.length,
                            iconCls : this.iconCls,
                            width   : 400,
                            height  : 300,
                            layout  : 'fit',
                            items   : [{
                                        xtype       : 'treepanel',
                                        border      : false,
                                        autoScroll  : true,
                                        loader      : new Ext.tree.TreeLoader(),
                                        root        : new Ext.tree.AsyncTreeNode({
                                                    expanded : true,
                                                    children : newNodes
                                                }),
                                        rootVisible : false
                                    }]
                        });
                win.show();
            },
            /**
             * Handles the beforecontextmenu event and gives a chance to change the menu
             * @param {Viz.tree.AzManFilter} tree
             * @param {Ext.tree.TreeNode} node
             */
            onBeforeContextMenu  : function(tree, node) {
                return true;
            },
            /**
             * Handles the beforedorefresh event and gives a chance to cancel it
             * @param {Viz.tree.AzManFilter} tree
             * @param {Ext.tree.TreeNode} node
             */
            onBeforeDoRefresh    : function(tree, node) {
                return true;
            },
            /**
             * Displays the contextual menu.
             * @param {Ext.tree.TreeNode} node
             * @param {Ext.EventObject} event
             */
            onContextMenu        : function(node, event) {
                event.stopEvent();
                if (this.hideMenu == true)
                    return;
                if (!node.isSelected())
                    node.select();
                if (!this.ctxMenu && this.menu)
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.menu
                            });
                if (this.fireEvent("beforecontextmenu", this, node) != false) {
                    if (this.ctxMenu instanceof Ext.menu.Menu && this.ctxMenu.items && this.ctxMenu.items.items) {
                        // we recreate the separator item that may have lost their dom after a destroy of the menu.
                        Ext.each(this.ctxMenu.items.items, function(item, index) {
                                    if (item.itemCls == 'x-menu-sep') {
                                        this.ctxMenu.items.items[index] = new Ext.menu.Separator({
                                                    hidden : item.hidden
                                                });
                                    }

                                }, this);

                        this.ctxMenu.on("hide", function(menu) {
                                    this.ctxMenu.destroy();
                                    this.ctxMenu = null;
                                }, this);
                        this.ctxMenu.showAt(event.getXY());
                    }
                }
            },
            /**
             * Masks the panel.
             */
            Mask                 : function() {
                if (this.rendered && this.body)
                    this.body.mask();
            },
            /**
             * Unmasks the panel.
             */
            UnMask               : function() {
                if (this.rendered && this.body)
                    this.body.unmask();
            },
            /**
             * Reloads a node.
             * @param {String} nodeId The id of the node to reload.
             */
            ReloadNode           : function(nodeId) {
                this.Mask();
                var node = this.getNodeById(nodeId) || this.getRootNode();
                this.getLoader().load(node, function() {
                            node.expand();
                            // node.reload();
                            node.ownerTree.UnMask();
                        }, this);
            },
            /**
             * Handler for the Add item.
             * @param {Object} formConfig the configuration of the form to open.
             */
            onAddItem            : function(formConfig) {
                var node = this.getSelectionModel().getSelectedNode();
                var entityParent;
                if (node) {
                    formConfig.common.messageSaveEndParams = node.id;
                    entityParent = Viz.clone(node.attributes.entity);
                }
                if (this.fireEvent("beforeaddnode", this, entityParent)) {
                    this.openFormCrud(formConfig, 'create', null, entityParent);
                }
            },
            /**
             * Handler for the Update item.
             * @param {Object} formConfig the configuration of the form to open.
             */
            onUpdateItem         : function(formConfig) {
                var node = this.getSelectionModel().getSelectedNode();
                if (node)
                    if (this.fireEvent("beforeupdatennode", this, node)) {
                        if (node.parentNode)
                            formConfig.common.messageSaveEndParams = node.parentNode.id;
                        else
                            formConfig.common.messageSaveEndParams = node.id;
                        this.openFormCrud(formConfig, 'update', node.attributes.entity);
                    }
            },
            /**
             * Opens a form crud.
             * @param {Object} config The configuration of the form.
             * @param {String} mode The mode of the form, either 'create' or 'update'.
             * @param {Object} entity The entity object is not null when mode == 'update'
             */
            openFormCrud         : function(config, mode, entity, entityParent) {
                Viz.openFormCrud(config, mode, entity, null, entityParent);
                return;
                /*
                 * if (Ext.isObject(config)) { var configForm = Viz.clone(config); configForm = Ext.applyIf(configForm[mode] || {}, configForm.common); var serviceParams = {}; if (Ext.isArray(configForm.serviceParamsSimple)) { Ext.each(configForm.serviceParamsSimple, function(item) { serviceParams[item.name] = item.value; }, this); } if (mode == 'update' && Ext.isObject(entity) && Ext.isArray(configForm.serviceParamsEntity)) { Ext.each(configForm.serviceParamsEntity, function(item) { serviceParams[item.name] = entity[item.value]; }, this); } var win = new Ext.Window({ title : mode == 'create' ? configForm.title : configForm.title + ' : ' + entity[configForm.titleEntity], iconCls : configForm.iconCls, // modal : true, width : configForm.width || 400, height : configForm.height || 300,
                 * layout : 'fit', // animateTarget : grid.getView().focusEl, items : [{ xtype : configForm.xtype, mode : mode, mode_load : configForm.mode_load ? configForm.mode_load : 'remote', messageSaveStart : configForm.messageSaveStart, messageSaveEnd : configForm.messageSaveEnd, messageSaveEndParams : configForm.messageSaveEndParams, serviceParams : serviceParams, entity : entity, entityParent : entityParent, initialConfigFromGrid : config }] }); win.show(); }
                 */
            },
            /**
             * Handler for the delete action
             * @param {Function} serviceHandlerDelete The service hanlder for deletion.
             * @param {String} messageSaveStart (optional) The message that is sent when starting the operation.
             * @param {String} messageSaveEnd (optional) The message that is sent when ending the operation.
             */
            onDeleteItem         : function(serviceHandlerDelete, messageSaveStart, messageSaveEnd, msgDeleteSingle, msgDeleteMultiple) {
                var node = this.getSelectionModel().getSelectedNode();
                var nodes = [].concat(node);
                var msgDelete;
                if (nodes.length > 1)
                    msgDelete = String.format(msgDeleteMultiple ? msgDeleteMultiple : $lang("msg_node_delete_confirmation_multiple"), nodes.length);
                else
                    msgDelete = String.format(msgDeleteSingle ? msgDeleteSingle : $lang("msg_node_delete_confirmation_single"));
                Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                            if (button == 'yes') {
                                if (messageSaveStart)
                                    Viz.util.MessageBusMgr.publish(messageSaveStart);
                                serviceHandlerDelete.call(this, {
                                            item    : nodes[0].attributes.entity,
                                            success : function() {
                                                // node.parentNode.reload();
                                                if (messageSaveEnd)
                                                    Viz.util.MessageBusMgr.publish(messageSaveEnd, node.parentNode.id, nodes[0].attributes.entity);
                                                else
                                                    node.parentNode.reload();
                                            },
                                            failure : function(error, arg, callname) {
                                                Ext.Msg.show({
                                                            width   : 400,
                                                            title   : $lang("error_ajax"),
                                                            msg     : error.Message,
                                                            buttons : Ext.Msg.OK,
                                                            icon    : Ext.MessageBox.ERROR
                                                        });
                                                if (messageSaveEnd)
                                                    Viz.util.MessageBusMgr.publish(messageSaveEnd, node.parentNode.id);
                                                else
                                                    node.parentNode.reload();
                                            },
                                            scope   : this
                                        });
                            }
                        }, this);
            },
            /**
             * Runs before node is dropped.
             * @private
             * @param {Object} e dropEvent object
             */
            onBeforeNodeDrop     : function(e) {
                var nodeSource = e.dropNode;
                var nodeTarget = e.target;
                var tree = e.tree;
                e.cancel = true;
                var question = String.format($lang('msg_tree_move'), nodeSource.text, nodeTarget.text);
                tree.suspendEvents();
                Ext.MessageBox.confirm($lang('msg_application_title'), question, function(btn, text) {
                            if (btn == 'yes') {
                                this.changeParent(nodeSource, nodeTarget, tree);
                            }
                        }, this);
                tree.resumeEvents();
            },
            /**
             * Changes the parent of an item after it was dropped on a node. There is no need to override this generic function in subclass
             * @param {Object} nodeSource The source node.
             * @param {Object} nodeTarget The target node.
             * @param {Viz.tree.TreePanel} tree The tree.
             */
            changeParent         : function(nodeSource, nodeTarget, tree) {
                var keyParent = null;
                if (nodeTarget.getDepth() != 0) {
                    keyParent = nodeTarget.attributes.Key;
                }
                var entitySource = nodeSource.attributes.entity;
                tree.Mask();
                this.changeParentKey(entitySource, keyParent);
                var serviceName = entitySource.IfcType.replace('Ifc', '') + '_FormUpdate';

                var module = Viz.Services.CoreWCF;
                if (entitySource.IfcType == 'IfcMeter')
                    module = Viz.Services.EnergyWCF;
                module[serviceName]({
                            item    : entitySource,
                            success : function(value) {
                                if (value.success == true) {
                                    nodeSource.remove();
                                    nodeTarget.reload();
                                }
                                tree.UnMask();
                            },
                            failure : function() {
                                tree.UnMask();
                            }
                        });
            },
            /**
             * Changes the KeyParent of the entitySource based on the new keyParent. This function should provide an implementation in subclass of TreePanel.
             * @param {Object} entitySource
             * @param {string} keyParent
             */
            changeParentKey      : function(entitySource, keyParent) {
                throw new Error("Not implemented");
            },
            /**
             * Runs when node is dropped. This method is empty by default but can be implemented by any subclass.
             * @param {Object} e dd event
             */
            onNodeDrop           : function(e) {
            },
            /**
             * Called while dragging over. This method is empty by default but can be implemented by any subclass.
             * @private
             * @param {Object} e dd event
             */
            onNodeDragOver       : function(e) {
            },

            /**
             * Handler for checking a node.
             */
            onCheckChange        : function(node, checked) {

            }
        });
Ext.reg('viztreepanel', Viz.tree.TreePanel);

﻿Ext.namespace('Viz.tree');
/**
 * Summary.
 * @class Viz.tree.DataAcquisition
 * @extends Viz.tree.TreePanel
 */
Viz.tree.DataAcquisition = Ext.extend(Viz.tree.TreePanel, {
            /**
             * @cfg {Object} root the root node
             */
            /**
             * @cfg {string} KeyChart the Key of the Chart in order to filter the list of classification based on the Chart FilterSpatial.
             */
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var rootEntity = Viz.Configuration.GetClassificationItemDefinitionEntityFromCategory("meter");
                var defaultConfig = {
                    title          : $lang('msg_dataacquisitioncontainer'),
                    iconCls        : 'viz-icon-small-dataacquisitioncontainer',
                    rootVisible    : false,
                    serviceHandler : Viz.Services.EnergyWCF.DataAcquisition_GetTree,
                    root           : {
                        iconCls : 'viz-icon-small-meter',
                        text    : $lang('msg_dataacquisitioncontainer')
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.tree.DataAcquisition.superclass.constructor.call(this, config);
            },
            /**
             * @private OnDestroy.
             */
            onDestroy   : function() {
                Viz.tree.DataAcquisition.superclass.onDestroy.apply(this, arguments);
            }

        });
Ext.reg('vizTreeDataAcquisition', Viz.tree.DataAcquisition);
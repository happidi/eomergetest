﻿Ext.namespace('Viz.tree');

/**
 * Summary.
 * @class Viz.tree.AzManFilter
 * @extends Viz.tree.TreePanel
 */
Viz.tree.AzManFilter = Ext.extend(Viz.tree.TreePanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor         : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    title          : $lang('msg_azmanfilter_tree'),
                    iconCls        : 'viz-icon-small-filter',
                    serviceHandler : Viz.Services.AuthenticationWCF.AzManFilter_GetTree,
                    pathSeparator  : ' / ',
                    root           : {
                        id      : ' ',
                        iconCls : 'viz-icon-small-filter',
                        text    : $lang('msg_azmanfilter_root')
                    },
                    menu           : [{
                                text    : $lang("msg_azmanfilter_add"),
                                iconCls : 'viz-icon-small-filter-add',
                                handler : this.CreateAzManFilter,
                                scope   : this
                            }, {
                                text    : $lang("msg_azmanfilter_update"),
                                iconCls : 'viz-icon-small-filter-update',
                                handler : this.UpdateAzManFilter,
                                scope   : this
                            }, {
                                text    : $lang("msg_azmanfilter_delete"),
                                iconCls : 'viz-icon-small-filter-delete',
                                handler : this.DeleteAzManFilter,
                                scope   : this
                            }],
                    menuRoot       : [{
                                text    : $lang("msg_azmanfilter_add"),
                                iconCls : 'viz-icon-small-filter-add',
                                handler : this.CreateAzManFilter,
                                scope   : this
                            }],

                    formConfig     : {
                        common : {
                            width            : 650,
                            height           : 400,
                            xtype            : 'vizFormAzManFilter',
                            messageSaveStart : 'AzManChangeStart',
                            messageSaveEnd   : 'AzManChange',
                            mode_load        : 'local'
                        },
                        create : {
                            title   : $lang("msg_azmanfilter_add"),
                            iconCls : 'viz-icon-small-filter-add'

                        },
                        update : {
                            title               : $lang("msg_azmanfilter_update"),
                            iconCls             : 'viz-icon-small-filter-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'Id'
                                    }]
                        }
                    }

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                config.plugins = [new Viz.plugins.RapidSearch({
                            pathTypeString : $lang('msg_azmanfilter_path'),
                            store          : {
                                xtype          : 'vizStoreRapidSearch',
                                serviceHandler : Viz.Services.AuthenticationWCF.AzManFilter_GetStoreByKeyword,
                                baseParams     : {
                                    filter : 'LocalId'
                                }
                            },
                            searchName: 'Projects'
                        })];

                Viz.tree.AzManFilter.superclass.constructor.call(this, config);

                // register to AzManChangeStart and AzManChange.
                Viz.util.MessageBusMgr.subscribe("AzManChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.subscribe("AzManChange", this.ReloadNode, this);
            },

            /**
             * @private
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("AzManChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.unsubscribe("AzManChange", this.ReloadNode, this);
                Viz.tree.AzManFilter.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Menu handler for creating a new AzManFilter.
             */
            CreateAzManFilter   : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var entityParent = null;
                if (node.attributes.entity)
                    entityParent = {
                        ParentId : node.attributes.entity.Id
                    };
                var formConfig = this.formConfig;
                formConfig.common.messageSaveEndParams = node.id;
                this.openFormCrud(formConfig, 'create', null, entityParent);
            },

            /**
             * Menu handler for updating an existing AzManFilter.
             */
            UpdateAzManFilter   : function() {
                this.onUpdateItem(this.formConfig);
            },

            /**
             * Menu handler for deleting an existing AzManFilter.
             */
            DeleteAzManFilter   : function() {
                this.onDeleteItem(Viz.Services.AuthenticationWCF.AzManFilter_Delete);
            },

            /**
             * Handles the beforecontextmenu event and gives a chance to change the menu
             * @param {Viz.tree.AzManFilter} tree
             * @param {Ext.tree.TreeNode} node
             */
            onBeforeContextMenu : function(tree, node) {
                if (node.isRoot)
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.menuRoot
                            });
                else {
                    if (node.attributes && node.attributes.entity && node.attributes.entity.ItemType == 'Filter')
                        this.ctxMenu = new Ext.menu.Menu({
                                    items : this.menu
                                });
                    else
                        this.ctxMenu = null;
                }
            }

        });

Ext.reg('vizTreeAzManFilter', Viz.tree.AzManFilter);

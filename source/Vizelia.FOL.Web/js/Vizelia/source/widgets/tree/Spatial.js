﻿Ext.namespace("Viz.tree");

/**
 * The Spatial tree.
 * @class Viz.tree.Spatial
 * @extends Viz.tree.TreePanel
 */
Viz.tree.Spatial = Ext.extend(Viz.tree.TreePanel, {
            /**
             * Ctor
             * @param {Object} config
             */
            constructor                      : function(config) {
                config = config || {};
                var defaultConfig = {
                    title                       : $lang('msg_tree_spatial'),
                    iconCls                     : 'viz-icon-small-site',
                    serviceParams               : {
                        flgFilter : true,
                        ExcludeMetersAndAlarms : false
                    },
                    disableCheckboxForAncestors : true
                };

                if (Ext.isBoolean(config.ExcludeMetersAndAlarms)) {
                    defaultConfig.serviceParams.ExcludeMetersAndAlarms = config.ExcludeMetersAndAlarms;
                }

                var siteEditPermissions = $authorized("@@@@ Site - Full Control") || $authorized("@@@@ Spatial Hierarchy Tree - Full Control");
                var siteViewPermissions = !siteEditPermissions && ($authorized("@@@@ Site") || $authorized("@@@@ Spatial Hierarchy Tree"));

                var buildingEditPermissions = $authorized("@@@@ Building - Full Control") || $authorized("@@@@ Spatial Hierarchy Tree - Full Control");
                var buildingViewPermissions = !buildingEditPermissions && ($authorized("@@@@ Building") || $authorized("@@@@ Spatial Hierarchy Tree"));

                var buildingStoreyEditPermissions = $authorized("@@@@ Building Storey - Full Control") || $authorized("@@@@ Spatial Hierarchy Tree - Full Control");
                var buildingStoreyViewPermissions = !buildingStoreyEditPermissions && ($authorized("@@@@ Building Storey") || $authorized("@@@@ Spatial Hierarchy Tree"));

                var spaceEditPermissions = $authorized("@@@@ Space - Full Control") || $authorized("@@@@ Spatial Hierarchy Tree - Full Control");
                var spaceViewPermissions = !spaceEditPermissions && ($authorized("@@@@ Space") || $authorized("@@@@ Spatial Hierarchy Tree"));

                var furnitureEditPermissions = $authorized("@@@@ Furniture - Full Control") || $authorized("@@@@ Spatial Hierarchy Tree - Full Control");
                var furnitureViewPermissions = !furnitureEditPermissions && ($authorized("@@@@ Furniture") || $authorized("@@@@ Spatial Hierarchy Tree"));

                var meterEditPermissions = $authorized("@@@@ Meters - Full Control");
                var meterViewPermissions = !meterEditPermissions && $authorized("@@@@ Meters");

                var alarmDefinitionEditPermissions = $authorized("@@@@ Alarm Definition - Full Control");
                var alarmDefinitionViewPermissions = !alarmDefinitionEditPermissions && $authorized("@@@@ Alarm Definition");

                var forcedConfig = {
                    serviceHandler            : Viz.Services.CoreWCF.Spatial_GetTree,
                    pathSeparator             : ' / ',
                    rootVisible               : false,
                    root                      : {
                        id       : ' ', // '#124823',
                        nodeType : 'async',
                        iconCls  : 'viz-icon-small-site',
                        text     : $lang("msg_tree_spatial_root"),
                        expanded : true
                    },
                    menuCalendar              : [new Ext.menu.Separator({
                                        hidden : !$authorized('@@@@ Calendar Event - Full Control') && !$authorized('@@@@ Calendar Event')
                                    }), {
                                text    : $lang("msg_calendar_manage_holiday"),
                                iconCls : 'viz-icon-small-calendarevent',
                                hidden  : true,
                                handler : this.onManageHoliday,
                                scope   : this
                            }, {
                                text    : $lang("msg_calendars_manage"),
                                hidden  : !$authorized("@@@@ Calendar Event - Full Control"),
                                iconCls : 'viz-icon-small-calendarevent',
                                menu    : {
                                    xtype                    : 'menustore',
                                    iconCls                  : 'viz-icon-small-calendarevent',
                                    store                    : new Viz.store.CalendarEventCategory(),
                                    createMenuItemFromRecord : function(record) {
                                        return {
                                            icon    : Ext.ux.colorImgSrc(record.data.ColorR, record.data.ColorG, record.data.ColorB),
                                            text    : record.data.Label,
                                            handler : this.onManageExtraCalendar.createDelegate(this, [record.data])
                                        }
                                    }.createDelegate(this)
                                }
                            }, {
                                text    : $lang("msg_calendar_show"),
                                iconCls : 'viz-icon-small-calendar',
                                handler : this.onShowCalendar,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Calendar Event")
                            }],
                    menuProject              : [new Ext.menu.Separator({
                                        hidden : !$authorized('@@@@ Project Hierarchy')
                                    }), {
                                text    : $lang("msg_azmanfilter_locationprojects"),
                                iconCls : 'viz-icon-small-filter',
                                hidden  : !$authorized('@@@@ Project Hierarchy'),
                                handler : this.onManageProjects,
                                scope   : this
                            }],
                    menuSite                  : [{
                                text    : $lang("msg_site_add"),
                                iconCls : 'viz-icon-small-site-add',
                                handler : this.onAddSite,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Site - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, {
                                text    : siteEditPermissions ? $lang("msg_site_update") : $lang("msg_site_view"),
                                iconCls : siteEditPermissions ? 'viz-icon-small-site-update' : 'viz-icon-small-site-view',
                                handler : this.onUpdateSite,
                                scope   : this,
                                hidden  : !siteViewPermissions && !siteEditPermissions
                            }, {
                                text    : $lang("msg_site_delete"),
                                iconCls : 'viz-icon-small-site-delete',
                                handler : this.onDeleteSite,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Site - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized("@@@@ Site - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                                    }), {
                                text    : $lang("msg_building_add"),
                                iconCls : 'viz-icon-small-building-add',
                                handler : this.onAddBuilding,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Building - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, {
                                text    : $lang("msg_furniture_add"),
                                iconCls : 'viz-icon-small-furniture-add',
                                handler : this.onAddFurniture,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Furniture - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, {
                                text    : $lang("msg_meter_add"),
                                iconCls : 'viz-icon-small-meter-add',
                                handler : this.onAddMeter,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Meters - Full Control")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized("@@@@ Site - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                                    }), {
                                text    : $lang("msg_psetattributehistorical"),
                                iconCls : 'viz-icon-small-pset',
                                hidden  : !$authorized("@@@@ Site - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control"),
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.PsetDefinition({
                                                UsageName : 'IfcSite'
                                            }),
                                    createMenuItemFromRecord : this.createMenuItemFromPsetDefinition.createDelegate(this)
                                }
                            }],
                    menuBuilding              : [{
                                text    : $lang("msg_buildingstorey_add"),
                                iconCls : 'viz-icon-small-buildingstorey-add',
                                handler : this.onAddBuildingStorey,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Building - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, {
                                text    : buildingEditPermissions ? $lang("msg_building_update") : $lang("msg_building_view"),
                                iconCls : buildingEditPermissions ? 'viz-icon-small-building-update' : 'viz-icon-small-building-view',
                                handler : this.onUpdateBuilding,
                                scope   : this,
                                hidden  : !buildingViewPermissions && !buildingEditPermissions
                            }, {
                                text    : $lang("msg_building_delete"),
                                iconCls : 'viz-icon-small-building-delete',
                                handler : this.onDeleteBuilding,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Building - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized("@@@@ Building Storey - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                                    }), {
                                text    : $lang("msg_classificationitem_associate"),
                                iconCls : 'viz-icon-small-classificationitem',
                                handler : this.onAssociateClassificationItem,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Classification Item")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized("@@@@ Building Storey - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                                    }), {
                                text    : $lang("msg_furniture_add"),
                                iconCls : 'viz-icon-small-furniture-add',
                                handler : this.onAddFurniture,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Furniture - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, {
                                text    : $lang("msg_meter_add"),
                                iconCls : 'viz-icon-small-meter-add',
                                handler : this.onAddMeter,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Meters - Full Control")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized("@@@@ Building - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                                    }), {
                                text    : $lang("msg_psetattributehistorical"),
                                iconCls : 'viz-icon-small-pset',
                                hidden  : !$authorized("@@@@ Building - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control"),
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.PsetDefinition({
                                                UsageName : 'IfcBuilding'
                                            }),
                                    createMenuItemFromRecord : this.createMenuItemFromPsetDefinition.createDelegate(this)
                                }
                            }],
                    menuBuildingStorey        : [{
                                text    : buildingStoreyEditPermissions ? $lang("msg_buildingstorey_update") : $lang("msg_buildingstorey_view"),
                                iconCls : buildingStoreyEditPermissions ? 'viz-icon-small-buildingstorey-update' : 'viz-icon-small-buildingstorey-view',
                                handler : this.onUpdateBuildingStorey,
                                scope   : this,
                                hidden  : !buildingStoreyViewPermissions && !buildingStoreyEditPermissions
                            }, {
                                text    : $lang("msg_buildingstorey_delete"),
                                iconCls : 'viz-icon-small-buildingstorey-delete',
                                handler : this.onDeleteBuildingStorey,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Building Storey - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized("@@@@ Building Storey - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                                    }), {
                                text    : $lang("msg_space_add"),
                                iconCls : 'viz-icon-small-space-add',
                                handler : this.onAddSpace,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Space - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, {
                                text    : $lang("msg_furniture_add"),
                                iconCls : 'viz-icon-small-furniture-add',
                                handler : this.onAddFurniture,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Furniture - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, {
                                text    : $lang("msg_meter_add"),
                                iconCls : 'viz-icon-small-meter-add',
                                handler : this.onAddMeter,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Meters - Full Control")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized("@@@@ Building Storey - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                                    }), {
                                text    : $lang("msg_psetattributehistorical"),
                                iconCls : 'viz-icon-small-pset',
                                hidden  : !$authorized("@@@@ Building Storey - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control"),
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.PsetDefinition({
                                                UsageName : 'IfcBuildingStorey'
                                            }),
                                    createMenuItemFromRecord : this.createMenuItemFromPsetDefinition.createDelegate(this)
                                }
                            }],
                    menuSpace                 : [{
                                text    : spaceEditPermissions ? $lang("msg_space_update") : $lang("msg_space_view"),
                                iconCls : spaceEditPermissions ? 'viz-icon-small-space-update' : 'viz-icon-small-space-view',
                                handler : this.onUpdateSpace,
                                scope   : this,
                                hidden  : !spaceViewPermissions && !spaceEditPermissions
                            }, {
                                text    : $lang("msg_space_delete"),
                                iconCls : 'viz-icon-small-space-delete',
                                handler : this.onDeleteSpace,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Space - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized("@@@@ Space - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                                    }), {
                                text    : $lang("msg_furniture_add"),
                                iconCls : 'viz-icon-small-furniture-add',
                                handler : this.onAddFurniture,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Furniture - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, {
                                text    : $lang("msg_meter_add"),
                                iconCls : 'viz-icon-small-meter-add',
                                handler : this.onAddMeter,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Meters - Full Control")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized("@@@@ Space - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                                    }), {
                                text    : $lang("msg_psetattributehistorical"),
                                iconCls : 'viz-icon-small-pset',
                                hidden  : !$authorized("@@@@ Space - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control"),
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.PsetDefinition({
                                                UsageName : 'IfcSpace'
                                            }),
                                    createMenuItemFromRecord : this.createMenuItemFromPsetDefinition.createDelegate(this)
                                }
                            }],
                    menuFurniture             : [{
                                text    : furnitureEditPermissions ? $lang("msg_furniture_update") : $lang("msg_furniture_view"),
                                iconCls : furnitureEditPermissions ? 'viz-icon-small-furniture-update' : 'viz-icon-small-furniture-view',
                                handler : this.onUpdateFurniture,
                                scope   : this,
                                hidden  : !furnitureViewPermissions && !furnitureEditPermissions
                            }, {
                                text    : $lang("msg_furniture_delete"),
                                iconCls : 'viz-icon-small-furniture-delete',
                                handler : this.onDeleteFurniture,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Furniture - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized("@@@@ Furniture - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                                    }), {
                                text    : $lang("msg_meter_add"),
                                iconCls : 'viz-icon-small-meter-add',
                                handler : this.onAddMeter,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Meters - Full Control")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized("@@@@ Furniture - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control")
                                    }), {
                                text    : $lang("msg_psetattributehistorical"),
                                iconCls : 'viz-icon-small-pset',
                                hidden  : !$authorized("@@@@ Furniture - Full Control") && !$authorized("@@@@ Spatial Hierarchy Tree - Full Control"),
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.PsetDefinition({
                                                UsageName : 'IfcFurniture'
                                            }),
                                    createMenuItemFromRecord : this.createMenuItemFromPsetDefinition.createDelegate(this)
                                }
                            }],
                    menuMeter                 : [{
                                text    : $lang("msg_meter_view"),
                                iconCls : 'viz-icon-small-display',
                                handler : this.onDisplayMeter,
                                scope   : this
                            }, {
                                text    : $lang("msg_meterdata"),
                                iconCls : 'viz-icon-small-meterdata',
                                handler : this.onDisplayMeterData,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Meters - Full Control")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized('@@@@ Meters - Full Control')
                                    }), {
                                text    : meterEditPermissions ? $lang("msg_meter_update") : $lang("msg_meter_view_form"),
                                iconCls : meterEditPermissions ? 'viz-icon-small-meter-update' : 'viz-icon-small-meter-view', 
                                handler : this.onUpdateMeter,
                                scope   : this,
                                hidden  : !meterViewPermissions && !meterEditPermissions
                            }, {
                                text    : $lang("msg_meter_delete"),
                                iconCls : 'viz-icon-small-meter-delete',
                                handler : this.onDeleteMeter,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Meters - Full Control")
                            }, new Ext.menu.Separator({
                                        hidden : !$authorized('@@@@ Meters - Full Control')
                                    }), {
                                text    : $lang("msg_psetattributehistorical"),
                                iconCls : 'viz-icon-small-pset',
                                hidden  : !$authorized("@@@@ Meters - Full Control"),
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.PsetDefinition({
                                                UsageName : 'IfcMeter'
                                            }),
                                    createMenuItemFromRecord : this.createMenuItemFromPsetDefinition.createDelegate(this)
                                }
                            }],
                    menuAlarmDefinition       : [{
                                text    : alarmDefinitionEditPermissions ? $lang("msg_alarmdefinition_update") : $lang("msg_alarmdefinition_view"),
                                iconCls : alarmDefinitionEditPermissions ? 'viz-icon-small-alarm-update' : 'viz-icon-small-alarm-view',
                                handler : this.onUpdateAlarmDefinition,
                                scope   : this,
                                hidden  : !alarmDefinitionViewPermissions && !alarmDefinitionEditPermissions
                            }, {
                                text    : $lang("msg_alarmdefinition_delete"),
                                iconCls : 'viz-icon-small-alarm-delete',
                                handler : this.onDeleteAlarmDefinition,
                                scope   : this,
                                hidden  : !$authorized("@@@@ Alarm Definition - Full Control")
                            }],
                    formConfigSite            : {
                        common : {
                            width            : 680,
                            height           : 400,
                            mode_load        : 'remote',
                            xtype            : 'vizFormSite',
                            messageSaveStart : 'SpatialChangeStart',
                            messageSaveEnd   : 'SpatialChange',
                            readonly         : siteViewPermissions
                        },
                        create : {
                            title   : $lang("msg_site_add"),
                            iconCls : 'viz-icon-small-site-add'
                        },
                        update : {
                            title               : $lang("msg_site_update"),
                            iconCls             : 'viz-icon-small-site-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeySite'
                                    }]
                        }
                    },
                    formConfigBuilding        : {
                        common : {
                            width            : 680,
                            height           : 400,
                            mode_load        : 'remote',
                            xtype            : 'vizFormBuilding',
                            messageSaveStart : 'SpatialChangeStart',
                            messageSaveEnd   : 'SpatialChange',
                            readonly         : buildingViewPermissions
                        },
                        create : {
                            title   : $lang("msg_building_add"),
                            iconCls : 'viz-icon-small-building-add'
                        },
                        update : {
                            title               : $lang("msg_building_update"),
                            iconCls             : 'viz-icon-small-building-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyBuilding'
                                    }]
                        }
                    },
                    formConfigBuildingStorey  : {
                        common : {
                            width            : 680,
                            height           : 400,
                            mode_load        : 'remote',
                            xtype            : 'vizFormBuildingStorey',
                            messageSaveStart : 'SpatialChangeStart',
                            messageSaveEnd   : 'SpatialChange',
                            readonly         : buildingStoreyViewPermissions
                        },
                        create : {
                            title   : $lang("msg_buildingstorey_add"),
                            iconCls : 'viz-icon-small-buildingstorey-add'
                        },
                        update : {
                            title               : $lang("msg_buildingstorey_update"),
                            iconCls             : 'viz-icon-small-buildingstorey-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyBuildingStorey'
                                    }]
                        }
                    },
                    formConfigSpace           : {
                        common : {
                            width            : 680,
                            height           : 400,
                            mode_load        : 'remote',
                            xtype            : 'vizFormSpace',
                            messageSaveStart : 'SpatialChangeStart',
                            messageSaveEnd   : 'SpatialChange',
                            readonly         : spaceViewPermissions
                        },
                        create : {
                            title   : $lang("msg_space_add"),
                            iconCls : 'viz-icon-small-space-add'
                        },
                        update : {
                            title               : $lang("msg_space_update"),
                            iconCls             : 'viz-icon-small-space-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeySpace'
                                    }]
                        }
                    },
                    formConfigFurniture       : {
                        common : {
                            width            : 680,
                            height           : 400,
                            mode_load        : 'remote',
                            xtype            : 'vizFormFurniture',
                            messageSaveStart : 'SpatialChangeStart',
                            messageSaveEnd   : 'SpatialChange',
                            readonly         : furnitureViewPermissions
                        },
                        create : {
                            title   : $lang("msg_furniture_add"),
                            iconCls : 'viz-icon-small-furniture-add'
                        },
                        update : {
                            title               : $lang("msg_furniture_update"),
                            iconCls             : 'viz-icon-small-funriture-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyFurniture'
                                    }]
                        }
                    },
                    formConfigMeter           : {
                        common : {
                            width            : 700,
                            height           : 430,
                            mode_load        : 'remote',
                            xtype            : 'vizFormMeter',
                            messageSaveStart : 'SpatialChangeStart',
                            messageSaveEnd   : 'SpatialChange',
                            readonly         : meterViewPermissions
                        },
                        create : {
                            title   : $lang("msg_meter_add"),
                            iconCls : 'viz-icon-small-meter-add'
                        },
                        update : {
                            title               : $lang("msg_meter_update"),
                            iconCls             : 'viz-icon-small-meter-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyMeter'
                                    }]
                        }
                    },
                    formConfigAlarmDefinition : {
                        common : {
                            width            : 800,
                            height           : 450,
                            xtype            : 'vizFormAlarmDefinition',
                            messageSaveStart : 'AlarmDefinitionChangeStart',
                            messageSaveEnd   : 'AlarmDefinitionChange',
                            readonly         : alarmDefinitionViewPermissions
                        },
                        create : {
                            title   : $lang("msg_alarmdefinition_add"),
                            iconCls : 'viz-icon-small-alarm-add'
                        },
                        update : {
                            title               : $lang("msg_alarmdefinition_update"),
                            iconCls             : 'viz-icon-small-alarm-update',
                            titleEntity         : 'Title',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyAlarmDefinition'
                                    }]
                        }
                    }
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                config.plugins = [new Viz.plugins.RapidSearch({
                            store : {
                                xtype          : 'vizStoreRapidSearch',
                                serviceHandler : Viz.Services.CoreWCF.Spatial_GetStoreByKeyword,
                                baseParams     : {
                                    filter : 'LocalId Name LongPath ShortPath'
                                }
                            },
                            searchName: 'Locations'
                        })];

                Viz.tree.Spatial.superclass.constructor.call(this, config);

                this.on('click', this.onNodeClick, this);

                // register to SpatialChangeStart and SpatialChange.
                // Viz.util.MessageBusMgr.subscribe("SpatialChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.subscribe("SpatialChange", this.ReloadNode, this);
                Viz.util.MessageBusMgr.subscribe("MeterDeleteSpatial", this.onMeterDeleteSpatial, this);
                Viz.util.MessageBusMgr.subscribe("AlarmDefinitionDeleteSpatial", this.onAlarmDefinitionDeleteSpatial, this);
            },
            /**
             * @private OnDestroy.
             */
            onDestroy                        : function() {
                // Viz.util.MessageBusMgr.unsubscribe("SpatialChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.unsubscribe("SpatialChange", this.ReloadNode, this);
                Viz.util.MessageBusMgr.unsubscribe("MeterDeleteSpatial", this.onMeterDeleteSpatial, this);
                Viz.util.MessageBusMgr.unsubscribe("AlarmDefinitionDeleteSpatial", this.onAlarmDefinitionDeleteSpatial, this);
                Viz.tree.Spatial.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Create extra menu entries based on the spatial pset definitions.
             * @param {} record
             */
            createMenuItemFromPsetDefinition : function(record) {
                var retVal = [];

                if (Ext.isArray(record.data.Attributes)) {
                    for (var i = 0; i < record.data.Attributes.length; i++) {
                        var att = record.data.Attributes[i];
                        if (att.AttributeComponent == 'vizGridPsetAttributeHistorical') {
                            var label = record.data.Label + ' - ' + $lang(att.AttributeLabel);
                            retVal.push({
                                        text    : record.data.Label + ' - ' + $lang(att.AttributeLabel),
                                        iconCls : 'viz-icon-small-psetattributehistorical',
                                        handler : this.onPsetAttributeHistoricalUpdate.createDelegate(this, [att.KeyPropertySingleValue, label])
                                    });
                        }
                    }
                }
                return retVal;
            },

            /**
             * Listener for the pset attribute historical menu entry.
             * @param {} KeyPropertySingleValue
             * @param {} label
             */
            onPsetAttributeHistoricalUpdate  : function(KeyPropertySingleValue, label) {
                var node = this.getSelectionModel().getSelectedNode();
                var win = new Ext.Window({
                            title   : node.text + ' - ' + label,
                            iconCls : 'viz-icon-small-psetattributehistorical',
                            width   : 700,
                            height  : 300,
                            layout  : 'fit',
                            items   : [{
                                        xtype                  : 'vizGridPsetAttributeHistorical',
                                        KeyObject              : node.attributes.Key,
                                        KeyPropertySingleValue : KeyPropertySingleValue
                                    }]
                        });
                win.show();
            },

            /**
             * Handles the beforecontextmenu event and gives a chance to change the menu
             * @param {Viz.tree.Pset} tree
             * @param {Ext.tree.TreeNode} node
             */
            onBeforeContextMenu              : function(tree, node) {
                if (!Ext.isObject(node.attributes.entity))
                    return false;
                if (node.attributes.Filtertype == Vizelia.FOL.BusinessEntities.FilterType.Ascendant)
                    return false;
                switch (node.attributes.entity.IfcType) {
                    case "IfcBuildingStorey" :
                        this.ctxMenu = new Ext.menu.Menu({
                                    items : this.menuBuildingStorey.concat(this.menuCalendar).concat(this.menuProject)
                                });
                        break;
                    case "IfcSpace" :
                        this.ctxMenu = new Ext.menu.Menu({
                                    items : this.menuSpace.concat(this.menuCalendar).concat(this.menuProject)
                                });
                        break;
                    case "IfcFurniture" :
                        this.ctxMenu = new Ext.menu.Menu({
                                    items : this.menuFurniture.concat(this.menuCalendar).concat(this.menuProject)
                                });
                        break;
                    case "IfcBuilding" :
                        this.ctxMenu = new Ext.menu.Menu({
                                    items : this.menuBuilding.concat(this.menuCalendar).concat(this.menuProject)
                                });
                        break;
                    case "IfcSite" :
                        this.ctxMenu = new Ext.menu.Menu({
                                    items : this.menuSite.concat(this.menuCalendar).concat(this.menuProject)
                                });
                        break;
                    case "IfcMeter" :
                        this.ctxMenu = new Ext.menu.Menu({
                                    items : this.menuMeter.concat(this.menuCalendar)
                                });
                        break;
                    default :
                        if (node.attributes && node.attributes.entity) {
                            var type = Viz.getEntityType(node.attributes.entity.__type);
                            if (type == 'Vizelia.FOL.BusinessEntities.AlarmDefinition') {
                                this.ctxMenu = new Ext.menu.Menu({
                                            items : this.menuAlarmDefinition
                                        });
                            }
                        }
                        break;
                }

                if (!this.ctxMenu || !this.ctxMenu.items || !this.ctxMenu.items.items || this.ctxMenu.items.items.length == 0) {
                    return false;
                }
                var visibleCount = 0;
                Ext.each(this.ctxMenu.items.items, function(item, index) {
                            // if the first visible item is a separator we hide it
                            if (visibleCount == 0 && item.itemCls == 'x-menu-sep')
                                item.hidden = true;

                            if (!item.hidden && item.itemCls != 'x-menu-sep')
                                visibleCount += 1;
                        }, this);
                return visibleCount > 0;
            },

            /**
             * Opens the site form in create mode.
             */
            onAddSite                        : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var entityParent = null;
                if (node) {
                    this.formConfigSite.common.messageSaveEndParams = node.id;
                    entityParent = {
                        KeySiteParent : node.attributes.Key
                    };
                }
                this.openFormCrud(this.formConfigSite, 'create', null, entityParent);
            },

            /**
             * Opens the building form in update mode.
             */
            onUpdateSite                     : function() {
                this.onUpdateItem(this.formConfigSite);
            },

            /**
             * Menu handler for deleting an existing Pset Attribute definition.
             */
            onDeleteSite                     : function() {
                // return;
                this.onDeleteItem(Viz.Services.CoreWCF.Site_Delete, 'SpatialChangeStart', 'SpatialChange');
            },

            onManageProjects                 : function() {
                var node = this.getSelectionModel().getSelectedNode();
                Viz.grid.AzManFilter.displayAzManFilter(node.attributes);
            },

            /**
             * Opens the grid for managing holidays.
             */
            onManageHoliday                  : function() {
                var node = this.getSelectionModel().getSelectedNode();
                Viz.App.Global.ViewPort.openModule({
                            title       : $lang('msg_calendar_manage_holiday') + ' : ' + node.parentNode.text + ' / ' + node.text,
                            entity      : node.attributes.entity,
                            KeyLocation : node.attributes.Key,
                            Category    : 'Holiday',
                            xtype       : 'vizPanelCalendarDefinition'
                        });
            },

            /**
             * Opens the grid for managing extra calendars.
             */
            onManageExtraCalendar            : function(calendarEventCategory) {
                var node = this.getSelectionModel().getSelectedNode();
                Viz.App.Global.ViewPort.openModule({
                            title         : $lang('msg_calendar') + ' ' + calendarEventCategory.Label + ' : ' + node.parentNode.text + ' / ' + node.text,
                            entity        : node.attributes.entity,
                            KeyLocation   : node.attributes.Key,
                            Category      : calendarEventCategory.LocalId,
                            CategoryLabel : calendarEventCategory.Label,
                            xtype         : 'vizPanelCalendarDefinition'
                        });

            },

            /**
             * Opens the calendar for a location.
             */
            onShowCalendar                   : function() {
                var node = this.getSelectionModel().getSelectedNode();
                Viz.App.Global.ViewPort.openModule({
                            title       : $lang('msg_calendar') + ' : ' + node.parentNode.text + ' / ' + node.text,
                            entity      : node.attributes.entity,
                            KeyLocation : node.attributes.Key,
                            xtype       : 'vizPanelCalendarLocation'
                        });

            },

            /**
             * Opens the building form in create mode.
             */
            onAddBuilding                    : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var entityParent = null;
                if (node) {
                    this.formConfigBuilding.common.messageSaveEndParams = node.id;
                    entityParent = {
                        KeySite : node.attributes.Key
                    };
                }
                this.openFormCrud(this.formConfigBuilding, 'create', null, entityParent);
            },

            /**
             * Opens the building form in update mode.
             */
            onUpdateBuilding                 : function() {
                this.onUpdateItem(this.formConfigBuilding);
            },

            /**
             * Menu handler for deleting an existing Pset Attribute definition.
             */
            onDeleteBuilding                 : function() {
                this.onDeleteItem(Viz.Services.CoreWCF.Building_Delete, 'SpatialChangeStart', 'SpatialChange');
            },

            /**
             * Opens the building form in create mode.
             */
            onAddBuildingStorey              : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var entityParent = null;
                if (node) {
                    this.formConfigBuildingStorey.common.messageSaveEndParams = node.id;
                    entityParent = {
                        KeyBuilding : node.attributes.Key
                    };
                }
                this.openFormCrud(this.formConfigBuildingStorey, 'create', null, entityParent);
            },

            /**
             * Opens the building form in update mode.
             */
            onUpdateBuildingStorey           : function() {
                this.onUpdateItem(this.formConfigBuildingStorey);
            },

            /**
             * Menu handler for deleting an existing building.
             */
            onDeleteBuildingStorey           : function() {
                this.onDeleteItem(Viz.Services.CoreWCF.BuildingStorey_Delete, 'SpatialChangeStart', 'SpatialChange');
            },

            /**
             * Opens the space form in create mode.
             */
            onAddSpace                       : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var entityParent = null;
                if (node) {
                    this.formConfigSpace.common.messageSaveEndParams = node.id;
                    entityParent = {
                        KeyBuildingStorey : node.attributes.Key
                    };
                }
                this.openFormCrud(this.formConfigSpace, 'create', null, entityParent);
            },

            /**
             * Opens the space form in update mode.
             */
            onUpdateSpace                    : function() {
                this.onUpdateItem(this.formConfigSpace);
            },

            /**
             * Menu handler for deleting an existing space.
             */
            onDeleteSpace                    : function() {
                this.onDeleteItem(Viz.Services.CoreWCF.Space_Delete, 'SpatialChangeStart', 'SpatialChange');
            },

            /**
             * Opens the Furniture form in create mode.
             */
            onAddFurniture                   : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var entityParent = null;
                if (node) {
                    this.formConfigFurniture.common.messageSaveEndParams = node.id;
                    entityParent = {
                        KeyLocation : node.attributes.Key
                    };
                }
                this.openFormCrud(this.formConfigFurniture, 'create', null, entityParent);
            },

            /**
             * Opens the Furniture form in update mode.
             */
            onUpdateFurniture                : function() {
                this.onUpdateItem(this.formConfigFurniture);
            },

            /**
             * Menu handler for deleting an existing Furniture.
             */
            onDeleteFurniture                : function() {
                this.onDeleteItem(Viz.Services.CoreWCF.Furniture_Delete, 'SpatialChangeStart', 'SpatialChange');
            },

            /**
             * Opens the Meter form in create mode.
             */
            onAddMeter                       : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var entity = null;
                if (node) {
                    this.formConfigMeter.common.messageSaveEndParams = node.id;
                    entity = {
                        KeyLocation      : node.attributes.Key,
                        LocationLongPath : Viz.util.Location.getLongPathFromNode(node)
                    };
                }
                this.openFormCrud(this.formConfigMeter, 'create', entity);
            },

            /**
             * Opens the Meter form in update mode.
             */
            onUpdateMeter                    : function() {
                this.onUpdateItem(this.formConfigMeter);
            },

            /**
             * Menu handler for deleting an existing Pset Attribute definition.
             */
            onDeleteMeter                    : function() {
                // we define here a new MessageBus to be able to catch the deletion of a Meter and 1. refresh the tree, 2. clear any opened meter datag grid for this meter.
                this.onDeleteItem(Viz.Services.EnergyWCF.Meter_Delete, 'SpatialChangeStart', 'MeterDeleteSpatial', $lang('msg_meter_delete_confirmation_single'), $lang('msg_meter_delete_confirmation_multiple'));
            },

            onMeterDeleteSpatial             : function(parentId, item) {
                this.ReloadNode(parentId);
                if (item)
                    Viz.util.MessageBusMgr.publish('MeterDelete', item);
            },

            /**
             * Opens the AlarmDefinition form in update mode.
             */
            onUpdateAlarmDefinition          : function() {
                this.onUpdateItem(this.formConfigAlarmDefinition);
            },

            /**
             * Menu handler for deleting an existing AlarmDefinition definition.
             */
            onDeleteAlarmDefinition          : function() {
                this.onDeleteItem(Viz.Services.EnergyWCF.AlarmDefinition_Delete, 'AlarmDefinitionChangeStart', 'AlarmDefinitionDeleteSpatial')
            },

            onAlarmDefinitionDeleteSpatial   : function(parentId, item) {
                this.ReloadNode(parentId);
            },

            /**
             * Display the chart of a single meter.
             */
            onDisplayMeter                   : function() {
                var node = this.getSelectionModel().getSelectedNode();
                Viz.grid.Meter.displayMeter(node.attributes.entity);
            },

            /**
             * Display the meter data tab.
             */
            onDisplayMeterData               : function() {
                var node = this.getSelectionModel().getSelectedNode();
                Viz.grid.Meter.displayMeterData(node.attributes.entity);
            },

            /**
             * Called while dragging over.
             * @private
             * @param {Object} e dd event
             */
            onNodeDragOver                   : function(e) {
                var dropRules = {
                    'IfcSite'           : ['IfcSite'],
                    'IfcBuilding'       : ['IfcSite'],
                    'IfcBuildingStorey' : ['IfcBuilding'],
                    'IfcSpace'          : ['IfcBuildingStorey'],
                    'IfcFurniture'      : ['IfcSite', 'IfcBuilding', 'IfcBuildingStorey', 'IfcSpace'],
                    'IfcMeter'          : ['IfcSite', 'IfcBuilding', 'IfcBuildingStorey', 'IfcSpace', 'IfcFurniture']
                };
                if (e.dropNode.getDepth() == 0) {
                    e.cancel = true;
                    return false;
                }
                var sourceType = e.dropNode.attributes.entity.IfcType;
                if (e.target.getDepth() == 0) {
                    if (sourceType == "IfcSite" || sourceType == "IfcBuilding") {
                        e.cancel = false
                        return true;
                    }
                    else {
                        e.cancel = true;
                        return false;
                    }
                }

                var targetType = e.target.attributes.entity.IfcType
                if (e.point != 'append') {
                    e.cancel = true;
                    return;
                }

                if (dropRules[sourceType] != null && dropRules[sourceType].indexOf(targetType) >= 0) {
                    e.cancel = false;
                }
                else {
                    e.cancel = true;
                }
            },

            /**
             * Changes the KeyParent of the entitySource based on the new KeyParent.
             * @param {Object} entitySource
             * @param {string} keyParent
             */
            changeParentKey                  : function(entitySource, keyParent) {
                switch (entitySource.IfcType) {
                    case 'IfcSite' :
                        Ext.apply(entitySource, {
                                    KeySiteParent : keyParent
                                });
                        break;

                    case 'IfcBuilding' :
                        Ext.apply(entitySource, {
                                    KeySite : keyParent
                                });
                        break;

                    case 'IfcBuildingStorey' :
                        Ext.apply(entitySource, {
                                    KeyBuilding : keyParent
                                });
                        break;

                    case 'IfcSpace' :
                        Ext.apply(entitySource, {
                                    KeyBuildingStorey : keyParent
                                });
                        break;

                    case 'IfcFurniture' :
                        Ext.apply(entitySource, {
                                    KeyLocation : keyParent
                                });
                        break;

                    case 'IfcMeter' :
                        Ext.apply(entitySource, {
                                    KeyLocation : keyParent
                                });
                        break;
                }
            },
            /**
             * Node click event handler.
             * @param {Node} node
             * @param {Ext.EventObject} ev
             */
            onNodeClick                      : function(node, ev) {
                if (!Ext.isObject(node.attributes.entity))
                    return false;
                switch (node.attributes.entity.IfcType) {
                    case 'IfcSite' :
                    case 'IfcBuilding' :
                    case 'IfcBuildingStorey' :
                    case 'IfcSpace' :
                    case 'IfcFurniture' :
                        Viz.util.MessageBusMgr.fireEvent('SpatialCalendarClick', node);
                        break;
                    case "IfcMeter" :
                        Viz.util.MessageBusMgr.fireEvent('SpatialCalendarClick', node);
                        Viz.util.MessageBusMgr.fireEvent('SpatialMeterClick', node.attributes.entity);
                        break;
                }
            },

            onAssociateClassificationItem    : function(node, ev) {
                var parentNode = this.getSelectionModel().getSelectedNode();
                var entity = parentNode.attributes.entity;

                this.storeObjectClassificationItem = new Viz.store.ObjectClassificationItem({
                            KeyObject : entity.KeyBuilding
                        });

                var win = new Ext.Window({
                            title   : $lang("msg_classificationitem_associate"),
                            iconCls : 'viz-icon-small-classificationitem',
                            width   : 600,
                            height  : 450,
                            layout  : 'fit',
                            items   : [{
                                        name                  : 'FilterClassificationItem',
                                        xtype                 : 'vizGridFilterClassificationItem',
                                        columns               : [{
                                                    dataIndex  : 'LongPath',
                                                    header     : $lang('msg_title'),
                                                    sortable   : true,
                                                    renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                                                        cell.css = 'viz-icon-cell ' + record.get('IconCls');
                                                        return data;
                                                    },
                                                    filterable : true
                                                }],
                                        loadMask              : false,
                                        store                 : this.storeObjectClassificationItem,
                                        getEntityFromTreeNode : function(node) {
                                            return node.attributes.entity;
                                        },
                                        border                : false,
                                        gridPlugins           : [new Viz.plugins.GridExport({
                                                    listeners : {
                                                        beforeExport : function(plugin) {
                                                            plugin.title = $lang('msg_azman_filterclassificationitem') + ' : ' + this.entity.Id
                                                        },
                                                        scope        : this
                                                    }
                                                })]
                                    }],
                            buttons : [{
                                        text    : $lang("msg_save"),
                                        iconCls : 'viz-icon-small-save',
                                        handler : function() {
                                            if (this.storeObjectClassificationItem.hasLoaded)
                                                this.storeObjectClassificationItem.save();

                                            win.getEl().mask();
                                            Viz.Services.CoreWCF.ObjectClassification_SaveStoreByKeyObject({
                                                        KeyObject : entity.KeyBuilding,
                                                        store     : this.storeObjectClassificationItem.crudStore,
                                                        success   : function(value) {
                                                            this.processAjaxSuccess(value, win);
                                                            parentNode.reload();
                                                        },
                                                        failure   : function() {
                                                            this.processAjaxFailure(win);
                                                        },
                                                        scope     : this
                                                    });

                                            win.close();

                                        },
                                        scope   : this
                                    }, {
                                        text    : $lang('msg_view_in_tree'),
                                        iconCls : 'viz-icon-small-classificationitem',
                                        handler : function() {

                                            var w = new Ext.Window({
                                                        title  : $lang('msg_view_in_tree'),
                                                        width  : 400,
                                                        height : 300,
                                                        layout : 'fit',
                                                        items  : [{
                                                                    xtype           : 'vizTreeClassificationItem',
                                                                    rootVisible     : false,
                                                                    border          : false,
                                                                    displayCheckbox : true,
                                                                    pathSeparator   : ' / '
                                                                }]
                                                    });
                                            w.show();
                                            var tree = w.findByType('vizTreeClassificationItem')[0];
                                            var const_root_header = String.format(' / {0} / ROOT / ', tree.root.text);
                                            var explore = function(bSuccess, oSelNode) {
                                                if (!bSuccess)
                                                    return;
                                                oSelNode.checked = true;
                                                oSelNode.ui.toggleCheck(true);
                                            };

                                            this.storeObjectClassificationItem.each(function(rec) {
                                                        Viz.Configuration.ClassificationItemDefinition.each(function(r) {
                                                                    if (rec.data['Code'].indexOf(r.data['Code']) == 0) {
                                                                        tree.selectPath(const_root_header + rec.data.LongPath, 'text', explore);
                                                                        tree.selectPath(const_root_header + r.data['Title'] + ' / ' + rec.data.LongPath, 'text', explore);
                                                                    }
                                                                });

                                                    }, this);

                                        },
                                        scope   : this

                                    }]
                        });
                win.show();
            },
            /**
             * Generic handler after calling a WCF function in case of a success.
             * @param {bool} value
             * @param {Ext.Window} win
             */
            processAjaxSuccess                     : function(value, win) {
                if (win) {
                    win.getEl().unmask();
                    win.close();
                }
                if (!value)
                    Ext.Msg.show({
                                width   : 400,
                                title   : $lang("error_ajax"),
                                msg     : $lang("error_save"),
                                buttons : Ext.Msg.OK,
                                icon    : Ext.MessageBox.ERROR
                            });
            },
            /**
             * Generic handler after calling a WCF function in case of an error.
             * @param {Ext.Window} win
             */
            processAjaxFailure                     : function(win) {
                if (win) {
                    win.getEl().unmask();
                    win.close();
                }
                Ext.Msg.show({
                            width   : 400,
                            title   : $lang("error_ajax"),
                            msg     : $lang("error_save"),
                            buttons : Ext.Msg.OK,
                            icon    : Ext.MessageBox.ERROR
                        });
            }

        });

Ext.reg("vizTreeSpatial", Viz.tree.Spatial);

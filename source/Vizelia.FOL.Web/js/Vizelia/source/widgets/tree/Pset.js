﻿Ext.namespace('Viz.tree');
/**
 * Summary.
 * @class Viz.tree.Pset
 * @extends Viz.tree.TreePanel
 */
Viz.tree.Pset = Ext.extend(Viz.tree.TreePanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                   : function(config) {
                config = config || {};
                var defaultConfig = {
                    title   : $lang('msg_pset_tree'),
                    iconCls : 'viz-icon-small-pset'
                };
                var forcedConfig = {
                    serviceHandler                    : Viz.Services.CoreWCF.Pset_GetTree,
                    root                              : {
                        id      : ' ',
                        iconCls : 'viz-icon-small-pset',
                        text    : $lang('msg_pset_root')
                    },
                    menuPsetAttributeDefinition       : [{
                                text    : $lang("msg_psetattribute_update"),
                                iconCls : 'viz-icon-small-psetattribute-update',
                                handler : this.UpdatePsetAttributeDefinition,
                                scope   : this
                            }, {
                                text    : $lang("msg_psetattribute_delete"),
                                iconCls : 'viz-icon-small-psetattribute-delete',
                                handler : this.DeletePsetAttributeDefinition,
                                scope   : this
                            }],
                    menuPsetDefinition                : [{
                                text    : $lang("msg_pset_update"),
                                iconCls : 'viz-icon-small-pset-update',
                                handler : this.UpdatePsetDefinition,
                                scope   : this
                            }, {
                                text    : $lang("msg_pset_delete"),
                                iconCls : 'viz-icon-small-pset-delete',
                                handler : this.DeletePsetDefinition,
                                scope   : this
                            }, '-', {
                                text    : $lang("msg_psetattribute_add"),
                                iconCls : 'viz-icon-small-psetattribute-add',
                                handler : this.CreatePsetAttributeDefinition,
                                scope   : this
                            }],
                    menuRoot                          : [{
                                text    : $lang("msg_pset_add"),
                                iconCls : 'viz-icon-small-pset-add',
                                handler : this.CreatePsetDefinition,
                                scope   : this
                            }],
                    formConfigPsetDefinition          : {
                        common : {
                            width            : 500,
                            height           : 350,
                            mode_load        : 'local',
                            xtype            : 'vizFormPsetDefinition',
                            messageSaveStart : 'PsetChangeStart',
                            messageSaveEnd   : 'PsetChange'
                        },
                        create : {
                            title   : $lang("msg_pset_add"),
                            iconCls : 'viz-icon-small-pset-add'
                        },
                        update : {
                            title       : $lang("msg_pset_update"),
                            iconCls     : 'viz-icon-small-pset-update',
                            titleEntity : 'PsetName'
                        }
                    },
                    formConfigPsetAttributeDefinition : {
                        common : {
                            width            : 720,
                            height           : 450,
                            mode_load        : 'local',
                            xtype            : 'vizFormPsetAttributeDefinition',
                            messageSaveStart : 'PsetChangeStart',
                            messageSaveEnd   : 'PsetChange'
                        },
                        create : {
                            title   : $lang("msg_psetattribute_add"),
                            iconCls : 'viz-icon-small-psetattribute-add'
                        },
                        update : {
                            title       : $lang("msg_psetattribute_update"),
                            iconCls     : 'viz-icon-small-psetattribute-update',
                            titleEntity : 'AttributeName'
                        }
                    }
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.tree.Pset.superclass.constructor.call(this, config);

                // register to PsetChangeStart and PsetChange.
                Viz.util.MessageBusMgr.subscribe("PsetChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.subscribe("PsetChange", this.ReloadNode, this);
            },
            /**
             * @private OnDestroy.
             */
            onDestroy                     : function() {
                Viz.util.MessageBusMgr.unsubscribe("PsetChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.unsubscribe("PsetChange", this.ReloadNode, this);
                Viz.tree.Pset.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * Menu handler for creating a new Pset definition.
             */
            CreatePsetDefinition          : function() {
                this.onAddItem(this.formConfigPsetDefinition);
            },
            /**
             * Menu handler for updating an existing Pset definition.
             */
            UpdatePsetDefinition          : function() {
                this.onUpdateItem(this.formConfigPsetDefinition);
            },
            /**
             * Menu handler for deleting an existing Pset definition.
             */
            DeletePsetDefinition          : function() {
                this.onDeleteItem(Viz.Services.CoreWCF.PsetDefinition_Delete, 'PsetChangeStart', 'PsetChange');
            },
            /**
             * Menu handler for creating a new Pset Attribute definition.
             */
            CreatePsetAttributeDefinition : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var entityParent = {
                    KeyPropertySet : node.attributes.entity.KeyPropertySet
                };
                var formConfig = this.formConfigPsetAttributeDefinition;
                formConfig.common.messageSaveEndParams = node.id;
                this.openFormCrud(formConfig, 'create', null, entityParent);
            },
            /**
             * Menu handler for updating an existing Pset Attribute definition.
             */
            UpdatePsetAttributeDefinition : function() {
                this.onUpdateItem(this.formConfigPsetAttributeDefinition);
            },
            /**
             * Menu handler for deleting an existing Pset Attribute definition.
             */
            DeletePsetAttributeDefinition : function() {
                this.onDeleteItem(Viz.Services.CoreWCF.PsetAttributeDefinition_Delete, 'PsetChangeStart', 'PsetChange');
            },
            /**
             * Handles the beforecontextmenu event and gives a chance to change the menu
             * @param {Viz.tree.Pset} tree
             * @param {Ext.tree.TreeNode} node
             */
            onBeforeContextMenu           : function(tree, node) {
                if (node.isRoot)
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.menuRoot
                            });
                else if (node.attributes.entity.IfcType == "PsetAttributeDefinition")
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.menuPsetAttributeDefinition
                            });
                else if (node.attributes.entity.IfcType == "PsetDefinition")
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.menuPsetDefinition
                            });
            }
        });
Ext.reg('vizTreePset', Viz.tree.Pset);
/**
 * Summary.
 * @class Viz.tree.AzManFilter
 * @extends Viz.tree.TreePanel
 */
Viz.tree.Pset2 = Ext.extend(Viz.tree.TreePanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                       : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    title             : $lang('msg_pset_tree'),
                    iconCls           : 'viz-icon-small-pset',
                    serviceHandler    : Viz.Services.CoreWCF.Pset_GetTree,
                    root              : {
                        id      : ' ',
                        iconCls : 'viz-icon-small-pset',
                        text    : $lang('msg_pset_root')
                    },
                    menuPsetAttribute : [{
                                text    : $lang("msg_psetattribute_update"),
                                iconCls : 'viz-icon-small-psetattribute-update',
                                handler : this.onUpdateAttribute,
                                scope   : this
                            }, {
                                text    : $lang("msg_psetattribute_delete"),
                                iconCls : 'viz-icon-small-psetattribute-delete',
                                handler : this.onDeleteAttribute,
                                scope   : this
                            }],
                    menu              : [{
                                text    : $lang("msg_pset_update"),
                                iconCls : 'viz-icon-small-pset-update',
                                handler : this.onUpdateItem,
                                scope   : this
                            }, {
                                text    : $lang("msg_pset_delete"),
                                iconCls : 'viz-icon-small-pset-delete',
                                handler : this.onDeleteItem,
                                scope   : this
                            }, '-', {
                                text    : $lang("msg_psetattribute_add"),
                                iconCls : 'viz-icon-small-psetattribute-add',
                                handler : this.onCreateAttribute,
                                scope   : this
                            }],
                    menuRoot          : [{
                                text    : $lang("msg_pset_add"),
                                iconCls : 'viz-icon-small-pset-add',
                                handler : this.onCreateItem,
                                scope   : this
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.tree.Pset2.superclass.constructor.call(this, config);
                // register to PsetChangeStart and PsetChange.
                Viz.util.MessageBusMgr.subscribe("PsetChangeStart", this.onChangeStart, this);
                Viz.util.MessageBusMgr.subscribe("PsetChangeEnd", this.onChangeEnd, this);
            },
            /**
             * Handles the beforecontextmenu event and gives a chance to change the menu
             * @param {Viz.tree.Pset} tree
             * @param {Ext.tree.TreeNode} node
             */
            onBeforeContextMenu               : function(tree, node) {
                if (node.isRoot)
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.menuRoot
                            });
                else {
                    if (node.leaf)
                        this.ctxMenu = new Ext.menu.Menu({
                                    items : this.menuPsetAttribute
                                });
                    else
                        this.ctxMenu = new Ext.menu.Menu({
                                    items : this.menu
                                });
                }
            },
            /**
             * Indicates that a user operation is starting. It should be followed by onUserManagementChange.
             */
            onChangeStart                     : function() {
                this.getEl().mask();
            },
            /**
             * Indicates that an operation occured and the panel should be reload.
             */
            onChangeEnd                       : function(nodeId) {
                this.getEl().mask();
                var node = this.getNodeById(nodeId) || this.getRootNode();
                node.reload();
                // this.getLoader().load(node, function() {
                // node.reload();
                this.getEl().unmask();
                // }, this);
            },
            /**
             * Creates a new PsetDefinition. Shows the PsetDefinition window form in create mode.
             */
            onCreateItem                      : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var config = {
                    title   : $lang("msg_pset_add"),
                    iconCls : 'viz-icon-small-pset-add',
                    mode    : 'create'
                }
                this.showWindowPsetDefinition(config);
            },
            /**
             * Updates a PsetDefinition. Shows the Psetdefinition window form in update mode
             */
            onUpdateItem                      : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var config = {
                    entity  : node.attributes.entity,
                    title   : $lang("msg_pset_update") + " : " + node.text,
                    iconCls : 'viz-icon-small-pset-update',
                    mode    : "update"
                };
                this.showWindowPsetDefinition(config);
            },
            /**
             * Deletes a PsetDefinition.
             */
            onDeleteItem                      : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var psetName = node.text;
                var psetId = node.id;
                var msgDelete;
                msgDelete = String.format($lang("msg_pset_delete_confirmation_single"), psetName);
                Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                            if (button == 'yes') {
                                Viz.util.MessageBusMgr.publish('PsetChangeStart');
                                Viz.Services.CoreWCF.PsetDefinition_Delete({
                                            Key     : psetId,
                                            success : function() {
                                                node.parentNode.reload();
                                                Viz.util.MessageBusMgr.publish('PsetChangeEnd');
                                            },
                                            failure : function(error, arg, callname) {
                                                alert(error.Message);
                                            },
                                            scope   : this
                                        });
                            }
                        }, this);
            },
            /**
             * Shows (opens) the window containing the form to change a PsetDefinition.
             * @param {Object} config A configuration object which may contains the following properties
             * <ul>
             * <li><b>title</b> {String} The title of the window.</li>
             * <li><b>iconCls</b> {String} The iconCls of the window.</li>
             * <li><b>mode</b> {String} Either <tt>create</tt> or <tt>update</tt>.</li>
             * <li><b>entity</b> {Viz.BusinessEntity.PsetDefinition} The entity to open the form with.</li>
             * <ul>
             */
            showWindowPsetDefinition          : function(config) {
                Ext.apply(config, {
                            xtype        : 'vizFormPsetDefinition',
                            entityParent : null
                        });
                this.showWindow(config);
            },
            /**
             * Creates a new PsetAttributeDefinition. Shows the PsetAttributionDefinition window form in create mode.
             */
            onCreateAttribute                 : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var config = {
                    entity       : null,
                    entityParent : node.attributes.entity,
                    title        : $lang("msg_psetattribute_add"),
                    iconCls      : 'viz-icon-small-psetattribute-add',
                    mode         : 'create'
                };
                this.showWindowPsetDefinitionAttribute(config);
            },
            /**
             * Updates a PsetDefinitionAttribute. Shows the PsetDefinitionAttribute window form in update mode.
             */
            onUpdateAttribute                 : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var config = {
                    entity  : node.attributes.entity,
                    title   : $lang("msg_psetattribute_update") + " : " + node.text,
                    mode    : 'update',
                    iconCls : 'viz-icon-small-psetattribute-update'
                };
                this.showWindowPsetDefinitionAttribute(config)
            },
            /**
             * Deletes a PsetDefinitionAttribute.
             */
            onDeleteAttribute                 : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var psetAttributeName = node.text;
                var psetAttributeId = node.id;
                var msgDelete;
                msgDelete = String.format($lang("msg_psetattribute_delete_confirmation_single"), psetAttributeName);
                Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                            if (button == 'yes') {
                                Viz.util.MessageBusMgr.publish('PsetChangeStart');
                                Viz.Services.CoreWCF.PsetAttributeDefinition_Delete({
                                            Key     : psetAttributeId,
                                            success : function() {
                                                Viz.util.MessageBusMgr.publish('PsetChangeEnd', node.parentNode.id);
                                            },
                                            failure : function(error, arg, callname) {
                                                alert(error.Message);
                                            },
                                            scope   : this
                                        });
                            }
                        }, this);
            },
            /**
             * Shows (opens) the window containing the form to change a PsetDefinitionAttribute.
             * @param {Object} config A configuration object which may contains the following properties
             * <ul>
             * <li><b>title</b> {String} The title of the window.</li>
             * <li><b>iconCls</b> {String} The iconCls of the window.</li>
             * <li><b>mode</b> {String} Either <tt>create</tt> or <tt>update</tt>.</li>
             * <li><b>entity</b> {Viz.BusinessEntity.PsetAttributeDefinition} The entity to open the form with.</li>
             * <li><b>entityParent</b> {Viz.BusinessEntity.PsetDefinition} The entity parent to open the form with.</li>
             * </ul>
             */
            showWindowPsetDefinitionAttribute : function(config) {
                Ext.apply(config, {
                            height : 450,
                            width  : 650,
                            xtype  : 'vizFormPsetAttributeDefinition'
                        });
                this.showWindow(config);
            },
            /**
             * @param {Object} config A config object which may contains the following properties
             * <ul>
             * <li><b>title</b> {String} The title of the window.</li>
             * <li><b>width</b> {Integer} (Optional) The width of the window.</li>
             * <li><b>height</b> {Integer} (Optional) The height of the window.</li>
             * <li><b>iconCls</b> {String} The iconCls property of the window.</li>
             * <li><b>mode</b> {String} Either <tt>create</tt> or <tt>update</tt>.</li>
             * <li><b>entity</b> {Object} The entity to open the form with.</li>
             * <li><b>entityParent</b> {Object} (Optional) The parent entity to open the form with.</li>
             * <li><b>xtype</b> {String} The xtype of the form.</li>
             * </ul>
             */
            showWindow                        : function(config) {
                Ext.applyIf(config, {
                            width  : 600,
                            height : 400,
                            mode   : 'create'
                        });
                var win = new Ext.Window({
                            title   : config.title,
                            iconCls : config.iconCls,
                            modal   : true,
                            width   : config.width,
                            height  : config.height,
                            layout  : 'fit',
                            items   : [{
                                        xtype        : config.xtype,
                                        mode         : config.mode,
                                        entity       : config.entity,
                                        entityParent : config.entityParent
                                    }]
                        });
                win.show();
            }
        });
Ext.reg('vizTreePset2', Viz.tree.Pset2);
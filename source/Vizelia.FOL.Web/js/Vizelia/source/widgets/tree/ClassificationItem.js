﻿Ext.namespace('Viz.tree');
/**
 * Summary.
 * @class Viz.tree.ClassificationItem
 * @extends Viz.tree.TreePanel
 */
Viz.tree.ClassificationItem = Ext.extend(Viz.tree.TreePanel, {
            /**
             * @cfg {Object} root the root node
             */
            /**
             * @cfg {Object} menu the default menu configuration
             */
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                            : function(config) {
                config = config || {};
                var defaultConfig = {
                    title                              : $lang('msg_tree_classificationitem'),
                    iconCls                            : 'viz-icon-small-classificationitem',
                    rootVisible                        : false,
                    serviceHandler                     : Viz.Services.CoreWCF.ClassificationItem_GetTree,
                    serviceParams                      : {
                        flgFilter : true
                    },
                    pathSeparator                      : ' / ',
                    root                               : config.root || {
                        id      : ' ',
                        iconCls : 'viz-icon-small-classificationitem',
                        text    : $lang('RootTitle')
                    },

                    /*
                     * dragConfig : { onBeforeDrag : function(data, e) { // we exclude from drag Root node, non IfcClassificationItem nodes, Relationship nodes if (data.node.isRoot) return false; if (data.node.attributes.entity.IfcType != 'IfcClassificationItem') return false; if (data.node.attributes.entity.Relation != null) return false; return true; } },
                     */
                    menuClassificationItemRequest      : [{
                                text    : $lang("msg_classificationitem_add"),
                                iconCls : 'viz-icon-small-add',
                                handler : this.AddClassificationItem,
                                scope   : this
                            }, {
                                text    : $lang("msg_classificationitem_update"),
                                iconCls : 'viz-icon-small-update',
                                handler : this.UpdateClassificationItem,
                                scope   : this
                            }, {
                                text    : $lang("msg_classificationitem_delete"),
                                iconCls : 'viz-icon-small-delete',
                                handler : this.DeleteClassificationItem,
                                scope   : this
                            }],
                    /*
                     * dragConfig : { onBeforeDrag : function(data, e) { // we exclude from drag Root node, non IfcClassificationItem nodes, Relationship nodes if (data.node.isRoot) return false; if (data.node.attributes.entity.IfcType != 'IfcClassificationItem') return false; if (data.node.attributes.entity.Relation != null) return false; return true; } },
                     */
                    menuClassificationItem             : [{
                                text    : $lang("msg_classificationitem_add"),
                                iconCls : 'viz-icon-small-add',
                                handler : this.AddClassificationItem,
                                scope   : this
                            }, {
                                text    : $lang("msg_classificationitem_update"),
                                iconCls : 'viz-icon-small-update',
                                handler : this.UpdateClassificationItem,
                                scope   : this
                            }, {
                                text    : $lang("msg_classificationitem_delete"),
                                iconCls : 'viz-icon-small-delete',
                                handler : this.DeleteClassificationItem,
                                scope   : this
                            }, '-', {
                                category : [],
                                text     : $lang("msg_pset_associate"),
                                iconCls  : 'viz-icon-small-pset',
                                handler  : this.AssociatePset,
                                scope    : this
                            }, {
                                text     : $lang("msg_pset_clean"),
                                iconCls  : 'viz-icon-small-clean',
                                handler  : this.CleanPset,
                                scope    : this,
                                disabled : false
                            }],
                    menuClassificationItemAction       : [{
                                text    : $lang("msg_classificationitem_add"),
                                iconCls : 'viz-icon-small-add',
                                handler : this.AddClassificationItem,
                                scope   : this
                            }, {
                                text    : $lang("msg_classificationitem_update"),
                                iconCls : 'viz-icon-small-update',
                                handler : this.UpdateClassificationItem,
                                scope   : this
                            }, {
                                text    : $lang("msg_classificationitem_delete"),
                                iconCls : 'viz-icon-small-delete',
                                handler : this.DeleteClassificationItem,
                                scope   : this
                            }, '-', {
                                text    : $lang("msg_pset_associate"),
                                iconCls : 'viz-icon-small-pset',
                                handler : this.AssociatePset,
                                scope   : this
                            }, {
                                text    : $lang("msg_workflowassemblydefinition_associate"),
                                iconCls : 'viz-icon-small-workflow',
                                handler : this.AssociateWorkflowAssemblyDefinition,
                                scope   : this
                            }],
                    menuClassificationItemWork         : [{
                                text    : $lang("msg_classificationitem_add"),
                                iconCls : 'viz-icon-small-add',
                                handler : this.AddClassificationItem,
                                scope   : this
                            }, {
                                text    : $lang("msg_classificationitem_update"),
                                iconCls : 'viz-icon-small-update',
                                handler : this.UpdateClassificationItem,
                                scope   : this
                            }, {
                                text    : $lang("msg_classificationitem_delete"),
                                iconCls : 'viz-icon-small-delete',
                                handler : this.DeleteClassificationItem,
                                scope   : this
                            }, '-', {
                                text    : $lang("msg_priority_associate"),
                                iconCls : 'viz-icon-small-priority',
                                handler : this.AssociatePriority,
                                scope   : this
                            }, {
                                text    : $lang("msg_workflowassemblydefinition_associate"),
                                iconCls : 'viz-icon-small-workflow',
                                handler : this.AssociateWorkflowAssemblyDefinition,
                                scope   : this
                            }, {
                                text    : $lang("msg_pset_associate"),
                                iconCls : 'viz-icon-small-pset',
                                handler : this.AssociatePset,
                                scope   : this
                            }, {
                                text     : $lang("msg_pset_clean"),
                                iconCls  : 'viz-icon-small-clean',
                                handler  : this.CleanPset,
                                scope    : this,
                                disabled : false
                            }],
                    menuPset                           : [{
                                text    : $lang("msg_pset_deassociate"),
                                iconCls : 'viz-icon-small-association-delete',
                                handler : this.DeassociatePset,
                                scope   : this
                            }, {
                                text    : $lang("msg_pset_deassociate_and_clean"),
                                iconCls : 'viz-icon-small-clean',
                                handler : this.DeassociatePsetAndClean,
                                scope   : this
                            }

                    ],
                    menuClassificationItemRelationship : [{
                                text    : $lang("msg_priority_associate"),
                                iconCls : 'viz-icon-small-association-add',
                                handler : this.AssociatePriority,
                                scope   : this
                            }, {
                                text    : $lang("msg_workflowassemblydefinition_associate"),
                                iconCls : 'viz-icon-small-workflow',
                                handler : this.AssociateWorkflowAssemblyDefinition,
                                scope   : this
                            }],
                    menuPriority                       : [{
                                text    : $lang("msg_priority_deassociate"),
                                iconCls : 'viz-icon-small-association-delete',
                                handler : this.DeassociatePriority,
                                scope   : this
                            }, {
                                text    : $lang("msg_priority_update"),
                                iconCls : 'viz-icon-small-priority-update',
                                handler : this.UpdatePriority,
                                scope   : this
                            }],
                    menuWorkflowAssemblyDefinition     : [{
                                text    : $lang("msg_workflowassemblydefinition_deassociate"),
                                iconCls : 'viz-icon-small-association-delete',
                                handler : this.DeassociateWorkflowAssemblyDefinition,
                                scope   : this
                            }],
                    formConfig                         : {
                        common : {
                            width            : 500,
                            height           : 200,
                            xtype            : 'vizFormClassificationItem',
                            messageSaveStart : 'ClassificationItemChangeStart',
                            messageSaveEnd   : 'ClassificationItemChange'
                        },
                        create : {
                            title   : $lang("msg_classificationitem_add"),
                            iconCls : 'viz-icon-small-classificationitem'
                        },
                        update : {
                            title               : $lang("msg_classificationitem_update"),
                            iconCls             : 'viz-icon-small-classificationitem',
                            titleEntity         : 'Title',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyClassificationItem'
                                    }]
                        }
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                config.plugins = [new Viz.plugins.RapidSearch({
                            store          : {
                                xtype          : 'vizStoreRapidSearch',
                                serviceHandler : Viz.Services.CoreWCF.ClassificationItem_GetStore,
                                baseParams     : {
                                    filter : 'LocalId Title Code'
                                }
                            },
                            displayField   : 'Title',
                            pathTypeString : $lang("msg_classificationitem_path"),
                            valueField: 'LocalId',
                            searchName: 'Classifications'
                        })];
                Viz.tree.ClassificationItem.superclass.constructor.call(this, config);
                // register to ClassificationItemChangeStart and ClassificationItemChange.
                Viz.util.MessageBusMgr.subscribe("ClassificationItemChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.subscribe("ClassificationItemChange", this.ReloadNode, this);
                Viz.util.MessageBusMgr.subscribe('ClassificationItemPriorityChange', this.ReloadPriorityNode, this);
            },
            /**
             * @private OnDestroy.
             */
            onDestroy                              : function() {
                Viz.util.MessageBusMgr.unsubscribe("ClassificationItemChangeStart", this.Mask, this);
                Viz.util.MessageBusMgr.unsubscribe("ClassificationItemChange", this.ReloadNode, this);
                Viz.util.MessageBusMgr.unsubscribe('ClassificationItemPriorityChange', this.ReloadPriorityNode, this);
                Viz.tree.ClassificationItem.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * @private reload the priority node.
             */
            ReloadPriorityNode                     : function() {
                var node = this.getSelectionModel().getSelectedNode();
                if (node.attributes && node.attributes.entity && node.attributes.entity.IfcType == 'IfcPriority') {
                    node.parentNode.reload();
                }
            },
            /**
             * Handles the beforedorefresh event and gives a chance to change the doRefresh
             */
            onBeforeDoRefresh                      : function(tree, node) {
                if (node.attributes && node.attributes.entity) {
                    switch (node.attributes.entity.IfcType) {
                        case 'PsetDefinition' :
                        case 'PsetAttributeDefinition' :
                            return false;
                            break;
                    }
                }
                return true;
            },
            /**
             * Menu handler for creating a new ClassificationItem.
             */
            AddClassificationItem                  : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var entityParent = null;
                if (node) {
                    this.formConfig.common.messageSaveEndParams = node.id;
                    entityParent = {
                        KeyParent : node.attributes.entity.KeyClassificationItem
                    };
                }
                this.openFormCrud(this.formConfig, 'create', null, entityParent);
            },
            /**
             * Menu handler for updating an existing ClassificationItem.
             */
            UpdateClassificationItem               : function() {
                this.onUpdateItem(this.formConfig);
            },
            /**
             * Menu handler for deleting an existing ClassificationItem.
             */
            DeleteClassificationItem               : function() {
                this.onDeleteItem(Viz.Services.CoreWCF.ClassificationItem_Delete, 'ClassificationItemChangeStart', 'ClassificationItemChange');
            },
            /**
             * Menu handler for adding an association between a Pset and a CLassificationItem.
             */
            AssociatePset                          : function() {
                var parentNode = this.getSelectionModel().getSelectedNode();
                var entity = parentNode.attributes.entity;
                var usageName = this.getPsetUsageNameFromClassificationItem(entity);
                var _treeId = Ext.id();
                var _winId = Ext.id();
                parentNode.expand(false, false, function() {
                            psetNode = parentNode.findChild('text', 'Pset');
                            if (psetNode != null)
                                psetNode.expand(true, false);
                        });
                var win = new Ext.Window({
                            id        : _winId,
                            title     : $lang("msg_pset_associate"),
                            width     : 350,
                            height    : 300,
                            layout    : 'fit',
                            buttons   : [{
                                        text    : $lang("msg_save"),
                                        iconCls : 'viz-icon-small-save',
                                        handler : function() {
                                            var checkNodes = Ext.getCmp(_treeId).getChecked();
                                            if (checkNodes.length <= 0) {
                                                win.close();
                                                return;
                                            }
                                            var psetDefinitions = [];
                                            var psetAttributeDefinitions = [];
                                            Ext.each(checkNodes, function(node) {
                                                        switch (node.attributes.entity.IfcType) {
                                                            case 'PsetDefinition' :
                                                                psetDefinitions.push(node.attributes.entity);
                                                                break;
                                                            case 'PsetAttributeDefinition' :
                                                                psetAttributeDefinitions.push(node.attributes.entity);
                                                                break;
                                                        }
                                                    });
                                            win.getEl().mask();
                                            Viz.Services.CoreWCF.ClassificationItemPset_Add({
                                                        Key                      : parentNode.attributes.Key,
                                                        psetDefinitions          : psetDefinitions,
                                                        psetAttributeDefinitions : psetAttributeDefinitions,
                                                        success                  : function(value) {
                                                            this.ProcessAjaxSuccess(value, win);
                                                            parentNode.reload();
                                                        },
                                                        failure                  : function() {
                                                            this.ProcessAjaxFailure(win);
                                                        },
                                                        scope                    : this
                                                    });
                                        },
                                        scope   : this
                                    }],
                            listeners : {
                                show  : this.updateTreePset,
                                scope : this
                            },
                            items     : [{
                                        id              : _treeId,
                                        xtype           : 'vizTreePset',
                                        hideMenu        : true,
                                        displayCheckbox : true,
                                        border          : false,
                                        pathSeparator   : '/',
                                        stateful        : false,
                                        serviceParams   : {
                                            usageName : usageName
                                        }
                                    }]
                        });
                win.show();
            },
            updateTreePset                         : function(win) {
                var psetNode = null;
                var parentNode = this.getSelectionModel().getSelectedNode();
                this.checkSelectedPsets.defer(500, this, [win, parentNode]);
            },

            checkSelectedPsets                     : function(win, parentNode) {
                var treePset = win.items.items[0];
                var _root_header = String.format('/{0}/', treePset.root.text);
                parentNode.expand(false, false, function() {
                            psetNode = parentNode.findChild('text', 'Pset');
                            if (psetNode == null)
                                return;
                            psetNode.expand(true, false, function() {
                                        // hear wee are
                                        psetNode.cascade(function(node) {
                                                    var psetAttPath = node.getPath('text');
                                                    psetAttPath = psetAttPath.substring(psetAttPath.indexOf(psetNode.text) + 5, psetAttPath.length);
                                                    if (psetAttPath.indexOf('/') < 0)
                                                        return;
                                                    treePset.selectPath(_root_header + psetAttPath, 'text', function(bSuccess, oSelNode) {
                                                                if (!bSuccess)
                                                                    return;
                                                                oSelNode.checked = true;
                                                                oSelNode.ui.toggleCheck(true);
                                                            });
                                                }, this, null)

                                    }, this);

                        }, this);
            },

            /**
             * Menu handler for cleaning the pset values from a pset that is associated to ClassificationItem.
             */
            CleanPset                              : function() {

                var node = this.getSelectionModel().getSelectedNode();

                if (!node || !node.attributes.entity || !node.attributes.entity.IfcType)
                    return;

                Viz.Services.CoreWCF.ClassificationItemPset_CleanAllPsetValues({

                            key     : node.attributes.Key,
                            success : function(value) {
                                Ext.MessageBox.show({
                                            title   : $lang('msg_application_title'),
                                            msg     : $lang("msg_classificationitem_clean_success"),
                                            width   : 350,
                                            icon    : Ext.MessageBox.INFO,
                                            buttons : Ext.Msg.OK
                                        });
                                this.ProcessAjaxSuccess(value);
                                node.reload();
                            },
                            failure : function() {
                                this.ProcessAjaxFailure();
                            },
                            scope   : this
                        });
            },
            /**
             * Menu handler for adding an association between a ClassificationItem and a Priority.
             */
            AssociatePriority                      : function() {
                var parentNode = this.getSelectionModel().getSelectedNode();
                var entity = parentNode.attributes.entity;
                var _comboId = Ext.id();
                var win = new Ext.Window({
                            title     : $lang("msg_priority_associate"),
                            iconCls   : 'viz-icon-small-priority',
                            width     : 350,
                            height    : 150,
                            layout    : 'form',
                            bodyStyle : 'padding:5px;background-color:transparent',
                            buttons   : [{
                                        text    : $lang("msg_save"),
                                        iconCls : 'viz-icon-small-save',
                                        handler : function() {
                                            var KeyPriority = Ext.getCmp(_comboId).getValue();
                                            if (KeyPriority == null || KeyPriority == "")
                                                return;
                                            win.getEl().mask();
                                            Viz.Services.CoreWCF.ClassificationItemPriority_Add({
                                                        KeyClassificationChildren : parentNode.attributes.entity.KeyClassificationItem,
                                                        KeyClassificationParent   : parentNode.attributes.entity.Relation ? parentNode.attributes.entity.Relation.KeyParent : null,
                                                        KeyPriority               : KeyPriority,
                                                        success                   : function(value) {
                                                            this.ProcessAjaxSuccess(value, win);
                                                            parentNode.reload();
                                                        },
                                                        failure                   : function() {
                                                            this.ProcessAjaxFailure(win);
                                                        },
                                                        scope                     : this
                                                    });
                                        },
                                        scope   : this
                                    }],
                            items     : [{
                                        id         : _comboId,
                                        anchor     : Viz.Const.UI.Anchor,
                                        hideLabel  : true,
                                        xtype      : 'vizComboPriority',
                                        allowBlank : false
                                    }]
                        });
                win.show();
            },
            /**
             * Menu handler for adding an association between a ClassificationItem and a WorkflowAssemblyDefinition.
             */
            AssociateWorkflowAssemblyDefinition    : function() {
                var parentNode = this.getSelectionModel().getSelectedNode();
                var entity = parentNode.attributes.entity;
                var _comboId = Ext.id();
                var win = new Ext.Window({
                            title     : $lang("msg_workflowassemblydefinition_associate"),
                            iconCls   : 'viz-icon-small-workflow',
                            width     : 650,
                            height    : 150,
                            layout    : 'form',
                            bodyStyle : 'padding:5px;background-color:transparent',
                            buttons   : [{
                                        text    : $lang("msg_save"),
                                        iconCls : 'viz-icon-small-save',
                                        handler : function() {
                                            var combo = Ext.getCmp(_comboId);
                                            var wadName = combo.getValue();
                                            if (wadName == null)
                                                return;
                                            var record = combo.findRecord('AssemblyShortName', wadName);
                                            var wadQualifiedName = record.get('AssemblyQualifiedName');

                                            win.getEl().mask();
                                            Viz.Services.CoreWCF.ClassificationItemWorkflowAssemblyDefinition_Add({
                                                        KeyClassificationChildren                       : parentNode.attributes.entity.KeyClassificationItem,
                                                        KeyClassificationParent                         : parentNode.attributes.entity.Relation ? parentNode.attributes.entity.Relation.KeyParent : null,
                                                        WorkflowAssemblyDefinitionName                  : wadName,
                                                        WorkflowAssemblyDefinitionAssemblyQualifiedName : wadQualifiedName,
                                                        success                                         : function(value) {
                                                            this.ProcessAjaxSuccess(value, win);
                                                            parentNode.reload();
                                                        },
                                                        failure                                         : function() {
                                                            this.ProcessAjaxFailure(win);
                                                        },
                                                        scope                                           : this
                                                    });
                                        },
                                        scope   : this
                                    }],
                            items     : [{
                                        id         : _comboId,
                                        anchor     : Viz.Const.UI.Anchor,
                                        hideLabel  : true,
                                        allQuery   : entity.Category == 'work' ? 'ActionRequest' : 'Task',
                                        xtype      : 'vizComboWorkflowAssemblyDefinition',
                                        allowBlank : false,
                                        listeners  : {
                                            scope       : Ext.getCmp(_comboId),
                                            beforequery : function(queryEvent) {
                                                var combo = queryEvent.combo
                                                combo.store.clearFilter();
                                                combo.store.on('load', function() {
                                                            combo.store.filter([{
                                                                        fn    : function(record) {
                                                                            return record.get('EntityShortName') == this.allQuery;
                                                                        },
                                                                        scope : this
                                                                    }]);

                                                        }, combo);
                                                combo.store.load();
                                                return false;
                                            }
                                        }
                                    }]
                        });
                win.show();
            },
            /**
             * Gets the usageName of possible psets from a ClassificationItem item.
             * @param {Viz.BusinessEntity.ClassificationItem} item
             * @return {String}
             */
            getPsetUsageNameFromClassificationItem : function(item) {
                var category = item.Category;
                switch (category) {
                    case 'work' :
                        return 'IfcActionRequest';
                        break;
                    case 'space' :
                        return 'IfcSpace'
                        break;
                    case 'task' :
                        return 'IfcTask';
                        break;
                    case 'worktype' :
                        return 'IfcOccupant';
                        break;
                    default :
                        return null;
                }
            },
            /**
             * Gets the possible sub categories of ClassificationItem that could be link to a branch of ClassificationItem.
             * @param {Viz.BusinessEntity.ClassificationItem} item
             * @return {String}
             */
            getSubCategoryFromClassificationItem   : function(item) {
                var category = item.Category;
                switch (category) {
                    case 'work' :
                        return 'actionrequest';
                        break;
                    case 'actionrequest' :
                        if (item.Relation != null)
                            return 'task';
                        else
                            return null;
                        break;
                    default :
                        return null;
                }
            },

            /**
             * Menu handler for deleting an association between a Pset and a ClassificationItem and cleaning the psets.
             */
            DeassociatePsetAndClean                : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var msgDelete = "";
                if (!node || !node.attributes.entity || !node.attributes.entity.IfcType)
                    return;
                var psetDefinitions = [];
                var psetAttributeDefinitions = [];
                switch (node.attributes.entity.IfcType) {
                    case "PsetDefinition" :
                        msgDelete = $lang("msg_pset_delete_association_confirmation_single");
                        psetDefinitions.push(node.attributes.entity);
                        break;
                    case "PsetAttributeDefinition" :
                        msgDelete = $lang("msg_psetattribute_delete_association_confirmation_single");
                        psetAttributeDefinitions.push(node.attributes.entity);
                        break;
                }
                Ext.MessageBox.confirm($lang('msg_application_title'), String.format(msgDelete, node.text), function(button) {
                            if (button == "yes") {
                                var nodeClassificationItem = node.findParentBy(function(node) {
                                            if (node.attributes.entity)
                                                return node.attributes.entity.IfcType == 'IfcClassificationItem';
                                            return false;
                                        }, this);
                                Viz.Services.CoreWCF.ClassificationItemPset_Delete({
                                            Key                      : nodeClassificationItem.attributes.Key,
                                            psetDefinitions          : psetDefinitions,
                                            psetAttributeDefinitions : psetAttributeDefinitions,
                                            shouldClean              : true,
                                            success                  : function(value) {
                                                this.ProcessAjaxSuccess(value);
                                                nodeClassificationItem.reload();
                                            },
                                            failure                  : function() {
                                                this.ProcessAjaxFailure();
                                            },
                                            scope                    : this
                                        });
                            }
                        }, this);
            }

            ,

            /**
             * Menu handler for deleting an association between a Pset and a ClassificationItem.
             */
            DeassociatePset                        : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var msgDelete = "";
                if (!node || !node.attributes.entity || !node.attributes.entity.IfcType)
                    return;
                var psetDefinitions = [];
                var psetAttributeDefinitions = [];
                switch (node.attributes.entity.IfcType) {
                    case "PsetDefinition" :
                        msgDelete = $lang("msg_pset_delete_association_confirmation_single");
                        psetDefinitions.push(node.attributes.entity);
                        break;
                    case "PsetAttributeDefinition" :
                        msgDelete = $lang("msg_psetattribute_delete_association_confirmation_single");
                        psetAttributeDefinitions.push(node.attributes.entity);
                        break;
                }
                Ext.MessageBox.confirm($lang('msg_application_title'), String.format(msgDelete, node.text), function(button) {
                            if (button == "yes") {
                                var nodeClassificationItem = node.findParentBy(function(node) {
                                            if (node.attributes.entity)
                                                return node.attributes.entity.IfcType == 'IfcClassificationItem';
                                            return false;
                                        }, this);
                                Viz.Services.CoreWCF.ClassificationItemPset_Delete({
                                            Key                      : nodeClassificationItem.attributes.Key,
                                            psetDefinitions          : psetDefinitions,
                                            psetAttributeDefinitions : psetAttributeDefinitions,
                                            success                  : function(value) {
                                                this.ProcessAjaxSuccess(value);
                                                nodeClassificationItem.reload();
                                            },
                                            failure                  : function() {
                                                this.ProcessAjaxFailure();
                                            },
                                            scope                    : this
                                        });
                            }
                        }, this);
            },
            /**
             * Menu handler for updating a Priority.
             */
            UpdatePriority                         : function() {
                var node = this.getSelectionModel().getSelectedNode();
                var entity = node.attributes.entity;
                var formConfig = {
                    common : {
                        width            : 550,
                        height           : 200,
                        xtype            : 'vizFormPriority',
                        messageSaveStart : 'ClassificationItemPriorityChangeStart',
                        messageSaveEnd   : 'ClassificationItemPriorityChange'
                    },
                    update : {
                        title               : $lang("msg_priority_update"),
                        iconCls             : 'viz-icon-small-priority-update',
                        titleEntity         : 'Label',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyPriority'
                                }]
                    }
                };
                Viz.grid.GridPanel.prototype.openFormCrud(formConfig, "update", entity);
            },
            /**
             * Menu handler for deleting an association between a ClassificationItem and a Priority.
             */
            DeassociatePriority                    : function() {
                var msgDelete = $lang("msg_classificationitem_delete_association_confirmation_single");
                this.DeassociateItem(msgDelete, Viz.Services.CoreWCF.ClassificationItemPriority_Delete);

            },
            /**
             * Menu handler for deleting an association between a ClassificationItem and a WorkflowAssemblyDefinition.
             */
            DeassociateWorkflowAssemblyDefinition  : function() {
                var msgDelete = $lang("msg_classificationitem_delete_association_confirmation_single");
                this.DeassociateItem(msgDelete, Viz.Services.CoreWCF.ClassificationItemWorkflowAssemblyDefinition_Delete);
            },
            /**
             * Deassociates the related item from the ClassificationItem
             * @param {String} msgDelete The confirmation message for deleting a related item.
             * @param {Function} serviceHandlerDelete The service handler method.
             */
            DeassociateItem                        : function(msgDelete, serviceHandlerDelete) {
                var node = this.getSelectionModel().getSelectedNode();
                if (!node || !node.attributes.entity)
                    return;
                Ext.MessageBox.confirm($lang('msg_application_title'), String.format(msgDelete, node.text), function(button) {
                            if (button == "yes") {
                                var parentNode = node.findParentBy(function(node) {
                                            if (node.attributes.entity)
                                                return node.attributes.entity.IfcType == 'IfcClassificationItem';
                                            return false;
                                        }, this);
                                var parentEntity = parentNode.attributes.entity;
                                serviceHandlerDelete.call(this, {
                                            KeyClassificationChildren : parentEntity.KeyClassificationItem,
                                            KeyClassificationParent   : parentEntity.Relation ? parentEntity.Relation.KeyParent : null,
                                            success                   : function(value) {
                                                this.ProcessAjaxSuccess(value);
                                                parentNode.reload();
                                            },
                                            failure                   : function() {
                                                this.ProcessAjaxFailure();
                                            },
                                            scope                     : this
                                        });
                            }
                        }, this);
            },
            /**
             * Handles the beforecontextmenu event and gives a chance to change the menu
             * @param {Viz.tree.ClassificationItem} tree
             * @param {Ext.tree.TreeNode} node
             */
            onBeforeContextMenu                    : function(tree, node) {
                if (node.isRoot)
                    this.ctxMenu = null; // new Ext.menu.Menu(this.menuRoot);
                else {
                    var entity = node.attributes.entity;
                    switch (entity.IfcType) {
                        case "IfcClassificationItem" :
                            if (entity.Relation) {
                                this.ctxMenu = new Ext.menu.Menu({
                                            items : this.menuClassificationItemRelationship
                                        });
                            }
                            else {
                                switch (entity.Category) {
                                    case "actionrequest" :
                                        this.ctxMenu = new Ext.menu.Menu({
                                                    items : this.menuClassificationItemRequest
                                                });
                                        break;
                                    case "work" :
                                        this.ctxMenu = new Ext.menu.Menu({
                                                    items : this.menuClassificationItemWork
                                                });
                                        break;
                                    case 'actionrequest' :
                                        this.ctxMenu = new Ext.menu.Menu({
                                                    items : this.menuClassificationItem.filter(function(element, i, array) {
                                                                return !Ext.isArray(element['category']) || (Ext.isArray(element['category']) && (element['category'].indexOf(entity.Category) >= 0));
                                                            }, this)
                                                });
                                        break;
                                    case "task" :
                                        this.ctxMenu = new Ext.menu.Menu({
                                                    items : this.menuClassificationItemAction
                                                });
                                        break;

                                    case "mail" :
                                        break;

                                    default :
                                        this.ctxMenu = new Ext.menu.Menu({
                                                    items : this.menuClassificationItem
                                                });
                                }
                            }
                            break;
                        case "PsetDefinition" :
                            if (entity.KeyPropertySet != null)
                                this.ctxMenu = new Ext.menu.Menu({
                                            items : this.menuPset
                                        });
                            else
                                this.ctxMenu = null;
                            break;
                        case "PsetAttributeDefinition" :
                            this.ctxMenu = new Ext.menu.Menu({
                                        items : this.menuPset
                                    });
                            break;
                        case "IfcPriority" :
                            this.ctxMenu = new Ext.menu.Menu({
                                        items : this.menuPriority
                                    });
                            break;
                        case 'IfcWorkflowAssemblyDefinition' :
                            this.ctxMenu = new Ext.menu.Menu({
                                        items : this.menuWorkflowAssemblyDefinition
                                    });
                            break;

                        default :
                            this.ctxMenu = null;
                    }
                }
            },
            /**
             * Changes the KeyParent of the entitySource based on the new KeyParent.
             * @param {Object} entitySource
             * @param {string} keyParent
             */
            changeParentKey                        : function(entitySource, keyParent) {
                Ext.apply(entitySource, {
                            KeyParent : keyParent
                        });
            },
            /**
             * Called while dragging over.
             * @private
             * @param {Object} e dd event
             */
            onNodeDragOver                         : function(e) {
                if (e.dropNode.getDepth() == 0) {
                    e.cancel = true;
                    return false;
                }
                if (e.target.getDepth() == 0) {
                    e.cancel = true;
                    return false;
                }

                // we cant drag and drop the first levels of classifications
                if (e.dropNode.getDepth() <= 2) {
                    e.cancel = true;
                    return false;
                }
                var sourceType = e.dropNode.attributes.entity.IfcType;
                var targetType = e.target.attributes.entity.IfcType
                // we cannot move non IfcClassificationItem nodes (pset, psetattribute etc...)
                if (targetType != 'IfcClassificationItem' || sourceType != 'IfcClassificationItem') {
                    e.cancel = true;
                    return false;
                }
                // we cannot move relationship
                if (e.target.attributes.entity.Relation != null || e.dropNode.attributes.entity.Relation != null) {
                    e.cancel = true;
                    return false;
                }
                if (e.point != 'append') {
                    e.cancel = true;
                    return;
                }
                e.cancel = false;
            },
            /**
             * Generic handler after calling a WCF function in case of a success.
             * @param {bool} value
             * @param {Ext.Window} win
             */
            ProcessAjaxSuccess                     : function(value, win) {
                if (win) {
                    win.getEl().unmask();
                    win.close();
                }
                if (!value)
                    Ext.Msg.show({
                                width   : 400,
                                title   : $lang("error_ajax"),
                                msg     : $lang("error_save"),
                                buttons : Ext.Msg.OK,
                                icon    : Ext.MessageBox.ERROR
                            });
            },
            /**
             * Generic handler after calling a WCF function in case of an error.
             * @param {Ext.Window} win
             */
            ProcessAjaxFailure                     : function(win) {
                if (win) {
                    win.getEl().unmask();
                    win.close();
                }
                Ext.Msg.show({
                            width   : 400,
                            title   : $lang("error_ajax"),
                            msg     : $lang("error_save"),
                            buttons : Ext.Msg.OK,
                            icon    : Ext.MessageBox.ERROR
                        });
            }
        });
Ext.reg('vizTreeClassificationItem', Viz.tree.ClassificationItem);

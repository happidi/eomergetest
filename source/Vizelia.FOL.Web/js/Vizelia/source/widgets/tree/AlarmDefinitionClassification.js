﻿Ext.namespace('Viz.tree');
/**
 * Summary.
 * @class Viz.tree.AlarmDefinitionClassification
 * @extends Viz.tree.TreePanel
 */
Viz.tree.AlarmDefinitionClassification = Ext.extend(Viz.tree.TreePanel, {
            /**
             * @cfg {Object} root the root node
             */
            /**
             * @cfg {string} KeyChart the Key of the Chart in order to filter the list of classification based on the Chart FilterSpatial.
             */
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var rootEntity = Viz.Configuration.GetClassificationItemDefinitionEntityFromCategory("alarm");
                var defaultConfig = {
                    title          : $lang('msg_tree_classificationitem'),
                    iconCls        : 'viz-icon-small-classificationitem',
                    rootVisible    : false,
                    serviceHandler : Viz.Services.EnergyWCF.AlarmDefinitionClassification_GetTree,
                    root           : {
                        id      : rootEntity.KeyClassificationItem,
                        iconCls : 'viz-icon-small-alarm',
                        text    : rootEntity.Title,
                        entity  : rootEntity
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.tree.AlarmDefinitionClassification.superclass.constructor.call(this, config);
            },
            /**
             * @private OnDestroy.
             */
            onDestroy   : function() {
                Viz.tree.AlarmDefinitionClassification.superclass.onDestroy.apply(this, arguments);
            }

        });
Ext.reg('vizTreeAlarmDefinitionClassification', Viz.tree.AlarmDefinitionClassification);
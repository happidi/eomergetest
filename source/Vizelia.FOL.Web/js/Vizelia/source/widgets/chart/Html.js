﻿Ext.namespace('Viz.chart');

/**
 * @class Viz.chart.Html
 * @extends Ext.BoxComponent encapsulate an html component.
 */
Viz.chart.Html = Ext.extend(Ext.BoxComponent, {

            /**
             * @cfg {Viz.BusinessEntity.Chart} entity The chart entity.
             */
            entity        : null,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor   : function(config) {

                this._chart = Ext.id();

                config = config || {};
                var defaultConfig = {
                    style  : 'padding:2px;overflow:auto;',
                    layout : 'fit'
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.chart.Html.superclass.constructor.call(this, config);

                this.on({
                            afterrender : {
                                scope : this,
                                fn    : this.onChartRender
                            }
                        });

            },

            /**
             * Listener for the afterrender event
             * @method onDestroy
             * @private
             */
            onChartRender : function() {
                this.reloadChart.defer(200, this);
            },

            /**
             * Loads the Chart With Data and renders it
             * @method reloadChart
             */
            reloadChart   : function() {
                if (!this.reloading || this.reloading == false) {
                    if (!this.el.dom) // in case the portal window is closed before this function is fired
                        return;
                    this.reloading = true;

                    this.el.mask();
                    Viz.Services.EnergyWCF.Chart_GetStreamHtml({
                                Key     : this.entity.KeyChart,
                                width   : this.getBodyWidth(),
                                height  : this.getBodyHeight(),
                                success : function(data) {
                                    if (!this.el.dom) {
                                    // in case the portal window is closed before this function is fired
                                        this.reloading = false;
                                        return;
                                    }
                                    var htmlChartNode = this.el.select('.htmlchart');
                                    if (htmlChartNode.elements.length == 0)
                                        Ext.DomHelper.append(this.el, {
                                                    tag  : 'div',
                                                    cls  : 'htmlchart',
                                                    html : data
                                                });
                                    else
                                        Ext.DomHelper.overwrite(htmlChartNode.elements[0], {
                                                    tag  : 'div',
                                                    cls  : 'htmlchart',
                                                    html : data
                                                });
                                    this.render();
                                    this.reloading = false;
                                    this.el.unmask();

                                    this.el.on({
                                                mousedown : function(event, target) {
                                                    if (event.browserEvent.originalTarget != event.browserEvent.target)
                                                        event.stopPropagation();
                                                }
                                            });
                                },
                                failure : function() {
                                    this.reloading = false;
                                    if (!this.el.dom) {
                                    // in case the portal window is closed before this function is fired
                                        return;
                                    }
                                    this.el.unmask();
                                },
                                scope   : this
                            });
                }
            },
            /**
             * Return the body height.
             * @return {}
             */
            getBodyHeight : function() {
                return this.getHeight() - this.el.getPadding('tb');
            },
            /**
             * Return the body height.
             * @return {}
             */
            getBodyWidth  : function() {
                return this.getWidth() - this.el.getPadding('lr');
            }

        });

Ext.reg('vizChartHtml', Viz.chart.Html);

﻿Ext.namespace('Viz.chart');

/**
 * @class Viz.chart.DynamicDisplayImage
 * @extends Viz.viewer.Image.
 */
Viz.chart.DynamicDisplayImage = Ext.extend(Viz.viewer.Image, {

            /**
             * @cfg {Viz.BusinessEntity.DynamicDisplay} entity The dynamicdisplay entity.
             */
            entity      : null,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    style : 'padding:0px;'
                };
                var forcedConfig = {
                    stretchImage     : true,
                    serviceInterface : 'Energy.svc',
                    serviceStream    : 'DynamicDisplay_GetStreamImage',
                    entityKey        : 'KeyDynamicDisplay',
                    reloadEventName  : 'DynamicDisplayChange',
                    serviceParams    : {
                        displayBogusChart : true
                    }
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.chart.DynamicDisplayImage.superclass.constructor.call(this, config);
            }
        });

Ext.reg('vizChartDynamicDisplayImage', Viz.chart.DynamicDisplayImage);

﻿Ext.namespace('Viz.chart');

/**
 * @class Viz.chart.DynamicDisplayImage
 * @extends Viz.viewer.Image
 */
Viz.chart.PortalTabImage = Ext.extend(Viz.viewer.Image, {

            /**
             * @cfg {Viz.BusinessEntity.PortalTab} entity The portaltab entity.
             */
            entity      : null,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {

                config = config || {};
                var defaultConfig = {
                    style : 'padding:0px;'
                };
                var forcedConfig = {
                    stretchImage     : true,
                    serviceInterface : 'Energy.svc',
                    serviceStream    : 'PortalTab_GetStreamImage',
                    entityKey        : 'KeyPortalTab',
                    reloadEventName  : 'PortalTabChange',
                    maskDeferDelay   : 41000
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.chart.PortalTabImage.superclass.constructor.call(this, config);

            }

        });

Ext.reg('vizChartPortalTabImage', Viz.chart.PortalTabImage);

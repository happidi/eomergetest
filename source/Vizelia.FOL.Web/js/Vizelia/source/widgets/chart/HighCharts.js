﻿Ext.namespace('Viz.chart');

/**
 * @class Viz.chart.HighCharts
 * @extends Ext.BoxComponent encapsulate an highchart component.
 */
Viz.chart.HighCharts = Ext.extend(Ext.BoxComponent, {

            /**
             * @cfg {Viz.BusinessEntity.Chart} entity The chart entity.
             */
            entity        : null,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor   : function(config) {

                this._chart = Ext.id();

                config = config || {};
                var defaultConfig = {
                    style  : 'padding:2px;',
                    layout : 'fit'
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.chart.HighCharts.superclass.constructor.call(this, config);
                this.setOptions();

                this.on({
                            afterrender : {
                                scope : this,
                                fn    : this.onChartRender
                            },

                            resize      : {
                                scope : this,
                                fn    : this.onChartResize
                            }
                        });

            },

            /**
             * Listener for the afterrender event
             * @method onDestroy
             * @private
             */
            onChartRender : function() {
                this.reloadChart.defer(200, this);
            },

            /**
             * Listener for the resize event.
             * @method onChartResize
             * @private
             */
            onChartResize : function() {
                if (this.highChart && this.highChart.setSize) {
                    this.highChart.setSize(this.getBodyWidth(), this.getBodyHeight(), false);
                }
            },

            /**
             * Loads the Chart With Data and renders it
             * @method reloadChart
             */
            reloadChart   : function() {
                if (!this.reloading || this.reloading == false) {
                    this.reloading = true;

                    this.el.mask();
                    Viz.Services.EnergyWCF.Chart_GetStreamJavascript({
                                Key     : this.entity.KeyChart,
                                width   : this.getBodyWidth(),
                                height  : this.getBodyHeight(),
                                success : function(data) {

                                    var json = Ext.decode(data);

                                    json.credits = {
                                        enabled : false
                                    };
                                    json.chart.height = this.getBodyHeight();
                                    json.chart.width = this.getBodyWidth();
                                    json.chart.spacingBottom = Math.max(json.chart.spacingBottom, 10);
                                    delete json.chart.renderTo;

                                    Ext.each(json.series, function(serie) {
                                                var toDelete = new Array();
                                                Ext.each(serie.data, function(point) {
                                                            if (!Ext.isNumber(point.y)) {
                                                                point.y = null;
                                                                // toDelete.push(point);
                                                            }
                                                        }, this);
                                                Ext.each(toDelete, function(point) {
                                                            serie.data.remove(point);
                                                        }, this);

                                            }, this);
                                    json.chart.renderTo = this.getEl().id;

                                    this.highChart = new JSCharting.Chart(json);

                                    // Force resize so the HTML highChart will render
                                    this.onChartResize();

                                    this.reloading = false;
                                    this.el.unmask();
                                },
                                failure : function() {
                                    this.reloading = false;
                                    this.el.unmask();
                                },
                                scope   : this
                            });
                }
            },

            /**
             * Set default DotNetCharting language.
             */
            setOptions    : function() {
                JSCharting.setOptions({
                            lang : {
                                downloadPNG       : 'PNG Image',
                                downloadJPEG      : 'JPG Image',
                                downloadPDF       : 'PDF Document',
                                downloadSVG       : 'SVG Vector',
                                exportButtonTitle : 'Export to raster or vector image',
                                printButtonTitle  : 'Print the chart',
                                loading           : 'Loading...',
                                resetZoom         : 'Zoom Out',
                                resetZoomTitle    : 'Reset Zoom',
                                decimalPoint      : '.',
                                thousandsSep      : ',',
                                weekdays          : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                                months            : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

                            }
                        });
            },

            /**
             * Return the body height.
             * @return {}
             */
            getBodyHeight : function() {
                return this.getHeight() - this.el.getPadding('tb');
            },
            /**
             * Return the body height.
             * @return {}
             */
            getBodyWidth  : function() {
                return this.getWidth() - this.el.getPadding('lr');
            }

        });

Ext.reg('vizChartHighCharts', Viz.chart.HighCharts);

﻿Ext.namespace('Viz.chart');
// Removed until we decide the best charting library
                    
/**
 * @class Viz.chart.Sencha
 * @extends Ext.BoxComponent encapsulate an Ext4 chart component.
 */


//Viz.chart.Sencha = Ext.extend(Ext.BoxComponent, {

//    /**
//    * @cfg {Viz.BusinessEntity.Chart} entity The chart entity.
//    */
//    entity: null,

//    /**
//    * Ctor.
//    * @param {Object} config The configuration options
//    */
//    constructor: function (config) {

//        this._chart = Ext.id();
//        config = config || {};
//        var defaultConfig = {
//            style: 'padding:5px;'
//        };
//        var forcedConfig = {};
//        Ext.applyIf(config, defaultConfig);
//        Ext.apply(config, forcedConfig);

//        Viz.chart.Sencha.superclass.constructor.call(this, config);

//        this.on({
//            afterrender: {
//                scope: this,
//                fn: this.onChartRender
//            },

//            resize: {
//                scope: this,
//                fn: this.onChartResize
//            }
//        });

//    },

//    /**
//    * Listener for the afterrender event
//    * @method onDestroy
//    * @private
//    */
//    onChartRender: function () {
//        this.reloadChart();
//    },

//    /**
//    * Listener for the resize event.
//    * @method onChartResize
//    * @private
//    */
//    onChartResize: function () {
//        var chart = Ext4.getCmp(this._chart);
//        if (chart) {
//            chart.setWidth(this.getWidth());
//            chart.setHeight(this.getHeight());
//        }
//    },

//    /**
//    * Loads the Chart With Data and renders it
//    * @method reloadChart
//    */
//    reloadChart: function () {
//        if (!this.reloading || this.reloading == false) {
//            this.reloading = true;

//            Viz.Services.EnergyWCF.Chart_GetItemWithData({
//                Key: this.entity.KeyChart,
//                success: function (data) {
//                    if (this.getEl().dom.children.length > 0) {
//                        var count = this.getEl().dom.children.length;
//                        for (var i = 0; i < count; i++) {
//                            this.getEl().dom.removeChild(this.getEl().dom.children[0]);
//                        }
//                    }

//                    Ext4.createWidget('chart', Ext.apply(this.getSenchaCfg(this.getWidth(), this.getHeight(), data), {
//                        renderTo: this.getEl().dom
//                    }));
//                    this.reloading = false;
//                },
//                failure: function () {
//                    this.reloading = false;
//                },
//                scope: this
//            });
//        }
//    },

//    /**
//    * Builds the sencha chart config
//    * @method getSenchaCfg
//    * @param {int} width
//    * @param {int} height
//    * @param {Viz.BusinessEntity.Chart} entityWithData
//    * @private
//    */
//    getSenchaCfg: function (width, height, entityWithData) {
//        var seriesName = [];
//        var seriesHash = [];
//        var series = [];
//        var hashdata = [];
//        var themeColors = [];
//        var themeSeriesThemes = [];
//        Ext.each(entityWithData.Series, function (serie) {
//            seriesName.push(serie.Name);
//            var type = this.getSenchaSerieType(serie.Type);
//            var seriesCfg = seriesHash[type];
//            if (seriesCfg) {
//                seriesCfg.yField.push(serie.Name);
//                seriesCfg.label.field.push(serie.Name);
//            }
//            else {
//                seriesCfg = this.getSenchaSerieCfg(serie.Name, type, entityWithData.DisplayStackSeries, false, entityWithData.DisplayValues, 1 - entityWithData.DisplayTransparency / 100);
//                seriesHash[type] = seriesCfg;
//                series.push(seriesCfg);
//            }
//            if (serie.ColorR && serie.ColorG && serie.ColorB) {
//                var fill = '#' + new Viz.RGBColor(String.format('rgb({0},{1},{2})', serie.ColorR, serie.ColorG, serie.ColorB)).toHex();
//                themeColors.push(fill);
//                themeSeriesThemes.push({
//                    fill: fill
//                });
//            }

//            Ext.each(serie.Points, function (point) {
//                var row = hashdata[point.Name];
//                if (!row) {
//                    row = {
//                        xdatetime: point.XDateTime
//                    };
//                }
//                row[serie.Name] = point.YValue;
//                hashdata[point.Name] = row;
//            }, this);
//        }, this);
//        this.setSenchaTheme(themeColors, themeColors);

//        var data = [];
//        for (var i in hashdata) {
//            if (Ext.isDate(hashdata[i].xdatetime))
//                data.push(hashdata[i]);
//        }

//        return {
//            id: this._chart,
//            xtype: 'chart',
//            animate: true,
//            shadow: true,
//            width: width,
//            height: height,
//            theme: 'Custom',
//            store: Ext4.create('Ext.data.JsonStore', {
//                fields: ['xdatetime'].concat(seriesName),
//                data: data
//            }),
//            legend: this.getSenchaLegendCfg(width, entityWithData),
//            gradients: this.getSenchaGradientCfg(),
//            axes: this.getSenchaAxisCfg(seriesName, entityWithData),
//            series: series
//        }
//    },

//    getSenchaSerieType: function (serieType) {
//        switch (serieType) {
//            case Vizelia.FOL.BusinessEntities.DataSerieType.Line:
//                return 'line';
//            default:
//                return 'column';
//        }
//    },

//    getSenchaSerieCfg: function (name, type, stacked, useExtraAxis, displayValues, opacity) {
//        return {
//            type: type,
//            axis: useExtraAxis ? 'right' : 'left',
//            gutter: 10,
//            highlight: {
//                lineWidth: 1,
//                stroke: '#ff0000',
//                fill: '#fe0000'
//            },
//            stacked: stacked,
//            tips: {
//                trackMouse: true,
//                width: 120,
//                height: 20,
//                renderer: function (storeItem, item) {
//                    this.setTitle(storeItem.get('xdatetime').format(Date.patterns.FrenchShortDate) + ': ' + item.value[1]);
//                }
//            },
//            label: {
//                display: displayValues ? 'insideEnd' : 'none',
//                field: [name],
//                orientation: 'horizontal',
//                color: '#333',
//                'text-anchor': 'middle'
//            },
//            renderer: function (sprite, storeItem, barAttr, i, store) {
//                barAttr.stroke = '#000000';
//                barAttr.lineWidth = 0.1;
//                return barAttr;
//            },
//            style: {
//                opacity: opacity
//            },
//            xField: 'xdatetime',
//            yField: [name]
//        }
//    },

//    getSenchaLegendCfg: function (width, entityWithData) {
//        return {
//            position: 'top',
//            labelFont: String.format('{0}px {1}', entityWithData.DisplayLegendFontSize, entityWithData.DisplayFontFamily),
//            visible: entityWithData.DisplayLegendVisible,
//            // boxFill : 'url(#legendGradient)',
//            boxStroke: '#000000',
//            boxStrokeWidth: 0.01,
//            width: width
//        };
//    },

//    getSenchaGradientCfg: function () {
//        return [{
//            id: 'axisGradient',
//            angle: 90,
//            stops: {
//                0: {
//                    color: '#ffffff'
//                },
//                100: {
//                    color: '#abc7ec'
//                }
//            }
//        }, {
//            id: 'legendGradient',
//            angle: 90,
//            stops: {
//                0: {
//                    color: '#dddddd'
//                },
//                100: {
//                    color: '#aaaaaa'
//                }
//            }
//        }];
//    },

//    setSenchaTheme: function (themeColors, themeSeriesThemes) {
//        Ext4.chart.theme.Custom = Ext.extend(Ext4.chart.theme.Base, {
//            constructor: function () {
//                Ext4.chart.theme.Custom.superclass.constructor.call(this, {
//                    colors: themeColors,
//                    seriesThemes: themeSeriesThemes
//                });
//            }
//        });
//    },


//    getSenchaAxisCfg: function (seriesName, entityWithData) {
//        var axes = [{
//            type: 'Numeric',
//            position: 'left',
//            fields: seriesName,
//            minimum: 0,
//            label: {
//                font: String.format('{0}px {1}', entityWithData.DisplayFontSize, entityWithData.DisplayFontFamily)
//                // renderer : Ext4.util.Format.numberRenderer('0,0')
//            },
//            grid: {
//                even: {
//                    fill: 'url(#axisGradient)',
//                    stroke: '#ccc',
//                    'stroke-width': 1
//                },
//                odd: {
//                    fill: '#eeeeee',
//                    stroke: '#ccc',
//                    'stroke-width': 1
//                }
//            }
//        }, {
//            type: 'Time',
//            position: 'bottom',
//            fields: ['xdatetime'],
//            dateFormat: Date.patterns.FrenchShortDate,
//            label: {
//                font: String.format('{0}px {1}', entityWithData.DisplayFontSize, entityWithData.DisplayFontFamily)
//            },
//            grid: true
//        }];

//        if (entityWithData.Axis && entityWithData.Axis.length > 0) {
//            Ext.each(entityWithData.Axis, function (hash) {

//            }, this);
//        }

//        return axes;
//    }

//});

//Ext.reg('vizChartSencha', Viz.chart.Sencha);

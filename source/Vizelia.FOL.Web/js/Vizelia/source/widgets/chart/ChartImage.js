﻿Ext.namespace('Viz.chart');

/**
 * @class Viz.chart.ChartImage
 * @extends Viz.viewer.Image.
 */
Viz.chart.ChartImage = Ext.extend(Viz.viewer.Image, {

            /**
             * @cfg {Viz.BusinessEntity.Chart} entity The chart entity.
             */
            entity      : null,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {

                config = config || {};
                var defaultConfig = {
                    style            : 'padding:0px;background-color:transparent',
                    stretchImage     : false,
                    serviceStream    : "Chart_GetStreamImageFromCache",
                    serviceInterface : 'public.svc',
                    serviceHandler   : Viz.Services.EnergyWCF.Chart_BuildImage,
                    serviceParams    : {
                        applicationName : config.entity.Application ? config.entity.Application : Viz.App.Global.User.ApplicationName,
                        Key             : config.entity.KeyChart
                    }
                };
                var forcedConfig = {

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.chart.ChartImage.superclass.constructor.call(this, config);
            },

            reloadChart : function() {
                this.reloadImage();
            }

        });

Ext.reg('vizChartImage', Viz.chart.ChartImage);

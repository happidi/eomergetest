﻿
/**
 * @class Viz.chart.CustomGrid
 * @extends Ext.Panel encapsulate an custom grid component.
 */
Viz.chart.CustomGrid = Ext.extend(Ext.Panel, {

            /**
             * @cfg {Viz.BusinessEntity.Chart} entity The chart entity.
             */
            entity                  : null,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor             : function(config) {
                this._grid = Ext.id();
                this._readOnly = true;
                this._saveButtonId = Ext.id();
                this._webRequest = null;

                config = config || {};
                var defaultConfig = {
                    style  : 'padding:0px;',
                    layout : 'fit',
                    border : false
                };

                var forcedConfig = {
                    items : []
                };

                if ($authorized('@@@@ Chart - Full Control')) {
                    Ext.apply(forcedConfig, {
                                tbar : [{
                                            text    : $lang('msg_chartcustomgrid_save'),
                                            iconCls : 'viz-icon-small-save',
                                            handler : this.onSave,
                                            id      : this._saveButtonId,
                                            hidden  : true,
                                            scope   : this
                                        }, {
                                            text         : $lang('msg_chartcustomgrid_toggleeditmode'),
                                            iconCls      : 'viz-icon-small-update',
                                            enableToggle : true,
                                            pressed      : false,
                                            handler      : this.onToggleEditMode,
                                            scope        : this
                                        }]
                            });
                }

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.chart.CustomGrid.superclass.constructor.call(this, config);

                this.on({
                            afterrender : {
                                scope : this,
                                fn    : this.onCustomGridAfterRender
                            }
                        });
            },
            /**
             * Destroy
             * @private
             */
            onDestroy               : function() {
                Viz.util.Chart.cancelRequest(this._webRequest);
                Viz.chart.CustomGrid.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the after render event.
             */
            onCustomGridAfterRender : function() {
                this.reloadChart.defer(500, this);
            },

            /**
             * Loads the Chart With Data and renders it
             * @method reloadChart
             */
            reloadChart             : function() {
                if (!this.reloading || this.reloading == false) {
                    this.reloading = true;
                    try {
                        this.el.mask();
                    }
                    catch (e) {
                        ;
                    }
                    delete this.ctxMenu;

                    this._webRequest = Viz.Services.EnergyWCF.ChartCustomGridCell_GetListByKeyChart({
                                KeyChart       : this.entity.KeyChart,
                                populateValues : this._readOnly,
                                success        : function(cells) {

                                    this.removeAll();
                                    this.add(this.getCustomDataGrid(cells, this._readOnly));
                                    this.doLayout();

                                    this.reloading = false;
                                    this.el.unmask();
                                },
                                failure        : function() {
                                    this.reloading = false;
                                    this.el.unmask();
                                },
                                scope          : this
                            });
                }
            },

            /**
             * Returns the Custom Grid.
             * @param {Viz.BusinessEntity.Chart} entity
             * @return {}
             */
            getCustomDataGrid       : function(cells, readOnly) {
                var rowCount = this.entity.CustomGridRowCount;
                var colCount = this.entity.CustomGridColumnCount;
                this._readOnly = readOnly;

                var customdata = [];
                for (var i = 0; i < rowCount; i++) {
                    var row = [];
                    for (var j = 0; j < colCount; j++) {
                        row.push('')
                        row.push('');
                    }
                    customdata.push(row);
                }

                for (var i = 0; i < cells.length; i++) {
                    var cell = cells[i];
                    if (cell.RowIndex < rowCount && cell.ColumnIndex < colCount) {
                        customdata[cell.RowIndex][cell.ColumnIndex * 2] = this._readOnly ? cell.Value : cell.Tag;
                        customdata[cell.RowIndex][cell.ColumnIndex * 2 + 1] = cell.KeyChartCustomGridCell;
                    }
                }

                var cols = [];
                var fields = []
                for (var i = 0; i < colCount; i++) {
                    cols.push({
                                id        : 'col' + i,
                                dataIndex : 'col' + i,
                                sortable  : false,
                                editor    : new Ext.form.TextField({
                                            allowBlank : true
                                        })
                            });
                    fields.push({
                                name : 'col' + i,
                                type : 'string'
                            })
                    fields.push({
                                name : 'keycol' + i,
                                type : 'string'
                            });
                }

                var cm = new Ext.grid.ColumnModel({
                            defaults : {
                                sortable : false
                            },
                            columns  : cols
                        });

                var store = new Ext.data.ArrayStore({
                            fields : fields
                        });
                if (readOnly) {
                    var g = Ext.grid.GridPanel;
                    store.loadData(customdata);
                }
                else {
                    var g = Ext.grid.EditorGridPanel;
                    store.loadData(customdata);
                }
                var grid = new g({
                            id               : this._grid,
                            stateId          : 'ChartCustomGrid' + this.entity.KeyChart,
                            enableColumnMove : false,
                            enableHdMenu     : false,
                            columnLines      : this._readOnly ? !this.entity.CustomGridHideLines : true,
                            stripeRows       : this._readOnly ? !this.entity.CustomGridHideLines : true,
                            border           : false,
                            hideHeaders      : this._readOnly,
                            store            : store,
                            cm               : cm,
                            clicksToEdit     : 2,
                            listeners        : {
                                cellcontextmenu : this.onCellContextMenu,
                                scope           : this
                            },
                            viewConfig       : {
                                forceFit    : true,
                                showRowLine : this._readOnly ? !this.entity.CustomGridHideLines : true,
                                getRowClass : function(record, rowIndex, rp, ds) {
                                    if (this.showRowLine) {
                                        return 'x-grid3-row';
                                    }
                                    return 'x-grid3-row-noborder';
                                }
                            }
                        })
                return grid;
            },

            getGrid                 : function() {
                return Ext.getCmp(this._grid);
            },

            /**
             * Event handler for the grid cell context menu
             * @param {} grid
             * @param {} rowIndex
             * @param {} colIndex
             * @param {} event
             */
            onCellContextMenu       : function(grid, rowIndex, colIndex, event) {
                event.stopEvent();
                var xy = event.getXY();
                if (!this._readOnly) {
                    var grid = this.getGrid();
                    var rec = grid.getStore().getAt(rowIndex);
                    var colName = grid.getColumnModel().getDataIndex(colIndex);

                    this.rec = rec;
                    this.colName = colName;

                    if (!this.ctxMenu) {
                        var ctxMenuWait;
                        this.intelliSenseUtil = new Viz.util.IntelliSense({
                                    serviceHandler   : Viz.Services.EnergyWCF.ChartModel_GetMenuTree,
                                    delimiterChar    : ',',
                                    serviceParams    : {
                                        KeyChart : this.entity.KeyChart
                                    },
                                    menuHandler      : this.clickHandler,
                                    menuHandlerScope : this,
                                    listeners        : {
                                        success : function(o, nodes) {
                                            this.nodes = nodes;
                                            if (this.fireEvent("success", this, nodes) != false) {
                                                ctxMenuWait.destroy();
                                                this.ctxMenu = this.intelliSenseUtil.createMenu(this.nodes);
                                                var personlizedMenuItem = this.getPersonalizedMenuItem();
                                                this.ctxMenu.addMenuItem(personlizedMenuItem);
                                                this.ctxMenu.items.items[this.ctxMenu.items.items.length - 1].hideOnClick = true;
                                                this.ctxMenu.showAt(xy);
                                            }
                                        },
                                        failure : function() {
                                            ctxMenuWait.destroy();
                                        },
                                        scope   : this
                                    }
                                });

                        ctxMenuWait = this.intelliSenseUtil.buildWaitContextMenu();
                        ctxMenuWait.showAt(xy);
                        this.intelliSenseUtil.getMenu();

                    }
                    else
                        this.ctxMenu.showAt(xy);
                }
            },

            /**
             * menu handler
             * @private
             */
            clickHandler            : function(path) {
                this.rec.set(this.colName, path);
            },

            /**
             * Builds the menu config.
             * @private
             */
            getPersonalizedMenuItem : function() {

                return {
                    text        : $lang('msg_chartcustomgridcell_htmleditor'),
                    iconCls     : 'viz-icon-small-html',
                    hideOnClick : true,
                    scope       : this,
                    handler     : function() {
                        var _id = Ext.id();
                        var win = new Ext.Window({
                                    title     : $lang('msg_chartcustomgridcell_htmleditor'),
                                    iconCls   : 'viz-icon-small-html',
                                    width     : 580,
                                    height    : 250,
                                    modal     : true,
                                    bodyStyle : 'padding:5px;',
                                    layout    : 'fit',
                                    items     : [{
                                                xtype     : 'htmleditor',
                                                id        : _id,
                                                plugins   : [new Ext.ux.form.HtmlEditor.DynamicDisplayImage(), new Viz.plugins.IntelliSense({
                                                                    delimiterChar  : ',',
                                                                    serviceHandler : Viz.Services.EnergyWCF.ChartModel_GetMenuTree,
                                                                    serviceParams  : {
                                                                        KeyChart : this.entity.KeyChart
                                                                    }
                                                                })],
                                                border    : false,
                                                hideLabel : true,
                                                readOnly  : false,
                                                value     : this.rec.get(this.colName),
                                                listeners : {
                                                    render : function(cmp) {
                                                        cmp.fireEvent('activate', cmp);
                                                    },
                                                    scope  : this
                                                }
                                            }],
                                    buttons   : [{
                                                text    : $lang('msg_save'),
                                                iconCls : 'viz-icon-small-save',
                                                handler : function() {
                                                    this.rec.set(this.colName, Ext.getCmp(_id).getValue());
                                                    win.close();
                                                },
                                                scope   : this
                                            }]
                                });

                        win.show();
                        // this.hideMenu();
                    }
                };
            },

            /**
             * Handler for the Save button.
             */
            onSave                  : function() {
                if (this._readOnly == false) {
                    var grid = this.getGrid();
                    var store = grid.getStore();

                    var cells = [];

                    var rowCount = grid.getStore().getCount();
                    var colCount = grid.getColumnModel().columns.length;
                    for (var rowIndex = 0; rowIndex < rowCount; rowIndex++) {
                        var rec = grid.getStore().getAt(rowIndex);
                        if (rec.dirty) {
                            for (var colIndex = 0; colIndex < colCount; colIndex++) {
                                var colName = grid.getColumnModel().getDataIndex(colIndex);
                                if (Ext.isDefined(rec.modified[colName])) {
                                    var val = rec.get(colName);
                                    var key = rec.get('key' + colName);
                                    if (!Ext.isEmpty(val, false) || !Ext.isEmpty(key, false)) {
                                        cells.push({
                                                    KeyChartCustomGridCell : key,
                                                    KeyChart               : this.entity.KeyChart,
                                                    RowIndex               : rowIndex,
                                                    ColumnIndex            : colIndex,
                                                    Tag                    : val || ""
                                                });
                                    }
                                }
                            }
                        }
                    }

                    store.commitChanges();

                    if (cells.length > 0) {
                    	this.el.mask();
                        Viz.Services.EnergyWCF.ChartCustomGridCell_SaveList({
                                    cells   : cells,
                                    success : function() {
                                    	this.el.unmask();
                                        this.reloadChart();
                                    },
                                    scope   : this
                                })
                    }
                }

            },

            /**
             * Handler for the Toggle Edit Mode button.
             */
            onToggleEditMode        : function(bt) {
                var grid = this.getGrid();
                if (this._readOnly == false) {
                    var isDirty = false;
                    for (var i = 0; i < grid.getStore().getCount(); i++) {
                        if (grid.getStore().getAt(i).dirty) {
                            isDirty = true;
                            break;
                        }
                    }

                    if (isDirty) {
                        Ext.MessageBox.confirm($lang('msg_save'), $lang('msg_saveconfirm'), function(buttonid) {
                                    if (buttonid == 'yes') {
                                        this.onSave();
                                        bt.toggle();
                                        return;
                                    }
                                    else {
                                        this._readOnly = !this._readOnly;
                                        this.reloadChart();
                                    }
                                }, this);
                    }
                    else {
                        this._readOnly = !this._readOnly;
                        this.reloadChart();
                    }
                }
                else {
                    this._readOnly = !this._readOnly;
                    this.reloadChart();
                }
                Ext.getCmp(this._saveButtonId).setVisible(!this._readOnly);
            }

        });

Ext.reg('vizChartCustomGrid', Viz.chart.CustomGrid);
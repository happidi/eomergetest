﻿Ext.namespace('Viz.chart');

/**
 * @class Viz.chart.Viewer
 * @extends Ext.Panel
 * @xtype vizChartViewer
 * <p>
 * Summary
 * </p>
 */
Viz.chart.Viewer = Ext.extend(Viz.portal.Portlet, {

            /**
             * @cfg {Viz.BusinessEntity.Chart} entity The chart entity.
             */
            entity                                          : null,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                                     : function(config) {
                config = config || {};
                if (!Ext.isDefined(config.readonly))
                    config.readonly = false;
                var defaultConfig = {};
                this._chartCmpt = Ext.id();
                this._updatingByDrop = false;

                this._startDateMenuItemId = Ext.id();
                this._endDateMenuItemId = Ext.id();

                var forcedConfig = {
                    header        : !config.useWindow,
                    iconCls       : this.getLockFilterSpatialCss(config.entity.LockFilterSpatial, false, config.entity.IsFavorite),
                    draggable     : $authorized('@@@@ Portlet - Full Control') && !config.readonly && !config.useWindow,
                    items         : this.getChartConfig(config.entity),
                    closable      : false,
                    layout        : 'fit',
                    updateHandler : Viz.grid.Chart.update,
                    bodyStyle     : 'padding:0px;background-color:transparent;'
                };
                if ($authorized('@@@@ Chart - Sliders') && !config.readonly) {
                    Ext.apply(forcedConfig, {
                                plugins : [this.getPluginsConfig(config)]
                            });
                }

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                this.addEvents(
                        /**
                         * @event afterentityupdate Fires after the chart entity has been updated.
                         * @param {Viz.chart.Viewer} this
                         * @param {Viz.BusinessEntity.Chart} the chart entity
                         */
                        "afterentityupdate");

                Viz.chart.Viewer.superclass.constructor.call(this, config);
                this.on({
                            afterentityupdate : {
                                scope : this,
                                fn    : this.onUpdateEntity
                            },
                            afterrender       : {
                                scope : this,
                                fn    : this.onAfterRender
                            }
                        });
                Viz.util.MessageBusMgr.subscribe("ChartAxisChange", this.onChartAxisChange, this);
                Viz.util.MessageBusMgr.subscribe("ChartChange", this.onChartChange, this);
                Viz.util.MessageBusMgr.subscribe("DataSerieChange", this.onDataSerieChange, this);
                Viz.util.MessageBusMgr.subscribe("DynamicDisplayChange", this.onDynamicDisplayChange, this);
                Viz.util.MessageBusMgr.subscribe("CalculatedSerieChange", this.onCalculatedSerieChange, this);
                Viz.util.MessageBusMgr.subscribe("StatisticalSerieChange", this.onStatisticalSerieChange, this);
                Viz.util.MessageBusMgr.subscribe("ChartHistoricalAnalysisChange", this.onChartHistoricalAnalysisChange, this);
                Viz.util.MessageBusMgr.subscribe("ChartAlgorithmChange", this.onChartAlgorithmChange, this);
                Viz.util.MessageBusMgr.subscribe("ChartDrillDownChange", this.onChartDrillDownChangeChange, this);
                Viz.util.MessageBusMgr.subscribe("ChartCalendarViewSelectionChange", this.onChartCalendarViewSelectionChange, this);
                Viz.util.MessageBusMgr.subscribe("ChartPsetAttributeHistoricalChange", this.onChartPsetAttributeHistoricalChange, this);
                Viz.util.MessageBusMgr.subscribe("DataSeriePsetRatioChange", this.onDataSeriePsetRatioChange, this);
                Viz.util.MessageBusMgr.subscribe("FilterSpatialPsetChange", this.onFilterSpatialPsetChange, this);
            },
            /**
             * AfterRender Handler to register the dropTarget for the Portlet Tree.
             */
            onAfterRender: function () {
                // Which drop container will handle the drop event is detrmined by the first container in a sorted array by zIndex of containers with the same ddGroup.
                // We want the chart to handle the drop event over the portlet so we assign it a zIndex to make it first in the array. zIndex takes effect only in positioned elements
                this.el.dom.style.zIndex = 1;
                this.el.dom.style.position = 'relative';

                Viz.chart.Viewer.superclass.onAfterRender.apply(this, arguments);
                if (this.entity.IsKPI == true || this.entity.KPIDisableMeterSelection == false || this.entity.KPIDisableSpatialSelection == false) {
                    this.dropTargetSpatial = new Ext.dd.DropTarget(this.el.id, {
                                // id : 'ViewerSpatialDD' + this.el.id,
                                ddGroup    : 'SpatialDD',
                                notifyDrop : this.onNotifyDrop.createDelegate(this)
                            });
                }
                if (this.entity.IsKPI == true || this.entity.KPIDisableMeterClassificationSelection == false) {
                    this.dropTargetClassification = new Ext.dd.DropTarget(this.el.id, {
                                // id : 'ViewerClassificationDD' + this.el.id,
                                ddGroup    : 'ClassificationDD',
                                notifyDrop : this.onNotifyDrop.createDelegate(this)
                            });
                }
                // Ext.get(this.el.id).on('contextmenu', function(e) {
                // this.onContextMenu(e);
                // }, this);
            },

            getEntities                                     : function() {
                return [this.entity];
            },
            /**
             * Locks the chart drop targets.
             */
            lockDropTargets                                 : function() {
                if (this.dropTargetSpatial)
                    this.dropTargetSpatial.lock();
                if (this.dropTargetClassification)
                    this.dropTargetClassification.lock();
            },

            /**
             * Unlocks the chart drop targets.
             */
            unlockDropTargets                               : function() {
                if (this.dropTargetSpatial)
                    this.dropTargetSpatial.unlock();
                if (this.dropTargetClassification)
                    this.dropTargetClassification.unlock();
            },

            onNotifyDrop                                    : function(dd, e, data) {
                if (data.node && data.node.attributes.entity) {
                    var entity = data.node.attributes.entity;
                    var type = Viz.getEntityType(entity.__type);
                    var key = data.node.attributes.Key || data.node.attributes.id;
                    this.writeDragAndDropEventToGoogleAnalytics(dd);
                    if (this._updatingByDrop === false) {
                        this._updatingByDrop = true;

                        if (dd.ddGroup == 'SpatialDD') {
                            if (this.entity.IsKPI == false && this.entity.KPIDisableMeterSelection == true && type == 'Vizelia.FOL.BusinessEntities.Meter') {
                                return false;
                            }
                            if (this.entity.IsKPI == false && this.entity.KPIDisableSpatialSelection == true && type != 'Vizelia.FOL.BusinessEntities.Meter') {
                                return false;
                            }
                            Viz.Services.EnergyWCF.Chart_AddSpatialEntity({
                                        KeyChart : this.entity.KeyChart,
                                        Key      : key,
                                        type     : type,
                                        success  : function(result) {
                                            this._updatingByDrop = false;
                                            Viz.util.MessageBusMgr.fireEvent('ChartChange', result);
                                        },
                                        failure: function (model, operation) {
                                            this._updatingByDrop = false;
                                            Ext.Msg.show({
                                                width: 400,
                                                title: $lang("error_ajax"),
                                                msg: model.Message,
                                                buttons: Ext.Msg.OK,
                                                icon: Ext.MessageBox.ERROR
                                            });
                                        },
                                        scope    : this

                                    });
                        }
                        else if (dd.ddGroup == 'ClassificationDD') {
                            if (this.entity.IsKPI == false && this.entity.KPIDisableMeterClassificationSelection == true) {
                                return false;
                            }
                            Viz.Services.EnergyWCF.Chart_AddClassificationItemEntity({
                                        KeyChart : this.entity.KeyChart,
                                        Key      : key,
                                        type     : type,
                                        success  : function(result) {
                                            this._updatingByDrop = false;
                                            Viz.util.MessageBusMgr.fireEvent('ChartChange', result);
                                        },
                                        failure: function (model, operation) {
                                            this._updatingByDrop = false;
                                            Ext.Msg.show({
                                                width: 400,
                                                title: $lang("error_ajax"),
                                                msg: model.Message,
                                                buttons: Ext.Msg.OK,
                                                icon: Ext.MessageBox.ERROR
                                            });
                                        },
                                        scope    : this
                                    });
                        }
                    }
                }
                return true;
            },
            /**
             * Destroy
             * @private
             */
            onDestroy                                       : function() {
                this.un('afterentityupdate', this.onUpdateEntity, this);
                this.un('afterrender', this.onAfterRender, this);

                if (this._datapointstore && this._datapointstore.proxy && this._datapointstore.proxy.lastRequest) {
                    Viz.util.Chart.cancelRequest(this._datapointstore.proxy.lastRequest);
                }

                if (this.dropTargetSpatial)
                    this.dropTargetSpatial.destroy();
                if (this.dropTargetClassification)
                    this.dropTargetClassification.destroy();

                Viz.util.MessageBusMgr.unsubscribe("ChartAxisChange", this.onChartAxisChange, this);
                Viz.util.MessageBusMgr.unsubscribe("ChartChange", this.onChartChange, this);
                Viz.util.MessageBusMgr.unsubscribe("DataSerieChange", this.onDataSerieChange, this);
                Viz.util.MessageBusMgr.unsubscribe("DynamicDisplayChange", this.onDynamicDisplayChange, this);
                Viz.util.MessageBusMgr.unsubscribe("CalculatedSerieChange", this.onCalculatedSerieChange, this);
                Viz.util.MessageBusMgr.unsubscribe("StatisticalSerieChange", this.onStatisticalSerieChange, this);
                Viz.util.MessageBusMgr.unsubscribe("ChartHistoricalAnalysisChange", this.onChartHistoricalAnalysisChange, this);
                Viz.util.MessageBusMgr.unsubscribe("ChartAlgorithmChange", this.onChartAlgorithmChange, this);
                Viz.util.MessageBusMgr.unsubscribe("ChartDrillDownChange", this.onChartDrillDownChangeChange, this);
                Viz.util.MessageBusMgr.unsubscribe("ChartCalendarViewSelectionChange", this.onChartCalendarViewSelectionChange, this);
                Viz.util.MessageBusMgr.unsubscribe("ChartPsetAttributeHistoricalChange", this.onChartPsetAttributeHistoricalChange, this);
                Viz.util.MessageBusMgr.unsubscribe("DataSeriePsetRatioChange", this.onDataSeriePsetRatioChange, this);
                Viz.util.MessageBusMgr.unsubscribe("FilterSpatialPsetChange", this.onFilterSpatialPsetChange, this);

                Viz.chart.Viewer.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Builds the inner item.
             * @param {Viz.BusinessEntity.Chart} entity
             * @private
             */
            getChartConfig                                  : function(entity) {
                var item;
                switch (entity.DisplayMode) {
                    case Vizelia.FOL.BusinessEntities.ChartDisplayMode.Image :
                        item = {
                            xtype  : 'vizChartImage',
                            id     : this._chartCmpt,
                            entity : entity
                        };
                        break;
                    // Removed until we decide the best charting library

                    case Vizelia.FOL.BusinessEntities.ChartDisplayMode.Grid :
                        var pageSize = 50;
                        item = {
                            bodyCssClass : 'viz-chart-loading',
                            border       : false
                        };
                        this._datapointstore = new Viz.store.DataPoint({
                                    KeyChart    : entity.KeyChart,
                                    autoDestroy : true,
                                    listeners   : {
                                        load : {
                                            fn     : function(store) {
                                                item = {
                                                    xtype                        : 'vizGridDataPoint',
                                                    store                        : store,
                                                    entity                       : entity,
                                                    title                        : '',// (this.entityPortlet && false) ? this.entityPortlet.Title : entity.Title,
                                                    id                           : this._chartCmpt,
                                                    border                       : false,
                                                    transpose                    : entity.GridTranspose,
                                                    transposeLocationWidth       : entity.GridTransposeLocationWidth,
                                                    transposeClassificationWidth : entity.GridTransposeClassificationWidth,
                                                    transposebuffered            : entity.GridTransposeBuffered,
                                                    transposeDisplayLegendSort   : entity.DisplayLegendSort,
                                                    direction                    : (entity.DisplayLegendSort == Vizelia.FOL.BusinessEntities.ChartLegendSort.ByNameDesc) ? 'DESC' : 'ASC',
                                                    numberFormat                 : Viz.util.Chart.getNumberFormat(entity),
                                                    pageSize                     : pageSize,
                                                    measure                      : entity.DataGridPercentage != Vizelia.FOL.BusinessEntities.ChartDataGridPercentage.None ? 'Tooltip' : 'YValue',
                                                    aggregator                   : entity.DataGridPercentage != Vizelia.FOL.BusinessEntities.ChartDataGridPercentage.None ? 'concat' : 'sumDataPoint'
                                                };
                                                this.removeAll();
                                                this.add(item);
                                                this.doLayout();
                                            },
                                            single : true,
                                            scope  : this
                                        }
                                    }
                                });

                        this._datapointstore.load({
                                    params : {
                                        start : 0,
                                        limit : pageSize
                                    }
                                });
                        break;

                    case Vizelia.FOL.BusinessEntities.ChartDisplayMode.Zing :
                        item = {
                            xtype    : 'vizChartZing',
                            id       : this._chartCmpt,
                            entity   : entity,
                            useFlash : false
                        };
                        break;

                    case Vizelia.FOL.BusinessEntities.ChartDisplayMode.HighCharts :
                        item = {
                            xtype    : 'vizChartHighCharts',
                            id       : this._chartCmpt,
                            entity   : entity,
                            useFlash : true
                        }
                        break;

                    case Vizelia.FOL.BusinessEntities.ChartDisplayMode.CustomGrid :
                        item = {
                            xtype  : 'vizChartCustomGrid',
                            id     : this._chartCmpt,
                            entity : entity
                        }
                        break;
                    case Vizelia.FOL.BusinessEntities.ChartDisplayMode.Html :
                        item = {
                            xtype  : 'vizChartHtml',
                            id     : this._chartCmpt,
                            entity : entity
                        }
                        break;

                }
                return item;

            },

            getChart                                        : function() {
                return Ext.getCmp(this._chartCmpt);
            },

            /**
             * Check if the inner item has the correct type and rebuild it if needed
             * @param {Viz.BusinessEntity.Chart} entity
             */
            checkChartType                                  : function(entity) {
                var updateItem = false;

                var chart = this.getChart();
                if (chart) {
                    var xtype = chart.getXType();

                    switch (entity.DisplayMode) {
                        case Vizelia.FOL.BusinessEntities.ChartDisplayMode.Image :
                            updateItem = (xtype != 'vizChartImage');
                            break;

                        // Removed until we decide the best charting library

                        case Vizelia.FOL.BusinessEntities.ChartDisplayMode.Grid :
                            updateItem = (xtype != 'vizGridDataPoint' || chart.transpose != entity.GridTranspose || chart.transposebuffered != entity.GridTransposeBuffered || chart.transposeLocationWidth != entity.GridTransposeLocationWidth || chart.transposeClassificationWidth != entity.GridTransposeClassificationWidth || chart.transposeDisplayLegendSort != entity.DisplayLegendSort);
                            break;
                        // Removed until we decide the best charting library

                        case Vizelia.FOL.BusinessEntities.ChartDisplayMode.HighCharts :
                            updateItem = (xtype != 'vizChartHighCharts');
                            break;

                        case Vizelia.FOL.BusinessEntities.ChartDisplayMode.CustomGrid :
                            updateItem = (xtype != 'vizChartCustomGrid' || entity.CustomGridColumnCount != chart.entity.CustomGridColumnCount || entity.CustomGridRowCount != chart.entity.CustomGridRowCount || entity.CustomGridHideLines != chart.entity.CustomGridHideLines);
                            break;

                        case Vizelia.FOL.BusinessEntities.ChartDisplayMode.Html :
                            updateItem = (xtype != 'vizChartHtml');
                            break;
                    }

                    if (updateItem) {
                        this.removeAll();
                        this.add(this.getChartConfig(entity));
                    }
                    this.doLayout();
                }
            },

            /**
             * Return the current background color.
             * @return {}
             */
            getBackgroundColor                              : function() {
                var chart = this.getChart();
                var xtype = chart.getXType();
                if (xtype == 'vizChartImage') {
                    return chart.getBackgroundColor();
                }
                return null;
            },

            /**
             * Builds the plugins config.
             * @private
             */
            getPluginsConfig                                : function(config) {
                var sliders = [{
                            text            : $lang('msg_chart_timeinterval'),
                            iconCls         : 'viz-icon-small-chart-timeinterval',
                            entityAttribute : 'TimeInterval',
                            hidden          : config.entity.IsKPI == false && config.entity.KPIDisableTimeIntervalSlider == true,
                            store           : new Viz.store.Enum({
                                        autoLoad      : false,
                                        serviceParams : {
                                            enumTypeName : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common'
                                        }
                                    })
                        }, {
                            text            : $lang('msg_chart_localisation'),
                            iconCls         : 'viz-icon-small-chart-localisation',
                            entityAttribute : 'Localisation',
                            hidden          : config.entity.IsKPI == false && config.entity.KPIDisableLocalisationSlider == true,
                            store           : new Viz.store.ChartLocalisation({
                                        autoLoad : false
                                    })
                        }, {
                            text            : $lang('msg_chart_classificationlevel'),
                            iconCls         : 'viz-icon-small-classificationitem',
                            entityAttribute : 'ClassificationLevel',
                            hidden          : config.entity.IsKPI == false && config.entity.KPIDisableClassificationLevelSlider == true,
                            store           : new Viz.store.ChartClassificationLevel({
                                        autoLoad : false
                                    }),
                            minValue        : 1
                        }, {
                            text            : $lang('msg_chart_displaymode'),
                            iconCls         : 'viz-icon-small-chart',
                            entityAttribute : 'DisplayMode',
                            hidden          : config.entity.IsKPI == false && config.entity.KPIDisableDisplayModeSlider == true,
                            store           : new Viz.store.Enum({
                                        autoLoad      : false,
                                        serviceParams : {
                                            enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartDisplayMode,Vizelia.FOL.Common'
                                        }
                                    })
                        }, {
                            text            : $lang('msg_chart_limitseriesnumber'),
                            iconCls         : 'viz-icon-small-gridfilter',
                            entityAttribute : 'LimitSeriesNumber',
                            hidden          : config.entity.IsKPI == false && config.entity.KPIDisableLimitSeriesNumberSlider == true,
                            store           : new Viz.store.Enum({
                                        autoLoad      : false,
                                        serviceParams : {
                                            enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartLimitSeriesNumber,Vizelia.FOL.Common'
                                        }
                                    })
                        }];

                if (config.entityPortlet) {
                    var activeAttribute = Ext.state.Manager.get(Viz.Const.State.PortletActiveSlider + config.entityPortlet.KeyPortlet);
                    if (!Ext.isEmpty(activeAttribute)) {
                        var active = null;
                        Ext.each(sliders, function(slider) {
                                    if (slider.entityAttribute == activeAttribute)
                                        active = slider;
                                }, this);

                        if (active) {
                            sliders.remove(active);
                            sliders = [active].concat(sliders);
                        }
                    }
                }

                return {
                    ptype   : 'vizChartViewerSlider',
                    sliders : sliders
                };
            },

            textToBold                                      : function(text) {
                return text.toBold();
            },
            /**
             * Create the MenuItems from a ChartType record.
             * @param {} record
             * @return {}
             */
            createMenuItemFromChartType                     : function(record) {
                var conf = {
                    group   : 'ChartType',
                    text    : this.entity.ChartType == record.data.Value ? this.textToBold(record.data.Label) : record.data.Label,
                    iconCls : record.data.IconCls,
                    handler : this.onChartUpdateType.createDelegate(this, [record.data.Value])
                }
                if (record.data.Value == Vizelia.FOL.BusinessEntities.ChartType.Combo || record.data.Value == Vizelia.FOL.BusinessEntities.ChartType.ComboHorizontal || record.data.Value == Vizelia.FOL.BusinessEntities.ChartType.Radar) {
                    conf.menu = {
                        xtype                    : 'menustore',
                        store                    : new Viz.store.Enum({
                                    serviceParams : {
                                        enumTypeName : 'Vizelia.FOL.BusinessEntities.DataSerieType,Vizelia.FOL.Common'
                                    }
                                }),
                        createMenuItemFromRecord : function(recordSerie) {
                            if (record.data.Value == Vizelia.FOL.BusinessEntities.ChartType.Radar) {
                                if (recordSerie.data.Value == Vizelia.FOL.BusinessEntities.DataSerieType.None || recordSerie.data.Value == Vizelia.FOL.BusinessEntities.DataSerieType.Spline || recordSerie.data.Value == Vizelia.FOL.BusinessEntities.DataSerieType.AreaLine)
                                    return null;
                            }
                            return {
                                group   : 'DataSerieType',
                                text    : this.entity.DataSerieType == recordSerie.data.Value ? this.textToBold(recordSerie.data.Label) : recordSerie.data.Label,
                                iconCls : recordSerie.data.IconCls,
                                handler : this.onChartUpdateDataSerieType.createDelegate(this, [recordSerie.data.Value, record.data.Value])
                            }
                        }.createDelegate(this)
                    };
                }
                if (record.data.Value == Vizelia.FOL.BusinessEntities.ChartType.Gauge) {
                    conf.menu = {
                        xtype                    : 'menustore',
                        store                    : new Viz.store.Enum({
                                    serviceParams : {
                                        enumTypeName : 'Vizelia.FOL.BusinessEntities.GaugeType,Vizelia.FOL.Common'
                                    }
                                }),
                        createMenuItemFromRecord : function(record) {
                            return {
                                group   : 'GaugeType',
                                text    : this.entity.GaugeType == record.data.Value ? this.textToBold(record.data.Label) : record.data.Label,
                                iconCls : record.data.IconCls,
                                handler : this.onChartUpdateGaugeType.createDelegate(this, [record.data.Value])
                            }
                        }.createDelegate(this)
                    };
                }
                return conf;
            },

            /**
             * Base function to create a MenuItem from a DataSerie record.
             * @param {Object} record
             * @param {bool} isClassificationItem true to display the classificationitem dataserie, false to display the real dataserie.
             * @return {}
             */
            createMenuItemFromDataSerie                     : function(record, isClassificationItem) {
                if (record.data.IsClassificationItem == isClassificationItem) {
                    return {
                        text    : record.data.Name,
                        iconCls : record.data.IconCls,
                        handler : this.onOpenFormCrudDataSerie.createDelegate(this, [record.data]),
                        menu    : {
                            items : [{
                                        text    : $lang("msg_dataserie_visible"),
                                        checked : !record.data.Hidden,
                                        handler : this.onDataSerieToggleVisibility.createDelegate(this, [record.data, isClassificationItem]),
                                        scope   : this
                                    }, {
                                        text    : $lang("msg_dataserie_delete"),
                                        iconCls : 'viz-icon-small-delete',
                                        handler : this.onDataSerieDelete.createDelegate(this, [record.data, isClassificationItem]),
                                        scope   : this
                                    }]
                        }
                    }
                }
            },

            /**
             * Create a MenuItem record from a DataSerie, without the ClassificationItem DataSerie.
             * @param {} record
             * @return {}
             */
            createMenuItemFromDataSerieNoClassificationItem : function(record) {
                return this.createMenuItemFromDataSerie(record, false);
            },

            /**
             * Create a MenuItem record from a DataSerie, with onlys the ClassificationItem DataSerie.
             * @param {} record
             * @return {}
             */
            createMenuItemFromDataSerieClassificationItem   : function(record) {
                return this.createMenuItemFromDataSerie(record, true);
            },

            /**
             * Create a MenuItem record from an Localisation.
             * @param {} record
             * @return {}
             */
            createMenuItemFromLocalisation                  : function(record) {
                return {
                    group   : 'Localisation',
                    text    : this.entity.Localisation == record.data.Value ? this.textToBold(record.data.Label) : record.data.Label,
                    iconCls : record.data.IconCls,
                    handler : this.onChartUpdateLocalisation.createDelegate(this, [record.data.Value, record.data])
                }
            },

            /**
             * Create a MenuItem record from an AxisTimeInterval.
             * @param {} record
             * @return {}
             */
            createMenuItemFromTimeInterval                  : function(record) {
                return {
                    group   : 'TimeInterval',
                    text    : this.entity.TimeInterval == record.data.Value ? this.textToBold(record.data.Label) : record.data.Label,
                    iconCls : record.data.IconCls,
                    handler : this.onChartUpdateTimeInterval.createDelegate(this, [record.data.Value])
                }
            },

            /**
             * Create a MenuItem record from an AxisTimeInterval.
             * @param {} record
             * @return {}
             */
            createMenuItemFromDisplayLegendSort             : function(record) {
                return {
                    group   : 'DisplayLegendSort',
                    text    : this.entity.DisplayLegendSort == record.data.Value ? this.textToBold(record.data.Label) : record.data.Label,
                    iconCls : record.data.IconCls,
                    handler : this.onChartUpdateDisplayLegendSort.createDelegate(this, [record.data.Value])
                }
            },

            /**
             * Create a MenuItem record from an ShaddingEffect.
             * @param {} record
             * @return {}
             */
            createMenuItemFromDisplayShaddingEffect         : function(record) {
                return {
                    group   : 'DisplayShaddingEffect',
                    text    : this.entity.DisplayShaddingEffect == record.data.Value ? this.textToBold(record.data.Label) : record.data.Label,
                    iconCls : 'viz-icon-small-chart-shaddingeffect',
                    handler : this.onChartUpdateDisplayShaddingEffect.createDelegate(this, [record.data.Value])
                }
            },

            createMenuItemFromPalette                       : function(record) {
                return {
                    group   : 'Palette',
                    text    : this.entity.KeyPalette == record.data.KeyPalette ? this.textToBold(record.data.Label) : record.data.Label,
                    iconCls : 'viz-icon-small-palette',
                    handler : this.onChartUpdatePalette.createDelegate(this, [record.data.KeyPalette])
                }
            },

            createMenuItemFromModernUIColor                 : function(record) {
                return {
                    group   : 'Palette',
                    text    : record.data.Label,
                    iconCls : 'viz-icon-small-modernui',
                    handler : this.onChartUpdateModernUI.createDelegate(this, [record.data.Value])
                }
            },

            /**
             * Builds the menu config.
             * @private
             */
            getMenuConfig                                   : function() {
                var seriesStore = new Viz.store.DataSerie({
                            KeyChart : this.entity.KeyChart
                        });
                return {
                    defaults : {
                        hideOnClick : false
                    },
                    items    : [{
                                text    : $lang("msg_chart_type"),
                                iconCls : 'viz-icon-small-chart',
                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.Enum({
                                                serviceParams : {
                                                    enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartType,Vizelia.FOL.Common'
                                                }
                                            }),
                                    createMenuItemFromRecord : this.createMenuItemFromChartType.createDelegate(this)
                                }
                            }, {
                                text    : $lang('msg_dataseries_customise'),
                                iconCls : 'viz-icon-small-dataserie',
                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : seriesStore,
                                    createMenuItemFromRecord : this.createMenuItemFromDataSerieNoClassificationItem.createDelegate(this),
                                    itemsOffset              : 1,
                                    items                    : [{
                                                text    : $lang('msg_dataserie_customise_add'),
                                                iconCls : 'viz-icon-small-add',
                                                handler : this.onOpenFormCrudDataSerie.createDelegate(this, [null, false]),
                                                scope   : this
                                            }]
                                }
                            }, {
                                text    : $lang('msg_dataseries_customise_byclassification'),
                                iconCls : 'viz-icon-small-classificationitem',
                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : seriesStore,
                                    createMenuItemFromRecord : this.createMenuItemFromDataSerieClassificationItem.createDelegate(this),
                                    itemsOffset              : 1,
                                    items                    : [{
                                                text    : $lang('msg_dataserie_customise_add'),
                                                iconCls : 'viz-icon-small-add',
                                                handler : this.onOpenFormCrudDataSerie.createDelegate(this, [null, true]),
                                                scope   : this
                                            }]
                                }
                            }, {
                                text    : $lang("msg_chart_display"),
                                iconCls : 'viz-icon-small-chart-display',
                                menu    : {
                                    items : [{
                                                text    : $lang('msg_chart_display_shaddingeffect'),
                                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                                iconCls : 'viz-icon-small-chart-shaddingeffect',
                                                scope   : this,
                                                menu    : {
                                                    xtype                    : 'menustore',
                                                    store                    : new Viz.store.Enum({
                                                                serviceParams : {
                                                                    enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartShaddingEffect,Vizelia.FOL.Common'
                                                                }
                                                            }),
                                                    createMenuItemFromRecord : this.createMenuItemFromDisplayShaddingEffect.createDelegate(this)
                                                }
                                            }, {
                                                text    : $lang('msg_chart_palette'),
                                                iconCls : 'viz-icon-small-palette',
                                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                                scope   : this,
                                                menu    : {
                                                    xtype                    : 'menustore',
                                                    store                    : new Viz.store.Palette(),
                                                    createMenuItemFromRecord : this.createMenuItemFromPalette.createDelegate(this)
                                                }
                                            }, {
                                                text    : $lang('msg_chart_modernui'),
                                                iconCls : 'viz-icon-small-modernui',
                                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                                scope   : this,
                                                menu    : {
                                                    xtype                    : 'menustore',
                                                    store                    : new Viz.store.Enum({
                                                                serviceParams : {
                                                                    enumTypeName : 'Vizelia.FOL.BusinessEntities.ModernUIColor,Vizelia.FOL.Common'
                                                                }
                                                            }),
                                                    createMenuItemFromRecord : this.createMenuItemFromModernUIColor.createDelegate(this)
                                                }
                                            }, new Ext.menu.Separator({
                                                        hidden : !$authorized('@@@@ Chart - Full Control')
                                                    }), {
                                                text        : $lang('msg_chart_toggle_legend'),
                                                xtype       : 'menucheckitemicon',
                                                checked     : this.entity.DisplayLegendVisible,
                                                textIconCls : 'viz-icon-small-chart-legend',
                                                handler     : this.onChartToggleLegend,
                                                scope       : this,
                                                menu        : !$authorized('@@@@ Chart - Full Control') ? null : {
                                                    xtype                    : 'menustore',
                                                    store                    : new Viz.store.Enum({
                                                                serviceParams : {
                                                                    enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartLegendSort,Vizelia.FOL.Common'
                                                                }
                                                            }),
                                                    createMenuItemFromRecord : this.createMenuItemFromDisplayLegendSort.createDelegate(this)
                                                }
                                            }, {
                                                text        : $lang('msg_chart_toggle_values'),
                                                xtype       : 'menucheckitemicon',
                                                checked     : this.entity.DisplayValues,
                                                textIconCls : 'viz-icon-small-chart-displayvalues',
                                                handler     : this.onChartToggleValues,
                                                scope       : this
                                            }, {
                                                text        : $lang('msg_chart_toggle_stack'),
                                                xtype       : 'menucheckitemicon',
                                                hidden      : !$authorized('@@@@ Chart - Full Control'),
                                                checked     : this.entity.DisplayStackSeries,
                                                textIconCls : 'viz-icon-small-chart-stack',
                                                handler     : this.onChartToggleStack,
                                                scope       : this
                                            }, {
                                                text        : $lang('msg_chart_toggle_dynamicdisplay'),
                                                xtype       : 'menucheckitemicon',
                                                hidden      : !$authorized('@@@@ Chart - Full Control') || Ext.isEmpty(this.entity.KeyDynamicDisplay),
                                                checked     : this.entity.UseDynamicDisplay,
                                                textIconCls : 'viz-icon-small-dynamicdisplay',
                                                handler     : this.onChartToggleDynamicDisplay,
                                                scope       : this
                                            }, {
                                                text        : $lang('msg_chart_display_startdateenddate'),
                                                xtype       : 'menucheckitemicon',
                                                checked     : this.entity.DisplayStartEndDate,
                                                textIconCls : 'viz-icon-small-calendar',
                                                handler     : this.onChartToggleDateDisplay,
                                                scope       : this
                                            }]
                                }
                            }, {
                                text    : $lang("msg_chart_timerange"),
                                iconCls : 'viz-icon-small-calendarevent',
                                hidden  : this.entity.IsKPI == false && this.entity.KPIDisableDateRangeMenu == true,
                                menu    : {
                                    defaults : {
                                        hideOnClick : true
                                    },
                                    items    : Viz.util.Chart.getTimeRangeMenu(this.entity, this, this._startDateMenuItemId, this._endDateMenuItemId)
                                    /*
                                     * [{ text : this.entity.DynamicTimeScaleEnabled == false ? this.textToBold($lang('msg_chart_startdate')) : $lang('msg_chart_startdate'), id : this._startDateMenuItemId, iconCls : 'viz-icon-small-calendar', menu : { hideOnClick : false, xtype : 'datemenu', handler : this.onChartUpdateStartDate, hideOnClick : false, value : this.entity.StartDate, scope : this } }, { text : this.entity.DynamicTimeScaleEnabled == false ? this.textToBold($lang('msg_chart_enddate')) : $lang('msg_chart_enddate'), id : this._endDateMenuItemId, iconCls : 'viz-icon-small-calendar', menu : { hideOnClick : false, xtype : 'datemenu', handler : this.onChartUpdateEndDate, hideOnClick : false, value : this.entity.EndDate, scope : this } }, '-', { text :
                                     * (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 1 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days && this.entity.DynamicTimeScaleCalendar && !this.entity.DynamicTimeScaleCompleteInterval) ? this.textToBold($lang("msg_chart_dynamic_timescale_today")) : $lang("msg_chart_dynamic_timescale_today"), iconCls : 'viz-icon-small-chart-timeinterval-day', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled &&
                                     * this.entity.DynamicTimeScaleFrequency == 1 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days && this.entity.DynamicTimeScaleCalendar && this.entity.DynamicTimeScaleCompleteInterval) ? this.textToBold($lang("msg_chart_dynamic_timescale_yesterday")) : $lang("msg_chart_dynamic_timescale_yesterday"), iconCls : 'viz-icon-small-chart-timeinterval-day', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true, true, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 1 &&
                                     * this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Weeks && this.entity.DynamicTimeScaleCalendar) ? this.textToBold($lang("msg_chart_dynamic_timescale_current_week")) : $lang("msg_chart_dynamic_timescale_current_week"), iconCls : 'viz-icon-small-chart-timeinterval-week', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Weeks, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 1 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months &&
                                     * this.entity.DynamicTimeScaleCalendar && !this.entity.DynamicTimeScaleCompleteInterval) ? this.textToBold($lang("msg_chart_dynamic_timescale_current_month")) : $lang("msg_chart_dynamic_timescale_current_month"), iconCls : 'viz-icon-small-chart-timeinterval-month', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 1 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months && this.entity.DynamicTimeScaleCalendar &&
                                     * this.entity.DynamicTimeScaleCompleteInterval) ? this.textToBold($lang("msg_chart_dynamic_timescale_previous_month")) : $lang("msg_chart_dynamic_timescale_previous_month"), iconCls : 'viz-icon-small-chart-timeinterval-month', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true, true, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 1 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Years && this.entity.DynamicTimeScaleCalendar) ?
                                     * this.textToBold($lang("msg_chart_dynamic_timescale_current_year")) : $lang("msg_chart_dynamic_timescale_current_year"), iconCls : 'viz-icon-small-chart-timeinterval-year', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Years, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months); }, scope : this }, '-', { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 1 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours && !this.entity.DynamicTimeScaleCalendar) ? this.textToBold($lang("msg_chart_dynamic_timescale_last_hour")) :
                                     * $lang("msg_chart_dynamic_timescale_last_hour"), iconCls : 'viz-icon-small-chart-timeinterval-hour', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours, false, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Minutes); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 1 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days && !this.entity.DynamicTimeScaleCalendar) ? this.textToBold($lang("msg_chart_dynamic_timescale_last_24_hours")) : $lang("msg_chart_dynamic_timescale_last_24_hours"), iconCls : 'viz-icon-small-chart-timeinterval-day',
                                     * handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, false, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 3 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days && !this.entity.DynamicTimeScaleCalendar) ? this.textToBold($lang("msg_chart_dynamic_timescale_last_3_days")) : $lang("msg_chart_dynamic_timescale_last_3_days"), iconCls : 'viz-icon-small-chart-timeinterval-day', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b);
                                     * this.onChartUpdateDynamicTimeScale(3, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, false, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 1 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Weeks && !this.entity.DynamicTimeScaleCalendar) ? this.textToBold($lang("msg_chart_dynamic_timescale_last_7_days")) : $lang("msg_chart_dynamic_timescale_last_7_days"), iconCls : 'viz-icon-small-chart-timeinterval-week', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Weeks, false, false,
                                     * Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 15 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days && !this.entity.DynamicTimeScaleCalendar) ? this.textToBold($lang("msg_chart_dynamic_timescale_last_15_days")) : $lang("msg_chart_dynamic_timescale_last_15_days"), iconCls : 'viz-icon-small-chart-timeinterval-week', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(15, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, false, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days); }, scope : this }, { text :
                                     * (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 30 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days && !this.entity.DynamicTimeScaleCalendar) ? this.textToBold($lang("msg_chart_dynamic_timescale_last_30_days")) : $lang("msg_chart_dynamic_timescale_last_30_days"), iconCls : 'viz-icon-small-chart-timeinterval-month', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(30, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, false, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 3 &&
                                     * this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months && !this.entity.DynamicTimeScaleCalendar) ? this.textToBold($lang("msg_chart_dynamic_timescale_last_3_months")) : $lang("msg_chart_dynamic_timescale_last_3_months"), iconCls : 'viz-icon-small-chart-timeinterval-month', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(3, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, false, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 6 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months &&
                                     * !this.entity.DynamicTimeScaleCalendar) ? this.textToBold($lang("msg_chart_dynamic_timescale_last_6_months")) : $lang("msg_chart_dynamic_timescale_last_6_months"), iconCls : 'viz-icon-small-chart-timeinterval-month', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(6, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, false, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 12 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months && !this.entity.DynamicTimeScaleCalendar) ?
                                     * this.textToBold($lang("msg_chart_dynamic_timescale_last_12_months")) : $lang("msg_chart_dynamic_timescale_last_12_months"), iconCls : 'viz-icon-small-chart-timeinterval-year', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(12, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, false, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months); }, scope : this }, { text : (this.entity.DynamicTimeScaleEnabled && this.entity.DynamicTimeScaleFrequency == 24 && this.entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months && !this.entity.DynamicTimeScaleCalendar) ? this.textToBold($lang("msg_chart_dynamic_timescale_last_24_months")) :
                                     * $lang("msg_chart_dynamic_timescale_last_24_months"), iconCls : 'viz-icon-small-chart-timeinterval-year', handler : function(b, e) { this.markDynamicTimeScaleMenuItemToRegularText(b); this.onChartUpdateDynamicTimeScale(24, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, false, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months); }, scope : this }]
                                     */
                                }
                            }, {
                                text    : $lang('msg_chart_timeinterval'),
                                hidden  : this.entity.IsKPI == false && this.entity.KPIDisableTimeIntervalSlider == true,
                                iconCls : 'viz-icon-small-chart-timeinterval',
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.Enum({
                                                serviceParams : {
                                                    enumTypeName : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common'
                                                }
                                            }),
                                    createMenuItemFromRecord : this.createMenuItemFromTimeInterval.createDelegate(this)
                                }
                            }, {
                                text    : $lang('msg_chart_localisation'),
                                iconCls : 'viz-icon-small-chart-localisation',
                                hidden  : true,
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.ChartLocalisation({
                                                autoLoad : false
                                            }),
                                    createMenuItemFromRecord : this.createMenuItemFromLocalisation.createDelegate(this)
                                }
                            }, {
                                text    : this.entity.LockFilterSpatial ? $lang("msg_chart_unlockspatialfilter") : $lang("msg_chart_lockspatialfilter"),
                                iconCls : this.getLockFilterSpatialCss(!this.entity.LockFilterSpatial, true),
                                handler : this.onChartLockFilterSpatial,
                                scope   : this
                            }, {
                                text    : $lang("msg_chart_calculatedseries"),
                                iconCls : 'viz-icon-small-calculatedserie',
                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                handler : this.onCalculatedSerie,
                                scope   : this,
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.CalculatedSerie({
                                                KeyChart : this.entity.KeyChart
                                            }),
                                    createMenuItemFromRecord : function(record) {
                                        return {
                                            text    : record.data.Name,
                                            iconCls : 'viz-icon-small-calculatedserie',
                                            scope   : this,
                                            handler : function() {
                                                Viz.grid.CalculatedSerie.update(record.data);
                                            }
                                        }
                                    },
                                    itemsOffset              : 1,
                                    items                    : [{
                                                text    : $lang('msg_calculatedserie_add'),
                                                iconCls : 'viz-icon-small-add',
                                                handler : function() {
                                                    Viz.grid.CalculatedSerie.create({
                                                                KeyChart : this.entity.KeyChart
                                                            });
                                                },
                                                scope   : this
                                            }]
                                }
                            }, {
                                text    : $lang("msg_chart_statisticalseries"),
                                iconCls : 'viz-icon-small-statisticalserie',
                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                handler : this.onStatisticalSerie,
                                scope   : this,
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.StatisticalSerie({
                                                KeyChart : this.entity.KeyChart
                                            }),
                                    createMenuItemFromRecord : function(record) {
                                        return {
                                            text    : Viz.Configuration.GetEnumLabel('MathematicOperator', record.data.Operator) + ' - ' + record.data.ClassificationItemTitle,
                                            iconCls : 'viz-icon-small-statisticalserie',
                                            scope   : this,
                                            handler : function() {
                                                Viz.grid.StatisticalSerie.update(record.data);
                                            }
                                        }
                                    },
                                    itemsOffset              : 1,
                                    items                    : [{
                                                text    : $lang('msg_statisticalserie_add'),
                                                iconCls : 'viz-icon-small-add',
                                                handler : function() {
                                                    Viz.grid.StatisticalSerie.create({
                                                                KeyChart : this.entity.KeyChart
                                                            });
                                                },
                                                scope   : this
                                            }]
                                }
                            }, {
                                text    : $lang("msg_charthistoricalanalysis"),
                                iconCls : 'viz-icon-small-chart-historicalanalysis',
                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                handler : this.onChartHistoricalAnalysis,
                                scope   : this,
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.ChartHistoricalAnalysis({
                                                KeyChart : this.entity.KeyChart
                                            }),
                                    createMenuItemFromRecord : function(record) {
                                        return {
                                            text    : record.data.Name,
                                            iconCls : 'viz-icon-small-chart-historicalanalysis',
                                            scope   : this,
                                            handler : function() {
                                                Viz.grid.ChartHistoricalAnalysis.update(record.data);
                                            }
                                        }
                                    },
                                    itemsOffset              : 1,
                                    items                    : [{
                                                text    : $lang('msg_charthistoricalanalysis_add'),
                                                iconCls : 'viz-icon-small-add',
                                                handler : function() {
                                                    Viz.grid.ChartHistoricalAnalysis.create({
                                                                KeyChart : this.entity.KeyChart
                                                            });
                                                },
                                                scope   : this
                                            }]
                                }
                            }, new Ext.menu.Separator({
                                        hidden : true
                                    }), {
                                text    : this.entity.DrillDownEnabled ? $lang("msg_chartdrilldown_disable") : $lang("msg_chartdrilldown_enable"),
                                iconCls : 'viz-icon-small-chartviewer',
                                hidden  : !$authorized('@@@@ ChartDrillDown - Full Control'),
                                handler : this.onChartToggleDrillDown,
                                // hidden : true,
                                scope   : this
                                // ,
                            /*
                             * menu : { items : [{ text : $lang("msg_chartdrilldown_reset"), iconCls : 'viz-icon-small-delete', handler : this.onChartDrillDownReset, scope : this }] }
                             */
                        }   , new Ext.menu.Separator({
                                        hidden : this.useWindow
                                    }), {
                                text    : $lang("msg_application_export"),
                                iconCls : 'viz-icon-small-report',
                                menu    : {
                                    items : [{
                                                text    : "PDF",
                                                iconCls : 'viz-icon-small-pdf',
                                                handler : this.onExportToPDF,
                                                scope   : this
                                            }, {
                                                text    : "Excel",
                                                iconCls : 'viz-icon-small-excel',
                                                handler : this.onExportToExcel,
                                                scope   : this
                                            }]
                                }
                            }, '-', {
                                text    : $lang("msg_chart_filterspatial"),
                                iconCls : 'viz-icon-small-site',
                                handler : this.onChartFilterSpatial,
                                scope   : this
                            }, '-'].concat(Viz.chart.Viewer.superclass.getMenuConfig.apply(this, arguments).items)
                };
            },

            /**
             * @param {bool} includeParentMenu : true to include a Parent Menu Item
             * @return {}
             */
            getMenuConfigUpdate                             : function(includeParentMenu) {
                var retVal = [{
                            text    : $lang('msg_general'),
                            iconCls : 'viz-icon-small-form-update',
                            hidden  : !$authorized('@@@@ Chart - Full Control'),
                            handler : function(menuItem) {
                                this.onOpenFormCrud(menuItem, {
                                            uniqueTab : 'general',
                                            width     : 780,
                                            height    : 400
                                        });
                            },
                            scope   : this
                        }, {
                            text    : $lang('msg_chart_datasource'),
                            iconCls : 'viz-icon-small-chart-datasource',
                            hidden  : !$authorized('@@@@ Chart - Full Control'),
                            handler : function(menuItem) {
                                this.onOpenFormCrud(menuItem, {
                                            uniqueTab : 'datasource',
                                            width     : 500,
                                            height    : 460
                                        });
                            },
                            scope   : this
                        }, {
                            text    : $lang('msg_chart_meterall'),
                            iconCls : 'viz-icon-small-meter-all',
                            hidden  : !$authorized('@@@@ Chart - Full Control'),
                            handler : function(menuItem) {
                                this.onOpenFormCrud(menuItem, {
                                            uniqueTab : 'meterall',
                                            width     : 500,
                                            height    : 400
                                        });
                            },
                            scope   : this
                        }, {
                            text    : $lang('msg_chart_display'),
                            hidden  : !$authorized('@@@@ Chart - Full Control'),
                            iconCls : 'viz-icon-small-chart-display',
                            handler : function(menuItem) {
                                this.onOpenFormCrud(menuItem, {
                                            uniqueTab : 'display',
                                            width     : 550,
                                            height    : 450
                                        });
                            },
                            scope   : this
                        }, {
                            text    : $lang('msg_chart_dynamicdisplay'),
                            hidden  : !$authorized('@@@@ Chart - Full Control'),
                            iconCls : 'viz-icon-small-dynamicdisplay',
                            handler : function(menuItem) {
                                this.onOpenFormCrud(menuItem, {
                                            uniqueTab : 'dynamicdisplay',
                                            width     : 750,
                                            height    : 450
                                        });
                            },
                            scope   : this,
                            menu    : {
                                xtype                    : 'menustore',
                                store                    : new Viz.store.DynamicDisplay({}),
                                createMenuItemFromRecord : function(record) {
                                    return {
                                        text    : record.data.Name,
                                        iconCls : 'viz-icon-small-dynamicdisplay',
                                        scope   : this,
                                        handler : function() {
                                            this.onChartSetDynamicDisplay(record.data.KeyDynamicDisplay);
                                        }
                                    }
                                }.createDelegate(this)
                            }

                        }, {
                            text    : $lang('msg_chart_dataseries') + ' & ' + $lang('msg_chartaxis'),
                            hidden  : !$authorized('@@@@ Chart - Full Control'),
                            iconCls : 'viz-icon-small-chart-hbar',
                            handler : function(menuItem) {
                                this.onOpenFormCrud(menuItem, {
                                            uniqueTab : 'dataseries',
                                            width     : 600,
                                            height    : 400
                                        });
                            },
                            scope   : this
                        }, {
                            text    : $lang('msg_chart_analysis'),
                            iconCls : 'viz-icon-small-chart-analysis',
                            hidden  : !$authorized('@@@@ Chart - Full Control'),
                            handler : function(menuItem) {
                                this.onOpenFormCrud(menuItem, {
                                            uniqueTab : 'analysis',
                                            width     : 500,
                                            height    : 400
                                        });
                            },
                            scope   : this,
                            menu    : {
                                xtype                    : $authorized('@@@@ Chart') ? 'menustore' : 'menu',
                                hidden                   : !$authorized('@@@@ Chart'),
                                store                    : new Viz.store.ChartAlgorithm({
                                            KeyChart : this.entity.KeyChart
                                        }),
                                createMenuItemFromRecord : function(record) {
                                    return {
                                        text    : record.data.AlgorithmName.replace('Vizelia.FOL.ConcreteProviders.', '').replace(', Vizelia.FOL.ConcreteProviders', ''),
                                        iconCls : 'viz-icon-small-python',
                                        scope   : this,
                                        handler : function() {
                                            Viz.grid.ChartAlgorithm.update(record.data);
                                        }
                                    }
                                },
                                itemsOffset              : 1,
                                items                    : [{
                                            text    : $lang('msg_algorithm_add'),
                                            iconCls : 'viz-icon-small-add',
                                            hidden  : !$authorized('@@@@ Chart - Full Control'),
                                            handler : function() {
                                                Viz.grid.ChartAlgorithm.create({
                                                            KeyChart : this.entity.KeyChart
                                                        });
                                            },
                                            scope   : this
                                        }]
                            }
                        }, {
                            text    : $lang('msg_chart_kpi'),
                            iconCls : 'viz-icon-small-chart-kpi',
                            hidden  : !$authorized('@@@@ Chart - Full Control') || !$authorized('@@@@ Chart KPI'),
                            handler : function(menuItem) {
                                this.onOpenFormCrud(menuItem, {
                                            uniqueTab : 'kpi',
                                            width     : 750,
                                            height    : 450
                                        });
                            },
                            scope   : this
                        }, {
                            text    : $lang('msg_portlet_frame'),
                            hidden  : !$authorized('@@@@ Chart - Full Control'),
                            iconCls : 'viz-icon-small-portlet',
                            handler : function() {
                                this.onOpenFormCrudPorlet()
                            },
                            scope   : this
                        }, '-', {
                            text    : $lang('msg_chart_completeform'),
                            iconCls : 'viz-icon-small-chart',
                            hidden  : !$authorized('@@@@ Chart - Full Control'),
                            handler : function() {
                                this.onOpenFormCrud();
                            },
                            scope   : this
                        }];

                if (includeParentMenu !== false) {
                    return [{
                                text    : $lang('msg_chart_update'),
                                iconCls : 'viz-icon-small-chart-update',
                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                menu    : {
                                    items : retVal
                                }
                            }]
                }
                else
                    return retVal;

            },

            /**
             * Return the css corresponding to the state of the LockFilterSpatial attribute.
             */
            getLockFilterSpatialCss                         : function(LockFilterSpatial, showLockIcon, isFavorite) {
                if (showLockIcon)
                    return LockFilterSpatial ? 'viz-icon-small-locked' : 'viz-icon-small-unlocked'
                else
                    return LockFilterSpatial ? 'viz-icon-small-locked' : (isFavorite ? 'viz-icon-small-chart-favorite' : 'viz-icon-small-chart');
            },

            /**
             * Listener for the refresh event.
             * @private
             */
            onRefresh                                       : function(clearCache) {
                Viz.chart.Viewer.superclass.onRefresh.apply(this, arguments);
                this.onClearCache(clearCache);
            },

            /**
             * Clear the Chart Cache and refresh the it.
             */
            onClearCache                                    : function(clearCache) {
                // this.makeToolbarBusy();
                if (clearCache !== false) {
                    Viz.Services.EnergyWCF.Chart_ClearCache({
                                Key               : this.entity.KeyChart,
                                KeyDynamicDisplay : this.entity.UseDynamicDisplay ? this.entity.KeyDynamicDisplay : null,
                                success           : function() {
                                    // this.clearToolbarStatus();
                                    this.reloadChart();
                                },
                                scope             : this
                            });
                }
                else {
                    // this.clearToolbarStatus();
                    this.reloadChart();
                }
            },

            /**
             * Handler to open the form of the chart
             * @private
             */
            onOpenFormCrudDataSerie                         : function(serie, isClassificationItem) {
                if (!Ext.isObject(serie)) {
                    serie = {
                        KeyChart             : this.entity.KeyChart,
                        IsClassificationItem : isClassificationItem
                    }
                    Viz.grid.DataSerie.create(serie);
                }
                else {
                    Viz.grid.DataSerie.update(serie);
                }
            },

            onOpenFormCrudFromTool                          : function(event) {
                var ctxMenu = new Ext.menu.Menu({
                            defaults : {
                                hideOnClick : true
                            },
                            items    : this.getMenuConfigUpdate(false)
                        });

                ctxMenu.on("hide", function(menu) {
                            menu.destroy();
                        });
                ctxMenu.showAt(event.getXY());
            },

            /**
             * Change the image src to point to the chart stream.
             * @private
             */
            reloadChart                                     : function() {
                if (!this.entity || this.collapsed || !this.rendered)
                    return;

                // / removed for now, since the fix in Image should prevent the error.
                // if (!this.useWindow && ((this.getParentPortalTab() && !this.getParentPortalTab().isActive) || (this.getParentPortalPanel() && !this.getParentPortalPanel().isVisible())))
                // return;

                this.checkChartType(this.entity);
                var chart = this.getChart();
                if (chart)
                    chart.reloadChart();
            },

            /**
             * Update the title of panel or of the parent window.
             * @private
             */
            updateTitle                                     : function() {
                var title = String.format('{0} : {1} -> {2}', this.entity.Title, this.entity.StartDate.format(Viz.getLocalizedDateFormat(false)), this.entity.EndDate.format(Viz.getLocalizedDateFormat(false)));
                if (this.useWindow) {
                    return this.findParentByType('vizChartWindow').setTitle(title);
                }
                else {
                    this.setTitle(title);
                }
            },

            /**
             * Listener for the chart change event.
             * @param {Viz.BusinessEntity.Chart} entity
             * @private
             */
            onChartChange                                   : function(entity) {
                if (this.entity.KeyChart == entity.KeyChart) {
                    this.entity = entity;
                    this.setIconClass(this.getLockFilterSpatialCss(this.entity.LockFilterSpatial, false, this.entity.IsFavorite));
                    this.reloadChart();
                }
            },
            /**
             * Listener for the chartaxis change event.
             * @param {Viz.BusinessEntity.Chart} entity
             * @private
             */
            onChartAxisChange                               : function(entity) {
                if (this.entity.KeyChart == entity.KeyChart) {
                    this.reloadChart();
                }
            },

            /**
             * Listener for the dataserie change event.
             * @param {Viz.BusinessEntity.DataSerie} entity
             * @private
             */
            onDataSerieChange                               : function(entity) {
                if (this.entity.KeyChart == entity.KeyChart) {
                    this.reloadChart();
                }
            },

            /**
             * Listener for the calculated change event.
             * @param {Viz.BusinessEntity.CalculatedSerie} entity
             * @private
             */
            onCalculatedSerieChange                         : function(entity) {
                if (this.entity.KeyChart == entity.KeyChart) {
                    this.reloadChart();
                }
            },

            /**
             * Listener for the statistical change event.
             * @param {Viz.BusinessEntity.StatisticalSerie} entity
             * @private
             */
            onStatisticalSerieChange                        : function(entity) {
                if (this.entity.KeyChart == entity.KeyChart) {
                    this.reloadChart();
                }
            },

            /**
             * Listener for the historical analysis change event.
             * @param {Viz.BusinessEntity.ChartHistoricalAnalysis} entity
             * @private
             */
            onChartHistoricalAnalysisChange                 : function(entity) {
                if (this.entity.KeyChart == entity.KeyChart) {
                    this.reloadChart();
                }
            },
            /**
             * Listener for the chart Algorithm change event.
             * @param {Viz.BusinessEntity.ChartAlgorithm} entity
             * @private
             */
            onChartAlgorithmChange                          : function(entity) {
                if (this.entity.KeyChart == entity.KeyChart) {
                    this.onRefresh(true);
                }
            },

            /**
             * Listener for the drill down change event.
             * @param {Viz.BusinessEntity.ChartDrillDown} entity
             * @private
             */
            onChartDrillDownChangeChange                    : function(entity) {
                if (this.entity.KeyChart == entity.KeyChart) {
                    this.reloadChart();
                }
            },

            /**
             * Listener for the calendar view change event.
             * @private
             */
            onChartCalendarViewSelectionChange              : function(entity) {
                if (this.entity.KeyChart == entity.KeyChart) {
                    this.onRefresh(true);
                }
            },

            /**
             * Listener for the pset attribute historical change event.
             * @private
             */
            onChartPsetAttributeHistoricalChange            : function(entity) {
                if (this.entity.KeyChart == entity.KeyChart) {
                    this.onRefresh(true);
                }
            },

            /**
             * Listener for the pset attribute historical change event.
             * @private
             */
            onDataSeriePsetRatioChange                      : function(entity) {
                if (this.entity.KeyChart == entity.KeyChart) {
                    this.onRefresh(true);
                }
            },

            /**
             * Listener for the filter spatial pset change event.
             * @private
             */
            onFilterSpatialPsetChange                       : function(entity) {
                if (this.entity.KeyChart == entity.KeyChart) {
                    this.onRefresh(true);
                }
            },
            /**
             * Listener for the dynamicdisplay change event.
             * @param {Viz.BusinessEntity.DynamicDisplay} entity
             * @private
             */
            onDynamicDisplayChange                          : function(entity) {
                if (this.entity.UseDynamicDisplay && this.entity.KeyDynamicDisplay == entity.KeyDynamicDisplay) {
                    this.reloadChart();
                }
            },

            /**
             * Listener for the afterentityupdate event.
             * @param {Viz.BusinessEntity.Chart} entity
             */
            onUpdateEntity                                  : function() {
                this.makeToolbarBusy();
                Viz.Services.EnergyWCF.Chart_ChangeView({
                            item    : this.entity,
                            success : function(response) {
                                this.clearToolbarStatus();
                                if (!response.success) {
                                    if (response.msg || response.message) {
                                        Ext.Msg.show({
                                            width: 400,
                                            title: $lang("error_ajax"),
                                            msg: response.msg || response.message,
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                    }
                                }
                                else
                                    Viz.util.MessageBusMgr.publish('ChartChange', response.data);
                            },
                            scope   : this
                        })
            },

            /**
             * Toggle the Chart Legend
             */
            onChartToggleLegend                             : function() {
                this.entity.DisplayLegendVisible = !this.entity.DisplayLegendVisible;
                this.onUpdateEntity(this.entity);
            },

            /**
             * Toggle the Chart Values
             */
            onChartToggleValues                             : function() {
                this.entity.DisplayValues = !this.entity.DisplayValues;
                this.onUpdateEntity(this.entity);
            },

            /**
             * Toggle the Chart Stack/Unstack of series
             */
            onChartToggleStack                              : function() {
                this.entity.DisplayStackSeries = !this.entity.DisplayStackSeries;
                this.onUpdateEntity(this.entity);
            },

            /**
             * Toggle the Chart Use DynamicDisplay
             */
            onChartToggleDynamicDisplay                     : function() {
                this.entity.UseDynamicDisplay = !this.entity.UseDynamicDisplay;
                this.onUpdateEntity(this.entity);
            },

            /**
             * Toggle the Chart Use DynamicDisplay
             */
            onChartSetDynamicDisplay                        : function(KeyDynamicDisplay) {
                this.entity.UseDynamicDisplay = true;
                this.entity.KeyDynamicDisplay = KeyDynamicDisplay;
                this.onUpdateEntity(this.entity);
            },

            /**
             * Toggle the Chart Drill Down
             */
            onChartToggleDrillDown                          : function(menu) {
                this.entity.DrillDownEnabled = !this.entity.DrillDownEnabled;

                if (this.entity.DrillDownEnabled)
                    menu.setText($lang("msg_chartdrilldown_disable"));
                else
                    menu.setText($lang("msg_chartdrilldown_enable"));

                var bbar = this.getBottomToolbar();
                if (bbar) {
                    if (this.entity.DrillDownEnabled)
                        bbar.disable();
                    else
                        bbar.enable();
                }
                this.onUpdateEntity(this.entity);
            },

            /**
             * Reset the Chart Drill Down.
             */
            onChartDrillDownReset                           : function() {
                Viz.grid.Chart.onChartDrillDownReset(this.entity.KeyChart);
            },

            /**
             * Toggle the display of startdate enddate
             */
            onChartToggleDateDisplay                        : function() {
                this.entity.DisplayStartEndDate = !this.entity.DisplayStartEndDate;
                this.onUpdateEntity(this.entity);
            },

            /**
             * Update the Chart Type
             */
            onChartUpdateType                               : function(type) {
                this.entity.ChartType = type;
                this.onUpdateEntity(this.entity);
            },

            /**
             * Update the Chart DataSerie Type
             */
            onChartUpdateDataSerieType                      : function(serietype, charttype) {
                this.entity.ChartType = charttype;// Vizelia.FOL.BusinessEntities.ChartType.Combo;
                this.entity.DataSerieType = serietype;
                this.onUpdateEntity(this.entity);
            },

            /**
             * Update the Chart Gauge Type
             */
            onChartUpdateGaugeType                          : function(type) {
                this.entity.ChartType = Vizelia.FOL.BusinessEntities.ChartType.Gauge;
                this.entity.GaugeType = type;
                this.onUpdateEntity(this.entity);
            },

            /*
             * Handler for the Chart StartDate menu.
             */
            onChartUpdateStartDate                          : function(menu, value) {
                this.markStartEndDatesMenuItemToBold(true);
                this.entity.StartDate = value;
                this.entity.DynamicTimeScaleEnabled = false;
                this.onUpdateEntity(this.entity);
            },
            /*
             * Handler for the Chart EndDate menu.
             */
            onChartUpdateEndDate                            : function(menu, value) {
                this.markStartEndDatesMenuItemToBold(true);
                this.entity.EndDate = value;
                this.entity.DynamicTimeScaleEnabled = false;
                this.onUpdateEntity(this.entity);
            },

            /*
             * Handler for the Chart DynamicTimeScale menu
             */
            onChartUpdateDynamicTimeScale                   : function(frequency, interval, iscalendar, completeinterval, timeinterval, endtonow) {
                this.markStartEndDatesMenuItemToBold(false);
                this.entity.DynamicTimeScaleEnabled = true;
                this.entity.DynamicTimeScaleInterval = interval;
                this.entity.DynamicTimeScaleFrequency = frequency;
                this.entity.DynamicTimeScaleCalendar = iscalendar;
                if (!(this.entity.IsKPI == false && this.entity.KPIDisableTimeIntervalSlider == true))
                    this.entity.TimeInterval = timeinterval;
                this.entity.DynamicTimeScaleCompleteInterval = Ext.isDefined(completeinterval) ? completeinterval : false;
                this.entity.DynamicTimeScaleCalendarEndToNow = Ext.isDefined(endtonow) ? endtonow : false;

                this.onUpdateEntity(this.entity);
            },

            /**
             * Private function to handle dynamic change in Time Range menu.
             * @param {} bold
             */
            markStartEndDatesMenuItemToBold                 : function(bold) {
                if (bold) {
                    var menuItem = Ext.getCmp(this._startDateMenuItemId);

                    if (menuItem)
                        menuItem.parentMenu.items.each(function(item) {
                                    if (item.setText && item.text)
                                        item.setText(item.text.replace('<b>', '').replace('<b/>'));
                                }, this);

                    if (menuItem)
                        menuItem.setText(this.textToBold($lang('msg_chart_startdate')));

                    menuItem = Ext.getCmp(this._endDateMenuItemId);
                    if (menuItem)
                        menuItem.setText(this.textToBold($lang('msg_chart_enddate')));

                }
                else {
                    var menuItem = Ext.getCmp(this._startDateMenuItemId);
                    if (menuItem)
                        menuItem.setText($lang('msg_chart_startdate'));
                    menuItem = Ext.getCmp(this._endDateMenuItemId);
                    if (menuItem)
                        menuItem.setText($lang('msg_chart_enddate'));
                }
            },

            markDynamicTimeScaleMenuItemToRegularText       : function(menuitem) {
                menuitem.parentMenu.items.each(function(item) {
                            if (item.setText && item.text)
                                item.setText(item.text.replace('<b>', '').replace('<b/>'));
                        }, this);

                menuitem.setText(this.textToBold(menuitem.text));
            },

            /*
             * Handler for the Chart TimeInterval menu
             */
            onChartUpdateTimeInterval                       : function(value) {
                this.entity.TimeInterval = value;
                this.onUpdateEntity(this.entity);
            },

            /*
             * Handler for the Chart TimeInterval menu
             */
            onChartUpdateLocalisation                       : function(value, data) {
                if (data.Id.startsWith('BySite')) {
                    var siteLevel = Viz.util.Location.getSiteLevel(data);
                    this.entity.Localisation = Vizelia.FOL.BusinessEntities.ChartLocalisation.BySite;
                    this.entity.LocalisationSiteLevel = siteLevel;
                }
                else {
                    this.entity.Localisation = value;
                }
                this.onUpdateEntity(this.entity);
            },

            /*
             * Handler for the Chart Legend sort menu
             */
            onChartUpdateDisplayLegendSort                  : function(value) {
                this.entity.DisplayLegendSort = value;
                this.onUpdateEntity(this.entity);
            },

            /*
             * Handler for the Chart ShaddingEffect menu
             */
            onChartUpdateDisplayShaddingEffect              : function(value) {
                this.entity.DisplayShaddingEffect = value;
                this.onUpdateEntity(this.entity);
            },

            onChartUpdatePalette                            : function(value) {
                this.entity.KeyPalette = value;
                this.onUpdateEntity(this.entity);
            },

            onChartUpdateModernUI                           : function(value) {
                this.makeToolbarBusy();
                Viz.Services.EnergyWCF.Chart_ApplyModernUIColor({
                            Key     : this.entity.KeyChart,
                            color   : value,
                            success : function(response) {
                                this.entity = response;
                                this.clearToolbarStatus();
                                Viz.util.MessageBusMgr.publish('ChartChange', this.entity);
                            },
                            scope   : this
                        })
            },

            /*
             * Handler for the Chart LockFilterSpatial menu
             */
            onChartLockFilterSpatial                        : function(menuItem) {
                this.hideCallingMenu(menuItem);
                this.entity.LockFilterSpatial = !this.entity.LockFilterSpatial;
                this.onUpdateEntity(this.entity);
            },

            /**
             * Handler for the Chart CalculatedSerie menu
             */
            onCalculatedSerie                               : function(menuItem) {
                this.hideCallingMenu(menuItem);

                var win = new Ext.Window({
                            layout  : 'fit',
                            title   : $lang('msg_chart_calculatedseries'),
                            iconCls : 'viz-icon-small-calculatedserie',
                            width   : 600,
                            height  : 350,
                            items   : {
                                xtype    : 'vizGridCalculatedSerie',
                                KeyChart : this.entity.KeyChart
                            }
                        });
                win.show();
            },

            /**
             * Handler for the Chart CalculatedSerie menu
             */
            onStatisticalSerie                              : function(menuItem) {
                this.hideCallingMenu(menuItem);

                var win = new Ext.Window({
                            layout  : 'fit',
                            title   : $lang('msg_chart_statisticalseries'),
                            iconCls : 'viz-icon-small-statisticalserie',
                            width   : 600,
                            height  : 350,
                            items   : {
                                xtype    : 'vizGridStatisticalSerie',
                                KeyChart : this.entity.KeyChart
                            }
                        });
                win.show();
            },

            /*
             * Handler for the Chart HistoricalAnalysis menu
             */
            onChartHistoricalAnalysis                       : function(menuItem) {
                this.hideCallingMenu(menuItem);

                var win = new Ext.Window({
                            layout  : 'fit',
                            title   : $lang('msg_charthistoricalanalysis'),
                            iconCls : 'viz-icon-small-chart-historicalanalysis',
                            width   : 800,
                            height  : 350,
                            items   : {
                                xtype    : 'vizGridChartHistoricalAnalysis',
                                KeyChart : this.entity.KeyChart
                            }
                        });
                win.show();
            },

            /**
             * Replace empty string by null
             * @param {Object} obj
             */
            replaceEmptyStringByNull                        : function(obj) {
                for (key in obj) {
                    if (obj[key] === "") {
                        obj[key] = null;
                    }
                }
            },

            /**
             * Listener for the afterentityupdate event.
             * @param {Viz.BusinessEntity.DataSerie} dataserie
             */
            onUpdateDataSerie                               : function(dataserie) {
                this.replaceEmptyStringByNull(dataserie);
                Viz.Services.EnergyWCF.DataSerie_FormUpdate({
                            item    : dataserie,
                            success : function() {
                                Viz.util.MessageBusMgr.publish('DataSerieChange', dataserie);
                            },
                            scope   : this
                        })
            },

            /**
             * Handler for the DataSerie Toggle Visibility menu.
             * @param {Viz.BusinessEntity.DataSerie} dataserie
             */
            onDataSerieToggleVisibility                     : function(dataserie) {
                dataserie.Hidden = !dataserie.Hidden
                this.onUpdateDataSerie(dataserie)
            },

            /**
             * Handler for the DataSerie Delete menu.
             * @param {Viz.BusinessEntity.DataSerie} dataserie
             */
            onDataSerieDelete                               : function(dataserie) {
                Viz.Services.EnergyWCF.DataSerie_Delete({
                            item    : dataserie,
                            success : function() {
                                Viz.util.MessageBusMgr.publish('DataSerieChange', dataserie);
                            },
                            scope   : this
                        })
            },

            /*
             * Handler for the Export to excel menu
             */
            onExportToExcel                                 : function() {
                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                            pollingInterval : 2000,
                            serviceStart    : Viz.Services.EnergyWCF.Chart_GetStreamExcelBegin,
                            serviceParams   : {
                                Key : this.entity.KeyChart
                            },
                            isFileResult    : true,
                            showProgress    : true
                        });
                submitter.start();
            },
            /*
             * Handler for the Export to PDF menu
             */
            onExportToPDF                                   : function() {
                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                            pollingInterval : 2000,
                            serviceStart    : Viz.Services.EnergyWCF.Chart_GetStreamPdfBegin,
                            serviceParams   : {
                                Key : this.entity.KeyChart
                            },
                            isFileResult    : true,
                            showProgress    : true
                        });
                submitter.start();
            },

            /**
             * Display the Chart FilterSpatial grid as readonly.
             */
            onChartFilterSpatial                            : function(menuItem) {
                this.hideCallingMenu(menuItem);
                var win = new Ext.Window({
                            layout  : 'fit',
                            title   : $lang('msg_chart_filterspatial'),
                            iconCls : 'viz-icon-small-site',
                            width   : 400,
                            height  : 350,
                            items   : {
                                xtype                       : 'vizGridFilterSpatial',
                                store                       : new Viz.store.ChartFilterSpatial({
                                            KeyChart : this.entity.KeyChart
                                        }),
                                border                      : false,
                                loadMask                    : false,
                                readonly                    : true,
                                disableCheckboxForAncestors : true
                            }
                        });
                win.show();
            },

            writeDragAndDropEventToGoogleAnalytics: function (dragDrop) {
                if (dragDrop && dragDrop.tree && dragDrop.tree.title)
                    Viz.writeEventToGoogleAnalytics('Left Tree', 'Drag And Drop', dragDrop.tree.title);
            },

        });
Ext.reg('vizChartViewer', Viz.chart.Viewer);

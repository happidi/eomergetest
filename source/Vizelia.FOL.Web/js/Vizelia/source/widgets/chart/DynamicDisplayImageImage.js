﻿Ext.namespace('Viz.chart');

/**
 * @class Viz.chart.DynamicDisplayImageImage
 * @extends Viz.viewer.Image
 */
Viz.chart.DynamicDisplayImageImage = Ext.extend(Viz.viewer.Image, {

            /**
             * @cfg {Viz.BusinessEntity.DynamicDisplayImage} entity The dynamicdisplayImage entity.
             */
            entity      : null,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {

                config = config || {};
                var defaultConfig = {
                    style : 'padding:0px;'
                };
                var forcedConfig = {
                    stretchImage    : true,
                    serviceInterface : 'Energy.svc',
                    serviceStream   : 'DynamicDisplayImage_GetStreamImage',
                    entityKey       : 'KeyDynamicDisplayImage',
                    reloadEventName : 'DynamicDisplayImageChange'
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.chart.DynamicDisplayImageImage.superclass.constructor.call(this, config);

            }

        });

Ext.reg('vizChartDynamicDisplayImageImage', Viz.chart.DynamicDisplayImageImage);

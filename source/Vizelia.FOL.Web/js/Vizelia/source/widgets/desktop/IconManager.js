﻿/**
 * author = Stanislav Golovenko published = 2009.10.18 version = 0.4 extversion = 3.x demo = http://code.google.com/p/extdim/w/list forum = http://www.extjs.com/forum/showthread.php?t=83224 documentation = http://code.google.com/p/extdim/w/list
 */

Ext.ns('Ext.ux.Desktop');

Ext.ux.Desktop.Icon = Ext.extend(Ext.BoxComponent, {
            iconX                            : 0,
            iconY                            : 0,
            autoEl                           : {
                tag : 'dt'
            },
            IconsManager                     : null,
            launchModuleId                   : null,
            style                            : 'position: absolute; z-index: 4000;',
            template                         : '<a><div ext:qtip="{[Ext.util.Format.htmlEncode(values.text)]}" class="x-icon-desktop-shortcut ' + ((Ext.isIE7 || Ext.isIE8) ? 'ie-legacy-css-' : '') + '{[Ext.util.Format.htmlEncode(values.iconCls)]}" ></div>' + '<div class="x-icon-desktop-shortcut-text" >{[Ext.util.Format.htmlEncode(values.text)]}</div></a>',
            // <tpl if="Ext.isEmpty(values.url)==false"><img src="{[values.url]}" width="100%" height="100%" /></tpl>
            menu                             : false,
            dd                               : false,
            elementDeletingFlag              : false,
            DDGroup                          : 'iconsDDgroup',
            zindex                           : 4000,
            initComponent                    : function() {
                this.menu = this.getConfigMenu();
                this.on('afterrender', function(c) {

                            this.initIcon();
                            this.initDD();
                            this.initSelectable();
                            this.initDesktopConfig();
                            if (this.menu)
                                this.initBaseMenu();
                        }.createDelegate(this));
                Ext.ux.Desktop.Icon.superclass.initComponent.call(this);
                this.addEvents({
                            "iconMove"         : true,
                            "iconSelect"       : true,
                            "iconDeselect"     : true,
                            "iconEnter"        : true,
                            "iconDroppedOn"    : true,
                            "iconBeforeDelete" : true,
                            "iconDblClick"     : true
                        })

            },

            initDesktopConfig                : function() {
                if (this.launchModuleId && this.IconsManager.desktop) {
                    this.getEl().on('dblclick', function() {

                                this.IconsManager.desktop.getModule(this.launchModuleId).createWindow();
                            }, this);
                }
            },
            /**
             * Returns the Icon Menu config
             * @return {}
             */
            getConfigMenu                    : function() {
                return {
                    defaults : {
                        hideOnClick : true
                    },
                    items    : [{
                                text    : $lang('msg_portalwindow_update'),
                                iconCls : 'viz-icon-small-update',
                                hidden  : !$authorized('@@@@ Portal - Full Control'),
                                handler : this.onPortalWindowUpdate,
                                scope   : this
                            }, {
                                text    : $lang('msg_portalwindow_copy'),
                                iconCls : 'viz-icon-small-copy',
                                hidden  : !$authorized('@@@@ Portal - Full Control'),
                                handler : this.onPortalWindowCopy,
                                scope   : this
                            }, {
                                text    : $lang('msg_portalwindow_delete'),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ Portal - Full Control'),
                                handler : this.onPortalWindowDelete,
                                scope   : this
                            }, {
                                text    : $lang('msg_portaltab_apply_spatialfilter'),
                                iconCls : 'viz-icon-small-apply',
                                hidden  : !$authorized('@@@@ Apply Spatial Filter'),
                                handler : this.onPortalWindowApplySpatialFilter,
                                scope   : this
                            }]
                };
            },

            initIcon                         : function() {
                this.getEl().setStyle('z-index', this.zindex);
                var template = new Ext.XTemplate(this.template);

                template.overwrite(this.getEl(), this);

                var coords = this.IconsManager.repairXY({
                            iconX : this.iconX,
                            iconY : this.iconY
                        });
                this.iconX = coords.iconX;
                this.iconY = coords.iconY;
                this.IconsManager.placeIcon(this);
            },

            initDD                           : function() {
                this.dd = new Ext.dd.DD(this.getEl(), this.DDGroup, {
                            isTarget : true
                        });
                Ext.apply(this.dd, {
                            startDrag   : function(x, y) {

                                var icon = Ext.fly(this.getEl());
                                var id = this.id;

                                this.coordsBeforeDrag = icon.getXY();

                                this.IconsManager.isDraggingElement = true;
                                if (this.IconsManager.selectedIconsArray.indexOf(this.id) == -1) {

                                    this.IconsManager.deselectAllIcons();
                                    this.IconsManager.selectIcon(id);
                                }

                                this.IconsManager.IconsGrid[this.iconY][this.iconX] = false;
                            }.createDelegate(this),
                            endDrag     : function(e) {

                                e.stopEvent();

                                if (!this.elementDeletingFlag) {

                                    var el = Ext.get(this.getEl());

                                    var icon = this;

                                    var elForPlace = this.IconsManager.repairXY({
                                                iconX : el.getStyle('left') + 'px',
                                                iconY : el.getStyle('top') + 'px'
                                            });
                                    icon.iconX = elForPlace.iconX;
                                    icon.iconY = elForPlace.iconY;

                                    this.IconsManager.placeIcon(icon);

                                    this.fireEvent('iconMove', icon);
                                    this.IconsManager.fireEvent('iconMove', this.IconsManager, icon);

                                    if (this.IconsManager.selectedIconsArray.length > 1) {

                                        var newCoords = el.getXY();
                                        var oldCoords = this.coordsBeforeDrag;
                                        this.IconsManager.moveMoreThenOneSelectedIcon(icon.id, {
                                                    iconX : newCoords[0] - oldCoords[0],
                                                    iconY : newCoords[1] - oldCoords[1]
                                                });
                                    }

                                }
                            }.createDelegate(this),
                            onDragEnter : function(evtObj, targetElId) {
                                // Colorize the drag target if the drag node's parent is not the same as the drop target

                                if (this.id != targetElId) {
                                    this.IconsManager.iconsArray[targetElId].fireEvent('iconEnter', this.IconsManager.iconsArray[targetElId], this);
                                }

                            }.createDelegate(this),
                            onDragDrop  : function(evtObj, targetElId) {

                                if (this.id != targetElId) {
                                    this.IconsManager.iconsArray[targetElId].fireEvent('icondroppedon', this.IconsManager.iconsArray[targetElId], this);
                                }
                            }.createDelegate(this)
                        });

                var rentedDDTarget = new Ext.dd.DDTarget(this.getEl(), this.DDGroup);
            },

            initSelectable                   : function() {
                this.getEl().on({
                            click    : function(e) {
                                e.stopEvent();
                                if (!e.ctrlKey) {

                                    if (Ext.isIE) {
                                        e.browserEvent.keyCode = 0;
                                    }
                                    if (!this.IconsManager.isDraggingElement)
                                        this.IconsManager.deselectAllIcons();
                                }
                                if (!this.IconsManager.isDraggingElement) {

                                    if (e.ctrlKey) {
                                        var icon = this;
                                        if (icon.selected)
                                            this.IconsManager.deselectIcon(this.id);
                                        else
                                            this.IconsManager.selectIcon(this.id);
                                    }
                                    else {
                                        this.IconsManager.selectIcon(this.id);
                                    }
                                }
                                this.IconsManager.isDraggingElement = false;
                            },
                            dblclick : function(e) {
                                e.stopEvent();
                                if (this.selected)
                                    this.IconsManager.deselectIcon(this.id);
                                this.fireEvent('iconDblClick', this);
                                this.IconsManager.fireEvent('iconDblClick', this.IconsManager, this);
                            },

                            scope    : this
                        });
            },

            initBaseMenu                     : function() {
                this.getEl().on('contextmenu', function(e) {
                    if ($authorized('@@@@ Portal - Full Control') || $authorized('@@@@ Apply Spatial Filter')) {
                        e.stopEvent();
                        this.menu = (this.menu instanceof Ext.menu.Menu) ? this.menu : new Ext.menu.Menu(this.menu);
                        if (!this.menu.el) {
                            this.menu.render();
                        }
                        var xy = e.getXY();

                        this.menu.showAt(xy);
                    }
                    else
                    {
                        return;
                    }
                }, this);
            },

            moveIconTo                       : function(XY, animate) {
                var el = this.getEl();
                var oldCoords = el.getXY();
                var newCoords = this.IconsManager.repairXY({
                            iconX : oldCoords[0] + parseInt(XY.iconX) + 'px',
                            iconY : oldCoords[1] + parseInt(XY.iconY) + 'px'
                        });
                this.IconsManager.IconsGrid[this.iconY][this.iconX] = false;
                el.moveTo(oldCoords[0] + XY.iconX, oldCoords[1] + XY.iconY, {
                            callback : function() {
                                this.iconX = newCoords.iconX;
                                this.iconY = newCoords.iconY;
                                this.IconsManager.placeIcon(this);
                                this.fireEvent('iconMove', this);
                                this.IconsManager.fireEvent('iconMove', this.IconsManager, this);
                            }.createDelegate(this)
                        });
            },

            selectIcon                       : function() {
                this.selected = true;
                var el = this.getEl();
                var zindex = el.getStyle('z-index');
                el.setStyle('z-index', parseInt(zindex) + 5);
                if (this.IconsManager.iconsSelectionElement != '') {
                    el = el.child(this.IconsManager.iconsSelectionElement);
                }

                if (!el.hasClass('ui-selected'))
                    el.addClass('ui-selected');
                // new
                this.fireEvent('iconSelect', this);
                this.IconsManager.fireEvent('iconSelect', this.IconsManager, this);
            },

            deselectIcon                     : function() {
                this.selected = false;
                var el = this.getEl();
                var zindex = el.getStyle('z-index');
                el.setStyle('z-index', parseInt(zindex) - 5);
                if (this.IconsManager.iconsSelectionElement != '')
                    el = el.child(this.IconsManager.iconsSelectionElement);
                el.removeClass('ui-selected');
                // new
                this.fireEvent('iconDeselect', this);
                this.IconsManager.fireEvent('iconDeselect', this.IconsManager, this);
            },

            deleteIcon: function () {
                this.fireEvent('iconBeforeDelete', this);
                this.IconsManager.fireEvent('iconBeforeDelete', this.IconsManager, this);
                this.elementDeletingFlag = true;
                this.IconsManager.IconsGrid[this.iconY][this.iconX] = false;
                var indexToDelete = this.IconsManager.selectedIconsArray.indexOf(this.id);
                if (indexToDelete != -1)
                    this.IconsManager.selectedIconsArray.splice(indexToDelete, 1);
                delete this.IconsManager.iconsArray[this.id];
                this.destroy();
                this.dd.destroy();

                return true;
            },
            /**
             * Handler for the PortalWindowCopy Menu.
             */
            onPortalWindowCopy               : function(menu, ev) {
                this.IconsManager.onPortalWindowCopy(this);
            },
            /**
             * Handler for the PortalWindowDelete Menu.
             */
            onPortalWindowDelete             : function(menu, ev) {
                this.IconsManager.onPortalWindowDelete(this);
            },

            onPortalWindowUpdate             : function(menu, ev) {
                this.IconsManager.onPortalWindowUpdate(this);
            },

            onPortalWindowApplySpatialFilter : function(menu, ev) {
                this.IconsManager.onPortalWindowApplySpatialFilter(this);
            }
        });
Ext.reg('desktopicon', Ext.ux.Desktop.Icon);

/**
 * Creates new IconsManager
 * @constructor
 * @param {Object} config Configuration object
 */
Ext.ux.Desktop.IconsManager = Ext.extend(Ext.util.Observable, {
            /**
             * @cfg {Object} desktop Desktop variable in your aplication
             */
            desktop                               : null,
            /**
             * @cfg {String} iconsArea Parent Element of all icon
             */
            iconsArea                             : 'dl',
            /**
             * @cfg {String} desktopEl Selector of desktop element
             */
            desktopEl                             : '#x-desktop',
            /**
             * @cfg {String} iconsSelectionElement Selector for icon selection view
             */
            iconsSelectionElement                 : '',
            /**
             * @cfg {String} iconsPostFix part of icon id that should be cut for detecting module thet should be launch
             */
            iconsPostFix                          : '-shortcut',
            /**
             * @cfg {Object} basicStyles styles for icons sectors
             */
            basicStyles                           : {
                width  : 150,
                height : 160
            },
            /**
             * @cfg {Bool} allowDesktopContextMenu icon template that should be use by default
             */
            firstLoad                             : true,
            constructor                           : function(config) {
                Ext.apply(this, config);
                if (!Ext.isArray(this.items))
                    this.items = [];
                this.addEvents({
                            // returns desktopIcon manager, icon record
                            "iconSelect"       : true,
                            "iconDeselect"     : true,
                            "iconMove"         : true,
                            "iconAfterAdd"     : true,
                            "iconBeforeDelete" : true,
                            "iconDblClick"     : true
                        });
                this.on({
                            iconDblClick : {
                                scope : this,
                                fn    : this.iconDblClick
                            },
                            iconSelect   : {
                                scope : this,
                                fn    : this.iconSelect
                            },
                            iconMove     : {
                                scope  : this,
                                fn     : this.onIconMove,
                                buffer : 1000
                            }
                        });

                this.initDesktopIconsConfiguration();
                this.initIcons();

                this.initSelectable();
                this.disableIconsManager();

                Ext.EventManager.onWindowResize(function() {
                            var desktopIconsCmp = Viz.App.Global.ViewPort.getDesktopIconsContainer();
                            if (desktopIconsCmp.activated !== false) {
                                Viz.App.Global.ViewPort.updateApplicationToolbarPortalWindows.defer(500, Viz.App.Global.ViewPort);
                            }
                        }, this);
                Ext.ux.Desktop.IconsManager.superclass.constructor.call(this);
            },

            onDestroy                             : function() {
                Ext.ux.Desktop.Icon.superclass.onDestroy.apply(this, arguments);
            },

            initIcons                             : function() {
                var shortcuts = Ext.get(this.desktopEl.replace('#', ''));
                shortcuts.removeAllListeners();
                this.isDraggingElement = false;
                this.selectedIconsArray = new Array();
            },

            initSelectable                        : function() {
                var desktopEl = Ext.get(this.desktopEl.replace('#', ''));
                var dragRegion = new Ext.lib.Region(0, 0, 0, 0);
                var bodyRegion = desktopEl.getRegion();
                var proxy, link = this;
                this.isDraggingElement = false;
                this.selectedIconsArray = new Array();

                this.tracker = new Ext.dd.DragTracker({
                            onStart : function() {
                                console.log('onStart');
                                if (!proxy) {
                                    proxy = desktopEl.createChild({
                                                cls : 'x-view-selector'
                                            });
                                }
                                else {
                                    proxy.setDisplayed('block');
                                }
                                link.deselectAllIcons();
                            },

                            onDrag  : function() {
                                var startXY = this.tracker.startXY;
                                var xy = this.tracker.getXY();

                                var x = Math.min(startXY[0], xy[0]);
                                var y = Math.min(startXY[1], xy[1]);
                                var w = Math.abs(startXY[0] - xy[0]);
                                var h = Math.abs(startXY[1] - xy[1]);

                                dragRegion.left = x;
                                dragRegion.top = y;
                                dragRegion.right = x + w;
                                dragRegion.bottom = y + h;

                                dragRegion.constrainTo(bodyRegion);
                                proxy.setRegion(dragRegion);

                                for (var i in this.iconsArray) {
                                    var r = this.iconsArray[i].region;
                                    var sel = dragRegion.intersect(r);
                                    if (sel && !r.selected) {
                                        r.selected = true;
                                        this.selectIcon(this.iconsArray[i].id);
                                    }
                                    else if (!sel && r.selected) {
                                        r.selected = false;
                                        this.deselectIcon(this.iconsArray[i].id);
                                    }
                                }
                            }.createDelegate(this),

                            onEnd   : function() {

                                if (proxy) {
                                    proxy.setDisplayed(false);
                                }
                            }
                        });
                this.tracker.initEl(Ext.get(this.desktopEl.replace('#', '')));
                this.tracker.on('mousedown', function(c, e) {
                            return e.target.id == desktopEl.id;
                        })
            },

            disableIconsManager                   : function() {
                this.disabled = true;
                this.deselectAllIcons();
                for (var i in this.iconsArray) {
                    this.iconsArray[i].dd.lock();
                }
            },

            enableIconsManager                    : function() {
                this.disabled = false;
                this.deselectAllIcons();
                for (var i in this.iconsArray) {
                    this.iconsArray[i].dd.unlock();
                }
            },

            repairXY                              : function(el) {
                var ret = {};

                el.iconX = (el.iconX <= 0) ? 0 : el.iconX;
                el.iconY = (el.iconY <= 0) ? 0 : el.iconY;

                if (typeof el.iconX == 'string')
                    ret.iconX = Math.round(parseInt(el.iconX) / this.basicStyles.width);
                else
                    ret.iconX = el.iconX;

                if (typeof el.iconY == 'string')
                    ret.iconY = Math.round(parseInt(el.iconY) / this.basicStyles.height);
                else
                    ret.iconY = el.iconY;

                return ret;
            },

            initDesktopIconsConfiguration         : function() {
                this.initGlobalGrid();
                this.IconsGrid = new Array();
                for (var i = 0; i <= this.globalGrid.y; i++) {
                    this.IconsGrid[i] = new Array();
                    for (var h = 0; h <= this.globalGrid.x; h++)
                        this.IconsGrid[i][h] = false;
                }

                if (!this.iconsArray) {
                    this.iconsArray = new Object();
                    for (var i = 0; i < this.items.length; i++)
                        this.iconsArray[this.items[i].id] = this.items[i];
                }
                this.initManualIconsConfiguration(this.iconsArray);
            },

            initGlobalGrid                        : function() {
                var DesktopWidth = Ext.fly(this.desktopEl.replace('#', '')).getWidth(); // 1920;//
                var DesktopHeight = Ext.fly(this.desktopEl.replace('#', '')).getHeight(); // 1080;//
                this.globalGrid = {};
                this.globalGrid.x = Math.floor(DesktopWidth / this.basicStyles.width);
                this.globalGrid.y = Math.floor(DesktopHeight / this.basicStyles.height);
            },

            initManualIconsConfiguration          : function(icons) {
                if (this.firstLoad) {
                    for (var i in icons) {
                        var item = icons[i];
                        this.addIcon(item);
                    }
                }
                else {
                    for (var i in icons) {
                        this.placeIcon(icons[i]);
                    }
                }
                this.firstLoad = false;
            },

            placeIcon                             : function(config) {
                var desktopBox = Ext.fly(this.desktopEl.replace('#', '')).getBox();
                if (config.iconX > this.globalGrid.x)
                    return config.iconX = 0; // this.globalGrid.x - 1;
                if (config.iconY > this.globalGrid.y)
                    return config.iconY = 0; // this.globalGrid.y - 1;

                if (config.iconX < 0)
                    config.iconX = 0;
                if (config.iconY < 0)
                    config.iconY = 0;

                // console.debug(this.IconsGrid);
                if (!this.IconsGrid[config.iconY][config.iconX]) {
                    // IE 7 & 8 specific workaround since IE8 does not support background images that resize to accommodate container.
                    // See http://stackoverflow.com/questions/4885145/ie-8-background-size-fix and cry too.
                    if ((Ext.isIE7 || Ext.isIE8) && config.iconCls) {
                        var ruleName = '.' + config.iconCls;
                        var cssRule = Ext.util.CSS.getRule(ruleName);

                        var ie8ruleName = '.ie-legacy-css-' + config.iconCls;
                        var ie8rule = Ext.util.CSS.getRule(ie8ruleName);

                        if (cssRule && !ie8rule) {
                            // Then we need to create a new CSS rule for IE8 based on the regular CSS rule.
                            var imgStyle = cssRule.style.backgroundImage;

                            if (imgStyle) {
                                var url = imgStyle.slice(4, -1); // Remove the 'url(' and ')' in the start end end of value.
                                var ruleContent = ' { filter : progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'' + url + '\',sizingMethod=\'scale\'); -ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'' + url + '\',sizingMethod=\'scale\');"}';
                                Ext.util.CSS.createStyleSheet(ie8ruleName + ruleContent, ie8ruleName);
                                Ext.util.CSS.refreshCache();
                            }
                        }
                    }
                    var el = Ext.fly(config.id);
                    this.IconsGrid[config.iconY][config.iconX] = true;

                    // el.moveTo(config.iconX * this.basicStyles.width, config.iconY * this.basicStyles.height);
                    var xPos = desktopBox.x + config.iconX * this.basicStyles.width;
                    var yPos = desktopBox.y + config.iconY * this.basicStyles.height;
                    el.moveTo(xPos, yPos);
                    this.iconsArray[config.id].region = el.getRegion();
                    this.iconsArray[config.id].iconX = config.iconX;
                    this.iconsArray[config.id].iconY = config.iconY;

                    if (config.data.DesktopShortcutIconX != config.iconX || config.data.DesktopShortcutIconY != config.iconY) {
                        this.onIconMove(this, config);
                    }

                }
                else {
                    config.iconX++;
                    if (config.iconX >= this.globalGrid.x) {
                        config.iconX = 0;
                        config.iconY = config.iconY + 1;
                    }
                    if (config.iconY < this.globalGrid.y)
                        this.placeIcon(config);
                }
            },

            selectIcon                            : function(id) {
                this.iconsArray[id].selectIcon();
                this.selectedIconsArray.push(id);
            },

            deselectIcon                          : function(id) {
                this.iconsArray[id].deselectIcon();
                this.selectedIconsArray.splice(this.selectedIconsArray.indexOf(id), 1);
            },

            deselectAllIcons                      : function() {
                var icons = Ext.apply([], this.getSelectedIconsIds());
                for (var i = 0; i < icons.length; i++) {
                    this.deselectIcon(icons[i]);
                }
            },

            getSelectedIcons                      : function() {
                var retArray = [];
                var iconsIds = this.getSelectedIconsIds();
                for (var i = 0; i < iconsIds.length; i++)
                    retArray.push(this.iconsArray[iconsIds[i]]);
                return retArray;
            },

            getSelectedIconsIds                   : function() {
                return this.selectedIconsArray;
            },

            moveMoreThenOneSelectedIcon           : function(currentId, XY) {
                var selectedIcons = this.selectedIconsArray;
                for (var i = 0; i < selectedIcons.length; i++) {
                    if (currentId != selectedIcons[i])
                        this.moveIconTo(selectedIcons[i], XY, true)
                }
            },

            moveIconTo                            : function(id, XY, animate) {
                this.iconsArray[id].moveIconTo(XY, animate);
            },

            getIconCoordsById                     : function(id) {
                if (this.iconsArray[id] != undefined)
                    return this.iconsArray[id];
                return false;
            },

            setIconsSortable                      : function(type) {
                this.autoSort = type;
                this.initDesktopIconsConfiguration();
                this.autoSort = !type;
            },

            checkForDeletedIcons                  : function(icons) {
                for (var i in icons) {
                    if (icons[i])
                        Ext.fly(icons[i].id).setStyle('display', 'none');
                }
            },

            deleteIcon                            : function(id) {
                return this.iconsArray[id].deleteIcon();
            },

            deleteAllIcons                        : function() {
                var icons = this.iconsArray;
                for (var i in this.iconsArray) {
                    this.deleteIcon(i);
                }
            },

            addIcon                               : function(item) {
                var iconsArea = Ext.fly(this.desktopEl.replace('#', ''));
                if (this.iconsArea)
                    iconsArea = iconsArea.child(this.iconsArea);

                if (!item.IconsManager)
                    item.IconsManager = this;

                this.iconsArray[item.id] = item instanceof Ext.ux.Desktop.Icon ? item : new Ext.ux.Desktop.Icon(item);
                this.iconsArray[item.id].render(iconsArea);

                this.fireEvent("iconAfterAdd", this, this.iconsArray[item.id]);
            },

            iconDblClick                          : function(manager, icon) {
                icon.handler();
            },

            iconSelect                            : function(manager, icon) {
                if (this.disabled) {
                    icon.handler();
                    icon.deselectIcon();
                }
            },

            /**
             * Adds all the portalWindow Icons. called from the taskbar as well
             * @param {} items
             */
            addPortalWindowIcons                  : function(items) {
                var maxX = 1;
                var maxY = 1;
                Ext.each(items, function(item) {
                            maxX = Math.max(maxX, (item.data.DesktopShortcutIconX || 0) + 1);
                            maxY = Math.max(maxY, (item.data.DesktopShortcutIconY || 0) + 1);

                        }, this);

                // We need to know if a new item was added
                var newItem;
                Ext.each(items, function(item) {
                            if ((item.data.DesktopShortcutIconX == null) && (item.data.DesktopShortcutIconY == null)) {
                                newItem = item;
                            }
                        }, this);

                // If a new item was added we need to check if we have room for it in the current maxX/maxY
                // If we dont, we push it to a new row and change the maxY accordingly.
                if (newItem) {

                    var slotsForItems = maxX * maxY;
                    if (items.length > slotsForItems) {
                        newItem.data.DesktopShortcutIconX = 0;
                        newItem.data.DesktopShortcutIconY = maxY;
                        maxY++;
                    }
                }

                // this.initIconsSize(items.length);
                this.initIconsSize(maxX, maxY);

                var iconsWithoutCoordinates = [];
                Ext.each(items, function(item) {
                            var icon = {
                                id      : Ext.id(),
                                data    : item.data,
                                iconCls : Ext.isEmpty(item.iconCls) ? Viz.util.Portal.getCssFromImage(item.data.KeyImage, 140, 140) : item.iconCls.replaceAll('viz-portal-large-', 'viz-desktop-shortcut-'),
                                iconX   : item.data.DesktopShortcutIconX || 0,
                                iconY   : item.data.DesktopShortcutIconY || 0,
                                enable  : true,
                                text    : item.tooltip,
                                handler : item.handler
                            };
                            // if the icon has a position we start by rendering them
                            if (Ext.isNumber(item.data.DesktopShortcutIconX) && Ext.isNumber(item.data.DesktopShortcutIconY)) {
                                this.addIcon(icon);
                            }
                            // we keep apart the icons that dont have a position yet in order to prevent them from taking an existing position.
                            else {
                                iconsWithoutCoordinates.push(icon);
                            }
                        }, this);

                Ext.each(iconsWithoutCoordinates, function(icon) {
                            this.addIcon(icon);
                        }, this);

                if (this.disabled) {
                    this.disableIconsManager();
                }
            },

            initIconsSize                         : function(maxX, maxY) {
                if (maxX > 0 && maxY > 0 && !Ext.isIE6) {
                    var DesktopWidth = Ext.fly(this.desktopEl.replace('#', '')).getWidth();
                    var DesktopHeight = Ext.fly(this.desktopEl.replace('#', '')).getHeight();

                    var availableSizePerIcon = DesktopWidth / maxX;
                    availableSizePerIcon = Math.min(availableSizePerIcon, DesktopHeight / maxY);
                    availableSizePerIcon = Math.max(availableSizePerIcon, 30);

                    if (availableSizePerIcon > 150) {
                        availableSizePerIcon = 150;
                    }
                    var iconSize = availableSizePerIcon - 20;

                    Ext.util.CSS.updateRule('.x-icon-desktop-shortcut', 'width', iconSize + 'px');
                    Ext.util.CSS.updateRule('.x-icon-desktop-shortcut', 'height', iconSize + 'px');
                    Ext.util.CSS.updateRule('.x-icon-desktop-shortcut-text', 'width', iconSize + 'px');
                    Ext.util.CSS.updateRule('#x-shortcuts dt', 'width', (iconSize) + 'px');

                    this.basicStyles.width = availableSizePerIcon;
                    this.basicStyles.height = availableSizePerIcon;

                    this.initDesktopIconsConfiguration();

                }
            },

            initIconsSizeOld                      : function(iconCount) {
                if (iconCount > 0 && !Ext.isIE6 && !Ext.isIE7 && !Ext.isIE8) {
                    var DesktopWidth = Ext.fly(this.desktopEl.replace('#', '')).getWidth();
                    var DesktopHeight = Ext.fly(this.desktopEl.replace('#', '')).getHeight();

                    var DesktopArea = DesktopWidth * DesktopHeight;
                    var availableSizePerIcon = Math.floor(Math.sqrt(DesktopArea / iconCount));

                    var iconPerLigne = Math.floor(DesktopWidth / availableSizePerIcon);
                    var iconPerColumn = Math.floor(DesktopHeight / availableSizePerIcon);

                    // until all icons can feat, we decrease the icon size.
                    while (availableSizePerIcon > 0 && iconPerLigne * iconPerColumn < iconCount) {
                        availableSizePerIcon = availableSizePerIcon - 10;
                        iconPerLigne = Math.floor(DesktopWidth / availableSizePerIcon);
                        iconPerColumn = Math.floor(DesktopHeight / availableSizePerIcon);
                    }
                    availableSizePerIcon = Math.max(availableSizePerIcon, 30);
                    if (availableSizePerIcon > 150) {
                        availableSizePerIcon = 150;
                    }
                    var iconSize = availableSizePerIcon - 20;

                    Ext.util.CSS.updateRule('.x-icon-desktop-shortcut', 'width', iconSize + 'px');
                    Ext.util.CSS.updateRule('.x-icon-desktop-shortcut', 'height', iconSize + 'px');
                    Ext.util.CSS.updateRule('.x-icon-desktop-shortcut-text', 'width', iconSize + 'px');
                    Ext.util.CSS.updateRule('#x-shortcuts dt', 'width', (iconSize) + 'px');

                    this.basicStyles.width = availableSizePerIcon;
                    this.basicStyles.height = availableSizePerIcon;

                    this.initDesktopIconsConfiguration();

                }
            },

            /**
             * Handler for the MessageBus PortalWindowChange event.
             */
            updateApplicationToolbarPortalWindows : function(items) {
                this.deleteAllIcons();
                this.addPortalWindowIcons(items);
                // if (Viz.DisplayAsDesktop) {
                // Viz.App.Global.Desktop.taskbar.updateStartMenuItems(items);
                // }
            },

            /**
             * @param {Ext.ux.Desktop.Icon} icon
             */
            onPortalWindowCopy                    : function(icon) {
                this.maskDesktop();
                Ext.MessageBox.confirm($lang('msg_portalwindow'), $lang('msg_portalwindow_copy_confirmation_single'), function(button) {
                            if (button == 'yes') {
                                Viz.Services.EnergyWCF.PortalWindow_Copy({
                                            keys    : [icon.data.KeyPortalWindow],
                                            success : function() {
                                                Viz.App.Global.ViewPort.updateApplicationToolbarPortalWindows.defer(500, Viz.App.Global.ViewPort);
                                                this.unmaskDesktop();
                                            },
                                            scope   : this
                                        });
                            }
                            else
                                this.unmaskDesktop();
                        }, this);

            },

            /**
             * @param {Ext.ux.Desktop.Icon} icon
             */
            onPortalWindowDelete                  : function(icon) {
                this.maskDesktop();
                Ext.MessageBox.confirm($lang('msg_portalwindow'), $lang('msg_portalwindow_delete_confirmation_single'), function(button) {
                            if (button == 'yes') {
                                Ext.MessageBox.confirm($lang('msg_portlet'), $lang('msg_portlet_delete_content'), function(button) {
                                            Viz.Services.EnergyWCF.PortalWindow_Delete({
                                                        item          : icon.data,
                                                        deleteContent : button == 'yes',
                                                        success       : function() {
                                                            Viz.App.Global.ViewPort.closePortalWindow(icon.data.KeyPortalWindow);
                                                            Viz.App.Global.ViewPort.updateApplicationToolbarPortalWindows();
                                                            this.unmaskDesktop();
                                                        },
                                                        scope         : this
                                                    });
                                        }, this);
                            }
                            else
                                this.unmaskDesktop();
                        }, this);
            },

            /**
             * @param {Ext.ux.Desktop.Icon} icon
             */
            onPortalWindowUpdate                  : function(icon) {
                Viz.grid.PortalWindow.update({
                            KeyPortalWindow : icon.data.KeyPortalWindow,
                            Title           : icon.data.Title
                        })
            },

            onPortalWindowApplySpatialFilter      : function(icon) {
                Viz.grid.PortalWindow.applySpatialFilter(icon.data.KeyPortalWindow);
            },

            /**
             * Listener for the iconMove Event. Save the new PortalWInodw desktop position.
             * @param {Ext.ux.Desktop.IconsManager} manager
             * @param {Ext.ux.Desktop.Icon} icon
             */
            onIconMove                            : function(manager, icon) {
                if ($authorized('@@@@ Portal Icons - Full Control')) {
                    Viz.Services.EnergyWCF.PortalWindow_UpdateDesktopShortcut({
                                KeyPortalWindow      : icon.data.KeyPortalWindow,
                                desktopShortcutIconX : icon.iconX,
                                desktopShortcutIconY : icon.iconY,
                                scope                : this
                            })
                }
            },

            maskDesktop                           : function() {
                Ext.get(this.desktopEl.replace('#', '')).mask();
            },

            unmaskDesktop                         : function() {
                Ext.get(this.desktopEl.replace('#', '')).unmask();
            }
        });
Ext.reg('desktopiconmanager', Ext.ux.Desktop.IconsManager);

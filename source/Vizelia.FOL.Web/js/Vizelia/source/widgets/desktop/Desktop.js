﻿
/**
 * @param {} app
 */
Ext.Desktop = function(app) {
    this.iconManager = new Ext.ux.Desktop.IconsManager();
    this.taskbar = new Ext.ux.TaskBar(this.iconManager);

    this.xTickSize = this.yTickSize = 1;
    var taskbar = this.taskbar;

    var desktopEl = Ext.get('x-desktop');
    var taskbarEl = Ext.get('ux-taskbar');

    var windows = new Ext.WindowGroup();
    windows.zseed = 6000;

    var activeWindow;

    function minimizeWin(win) {
        win.minimized = true;
        win.hide();
    }

    function markActive(win) {
        if (activeWindow && activeWindow != win) {
            markInactive(activeWindow);
        }
        taskbar.setActiveButton(win.taskButton);
        activeWindow = win;
        Ext.fly(win.taskButton.el).addClass('active-win');
        win.minimized = false;
    }

    function markInactive(win) {
        if (win == activeWindow) {
            activeWindow = null;
            Ext.fly(win.taskButton.el).removeClass('active-win');
        }
    }

    function removeWin(win) {
        taskbar.removeTaskButton(win.taskButton);
        layout();
    }

    function layout() {
        desktopEl.setHeight(Ext.lib.Dom.getViewHeight() - taskbarEl.getHeight());
    }
    Ext.EventManager.onWindowResize(layout);

    this.layout = layout;

    this.createWindow = function(configTab, cls) {

        Ext.apply(configTab, {
                    header    : false,
                    bodyStyle : 'padding:0px',
                    border    : false,
                    useWindow : true
                });
         var win = new Ext.Window({
                    maximized   : false,
                    maximizable : true,
                    width       : 800,
                    height      : 600,
                    layout      : 'fit',
                    renderTo    : desktopEl,
                    manager     : windows,
                    minimizable : true,
                    maximizable : true,
                    items       : configTab,
                    padding     : '5px 5px 5px 0px'
                });
        win.on('show', function() {
                    this.maximize();
                }, win);

        win.setTitle(win.items.get(0).initialConfig.title);
        if (win.items.get(0).initialConfig.iconCls)
            win.setIconClass(win.items.get(0).initialConfig.iconCls.replaceAll('-large-', '-small-'));

        /*
         * if (panel.items && panel.items.each) { panel.items.each(function(item) { win.add(item); }, this); }
         */
        win.dd.xTickSize = this.xTickSize;
        win.dd.yTickSize = this.yTickSize;

        if (win.resizer) {
            win.resizer.widthIncrement = this.xTickSize;
            win.resizer.heightIncrement = this.yTickSize;
        }
        win.render(desktopEl);
        win.taskButton = taskbar.addTaskButton(win);

        win.cmenu = new Ext.menu.Menu({
                    items : [

                    ]
                });

        win.animateTarget = win.taskButton.el;

        win.on({
                    'activate'   : {
                        fn : markActive
                    },
                    'beforeshow' : {
                        fn : markActive
                    },
                    'deactivate' : {
                        fn : markInactive
                    },
                    'minimize'   : {
                        fn : minimizeWin
                    },
                    'close'      : {
                        fn : removeWin
                    }
                });

        layout();
        return win;
    };

    this.getManager = function() {
        return windows;
    };

    this.getWindow = function(id) {
        return windows.get(id);
    };

    this.getWinWidth = function() {
        var width = Ext.lib.Dom.getViewWidth();
        return width < 200 ? 200 : width;
    };

    this.getWinHeight = function() {
        var height = (Ext.lib.Dom.getViewHeight() - taskbarEl.getHeight());
        return height < 100 ? 100 : height;
    };

    this.getWinX = function(width) {
        return (Ext.lib.Dom.getViewWidth() - width) / 2;
    };

    this.getWinY = function(height) {
        return (Ext.lib.Dom.getViewHeight() - taskbarEl.getHeight() - height) / 2;
    };

    this.setTickSize = function(xTickSize, yTickSize) {
        this.xTickSize = xTickSize;
        if (arguments.length == 1) {
            this.yTickSize = xTickSize;
        }
        else {
            this.yTickSize = yTickSize;
        }
        windows.each(function(win) {
                    win.dd.xTickSize = this.xTickSize;
                    win.dd.yTickSize = this.yTickSize;
                    win.resizer.widthIncrement = this.xTickSize;
                    win.resizer.heightIncrement = this.yTickSize;
                }, this);
    };

    this.cascade = function() {
        var availWidth = desktopEl.getWidth(true);
        var availHeight = desktopEl.getHeight(true);

        var x = 0, y = 0;

        windows.each(function(win) {
                    if (win.isVisible() && !win.maximized) {
                        win.setPosition(x, y);
                        win.setWidth(availWidth - x);
                        win.setHeight(availHeight - y);
                        x += 20;
                        y += 20;
                    }
                }, this);
    };

    this.tile = function() {
        var availWidth = desktopEl.getWidth(true);
        var availHeight = desktopEl.getHeight(true);
        var x = this.xTickSize;
        var y = this.yTickSize;
        var nextY = y;

        var windowsCount = 0;
        windows.each(function(win) {
                    windowsCount += 1;
                });

        windows.each(function(win) {
                    if (win.isVisible() && !win.maximized) {
                        var w = availWidth / windowsCount - this.xTickSize;// win.el.getWidth();

                        // Wrap to next row if we are not at the line start and this Window will go off the end
                        if ((x > this.xTickSize) && (x + w > availWidth)) {
                            x = this.xTickSize;
                            y = nextY;
                        }

                        win.setPosition(x, y);
                        win.setWidth(w);
                        win.setHeight(availHeight);
                        x += w + this.xTickSize;
                        nextY = Math.max(nextY, y + win.el.getHeight() + this.yTickSize);
                    }
                }, this);
    };

    /*
     * this.contextMenu = new Ext.menu.Menu({ items : [{ text : $lang('msg_desktop_tile'), iconCls : 'viz-icon-desktop-tile', handler : this.tile, scope : this }, { text : $lang('msg_desktop_cascade'), iconCls : 'viz-icon-desktop-cascade', handler : this.cascade, scope : this }] }); desktopEl.on('contextmenu', function(e) { e.stopEvent(); this.contextMenu.showAt(e.getXY()); }, this);
     */
    layout();
};

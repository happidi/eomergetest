﻿Ext.namespace('Viz.portal');

/**
 * @class Viz.portal.PortletWebFrame
 * @extends Ext.Panel
 * @xtype vizPortletWebFrame
 * <p>
 * Summary
 * </p>
 */
Viz.portal.PortletWebFrame = Ext.extend(Viz.portal.Portlet, {
            /**
             * @cfg {Viz.BusinessEntity.WebFrame} entity The WebFrame entity.
             * @type Viz.BusinessEntity.WebFrame
             */
            entity           : null,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor      : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    updateHandler : Viz.grid.WebFrame.update,
                    layout        : 'fit',
                    iconCls       : 'viz-icon-small-webframe'
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.portal.PortletWebFrame.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("WebFrameChange", this.onWebFrameChange, this);
            },

            /**
             * Destroy
             */
            onDestroy        : function() {
                Viz.util.MessageBusMgr.unsubscribe("WebFrameChange", this.onWebFrameChange, this);
                Viz.portal.PortletWebFrame.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the afterrender event.
             */
            onAfterRender    : function() {
                Viz.portal.PortletWebFrame.superclass.onAfterRender.apply(this, arguments);
                this.buildTemplate.defer(500, this);
            },

            /**
             * Handler for the WebFrame change
             * @param {} entity
             */
            onWebFrameChange : function(entity) {
                if (this.entity.KeyWebFrame == entity.KeyWebFrame) {
                    this.entity = entity;
                    this.buildTemplate();
                }
            },

            /**
             * Build and apply the template ot the body.
             */
            buildTemplate    : function() {
                if (!Ext.isEmpty(this.entity.Url)) {
                    var html = '<iframe id="iframe-' + this.id + '"' + ' style="overflow:auto;width:100%;height:100%;"' + ' frameborder="0" ' + ' src="' + this.entity.Url + '"' + '></iframe>';
                    Ext.get(this.body).update(html);
                    // Ext.get('iframe-' + this.id).setStyle("height", this.getHeight() + 'px');
                }
                else if (!Ext.isEmpty(this.entity.Body)) {
                    if (this.entity.Body.contains('xtype')) {
                        this.add(Ext.decode(this.entity.Body));
                        this.doLayout();
                    }
                    else {
                        Ext.get(this.body).update(this.entity.Body);
                    }
                }
            },

            /**
             * Listener for the refresh event.
             * @method onRefresh
             * @private
             */
            onRefresh        : function() {
                this.buildTemplate();
            }
        });

Ext.reg('vizPortletWebFrame', Viz.portal.PortletWebFrame);
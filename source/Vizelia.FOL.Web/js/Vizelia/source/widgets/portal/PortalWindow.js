﻿Ext.namespace('Viz.portal');

/**
 * @class Viz.portal.Window
 * @extends Ext.Window Summary.
 */
Viz.portal.Window = Ext.extend(Ext.Window, {
            /**
             * @cfg {string} KeyPortalWindow : The Key of the portal window the be displayed.
             */
            KeyPortalWindow : null,

            deferredRender  : true,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor     : function(config) {
                config = config || {};

                var defaultConfig = {
                    maximized       : false,
                    maximizable     : true,
                    collapsible     : true,
                    closable        : true,
                    iconCls         : 'viz-icon-small-portalwindow',
                    KeyPortalWindow : config.KeyPortalWindow,
                    padding         : '2px 2px 2px 2px',
                    layout          : 'fit',
                    items           : {
                        xtype           : 'vizPortalPanel',
                        KeyPortalWindow : config.KeyPortalWindow,
                        border          : false,
                        useWindow       : true
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.portal.Window.superclass.constructor.call(this, config);
            }
        });

Ext.reg('vizPortalWindow', Viz.portal.Window);

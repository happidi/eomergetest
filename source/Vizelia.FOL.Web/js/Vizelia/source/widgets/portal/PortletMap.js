﻿Ext.namespace('Viz.portal');

/**
 * @class Viz.portal.PortletMap
 * @extends Viz.portal.Portlet
 * @xtype vizPortletMap
 */
Viz.portal.PortletMap = Ext.extend(Viz.portal.Portlet, {

    entity                    : null,

    /**
     * The reference script to be loaded.
     * @cfg {String}
     */
    scriptUrl                 : window.location.protocol + '//ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx',

    /**
     * The current version of bing map
     * @cfg {String}
     */
    version                   : '7.0',

    /**
     * List of supported culture for the map.
     * @cfg {String}
     */
    supportedCultures         : ['nl-BE', 'en-CA', 'en-IN', 'en-GB', 'en-US', 'fr-CA', 'fr-FR', 'de-DE', 'it-IT', 'ja-JP', 'es-MX', 'es-ES', 'es-US'],

    /**
     * delay between 2 updates of the entity
     * @cfg {Number}
     */
    updateTaskDelay           : 2000,

    /**
     * @cfg {String} the loading class
     */
    loadingClass              : 'viz-chart-loading',

    /**
     * Ctor.
     * @param {Object} config The configuration options
     */
    constructor               : function(config) {
        config = config || {};
        var defaultConfig = {};
        var forcedConfig = {
            bodyStyle     : 'padding : 0px',
            updateHandler : Viz.grid.Map.update,
            iconCls       : 'viz-icon-small-map'
            // hideMenu : true
        };
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);
        Viz.portal.PortletMap.superclass.constructor.call(this, config);

        this.loadMapCount = 0;
        this.on('beforedestroy', this.onMapBeforeDestroy, this);
        this.updateTask = new Ext.util.DelayedTask(this.onUpdateEntity, this);

        Viz.util.MessageBusMgr.subscribe("MapChange", this.onMapChange, this);
    },

    /**
     * Destroy
     * @private
     */
    onDestroy                 : function() {
        Viz.util.MessageBusMgr.unsubscribe("MapChange", this.onMapChange, this);
        Viz.portal.PortletMap.superclass.onDestroy.apply(this, arguments);
    },

    /**
     * Handler for the afterrender event. Go and Fetch the BingKey and the script for the first load.
     */
    onAfterRender             : function() {
        Viz.portal.PortletMap.superclass.onAfterRender.apply(this, arguments);
        this.body.addClass(this.loadingClass);
        if (Ext.isEmpty(Viz.portal.PortletMap.prototype.bingKey)) {
            Viz.Services.CoreWCF.Versions_GetItem({
                        success : function(versions) {
                            Viz.Services.EnergyWCF.Map_GetKey({
                                        success : function(response) {
                                            Viz.portal.PortletMap.prototype.bingKey = response;

                                            var params = {
                                                v : this.version
                                            };
                                            if (this.supportedCultures.indexOf(versions.Culture) >= 0) {
                                                params.mkt = versions.Culture;
                                            }
                                            if (Ext.isSecure) {
                                                params.s = 1;
                                            }

                                            Ext.ux.ScriptMgr.load({
                                                        jsonp        : true,
                                                        scripts      : [this.scriptUrl],
                                                        params       : params,
                                                        timeout      : 2,
                                                        expectResult : false,
                                                        callback     : this.loadMap,
                                                        scope        : this
                                                    });
                                        },
                                        scope   : this
                                    })
                        },
                        scope   : this
                    })
        }
        else {
            this.loadMap.defer(500, this);
        }
    },

    /**
     * Listener for the map change event.
     * @private
     */
    onMapChange               : function(entity) {
        if (this.entity.KeyMap == entity.KeyMap) {
            this.entity = entity;
            this.onMapBeforeDestroy();
            this.loadMap();
        }
    },

    /**
     * Load the Map.
     */
    loadMap                   : function() {
        if (this.destroying !== true && this.isDestroyed !== true && Microsoft && Microsoft.Maps && Microsoft.Maps.Map && this.loadMapCount < 3) {
            this._map = new Microsoft.Maps.Map(this.body, {
                        credentials         : Viz.portal.PortletMap.prototype.bingKey,
                        enableClickableLogo : false,
                        enableSearchLogo    : false,
                        center              : new Microsoft.Maps.Location(this.entity.LocationLatitude, this.entity.LocationLongitude),
                        zoom                : this.entity.Zoom,
                        mapTypeId           : this.getMicrosoftMapType(this.entity.MapType),
                        showBreadcrumb      : true,
                        showMapTypeSelector : true,
                        showCopyright       : false,
                        showDashboard       : this.entity.ShowDashboard

                    });
            Microsoft.Maps.Events.addHandler(this._map, 'viewchangeend', this.onMapViewChangeEnd.createDelegate(this));

            this.loadPushpinsLongOperation();
            this.onPortletResize();

            this.body.removeClass(this.loadingClass);
        }
        else {
            this.loadMapCount += 1;
            if (this.loadMapCount < 3) {
                this.loadMap.defer(1000, this);

            }
        }
    },

    /**
     * @return {Boolean}
     */
    onMapBeforeDestroy        : function() {
        try {
            if (!this.body.removeChild)
                this.body.removeChild = Ext.emptyFn;
            if (this._map)
                this._map.dispose();
        }
        finally {
            this._map = null;
        }

        try {
            if (this.body && this.body.dom && this.body.dom.childNodes && this.body.dom.childNodes.length > 0)
                Ext.get(this.body.dom.childNodes[0].id).remove();
        }
        finally {
        }

        return true;
    },

    /**
     * Event handler for the Resize event
     */
    onPortletResize           : function() {
        Viz.portal.PortletMap.superclass.onPortletResize.apply(this, arguments);
        if (this._map) {
            this._map.setOptions({
                        height : this.body.getSize().height - this.body.getPadding('tb'),
                        width  : this.body.getSize().width - this.body.getPadding('lr')
                    });
        }
    },

    onBeforeContextMenu       : function() {

        this.ctxMenu = new Ext.menu.Menu(this.getMenuConfig());
        return true;
    },

    /**
     * Builds the menu config.
     * @private
     */
    getMenuConfig             : function() {
        return {
            defaults : {
                hideOnClick : true
            },
            items    : [{
                        text    : $lang("msg_map_viewpushpin"),
                        iconCls : 'viz-icon-small-display',
                        handler : this.onViewPushpin,
                        scope   : this
                    }, {
                        text    : $lang("msg_map_hidepushpin"),
                        iconCls : 'viz-icon-small-cancel',
                        handler : this.onHidePushpin,
                        scope   : this
                    }].concat(Viz.chart.Viewer.superclass.getMenuConfig.apply(this, arguments).items)
        }
    },

    /**
     * Gets Microsoft MapType based on the Entity MapType.
     */
    getMicrosoftMapType       : function(type) {
        if (type) {
            switch (type) {
                case Vizelia.FOL.BusinessEntities.MapType.Aerial :
                    return Microsoft.Maps.MapTypeId.aerial;
                case Vizelia.FOL.BusinessEntities.MapType.Road :
                    return Microsoft.Maps.MapTypeId.road;
                case Vizelia.FOL.BusinessEntities.MapType.Auto :
                default :
                    return Microsoft.Maps.MapTypeId.auto;
            }
        }
        return Microsoft.Maps.MapTypeId.auto;;
    },
    /**
     * Gets Entity MapType based on the Microsoft MapType.
     */
    getEntityMapType          : function(type) {
        if (type) {
            switch (type) {
                case Microsoft.Maps.MapTypeId.aerial :
                    return Vizelia.FOL.BusinessEntities.MapType.Aerial;
                case Microsoft.Maps.MapTypeId.road :
                    return Vizelia.FOL.BusinessEntities.MapType.Road;
                case Microsoft.Maps.MapTypeId.auto :
                default :
                    return Vizelia.FOL.BusinessEntities.MapType.Auto;

            }
        }
        return Vizelia.FOL.BusinessEntities.MapType.Auto;
    },

    /**
     * Load the Pushpins.
     */
    loadPushpins              : function() {
        Viz.Services.EnergyWCF.Map_GetPushpins({
                    Key     : this.entity.KeyMap,
                    success : function(response) {
                        this.displayPushpins(response);
                    },
                    scope   : this
                })

    },

    /**
    * Load the Pushpins with long running operation.
    */
    loadPushpinsLongOperation: function () {
        var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
            pollingInterval: 1000,
            serviceStart: Viz.Services.EnergyWCF.Map_BeginGetPushpins,
            serviceParams: {
                key: this.entity.KeyMap
            },
            showProgress: false,
            showProgressPercentage: false,
            modal: true,
            progressWindowTitle: $lang('msg_alarminstance_delete'),
            isFileResult: false,
            resultHandler: function (response) {
                this.displayPushpins(response);
            },
            scope: this
        });
        submitter.start();
    },
    /**
     * Display the pushpins
     */
    displayPushpins           : function(pushpins) {
        if (this._map) {
            this._map.entities.clear();
            if (pushpins && pushpins.MapPushpinsList && pushpins.MapPushpinsList.length > 0) {
                Ext.each(pushpins.MapPushpinsList, function (p) {
                    if (p) {
                        var id = Ext.id();
                        var pushpinOptions = {
                            typeName: id
                        };
                        if (Ext.isNumber(p.AlarmNumber) && p.AlarmNumber > 0) {
                            pushpinOptions.text = p.AlarmNumber + "";
                            pushpinOptions.icon = String.format("~/color.asmx?pushpin=true&R={0}&G={1}&B={2}", this.entity.ClassificationItemAlarmDefinitionColorR, this.entity.ClassificationItemAlarmDefinitionColorG, this.entity.ClassificationItemAlarmDefinitionColorB);
                        } else {
                            var icon = $imgSrc(Viz.Configuration.LocationCss(p.Entity.IfcType).replace('-small-', '-medium-'));
                            if (this.entity.UseSpatialIcons && Ext.isEmpty(icon) == false) {
                                pushpinOptions.icon = icon;
                                // pushpinOptions.text = p.Entity.Name;
                            }
                        }
                        var pushpin = new Microsoft.Maps.Pushpin(new Microsoft.Maps.Location(p.Location.Latitude, p.Location.Longitude), pushpinOptions);
                        // Add handler for the pushpin click event.
                        Microsoft.Maps.Events.addHandler(pushpin, 'click', this.displayInfoBox.createDelegate(this, [p]));
                        if ($authorized('Chart - Read Write')) {
                            Microsoft.Maps.Events.addHandler(pushpin, 'rightclick', this.addToSpatialFitlerInfoBox.createDelegate(this, [p.Entity]));
                        }
                        this._map.entities.push(pushpin);
                        try {
                            Ext.query('.' + id)[0].children[0].setAttribute('title', p.Entity.LongPath)
                        } catch (e) {
                            ;
                        }
                    }
                }, this);
            }
        }
    },

    /**
     * Display the infobox when a pushpin is clicked
     */
    displayInfoBox            : function(pushpin) {
        var win = new Ext.Window({
                    title     : pushpin.Entity.Name,
                    iconCls   : Viz.Configuration.LocationCss(pushpin.Entity.IfcType),
                    width     : 450,
                    height    : 130,
                    layout    : 'form',
                    bodyStyle : 'padding: 5px;background-color: transparent;',
                    defaults  : {
                        msgTarget : 'side',
                        anchor    : Viz.Const.UI.Anchor
                    },
                    items     : [{
                                fieldLabel : $lang('msg_location_path'),
                                readOnly   : true,
                                xtype      : 'textfield',
                                value      : pushpin.Entity.LongPath
                            }, {
                                fieldLabel : $lang('msg_address'),
                                readOnly   : true,
                                xtype      : 'textfield',
                                value      : pushpin.Location.Address
                            }],
                    buttons   : [{
                                text    : $lang("msg_map_viewlocationinfo"),
                                iconCls : 'viz-icon-small-display',
                                handler : this.openFormReadOnlyLocation.createDelegate(this, [pushpin.Entity]),
                                hidden  : !$authorized('@@@@ Spatial Hierarchy Tree'),
                                scope   : this
                            }, {
                                text    : $lang("msg_map_addtofilter"),
                                hidden  : !$authorized('Chart - Read Write'),
                                iconCls : 'viz-icon-small-apply',
                                handler : this.addToSpatialFitlerInfoBox.createDelegate(this, [pushpin.Entity]),
                                scope   : this
                            }]
                });
        win.show();
    },

    /**
     * Add the click location to the spatialfilter if the map is inside a portal tab.
     */
    addToSpatialFitlerInfoBox : function(entity) {
        this.forceHideMenu();

        var panel = this.getParentPortalPanel();
        if (panel) {
            var array = new Array();
            array.push(entity);
            panel.applySpatialFilter(array);
        }

        this.resetMenu.defer(2000, this);
    },

    forceHideMenu             : function() {
        this.hideMenu = true;
    },

    resetMenu                 : function() {
        this.hideMenu = false;
    },

    /**
     * Listener for the Map ViewChangeEnd event
     */
    onMapViewChangeEnd        : function() {
        if (this._map) {
            this.entity.Zoom = this._map.getZoom();
            var latlon = this._map.getCenter();

            this.entity.LocationLatitude = latlon.latitude;
            this.entity.LocationLongitude = latlon.longitude;
            this.entity.MapType = this.getEntityMapType(this._map.getMapTypeId());
            this.updateTask.delay(this.updateTaskDelay);
        }
    },

    /**
     * Listener for the afterentityupdate event.
     */
    onUpdateEntity            : function() {
        if (!$authorized('@@@@ Map - Full Control') && !$authorized('@@@@ Map - Location Save'))
            return;

        Viz.Services.EnergyWCF.Map_FormUpdate({
                    item    : this.entity,
                    success : function() {
                    },
                    scope   : this
                })
    },

    /**
     * Handler for the AddPushPin menu
     */

    onViewPushpin             : function() {
        this.setPushpinVisibility(true);
    },

    /**
     * 
     */
    onHidePushpin             : function() {
        this.setPushpinVisibility(false);
    },
    /**
     * @param {} visible
     */
    setPushpinVisibility      : function(visible) {
        if (this._map) {
            for (var i = 0; i < this._map.entities.getLength(); i++) {
                var pushpin = this._map.entities.get(i);
                pushpin.setOptions({
                            visible : visible
                        })
            }
        }
    },

    /**
     * Listener for the refresh event.
     * @private
     */
    onRefresh                 : function() {
        this.loadPushpinsLongOperation();
    },

    /**
     * Handler to open the form of the chart
     * @private
     */
    openFormReadOnlyLocation  : function(entity) {
        var xtype = "";
        var paramName = "";
        var title = "";
        switch (entity.IfcType) {
            case 'IfcSite' :
                xtype = 'vizFormSite';
                paramName = 'KeySite';
                title = $lang("msg_site");
                break;

            case 'IfcBuilding' :
                xtype = 'vizFormBuilding';
                paramName = 'KeyBuilding';
                title = $lang("msg_building");
                break;

            case 'IfcBuildingStorey' :
                xtype = 'vizFormBuildingStorey';
                paramName = 'KeyBuildingStorey';
                title = $lang("msg_floor");
                break;

            case 'IfcSpace' :
                xtype = 'vizFormSpace';
                paramName = 'KeySpace';
                title = $lang("msg_space");
                break;

            case 'IfcFurniture' :
                xtype = 'vizFormFurniture';
                paramName = 'KeyFurniture';
                title = $lang("msg_furniture");
                break;

        }
        var newEntity = {
            Name : entity.Name
        };
        newEntity[paramName] = entity.KeyLocation;

        if (entity) {
            Viz.openFormCrud({
                        common : {
                            width               : 740,
                            height              : 470,
                            xtype               : xtype,
                            title               : title,
                            iconCls             : Viz.Configuration.LocationCss(entity.IfcType),
                            readonly            : true,
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : paramName
                                    }]
                        }
                    }, 'update', newEntity);
        }
    }

        /*
         * onOpenFormCrudFromTool : function(event) { if (this.fireEvent("beforecontextmenu", this, this.entity) != false) { if (this.ctxMenu && this.ctxMenu instanceof Ext.menu.Menu) { this.ctxMenu.on("hide", function(menu) { menu.destroy(); }); this.ctxMenu.showAt(event.getXY()); } } }
         */
    });

Ext.reg('vizPortletMap', Viz.portal.PortletMap);
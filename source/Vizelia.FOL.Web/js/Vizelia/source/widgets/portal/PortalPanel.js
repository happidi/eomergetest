﻿Ext.namespace('Viz.portal');

/**
 * @class Viz.portal.Panel
 * @extends Ext.Panel Summary.
 */
Viz.portal.Panel = Ext.extend(Ext.Panel, {
            /**
             * @cfg {string} KeyPortalWindow : The Key of the portal window the be displayed.
             */
            KeyPortalWindow                                  : null,

            /**
             * @cfg {string} KeyInitialActiveTab : The Key of the active tab (can be null).
             */
            KeyInitialActiveTab                              : null,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                                      : function(config) {
                config = config || {};

                this._tabContainerId = Ext.id();
                this._treeSpatialId = Ext.id();

                if (config.useWindow === true) {
                    var defaultConfig = {
                        closable  : true,
                        layout    : 'border',
                        header    : false,
                        padding   : '1px',
                        bodyStyle : 'background-color: transparent;',
                        items     : [this.buildRegionCenter(config)],
                        menu      : this.getMenuConfig(config)
                    };
                }
                else {
                    var defaultConfig = {
                        closable     : true,
                        maskDisabled : false,
                        layout       : 'fit',
                        header       : false,
                        padding      : '0px',
                        items        : {
                            layout : 'border',
                            border : false,
                            items  : [this.buildRegionCenter(config)]
                        },
                        menu         : this.getMenuConfig(config)
                    };
                }
                var forcedConfig = {
                    hideMode : 'offsets'
                };

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.portal.Panel.superclass.constructor.call(this, config);

                this.on({
                            render     : {
                                fn     : this.loadEntity.createDelegate(this, []),
                                scope  : this,
                                single : true
                            },
                            activate   : {
                                fn    : this.onActivate,
                                scope : this
                            },
                            deactivate : {
                                fn    : this.onDeactivate,
                                scope : this
                            }
                        });
                Viz.util.MessageBusMgr.subscribe("PortalWindowChange", this.onPortalWindowChange, this);
                Viz.util.MessageBusMgr.subscribe("PortalTabChange", this.onPortalTabChange, this);
                Viz.util.MessageBusMgr.subscribe("PortalColumnChange", this.onPortalColumnChange, this);
                Viz.util.MessageBusMgr.subscribe("PortalTabDelete", this.onPortalTabDelete2, this);
                Viz.util.MessageBusMgr.subscribe("PortalColumnDelete", this.onPortalColumnDelete, this);
                Viz.util.MessageBusMgr.subscribe("PortletChange", this.onPortletChange, this);

            },
            /**
             * Destroy
             */
            onDestroy                                        : function() {
                Viz.util.MessageBusMgr.unsubscribe("PortalWindowChange", this.onPortalWindowChange, this);
                Viz.util.MessageBusMgr.unsubscribe("PortalTabChange", this.onPortalTabChange, this);
                Viz.util.MessageBusMgr.unsubscribe("PortalColumnChange", this.onPortalColumnChange, this);
                Viz.util.MessageBusMgr.unsubscribe("PortalTabDelete", this.onPortalTabDelete2, this);
                Viz.util.MessageBusMgr.unsubscribe("PortalColumnDelete", this.onPortalColumnDelete, this);
                Viz.util.MessageBusMgr.unsubscribe("PortletChange", this.onPortletChange, this);

                Viz.portal.Panel.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Builds the center region
             * @return {Object}
             */
            buildRegionCenter                                : function(config) {
                var retVal = {
                    xtype             : 'tabpanel',
                    region            : 'center',
                    id                : this._tabContainerId,
                    tabPosition       : 'bottom',

                    activeTab         : 0,
                    deferredRender    : true,
                    plain             : false,
                    frame             : false,
                    border            : true,
                    enableTabScroll   : true,
                    defaults          : {
                        autoScroll : false,
                        bodyStyle  : 'padding: 1px;'
                    },
                    layoutOnTabChange : true,
                    listeners         : {
                        contextmenu : this.onTabContextMenu,
                        tabmove     : this.onTabMove,
                        scope       : this
                    }
                };

                if ($authorized('@@@@ Portal - Full Control')) {
                     retVal.plugins = [{
                     ptype : 'vizDragDropTabs'
                     }];
                }
                return retVal;
            },

            onTabContextMenu                                 : function(tabpanel, portal, event) {
                portal.fireEvent('contextmenu', portal, event);
            },

            /**
             * Handler for the activate Event.
             */
            onActivate                                       : function() {
                var tab = this.getActivePortalTab();
                if (tab) {
                    tab.doLayout();
                    tab.onActivate();
                }
                // disabled for now because if we activate a panel when a form is open, then the multiple DD problem appears again.
                // this.unlockDropTargets();
            },

            /**
             * Handler for the deactivate Event.
             */
            onDeactivate                                     : function() {
                // disabled for now because if we activate a panel when a form is open, then the multiple DD problem appears again.
                // this.lockDropTargets();
            },

            /**
             * Returns the Menu Config
             * @param {} config
             * @return {}
             */
            getMenuConfig                                    : function(config) {
                return [{
                            text    : $lang("msg_portaltab_showallheaderandtoolbar"),
                            iconCls : 'viz-icon-small-portlet-headerandtoolbarvisible',
                            handler : this.onPortalTabSetPortletsHeaderAndToolbarVisibility.createDelegate(this, [true]),
                            scope   : this
                        }, {
                            text    : $lang("msg_portaltab_hideallheaderandtoolbar"),
                            iconCls : 'viz-icon-small-portlet-headerandtoolbarvisible',
                            handler : this.onPortalTabSetPortletsHeaderAndToolbarVisibility.createDelegate(this, [false]),
                            scope   : this
                        }, '-', {
                            text    : $lang("msg_portaltab_refresh"),
                            iconCls : 'viz-icon-small-refresh',
                            handler : this.onPortalTabRefresh.createDelegate(this, []),
                            scope   : this
                        }, '-', {
                            text    : $lang("msg_portaltab_add"),
                            iconCls : 'viz-icon-small-add',
                            hidden  : !$authorized('@@@@ Portal - Full Control'),
                            handler : this.onOpenFormCrudPortalTab.createDelegate(this, ['create']),
                            scope   : this
                        }, {
                            text    : $lang("msg_portaltab_update"),
                            iconCls : 'viz-icon-small-update',
                            hidden  : !$authorized('@@@@ Portal - Full Control'),
                            handler : this.onOpenFormCrudPortalTab.createDelegate(this, ['update']),
                            scope   : this
                        }, {
                            text    : $lang("msg_portaltab_delete"),
                            iconCls : 'viz-icon-small-delete',
                            hidden  : !$authorized('@@@@ Portal - Full Control'),
                            handler : this.onPortalTabDelete.createDelegate(this, []),
                            scope   : this
                        }, {
                            text    : $lang("msg_portaltab_copy"),
                            iconCls : 'viz-icon-small-copy',
                            hidden  : !$authorized('@@@@ Portal - Full Control'),
                            handler : this.onPortalTabCopy.createDelegate(this, []),
                            scope   : this
                        }, '-', {
                            text    : $lang("msg_portalcolumn_add"),
                            iconCls : 'viz-icon-small-add',
                            hidden  : !$authorized('@@@@ Portal - Full Control'),
                            handler : this.onOpenFormCreatePortalColumn.createDelegate(this, []),
                            scope   : this
                        }, {
                            text    : $lang("msg_application_export"),
                            iconCls : 'viz-icon-small-report',
                            menu    : {
                                items : [{
                                            text    : $lang("msg_portaltab_exportexcel"),
                                            iconCls : 'viz-icon-small-excel',
                                            handler : this.onPortalTabExportToExcel.createDelegate(this, []),
                                            scope   : this
                                        }, {
                                            text    : $lang("msg_portaltab_exportpdf"),
                                            iconCls : 'viz-icon-small-pdf',
                                            handler : this.onPortalTabExportToPDF.createDelegate(this, []),
                                            scope   : this
                                        }, '-', {
                                            text    : $lang("msg_portalwindow_exportexcel"),
                                            iconCls : 'viz-icon-small-excel',
                                            handler : this.onPortalWindowExportToExcel.createDelegate(this, []),
                                            scope   : this
                                        }, {
                                            text    : $lang("msg_portalwindow_exportpdf"),
                                            iconCls : 'viz-icon-small-pdf',
                                            handler : this.onPortalWindowExportToPDF.createDelegate(this, []),
                                            scope   : this
                                        }]
                            }
                        }, {
                            text    : $lang("msg_chart_timerange"),
                            iconCls : 'viz-icon-small-calendarevent',
                            hidden  : !$authorized('@@@@ Portal - Full Control'),
                            menu    : {
                                defaults : {
                                    hideOnClick : true
                                },
                                items    : Viz.util.Chart.getTimeRangeMenu(null, this, Ext.id(), Ext.id())
                            }
                        }];
            },

            /**
             * forward the call from the portal tab menu to the chart
             */
            onChartUpdateStartDate                           : function() {
                var portlets = this.getActivePortalTab().getPortlets();
                for (var i = 0; i < portlets.length; i++) {
                    var p = portlets[i];
                    if (Ext.isFunction(p.onChartUpdateStartDate))
                        p.onChartUpdateStartDate.apply(p, arguments);
                }
            },
            /**
             * forward the call from the portal tab menu to the chart
             */
            onChartUpdateEndDate                             : function() {
                var portlets = this.getActivePortalTab().getPortlets();
                for (var i = 0; i < portlets.length; i++) {
                    var p = portlets[i];
                    if (Ext.isFunction(p.onChartUpdateEndDate))
                        p.onChartUpdateEndDate.apply(p, arguments);
                }
            },
            /**
             * forward the call from the portal tab menu to the chart
             */
            onChartUpdateDynamicTimeScale                    : function() {
                var portlets = this.getActivePortalTab().getPortlets();
                for (var i = 0; i < portlets.length; i++) {
                    var p = portlets[i];
                    if (Ext.isFunction(p.onChartUpdateDynamicTimeScale))
                        p.onChartUpdateDynamicTimeScale.apply(p, arguments);
                }
            },
            /**
             * Load the {Viz.BusinessEntity.Portal} entity from the server and construct the portalwindow childs.
             */
            loadEntity                                       : function(KeyActiveTab) {
                if (!Ext.isEmpty(KeyActiveTab)) {
                    Viz.Services.EnergyWCF.PortalTab_GetItem({
                                Key     : KeyActiveTab,// this.KeyPortalTab,
                                success : function(entity) {
                                    this.buildFromPortalTab(entity);
                                },
                                scope   : this
                            });
                }
                else if (!Ext.isEmpty(this.KeyPortalWindow)) {
                    Viz.Services.EnergyWCF.PortalWindow_GetPortal({
                                Key     : this.KeyPortalWindow,
                                success : function(entity) {
                                    this.buildFromPortalWindow(entity);
                                },
                                scope   : this
                            });
                }
            },

            /**
             * Handler for the PortalChange Event. We check it s the same entity and rebuild the tabs.
             * @param {Viz.BusinessEntity.PortalWindow} entity
             */
            onPortalWindowChange                             : function(entity) {
                if (this.KeyPortalWindow == entity.KeyPortalWindow) {
                    this.loadEntity(entity.KeyActiveTab);
                    this.setTitle(Viz.util.Portal.localizeTitle(entity.Title));
                }
            },

            /**
             * Handler for the PortalTabChange Event. We check it s the same entity and rebuild the tabs.
             * @param {Viz.BusinessEntity.PortalTab} entity
             */
            onPortalTabChange                                : function(entity) {
                if (this.KeyPortalWindow == entity.KeyPortalWindow)
                    this.buildFromPortalTab(entity);
            },

            /**
             * Handler for the PortalTabChange Event. We check it s the same entity and rebuild the tabs.
             * @param {Viz.BusinessEntity.PortalColumn} entity
             */
            onPortalColumnChange                             : function(entity) {
                var tab = this.getPortalTab(entity.KeyPortalTab);
                if (tab)
                    this.buildFromPortalColumn(entity);
            },

            /**
             * Handler for the PortalTabChange Event. We check it s the same entity and rebuild the tabs.
             * @param {Viz.BusinessEntity.PortalTab} entity
             */
            onPortalTabDelete2                               : function(entity) {
                if (this.KeyPortalWindow == entity.KeyPortalWindow) {
                    var tabpanel = this.removePortalTab(entity);
                    tabpanel.doLayout();
                }
            },

            /**
             * Handler for the PortalTabChange Event. We check it s the same entity and rebuild the tabs.
             * @param {Viz.BusinessEntity.PortalColumn} entity
             */
            onPortalColumnDelete                             : function(entity) {
// WARNING: This is the first of TWO declarations for "onPortalColumnDelete" in this class.
// It is NOT USED. 
// An expert in this code should determine what to do with this declaration
                var tabpanel = Ext.getCmp(this._tabContainerId);
                var parentTab = this.getPortalTab(entity.KeyPortalTab);
                if (parentTab) {
                    var col = this.getPortalColumn(entity.KeyPortalColumn);
                    if (col) {
                        parentTab.removeColumn(col);
                    }
                    /*
                     * var insertIndex = 0; var totalFlex = 0; var cols = parentTab.getPortalColumns(); for (var i = 0; i < cols.length; i++) { if (cols[i].entity.Type == entity.Type) { totalFlex += cols[i].entity.Flex; if (cols[i].entity.Order < entity.Order) { insertIndex = i + 1; } } } totalFlex += entity.Flex; Ext.each(cols, function(col) { if (col.entity.Type == entity.Type) { col.updateFlex(totalFlex); } }, this);
                     */
                }
                tabpanel.doLayout();
                tabpanel.setActiveTab(parentTab);
                // we need to wait until the new column has been rendered.
                this.adjustPortalColumnsPadding.defer(500, this);
            },

            /**
             * Handler for the PortletChange Event. We check it s the same entity and rebuild the tabs.
             * @param {Viz.BusinessEntity.Portlet} entity
             */
            onPortletChange                                  : function(entity) {
                var col = this.getPortalColumn(entity.KeyPortalColumn);
                if (col)
                    this.buildFromPortlet(entity);
            },

            /*
             * Handler for the tabmove Event.
             */
            onTabMove                                        : function() {
                var tabs = this.getPortalTabs();
                for (var i = 0; i < tabs.length; i++) {
                    tabs[i].entity.Order = i + 1;

                    Viz.Services.EnergyWCF.PortalTab_FormUpdate({
                                item    : tabs[i].entity,
                                success : function(entity) {
                                },
                                scope   : this
                            });
                }
            },

            /**
             * Build the differents tabs based on the portal window entity.
             * @param {Viz.BusinessEntity.PortalWindow} entity
             */
            buildFromPortalWindow                            : function(entity, KeyPortalTab) {
                var tabpanel = Ext.getCmp(this._tabContainerId);
                if (!tabpanel)  // the user managed to close the panel before the API function PortalWindow_GetPortal returns
                    return;
                this.entity = entity;
                if (this.entity.IconClsSmall && this.entity.IconClsSmall.length > 0)
                    this.setIconClass(this.entity.IconClsSmall);
                else if (Ext.isEmpty(this.entity.KeyImage) == false)
                    this.setIconClass(Viz.util.Portal.getCssFromImage(this.entity.KeyImage, 16, 16));

                var activeTabIndex = 0;
                tabpanel.setDisabled(true);
                if (tabpanel.items && tabpanel.items.length > 0) {
                    while (tabpanel.items.length > 0) {
                        tabpanel.remove(tabpanel.items.get(0), true);
                    }
                }
                tabpanel.add(Viz.util.Portal.getPortalWindowConfig(entity));

                if (!Ext.isEmpty(this.KeyInitialActiveTab)) {
                    for (var i = 0; i < tabpanel.items.length; i++) {
                        if (tabpanel.items.get(i).entity.KeyPortalTab == this.KeyInitialActiveTab) {
                            activeTabIndex = i;
                        }
                    }

                }

                tabpanel.doLayout();
                tabpanel.setDisabled(false);
				tabpanel.setActiveTab(activeTabIndex);
            },

            /**
             * Build the differents tabs based on the portal entity.
             * @param {Viz.BusinessEntity.PortalTab} entity
             */
            buildFromPortalTab                               : function(entity) {
                var tabpanel = this.removePortalTab(entity);
                if (!tabpanel)  // the user managed to close the panel before the API function PortalWindow_GetPortal returns
                    return;

                var insertIndex = 0;
                var tabs = this.getPortalTabs();
                for (var i = 0; i < tabs.length; i++) {
                    if (tabs[i].entity.Order < entity.Order) {
                        insertIndex = i + 1;
                    }
                }
                tabpanel.insert(insertIndex, Viz.util.Portal.getPortalTabConfig(entity));
                tabpanel.doLayout();
                tabpanel.setActiveTab(insertIndex);
            },

            /**
             * Remove an existing PortalTab and returns the parent tabpanel container.
             * @param {Viz.BusinessEntity.PortalTab} entity
             */
            removePortalTab                                  : function(entity) {
                var tabpanel = Ext.getCmp(this._tabContainerId);
                var tab = this.getPortalTab(entity.KeyPortalTab);
                if (tab) {
                    tabpanel.remove(tab);
                }
                return tabpanel;
            },

            /**
             * Build the different portlet based on the portal Column.
             * @param {Viz.BusinessEntity.PortalColumn} entity
             */
            buildFromPortalColumn                            : function(entity) {
                var tabpanel = Ext.getCmp(this._tabContainerId);
                var parentTab = this.getPortalTab(entity.KeyPortalTab);
                if (parentTab) {
                    var col = this.getPortalColumn(entity.KeyPortalColumn);
                    if (col) {
                        parentTab.removeColumn(col);
                    }

                    var insertIndex = 0;
                    var totalFlex = 0;
                    var cols = parentTab.getPortalColumns();
                    for (var i = 0; i < cols.length; i++) {
                        if (cols[i].entity.Type == entity.Type) {
                            totalFlex += cols[i].entity.Flex;
                            if (cols[i].entity.Order < entity.Order) {
                                insertIndex = i + 1;
                            }
                        }
                    }
                    totalFlex += entity.Flex;

                    Ext.each(cols, function(col) {
                                if (col.entity.Type == entity.Type) {
                                    col.updateFlex(totalFlex);
                                }
                            }, this);

                    parentTab.insertColumn(entity.Type, insertIndex, Viz.util.Portal.getPortalColumnConfig(entity, totalFlex, insertIndex, cols.length + 1));
                }
                tabpanel.doLayout();
                tabpanel.setActiveTab(parentTab);
                // we need to wait until the new column has been rendered.
                this.adjustPortalColumnsPadding.defer(500, this);
            },

            /**
             * Build the different portlet based on the portal Column.
             * @param {Viz.BusinessEntity.Portlet} entity
             */
            buildFromPortlet                                 : function(entity) {
                var tabpanel = Ext.getCmp(this._tabContainerId);
                var parentColumn = this.getPortalColumn(entity.KeyPortalColumn);
                if (parentColumn) {
                    var portlet = this.getPortlet(entity.KeyPortlet);
                    if (portlet) {
                        parentColumn.remove(portlet);
                    }
                    portlet = Viz.util.Portal.getPortletConfig(entity);
                    var insertIndex = 0;
                    var portlets = parentColumn.getPortlets();
                    for (var i = 0; i < portlets.length; i++) {
                        if (portlets[i].entityPortlet.Order < entity.Order) {
                            insertIndex = i + 1;
                        }
                    }

                    parentColumn.insert(insertIndex, portlet);
                    parentColumn.doLayout();
                    tabpanel.setActiveTab(parentColumn.getParentPortalTab());

                    parentColumn.updatePortletLayout();
                }
            },

            /**
             * Get the child portals.
             * @return {Array} Array of Viz.portal.Portal
             */
            getPortalTabs                                    : function() {
                return this.findByType('vizPortalTab');
            },

            /**
             * Get a specific tab.
             * @param {string} KeyPortalTab: the Key of the portal tab
             * @return {Viz.portal.Portal} the tab
             */
            getPortalTab                                     : function(KeyPortalTab) {
                var tabs = this.getPortalTabs();
                var result = null;
                Ext.each(tabs, function(tab) {
                            if (tab.entity && tab.entity.KeyPortalTab == KeyPortalTab) {
                                result = tab;
                                return false;
                            }
                        }, this)
                return result;
            },

            /**
             * Set the active portal tab.
             * @param {} KeyPortalTab
             */

            setActivePortalTab                               : function(KeyPortalTab) {
                var tabpanel = Ext.getCmp(this._tabContainerId);
                var tab = this.getPortalTab(KeyPortalTab);
                if (tab) {
                    tabpanel.setActiveTab(tab.id);
                }
            },

            /**
             * Get the active Portaltab.
             * @return {Viz.portal.Portal} the tab
             */
            getActivePortalTab                               : function() {
                var tabpanel = Ext.getCmp(this._tabContainerId);
                return tabpanel.getActiveTab();
            },

            /**
             * Get the Key of the active Portaltab.
             * @return {string} KeyPortalTab: the Key of the portal tab
             */
            getActiveKeyPortalTab                            : function() {
                return this.getActivePortalTab().entity.KeyPortalTab;
            },

            /**
             * Get the child portals.
             * @return {Array} Array of Viz.portal.Portal
             */
            getPortalColumns                                 : function() {
                return this.findByType('vizPortalColumn');
            },

            /**
             * Get a specific column.
             * @param {string} KeyPortalColumn: the Key of the portal column
             * @return {Viz.portal.PortalColumn} the column
             */
            getPortalColumn                                  : function(KeyPortalColumn) {
                var cols = this.getPortalColumns();
                var result = null;
                Ext.each(cols, function(col) {
                            if (col.entity && col.entity.KeyPortalColumn == KeyPortalColumn) {
                                result = col;
                                return false;
                            }
                        }, this)
                return result;
            },

            /**
             * Adjust the portal columns padding after a column has been added for example.
             */
            adjustPortalColumnsPadding                       : function() {
                var cols = this.getPortalColumns();
                var count = 0;
                Ext.each(cols, function(col) {
                            col.el.setStyle('padding', Viz.util.Portal.getPortalColumnPadding(count, cols.length));
                            count += 1;
                        }, this)
            },
            /**
             * Get the child portlets.
             * @return {Array} Array of Viz.portal.Portlet
             */
            getPortlets                                      : function() {
                var columns = this.getPortalColumns();
                var retVal = [];
                Ext.each(columns, function(col) {
                            retVal = retVal.concat(col.getPortlets());
                        }, this);
                return retVal;
                // return this.findByType('vizPortlet');
            },

            /**
             * Get a specific portlet
             * @param {string} KeyPortlet: the Key of the portlet
             * @return {Viz.portal.Portlet} the porlet
             */
            getPortlet                                       : function(KeyPortlet) {
                var portlets = this.getPortlets();
                var result = null;
                Ext.each(portlets, function(p) {
                            if (p.entityPortlet && p.entityPortlet.KeyPortlet == KeyPortlet) {
                                result = p;
                                return false;
                            }
                        }, this)
                return result;
            },

            /**
             * Displays the contextual menu.
             * @param {Viz.portal.PortalTab} portaltab
             * @param {Ext.EventObject} event
             */
            onContextMenu                                    : function(portaltab, event) {
                event.stopEvent();
                this.tabMenu = portaltab;
                if (this.hideMenu == true)
                    return;
                if (!this.ctxMenu && this.menu)
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.menu
                            });
                if (this.ctxMenu instanceof Ext.menu.Menu) {
                    this.ctxMenu.on("hide", function(menu) {
                                if (this.ctxMenu && this.ctxMenu.destroy)
                                    this.ctxMenu.destroy();
                                this.ctxMenu = null;
                            }, this);
                    this.ctxMenu.showAt(event.getXY());
                }

            },

            /*
             * Open the portalwindow form crud
             */
            onOpenFormCrudPortalWindow                       : function() {
                Viz.grid.PortalWindow.update(this.entity);
            },

            /*
             * Open the portaltab form crud @param {string} mode: update or create
             */
            onOpenFormCrudPortalTab                          : function(mode) {
                if (mode == 'update') {
                    Viz.grid.PortalTab.update(this.tabMenu.entity);
                }
                else {
                    Viz.grid.PortalTab.create({
                                KeyPortalWindow : this.entity.KeyPortalWindow,
                                Order           : Ext.getCmp(this._tabContainerId).items.getCount() + 1
                            })
                }
            },

            /**
             * Handler for the PortalTab Delete menu.
             */
            onPortalTabDelete                                : function() {
                var tabs = this.getPortalTabs();
                if (tabs.length > 1) {
                    var msgDelete = $lang("msg_record_delete_confirmation_single");
                    Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                                if (button == 'yes') {
                                    Viz.Services.EnergyWCF.PortalTab_Delete({
                                                item    : this.tabMenu.entity,
                                                success : function() {
                                                    var tabpanel = this.removePortalTab(this.tabMenu.entity);
                                                    tabpanel.doLayout();
                                                },
                                                scope   : this
                                            })
                                }
                            }, this);
                }
            },

            /**
             * Handler for the PortalColumn Delete menu.
             */
            onPortalColumnDelete                             : function(entity) {
                var msgDelete = $lang("msg_record_delete_confirmation_single");
                Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                            if (button == 'yes') {
                                Viz.Services.EnergyWCF.PortalColumn_Delete({
                                            item          : entity,
                                            deleteContent : true,
                                            success       : function() {
                                                this.loadEntity(entity.KeyPortalTab);
                                            },
                                            scope         : this
                                        })
                            }
                        }, this);
            },

            /**
             * Handler for the PortalTab Copy menu.
             */
            onPortalTabCopy                                  : function() {
                Ext.MessageBox.confirm($lang('msg_portaltab'), $lang('msg_portaltab_copy_confirmation_single'), function(button) {
                            if (button == 'yes') {
                                Viz.Services.EnergyWCF.PortalTab_Copy({
                                            keys    : [this.tabMenu.entity.KeyPortalTab],
                                            success : function(response) {
                                                this.buildFromPortalTab(response.data);
                                            },
                                            scope   : this
                                        });
                            }
                        }, this);

            },

            /*
             * Open the portalcolumn form crud @param
             */
            onOpenFormCreatePortalColumn                     : function(order) {
                Viz.grid.PortalColumn.create({
                            KeyPortalTab : this.tabMenu.entity.KeyPortalTab,
                            Order        : order || 1
                        });
            },

            /*
             * Open the portaltab form crud
             */
            onOpenFormCreatePortlet                          : function() {
                var cols = this.tabMenu.getPortalColumns();
                if (cols.length > 0) {
                    Viz.grid.Portlet.create({
                                KeyPortalColumn : cols[0].entity.KeyPortalColumn
                            });
                }
            },

            /*
             * Handler for the Export to excel menu of the current PortalWindow.
             */
            onPortalWindowExportToExcel                      : function() {
                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                            pollingInterval : 2000,
                            serviceStart    : Viz.Services.EnergyWCF.PortalWindow_GetStreamExcelBegin,
                            serviceParams   : {
                                Key : this.entity.KeyPortalWindow
                            },
                            isFileResult    : true,
                            showProgress    : true
                        });
                submitter.start();
            },

            /*
             * Handler for the Export to PDF menu of the current PortalWindow.
             */
            onPortalWindowExportToPDF                        : function() {
                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                            pollingInterval : 2000,
                            serviceStart    : Viz.Services.EnergyWCF.PortalWindow_GetStreamPdfBegin,
                            serviceParams   : {
                                Key : this.entity.KeyPortalWindow
                            },
                            isFileResult    : true,
                            showProgress    : true
                        });
                submitter.start();
            },

            /**
             * Handler for the Refresh menu.
             */
            onPortalTabRefresh                               : function() {
                this.refreshActiveTabPortlets(true);
            },

            /**
             * Set Portlets Header And Toolbar Visibility
             * @param {} visibility
             */
            onPortalTabSetPortletsHeaderAndToolbarVisibility : function(visibility) {
                var portlets = this.getActivePortalTab().getPortlets();
                Ext.each(portlets, function(p) {
                            if (Ext.isFunction(p.updatePortletHeaderAndToolbarVisibility))
                                p.updatePortletHeaderAndToolbarVisibility(visibility);
                        }, this)
            },

            /*
             * Handler for the Export to excel menu of the current PortalTab.
             */
            onPortalTabExportToExcel                         : function() {
                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                            pollingInterval : 2000,
                            serviceStart    : Viz.Services.EnergyWCF.PortalTab_GetStreamExcelBegin,
                            serviceParams   : {
                                Key : this.tabMenu.entity.KeyPortalTab
                            },
                            isFileResult    : true,
                            showProgress    : true
                        });
                submitter.start();
            },

            /*
             * Handler for the Export to PDF menu of the current PortalTab.
             */
            onPortalTabExportToPDF                           : function() {
                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                            pollingInterval : 2000,
                            serviceStart    : Viz.Services.EnergyWCF.PortalTab_GetStreamPdfBegin,
                            serviceParams   : {
                                Key : this.tabMenu.entity.KeyPortalTab
                            },
                            isFileResult    : true,
                            showProgress    : true
                        });
                submitter.start();
            },

            /**
             * Create a new portlet . Triggered when a node is dropped from the tree Portlet.
             * @param {string} title
             * @param {string} KeyEntity
             * @param {string} TypeEntity
             * @param {string} KeyPortalColumn
             */
            addPortletFromDrop                               : function(title, KeyEntity, TypeEntity, KeyPortalColumn, Order) {
                this.el.mask();
                var portlet = {
                    Collapsed               : false,
                    Flex                    : 1,
                    KeyEntity               : KeyEntity,
                    TypeEntity              : TypeEntity,
                    KeyPortalColumn         : KeyPortalColumn,
                    HeaderAndToolbarVisible : true,
                    KeyPortlet              : null,
                    Order                   : Order || 1,
                    Title                   : title

                };
                Viz.Services.EnergyWCF.Portlet_FormCreate({
                    item    : portlet,
                    success : function(response) {
                        var entity = response.data;
                        this.buildFromPortlet(entity);
                        this.el.unmask();
                    },
                    failure : function () {
                        this.el.unmask();
                    },
                    scope   : this
                });
            },

            /**
             * Apply the current checked nodes in the spatial filter to all Charts in the current tab.
             */
            applySpatialFilterFromTree                       : function() {
                var tree = Ext.getCmp(this._treeSpatialId);
                var checkedNodes = tree.getChecked();

                if (checkedNodes.length > 0) {
                    this.el.mask();
                    var filterSpatial = new Array();
                    Ext.each(checkedNodes, function(node) {
                                filterSpatial.push(node.attributes.entity);
                            }, this);
                    this.applySpatialFilter(filterSpatial);
                }

            },

            /**
             * Apply the provided Array of Location as a SpatialFilter to all Charts in the current tab.
             * @param {Array} filterSpatial
             */
            applySpatialFilter                               : function(filterSpatial) {
                Viz.Services.EnergyWCF.PortalTab_ApplySpatialFilter({
                            Key           : this.getActiveKeyPortalTab(),
                            filterSpatial : filterSpatial,
                            success       : function(response) {
                                this.el.unmask();
                                this.refreshActiveTabPortlets(false);
                            },
                            scope         : this
                        });
            },

            /**
             * Refresh all the portlets of the active tab.
             */
            refreshActiveTabPortlets                         : function() {
                if (this.entity.PreloadTab === true) {
                    this.getActivePortalTab().setDisabledOtherTabs(true);
                    this.el.mask($lang('msg_portaltab_preload'), 'x-mask-loading-modernui');

                    var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                pollingInterval   : 1000,
                                serviceStart      : Viz.Services.EnergyWCF.Chart_PreLoadAllBegin,
                                serviceParams     : {
                                    KeyPortalTab : this.getActivePortalTab().entity.KeyPortalTab,
                                    clearCache   : false
                                },
                                scope             : this,
                                isFileResult      : false,
                                showProgress      : false,
                                resultHandler : function() {
                                    this.getActivePortalTab().setDisabledOtherTabs(false);
                                    this.el.unmask();
                                    this.refreshActiveTabPortletsSub(false);
                                }
                            });
                    submitter.start();
                }
                else
                    this.refreshActiveTabPortletsSub(true);
            },

            refreshActiveTabPortletsSub                      : function(clearCache) {
                var portlets = this.getActivePortalTab().getPortlets();
                Ext.each(portlets, function(p) {
                            p.onRefresh(clearCache);
                        }, this)
            },

            /**
             * Locks the portal panel drop targets.
             */
            lockDropTargets                                  : function() {
                var columns = this.getPortalColumns();
                Ext.each(columns, function(col) {
                            col.lockDropTargets();
                        });

                var portlets = this.getPortlets();
                Ext.each(portlets, function(portlet) {
                            if (portlet.lockDropTargets)
                                portlet.lockDropTargets();
                        });
            },

            /**
             * Unlocks the the portal panel drop targets.
             */
            unlockDropTargets                                : function() {
                var columns = this.getPortalColumns();
                Ext.each(columns, function(col) {
                            col.unlockDropTargets();
                        });

                var portlets = this.getPortlets();
                Ext.each(portlets, function(portlet) {
                            if (portlet.unlockDropTargets)
                                portlet.unlockDropTargets();
                        });
            }

        });

Ext.reg('vizPortalPanel', Viz.portal.Panel);

﻿Ext.namespace('Viz.portal');

/**
 * @class Viz.portal.PortletWindow
 * @extends Ext.Window Summary.
 */
Viz.portal.PortletWindow = Ext.extend(Ext.Window, {

            /**
             * @cfg {Viz.BusinessEntity.Chart} entity The portlet entity.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor    : function(config) {

                config = config || {};
                this._portletId = Ext.id();

                var stateId = null;
                if (config.entityPortlet && config.entity)
                    stateId = 'portletWindow_' + config.xtypePortlet + '_' + config.entity[Viz.util.Portal.getPortletKey(config.entityPortlet.TypeEntity)]

                var defaultConfig = {
                    stateId       : stateId,
                    layout        : 'fit',
                    width         : config.width || 640,
                    height        : config.height || 480,
                    title         : config.entityPortlet && config.entityPortlet.Title ? Viz.util.Portal.localizeTitle(config.entityPortlet.Title):Viz.util.Portal.localizeTitle(config.entity.Title),
                    animateTarget : config.animateTarget,
                    maximizable   : true,
                    tools         : [{
                                id      : 'refresh',
                                qtip    : $lang('msg_portlet_refresh'),
                                handler : this.onRefresh,
                                scope   : this
                            }, {
                                id      : 'gear',
                                qtip    : $lang('msg_portlet_update'),
                                handler : this.onUpdateEntity,
                                hidden  : config.entityPortlet ? (!$authorized(Viz.util.Portal.getPortletAuthorization(config.entityPortlet.TypeEntity, true)) || config.readonly) : true,
                                scope   : this
                            }]
                };
                var forcedConfig = {
                    items : {
                        id            : this._portletId,
                        xtype         : config.xtypePortlet,
                        entity        : config.entity,
                        entityPortlet : config.entityPortlet,
                        header        : false,
                        border        : false,
                        draggable     : false,
                        useWindow     : true,
                        readonly      : config.readonly
                    }
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.portal.PortletWindow.superclass.constructor.call(this, config);
            },

            /**
             * Get the child chart viewer
             * @private
             */
            getPortlet     : function() {
                return Ext.getCmp(this._portletId);
            },

            /**
             * Handler for the refresh event.
             * @private
             */
            onRefresh      : function() {
                var portlet = this.getPortlet();
                if (portlet.onRefresh) {
                    portlet.onRefresh();
                }
                else if (portlet.reloadImage) {
                    portlet.reloadImage();
                }
            },

            /**
             * Handler to open the form of the portlet
             * @private
             */
            onUpdateEntity : function() {
                if (this.entity && this.updateHandler) {
                    this.updateHandler(this.entity, this);
                }
            }
        });

Ext.reg('vizPortletWindow', Viz.portal.PortletWindow);

﻿Ext.namespace('Viz.portal');

/**
 * @class Viz.portal.PortletWeather
 * @extends Ext.Panel
 * @xtype vizPortletWeather
 * <p>
 * Summary
 * </p>
 */
Viz.portal.PortletWeather = Ext.extend(Viz.portal.Portlet, {
            /**
             * @cfg {Viz.BusinessEntity.WeatherLocation} entity The WeatherLocation entity.
             * @type Viz.BusinessEntity.WeatherLocation
             */
            entity                       : null,

            /**
             * Today template
             * @type Ext.XTemplate
             * @private
             */
            todayTpl                     : null,

            /**
             * forecast template
             * @type Ext.XTemplate
             * @private
             */
            forecastTpl                  : null,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                  : function(config) {
                this._updatingLayout = false;
                config = config || {};
                var defaultConfig = {
                    updateHandler          : Viz.grid.WeatherLocation.update,
                    largeDisplayMinHeight  : 380,
                    largeDisplayMinWidth   : 320,
                    mediumDisplayMinHeight : 240,
                    mediumDisplayMinWidth  : 200,
                    loadingImageCls        : 'viz-chart-loading',
                    iconCls                : 'viz-icon-small-weatherlocation'
                };
                var forcedConfig = {
                    bodyStyle : 'padding : 2px'
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.portal.PortletWeather.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("WeatherLocationChange", this.onWeatherLocationChange, this);
            },

            /**
             * Destroy
             * @method onDestroy
             * @private
             */
            onDestroy                    : function() {
                Viz.util.MessageBusMgr.unsubscribe("WeatherLocationChange", this.onWeatherLocationChange, this);
                Viz.portal.PortletWeather.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Listener for the refresh event.
             * @method onRefresh
             * @private
             */
            onRefresh                    : function() {
                this.reloadWeather();
            },

            /*
             * Handler for the afterrender event.
             */
            onAfterRender                : function() {
                Viz.portal.PortletWeather.superclass.onAfterRender.apply(this, arguments);
                this.reloadWeather();
            },
            /**
             * Porltet resize event handler.
             */
            onPortletResize              : function() {
                this.updateWeatherLayout();
            },

            /**
             * Reload the Weather information and rebuild the template.
             */
            reloadWeather                : function() {
                if (!this.checkBody())
                    return;

                var loadingDiv = Ext.DomHelper.overwrite(this.body, {
                            tag : 'div',
                            cls : this.loadingImageCls
                        });
                Ext.DomHelper.applyStyles(loadingDiv, 'width:100%; height:100%;');
                Viz.Services.EnergyWCF.WeatherLocation_GetFiveDayForecast({

                            KeyWeatherLocation : this.entity.KeyWeatherLocation,
                            success            : function(weatherForecasts) {
                                this.buildTemplate(weatherForecasts);
                            },
                            scope              : this

                        });
            },

            checkBody                    : function() {
                return this.rendered && Ext.isDefined(this.body) && Ext.isDefined(this.body.dom);
            },

            /**
             * Build and apply the template of the body.
             * @param {Array} weatherForecasts
             */
            buildTemplate                : function(weatherForecast) {
                if (!this.checkBody())
                    return;

                var todayData = weatherForecast[0];
                var displayType;
                this.body.removeClass('vertical');
                this.body.removeClass('horizontal');
                this.body.removeClass('metroOneDay');
                this.body.removeClass('metroFullForecast');
                switch (this.entity.DisplayType) {
                    case Vizelia.FOL.BusinessEntities.WeatherDisplayType.Horizontal :
                        this.body.addClass('horizontal');
                        this.getTemplateHorizontal();
                        break;
                    case Vizelia.FOL.BusinessEntities.WeatherDisplayType.Vertical :
                        this.body.addClass('vertical');
                        this.getTemplateVertical();
                        break;
                    case Vizelia.FOL.BusinessEntities.WeatherDisplayType.MetroOneDay :
                        this.body.addClass('metroOneDay');
                        this.getTemplateMetroOneDay();
                        break;
                    case Vizelia.FOL.BusinessEntities.WeatherDisplayType.MetroFullForecast :
                        this.body.addClass('metroFullForecast');
                        this.getTemplateMetroFullForecast();
                        break;
                }

                // this.body.dom.className = ' ' + displayType;
                if (this.entity.DisplayFontFamily != null) {
                    Ext.DomHelper.applyStyles(this.body, 'font-family:' + this.entity.DisplayFontFamily);
                }
                this.todayTpl.overwrite(this.body, todayData);
                if (this.forecastTpl)
                    this.forecastTpl.append(this.body, weatherForecast);
                this.updateWeatherLayout();
            },
            /**
             * Updates the portlet layout according to the change of size and/or entity WeatherLocation.
             */
            updateWeatherLayout          : function() {
                if (!this._updatingLayout) {
                    this._updatingLayout = true;
                    try {
                        if (this.rendered) {
                            if (!Ext.isIE6 && !Ext.isIE7 && !Ext.isIE8)
                                Ext.DomHelper.applyStyles(this.body, 'background-color: null');
                            var bodyWidth = this.body.getWidth() - 4; // 2px padding
                            var bodyHeight = this.body.getHeight() - 4; // 2px padding
                            switch (this.entity.DisplayType) {
                                case Vizelia.FOL.BusinessEntities.WeatherDisplayType.Horizontal :
                                    var boxWidth = bodyWidth / 6;
                                    var boxMargin = Math.floor(boxWidth / 50);
                                    var fontSize = Math.round(boxWidth / 10);
                                    var boxPadding = 5;
                                    boxWidth = Math.floor(boxWidth - (2 * boxMargin) - (2 * boxPadding) - 2); // the extra 2 is the border
                                    var titleNode = Ext.DomQuery.selectNode("div.weatherTitle");
                                    if (titleNode == undefined)
                                        titleNode = Ext.DomQuery.selectNode("div.weatherTitle");
                                    var boxHeight = bodyHeight - (titleNode ? titleNode.clientHeight : 21) - (2 * boxMargin) - (2 * boxPadding) - 2; // the extra 2 is the border
                                    Ext.each(this.body.query("span.weatherRow"), function(node) {
                                                var nodeStyle = 'height:' + boxHeight + 'px; width:' + boxWidth + 'px; margin:' + boxMargin + 'px; padding:' + boxPadding + 'px; font-size:' + fontSize + 'px;';
                                                if (this.entity.TextColor != null)
                                                    nodeStyle += ' color:#' + this.entity.TextColor + ';';
                                                if (this.entity.MainBackgroundColor != null)
                                                    nodeStyle += ' background-color: #' + this.entity.MainBackgroundColor + ';';
                                                Ext.DomHelper.applyStyles(node, nodeStyle);
                                            }, this);
                                    break;
                                case Vizelia.FOL.BusinessEntities.WeatherDisplayType.Vertical :
                                    var minWidthHeight = Math.min(bodyHeight, bodyWidth);
                                    var bigFontSize = Math.round(minWidthHeight / 8);
                                    var fontSize = Math.round(minWidthHeight / 18);
                                    var bigRowEl = this.body.child('.weatherRow.first');
                                    var titleFontSize = bodyHeight * (3 / 100) - 2;
                                    var titleElements = Ext.isIE8 || Ext.isIE7 ? document.querySelectorAll('.weatherTitle') : document.getElementsByClassName('weatherTitle');
                                    Ext.each(titleElements, function(node) {
                                        Ext.DomHelper.applyStyles(node, 'font-size:' + titleFontSize + 'px');
                                    } );

                                    var from, to;
                                    if (bigRowEl != null) {
                                        Ext.DomHelper.applyStyles(bigRowEl.child('.tempDesc'), 'font-size:' + bigFontSize + 'px');
                                        var bigIconEl;
                                        if (bodyHeight < this.largeDisplayMinHeight || bodyWidth < this.largeDisplayMinWidth) {
                                            from = /large/g;
                                            to = 'medium';
                                            bigIconEl = bigRowEl.child('.viz-weather-large-icon');
                                        }
                                        else {
                                            from = /medium/g;
                                            to = 'large';
                                            bigIconEl = bigRowEl.child('.viz-weather-medium-icon');
                                        }
                                        if (bigIconEl != null)
                                            bigIconEl.dom.className = bigIconEl.dom.className.replace(from, to);
                                    }
                                    if (bodyHeight < this.mediumDisplayMinHeight || bodyWidth < this.mediumDisplayMinWidth) {
                                        from = /medium/g;
                                        to = 'small';
                                    }
                                    else {
                                        from = /small/g;
                                        to = 'medium';
                                    }
                                    Ext.each(this.body.query("div.weatherRow"), function(node) {
                                                var nodeStyle = 'font-size:' + fontSize + 'px;';
                                                if (this.entity.TextColor != null)
                                                    nodeStyle += ' color:#' + this.entity.TextColor + ';';
                                                Ext.DomHelper.applyStyles(node, nodeStyle);
                                            }, this);
                                    Ext.each(this.body.query("div.weatherRow .iconCt"), function(node) {
                                                node.childNodes[0].className = node.childNodes[0].className.replace(from, to);
                                            }, this);
                                    break;
                                case Vizelia.FOL.BusinessEntities.WeatherDisplayType.MetroOneDay :
                                    if (this.entity.MainBackgroundColor != null) {
                                        Ext.DomHelper.applyStyles(this.body, 'background-color: #' + this.entity.MainBackgroundColor);
                                    }
                                    if (this.entity.TextColor != null) {
                                        Ext.DomHelper.applyStyles(this.body, 'color: #' + this.entity.TextColor);
                                    }
                                    var marginTop = (bodyHeight - 80) / 2; // tile icon height is 80px.
                                    Ext.each(this.body.query(".viz-weather-tile-icon"), function(node) {
                                                var nodeStyle = 'margin-top:' + marginTop + 'px;';
                                                Ext.DomHelper.applyStyles(node, nodeStyle);
                                            }, this);
                                    marginTop = (bodyHeight - 120) / 2; // text height is 120px.
                                    Ext.each(this.body.query(".dayDesc"), function(node) {
                                                var nodeStyle = 'margin-top:' + marginTop + 'px;';
                                                Ext.DomHelper.applyStyles(node, nodeStyle);
                                            }, this);
                                    break;
                                case Vizelia.FOL.BusinessEntities.WeatherDisplayType.MetroFullForecast :
                                    if (this.entity.MainBackgroundColor != null) {
                                        Ext.DomHelper.applyStyles(this.body, 'background-color: #' + this.entity.MainBackgroundColor);
                                    }
                                    if (this.entity.TextColor != null) {
                                        Ext.DomHelper.applyStyles(this.body, 'color: #' + this.entity.TextColor);
                                    }
                                    var boxWidth = (bodyWidth - 10) / 5;
                                    var boxMargin = Math.floor(boxWidth / 50);
                                    var fontSize = Math.round(boxWidth / 10);
                                    var boxPadding = 5;
                                    boxWidth = Math.floor(boxWidth - (2 * boxMargin) - (2 * boxPadding) - 2); // the extra 2 is the border
                                    var titleNode = Ext.DomQuery.selectNode("div.weatherTitle");
                                    if (titleNode == undefined)
                                        titleNode = Ext.DomQuery.selectNode("div.weatherTitle");
                                    var boxHeight = bodyHeight - (titleNode ? titleNode.clientHeight : 21) - (2 * boxMargin) - (2 * boxPadding) - 2; // the extra 2 is the border
                                    Ext.each(this.body.query("span.weatherRow"), function(node) {
                                                var nodeStyle = 'height:' + boxHeight + 'px; width:' + boxWidth + 'px; margin:' + boxMargin + 'px; padding:' + boxPadding + 'px; font-size:' + fontSize + 'px;';
                                                Ext.DomHelper.applyStyles(node, nodeStyle);
                                            }, this);
                                    break;
                            }
                            this._updatingLayout = false;
                        }
                    }
                    catch (error) {
                        this._updatingLayout = false;
                    }
                }

            },

            /**
             * Listener for the weather location change event.
             * @param {Viz.BusinessEntity.WeatherLocation} entity
             * @private
             */
            onWeatherLocationChange      : function(entity) {
                if (this.entity.KeyWeatherLocation == entity.KeyWeatherLocation) {
                    this.entity = entity;
                    this.reloadWeather();
                }
            },
            getTemplateHorizontal        : function() {
                this.todayTpl = new Ext.XTemplate('<tpl for="."><div class="weatherTitle">{[Ext.util.Format.htmlEncode(values.LocationTitle)]}</div>',
                        /**/
                        '<span class="weatherRow">',
                        /**/
                        '<div class="dayDesc" ext:qtip="{[this.dayText()]}">{[this.dayText()]}</div>',
                        /**/
                        '<div class="viz-weather-medium-icon viz-weather-medium-{[Ext.util.Format.htmlEncode(values.DayIconNumber)]}"></div>',
                        /**/
                        '<div class="tempDesc" ext:qtip="{[this.tempText(values)]}">{[this.tempText(values)]}</div>',
                        /**/
                        '<div class="other" ext:qtip="{[this.humidityText(values)]}">{[this.humidityText(values)]}</div>',
                        /**/
                        '<div class="other" ext:qtip="{[this.timeText(values)]}">{[this.timeText(values)]}</div>',
                        /**/
                        '</span></tpl>', {
                            timeText     : function(values) {
                                var currTime = values.CurrentTime;
                                return Ext.util.Format.htmlEncode($lang('msg_time') + ': ' + currTime); // .format(Viz.getLocalizedTimeFormat(false));
                            },
                            humidityText : function(values) {
                                return Ext.util.Format.htmlEncode($lang('msg_humidity') + ': ' + values.Humidity);
                            },
                            tempText     : function(values) {
                                return Ext.util.Format.htmlEncode(values.CurrentTemperature + '\u00B0' + $lang("msg_weatherunit_" + values.Unit));
                            },
                            dayText      : function() {
                                return Ext.util.Format.htmlEncode($lang('msg_current'));
                            }
                        });

                this.forecastTpl = new Ext.XTemplate('<tpl for="."><span class="weatherRow">',
                        /**/
                        '<div class="dayDesc" ext:qtip="{[this.dayOfTheWeek(values.Date)]}">{[this.dayOfTheWeek(values.Date)]}</div>',
                        /**/
                        '<div class="viz-weather-medium-icon viz-weather-medium-{[Ext.util.Format.htmlEncode(values.DayIconNumber)]}"></div>',
                        /**/
                        '<div class="tempDesc" ext:qtip="{[this.tempText(values)]}">{[this.tempText(values)]}</div>',
                        /**/
                        '<div class="other" ext:qtip="{[this.sunriseText(values)]}">{[this.sunriseText(values)]}</div>',
                        /**/
                        '<div class="other" ext:qtip="{[this.sunsetText(values)]}">{[this.sunsetText(values)]}</div>',
                        /**/
                        '</span></tpl>', {
                            dayOfTheWeek : function(date) {
                                return Ext.util.Format.htmlEncode($lang("msg_day_" + (date.getDay() + 1)));
                            },
                            sunriseText  : function(values) {
                                return Ext.util.Format.htmlEncode($lang('msg_sunrise') + ': ' + values.SunriseTime); // .format(Viz.getLocalizedTimeFormat(false));
                            },
                            sunsetText   : function(values) {
                                return Ext.util.Format.htmlEncode($lang('msg_sunset') + ': ' + values.SunsetTime); // .format(Viz.getLocalizedTimeFormat(false));
                            },
                            tempText     : function(values) {
                                return Ext.util.Format.htmlEncode((Ext.isNumber(values.LowTemperature) ? (values.LowTemperature + '\u00B0' + $lang("msg_weatherunit_" + values.Unit)) : '-') + ' | ' + (Ext.isNumber(values.HighTemperature) ? (values.HighTemperature + '\u00B0' + $lang("msg_weatherunit_" + values.Unit)) : '-'));
                            }
                        });
            },
            getTemplateVertical          : function() {
                this.todayTpl = new Ext.XTemplate('<tpl for="."><div class="weatherTitle">{[Ext.util.Format.htmlEncode(values.LocationTitle)]}</div><div class="weatherRow first dark" style="{[this.rowBackgroundColor("' + this.entity.MainBackgroundColor + '")]}">',
                        /**/
                        '<span class="dayDesc">{[Ext.util.Format.htmlEncode(values.Title)]}</span>',
                        /**/
                        '<span class="iconCurrentCt"><span class="viz-weather-large-icon viz-weather-large-{[Ext.util.Format.htmlEncode(values.DayIconNumber)]}"></span></span>',
                        /**/
                        '<span class="tempDesc">{[this.tempText(values)]}</span>',
                        /**/
                        '</div></tpl>', {
                            rowBackgroundColor : function(color) {
                                color = Ext.util.Format.htmlEncode(color);
                                return color != null ? "background-color: #" + color : "";
                            },

                            tempText           : function(values) {
                                return Ext.util.Format.htmlEncode(values.CurrentTemperature) + '\u00B0' + Ext.util.Format.htmlEncode($lang("msg_weatherunit_" + values.Unit));
                            }
                        });

                this.forecastTpl = new Ext.XTemplate('<tpl for="."><div class="weatherRow {[this.rowClass(xindex, xcount)]}" style="{[this.rowBackgroundColor(xindex, "' + this.entity.MainBackgroundColor + '","' + this.entity.AlternativeBackgroundColor + '")]}">',
                        /**/
                        '<span class="dayDesc">{[this.dayOfTheWeek(values.Date)]}</span>',
                        /**/
                        '<span class="iconCt"><span class="viz-weather-medium-icon viz-weather-medium-{[Ext.util.Format.htmlEncode(values.DayIconNumber)]}"></span></span>',
                        /**/
                        '<span class="tempDesc">{[this.tempText(values)]}</span>',
                        /**/
                        '</div></tpl>', {
                            dayOfTheWeek : function(date) {
                                return Ext.util.Format.htmlEncode($lang("msg_day_" + (date.getDay() + 1)));
                            },

                            tempText     : function(values) {
                                return Ext.util.Format.htmlEncode((Ext.isNumber(values.LowTemperature) ? (values.LowTemperature + '\u00B0' + $lang("msg_weatherunit_" + values.Unit)) : '-') + ' | ' + (Ext.isNumber(values.HighTemperature) ? (values.HighTemperature + '\u00B0' + $lang("msg_weatherunit_" + values.Unit)) : '-'));
                            }
                        }, {
                            rowClass : function(xindex, xcount) {
                                var retVal = xindex % 2 == 0 ? "dark" : "light";
                                retVal += xindex == xcount ? " last" : "";
                                return Ext.util.Format.htmlEncode(retVal);
                            }
                        }, {
                            rowBackgroundColor : function(xindex, mainBackgroundColor, alternativeBackgroundColor) {
                                var retVal = xindex % 2 == 0 ? mainBackgroundColor : alternativeBackgroundColor;
                                return Ext.util.Format.htmlEncode(retVal != null ? "background-color: #" + retVal : "");
                            }
                        });
            },
            getTemplateMetroFullForecast : function() {
                this.todayTpl = new Ext.XTemplate('<tpl for=".">',
                        /**/
                        '<div class="weatherTitle">',
                        /**/
                        '<div><span style="font-size: 70px; line-height: 60px;">{[this.tempText(values)]}</span>',
                        /**/
                        '<span style="font-size: 16px;">{[Ext.util.Format.htmlEncode(values.Title)]}</span></div>',
                        /**/
                        '<div style="font-size: 20px;">{[Ext.util.Format.htmlEncode(values.LocationTitle)]}</span></div>',
                        /**/
                        '</div></tpl>', {
                            tempText : function(values) {
                                return Ext.util.Format.htmlEncode(values.CurrentTemperature) + '\u00B0';
                            }
                        });

                this.forecastTpl = new Ext.XTemplate('<tpl for="."><span class="weatherRow weatherRow{#}">',
                        /**/
                        '<div class="dayDesc" ext:qtip="{[this.dayOfTheWeek(values.Date)]}">{[this.dayOfTheWeek(values.Date)]}</div>',
                        /**/
                        '<div class="viz-weather-tile-icon viz-weather-tile-{[Ext.util.Format.htmlEncode(values.DayIconNumber)]}"></div>',
                        /**/
                        '<div class="tempDesc" ext:qtip="{[this.tempText(values)]}">{[this.tempText(values)]}</div>',
                        /**/
                        '<div class="other" ext:qtip="{[Ext.util.Format.htmlEncode(values.Title)]}">{[Ext.util.Format.htmlEncode(values.Title)]}</div>',
                        /**/
                        '<div class="other" ext:qtip="{[this.sunriseText(values)]}">{[this.sunriseText(values)]}</div>',
                        /**/
                        '<div class="other" ext:qtip="{[this.sunsetText(values)]}">{[this.sunsetText(values)]}</div>',
                        /**/
                        '</span></tpl>', {
                            dayOfTheWeek : function(date) {
                                return Ext.util.Format.htmlEncode($lang("msg_day_" + (date.getDay() + 1)) + ' ' + date.dateFormat('d'));
                            },
                            sunriseText  : function(values) {
                                return Ext.util.Format.htmlEncode($lang('msg_sunrise') + ': ' + values.SunriseTime); // .format(Viz.getLocalizedTimeFormat(false));
                            },
                            sunsetText   : function(values) {
                                return Ext.util.Format.htmlEncode($lang('msg_sunset') + ': ' + values.SunsetTime); // .format(Viz.getLocalizedTimeFormat(false));
                            },
                            tempText     : function(values) {
                                return Ext.util.Format.htmlEncode((Ext.isNumber(values.LowTemperature) ? (values.LowTemperature + '\u00B0' + $lang("msg_weatherunit_" + values.Unit)) : '-') + ' | ' + (Ext.isNumber(values.HighTemperature) ? (values.HighTemperature + '\u00B0' + $lang("msg_weatherunit_" + values.Unit)) : '-'));
                            }
                        });
            },
            getTemplateMetroOneDay       : function() {
                this.todayTpl = new Ext.XTemplate('<tpl for=".">',
                        /**/
                        '<div class="viz-weather-tile-icon viz-weather-tile-{[Ext.util.Format.htmlEncode(values.DayIconNumber)]}"></div>',
                        /**/
                        '<div class="dayDesc" style="margin-left: 30px; display: inline-block; position: absolute;">',
                        /***/
                        '<span style="font-size: 48px;">{[this.tempText(values)]}</span>',
                        /**/
                        '<span style="font-size: 20px;">{[Ext.util.Format.htmlEncode(values.LocationTitle)]}</span>',
                        /**/
                        '<span style="font-size: 20px;">{[Ext.util.Format.htmlEncode(values.Title)]}</span>',
                        /**/
                        '</div></tpl>', {
                            tempText : function(values) {
                                return Ext.util.Format.htmlEncode(values.CurrentTemperature + '\u00B0 ' + $lang("msg_weatherunit_" + values.Unit));
                            }
                        });
                this.forecastTpl = null;
            },

            getBackgroundColor           : function() {
                return '#' + this.entity.MainBackgroundColor;
            }
        });

Ext.reg('vizPortletWeather', Viz.portal.PortletWeather);

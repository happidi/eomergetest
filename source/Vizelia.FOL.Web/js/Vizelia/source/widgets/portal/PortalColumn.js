﻿Ext.namespace('Viz.portal');

/**
 * @class Viz.portal.PortalColumn
 * @extends Ext.ux.PortalColumn Summary.
 */
Viz.portal.PortalColumn = Ext.extend(Ext.ux.PortalColumn, {

    defaultType: 'vizPortlet',
    /**
    * Ctor.
    * @param {Object} config The configuration options
    */
    constructor: function (config) {
        config = config || {};
        var defaultConfig = {};
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.portal.PortalColumn.superclass.constructor.call(this, config);

        this.on('afterrender', this.onAfterRender, this);
        this.on('resize', this.onColumnResize, this);

        this.setColumnHeightTask = new Ext.util.DelayedTask(this.setColumnHeight, this);
    },

    /**
    * AfterRender Handler to register the dropTarget for the Portlet Tree.
    */
    onAfterRender: function () {
        // this.setColumnHeight();
        Ext.get(this.id).on('click', function (e) {
            if (this.items.length == 0) {
                this.el.frame("ff0000");
            }
        }, this);

        Ext.get(this.id).on('contextmenu', function (event) {
            this.onContextMenu(event);
        }, this);

        this.dropTargetPortlet = new Ext.dd.DropTarget(this.el.id, {
            // id : 'PortalColumnPortletDD' + this.el.id,
            ddGroup: 'PortletDD',
            notifyDrop: this.onNotifyDrop.createDelegate(this)
        });
        this.dropTargetPortletSpatial = new Ext.dd.DropTarget(this.el.id, {
            // id : 'PortalColumnSpatialDD' + this.el.id,
            ddGroup: 'SpatialDD',
            notifyDrop: this.onNotifyDrop.createDelegate(this)
        });
        this.dropTargetPortletClassification = new Ext.dd.DropTarget(this.el.id, {
            // id : 'PortalColumnClassificationDD' + this.el.id,
            ddGroup: 'ClassificationDD',
            notifyDrop: this.onNotifyDrop.createDelegate(this)
        });
    },

    /**
    * Column context menu
    * @param {} event
    */
    onContextMenu: function (event) {
        event.stopEvent();
        var ctxMenu = new Ext.menu.Menu({
            items: [{
                iconCls: 'viz-icon-small-chart',
                text: $lang('msg_chart_wizard'),
                hidden: !$authorized('@@@@ Chart - Full Control') && !$authorized('@@@@ Chart - Wizard'),
                handler: Viz.grid.Chart.launchWizard.createDelegate(this, [{
                    KeyPortalColumn: this.entity.KeyPortalColumn
                }])
            }, new Ext.menu.Separator({
                hidden: !$authorized('@@@@ Portal - Full Control') || (!$authorized('@@@@ Chart - Full Control') && !$authorized('@@@@ Chart - Wizard'))
            }), {
                text: $lang("msg_portalcolumn_add"),
                iconCls: 'viz-icon-small-add',
                hidden: !$authorized('@@@@ Portal - Full Control'),
                handler: function () {
                    var tab = this.getParentPortalTab();
                    var panel = this.getParentPortalPanel();
                    panel.tabMenu = tab;
                    panel.onOpenFormCreatePortalColumn(this.entity.Order + 1);
                },
                scope: this
            }, {
                text: $lang("msg_portalcolumn_delete"),
                iconCls: 'viz-icon-small-delete',
                hidden: !$authorized('@@@@ Portal - Full Control'),
                handler: function () {
                    var panel = this.getParentPortalPanel();
                    panel.onPortalColumnDelete(this.entity);
                },
                scope: this
            }]
        });
        ctxMenu.on("hide", function (menu) {
            menu.destroy();
            menu = null;
        }, this);
        ctxMenu.showAt(event.getXY());
    },
    /**
    * Locks the column drop targets.
    */

    lockDropTargets: function () {
        if (this.dropTargetPortlet)
            this.dropTargetPortlet.lock();
        if (this.dropTargetPortletSpatial)
            this.dropTargetPortletSpatial.lock();
        if (this.dropTargetPortletClassification)
            this.dropTargetPortletClassification.lock();
    },

    /**
    * Unlocks the column drop targets.
    */
    unlockDropTargets: function () {
        if (this.dropTargetPortlet)
            this.dropTargetPortlet.unlock();
        if (this.dropTargetPortletSpatial)
            this.dropTargetPortletSpatial.unlock();
        if (this.dropTargetPortletClassification)
            this.dropTargetPortletClassification.unlock();
    },

    /**
    * Destroy
    * @method onDestroy
    * @private
    */
    onDestroy: function () {
        this.dropTargetPortlet.destroy();
        this.dropTargetPortletSpatial.destroy();
        this.dropTargetPortletClassification.destroy();

        Viz.portal.PortalColumn.superclass.onDestroy.apply(this, arguments);
    },

    onNotifyDrop: function (dd, e, data) {
        if (data.node && data.node.attributes.entity) {
            var entity = data.node.attributes.entity;
            var type = Viz.getEntityType(entity.__type);
            var key = data.node.attributes.Key || data.node.attributes.id;
            // we check here if this the portlet root node (Chart, map etc..) that is dragged
            if (key == type) {
                key = null;
            }
            this.writeDragAndDropEventToGoogleAnalytics(dd);
            var order = this.items.length + 1;
            
            if (dd.ddGroup == 'PortletDD' && type != 'Vizelia.FOL.BusinessEntities.ClassificationItem') {
                if (!Ext.isEmpty(type))
                    this.getParentPortalPanel().addPortletFromDrop(entity.Title || $lang('msg_portlet'), key, type, this.entity.KeyPortalColumn, order);
            }
            else {
                // We need to get the timezoneid...
                var createFromDrop = function (timezoneid) {
                    if (dd.ddGroup == 'SpatialDD' && type == 'Vizelia.FOL.BusinessEntities.Meter') {
                        this.el.mask();
                        Viz.Services.EnergyWCF.Portlet_CreateFromMeter({
                            KeyMeter: key,
                            KeyPortalColumn: this.entity.KeyPortalColumn,
                            KeyChartToCopy: Viz.App.Global.User.Preferences.DefaultChartKPI,
                            Order: order,
                            TimeZoneId: timezoneid,
                            success: function (portlet) {
                                this.el.unmask();
                                Viz.util.MessageBusMgr.fireEvent('PortletChange', portlet);
                            },
                            failure: function () {
                                this.el.unmask();
                            },
                            scope: this
                        });
                    }
                    else if (dd.ddGroup == 'SpatialDD') {
                        this.el.mask();
                        Viz.Services.EnergyWCF.Portlet_CreateFromLocation({
                            KeyLocation: key,
                            KeyPortalColumn: this.entity.KeyPortalColumn,
                            KeyChartToCopy: Viz.App.Global.User.Preferences.DefaultChartKPI,
                            Order: order,
                            TimeZoneId: timezoneid,
                            success: function (portlet) {
                                this.el.unmask();
                                Viz.util.MessageBusMgr.fireEvent('PortletChange', portlet);
                            },
                            failure: function () {
                                this.el.unmask();
                            },
                            scope: this
                        });
                    }
                    else if (type == 'Vizelia.FOL.BusinessEntities.ClassificationItem' && entity.Category == 'meter') {
                        this.el.mask();
                        Viz.Services.EnergyWCF.Portlet_CreateFromClassification({
                            KeyClassificationItem: key,
                            KeyPortalColumn: this.entity.KeyPortalColumn,
                            KeyChartToCopy: Viz.App.Global.User.Preferences.DefaultChartKPI,
                            Order: order,
                            TimeZoneId: timezoneid,
                            success: function (portlet) {
                                this.el.unmask();
                                Viz.util.MessageBusMgr.fireEvent('PortletChange', portlet);
                            },
                            failure: function () {
                                this.el.unmask();
                            },
                            scope: this
                        });
                    }
                }
                Viz.util.Chart.getTimeZoneId(createFromDrop.createDelegate(this));
            }
        }
        return true;
    },

    writeDragAndDropEventToGoogleAnalytics: function (dragDrop) {
        if (dragDrop && dragDrop.tree && dragDrop.tree.title)
            Viz.writeEventToGoogleAnalytics('Left Tree', 'Drag And Drop', dragDrop.tree.title);
    },

    onColumnResize: function () {
        this.setColumnHeightTask.delay(500);
    },

    setColumnHeight: function () {
        var tab = this.getParentPortalTabRegion();
        if (tab && tab.body) {
            this.setHeight(tab.body.getSize().height - tab.body.getPadding('tb'));
        }
        else {
            // we are here when we display a unique portaltab as a simple panel in a viewport
            var viewport = this.findParentByType('viewport');
            if (viewport)
                this.setHeight(viewport.getSize().height - 5);
        }
        this.updatePortletLayout();
    },

    /*
    * @private : method to set automatic height for portlets
    */
    updatePortletLayout: function () {
    	
    	// we reset to the proper height first
    	var tab = this.getParentPortalTabRegion();
        if (tab && tab.body) {
        	if (this.el && this.el.dom) {
                var rz = this.getResizeEl();
                rz.setHeight(tab.body.getSize().height - tab.body.getPadding('tb'));
           
            }
        }
        
        var portlets = this.getPortlets();
        var totalHeight = 0;
        Ext.each(portlets, function (portlet) {
            portlet.updatePortletLayout();
            totalHeight += portlet.getHeight();
        }, this);
        
        
        try {
            if (this.el && this.el.dom) {
                var rz = this.getResizeEl();
                rz.setHeight(Math.max(totalHeight, this.getHeight()));
            }
        }
        catch (e) { ; }
    },

    /**
    * Get the child portlets.
    * @return {Array} Array of Viz.portal.Portlet
    */
    getPortlets: function () {
        var retVal = [];
        Ext.each(this.items.items, function (p) {
            if (p.isXType('vizPortlet'))
                retVal.push(p);
        }, this);
        return retVal;
        // return this.findByType('vizPortlet');
    },
    /**
    * Get the parent portal tab.
    * @return {Viz.portal.PortalTab} the parent portal tab
    */
    getParentPortalTab: function () {
        return this.findParentByType('vizPortalTab');
    },

    /**
    * Get the parent portal tab.
    * @return {Viz.portal.PortalTab} the parent portal tab
    */
    getParentPortalTabRegion: function () {
        return this.findParentByType('vizPortalTabRegion');
    },

    /**
    * Update the columnWidth base on a new totalFlex
    * @param {int} totalFlex
    */
    updateFlex: function (totalFlex) {
        this.columnWidth = this.entity.Flex / totalFlex;
        this.doLayout();
    },

    /**
    * Get the parent portal panel that conatins all the tabs.
    * @return {Viz.portal.PortalPanel} the parent portal panel
    */
    getParentPortalPanel: function () {
        return this.findParentByType('vizPortalPanel');
    }

});

Ext.reg('vizPortalColumn', Viz.portal.PortalColumn);

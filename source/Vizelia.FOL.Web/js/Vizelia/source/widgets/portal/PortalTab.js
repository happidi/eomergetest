﻿Ext.namespace('Viz.portal');

/**
 * @class Viz.portal.PortalTabRegion
 * @extends Ext.ux.Portal Summary.
 */
Viz.portal.PortalTabRegion = Ext.extend(Ext.ux.Portal, {

            /**
             * @cfg {Boolean} hideMenu True to hide the contextual menu.
             */
            hideMenu           : false,

            isActive           : false,

            defaultType        : 'vizPortalColumn',

            hideMode           : 'offsets',

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor        : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    autoScroll : false
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.portal.PortalTabRegion.superclass.constructor.call(this, config);

                this.on({
                            drop        : {
                                scope : this,
                                fn    : this.onDrop
                            },
                            afterrender : {
                                scope : this,
                                fn    : function() {
                                    this.body.setStyle('overflow-y', 'auto');
                                }
                            }
                        });
            },

            /*
             * Handler for the drop Event. @param {Object} dropEvent The dropevent event
             */
            onDrop             : function(dropEvent) {

                var parent = this.getParentPortalTab();
                parent.updatePortletLayout.defer(300, parent);

                var portlet = dropEvent.panel.entityPortlet;
                portlet.KeyPortalColumn = dropEvent.column.entity.KeyPortalColumn;

                var col = parent.getPortalColumn(portlet.KeyPortalColumn);
                var portlets = col.getPortlets();
                for (var i = 0; i < portlets.length; i++) {
                    portlets[i].entityPortlet.Order = i + 1;
                    Viz.Services.EnergyWCF.Portlet_FormUpdate({
                                item    : portlets[i].entityPortlet,
                                success : function(entity) {
                                    // Viz.util.MessageBusMgr.publish('PortletChange', entity);
                                },
                                scope   : this
                            });
                }
            },

            /**
             * Get the parent portal tab
             * @return {Viz.portal.PortalTab} the parent portal panel
             */
            getParentPortalTab : function() {
                return this.findParentByType('vizPortalTab');
            }

        });

Ext.reg('vizPortalTabRegion', Viz.portal.PortalTabRegion);

Viz.portal.PortalTab = Ext.extend(Ext.Panel, {

            /*
             * Ctor. @param {Object} config The configuration options
             */
            constructor          : function(config) {

                this._northRegionId = Ext.id();
                this._centerRegionId = Ext.id();
                this._southRegionId = Ext.id();

                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    layout    : 'border',
                    xtype     : 'panel',
                    title     : Viz.util.Portal.localizeTitle(config.entity.Title),
                    bodyStyle : 'background-color: transparent;',
                    hideMode  : 'offsets',
                    items     : [{
                                region    : 'north',
                                id        : this._northRegionId,
                                border    : false,
                                bodyStyle : 'background-color: transparent;overflow-x:hidden !important;overflow-y:auto',
                                height    : config.entity.HeaderHeight,
                                xtype     : 'vizPortalTabRegion',
                                entity    : config.entity,
                                hidden    : !(config.entity.HeaderHeight > 0),
                                items     : []
                            }, {
                                region     : 'center',
                                id         : this._centerRegionId,
                                border     : false,
                                bodyStyle  : 'background-color: transparent;overflow-x:hidden !important;overflow-y:auto',
                                autoScroll : true,
                                xtype      : 'vizPortalTabRegion',
                                entity     : config.entity,
                                items      : []
                            }, {
                                region    : 'south',
                                id        : this._southRegionId,
                                border    : false,
                                bodyStyle : 'background-color: transparent;overflow-x:hidden !important;overflow-y:auto',
                                height    : config.entity.FooterHeight,
                                xtype     : 'vizPortalTabRegion',
                                entity    : config.entity,
                                hidden    : !(config.entity.FooterHeight > 0),
                                items     : []
                            }]

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                if (!Ext.isEmpty(config.entity.BackgroundCls)) {
                    config.bodyCssClass = 'viz-playlist-background ' + config.entity.BackgroundCls;
                }

                Viz.portal.PortalTab.superclass.constructor.call(this, config);

                this.on({
                            activate    : {
                                scope : this,
                                fn    : this.onActivate
                            },
                            deactivate  : {
                                scope : this,
                                fn    : this.onDeactivate
                            },
                            contextmenu : {
                                scope : this,
                                fn    : this.onContextMenu
                            },
                            resize      : {
                                scope : this,
                                fn    : this.onPortalResize
                            }
                        });
            },

            /**
             * Displays the contextual menu.
             * @param {Viz.portal.PortalTabRegion} portal
             * @param {Ext.EventObject} event
             */
            onContextMenu        : function(portal, event) {
                event.stopEvent();
                this.getParentPortalPanel().onContextMenu(this, event);
            },

            /**
             * Handler for the activate Event.
             */
            onActivate           : function() {
                this.isActive = true;

                if (!this.populated) {
                    var parentPanel = this.getParentPortalPanel();
                    if (parentPanel && parentPanel.entity && parentPanel.entity.PreloadTab === true && !this.alreadyLoaded) {
                        this.alreadyLoaded = true;
                        this.el.mask($lang('msg_portaltab_preload'), 'x-mask-loading-modernui');
                        this.setDisabledOtherTabs(true);
                        parentPanel.setDisabled(true);
                        var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                    pollingInterval : 1000,
                                    serviceStart    : Viz.Services.EnergyWCF.Chart_PreLoadAllBegin,
                                    serviceParams   : {
                                        KeyPortalTab : this.entity.KeyPortalTab,
                                        clearCache   : false
                                    },
                                    scope           : this,
                                    isFileResult    : false,
                                    showProgress    : false,
                                    resultHandler   : function(portalTab) {
                                        this.entity = portalTab;
                                        this.populate();                                        
                                        this.setDisabledOtherTabs(false);
                                        parentPanel.setDisabled(false);
                                        this.el.unmask.defer(2000, this.el);                                        
                                    }
                                });
                        submitter.start();
                    }
                    else
                        this.populate();
                }
                else
                    this.updatePortletLayout.defer(1000, this);
            },

            setDisabledOtherTabs : function(disabled) {
                var parentTab = this.getParentPortalPanel();
                var tabs = parentTab.getPortalTabs();
                for (var i = 0; i < tabs.length; i++) {
                    if (tabs[i].entity != this.entity) {
                        tabs[i].setDisabled(disabled);
                    }
                }
            },

            /**
             * Populate the tab with all the portlets
             */
            populate             : function() {
                if (this.entity.Columns && this.entity.Columns.length > 0) {

                    var headerCount = 0;
                    var mainCount = 0;
                    var footerCount = 0;

                    Ext.each(this.entity.Columns, function(col) {
                                if (col.Type == Vizelia.FOL.BusinessEntities.PortalColumnType.Header)
                                    headerCount += 1;
                                if (col.Type == Vizelia.FOL.BusinessEntities.PortalColumnType.Main)
                                    mainCount += 1;
                                if (col.Type == Vizelia.FOL.BusinessEntities.PortalColumnType.Footer)
                                    footerCount += 1;
                            }, this);

                    var headerInsert = 0;
                    var mainInsert = 0;
                    var footerInsert = 0;

                    Ext.each(this.entity.Columns, function(col) {
                                if (col.Type == Vizelia.FOL.BusinessEntities.PortalColumnType.Header) {
                                    this.getHeaderRegion().add(new Viz.portal.PortalColumn(Viz.util.Portal.getPortalColumnConfig(col, this.entity.HeaderTotalFlex, headerInsert, headerCount)));
                                    headerInsert += 1;
                                }
                                if (col.Type == Vizelia.FOL.BusinessEntities.PortalColumnType.Main) {
                                    this.getMainRegion().add(new Viz.portal.PortalColumn(Viz.util.Portal.getPortalColumnConfig(col, this.entity.TotalFlex, mainInsert, mainCount)));
                                    mainInsert += 1;
                                }
                                if (col.Type == Vizelia.FOL.BusinessEntities.PortalColumnType.Footer) {
                                    this.getFooterRegion().add(new Viz.portal.PortalColumn(Viz.util.Portal.getPortalColumnConfig(col, this.entity.FooterTotalFlex, footerInsert, footerCount)));
                                    footerInsert += 1;
                                }
                            }, this);
                    this.populated = true;
                    this.doLayout();
                }
            },

            /**
             * Remove a column based on it s type
             * @param {} col
             */
            removeColumn         : function(col) {
                if (col && col.entity) {
                    switch (col.entity.Type) {
                        case Vizelia.FOL.BusinessEntities.PortalColumnType.Header :
                            this.getHeaderRegion().remove(col);
                            break;

                        case Vizelia.FOL.BusinessEntities.PortalColumnType.Main :
                            this.getMainRegion().remove(col);
                            break;

                        case Vizelia.FOL.BusinessEntities.PortalColumnType.Footer :
                            this.getFooterRegion().remove(col);
                            break;
                    }
                }
            },

            /**
             * Insert a column based on it s type
             * @param {} col
             */
            insertColumn         : function(type, index, col) {
                switch (type) {
                    case Vizelia.FOL.BusinessEntities.PortalColumnType.Header :
                        this.getHeaderRegion().insert(index, col);
                        break;

                    case Vizelia.FOL.BusinessEntities.PortalColumnType.Main :
                        this.getMainRegion().insert(index, col);
                        break;

                    case Vizelia.FOL.BusinessEntities.PortalColumnType.Footer :
                        this.getFooterRegion().insert(index, col);
                        break;
                }

            },

            getHeaderRegion      : function() {
                return Ext.getCmp(this._northRegionId);
            },

            getMainRegion        : function() {
                return Ext.getCmp(this._centerRegionId);
            },

            getFooterRegion      : function() {
                return Ext.getCmp(this._southRegionId);
            },

            /**
             * Handler for the activate Event.
             */
            onDeactivate         : function() {
                this.isActive = false;
            },

            /**
             * Handler for the resize Event.
             */
            onPortalResize       : function() {
                if (this.rendered) {
                    this.updatePortletLayout.defer(1000, this);
                }
            },

            /**
             * Get the child portlets.
             * @return {Array} Array of Viz.portal.Portlets
             */
            getPortlets          : function() {
                var columns = this.getPortalColumns();
                var retVal = [];
                Ext.each(columns, function(col) {
                            retVal = retVal.concat(col.getPortlets());
                        }, this);
                return retVal;
                // return this.findByType('vizPortlet');
            },
            /**
             * Get the child columns.
             * @return {Array} Array of Viz.portal.PortalColumn
             */
            getPortalColumns     : function() {
                return this.findByType('vizPortalColumn');
            },

            /**
             * Get a specific column.
             * @param {string} KeyPortalColumn: the Key of the portal column
             * @return {Viz.portal.PortalColumn} the column
             */
            getPortalColumn      : function(KeyPortalColumn) {
                var cols = this.getPortalColumns();
                var result = null;
                Ext.each(cols, function(col) {
                            if (col.entity && col.entity.KeyPortalColumn == KeyPortalColumn) {
                                result = col;
                                return false;
                            }
                        }, this)
                return result;
            },

            /**
             * Get the parent portal panel that conatins all the tabs.
             * @return {Viz.portal.PortalPanel} the parent portal panel
             */
            getParentPortalPanel : function() {
                return this.findParentByType('vizPortalPanel');
            },

            /*
             * @private : method to set automatic height for portlets
             */
            updatePortletLayout  : function() {
                var cols = this.getPortalColumns();
                Ext.each(cols, function(column) {
                            column.updatePortletLayout();
                        }, this);
            }
        });
Ext.reg('vizPortalTab', Viz.portal.PortalTab);

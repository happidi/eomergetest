﻿Ext.namespace('Viz.portal');

/**
 * @class Viz.portal.PortletFlipCard
 * @extends Ext.Panel
 * @xtype vizPortletFlipCard
 * <p>
 * Summary
 * </p>
 */
Viz.portal.PortletFlipCard = Ext.extend(Viz.portal.Portlet, {
            /**
             * @cfg {Viz.BusinessEntity.FlipCard} entity The FlipCard entity.
             * @type Viz.BusinessEntity.FlipCard
             */
            entity                    : null,

            _activeCard               : -1,

            _cardPortletId            : Ext.id(),
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor               : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    updateHandler : Viz.grid.FlipCard.update,
                    layout        : 'fit',
                    iconCls       : 'viz-icon-small-flipcard',
                    style         : 'padding:0px;background-color:transparent'
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.portal.PortletFlipCard.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("FlipCardChange", this.onFlipCardChange, this);
                Viz.util.MessageBusMgr.subscribe("FlipCardEntryChange", this.onFlipCardEntryChange, this);
            },

            /**
             * Destroy
             */
            onDestroy                 : function() {
                Viz.util.MessageBusMgr.unsubscribe("FlipCardEntryChange", this.onFlipCardEntryChange, this);
                Viz.util.MessageBusMgr.unsubscribe("FlipCardChange", this.onFlipCardChange, this);
                Viz.portal.PortletFlipCard.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the afterrender event.
             */
            onAfterRender             : function() {
                Viz.portal.PortletFlipCard.superclass.onAfterRender.apply(this, arguments);
                this.el.setStyle('cursor', 'help');
                this.buildTemplate.defer(500, this);
            },

            /**
             * Handler for the FlipCard change
             * @param {Viz.BusinessEntity.FlipCard}} entity
             */
            onFlipCardChange          : function(entity) {
                if (this.entity.KeyFlipCard == entity.KeyFlipCard) {
                    this.entity = entity;
                    this._activeCard = -1;
                    this.buildTemplate();
                }
            },
            /**
             * @param {Viz.BusinessEntity.FlipCardEntry}} entity
             */
            onFlipCardEntryChange     : function(entity) {
                if (this.entity.KeyFlipCard == entity.KeyFlipCard) {
                    Viz.Services.EnergyWCF.FlipCard_GetItem({
                                Key     : this.entity.KeyFlipCard,
                                success : function(flipcard) {
                                    this.entity = flipcard;
                                    this._activeCard = -1;
                                    this.buildTemplate();
                                },
                                scope   : this
                            })
                }
            },

            /**
             * Build and apply the template of the body.
             */
            buildTemplate             : function() {
                if (this._activeCard == -1) {
                    this.buildTemplateAfter();
                }
                else {
                    $(this.el.dom).flip({
                                direction       : 'tb',
                                // we need to get the next background color not the existing one.
                                // color : this.getCurrentBackgroundColor(),
                                dontChangeColor : true,
                                onAnimation     : this.buildTemplateAfter.createDelegate(this)
                            });
                }
            },

            /**
             * Build the template after the flip animation.
             */
            buildTemplateAfter        : function() {
                this.removeAll();
                this._cardPortletId = Ext.id();
                var activeCard = this.getActiveCard();
                if (activeCard) {
                    var portletConfig = Viz.util.Portal.getPortletConfig(activeCard);

                    portletConfig.useWindow = true;
                    portletConfig.readonly = true;
                    portletConfig.border = false;
                    portletConfig.header = false;
                    portletConfig.disableDblClick = true;
                    portletConfig.hideMenu = true;
                    portletConfig.style = 'padding:0px;background-color:transparent';
                    delete portletConfig.title;
                    portletConfig.id = this._cardPortletId;
                    this.add(portletConfig);
                    this.doLayout();
                }
            },

            getCurrentBackgroundColor : function() {
                var child = Ext.getCmp(this._cardPortletId);
                if (child && child.getBackgroundColor) {
                    return child.getBackgroundColor();
                };
                return null;
            },

            /** Return the Active Tab */
            getActiveCard             : function() {
                if (this.entity.Cards && this.entity.Cards.length > 0) {
                    if (this.entity.Random) {
                        var index = Math.randRange(0, this.entity.Cards.length - 1);
                        return this.entity.Cards[index];
                    }
                    else {
                        if (this._activeCard < 0 || this._activeCard >= this.entity.Cards.length - 1) {
                            this._activeCard = 0;
                        }
                        else {
                            this._activeCard += 1;
                        }
                        return this.entity.Cards[this._activeCard];
                    }
                }
                return null;
            },
            /**
             * Listener for the refresh event.
             * @method onRefresh
             * @private
             */
            onRefresh                 : function() {
                if (this.entity.FlipOnRefresh) {
                    this.buildTemplate();
                }
                else {
                    var child = Ext.getCmp(this._cardPortletId);
                    if (child && child.onRefresh) {
                        child.onRefresh();
                    }
                }
            },

            onDblClick                : function(e) {
                e.stopEvent();
                this.buildTemplate();
            }
        });

Ext.reg('vizPortletFlipCard', Viz.portal.PortletFlipCard);
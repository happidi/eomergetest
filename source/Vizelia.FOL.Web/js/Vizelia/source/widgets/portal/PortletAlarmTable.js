﻿Ext.namespace('Viz.portal');

/**
 * @class Viz.portal.PortletAlarmTable
 * @extends Ext.Panel
 * @xtype vizPortletAlarmTable
 */
Viz.portal.PortletAlarmTable = Ext.extend(Viz.portal.Portlet, {
            /**
             * @cfg {Viz.BusinessEntity.AlarmTable} entity The AlarmTable entity.
             * @type Viz.BusinessEntity.AlarmTable
             */
            entity                 : null,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor            : function(config) {
                this._gridId = Ext.id();
                config = config || {};
                var defaultConfig = {};

                this.store = new Viz.store.AlarmTableAlarmInstance({
                            KeyAlarmTable : config.entity.KeyAlarmTable
                        });
                var forcedConfig = {
                    iconCls       : 'viz-icon-small-alarmtable',
                    bodyStyle     : 'padding : 0px',
                    layout        : 'fit',
                    updateHandler : Viz.grid.AlarmTable.update,
                    items         : {
                        id                : this._gridId,
                        stateId           : 'vizGridAlarmInstance_' + config.entity.KeyAlarmTable,
                        xtype             : 'vizGridAlarmInstance',
                        hideDeleteAll     : true,
                        enableRowDblClick : config.readonly != true,
                        tbar              : config.readonly ? [] : null,
                        border            : false,
                        store             : this.store,
                        listeners         : {
                            contextmenu : function() {
                                this.onContextMenu.apply(this, arguments);
                            },
                            scope       : this
                        }
                    }
                };
                if (config.readonly !== true)
                    delete forcedConfig.items.tbar;
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.portal.PortletAlarmTable.superclass.constructor.call(this, config);

            },
            /**
             * Destroy
             * @method onDestroy
             * @private
             */
            onDestroy              : function() {
                Viz.portal.PortletAlarmTable.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Listener for the refresh event.
             * @method onRefresh
             * @private
             */
            onRefresh              : function() {
                this.store.reload();
            },

            /**
             * we show the grid border if the header and toolbar are not visible.
             */
            toggleHeaderAndToolbar : function(show) {
                Viz.portal.PortletAlarmTable.superclass.toggleHeaderAndToolbar.apply(this, arguments);
                // if (!show) {
                this.body.replaceClass(this.bodyCls + "-no-border", this.bodyCls);
                // }
                var grid = Ext.getCmp(this._gridId);
                /*
                 * if (!show) { if (grid.getBottomToolbar()) { grid.getBottomToolbar().show(); // this.bbar.dom.style.display = ""; } }
                 */
                // else {
                // this.body.replaceClass(this.bodyCls, this.bodyCls + "-no-border");
                // }
                this.doLayout();
            }

        });

Ext.reg('vizPortletAlarmTable', Viz.portal.PortletAlarmTable);
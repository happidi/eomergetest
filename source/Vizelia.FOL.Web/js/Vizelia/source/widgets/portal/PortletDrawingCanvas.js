﻿Ext.namespace('Viz.portal');

/**
 * @class Viz.portal.PortletDrawingCanvas
 * @extends Ext.Panel
 * @xtype vizPortletDrawingCanvas
 * <p>
 * Summary
 * </p>
 */
Viz.portal.PortletDrawingCanvas = Ext.extend(Viz.portal.Portlet, {
            /**
             * @cfg {Viz.BusinessEntity.DrawingCanvas} entity The DrawingCanvas entity.
             * @type Viz.BusinessEntity.DrawingCanvas
             */
            entity                        : null,

            resizeColor                   : '#22B14C',

            resizingRectPrefix            : 'resizing-rect',

            resizingCirclePrefix          : 'resizing-circle',

            resizingSpriteAttribute       : 'isResizing',

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                   : function(config) {
                this._canvasId = Ext.id();

                this._resizingGroup = 'ResizingBox';
                this._itemGroup = 'ChartorImage';
                this._updatingByDrop = false;
                this._resizeMode = false;

                config = config || {};
                var defaultConfig = {
                    tools : [{
                                id      : 'save',
                                handler : this.onSave,
                                hidden  : !$authorized('@@@@ DrawingCanvas - Full Control'),
                                qtip    : $lang('msg_drawingcanvas_save'),
                                scope   : this
                            }]
                };
                var forcedConfig = {
                    updateHandler : Viz.grid.DrawingCanvas.update
                };

                config.entity.DisplaySliders = false;

                if ($authorized('@@@@ Chart - Sliders') && !config.readonly && config.entity && config.entity.DisplaySliders === true) {
                    Ext.apply(forcedConfig, {
                                plugins : [this.getPluginsConfig(config)]
                            });
                }

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.portal.PortletDrawingCanvas.superclass.constructor.call(this, config);

                /*
                 * this.on({ afterentityupdate : { scope : this, fn : this.onUpdateEntity } });
                 */
                Viz.util.MessageBusMgr.subscribe("ChartChange", this.onChartChange, this);
                Viz.util.MessageBusMgr.subscribe("DrawingCanvasChange", this.onDrawingCanvasChange, this);
                Viz.util.MessageBusMgr.subscribe("DrawingCanvasChartChange", this.onDrawingCanvasItemChange, this);
                Viz.util.MessageBusMgr.subscribe("DrawingCanvasImageChange", this.onDrawingCanvasItemChange, this);
            },

            getPluginsConfig              : function(config) {
                return {
                    ptype   : 'vizChartViewerSlider',
                    sliders : [{
                                text            : $lang('msg_chart_timeinterval'),
                                iconCls         : 'viz-icon-small-chart-timeinterval',
                                entityAttribute : 'TimeInterval',
                                store           : new Viz.store.Enum({
                                            autoLoad      : false,
                                            serviceParams : {
                                                enumTypeName : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common'
                                            }
                                        })
                            }, {
                                text            : $lang('msg_chart_localisation'),
                                iconCls         : 'viz-icon-small-chart-localisation',
                                entityAttribute : 'Localisation',
                                store           : new Viz.store.ChartLocalisation({
                                            autoLoad : false
                                        })
                            }]
                }
            },

            /**
             * Drop handler from portlet tree.
             * @param {} dd
             * @param {} e
             * @param {} data
             * @return {Boolean}
             */
            onNotifyDrop                  : function(dd, e, data) {
                if (data.node && data.node.attributes.entity) {
                    var entity = data.node.attributes.entity;
                    var type = Viz.getEntityType(entity.__type);
                    if (dd.ddGroup == 'PortletDD' && type == 'Vizelia.FOL.BusinessEntities.Chart' && entity.KeyChart) {

                        if (this._updatingByDrop === false) {
                            this._updatingByDrop = true;

                            this.el.mask();

                            var box = this.el.getBox();
                            var x = e.xy[0] - box.x;
                            var y = e.xy[1] - box.y;

                            var chart = {
                                KeyDrawingCanvas : this.entity.KeyDrawingCanvas,
                                KeyChart         : entity.KeyChart,
                                Width            : 300,
                                Height           : 300,
                                X                : x,
                                Y                : y,
                                ZIndex           : 0
                            };

                            Viz.Services.EnergyWCF.DrawingCanvasChart_FormCreate({
                                        item    : chart,
                                        success : function(response) {
                                            this._updatingByDrop = false;
                                            var entity = response.data;
                                            this.el.unmask();
                                            this.addSprite(entity);
                                        },
                                        failure : function() {
                                            this._updatingByDrop = false;
                                            this.el.unmask();
                                        },
                                        scope   : this
                                    });
                        }
                    }
                }
                return true;
            },

            /**
             * Destroy
             * @method onDestroy
             * @private
             */
            onDestroy                     : function() {
                Viz.util.MessageBusMgr.unsubscribe("ChartChange", this.onChartChange, this);
                Viz.util.MessageBusMgr.unsubscribe("DrawingCanvasChange", this.onDrawingCanvasChange, this);
                Viz.util.MessageBusMgr.unsubscribe("DrawingCanvasChartChange", this.onDrawingCanvasItemChange, this);
                Viz.util.MessageBusMgr.unsubscribe("DrawingCanvasImageChange", this.onDrawingCanvasItemChange, this);
                Viz.portal.PortletDrawingCanvas.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Porltet resize event handler.
             */
            onPortletResize               : function() {
                Viz.portal.PortletDrawingCanvas.superclass.onPortletResize.apply(this, arguments);
                var canvas = this.getCanvas();
                if (canvas) {
                    canvas.setWidth(this.body.getSize().width);
                    canvas.setHeight(this.body.getSize().height);
                }
            },

            /**
             * Listener for the DrawingCanvasChange change event.
             * @param {Viz.BusinessEntity.DrawingCanvas} entity
             * @private
             */
            onDrawingCanvasChange         : function(entity) {
                if (this.entity.KeyDrawingCanvas == entity.KeyDrawingCanvas) {
                    this.entity = entity;
                    this.loadSprites();
                }
            },

            /**
             * Listener for the DrawingCanvasChartChange and DrawingCanvasImageChange change event.
             * @param {Viz.BusinessEntity.DrawingCanvasItem} entity
             * @private
             */
            onDrawingCanvasItemChange     : function(entity) {
                if (this.entity.KeyDrawingCanvas == entity.KeyDrawingCanvas) {
                    this.loadSprites();
                }
            },

            /**
             * Listener for the chart change event.
             * @param {Viz.BusinessEntity.Chart} entity
             * @private
             */
            onChartChange                 : function(entity) {
                var surface = this.getSurface();
                var group = surface.getGroup(this._itemGroup);
                group.each(function(sprite) {
                            if (Viz.getEntityType(entity) == 'Vizelia.FOL.BusinessEntities.Chart') {
                                if (sprite.entity && sprite.entity.KeyChart == entity.KeyChart) {
                                    sprite.entity.Chart = entity;
                                    var bbox = sprite.getBBox();
                                    sprite.setAttributes({
                                                src : this.buildSpriteUrl(sprite.entity, bbox.width, bbox.height)
                                            }, true);
                                }
                            }

                            if (Viz.getEntityType(entity) == 'Vizelia.FOL.BusinessEntities.DrawingCanvasChart') {
                                if (sprite.entity && sprite.entity.KeyChart == entity.KeyChart) {
                                    sprite.entity = entity;
                                    var bbox = sprite.getBBox();
                                    sprite.setAttributes({
                                                src : this.buildSpriteUrl(entity, bbox.width, bbox.height)
                                            }, true);
                                }
                            }
                        }, this);
            },

            /**
             * Listener for the afterentityupdate event.
             * @param {Viz.BusinessEntity.Chart} entity
             */
            /*
             * onUpdateEntity : function(drawingChart, entity) { this.makeToolbarBusy(); if (Viz.getEntityType(entity) == 'Vizelia.FOL.BusinessEntities.Chart') { Viz.Services.EnergyWCF.Chart_FormUpdate({ item : entity, success : function() { this.clearToolbarStatus(); Viz.util.MessageBusMgr.publish('ChartChange', entity); }, scope : this }) } },
             */

            /**
             * Return a collection of Chart entity displayed on this drawing.
             * @param {bool} enableSliderOnly : false to return all Charts, true to returns only the one that have the DisableAllSliders parameter set to false.
             */
            getChartEntities              : function(enableSliderOnly) {
                var retVal = [];
                var surface = this.getSurface();
                var group = surface.getGroup(this._itemGroup);
                group.each(function(sprite) {
                            if (sprite.entity && Viz.getEntityType(sprite.entity) == 'Vizelia.FOL.BusinessEntities.DrawingCanvasChart') {
                                if (enableSliderOnly !== true || sprite.entity.DisableAllSliders === false) {
                                    retVal.push(sprite.entity.Chart);
                                }
                            }
                        }, this);
                return retVal;
            },
            /**
             * Handler for switching to ReadOnly mode menu
             */
            onReadOnlyMode                : function() {
                this.removeResizingSprites();
                this.lockSprites();
                this._resizeMode = false;
            },

            /**
             * Handler for switching to Move mode menu
             */
            onMoveMode                    : function() {
                this.removeResizingSprites();
                this.unlockSprites();
                this._resizeMode = false;
            },

            /**
             * Handler for switching to Resize mode menu
             */
            onResizeMode                  : function() {
                this.lockSprites();
                this._resizeMode = true;
            },

            /**
             * Add a new Chart to the surface.
             */
            onAddChart                    : function() {
                Viz.grid.DrawingCanvasChart.create({
                            KeyDrawingCanvas : this.entity.KeyDrawingCanvas
                        });
            },

            /**
             * Add a new Image to the surface.
             */
            onAddImage                    : function() {
                Viz.grid.DrawingCanvasImage.create({
                            KeyDrawingCanvas : this.entity.KeyDrawingCanvas
                        });
            },

            /**
             * Click event handler of each sprite.
             * @param {} sprite
             * @param {} event
             */
            onSpriteClick                 : function(sprite, event) {
                if (this._resizeMode === true && sprite.dd.locked && !sprite[this.resizingSpriteAttribute] === true) {
                    this.createResizingSprite(sprite);
                }
            },

            /**
             * Handler for the afterrender event.
             */
            onAfterRender                 : function() {
                Viz.portal.PortletDrawingCanvas.superclass.onAfterRender.apply(this, arguments);
                if (!this.dropTargetPortlet) {
                    this.dropTargetPortlet = new Ext.dd.DropTarget(this.el.id, {
                                // id : 'PortalColumnPortletDD' + this.el.id,
                                ddGroup    : 'PortletDD',
                                notifyDrop : this.onNotifyDrop.createDelegate(this)
                            });
                }
                this.createDrawComponent.defer(500, this);
            },

            /**
             * Draw component afterrender event handler.
             */
            onCanvasAfterRender           : function() {
                this.loadSprites();
            },

            /**
             * Listener for the refresh event.
             * @method onRefresh
             * @private
             */
            onRefresh                     : function() {
                Viz.portal.PortletDrawingCanvas.superclass.onRefresh.apply(this, arguments);
                this.loadSprites();
            },

            /**
             * Handler for the Save menu event.
             */
            onSave                        : function() {
                var surface = this.getSurface();
                var group = surface.getGroup(this._itemGroup);
                group.each(function(sprite) {
                            if (sprite.entity) {
                                var bbox = sprite.getBBox();

                                sprite.entity.X = bbox.x;
                                sprite.entity.Y = bbox.y;
                                sprite.entity.Width = Math.round(bbox.width);
                                sprite.entity.Height = Math.round(bbox.height);

                                switch (Viz.getEntityType(sprite.entity.__type)) {
                                    case 'Vizelia.FOL.BusinessEntities.DrawingCanvasChart' :
                                        Viz.Services.EnergyWCF.DrawingCanvasChart_FormUpdate({
                                                    item    : sprite.entity,
                                                    success : function(response) {
                                                    },
                                                    scope   : this
                                                })
                                        break;
                                    case 'Vizelia.FOL.BusinessEntities.DrawingCanvasImage' :
                                        Viz.Services.EnergyWCF.DrawingCanvasImage_FormUpdate({
                                                    item    : sprite.entity,
                                                    success : function(response) {
                                                    },
                                                    scope   : this
                                                })
                                        break;
                                }
                            }
                        }, this);
            },
            /**
             * Load the DrawingCanvasChart and create the sprites.
             */
            loadSprites                   : function() {
                this.el.mask();
                this.removeSprites(this._resizingGroup);
                this.removeSprites(this._itemGroup);

                Viz.Services.EnergyWCF.DrawingCanvasImage_GetListByKeyDrawingCanvas({
                            KeyDrawingCanvas : this.entity.KeyDrawingCanvas,
                            success          : function(response) {
                                Ext.each(response, function(image) {
                                            this.addSprite(image);
                                        }, this);

                                Viz.Services.EnergyWCF.DrawingCanvasChart_GetListByKeyDrawingCanvas({
                                            KeyDrawingCanvas : this.entity.KeyDrawingCanvas,
                                            success          : function(response) {
                                                Ext.each(response, function(chart) {
                                                            Viz.Services.EnergyWCF.Chart_ClearCache({
                                                                        Key               : chart.KeyChart,
                                                                        KeyDynamicDisplay : chart.KeyDynamicDisplay ? chart.KeyDynamicDisplay : null,
                                                                        scope             : this
                                                                    });
                                                            this.addSprite(chart);
                                                        }, this);
                                                this.lockSprites();
                                                this.el.unmask();
                                            },
                                            scope            : this
                                        });
                            },
                            scope            : this
                        })
            },

            /**
             * Creates the Ext4 Draw Component.
             */
            createDrawComponent           : function() {
                Ext4.create('Ext.draw.Component', {
                            id        : this._canvasId,
                            renderTo  : this.body.dom,
                            height    : this.body.getSize().height,
                            width     : this.body.getSize().width,
                            viewBox   : false,
                            listeners : {
                                'afterrender' : this.onCanvasAfterRender,
                                scope         : this
                            }
                        });
            },
            /**
             * Returns the Ext4 Draw Component.
             * @return {}
             */
            getCanvas                     : function() {
                return Ext4.getCmp(this._canvasId);
            },

            /**
             * Returns the Surface component.
             * @return {}
             */
            getSurface                    : function() {
                var canvas = this.getCanvas();
                return canvas.surface;
            },

            /**
             * Add a new sprite based on a DrawingCanvasChart entity.
             * @param {Viz.BusinessEntity.DrawingCanvasItem} item :chart or image
             */
            addSprite                     : function(item) {
                var surface = this.getSurface();
                var sprite = surface.add({
                            entity    : item,
                            type      : 'image',
                            group     : this._itemGroup,
                            draggable : true,
                            width     : item.Width,
                            height    : item.Height,
                            x         : item.X,
                            y         : item.Y,
                            zIndex    : item.ZIndex,
                            src       : Viz.Const.Url.ChartLoadingBackground,
                            style     : {
                                'cursor' : 'default'
                            }
                        });
                sprite.on('click', this.onSpriteClick, this);
                sprite.show(true);
                sprite.setAttributes.defer(200, sprite, [{
                                    width  : item.Width,
                                    height : item.Height,
                                    src    : this.buildSpriteUrl(item, item.Width, item.Height)
                                }, true]);
            },

            /**
             * Build the sprite chart Url.
             * @param {Viz.BusinessEntity.DrawingCanvasItem} item :chart or image
             * @param {} Width
             * @param {} Height
             * @return {}
             */
            buildSpriteUrl                : function(item, Width, Height) {
                switch (Viz.getEntityType(item.__type)) {
                    case 'Vizelia.FOL.BusinessEntities.DrawingCanvasChart' :
                        return Viz.Services.BuildUrlStream('Public.svc', 'Chart_GetStreamImage', {
                                    Key             : item.KeyChart,
                                    applicationName : item.Application ? item.Application : Viz.App.Global.User.ApplicationName,
                                    width           : Width,
                                    height          : Height
                                });

                    case 'Vizelia.FOL.BusinessEntities.DrawingCanvasImage' :
                        return Viz.Services.BuildUrlStream('Public.svc', 'DynamicDisplayImage_GetStreamImage', {
                                    Key             : item.KeyImage,
                                    applicationName : item.Application ? item.Application : Viz.App.Global.User.ApplicationName,
                                    width           : Width,
                                    height          : Height
                                });
                    default :
                        return Viz.Const.Url.BlankImage;
                }
            },

            /**
             * Lock the sprites (no DD possible)
             */
            lockSprites                   : function() {
                var surface = this.getSurface();
                Ext.each(surface.items.items, function(sprite) {
                            if (sprite.dd) {
                                sprite.dd.lock();
                                sprite.setStyle({
                                            'cursor' : 'default'
                                        });
                            }
                        });
            },
            /**
             * Unlock the sprites (DD possible)
             */
            unlockSprites                 : function() {
                var surface = this.getSurface();
                Ext.each(surface.items.items, function(sprite) {
                            if (sprite.dd) {
                                sprite.dd.unlock();
                                sprite.setStyle({
                                            'cursor' : 'pointer'
                                        });
                            }
                        });
            },

            /**
             * Create a resizing sprite around an existing sprite
             */
            createResizingSprite          : function(sprite) {
                sprite[this.resizingSpriteAttribute] = true;
                var surface = this.getSurface();
                var bbox = sprite.getBBox();

                var rect = surface.add({
                            id             : sprite.id + this.resizingRectPrefix,
                            type           : 'rect',
                            group          : this._resizingGroup,
                            draggable      : false,
                            width          : bbox.width,
                            height         : bbox.height,
                            x              : bbox.x,
                            y              : bbox.y,
                            zindex         : sprite.zindex + 1,
                            stroke         : this.resizeColor,
                            'stroke-width' : 3
                        });
                var circle = surface.add({
                            id        : sprite.id + this.resizingCirclePrefix,
                            type      : 'circle',
                            group     : this._resizingGroup,
                            draggable : true,
                            radius    : 10,
                            x         : bbox.x + bbox.width,
                            y         : bbox.y + bbox.height,
                            fill      : this.resizeColor,
                            zindex    : sprite.zindex + 1,
                            listeners : {
                                mouseup : function(circle) {
                                    this.onResizingSpriteCircleMouseUp.defer(500, this, [circle]);
                                },
                                scope   : this
                            }
                        });
                circle.show(true);
                rect.show(true);

            },

            /**
             * Handler for the mouse up event of the resize circle.
             * @param {Ext4.draw.Sprite} circle
             */
            onResizingSpriteCircleMouseUp : function(circle) {
                var surface = this.getSurface();
                var rect = surface.items.get(circle.id.replace(this.resizingCirclePrefix, this.resizingRectPrefix));
                var sprite = surface.items.get(circle.id.replace(this.resizingCirclePrefix, ''));

                var bboxRect = rect.getBBox();

                var xCircle = circle.getBBox().x;// Ext.num(circle.x, 0) + Ext.num(circle.dd.translateX, 0);
                var yCircle = circle.getBBox().y; // Ext.num(circle.y, 0) + Ext.num(circle.dd.translateY, 0);

                var newWidth = Math.round(xCircle - bboxRect.x, 0);
                var newHeight = Math.round(yCircle - bboxRect.y, 0);

                // the red circle is correctly positionned for resizing
                if (newWidth > 0 && newHeight > 0) {
                    rect.setAttributes({
                                width  : newWidth,
                                height : newHeight
                            }, true);
                    sprite.setAttributes({
                                width  : newWidth,
                                height : newHeight,
                                src    : this.buildSpriteUrl(sprite.entity, newWidth, newHeight)
                            }, true);
                }
            },

            /**
             * Remove all the resizing sprites (call when mode is changing);
             */
            removeSprites                 : function(group) {
                var surface = this.getSurface();
                var group = surface.getGroup(group);
                group.each(function(sprite) {
                            if (Ext4.dd.DragDropManager.ids['default'][sprite.id]) {
                                delete Ext4.dd.DragDropManager.ids['default'][sprite.id];
                            }
                        }, this);
                group.destroy();
            },

            /**
             * Remove all the resizing sprites (call when mode is changing);
             */
            removeResizingSprites         : function() {
                var surface = this.getSurface();
                var group = surface.getGroup(this._resizingGroup);
                group.each(function(rect) {
                            if (rect.type == 'rect') {
                                var sprite = surface.items.get(rect.id.replace(this.resizingRectPrefix, ''));
                                sprite[this.resizingSpriteAttribute] = false;
                            }
                        }, this);
                this.removeSprites(this._resizingGroup);
            },

            /**
             * Builds the menu config.
             * @private
             */
            getMenuConfig                 : function() {
                return {
                    defaults : {
                        hideOnClick : true
                    },
                    items    : [{
                                text    : $lang("msg_chart_add"),
                                iconCls : 'viz-icon-small-add',
                                handler : this.onAddChart,
                                scope   : this
                            }, {
                                text    : $lang("msg_drawingcanvasimage_add"),
                                iconCls : 'viz-icon-small-image',
                                handler : this.onAddImage,
                                scope   : this
                            }, '-', {
                                text    : $lang("msg_drawingcanvas_readonly"),
                                iconCls : 'viz-icon-small-display',
                                handler : this.onReadOnlyMode,
                                scope   : this
                            }, {
                                text    : $lang("msg_drawingcanvas_move"),
                                iconCls : 'viz-icon-small-drawingcanvas-move',
                                handler : this.onMoveMode,
                                scope   : this
                            }, {
                                text    : $lang("msg_drawingcanvas_resize"),
                                iconCls : 'viz-icon-small-drawingcanvas-resize',
                                handler : this.onResizeMode,
                                scope   : this
                            }, '-', {
                                text    : $lang("msg_drawingcanvas_save"),
                                iconCls : 'viz-icon-small-save',
                                handler : this.onSave,
                                scope   : this
                            }].concat(Viz.chart.Viewer.superclass.getMenuConfig.apply(this, arguments).items)
                }
            }
        });

Ext.reg('vizPortletDrawingCanvas', Viz.portal.PortletDrawingCanvas);
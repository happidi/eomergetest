﻿Ext.namespace('Viz.portal');

/**
 * @class Viz.portal.Portlet
 * @extends Ext.ux.Portlet Summary.
 */
Viz.portal.Portlet = Ext.extend(Ext.ux.Portlet, {
            frame                                   : false,
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */

            /**
             * @cfg {bool} useWindow : true if the panel is embedded in a window to hide the tools.
             */
            useWindow                               : false,

            readonly                                : false,

            disableDblClick                         : false,

            /**
             * To handle the resizing of the portlet
             * @type Boolean
             */
            cacheSizes                              : false,

            /**
             * @cfg {Viz.BusinessEntity.Portlet} entityPortlet The Portlet entity.
             * @type Viz.BusinessEntity.Portlet
             */
            entityPortlet                           : null,

            constructor                             : function(config) {
                config = config || {};
                if (!Ext.isDefined(config.readonly))
                    config.readonly = this.readonly;

                var defaultConfig = {
                    draggable     : $authorized('@@@@ Portlet - Full Control') && !config.readonly,
                    layout        : 'fit',
                    updateHandler : null
                    // border : !config.useWindow,
                    // !config.useWindow
                };

                var tools = config.tools || [];
                tools = tools.concat([{
                            id      : 'plus',
                            qtip    : $lang('msg_portlet_viewassociatedtab'),
                            handler : this.onViewAssociatedTab,
                            scope   : this
                        }, {
                            id      : 'refresh',
                            qtip    : $lang('msg_portlet_refresh'),
                            handler : function() {
                                this.onRefresh(false);
                            },
                            scope   : this
                        }, {
                            id      : 'gear',
                            qtip    : $lang('msg_update'),
                            handler : this.onOpenFormCrudFromTool,
                            hidden  : !$authorized(Viz.util.Portal.getPortletAuthorization(config.entityPortlet.TypeEntity, true)) || config.readonly,
                            scope   : this
                        }, /*
                             * { id : 'pin', qtip : $lang('msg_portlet_updateframe'), hidden : !$authorized(Viz.util.Portal.getPortletAuthorization(config.entityPortlet.TypeEntity, true)), handler : this.onOpenFormCrudPorlet, scope : this },
                             */{
                            id      : 'maximize',
                            handler : this.onMaximize,
                            hidden  : this.useWindow || config.readonly,
                            qtip    : $lang('msg_portlet_maximize'),
                            scope   : this
                        }, {
                            id      : 'close',
                            handler : this.onDelete,
                            hidden  : !$authorized(Viz.util.Portal.getPortletAuthorization(config.entityPortlet.TypeEntity, true)) || config.readonly,
                            qtip    : $lang('msg_portlet_delete'),
                            scope   : this
                        }]);

                var forcedConfig = {
                    tools       : tools,
                    collapsible : $authorized('@@@@ Portlet - Full Control') && !config.readonly
                };

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                if (!Ext.isEmpty(config.title))
                    config.title = Viz.util.Portal.localizeTitle(config.title);

                Viz.portal.Portlet.superclass.constructor.call(this, config);

                this.addEvents(
                        /**
                         * @event beforecontextmenu Fires before the context menu is shown and gives a chance to change the ctxMenu.
                         * @param {Viz.chart.Viewer} this
                         * @param {Viz.BusinessEntity.Chart} Viz.BusinessEntity.Chart.
                         */
                        "beforecontextmenu");

                this.on({
                            afterrender       : {
                                scope : this,
                                fn    : this.onAfterRender
                            },
                            collapse          : {
                                scope : this,
                                fn    : this.onCollapseExpand
                            },
                            expand            : {
                                scope : this,
                                fn    : this.onCollapseExpand
                            },
                            beforecontextmenu : {
                                scope : this,
                                fn    : this.onBeforeContextMenu
                            },
                            resize            : {
                                scope : this,
                                fn    : this.onPortletResize
                            }
                        });

                Viz.util.MessageBusMgr.subscribe("PortletChange", this.onPortletChange, this);

                Viz.util.MessageBusMgr.subscribe("PortletDelete", this.onPortletDelete, this);

                if (!this.useWindow && this.entityPortlet) {
                    this._autoReloadTask = {
                        run   : this.autoReloadHandler,
                        scope : this
                    };

                    if (Ext.isNumber(this.entityPortlet.RefreshFrequency) && this.entityPortlet.RefreshFrequency > 0) {
                        this._autoReloadTask.interval = this.entityPortlet.RefreshFrequency * 1000;
                        Ext.TaskMgr.start.defer(this._autoReloadTask.interval,this,[this._autoReloadTask]);
                    }
                }
            },

            /**
             * Handler for the autoreload task
             */
            autoReloadHandler                       : function() {
                var parent = this.getParentPortalTab();
                if (parent && parent.isActive)
                    this.onRefresh(true);
            },

            /**
             * Destroy
             * @method onDestroy
             * @private
             */
            onDestroy                               : function() {
                Viz.util.MessageBusMgr.unsubscribe("PortletChange", this.onPortletChange, this);
                Viz.util.MessageBusMgr.unsubscribe("PortletDelete", this.onPortletDelete, this);

                if (this._autoReloadTask) {
                    Ext.TaskMgr.stop(this._autoReloadTask);
                }
                this.el.un({
                            contextmenu : this.onContextMenu,
                            scope       : this
                        });

                Viz.portal.Portlet.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * PortletChange messagebus listener.
             * @param {} portlet
             */
            onPortletChange                         : function(portlet) {
                if (!this.useWindow && this.entityPortlet && this.entityPortlet.KeyPortlet == portlet.KeyPortlet) {
                    Ext.TaskMgr.stop(this._autoReloadTask);
                    if (portlet.RefreshFrequency > 0) {
                        this._autoReloadTask = {
                            run      : this.autoReloadHandler,
                            scope    : this,
                            interval : portlet.RefreshFrequency * 1000
                        };
                        Ext.TaskMgr.start(this._autoReloadTask);
                    }
                }
            },
            /**
             * @private onContextMenu
             */
            onContextMenu                           : function(event) {
                // we stop the propagation is the portlet is not contained in a FlipCard otherwise the event doesnt buble up to the flip card level.
                if (this.findParentByType('vizPortletFlipCard') == null) {
                    event.stopEvent();
                }
                if (this.hideMenu == true)
                    return;

                if (this.fireEvent("beforecontextmenu", this, this.entity) != false) {
                    if (this.ctxMenu && this.ctxMenu instanceof Ext.menu.Menu) {
                        this.ctxMenu.on("hide", function(menu) {
                                    menu.destroy();
                                });
                        this.ctxMenu.showAt(event.getXY());
                    }
                }
                // }
            },

            /**
             * Handles the beforecontextmenu event and gives a chance to change the menu
             */
            onBeforeContextMenu                     : function() {
                if (!$authorized(Viz.util.Portal.getPortletAuthorization(this.entityPortlet.TypeEntity, true)) && !$authorized(Viz.util.Portal.getPortletAuthorization(this.entityPortlet.TypeEntity, false) + ' - Simple Menu'))
                    return false;
                if (this.readonly)
                    return false;
                this.ctxMenu = new Ext.menu.Menu(this.getMenuConfig());
                return true;
            },

            /**
             * Builds the menu config.
             * @private
             */
            getMenuConfig                           : function() {
                var fullControl = $authorized(Viz.util.Portal.getPortletAuthorization(this.entityPortlet.TypeEntity, true));
                return {
                    defaults : {
                        hideOnClick : true
                    },
                    items    : [{
                                text    : $lang("msg_portlet_refresh"),
                                iconCls : 'viz-icon-small-refresh',
                                handler : this.onRefresh,
                                scope   : this
                            }].concat(this.getMenuConfigUpdate()).concat([{
                                text    : $lang('msg_portlet_copy'),
                                iconCls : 'viz-icon-small-copy',
                                handler : this.onCopy,
                                hidden  : this.useWindow || !fullControl,
                                scope   : this
                            }, {
                                text    : $lang('msg_portlet_delete'),
                                iconCls : 'viz-icon-small-delete',
                                handler : this.onDelete,
                                hidden  : !$authorized(Viz.util.Portal.getPortletAuthorization(this.entityPortlet.TypeEntity, true)) || this.readonly,
                                scope   : this
                            }, '-', {
                                text    : $lang("msg_portlet_fullscreen"),
                                iconCls : 'viz-icon-small-chart-fullscreen',
                                handler : this.onMaximize ,
                                hidden  : this.useWindow,
                                scope   : this
                            }, {
                                text    : $lang("msg_portlet_headerandtoolbarvisible"),
                                iconCls : 'viz-icon-small-portlet-headerandtoolbarvisible',
                                handler : this.onHeaderAndToolBarVisible,
                                hidden  : this.useWindow || !fullControl,
                                scope   : this
                            }, {
                                text    : this.entity && this.entity.IsFavorite == true ? $lang('msg_chart_favorite').toBold() : $lang('msg_chart_favorite'),
                                iconCls : 'viz-icon-small-chart-favorite',
                                hidden  : !fullControl,
                                handler : this.onToggleFavorite || !fullControl,
                                scope   : this
                            }, {
                                text    : this.entity && this.entity.OpenOnStartup == true ? $lang('msg_portalwindow_openonstartup').toBold() : $lang('msg_portalwindow_openonstartup'),
                                iconCls : 'viz-icon-small-logout',
                                hidden  : !fullControl,
                                handler : this.onToggleOpenOnStartup || !fullControl,
                                scope   : this
                            }])
                }
            },

            getMenuConfigUpdate                     : function() {
                var fullControl = $authorized(Viz.util.Portal.getPortletAuthorization(this.entityPortlet.TypeEntity, true));
                return [{
                            text    : $lang('msg_portlet_updateframe'),
                            iconCls : 'viz-icon-small-portlet',
                            hidden  : this.useWindow || !$authorized(Viz.util.Portal.getPortletAuthorization(this.entityPortlet.TypeEntity, true)),
                            handler : function() {
                                this.onOpenFormCrudPorlet();
                            },
                            scope   : this
                        }, {
                            text    : $lang('msg_portlet_update'),
                            iconCls : 'viz-icon-small-update',
                            handler : function() {
                                this.onOpenFormCrud()
                            },
                            hidden  : !fullControl,
                            scope   : this
                        }];
            },

            /**
             * Toggle IsFavorite
             */
            onToggleFavorite                        : function(menuItem) {
                this.hideCallingMenu(menuItem);

                this.entity.IsFavorite = !this.entity.IsFavorite;
                this.onUpdateEntity(this.entity);
            },

            /**
             * Listener for the afterentityupdate event.
             * @param {Viz.BusinessEntity.Chart} entity
             */
            onUpdateEntity                          : function() {
                this.makeToolbarBusy();
                if (this.entity) {
                    var type = Viz.getEntityName(this.entity.__type);
                    Viz.Services.EnergyWCF[type + "_FormUpdate"]({
                                item    : this.entity,
                                success : function(response) {
                                    this.clearToolbarStatus();
                                    if (!response.success) {
                                        Ext.Msg.show({
                                                    width   : 400,
                                                    title   : $lang("error_ajax"),
                                                    msg     : response.msg || esponse.message,
                                                    buttons : Ext.Msg.OK,
                                                    icon    : Ext.MessageBox.ERROR
                                                });
                                    }
                                    else
                                        Viz.util.MessageBusMgr.publish(type + 'Change', this.entity);
                                },
                                scope   : this
                            })
                }
            },

            /**
             * Toggle OpenOnStartup
             */
            onToggleOpenOnStartup                   : function(menuItem) {
                this.hideCallingMenu(menuItem);

                this.entity.OpenOnStartup = !this.entity.OpenOnStartup;
                this.onUpdateEntity(this.entity);
            },

            /*
             * Handler for the afterrender event.
             */
            onAfterRender                           : function() {
                if (this.entityPortlet && Ext.isDefined(this.entityPortlet.HeaderAndToolbarVisible))
                    this.toggleHeaderAndToolbar(this.entityPortlet.HeaderAndToolbarVisible === true);
                this.el.on({
                            contextmenu : this.onContextMenu,
                            dblclick    : this.disableDblClick === true ? Ext.emptyFn : this.onDblClick,
                            scope       : this
                        });
                if (!Ext.isEmpty(this.entityPortlet.AssociatedPortalTabKey)) {
                    this.el.setStyle('cursor', 'help');
                }
                this.updatePortletLayout.defer(500, this);
            },

            onDblClick                              : function() {
                this.onViewAssociatedTab();
            },
            /*
             * Handler for the collapse-expand event.
             */
            onCollapseExpand                        : function() {
                if (!$authorized('@@@@ Portlet - Full Control'))
                    return;
                if (this.entityPortlet && this.entityPortlet.Collapsed !== this.collapsed) {
                    this.entityPortlet.Collapsed = this.collapsed;
                    Viz.Services.EnergyWCF.Portlet_FormUpdate({
                                item    : this.entityPortlet,
                                success : function(entity) {
                                    Viz.util.MessageBusMgr.publish('PortletChange', entity);
                                },
                                scope   : this
                            });
                    var col = this.getParentPortalColumn();
                    col.updatePortletLayout();
                }
            },
            /**
             * Handler for the HeaderAndToolBarVisible menu.
             */
            onHeaderAndToolBarVisible               : function(menuItem) {
                this.hideCallingMenu(menuItem);

                if (this.entityPortlet) {
                    this.updatePortletHeaderAndToolbarVisibility(!this.entityPortlet.HeaderAndToolbarVisible);
                }
            },

            /**
             * update Portlet Header And Toolbar Visibility
             * @param {} visibility
             */
            updatePortletHeaderAndToolbarVisibility : function(visibility) {
                if (this.entityPortlet) {
                    this.entityPortlet.HeaderAndToolbarVisible = visibility;
                    Viz.Services.EnergyWCF.Portlet_FormUpdate({
                                item    : this.entityPortlet,
                                success : function(entity) {
                                    Viz.util.MessageBusMgr.publish('PortletChange', entity);
                                },
                                scope   : this
                            });
                    var col = this.getParentPortalColumn();
                    col.updatePortletLayout.defer(500, col);
                }

            },
            /*
             * Handler for the delete event.
             */
            onDelete                                : function(menuItem) {
                this.hideCallingMenu(menuItem);
                if (this.entity) {
                    var type = Viz.getEntityType(this.entity.__type);
                    // if (type == 'Vizelia.FOL.BusinessEntities.Chart') {
                    Ext.MessageBox.confirm(this.entity.IsFavorite === true ? $lang('msg_portlet_container') : $lang('msg_portlet'), this.entity.IsFavorite === true ? $lang('msg_portlet_delete_confirmation_single') : $lang('msg_portlet_deletenofavorite_confirmation_single'), function(button) {
                                if (button == 'yes') {
                                    this.onDeleteSilent();
                                }
                            }, this);
                }
                else {
                    this.onDeleteSilent();
                }
            },

            /**
             * Delete the portlet without any warning.
             */
            onDeleteSilent                          : function() {
                Viz.Services.EnergyWCF.Portlet_Delete({
                            item          : this.entityPortlet,
                            deleteContent : this.entity ? (this.entity.IsFavorite !== true) : false,
                            success       : function(entity) {
                                var parent = this.getParentPortalColumn();
                                this.ownerCt.remove(this);
                                parent.updatePortletLayout();
                            },
                            scope         : this
                        });
            },

            /**
             * Listeners for the PortletDelete messagebus.
             * @param {} entity
             */
            onPortletDelete                         : function(entity) {
                if (Viz.getEntityType(this.entity) == Viz.getEntityType(entity) && Viz.getEntityKey(this.entity) == Viz.getEntityKey(entity)) {
                    this.onDeleteSilent();
                }
                if (this.entityPortlet && entity && this.entityPortlet.KeyPortlet == entity.KeyPortlet) {
                    this.onDeleteSilent();
                }
            },

            hideCallingMenu                         : function(menuItem) {
                if (menuItem && menuItem.getXType && menuItem.getXType() == 'menuitem')
                    menuItem.parentMenu.hide();
            },
            /**
             * Handler to open the form of the porlet
             */
            onOpenFormCrudPorlet                    : function(menuItem) {
                this.hideCallingMenu(menuItem);

                if (this.entityPortlet) {
                    Viz.grid.Portlet.update(this.entityPortlet);
                }
            },
            /**
             * Handler to open the form of the porlet triggered by the tool
             */
            onOpenFormCrudFromTool                  : function(event) {
                this.onOpenFormCrud();
            },
            /**
             * Handler to open the form of the real entity
             */
            onOpenFormCrud                          : function(menuItem, extraConfig) {
                this.hideCallingMenu(menuItem);

                if (this.entity && Ext.isFunction(this.updateHandler)) {
                    this.updateHandler(this.entity, null, extraConfig);
                }
            },
            /**
             * Copy the current Portlet and refresh the Column
             */
            onCopy                                  : function(menuItem) {
                this.hideCallingMenu(menuItem);

                this.getParentPortalColumn().el.mask();
                Viz.Services.EnergyWCF.Portlet_Copy({
                            keys    : [this.entityPortlet.KeyPortlet],
                            success : function(response) {
                                this.getParentPortalColumn().el.unmask();
                                var entity = response.data;
                                Viz.util.MessageBusMgr.fireEvent("PortletChange", entity);
                            },
                            scope   : this
                        });
            },

            /**
             * Open the associated portal tab.
             */
            onViewAssociatedTab                     : function() {
                if (!Ext.isEmpty(this.entityPortlet.AssociatedPortalTabKey)) {
                    Viz.App.Global.ViewPort.openOrActivatePortalTab(this.entityPortlet.AssociatedPortalTabKey);
                }
            },

            /**
             * Listener for the refresh event.
             * @method onRefresh
             * @private
             */
            onRefresh                               : function() {
                if (arguments && arguments.length > 0 && Ext.isFunction(arguments[0].getXType) && arguments[0].getXType() == 'menuitem' && arguments[0].parentMenu)
                    arguments[0].parentMenu.hide();
            },

            /**
             * Porltet resize event handler.
             */
            onPortletResize                         : function() {

            },

            /**
             * Porltet maximize event handler.
             */
            onMaximize                              : function(menuItem) {
                this.hideCallingMenu(menuItem);
                var win = new Viz.portal.PortletWindow({
                            entity        : this.entity,
                            entityPortlet : this.entityPortlet,
                            maximized     : true,
                            xtypePortlet  : this.xtype,
                            updateHandler : this.updateHandler,
                            iconCls       : this.iconCls
                        });
                win.show();
            },

            /**
             * Set autocalculated height of the portlet
             * @method updatePortletLayout
             * @private
             */
            updatePortletLayout                     : function() {
                if (this.rendered && this.entityPortlet) {
                    if (Ext.isDefined(this.entityPortlet.HeaderAndToolbarVisible))
                        this.toggleHeaderAndToolbar(this.entityPortlet.HeaderAndToolbarVisible === true);
                    var col = this.getParentPortalColumn();
                    if (!col)
                        return;
                    var totalHeight = col.getHeight() - col.el.getPadding('tb');
                    var width = col.el.getWidth() - col.el.getPadding('lr');
                    var margins = this.el.getMargins();

                    var portlets = col.getPortlets();
                    var totalFlex = 0;
                    Ext.each(portlets, function(p) {
                                if (p.collapsed) {
                                    if (p.rendered) {
                                        totalHeight -= (p.getHeight() + margins.top + margins.bottom);
                                    }
                                }
                                else {
                                    if (p.entityPortlet.FixedHeight > 0) {
                                        totalHeight -= (p.entityPortlet.FixedHeight + margins.top + margins.bottom);
                                    }
                                    else {
                                        totalFlex += p.entityPortlet.Flex;
                                    }
                                }
                            }, this);
                    if (!this.collapsed) {
                        var frameHeight = this.getFrameHeight();
                        var bodyHeight = 0;
                        if (this.entityPortlet.FixedHeight > 0) {
                            bodyHeight = this.entityPortlet.FixedHeight - frameHeight;
                        }
                        else {
                            bodyHeight = totalHeight / totalFlex * this.entityPortlet.Flex - (margins.top + margins.bottom) - frameHeight;
                            if (bodyHeight < 0)
                                bodyHeight = 100;

                        }
                        this.setHeight(bodyHeight + frameHeight);
                        this.setWidth(width);
                        this.doLayout();
                    }
                }
            },
            /**
             * Get the parent column.
             * @method getParentPortalColumn
             * @return {Viz.portal.PortalColumn} the parent column
             */
            getParentPortalColumn                   : function() {
                return this.findParentByType('vizPortalColumn');
            },

            /**
             * Get the parent portal.
             * @method getParentPortalTab
             * @return {Viz.portal.PortalTab} the parent portal tab
             */
            getParentPortalTab                      : function() {
                return this.findParentByType('vizPortalTab');
            },

            /**
             * Get the parent portal.
             * @method getParentPortalTab
             * @return {Viz.portal.PortalPanel} the parent portal panel
             */
            getParentPortalPanel                    : function() {
                return this.findParentByType('vizPortalPanel');
            },

            /**
             * Get the parent window.
             * @method getParentPortalWindow
             * @return {Viz.portal.PortalWindow} the parent window
             */
            getParentPortalWindow                   : function() {
                return this.findParentByType('vizPortalWindow');
            },

            /**
             * Hide or Show the Panel header and toolbar.
             * @param {Bool} show True to show, False to Hide.
             */
            toggleHeaderAndToolbar                  : function(show) {
                if (show) {
                    if (this.getBottomToolbar()) {
                        this.getBottomToolbar().show();
                        // this.bbar.dom.style.display = "";
                    }
                    if (this.header && this.header.setDisplayed && this.header.dom)
                        this.header.setDisplayed(true);
                    if (this.el && this.el.dom)
                        this.el.replaceClass(this.baseCls + "-no-border", this.baseCls);
                    if (this.body && this.body.dom)
                        this.body.replaceClass(this.bodyCls + "-no-border", this.bodyCls);
                }
                else {
                    if (this.getBottomToolbar()) {
                        this.getBottomToolbar().hide();
                        // this.bbar.dom.style.display = "none";
                    }
                    if (this.header && this.header.setDisplayed && this.header.dom)
                        this.header.setDisplayed(false);
                    if (this.el && this.el.dom)
                        this.el.replaceClass(this.baseCls, this.baseCls + "-no-border");
                    if (this.body && this.body.dom)
                        this.body.replaceClass(this.bodyCls, this.bodyCls + "-no-border");
                }
            },

            makeToolbarBusy                         : function() {
                if (this.entityPortlet.HeaderAndToolbarVisible) {
                    var tb = this.getBottomToolbar();
                    if (tb && tb.showBusy)
                        tb.showBusy($lang('msg_loading'));
                }
                else {
                    if (this.entityPortlet.RefreshFrequency > 0)
                        return;
                    this.el.mask();
                }
            },

            clearToolbarStatus                      : function() {
                if (this.entityPortlet.HeaderAndToolbarVisible) {
                    var tb = this.getBottomToolbar();
                    if (tb && tb.clearStatus)
                        tb.clearStatus();
                }
                else {
                    if (this.entityPortlet.RefreshFrequency > 0)
                        return;
                    this.el.unmask();
                }
            },

            /**
             * Return the current background color.
             * @return {}
             */
            getBackgroundColor                      : function() {
                return null;
            }

        });

Ext.reg('vizPortlet', Viz.portal.Portlet);

﻿/**
 * @class Viz.grid.ChartScheduler
 * <p>
 * A grid exposing the business entity ChartScheduler
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.ChartScheduler = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor         : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_chartscheduler_add"),
                                iconCls : 'viz-icon-small-chartscheduler-add',
                                hidden  : !$authorized('@@@@ Chart Scheduler - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_chartscheduler_update"),
                                iconCls : 'viz-icon-small-chartscheduler-update',
                                hidden  : !$authorized('@@@@ Chart Scheduler - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_chartscheduler_delete"),
                                iconCls : 'viz-icon-small-chartscheduler-delete',
                                hidden  : !$authorized('@@@@ Chart Scheduler - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                xtype : 'tbseparator'
                            }, {
                                id           : 'timezone-view-local',
                                iconCls      : 'viz-icon-small-world',
                                enableToggle : true,
                                text         : $lang('Timezone'),
                                handler      : this.viewOnLocalCalendar,
                                scope        : this
                            }, {
                                text    : $lang("msg_chartscheduler_generate"),
                                iconCls : 'viz-icon-small-mail',
                                hidden  : !$authorized('@@@@ Chart Scheduler - Full Control'),
                                scope   : this,
                                handler : this.onGenerateRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 740,
                            height           : 430,
                            xtype            : 'vizFormChartScheduler',
                            messageSaveStart : 'ChartSchedulerChangeStart',
                            messageSaveEnd   : 'ChartSchedulerChange',
                            readonly         : !$authorized('@@@@ Chart Scheduler - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_chartscheduler_add"),
                            iconCls : 'viz-icon-small-chartscheduler-add'
                        },
                        update      : {
                            title               : $lang("msg_chartscheduler_update"),
                            iconCls             : 'viz-icon-small-chartscheduler-update',
                            titleEntity         : 'Title',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyChartScheduler'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-chart-chartscheduler-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.ChartScheduler_FormUpdateBatch
                        }
                    },
                    formModal    : false,
                    store        : new Viz.store.ChartScheduler(),
                    columns      : Viz.Configuration.Columns.ChartScheduler
                };
                defaultConfig.store.on('load', function() {
                            var button = this.getTopToolbar().find('id', 'timezone-view-local')[0];
                            if (button)
                                button.toggle(false);
                        }, this);
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.ChartScheduler.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("ChartSchedulerChange", this.reloadStore, this);

            },
            /**
             * Destroy
             * @method onDestroy
             * @private
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("ChartSchedulerChange", this.reloadStore, this);
                Viz.grid.ChartScheduler.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Generate the Scheduled Report and send it by email right now.
             */
            onGenerateRecord    : function() {
                var selections = this.getSelectionModel().getSelections();

                if (selections.length == 0)
                    return;

                if (this.editor) {
                    this.editor.stopEditing();
                }

                for (var i = 0, r; r = selections[i]; i++) {
                    var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                pollingInterval : 2000,
                                serviceStart    : Viz.Services.EnergyWCF.ChartScheduler_GenerateBegin,
                                serviceParams   : {
                                    KeyChartScheduler : r.data.KeyChartScheduler
                                },
                                isFileResult    : false,
                                showProgress    : true,
                                modal           : false
                            });
                    submitter.start();
                }
            },
            viewOnLocalCalendar : function(btn) {
                if (btn.pressed) {
                    this.store.each(function(r) {
                                r.data.StartDate = Viz.date.convertToLocalDate(r.data.StartDate, r.data['TimeZoneOffset'][0]);
                                r.data.EndDate = Viz.date.convertToLocalDate(r.data.EndDate, r.data['TimeZoneOffset'][1]);
                                r.data.NextOccurenceDate = Viz.date.convertToLocalDate(r.data.NextOccurenceDate, r.data['TimeZoneOffset'][0]);
                            }, this);

                    this.view.refresh(true);
                }
                else {
                    this.store.load();
                }
            }
        });
Ext.reg("vizGridChartScheduler", Viz.grid.ChartScheduler);
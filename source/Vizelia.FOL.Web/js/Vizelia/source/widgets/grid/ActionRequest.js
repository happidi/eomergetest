﻿/**
 * @class Viz.grid.ActionRequest
 * <p>
 * A grid exposing the business entity ActionRequest
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.ActionRequest = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor           : function(config) {
                config = config || {};
                this._buttonAction = Ext.id();
                var defaultConfig = {
                    hasRowEditor       : false,
                    stripeRows         : false,
                    hasToolbar         : false,
                    hidden             : !$authorized('@@@@ Action Request'),
                    autoLoadStore      : false,
                    tbar               : [{
                                text    : $lang("msg_actionrequest_add"),
                                iconCls : 'viz-icon-small-actionrequest-add',
                                hidden  : !$authorized('@@@@ Action Request - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_actionrequest_update"),
                                iconCls : 'viz-icon-small-actionrequest-update',
                                hidden  : !$authorized('@@@@ Action Request - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord.createDelegate(this, ['update'])
                            }, {
                                text    : $lang("msg_actionrequest_delete"),
                                iconCls : 'viz-icon-small-actionrequest-delete',
                                hidden  : !$authorized('@@@@ Action Request - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_otheractions"),
                                id      : this._buttonAction,
                                iconCls : 'viz-icon-small-action',
                                scope   : this,
                                menu    : new Ext.menu.Menu({
                                            items     : [], // an empty menu to get the arrow on the button.
                                            listeners : {
                                                beforeshow : this.updateWorkflowActions,
                                                scope      : this
                                            }
                                        })
                            }],
                    formConfig         : {
                        historyEnabled : false,
                        common         : {
                            width            : 800,
                            height           : 500,
                            xtype            : 'vizFormActionRequest',
                            messageSaveStart : 'ActionRequestChangeStart',
                            messageSaveEnd   : 'ActionRequestChange',
                            readonly         : !$authorized('@@@@ Action Request - Full Control')
                        },
                        create         : {
                            title   : $lang("msg_actionrequest_add"),
                            iconCls : 'viz-icon-small-actionrequest-add',
                            xtype   : 'vizFormActionRequestWizard'
                        },
                        update         : {
                            title               : $lang("msg_actionrequest_update"),
                            iconCls             : 'viz-icon-small-actionrequest-update',
                            titleEntity         : 'RequestID',
                            xtype               : 'vizFormActionRequest',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyActionRequest'
                                    }]
                        },
                        updatebatch    : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-actionrequest-update',
                            serviceHandlerUpdateBatch : Viz.Services.ServiceDeskWCF.ActionRequest_FormUpdateBatch
                        },
                        updateConfig   : function(entity, targetAction) {
                            this['update'].xtype = entity.ReadOnly || targetAction != 'update' ? 'vizFormActionRequest' : 'vizFormActionRequestWizard';
                            this.historyEnabled = entity.ReadOnly;
                        }
                    },
                    store              : new Viz.store.ActionRequest(),
                    columns            : Viz.Configuration.Columns.ActionRequest,
                    autoReloadInterval : 60
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.ActionRequest.superclass.constructor.call(this, config);
                this.on({
                            // when the filter is cleared we have to reset the filter on ActualStart
                            filtercleared   : {
                                scope : this,
                                fn    : function(grid) {
                                    this.onFilterAvailable();
                                }
                            },
                            beforeaddrecord : {
                                scope : this,
                                fn    : function(grid, entity) {
                                    // forces the initial value of requestor as the user Occupant.KeyOccupant.
                                    entity.Requestor = Viz.App.Global.User.ProviderUserKey.KeyOccupant;
                                    // forces the initial value of location as the user Occupant.KeyLocation
                                    entity.KeyLocation = Viz.App.Global.User.ProviderUserKey.Occupant.KeyLocation;
                                    entity.LocationLongPath = Viz.App.Global.User.ProviderUserKey.Occupant.LocationLongPath
                                }
                            },
                            rowcontextmenu  : {
                                scope : this,
                                fn    : this.onRowContextMenu
                            },
                            render          : {
                                scope : this,
                                fn    : function() {
                                    // forces an initial load of an empty store then we will reset the paging parameter in filteravailable.
                                    this.store.load({
                                                params : {
                                                    start : 0,
                                                    limit : -1
                                                }
                                            });

                                }
                            },
                            filteravailable : {
                                scope : this,
                                fn    : this.onFilterAvailable
                            }
                        });

                Viz.util.MessageBusMgr.subscribe("ActionRequestChange", this.reloadStore, this);
            },

            /**
             * Event handler for the filter available event.
             */
            onFilterAvailable     : function() {
                // we reset the paging parameter
                this.store.lastOptions.params.limit = this.pageSize;
                // we make the list show only last 2 months data.
                var actualStartFilter = this.filters.getFilter("ActualStart");
                this.filters.applyingState = true;
                if (actualStartFilter) {
                    actualStartFilter.setValue({
                                after : new Date().add(Date.MONTH, -2)
                            });
                    actualStartFilter.setActive(true);
                    this.reloadStore();
                }
                else {
                    this.store.load();
                }
            },

            /**
             * @private Destroy
             */
            onDestroy             : function() {
                Viz.util.MessageBusMgr.unsubscribe("ActionRequestChange", this.reloadStore, this);
                Viz.grid.ActionRequest.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private
             */
            onRowContextMenu      : function(grid, row, event) {
                event.stopEvent();
                var xy = event.getXY();
                this.getSelectionModel().selectRow(row, false);
                if (this.hideMenu == true)
                    return;

                this.workflowUtil = new Viz.util.Workflow({
                            comp      : this,
                            listeners : {
                                success            : function(o, actions) {
                                    ctxMenuWait.destroy();
                                    var ctxMenu = new Ext.menu.Menu({
                                                items : actions
                                            });
                                    ctxMenu.on("hide", function(menu) {
                                                menu.destroy();
                                            });

                                    ctxMenu.add(this.buildPrintActions());
                                    // if (ctxMenu.items.getCount() > 2) {
                                    ctxMenu.showAt(xy);

                                    // }
                                    // else
                                    // ctxMenu.destroy();
                                },
                                failure            : function() {
                                    ctxMenuWait.destroy();
                                },
                                beforeaction       : function() {
                                    if (this.loadMask)
                                        Ext.get(this.bwrap).mask();
                                    return true;
                                },
                                afteractionsuccess : function() {
                                    Ext.get(this.bwrap).unmask();
                                    this.publishMessage("ActionRequestChange");
                                    this.publishMessage("TaskChange");
                                },
                                afteractionfailure : function() {
                                    Ext.get(this.bwrap).unmask();
                                },
                                scope              : this
                            }
                        });

                var ctxMenuWait = this.workflowUtil.buildWaitContextMenu();
                ctxMenuWait.showAt(xy);
                this.workflowUtil.getStateMachineActions();
            },
            updateWorkflowActions : function() {
                var button = Ext.getCmp(this._buttonAction);
                if (this.getSelectionModel().getSelected() == null) {
                    return false;
                }
                // this.getSelectionModel().selectRow(row, false);
                if (this.hideMenu == true)
                    return false;

                this.workflowUtil = new Viz.util.Workflow({
                            comp      : this,
                            listeners : {
                                success            : function(o, actions) {
                                    ctxMenuWait.destroy();
                                    var ctxMenu = new Ext.menu.Menu({
                                                items : actions
                                            });
                                    ctxMenu.on("hide", function(menu) {
                                                menu.destroy();
                                            });

                                    if (ctxMenu.items.getCount() > 0) {
                                        ctxMenu.show(button.getEl());
                                    }
                                    else
                                        ctxMenu.destroy();
                                },
                                failure            : function() {
                                    ctxMenuWait.destroy();
                                },
                                beforeaction       : function() {
                                    if (this.loadMask)
                                        Ext.get(this.bwrap).mask();
                                    return true;
                                },
                                afteractionsuccess : function() {
                                    Ext.get(this.bwrap).unmask();
                                    this.publishMessage("ActionRequestChange");
                                    this.publishMessage("TaskChange");
                                },
                                afteractionfailure : function() {
                                    Ext.get(this.bwrap).unmask();
                                },
                                scope              : this
                            }
                        });

                var ctxMenuWait = this.workflowUtil.buildWaitContextMenu();
                ctxMenuWait.show(button.getEl());
                this.workflowUtil.getStateMachineActions();
                return false;
            },

            /**
             * Builds the print actions
             */
            buildPrintActions     : function(item) {
                var actions = [];
                actions.push({
                            scope   : this,
                            text    : $lang("msg_application_export"),
                            tooltip : $lang("msg_application_export"),
                            iconCls : 'viz-icon-small-report',
                            menu    : [{
                                        text    : 'PDF',
                                        iconCls : 'viz-icon-small-pdf',
                                        handler : this.printActionRequest.createDelegate(this, [Vizelia.FOL.BusinessEntities.ExportType.Pdf])
                                    }, {
                                        text    : 'Excel',
                                        iconCls : 'viz-icon-small-excel',
                                        handler : this.printActionRequest.createDelegate(this, [Vizelia.FOL.BusinessEntities.ExportType.Excel2007])
                                    }]
                        });
                return actions;
            },

            /**
             * Printing a selected ticket
             * @param {Viz.BusinessEntity.ActionRequest} item
             */
            printActionRequest    : function(exportType) {
                var item = this.workflowUtil.getWorkflowEntity(this);
                var submitter = new Viz.Services.StreamLongRunningOperationSubmitter({
                            pollingInterval     : 1000,
                            serviceStart        : Viz.Services.ReportingWCF.BeginRenderReportActionRequest,
                            serviceParams       : {
                                Key            : item.KeyActionRequest,
                                exportType     : exportType,
                                reportTitle    : 'ActionRequest',
                                reportFile     : 'ActionRequestStandard.rdlc',
                                formatPageType : Viz.Configuration.GetEnumValue('FormatPageType', 'A4Portrait')

                            },
                            isFileResult        : true,
                            showProgress        : true,
                            progressWindowTitle : $lang('msg_application_export_in_progress'),
                            allowCancel         : true,
                            allowEmailResult    : true
                        });
                submitter.start();
            }

        });
Ext.reg("vizGridActionRequest", Viz.grid.ActionRequest);

// Static Methods
Ext.apply(Viz.grid.ActionRequest, {

            /**
             * Launch the chart wizard
             */
            launchWizard : function() {
                var entity = {
                    // forces the initial value of requestor as the user Occupant.KeyOccupant
                    Requestor        : Viz.App.Global.User.ProviderUserKey.KeyOccupant,
                    // forces the initial value of location as the user Occupant.KeyLocation
                    KeyLocation      : Viz.App.Global.User.ProviderUserKey.Occupant.KeyLocation,
                    LocationLongPath : Viz.App.Global.User.ProviderUserKey.Occupant.LocationLongPath
                };

                Viz.openFormCrud({
                            common : {
                                width               : 800,
                                height              : 500,
                                xtype               : 'vizFormActionRequestWizard',
                                title               : $lang("msg_actionrequest_add"),
                                iconCls             : 'viz-icon-small-wizard',
                                titleEntity         : 'Title',
                                serviceParamsEntity : [{
                                            name  : 'Key',
                                            value : 'KeyActionRequest'
                                        }]
                            }
                        }, 'create', entity);
            }

        });

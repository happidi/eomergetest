﻿/**
 * @class Viz.grid.CheckboxSelectionModel
 * <p>
 * A checkbox selection model with paging
 * </p>
 * @extends Ext.grid.CheckboxSelectionModel
 */
Viz.grid.CheckboxSelectionModel = Ext.extend(Ext.grid.CheckboxSelectionModel, {
            multiSelectState    : null,
            statusLabelId       : null,
            nonRemovableColumn  : true,

            clearSelection      : function() {
                this.multiSelectState = {};
                Ext.getCmp(this.statusLabelId).setText(0);
                this.clearSelections();
                this.updateSelectionHook();
            },

            reloadSelection     : function() {
                var rowsIndexesToSelectInPage = [];
                this.grid.store.data.each(function(row, index) {
                            if (row.json.KeyUser in this.multiSelectState) {
                                rowsIndexesToSelectInPage.push(index);
                            }
                        }, this);
                this.selectRows(rowsIndexesToSelectInPage);
                Ext.getCmp(this.statusLabelId).setText(Object.size(this.multiSelectState));
            },

            onRowSelect         : function(model, index, row) {
                this.multiSelectState[row.json.KeyUser] = row.json;
                // console.log(this.multiSelectState);
                var size = Object.size(this.multiSelectState);
                Ext.getCmp(this.statusLabelId).setText(size);
                this.updateSelectionHook();
            },

            onRowDeselect       : function(model, index, row) {
                delete this.multiSelectState[row.json.KeyUser];
                // console.log(this.multiSelectState);
                var size = Object.size(this.multiSelectState);
                Ext.getCmp(this.statusLabelId).setText(size);
                this.updateSelectionHook();
            },

            updateSelectionHook : function() {
                if (this.updateSelectionHookHandler) {
                    this.updateSelectionHookHandler(this);
                }
            },

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor         : function(config) {
                this.multiSelectState = {};

                config = config || {};

                this.statusLabelId = config.statusLabelId;
                this.updateSelectionHookHandler = config.updateSelectionHookHandler;

                var forcedConfig = {};
                var defaultConfig = {
                    listeners : {
                        rowselect   : {
                            fn    : this.onRowSelect,
                            scope : this
                        },
                        rowdeselect : {
                            fn    : this.onRowDeselect,
                            scope : this
                        }
                    }
                }

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.CheckboxSelectionModel.superclass.constructor.call(this, config);
            }
        });
﻿/**
 * @class Viz.grid.AuditEntity
 * <p>
 * A grid exposing the business entity AuditEntity
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.AuditEntity = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_auditentity_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden  : !$authorized('@@@@ AuditEntity - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_auditentity_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden  : !$authorized('@@@@ AuditEntity - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_auditentity_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ AuditEntity - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common       : {
                            width            : 450,
                            height           : 200,
                            xtype            : 'vizFormAuditEntity',
                            messageSaveStart : 'AuditEntityChangeStart',
                            messageSaveEnd   : 'AuditEntityChange',
                            readonly         : !$authorized('@@@@ AuditEntity - Full Control')
                        },
                        create       : {
                            title   : $lang("msg_auditentity_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update       : {
                            title               : $lang("msg_auditentity_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'Subject',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyAuditEntity'
                                    }]
                        },
                        updatebatch  : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.ServiceDeskWCF.AuditEntity_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.AuditEntity(),
                    columns      : Viz.Configuration.Columns.AuditEntity
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.AuditEntity.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("AuditEntityChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("AuditEntityChange", this.reloadStore, this);
                Viz.grid.AuditEntity.superclass.onDestroy.apply(this, arguments);
            }
        });
Ext.reg("vizGridAuditEntity", Viz.grid.AuditEntity);

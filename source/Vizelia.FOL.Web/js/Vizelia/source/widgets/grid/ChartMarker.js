﻿/**
 * @class Viz.grid.ChartMarker
 * <p>
 * A grid exposing the business entity ChartMarker
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.ChartMarker = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor          : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_chartmarker_add"),
                                iconCls : 'viz-icon-small-add', // hidden : !$authorized('ChartMarker_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_chartmarker_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('ChartMarker_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_chartmarker_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('ChartMarker_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 400,
                            height           : 300,
                            xtype            : 'vizFormChartMarker',
                            messageSaveStart : 'ChartMarkerChangeStart',
                            messageSaveEnd   : 'ChartMarkerChange'
                        },
                        create      : {
                            title   : $lang("msg_chartmarker_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_chartmarker_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyChartMarker'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.ChartMarker_FormUpdateBatch
                        }
                    },
                    store        : config.store || new Viz.store.ChartMarker({
                                KeyChart : config.KeyChart
                            }),
                    columns      : Viz.Configuration.Columns.ChartMarker
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.ChartMarker.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("ChartMarkerChange", this.reloadStore, this);
            },

            /**
             * @private Handler for updating the entity with the parent KeyChart.
             */
            onBeforeAddRecord    : function(grid, entity) {
                var KeyChart = this.KeyChart;
                if (KeyChart == '' || KeyChart == null) {
                    return false;
                }
                entity.KeyChart = KeyChart;
                return true;
            },
            /**
             * @private Handler for updating the entity with the parent KeyChart.
             */
            onBeforeUpdateRecord : function(grid, record, entity) {
                var KeyChart = this.KeyChart;
                if (KeyChart == '' || KeyChart == null) {
                    return false;
                }
                entity.KeyChart = KeyChart;
                return true;
            },

            /**
             * Destroy
             */
            onDestroy            : function() {
                Viz.util.MessageBusMgr.unsubscribe("ChartMarkerChange", this.reloadStore, this);
                Viz.grid.ChartMarker.superclass.onDestroy.apply(this, arguments);
            }

        });

Ext.reg("vizGridChartMarker", Viz.grid.ChartMarker);
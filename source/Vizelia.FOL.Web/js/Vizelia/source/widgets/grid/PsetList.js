﻿Ext.namespace('Viz.grid');

/**
 * @class Viz.grid.PsetList
 * @extends Viz.grid.GridPanel The Grid Pset List.
 */
Viz.grid.PsetList = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                this.store = new Viz.store.ListElement({
                            listName : 'PsetListDefinition'
                        });

                var defaultConfig = {
                    hasRowEditor : false,
                    hasToolbar   : false,
                    hasPagingToolbar: false,
                    stripeRows   : false,
                    hasFilter    : false,
                    iconCls      : 'viz-icon-small-list',
                    title        : $lang('msg_component_combo'),
                    tbar         : [{
                                text    : $lang('msg_add'),
                                iconCls : 'viz-icon-small-list-add',
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_update'),
                                iconCls : 'viz-icon-small-list-update',
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {

                                text    : $lang("msg_delete"),
                                iconCls : 'viz-icon-small-list-delete',
                                scope   : this,
                                hidden  : true,// no delete here, to delete occurs when all element of the list are deleted.
                                handler : this.onDeleteRecord
                            }],
                    columns      : Viz.Configuration.Columns.PsetList,
                    formConfig   : {
                        common : {
                            width          : 650,
                            height         : 350,
                            xtype          : 'vizFormPsetList',
                            // messageSaveStart : 'PsetListChangeStart',
                            messageSaveEnd : 'PsetListChange'
                        },
                        create : {
                            title   : $lang("msg_add"),
                            iconCls : 'viz-icon-small-list-add'

                        },
                        update : {
                            title       : $lang("msg_update"),
                            iconCls     : 'viz-icon-small-list-update',
                            titleEntity : 'Label'
                        }
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.PsetList.superclass.constructor.call(this, config);
                this.on("beforeupdaterecord", function(grid, record, entity) {
                            var selected = grid.getSelectionModel().getSelected();
                            var listName = selected.get("MsgCode");
                            Ext.apply(entity, {
                                        listName : listName
                                    });

                        }, this);
                // this.on("rowdblclick", this.onManage.createDelegate(this, [true]), this);
                Viz.util.MessageBusMgr.subscribe("PsetListChange", this.reloadStore, this);
            },

            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("PsetListChange", this.reloadStore, this);
                Viz.grid.PsetList.superclass.onDestroy.apply(this, arguments);
            }
        });

Ext.reg('vizGridPsetList', Viz.grid.PsetList);

﻿/**
 * @class Viz.grid.Link
 * <p>
 * A grid exposing the business entity Link
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Link = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_link_add"),
                                //iconCls : 'viz-icon-small-link-add',
                                iconCls: 'viz-icon-small-link-add',
                                hidden  : !$authorized('@@@@ Link - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_link_update"),
                                //iconCls : 'viz-icon-small-link-update',
                                iconCls: 'viz-icon-small-link-update',
                                hidden  : !$authorized('@@@@ Link - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_link_delete"),
                                //iconCls : 'viz-icon-small-link-delete',
                                iconCls: 'viz-icon-small-link-delete',
                                hidden  : !$authorized('@@@@ Link - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        auditEnabled : false,
                        common         : {
                            width            : 850,
                            height           : 580,
                            xtype            : 'vizFormLink',
                            messageSaveStart : 'LinkChangeStart',
                            messageSaveEnd   : 'LinkChange',
                            readonly         : !$authorized('@@@@ Link - Full Control')
                        },
                        create         : {
                            title   : $lang("msg_link_add"),
                            iconCls : 'viz-icon-small-link-add'
                        },
                        update         : {
                            title               : $lang("msg_link_update"),
                            iconCls             : 'viz-icon-small-link-update',
                            titleEntity: 'LocalId',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyLink'
                                    }]
                        },
                        updatebatch    : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-link-update',
                            serviceHandlerUpdateBatch : Viz.Services.CoreWCF.Link_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.Link(),
                    columns      : Viz.Configuration.Columns.Link
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.Link.superclass.constructor.call(this, config);
                this.on('deleterecord', this.deleteRecordHandler, this);
                Viz.util.MessageBusMgr.subscribe("LinkChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("LinkChange", this.reloadStore, this);
                Viz.grid.Link.superclass.onDestroy.apply(this, arguments);
            },
             deleteRecordHandler   : function() {
                Viz.util.MessageBusMgr.fireEvent.defer(300, Viz.util.MessageBusMgr, ['LinkChange', {}]);
            }
        });
Ext.reg("vizGridLink", Viz.grid.Link);

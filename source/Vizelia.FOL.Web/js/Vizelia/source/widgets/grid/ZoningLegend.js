﻿/**
 * @class Viz.grid.ZoningLegend
 * <p>
 * A grid exposing the business entity ZoningLegend
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.ZoningLegend = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                this.store = new Viz.data.WCFJsonSimpleStore({
                            serviceHandler : Viz.Services.SpaceManagementWCF.BuildingStorey_GetZoning,
                            serviceParams  : {
                                KeyBuildingStorey : config.KeyBuildingStorey
                            },
                            groupField     : 'Zoning_KeyClassificationItem'
                        });

                // define a custom summary function
                Ext.ux.grid.GroupSummary.Calculations['rgb'] = function(v, record, field) {
                    var r = record.json.Zoning.ColorR;
                    var g = record.json.Zoning.ColorG;
                    var b = record.json.Zoning.ColorB;
                    var backgroundcolor = "background-color:rgb(' + r + ',' + g + ',' + b + ')";
                    // return '<span class = "zoninglegend-color" style = "' + backgroundcolor + '"></span>';
                    return '<img style="cursor:pointer" class="x-tree-node-icon x-tree-node-inline-icon"  src="~/color.asmx?R=' + r + '&amp;G=' + g + '&amp;B=' + b + '">';
                };

                Ext.ux.grid.GroupSummary.Calculations['title'] = Ext.ux.grid.GroupSummary.Calculations['max'];

                // define a custom summary function
                Ext.ux.grid.GridSummary.Calculations['rgb'] = function(v, record, field) {
                    return '';
                };

                Ext.ux.grid.GridSummary.Calculations['title'] = function(v, record, field) {
                    return 'TOTAL';
                };

                var summaryGroup = new Ext.ux.grid.GroupSummary();
                var summaryGrid = new Ext.ux.grid.GridSummary();

                var defaultConfig = {
                    title            : 'Legende',
                    hasRowEditor     : false,
                    stripeRows       : false,
                    store            : this.store,
                    hasRowNumber     : false,
                    hasPagingToolbar : false,
                    stripeRows       : false,
                    autoLoadStore    : false,
                    cls              : 'zoninglegend',
                    autoExpandColumn : 'Zoning_Title',
                    sm               : new Ext.grid.CellSelectionModel(),
                    view             : new Ext.grid.GroupingView({
                                forceFit           : false,
                                startCollapsed     : true,
                                showGroupName      : false,
                                enableNoGroups     : false,
                                enableGroupingMenu : false,
                                hideGroupedColumn  : true,
                                groupTextTpl       : '{text}'
                            }),
                    plugins          : [summaryGroup, summaryGrid],
                    columns          : [{
                                header          : 'Color',
                                summaryType     : 'rgb',
                                width           : 40,
                                dataIndex       : '',
                                summaryRenderer : function(v, params, data) {
                                    return v;
                                },
                                renderer        : function(data, cell, record) {
                                    return '';
                                }
                            }, {
                                header          : 'Classification',
                                dataIndex       : 'Zoning_KeyClassificationItem',
                                id              : 'Zoning_KeyClassificationItem',                              
                                summaryType     : 'title',
                                summaryRenderer : function(v, params, data) {
                                    if (v == 'TOTAL')
                                        params.style += "text-align:right;";
                                    return v;
                                }
                            }, {
                                header          : 'Classification',
                                dataIndex       : 'Zoning_Title',
                                id              : 'Zoning_Title',
                                summaryType     : 'title',
                                summaryRenderer : function(v, params, data) {
                                    if (v == 'TOTAL')
                                        params.style += "text-align:right;";
                                    return v;
                                }
                            }, {
                                header             : 'Number of spaces',
                                dataIndex          : 'KeySpace',
                                summaryType        : 'count',
                                align              : 'right',
                                width              : 100,
                                lllsummaryRenderer : function(v, params, data) {
                                    return v;
                                }
                            }, {
                                header            : 'Area (M2)',
                                dataIndex         : 'AreaValue',
                                xtype             : 'numbercolumn',
                                summaryType       : 'sum',
                                width             : 100,
                                align             : 'right',
                                llsummaryRenderer : function(v, params, data) {
                                    return v;
                                }
                            }]
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                this.addEvents(
                        /**
                         * @event toggle Fires when an legend item is toogled.
                         * @param {Viz.grid.ZoningLegend} this
                         * @param {Object} item the item that is beeing toggled.
                         * @param {Boolean} display true if the item should be displayed, false otherwise
                         */
                        'toogle');

                Viz.grid.ZoningLegend.superclass.constructor.call(this, config);

                this.on("groupclick", function(grid, grouptitle, groupvalue, event) {
                            var target = event.getTarget();
                            var display;
                            if (target.tagName == "SPAN") {
                                var el = Ext.get(target);
                                if (el.hasClass("zoninglegend-color-black"))
                                    el.removeClass("zoninglegend-color-black");
                                else
                                    el.addClass("zoninglegend-color-black");
                            }
                            if (target.tagName == "IMG") {
                                var el = Ext.get(target);
                                var src = el.dom.src;
                                if (src.contains("&cancel=true")) {
                                    src = src.replace("&cancel=true", "");
                                    display = true;
                                }
                                else {
                                    src += "&cancel=true";
                                    display = false;
                                }
                                this.fireEvent("toogle", this, groupvalue, display);
                                el.dom.src = src;

                            }
                        }, this);

            }

        });

Ext.reg("vizGridZoningLegend", Viz.grid.ZoningLegend);

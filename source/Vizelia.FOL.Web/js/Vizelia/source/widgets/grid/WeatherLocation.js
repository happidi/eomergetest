﻿/**
 * @class Viz.grid.WeatherLocation
 * <p>
 * A grid exposing the business entity WeatherLocation
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.WeatherLocation = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor              : function(config) {
                config = config || {};
                this._favoritebutton_id = Ext.id();

                var defaultConfig = {
                    hasRowEditor  : false,
                    stripeRows    : false,
                    hasToolbar    : false,
                    autoLoadStore : false,
                    tbar          : [{
                                id            : this._favoritebutton_id,
                                iconCls       : 'viz-icon-small-chart-favorite',
                                text          : $lang("msg_chart_favorite"),
                                enableToggle  : true,
                                toggleHandler : function(button, state) {
                                    Viz.util.Portal.onToggleFavoriteButton.createDelegate(this, [button, state])();
                                },
                                scope         : this,
                                pressed       : true
                            }, '-', {
                                text    : $lang("msg_weatherlocation_add"),
                                iconCls : 'viz-icon-small-weatherlocation-add',
                                hidden  : !$authorized('@@@@ Weather Location - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_weatherlocation_update"),
                                iconCls : 'viz-icon-small-weatherlocation-update',
                                hidden  : !$authorized('@@@@ Weather Location - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_weatherlocation_delete"),
                                iconCls : 'viz-icon-small-weatherlocation-delete',
                                hidden  : !$authorized('@@@@ Weather Location - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_weatherlocation_view"),
                                iconCls : 'viz-icon-small-display',
                                // hidden : !$authorized('@@@@ Weather Location - Full Control'),
                                scope   : this,
                                handler : this.onWeatherLocationDisplay
                            }, '-', {
                                text    : $lang("msg_portlets"),
                                iconCls : 'viz-icon-small-portlet',
                                hidden  : !$authorized('@@@@ Portlet'),
                                scope   : this,
                                handler : Viz.util.Portal.displayPortletGridFromEntity.createDelegate(this)
                            }],
                    formConfig    : this.getFormConfig(),
                    store         : new Viz.store.WeatherLocation(),
                    columns       : Viz.Configuration.Columns.WeatherLocation
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.WeatherLocation.superclass.constructor.call(this, config);
                this.addEvents(
                        /**
                         * @event beforeweatherlocationdisplay Fires before displaying a WeatherLocation.
                         * @param {Viz.grid.GridPanel} this.
                         * @param {Object} record The record of the WeatherLocation that is being displayed.
                         * @param {Object} entity The entity of the WeatherLocation that is being displayed. It could be changed by the handler.
                         */
                        'beforeweatherlocationdisplay');

                Viz.util.MessageBusMgr.subscribe("WeatherLocationChange", this.reloadStore, this);

                this.on({
                            render          : Viz.util.Portal.gridRender,
                            filteravailable : Viz.util.Portal.gridFilterAvailable,
                            filtercleared   : Viz.util.Portal.toggleFavoriteButton.createDelegate(this, [this._favoritebutton_id, false]),
                            scope           : this
                        });
            },

            /**
             * After deleting we make sure the portlet is deleted as well.
             * @param {} record
             * @param {} selections
             */
            onAfterDeleteRecord      : function(record, selections) {
                for (var i = 0, r; r = selections[i]; i++) {
                    Viz.util.MessageBusMgr.fireEvent('PortletDelete', r.json);
                }
            },
            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig            : function() {
                return {
                    common      : {
                        width            : 400,
                        height           : 350,
                        xtype            : 'vizFormWeatherLocation',
                        messageSaveStart : 'WeatherLocationChangeStart',
                        messageSaveEnd   : 'WeatherLocationChange',
                        readonly         : !$authorized('@@@@ Weather Location - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_weatherlocation_add"),
                        iconCls : 'viz-icon-small-weatherlocation-add'
                    },
                    update      : {
                        title               : $lang("msg_weatherlocation_update"),
                        iconCls             : 'viz-icon-small-weatherlocation-update',
                        titleEntity         : 'Name',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyWeatherLocation'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-weatherlocation-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.WeatherLocation_FormUpdateBatch
                    }
                };
            },
            /**
             * Destroy
             */
            onDestroy                : function() {
                Viz.util.MessageBusMgr.unsubscribe("WeatherLocationChange", this.reloadStore, this);
                Viz.grid.WeatherLocation.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for displaying a weatherlocation.
             * @method onChartDisplay
             */
            onWeatherLocationDisplay : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    if (this.fireEvent("beforeweatherlocationdisplay", this, record, entity)) {
                        Viz.grid.WeatherLocation.openWeatherLocationDisplay(entity, 780, 240);
                    }
                }
            }
        });
Ext.reg("vizGridWeatherLocation", Viz.grid.WeatherLocation);

// Static Methods
Ext.apply(Viz.grid.WeatherLocation, {
            /**
             * Displays a WeatherLocation.
             * @method openWeatherLocationDisplay
             * @static
             * @param {Viz.BusinessEntity.WeatherLocation} entity : the WeatherLocation entity.
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openWeatherLocationDisplay : function(entity, width, height) {
                if (entity.DisplayType == Vizelia.FOL.BusinessEntities.WeatherDisplayType.Horizontal) {
                    width = 775;
                    height = 220;
                }
                else {
                    width = 380;
                    height = 450;
                }

                var win = new Viz.portal.PortletWindow({
                            entity        : entity,
                            xtypePortlet  : 'vizPortletWeather',
                            entityPortlet : {
                                TypeEntity : 'Vizelia.FOL.BusinessEntities.WeatherLocation'
                            },
                            updateHandler : Viz.grid.WeatherLocation.update,
                            width         : width,
                            height        : height
                        });
                win.show();
            },

            /**
             * Create a new Weather (to be called from desktop or tree)
             * @param {} callback
             */
            create                     : function(entity) {
                Viz.openFormCrud(Viz.grid.WeatherLocation.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing Weather (to be called from desktop or tree)
             * @param {} callback
             */
            update                     : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.WeatherLocation.prototype.getFormConfig(), 'update', entity);
                }
            }

        });
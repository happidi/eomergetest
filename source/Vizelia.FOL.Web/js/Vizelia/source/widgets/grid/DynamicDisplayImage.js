﻿/**
 * @class Viz.grid.DynamicDisplayImage
 * <p>
 * A grid exposing the business entity DynamicDisplayImage
 * </p>
 * @extends Viz.grid.GridPanel
 */

Viz.grid.DynamicDisplayImage = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor                  : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_dynamicdisplayimage_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden  : !$authorized('@@@@ Dynamic Display Image - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_dynamicdisplayimage_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden  : !$authorized('@@@@ Dynamic Display Image - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_dynamicdisplayimage_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ Dynamic Display Image - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_dynamicdisplayimage_view"),
                                iconCls : 'viz-icon-small-display',
                                hidden  : !$authorized('@@@@ Dynamic Display Image - Full Control'),
                                scope   : this,
                                handler : this.onDynamicDisplayImageDisplay
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : {
                        xtype : 'vizStoreDynamicDisplayImage'
                    },
                    columns      : Viz.Configuration.Columns.DynamicDisplayImage
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.DynamicDisplayImage.superclass.constructor.call(this, config);

                this.addEvents(
                        /**
                         * @event beforechartdiplay Fires before displaying a chart.
                         * @param {Viz.grid.GridPanel} this.
                         * @param {Object} record The record of the chart that is being displayed.
                         * @param {Object} entity The entity of the chart that is being displayed. It could be changed by the handler.
                         */
                        'beforedynamicdisplayimagedisplay');

                Viz.util.MessageBusMgr.subscribe("DynamicDisplayImageChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy                    : function() {
                Viz.util.MessageBusMgr.unsubscribe("DynamicDisplayImageChange", this.reloadStore, this);
                Viz.grid.DynamicDisplayImage.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig                : function() {
                return {
                    common      : {
                        width            : 500,
                        height           : 270,
                        xtype            : 'vizFormDynamicDisplayImage',
                        messageSaveStart : 'DynamicDisplayImageChangeStart',
                        messageSaveEnd   : 'DynamicDisplayImageChange',
                        readonly         : !$authorized('@@@@ Dynamic Display Image - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_dynamicdisplayimage_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_dynamicdisplayimage_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'Name',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyDynamicDisplayImage'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.DynamicDisplayImage_FormUpdateBatch
                    }
                };
            },
            /**
             * Handler for displaying a dynamicdisplayimage.
             * @method onChartDisplay
             */
            onDynamicDisplayImageDisplay : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    if (this.fireEvent("beforechartdisplay", this, record, entity)) {
                        Viz.grid.DynamicDisplayImage.openDynamicDisplayImageDisplay(entity);
                    }
                }
            }
        });

Ext.reg("vizGridDynamicDisplayImage", Viz.grid.DynamicDisplayImage);

// Static Methods
Ext.apply(Viz.grid.DynamicDisplayImage, {
            /**
             * Displays a DynamicDisplayImage.
             * @method openDynamicDisplayImageDisplay
             * @static
             * @param {Viz.BusinessEntity.DynamicDisplay} entity : the DynamicDisplay entity.
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openDynamicDisplayImageDisplay : function(entity, width, height) {
                var win = new Viz.portal.PortletWindow({
                            entity        : entity,
                            xtypePortlet  : 'vizChartDynamicDisplayImageImage',
                            updateHandler : Viz.grid.DynamicDisplayImage.update
                        });
                win.show();
            },

            /**
             * Create a new DynamicDisplay (to be called from desktop or tree)
             * @param {} callback
             */
            create                         : function(entity) {
                Viz.openFormCrud(Viz.grid.DynamicDisplayImage.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing DynamicDisplay (to be called from desktop or tree)
             * @param {} callback
             */
            update                         : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.DynamicDisplayImage.prototype.getFormConfig(), 'update', entity);
                }
            }
        });

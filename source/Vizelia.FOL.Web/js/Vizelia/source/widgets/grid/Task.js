﻿/**
 * @class Viz.grid.Task
 * <p>
 * A grid exposing the business entity Task
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Task = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor           : function(config) {
                config = config || {};
                this._buttonAction = Ext.id();
                var defaultConfig = {
                    hasRowEditor  : false,
                    stripeRows    : false,
                    hidden        : !$authorized('@@@@ Task'),
                    autoLoadStore : false,
                    hasToolbar    : false,
                    tbar          : [{
                                text    : $lang("msg_task_add"),
                                iconCls : 'viz-icon-small-technician-add',
                                hidden  : !$authorized('@@@@ Task - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_task_update"),
                                iconCls : 'viz-icon-small-technician-update',
                                hidden  : !$authorized('@@@@ Task - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_task_delete"),
                                iconCls : 'viz-icon-small-technician-delete',
                                hidden  : !$authorized('@@@@ Task - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_otheractions"),
                                id      : this._buttonAction,
                                iconCls : 'viz-icon-small-action',
                                scope   : this,
                                menu    : new Ext.menu.Menu({
                                            items     : [], // an empty menu to get the arrow on the button.
                                            listeners : {
                                                beforeshow : this.updateWorkflowActions,
                                                scope      : this
                                            }
                                        })
                            }],
                    formConfig    : {
                        common      : {
                            width            : 580,
                            height           : 450,
                            xtype            : 'vizFormTask',
                            messageSaveStart : 'TaskChangeStart',
                            messageSaveEnd   : 'TaskChange',
                            readonly         : !$authorized('@@@@ Task - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_task_add"),
                            iconCls : 'viz-icon-small-technician-add'
                        },
                        update      : {
                            title               : $lang("msg_task_update"),
                            iconCls             : 'viz-icon-small-technician-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyTask'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-technician-update',
                            serviceHandlerUpdateBatch : Viz.Services.ServiceDeskWCF.Task_FormUpdateBatch
                        }
                    },
                    store         : new Viz.store.Task(),
                    columns       : Viz.Configuration.Columns.Task
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.Task.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("TaskChange", this.reloadStore, this);

                this.on({
                            rowcontextmenu : {
                                scope : this,
                                fn    : this.onRowContextMenu
                            }
                        });

            },
            /**
             * @private Handler for creating the entity with the related ActionRequest.
             */
            onBeforeAddRecord     : function(grid, entity) {
                entity.KeyActionRequest = grid.store.serviceParams.KeyActionRequest;
                return true;
            },

            /**
             * Destroy
             */
            onDestroy             : function() {
                Viz.util.MessageBusMgr.unsubscribe("TaskChange", this.reloadStore, this);
                Viz.grid.Task.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * @private
             */
            onRowContextMenu      : function(grid, row, event) {
                event.stopEvent();
                var xy = event.getXY();
                this.getSelectionModel().selectRow(row, false);
                if (this.hideMenu == true)
                    return;

                this.workflowUtil = new Viz.util.Workflow({
                            comp      : this,
                            listeners : {
                                success            : function(o, actions) {
                                    ctxMenuWait.destroy();
                                    var ctxMenu = new Ext.menu.Menu({
                                                items : actions
                                            });
                                    ctxMenu.on("hide", function(menu) {
                                                menu.destroy();
                                            });

                                    ctxMenu.add(this.buildPrintActions());

                                    if (ctxMenu.items.getCount() > 0) {
                                        ctxMenu.showAt(xy);
                                    }
                                    else
                                        ctxMenu.destroy();
                                },
                                failure            : function() {
                                    ctxMenuWait.destroy();
                                },
                                beforeaction       : function() {
                                    if (this.loadMask)
                                        Ext.get(this.bwrap).mask();
                                    return true;
                                },
                                afteractionsuccess : function() {
                                    Ext.get(this.bwrap).unmask();
                                    this.publishMessage("TaskChange");
                                    this.publishMessage("ActionRequestChange");
                                },
                                afteractionfailure : function() {
                                    Ext.get(this.bwrap).unmask();
                                },
                                scope              : this
                            }
                        });

                var ctxMenuWait = this.workflowUtil.buildWaitContextMenu();
                ctxMenuWait.showAt(xy);
                this.workflowUtil.getStateMachineActions();
            },
            updateWorkflowActions : function() {
                var button = Ext.getCmp(this._buttonAction);
                if (this.getSelectionModel().getSelected() == null) {
                    return false;
                }
                // this.getSelectionModel().selectRow(row, false);
                if (this.hideMenu == true)
                    return false;

                this.workflowUtil = new Viz.util.Workflow({
                            comp      : this,
                            listeners : {
                                success            : function(o, actions) {
                                    ctxMenuWait.destroy();
                                    var ctxMenu = new Ext.menu.Menu({
                                                items : actions
                                            });
                                    ctxMenu.on("hide", function(menu) {
                                                menu.destroy();
                                            });

                                    if (ctxMenu.items.getCount() > 0) {
                                        ctxMenu.show(button.getEl());
                                    }
                                    else
                                        ctxMenu.destroy();
                                },
                                failure            : function() {
                                    ctxMenuWait.destroy();
                                },
                                beforeaction       : function() {
                                    if (this.loadMask)
                                        Ext.get(this.bwrap).mask();
                                    return true;
                                },
                                afteractionsuccess : function() {
                                    Ext.get(this.bwrap).unmask();
                                    this.publishMessage("TaskChange");
                                    this.publishMessage("ActionRequestChange");
                                },
                                afteractionfailure : function() {
                                    Ext.get(this.bwrap).unmask();
                                },
                                scope              : this
                            }
                        });

                var ctxMenuWait = this.workflowUtil.buildWaitContextMenu();
                ctxMenuWait.show(button.getEl());
                this.workflowUtil.getStateMachineActions();
                return false;
            },

            /**
             * Builds the print actions
             */
            buildPrintActions     : function(item) {
                var actions = [];
                actions.push({
                            scope   : this,
                            text    : $lang("msg_application_export"),
                            tooltip : $lang("msg_application_export"),
                            iconCls : 'viz-icon-small-report',
                            menu    : [{
                                        text    : 'PDF',
                                        iconCls : 'viz-icon-small-pdf',
                                        handler : this.printActionRequest.createDelegate(this, [Vizelia.FOL.BusinessEntities.ExportType.Pdf])
                                    }, {
                                        text    : 'Excel',
                                        iconCls : 'viz-icon-small-excel',
                                        handler : this.printActionRequest.createDelegate(this, [Vizelia.FOL.BusinessEntities.ExportType.Excel2007])
                                    }]
                        });
                return actions;
            },

            /**
             * Printing a selected ticket
             * @param {Viz.BusinessEntity.ActionRequest} item
             */
            printActionRequest    : function(exportType) {
                var item = this.workflowUtil.getWorkflowEntity(this);
                var submitter = new Viz.Services.StreamLongRunningOperationSubmitter({
                            pollingInterval     : 1000,
                            serviceStart        : Viz.Services.ReportingWCF.BeginRenderReportTask,
                            serviceParams       : {
                                Key            : item.KeyActionRequest,
                                KeyTask        : item.KeyTask,
                                exportType     : exportType,
                                reportTitle    : 'Task',
                                reportFile     : 'ActionRequestStandard.rdlc',
                                formatPageType : Viz.Configuration.GetEnumValue('FormatPageType', 'A4Portrait')

                            },
                            isFileResult        : true,
                            showProgress        : true,
                            progressWindowTitle : $lang('msg_application_export_in_progress'),
                            allowCancel         : true,
                            allowEmailResult    : true
                        });
                submitter.start();
            }

        });

Ext.reg("vizGridTask", Viz.grid.Task);

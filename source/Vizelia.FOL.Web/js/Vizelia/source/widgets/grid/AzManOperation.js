﻿/**
 * @class Viz.grid.AzManOperation
 * <p>
 * A grid exposing the business entity AzManItem Operation
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.AzManOperation = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor      : false,
                    enableRowDblClick : false,
                    stripeRows        : false,
                    hasToolbar        : false,
                    tbar              : [{
                                text    : $lang("msg_azmanoperation_add"),
                                iconCls : 'viz-icon-small-azmanoperation-add',
                                hidden  : !$authorized('@@@@ Operation management - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_azmanoperation_getparenttree"),
                                iconCls : 'viz-icon-small-azmanoperation-add',
                                hidden  : !$authorized('@@@@ Operation management - Full Control'),
                                scope   : this,
                                handler : function() {

                                    var win = new Ext.Window({
                                                title       : $lang('msg_azmanoperation_tree'),
                                                iconCls     : 'viz-icon-small-azmanoperation',
                                                width       : 800,
                                                height      : 600,
                                                maximazable : true,
                                                minimizable : true,
                                                layout      : 'fit',
                                                items       : {
                                                    xtype : 'vizTreeAzManOperation',
                                                    root  : {
                                                        id      : this.getSelectionModel().getSelected().id,
                                                        iconCls : this.getSelectionModel().getSelected().data.IconCls,
                                                        text    : this.getSelectionModel().getSelected().data.Name
                                                    }

                                                }
                                            });
                                    win.show();

                                }
                            }],
                    formConfig        : {
                        common      : {
                            width            : 450,
                            height           : 200,
                            xtype            : 'vizFormAzManOperation',
                            messageSaveStart : 'AzManOperationChangeStart',
                            messageSaveEnd   : 'AzManOperationChange',
                            readonly         : !$authorized('@@@@ Operation management - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_azmanoperation_add"),
                            iconCls : 'viz-icon-small-azmanoperation-add'
                        },
                        update      : {
                            title               : $lang("msg_azmanoperation_update"),
                            iconCls             : 'viz-icon-small-azmanoperation-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'Id'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-azmanoperation-update',
                            serviceHandlerUpdateBatch : Viz.Services.AuthenticationWCF.AzManOperation_FormUpdateBatch
                        }
                    },
                    store             : new Viz.store.AzManOperation(),
                    columns           : Viz.Configuration.Columns.AzManItem
                };
                var forcedConfig = {
                    auditEntityName : 'AuthorizationItem.Operation',
                    // avoids the grid from being masked endlessly after applying an AzManTask form.
                    loadMask        : false,
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.AzManOperation.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("AzManOperationChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("AzManOperationChange", this.reloadStore, this);
                Viz.grid.AzManOperation.superclass.onDestroy.apply(this, arguments);
            }

        });

Ext.reg("vizGridAzManOperation", Viz.grid.AzManOperation);

﻿/**
 * @class Viz.grid.RESTDataAcquisitionEndpoint
 * <p>
 * A grid exposing the business entity RESTDataAcquisitionEndpoint
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.RESTDataAcquisitionEndpoint = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor                          : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_restdataacquisitionendpoint_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden  : !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_restdataacquisitionendpoint_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden  : !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_restdataacquisitionendpoint_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_restdataacquisitionendpoint_view"),
                                iconCls : 'viz-icon-small-display',
                                hidden  : !$authorized('@@@@ Data Acquistion'),
                                scope   : this,
                                handler : this.onRESTDataAcquisitionEndpointDisplay
                            }],
                    formConfig   : {
                        common      : {
                            width            : 600,
                            height           : 300,
                            xtype            : 'vizFormRESTDataAcquisitionEndpoint',
                            messageSaveStart : 'RESTDataAcquisitionEndpointChangeStart',
                            messageSaveEnd   : 'RESTDataAcquisitionEndpointChange',
                            readonly         : !$authorized('@@@@ Data Acquistion - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_restdataacquisitionendpoint_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_restdataacquisitionendpoint_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyDataAcquisitionEndpoint'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.RESTDataAcquisitionEndpoint_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.RESTDataAcquisitionEndpoint({
                                KeyDataAcquisitionContainer : config.KeyDataAcquisitionContainer
                            }),
                    columns      : Viz.Configuration.Columns.RESTDataAcquisitionEndpoint
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.RESTDataAcquisitionEndpoint.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("RESTDataAcquisitionEndpointChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy                            : function() {
                Viz.util.MessageBusMgr.unsubscribe("RESTDataAcquisitionEndpointChange", this.reloadStore, this);
                Viz.grid.RESTDataAcquisitionEndpoint.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyDataAcquisitionContainer and ProviderType.
             */
            onBeforeAddRecord                    : function(grid, entity) {
                var KeyDataAcquisitionContainer = this.KeyDataAcquisitionContainer;
                if (KeyDataAcquisitionContainer == '' || KeyDataAcquisitionContainer == null) {
                    return false;
                }
                entity.KeyDataAcquisitionContainer = KeyDataAcquisitionContainer;
                entity.ProviderType = this.entityContainer.ProviderType;
                return true;
            },

            onBeforeUpdateRecord                 : function(grid, entity) {
                entity.ProviderType = this.entityContainer.ProviderType;
                return true;
            },

            onRESTDataAcquisitionEndpointDisplay : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    Viz.grid.RESTDataAcquisitionEndpoint.openRESTDataAcquisitionEndpointDisplay(entity, 800, 550);

                }

            }
        });
Ext.reg("vizGridRESTDataAcquisitionEndpoint", Viz.grid.RESTDataAcquisitionEndpoint);

// Static Methods
Ext.apply(Viz.grid.RESTDataAcquisitionEndpoint, {
            /**
             * Displays a RESTDataAcquisitionEndpoint.
             * @method openRESTDataAcquisitionEndpoint
             * @static
             * @param {Viz.BusinessEntity.RESTDataAcquisitionEndpoint} entity : the RESTDataAcquisitionEndpoint entity.
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openRESTDataAcquisitionEndpointDisplay : function(entity, width, height, animateTarget) {
                var type = Viz.getEntityType(entity.__type);

                var win = new Ext.Window({
                            title       : entity.Name,
                            width       : 600,
                            height      : 400,
                            maximazable : true,
                            minimizable : true,
                            layout      : 'fit',
                            items       : {
                                xtype            : 'vizViewerImage',
                                stretchImage     : false,
                                serviceStream    : "Chart_GetStreamImageFromCache",
                                serviceInterface : 'Public.svc',
                                serviceHandler   : Viz.Services.EnergyWCF.DataAcquisitionEndpoint_BuildImage,
                                serviceParams    : {
                                    EndpointType    : type,
                                    KeyEndpoint     : entity.KeyDataAcquisitionEndpoint,
                                    applicationName : entity.Application ? entity.Application : Viz.App.Global.User.ApplicationName
                                }
                            }
                        });
                win.show();
            }

        });

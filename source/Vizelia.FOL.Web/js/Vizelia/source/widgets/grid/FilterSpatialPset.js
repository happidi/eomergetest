﻿/**
 * @class Viz.grid.FilterSpatialPset
 * <p>
 * A grid exposing the business entity FilterSpatialPset
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterSpatialPset = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_filterspatialpset_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterSpatialPset_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_filterspatialpset_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('FilterSpatialPset_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_filterspatialpset_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterSpatialPset_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_filterspatialpset_location_view"),
                                iconCls : 'viz-icon-small-display',
                                // hidden : !$authorized('FilterSpatialPset_write'),
                                scope   : this,
                                handler : this.onLocationDisplay
                            }],
                    formConfig   : {
                        common      : {
                            width            : 500,
                            height           : 250,
                            xtype            : 'vizFormFilterSpatialPset',
                            messageSaveStart : 'FilterSpatialPsetChangeStart',
                            messageSaveEnd   : 'FilterSpatialPsetChange'
                        },
                        create      : {
                            title   : $lang("msg_filterspatialpset_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_filterspatialpset_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'Value',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyFilterSpatialPset'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.FilterSpatialPset_FormUpdateBatch
                        }
                    },
                    store        : config.store || config.KeyChart ? new Viz.store.ChartFilterSpatialPset({
                                KeyChart : config.KeyChart
                            }) : new Viz.store.AlarmDefinitionFilterSpatialPset({
                                KeyAlarmDefinition : config.KeyAlarmDefinition
                            }),
                    columns      : Viz.Configuration.Columns.FilterSpatialPset
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterSpatialPset.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("FilterSpatialPsetChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterSpatialPsetChange", this.reloadStore, this);
                Viz.grid.FilterSpatialPset.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyChart or KeyAlarmDefinition.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyChart = this.KeyChart;
                var KeyAlarmDefinition = this.KeyAlarmDefinition;
                if (Ext.isEmpty(KeyChart) && Ext.isEmpty(KeyAlarmDefinition)) {
                    return false;
                }
                entity.KeyChart = KeyChart;
                entity.KeyAlarmDefinition = KeyAlarmDefinition;

                entity.Order = this.getMaxValueFromStore('Order') + 1;
                return true;
            },

            /**
             * Display the list of Locations corresponding to the created PsetFilters.
             */
            onLocationDisplay : function() {
                var win = new Ext.Window({
                            title       : $lang('msg_filterspatialpset_location_view'),
                            iconCls     : 'viz-icon-small-site',
                            maximizable : true,
                            width       : 550,
                            height      : 250,
                            layout      : 'fit',
                            items       : [{
                                        xtype              : 'vizGridFilterSpatialPsetLocation',
                                        KeyChart           : this.KeyChart,
                                        KeyAlarmDefinition : this.KeyAlarmDefinition
                                    }]
                        });
                win.show();
            }

        });

Ext.reg("vizGridFilterSpatialPset", Viz.grid.FilterSpatialPset);
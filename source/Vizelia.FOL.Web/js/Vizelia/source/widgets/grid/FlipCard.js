﻿/**
 * @class Viz.grid.Playlist
 * <p>
 * A grid exposing the business entity FlipCard
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FlipCard = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor         : function(config) {
                this._favoritebutton_id = Ext.id();
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                id            : this._favoritebutton_id,
                                iconCls       : 'viz-icon-small-chart-favorite',
                                text          : $lang("msg_chart_favorite"),
                                enableToggle  : true,
                                toggleHandler : function(button, state) {
                                    Viz.util.Portal.onToggleFavoriteButton.createDelegate(this, [button, state])();
                                },
                                scope         : this,
                                pressed       : true
                            }, {
                                text    : $lang("msg_flipcard_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden  : !$authorized('@@@@ FlipCard - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_flipcard_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden  : !$authorized('@@@@ FlipCard - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_flipcard_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ FlipCard - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_display"),
                                iconCls : 'viz-icon-small-display',
                                // hidden : !$authorized('@@@@ FlipCard - Full Control'),
                                scope   : this,
                                handler : this.onFlipCardDisplay
                            }, '-', {
                                text    : $lang("msg_portlets"),
                                iconCls : 'viz-icon-small-portlet',
                                hidden  : !$authorized('@@@@ Portlet'),
                                scope   : this,
                                handler : Viz.util.Portal.displayPortletGridFromEntity.createDelegate(this)
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : new Viz.store.FlipCard(),
                    columns      : Viz.Configuration.Columns.FlipCard
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.FlipCard.superclass.constructor.call(this, config);
                this.addEvents(
                        /**
                         * @event beforeflipcarddisplay Fires before displaying a FlipCard.
                         * @param {Viz.grid.GridPanel} this.
                         * @param {Object} record The record of the FlipCard that is being displayed.
                         * @param {Object} entity The entity of the FlipCard that is being displayed. It could be changed by the handler.
                         */
                        'beforeflipcarddisplay');
                this.on({
                            render          : Viz.util.Portal.gridRender,
                            filteravailable : Viz.util.Portal.gridFilterAvailable,
                            filtercleared   : Viz.util.Portal.toggleFavoriteButton.createDelegate(this, [this._favoritebutton_id, false]),
                            scope           : this
                        });

                Viz.util.MessageBusMgr.subscribe("FlipCardChange", this.reloadStore, this);
            },

            /**
             * After deleting we make sure the portlet is deleted as well.
             * @param {} record
             * @param {} selections
             */
            onAfterDeleteRecord : function(record, selections) {
                for (var i = 0, r; r = selections[i]; i++) {
                    Viz.util.MessageBusMgr.fireEvent('PortletDelete', r.json);
                }
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig       : function() {
                return {
                    common      : {
                        width            : 760,
                        height           : 300,
                        xtype            : 'vizFormFlipCard',
                        messageSaveStart : 'FlipCardChangeStart',
                        messageSaveEnd   : 'FlipCardChange',
                        readonly         : !$authorized('@@@@ FlipCard - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_flipcard_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_flipcard_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyFlipCard'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.FlipCard_FormUpdateBatch
                    }
                };
            },

            /**
             * Destroy
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("FlipCardChange", this.reloadStore, this);
                Viz.grid.FlipCard.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for displaying a map.
             * @method onChartDisplay
             */
            onFlipCardDisplay   : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    if (this.fireEvent("beforeflipcarddisplay", this, record, entity)) {
                        if (entity && entity.KeyFlipCard) {
                            Viz.Services.EnergyWCF.FlipCard_GetItem({
                                        Key     : entity.KeyFlipCard,
                                        success : function(flipcard) {
                                            Viz.grid.FlipCard.openFlipCardDisplay(flipcard);

                                        },
                                        scope   : this
                                    })
                        }
                    }
                }
            }
        });
Ext.reg("vizGridFlipCard", Viz.grid.FlipCard);

// Static Methods
Ext.apply(Viz.grid.FlipCard, {
            /**
             * Displays a WebFrame.
             * @method openWebFrameDisplay
             * @static
             * @param {Viz.BusinessEntity.FlipCard} entity : the FlipCard entity.
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openFlipCardDisplay : function(entity, width, height) {
                var win = new Viz.portal.PortletWindow({
                            entity        : entity,
                            xtypePortlet  : 'vizPortletFlipCard',
                            entityPortlet : {
                                TypeEntity : 'Vizelia.FOL.BusinessEntities.FlipCard'
                            },
                            updateHandler : Viz.grid.FlipCard.update
                        });
                win.show();
            },

            /**
             * Create a new FlipCard (to be called from desktop or tree)
             * @param {} callback
             */
            create              : function(entity) {
                Viz.openFormCrud(Viz.grid.FlipCard.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing FlipCard (to be called from desktop or tree)
             * @param {} callback
             */
            update              : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.FlipCard.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
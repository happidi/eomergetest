﻿/**
 * @class Viz.grid.LoginHistory
 * <p>
 * A grid exposing the business entity LoginHistory
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.LoginHistory = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        autoFill : false
                    },
                    tbar         : [{
                                text    : $lang("msg_delete"),
                                iconCls : 'viz-icon-small-loginhistory-delete',
                                scope   : this,
                                handler : this.onDeleteRecord,
                                hidden  : !$authorized('@@@@ Historical Logins - Full Control')
                            }],
                    store        : new Viz.store.LoginHistory(),
                    columns      : Viz.Configuration.Columns.LoginHistory,
                    plugins      : [new Viz.plugins.GridExport({
                                paging          : {
                                    start : 0,
                                    sort  : 'LoginDate',
                                    dir   : 'DESC'
                                },
                                maxRecords      : 1000,
                                promptMaxRecord : true
                            })]
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.LoginHistory.superclass.constructor.call(this, config);
            }
        });
Ext.reg("vizGridLoginHistory", Viz.grid.LoginHistory);

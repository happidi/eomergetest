﻿/**
 * @class Viz.grid.FlipCardEntry
 * <p>
 * A grid exposing the business entity FlipCardEntry
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FlipCardEntry = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    hasFilter    : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_flipcardentry_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FlipCardEntry_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_flipcardentry_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('FlipCardEntry_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_flipcardentry_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FlipCardEntry_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_otheractions"),
                                iconCls : 'viz-icon-small-action',
                                scope   : this,
                                menu    : new Ext.menu.Menu({
                                            items : [{
                                                        text    : $lang("msg_chart_add"),
                                                        iconCls : 'viz-icon-small-chart-add',
                                                        // hidden : !$authorized('FilterChart_write'),
                                                        scope   : this,
                                                        handler : this.onPortletAddRecord.createDelegate(this, [$lang('msg_chart'), 'viz-icon-small-chart', 'vizGridChart'])
                                                    }, {
                                                        text    : $lang("msg_webframe_add"),
                                                        iconCls : 'viz-icon-small-webframe-add',
                                                        // hidden : !$authorized('FilterChart_write'),
                                                        scope   : this,
                                                        handler : this.onPortletAddRecord.createDelegate(this, [$lang('msg_webframe'), 'viz-icon-small-webframe', 'vizGridWebFrame'])
                                                    }, {
                                                        text    : $lang("msg_weatherlocation_add"),
                                                        iconCls : 'viz-icon-small-weatherlocation-add',
                                                        // hidden : !$authorized('FilterChart_write'),
                                                        scope   : this,
                                                        handler : this.onPortletAddRecord.createDelegate(this, [$lang('msg_weatherlocation'), 'viz-icon-small-weatherlocation', 'vizGridWeatherLocation'])
                                                    }, {
                                                        text    : $lang("msg_map_add"),
                                                        iconCls : 'viz-icon-small-map-add',
                                                        // hidden : !$authorized('FilterChart_write'),
                                                        scope   : this,
                                                        handler : this.onPortletAddRecord.createDelegate(this, [$lang('msg_map'), 'viz-icon-small-map', 'vizGridMap'])
                                                    }, {
                                                        text    : $lang("msg_drawingcanvas_add"),
                                                        iconCls : 'viz-icon-small-drawingcanvas-add',
                                                        // hidden : !$authorized('FilterChart_write'),
                                                        scope   : this,
                                                        handler : this.onPortletAddRecord.createDelegate(this, [$lang('msg_drawingcanvas'), 'viz-icon-small-drawingcanvas', 'vizGridDrawingCanvas'])
                                                    }, {
                                                        text    : $lang("msg_alarmtable_add"),
                                                        iconCls : 'viz-icon-small-alarmtable-add',
                                                        // hidden : !$authorized('FilterChart_write'),
                                                        scope   : this,
                                                        handler : this.onPortletAddRecord.createDelegate(this, [$lang('msg_alarmtable'), 'viz-icon-small-alarmtable', 'vizGridAlarmTable'])
                                                    }]
                                        })
                            }],
                    formConfig   : {
                        common      : {
                            width            : 650,
                            height           : 180,
                            xtype            : 'vizFormFlipCardEntry',
                            messageSaveStart : 'FlipCardEntryChangeStart',
                            messageSaveEnd   : 'FlipCardEntryChange'
                        },
                        create      : {
                            title   : $lang("msg_flipcardentry_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_flipcardentry_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'TitleEntity',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyFlipCardEntry'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.FlipCardEntry_FormUpdateBatch
                        }
                    },
                    store        : config.store || new Viz.store.FlipCardEntry({
                                KeyFlipCard : config.KeyFlipCard
                            }),
                    columns      : Viz.Configuration.Columns.FlipCardEntry
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FlipCardEntry.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("FlipCardEntryChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy               : function() {
                Viz.util.MessageBusMgr.unsubscribe("FlipCardEntryChange", this.reloadStore, this);
                Viz.grid.FlipCardEntry.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortal.
             */
            onBeforeAddRecord       : function(grid, entity) {
                var KeyFlipCard = this.KeyFlipCard;
                if (KeyFlipCard == '' || KeyFlipCard == null) {
                    return false;
                }
                entity.KeyFlipCard = KeyFlipCard;
                entity.Order = this.getMaxValueFromStore('Order') + 1;

                return true;
            },

            onPortletAddRecord      : function(title, iconscls, xtype) {
                this.openGridDetail({
                            windowTitle     : title,
                            windowWidth     : 1000,
                            windowHeight    : 400,
                            windowIconCls   : iconscls,
                            xtypeGridDetail : xtype
                        });
            },

            getEntityFromGridRecord : function(r) {
                var type = Viz.getEntityType(r.json.__type);
                var key = r.data[Viz.util.Portal.getPortletKey(type)];

                var data = {
                    Order       : this.getMaxValueFromStore('Order') + 1,
                    KeyEntity   : key,
                    TypeEntity  : type,
                    TitleEntity : r.data.Title,
                    KeyFlipCard : this.KeyFlipCard
                };
                return data;
            }
        });

Ext.reg("vizGridFlipCardEntry", Viz.grid.FlipCardEntry);
﻿/**
 * @class Viz.grid.DataSerieColorElement
 * <p>
 * A grid exposing the business entity DataSerieColorElement
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.DataSerieColorElement = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_dataseriecolorelement_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('DataSerieColorElement_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_dataseriecolorelement_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('DataSerieColorElement_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_dataseriecolorelement_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('DataSerieColorElement_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 400,
                            height           : 200,
                            xtype            : 'vizFormDataSerieColorElement',
                            messageSaveStart : 'DataSerieColorElementChangeStart',
                            messageSaveEnd   : 'DataSerieColorElementChange'
                        },
                        create      : {
                            title   : $lang("msg_dataseriecolorelement_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_dataseriecolorelement_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'Color',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyDataSerieColorElement'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.DataSerieColorElement_FormUpdateBatch
                        }
                    },
                    store        : config.store || new Viz.store.DataSerieColorElement({
                                KeyDataSerie : config.KeyDataSerie
                            }),
                    columns      : Viz.Configuration.Columns.DataSerieColorElement
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.DataSerieColorElement.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("DataSerieColorElementChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("DataSerieColorElementChange", this.reloadStore, this);
                Viz.grid.DataSerieColorElement.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyDataSerie.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyDataSerie = this.KeyDataSerie;
                if (KeyDataSerie == '' || KeyDataSerie == null) {
                    return false;
                }
                entity.KeyDataSerie = KeyDataSerie;
                return true;
            }

        });

Ext.reg("vizGridDataSerieColorElement", Viz.grid.DataSerieColorElement);
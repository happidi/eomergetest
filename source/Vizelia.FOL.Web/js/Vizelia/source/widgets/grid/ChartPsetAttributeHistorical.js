﻿/**
 * @class Viz.grid.ChartPsetAttributeHistorical
 * <p>
 * A grid exposing the business entity ChartPsetAttributeHistorical
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.ChartPsetAttributeHistorical = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_chartpsetattributehistorical_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('ChartPsetAttributeHistorical_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_chartpsetattributehistorical_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('ChartPsetAttributeHistorical_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_chartpsetattributehistorical_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('ChartPsetAttributeHistorical_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 400,
                            height           : 250,
                            xtype            : 'vizFormChartPsetAttributeHistorical',
                            messageSaveStart : 'ChartPsetAttributeHistoricalChangeStart',
                            messageSaveEnd   : 'ChartPsetAttributeHistoricalChange'
                        },
                        create      : {
                            title   : $lang("msg_chartpsetattributehistorical_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_chartpsetattributehistorical_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyChartPsetAttributeHistorical'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.ChartPsetAttributeHistorical_FormUpdateBatch
                        }
                    },
                    store        : config.store || new Viz.store.ChartPsetAttributeHistorical({
                                KeyChart : config.KeyChart
                            }),
                    columns      : Viz.Configuration.Columns.ChartPsetAttributeHistorical
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.ChartPsetAttributeHistorical.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("ChartPsetAttributeHistoricalChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("ChartPsetAttributeHistoricalChange", this.reloadStore, this);
                Viz.grid.ChartPsetAttributeHistorical.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyChart.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyChart = this.KeyChart;
                if (KeyChart == '' || KeyChart == null) {
                    return false;
                }
                entity.KeyChart = KeyChart;
                return true;
            }

        });

Ext.reg("vizGridChartPsetAttributeHistorical", Viz.grid.ChartPsetAttributeHistorical);
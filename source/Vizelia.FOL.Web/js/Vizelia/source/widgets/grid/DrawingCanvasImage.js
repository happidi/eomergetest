﻿/**
 * @class Viz.grid.DrawingCanvasImage
 * <p>
 * A grid exposing the business entity DrawingCanvasImage
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.DrawingCanvasImage = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_drawingcanvasimage_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('DrawingCanvasImage_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_drawingcanvasimage_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('DrawingCanvasImage_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_drawingcanvasimage_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('DrawingCanvasImage_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : config.store || new Viz.store.DrawingCanvasImage({
                                KeyPortalWindow : config.KeyPortalWindow
                            }),
                    columns      : Viz.Configuration.Columns.DrawingCanvasImage
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.DrawingCanvasImage.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("DrawingCanvasImageChange", this.reloadStore, this);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig     : function() {
                return {
                    common      : {
                        width            : 550,
                        height           : 300,
                        xtype            : 'vizFormDrawingCanvasImage',
                        messageSaveStart : 'DrawingCanvasImageChangeStart',
                        messageSaveEnd   : 'DrawingCanvasImageChange'
                    },
                    create      : {
                        title   : $lang("msg_drawingcanvasimage_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_drawingcanvasimage_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'ZIndex',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyDrawingCanvasImage'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.DrawingCanvasImage_FormUpdateBatch
                    }
                };
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("DrawingCanvasImageChange", this.reloadStore, this);
                Viz.grid.DrawingCanvasImage.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortalWindow.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyDrawingCanvas = this.KeyDrawingCanvas;
                if (KeyDrawingCanvas == '' || KeyDrawingCanvas == null) {
                    return false;
                }
                entity.KeyDrawingCanvas = KeyDrawingCanvas;
                return true;
            }

        });

Ext.reg("vizGridDrawingCanvasImage", Viz.grid.DrawingCanvasImage);

// Static Methods
Ext.apply(Viz.grid.DrawingCanvasImage, {
            /**
             * Create a new DrawingCanvasImage (to be called from desktop or tree)
             * @param {} callback
             */
            create : function(entity) {
                Viz.openFormCrud(Viz.grid.DrawingCanvasImage.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing DrawingCanvasImage (to be called from desktop or tree)
             * @param {} callback
             */
            update : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.DrawingCanvasImage.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
﻿/**
 * @class Viz.grid.FilterChartScheduler
 * <p>
 * A grid exposing the business entity FilterChartScheduler
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterChartScheduler = Ext.extend(Viz.grid.GridPanel, {

            /**
             * @param {Boolean} isSecurableEntity <t>true</t> if the store is a SecurableObject store, <t>false</t> (default) otherwise
             * @type Boolean
             */
            isSecurableEntity       : false,
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang("msg_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterChartScheduler_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterChartScheduler_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],

                    columns      : config.isSecurableEntity ? Viz.Configuration.Columns.SecurableEntity : Viz.Configuration.Columns.ChartScheduler

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterChartScheduler.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("FilterChartSchedulerChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy               : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterChartSchedulerChange", this.reloadStore, this);
                Viz.grid.FilterChartScheduler.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord             : function() {
                this.openGridDetail({
                            windowTitle     : $lang('msg_chartscheduler'),
                            windowWidth     : 1000,
                            windowHeight    : 400,
                            windowIconCls   : 'viz-icon-small-chartscheduler',
                            xtypeGridDetail : 'vizGridChartScheduler'
                        });
            },
            /**
             * Return the data to add to the store from the detail grid selected record.
             */
            getEntityFromGridRecord : function(r) {
                if (this.isSecurableEntity) {
                    var data = {
                        KeySecurable      : r.data.KeyChartScheduler,
                        SecurableName     : r.data.Title,
                        SecurableTypeName : 'Vizelia.FOL.BusinessEntities.ChartScheduler',
                        SecurableIconCls  : 'viz-icon-small-chartscheduler'
                    };
                    return data;
                }
                else {
                    return Viz.grid.FilterChartScheduler.superclass.getEntityFromGridRecord.apply(this, arguments);
                }
            }
        });

Ext.reg("vizGridFilterChartScheduler", Viz.grid.FilterChartScheduler);
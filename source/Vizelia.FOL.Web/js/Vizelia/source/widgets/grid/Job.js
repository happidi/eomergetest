﻿/**
 * @class Viz.grid.Job
 * <p>
 * A grid exposing the business entity Job
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Job = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor  : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    // cls : 'viz-grid-norowborder',
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_job_add"),
                                iconCls : 'viz-icon-small-job-add',
                                hidden  : !$authorized('@@@@ Job management - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_job_update"),
                                iconCls : 'viz-icon-small-job-update',
                                hidden  : !$authorized('@@@@ Job management - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_job_delete"),
                                iconCls : 'viz-icon-small-job-delete',
                                hidden  : !$authorized('@@@@ Job management - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_job_execute"),
                                iconCls : 'viz-icon-small-job',
                                hidden  : !$authorized('@@@@ Job management - Full Control'),
                                scope   : this,
                                handler : this.onExecuteJob
                            }, {
                                text    : $lang("msg_job_pause"),
                                iconCls : 'viz-icon-small-job-pause',
                                hidden  : !$authorized('@@@@ Job management - Full Control'),
                                scope   : this,
                                handler : this.onPauseJob
                            }, {
                                text    : $lang("msg_job_resume"),
                                iconCls : 'viz-icon-small-job-resume',
                                hidden  : !$authorized('@@@@ Job management - Full Control'),
                                scope   : this,
                                handler : this.onResumeJob
                            }, {
                                text    : $lang("msg_job_history"),
                                iconCls : 'viz-icon-small-trace',
                                hidden  : !$authorized('@@@@ Job management - Full Control'),
                                scope   : this,
                                handler : this.onHistory
                            }],
                    formConfig   : {
                        common      : {
                            width            : 800,
                            height           : 430,
                            xtype            : 'vizFormJob',
                            messageSaveStart : 'JobChangeStart',
                            messageSaveEnd   : 'JobChange',
                            readonly         : !$authorized('@@@@ Job management - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_job_add"),
                            iconCls : 'viz-icon-small-job-add'
                        },
                        update      : {
                            title               : $lang("msg_job_update"),
                            iconCls             : 'viz-icon-small-job-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyJob'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-job-update',
                            serviceHandlerUpdateBatch : Viz.Services.CoreWCF.Job_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.Job(),
                    columns      : Viz.Configuration.Columns.Job
                };
                var forcedConfig = {
                    createTooltip : function() {
                        var view = this.getView();
                        this.tip = new Ext.ToolTip({
                                    target     : view.mainBody,
                                    delegate   : '.x-grid3-cell',
                                    trackMouse : false,
                                    renderTo   : document.body,
                                    listeners  : {
                                        beforeshow : function updateTipBody(tip) {
                                            try {
                                                var rowIndex = view.findRowIndex(tip.triggerElement);
                                                var cellIndex = view.findCellIndex(tip.triggerElement);
                                                if (rowIndex >= 0 && cellIndex >= 0 && rowIndex < this.store.data.length) {
                                                    var cell = Ext.fly(view.getCell(rowIndex, cellIndex));
                                                    if (this.cellHasTooltip(cell))
                                                        return false;
                                                    // we calculate if the text is hidden. Special case for column with an icon.
                                                    var textWidth = cell.getTextWidth() + (cell.hasClass('viz-icon-cell') ? 22 : 2);
                                                    if (this.getColumnModel().getDataIndex(cellIndex) === 'LastRunTime') {
                                                        var lastRunSeverity = this.store.getAt(rowIndex).get('LastRunSeverity');
                                                        var lastRunMessage = this.store.getAt(rowIndex).get('LastRunErrorMessage');
                                                        if (lastRunSeverity > 0 && lastRunMessage !== '') {
                                                            tip.setInfo(lastRunMessage);
                                                        }
                                                        else {
                                                            return false;
                                                        }
                                                    }
                                                    else if (textWidth > cell.getWidth()) {
                                                        // the text of the cell could be altered by a renderer so we cannot rely on columnModel

                                                        // code for tooltip
                                                        tip.setInfo(cell.dom.textContent || cell.dom.innerText);
                                                    }
                                                    else
                                                        return false;
                                                    return;
                                                }
                                                else
                                                    return false;
                                            }
                                            catch (ex) {
                                                return false;
                                            }
                                        },
                                        scope      : this
                                    }
                                });
                    }
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.Job.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("JobChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy    : function() {
                Viz.util.MessageBusMgr.unsubscribe("JobChange", this.reloadStore, this);
                Viz.grid.Job.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Generate item button.
             */
            onExecuteJob : function() {

                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                if (selections.length > 1) {
                    Ext.MessageBox.alert($lang('msg_job_execute'), $lang('msg_job_single_execute'));
                    return;
                }

                Viz.Services.CoreWCF.Job_ExecuteItem({
                            Key     : selections[0].json.KeyJob,
                            success : function() {
                                Ext.MessageBox.alert($lang('msg_job_execute'), $lang('msg_job_execute_success'));
                            }
                        });

            },

            /**
             * Handler for the Generate item button.
             */
            onPauseJob   : function() {

                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                if (selections.length > 1) {
                    Ext.MessageBox.alert($lang('msg_job_pause'), $lang('msg_job_single_pause'));
                    return;
                }

                Viz.Services.CoreWCF.Job_PauseItem({
                            Key     : selections[0].json.KeyJob,
                            success : function() {
                                this.reloadStore();
                            },
                            scope   : this
                        });

            },

            /**
             * Handler for the Generate item button.
             */
            onResumeJob  : function() {

                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                if (selections.length > 1) {
                    Ext.MessageBox.alert($lang('msg_job_resume'), $lang('msg_job_single_resume'));
                    return;
                }

                Viz.Services.CoreWCF.Job_ResumeItem({
                            Key     : selections[0].json.KeyJob,
                            success : function() {
                                this.reloadStore();
                            },
                            scope   : this
                        });

            },

            onHistory    : function() {
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;

                    // Copy the columns of the TraceEntry grid and make the RequestId column hidden.
                    /*
                     * var newColumnsArray = []; Ext.each(Viz.Configuration.Columns.TraceEntry, function (item) { var copiedColumn = Ext.apply({}, item); if (copiedColumn.dataIndex === 'RequestId') { copiedColumn.hidden = true; } newColumnsArray.push(copiedColumn); }, this);
                     */

                    var numberFieldId = Ext.id();
                    var gridId = Ext.id();
                    var win = new Ext.Window({
                                title       : $lang('msg_jobinstance'),
                                width       : 1000,
                                maximizable : true,
                                height      : 400,
                                layout      : 'fit',
                                iconCls     : 'viz-icon-small-trace',
                                items       : [{
                                            xtype   : 'vizGridJobInstance',
                                            id      : gridId,
                                            KeyJob: entity.KeyJob + '.' + entity.Name,
                                            /*
                                             * tbar: [{ xtype: 'numberfield', id: numberFieldId, text: $lang("msg_delete"), iconCls: 'viz-icon-small-trace-delete', enableKeyEvents: true, listeners: { keyup: function (obj, e) { //console.log(obj.getValue()); Ext.getCmp(gridId).store.serviceParams.jobRunInstanceNumber = obj.getValue(); Ext.getCmp(gridId).store.reload(); //Ext.getCmp(gridId).view.refresh(); } } }, { xtype: 'button', text: 'refresh', handler: function () { Ext.getCmp(gridId).store.serviceParams.jobRunInstanceNumber = Ext.getCmp(numberFieldId).getValue(); Ext.getCmp(gridId).store.reload(); Ext.getCmp(gridId).view.refresh(); } } ],
                                             */
                                            stateId : 'vizGridJobHistory.vizGridJobInstance'/*
                                                                                             * , columns: newColumnsArray, plugins: [new Viz.plugins.GridExport({ paging: { start: 0, sort: 'Timestamp', dir: 'ASC' }, maxRecords: 1000, promptMaxRecord: true })], store: new Viz.data.WCFJsonSimpleStore({ serviceHandler: Viz.Services.CoreWCF.TraceEntry_GetStoreByJobRunInstanceRequestId, serviceParams: { location: Vizelia.FOL.BusinessEntities.PagingLocation.Database, jobRunInstanceRequestId: entity.JobRunInstanceRequestId, jobRunInstanceNumber: 1 }, sortInfo: { field: 'Timestamp', direction: 'ASC' } })
                                                                                             */
                                        }]
                            });
                    win.show();
                }

            }
        });
Ext.reg("vizGridJob", Viz.grid.Job);
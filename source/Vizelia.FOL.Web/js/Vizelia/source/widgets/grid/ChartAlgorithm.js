﻿/**
 * @class Viz.grid.ChartAlgorithm
 * <p>
 * A grid exposing the business entity ChartAlgorithm
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.ChartAlgorithm = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_algorithm_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('ChartScriptPython_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_algorithm_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('ChartScriptPython_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_algorithm_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('ChartScriptPython_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : config.store || new Viz.store.ChartAlgorithm({
                                KeyChart : config.KeyChart
                            }),
                    columns      : Viz.Configuration.Columns.ChartAlgorithm
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.ChartAlgorithm.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("ChartAlgorithmChange", this.reloadStore, this);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig     : function() {
                return {
                    common      : {
                        width            : 600,
                        height           : 500,
                        xtype            : 'vizFormChartAlgorithm',
                        messageSaveStart : 'ChartAlgorithmChangeStart',
                        messageSaveEnd   : 'ChartAlgorithmChange',
                        readonly         : !$authorized('@@@@ Chart - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_algorithm_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_algorithm_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'AlgorithmName',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyChartAlgorithm'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.ChartAlgorithm_FormUpdateBatch
                    }
                };
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("ChartAlgorithmChange", this.reloadStore, this);
                Viz.grid.ChartAlgorithm.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortalWindow.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyChart = this.KeyChart;
                if (KeyChart == '' || KeyChart == null) {
                    return false;
                }
                entity.KeyChart = KeyChart;
                entity.Order = this.getMaxValueFromStore('Order') + 1;
                return true;
            },

            /**
             * Handler for delete event.
             * @param {} selections
             */
            onDeleteRecord    : function(selections) {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;

                var msgDelete;
                if (selections.length > 1)
                    msgDelete = String.format($lang("msg_record_delete_confirmation_multiple"), selections.length);
                else
                    msgDelete = $lang("msg_record_delete_confirmation_single");

                Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                            if (button == 'yes') {
                                for (var i = 0, r; r = selections[i]; i++) {
                                    this.store.remove(r);
                                }
                                this.store.save();
                                Viz.Services.EnergyWCF.ChartAlgorithm_SaveStore({
                                            store   : this.store.crudStore,
                                            success : function(result) {
                                                // we sending the incremental change. 
                                                Viz.util.MessageBusMgr.publish('ChartAlgorithmCountChange', this.KeyChart, (-1) * result.records.length);
                                                if (Ext.isArray(selections)) {
                                                    for (var i = 0, r; r = selections[i]; i++) {
                                                        Viz.util.MessageBusMgr.fireEvent('ChartAlgorithmChange', r.data);
                                                    }
                                                }
                                                else {
                                                    Viz.util.MessageBusMgr.fireEvent('ChartAlgorithmChange', selections);
                                                }
                                            },
                                            scope   : this
                                        });
                            }
                        }, this);

            }

        });

Ext.reg("vizGridChartAlgorithm", Viz.grid.ChartAlgorithm);

// Static Methods
Ext.apply(Viz.grid.ChartAlgorithm, {

            /**
             * Create a new Chart (to be called from desktop or tree)
             * @param {} callback
             */
            create : function(entity) {
                Viz.openFormCrud(Viz.grid.ChartAlgorithm.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing Chart (to be called from desktop or tree) *
             * @param {Chart} entity
             * @param {Ext.Window} window
             * @param {object} extraConfig
             */
            update : function(entity, window, extraConfig) {
                if (entity) {
                    var conf = Viz.grid.ChartAlgorithm.prototype.getFormConfig();
                    if (Ext.isObject(extraConfig))
                        Ext.apply(conf.common, extraConfig);
                    if (window) {
                        var box = window.getBox();
                        conf.common.x = box.width + box.x;
                        conf.common.y = box.y;
                    }
                    Viz.openFormCrud(conf, 'update', entity);
                }
            }
        });

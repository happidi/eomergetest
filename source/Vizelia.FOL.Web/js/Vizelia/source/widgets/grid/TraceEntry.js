﻿/**
 * @class Viz.grid.TraceEntry
 * <p>
 * A grid exposing the business entity TraceEntry
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.TraceEntry = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor     : false,
                    stripeRows       : false,

                    // cls : 'viz-grid-norowborder',
                    store            : new Viz.store.TraceEntry({
                                storeFilters : this.storeFilters || config.storeFilters
                            }),
                    autoExpandColumn : 'messageColumn',
                    autoExpandMax    : 3000,
                    viewConfig       : {
                        forceFit  : false,
                        emptyText : $lang('msg_trace_no_entries')
                    },
                    columns          : Viz.Configuration.Columns.TraceEntry,
                    hasToolbar       : false,
                    tbar             : [{
                                text    : $lang("msg_delete"),
                                iconCls : 'viz-icon-small-trace-delete',
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    plugins          : [new Viz.plugins.GridExport({
                                paging          : {
                                    start : 0,
                                    sort  : 'Timestamp',
                                    dir   : 'DESC'

                                },
                                maxRecords      : 1000,
                                promptMaxRecord : true
                            })]
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.TraceEntry.superclass.constructor.call(this, config);
                
                this.on('celldblclick', function(grid, rowIndex, cellIndex, e) {
                    var store = grid.getStore().getAt(rowIndex);
                    var columnName = grid.getColumnModel().getDataIndex(cellIndex);
                    if (columnName === null || columnName === '')
                        return;
                    var cellValue = store.get(columnName);
                    if (columnName === 'Severity') {
                        switch (cellValue) {
                            case 0 :
                                cellValue = 'Information';
                                break;
                            case 1 :
                                cellValue = 'Warning';
                                break;
                            case 2 :
                                cellValue = 'Error';
                                break;
                            case 3 :
                                cellValue = 'Summary';
                                break;
                            default :
                                break;
                        }
                    }

                    var win = new Ext.Window({
                                title   : columnName,
                                width   : 400,
                                height  : 200,
                                layout  : 'fit',
                                iconCls : 'viz-icon-small-trace',
                                items   : [{
                                            xtype         : 'textarea',
                                            value         : cellValue,
                                            selectOnFocus : true
                                        }]
                            });
                    win.show();

                        // Ext.MessageBox.alert(columnName, cellValue);

                    });
            }
        });

Ext.reg("vizGridTraceEntry", Viz.grid.TraceEntry);
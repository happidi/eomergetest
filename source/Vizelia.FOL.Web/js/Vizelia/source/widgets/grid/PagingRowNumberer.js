﻿Ext.namespace('Viz.grid');

/**
 * @class Viz.grid.PagingRowNumberer This is a utility class that can be passed into a {@link Ext.grid.ColumnModel} as a column config that provides an automatic row numbering column when using paging. <br>
 * Usage:<br>
 * 
 * <pre><code>
 * // This is a typical column config with the first column providing row numbers
 * var colModel = new Ext.grid.ColumnModel([new Viz.grid.PagingRowNumberer(), {
 *             header   : &quot;Name&quot;,
 *             width    : 80,
 *             sortable : true
 *         }, {
 *             header   : &quot;Code&quot;,
 *             width    : 50,
 *             sortable : true
 *         }, {
 *             header   : &quot;Description&quot;,
 *             width    : 200,
 *             sortable : true
 *         }]);
 * </code></pre>
 * 
 * @constructor
 * @param {Object} config The configuration options
 */

Viz.grid.PagingRowNumberer = Ext.extend(Ext.grid.RowNumberer, {
            getEditor : Ext.emptyFn,
            width     : 46,
            renderer  : function(v, p, record, rowIndex, colIndex, store) {
                if (this.rowspan) {
                    p.cellAttr = 'rowspan="' + this.rowspan + '"';
                }
                var so = store.lastOptions;
                var sop = so ? so.params : null;

                return Ext.util.Format.Number(((sop && sop.start) ? sop.start : 0) + rowIndex + 1);
            }
        });

try {
    // specific to version "3.0"
    Ext.apply(Ext.grid.Column.types, {
                "viz-grid-pagingrowumberer" : Viz.grid.PagingRowNumberer,
                "checkcolumn"               : Ext.ux.grid.CheckColumn
            });
}
catch (e) {
    ;
}

Ext.reg('viz-grid-pagingrowumberer', Viz.grid.PagingRowNumberer);
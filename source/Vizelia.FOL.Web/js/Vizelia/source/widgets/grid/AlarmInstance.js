﻿/**
 * @class Viz.grid.AlarmInstance
 * <p>
 * A grid exposing the business entity AlarmInstance
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.AlarmInstance = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor      : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_alarminstance_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden  : !$authorized('@@@@ Alarm Table - Full Control') && !$authorized('Alarm - Read Write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_alarminstance_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ Alarm Table - Full Control') && !$authorized('Alarm - Read Write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_alarminstance_deleteall"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : (!$authorized('@@@@ Alarm Table - Full Control') && !$authorized('Alarm - Read Write')) || config.hideDeleteAll,
                                scope   : this,
                                handler : this.onDeleteAll
                            }, {
                                text    : $lang("msg_chart_view"),
                                iconCls : 'viz-icon-small-display',
                                hidden  : config.hideChartViewButton || !$authorized('@@@@ Alarm Table - Full Control') && !$authorized('Alarm - Read Write'),
                                scope   : this,
                                handler : this.onChartDisplay
                            }],
                    formConfig   : {
                        common      : {
                            width            : 350,
                            height           : 200,
                            xtype            : 'vizFormAlarmInstance',
                            messageSaveStart : 'AlarmInstanceChangeStart',
                            messageSaveEnd   : 'AlarmInstanceChange',
                            readonly         : !$authorized('@@@@ Alarm Table - Full Control') && !$authorized('Alarm - Read Write')
                        },
                        create      : {
                            title   : $lang("msg_alarminstance_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_alarminstance_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'KeyAlarmInstance',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyAlarmInstance'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.AlarmInstance_FormUpdateBatch
                        }
                    },
                    store        : config.store ? config.store : new Viz.store.AlarmInstance(),
                    plugins: [new Viz.plugins.GridExport({
                        paging: {
                            start: 0,
                            sort: 'InstanceDateTime',
                            dir: 'ASC'
                        },
                        maxRecords: 1000,
                        promptMaxRecord: true
                    })],
                    columns: Viz.Configuration.Columns.AlarmInstance
                };
                var forcedConfig = {};

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.AlarmInstance.superclass.constructor.call(this, config);

                this.on({
                            rowcontextmenu : {
                                scope : this,
                                fn    : this.onRowContextMenu
                            }
                        })

                Viz.util.MessageBusMgr.subscribe("AlarmInstanceChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy        : function() {
                Viz.util.MessageBusMgr.unsubscribe("AlarmInstanceChange", this.reloadStore, this);
                Viz.grid.AlarmInstance.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * RowContextMenu listener.
             * @param {} grid
             * @param {} row
             * @param {} event
             */
            onRowContextMenu : function(grid, row, event) {
                event.stopEvent();
                var xy = event.getXY();
                this.getSelectionModel().selectRow(row, false);
                if (this.hideMenu == true)
                    return;

                var entity = this.store.getAt(row).json;
                if (entity && entity.MeterValidationRule) {

                    var ctxMenu = new Ext.menu.Menu({
                                defaults : {
                                    hideOnClick : true
                                },
                                items    : [{
                                            text    : $lang("msg_metervalidationrule_viewdefinition"),
                                            iconCls : 'viz-icon-small-display',
                                            handler : function() {
                                                var config = Viz.grid.MeterValidationRule.prototype.getFormConfig();
                                                config.common.readonly = true;
                                                Viz.openFormCrud(config, 'update', entity.MeterValidationRule);
                                            },
                                            scope   : this,
                                            hidden  : !$authorized("@@@@ Meters - Full Control")
                                        }]
                            });
                    ctxMenu.on("hide", function(menu) {
                                menu.destroy();
                            });
                    ctxMenu.showAt(event.getXY());
                }
            },

            /**
             * Handler for displaying a chart.
             * @method onChartDisplay
             */
            onChartDisplay   : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    if (!Ext.isEmpty(record.json.KeyChart)) {
                        Viz.grid.Chart.openChartDisplay(record.json.KeyChart, null, null, true);
                    }
                    else {
                        Ext.MessageBox.show({
                                    title    : $lang("msg_chart_view"),
                                    msg      : $lang("msg_alarminstance_nochartassociated"),
                                    buttons  : Ext.MessageBox.OK,
                                    icon     : Ext.MessageBox.INFO,
                                    minWidth : 200
                                });
                    }
                }
            },

            onDeleteAll      : function() {
                var msgDelete = $lang("msg_alarminstance_deleteall_confirmation");
                Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                            if (button == 'yes') {

                                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                            pollingInterval        : 1000,
                                            serviceStart           : Viz.Services.EnergyWCF.AlarmInstance_BeginDeleteAll,
                                            serviceParams          : {
                                                KeyAlarmDefinition     : this.store.serviceParams.KeyAlarmDefinition,
                                                KeyMeterValidationRule : this.store.serviceParams.KeyMeterValidationRule
                                            },
                                            showProgress           : true,
                                            showProgressPercentage : true,
                                            modal                  : true,
                                            progressWindowTitle    : $lang('msg_alarminstance_delete'),
                                            completionHandler      : function() {
                                                this.reloadStore();
                                            },
                                            scope                  : this
                                        });
                                submitter.start();
                            }
                        }, this);

            }
        });
Ext.reg("vizGridAlarmInstance", Viz.grid.AlarmInstance);

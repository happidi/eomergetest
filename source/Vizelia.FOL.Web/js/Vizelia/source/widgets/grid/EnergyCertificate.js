﻿/**
 * @class Viz.grid.EnergyCertificate
 * <p>
 * A grid exposing the business entity EnergyCertificate
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.EnergyCertificate = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_energycertificate_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden  : !$authorized('@@@@ Energy certificate - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_energycertificate_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden  : !$authorized('@@@@ Energy certificate - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_energycertificate_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ Energy certificate - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            },{
                                text    : $lang("msg_copy"),
                                iconCls : 'viz-icon-small-copy',
                                hidden  : !$authorized('@@@@ Energy certificate - Full Control'),
                                scope   : this,
                                handler : this.onCopyRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 700,
                            height           : 300,
                            xtype            : 'vizFormEnergyCertificate',
                            messageSaveStart : 'EnergyCertificateChangeStart',
                            messageSaveEnd   : 'EnergyCertificateChange',
                            readonly         : !$authorized('@@@@ Energy certificate - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_energycertificate_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_energycertificate_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyEnergyCertificate'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.EnergyCertificate_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.EnergyCertificate(),
                    columns      : Viz.Configuration.Columns.EnergyCertificate
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.EnergyCertificate.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("EnergyCertificateChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("EnergyCertificateChange", this.reloadStore, this);
                Viz.grid.EnergyCertificate.superclass.onDestroy.apply(this, arguments);
            },
            
            /**
             * @private Handler for copying a chart.
             */
            onCopyRecord   : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;

                var keys = new Array();
                for (var i = 0, r; r = selections[i]; i++) {
                    keys.push(r.data.KeyEnergyCertificate);
                }
                this.el.mask();
                Viz.Services.EnergyWCF.EnergyCertificate_Copy({
                            keys    : keys,
                            success : function(data) {
                                this.el.unmask();
                                this.reloadStore();
                            },
                            scope   : this
                        });
            }
        });
Ext.reg("vizGridEnergyCertificate", Viz.grid.EnergyCertificate);
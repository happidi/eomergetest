﻿/**
 * @class Viz.grid.StruxureWareDataAcquisitionContainer
 * <p>
 * A grid exposing the business entity StruxureWareDataAcquisitionContainer
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.StruxureWareDataAcquisitionContainer = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor   : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_struxurewaredataacquisitioncontainer_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden: !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_struxurewaredataacquisitioncontainer_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden: !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_struxurewaredataacquisitioncontainer_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden: !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : new Viz.store.StruxureWareDataAcquisitionContainer(),
                    columns      : Viz.Configuration.Columns.StruxureWareDataAcquisitionContainer
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.StruxureWareDataAcquisitionContainer.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("StruxureWareDataAcquisitionContainerChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy     : function() {
                Viz.util.MessageBusMgr.unsubscribe("StruxureWareDataAcquisitionContainerChange", this.reloadStore, this);
                Viz.grid.StruxureWareDataAcquisitionContainer.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig : function() {
                return {
                    common      : {
                        width            : 600,
                        height           : 400,
                        xtype            : 'vizFormStruxureWareDataAcquisitionContainer',
                        messageSaveStart : 'StruxureWareDataAcquisitionContainerChangeStart',
                        messageSaveEnd: 'StruxureWareDataAcquisitionContainerChange',
                        readonly: !$authorized('@@@@ Data Acquistion - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_struxurewaredataacquisitioncontainer_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_struxurewaredataacquisitioncontainer_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyDataAcquisitionContainer'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.StruxureWareDataAcquisitionContainer_FormUpdateBatch
                    }
                };
            }
        });
Ext.reg("vizGridStruxureWareDataAcquisitionContainer", Viz.grid.StruxureWareDataAcquisitionContainer);

// Static Methods
Ext.apply(Viz.grid.StruxureWareDataAcquisitionContainer, {

            /**
             * Create a new struxurewaredataacquisitioncontainer (to be called from desktop or tree)
             * @param {} callback
             */
            create : function() {
                Viz.openFormCrud(Viz.grid.StruxureWareDataAcquisitionContainer.prototype.getFormConfig(), 'create', {});
            },

            /**
             * Update an existing struxurewaredataacquisitioncontainer (to be called from desktop or tree)
             * @param {} callback
             */
            update : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.StruxureWareDataAcquisitionContainer.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
﻿/**
 * @class Viz.grid.DataSerie
 * <p>
 * A grid exposing the business entity DataSerie
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.DataSerie = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor                     : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_dataserie_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('DataSerie_write'),
                                scope   : this,
                                handler : this.onAddNoClassificationItemRecord
                            }, {
                                text    : $lang("msg_dataserie_classification_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('DataSerie_write'),
                                scope   : this,
                                handler : this.onAddClassificationItemRecord
                            }, {
                                text    : $lang("msg_dataserie_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('DataSerie_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_dataserie_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('DataSerie_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : config.store || new Viz.store.DataSerie({
                                KeyChart : config.KeyChart
                            }),
                    columns      : Viz.Configuration.Columns.DataSerie
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.DataSerie.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("DataSerieChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy                       : function() {
                Viz.util.MessageBusMgr.unsubscribe("DataSerieChange", this.reloadStore, this);
                Viz.grid.DataSerie.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig                   : function() {
                return {
                    common      : {
                        width              : 600,
                        height             : 580,
                        xtype              : 'vizFormDataSerie',
                        messageSaveStart   : 'DataSerieChangeStart',
                        messageSaveEnd     : 'DataSerieChange',
                        ClassificationItem : false
                    },
                    create      : {
                        title   : $lang("msg_dataserie_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_dataserie_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'Name',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyDataSerie'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.DataSerie_FormUpdateBatch
                    }
                };
            },
            /**
             * @private Handler for updating the entity with the parent KeyChart.
             */
            onBeforeAddRecord               : function(grid, entity) {
                var KeyChart = this.KeyChart;
                if (KeyChart == '' || KeyChart == null) {
                    return false;
                }
                entity.KeyChart = KeyChart;
                return true;
            },

            onAddNoClassificationItemRecord : function() {
                var entity = {
                    IsClassificationItem : false
                };
                if (this.fireEvent("beforeaddrecord", this, entity)) {
                    this.openFormCrud(this.formConfig, 'create', entity);
                }
            },

            onAddClassificationItemRecord   : function() {
                var entity = {
                    IsClassificationItem : true
                };
                if (this.fireEvent("beforeaddrecord", this, entity)) {
                    this.openFormCrud(this.formConfig, 'create', entity);
                }
            }

        });

Ext.reg("vizGridDataSerie", Viz.grid.DataSerie);

// Static Methods
Ext.apply(Viz.grid.DataSerie, {
            /**
             * Create a new DataSerie (to be called from desktop or tree)
             * @param {} callback
             */
            create : function(entity) {
                Viz.openFormCrud(Viz.grid.DataSerie.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing DataSerie (to be called from desktop or tree)
             * @param {} callback
             */
            update : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.DataSerie.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
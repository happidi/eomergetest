﻿/**
 * @class Viz.grid.Step
 * <p>
 * A grid exposing the business entity Step
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Step = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang("msg_step_add"),
                                iconCls : 'viz-icon-small-step-add',
                                // hidden : !$authorized('Step_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_step_update"),
                                iconCls : 'viz-icon-small-step-update',
                                // hidden : !$authorized('Step_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_step_delete"),
                                iconCls : 'viz-icon-small-step-delete',
                                // hidden : !$authorized('Step_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang('msg_step_logging'),
                                iconCls : 'viz-icon-small-logging',
                                handler : this.openStepLogging,
                                scope   : this
                            }],

                    formModal    : false,
                    columns      : Viz.Configuration.Columns.Step
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.Step.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("StepChange", this.reloadStore, this);

            },
            /**
             * Destroy
             * @method onDestroy
             * @private
             */
            onDestroy               : function() {
                Viz.util.MessageBusMgr.unsubscribe("StepChange", this.reloadStore, this);
                Viz.grid.Step.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Opens the logging for a specific step.
             */
            openStepLogging         : function() {
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    var loggingGridId = Ext.id();

                    var win = new Ext.Window({
                                title       : $lang('msg_logging'),
                                width       : 800,
                                height      : 500,
                                maximizable : true,
                                layout      : 'fit',
                                iconCls     : 'viz-icon-small-logging',
                                items       : [{
                                            xtype              : 'vizGrid',
                                            id                 : loggingGridId,
                                            stateId            : 'vizGridStepLogging',
                                            autoReloadInterval : 30,
                                            store              : new Viz.data.WCFJsonSimpleStore({
                                                        serviceHandler : Viz.Services.CoreWCF.Logging_GetStoreByMethodName,
                                                        serviceHandlerCrud : Viz.Services.CoreWCF.Logging_SaveStore,
                                                        serviceParams  : {
                                                            location : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                                                            taskType : entity.KeyStep
                                                        },
                                                        sortInfo       : {
                                                            field     : 'Timestamp',
                                                            direction : 'DESC'
                                                        }
                                                    }),
                                            tbar               : [{
                                                        text    : $lang('msg_logging_view_trace'),
                                                        iconCls : 'viz-icon-small-trace',
                                                        scope   : this,
                                                        handler : function() {
                                                            var loggingGridScope = Ext.getCmp(loggingGridId);
                                                            this.openStepInstanceTracing.apply(loggingGridScope);
                                                        }
                                                    }, {
                                                        text    : $lang("msg_steplogging_delete"),
                                                        iconCls : 'viz-icon-small-delete',
                                                        hidden  : !$authorized('@@@@ Data Acquistion - Full Control'),
                                                        scope   : this,
                                                        handler : function(button, event) {
                                                            var loggingGrid = Ext.getCmp(loggingGridId);
                                                            loggingGrid.onDeleteRecord.apply(loggingGrid, arguments);
                                                        }

                                                    }],
                                            columns            : Viz.Configuration.Columns.Logging,
                                            plugins            : [new Viz.plugins.GridExport({
                                                        paging          : {
                                                            start : 0,
                                                            sort  : 'Timestamp',
                                                            dir   : 'DESC'

                                                        },
                                                        maxRecords      : 1000,
                                                        promptMaxRecord : true
                                                    })]
                                        }]
                            });
                    win.show();
                }

            },

            /**
             * Opens the tacing for a specific step instance.
             */
            openStepInstanceTracing : function() {
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;

                    // Copy the columns of the TraceEntry grid and make the RequestId column hidden.
                    var newColumnsArray = [];
                    Ext.each(Viz.Configuration.Columns.TraceEntry, function(item) {
                                var copiedColumn = Ext.apply({}, item);

                                if (copiedColumn.dataIndex === 'RequestId') {
                                    copiedColumn.hidden = true;
                                }

                                newColumnsArray.push(copiedColumn);
                            }, this);

                    var win = new Ext.Window({
                                title       : $lang('msg_tracing'),
                                width       : 1000,
                                maximizable : true,
                                height      : 400,
                                layout      : 'fit',
                                iconCls     : 'viz-icon-small-trace',
                                items       : [{
                                            xtype   : 'vizGridTraceEntry',
                                            stateId : 'vizGridStep.vizGridTraceEntry',
                                            columns : newColumnsArray,
                                            plugins : [new Viz.plugins.GridExport({
                                                        paging          : {
                                                            start : 0,
                                                            sort  : 'Timestamp',
                                                            dir   : 'ASC'

                                                        },
                                                        maxRecords      : 1000,
                                                        promptMaxRecord : true
                                                    })],
                                            store   : new Viz.data.WCFJsonSimpleStore({
                                                        serviceHandler : Viz.Services.CoreWCF.TraceEntry_GetStoreByRequestId,
                                                        serviceParams  : {
                                                            location  : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                                                            requestId : entity.RequestId
                                                        },
                                                        sortInfo       : {
                                                            field     : 'Timestamp',
                                                            direction : 'ASC'
                                                        }
                                                    })
                                        }]
                            });
                    win.show();
                }

            }

        });

Ext.reg("vizGridStep", Viz.grid.Step);
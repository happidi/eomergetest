﻿/**
 * @class Viz.grid.FilterClassificationItem
 * <p>
 * A grid exposing the business entity FilterClassificationItem
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterClassificationItem = Ext.extend(Viz.grid.GridPanel, {

            /**
             * @param {Boolean} isSecurableEntity <t>true</t> if the store is a SecurableObject store, <t>false</t> (default) otherwise
             * @type Boolean
             */
            isSecurableEntity     : false,
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor           : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang("msg_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterClassificationItem_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterClassificationItem_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],

                    columns      : config.isSecurableEntity ? Viz.Configuration.Columns.SecurableEntity : Viz.Configuration.Columns.FilterClassificationItem

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterClassificationItem.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("FilterClassificationItemChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy             : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterClassificationItemChange", this.reloadStore, this);
                Viz.grid.FilterClassificationItem.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord           : function() {
                this.openTreeDetail({
                            windowTitle                     : $lang('msg_tree_classificationitem'),
                            windowWidth                     : 350,
                            windowHeight                    : 400,
                            windowIconCls                   : 'viz-icon-small-classificationitem',
                            xtypeTreeDetail                 : 'vizTreeClassificationItem',
                            checkboxExcludedTypesTreeDetail : ['Vizelia.FOL.BusinessEntities.Meter', 'Vizelia.FOL.BusinessEntities.AlarmDefinition'],
                            rootVisibleTreeDetail           : false,
                            serviceParamsTreeDetail         : {
                                flgFilter           : true,
                                excludePset         : true,
                                excludeRelationship : true
                            }
                        });
            },

            getEntityFromTreeNode : function(node) {
                if (this.isSecurableEntity) {
                    var data = {
                        // 
                        KeySecurable      : node.attributes.Key,
                        SecurableName     : node.text,
                        SecurableTypeName : 'Vizelia.FOL.BusinessEntities.ClassificationItem',
                        SecurableIconCls  : node.attributes.iconCls
                    };
                    return data;

                }
                else {
                    var data = {
                        KeyLocation : node.attributes.Key,
                        Name        : node.text,
                        TypeName    : node.attributes.entity.IfcType,
                        IconCls     : node.attributes.iconCls
                    };
                    return data;

                }

            }
        });

Ext.reg("vizGridFilterClassificationItem", Viz.grid.FilterClassificationItem);
﻿/**
 * @class Viz.grid.AlarmDefinition
 * <p>
 * A grid exposing the business entity AlarmDefinition
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.AlarmDefinition = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor              : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_alarmdefinition_add"),
                                iconCls : 'viz-icon-small-alarm-add',
                                hidden  : !$authorized('@@@@ Alarm Definition - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_alarmdefinition_update"),
                                iconCls : 'viz-icon-small-alarm-update',
                                hidden  : !$authorized('@@@@ Alarm Definition - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_alarmdefinition_delete"),
                                iconCls : 'viz-icon-small-alarm-delete',
                                hidden  : !$authorized('@@@@ Alarm Definition - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                iconCls : 'viz-icon-small-alarm-meter',
                                text    : $lang('msg_alarmdefinition_wizard_meter'),
                                handler : Viz.grid.AlarmDefinition.launchWizard.createDelegate(this, ['Meter']),
                                hidden  : !$authorized('@@@@ Alarm Definition')
                            }, {
                                iconCls : 'viz-icon-small-alarm-chart',
                                text    : $lang('msg_alarmdefinition_wizard_chart'),
                                handler : Viz.grid.AlarmDefinition.launchWizard.createDelegate(this, ['Chart']),
                                hidden  : !$authorized('@@@@ Alarm Definition')
                            }, {
                                text    : $lang("msg_alarmdefinition_process"),
                                iconCls : 'viz-icon-small-action',
                                hidden  : !$authorized('@@@@ Alarm Definition - Full Control'),
                                scope   : this,
                                handler : this.onProcessAlarmDefinition
                            }],
                    formConfig   : {
                        common      : {
                            width            : 850,
                            height           : 450,
                            xtype            : 'vizFormAlarmDefinition',
                            messageSaveStart : 'AlarmDefinitionChangeStart',
                            messageSaveEnd   : 'AlarmDefinitionChange',
                            readonly         : !$authorized('@@@@ Alarm Definition - Full Control')

                        },
                        create      : {
                            title   : $lang("msg_alarmdefinition_add"),
                            iconCls : 'viz-icon-small-alarm-add'
                        },
                        update      : {
                            title               : $lang("msg_alarmdefinition_update"),
                            iconCls             : 'viz-icon-small-alarm-update',
                            titleEntity         : 'Title',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyAlarmDefinition'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-alarm-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.AlarmDefinition_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.AlarmDefinition(),
                    columns      : Viz.Configuration.Columns.AlarmDefinition
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.AlarmDefinition.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("AlarmDefinitionChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy                : function() {
                Viz.util.MessageBusMgr.unsubscribe("AlarmDefinitionChange", this.reloadStore, this);
                Viz.grid.AlarmDefinition.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Process the selected Alarm Definition
             */
            onProcessAlarmDefinition : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;

                var msgDelete;
                if (selections.length > 1)
                    msgDelete = String.format($lang('msg_alarmdefinition_process_confirmation_multiple'), selections.length);
                else
                    msgDelete = $lang('msg_alarmdefinition_process_confirmation_single');

                Ext.MessageBox.confirm($lang('msg_alarmdefinition_process'), msgDelete, function(button) {
                            if (button == 'yes') {
                                var keys = [];
                                for (var i = 0, r; r = selections[i]; i++) {
                                    keys.push(r.json.KeyAlarmDefinition);
                                }
                                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                            pollingInterval        : 1000,
                                            serviceStart           : Viz.Services.EnergyWCF.AlarmDefinition_ProcessBegin,
                                            serviceParams          : {
                                                Keys : keys
                                            },
                                            showProgress           : true,
                                            showProgressPercentage : true,
                                            modal                  : false,
                                            progressWindowTitle    : $lang('msg_alarmdefinition_process')
                                        });
                                submitter.start();
                            }

                        }, this);
            },

            deleteRecordInternal     : function(selections) {
                for (var i = 0; i < selections.length; i++) {
                    var r = selections[i];
                    this.store.remove(r);
                }
                if (this.store.writer) { // We don't need to call save if the store does not have a writer, for example a memory store.
                    // this save purpose is only to create this.store.crudStore
                    this.onSaveStore();
                    var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                pollingInterval        : 1000,
                                serviceStart           : Viz.Services.EnergyWCF.AlarmDefinition_BeginSaveStore,
                                serviceParams          : {
                                    store : this.store.crudStore
                                },
                                showProgress           : true,
                                showProgressPercentage : true,
                                modal                  : true,
                                progressWindowTitle    : $lang('msg_metervalidationrule_delete')
                            });
                    submitter.start();
                }
                this.fireEvent("deleterecord", this, selections);
            }
        });
Ext.reg("vizGridAlarmDefinition", Viz.grid.AlarmDefinition);

Ext.apply(Viz.grid.AlarmDefinition, {

            /**
             * Launch the wizard
             */
            launchWizard : function(type) {
                Viz.openFormCrud({
                            common : {
                                width               : 600,
                                height              : 400,
                                xtype               : 'vizFormAlarmDefinitionWizard',
                                title               : $lang("msg_alarmdefinition_wizard"),
                                iconCls             : 'viz-icon-small-wizard',
                                titleEntity         : 'Title',
                                serviceParamsEntity : [{
                                            name  : 'Key',
                                            value : 'KeyAlarmDefinition'
                                        }]
                            }
                        }, 'create', {
                            type : type
                        });
            }

        });

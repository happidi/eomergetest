﻿/**
 * @class Viz.grid.MappingTask
 * <p>
 * A grid exposing the business entity MappingTask
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.MappingTask = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor  : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    // cls : 'viz-grid-norowborder',
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_mapping_task_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('MappingTask_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_mapping_task_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('MappingTask_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_mapping_task_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('MappingTask_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_mapping_task_test_action"),
                                iconCls : 'viz-icon-small-apply',
                                // hidden : !$authorized('MappingTask_write'),
                                scope   : this,
                                handler : this.onTestRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 520,
                            height           : 580,
                            xtype            : 'vizFormMappingTask',
                            messageSaveStart : 'MappingTaskChangeStart',
                            messageSaveEnd   : 'MappingTaskChange'
                        },
                        create      : {
                            title   : $lang("msg_mapping_task_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_mapping_task_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyMappingTask'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-mapping_task-update',
                            serviceHandlerUpdateBatch : Viz.Services.MappingWCF.MappingTask_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.MappingTask(),
                    columns      : Viz.Configuration.Columns.MappingTask
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.MappingTask.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("MappingTaskChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy    : function() {
                Viz.util.MessageBusMgr.unsubscribe("MappingTaskChange", this.reloadStore, this);
                Viz.grid.MappingTask.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Tests the record
             */
            onTestRecord : function() {
                var selections = this.getSelectionModel().getSelections();

                if (selections.length !== 1) {
                    // tell them we can't test multiple.
                    Ext.MessageBox.alert($lang('msg_select_exactly_one_item'));
                    return;
                }

                Ext.MessageBox.show({
                            title   : $lang('msg_mapping_task_test_title'),
                            msg     : $lang('msg_mapping_task_test_instructions'),
                            buttons : Ext.MessageBox.OKCANCEL,
                            fn      : function(button) {
                                if (button !== 'ok') {
                                    return;
                                }

                                // This test is a long running operation that returns a business entity indicating the test result.
                                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                            serviceStart  : Viz.Services.MappingWCF.BeginTestConnection,
                                            serviceParams : {
                                                keyMappingTask : selections[0].data.KeyMappingTask
                                            },
                                            showProgress  : true,
                                            resultHandler : function(result) {
                                                // Convert result to friendly message text.
                                                if (result) {
                                                    var resultMsg = $lang('msg_mapping_task_test_result_connect') + ': <b>' + result.Connect + '</b><br/>';
                                                    resultMsg += $lang('msg_mapping_task_test_result_create_file') + ': <b>' + result.CreateFile + '</b><br/>';
                                                    resultMsg += $lang('msg_mapping_task_test_result_delete_file') + ': <b>' + result.DeleteFile + '</b><br/>';
                                                    resultMsg += $lang('msg_mapping_task_test_result_create_directory') + ': <b>' + result.CreateDirectory + '</b><br/>';
                                                    resultMsg += $lang('msg_mapping_task_test_result_delete_directory') + ': <b>' + result.DeleteDirectory + '</b><br/>';
                                                    resultMsg += '<br/>' + $lang('msg_mapping_task_test_result_disclaimer');
                                                }
                                                else {
                                                    resultMsg += ($lang('msg_mapping_task_test_result_error_details') + ':<br/>' + Ext.util.Format.htmlEncode(result.ConnectException) + '<br/>');
                                                }

                                                Ext.MessageBox.show({
                                                            title   : $lang('msg_mapping_task_test_result_title'),
                                                            msg     : resultMsg,
                                                            buttons : Ext.MessageBox.OK
                                                        });
                                            }
                                        });

                                submitter.start();
                            }

                        })

            }

        });

Ext.reg("vizGridMappingTask", Viz.grid.MappingTask);
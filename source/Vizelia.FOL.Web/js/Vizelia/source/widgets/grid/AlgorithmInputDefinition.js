﻿/**
 * @class Viz.grid.Algorithm
 * <p>
 * A grid exposing the business entity Algorithm
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.AlgorithmInputDefinition = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor  : false,
                    hasToolbar    : false,
                    autoLoadStore : true,
                    tbar          : [{
                        text: $lang("msg_algorithminputdefinition_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden: !$authorized('@@@@ Algorithm - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text: $lang("msg_algorithminputdefinition_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden: !$authorized('@@@@ Algorithm - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text: $lang("msg_algorithminputdefinition_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden: !$authorized('@@@@ Algorithm - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig    : this.getFormConfig(),
                    store: config.store || new Viz.store.AlgorithmInputDefinition({
                        KeyAlgorithm: config.KeyAlgorithm
                            }),
                    columns: Viz.Configuration.Columns.AlgorithmInputDefinition
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.AlgorithmInputDefinition.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("AlgorithmInputDefinitionChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("AlgorithmInputDefinitionChange", this.reloadStore, this);
                Viz.grid.AlgorithmInputDefinition.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig     : function() {
                return {
                    common      : {
                        width            : 500,
                        height           : 160,
                        xtype: 'vizFormAlgorithmInputDefinition',
                        messageSaveStart: 'AlgorithmInputDefinitionChangeStart',
                        messageSaveEnd: 'AlgorithmInputDefinitionChange',
                        readonly: !$authorized('@@@@ Algorithm - Full Control')
                    },
                    create      : {
                        title: $lang("msg_algorithminputdefinition_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title: $lang("msg_algorithminputdefinition_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'Name',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value: 'KeyAlgorithmInputDefinition'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch: Viz.Services.EnergyWCF.AlgorithmInputDefinition_FormUpdateBatch
                    }
                };
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortal.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyAlgorithm = this.KeyAlgorithm;
                if (KeyAlgorithm == '' || KeyAlgorithm == null) {
                    return false;
                }
                entity.KeyAlgorithm = KeyAlgorithm;
                return true;
            }

        });
        Ext.reg("vizGridAlgorithmInputDefinition", Viz.grid.AlgorithmInputDefinition);
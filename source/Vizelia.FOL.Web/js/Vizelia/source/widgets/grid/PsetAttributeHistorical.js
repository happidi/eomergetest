﻿/**
 * @class Viz.grid.PsetAttributeHistorical
 * <p>
 * A grid exposing the business entity PsetAttributeHistorical
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.PsetAttributeHistorical = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor         : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_psetattributehistorical_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden  : !$authorized('@@@@ Pset - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_psetattributehistorical_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden  : !$authorized('@@@@ Pset - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_psetattributehistorical_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ Pset - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_psetattributehistorical_associateendpoint"),
                                iconCls : 'viz-icon-small-dataacquisitioncontainer',
                                hidden  : !$authorized('@@@@ Pset - Full Control'),
                                scope   : this,
                                handler : this.onAssociateEndpoint
                            }],
                    formConfig   : {
                        common      : {
                            width            : 410,
                            height           : 180,
                            xtype            : 'vizFormPsetAttributeHistorical',
                            messageSaveStart : 'PsetAttributeHistoricalChangeStart',
                            messageSaveEnd   : 'PsetAttributeHistoricalChange',
                            psetDataType     : config.psetDataType,
                            readonly         : !$authorized('@@@@ Pset - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_psetattributehistorical_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_psetattributehistorical_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'KeyPsetAttributeHistorical',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyPsetAttributeHistorical'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.CoreWCF.PsetAttributeHistorical_FormUpdateBatch
                        }
                    },
                    store        : config.store || new Viz.store.PsetAttributeHistorical({
                                KeyObject              : config.KeyObject,
                                KeyPropertySingleValue : config.KeyPropertySingleValue
                            }),
                    columns      : Viz.Configuration.Columns.PsetAttributeHistorical
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.PsetAttributeHistorical.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("PsetAttributeHistoricalChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("PsetAttributeHistoricalChange", this.reloadStore, this);
                Viz.grid.PsetAttributeHistorical.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortal.
             */
            onBeforeAddRecord   : function(grid, entity) {
                if (Ext.isEmpty(this.KeyObject))
                    return false;
                if (Ext.isEmpty(this.KeyPropertySingleValue))
                    return false;

                entity.KeyObject = this.KeyObject;
                entity.KeyPropertySingleValue = this.KeyPropertySingleValue;

                return true;
            },

            onAssociateEndpoint : function() {
                if (!Ext.isEmpty(this.KeyObject) && !Ext.isEmpty(this.KeyPropertySingleValue)) {
                    Viz.Services.EnergyWCF.PsetAttributeHistoricalEndpoint_GetItemByKeyObject({
                                KeyObject              : this.KeyObject,
                                KeyPropertySingleValue : this.KeyPropertySingleValue,
                                success                : function(result) {
                                    var mode = 'update';
                                    var entity = result;
                                    if (!entity) {
                                        mode = 'create';
                                        entity = {
                                            KeyObject              : this.KeyObject,
                                            KeyPropertySingleValue : this.KeyPropertySingleValue
                                        };
                                    }

                                    Viz.openFormCrud({
                                                common : {
                                                    width            : 450,
                                                    height           : 140,
                                                    mode             : 'local',
                                                    xtype            : 'vizFormPsetAttributeHistoricalEndpoint',
                                                    messageSaveStart : 'PsetAttributeHistoricalEndpointChangeStart',
                                                    messageSaveEnd   : 'PsetAttributeHistoricalEndpointChange',
                                                    title            : $lang('msg_psetattributehistorical_associateendpoint'),
                                                    iconCls          : 'viz-icon-small-dataacquisitioncontainer'
                                                },
                                                update : {
                                                    titleEntity : 'KeyPsetAttributeHistoricalEndpoint'
                                                }
                                            }, mode, entity)
                                },
                                scope                  : this

                            });

                }
            }

        });

Ext.reg("vizGridPsetAttributeHistorical", Viz.grid.PsetAttributeHistorical);
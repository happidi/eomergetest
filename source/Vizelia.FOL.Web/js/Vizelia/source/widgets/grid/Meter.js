﻿/**
 * @class Viz.grid.Meter
 * <p>
 * A grid exposing the business entity Meter
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Meter = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor         : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_meter_add"),
                                iconCls : 'viz-icon-small-meter-add',
                                hidden  : !$authorized('@@@@ Meters - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_meter_update"),
                                iconCls : 'viz-icon-small-meter-update',
                                hidden  : !$authorized('@@@@ Meters - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_meter_delete"),
                                iconCls : 'viz-icon-small-meter-delete',
                                hidden  : !$authorized('@@@@ Meters - Full Control'),
                                scope   : this,
                                handler : function(button, event) {
                                    this.onDeleteRecord(button, event, $lang('msg_meter_delete_confirmation_single'), $lang('msg_meter_delete_confirmation_multiple'));
                                }
                            }],
                    formConfig   : {
                        common      : {
                            width            : 700,
                            height           : 430,
                            xtype            : 'vizFormMeter',
                            messageSaveStart : 'MeterChangeStart',
                            messageSaveEnd   : 'MeterChange',
                            readonly         : !$authorized('@@@@ Meters - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_meter_add"),
                            iconCls : 'viz-icon-small-meter-add'
                        },
                        update      : {
                            title               : $lang("msg_meter_update"),
                            iconCls             : 'viz-icon-small-meter-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyMeter'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-meter-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.Meter_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.Meter(),
                    plugins      : [new Viz.plugins.GridExport({
                                paging          : {
                                    start : 0,
                                    sort  : 'Name',
                                    dir   : 'ASC'
                                },
                                maxRecords      : 1000,
                                promptMaxRecord : true
                            })],
                    columns      : Viz.Configuration.Columns.Meter
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.Meter.superclass.constructor.call(this, config);

                this.on({
                            rowcontextmenu : {
                                scope : this,
                                fn    : this.onRowContextMenu
                            }
                        })
                Viz.util.MessageBusMgr.subscribe("MeterChange", this.reloadStore, this);
                Viz.util.MessageBusMgr.subscribe("MeterDelete", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("MeterChange", this.reloadStore, this);
                Viz.util.MessageBusMgr.unsubscribe("MeterDelete", this.reloadStore, this);
                Viz.grid.Meter.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * RowContextMenu listener.
             * @param {} grid
             * @param {} row
             * @param {} event
             */
            onRowContextMenu    : function(grid, row, event) {
                event.stopEvent();
                var xy = event.getXY();
                this.getSelectionModel().selectRow(row, false);
                if (this.hideMenu == true)
                    return;

                var entity = this.store.getAt(row).json;
                if (entity) {
                    var ctxMenu = new Ext.menu.Menu({
                                defaults : {
                                    hideOnClick : true
                                },
                                items    : [{
                                            text    : $lang("msg_meter_view"),
                                            iconCls : 'viz-icon-small-display',
                                            handler : Viz.grid.Meter.displayMeter.createDelegate(this, [entity]),
                                            scope   : this
                                        }, {
                                            text    : $lang("msg_meterdata"),
                                            iconCls : 'viz-icon-small-meterdata',
                                            handler : Viz.grid.Meter.displayMeterData.createDelegate(this, [entity]),
                                            scope   : this,
                                            hidden  : !$authorized("@@@@ Meters - Full Control")
                                        }, {
                                            text    : $lang("msg_meterdata_viewalarminstances"),
                                            iconCls : 'viz-icon-small-alarminstance',
                                            handler : Viz.grid.Meter.displayMeterValidationRule.createDelegate(this, [entity]),
                                            scope   : this,
                                            hidden  : entity.AlarmInstanceFromMeterValidationRuleCount <= 0 || !$authorized("@@@@ Meter Validation Rule")
                                        }]
                            });
                    ctxMenu.on("hide", function(menu) {
                                menu.destroy();
                            });
                    ctxMenu.showAt(event.getXY());
                }
            },

            onAfterDeleteRecord : function(grid, selections) {
                if (Ext.isArray(selections)) {
                    Ext.each(selections, function(rec) {
                                this.publishMessage('MeterDelete', rec.json);
                                this.publishMessage('SpatialChange', rec.json.KeyLocation);
                            }, this);
                }
            }
        });
Ext.reg("vizGridMeter", Viz.grid.Meter);

// Static Methods
Ext.apply(Viz.grid.Meter, {

            /**
             * Display a chart preview of this meter
             * @param {} entity
             */
            displayMeter               : function(entity) {
                var win = new Ext.Window({
                            title       : $lang('msg_meter_view') + ': ' + entity.Name,
                            iconCls     : 'viz-icon-small-meter',
                            maximizable : true,
                            width       : 600,
                            height      : 400,
                            layout      : 'fit',
                            bodyStyle   : 'background:white',
                            items       : [{
                                        xtype          : 'vizChartImage',
                                        entity         : entity,
                                        serviceHandler : Viz.Services.EnergyWCF.Meter_BuildImage,
                                        serviceParams  : {
                                            Key             : entity.KeyMeter,
                                            applicationName : entity.Application ? entity.Application : Viz.App.Global.User.ApplicationName
                                        }
                                    }]
                        });
                win.show();
            },

            /**
             * Open the MeterData panel with the selected Meter.
             * @param {object/string} entity : either a Meter entity or a KeyMeter
             */
            displayMeterData           : function(entity) {
                if (Ext.isObject(entity)) {
                    Viz.App.Global.ViewPort.openModule({
                                xtype  : 'vizPanelMeterData',
                                entity : entity
                            })
                }
                else {
                    Viz.Services.EnergyWCF.Meter_GetItem({
                                Key     : entity,
                                success : function(meter) {
                                    Viz.App.Global.ViewPort.openModule({
                                                xtype  : 'vizPanelMeterData',
                                                entity : meter
                                            })
                                }
                            });
                }
            },

            /**
             * Open the list of alarminstances generated by metervalidation rule
             * @param {object/string} entity : Meter entity
             */
            displayMeterValidationRule : function(entity) {
                var win = new Ext.Window({
                            title       : $lang('msg_metervalidationrule') + ' : ' + $lang('msg_alarminstances'),
                            iconCls     : 'viz-icon-small-alarminstance',
                            maximizable : true,
                            width       : 800,
                            height      : 250,
                            layout      : 'fit',
                            items       : [{
                                        readonly            : true,
                                        xtype               : 'vizGridAlarmInstance',
                                        hideChartViewButton : true,
                                        store               : new Viz.store.MeterAlarmInstance({
                                                    KeyMeter : entity.KeyMeter
                                                }),
                                        columns             : Viz.Configuration.Columns.MeterValidationRuleAlarmInstance
                                    }]
                        });
                win.show();
            }
        });

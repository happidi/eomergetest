﻿/**
 * @class Viz.grid.Chart
 * <p>
 * A grid exposing the business entity Chart
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Chart = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor         : function(config) {
                config = config || {};

                this._favoritebutton_id = Ext.id();

                var defaultConfig = {
                    hasRowEditor     : false,
                    stripeRows       : false,
                    hasToolbar       : false,
                    hasRowNumber     : true,
                    autoLoadStore    : false,
                    allowupdatebatch : true,
                    // hasRowExpander : true,
                    // rowExpanderTemplate : new Ext.Template(
                    // /**/
                    // '<br><p><b>Title:</b> {Title}</p>',
                    // /**/
                    // '<p><b>Description:</b> {Description}</p>',
                    // /**/
                    // '<p><b>Specific Analysis:</b> {SpecificAnalysis}</p>'),
                    tbar             : [{
                                id            : this._favoritebutton_id,
                                iconCls       : 'viz-icon-small-chart-favorite',
                                text          : $lang("msg_chart_favorite"),
                                enableToggle  : true,
                                toggleHandler : function(button, state) {
                                    Viz.util.Portal.onToggleFavoriteButton.createDelegate(this, [button, state])();
                                },
                                scope         : this,
                                pressed       : true
                            }, '-', {
                                text    : $lang("msg_chart_add"),
                                iconCls : 'viz-icon-small-chart-add',
                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_chart_update"),
                                iconCls : 'viz-icon-small-chart-update',
                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_chart_delete"),
                                iconCls : 'viz-icon-small-chart-delete',
                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_chart_copy"),
                                iconCls : 'viz-icon-small-copy',
                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                scope   : this,
                                handler : this.onCopyRecord
                            }, {
                                text    : $lang("msg_chart_view"),
                                iconCls : 'viz-icon-small-display',
                                // hidden: !$authorized('@@@@ Chart - Full Control'),
                                scope   : this,
                                handler : this.onChartDisplay
                            }, {
                                text    : $lang("msg_chart_wizard"),
                                iconCls : 'viz-icon-small-wizard',
                                hidden  : !$authorized('@@@@ Chart - Full Control'),
                                scope   : this,
                                handler : Viz.grid.Chart.launchWizard
                            }, '-', {
                                text    : $lang("msg_portlets"),
                                iconCls : 'viz-icon-small-portlet',
                                hidden  : !$authorized('@@@@ Portlet'),
                                scope   : this,
                                handler : Viz.util.Portal.displayPortletGridFromEntity.createDelegate(this)
                            }],
                    formConfig       : this.getFormConfig(),
                    formModal        : false,
                    store            : new Viz.store.Chart(),
                    columns          : Viz.Configuration.Columns.Chart
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.Chart.superclass.constructor.call(this, config);
                this.addEvents(
                        /**
                         * @event beforechartdiplay Fires before displaying a chart.
                         * @param {Viz.grid.GridPanel} this.
                         * @param {Object} record The record of the chart that is being displayed.
                         * @param {Object} entity The entity of the chart that is being displayed. It could be changed by the handler.
                         */
                        'beforechartdisplay');

                this.on({
                            render          : Viz.util.Portal.gridRender,
                            filteravailable : Viz.util.Portal.gridFilterAvailable,
                            filtercleared   : Viz.util.Portal.toggleFavoriteButton.createDelegate(this, [this._favoritebutton_id, false]),
                            scope           : this
                        });

                Viz.util.MessageBusMgr.subscribe("ChartChange", this.reloadStore, this);
            },
            /**
             * After deleting we make sure the portlet is deleted as well.
             * @param {} record
             * @param {} selections
             */
            onAfterDeleteRecord : function(record, selections) {
                for (var i = 0, r; r = selections[i]; i++) {
                    Viz.util.MessageBusMgr.fireEvent('PortletDelete', r.json);
                }
            },
            /**
             * Destroy
             * @method onDestroy
             * @private
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("ChartChange", this.reloadStore, this);
                Viz.grid.Chart.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig       : function() {
                return {
                    common      : {
                        width            : 980,
                        height           : 610,
                        xtype            : 'vizFormChart',
                        messageSaveStart : 'ChartChangeStart',
                        messageSaveEnd   : 'ChartChange',
                        readonly         : !$authorized('@@@@ Chart - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_chart_add"),
                        iconCls : 'viz-icon-small-chart-add'
                    },
                    update      : {
                        title               : $lang("msg_chart_update"),
                        iconCls             : 'viz-icon-small-chart-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyChart'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-chart-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.Chart_FormUpdateBatch,
                        uniqueTabs                : ['general', 'display'],// , 'dynamicdisplay'],
                        deferredRender            : false
                    }
                };
            },
            /**
             * Handler for displaying a chart.
             * @method onChartDisplay
             */
            onChartDisplay      : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    if (this.fireEvent("beforechartdisplay", this, record, entity)) {
                        Viz.grid.Chart.openChartDisplay(entity);
                    }
                }
            },

            /**
             * @private Handler for copying a chart.
             */
            onCopyRecord        : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;

                var keys = new Array();
                for (var i = 0, r; r = selections[i]; i++) {
                    keys.push(r.data.KeyChart);
                }
                this.el.mask();
                Viz.Services.EnergyWCF.Chart_Copy({
                            keys    : keys,
                            success : function(data) {
                                this.el.unmask();
                                this.reloadStore();
                            },
                            scope   : this
                        });
            }

        });
Ext.reg("vizGridChart", Viz.grid.Chart);

// Static Methods
Ext.apply(Viz.grid.Chart, {
            /**
             * Displays a chart.
             * @method openChartDisplay
             * @static
             * @param {Object} obj : the chart entity or the KeyChart.
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openChartDisplay              : function(obj, width, height, readonly) {
                if (Ext.isObject(obj)) {
                    Viz.grid.Chart.openChartDisplayFromEntity(obj, width, height, readonly);
                }
                else {
                    Viz.Services.EnergyWCF.Chart_GetItem({
                                Key     : obj,
                                success : function(chart) {
                                    Viz.grid.Chart.openChartDisplayFromEntity(chart, width, height, readonly);
                                }
                            });
                }
            },

            /**
             * Displays a chart.
             * @method openChartDisplay
             * @static
             * @param {Viz.BusinessEntity.Chart} entity : the chart entity .
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openChartDisplayFromEntity    : function(entity, width, height, readonly) {
                var win = new Viz.portal.PortletWindow({
                            entity        : entity,
                            entityPortlet : {
                                TypeEntity : 'Vizelia.FOL.BusinessEntities.Chart'
                            },
                            xtypePortlet  : 'vizChartViewer',
                            updateHandler : Viz.grid.Chart.update,
                            readonly      : readonly
                        });
                win.show();
            },

            /**
             * Create a new Chart (to be called from desktop or tree)
             * @param {} callback
             */
            create                        : function(entity) {
                Viz.openFormCrud(Viz.grid.Chart.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing Chart (to be called from desktop or tree) *
             * @param {Chart} entity
             * @param {Ext.Window} window
             * @param {object} extraConfig
             */
            update                        : function(entity, window, extraConfig) {
                if (entity) {
                    var conf = Viz.grid.Chart.prototype.getFormConfig();
                    if (Ext.isObject(extraConfig))
                        Ext.apply(conf.common, extraConfig);
                    if (window) {
                        var box = window.getBox();
                        conf.common.x = box.width + box.x;
                        conf.common.y = box.y;
                    }
                    Viz.openFormCrud(conf, 'update', entity);
                }
            },

            /**
             * Javascript method called when clicking a point in dotnetcharting correlation chart.
             * @param {} KeyChart
             * @param {} xValue
             * @param {} yValue
             */
            onCorrelationCloudPointClick  : function(KeyChart, xValue, yValue, localId) {
                if (!$authorized('@@@@ Chart - Full Control'))
                    return;
                Viz.Services.EnergyWCF.Chart_CorrelationRemovePoint({
                            KeyChart : KeyChart,
                            xValue   : xValue,
                            yValue   : yValue,
                            LocalId  : localId,
                            success  : function(chart) {
                                Viz.util.MessageBusMgr.publish('ChartChange', chart);
                            },
                            scope    : this

                        });
            },

            /**
             * Javascript method called when clicking a dataserie legendentry.
             * @param {} KeyChart
             * @param {} KeyDataSerie
             * @param {} localId
             * @param {} visible
             */
            onDataSerieLegendEntryClick   : function(area, KeyChart, KeyDataSerie, localId, name, visible) {
                if (!$authorized('@@@@ Chart - Full Control'))
                    return;
                var toggleVisibility = function() {
                    Viz.Services.EnergyWCF.DataSerie_ToggleVisibility({
                                KeyChart     : KeyChart,
                                KeyDataSerie : KeyDataSerie,
                                localId      : localId,
                                Visible      : false,
                                serieName    : name,
                                success      : function(response) {
                                    Viz.util.MessageBusMgr.fireEvent("DataSerieChange", response);
                                }
                            });
                }

                var typeUpdate = function(type) {
                    Viz.Services.EnergyWCF.DataSerie_TypeUpdate({
                                KeyChart     : KeyChart,
                                KeyDataSerie : KeyDataSerie,
                                localId      : localId,
                                type         : type,
                                serieName    : name,
                                success      : function(response) {
                                    Viz.util.MessageBusMgr.fireEvent("DataSerieChange", response);
                                }
                            });
                }

                var secondaryAxis = function() {
                    Viz.Services.EnergyWCF.DataSerie_UseSecondaryAxis({
                                KeyChart     : KeyChart,
                                KeyDataSerie : KeyDataSerie,
                                localId      : localId,
                                serieName    : name,
                                success: function (response) {

                                    Viz.util.MessageBusMgr.fireEvent("DataSerieChange", response);
                                }
                            });
                }

                var customizeDataserie = function() {
                    if (Ext.isEmpty(KeyDataSerie) == false) {
                        Viz.Services.EnergyWCF.DataSerie_GetItem({
                                    Key     : KeyDataSerie,
                                    success : function(response) {
                                        Viz.grid.DataSerie.update(response);
                                    }
                                })
                    }
                    else if (Ext.isEmpty(localId) == false) {
                        Viz.grid.DataSerie.create({
                                    KeyChart             : KeyChart,
                                    LocalId              : localId,
                                    Name                 : name,
                                    IsClassificationItem : false
                                });
                    }
                };

                var menu = {
                    defaults : {
                        hideOnClick : true
                    },
                    items    : [{
                                text    : $lang('msg_dataseries_customise') + ' :' + name,
                                iconCls : 'viz-icon-small-dataserie',
                                handler : customizeDataserie
                            }, '-', {
                                text    : $lang("msg_dataserie_hide"),
                                iconCls : 'viz-icon-small-cancel',
                                handler : toggleVisibility,
                                scope   : this
                            }, {
                                text    : $lang("msg_dataserie_usesecondaryaxis"),
                                iconCls : 'viz-icon-small-chart-axis',
                                handler : secondaryAxis,
                                scope   : this
                            }, '-', {
                                text    : $lang("msg_chart_type"),
                                iconCls : 'viz-icon-small-chart',
                                menu    : {
                                    xtype                    : 'menustore',
                                    store                    : new Viz.store.Enum({
                                                serviceParams : {
                                                    enumTypeName : 'Vizelia.FOL.BusinessEntities.DataSerieType,Vizelia.FOL.Common'
                                                }
                                            }),
                                    createMenuItemFromRecord : function(record) {
                                        return {
                                            text    : record.data.Label,
                                            iconCls : record.data.IconCls,
                                            handler : typeUpdate.createDelegate(this, [record.data.Value]),
                                            scope   : this
                                        }
                                    }.createDelegate(this)
                                }
                            }]
                };
                Viz.grid.Chart.displayAreaMenu(menu);
            },

            /**
             * Launch the chart wizard
             */
            launchWizard                  : function(config) {
                Viz.openFormCrud({
                            common : {
                                width               : 675,
                                height              : 430,
                                stateId             : 'wizard_vizFormChartWizard',
                                xtype               : 'vizFormChartWizard',
                                title               : $lang("msg_chart_wizard"),
                                iconCls             : 'viz-icon-small-wizard',
                                titleEntity         : 'Title',
                                serviceParamsEntity : [{
                                            name  : 'Key',
                                            value : 'KeyChart'
                                        }]
                            }
                        }, 'create', {
                            KeyPortalColumn : config ? config.KeyPortalColumn : null
                        });
            },

            getAreaCenter                 : function(shape, coords) {
                var coordsArray = coords.split(','), center = [];
                if (shape == 'circle') {
                    // For circle areas the center is given by the first two values
                    center = [coordsArray[0], coordsArray[1]];
                }
                else {
                    // For rect and poly areas we need to loop through the coordinates
                    var coord, minX = maxX = parseInt(coordsArray[0], 10), minY = maxY = parseInt(coordsArray[1], 10);
                    for (var i = 0, l = coordsArray.length; i < l; i++) {
                        coord = parseInt(coordsArray[i], 10);
                        if (i % 2 == 0) { // Even values are X coordinates
                            if (coord < minX) {
                                minX = coord;
                            }
                            else if (coord > maxX) {
                                maxX = coord;
                            }
                        }
                        else { // Odd values are Y coordinates
                            if (coord < minY) {
                                minY = coord;
                            }
                            else if (coord > maxY) {
                                maxY = coord;
                            }
                        }
                    }
                    center = [parseInt((minX + maxX) / 2, 10), parseInt((minY + maxY) / 2, 10)];
                }
                return (center);
            },

            displayAreaMenu               : function(menu) {
                var ctxMenu = new Ext.menu.Menu(menu);
                ctxMenu.on("hide", function(menu) {
                            menu.destroy();
                        });
                ctxMenu.showAt([Viz.MouseX, Viz.MouseY]);
                return;
            },

            onDataPointClick              : function(KeyChart, datetime, KeyLocation, KeyClassificationItem, locationname, classificationname, datetimename, mode) {
                if (!$authorized('@@@@ ChartDrillDown - Full Control'))
                    return;

                var portalWindow = Viz.App.Global.ViewPort.getMainTab().getActiveTab();
                if (!portalWindow || !portalWindow.getActiveKeyPortalTab)
                    return;

                var drillDown = function(type) {
                    Viz.Services.EnergyWCF.ChartDrillDown_Update({
                                KeyChart              : KeyChart,
                                XDateTime             : datetime,
                                KeyLocation           : KeyLocation,
                                KeyClassificationItem : KeyClassificationItem,
                                KeyPortalTab          : portalWindow.getActiveKeyPortalTab(),
                                KeyPortalWindow       : portalWindow.KeyPortalWindow,
                                Type                  : type,
                                success               : function(response) {
                                    if (Ext.isArray(response)) {
                                        Ext.each(response, function(drilldown) {
                                                    Viz.util.MessageBusMgr.fireEvent("ChartDrillDownChange", drilldown);
                                                }, this);
                                    }
                                }
                            });
                };

                var menu = {
                    defaults : {
                        hideOnClick : false
                    },
                    items    : [{
                                text    : mode,// $lang('msg_chartdrilldown') + ': ' + .toBold(),
                                iconCls : 'viz-icon-small-chartviewer'
                            }, '-', {
                                text    : datetimename,// $lang("msg_chartmodel_date") + ': ' + .toBold(),
                                iconCls : 'viz-icon-small-calendar',
                                hidden  : Ext.isEmpty(datetime),
                                menu    : {
                                    items : [{
                                                text    : $lang("msg_chartdrilldown_focusdate"),
                                                iconCls : 'viz-icon-small-calendar',
                                                handler : drillDown.createDelegate(this, [Vizelia.FOL.BusinessEntities.DrillDownType.FocusDate]),
                                                scope   : this
                                            }, '-', {
                                                text    : $lang("msg_chartdrilldown_timeinterval_drilldown"),
                                                iconCls : 'viz-icon-small-chart-timeinterval',
                                                handler : drillDown.createDelegate(this, [Vizelia.FOL.BusinessEntities.DrillDownType.DrillDownTimeInterval]),
                                                scope   : this
                                            }, {
                                                text    : $lang("msg_chartdrilldown_timeinterval_drillup"),
                                                iconCls : 'viz-icon-small-chart-timeinterval',
                                                handler : drillDown.createDelegate(this, [Vizelia.FOL.BusinessEntities.DrillDownType.DrillUpTimeInterval]),
                                                scope   : this
                                            }, '-', {
                                                text    : $lang("msg_chartdrilldown_startdate"),
                                                iconCls : 'viz-icon-small-calendar',
                                                handler : drillDown.createDelegate(this, [Vizelia.FOL.BusinessEntities.DrillDownType.StartDate]),
                                                scope   : this
                                            }, {
                                                text    : $lang("msg_chartdrilldown_enddate"),
                                                iconCls : 'viz-icon-small-calendar',
                                                handler : drillDown.createDelegate(this, [Vizelia.FOL.BusinessEntities.DrillDownType.EndDate]),
                                                scope   : this
                                            }]
                                }
                            }, {
                                text    : locationname,// $lang("msg_location") + ': ' + ,
                                iconCls : 'viz-icon-small-site',
                                hidden  : Ext.isEmpty(KeyLocation),
                                menu    : {
                                    items : [{
                                                text    : $lang("msg_chartdrilldown_focuslocation"),
                                                iconCls : 'viz-icon-small-calendar',
                                                handler : drillDown.createDelegate(this, [Vizelia.FOL.BusinessEntities.DrillDownType.FocusLocation]),
                                                scope   : this
                                            }, '-', {
                                                text    : $lang("msg_chartdrilldown_location_drilldown"),
                                                iconCls : 'viz-icon-small-building',
                                                handler : drillDown.createDelegate(this, [Vizelia.FOL.BusinessEntities.DrillDownType.DrillDownLocalisation]),
                                                scope   : this
                                            }, {
                                                text    : $lang("msg_chartdrilldown_location_drillup"),
                                                iconCls : 'viz-icon-small-site',
                                                handler : drillDown.createDelegate(this, [Vizelia.FOL.BusinessEntities.DrillDownType.DrillUpLocalisation]),
                                                scope   : this
                                            }]
                                }
                            }, {
                                text    : classificationname,// $lang("msg_meter_classification") + ': ' +,
                                iconCls : 'viz-icon-small-meter',
                                hidden  : Ext.isEmpty(KeyClassificationItem),
                                menu    : {
                                    items : [{
                                                text    : $lang("msg_chartdrilldown_focusclassification"),
                                                iconCls : 'viz-icon-small-meter',
                                                handler : drillDown.createDelegate(this, [Vizelia.FOL.BusinessEntities.DrillDownType.FocusClassification]),
                                                scope   : this
                                            }, '-', {
                                                text    : $lang("msg_chartdrilldown_classification_drilldown"),
                                                iconCls : 'viz-icon-small-meter',
                                                handler : drillDown.createDelegate(this, [Vizelia.FOL.BusinessEntities.DrillDownType.DrillDownClassification]),
                                                scope   : this
                                            }, {
                                                text    : $lang("msg_chartdrilldown_classification_drillup"),
                                                iconCls : 'viz-icon-small-meter',
                                                handler : drillDown.createDelegate(this, [Vizelia.FOL.BusinessEntities.DrillDownType.DrillUpClassification]),
                                                scope   : this
                                            }]
                                }
                            }, '-', {
                                text    : $lang("msg_chartdrilldown_reset"),
                                iconCls : 'viz-icon-small-delete',
                                handler : Viz.grid.Chart.onChartDrillDownReset.createDelegate(this, [KeyChart]),
                                scope   : this
                            }]
                };
                Viz.grid.Chart.displayAreaMenu(menu);
            },

            onChartDrillDownReset         : function(KeyChart) {
                if (!$authorized('@@@@ ChartDrillDown - Full Control'))
                    return;
                Viz.Services.EnergyWCF.ChartDrillDown_Reset({
                            KeyChart : KeyChart,
                            success  : function() {
                                Viz.util.MessageBusMgr.fireEvent("ChartDrillDownChange", {
                                            KeyChart : KeyChart
                                        });
                            },
                            scope    : this
                        })
            },

            onCalendarViewAreaClick       : function(KeyChart, startDate, endDate) {
                Viz.Services.EnergyWCF.ChartCalendarViewSelection_Add({
                            KeyChart  : KeyChart,
                            startDate : startDate,
                            endDate   : endDate,
                            success   : function() {
                                Viz.util.MessageBusMgr.fireEvent("ChartCalendarViewSelectionChange", {
                                            KeyChart : KeyChart
                                        });
                            },
                            scope     : this
                        })
            },

            onCalendarViewSelectionDelete : function(KeyChart, KeyChartCalendarViewSelection) {
                Viz.Services.EnergyWCF.ChartCalendarViewSelection_Delete({
                            KeyChartCalendarViewSelection : KeyChartCalendarViewSelection,
                            success                       : function() {
                                Viz.util.MessageBusMgr.fireEvent("ChartCalendarViewSelectionChange", {
                                            KeyChart : KeyChart
                                        });
                            },
                            scope                         : this
                        })
            }
        });

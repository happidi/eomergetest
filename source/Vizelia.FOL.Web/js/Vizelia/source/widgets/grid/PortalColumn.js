﻿/**
 * @class Viz.grid.PortalColumn
 * <p>
 * A grid exposing the business entity PortalColumn
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.PortalColumn = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_portalcolumn_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('PortalColumn_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_portalcolumn_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('PortalColumn_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_portalcolumn_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('PortalColumn_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : config.store || new Viz.store.PortalColumn({
                                KeyPortalTab : config.KeyPortalTab
                            }),
                    columns      : Viz.Configuration.Columns.PortalColumn
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.PortalColumn.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("PortalColumnChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("PortalColumnChange", this.reloadStore, this);
                Viz.grid.PortalColumn.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig     : function() {
                return {
                    common      : {
                        width            : 550,
                        height           : 400,
                        xtype            : 'vizFormPortalColumn',
                        messageSaveStart : 'PortalColumnChangeStart',
                        messageSaveEnd   : 'PortalColumnChange'
                    },
                    create      : {
                        title   : $lang("msg_portalcolumn_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_portalcolumn_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'Order',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyPortalColumn'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.PortalColumn_FormUpdateBatch
                    }
                };
            },
            
             /**
             * After deleting we make sure the portlet is deleted as well.
             * @param {} record
             * @param {} selections
             */
            onAfterDeleteRecord : function(record, selections) {
                for (var i = 0, r; r = selections[i]; i++) {
                    Viz.util.MessageBusMgr.fireEvent('PortalColumnDelete', r.json);
                }
            },
            
            /**
             * @private Handler for updating the entity with the parent KeyPortalTab.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyPortalTab = this.KeyPortalTab;
                if (KeyPortalTab == '' || KeyPortalTab == null) {
                    return false;
                }
                entity.KeyPortalTab = KeyPortalTab;
                entity.Order = this.getMaxValueFromStore('Order') + 1;

                return true;
            }

        });

Ext.reg("vizGridPortalColumn", Viz.grid.PortalColumn);

// Static Methods
Ext.apply(Viz.grid.PortalColumn, {
            /**
             * Create a new PortalColumn (to be called from desktop or tree)
             * @param {} callback
             */
            create : function(entity) {
                Viz.openFormCrud(Viz.grid.PortalColumn.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing PortalColumn (to be called from desktop or tree)
             * @param {} callback
             */
            update : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.PortalColumn.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
﻿/**
 * @class Viz.grid.DataSeriePsetRatio
 * <p>
 * A grid exposing the business entity DataSeriePsetRatio
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.DataSeriePsetRatio = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_dataseriepsetratio_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('DataSeriePsetRatio_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_dataseriepsetratio_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('DataSeriePsetRatio_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_dataseriepsetratio_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('DataSeriePsetRatio_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 600,
                            height           : 280,
                            xtype            : 'vizFormDataSeriePsetRatio',
                            messageSaveStart : 'DataSeriePsetRatioChangeStart',
                            messageSaveEnd   : 'DataSeriePsetRatioChange'
                        },
                        create      : {
                            title   : $lang("msg_dataseriepsetratio_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_dataseriepsetratio_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'PropertySingleValueLongPath',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyDataSeriePsetRatio'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.DataSeriePsetRatio_FormUpdateBatch
                        }
                    },
                    store        : config.store || new Viz.store.DataSeriePsetRatio({
                                KeyDataSerie : config.KeyDataSerie
                            }),
                    columns      : Viz.Configuration.Columns.DataSeriePsetRatio
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.DataSeriePsetRatio.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("DataSeriePsetRatioChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("DataSeriePsetRatioChange", this.reloadStore, this);
                Viz.grid.DataSeriePsetRatio.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyDataSerie.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyDataSerie = this.KeyDataSerie;
                if (KeyDataSerie == '' || KeyDataSerie == null) {
                    return false;
                }
                entity.KeyDataSerie = KeyDataSerie;
                entity.Order = this.getMaxValueFromStore('Order') + 1;
                return true;
            }

        });

Ext.reg("vizGridDataSeriePsetRatio", Viz.grid.DataSeriePsetRatio);
﻿/**
 * @class Viz.grid.PortalTab
 * <p>
 * A grid exposing the business entity PortalTab
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.PortalTab = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_portaltab_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('PortalTab_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_portaltab_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('PortalTab_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_portaltab_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('PortalTab_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : config.store || new Viz.store.PortalTab({
                                KeyPortalWindow : config.KeyPortalWindow
                            }),
                    columns      : Viz.Configuration.Columns.PortalTab
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.PortalTab.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("PortalTabChange", this.reloadStore, this);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig     : function() {
                return {
                    common      : {
                        width            : 550,
                        height           : 400,
                        xtype            : 'vizFormPortalTab',
                        messageSaveStart : 'PortalTabChangeStart',
                        messageSaveEnd   : 'PortalTabChange'
                    },
                    create      : {
                        title   : $lang("msg_portaltab_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_portaltab_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyPortalTab'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.PortalTab_FormUpdateBatch
                    }
                };
            },
            
             /**
             * After deleting we make sure the portlet is deleted as well.
             * @param {} record
             * @param {} selections
             */
            onAfterDeleteRecord : function(record, selections) {
                for (var i = 0, r; r = selections[i]; i++) {
                    Viz.util.MessageBusMgr.fireEvent('PortalTabDelete', r.json);
                }
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("PortalTabChange", this.reloadStore, this);
                Viz.grid.PortalTab.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortalWindow.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyPortalWindow = this.KeyPortalWindow;
                if (KeyPortalWindow == '' || KeyPortalWindow == null) {
                    return false;
                }
                entity.KeyPortalWindow = KeyPortalWindow;
                entity.Order = this.getMaxValueFromStore('Order') + 1;
                return true;
            }

        });

Ext.reg("vizGridPortalTab", Viz.grid.PortalTab);

// Static Methods
Ext.apply(Viz.grid.PortalTab, {

            /**
             * Create a new PortalTab (to be called from desktop or tree)
             * @param {} callback
             */
            create               : function(entity) {
                Viz.openFormCrud(Viz.grid.PortalTab.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing PortalTab (to be called from desktop or tree)
             * @param {} callback
             */
            update               : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.PortalTab.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
﻿/**
 * @class Viz.grid.DrawingCanvasChart
 * <p>
 * A grid exposing the business entity DrawingCanvasChart
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.DrawingCanvasChart = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_drawingcanvaschart_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('DrawingCanvasChart_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_drawingcanvaschart_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('DrawingCanvasChart_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_drawingcanvaschart_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('DrawingCanvasChart_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : config.store || new Viz.store.DrawingCanvasChart({
                                KeyPortalWindow : config.KeyPortalWindow
                            }),
                    columns      : Viz.Configuration.Columns.DrawingCanvasChart
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.DrawingCanvasChart.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("DrawingCanvasChartChange", this.reloadStore, this);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig     : function() {
                return {
                    common      : {
                        width            : 550,
                        height           : 300,
                        xtype            : 'vizFormDrawingCanvasChart',
                        messageSaveStart : 'DrawingCanvasChartChangeStart',
                        messageSaveEnd   : 'DrawingCanvasChartChange'
                    },
                    create      : {
                        title   : $lang("msg_drawingcanvaschart_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_drawingcanvaschart_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'ZIndex',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyDrawingCanvasChart'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.DrawingCanvasChart_FormUpdateBatch
                    }
                };
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("DrawingCanvasChartChange", this.reloadStore, this);
                Viz.grid.DrawingCanvasChart.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortalWindow.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyDrawingCanvas = this.KeyDrawingCanvas;
                if (KeyDrawingCanvas == '' || KeyDrawingCanvas == null) {
                    return false;
                }
                entity.KeyDrawingCanvas = KeyDrawingCanvas;
                return true;
            }

        });

Ext.reg("vizGridDrawingCanvasChart", Viz.grid.DrawingCanvasChart);

// Static Methods
Ext.apply(Viz.grid.DrawingCanvasChart, {
            /**
             * Create a new DrawingCanvasChart (to be called from desktop or tree)
             * @param {} callback
             */
            create : function(entity) {
                Viz.openFormCrud(Viz.grid.DrawingCanvasChart.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing DrawingCanvasChart (to be called from desktop or tree)
             * @param {} callback
             */
            update : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.DrawingCanvasChart.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
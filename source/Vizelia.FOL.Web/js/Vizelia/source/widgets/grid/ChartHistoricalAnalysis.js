﻿/**
 * @class Viz.grid.ChartHistoricalAnalysis
 * <p>
 * A grid exposing the business entity ChartHistoricalAnalysis
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.ChartHistoricalAnalysis = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_charthistoricalanalysis_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('ChartHistoricalAnalysis_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_charthistoricalanalysis_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('ChartHistoricalAnalysis_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_charthistoricalanalysis_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('ChartHistoricalAnalysis_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : config.store || new Viz.store.ChartHistoricalAnalysis({
                                KeyChart : config.KeyChart
                            }),
                    columns      : Viz.Configuration.Columns.ChartHistoricalAnalysis
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.ChartHistoricalAnalysis.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("ChartHistoricalAnalysisChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("ChartHistoricalAnalysisChange", this.reloadStore, this);
                Viz.grid.ChartHistoricalAnalysis.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig     : function() {
                return {
                    common      : {
                        width            : 700,
                        height           : 300,
                        xtype            : 'vizFormChartHistoricalAnalysis',
                        messageSaveStart : 'ChartHistoricalAnalysisChangeStart',
                        messageSaveEnd   : 'ChartHistoricalAnalysisChange'
                    },
                    create      : {
                        title   : $lang("msg_charthistoricalanalysis_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_charthistoricalanalysis_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'Name',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyChartHistoricalAnalysis'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.ChartHistoricalAnalysis_FormUpdateBatch
                    }
                };
            },
            /**
             * @private Handler for updating the entity with the parent KeyChart.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyChart = this.KeyChart;
                if (KeyChart == '' || KeyChart == null) {
                    return false;
                }
                entity.KeyChart = KeyChart;
                return true;
            },

            /**
             * Handler for delete event.
             * @param {} selections
             */
            onDeleteRecord    : function(selections) {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;

                var msgDelete;
                if (selections.length > 1)
                    msgDelete = String.format($lang("msg_record_delete_confirmation_multiple"), selections.length);
                else
                    msgDelete = $lang("msg_record_delete_confirmation_single");

                Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                            if (button == 'yes') {
                                for (var i = 0, r; r = selections[i]; i++) {
                                    this.store.remove(r);
                                }
                                this.store.save();
                                Viz.Services.EnergyWCF.ChartHistoricalAnalysis_SaveStore({
                                            store   : this.store.crudStore,
                                            success : function() {
                                                if (Ext.isArray(selections)) {
                                                    for (var i = 0, r; r = selections[i]; i++) {
                                                        Viz.util.MessageBusMgr.fireEvent('ChartHistoricalAnalysisChange', r.data);
                                                    }
                                                }
                                                else {
                                                    Viz.util.MessageBusMgr.fireEvent('ChartHistoricalAnalysisChange', selections);
                                                }
                                            },
                                            scope   : this
                                        });
                            }
                        }, this);

            }

        });

Ext.reg("vizGridChartHistoricalAnalysis", Viz.grid.ChartHistoricalAnalysis);

// Static Methods
Ext.apply(Viz.grid.ChartHistoricalAnalysis, {

            /**
             * Create a new Chart (to be called from desktop or tree)
             * @param {} callback
             */
            create : function(entity) {
                Viz.openFormCrud(Viz.grid.ChartHistoricalAnalysis.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing Chart (to be called from desktop or tree) *
             * @param {Chart} entity
             * @param {Ext.Window} window
             * @param {object} extraConfig
             */
            update : function(entity, window, extraConfig) {
                if (entity) {
                    var conf = Viz.grid.ChartHistoricalAnalysis.prototype.getFormConfig();
                    if (Ext.isObject(extraConfig))
                        Ext.apply(conf.common, extraConfig);
                    if (window) {
                        var box = window.getBox();
                        conf.common.x = box.width + box.x;
                        conf.common.y = box.y;
                    }
                    Viz.openFormCrud(conf, 'update', entity);
                }
            }
        });

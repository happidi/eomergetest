﻿Ext.namespace('Viz.grid');
/**
 * @class Viz.grid.User
 * <p>
 * A grid exposing FOLMembership users
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.User = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor          : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    store        : new Viz.store.User(),
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_user_add"),
                                iconCls : 'viz-icon-small-user-add',
                                scope   : this,
                                handler : this.onAddRecord,
                                hidden  : !$authorized('@@@@ User management - Full Control')
                            }, {
                                text    : $lang("msg_user_update"),
                                iconCls : 'viz-icon-small-user-update',
                                scope   : this,
                                handler : this.onUpdateRecord,
                                hidden  : !$authorized('@@@@ User management - Full Control')
                            }, {
                                text    : $lang("msg_user_delete"),
                                iconCls : 'viz-icon-small-user-delete',
                                scope   : this,
                                handler : this.onDeleteRecord,
                                hidden  : !$authorized('@@@@ User management - Full Control')
                            }, {
                                text    : $lang('msg_resetpassword'),
                                iconCls : 'viz-icon-small-changepassword',
                                scope   : this,
                                handler : this.onResetPassword,
                                hidden  : !$authorized('@@@@ Change password')
                            }],
                    plugins      : [new Viz.plugins.GridExport({
                                paging          : {
                                    start : 0,
                                    sort  : 'Name',
                                    dir   : 'ASC'
                                },
                                maxRecords      : 1000,
                                promptMaxRecord : true
                            })],
                    columns      : Viz.Configuration.Columns.User,
                    formConfig   : {
                        common : {
                            width            : 530,
                            height           : 495,
                            xtype            : 'vizFormUser',
                            messageSaveStart : 'UserManagementChangeStart',
                            messageSaveEnd   : 'UserManagementChange',
                            readonly         : !$authorized('@@@@ User management - Full Control')
                        },
                        create : {
                            title   : $lang('msg_user_add'),
                            iconCls : 'viz-icon-small-user-add'
                        },
                        update : {
                            title               : $lang('msg_user_update'),
                            iconCls             : 'viz-icon-small-user-update',
                            titleEntity         : 'UserName',
                            serviceParamsEntity : [{
                                        name  : 'username',
                                        value : 'UserName'
                                    }]
                        }
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.User.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("UserManagementChange", this.reloadStore, this);
            },
            /**
             * @private
             */
            onDestroy            : function() {
                Viz.util.MessageBusMgr.unsubscribe("UserManagementChange", this.reloadStore, this);
                Viz.grid.User.superclass.onDestroy.apply(this, arguments);
            },
            // An override for the parent class function.
            deleteRecordInternal : function(selections) {
                var systemUserName = null;

                for (var i = 0, r; r = selections[i]; i++) {
                    if ((r != null && r.data != null) && (r.data.UserName == 'Admin' || r.data.UserName == 'Scheduler' || r.data.UserName == 'SystemMonitor')) {
                        systemUserName = r.data.UserName;
                        break;
                    }
                }

                if (systemUserName != null) {
                    Ext.MessageBox.confirm($lang('msg_application_title'), '<span style="color:red;font-weight:bold;">' + systemUserName + ' ' + $lang('msg_deleting_system_user') + '</span>', function(button) {
                                if (button == 'yes') {
                                    Viz.grid.User.superclass.deleteRecordInternal.call(this, selections);
                                }
                            }, this);
                }
                else {
                    Viz.grid.User.superclass.deleteRecordInternal.call(this, selections);
                }
            },
            /**
             * Handler for the reset password button.
             */
            onResetPassword      : function() {
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var username = record.get("UserName");
                    var applicationName = record.get("ApplicationName");
                    Ext.MessageBox.confirm($lang('msg_application_title'), String.format($lang('msg_resetpassword_confirmation'), username), function(button) {
                                if (button == 'yes') {
                                    Viz.Services.AuthenticationWCF.User_ResetPassword({
                                                username        : username,
                                                applicationName : applicationName,
                                                notify          : true,
                                                success         : function(value) {
                                                    Ext.MessageBox.show({
                                                                title   : $lang('msg_application_title'),
                                                                msg     : $lang("msg_changepassword_successful"),
                                                                width   : 350,
                                                                icon    : Ext.MessageBox.INFO,
                                                                buttons : Ext.Msg.OK
                                                            });
                                                }
                                            });
                                }
                            }, this);
                }
            }
        });
Ext.reg("vizGridUser", Viz.grid.User);
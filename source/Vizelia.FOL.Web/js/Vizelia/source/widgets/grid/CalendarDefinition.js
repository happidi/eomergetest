﻿/**
 * @class Viz.grid.CalendarDefinition
 * <p>
 * A grid exposing the business entity CalendarDefinition
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.CalendarDefinition = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor         : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : config.Category == 'Holiday' ? $lang("msg_calendar_holiday_add") : $lang("msg_calendar_extra_add"),
                                iconCls : 'viz-icon-small-calendarevent-add',
                                // hidden : !$authorized('CalendarDefinition_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : config.Category == 'Holiday' ? $lang("msg_calendar_holiday_update") : $lang("msg_calendar_extra_update"),
                                iconCls : 'viz-icon-small-calendarevent-update',
                                // hidden : !$authorized('CalendarDefinition_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : config.Category == 'Holiday' ? $lang("msg_calendar_holiday_delete") : $lang("msg_calendar_extra_delete"),
                                iconCls : 'viz-icon-small-calendarevent-delete',
                                // hidden : !$authorized('CalendarDefinition_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_calendarevent_copy"),
                                iconCls : 'viz-icon-small-copy',
                                // hidden : !$authorized('@@@@ Dynamic Display - Full Control'),
                                scope   : this,
                                handler : this.onCopyRecord
                            }, {
                                xtype : 'tbseparator'
                            }, {
                                id           : 'timezone-view-local',
                                iconCls      : 'viz-icon-small-world',
                                enableToggle : true,
                                text         : $lang('Timezone'),
                                handler      : this.viewOnLocalCalendar,
                                scope        : this
                            }, {
                                text    : $lang("msg_calendar_show"),
                                iconCls : 'viz-icon-small-calendar',
                                // hidden : !$authorized('CalendarDefinition_write'),
                                scope   : this,
                                handler : this.onShowCalendar
                            }],
                    formConfig   : {
                        common      : {
                            width               : 610,
                            height              : 480,
                            xtype               : 'vizFormCalendarDefinition',
                            messageSaveStart    : 'CalendarDefinitionChangeStart',
                            messageSaveEnd      : 'CalendarDefinitionChange',
                            serviceParamsSimple : [{
                                        name  : 'KeyLocation',
                                        value : config.KeyLocation
                                    }, {
                                        name  : 'Category',
                                        value : config.Category
                                    }]
                        },
                        create      : {
                            title   : config.Category == 'Holiday' ? $lang("msg_calendar_holiday_add") : $lang("msg_calendar_extra_add"),
                            iconCls : 'viz-icon-small-calendarevent-add'
                        },
                        update      : {
                            title               : config.Category == 'Holiday' ? $lang("msg_calendar_holiday_update") : $lang("msg_calendar_extra_update"),
                            iconCls             : 'viz-icon-small-calendarevent-update',
                            titleEntity         : 'Title',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'Key'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-calendarevent-update',
                            serviceHandlerUpdateBatch : Viz.Services.CoreWCF.CalendarEvent_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.CalendarDefinition({
                                KeyLocation : config.KeyLocation,
                                Category    : config.Category
                            }),
                    columns      : Viz.Configuration.Columns.CalendarEvent,
                    viewConfig   : {
                        forceFit    : true,
                        // Return CSS class to apply to rows representing exception dates.
                        getRowClass : function(record, index) {
                            var related = record.get('RelatedTo');
                            if (related != '') {
                                return 'viz-calendar-exdate';
                            }
                            else {
                                return '';
                            }
                        }

                    }
                };

                defaultConfig.store.on('load', function() {
                            var button = this.getTopToolbar().find('id', 'timezone-view-local')[0];
                            if (button)
                                button.toggle(false);
                        }, this);
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.CalendarDefinition.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("SpatialCalendarClick", this.reloadCalendarStore, this);
                Viz.util.MessageBusMgr.subscribe("CalendarDefinitionChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("SpatialCalendarClick", this.reloadCalendarStore, this);
                Viz.util.MessageBusMgr.unsubscribe("CalendarDefinitionChange", this.reloadStore, this);
                Viz.grid.CalendarDefinition.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * Reload a store for the selected location
             */
            reloadCalendarStore : function(node) {
                if (this.findParentByType('tabpanel').activeTab.id != this.ownerCt.id)
                    return;
                this.formConfig.common.serviceParamsSimple[0].value = node.attributes.Key;
                this.KeyLocation = node.attributes.Key;
                this.entity = node.attributes.entity;
                this.store.serviceParams.KeyLocation = node.attributes.Key;
                this.store.load();
                this.findParentByType('panel').setTitle($lang('msg_calendar') + ' ' + this.CategoryLabel + ' : ' + node.parentNode.text + ' / ' + node.text);
            },
            /**
             * Opens the calendar for a this location.
             */
            onShowCalendar      : function() {
                Viz.App.Global.ViewPort.openModule({
                            title       : $lang('msg_calendar'), // $lang('msg_calendar') + ' : ' + node.parentNode.text + ' / ' + node.text,
                            KeyLocation : this.KeyLocation,
                            xtype       : 'vizPanelCalendarLocation'
                        });

            },
            viewOnLocalCalendar : function(btn) {
                if (btn.pressed) {
                    this.store.each(function(r) {
                                r.data.StartDate = Viz.date.convertToLocalDate(r.data.StartDate, r.data['TimeZoneOffset'][0]);
                                r.data.EndDate = Viz.date.convertToLocalDate(r.data.EndDate, r.data['TimeZoneOffset'][1]);
                            }, this);

                    this.view.refresh(true);
                }
                else {
                    this.store.load();
                }
            },

            /**
             * @private Handler for copying a calendar event.
             */
            onCopyRecord        : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;

                var keys = new Array();
                for (var i = 0, r; r = selections[i]; i++) {
                    keys.push([r.data.Key, r.data.KeyLocation]);
                }
                this.el.mask();
                Viz.Services.CoreWCF.CalendarEvent_Copy({
                            keys    : keys,
                            success : function(data) {
                                this.el.unmask();
                                this.reloadStore();
                            },
                            scope   : this
                        });
            }

        });

Ext.reg("vizGridCalendarDefinition", Viz.grid.CalendarDefinition);

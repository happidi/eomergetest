﻿/**
 * @class Viz.grid.Localization
 * <p>
 * A grid exposing the business entity Localization
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Localization = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor     : function(config) {

                this._comboCultureId = Ext.id();
                this.storeLocalization = new Viz.store.Localization();

                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    store        : this.storeLocalization,
                    columns      : Viz.Configuration.Columns.Localization,
                    hasToolbar   : false,
                    tbar         : [$lang('msg_localization_language') + ':', {
                                id         : this._comboCultureId,
                                xtype      : 'vizComboLocalizationCulture',
                                width      : 250,
                                listeners  : {
                                    setvalue : this.onSearch,
                                    scope    : this

                                },
                                allowBlank : true
                            }, {
                                text    : $lang("msg_add"),
                                iconCls : 'viz-icon-small-localization-add',
                                hidden  : !$authorized('@@@@ Language Managment - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_update"),
                                iconCls : 'viz-icon-small-localization-update',
                                hidden  : !$authorized('@@@@ Language Managment - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {

                                text    : $lang("msg_delete"),
                                iconCls : 'viz-icon-small-localization-delete',
                                hidden  : !$authorized('@@@@ Language Managment - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common : {
                            width            : 500,
                            height           : 200,
                            xtype            : 'vizFormLocalization',
                            messageSaveStart : 'LocalizationStart',
                            messageSaveEnd   : 'LocalizationChange',
                            readonly         : !$authorized('@@@@ Language Managment - Full Control')
                        },
                        create : {
                            title   : $lang("msg_add"),
                            iconCls : 'viz-icon-small-localization-add'

                        },
                        update : {
                            title               : $lang("msg_update"),
                            iconCls             : 'viz-icon-small-localization-update',
                            titleEntity         : 'Key',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'Key'
                                    }]
                        }
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.Localization.superclass.constructor.call(this, config);
                this.on("beforeupdaterecord", function(grid, record, entity) {
                            var language = this.getComboCulture().getValue();
                            Ext.apply(entity, {
                                        language : language
                                    })
                        }, this);
                this.on("beforeaddrecord", function(grid, entity) {
                            var language = this.getComboCulture().getValue();
                            Ext.apply(entity, {
                                        language : language
                                    })
                        }, this);

                Viz.util.MessageBusMgr.subscribe("LocalizationChange", this.reloadStore, this);
            },

            onDestroy       : function() {
                Viz.util.MessageBusMgr.unsubscribe("LocalizationChange", this.reloadStore, this);
                Viz.grid.Localization.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Gets the combo culture.
             * @return {Viz.combo.Culture}
             */
            getComboCulture : function() {
                return Ext.getCmp(this._comboCultureId);
            },

            /**
             * Starts a search request.
             */
            onSearch        : function() {
                var language = this.getComboCulture().getValue();
                var serviceParams = {
                    language : language
                };
                this.storeLocalization.proxy.serviceParams = serviceParams;
                this.storeLocalization.serviceParams = this.storeLocalization.proxy.serviceParams;
                // very important in order to keep the filter when navigating through the paging toolbar.
                // this.storeOccupant.baseParams = p;
                this.storeLocalization.load({
                            scope  : this,
                            params : {
                                start : 0,
                                limit : this.pageSize
                            }
                        });
            }

        });

Ext.reg("vizGridLocalization", Viz.grid.Localization);

﻿/**
 * @class Viz.grid.ApplicationGroup
 * <p>
 * A grid exposing the business entity ApplicationGroup
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.ApplicationGroup = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    store        : new Viz.store.ApplicationGroup(),
                    columns      : Viz.Configuration.Columns.AzManItem,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_applicationgroup_add"),
                                iconCls : 'viz-icon-small-applicationgroup-add',
                                hidden  : !$authorized('@@@@ Application Groups - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_applicationgroup_update"),
                                iconCls : 'viz-icon-small-applicationgroup-update',
                                hidden  : !$authorized('@@@@ Application Groups - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {

                                text    : $lang("msg_applicationgroup_delete"),
                                iconCls : 'viz-icon-small-applicationgroup-delete',
                                hidden  : !$authorized('@@@@ Application Groups - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common : {
                            width            : 500,
                            height           : 350,
                            xtype            : 'vizFormApplicationGroup',
                            messageSaveStart : 'ApplicationGroupChangeStart',
                            messageSaveEnd   : 'ApplicationGroupChange'
                        },
                        create : {
                            title   : $lang("msg_applicationgroup_add"),
                            iconCls : 'viz-icon-small-applicationgroup-add'

                        },
                        update : {
                            title               : $lang("msg_applicationgroup_update"),
                            iconCls             : 'viz-icon-small-applicationgroup-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'Id'
                                    }]
                        }
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.ApplicationGroup.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("ApplicationGroupChange", this.reloadStore, this);
            },

            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("ApplicationGroupChange", this.reloadStore, this);
                Viz.grid.ApplicationGroup.superclass.onDestroy.apply(this, arguments);
            }

        });

Ext.reg("vizGridApplicationGroup", Viz.grid.ApplicationGroup);

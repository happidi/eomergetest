﻿/**
 * @class Viz.grid.MeterOperation
 * <p>
 * A grid exposing the business entity MeterOperation
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.MeterOperation = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    pageSize     : 100,
                    tbar         : [{
                                text    : $lang("msg_meteroperation_add"),
                                iconCls : 'viz-icon-small-meteroperation-add',
                                // hidden : !$authorized('MeterOperation_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_meteroperation_update"),
                                iconCls : 'viz-icon-small-meteroperation-update',
                                // hidden : !$authorized('MeterOperation_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_meteroperation_delete"),
                                iconCls : 'viz-icon-small-meteroperation-delete',
                                // hidden : !$authorized('MeterOperation_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 800,
                            height           : 300,
                            xtype            : 'vizFormMeterOperation',
                            messageSaveStart : 'MeterOperationChangeStart',
                            messageSaveEnd   : 'MeterOperationChange'
                        },
                        create      : {
                            title   : $lang("msg_meteroperation_add"),
                            iconCls : 'viz-icon-small-meteroperation-add'
                        },
                        update      : {
                            title               : $lang("msg_meteroperation_update"),
                            iconCls             : 'viz-icon-small-meteroperation-update',
                            titleEntity         : 'LocalId',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyMeterOperation'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-meteroperation-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.MeterOperation_FormUpdateBatch
                        }
                    },
                    store        : config.store || new Viz.store.MeterOperation({
                                KeyMeter : config.KeyMeter
                            }),
                    columns      : Viz.Configuration.Columns.MeterOperation
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.MeterOperation.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("MeterOperationChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("MeterOperationChange", this.reloadStore, this);
                Viz.grid.MeterOperation.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyMeter.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyMeter = this.KeyMeter;
                if (KeyMeter == '' || KeyMeter == null) {
                    return false;
                }
                entity.KeyMeter = KeyMeter;
                entity.Order = this.getMaxValueFromStore('Order') + 1;
                return true;
            }

        });

Ext.reg("vizGridMeterOperation", Viz.grid.MeterOperation);
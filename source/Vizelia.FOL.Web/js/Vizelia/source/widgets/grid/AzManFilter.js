﻿/**
 * @class Viz.grid.AzManTask
 * <p>
 * A grid exposing the business entity AzManItem Filter
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.AzManFilter = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor      : function(config) {
                this._fieldLocation = Ext.id();
                this._keyLocation = config.entity && config.entity.id;
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                        id                     : this._fieldLocation,
                        fieldLabel             : $lang('msg_location_path'),
                        xtype                  : 'viztreecomboSpatial',
                        validator              : Ext.form.VTypes.locationValidator,
                        name                   : 'KeyLocation',
                        width                  : 500,
                        originalDisplayValue   : 'LocationLongPath',
                        ExcludeMetersAndAlarms : true,
                        allowBlank             : true,
                        stateful               : true,
                        listeners              : {
                            select : {
                                fn    : this.onSelectLocation,
                                scope : this
                            },
                            clear  : {
                                fn    : this.onClearCombo,
                                scope : this
                            },
                            load   : {
                                fn    : this.onLoadCombo,
                                scope : this
                            },
                            scope  : this
                        }
                            /*
                             * }, { text: $lang("msg_azmanfilter_add"), iconCls: 'viz-icon-small-filter-add', hidden: !$authorized('@@@@ Project Hierarchy - Full Control'), scope: this, handler: this.onAddRecord }, { text: $lang("msg_azmanfilter_update"), iconCls: 'viz-icon-small-filter-update', hidden: !$authorized('@@@@ Project Hierarchy - Full Control'), scope: this, handler: this.onUpdateRecord }, { text: $lang("msg_azmanfilter_delete"), iconCls: 'viz-icon-small-filter-delete', hidden: !$authorized('@@@@ Project Hierarchy - Full Control'), scope: this, handler: this.onDeleteRecord
                             */
                        }],
                    formConfig   : {
                        common : {
                            width            : 500,
                            height           : 400,
                            xtype            : 'vizFormAzManFilter',
                            messageSaveStart : 'AzManFilterChangeStart',
                            messageSaveEnd   : 'AzManFilterChange',
                            readonly         : !$authorized('@@@@ Project Hierarchy - Full Control')
                        },
                        create : {
                            title   : $lang("msg_azmanfilter_add"),
                            iconCls : 'viz-icon-small-filter-add'
                        },
                        update : {
                            title               : $lang("msg_azmanfilter_update"),
                            iconCls             : 'viz-icon-small-filter-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'Id'
                                    }]
                        }
                    },
                    store        : new Viz.store.AzManFilterByLocation(),
                    plugins      : [new Viz.plugins.GridExport({
                                paging          : {
                                    start : 0,
                                    sort  : 'Name',
                                    dir   : 'ASC'
                                },
                                maxRecords      : 1000,
                                promptMaxRecord : true
                            })],
                    columns      : Viz.Configuration.Columns.AzManFilter

                };
                var forcedConfig = {
                    auditEntityName : 'AuthorizationItem.AzManFilter'
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.AzManFilter.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("AzManFilterChange", this.reloadStore, this);
            },

            onSelectLocation : function(combo, record) {
                this.selectLocation(record.id);
            },

            onClearCombo     : function() {
                this.selectLocation(null);
            },

            onLoadCombo      : function() {
                if (!this._keyLocation)
                    return;

                var spatialCombo = Ext.getCmp(this._fieldLocation);
                var keyLocation = this._keyLocation;
                this.selectLocation(keyLocation);

                Viz.Services.CoreWCF.Location_GetItem({
                            Key     : keyLocation,
                            success : function(data) {
                                spatialCombo.getTree().selectPath(" / Locations / " + data.LongPath, "text", null);
                                spatialCombo.setValue(keyLocation, true);
                            }
                        });
            },

            selectLocation   : function(keyLocation) {
                var serviceParams = {
                    location    : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                    keyLocation : keyLocation || ""
                };
                var params = {
                    start : 0,
                    limit : this.pageSize
                };
                var storeAzManFilter = this.getStore();
                storeAzManFilter.proxy.serviceParams = serviceParams;
                storeAzManFilter.serviceParams = storeAzManFilter.proxy.serviceParams;
                storeAzManFilter.load({
                            params   : params,
                            callback : function() {
                            }
                        });
            },

            /**
             * Destroy
             */
            onDestroy        : function() {
                Viz.util.MessageBusMgr.unsubscribe("AzManFilterChange", this.reloadStore, this);
                Viz.grid.AzManFilter.superclass.onDestroy.apply(this, arguments);
            }
        });

Ext.reg("vizGridAzManFilter", Viz.grid.AzManFilter);

// Static Methods
Ext.apply(Viz.grid.AzManFilter, {
            displayAzManFilter : function(entity) {
                if (Ext.isObject(entity)) {
                    Viz.App.Global.ViewPort.openModule({
                                xtype  : 'vizPanelAzManFilter',
                                entity : entity
                            });
                }
            }
        });
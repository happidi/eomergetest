﻿Ext.namespace("Viz.grid");
/**
 * @class Viz.grid.GridPanel
 * @extends Ext.grid.GridPanel
 * <p>
 * An extended GridPanel with PagingToolbar and Crud buttons.
 * </p>
 * <p>
 * <u>To add a numbered column :</u>
 * <p class="sub-desc">
 * Use the configuration option hasRowNumber (default to <tt>true</tt>).
 * </p>
 * </p>
 * <p>
 * <u>To render a light grid :</u>
 * <p class="sub-desc">
 * Use the following settings
 * 
 * <pre>
 * <code>
 * {hasRowEditor : false,
 * hasToolbar : false,
 * hideHeaders : true,
 * cls : 'viz-grid-norowborder',
 * stripeRows : false}
 * </code></pre>
 * 
 * </ul>
 * </p>
 * </p>
 * <p>
 * <u>To display an icon in a column :</u>
 * <p class="sub-desc">
 * Use the following renderer in the column configuration
 * 
 * <pre><code>
 *  renderer  : function(data, cell, record, rowIndex, colIndex, store) {
 *  cell.css = 'viz-icon-cell ' + 'viz-icon-small-user';
 *  return data;
 *  }
 * </code></pre>
 * 
 * </p>
 * </p>
 * <p>
 * <u>To mark a row with a specific class :</u>
 * <p class="sub-desc">
 * Use the following code
 * 
 * <pre><code>
 * Ext.fly(grid.getView().getRow(0).addClass('error'));
 * </code></pre>
 * 
 * </p>
 * <p>
 * <u>To get access to lower json data not exposed by the reader of the store :</u>
 * <p class="sub-desc">
 * use the following configuration
 * 
 * <pre><code>
 * {
 * 	header     : $lang('msg_occupant_fullname'),
 *  dataIndex  : '',
 *  filterable : true,
 *  sortable   : true,
 *  renderer   : function(data, cell, record, rowIndex, colIndex, store) {
 *  	return record.json.ProviderUserKey.Occupant.FullName;
 *  	}
 *  }
 * </code></pre>
 * 
 * </p>
 * </p>
 * </p>
 * <p>
 * The grid can automatically manage the opening of crud forms. All the configuration is contained in a formConfig element
 * 
 * <pre><code>
 * formConfig = {
 *     common : {
 *         width            : 500,
 *         height           : 350,
 *         xtype            : 'vizFormPsetList',
 *         messageSaveStart : 'PsetListChangeStart',
 *         messageSaveEnd   : 'PsetListChange'
 *     },
 *     create : {
 *         title   : $lang(&quot;msg_add&quot;),
 *         iconCls : 'viz-icon-small-list-add'
 *     },
 *     update : {
 *         title       : $lang(&quot;msg_update&quot;),
 *         iconCls     : 'viz-icon-small-list-update',
 *         titleEntity : 'Label'
 *     }
 * };
 * </pre></code> The configuration (usually the update node) can take advantage of the following elements especially if the form is in mode remote and has a FormLoad handler:
 * <p>
 * serviceParamsSimple : an array of {name,value} pairs that should be passed to the FormLoad handler
 * </p>
 * <p>
 * serviceParamsEntity : an array of {name,value} pairs to do the mapping between the entity and the expected arguments of the FormLoad handler.
 * </p>
 * </p>
 * @xtype vizGrid
 */
Viz.grid.GridPanel = Ext.extend(Ext.grid.GridPanel, {
    loadMask                : false,
    pageSize                : Viz.Const.UI.DefaultGridPageSize,
    stripeRows              : false,
    border                  : false,
    /**
     * @cfg {Boolean} hasPagingToolbar <tt>true</tt> to show a paging toolbar, <tt>false</tt> otherwise. (default to <tt>true</tt>).
     */
    hasPagingToolbar        : true,
    /**
     * @cfg {Boolean} hasRowEditor <tt>true</tt> to add a RowEditor, <tt>false</tt> otherwise. (default to <tt>true</tt>).
     */
    hasRowEditor            : false,
    /**
     * @cfg {Boolean} hasToolbar <tt>true</tt> to add a toolbar with add, update and delete buttons, <tt>false</tt> otherwise. (default to <tt>true</tt>).
     */
    hasToolbar              : false,
    /**
     * @cfg {Boolean} hasTooltip <tt>true</tt> to display a tooltip for cells, <tt>false</tt> otherwise. (defaults to <tt>true</tt>).
     */
    hasTooltip              : true,
    autoLoadStore           : true,
    /**
     * @cfg {Boolean} hasRowNumber <tt>true</tt> to add a row numbered column.
     */
    hasRowNumber            : true,
    /**
     * @cfg {Boolean} hasRowExpander <tt>true</tt> to add a row expander column.(with rowExpanderTemplate)
     */
    hasRowExpander          : false,

    /**
     * @cfg {Ext.Template} rowExpanderTemplate the template to use with the rowExpanderPlugin
     * @type Ext.Template
     */
    rowExpanderTemplate     : null,

    enableColumnHide        : true,
    /**
     * @cfg {Boolean} enableRowDblClick <tt>true</tt> to enable opening update form when double clicking on a row, <tt>false otherwise</tt>. (defaults to <tt>true</tt>).
     */
    enableRowDblClick       : true,
    /**
     * @cfg {Boolean} hasFilter <tt>true</tt> to add a grid filter plugin, <tt>false</tt> otherwise.
     */
    hasFilter               : true,

    /**
     * @cfg {Boolean} enableDropFromTree <tt>true</tt> to enable the droping of a tree node to the grid.
     */
    enableDropFromTree      : false,
    /**
     * @config {Object} formConfig The configuration of the form crud. Usually this object config contains 4 sections : common , create , update, updatebatch (optional). The values defined in common are applied unless they are overrided in create or delete.
     * 
     * <pre><code>
     * formConfig : {
     *     common : {
     *         width  : 300,
     *         height : 300,
     *         xtype  : 'vizFormXXX',
     *         messageSaveStart : 'UserManagementChangeStart',
     *         messageSaveEnd : 'UserManagementChange'
     *     },
     *     create : {
     *         title : 'Add'
     *     },
     *     update : {
     *         title               : 'Update',
     *         titleEntity         : 'UserName', // the attribute of the entity that should be used in the title of the window in the case of an update
     *         serviceParamsSimple : [{
     *                     name  : name,
     *                     value : value
     *                 }],
     *         serviceParamsEntity : [{
     *                     name  : name,
     *                     value : value
     *                 }]
     *     },
     *     updatebatch : {
     *     		title : 'Update batch',
     *     		serviceHandlerUpdateBatch : Viz....
     *     }
     * }
     * </code></pre>
     */

    /**
     * @cfg {Boolean} formModal <tt>true</tt> to display the form crud as modal, <tt>false</tt> otherwise. (defaults to <tt>true</tt>).
     * @type Boolean
     */
    formModal               : true,

    /**
     * @cfg {Boolean} filterRemote <tt>true</tt> to filter on server, <tt>false</tt> otherwise. (defaults to <tt>true</tt>).
     * @type Boolean
     */
    filterRemote            : true,

    /**
     * @cfg {Boolean} stateful <tt>true</tt> to have a stateful grid, <tt>false</tt> otherwise. (defaults to <tt>true</tt>).
     * @type Boolean
     */
    stateful                : true,
    /**
     * Ctor.
     * @param {Object} config
     */
    /**
     * @cfg {Number} autoReloadInterval : if the value is >0 then the store will be automatically reloaded every X seconds.
     * @type Number
     */
    autoReloadInterval      : 0,
    /**
     * @cfg {Boolean} readonly Indicates whether the form should be readonly. Default to <tt>false</tt>
     */
    readonly                : false,
    /**
     * @cfg {Boolean} alloweditbyclick Allows to edit a row by dbl click even when the grid is readonly whether the form should be readonly. Default to <tt>false</tt>
     */
    alloweditbyclick        : false,

    allowupdatebatch        : false,

    invalidClass            : 'x-grid-invalid',

    invalidText             : '',

    allowEmpty              : true,

    constructor             : function(config) {
        config = config || {};
        var defaultConfig = {
            viewConfig : {
                forceFit  : true,
                markDirty : false,
                emptyText : Ext.PagingToolbar.prototype.emptyMsg
            }
        };
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.applyIf(config, this);
        Ext.apply(config, forcedConfig);
        this._buttonDeleteId = Ext.id();
        this._filterButtonId = Ext.id();
        this._pagingId = Ext.id();
        this._auditGridId = Ext.id();
        // very important change in order to avoid change in static column definition that will corrupt it for further use.
        if (config.columns && !config.skipCloningColumns) {
            config.columns = Viz.clone(config.columns);
            this.originalColumnConfig = Viz.clone(config.columns);
        }

        if (!config.stateId && config.stateful !== false)
            config.stateId = config.xtype;

        this.pageSize = Viz.App.Global.User.Preferences.GridsPageSize;
        config.pageSize = this.pageSize;

        if (config.hasPagingToolbar) {
            var pagingConfig = {
                xtype          : 'paging',
                id             : this._pagingId,
                displayInfo    : true,
                enableOverflow : true,
                pageSize       : config.pageSize,
                store          : config.store
            };
            if (config.bbar) {
                config.bbar.splice(0, 0, pagingConfig);
            }
            else
                config.bbar = pagingConfig;
        }
        if (config.hasRowEditor) {
            this.editor = this.createRowEditor();
            if (Ext.isArray(config.plugins))
                config.plugins.push(this.editor);
            else
                config.plugins = [this.editor];
        }

        if (config.hasRowNumber) {
            var newColumns = [new Viz.grid.PagingRowNumberer()];
            config.columns = newColumns.concat(config.columns);
        }

        if (config.hasRowExpander && Ext.isDefined(config.rowExpanderTemplate)) {
            this.expander = new Ext.ux.grid.RowExpander({
                        expandOnDblClick : false,
                        tpl              : config.rowExpanderTemplate
                    });
            config.columns = [this.expander].concat(config.columns);
            if (config.plugins) {
                config.plugins = [].concat(config.plugins);
                config.plugins.push(this.expander);
            }
            else
                config.plugins = [this.expander];
        }

        if (Ext.isArray(config.columns)) {
            Ext.each(config.columns, function(col) {
                        if (!Ext.isEmpty(col.header) && Ext.isEmpty(col.tooltip)) {
                            col.tooltip = Ext.util.Format.htmlEncode(col.header);
                        }
                    }, this);
        }

        try {
            var curState = Ext.state.Manager.get(config.stateId || this.stateId);
        }
        catch (e) {
        }

        if (config.hasFilter) {
            var tbarFilter = ['->', {
                        id      : this._filterButtonId,
                        xtype   : 'button',
                        text    : (curState && curState.savedView && this.getFilterTitle(curState.savedView, curState.savedViewGlobal, true)) || (curState && curState.showAllItems && $lang('msg_gridfilter_clear')) || $lang('msg_gridfilter_custom'),
                        iconCls : 'viz-icon-small-gridfilter',
                        scope   : this,
                        menu    : [{
                                    text    : $lang('msg_gridfilter_clear'),
                                    iconCls : 'viz-icon-small-gridfilter-load',
                                    hidden  : !config.stateId,
                                    handler : this.onGridFilterClear,
                                    scope   : this
                                }, {
                                    text    : $lang('msg_gridfilter_save_global'),
                                    iconCls : 'viz-icon-small-gridfilter-save',
                                    hidden  : !config.stateId || !$authorized('@@@@ User Profile Preference - Full Control'),
                                    handler : function() {
                                        this.onGridFilterSave(true);
                                    }.createDelegate(this),
                                    scope   : this
                                }, {
                                    text    : $lang('msg_gridfilter_save'),
                                    iconCls : 'viz-icon-small-gridfilter-save',
                                    hidden  : !config.stateId,
                                    handler : function() {
                                        this.onGridFilterSave(false);
                                    }.createDelegate(this),
                                    scope   : this
                                }/*
                                     * , { text: $lang('msg_grid _load'), iconCls: 'viz-icon-small-gridfilter-load', hidden: !config.stateId, handler: this.onGridFilterLoad, scope: this }
                                     */]
                    }];

            this.filters = new Ext.ux.grid.GridFilters({
                        // encode and local configuration options defined previously for easier reuse
                        encode       : true, // json encode the filter query
                        local        : !config.filterRemote, // defaults to false (remote filtering)
                        paramPrefix  : 'filters',
                        updateBuffer : 800
                    });
            if (config.plugins) {
                config.plugins = [].concat(config.plugins);
                config.plugins.push(this.filters);
            }
            else
                config.plugins = [this.filters];
            if (config.bbar) {
                if (config.bbar.plugins) {
                    config.bbar.plugins = [].concat(config.bbar.plugins);
                    config.bbar.plugins.push(this.filters);
                }
                else {
                    config.bbar.plugins = [this.filters];
                }

            }

            if (config.tbar) {
                config.tbar = [].concat(config.tbar, tbarFilter);
            }
            else {
                config.tbar = tbarFilter;
            }
        }

        // we make sure we have a place holder for the top toolbar even if no tbar was present in initial config.
        if (this.hasRowEditor || this.hasToolbar) {
            this.tbar = this.tbar || {};
        }
        else if (config.tbar == null) {
            delete config.tbar;
        }

        // enable overflow
        if (Ext.isArray(config.tbar)) {
            config.tbar = {
                enableOverflow : true,
                items          : config.tbar
            }
        }
        else {
            Ext.apply(config.tbar, {
                        enableOverflow : true
                    });
        }

        Viz.grid.GridPanel.superclass.constructor.call(this, config);
        this.addEvents(
                /**
                 * @event beforeupdaterecord Fires before the update action on a record.
                 * @param {Viz.grid.GridPanel} this.
                 * @param {Object} record The record that is being updated.
                 * @param {Object} entity The entity that is being updated. It could be changed by the handler.
                 */
                'beforeupdaterecord',
                /**
                 * @event beforeupdatebactchrecords Fires before the update bacth action on records.
                 * @param {Viz.grid.GridPanel} this.
                 * @param {Object} records The collection of records that are being updated.
                 * @param {Object} entity The entity that will be used to intially fill the form. It could be changed by the handler.
                 */
                'beforeupdatebatchrecords',
                /**
                 * @event beforeaddrecord Fires before the add action on a record.
                 * @param {Viz.grid.GridPanel} this.
                 * @param {Object} entity The entity that is being added.
                 */
                'beforeaddrecord',
                /**
                 * @event beforedeleterecord Fires before the delete action on a record.
                 * @param {Viz.grid.GridPanel} this.
                 * @param {Array} records The array of records that are being deleted. It could be changed by the handler.
                 */
                'beforedeleterecord',
                /**
                 * @event deleterecord Fires before the delete action on a record.
                 * @param {Viz.grid.GridPanel} this.
                 * @param {Array} selections The array of records that are being deleted.
                 */
                'deleterecord',
                /**
                 * @event beforecopyrecord Fires before the copy action on a record.
                 * @param {Viz.grid.GridPanel} this.
                 * @param {Array} records The array of records that are being copied. It could be changed by the handler.
                 */
                'beforecopyrecord',
                /**
                 * @event filtercleared Fires when the filter grid are cleared
                 * @param {Viz.grid.GridPanel} this.
                 */
                'filtercleared',

                /**
                 * @event load Fires when the store has loaded (relay event from 'load' event of store)
                 */
                'load');

        this.on("beforeaddrecord", this.onBeforeAddRecord, this);
        this.on("beforeupdaterecord", this.onBeforeUpdateRecord, this);
        this.on("deleterecord", this.onAfterDeleteRecord, this);
        this.on("beforecopyrecord", this.onBeforeCopyRecord, this);
        if (this.hasRowEditor || this.hasToolbar) {
            var tbar = this.getTopToolbar();
            if (tbar) {
                tbar.add(this.configToolbarCrud());
            }
            else {
                // create the toolbar
                this.add(new Ext.Toolbar(this.configToolbarCrud()));
                // store a reference to the toolbar
                this.tbar = this.getTopToolbar();
            }
        }
        if (this.hasRowEditor) {
            this.getSelectionModel().on('selectionchange', function(sm) {
                        Ext.getCmp(this._buttonDeleteId).setDisabled(sm.getCount() < 1);
                    }, this);
        }
        this.on("afterrender", function(grid) {
                    var store = this.store; // grid.getStore();
                    if (this.storeBaseParams)
                        store.baseParams = this.storeBaseParams;
                    this.relayEvents(this.store, ['load']);
                    // the paging will not be sent to the server unless the serviceHandler function has a parameter of type PagingParameter paging.
                    if (this.autoLoadStore && !(store.autoLoad === true)) {

                        store.load.defer(100, store, [{
                                            params : {
                                                start : 0,
                                                limit : this.hasPagingToolbar ? this.pageSize : 0
                                            }

                                        }]);
                    }
                    if (this.hasTooltip) {
                        this.createTooltip();
                    }
                    if (this.readonly === true) {
                        this.setReadOnly(this.readonly);
                    }

                    if (this.enableDropFromTree && this.ddGroup) {
                        var gridDropTargetEl = this.id; // this.getView().el.dom.childNodes[0].childNodes[1];
                        this._gridDropTarget = new Ext.dd.DropTarget(gridDropTargetEl, {
                                    ddGroup    : this.ddGroup,
                                    copy       : false,
                                    notifyDrop : this.onDropFromTree.createDelegate(this)
                                });
                    }

                    this.loadFilterMenu();
                }, this);

        if (this.enableRowDblClick && (!this.readonly === true || this.alloweditbyclick === true)) {
            this.on("rowdblclick", function(grid, rowIndex, e) {
                        this.getSelectionModel().selectRow(rowIndex);
                        this.onUpdateRecord();
                    }, this);
        }

        if (Ext.isNumber(this.autoReloadInterval) && this.autoReloadInterval > 0) {
            this._autoReloadTask = {
                run      : this.autoReloadHandler,
                interval : 1000,
                scope    : this
            };
            Ext.TaskMgr.start(this._autoReloadTask);
        }
        this.on('load', function(store) {
                    if (this.savedView) {
                        this.savedView = this.stripBoldWrappingTag(this.savedView);
                        if (this.savedViewGlobal) {
                            this.loadSerializedFilter(this.savedView, true);
                        }
                        else {
                            this.loadSerializedFilter(this.savedView, false);
                        }
                    }
                    else if (this.showAllItems) {
                        this.onGridFilterClear();
                    }

                    // Audit Module
                    if (store && store.reader && store.reader.meta && store.reader.meta.__type) {
                        var entityName = config.auditEntityName || Viz.getEntityName(store.reader.meta.__type).replace('JsonMetaDataOf', '');
                        if ($isAuditEnabled(entityName)) {
                            var tbar = this.getTopToolbar();
                            tbar.addButton({
                                        text    : $lang("msg_audit"),
                                        iconCls : 'viz-icon-small-audit',
                                        hidden  : !$authorized('Audit - Read'),
                                        scope   : this,
                                        handler : function(button, event) {
                                            this.onShowEntitiesAudit(entityName);
                                        }
                                    });
                            tbar.doLayout();
                        }
                    }
                }, this, {
                    single : true
                });
    },

    stripBoldWrappingTag    : function(target) {
        try {
            if (Ext.util.Format.substr(target, 0, 3) === '<b>' && Ext.util.Format.substr(target, target.length - 4, 4) === '</b>')
                return Ext.util.Format.substr(target, 3, target.length - 7);
        }
        catch (e) {
        }
        return target;
    },

    /**
     * Handler for the drop target when enableDropFromTree has been defined.
     * @param {} ddSource
     * @param {} e
     * @param {} data
     * @return {Boolean}
     */
    onDropFromTree          : function(ddSource, e, data) {
        var node = data.node;
        // we filter the node to be the same type as the store
        if (node && node.attributes && node.attributes.entity && this.store.reader.meta.__type == "JsonMetaDataOf" + node.attributes.entity.__type) {
            if (this.store.find(this.store.reader.meta.idProperty, node.attributes.Key || node.attributes.id) == -1) {
                this.isDirty = true;
                var form = this.getParentForm();
                if (form)
                    form.isDirty = true;
                this.store.add(new this.store.recordType(this.getEntityFromTreeNode(node)));
                return true;
            }
        }
        return false;
    },
    /**
     * Destroy
     */
    onDestroy               : function() {
        if (this._autoReloadTask) {
            Ext.TaskMgr.stop(this._autoReloadTask);
        }
        if (this._gridDropTarget)
            this._gridDropTarget.destroy();

        if (this.store && this.store.autoDestroy) {
            this.store.destroy();
        }

        Viz.grid.GridPanel.superclass.onDestroy.apply(this, arguments);
    },

    /**
     * Hide the toolbars to make the grid readonly
     * @param {} bReadOnly
     */

    setReadOnly             : function(bReadOnly) {
        if (bReadOnly)
            this.getTopToolbar().hide();
        else
            this.getTopToolbar().show();
    },

    /**
     * Publishes a mesageBus message.
     * @param {String} message The message to publish.
     * @param {Array} messageParams The parameters of the publication.
     */
    publishMessage          : function(message, messageParams) {
        var params;
        if (message) {
            params = [message];
            if (messageParams) {
                params = params.concat(messageParams);
            }
            Viz.util.MessageBusMgr.publish.apply(Viz.util.MessageBusMgr, params);
        }
    },

    /**
     * Auto reload handler that refresh the statusbar and calls the store reload every {autoReloadInterval} seconds
     */
    autoReloadHandler       : function() {
        if (!this._nextRefresh)
            this._nextRefresh = this.autoReloadInterval;
        this._nextRefresh -= 1;
        var text = String.format(' - {0} : {1} {2} ', $lang("msg_grid_nextrefresh"), this._nextRefresh, $lang("msg_seconds"));
        if (this._nextRefresh == 0) {
            text = $lang("msg_loading");
            this.store.reload();
            this._nextRefresh = this.autoReloadInterval;
        }
        var statusBar = this.getBottomToolbar();
        if (!this.statusBarText) {
            this.statusBarText = statusBar.addText(text);
        }
        else {
            this.statusBarText.setText(text);
        }
    },

    onGridColumnClear       : function() {
        alert("onGridColumnClear");
    },
    onGridColumnSave        : function(isGlobal) {
        alert("onGridColumnSave " + isGlobal);
    },
    onGridColumnLoad        : function(isGlobal) {
        alert("onGridColumnClear");
    },

    /**
     * Clears grid filters.
     */
    onGridFilterClear       : function() {
        this.showAllItems = true;
        this.showAllItemsChanging = true;
        Ext.getCmp(this._filterButtonId).setText($lang('msg_gridfilter_clear'));
        var curState = this.getState();
        delete curState.savedView;
        Ext.state.Manager.set(this.stateId, curState);
        if (this.filters) {
            this.filters.clearFilters();
            this.showAllColumns();
            this.fireEvent("filtercleared", this);
        }
        this.reloadStore();
        Ext.defer(function() {
                    this.showAllItemsChanging = false;
                }, 1000, this);
    },

    showAllColumns          : function() {
        delete this.getStore().sortInfo;

        // var initialConfig = Viz.Configuration.Columns[this.xtype.substr(7)] || this.initialConfig.columns;
        var initialConfig = this.originalColumnConfig || Viz.Configuration.Columns[this.xtype.substr(7)] || this.initialConfig.columns;
        initialConfig = Viz.clone(initialConfig);
        this.setConfigWithNumberer(initialConfig);
    },

    setConfigWithNumberer   : function(config) {
        for (index in this.colModel.config) {
            var column = this.colModel.config[index];
            if (column.nonRemovableColumn === true) {
                var nonRemovableColumns = [column];
                config = nonRemovableColumns.concat(config);
            }
        }
        if (this.hasRowNumber) {
            var newColumns = [new Viz.grid.PagingRowNumberer()];
            config = newColumns.concat(config);
        }

        this.colModel.setConfig(config, false);
    },

    adjustColumnWidth       : function(force) {
        if (force || this.shouldAdjustColumnWidth()) {
            Ext.each(this.colModel.config, function(item) {
                        if (!item.fixed) {
                            item.width = 1;
                        }
                    }, this);
            this.colModel.totalWidth = 10;
            this.getView().fitColumns();
        }
    },

    shouldAdjustColumnWidth : function() {
        var totalColumnSum = Ext.sum(this.colModel.config.map(function(item) {
                    return item.hidden ? 0 : item.width;
                }));
        var gridWidth = this.getView().getGridInnerWidth();
        return totalColumnSum > gridWidth;
    },

    saveState               : function() {
        try {
            if (!this.savedViewChanging) {
                var curState = this.getState();
                var labelText;
                if (!this.showAllItemsChanging) {
                    curState.showAllItems = false;
                    this.showAllItems = false;
                }
                if (curState.showAllItems) {
                    labelText = 'msg_gridfilter_clear';
                }
                else {
                    labelText = 'msg_gridfilter_custom';
                }
                Ext.getCmp(this._filterButtonId).setText($lang(labelText));
                delete curState.savedView;
                delete this.savedView;
                Ext.state.Manager.set(this.stateId, curState);
            }
        }
        catch (e) {
        }
        Viz.grid.GridPanel.superclass.saveState.apply(this, arguments);
    },

    /**
     * Save grid filters to user or tenant preference.
     */
    onGridFilterSave        : function(isGlobal) {
        if (this.filters && this.stateId) {
            var f = {};
            this.filters.filters.each(function(filter) {
                        if (filter.active) {
                            f[filter.dataIndex] = filter.getValue();
                            switch (filter.type) {
                                case 'treecombomultiselect' :
                                    f[filter.dataIndex + 'Path'] = filter.getPathAsArray();
                                    break;
                                default :
                            }

                        }
                    });

            var _comboId = Ext.id();
            var button = {
                text    : $lang('msg_save'),
                iconCls : 'viz-icon-small-gridfilter-save',
                handler : function() {
                    if (Ext.getCmp(_comboId).isValid()) {
                        var title = Ext.getCmp(_comboId).getValue();
                        // IE8 slow fix
                        if (title === undefined) {
                            title = Ext.getCmp(_comboId).lastQuery;
                        }
                        if (!Ext.isEmpty(title)) {
                            var newFilter = {};
                            newFilter[title] = f;
                            newFilter[title]["_Columns"] = this.getState().columns;
                            newFilter[title]["_Sort"] = this.getState().sort;

                            var existingFilter = this.getExistingFilter(true, isGlobal);
                            Ext.apply(existingFilter, newFilter);
                            this.saveFitlersState(existingFilter, isGlobal);
                            this.loadFilterMenu();
                            this.loadSerializedFilter(title, isGlobal);
                            win.close();
                        }
                    }
                },
                scope   : this
            };
            var saveFilterWindowTitle = isGlobal ? $lang('msg_gridfilter_save_global') : $lang('msg_gridfilter_save');
            var win = this.getSaveFilterWindow(saveFilterWindowTitle, 'viz-icon-small-gridfilter', _comboId, false, button, isGlobal);
            win.show();
        }
    },

    saveFitlersState        : function(filters, isGlobal) {
        Ext.state.Manager.set(this.stateId + '_filters' + (isGlobal ? '_global' : ''), this.encodeExtEncodeFix(filters));
    },

    /**
     * Save grid filters to tenant preference.
     */
    /*
     * onGridFilterSaveGlobal: function () { if (this.filters && this.stateId) { var f = {}; this.filters.filters.each(function (filter) { if (filter.active) { f[filter.dataIndex] = filter.getValue(); switch (filter.type) { case 'treecombomultiselect': f[filter.dataIndex + 'Path'] = filter.getPathAsArray(); break; default: } } }); var _comboId = Ext.id(); var button = { text: $lang('msg_save'), iconCls: 'viz-icon-small-gridfilter-save', handler: function () { var title = Ext.getCmp(_comboId).getValue(); if (!Ext.isEmpty(title)) { var newFilter = {}; newFilter[title] = f; var existingFilter = this.getExistingFilter(true, true); Ext.apply(existingFilter, newFilter); Ext.state.Manager.set(this.stateId + '_filters_global', existingFilter); this.loadFilterMenu(); } win.close(); }, scope: this };
     * var win = this.getSaveFilterWindow($lang('msg_gridfilter_save'), 'viz-icon-small-gridfilter', _comboId, false, button, true); win.show(); } },
     */

    /**
     * Load a previous saved filter from user preference.
     */
    onGridFilterLoad        : function() {
        if (this.filters && this.stateId) {
            var _comboId = Ext.id();

            var button = {
                text    : $lang('msg_gridfilter_load'),
                iconCls : 'viz-icon-small-gridfilter-load',
                handler : function() {
                    var title = Ext.getCmp(_comboId).getValue();
                    var existingFilter = this.getExistingFilter(false);
                    this.loadSerializedFilter(title);
                    win.close();
                },
                scope   : this
            };
            var win = this.getSaveFilterWindow($lang('msg_gridfilter_load'), 'viz-icon-small-gridfilter', _comboId, true, button);
            win.show();
        }
    },
    loadFilterMenu          : function() {
        var filterButton = Ext.getCmp(this._filterButtonId);
        if (filterButton && filterButton.menu) {
            var filterMenu = filterButton.menu;
            for (var i = filterMenu.items.length - 1; i > 0; i--) {
                var item = filterMenu.items.items[i];
                if (item.id.indexOf(this._filterButtonId) == 0)
                    filterMenu.remove(item);
            }

            // get global filters
            var serializedfilters = this.getSavedFiltersArray(true);
            if (serializedfilters.length > 0) {
                filterMenu.addMenuItem({
                            id    : this._filterButtonId + '-spr',
                            xtype : 'menuseparator'
                        });
                for (var i = 0; i < serializedfilters.length; i++) {
                    var menuText = serializedfilters[i][0];
                    filterMenu.addMenuItem({
                                id           : this._filterButtonId + '-' + Ext.util.Format.htmlEncode(menuText) + '-global',
                                text         : Ext.util.Format.htmlEncode(menuText).toBold(),
                                originalText : menuText,
                                scope        : this,
                                iconCls      : 'viz-icon-small-filter',
                                handler      : function(m, e) {
                                    this.loadSerializedFilter(m.originalText, true);
                                }
                            });
                }
            }
            // get local filters
            var serializedfilters = this.getSavedFiltersArray(false);
            if (serializedfilters.length > 0) {
                filterMenu.addMenuItem({
                            id    : this._filterButtonId + '-spr2',
                            xtype : 'menuseparator'
                        });
                for (var i = 0; i < serializedfilters.length; i++) {
                    var menuText = serializedfilters[i][0];
                    filterMenu.addMenuItem({
                                id           : this._filterButtonId + '-' + Ext.util.Format.htmlEncode(menuText),
                                text         : Ext.util.Format.htmlEncode(menuText),
                                originalText : menuText,
                                scope        : this,
                                iconCls      : 'viz-icon-small-filter',
                                handler      : function(m, e) {
                                    this.loadSerializedFilter(m.originalText, false);
                                }
                            });
                }
            }
        }
    },
    loadSerializedFilter    : function(newFilter, isGlobal) {
        var curFilter;
        try {
            if (!Ext.isEmpty(newFilter)) {
                this.filters.applyingState = true;
                this.filters.clearFilters();
                if (Ext.isString(newFilter)) {
                    var existingFilter = this.getExistingFilter(false, isGlobal);
                    curFilter = existingFilter[newFilter];
                    var curState = Ext.state.Manager.get(this.stateId);
                    if (!Ext.isObject(curState)) {
                        curState = this.getState();
                    }

                    if (curState && curFilter) {
                        curState = Viz.clone(curState);
                        curState.savedView = newFilter;
                        curState.savedViewGlobal = isGlobal;
                        curState.showAllItems = true;
                        this.savedView = newFilter;
                        this.savedViewGlobal = isGlobal;
                        this.savedViewChanging = true;
                        this.showAllItems = true;
                        if (!curFilter["_Columns"]) {
                            Ext.each(curState.columns, function(x) {
                                        x.hidden = false;
                                    }, this);
                        }
                        else {
                            curState.columns = curFilter["_Columns"];
                        }
                        curState.filters = curFilter;
                        curState.lastFilters = curFilter;

                        // Set state of columns
                        for (var i = 0; i < this.colModel.config.length; i++) {
                            var id = this.colModel.config[i].id;
                            var index = this.findIndexById(curState.columns, id);
                            if (index !== undefined) {
                                var state = curState.columns[index];
                                this.colModel.setState(i, state);
                                if (!state.hidden) {
                                    this.colModel.config[i].hidden = false;
                                }
                            }
                        };

                        // Set order of columns
                        var newConfig = [];
                        for (var i = 0; i < this.colModel.config.length; i++) {
                            var id = this.colModel.config[i].id;
                            var index = this.findIndexById(curState.columns, id);
                            if (index !== undefined) {
                                newConfig[index] = this.colModel.config[i];
                            }
                        };
                        var newConfigWithoutUndefined = [];
                        for (i = 0; i < newConfig.length; i++) {
                            if (newConfig[i] !== undefined) {
                                newConfigWithoutUndefined.push(newConfig[i]);
                            }
                        }
                        this.colModel.config = newConfigWithoutUndefined;
                        this.adjustColumnWidth();

                        // Set sorting
                        if (!curFilter["_Sort"]) {
                            delete this.getStore().sortInfo;
                            // this.view.getHeaderCell(columnIndex).removeClass(["sort-asc", "sort-desc"]);
                        }
                        else {
                            this.getStore().sort(curFilter["_Sort"].field, curFilter["_Sort"].direction);
                        }

                        this.colModel.totalWidth = null;
                        // this.getView().updateAllColumnWidths();

                        this.getView().refresh(true);
                        this.doLayout();
                        Ext.state.Manager.set(this.stateId, curState);
                    }
                }
                else
                    curFilter = newFilter;

                for (key in curFilter) {
                    var filter = this.filters.filters.get(key);
                    if (filter) {
                        filter.setValue(curFilter[key]);
                        filter.setActive(true);
                        switch (filter.type) {
                            case 'treecombomultiselect' :
                                if (curFilter[key + 'Path'])
                                    filter.selectPathArray(curFilter[key + 'Path']);
                                break;
                            default :
                        }
                    }
                }
                this.filters.deferredUpdate.cancel();
                delete this.filters.applyingState;
                this.resetStore();
                this.filters.reload();

                Ext.getCmp(this._filterButtonId).setText(this.getFilterTitle(newFilter, isGlobal, true));
                Ext.defer(function() {
                            this.savedViewChanging = false;
                            Ext.getCmp(this._filterButtonId).setText(this.getFilterTitle(newFilter, isGlobal, true));
                        }, 1500, this, [newFilter]);

            }
        }
        catch (e) {
            this.onGridFilterClear();
        }
    },

    findIndexById           : function(columns, id) {
        var result;
        Ext.each(columns, function(item, index) {
                    if (item.id === id) {
                        result = index;
                    }
                }, this);
        return result;
    },

    findIndexByDataIndex    : function(columns, dataIndex) {
        var result;
        Ext.each(columns, function(item, index) {
                    if (item.dataIndex === dataIndex) {
                        result = index;
                    }
                }, this);
        return result;
    },

    extEncodeFix            : function(original) {
        var encoded = original.replace(/\^/g, "&kova;");
        encoded = encoded.replace(/\=/g, "&shave;");
        encoded = encoded.replace(/\:/g, "&nekudotaim;");
        encoded = Ext.util.Format.htmlEncode(encoded);
        return encoded;
    },

    extDecodeFix            : function(encoded) {
        var decoded = Ext.util.Format.htmlDecode(encoded);
        decoded = decoded.replace(/&kova;/g, "^");
        decoded = decoded.replace(/&shave;/g, "=");
        decoded = decoded.replace(/&nekudotaim;/g, ":");
        return decoded;
    },

    encodeExtEncodeFix      : function(obj) {
        fixedObj = {};
        for (key in obj) {
            fixedObj[this.extEncodeFix(key)] = obj[key];
        }
        return fixedObj;
    },

    decodeExtEncodeFix      : function(obj) {
        fixedObj = {};
        for (key in obj) {
            fixedObj[this.extDecodeFix(key)] = obj[key];
        }
        return fixedObj;
    },

    getExistingFilter       : function(clone, isGlobal) {
        var retVal;
        if (isGlobal) {
            var retVal = Ext.state.Manager.get(this.stateId + '_filters_global') || {};
        }
        else {
            var retVal = Ext.state.Manager.get(this.stateId + '_filters') || {};
        }

        if (Ext.isObject(retVal)) {
            retVal = this.decodeExtEncodeFix(retVal);
        }
        else {
            retVal = {};
        }

        if (clone)
            retVal = Viz.clone(retVal);

        return retVal;
    },

    getSavedFiltersArray    : function(isGlobal, htmlEncode) {
        var retVal = [];
        var existingFilter = this.getExistingFilter(false, isGlobal);
        if (Ext.isObject(existingFilter)) {
            for (var i in existingFilter) {
                var value = htmlEncode ? Ext.util.Format.htmlEncode(i) : i;
                retVal.push([value]);
            }
        };
        return retVal;
    },

    getSaveFilterWindow     : function(title, iconCls, comboId, forceSelection, button, isGlobal) {
        var win = new Ext.Window({
                    title     : title,
                    width     : 400,
                    height    : 100,
                    iconCls   : iconCls,
                    bodyStyle : 'padding:5px;background-color:transparent',
                    layout    : 'form',
                    items     : [{
                        id             : comboId,
                        allowBlank     : false,
                        xtype          : 'combo',
                        anchor         : '-20',
                        fieldLabel     : $lang('msg_title'),
                        forceSelection : forceSelection,
                        mode           : 'local',
                        store          : new Ext.data.SimpleStore({
                                    autoDestroy : true,
                                    fields      : ['val'],
                                    data        : this.getSavedFiltersArray(isGlobal, true)
                                }),
                        triggerAction  : 'all',
                        displayField   : 'val',
                        valueField     : 'val',
                        listeners      : {
                            select : function(combo, record, index) {
                                var title = record.data[this.valueField || this.displayField || 'val'];
                                title = Ext.util.Format.htmlDecode(title);
                                combo.setValue(title);
                            },
                            scope  : this
                        }
                            // vtype : 'gridViewFilterName'
                        }],
                    buttons   : [button, {
                                text    : $lang('msg_delete'),
                                iconCls : 'viz-icon-small-delete',
                                handler : function() {
                                    var shouldShowAllItems = false;
                                    var combo = Ext.getCmp(comboId);
                                    var title = combo.getValue();
                                    if (!Ext.isEmpty(title)) {
                                        var existingFilter = this.getExistingFilter(true, isGlobal);
                                        delete existingFilter[title];
                                        this.saveFitlersState(existingFilter, isGlobal);

                                        // If deleting currently selected view then delete it also from grid state
                                        if (this.savedView === title && this.savedViewGlobal === isGlobal) {
                                            var curState = this.getState();
                                            delete curState.savedView;
                                            delete curState.savedViewGlobal;
                                            delete curState.showAllItems;
                                            Ext.state.Manager.set(this.stateId, curState);
                                        }

                                        this.loadFilterMenu();

                                        // If deleting currently selected view then change the text in the combo box
                                        var rawFilterName = this.getFilterTitle(Ext.util.Format.htmlEncode(title), isGlobal);
                                        if (Ext.getCmp(this._filterButtonId).getText() === rawFilterName) {
                                            Ext.getCmp(this._filterButtonId).setText($lang('msg_gridfilter_custom'));
                                            shouldShowAllItems = true;
                                        }
                                    }
                                    combo.clearValue();
                                    combo.store.remove(combo.store.getAt(combo.store.find('val', title)));
                                    win.close();
                                    if (shouldShowAllItems) {
                                        this.onGridFilterClear();
                                    }
                                },
                                scope   : this
                            }]
                });
        return win;
    },

    /**
     * Surround with bold tag
     * @return {string}
     */
    getFilterTitle          : function(title, isGlobal, encode) {
        if (encode) {
            title = Ext.util.Format.htmlEncode(title);
        }
        var rawFilterName = isGlobal ? title.toBold() : title;
        return rawFilterName;
    },

    /**
     * Creates a instance of the RowEditor plugin
     * @return {Ext.ux.grid.RowEditor}
     */
    createRowEditor         : function() {
        return new Ext.ux.grid.RowEditor({
                    clicksToEdit : 2,
                    saveText     : $lang('msg_update'),
                    cancelText   : $lang('msg_cancel'),
                    tooltip      : new Ext.ToolTip({
                                maxWidth       : 600,
                                cls            : 'errorTip',
                                width          : 300,
                                title          : $lang('msg_error_validation'),
                                autoHide       : false,
                                anchor         : 'left',
                                anchorToTarget : true,
                                mouseOffset    : [4, 0]
                            }),
                    listeners    : {
                        // this is to force hiding the tooltip when all fields are valid.
                        canceledit : function(editor, obj, record, rowIndex) {
                            if (!this.editor.isValid() && !this.editor.record.isValid()) {
                                this.editor.stopEditing();
                                this.store.remove(this.editor.record);
                            }
                        },
                        scope      : this
                    }
                });
    },
    /**
     * The configuration for crud toolbar.
     * @return {Array}
     */
    configToolbarCrud       : function() {
        return [{
                    text    : $lang("msg_add"),
                    iconCls : 'viz-icon-small-add',
                    scope   : this,
                    handler : this.onAddRecord
                }, {
                    text    : $lang("msg_update"),
                    iconCls : 'viz-icon-small-update',
                    scope   : this,
                    handler : this.onUpdateRecord
                }, {
                    id      : this._buttonDeleteId,
                    text    : $lang("msg_delete"),
                    iconCls : 'viz-icon-small-delete',
                    scope   : this,
                    handler : this.onDeleteRecord
                }, {
                    text    : $lang("msg_save"),
                    iconCls : 'viz-icon-small-save',
                    scope   : this,
                    hidden  : !(this.hasRowEditor === true),
                    handler : this.onSaveStore
                }];
    },

    /**
     * @private Handler for beforeaddrecord event.
     */
    onBeforeAddRecord       : function(grid, entity) {
        return true;
    },

    /**
     * @private Handler for beforeupdaterecord event.
     */
    onBeforeUpdateRecord    : function(grid, record, entity) {
        return true;
    },
    /**
     * Handler for the Add item button.
     */
    onAddRecord             : function() {
        if (this.editor) {
            var reader = this.store.reader;
            var idProperty = reader.meta.idProperty
            if (reader.meta.idProperty && this.store.fields.containsKey(reader.meta.idProperty))
                var data = {};
            data[idProperty] = [Ext.data.Record.PREFIX, '-', Ext.data.Record.AUTO_ID++].join('');
            var rec = new this.store.recordType(data);
            this.editor.stopEditing();
            this.store.insert(0, rec);
            this.getView().refresh();
            this.getSelectionModel().selectRow(0);
            this.editor.startEditing(0);
        }
        else {
            var entity = {};
            if (this.fireEvent("beforeaddrecord", this, entity)) {
                this.openFormCrud(this.formConfig, 'create', entity);
            }
        }
    },
    /**
     * Handler for the Update item button.
     */
    onUpdateRecord          : function(targetAction) {
        var selections = this.getSelectionModel().getSelections();
        if (selections.length == 0)
            return;
        if (selections.length > 1 && this.formConfig.updatebatch) {
            if (this.allowupdatebatch !== true) {
                Ext.MessageBox.show({
                            title    : $lang("msg_update"),
                            msg      : $lang("msg_record_update_confirmation_multiple_notallowed"),
                            buttons  : Ext.MessageBox.OK,
                            icon     : Ext.MessageBox.WARNING,
                            minWidth : 200
                        });
                return;
            }

            var msgUpdate = String.format($lang("msg_record_update_confirmation_multiple"), selections.length);
            Ext.MessageBox.confirm($lang('msg_application_title'), msgUpdate, function(button) {
                        if (button == 'yes') {
                            if (this.fireEvent("beforeupdatebatchrecords", this, selections)) {
                                var keys = [];
                                var entity = {};
                                for (var i = 0, r; r = selections[i]; i++) {
                                    keys.push(r.id);
                                }
                                this.openFormCrud(this.formConfig, 'updatebatch', entity, keys);
                            }
                        }
                    }, this);
        }
        else {
            var record = this.getSelectionModel().getSelected();
            if (record) {
                var entity = record.json;
                if (entity && this.fireEvent("beforeupdaterecord", this, record, entity)) {
                    // adding the columns and the entity key to formConfig as historyConfig
                    Ext.apply(this.formConfig, {
                                historyConfig : {
                                    entityKey : record.id
                                }
                            });
                    if (this.formConfig && Ext.isFunction(this.formConfig.updateConfig))
                        this.formConfig.updateConfig(entity, targetAction);
                    this.openFormCrud(this.formConfig, 'update', entity);
                }
            }
        }
    },
    /**
     * Handler for the Delete item button.
     */
    onDeleteRecord          : function(button, event, msgDeleteSingle, msgDeleteMultiple) {
        var selections = this.getSelectionModel().getSelections();
        if (selections.length == 0)
            return;
        if (this.editor) {
            this.editor.stopEditing();
            for (var i = 0, r; r = selections[i]; i++) {
                this.store.remove(r);
            }
            this.getSelectionModel().selectRow(0);
        }
        else {
            if (this.fireEvent("beforedeleterecord", this, selections)) {
                var msgDelete;
                if (selections.length > 1)
                    msgDelete = String.format(msgDeleteMultiple ? msgDeleteMultiple : $lang("msg_record_delete_confirmation_multiple"), selections.length);
                else
                    msgDelete = msgDeleteSingle ? msgDeleteSingle : $lang("msg_record_delete_confirmation_single");
                Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                            if (button == 'yes') {
                                this.deleteRecordInternal(selections);
                            }
                        }, this);
            }
        }
    },
    // protected.
    deleteRecordInternal    : function(selections) {
        for (var i = 0, r; r = selections[i]; i++) {
            this.store.remove(r);
        }
        if (this.store.writer) // We don't need to call save if the store does not have a writer, for example a memory store.
            this.onSaveStore();

        this.fireEvent("deleterecord", this, selections)
    },
    /**
     * Handler for after delete event.
     * @param {} selections
     */
    onAfterDeleteRecord     : function(grid, selections) {
    },
    /**
     * Handler for the before copy event
     * @param {} selections
     */
    onBeforeCopyRecord      : function(selections) {

    },
    /**
     * Handler for the Copy item button.
     */
    onCopyRecord            : function() {
        var selections = this.getSelectionModel().getSelections();
        if (selections.length == 0)
            return;
        if (this.editor) {
            this.editor.stopEditing();
        }
        if (this.fireEvent("beforecopyrecord", selections)) {
            for (var i = 0, r; r = selections[i]; i++) {
                var newdata = Viz.clone(r.data);
                var reader = this.store.reader;
                var idProperty = reader.meta.idProperty
                if (idProperty)
                    newdata[idProperty] = '';
                newdata['LocalId'] = null;
                newdata['Creator'] = null;
                this.store.add(new this.store.recordType(newdata));
            }
            this.onSaveStore();
        }
    },
    /**
     * Opens a form crud.
     * @param {Object} config The configuration of the form.
     * @param {String} mode The mode of the form, either 'create' or 'update'.
     * @param {Object} entity The entity object is not null when mode == 'update'
     */
    openFormCrud            : function(config, mode, entity, updatebatchkeys) {
        Viz.openFormCrud.apply(this, arguments);
    },
    /**
     * Handler for the Save store button.
     */
    onSaveStore             : function() {
        // this will mask the grid.
        // this.store.fireEvent("beforesave", store);
        this.store.save();
    },

    /**
     * Checks if the cell has a tooltip provided by a renderer.
     * @param {Object} cell
     * @return {Boolean}
     */
    cellHasTooltip          : function(cell) {
        try {
            return !Ext.isEmpty(Ext.fly(cell.dom.childNodes[0]).getAttributeNS("ext", "qtip"));
        }
        catch (e) {
            ;
        }

    },
    /**
     * Creates a tootip for each cell.
     */
    createTooltip           : function() {
        var view = this.getView();
        this.tip = new Ext.ToolTip({
                    target     : view.mainBody,
                    delegate   : '.x-grid3-cell',
                    trackMouse : false,
                    renderTo   : document.body,
                    listeners  : {
                        beforeshow : function updateTipBody(tip) {
                            try {
                                var rowIndex = view.findRowIndex(tip.triggerElement);
                                var cellIndex = view.findCellIndex(tip.triggerElement);
                                if (rowIndex >= 0 && cellIndex >= 0 && rowIndex < this.store.data.length) {
                                    var cell = Ext.fly(view.getCell(rowIndex, cellIndex));
                                    if (this.cellHasTooltip(cell))
                                        return false;
                                    // we calculate if the text is hidden. Special case for column with an icon.
                                    var textWidth = cell.getTextWidth() + (cell.hasClass('viz-icon-cell') ? 22 : 2);
                                    if (textWidth > cell.getWidth()) {
                                        // the text of the cell could be altered by a renderer so we cannot rely on columnModel
                                        tip.setInfo(cell.dom.textContent || cell.dom.innerText);
                                    }
                                    else
                                        return false;
                                    return;
                                }
                                else
                                    return false;
                            }
                            catch (ex) {
                                return false;
                            }
                        },
                        scope      : this
                    }
                });
    },
    /**
     * Deletes the selected row from the grid.
     */
    deleteSelectedRows      : function() {
        this.stopEditing();
        var s = this.getSelectionModel().getSelections();
        for (var i = 0, r; r = s[i]; i++) {
            this.store.remove(r);
        }
        var form = this.getParentForm();
        if (form)
            form.isDirty = true;
    },
    /**
     * Reloads the store.
     */
    reloadStore             : function() {
        this.store.reload();
    },

    /**
     * Reload the store and reset the paging to first page.
     */
    resetStore              : function() {
        var paging = this.getPaging();
        if (paging) {
            paging.moveFirst();
        }
    },

    /**
     * Return the Paging toolbar.
     * @return {}
     */
    getPaging               : function() {
        var retVal = Ext.getCmp(this._pagingId);
        return retVal;
    },

    /**
     * Returns the parent Form if the grid is contained in a Form.
     */
    getParentForm           : function() {
        var retVal = this.findParentByType('form');
        return retVal;
    },

    /**
     * Opens the window for adding existing records based on a grid.
     * @param {Object} paramConfig The configuration object.
     */
    openGridDetail          : function(paramConfig) {
        return this.openTreeOrGridDetail(paramConfig);

    },

    /**
     * Opens the window for adding existing records based on a tree.
     * @param {Object} paramConfig The configuration object.
     */
    openTreeDetail          : function(paramConfig) {
        return this.openTreeOrGridDetail(paramConfig);
    },

    /**
     * @private Opens the window for adding existing records based on a tree or a grid.
     * @param {Object} paramConfig The configuration object.
     */
    openTreeOrGridDetail    : function(paramConfig) {

        var componentId = Ext.id();

        var componentConfig = {
            header : false,
            border : false,
            id     : componentId
        };

        if (paramConfig.xtypeTreeDetail) {
            Ext.apply(componentConfig, {
                        xtype                       : paramConfig.xtypeTreeDetail,
                        root                        : paramConfig.rootTreeDetail,
                        bodyStyle                   : 'padding: 5px',
                        rootVisible                 : paramConfig.rootVisibleTreeDetail || false,
                        displayCheckbox             : true,
                        serviceParams               : paramConfig.serviceParamsTreeDetail,
                        checkboxExcludedTypes       : paramConfig.checkboxExcludedTypesTreeDetail,
                        disableCheckboxForAncestors : paramConfig.disableCheckboxForAncestors,
                        stateful                    : paramConfig.stateful === true ? true : false,
                        ExcludeMetersAndAlarms      : paramConfig.ExcludeMetersAndAlarms
                    });
        }
        else {
            Ext.apply(componentConfig, {
                        xtype             : paramConfig.xtypeGridDetail,
                        tbar              : null,
                        enableRowDblClick : false
                    });
        }
        if (paramConfig.xtypeComponentExtraConfig) {
            Ext.apply(componentConfig, paramConfig.xtypeComponentExtraConfig);
        }

        var win = new Ext.Window({
                    title   : paramConfig.windowTitle,
                    width   : paramConfig.windowWidth || 350,
                    height  : paramConfig.windowHeight || 400,
                    modal   : true,
                    iconCls : paramConfig.windowIconCls,
                    layout  : 'fit',
                    items   : componentConfig,
                    buttons : [{
                                text    : $lang('msg_add'),
                                iconCls : 'viz-icon-small-add',
                                handler : function() {
                                    var form = this.getParentForm();

                                    var idField = paramConfig.idField ? paramConfig.idField : this.store.reader.meta.idProperty;

                                    if (paramConfig.xtypeTreeDetail) {
                                        var tree = Ext.getCmp(componentId);
                                        var checkedNodes = tree.getChecked();
                                        Ext.each(checkedNodes, function(node) {
                                                    var nodeEntity = this.getEntityFromTreeNode(node);
                                                    if (nodeEntity.Path == null)
                                                        nodeEntity.Path = this.getNodeFullPath(node);
                                                    if (this.store.find(idField, nodeEntity[idField]) == -1) {
                                                        this.isDirty = true;
                                                        if (form)
                                                            form.isDirty = true;
                                                        this.store.add(new this.store.recordType(nodeEntity));
                                                    }
                                                }, this);
                                    }
                                    else {
                                        var grid = Ext.getCmp(componentId);
                                        var s = grid.getSelectionModel().getSelections();
                                        for (var i = 0, r; r = s[i]; i++) {
                                            var gridEntity = this.getEntityFromGridRecord(r);
                                            if (this.store.find(idField, gridEntity[idField]) == -1) {
                                                this.isDirty = true;
                                                if (form)
                                                    form.isDirty = true;
                                                this.store.add(new this.store.recordType(gridEntity));
                                            }
                                        }
                                    }
                                    this.clearInvalid();
                                    if (paramConfig.saveAfterAdd && this.isDirty) {
                                        this.store.save();
                                    }
                                    win.close();
                                },
                                scope   : this
                            }]
                });
        win.show();
    },
    /**
     * Return the data to add to the store from the detail tree checked node.
     */
    getEntityFromTreeNode   : function(node) {
        return node.attributes.entity;
    },
    /**
     * Return the full path to the node.
     */
    getNodeFullPath         : function(node) {
        var path = '';
        var rootVisible = node.ownerTree.rootVisible;
        var parent = node.parentNode;
        while (parent) {
            if (parent.text && !parent.hidden && (parent.parentNode || rootVisible)) {
                path = parent.text + ' / ' + path;
            }
            parent = parent.parentNode;
        }
        if (path.length > 2) {
            path = path.substring(0, path.length - 3);
        }
        return path;
    },
    /**
     * Return the data to add to the store from the detail grid selected record.
     */
    getEntityFromGridRecord : function(r) {
        return r.data;
    },

    /**
     * Return the max value of all the records in the store for a specific field.
     * @param {String} field the field to get the value from.
     * @param {float} (Optional) minValue : the minValue in case there are no records. Defaults to 0.
     * @return {float}
     */
    getMaxValueFromStore    : function(field, minValue) {
        var value = minValue || 0;
        Ext.each(this.store.getArray(), function(data) {
                    value = Math.max(value, data[field]);
                }, this);

        return value;
    },

    /**
     * Mark the grid as invalid (when it s used in a field for example
     * @param {} msg
     */
    markInvalid             : function(msg) {
        if (this.rendered) {
            this.el.addClass(this.invalidClass)
            if (this.invalidText) {

                Ext.MessageBox.show({
                            title    : $lang('msg_error_validation'),
                            msg      : this.invalidText,
                            buttons  : Ext.MessageBox.OK,
                            minWidth : 250
                        });
            }
        }
    },

    /**
     * Remove the invalid class.
     */
    clearInvalid            : function() {
        this.el.removeClass(this.invalidClass)

    },

    /**
     * Check if a grid is valid or not
     */
    isValid                 : function() {
        if (this.allowEmpty === false && this.store.getCount() <= 0) {
            this.markInvalid();
            return false;
        }
        return true;
    },
    /**
     * Builds the audit grid and open a window with the grid
     * @param {} entityName
     */
    onShowEntitiesAudit     : function(entityName) {
        var pagingConfig = {
            xtype : 'paging',
            start : 0,
            limit : this.pageSize
        };

        var auditStore = new Viz.store.Audit({
                    entityName : entityName,
                    paging     : pagingConfig
                });

        auditStore.load({
                    params   : pagingConfig,
                    callback : function(r, options, success) {
                        var columnPrefix = 'Entity.';
                        var columns = Viz.Configuration.Columns.Audit;
                        var columnsCount = columns.length;
                        Ext.each(auditStore.fields.items, function(item) {
                                    if (item.mapping.startsWith(columnPrefix)) {
                                        var newColumn = {
                                            header    : item.name.replace(columnPrefix, ''),
                                            dataIndex : item.name,
                                            renderer  : function(value, metadata, record, rowIndex, colIndex, store) {
                                                var changedFields = record.get('ChangedFields').split(',');
                                                var columnName = record.fields.get(colIndex + columnsCount + 1).mapping.replace(columnPrefix, '');
                                                for (var j = 0; j < changedFields.length; j++) {
                                                    if (changedFields[j] == columnName) {
                                                        value = value.toBold();
                                                        break;
                                                    }
                                                }
                                                return value;
                                            }
                                        };
                                        if (item.mapping == 'Entity.AcquisitionDateTime') {
                                            newColumn = Ext.apply({
                                                        xtype  : 'datecolumn',
                                                        format : Viz.getLocalizedDateTimeFormat(true)
                                                    }, newColumn);
                                        }

                                        columns.push(newColumn);
                                    }
                                });

                        this._auditAddButtonId = Ext.id();
                        this._auditUpdateButtonId = Ext.id();
                        this._auditDeleteButtonId = Ext.id();
                        var auditGrid = {
                            id            : this._auditGridId,
                            xtype         : 'vizGrid',
                            labelAlign    : 'top',
                            hasRowEditor  : false,
                            stripeRows    : false,
                            hasToolbar    : false,
                            columns       : columns,
                            store         : auditStore,
                            autoLoadStore : false,
                            stateful      : false,
                            /*
                             * tbar : [$lang('msg_audit_operationfilter'), { id : this._auditAddButtonId, iconCls : 'viz-icon-small-add', text : $lang("msg_add"), enableToggle : true, toggleHandler : function(button, state) { this.toggleOperationFilter('IsInsert', state); }, scope : this, pressed : true }, { id : this._auditUpdateButtonId, iconCls : 'viz-icon-small-update', text : $lang("msg_update"), enableToggle : true, toggleHandler : function(button, state) { this.toggleOperationFilter('IsUpdate', state); }, scope : this, pressed : true }, { id : this._auditDeleteButtonId, iconCls : 'viz-icon-small-delete', text : $lang("msg_delete"), enableToggle : true, toggleHandler : function(button, state) { this.toggleOperationFilter('IsDelete', state); }, scope : this, pressed : true }],
                             */
                            view          : new Ext.grid.GroupingView({
                                        groupTextTpl : '{[Ext.util.Format.htmlEncode(values.text)]} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
                                    })
                            /*
                             * , listeners : { filtercleared : this.clearAuditFilters, scope : this }
                             */
                        };

                        var auditWindow = new Ext.Window({
                                    title   : $lang('msg_audit'),
                                    iconCls : 'viz-icon-small-audit',
                                    modal   : false,
                                    layout  : 'fit',
                                    width   : 600,
                                    height  : 400,
                                    items   : auditGrid
                                });
                        auditWindow.show();
                        columns = columns.splice(columnsCount, columns.length - columnsCount);
                    },
                    scope    : this
                });
    }
        /*
         * , toggleOperationFilter : function(filterName, filterValue) { var auditGridCmp = Ext.getCmp(this._auditGridId); var dmlTypeFilter = auditGridCmp.filters.getFilter(filterName); if (!dmlTypeFilter) { dmlTypeFilter = auditGridCmp.filters.addFilter({ type : 'bool', dataIndex : filterName }); } if (dmlTypeFilter) { dmlTypeFilter.setValue(filterValue); dmlTypeFilter.setActive(!filterValue); } else { auditGridCmp.store.load(); } }, clearAuditFilters : function() { var addButton = Ext.getCmp(this._auditAddButtonId); if (addButton && !addButton.pressed) { addButton.toggle(); } var updateButton = Ext.getCmp(this._auditUpdateButtonId); if (updateButton && !updateButton.pressed) { updateButton.toggle(); } var deleteButton = Ext.getCmp(this._auditDeleteButtonId); if (deleteButton &&
         * !deleteButton.pressed) { deleteButton.toggle(); } }
         */
    });
Ext.reg("vizGrid", Viz.grid.GridPanel);

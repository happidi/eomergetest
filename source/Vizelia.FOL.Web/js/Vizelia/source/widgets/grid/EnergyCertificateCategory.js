﻿/**
 * @class Viz.grid.EnergyCertificateCategory
 * <p>
 * A grid exposing the business entity EnergyCertificateCategory
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.EnergyCertificateCategory = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_energycertificatecategory_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('EnergyCertificateCategory_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_energycertificatecategory_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('EnergyCertificateCategory_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_energycertificatecategory_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('EnergyCertificateCategory_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 350,
                            height           : 180,
                            xtype            : 'vizFormEnergyCertificateCategory',
                            messageSaveStart : 'EnergyCertificateCategoryChangeStart',
                            messageSaveEnd   : 'EnergyCertificateCategoryChange'
                        },
                        create      : {
                            title   : $lang("msg_energycertificatecategory_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_energycertificatecategory_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'KeyEnergyCertificateCategory',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyEnergyCertificateCategory'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.EnergyCertificateCategory_FormUpdateBatch
                        }
                    },
                    store        : config.store || new Viz.store.EnergyCertificateCategory({
                                KeyEnergyCertificate : config.KeyEnergyCertificate
                            }),
                    columns      : Viz.Configuration.Columns.EnergyCertificateCategory
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.EnergyCertificateCategory.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("EnergyCertificateCategoryChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy               : function() {
                Viz.util.MessageBusMgr.unsubscribe("EnergyCertificateCategoryChange", this.reloadStore, this);
                Viz.grid.EnergyCertificateCategory.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortal.
             */
            onBeforeAddRecord       : function(grid, entity) {
                var KeyEnergyCertificate = this.KeyEnergyCertificate;
                if (KeyEnergyCertificate == '' || KeyEnergyCertificate == null) {
                    return false;
                }
                entity.KeyEnergyCertificate = KeyEnergyCertificate;
                return true;
            }

        });

Ext.reg("vizGridEnergyCertificateCategory", Viz.grid.EnergyCertificateCategory);
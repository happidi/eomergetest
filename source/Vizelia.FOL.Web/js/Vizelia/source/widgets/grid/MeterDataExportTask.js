﻿/**
 * @class Viz.grid.MeterDataExportTask
 * <p>
 * A grid exposing the business entity MeterDataExportTask
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.MeterDataExportTask = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor  : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    // cls : 'viz-grid-norowborder',
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_meter_data_export_task_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('MeterDataExportTask_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_meter_data_export_task_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('MeterDataExportTask_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_meter_data_export_task_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('MeterDataExportTask_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 600,
                            height           : 380,
                            xtype            : 'vizFormMeterDataExportTask',
                            messageSaveStart : 'MeterDataExportTaskChangeStart',
                            messageSaveEnd   : 'MeterDataExportTaskChange'
                        },
                        create      : {
                            title   : $lang("msg_meter_data_export_task_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_meter_data_export_task_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyMeterDataExportTask'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-meterdataexporttask-update',
                            serviceHandlerUpdateBatch : Viz.Services.MappingWCF.MeterDataExportTask_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.MeterDataExportTask(),
                    columns      : Viz.Configuration.Columns.MeterDataExportTask
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.MeterDataExportTask.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("MeterDataExportTaskChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy    : function() {
                Viz.util.MessageBusMgr.unsubscribe("MeterDataExportTaskChange", this.reloadStore, this);
                Viz.grid.MeterDataExportTask.superclass.onDestroy.apply(this, arguments);
            }

        });

Ext.reg("vizGridMeterDataExportTask", Viz.grid.MeterDataExportTask);
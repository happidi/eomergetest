﻿/**
 * @class Viz.grid.EWSDataAcquisitionEndpoint
 * <p>
 * A grid exposing the business entity EWSDataAcquisitionEndpoint
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.EWSDataAcquisitionEndpoint = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor                      : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_ewsdataacquisitionendpoint_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden  : !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_ewsdataacquisitionendpoint_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_restdataacquisitionendpoint_view"),
                                iconCls : 'viz-icon-small-display',
                                hidden  : !$authorized('@@@@ Data Acquistion'),
                                scope   : this,
                                handler : this.onDataAcquisitionEndpointDisplay
                            }],
                    store        : new Viz.store.EWSDataAcquisitionEndpoint({
                                KeyDataAcquisitionContainer : config.KeyDataAcquisitionContainer
                            }),
                    columns      : Viz.Configuration.Columns.EWSDataAcquisitionEndpoint
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.EWSDataAcquisitionEndpoint.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("EWSDataAcquisitionEndpointChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy                        : function() {
                Viz.util.MessageBusMgr.unsubscribe("EWSDataAcquisitionEndpointChange", this.reloadStore, this);
                Viz.grid.EWSDataAcquisitionEndpoint.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortal.
             */
            onBeforeAddRecord                : function(grid, entity) {
                var KeyDataAcquisitionContainer = this.KeyDataAcquisitionContainer;
                if (KeyDataAcquisitionContainer == '' || KeyDataAcquisitionContainer == null) {
                    return false;
                }
                entity.KeyDataAcquisitionContainer = KeyDataAcquisitionContainer;
                return true;
            },

            onAddRecord                      : function() {
                this.openTreeDetail({
                            windowTitle                     : $lang('msg_ewsdataacquisitionendpoint'),
                            windowWidth                     : 350,
                            windowHeight                    : 400,
                            windowIconCls                   : 'viz-icon-small-ewsdataacquisitionendpoint',
                            xtypeTreeDetail                 : 'vizTreeDataAcquisition',
                            checkboxExcludedTypesTreeDetail : ['Vizelia.FOL.BusinessEntities.EWSDataAcquisitionContainer', 'Vizelia.FOL.BusinessEntities.EWSDataAcquisitionFolder'],
                            saveAfterAdd                    : true,
                            idField                         : 'LocalId',
                            xtypeComponentExtraConfig       : {
                                serviceParams : {
                                    entityContainer : this.entityContainer
                                }
                            }
                        });
            },

            onDataAcquisitionEndpointDisplay : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    Viz.grid.RESTDataAcquisitionEndpoint.openRESTDataAcquisitionEndpointDisplay(entity, 800, 550);
                }
            }
        });
Ext.reg("vizGridEWSDataAcquisitionEndpoint", Viz.grid.EWSDataAcquisitionEndpoint);

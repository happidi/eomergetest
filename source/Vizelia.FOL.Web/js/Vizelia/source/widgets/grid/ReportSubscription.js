﻿/**
 * @class Viz.grid.ReportSubscription
 * <p>
 * A grid exposing the business entity ReportSubscription
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.ReportSubscription = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    enableRowDblClick : false,
                    hasRowEditor      : false,
                    stripeRows        : false,
                    // cls : 'viz-grid-norowborder',
                    hasToolbar        : false,
                    tbar              : [{
                                text    : $lang("msg_reportsubscription_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('ReportSubscription_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig        : {
                        common      : {
                            width            : 400,
                            height           : 250,
                            xtype            : 'vizFormReportSubscription',
                            messageSaveStart : 'ReportSubscriptionChangeStart',
                            messageSaveEnd   : 'ReportSubscriptionChange'
                        },
                        create      : {
                            title   : $lang("msg_reportsubscription_add"),
                            iconCls : 'viz-icon-small-reportsubscription-add'
                        },
                        update      : {
                            title               : $lang("msg_reportsubscription_update"),
                            iconCls             : 'viz-icon-small-reportsubscription-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyReportSubscription'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-reportsubscription-update',
                            serviceHandlerUpdateBatch : Viz.Services.ReportingWCF.ReportSubscription_FormUpdateBatch
                        }
                    },
                    store             : new Viz.store.ReportSubscription({
                                reportPath : config.reportPath
                            }),
                    columns           : Viz.Configuration.Columns.ReportSubscription
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.ReportSubscription.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("ReportSubscriptionChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("ReportSubscriptionChange", this.reloadStore, this);
                Viz.grid.ReportSubscription.superclass.onDestroy.apply(this, arguments);
            }

        });

Ext.reg("vizGridReportSubscription", Viz.grid.ReportSubscription);
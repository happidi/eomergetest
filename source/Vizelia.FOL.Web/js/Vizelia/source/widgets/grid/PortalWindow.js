﻿/**
 * @class Viz.grid.Portal
 * <p>
 * A grid exposing the business entity Portal
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.PortalWindow = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor           : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_portalwindow_add"),
                                iconCls : 'viz-icon-small-portalwindow-add',
                                hidden  : !$authorized('@@@@ Portal - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_portalwindow_update"),
                                iconCls : 'viz-icon-small-portalwindow-update',
                                hidden  : !$authorized('@@@@ Portal - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_portalwindow_delete"),
                                iconCls : 'viz-icon-small-portalwindow-delete',
                                hidden  : !$authorized('@@@@ Portal - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_portalwindow_copy"),
                                iconCls : 'viz-icon-small-copy',
                                hidden  : !$authorized('@@@@ Portal - Full Control'),
                                scope   : this,
                                handler : this.onCopyRecord
                            }, {
                                text    : $lang("msg_portalwindow_display"),
                                iconCls : 'viz-icon-small-display',
                                // hidden : !$authorized('PortalWindow_write'),
                                scope   : this,
                                handler : this.onPortalWindowDisplay
                            }],
                    formConfig   : this.getFormConfig(),
                    formModal    : false,
                    store        : new Viz.store.PortalWindow(),
                    columns      : Viz.Configuration.Columns.PortalWindow
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.PortalWindow.superclass.constructor.call(this, config);
                this.addEvents(
                        /**
                         * @event beforeportaldiplay Fires before displaying a portal.
                         * @param {Viz.grid.GridPanel} this.
                         * @param {Object} record The record of the portal that is being displayed.
                         * @param {Object} entity The entity of the portal that is being displayed. It could be changed by the handler.
                         */
                        'beforeportaldiplay');
                this.on('deleterecord', this.deleteRecordHandler, this);
                Viz.util.MessageBusMgr.subscribe("PortalWindowChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy             : function() {
                Viz.util.MessageBusMgr.unsubscribe("PortalWindowChange", this.reloadStore, this);
                Viz.grid.PortalWindow.superclass.onDestroy.apply(this, arguments);
            },
            // We fire the PortalWindowChange change when a portal is deleted to update the toolbar ans the desktop.
            deleteRecordHandler   : function() {
                Viz.util.MessageBusMgr.fireEvent.defer(300, Viz.util.MessageBusMgr, ['PortalWindowChange', {}]);
            },
            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig         : function() {
                return {
                    common      : {
                        width            : 550,
                        height           : 400,
                        xtype            : 'vizFormPortalWindow',
                        messageSaveStart : 'PortalWindowChangeStart',
                        messageSaveEnd   : 'PortalWindowChange',
                        readonly         : !$authorized('@@@@ Portal - Full Control') && !$authorized('@@@@ Portal - Wizard')
                    },
                    create      : {
                        title   : $lang("msg_portalwindow"),
                        iconCls : 'viz-icon-small-portalwindow-add'
                    },
                    update      : {
                        title               : $lang("msg_portalwindow_update"),
                        iconCls             : 'viz-icon-small-portalwindow-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyPortalWindow'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-portalwindow-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.PortalWindow_FormUpdateBatch
                    }
                };
            },
            /**
             * Handler for displaying a portal.
             * @param {bool} useSilverlight : true to display as silverlight, false otherwise
             */
            onPortalWindowDisplay : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    if (this.fireEvent("beforeportalwindowdisplay", this, record, entity)) {
                        Viz.grid.PortalWindow.openPortalWindowDisplay(entity, 800, 550);
                    }
                }
            },

            /**
             * @private Handler for copying a chart.
             */
            onCopyRecord          : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;

                var keys = new Array();
                for (var i = 0, r; r = selections[i]; i++) {
                    keys.push(r.data.KeyPortalWindow);
                }

                Viz.Services.EnergyWCF.PortalWindow_Copy({
                            keys    : keys,
                            success : function(data) {
                                this.reloadStore();
                                Viz.App.Global.ViewPort.updateApplicationToolbarPortalWindows();
                            },
                            scope   : this
                        });
            }

        });
Ext.reg("vizGridPortalWindow", Viz.grid.PortalWindow);

// Static Methods
Ext.apply(Viz.grid.PortalWindow, {
            /**
             * Displays a portal window.
             * @param {Viz.BusinessEntity.PortalWindow} entity : the portal entity.
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openPortalWindowDisplay : function(entity, width, height) {
                var win = null;
                Ext.WindowMgr.each(function(w) {
                            if (w.getXType() == 'vizPortalWindow' && w.KeyPortalWindow == entity.KeyPortalWindow) {
                                win = w;
                                Ext.WindowMgr.bringToFront(win);
                                return false;
                            }
                        }, this);
                if (!win) {
                    win = new Viz.portal.Window({
                                width           : width,
                                height          : height,
                                title           : entity.Title,
                                iconCls         : entity.IconClsSmall,
                                KeyPortalWindow : entity.KeyPortalWindow
                            });
                    win.show();
                }
            },

            /**
             * Create a new portalWindow (to be called from desktop)
             * @param {} callback
             */
            create                  : function(entity) {
                Viz.openFormCrud(Viz.grid.PortalWindow.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing portalWindow (to be called from desktop or tree)
             * @param {} callback
             */
            update                  : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.PortalWindow.prototype.getFormConfig(), 'update', entity);
                }
            },

            /**
             * Launch the chart wizard
             */
            launchWizard            : function() {
                var config = Viz.grid.PortalWindow.prototype.getFormConfig();
                config.common.isWizard = true;
                config.common.isWizardSingleTab = true;
                Viz.openFormCrud(config, 'create', {});
            },

            applySpatialFilter      : function(KeyPortalWindow) {
                var filterSpatial = Viz.App.Global.ViewPort.getCurrentSpatialFilter();
                if (filterSpatial.length > 0) {
                    Viz.App.Global.IconManager.maskDesktop();
                    Viz.App.Global.ViewPort.closePortalWindow(KeyPortalWindow);
                    Viz.Services.EnergyWCF.PortalWindow_ApplySpatialFilter({
                                Key           : KeyPortalWindow,
                                filterSpatial : filterSpatial,
                                success       : function(response) {
                                    Viz.App.Global.IconManager.unmaskDesktop();
                                },
                                failure       : function(response) {
                                    Viz.App.Global.IconManager.unmaskDesktop();
                                },
                                scope         : this
                            });
                }
            }

        });

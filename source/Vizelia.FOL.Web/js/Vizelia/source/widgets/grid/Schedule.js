﻿/**
 * @class Viz.grid.Schedule
 * <p>
 * A grid exposing the business entity Schedule
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Schedule = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor         : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang("msg_schedule_add"),
                                iconCls : 'viz-icon-small-schedule-add',
                                // hidden : !$authorized('Schedule_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_schedule_update"),
                                iconCls : 'viz-icon-small-schedule-update',
                                // hidden : !$authorized('Schedule_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_schedule_delete"),
                                iconCls : 'viz-icon-small-schedule-delete',
                                // hidden : !$authorized('Schedule_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                id           : 'timezone-view-local',
                                iconCls      : 'viz-icon-small-world',
                                enableToggle : true,
                                text         : $lang('Timezone'),
                                handler      : this.viewOnLocalCalendar,
                                scope        : this
                            }],

                    formModal    : false,
                    columns      : Viz.Configuration.Columns.Schedule
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.Schedule.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("ScheduleChange", this.reloadStore, this);

            },
            /**
             * Destroy
             * @method onDestroy
             * @private
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("ScheduleChange", this.reloadStore, this);
                Viz.grid.Schedule.superclass.onDestroy.apply(this, arguments);
            },
            viewOnLocalCalendar : function(btn) {
                if (btn.pressed) {
                    this.store.each(function(r) {
                                r.data.OrgStartDate = r.data.StartDate;

                                r.data.StartDate = Viz.date.convertToLocalDate(r.json['StartDate'], r.json['TimeZoneOffset'][0]);
                            }, this);

                    this.view.refresh(true);
                }
                else {
                    this.store.each(function(r) {
                                r.data.StartDate = r.data.OrgStartDate;
                            }, this);
                    this.view.refresh(true);
                }
            }
        });
Ext.reg("vizGridSchedule", Viz.grid.Schedule);
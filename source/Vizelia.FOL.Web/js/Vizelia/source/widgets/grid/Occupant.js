﻿/**
 * @class Viz.grid.Occupant
 * <p>
 * A grid exposing the business entity Occupant
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Occupant = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor     : false,
                    stripeRows       : false,
                    hasToolbar       : false,
                    allowupdatebatch : true,
                    viewConfig       : {
                        autoFill : true
                    },
                    tbar             : [{
                                text    : $lang("msg_occupant_add"),
                                iconCls : 'viz-icon-small-search-occupant-add',
                                hidden  : !$authorized('@@@@ Search - Occupants - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_occupant_update"),
                                iconCls : 'viz-icon-small-search-occupant-update',
                                hidden  : !$authorized('@@@@ Search - Occupants - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_occupant_delete"),
                                iconCls : 'viz-icon-small-search-occupant-delete',
                                hidden  : !$authorized('@@@@ Search - Occupants - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig       : {
                        historyEnabled : true,
                        common         : {
                            width            : 600,
                            height           : 400,
                            xtype            : 'vizFormOccupant',
                            messageSaveStart : 'OccupantChangeStart',
                            messageSaveEnd   : 'OccupantChange',
                            readonly         : !$authorized('@@@@ Search - Occupants - Full Control')

                        },
                        create         : {
                            title   : $lang("msg_occupant_add"),
                            iconCls : 'viz-icon-small-search-occupant-add'
                        },
                        update         : {
                            title               : $lang("msg_occupant_update"),
                            iconCls             : 'viz-icon-small-search-occupant-update',
                            titleEntity         : 'Id',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyOccupant'
                                    }]
                        },
                        updatebatch    : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-search-occupant-update',
                            serviceHandlerUpdateBatch : Viz.Services.CoreWCF.Occupant_FormUpdateBatch
                        }
                    },
                    store            : new Viz.store.Occupant(),
                    plugins          : [new Viz.plugins.GridExport({
                                paging          : {
                                    start : 0,
                                    sort  : 'LastName',
                                    dir   : 'ASC'

                                },
                                maxRecords      : 1000,
                                promptMaxRecord : true
                            })],
                    columns          : Viz.Configuration.Columns.Occupant
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.Occupant.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("OccupantChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("OccupantChange", this.reloadStore, this);
                Viz.grid.Occupant.superclass.onDestroy.apply(this, arguments);
            }
        });
Ext.reg("vizGridOccupant", Viz.grid.Occupant);

﻿/**
 * @class Viz.grid.AlarmTable
 * <p>
 * A grid exposing the business entity AlarmTable
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.AlarmTable = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor         : function(config) {
                config = config || {};

                this._favoritebutton_id = Ext.id();

                var defaultConfig = {
                    hasRowEditor  : false,
                    stripeRows    : false,
                    hasToolbar    : false,
                    autoLoadStore : false,
                    tbar          : [{
                                id            : this._favoritebutton_id,
                                iconCls       : 'viz-icon-small-chart-favorite',
                                text          : $lang("msg_chart_favorite"),
                                enableToggle  : true,
                                toggleHandler : function(button, state) {
                                    Viz.util.Portal.onToggleFavoriteButton.createDelegate(this, [button, state])();
                                },
                                scope         : this,
                                pressed       : true
                            }, '-', {
                                text    : $lang("msg_alarmtable_add"),
                                iconCls : 'viz-icon-small-alarmtable-add',
                                hidden  : !$authorized('@@@@ Alarm Table - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_alarmtable_update"),
                                iconCls : 'viz-icon-small-alarmtable-update',
                                hidden  : !$authorized('@@@@ Alarm Table - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_alarmtable_delete"),
                                iconCls : 'viz-icon-small-alarmtable-delete',
                                hidden  : !$authorized('@@@@ Alarm Table - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_alarmtable_view"),
                                iconCls : 'viz-icon-small-display',
                                // hidden : !$authorized('AlarmTable_view'),
                                scope   : this,
                                handler : this.onAlarmTableDisplay
                            }, '-', {
                                text    : $lang("msg_portlets"),
                                iconCls : 'viz-icon-small-portlet',
                                hidden  : !$authorized('@@@@ Portlet'),
                                scope   : this,
                                handler : Viz.util.Portal.displayPortletGridFromEntity.createDelegate(this)
                            }],
                    formConfig    : this.getFormConfig(),
                    store         : new Viz.store.AlarmTable(),
                    columns       : Viz.Configuration.Columns.AlarmTable
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.AlarmTable.superclass.constructor.call(this, config);
                this.addEvents(
                        /**
                         * @event beforealarmtabledisplay Fires before displaying a AlarmTable.
                         * @param {Viz.grid.GridPanel} this.
                         * @param {Object} record The record of the AlarmTable that is being displayed.
                         * @param {Object} entity The entity of the AlarmTable that is being displayed. It could be changed by the handler.
                         */
                        'beforealarmtabledisplay');

                Viz.util.MessageBusMgr.subscribe("AlarmTableChange", this.reloadStore, this);

                this.on({
                            render          : Viz.util.Portal.gridRender,
                            filteravailable : Viz.util.Portal.gridFilterAvailable,
                            filtercleared   : Viz.util.Portal.toggleFavoriteButton.createDelegate(this, [this._favoritebutton_id, false]),
                            scope           : this
                        });
            },
            /**
             * Destroy
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("AlarmTableChange", this.reloadStore, this);
                Viz.grid.AlarmTable.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * After deleting we make sure the portlet is deleted as well.
             * @param {} record
             * @param {} selections
             */
            onAfterDeleteRecord : function(record, selections) {
                for (var i = 0, r; r = selections[i]; i++) {
                    Viz.util.MessageBusMgr.fireEvent('PortletDelete', r.json);
                }
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig       : function() {
                return {
                    common      : {
                        width            : 600,
                        height           : 450,
                        xtype            : 'vizFormAlarmTable',
                        messageSaveStart : 'AlarmTableChangeStart',
                        messageSaveEnd   : 'AlarmTableChange',
                        readonly         : !$authorized('@@@@ Alarm Table - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_alarmtable_add"),
                        iconCls : 'viz-icon-small-alarmtable-add'
                    },
                    update      : {
                        title               : $lang("msg_alarmtable_update"),
                        iconCls             : 'viz-icon-small-alarmtable-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyAlarmTable'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-alarmtable-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.AlarmTable_FormUpdateBatch
                    }
                };
            },
            /**
             * Handler for displaying a alarmtable.
             * @method onChartDisplay
             */
            onAlarmTableDisplay : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    if (this.fireEvent("beforealarmtabledisplay", this, record, entity)) {
                        Viz.grid.AlarmTable.openAlarmTableDisplay(entity);
                    }
                }
            }
        });
Ext.reg("vizGridAlarmTable", Viz.grid.AlarmTable);

// Static Methods
Ext.apply(Viz.grid.AlarmTable, {
            /**
             * Displays a AlarmTable.
             * @method openAlarmTableDisplay
             * @static
             * @param {Viz.BusinessEntity.AlarmTable} entity : the AlarmTable entity.
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openAlarmTableDisplay : function(entity, width, height) {
                var win = new Viz.portal.PortletWindow({
                            entity        : entity,
                            xtypePortlet  : 'vizPortletAlarmTable',
                            entityPortlet : {
                                TypeEntity : 'Vizelia.FOL.BusinessEntities.AlarmTable'
                            },
                            readonly      : true,
                            updateHandler : Viz.grid.AlarmTable.update
                        });
                win.show();
            },

            /**
             * Create a new alarmtable (to be called from desktop or tree)
             * @param {} callback
             */
            create                : function() {
                Viz.openFormCrud(Viz.grid.AlarmTable.prototype.getFormConfig(), 'create', {});
            },

            /**
             * Update an existing alarmtable (to be called from desktop or tree)
             * @param {} callback
             */
            update                : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.AlarmTable.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
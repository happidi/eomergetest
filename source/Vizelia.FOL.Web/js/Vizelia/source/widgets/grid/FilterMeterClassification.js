﻿/**
 * @class Viz.grid.FilterMeterClassification
 * <p>
 * A grid exposing a list of Meter Classification
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterMeterClassification = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor       : false,
                    stripeRows         : false,
                    hasToolbar         : false,
                    ddGroup            : 'ClassificationDD',
                    enableDropFromTree : true,
                    tbar               : [{
                                text    : $lang('msg_meter_classification_add'),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterMeterClassification_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_meter_classification_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterMeterClassification_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],
                    columns            : Viz.Configuration.Columns.ClassificationItem
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterMeterClassification.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("FilterMeterClassificationChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterMeterClassificationlChange", this.reloadStore, this);
                Viz.grid.FilterMeterClassification.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord : function() {
                var entity = {};
                if (this.fireEvent("beforeaddrecord", this, entity)) {
                    this.openTreeDetail({
                                windowTitle               : $lang('msg_meter_classification'),
                                windowWidth               : 350,
                                windowHeight              : 400,
                                windowIconCls             : 'viz-icon-small-classificationitem',
                                xtypeTreeDetail           : 'vizTreeMeterClassification',
                                xtypeComponentExtraConfig : {
                                    KeyChart           : entity.KeyChart,
                                    KeyAlarmDefinition : entity.KeyAlarmDefinition,
                                    KeyMeterValidationRule : entity.KeyMeterValidationRule,
                                    filterSpatial      : entity.filterSpatial
                                },
                                keyColumn                 : 'KeyClassificationItem'
                            });
                }
            }
        });

Ext.reg("vizGridFilterMeterClassification", Viz.grid.FilterMeterClassification);
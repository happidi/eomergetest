﻿/**
 * @class Viz.grid.PortalTemplate
 * <p>
 * A grid exposing the business entity PortalTemplate
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.PortalTemplate = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor           : function(config) {
                this.btnPushUpdatesId = Ext.id();
                this.btnPushUpdatesAdvancedId = Ext.id();
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_portaltemplate_add"),
                                iconCls : 'viz-icon-small-portaltemplate-add',
                                hidden  : !$authorized('@@@@ PortalTemplate - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_portaltemplate_update"),
                                iconCls : 'viz-icon-small-portaltemplate-update',
                                hidden  : !$authorized('@@@@ PortalTemplate - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_portaltemplate_delete"),
                                iconCls : 'viz-icon-small-portaltemplate-delete',
                                hidden  : !$authorized('@@@@ PortalTemplate - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                xtype    : 'splitbutton',
                                text     : $lang("msg_portaltemplate_pushupdates"),
                                iconCls  : 'viz-icon-small-action',
                                hidden   : !$authorized('@@@@ PortalTemplate - Full Control'),
                                scope    : this,
                                handler  : this.onPushUpdates,
                                id       : this.btnPushUpdatesId,
                                disabled : true,
                                menu     : new Ext.menu.Menu({
                                            items : [{
                                                        text     : $lang("msg_portaltemplate_pushupdates_advanced") + '...',
                                                        iconCls  : 'viz-icon-small-action',
                                                        hidden   : !$authorized('@@@@ PortalTemplate - Full Control'),
                                                        scope    : this,
                                                        handler  : this.onPushUpdatesAdvanced,
                                                        id       : this.btnPushUpdatesAdvancedId,
                                                        disabled : true
                                                    }]
                                        })
                            }],
                    formConfig   : {
                        common      : {
                            width            : 740,
                            height           : 430,
                            xtype            : 'vizFormPortalTemplate',
                            messageSaveStart : 'PortalTemplateChangeStart',
                            messageSaveEnd   : 'PortalTemplateChange',
                            readonly         : !$authorized('@@@@ PortalTemplate - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_portaltemplate_add"),
                            iconCls : 'viz-icon-small-portaltemplate-add'
                        },
                        update      : {
                            title               : $lang("msg_portaltemplate_update"),
                            iconCls             : 'viz-icon-small-portaltemplate-update',
                            titleEntity         : 'Title',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyPortalTemplate'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-portaltemplate-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.PortalTemplate_FormUpdateBatch
                        }
                    },
                    formModal    : false,
                    store        : new Viz.store.PortalTemplate(),
                    columns      : Viz.Configuration.Columns.PortalTemplate,
                    sm           : new Ext.grid.RowSelectionModel({
                                listeners : {
                                    selectionchange : {
                                        fn    : this.onSelectionChange,
                                        scope : this
                                    }
                                }
                            })
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.PortalTemplate.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("PortalTemplateChange", this.reloadStore, this);
            },
            /**
             * Destroy
             * @method onDestroy
             * @private
             */
            onDestroy             : function() {
                Viz.util.MessageBusMgr.unsubscribe("PortalTemplateChange", this.reloadStore, this);
                Viz.grid.PortalTemplate.superclass.onDestroy.apply(this, arguments);
            },

            onSelectionChange     : function() {
                var simpleDisabled;
                var advancedDisabled;

                var selections = this.getSelectionModel().getSelections();
                if (selections.length <= 0) {
                    simpleDisabled = true;
                    advancedDisabled = true;
                }
                else if (selections.length > 1) {
                    simpleDisabled = false;
                    advancedDisabled = true;
                }
                else {
                    simpleDisabled = false;
                    advancedDisabled = false;
                }

                Ext.getCmp(this.btnPushUpdatesId).setDisabled(simpleDisabled);
                Ext.getCmp(this.btnPushUpdatesAdvancedId).setDisabled(advancedDisabled);
            },

            /**
             * Push the modification made to the associated portalwindows to thz associated users.
             */
            onPushUpdates         : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;

                var msgDelete;
                if (selections.length > 1)
                    msgDelete = String.format($lang('msg_portaltemplate_pushupdates_confirmation_multiple'), selections.length);
                else
                    msgDelete = $lang('msg_portaltemplate_pushupdates_confirmation_single');

                Ext.MessageBox.confirm($lang('msg_portaltemplate_pushupdates'), msgDelete, function(button) {
                            if (button == 'yes') {
                                var keys = [];
                                for (var i = 0, r; r = selections[i]; i++) {
                                    keys.push(r.json.KeyPortalTemplate);
                                }
                                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                            pollingInterval        : 1000,
                                            serviceStart           : Viz.Services.EnergyWCF.PortalTemplate_PushUpdatesBegin,
                                            serviceParams          : {
                                                Keys : keys
                                            },
                                            showProgress           : true,
                                            showProgressPercentage : true,
                                            modal                  : false,
                                            progressWindowTitle    : $lang('msg_portaltemplate_pushupdates')
                                        });
                                submitter.start();
                            }

                        }, this);
            },

            onPushUpdatesAdvanced : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length != 1)
                    return;

                var keyPortalTemplate = selections[0].json.KeyPortalTemplate;
                var win = new Ext.Window({
                            title       : $lang('msg_portaltemplate_pushupdates_user_manager'),
                            width       : 1000,
                            maximizable : true,
                            height      : 400,
                            layout      : 'fit',
                            iconCls     : 'viz-icon-small-user',
                            items       : [{
                                        xtype             : 'vizFormUserPortalTemplate',
                                        keyPortalTemplate : keyPortalTemplate
                                    }]
                        });

                win.show();
            },

            /**
             * Push the modification made to the associated portalwindows to thz associated users.
             */
            onDeleteRecord        : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;

                var msgDelete;
                if (selections.length > 1)
                    msgDelete = String.format($lang('msg_record_delete_confirmation_multiple'), selections.length);
                else
                    msgDelete = $lang('msg_record_delete_confirmation_single');

                Ext.MessageBox.confirm($lang('msg_portaltemplate_delete'), msgDelete, function(button) {
                            if (button == 'yes') {
                                var keys = [];
                                for (var i = 0, r; r = selections[i]; i++) {
                                    keys.push(r.json.KeyPortalTemplate);
                                }
                                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                            pollingInterval        : 1000,
                                            serviceStart           : Viz.Services.EnergyWCF.PortalTemplate_DeleteBegin,
                                            serviceParams          : {
                                                Keys : keys
                                            },
                                            showProgress           : true,
                                            showProgressPercentage : false,
                                            modal                  : false,
                                            progressWindowTitle    : $lang('msg_portaltemplate_delete'),
                                            completionHandler      : function() {
                                                this.store.reload();
                                            },
                                            scope                  : this
                                        });
                                submitter.start();
                            }

                        }, this);
            }

        });
Ext.reg("vizGridPortalTemplate", Viz.grid.PortalTemplate);
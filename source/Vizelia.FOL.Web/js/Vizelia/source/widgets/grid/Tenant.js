﻿/**
 * @class Viz.grid.Tenant
 * <p>
 * A grid exposing the business entity Tenant
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Tenant = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor                : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    cls          : 'viz-grid-norowborder',
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_tenant_add"),
                                iconCls : 'viz-icon-small-tenant-add',
                                // hidden : !$authorized('Tenant_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_tenant_update"),
                                iconCls : 'viz-icon-small-tenant-update',
                                // hidden : !$authorized('Tenant_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_tenant_delete"),
                                iconCls : 'viz-icon-small-tenant-delete',
                                // hidden : !$authorized('Tenant_write'),
                                scope   : this,
                                handler : this.onDeleteTenantContent
                            }, {
                                text    : $lang("msg_tenant_initialize"),
                                iconCls : 'viz-icon-small-tenant',
                                // hidden : !$authorized('Tenant_write'),
                                scope   : this,
                                handler : this.onInitializeTenant
                            }],
                    formConfig   : {
                        common      : {
                            width            : 400,
                            height           : 220,
                            xtype            : 'vizFormTenant',
                            messageSaveStart : 'TenantChangeStart',
                            messageSaveEnd   : 'TenantChange'
                        },
                        create      : {
                            title   : $lang("msg_tenant_add"),
                            iconCls : 'viz-icon-small-tenant-add'
                        },
                        update      : {
                            title               : $lang("msg_tenant_update"),
                            iconCls             : 'viz-icon-small-tenant-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyTenant'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-tenant-update',
                            serviceHandlerUpdateBatch: Viz.Services.TenancyWCF.Tenant_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.Tenant(),

                    columns      : Viz.Configuration.Columns.Tenant

                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.Tenant.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("TenantChange", this.reloadStore, this);
            },

            /**
             * Handler for the Delete Tenant Content button.
             */
            onDeleteTenantContent      : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length != 1)
                    return;

                var msgDelete = $lang('msg_tenant_delete_all_content_confirm');
                Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                            if (button == 'yes') {
                                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                    serviceStart: Viz.Services.TenancyWCF.Tenant_Delete,
                                            serviceParams     : {
                                                keyTenant : selections[0].data.KeyTenant
                                            },
                                            showProgress      : true,
                                            completionHandler : function() {
                                                this.store.reload();
                                                Ext.MessageBox.show({
                                                            title   : $lang('msg_long_running_operation_result'),
                                                            msg     : $lang('msg_tenant_delete_all_content_complete'),
                                                            buttons : Ext.MessageBox.OK,
                                                            icon    : Ext.MessageBox.INFO
                                                        });
                                            }.createDelegate(this)
                                        });

                                submitter.start();
                            }
                        }, this);
            },

            /**
             * Destroy
             */
            onDestroy                  : function() {
                Viz.util.MessageBusMgr.unsubscribe("TenantChange", this.reloadStore, this);
                Viz.grid.Tenant.superclass.onDestroy.apply(this, arguments);
            },
            onInitializeTenant         : function(targetAction) {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                if (selections.length > 1 && this.formConfig.updatebatch) {

                    Ext.MessageBox.alert({
                                title   : $lang('msg_unsupported'),
                                message : $lang('msg_tenant_multiple_init_unsupported')
                            });
                }
                else {
                    var record = this.getSelectionModel().getSelected();
                    if (record) {
                        var entity = record.json;
                        if (!entity.IsInitialized) {

                            var fieldpasswordId = Ext.id();
                            var myWindow = new Ext.Window({
                                        title   : $lang('msg_tenant_initialize'),
                                        height  : 135,
                                        width   : 340,
                                        items   : [{
                                                    xtype                : 'form',
                                                    frame                : true,
                                                    border               : false,
                                                    bodyStyle            : 'padding: 5px;background-color: transparent;',
                                                    labelWidth           : 150,
                                                    hasValidationToolbar : true,
                                                    defaultType          : 'textfield',
                                                    defaults             : {
                                                        msgTarget : 'side',
                                                        anchor    : Viz.Const.UI.Anchor
                                                    },
                                                    items                : [{
                                                                fieldLabel : $lang('msg_tenant_admin_password'),
                                                                id         : fieldpasswordId,
                                                                inputType  : 'password',
                                                                name       : 'AdminPassword',
                                                                allowBlank : false,
                                                                emptyText  : 'This information is protected'
                                                            }, {
                                                                fieldLabel           : $lang('msg_password_confirm'),
                                                                name                 : 'confirmPassword',
                                                                validator            : Ext.form.VTypes.passwordconfirmValidator,
                                                                inputType            : 'password',
                                                                initialPasswordField : fieldpasswordId,
                                                                emptyText            : 'This information is protected',
                                                                allowBlank           : false
                                                            }]
                                                }],
                                        buttons : [{
                                                    text    : 'OK',
                                                    handler : function() {
                                                        Ext.ux.ScriptMgr.load({
                                                                    jsonp        : true,
                                                                    scripts      : [document.location.protocol + '//' + entity.Url + (entity.Url.endsWith('/') ? '' : '/') + 'Public.svc/InitializeTenant'],
                                                                    params       : {
                                                                        operationId   : new Ext.ux.GUID().valueOf(),
                                                                        adminPassword : Ext.get(fieldpasswordId).dom.value,
                                                                        tenantName    : entity.Name
                                                                    },
                                                                    timeout      : 2,
                                                                    expectResult : false,
                                                                    callback     : function() {
                                                                        Ext.Msg.show({
                                                                                    title   : $lang('msg_wait'),
                                                                                    msg     : $lang('msg_tenant_initializing_please_refresh_later'),
                                                                                    buttons : Ext.MessageBox.OK,
                                                                                    icon    : Ext.MessageBox.INFO
                                                                                });
                                                                    }
                                                                });
                                                        myWindow.close();
                                                    }
                                                }, {
                                                    text    : 'Cancel',
                                                    handler : function() {
                                                        myWindow.close();
                                                    }
                                                }]

                                    }).show();
                            /*
                             * Ext.Msg.prompt($lang('msg_tenant_initialize'), $lang('msg_password'), function(btn, text) { if (btn == 'ok') { Ext.ux.ScriptMgr.load({ jsonp : true, scripts : [(entity.Url.startsWith('http://') ? '' : 'http://') + entity.Url + (entity.Url.endsWith('/') ? '' : '/') + 'Public.svc/InitializeTenant'], params : { operationId : new Ext.ux.GUID().valueOf(), adminPassword : text, tenantName : entity.Name }, timeout : 2, expectResult : false, callback : function() { Ext.Msg.show({ title : $lang('msg_wait'), msg : $lang('msg_tenant_initializing_please_refresh_later'), buttons : Ext.MessageBox.OK, icon : Ext.MessageBox.INFO }); } }); } });
                             */
                        }
                        else {
                            Ext.Msg.alert($lang('msg_tenant_initialize'), $lang('msg_tenant_already_initialized'));
                        }
                    }
                }
            },
            invokeTenantInitialization : function(tenant) {

            }
        });

Ext.reg("vizGridTenant", Viz.grid.Tenant);

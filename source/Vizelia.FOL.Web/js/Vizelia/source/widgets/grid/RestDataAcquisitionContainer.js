﻿/**
 * @class Viz.grid.RESTDataAcquisitionContainer
 * <p>
 * A grid exposing the business entity RESTDataAcquisitionContainer
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.RESTDataAcquisitionContainer = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor   : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_restdataacquisitioncontainer_add"),
                                iconCls : 'viz-icon-small-dataacquisitioncontainer-add',
                                hidden  : !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_restdataacquisitioncontainer_update"),
                                iconCls : 'viz-icon-small-dataacquisitioncontainer-update',
                                hidden  : !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_restdataacquisitioncontainer_delete"),
                                iconCls : 'viz-icon-small-dataacquisitioncontainer-delete',
                                hidden  : !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : function(button, event) {
                                    this.onDeleteRecord(button, event, $lang("msg_container_delete_confirmation_single"), $lang("msg_container_delete_confirmation_multiple"));
                                }
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : new Viz.store.RESTDataAcquisitionContainer(),
                    columns      : Viz.Configuration.Columns.RESTDataAcquisitionContainer
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.RESTDataAcquisitionContainer.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("RESTDataAcquisitionContainerChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy     : function() {
                Viz.util.MessageBusMgr.unsubscribe("RESTDataAcquisitionContainerChange", this.reloadStore, this);
                Viz.grid.RESTDataAcquisitionContainer.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig : function() {
                return {
                    common      : {
                        width            : 650,
                        height           : 525,
                        xtype            : 'vizFormRESTDataAcquisitionContainer',
                        messageSaveStart : 'RESTDataAcquisitionContainerChangeStart',
                        messageSaveEnd   : 'RESTDataAcquisitionContainerChange',
                        readonly         : !$authorized('@@@@ Data Acquistion - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_restdataacquisitioncontainer_add"),
                        iconCls : 'viz-icon-small-dataacquisitioncontainer-add'
                    },
                    update      : {
                        title               : $lang("msg_restdataacquisitioncontainer_update"),
                        iconCls             : 'viz-icon-small-dataacquisitioncontainer-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyDataAcquisitionContainer'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-dataacquisitioncontainer-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.RESTDataAcquisitionContainer_FormUpdateBatch
                    }
                };
            }
        });
Ext.reg("vizGridRESTDataAcquisitionContainer", Viz.grid.RESTDataAcquisitionContainer);

// Static Methods
Ext.apply(Viz.grid.RESTDataAcquisitionContainer, {

            /**
             * Create a new restdataacquisitioncontainer (to be called from desktop or tree)
             * @param {} callback
             */
            create : function() {
                Viz.openFormCrud(Viz.grid.RESTDataAcquisitionContainer.prototype.getFormConfig(), 'create', {});
            },

            /**
             * Update an existing restdataacquisitioncontainer (to be called from desktop or tree)
             * @param {} callback
             */
            update : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.RESTDataAcquisitionContainer.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
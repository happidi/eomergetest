﻿/**
 * @class Viz.grid.FilterAlarmDefinition
 * <p>
 * A grid exposing the business entity FilterAlarmDefinition
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterAlarmDefinition = Ext.extend(Viz.grid.GridPanel, {

            /**
             * @param {Boolean} isSecurableEntity <t>true</t> if the store is a SecurableObject store, <t>false</t> (default) otherwise
             * @type Boolean
             */
            isSecurableEntity       : false,
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang("msg_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterAlarmDefinition_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterAlarmDefinition_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],

                    columns      : config.isSecurableEntity ? Viz.Configuration.Columns.SecurableEntity : Viz.Configuration.Columns.AlarmDefinition

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterAlarmDefinition.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("FilterAlarmDefinitionChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy               : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterAlarmDefinitionChange", this.reloadStore, this);
                Viz.grid.FilterAlarmDefinition.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord             : function() {
                this.openGridDetail({
                            windowTitle     : $lang('msg_alarmdefinition'),
                            windowWidth     : 1000,
                            windowHeight    : 400,
                            windowIconCls   : 'viz-icon-small-alarm',
                            xtypeGridDetail : 'vizGridAlarmDefinition'
                        });
            },
            /**
             * Return the data to add to the store from the detail grid selected record.
             */
            getEntityFromGridRecord : function(r) {
                if (this.isSecurableEntity) {
                    var data = {
                        KeySecurable      : r.data.KeyAlarmDefinition,
                        SecurableName     : r.data.Title,
                        SecurableTypeName : 'Vizelia.FOL.BusinessEntities.AlarmDefinition',
                        SecurableIconCls  : 'viz-icon-small-alarm'
                    };
                    return data;
                }
                else {
                    return Viz.grid.FilterAlarmDefinition.superclass.getEntityFromGridRecord.apply(this, arguments);
                }
            }
        });

Ext.reg("vizGridFilterAlarmDefinition", Viz.grid.FilterAlarmDefinition);
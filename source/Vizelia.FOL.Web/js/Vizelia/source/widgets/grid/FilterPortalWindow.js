﻿/**
 * @class Viz.grid.FilterPortalWindow
 * <p>
 * A grid exposing the business entity FilterPortalWindow
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterPortalWindow = Ext.extend(Viz.grid.GridPanel, {
            /**
             * @param {Boolean} isSecurableEntity <t>true</t> if the store is a SecurableObject store, <t>false</t> (default) otherwise
             * @type Boolean
             */
            isSecurableEntity       : false,
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang("msg_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterPortalWindow_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterPortalWindow_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],

                    columns      : config.isSecurableEntity ? Viz.Configuration.Columns.SecurableEntity : Viz.Configuration.Columns.PortalWindow

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterPortalWindow.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("FilterPortalWindowChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy               : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterPortalWindowChange", this.reloadStore, this);
                Viz.grid.FilterPortalWindow.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord             : function() {
                this.openGridDetail({
                            windowTitle     : $lang('msg_portalwindow'),
                            windowWidth     : 1000,
                            windowHeight    : 400,
                            windowIconCls   : 'viz-icon-small-portalwindow',
                            xtypeGridDetail : 'vizGridPortalWindow'
                        });
            },
            /**
             * Return the data to add to the store from the detail grid selected record.
             */
            getEntityFromGridRecord : function(r) {
                if (this.isSecurableEntity) {
                    var data = {
                        KeySecurable      : r.data.KeyPortalWindow,
                        SecurableName     : r.data.Title,
                        SecurableTypeName : 'Vizelia.FOL.BusinessEntities.PortalWindow',
                        SecurableIconCls  : 'viz-icon-small-portalwindow'
                    };
                    return data;
                }
                else {
                    return Viz.grid.FilterPortalWindow.superclass.getEntityFromGridRecord.apply(this, arguments);
                }
            }
        });

Ext.reg("vizGridFilterPortalWindow", Viz.grid.FilterPortalWindow);
﻿/**
 * @class Viz.grid.FilterSpatialPsetLocation
 * <p>
 * A grid exposing the business entity Location
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterSpatialPsetLocation = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    readonly     : true,
                    viewConfig   : {
                        markDirty : false
                    },
                    store        : config.store || config.KeyChart ? new Viz.store.ChartFilterSpatialPsetLocation({
                                KeyChart : config.KeyChart
                            }) : new Viz.store.AlarmDefinitionFilterSpatialPsetLocation({
                                KeyAlarmDefinition : config.KeyAlarmDefinition
                            }),
                    columns      : Viz.Configuration.Columns.FilterSpatial
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterSpatialPsetLocation.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("FilterSpatialPsetChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterSpatialPsetChange", this.reloadStore, this);
                Viz.grid.FilterSpatialPsetLocation.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent Key.
             */
            onBeforeAddRecord : function(grid, entity) {
                return false;
            }

        });

Ext.reg("vizGridFilterSpatialPsetLocation", Viz.grid.FilterSpatialPsetLocation);
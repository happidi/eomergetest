﻿/**
 * @class Viz.grid.Algorithm
 * <p>
 * A grid exposing the business entity Algorithm
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Algorithm = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor          : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor  : false,
                    hasToolbar    : false,
                    autoLoadStore : true,
                    tbar          : [{
                        text: $lang("msg_algorithm_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden: !$authorized('@@@@ Algorithm - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text: $lang("msg_algorithm_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden: !$authorized('@@@@ Algorithm - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text: $lang("msg_algorithm_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden: !$authorized('@@@@ Algorithm - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig    : this.getFormConfig(),
                    store: new Viz.store.Algorithm(),
                    columns: Viz.Configuration.Columns.Algorithm
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.Algorithm.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("AlgorithmChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy            : function() {
                Viz.util.MessageBusMgr.unsubscribe("AlgorithmChange", this.reloadStore, this);
                Viz.grid.Algorithm.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig        : function() {
                return {
                    common      : {
                        width            : 750,
                        height           : 570,
                        xtype: 'vizFormAlgorithm',
                        messageSaveStart: 'AlgorithmChangeStart',
                        messageSaveEnd: 'AlgorithmChange',
                        readonly: !$authorized('@@@@ Algorithm - Full Control')
                    },
                    create      : {
                        title: $lang("msg_algorithm_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title: $lang("msg_algorithm_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'Name',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value: 'KeyAlgorithm'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch: Viz.Services.EnergyWCF.Algorithm_FormUpdateBatch
                    }
                };
            },

            /**
             * @private Handler for beforeupdaterecord event.
             */
            onBeforeUpdateRecord : function(grid, record, entity) {
                return !record.data.IsNotDynamicallyCompiled;
            }

        });
        Ext.reg("vizGridAlgorithm", Viz.grid.Algorithm);
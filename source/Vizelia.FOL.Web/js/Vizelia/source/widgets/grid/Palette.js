﻿/**
 * @class Viz.grid.Palette
 * <p>
 * A grid exposing the business entity Palette
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Palette = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor   : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_palette_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden  : !$authorized('@@@@ Palette - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_palette_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden  : !$authorized('@@@@ Palette - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_palette_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ Palette - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : new Viz.store.Palette(),
                    columns      : Viz.Configuration.Columns.Palette
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.Palette.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("PaletteChange", this.reloadStore, this);
            },

            getFormConfig : function() {
                return {
                    common      : {
                        width            : 500,
                        height           : 300,
                        xtype            : 'vizFormPalette',
                        messageSaveStart : 'PaletteChangeStart',
                        messageSaveEnd   : 'PaletteChange',
                        readonly         : !$authorized('@@@@ Palette - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_palette_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_palette_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'Label',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyPalette'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.Palette_FormUpdateBatch
                    }
                };
            },

            /**
             * Destroy
             */
            onDestroy     : function() {
                Viz.util.MessageBusMgr.unsubscribe("PaletteChange", this.reloadStore, this);
                Viz.grid.Palette.superclass.onDestroy.apply(this, arguments);
            }
        });
Ext.reg("vizGridPalette", Viz.grid.Palette);
﻿/**
 * @class Viz.grid.VirtualFile
 * <p>
 * A grid exposing the business entity VirtualFile
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.VirtualFile = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_virtualfile_add"),
                                iconCls: 'viz-icon-small-virtualfile-add',
                                hidden  : !$authorized('@@@@ Virtual File - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_virtualfile_update"),
                                iconCls: 'viz-icon-small-virtualfile-update',
                                hidden  : !$authorized('@@@@ Virtual File - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_virtualfile_delete"),
                                iconCls: 'viz-icon-small-virtualfile-delete',
                                hidden  : !$authorized('@@@@ Virtual File - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 600,
                            height           : 220,
                            xtype            : 'vizFormVirtualFile',
                            messageSaveStart : 'VirtualFileChangeStart',
                            messageSaveEnd   : 'VirtualFileChange',
                            readonly         : !$authorized('@@@@ Virtual File - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_virtualfile_add"),
                            iconCls: 'viz-icon-small-virtualfile-add'
                        },
                        update      : {
                            title               : $lang("msg_virtualfile_update"),
                            iconCls: 'viz-icon-small-virtualfile-update',
                            titleEntity         : 'Path',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyVirtualFile'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls: 'viz-icon-small-virtualfile-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.VirtualFile_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.VirtualFile(),
                    columns      : Viz.Configuration.Columns.VirtualFile
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.VirtualFile.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("VirtualFileChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("VirtualFileChange", this.reloadStore, this);
                Viz.grid.VirtualFile.superclass.onDestroy.apply(this, arguments);
            }

        });

Ext.reg("vizGridVirtualFile", Viz.grid.VirtualFile);
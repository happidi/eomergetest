﻿/**
 * @class Viz.grid.JobInstance
 * <p>
 * A grid exposing the business entity JobInstance
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.JobInstance = Ext.extend(Viz.grid.GridPanel, {

    /**
    * Ctor.
    * @param {Object} config
    */
    constructor: function (config) {
        config = config || {};
        var defaultConfig = {
            hasRowEditor: false,
            stripeRows: false,

            // cls : 'viz-grid-norowborder',
            store: new Viz.store.JobInstance({ KeyJob: config.KeyJob }),
            //autoExpandColumn: 'jobRunInstanceRequestIdColumn',
            autoExpandMax: 3000,
            viewConfig: {
                forceFit: false,
                emptyText: $lang('msg_jobinstance_no_entries')
            },
            columns: Viz.Configuration.Columns.JobInstance,
            hasToolbar: false,
            plugins: [new Viz.plugins.GridExport({
                paging: {
                    start: 0,
                    sort: 'EndDate',
                    dir: 'DESC'

                },
                maxRecords: 1000,
                promptMaxRecord: true
            })]
        };
        defaultConfig.store.groupField = 'EndDate';
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.grid.JobInstance.superclass.constructor.call(this, config);

        this.on('celldblclick', function () {
            var record = this.getSelectionModel().getSelected();
            if (record) {
                var entity = record.json;

                // Copy the columns of the TraceEntry grid and make the RequestId column hidden.
                var newColumnsArray = [];
                Ext.each(Viz.Configuration.Columns.TraceEntry, function (item) {
                    var copiedColumn = Ext.apply({}, item);

                    if (copiedColumn.dataIndex === 'RequestId') {
                        copiedColumn.hidden = true;
                    }

                    newColumnsArray.push(copiedColumn);
                }, this);

                var win = new Ext.Window({
                    title: $lang('msg_tracing'),
                    width: 1000,
                    maximizable: true,
                    height: 400,
                    layout: 'fit',
                    iconCls: 'viz-icon-small-trace',
                    items: [{
                        xtype: 'vizGridTraceEntry',
                        stateId: 'vizGridStep.vizGridTraceEntry',
                        columns: newColumnsArray,
                        plugins: [new Viz.plugins.GridExport({
                            paging: {
                                start: 0,
                                sort: 'Timestamp',
                                dir: 'ASC'

                            },
                            maxRecords: 1000,
                            promptMaxRecord: true
                        })],
                        store: new Viz.data.WCFJsonSimpleStore({
                            serviceHandler: Viz.Services.CoreWCF.TraceEntry_GetStoreByJobRunInstanceRequestId,
                            serviceParams: {
                                location: Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                                jobRunInstanceRequestId: entity.JobRunInstanceRequestId
                            },
                            sortInfo: {
                                field: 'Timestamp',
                                direction: 'ASC'
                            }
                        })
                    }]
                });
                win.show();
            }

        });
    }
});

Ext.reg("vizGridJobInstance", Viz.grid.JobInstance);
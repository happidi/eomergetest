﻿/**
 * @class Viz.grid.MeterData
 * <p>
 * A grid exposing the business entity MeterData
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.MeterData = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor               : function(config) {
                config = config || {};
                this._combometer_id = Ext.id();
                this._combotimezone_id = Ext.id();
                this._checkboxrawdata_id = Ext.id();

                this._asyncDelay = 5000;

                this._add_id = Ext.id();
                this._update_id = Ext.id();
                this._delete_id = Ext.id();
                this._generate_id = Ext.id();
                this._getfromendpoint_id = Ext.id();
                this._startdate_id = Ext.id();
                this._enddate_id = Ext.id();
                this._otheractions_id = Ext.id();

                var buttonsAuthorized = $authorized('@@@@ Meter Data - Full Control') || $authorized('@@@@ Meter Data - Input Only') || $authorized('@@@@ Meter Data - Validator');

                var defaultConfig = {
                    hasRowEditor     : false,
                    stripeRows       : false,
                    hasToolbar       : false,
                    allowupdatebatch : true,
                    viewConfig       : {
                        autoFill : false,
                        forceFit : false
                    },
                    tbar             : [{
                                id        : this._combometer_id,
                                xtype     : 'vizComboMeter',
                                width     : 200,
                                listeners : {
                                    select : {
                                        fn    : this.onSelectMeter,
                                        scope : this
                                    }
                                }
                            }, ' ', {
                                id         : this._combotimezone_id,
                                xtype      : 'vizComboTimeZone',
                                width      : 200,
                                allowBlank : true,
                                listeners  : {
                                    select : this.onSelectTimeZone,
                                    scope  : this
                                }
                            }, ' ', {
                                id            : this._checkboxrawdata_id,
                                text          : $lang("msg_meterdata_rawdata"),
                                enableToggle  : true,
                                toggleHandler : this.onCheckRawData.createDelegate(this),
                                pressed       : Viz.ApplicationSettings.RawDataUsedInMeterGridByDefault
                            }, {
                                text     : $lang("msg_meterdata_add"),
                                iconCls  : 'viz-icon-small-meterdata-add',
                                id       : this._add_id,
                                disabled : true,
                                hidden   : !buttonsAuthorized,
                                scope    : this,
                                handler  : this.onAddRecord
                            }, {
                                text     : $lang("msg_meterdata_update"),
                                iconCls  : 'viz-icon-small-meterdata-update',
                                hidden   : !buttonsAuthorized,
                                id       : this._update_id,
                                disabled : true,
                                scope    : this,
                                handler  : this.onUpdateRecord
                            }, {
                                text     : $lang("msg_meterdata_delete"),
                                iconCls  : 'viz-icon-small-meterdata-delete',
                                hidden   : !buttonsAuthorized,
                                id       : this._delete_id,
                                disabled : true,
                                scope    : this,
                                handler  : this.onDeleteRecord
                            }, {
                                text     : $lang("msg_otheractions"),
                                id       : this._otheractions_id,
                                iconCls  : 'viz-icon-small-action',
                                hidden   : !buttonsAuthorized && !$authorized('@@@@ Meter Data - Generate'),
                                disabled : true,
                                scope    : this,
                                menu     : new Ext.menu.Menu({
                                            items : [{
                                                        text    : $lang("msg_meterdata_generate"),
                                                        iconCls : 'viz-icon-small-meterdata-generate',
                                                        hidden  : !$authorized('@@@@ Meter Data - Generate'),
                                                        scope   : this,
                                                        handler : this.onGenerateRecord
                                                    }, {
                                                        text    : $lang("msg_meterdata_deletebydaterange"),
                                                        iconCls : 'viz-icon-small-meterdata-delete',
                                                        hidden  : !buttonsAuthorized,
                                                        scope   : this,
                                                        handler : this.onDeleteByDateRangeRecord
                                                    }, {
                                                        text    : $lang("msg_meterdata_offset"),
                                                        iconCls : 'viz-icon-small-meterdata-update',
                                                        hidden  : !buttonsAuthorized,
                                                        scope   : this,
                                                        handler : this.onOffsetRecord
                                                    }]
                                        })
                            }],
                    formConfig       : {
                        historyEnabled : false,
                        common         : {
                            width            : 450,
                            height           : 200,
                            xtype            : 'vizFormMeterData',
                            messageSaveStart : 'MeterDataChangeStart',
                            messageSaveEnd   : 'MeterDataChange',
                            readonly         : !buttonsAuthorized
                        },
                        create         : {
                            title   : $lang("msg_meterdata_add"),
                            iconCls : 'viz-icon-small-meter-add'
                        },
                        update         : {
                            title               : $lang("msg_meterdata_update"),
                            iconCls             : 'viz-icon-small-meter-update',
                            titleEntity         : 'Value',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyMeterData'
                                    }]
                        },
                        updatebatch    : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-meter-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.MeterData_FormUpdateBatch
                        }
                    },
                    // we set the manualInputTimeInterval so it will be avaliable to the AcquisitionDateTime column renderer.
                    store            : new Viz.store.MeterData({
                                ManualInputTimeInterval : config.entity ? config.entity.ManualInputTimeInterval : null
                            }),
                    plugins          : [new Viz.plugins.GridExport({
                                paging          : {
                                    start : 0,
                                    sort  : 'AcquisitionDateTime',
                                    dir   : 'ASC'
                                },
                                maxRecords      : 1000,
                                promptMaxRecord : true
                            })],
                    columns          : Viz.Configuration.Columns.MeterData
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                /*
                 * Ext.each(config.columns, function(col) { if (col.dataIndex == 'AcquisitionDateTime') { col.renderer = this.rendererAcquisitionDateTime.createDelegate(this); } }, this);
                 */

                Viz.grid.MeterData.superclass.constructor.call(this, config);

                this.on({
                            rowcontextmenu : {
                                scope : this,
                                fn    : this.onRowContextMenu
                            },
                            afterrender    : {
                                scope : this,
                                fn    : function() {
                                    this.onTimeZoneFirstLoad();
                                }
                            }
                        });

                Viz.util.MessageBusMgr.subscribe("MeterDataChange", this.reloadStore, this);
                Viz.util.MessageBusMgr.subscribe("SpatialMeterClick", this.onSpatialMeterClick, this);
                Viz.util.MessageBusMgr.subscribe("MeterDelete", this.onMeterDelete, this);

                Viz.util.MessageBusMgr.subscribe("MeterDataGenerate", this.resetStore, this);
                Viz.util.MessageBusMgr.subscribe("MeterDataDeleteByDateRange", this.resetStore, this);
                Viz.util.MessageBusMgr.subscribe("MeterDataOffset", this.resetStore, this);
            },

            onTimeZoneFirstLoad       : function() {
                var v = "" + (-new Date().getTimezoneOffset());
                var comboTimeZone = Ext.getCmp(this._combotimezone_id);
                if (comboTimeZone) {
                    var r = comboTimeZone.findRecord("Value", v);
                    if (r) {
                        comboTimeZone.setValue(r.data["Id"]);
                    }

                    if (this.entity && !Ext.isEmpty(this.entity.KeyMeter)) {
                        this.store.serviceParams.timeZoneId = comboTimeZone.getValue();
                        this.onSpatialMeterClick(this.entity);
                    }
                }
                this.entity = null;
            },

            /**
             * RowContextMenu listener.
             * @param {} grid
             * @param {} row
             * @param {} event
             */
            onRowContextMenu          : function(grid, row, event) {
                event.stopEvent();
                var xy = event.getXY();
                this.getSelectionModel().selectRow(row, false);
                if (this.hideMenu == true)
                    return;

                var entity = this.store.getAt(row).json;
                if (entity.Validity != Vizelia.FOL.BusinessEntities.MeterDataValidity.Invalid || entity.IsVirtual)
                    return;

                var ctxMenu = new Ext.menu.Menu({
                            defaults : {
                                hideOnClick : false
                            },
                            items    : [{
                                        text    : $lang("msg_meterdata_viewalarminstances"),
                                        iconCls : 'viz-icon-small-metervalidationrule',
                                        handler : Viz.grid.MeterData.displayMeterDataValidationRule.createDelegate(this, [entity]),
                                        scope   : this,
                                        hidden  : !$authorized("@@@@ Meter Validation Rule")
                                    }]
                        });
                ctxMenu.on("hide", function(menu) {
                            menu.destroy();
                        });
                ctxMenu.showAt(event.getXY());
            },

            /**
             * Destroy
             */
            onDestroy                 : function() {
                Viz.util.MessageBusMgr.unsubscribe("MeterDataChange", this.reloadStore, this);
                Viz.util.MessageBusMgr.unsubscribe("SpatialMeterClick", this.onSpatialMeterClick, this);
                Viz.util.MessageBusMgr.unsubscribe("MeterDelete", this.onMeterDelete, this);

                Viz.util.MessageBusMgr.unsubscribe("MeterDataGenerate", this.resetStore, this);
                Viz.util.MessageBusMgr.unsubscribe("MeterDataDeleteByDateRange", this.resetStore, this);
                Viz.util.MessageBusMgr.unsubscribe("MeterDataOffset", this.resetStore, this);

                Viz.grid.MeterData.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Override for the reload of the store, because now MeterData is asyncronously sent to EA
             */
            // reloadStore : function() {
            // this.getEl().mask();
            // this.reloadStoreDelayed.defer(this._asyncDelay, this);
            // },
            /**
             * Called after 11 seconds by the reloadStore.
             */
            // reloadStoreDelayed : function() {
            // this.getEl().unmask();
            // this.store.reload();
            // },
            // onDeleteRecord : function() {
            // Viz.grid.MeterData.superclass.onDeleteRecord.apply(this, arguments);
            // this.reloadStore();
            // },
            // onAddRecord : function() {
            // Viz.grid.MeterData.superclass.onAddRecord.apply(this, arguments);
            // this.reloadStore();
            // },
            // onUpdateRecord : function() {
            // Viz.grid.MeterData.superclass.onUpdateRecord.apply(this, arguments);
            // this.reloadStore();
            // },
            /**
             * @private Handler for updating the entity with the parent KeyMeter.
             */
            onBeforeAddRecord         : function(grid, entity) {
                var KeyMeter = this.getKeyMeter();
                if (KeyMeter == '' || KeyMeter == null) {
                    Ext.MessageBox.show({
                                title   : $lang('msg_application_title'),
                                msg     : $lang('msg_meterdata_selectmeter'),
                                width   : 350,
                                icon    : Ext.MessageBox.INFO,
                                buttons : Ext.MessageBox.OK
                            });
                    return false;
                }
                if (entity) {
                    entity.KeyMeter = KeyMeter;
                    entity.ManualInputTimeInterval = this._meter.ManualInputTimeInterval;
                }
                return true;
            },
            /**
             * Private handler for the Update event.
             * @return {Boolean}
             */
            onBeforeUpdateRecord      : function(grid, record, entity) {
                if (this.getCurrentMeter().IsVirtual)
                    return false;
                else {
                    entity.ManualInputTimeInterval = this._meter.ManualInputTimeInterval;
                    return true;
                }
            },

            /**
             * Handler for the Generate item button.
             */
            onGenerateRecord          : function() {
                var KeyMeter = this.getKeyMeter();
                var MeterTimeInterval = this.getCurrentMeter().FrequencyInput;
                var MeterTimeIntervalUnit = this.getCurrentMeter().ManualInputTimeInterval;
                if (this.onBeforeAddRecord()) {
                    Viz.openFormCrud({
                                common : {
                                    width            : 450,
                                    height           : 300,
                                    xtype            : 'vizFormMeterDataGenerate',
                                    messageSaveStart : 'MeterDataGenerateStart',
                                    messageSaveEnd   : 'MeterDataGenerate',
                                    title            : $lang("msg_meterdata_generate"),
                                    iconCls          : 'viz-icon-small-meterdata-add'
                                }
                            }, 'create', {
                                KeyMeter: KeyMeter,
                                MeterTimeInterval: MeterTimeInterval,
                                MeterTimeIntervalUnit: MeterTimeIntervalUnit
                            });
                }
            },

            /**
             * Handler for the Delete By Date Range item button.
             */
            onDeleteByDateRangeRecord : function() {
                var KeyMeter = this.getKeyMeter();
                if (this.onBeforeAddRecord()) {
                    Viz.openFormCrud({
                                common : {
                                    width            : 450,
                                    height           : 150,
                                    xtype            : 'vizFormMeterDataDeleteByDateRange',
                                    messageSaveStart : 'MeterDataDeleteByDateRangeStart',
                                    messageSaveEnd   : 'MeterDataDeleteByDateRange',
                                    title            : $lang("msg_meterdata_deletebydaterange"),
                                    iconCls          : 'viz-icon-small-meterdata-delete'
                                }
                            }, 'create', {
                                KeyMeter : KeyMeter
                            });
                }
            },

            /**
             * Handler for the Offset By Date Range item button.
             */
            onOffsetRecord            : function() {
                var KeyMeter = this.getKeyMeter();
                if (this.onBeforeAddRecord()) {
                    Viz.openFormCrud({
                                common : {
                                    width            : 450,
                                    height           : 250,
                                    xtype            : 'vizFormMeterDataOffsetByDateRange',
                                    messageSaveStart : 'MeterDataOffsetStart',
                                    messageSaveEnd   : 'MeterDataOffset',
                                    title            : $lang("msg_meterdata_offset"),
                                    iconCls          : 'viz-icon-small-meterdata-update'
                                }
                            }, 'create', {
                                KeyMeter : KeyMeter
                            });
                }
            },

            /**
             * Fetch data from the endpoint and store it as a MeterData
             */
            onGetFromEndpoint         : function() {
                this.el.mask();
                var KeyMeter = this.getKeyMeter();
                if (this.onBeforeAddRecord()) {
                    Viz.Services.EnergyWCF.Meter_SaveEndpointValue({
                                KeyMeter : KeyMeter,
                                success  : function(updated) {
                                    this.el.unmask();

                                    if (updated) {
                                        var store = this.getStore();
                                        store.reload.defer(500, store);
                                    }
                                }
                            }, this);
                }
            },

            onMeterDelete             : function(item) {
                var meter = this.getCurrentMeter();
                if (meter && meter.KeyMeter == item.KeyMeter) {
                    this.clearComboValue();
                    this.store.removeAll(true);
                    this.getView().refresh(true);
                }
            },

            /**
             * @private Handler for selecting a meter in the combo of meters.
             * @param {Viz.form.ComboBox} combo
             * @param {Ext.data.Record} record
             * @param {int} index
             */
            onSelectMeter             : function(combo, record, index) {
                this.selectMeter(record.data);
            },

            /**
             * private function to update the store of meter data based on the selected meter.
             * @param {Viz.BusinessEntity.Meter} meter
             */
            selectMeter               : function(meter) {
                this.setCurrentMeter(meter);
                this.store.ManualInputTimeInterval = meter.ManualInputTimeInterval;
                var serviceParams = {
                    location       : Vizelia.FOL.BusinessEntities.PagingLocation.Database,
                    KeyMeter       : meter.KeyMeter,
                    timeZoneId     : Ext.getCmp(this._combotimezone_id).getValue(),
                    rawData        : Ext.getCmp(this._checkboxrawdata_id).pressed,
                    datetimeFormat : Viz.getLocalizedDateFormat(true)
                };
                var params = {
                    start : 0,
                    limit : this.pageSize
                };
                var storeMeterData = this.getStore();
                storeMeterData.proxy.serviceParams = serviceParams;
                storeMeterData.serviceParams = storeMeterData.proxy.serviceParams;
                storeMeterData.load({
                            params   : params,
                            callback : function() {
                            }
                        });

                this.updateButtonStatus(meter);
            },

            updateButtonStatus        : function(meter) {
                if (!meter || meter.IsVirtual === true) {
                    Ext.getCmp(this._add_id).disable();
                    Ext.getCmp(this._update_id).disable();
                    Ext.getCmp(this._delete_id).disable();
                    Ext.getCmp(this._otheractions_id).disable();
                }
                else {
                    Ext.getCmp(this._add_id).enable();
                    Ext.getCmp(this._update_id).enable();
                    Ext.getCmp(this._delete_id).enable();
                    Ext.getCmp(this._otheractions_id).enable();
                }
            },

            /**
             * Set the grid's current meter.
             * @param {Viz.BusinessEntity.Meter} meter
             */
            setCurrentMeter           : function(meter) {
                this._meter = meter;
            },

            /**
             * Get's the current selected meter.
             * @return {Viz.BusinessEntity.Meter}
             */
            getCurrentMeter           : function() {
                return this._meter;
            },

            /**
             * Get the Key of the meter.
             * @private
             */
            getKeyMeter               : function() {
                return this.getCurrentMeter().KeyMeter;
            },
            /**
             * Handler for the MessageBus SpatialMeterClick event. triggered when a user click on a meter in the spatial filter tree.
             * @param {Viz.BusinessEntity.Meter} meter
             */
            onSpatialMeterClick       : function(meter) {
                if (this.findParentByType('tabpanel').activeTab.id != this.ownerCt.id)
                    return;
                this.setComboValue(meter.KeyMeter);
                this.selectMeter(meter);

                this.findParentByType('panel').setTitle($lang('msg_meterdata') + ' : ' + meter.Name);
            },

            /**
             * Handler for the select timezone.
             */
            onSelectTimeZone          : function() {
                this.store.serviceParams.timeZoneId = Ext.getCmp(this._combotimezone_id).getValue();
                this.reloadStore();
            },
            /**
             * Handler for the check on the rawdata checkbox.
             */
            onCheckRawData            : function() {
                this.store.serviceParams.rawData = Ext.getCmp(this._checkboxrawdata_id).pressed;
                this.reloadStore();
            },

            getCombo                  : function() {
                var combo = Ext.getCmp(this._combometer_id);
                return combo;
            },
            setComboValue             : function(KeyMeter) {
                var combo = this.getCombo();
                combo.setValue(KeyMeter);
            },

            clearComboValue           : function() {
                var combo = this.getCombo();
                combo.clearValue();
                this.updateButtonStatus(null);
            }

        });
Ext.reg("vizGridMeterData", Viz.grid.MeterData);

// Static Methods
Ext.apply(Viz.grid.MeterData, {

            /**
             * Open the list of alarminstances generated by metervalidation rule
             * @param {object/string} entity : MeterData entity
             */
            displayMeterDataValidationRule : function(entity) {
                var win = new Ext.Window({
                            title       : $lang('msg_metervalidationrule'),
                            iconCls     : 'viz-icon-small-metervalidationrule',
                            maximizable : true,
                            width       : 800,
                            height      : 250,
                            layout      : 'fit',
                            items       : [{
                                        readonly            : true,
                                        xtype               : 'vizGridAlarmInstance',
                                        hideChartViewButton : true,
                                        store               : new Viz.store.MeterDataAlarmInstance({
                                                    KeyMeterData : entity.KeyMeterData
                                                }),
                                        columns             : Viz.Configuration.Columns.MeterValidationRuleAlarmInstance
                                    }]
                        });
                win.show();
            }
        });

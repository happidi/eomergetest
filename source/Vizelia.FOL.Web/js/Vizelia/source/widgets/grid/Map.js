﻿/**
 * @class Viz.grid.Map
 * <p>
 * A grid exposing the business entity Map
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Map = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor         : function(config) {

                this._favoritebutton_id = Ext.id();

                config = config || {};
                var defaultConfig = {
                    hasRowEditor  : false,
                    stripeRows    : false,
                    hasToolbar    : false,
                    autoLoadStore : false,
                    tbar          : [{
                                id            : this._favoritebutton_id,
                                iconCls       : 'viz-icon-small-chart-favorite',
                                text          : $lang("msg_chart_favorite"),
                                enableToggle  : true,
                                toggleHandler : function(button, state) {
                                    Viz.util.Portal.onToggleFavoriteButton.createDelegate(this, [button, state])();
                                },
                                scope         : this,
                                pressed       : true
                            }, '-', {
                                text    : $lang("msg_map_add"),
                                iconCls : 'viz-icon-small-map-add',
                                hidden  : !$authorized('@@@@ Map - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_map_update"),
                                iconCls : 'viz-icon-small-map-update',
                                hidden  : !$authorized('@@@@ Map - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_map_delete"),
                                iconCls : 'viz-icon-small-map-delete',
                                hidden  : !$authorized('@@@@ Map - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_map_view"),
                                iconCls : 'viz-icon-small-display',
                                // hidden : !$authorized('Map_view'),
                                scope   : this,
                                handler : this.onMapDisplay
                            }, '-', {
                                text    : $lang("msg_portlets"),
                                iconCls : 'viz-icon-small-portlet',
                                hidden  : !$authorized('@@@@ Portlet'),
                                scope   : this,
                                handler : Viz.util.Portal.displayPortletGridFromEntity.createDelegate(this)
                            }],
                    formConfig    : this.getFormConfig(),
                    store         : new Viz.store.Map(),
                    columns       : Viz.Configuration.Columns.Map
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.Map.superclass.constructor.call(this, config);
                this.addEvents(
                        /**
                         * @event beforemapdisplay Fires before displaying a Map.
                         * @param {Viz.grid.GridPanel} this.
                         * @param {Object} record The record of the Map that is being displayed.
                         * @param {Object} entity The entity of the Map that is being displayed. It could be changed by the handler.
                         */
                        'beforemapdisplay');

                Viz.util.MessageBusMgr.subscribe("MapChange", this.reloadStore, this);

                this.on({
                            render          : Viz.util.Portal.gridRender,
                            filteravailable : Viz.util.Portal.gridFilterAvailable,
                            filtercleared   : Viz.util.Portal.toggleFavoriteButton.createDelegate(this, [this._favoritebutton_id, false]),
                            scope           : this
                        });
            },
            /**
             * Destroy
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("MapChange", this.reloadStore, this);
                Viz.grid.Map.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * After deleting we make sure the portlet is deleted as well.
             * @param {} record
             * @param {} selections
             */
            onAfterDeleteRecord : function(record, selections) {
                for (var i = 0, r; r = selections[i]; i++) {
                    Viz.util.MessageBusMgr.fireEvent('PortletDelete', r.json);
                }
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig       : function() {
                return {
                    common      : {
                        width            : 500,
                        height           : 350,
                        xtype            : 'vizFormMap',
                        messageSaveStart : 'MapChangeStart',
                        messageSaveEnd   : 'MapChange',
                        readonly         : !$authorized('@@@@ Map - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_map_add"),
                        iconCls : 'viz-icon-small-map-add'
                    },
                    update      : {
                        title               : $lang("msg_map_update"),
                        iconCls             : 'viz-icon-small-map-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyMap'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-map-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.Map_FormUpdateBatch
                    }
                };
            },
            /**
             * Handler for displaying a map.
             * @method onChartDisplay
             */
            onMapDisplay        : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    if (this.fireEvent("beforemapdisplay", this, record, entity)) {
                        Viz.grid.Map.openMapDisplay(entity);
                    }
                }
            }
        });
Ext.reg("vizGridMap", Viz.grid.Map);

// Static Methods
Ext.apply(Viz.grid.Map, {
            /**
             * Displays a Map.
             * @method openMapDisplay
             * @static
             * @param {Viz.BusinessEntity.Map} entity : the Map entity.
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openMapDisplay : function(entity, width, height) {
                var win = new Viz.portal.PortletWindow({
                            entity        : entity,
                            xtypePortlet  : 'vizPortletMap',
                            entityPortlet : {
                                TypeEntity : 'Vizelia.FOL.BusinessEntities.Map'
                            },
                            updateHandler : Viz.grid.Map.update
                        });
                win.show();
            },

            /**
             * Create a new map (to be called from desktop or tree)
             * @param {} callback
             */
            create         : function() {
                Viz.openFormCrud(Viz.grid.Map.prototype.getFormConfig(), 'create', {});
            },

            /**
             * Update an existing map (to be called from desktop or tree)
             * @param {} callback
             */
            update         : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.Map.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
﻿/**
 * @class Viz.grid.DynamicDisplay
 * <p>
 * A grid exposing the business entity DynamicDisplay
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.DynamicDisplay = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_dynamicdisplay_add"),
                                iconCls : 'viz-icon-small-dynamicdisplay-add',
                                hidden  : !$authorized('@@@@ Dynamic Display - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_dynamicdisplay_update"),
                                iconCls : 'viz-icon-small-dynamicdisplay-update',
                                hidden  : !$authorized('@@@@ Dynamic Display - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_dynamicdisplay_delete"),
                                iconCls : 'viz-icon-small-dynamicdisplay-delete',
                                hidden  : !$authorized('@@@@ Dynamic Display - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_dynamicdisplay_copy"),
                                iconCls : 'viz-icon-small-copy',
                                hidden  : !$authorized('@@@@ Dynamic Display - Full Control'),
                                scope   : this,
                                handler : this.onCopyRecord
                            }, {
                                text    : $lang("msg_dynamicdisplay_view"),
                                iconCls : 'viz-icon-small-display',
                                // hidden : !$authorized('@@@@ Dynamic Display - Full Control'),
                                scope   : this,
                                handler : this.onDynamicDisplayDisplay
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : new Viz.store.DynamicDisplay(),
                    columns      : Viz.Configuration.Columns.DynamicDisplay
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.DynamicDisplay.superclass.constructor.call(this, config);

                this.addEvents(
                        /**
                         * @event beforechartdiplay Fires before displaying a chart.
                         * @param {Viz.grid.GridPanel} this.
                         * @param {Object} record The record of the chart that is being displayed.
                         * @param {Object} entity The entity of the chart that is being displayed. It could be changed by the handler.
                         */
                        'beforedynamicdisplaydisplay');

                Viz.util.MessageBusMgr.subscribe("DynamicDisplayChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy               : function() {
                Viz.util.MessageBusMgr.unsubscribe("DynamicDisplayChange", this.reloadStore, this);
                Viz.grid.DynamicDisplay.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig           : function() {
                return {
                    common      : {
                        width            : 750,
                        height           : 400,
                        xtype            : 'vizFormDynamicDisplay',
                        messageSaveStart : 'DynamicDisplayChangeStart',
                        messageSaveEnd   : 'DynamicDisplayChange',
                        readonly         : !$authorized('@@@@ Dynamic Display - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_dynamicdisplay_add"),
                        iconCls : 'viz-icon-small-dynamicdisplay-add'
                    },
                    update      : {
                        title               : $lang("msg_dynamicdisplay_update"),
                        iconCls             : 'viz-icon-small-dynamicdisplay-update',
                        titleEntity         : 'Name',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyDynamicDisplay'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-dynamicdisplay-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.DynamicDisplay_FormUpdateBatch
                    }
                };
            },

            onBeforeCopyRecord      : function(selections) {
                for (var i = 0, r; r = selections[i]; i++) {
                    r.data.Name = $lang('msg_copyof') + ' ' + r.data.Name;
                }
            },

            /**
             * Handler for displaying a dynamicdisplay.
             * @method onChartDisplay
             */
            onDynamicDisplayDisplay : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    if (this.fireEvent("beforedynamicdisplaydisplay", this, record, entity)) {
                        Viz.grid.DynamicDisplay.openDynamicDisplayDisplay(entity, 800, 550);
                    }
                }
            }
        });
Ext.reg("vizGridDynamicDisplay", Viz.grid.DynamicDisplay);

// Static Methods
Ext.apply(Viz.grid.DynamicDisplay, {
            /**
             * Displays a DynamicDisplay.
             * @method openDynamicDisplayDisplay
             * @static
             * @param {Viz.BusinessEntity.DynamicDisplay} entity : the DynamicDisplay entity.
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openDynamicDisplayDisplay : function(entity, width, height, animateTarget) {
                var win = new Viz.portal.PortletWindow({
                            entity        : entity,
                            xtypePortlet  : 'vizChartDynamicDisplayImage',
                            updateHandler : Viz.grid.DynamicDisplay.update,
                            animateTarget : animateTarget,
                            entityPortlet : {
                                TypeEntity : 'Vizelia.FOL.BusinessEntities.DynamicDisplay'
                            }
                        });
                win.show();
            },

            /**
             * Create a new DynamicDisplay (to be called from desktop or tree)
             * @param {} callback
             */
            create                    : function(entity) {
                Viz.openFormCrud(Viz.grid.DynamicDisplay.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing DynamicDisplay (to be called from desktop or tree)
             * @param {} callback
             */
            update                    : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.DynamicDisplay.prototype.getFormConfig(), 'update', entity);
                }
            }
        });

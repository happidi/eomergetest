﻿/**
 * @class Viz.grid.WorkflowActionElement
 * <p>
 * A grid exposing the business entity WorkflowActionElement
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.WorkflowActionElement = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    store        : new Viz.store.WorkflowActionElement({
                                serviceParams : config.serviceParams || {}
                            }),
                    columns      : [{
                                header     : $lang('msg_title'),
                                dataIndex  : 'Label',
                                renderer   : Ext.ux.iconClsRenderer(),
                                sortable   : true,
                                filterable : false
                            }, {
                                header     : $lang('msg_workflow'),
                                dataIndex  : 'AssemblyShortName',
                                sortable   : true,
                                filterable : false
                            }]
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.WorkflowActionElement.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("WorkflowActionElementChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("WorkflowActionElementChange", this.reloadStore, this);
                Viz.grid.WorkflowActionElement.superclass.onDestroy.apply(this, arguments);
            }
        });
Ext.reg("vizGridWorkflowActionElement", Viz.grid.WorkflowActionElement);
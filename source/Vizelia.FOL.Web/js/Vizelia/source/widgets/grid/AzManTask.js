﻿/**
 * @class Viz.grid.AzManTask
 * <p>
 * A grid exposing the business entity AzManItem Task
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.AzManTask = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                id            : this._favoritebutton_id,
                                iconCls       : 'viz-icon-small-chart-favorite',
                                text          : $lang("msg_only_ui_tasks"),
                                enableToggle  : true,
                                toggleHandler : function(button, state) {
                                    this.store.serviceParams.onlyUITasks = state;
                                    this.reloadStore();
                                },
                                scope         : this,
                                pressed       : true
                            }, {
                                text    : $lang("msg_azmantask_add"),
                                iconCls : 'viz-icon-small-azmantask-add',
                                hidden  : !$authorized('@@@@ Task management - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_azmantask_update"),
                                iconCls : 'viz-icon-small-azmantask-update',
                                hidden  : !$authorized('@@@@ Task management - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_azmantask_delete"),
                                iconCls : 'viz-icon-small-azmantask-delete',
                                hidden  : !$authorized('@@@@ Task management - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 500,
                            height           : 400,
                            xtype            : 'vizFormAzManTask',
                            messageSaveStart : 'AzManTaskChangeStart',
                            messageSaveEnd   : 'AzManTaskChange',
                            readonly         : !$authorized('@@@@ Task management - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_azmantask_add"),
                            iconCls : 'viz-icon-small-azmantask-add'
                        },
                        update      : {
                            title               : $lang("msg_azmantask_update"),
                            iconCls             : 'viz-icon-small-azmantask-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'Id'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-azmantask-update',
                            serviceHandlerUpdateBatch : Viz.Services.AuthenticationWCF.AzManTask_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.AzManTask(),
                    columns      : Viz.Configuration.Columns.AzManItem

                };
                var forcedConfig = {
                    auditEntityName : 'AuthorizationItem.Task'
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.AzManTask.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("AzManTaskChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("AzManTaskChange", this.reloadStore, this);
                Viz.grid.AzManTask.superclass.onDestroy.apply(this, arguments);
            }

        });

Ext.reg("vizGridAzManTask", Viz.grid.AzManTask);
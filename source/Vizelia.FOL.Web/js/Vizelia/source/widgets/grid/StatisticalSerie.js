﻿/**
 * @class Viz.grid.StatisticalSerie
 * <p>
 * A grid exposing the business entity StatisticalSerie
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.StatisticalSerie = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_statisticalserie_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('StatisticalSerie_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_statisticalserie_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('StatisticalSerie_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_statisticalserie_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('StatisticalSerie_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : config.store || new Viz.store.StatisticalSerie({
                                KeyChart : config.KeyChart
                            }),
                    columns      : Viz.Configuration.Columns.StatisticalSerie
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.StatisticalSerie.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("StatisticalSerieChange", this.reloadStore, this);

            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("StatisticalSerieChange", this.reloadStore, this);
                Viz.grid.StatisticalSerie.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig     : function() {
                return {
                    common      : {
                        width            : 450,
                        height           : 350,
                        xtype            : 'vizFormStatisticalSerie',
                        messageSaveStart : 'StatisticalSerieChangeStart',
                        messageSaveEnd   : 'StatisticalSerieChange'
                    },
                    create      : {
                        title   : $lang("msg_statisticalserie_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_statisticalserie_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'LocalId',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyStatisticalSerie'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.StatisticalSerie_FormUpdateBatch
                    }
                };
            },

            /**
             * @private Handler for updating the entity with the parent KeyChart.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyChart = this.KeyChart;
                if (KeyChart == '' || KeyChart == null) {
                    return false;
                }
                entity.KeyChart = KeyChart;
                return true;
            },

            /**
             * Handler for delete event.
             * @param {} selections
             */
            onDeleteRecord    : function(selections) {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;

                var msgDelete;
                if (selections.length > 1)
                    msgDelete = String.format($lang("msg_record_delete_confirmation_multiple"), selections.length);
                else
                    msgDelete = $lang("msg_record_delete_confirmation_single");

                Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                            if (button == 'yes') {
                                for (var i = 0, r; r = selections[i]; i++) {
                                    this.store.remove(r);
                                }
                                this.store.save();
                                Viz.Services.EnergyWCF.StatisticalSerie_SaveStore({
                                            store   : this.store.crudStore,
                                            success : function() {
                                                if (Ext.isArray(selections)) {
                                                    for (var i = 0, r; r = selections[i]; i++) {
                                                        Viz.util.MessageBusMgr.fireEvent('StatisticalSerieChange', r.data);
                                                    }
                                                }
                                                else {
                                                    Viz.util.MessageBusMgr.fireEvent('StatisticalSerieChange', selections);
                                                }
                                            },
                                            scope   : this
                                        });
                            }
                        }, this);

            }

        });

Ext.reg("vizGridStatisticalSerie", Viz.grid.StatisticalSerie);


// Static Methods
Ext.apply(Viz.grid.StatisticalSerie, {

            /**
             * Create a new Chart (to be called from desktop or tree)
             * @param {} callback
             */
            create : function(entity) {
                Viz.openFormCrud(Viz.grid.StatisticalSerie.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing Chart (to be called from desktop or tree) *
             * @param {Chart} entity
             * @param {Ext.Window} window
             * @param {object} extraConfig
             */
            update : function(entity, window, extraConfig) {
                if (entity) {
                    var conf = Viz.grid.StatisticalSerie.prototype.getFormConfig();
                    if (Ext.isObject(extraConfig))
                        Ext.apply(conf.common, extraConfig);
                    if (window) {
                        var box = window.getBox();
                        conf.common.x = box.width + box.x;
                        conf.common.y = box.y;
                    }
                    Viz.openFormCrud(conf, 'update', entity);
                }
            }
        });

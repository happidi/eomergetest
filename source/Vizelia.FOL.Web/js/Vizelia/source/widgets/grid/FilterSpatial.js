﻿/**
 * @class Viz.grid.FilterSpatial
 * <p>
 * A grid exposing the business entity FilterSpatial
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterSpatial = Ext.extend(Viz.grid.GridPanel, {

            /**
             * @param {Boolean} isSecurableEntity <t>true</t> if the store is a SecurableObject store, <t>false</t> (default) otherwise
             * @type Boolean
             */
            isSecurableEntity     : false,
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor           : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor       : false,
                    stripeRows         : false,
                    hasToolbar         : false,
                    ddGroup            : 'SpatialDD',
                    enableDropFromTree : true,
                    tbar               : [{
                                text    : $lang("msg_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterSpatial_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterSpatial_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],

                    columns            : config.isSecurableEntity ? Viz.Configuration.Columns.SecurableEntity : Viz.Configuration.Columns.FilterSpatial

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterSpatial.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("FilterSpatialChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy             : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterSpatialChange", this.reloadStore, this);
                Viz.grid.FilterSpatial.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord           : function() {
                this.openTreeDetail({
                            windowTitle                     : $lang('msg_tree_spatial'),
                            windowWidth                     : 350,
                            windowHeight                    : 400,
                            windowIconCls                   : 'viz-icon-small-site',
                            xtypeTreeDetail                 : 'vizTreeSpatial',
                            ExcludeMetersAndAlarms          : true,
                            disableCheckboxForAncestors     : this.disableCheckboxForAncestors,
                            checkboxExcludedTypesTreeDetail : ['Vizelia.FOL.BusinessEntities.Meter', 'Vizelia.FOL.BusinessEntities.AlarmDefinition']
                        });
            },

            getEntityFromTreeNode : function(node) {
                if (this.isSecurableEntity) {
                    var data = {
                        KeySecurable      : node.id,
                        SecurableName     : node.text,
                        SecurableTypeName : 'Vizelia.FOL.BusinessEntities.Location',
                        SecurableIconCls  : node.attributes.iconCls
                    };
                    return data;

                }
                else {
                    var data = Viz.util.Location.getLocationEntityFromSpatialNode(node);
                    return data;

                }
            },

            onDropFromTree        : function(ddSource, e, data) {
                var node = data.node;
                var entity = this.getEntityFromTreeNode
                // we filter the node to be the same type as the store
                if (node && node.attributes && node.attributes.entity) {
                    var type = Viz.getEntityType(node.attributes.entity);
                    if (type != 'Vizelia.FOL.BusinessEntities.Meter' && type != 'Vizelia' + '.FOL.BusinessEntities.AlarmDefinition') {
                        if (this.store.find(this.store.reader.meta.idProperty, node.attributes.Key || node.attributes.id) == -1) {
                            this.isDirty = true;
                            var form = this.getParentForm();
                            if (form)
                                form.isDirty = true;
                            this.store.add(new this.store.recordType(this.getEntityFromTreeNode(node)));
                            return true;
                        }
                    }
                }
                return false;
            }
        });

Ext.reg("vizGridFilterSpatial", Viz.grid.FilterSpatial);
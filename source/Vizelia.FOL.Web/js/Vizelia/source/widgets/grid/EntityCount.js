﻿Ext.namespace('Viz.grid');

/**
 * @class Viz.grid.EntityCount
 * @extends Viz.grid.GridPanel The Grid Pset List.
 */
Viz.grid.EntityCount = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};

                this.store = new Viz.store.EntityCount({
                            autoLoad : false
                        });
                this.store.on("beforeload", function() {
                            return ($authorized('@@@@ EntityCount'))
                        });

                var defaultConfig = {
                    hasRowEditor : false,
                    hasToolbar: false,
                    hasPagingToolbar: false,
                    // hideHeaders : true,
                    stripeRows   : false,
                    iconCls      : 'viz-icon-small-entitycount',
                    title        : $lang('msg_entity_count'),
                    hasFilter    : false,
                    columns      : Viz.Configuration.Columns.EntityCount

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.EntityCount.superclass.constructor.call(this, config);

            }

        });

Ext.reg('vizGridEntityCount', Viz.grid.EntityCount);
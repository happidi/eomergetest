﻿/**
 * @class Viz.grid.UserFiltered
 * <p>
 * A grid exposing the business entity UserFiltered
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.UserFiltered = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    store        : new Viz.store.UserFiltered({
                                roles     : config.roles,
                                locations : config.locations
                            }),

                    columns      : [{
                                header    : $lang('msg_login'),
                                dataIndex : 'UserName',
                                sortable  : true,
                                filtrable : true,
                                renderer  : function(data, cell, record, rowIndex, colIndex, store) {
                                    cell.css = 'viz-icon-cell ' + 'viz-icon-small-user';
                                    return data;
                                }
                            }, {
                                header    : $lang('msg_occupant_firstname'),
                                dataIndex : 'ProviderUserKey.Occupant.FirstName',
                                sortable  : true,
                                filtrable : true
                            }, {
                                header    : $lang('msg_occupant_lastname'),
                                dataIndex : 'ProviderUserKey.Occupant.LastName',
                                sortable  : true,
                                filtrable : true
                            }, {
                                header    : $lang('msg_occupant_email'),
                                dataIndex : 'ProviderUserKey.Occupant.ElectronicMailAddresses',
                                sortable  : true,
                                filtrable : true
                            }]
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.UserFiltered.superclass.constructor.call(this, config);

            }

        });

Ext.reg("vizGridUserFiltered", Viz.grid.UserFiltered);
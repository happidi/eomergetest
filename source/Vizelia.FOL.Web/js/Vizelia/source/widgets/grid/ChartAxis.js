﻿/**
 * @class Viz.grid.ChartAxis
 * <p>
 * A grid exposing the business entity ChartAxis
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.ChartAxis = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_chartaxis_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('ChartAxis_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_chartaxis_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('ChartAxis_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_chartaxis_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('ChartAxis_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 350,
                            height           : 200,
                            xtype            : 'vizFormChartAxis',
                            messageSaveStart : 'ChartAxisChangeStart',
                            messageSaveEnd   : 'ChartAxisChange'
                        },
                        create      : {
                            title   : $lang("msg_chartaxis_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_chartaxis_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyChartAxis'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.ChartAxis_FormUpdateBatch
                        }
                    },
                    store        : config.store || new Viz.store.ChartAxis({
                                KeyChart : config.KeyChart
                            }),
                    columns      : Viz.Configuration.Columns.ChartAxis
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.ChartAxis.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("ChartAxisChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("ChartAxisChange", this.reloadStore, this);
                Viz.grid.ChartAxis.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyChart.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyChart = this.KeyChart;
                if (KeyChart == '' || KeyChart == null) {
                    return false;
                }
                entity.KeyChart = KeyChart;
                return true;
            }

        });

Ext.reg("vizGridChartAxis", Viz.grid.ChartAxis);
﻿/**
* @class Viz.grid.FilterAzmanRole
* <p>
* A grid exposing a list of Meter Classification
* </p>
* @extends Viz.grid.GridPanel
*/
Viz.grid.FilterAzManRole = Ext.extend(Viz.grid.GridPanel, {
    /**
    * Ctor.
    * @param {Object} config
    */
    constructor: function (config) {
        config = config || {};
        var defaultConfig = {
            hasRowEditor: false,
            stripeRows: false,
            hasToolbar: false,
            tbar: [{
                text: $lang('msg_azmanrole_add'),
                iconCls: 'viz-icon-small-role-add',
                // hidden : !$authorized('FilterAzManRole_write'),
                scope: this,
                handler: this.onAddRecord
            }, {
                text: $lang('msg_azmanrole_delete'),
                iconCls: 'viz-icon-small-role-delete',
                // hidden : !$authorized('FilterAzManRole_write'),
                scope: this,
                handler: this.deleteSelectedRows
            }],
            columns: Viz.Configuration.Columns.AzManItem
        };
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.grid.FilterAzManRole.superclass.constructor.call(this, config);
        Viz.util.MessageBusMgr.subscribe("FilterAzManRoleChange", this.reloadStore, this);
    },

    /**
    * Destroy
    */
    onDestroy: function () {
        Viz.util.MessageBusMgr.unsubscribe("FilterAzManRoleChange", this.reloadStore, this);
        Viz.grid.FilterAzManRole.superclass.onDestroy.apply(this, arguments);
    },

    /**
    * Handler for the Add item button.
    */
    onAddRecord: function () {
        var entity = {};
        if (this.fireEvent("beforeaddrecord", this, entity)) {
            this.openTreeDetail({
                windowTitle: $lang('msg_azmanrole_tree'),
                windowWidth: 350,
                windowHeight: 400,
                windowIconCls: 'viz-icon-small-role',
                xtypeTreeDetail: 'vizTreeAzManRole',
                keyColumn: 'Id',
                rootTreeDetail: {
                    onlyRoles: true
                }

            });
        }
    }
});

Ext.reg("vizGridFilterAzManRole", Viz.grid.FilterAzManRole);
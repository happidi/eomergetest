﻿/**
 * @class Viz.grid.FilterDynamicDisplay
 * <p>
 * A grid exposing the business entity FilterDynamicDisplay
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterDynamicDisplay = Ext.extend(Viz.grid.GridPanel, {

            /**
             * @param {Boolean} isSecurableEntity <t>true</t> if the store is a SecurableObject store, <t>false</t> (default) otherwise
             * @type Boolean
             */
            isSecurableEntity       : false,
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang("msg_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterDynamicDisplay_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterDynamicDisplay_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],

                    columns      : config.isSecurableEntity ? Viz.Configuration.Columns.SecurableEntity : Viz.Configuration.Columns.DynamicDisplay

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterDynamicDisplay.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("FilterDynamicDisplayChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy               : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterDynamicDisplayChange", this.reloadStore, this);
                Viz.grid.FilterDynamicDisplay.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord             : function() {
                this.openGridDetail({
                            windowTitle     : $lang('msg_dynamicdisplay'),
                            windowWidth     : 1000,
                            windowHeight    : 400,
                            windowIconCls   : 'viz-icon-small-dynamicdisplay',
                            xtypeGridDetail : 'vizGridDynamicDisplay'
                        });
            },
            /**
             * Return the data to add to the store from the detail grid selected record.
             */
            getEntityFromGridRecord : function(r) {
                if (this.isSecurableEntity) {
                    var data = {
                        KeySecurable      : r.data.KeyDynamicDisplay,
                        SecurableName     : r.data.Name,
                        SecurableTypeName : 'Vizelia.FOL.BusinessEntities.DynamicDisplay',
                        SecurableIconCls  : 'viz-icon-small-dynamicdisplay'
                    };
                    return data;
                }
                else {
                    return Viz.grid.FilterDynamicDisplay.superclass.getEntityFromGridRecord.apply(this, arguments);
                }
            }
        });

Ext.reg("vizGridFilterDynamicDisplay", Viz.grid.FilterDynamicDisplay);
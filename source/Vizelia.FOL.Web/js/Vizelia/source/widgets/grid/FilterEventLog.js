﻿/**
 * @class Viz.grid.FilterEventLogClassification
 * <p>
 * A grid exposing a list of EventLog Classification.
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterEventLogClassification = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor    : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang('msg_eventlog_classification_add'),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterEventLogClassification_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_eventlog_classification_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterEventLogClassification_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],
                    columns      : Viz.Configuration.Columns.ClassificationItem
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterEventLogClassification.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("FilterEventLogClassificationChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy      : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterEventLogClassificationlChange", this.reloadStore, this);
                Viz.grid.FilterEventLogClassification.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord    : function() {
                this.openTreeDetail({
                            windowTitle     : $lang('msg_eventlog_classification_add'),
                            windowWidth     : 350,
                            windowHeight    : 400,
                            windowIconCls   : 'viz-icon-small-classificationitem',
                            xtypeTreeDetail : 'vizTreeClassificationItem',
                            rootTreeDetail  : {
                                id : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory('eventlog')
                            },
                            keyColumn       : 'KeyClassificationItem'
                        });
            }
        });

Ext.reg("vizGridFilterEventLogClassification", Viz.grid.FilterEventLogClassification);
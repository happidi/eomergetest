﻿/**
 * @class Viz.grid.Portlet
 * <p>
 * A grid exposing the business entity Portlet
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Portlet = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor         : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_portlet_add"),
                                iconCls : 'viz-icon-small-portlet-add',
                                // hidden : !$authorized('Portlet_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_portlet_update"),
                                iconCls : 'viz-icon-small-portlet-update',
                                // hidden : !$authorized('Portlet_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_portlet_delete"),
                                iconCls : 'viz-icon-small-portlet-delete',
                                // hidden : !$authorized('Portlet_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : this.getFormConfig(config),
                    store        : config.store || new Viz.store.Portlet({
                                KeyPortalColumn : config.KeyPortalColumn
                            }),
                    columns      : Viz.Configuration.Columns.Portlet
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.Portlet.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("PortletChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("PortletChange", this.reloadStore, this);
                Viz.grid.Portlet.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig       : function(config) {
                return {
                    common      : {
                        width            : 500,
                        height           : 290,
                        xtype            : 'vizFormPortlet',
                        messageSaveStart : 'PortletChangeStart',
                        messageSaveEnd   : 'PortletChange',
                        showEntityCombo  : config ? config.showEntityCombo : false
                    },
                    create      : {
                        title   : $lang("msg_portlet_add"),
                        iconCls : 'viz-icon-small-portlet-add'
                    },
                    update      : {
                        title               : $lang("msg_portlet_updateframe"),
                        iconCls             : 'viz-icon-small-portlet-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyPortlet'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-portlet-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.Portlet_FormUpdateBatch
                    }
                };
            },

            /**
             * After deleting we make sure the portlet is deleted as well.
             * @param {} record
             * @param {} selections
             */
            onAfterDeleteRecord : function(record, selections) {
                for (var i = 0, r; r = selections[i]; i++) {
                    Viz.util.MessageBusMgr.fireEvent('PortletDelete', r.json);
                }
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortalColumn.
             */
            onBeforeAddRecord   : function(grid, entity) {
                var KeyPortalColumn = this.KeyPortalColumn;
                if (KeyPortalColumn == '' || KeyPortalColumn == null) {
                    return false;
                }
                entity.KeyPortalColumn = KeyPortalColumn;
                entity.Order = this.getMaxValueFromStore('Order') + 1;

                return true;
            }

        });

Ext.reg("vizGridPortlet", Viz.grid.Portlet);

// Static Methods
Ext.apply(Viz.grid.Portlet, {
            /**
             * Create a new Portlet (to be called from desktop or tree)
             * @param {} callback
             */
            create : function(entity) {
                Viz.openFormCrud(Viz.grid.Portlet.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing Portlet (to be called from desktop or tree)
             * @param {Viz.BusinessEntity.Portlet} entity
             */
            update : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.Portlet.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
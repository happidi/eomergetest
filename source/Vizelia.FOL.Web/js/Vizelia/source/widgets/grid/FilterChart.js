﻿/**
 * @class Viz.grid.FilterChart
 * <p>
 * A grid exposing the business entity FilterChart
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterChart = Ext.extend(Viz.grid.GridPanel, {

            /**
             * @param {Boolean} isSecurableEntity <t>true</t> if the store is a SecurableObject store, <t>false</t> (default) otherwise
             * @type Boolean
             */
            isSecurableEntity       : false,
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang("msg_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterChart_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterChart_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],

                    columns      : config.isSecurableEntity ? Viz.Configuration.Columns.SecurableEntity : Viz.Configuration.Columns.Chart

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterChart.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("FilterChartChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy               : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterChartChange", this.reloadStore, this);
                Viz.grid.FilterChart.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord             : function() {
                this.openGridDetail({
                            windowTitle     : $lang('msg_chart'),
                            windowWidth     : 1000,
                            windowHeight    : 400,
                            windowIconCls   : 'viz-icon-small-chart',
                            xtypeGridDetail : 'vizGridChart'
                        });
            },
            /**
             * Return the data to add to the store from the detail grid selected record.
             */
            getEntityFromGridRecord : function(r) {
                if (this.isSecurableEntity) {
                    var data = {
                        KeySecurable      : r.data.KeyChart,
                        SecurableName     : r.data.Title,
                        SecurableTypeName : 'Vizelia.FOL.BusinessEntities.Chart',
                        SecurableIconCls  : 'viz-icon-small-chart'
                    };
                    return data;
                }
                else {
                    return Viz.grid.FilterChart.superclass.getEntityFromGridRecord.apply(this, arguments);
                }
            }
        });

Ext.reg("vizGridFilterChart", Viz.grid.FilterChart);
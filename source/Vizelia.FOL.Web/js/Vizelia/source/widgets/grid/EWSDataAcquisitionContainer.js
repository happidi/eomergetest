﻿/**
 * @class Viz.grid.EWSDataAcquisitionContainer
 * <p>
 * A grid exposing the business entity EWSDataAcquisitionContainer
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.EWSDataAcquisitionContainer = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor   : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_ewsdataacquisitioncontainer_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden  : !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_ewsdataacquisitioncontainer_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden  : !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_ewsdataacquisitioncontainer_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ Data Acquistion - Full Control'),
                                scope   : this,
                                handler : function(button, event) {
                                    this.onDeleteRecord(button, event, $lang("msg_container_delete_confirmation_single"), $lang("msg_container_delete_confirmation_multiple"));
                                }
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : new Viz.store.EWSDataAcquisitionContainer(),
                    columns      : Viz.Configuration.Columns.EWSDataAcquisitionContainer
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.EWSDataAcquisitionContainer.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("EWSDataAcquisitionContainerChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy     : function() {
                Viz.util.MessageBusMgr.unsubscribe("EWSDataAcquisitionContainerChange", this.reloadStore, this);
                Viz.grid.EWSDataAcquisitionContainer.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig : function() {
                return {
                    common      : {
                        width            : 600,
                        height           : 400,
                        xtype            : 'vizFormEWSDataAcquisitionContainer',
                        messageSaveStart : 'EWSDataAcquisitionContainerChangeStart',
                        messageSaveEnd   : 'EWSDataAcquisitionContainerChange',
                        readonly         : !$authorized('@@@@ Data Acquistion - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_ewsdataacquisitioncontainer_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title               : $lang("msg_ewsdataacquisitioncontainer_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyDataAcquisitionContainer'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.EWSDataAcquisitionContainer_FormUpdateBatch
                    }
                };
            }
        });
Ext.reg("vizGridEWSDataAcquisitionContainer", Viz.grid.EWSDataAcquisitionContainer);

// Static Methods
Ext.apply(Viz.grid.EWSDataAcquisitionContainer, {

            /**
             * Create a new EWSdataacquisitioncontainer (to be called from desktop or tree)
             * @param {} callback
             */
            create : function() {
                Viz.openFormCrud(Viz.grid.EWSDataAcquisitionContainer.prototype.getFormConfig(), 'create', {});
            },

            /**
             * Update an existing EWSdataacquisitioncontainer (to be called from desktop or tree)
             * @param {} callback
             */
            update : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.EWSDataAcquisitionContainer.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
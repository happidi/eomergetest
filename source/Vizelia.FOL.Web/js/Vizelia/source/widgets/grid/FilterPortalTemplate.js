﻿/**
 * @class Viz.grid.FilterPortalTemplate
 * <p>
 * A grid exposing the business entity FilterPortalTemplate
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterPortalTemplate = Ext.extend(Viz.grid.GridPanel, {

            /**
             * @param {Boolean} isSecurableEntity <t>true</t> if the store is a SecurableObject store, <t>false</t> (default) otherwise
             * @type Boolean
             */
            isSecurableEntity       : false,
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang("msg_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterPortalTemplate_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterPortalTemplate_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],

                    columns      : config.isSecurableEntity ? Viz.Configuration.Columns.SecurableEntity : Viz.Configuration.Columns.PortalTemplate

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterPortalTemplate.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("FilterPortalTemplateChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy               : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterPortalTemplateChange", this.reloadStore, this);
                Viz.grid.FilterPortalTemplate.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord             : function() {
                this.openGridDetail({
                            windowTitle     : $lang('msg_portaltemplate'),
                            windowWidth     : 1000,
                            windowHeight    : 400,
                            windowIconCls   : 'viz-icon-small-portaltemplate',
                            xtypeGridDetail : 'vizGridPortalTemplate'
                        });
            },
            /**
             * Return the data to add to the store from the detail grid selected record.
             */
            getEntityFromGridRecord : function(r) {
                if (this.isSecurableEntity) {
                    var data = {
                        KeySecurable      : r.data.KeyPortalTemplate,
                        SecurableName     : r.data.Title,
                        SecurableTypeName : 'Vizelia.FOL.BusinessEntities.PortalTemplate',
                        SecurableIconCls  : 'viz-icon-small-portaltemplate'
                    };
                    return data;
                }
                else {
                    return Viz.grid.FilterPortalTemplate.superclass.getEntityFromGridRecord.apply(this, arguments);
                }
            }
        });

Ext.reg("vizGridFilterPortalTemplate", Viz.grid.FilterPortalTemplate);
﻿/**
 * @class Viz.grid.FilterPlaylist
 * <p>
 * A grid exposing the business entity FilterPlaylist
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterPlaylist = Ext.extend(Viz.grid.GridPanel, {

            /**
             * @param {Boolean} isSecurableEntity <t>true</t> if the store is a SecurableObject store, <t>false</t> (default) otherwise
             * @type Boolean
             */
            isSecurableEntity       : false,
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang("msg_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterPlaylist_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterPlaylist_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],

                    columns      : config.isSecurableEntity ? Viz.Configuration.Columns.SecurableEntity : Viz.Configuration.Columns.Playlist

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterPlaylist.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("FilterPlaylistChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy               : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterPlaylistChange", this.reloadStore, this);
                Viz.grid.FilterPlaylist.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord             : function() {
                this.openGridDetail({
                            windowTitle     : $lang('msg_playlist'),
                            windowWidth     : 1000,
                            windowHeight    : 400,
                            windowIconCls   : 'viz-icon-small-playlist',
                            xtypeGridDetail : 'vizGridPlaylist'
                        });
            },
            /**
             * Return the data to add to the store from the detail grid selected record.
             */
            getEntityFromGridRecord : function(r) {
                if (this.isSecurableEntity) {
                    var data = {
                        KeySecurable      : r.data.KeyPlaylist,
                        SecurableName     : r.data.Name,
                        SecurableTypeName : 'Vizelia.FOL.BusinessEntities.Playlist',
                        SecurableIconCls  : 'viz-icon-small-playlist'
                    };
                    return data;
                }
                else {
                    return Viz.grid.FilterPlaylist.superclass.getEntityFromGridRecord.apply(this, arguments);
                }
            }
        });

Ext.reg("vizGridFilterPlaylist", Viz.grid.FilterPlaylist);
﻿/**
 * @class Viz.grid.CalendarEventCategory
 * <p>
 * A grid exposing the business entity CalendarEventCategory
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.CalendarEventCategory = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_calendareventcategory_add"),
                                iconCls : 'viz-icon-small-calendareventcategory-add',
                                hidden  : !$authorized('@@@@ Calendar Event - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_calendareventcategory_update"),
                                iconCls : 'viz-icon-small-calendareventcategory-update',
                                hidden  : !$authorized('@@@@ Calendar Event - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_calendareventcategory_delete"),
                                iconCls : 'viz-icon-small-calendareventcategory-delete',
                                hidden  : !$authorized('@@@@ Calendar Event - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 550,
                            height           : 170,
                            xtype            : 'vizFormCalendarEventCategory',
                            messageSaveStart : 'CalendarEventCategoryChangeStart',
                            messageSaveEnd   : 'CalendarEventCategoryChange',
                            readonly         : !$authorized('@@@@ Calendar Event - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_calendareventcategory_add"),
                            iconCls : 'viz-icon-small-calendareventcategory-add'
                        },
                        update      : {
                            title               : $lang("msg_calendareventcategory_update"),
                            iconCls             : 'viz-icon-small-calendareventcategory-update',
                            titleEntity         : 'Label',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyCalendarEventCategory'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-calendareventcategory-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.CalendarEventCategory_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.CalendarEventCategory(),
                    columns      : Viz.Configuration.Columns.CalendarEventCategory
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.CalendarEventCategory.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("CalendarEventCategoryChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("CalendarEventCategoryChange", this.reloadStore, this);
                Viz.grid.CalendarEventCategory.superclass.onDestroy.apply(this, arguments);
            }
        });
Ext.reg("vizGridCalendarEventCategory", Viz.grid.CalendarEventCategory);
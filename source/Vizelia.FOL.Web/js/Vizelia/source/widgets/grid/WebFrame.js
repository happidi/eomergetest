﻿/**
 * @class Viz.grid.WebFrame
 * <p>
 * A grid exposing the business entity WebFrame
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.WebFrame = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor         : function(config) {
                config = config || {};

                this._favoritebutton_id = Ext.id();

                var defaultConfig = {
                    hasRowEditor  : false,
                    stripeRows    : false,
                    hasToolbar    : false,
                    autoLoadStore : false,
                    tbar          : [{
                                id            : this._favoritebutton_id,
                                iconCls       : 'viz-icon-small-chart-favorite',
                                text          : $lang("msg_chart_favorite"),
                                enableToggle  : true,
                                toggleHandler : function(button, state) {
                                    Viz.util.Portal.onToggleFavoriteButton.createDelegate(this, [button, state])();
                                },
                                scope         : this,
                                pressed       : true
                            }, '-', {
                                text    : $lang("msg_webframe_add"),
                                iconCls : 'viz-icon-small-webframe-add',
                                hidden  : !$authorized('@@@@ Web Frame - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_webframe_update"),
                                iconCls : 'viz-icon-small-webframe-update',
                                hidden  : !$authorized('@@@@ Web Frame - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_webframe_delete"),
                                iconCls : 'viz-icon-small-webframe-delete',
                                hidden  : !$authorized('@@@@ Web Frame - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_webframe_view"),
                                iconCls : 'viz-icon-small-display',
                                // hidden : !$authorized('WebFrame_view'),
                                scope   : this,
                                handler : this.onWebFrameDisplay
                            }, '-', {
                                text    : $lang("msg_portlets"),
                                iconCls : 'viz-icon-small-portlet',
                                hidden  : !$authorized('@@@@ Portlet'),
                                scope   : this,
                                handler : Viz.util.Portal.displayPortletGridFromEntity.createDelegate(this)
                            }],
                    formConfig    : this.getFormConfig(),
                    store         : new Viz.store.WebFrame(),
                    columns       : Viz.Configuration.Columns.WebFrame
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.WebFrame.superclass.constructor.call(this, config);
                this.addEvents(
                        /**
                         * @event beforewebframedisplay Fires before displaying a WebFrame.
                         * @param {Viz.grid.GridPanel} this.
                         * @param {Object} record The record of the WebFrame that is being displayed.
                         * @param {Object} entity The entity of the WebFrame that is being displayed. It could be changed by the handler.
                         */
                        'beforewebframedisplay');

                Viz.util.MessageBusMgr.subscribe("WebFrameChange", this.reloadStore, this);

                this.on({
                            render          : Viz.util.Portal.gridRender,
                            filteravailable : Viz.util.Portal.gridFilterAvailable,
                            filtercleared   : Viz.util.Portal.toggleFavoriteButton.createDelegate(this, [this._favoritebutton_id, false]),
                            scope           : this
                        });

            },

            /**
             * Destroy
             */
            onDestroy           : function() {
                Viz.util.MessageBusMgr.unsubscribe("WebFrameChange", this.reloadStore, this);
                Viz.grid.WebFrame.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * After deleting we make sure the portlet is deleted as well.
             * @param {} record
             * @param {} selections
             */
            onAfterDeleteRecord : function(record, selections) {
                for (var i = 0, r; r = selections[i]; i++) {
                    Viz.util.MessageBusMgr.fireEvent('PortletDelete', r.json);
                }
            },
            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig       : function() {
                return {
                    common      : {
                        width            : 700,
                        height           : 400,
                        xtype            : 'vizFormWebFrame',
                        messageSaveStart : 'WebFrameChangeStart',
                        messageSaveEnd   : 'WebFrameChange',
                        readonly         : !$authorized('@@@@ Web Frame - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_webframe_add"),
                        iconCls : 'viz-icon-small-webframe-add'
                    },
                    update      : {
                        title               : $lang("msg_webframe_update"),
                        iconCls             : 'viz-icon-small-webframe-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyWebFrame'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-webframe-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.WebFrame_FormUpdateBatch
                    }
                };
            },

            /**
             * Handler for displaying a webframe.
             * @method onChartDisplay
             */
            onWebFrameDisplay   : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    if (this.fireEvent("beforewebframedisplay", this, record, entity)) {
                        Viz.grid.WebFrame.openWebFrameDisplay(entity);
                    }
                }
            }
        });
Ext.reg("vizGridWebFrame", Viz.grid.WebFrame);

// Static Methods
Ext.apply(Viz.grid.WebFrame, {
            /**
             * Displays a WebFrame.
             * @method openWebFrameDisplay
             * @static
             * @param {Viz.BusinessEntity.WebFrame} entity : the WebFrame entity.
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openWebFrameDisplay : function(entity, width, height) {
                var win = new Viz.portal.PortletWindow({
                            entity        : entity,
                            xtypePortlet  : 'vizPortletWebFrame',
                            entityPortlet : {
                                TypeEntity : 'Vizelia.FOL.BusinessEntities.WebFrame'
                            },
                            updateHandler : Viz.grid.WebFrame.update
                        });
                win.show();
            },

            /**
             * Create a new WebFrame (to be called from desktop or tree)
             * @param {} callback
             */
            create              : function(entity) {
                Viz.openFormCrud(Viz.grid.WebFrame.prototype.getFormConfig(), 'create', entity || {});
            },

            /**
             * Update an existing WebFrame (to be called from desktop or tree)
             * @param {} callback
             */
            update              : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.WebFrame.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
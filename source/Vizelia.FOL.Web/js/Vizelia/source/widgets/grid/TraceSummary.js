﻿/**
 * @class Viz.grid.TraceSummary
 * <p>
 * A grid exposing the business entity TraceSummary
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.TraceSummary = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor     : false,
                    stripeRows       : false,

                    // cls : 'viz-grid-norowborder',
                    store            : new Viz.store.TraceSummary(),
                    autoExpandColumn : 'traceCategoryColumn',
                    autoExpandMax    : 3000,
                    viewConfig       : {
                        forceFit  : false,
                        emptyText : $lang('msg_trace_no_entries')
                    },
                    columns          : Viz.Configuration.Columns.TraceSummary,
                    hasToolbar: false,
                    hasPagingToolbar : false,
                    plugins          : [new Viz.plugins.GridExport({
                                        paging          : {
                                            start : 0,
                                            sort  : 'Severity',
                                            dir   : 'DESC'

                                        },
                                        maxRecords      : 1000,
                                        promptMaxRecord : true
                                    }), new Ext.ux.grid.GroupSummary()]
                };
                defaultConfig.store.groupField = 'Severity';
                var forcedConfig = {
                    pageSize : 500,
                    getView  : function() {
                        if (!this.view) {
                            this.view = new Ext.grid.GroupingView(this.viewConfig);
                        }

                        return this.view;
                    }
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.TraceSummary.superclass.constructor.call(this, config);

                this.on('celldblclick', function(grid, rowIndex, cellIndex, e) {
                            var store = grid.getStore().getAt(rowIndex);
                            var columnName = grid.getColumnModel().getDataIndex(cellIndex);
                            if (columnName === null || columnName === '')
                                return;
                            var cellValue = store.get(columnName);
                            var severityValue = store.get('Severity');
                            switch (severityValue) {
                                case 0 :
                                    severityValue = 'Information';
                                    break;
                                case 1 :
                                    severityValue = 'Warning';
                                    break;
                                case 2 :
                                    severityValue = 'Error';
                                    break;
                                case 3 :
                                    severityValue = 'Verbose';
                                    break;
                                default :
                                    break;
                            }
                            if (columnName === 'Severity') {
                                if (severityValue === 'Verbose') {
                                    cellValue = 'Summary';
                                }
                                else {
                                    cellValue = severityValue;
                                }
                            }

                            Viz.App.Global.ViewPort.openModule({
                                        xtype       : 'vizPanelTraceEntry',
                                        storeFilters : [{
                                                    data  : {
                                                        dataIndex : 'TraceCategory',
                                                        type      : 'string',
                                                        value     : cellValue
                                                    },
                                                    field : 'TraceCategory'
                                                }, {
                                                    data  : {
                                                        dataIndex : 'Severity',
                                                        type      : 'string',
                                                        value     : severityValue
                                                    },
                                                    field : 'Severity'
                                                }],
                                        title       : 'Trace Viewer'
                                    }, false, true);
                        });
            }
        });

Ext.reg("vizGridTraceSummary", Viz.grid.TraceSummary);
﻿/**
 * @class Viz.grid.EventLog
 * <p>
 * A grid exposing the business entity EventLog
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.EventLog = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_eventlog_add"),
                                iconCls: 'viz-icon-small-eventlog-add',
                                hidden  : !$authorized('@@@@ Event Log - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_eventlog_update"),
                                iconCls: 'viz-icon-small-eventlog-update',
                                hidden  : !$authorized('@@@@ Event Log - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_eventlog_delete"),
                                iconCls: 'viz-icon-small-eventlog-delete',
                                hidden  : !$authorized('@@@@ Event Log - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 430,
                            height           : 350,
                            xtype            : 'vizFormEventLog',
                            messageSaveStart : 'EventLogChangeStart',
                            messageSaveEnd   : 'EventLogChange',
                            readonly         : !$authorized('@@@@ Event Log - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_eventlog_add"),
                            iconCls: 'viz-icon-small-eventlog-add'
                        },
                        update      : {
                            title               : $lang("msg_eventlog_update"),
                            iconCls: 'viz-icon-small-eventlog-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyEventLog'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls: 'viz-icon-small-eventlog-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.EventLog_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.EventLog(),

                    columns      : Viz.Configuration.Columns.EventLog
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.EventLog.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("EventLogChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("EventLogChange", this.reloadStore, this);
                Viz.grid.EventLog.superclass.onDestroy.apply(this, arguments);
            }

        });

Ext.reg("vizGridEventLog", Viz.grid.EventLog);
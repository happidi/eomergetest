﻿/**
 * @class Viz.grid.AlarmDefinition
 * <p>
 * A grid exposing the business entity AlarmDefinition
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.MeterValidationRule = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor                  : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_metervalidationrule_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden  : !$authorized('@@@@ Meter Validation Rule - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_metervalidationrule_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden  : !$authorized('@@@@ Meter Validation Rule - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_metervalidationrule_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ Meter Validation Rule - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_metervalidationrule_wizard"),
                                iconCls : 'viz-icon-small-wizard',
                                hidden  : !$authorized('@@@@ Meter Validation Rule - Full Control'),
                                scope   : this,
                                handler : Viz.grid.MeterValidationRule.launchWizard
                            }, {
                                text    : $lang("msg_metervalidationrule_process"),
                                iconCls : 'viz-icon-small-action',
                                hidden  : !$authorized('@@@@ Meter Validation Rule - Full Control'),
                                scope   : this,
                                handler : this.onProcessMeterValidationRule
                            }],
                    formConfig   : this.getFormConfig(),
                    store        : new Viz.store.MeterValidationRule(),
                    columns      : Viz.Configuration.Columns.MeterValidationRule
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.MeterValidationRule.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("MeterValidationRuleChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy                    : function() {
                Viz.util.MessageBusMgr.unsubscribe("MeterValidationRuleChange", this.reloadStore, this);
                Viz.grid.MeterValidationRule.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Return the form config
             * @return {}
             */
            getFormConfig                : function() {
                return {
                    common      : {
                        width            : 800,
                        height           : 450,
                        xtype            : 'vizFormMeterValidationRule',
                        messageSaveStart : 'MeterValidationRuleChangeStart',
                        messageSaveEnd   : 'MeterValidationRuleChange',
                        readonly         : !$authorized('@@@@ Meter Validation Rule - Full Control')

                    },
                    create      : {
                        title   : $lang("msg_metervalidationrule_add"),
                        iconCls : 'viz-icon-small-metervalidationrule-add'
                    },
                    update      : {
                        title               : $lang("msg_metervalidationrule_update"),
                        iconCls             : 'viz-icon-small-metervalidationrule-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyMeterValidationRule'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-metervalidationrule-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.MeterValidationRule_FormUpdateBatch
                    }
                };
            },

            /**
             * Process the selected Meter Validation Rule
             */
            onProcessMeterValidationRule : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;

                var msgDelete;
                if (selections.length > 1)
                    msgDelete = String.format($lang('msg_metervalidationrule_process_confirmation_multiple'), selections.length);
                else
                    msgDelete = $lang('msg_metervalidationrule_process_confirmation_single');

                Ext.MessageBox.confirm($lang('msg_metervalidationrule_process'), msgDelete, function(button) {
                            if (button == 'yes') {
                                var keys = [];
                                for (var i = 0, r; r = selections[i]; i++) {
                                    keys.push(r.json.KeyMeterValidationRule);
                                }
                                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                            pollingInterval        : 1000,
                                            serviceStart           : Viz.Services.EnergyWCF.MeterValidationRule_ProcessBegin,
                                            serviceParams          : {
                                                Keys : keys
                                            },
                                            showProgress           : true,
                                            showProgressPercentage : true,
                                            modal                  : false,
                                            progressWindowTitle    : $lang('msg_metervalidationrule_process')
                                        });
                                submitter.start();
                            }

                        }, this);
            },

            deleteRecordInternal         : function(selections) {
                for (var i = 0; i < selections.length; i++) {
                    var r = selections[i];
                    this.store.remove(r);
                }
                if (this.store.writer) { // We don't need to call save if the store does not have a writer, for example a memory store.
                    this.onSaveStore();
                    var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                pollingInterval        : 1000,
                                serviceStart           : Viz.Services.EnergyWCF.MeterValidationRule_BeginSaveStore,
                                serviceParams          : {
                                    store : this.store.crudStore
                                },
                                showProgress           : true,
                                showProgressPercentage : true,
                                modal                  : true,
                                progressWindowTitle    : $lang('msg_alarmdefinition_delete')
                            });
                    submitter.start();
                }
                this.fireEvent("deleterecord", this, selections);
            }
        });
Ext.reg("vizGridMeterValidationRule", Viz.grid.MeterValidationRule);

Ext.apply(Viz.grid.MeterValidationRule, {

            /**
             * Launch the wizard
             */
            launchWizard : function(type) {
                var config = Viz.grid.MeterValidationRule.prototype.getFormConfig();
                config.common.isWizard = true;
                Viz.openFormCrud(config, 'create', {});
            }
        });

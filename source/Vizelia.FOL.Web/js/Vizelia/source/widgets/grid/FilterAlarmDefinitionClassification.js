﻿/**
 * @class Viz.grid.FilterAlarmDefinitionClassification
 * <p>
 * A grid exposing a list of AlarmDefinition Classification
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterAlarmDefinitionClassification = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang('msg_alarmdefinition_classification_add'),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterAlarmDefinitionClassification_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_alarmdefinition_classification_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterAlarmDefinitionClassification_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],
                    columns      : Viz.Configuration.Columns.ClassificationItem
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterAlarmDefinitionClassification.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("FilterAlarmDefinitionClassificationChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterAlarmDefinitionClassificationlChange", this.reloadStore, this);
                Viz.grid.FilterAlarmDefinitionClassification.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord : function() {
                var entity = {};
                if (this.fireEvent("beforeaddrecord", this, entity)) {
                    this.openTreeDetail({
                                windowTitle     : $lang('msg_alarmdefinition_classification'),
                                windowWidth     : 350,
                                windowHeight    : 400,
                                windowIconCls   : 'viz-icon-small-classificationitem',
                                xtypeTreeDetail : 'vizTreeAlarmDefinitionClassification',
                                keyColumn       : 'KeyClassificationItem'
                            });
                }
            }
        });

Ext.reg("vizGridFilterAlarmDefinitionClassification", Viz.grid.FilterAlarmDefinitionClassification);
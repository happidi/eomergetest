﻿/**
 * @class Viz.grid.DataPoint
 * <p>
 * A grid exposing the business entity DataPoint
 * </p>
 * @extends Viz.grid.GridPanel
 */
/*
 * Viz.grid.DataPoint1 = Ext.extend(Viz.grid.GridPanel, { constructor : function(config) { config = config || {}; var defaultConfig = { hasRowEditor : false, stripeRows : true, store : config.store || new Viz.store.DataPoint({ KeyChart : config.KeyChart }), columns : Viz.Configuration.Columns.DataPoint, view : new Ext.grid.GroupingView({ groupTextTpl : '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "' + $lang('msg_grid_items') + '" : "' + $lang('msg_grid_item') + '"]})', groupByText : $lang('msg_grid_groupbythisfield'), showGroupsText : $lang('msg_grid_showbygroups') }) }; var forcedConfig = {}; Ext.applyIf(config, defaultConfig); Ext.apply(config, forcedConfig); Viz.grid.DataPoint1.superclass.constructor.call(this, config); }, reloadChart : function() { this.reloadStore(); } });
 */

Ext.grid.PivotAggregatorMgr.registerType('sumDataPoint', function(records, measure) {
            var length = records.length, total = 0, i;
            for (i = 0; i < length; i++) {
                total += records[i].get(measure);
            }
            return length > 0 ? total : ' - ';
        });

Viz.grid.DataPoint = Ext.extend(Ext.grid.PivotGrid, {

            /**
             * @cfg {KeyChart} The key of the chart .
             */

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                /*
                 * var store = new Viz.store.DataPoint({ KeyChart : config.KeyChart, autoLoad : true });
                 */
                config = config || {};
                var defaultConfig = {
                    store                        : config.store,
                    pageSize                     : 50,
                    stripeRows                   : true,
                    loadMask                     : true,
                    aggregator                   : 'sumDataPoint',
                    measure                      : 'YValue',
                    header: false,
                    direction: 'ASC',
                    transpose                    : false,
                    transposeLocationWidth       : 150,
                    transposeClassificationWidth : 120,
                    numberFormat                 : '0,000',
                    autoLoadStore                : false,
                    viewConfig                   : {
                        title : config.title
                    },
                    renderer                     : function(value) {
                        if (Ext.isNumber(value)) {
                            var retVal = Ext.util.Format.number(value, this.numberFormat);
                            return retVal;
                        }
                        return value;
                    }.createDelegate(this),
                    listeners                    : {
                        afterrender : function(grid) {
                            this.view.refresh(true);
                        },
                        scope       : this
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                var pagingConfig = {
                    xtype          : 'paging',
                    displayInfo    : false,
                    enableOverflow : true,
                    pageSize       : config.pageSize,
                    store          : config.store
                };
                if (config.bbar) {
                    config.bbar.splice(0, 0, pagingConfig);
                }
                else
                    config.bbar = pagingConfig;

                var rendererEllipsis = function(val) {
                    return '<div class="x-pivotgrid-ellipsis" ext:qtip="' + val + '">' + val + '</div>';
                }

                if (config.transpose) {
                    Ext.apply(config, {
                                topAxis  : [{
                                            renderer  : function(val) {
                                                if (val && Ext.isFunction(val.format))
                                                    return '<span style="display:none" >' + val.format(Date.patterns.UniversalSortableDateTime) + '</span>';
                                                return val;
                                            },
                                            dataIndex : 'XDateTime' // in order to sort the columns properly
                                        }, {
                                            width     : 100,
                                            dataIndex: 'Name',
                                            renderer  : rendererEllipsis
                                        }],

                                leftAxis : [{
                                            width     : config.transposeLocationWidth ? config.transposeLocationWidth : 150,
                                            dataIndex : 'Location',
                                            direction: config.direction,
                                            renderer: rendererEllipsis
                                        }, {
                                            width     : config.transposeClassificationWidth ? config.transposeClassificationWidth : 120,
                                            dataIndex : 'Classification',
                                            direction: config.direction,
                                            renderer: rendererEllipsis
                                        }]
                            });
                }
                else {
                    Ext.apply(config, {
                                leftAxis : [{
                                            width     : 1,
                                            renderer  : function(val) {
                                                if (val && Ext.isFunction(val.format))
                                                    return val.format(Date.patterns.UniversalSortableDateTime);
                                                return val;
                                            },
                                            dataIndex : 'XDateTime'// in order to sort the rows properly
                                        }, {
                                            width     : 100,
                                            dataIndex : 'Name',
                                            renderer  : rendererEllipsis
                                        }],

                                topAxis  : [{
                                            width     : 120,
                                            dataIndex : 'Location',
                                            renderer  : rendererEllipsis
                                        }, {
                                            width     : 120,
                                            dataIndex : 'Classification',
                                            renderer  : rendererEllipsis
                                        }]
                            });
                }

                Viz.grid.DataPoint.superclass.constructor.call(this, config);

                this.on({
                            rowclick : {
                                scope : this,
                                fn    : this.onRowClick
                            }
                        });
            },

            /**
             * Destroy
             * @private
             */
            onDestroy   : function() {
            	this.un('rowclick', this.onRowClick, this);
            	
                if (this.store && this.store.proxy && this.store.proxy.lastRequest) {
                    Viz.util.Chart.cancelRequest(this.store.proxy.lastRequest);
                }
                Viz.grid.DataPoint.superclass.onDestroy.apply(this, arguments);
            },

            reloadChart : function() {
                this.store.reload({
                            params : {
                                start : 0,
                                limit : this.pageSize
                            }
                        });
            },

            onRowClick  : function(grid, row, event) {
                if (this.entity && this.entity.DrillDownEnabled) {
                    event.stopEvent();
                    var xy = event.getXY();
                    this.getSelectionModel().selectRow(row, false);
                    if (this.hideMenu == true)
                        return;

                    var entity = this.store.getAt(row).json;
                    var mode = Viz.Configuration.GetEnumLabel('ChartDrillDownMode', this.entity.DrillDownMode);

                    Viz.grid.Chart.onDataPointClick(this.entity.KeyChart, entity.XDateTime, null, null, entity.Location, entity.Classification, entity.Name, mode);
                }
            }

        });

Ext.reg("vizGridDataPoint", Viz.grid.DataPoint);
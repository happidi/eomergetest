﻿/**
 * @class Viz.grid.FilterReport
 * <p>
 * A grid exposing the business entity FilterReport
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.FilterReport = Ext.extend(Viz.grid.GridPanel, {
            /**
             * @param {Boolean} isSecurableEntity <t>true</t> if the store is a SecurableObject store, <t>false</t> (default) otherwise
             * @type Boolean
             */
            isSecurableEntity     : false,
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor           : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,

                    tbar         : [{
                                text    : $lang("msg_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('FilterReport_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang('msg_delete'),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('FilterReport_write'),
                                scope   : this,
                                handler : this.deleteSelectedRows
                            }],

                    columns      : config.isSecurableEntity ? Viz.Configuration.Columns.SecurableEntity : Viz.Configuration.Columns.Report

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.FilterReport.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("FilterReportChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy             : function() {
                Viz.util.MessageBusMgr.unsubscribe("FilterReportChange", this.reloadStore, this);
                Viz.grid.FilterReport.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the Add item button.
             */
            onAddRecord           : function() {
                this.openTreeDetail({
                            windowTitle     : $lang('msg_reports'),
                            windowWidth     : 350,
                            windowHeight    : 400,
                            windowIconCls   : 'viz-icon-small-report',
                            xtypeTreeDetail : 'vizTreeReport'
                        });
            },

            getEntityFromTreeNode : function(node) {
                if (this.isSecurableEntity) {
                    var data = {
                        // the Key should be removed when we refactor AzManFilter
                        KeySecurable      : node.attributes.Key,
                        SecurableName     : node.text,
                        SecurableTypeName : 'Vizelia.FOL.BusinessEntities.Report',
                        SecurableIconCls  : node.attributes.iconCls
                    };
                    return data;
                }
                else {
                    return Viz.grid.FilterReport.getEntityFromTreeNode.constructor.apply(this, arguments);
                }
            }
        });

Ext.reg("vizGridFilterReport", Viz.grid.FilterReport);
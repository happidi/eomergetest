﻿/**
 * @class Viz.grid.MachineInstance
 * <p>
 * A grid exposing the business entity MachineInstance
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.MachineInstance = Ext.extend(Viz.grid.GridPanel, {

            refreshInterval             : 60,
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor                 : function(config) {
                config = config || {};
                var summaryGroup = new Ext.ux.grid.GroupSummary();
                var summaryGrid = new Ext.ux.grid.GridSummary();

                var defaultConfig = {
                    hasRowEditor       : false,
                    stripeRows         : false,
                    hasToolbar         : false,
                    tbar               : [{
                                text    : $lang("msg_machineinstance_add"),
                                iconCls : 'viz-icon-small-machineinstance-add',
                                // hidden : !$authorized('MachineInstance_add'),
                                scope   : this,
                                handler : this.onMachineDelete
                            }, {
                                text    : $lang("msg_machineinstance_restart"),
                                iconCls : 'viz-icon-small-machineinstance-restart',
                                // hidden : !$authorized('MachineInstance_restart'),
                                scope   : this,
                                handler : this.onMachineRestart
                            }, {
                                text    : $lang("msg_machineinstance_delete"),
                                iconCls : 'viz-icon-small-machineinstance-delete',
                                // hidden : !$authorized('Map_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_machineinstance_memoryusage_view"),
                                iconCls : 'viz-icon-small-display',
                                scope   : this,
                                handler : this.onMachineMemoryUsageDisplay
                            }],
                    store              : new Viz.store.MachineInstance({
                                groupField : 'KeyMachineInstance'
                            }),
                    view               : new Ext.grid.GroupingView({
                                forceFit           : true,
                                startCollapsed     : false,
                                showGroupName      : false,
                                enableNoGroups     : true,
                                enableGroupingMenu : false,
                                hideGroupedColumn  : true,
                                enableGrouping     : false,
                                groupTextTpl       : '{text}'
                            }),
                    formConfig         : this.getFormConfig(),
                    columns            : Viz.Configuration.Columns.MachineInstance,
                    plugins            : [summaryGroup, summaryGrid],
                    loadMask           : false,
                    autoReloadInterval : 60
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.MachineInstance.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("MachineInstanceChange", this.reloadStore, this);

            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig               : function() {
                return {
                    common : {
                        width            : 550,
                        height           : 200,
                        xtype            : 'vizFormMachineInstance',
                        messageSaveStart : 'MachineInstanceChangeStart',
                        messageSaveEnd   : 'MachineInstanceChange'
                    },
                    create : {
                        title   : $lang("msg_machineinstance_add"),
                        iconCls : 'viz-icon-small-machineinstance-add'
                    }
                };
            },

            /**
             * Destroy
             */
            onDestroy                   : function() {
                Viz.util.MessageBusMgr.unsubscribe("MachineInstanceChange", this.reloadStore, this);
                Viz.grid.MachineInstance.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Handler for the restart button.
             */
            onMachineDelete             : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    this.store.remove(record);
                    var entity = record.json;
                    Viz.Services.EnergyWCF.MachineInstance_Delete({
                                item    : entity,
                                success : function() {

                                },
                                scope   : this
                            });
                }
            },

            /**
             * Handler for the restart button.
             */
            onMachineRestart            : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    Viz.Services.EnergyWCF.MachineInstance_Restart({
                                item    : entity,
                                success : function() {

                                },
                                scope   : this
                            });
                }
            },

            /**
             * Display the memory usage chart.
             */
            onMachineMemoryUsageDisplay : function() {
                var win = new Ext.Window({
                            width       : 800,
                            height      : 600,
                            maximazable : true,
                            minimizable : true,
                            layout      : 'fit',
                            items       : {
                                xtype            : 'vizViewerImage',
                                stretchImage     : false,
                                serviceInterface : 'Energy.svc',
                                serviceStream    : "MachineInstance_MemoryUsageGetStreamImageFromCache",
                                serviceHandler   : Viz.Services.EnergyWCF.MachineInstance_MemoryUsageBuildImage
                            }
                        });
                win.show();
            }

        });
Ext.reg("vizGridMachineInstance", Viz.grid.MachineInstance);

﻿/**
 * @class Viz.grid.Playlist
 * <p>
 * A grid exposing the business entity Playlist
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Playlist = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor        : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_playlist_add"),
                                iconCls : 'viz-icon-small-playlist-add',
                                hidden  : !$authorized('@@@@ Playlist - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_playlist_update"),
                                iconCls: 'viz-icon-small-playlist-update',
                                hidden  : !$authorized('@@@@ Playlist - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_playlist_delete"),
                                iconCls: 'viz-icon-small-playlist-delete',
                                hidden  : !$authorized('@@@@ Playlist - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_playlist_generate"),
                                iconCls : 'viz-icon-small-action',
                                hidden  : !$authorized('@@@@ Playlist - Full Control'),
                                scope   : this,
                                handler : this.onGeneratePlaylist
                            }],
                    formConfig   : {
                        common      : {
                            width            : 760,
                            height           : 300,
                            xtype            : 'vizFormPlaylist',
                            messageSaveStart : 'PlaylistChangeStart',
                            messageSaveEnd   : 'PlaylistChange',
                            readonly         : !$authorized('@@@@ Playlist - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_playlist_add"),
                            iconCls: 'viz-icon-small-playlist-add'
                        },
                        update      : {
                            title               : $lang("msg_playlist_update"),
                            iconCls: 'viz-icon-small-playlist-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyPlaylist'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls: 'viz-icon-small-playlist-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.Playlist_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.Playlist(),
                    columns      : Viz.Configuration.Columns.Playlist
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.Playlist.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("PlaylistChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy          : function() {
                Viz.util.MessageBusMgr.unsubscribe("PlaylistChange", this.reloadStore, this);
                Viz.grid.Playlist.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Generate the selected Playlist
             */
            onGeneratePlaylist : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;

                var msgDelete;
                if (selections.length > 1)
                    msgDelete = String.format($lang('msg_playlist_generate_confirmation_multiple'), selections.length);
                else
                    msgDelete = $lang('msg_playlist_generate_confirmation_single');

                Ext.MessageBox.confirm($lang('msg_playlist_generate'), msgDelete, function(button) {
                            if (button == 'yes') {
                                var keys = [];
                                for (var i = 0, r; r = selections[i]; i++) {
                                    keys.push(r.json.KeyPlaylist);
                                }

                                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                            pollingInterval     : 1000,
                                            serviceStart        : Viz.Services.EnergyWCF.Playlist_GenerateBegin,
                                            serviceParams       : {
                                                Keys : keys
                                            },
                                            showProgress: true,
                                            showProgressPercentage: false,
                                            modal               : false,
                                            progressWindowTitle : $lang('msg_playlist_generate')
                                        });
                                submitter.start();
                            }

                        }, this);
            }
        });
Ext.reg("vizGridPlaylist", Viz.grid.Playlist);
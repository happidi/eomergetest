﻿/**
 * @class Viz.grid.Approval
 * <p>
 * A grid exposing the business entity Approval
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Approval = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor  : false,
                    stripeRows    : false,
                    autoLoadStore : false,
                    hasToolbar    : false,
                    tbar          : [{
                                text    : $lang("msg_approval_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('Approval_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_approval_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('Approval_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_approval_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('Approval_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig    : {
                        common      : {
                            width            : 400,
                            height           : 250,
                            xtype            : 'vizFormApproval',
                            messageSaveStart : 'ApprovalChangeStart',
                            messageSaveEnd   : 'ApprovalChange'
                        },
                        create      : {
                            title   : $lang("msg_approval_add"),
                            iconCls : 'viz-icon-small-approval-add'
                        },
                        update      : {
                            title               : $lang("msg_approval_update"),
                            iconCls             : 'viz-icon-small-approval-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyApproval'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-approval-update',
                            serviceHandlerUpdateBatch : Viz.Services.ServiceDeskWCF.Approval_FormUpdateBatch
                        }
                    },
                    store         : new Viz.store.Approval(),
                    columns       : Viz.Configuration.Columns.Approval
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.Approval.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("ApprovalChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("ApprovalChange", this.reloadStore, this);
                Viz.grid.Approval.superclass.onDestroy.apply(this, arguments);
            }

        });

Ext.reg("vizGridApproval", Viz.grid.Approval);
﻿/**
 * @class Viz.grid.Mail
 * <p>
 * A grid exposing the business entity Mail
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Mail = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_mail_add"),
                                iconCls : 'viz-icon-small-mail-add',
                                hidden  : !$authorized('@@@@ Mail - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_mail_update"),
                                iconCls : 'viz-icon-small-mail-update',
                                hidden  : !$authorized('@@@@ Mail - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_mail_delete"),
                                iconCls : 'viz-icon-small-mail-delete',
                                hidden  : !$authorized('@@@@ Mail - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        auditEnabled : false,
                        common         : {
                            width            : 850,
                            height           : 580,
                            xtype            : 'vizFormMail',
                            messageSaveStart : 'MailChangeStart',
                            messageSaveEnd   : 'MailChange',
                            readonly         : !$authorized('@@@@ Mail - Full Control')
                        },
                        create         : {
                            title   : $lang("msg_mail_add"),
                            iconCls : 'viz-icon-small-mail-add'
                        },
                        update         : {
                            title               : $lang("msg_mail_update"),
                            iconCls             : 'viz-icon-small-mail-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyMail'
                                    }]
                        },
                        updatebatch    : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-mail-update',
                            serviceHandlerUpdateBatch : Viz.Services.ServiceDeskWCF.Mail_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.Mail(),
                    columns      : Viz.Configuration.Columns.Mail
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.Mail.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("MailChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("MailChange", this.reloadStore, this);
                Viz.grid.Mail.superclass.onDestroy.apply(this, arguments);
            }
        });
Ext.reg("vizGridMail", Viz.grid.Mail);

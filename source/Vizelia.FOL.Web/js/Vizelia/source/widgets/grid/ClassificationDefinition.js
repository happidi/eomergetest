﻿/**
 * @class Viz.grid.ClassificationDefinition
 * <p>
 * A grid exposing the business entity ClassificationDefinition
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.ClassificationDefinition = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_classificationdefinition_add"),
                                iconCls : 'viz-icon-small-classificationdefinition-add',
                                hidden  : !$authorized('@@@@ Manage Classification definitions - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_classificationdefinition_update"),
                                iconCls : 'viz-icon-small-classificationdefinition-update',
                                hidden  : !$authorized('@@@@ Manage Classification definitions - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_classificationdefinition_delete"),
                                iconCls : 'viz-icon-small-classificationdefinition-delete',
                                hidden  : !$authorized('@@@@ Manage Classification definitions - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common : {
                            width            : 450,
                            height           : 200,
                            xtype            : 'vizFormClassificationDefinition',
                            messageSaveStart : 'ClassificationDefinitionChangeStart',
                            messageSaveEnd   : 'ClassificationDefinitionChange',
                            readonly         : !$authorized('@@@@ Manage Classification definitions - Full Control')
                        },
                        create : {
                            title   : $lang("msg_classificationdefinition_add"),
                            iconCls : 'viz-icon-small-classificationdefinition-add'
                        },
                        update : {
                            title               : $lang("msg_classificationdefinition_update"),
                            iconCls             : 'viz-icon-small-classificationdefinition-update',
                            titleEntity         : 'Category',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'Category'
                                    }]
                        }
                    },
                    store        : new Viz.store.ClassificationDefinition(),
                    columns      : Viz.Configuration.Columns.ClassificationDefinition
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.ClassificationDefinition.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("ClassificationDefinitionChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("ClassificationDefinitionChange", this.reloadStore, this);
                Viz.grid.ClassificationDefinition.superclass.onDestroy.apply(this, arguments);
            }

        });

Ext.reg("vizGridClassificationDefinition", Viz.grid.ClassificationDefinition);

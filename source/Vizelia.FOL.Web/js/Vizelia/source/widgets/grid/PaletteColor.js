﻿/**
 * @class Viz.grid.PaletteColor
 * <p>
 * A grid exposing the business entity PaletteColor
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.PaletteColor = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_palettecolor_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('PaletteColor_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_palettecolor_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('PaletteColor_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_palettecolor_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('PaletteColor_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 350,
                            height           : 180,
                            xtype            : 'vizFormPaletteColor',
                            messageSaveStart : 'PaletteColorChangeStart',
                            messageSaveEnd   : 'PaletteColorChange'
                        },
                        create      : {
                            title   : $lang("msg_palettecolor_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_palettecolor_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'Title',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyPaletteColor'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.PaletteColor_FormUpdateBatch
                        }
                    },
                    store        : config.store || new Viz.store.PaletteColor({
                                KeyPalette : config.KeyPalette
                            }),
                    columns      : Viz.Configuration.Columns.PaletteColor
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.PaletteColor.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("PaletteColorChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("PaletteColorChange", this.reloadStore, this);
                Viz.grid.PaletteColor.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortal.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyPalette = this.KeyPalette;
                if (KeyPalette == '' || KeyPalette == null) {
                    return false;
                }
                entity.KeyPalette = KeyPalette;
                entity.Order = this.getMaxValueFromStore('Order') + 1;

                return true;
            }

        });

Ext.reg("vizGridPaletteColor", Viz.grid.PaletteColor);
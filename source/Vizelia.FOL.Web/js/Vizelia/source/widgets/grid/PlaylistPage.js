﻿/**
 * @class Viz.grid.PlaylistPage
 * <p>
 * A grid exposing the business entity PlaylistPage
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.PlaylistPage = Ext.extend(Viz.grid.GridPanel, {

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    viewConfig   : {
                        markDirty : false
                    },
                    tbar         : [{
                                text    : $lang("msg_playlistpage_add"),
                                iconCls : 'viz-icon-small-add',
                                // hidden : !$authorized('PlaylistPage_write'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_playlistpage_update"),
                                iconCls : 'viz-icon-small-update',
                                // hidden : !$authorized('PlaylistPage_write'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_playlistpage_delete"),
                                iconCls : 'viz-icon-small-delete',
                                // hidden : !$authorized('PlaylistPage_write'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_playlistpage_view"),
                                iconCls : 'viz-icon-small-display',
                                // hidden : !$authorized('PlaylistPage_write'),
                                scope   : this,
                                handler : this.onPlaylistPageDisplay
                            }],
                    formConfig   : {
                        common      : {
                            width            : 650,
                            height           : 180,
                            xtype            : 'vizFormPlaylistPage',
                            messageSaveStart : 'PlaylistPageChangeStart',
                            messageSaveEnd   : 'PlaylistPageChange'
                        },
                        create      : {
                            title   : $lang("msg_playlistpage_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_playlistpage_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'KeyPlaylistPage',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyPlaylistPage'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.PlaylistPage_FormUpdateBatch
                        }
                    },
                    store        : config.store || new Viz.store.PlaylistPage({
                                KeyPlaylist : config.KeyPlaylist
                            }),
                    columns      : Viz.Configuration.Columns.PlaylistPage
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.grid.PlaylistPage.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("PlaylistPageChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy               : function() {
                Viz.util.MessageBusMgr.unsubscribe("PlaylistPageChange", this.reloadStore, this);
                Viz.grid.PlaylistPage.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortal.
             */
            onBeforeAddRecord       : function(grid, entity) {
                var KeyPlaylist = this.KeyPlaylist;
                if (KeyPlaylist == '' || KeyPlaylist == null) {
                    return false;
                }
                entity.KeyPlaylist = KeyPlaylist;
                entity.Order = this.getMaxValueFromStore('Order') + 1;

                return true;
            },

            /**
             * Handler for displaying a chart.
             * @method onChartDisplay
             */
            onPlaylistPageDisplay   : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    this.openPlaylistPageDisplay(entity, 1024, 768);

                }
            },

            /**
             * Displays a PlaylistPage.
             * @method openPlaylistPageDisplay
             * @param {Viz.BusinessEntity.PlaylistPage} entity : the PlaylistPage entity.
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openPlaylistPageDisplay : function(entity, width, height) {
                var win = new Ext.Window({
                            maximized   : true,
                            maximizable : true,
                            height      : entity.PlaylistHeight,
                            width       : entity.PlaylistWidth,
                            layout      : 'fit',
                            title       : $lang('msg_portaltab'),
                            items       : {
                                xtype          : 'vizChartPortalTabImage',
                                entity         : {
                                    KeyPortalTab : entity.KeyPortalTab
                                },
                                overrideSize   : true,
                                overrideHeight : entity.PlaylistHeight,
                                overrideWidth  : entity.PlaylistWidth
                            }
                        });
                win.show();
            }

        });

Ext.reg("vizGridPlaylistPage", Viz.grid.PlaylistPage);
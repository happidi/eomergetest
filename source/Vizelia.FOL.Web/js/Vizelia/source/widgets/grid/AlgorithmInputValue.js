﻿/**
 * @class Viz.grid.Algorithm
 * <p>
 * A grid exposing the business entity Algorithm
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.AlgorithmInputValue = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor  : false,
                    hasToolbar    : false,
                    autoLoadStore : true,
                    tbar          : [{
                        text: $lang("msg_algorithminputvalue_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden: !$authorized('@@@@ Chart - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text: $lang("msg_algorithminputvalue_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden: !$authorized('@@@@ Chart - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text: $lang("msg_algorithminputvalue_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden: !$authorized('@@@@ Chart - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig    : this.getFormConfig(),
                    store: config.store || new Viz.store.AlgorithmInputValue({
                        KeyAlgorithm: config.KeyAlgorithm,
                                KeyChart  : config.KeyChart
                            }),
                            columns: Viz.Configuration.Columns.AlgorithmInputValue
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.AlgorithmInputValue.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("AlgorithmInputValueChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy         : function() {
                Viz.util.MessageBusMgr.unsubscribe("AlgorithmInputValueChange", this.reloadStore, this);
                Viz.grid.AlgorithmInputValue.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig     : function() {
                return {
                    common      : {
                        width            : 500,
                        height           : 160,
                        xtype: 'vizFormAlgorithmInputValue',
                        messageSaveStart: 'AlgorithmInputValueChangeStart',
                        messageSaveEnd: 'AlgorithmInputValueChange',
                        readonly: !$authorized('@@@@ Chart - Full Control')
                    },
                    create      : {
                        title: $lang("msg_algorithminputvalue_add"),
                        iconCls : 'viz-icon-small-add'
                    },
                    update      : {
                        title: $lang("msg_algorithminputvalue_update"),
                        iconCls             : 'viz-icon-small-update',
                        titleEntity         : 'Name',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value: 'KeyAlgorithmInputValue'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-update',
                        serviceHandlerUpdateBatch: Viz.Services.EnergyWCF.AlgorithmInputValue_FormUpdateBatch
                    }
                };
            },

            /**
             * @private Handler for updating the entity with the parent KeyPortal.
             */
            onBeforeAddRecord : function(grid, entity) {
                var KeyAlgorithm = this.KeyAlgorithm;
                if (KeyAlgorithm == '' || KeyAlgorithm == null) {
                    return false;
                }
                entity.KeyAlgorithm = KeyAlgorithm;

                var KeyChart = this.KeyChart;
                if (KeyChart == '' || KeyChart == null) {
                    return false;
                }
                entity.KeyChart = KeyChart;

                return true;
            }

        });
        Ext.reg("vizGridAlgorithmInputValue", Viz.grid.AlgorithmInputValue);
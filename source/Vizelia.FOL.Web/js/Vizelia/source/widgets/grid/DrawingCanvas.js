﻿/**
 * @class Viz.grid.DrawingCanvas
 * <p>
 * A grid exposing the business entity DrawingCanvas
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.DrawingCanvas = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor            : function(config) {
                config = config || {};

                this._favoritebutton_id = Ext.id();

                var defaultConfig = {
                    hasRowEditor  : false,
                    stripeRows    : false,
                    hasToolbar    : false,
                    autoLoadStore : false,
                    tbar          : [{
                                id            : this._favoritebutton_id,
                                iconCls       : 'viz-icon-small-chart-favorite',
                                text          : $lang("msg_chart_favorite"),
                                enableToggle  : true,
                                toggleHandler : function(button, state) {
                                    Viz.util.Portal.onToggleFavoriteButton.createDelegate(this, [button, state])();
                                },
                                scope         : this,
                                pressed       : true
                            }, '-', {
                                text    : $lang("msg_drawingcanvas_add"),
                                iconCls : 'viz-icon-small-drawingcanvas-add',
                                hidden  : !$authorized('@@@@ Drawing Canvas - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_drawingcanvas_update"),
                                iconCls : 'viz-icon-small-drawingcanvas-update',
                                hidden  : !$authorized('@@@@ Drawing Canvas - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_drawingcanvas_delete"),
                                iconCls : 'viz-icon-small-drawingcanvas-delete',
                                hidden  : !$authorized('@@@@ Drawing Canvas - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }, {
                                text    : $lang("msg_drawingcanvas_view"),
                                iconCls : 'viz-icon-small-display',
                                // hidden : !$authorized('DrawingCanvas_view'),
                                scope   : this,
                                handler : this.onDrawingCanvasDisplay
                            }, '-', {
                                text    : $lang("msg_portlets"),
                                iconCls : 'viz-icon-small-portlet',
                                hidden  : !$authorized('@@@@ Portlet'),
                                scope   : this,
                                handler : Viz.util.Portal.displayPortletGridFromEntity.createDelegate(this)
                            }],
                    formConfig    : this.getFormConfig(),
                    store         : new Viz.store.DrawingCanvas(),
                    columns       : Viz.Configuration.Columns.DrawingCanvas
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.DrawingCanvas.superclass.constructor.call(this, config);
                this.addEvents(
                        /**
                         * @event beforedrawingcanvasdisplay Fires before displaying a DrawingCanvas.
                         * @param {Viz.grid.GridPanel} this.
                         * @param {Object} record The record of the DrawingCanvas that is being displayed.
                         * @param {Object} entity The entity of the DrawingCanvas that is being displayed. It could be changed by the handler.
                         */
                        'beforedrawingcanvasdisplay');

                Viz.util.MessageBusMgr.subscribe("DrawingCanvasChange", this.reloadStore, this);

                this.on({
                            render          : Viz.util.Portal.gridRender,
                            filteravailable : Viz.util.Portal.gridFilterAvailable,
                            filtercleared   : Viz.util.Portal.toggleFavoriteButton.createDelegate(this, [this._favoritebutton_id, false]),
                            scope           : this
                        });
            },
            /**
             * Destroy
             */
            onDestroy              : function() {
                Viz.util.MessageBusMgr.unsubscribe("DrawingCanvasChange", this.reloadStore, this);
                Viz.grid.DrawingCanvas.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * After deleting we make sure the portlet is deleted as well.
             * @param {} record
             * @param {} selections
             */
            onAfterDeleteRecord    : function(record, selections) {
                for (var i = 0, r; r = selections[i]; i++) {
                    Viz.util.MessageBusMgr.fireEvent('PortletDelete', r.json);
                }
            },

            /**
             * Returns the form Config
             * @return {}
             */
            getFormConfig          : function() {
                return {
                    common      : {
                        width            : 500,
                        height           : 400,
                        xtype            : 'vizFormDrawingCanvas',
                        messageSaveStart : 'DrawingCanvasChangeStart',
                        messageSaveEnd   : 'DrawingCanvasChange',
                        readonly         : !$authorized('@@@@ Drawing Canvas - Full Control')
                    },
                    create      : {
                        title   : $lang("msg_drawingcanvas_add"),
                        iconCls : 'viz-icon-small-drawingcanvas-add'
                    },
                    update      : {
                        title               : $lang("msg_drawingcanvas_update"),
                        iconCls             : 'viz-icon-small-drawingcanvas-update',
                        titleEntity         : 'Title',
                        serviceParamsEntity : [{
                                    name  : 'Key',
                                    value : 'KeyDrawingCanvas'
                                }]
                    },
                    updatebatch : {
                        title                     : $lang("msg_updatebatch"),
                        iconCls                   : 'viz-icon-small-drawingcanvas-update',
                        serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.DrawingCanvas_FormUpdateBatch
                    }
                };
            },
            /**
             * Handler for displaying a drawingcanvas.
             * @method onChartDisplay
             */
            onDrawingCanvasDisplay : function() {
                var selections = this.getSelectionModel().getSelections();
                if (selections.length == 0)
                    return;
                var record = this.getSelectionModel().getSelected();
                if (record) {
                    var entity = record.json;
                    if (this.fireEvent("beforedrawingcanvasdisplay", this, record, entity)) {
                        Viz.grid.DrawingCanvas.openDrawingCanvasDisplay(entity);
                    }
                }
            }
        });
Ext.reg("vizGridDrawingCanvas", Viz.grid.DrawingCanvas);

// Static Methods
Ext.apply(Viz.grid.DrawingCanvas, {
            /**
             * Displays a DrawingCanvas.
             * @method openDrawingCanvasDisplay
             * @static
             * @param {Viz.BusinessEntity.DrawingCanvas} entity : the DrawingCanvas entity.
             * @param {int} width : width of the window.
             * @param {int} width : height of the window.
             */
            openDrawingCanvasDisplay : function(entity, width, height) {
                var win = new Viz.portal.PortletWindow({
                            entity        : entity,
                            xtypePortlet  : 'vizPortletDrawingCanvas',
                            entityPortlet : {
                                TypeEntity : 'Vizelia.FOL.BusinessEntities.DrawingCanvas'
                            },
                            updateHandler : Viz.grid.DrawingCanvas.update
                        });
                win.show();
            },

            /**
             * Create a new drawingcanvas (to be called from desktop or tree)
             * @param {} callback
             */
            create                   : function() {
                Viz.openFormCrud(Viz.grid.DrawingCanvas.prototype.getFormConfig(), 'create', {});
            },

            /**
             * Update an existing drawingcanvas (to be called from desktop or tree)
             * @param {} callback
             */
            update                   : function(entity) {
                if (entity) {
                    Viz.openFormCrud(Viz.grid.DrawingCanvas.prototype.getFormConfig(), 'update', entity);
                }
            }
        });
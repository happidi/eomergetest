﻿/**
 * @class Viz.grid.Priority
 * <p>
 * A grid exposing the business entity Priority
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.Priority = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_priority_add"),
                                iconCls : 'viz-icon-small-priority-add',
                                hidden  : !$authorized('@@@@ Manage Priorities - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_priority_update"),
                                iconCls : 'viz-icon-small-priority-update',
                                hidden  : !$authorized('@@@@ Manage Priorities - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_priority_delete"),
                                iconCls : 'viz-icon-small-priority-delete',
                                hidden  : !$authorized('@@@@ Manage Priorities - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 550,
                            height           : 200,
                            xtype            : 'vizFormPriority',
                            messageSaveStart : 'PriorityChangeStart',
                            messageSaveEnd   : 'PriorityChange',
                            readonly         : !$authorized('@@@@ Manage Priorities - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_priority_add"),
                            iconCls : 'viz-icon-small-priority-add'
                        },
                        update      : {
                            title               : $lang("msg_priority_update"),
                            iconCls             : 'viz-icon-small-priority-update',
                            titleEntity         : 'Label',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyPriority'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-priority-update',
                            serviceHandlerUpdateBatch : Viz.Services.ServiceDeskWCF.Priority_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.Priority(),
                    columns      : Viz.Configuration.Columns.Priority
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.Priority.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("PriorityChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("PriorityChange", this.reloadStore, this);
                Viz.grid.Priority.superclass.onDestroy.apply(this, arguments);
            }
        });
Ext.reg("vizGridPriority", Viz.grid.Priority);
﻿/**
 * @class Viz.grid.DynamicDisplayColorTemplate
 * <p>
 * A grid exposing the business entity DynamicDisplayColorTemplate
 * </p>
 * @extends Viz.grid.GridPanel
 */
Viz.grid.DynamicDisplayColorTemplate = Ext.extend(Viz.grid.GridPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    hasRowEditor : false,
                    stripeRows   : false,
                    hasToolbar   : false,
                    tbar         : [{
                                text    : $lang("msg_dynamicdisplaycolortemplate_add"),
                                iconCls : 'viz-icon-small-add',
                                hidden  : !$authorized('@@@@ Dynamic Display color template - Full Control'),
                                scope   : this,
                                handler : this.onAddRecord
                            }, {
                                text    : $lang("msg_dynamicdisplaycolortemplate_update"),
                                iconCls : 'viz-icon-small-update',
                                hidden  : !$authorized('@@@@ Dynamic Display color template - Full Control'),
                                scope   : this,
                                handler : this.onUpdateRecord
                            }, {
                                text    : $lang("msg_dynamicdisplaycolortemplate_delete"),
                                iconCls : 'viz-icon-small-delete',
                                hidden  : !$authorized('@@@@ Dynamic Display color template - Full Control'),
                                scope   : this,
                                handler : this.onDeleteRecord
                            }],
                    formConfig   : {
                        common      : {
                            width            : 500,
                            height           : 220,
                            xtype            : 'vizFormDynamicDisplayColorTemplate',
                            messageSaveStart : 'DynamicDisplayColorTemplateChangeStart',
                            messageSaveEnd   : 'DynamicDisplayColorTemplateChange',
                            readonly         : !$authorized('@@@@ Dynamic Display color template - Full Control')
                        },
                        create      : {
                            title   : $lang("msg_dynamicdisplaycolortemplate_add"),
                            iconCls : 'viz-icon-small-add'
                        },
                        update      : {
                            title               : $lang("msg_dynamicdisplaycolortemplate_update"),
                            iconCls             : 'viz-icon-small-update',
                            titleEntity         : 'Label',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyDynamicDisplayColorTemplate'
                                    }]
                        },
                        updatebatch : {
                            title                     : $lang("msg_updatebatch"),
                            iconCls                   : 'viz-icon-small-update',
                            serviceHandlerUpdateBatch : Viz.Services.EnergyWCF.DynamicDisplayColorTemplate_FormUpdateBatch
                        }
                    },
                    store        : new Viz.store.DynamicDisplayColorTemplate(),
                    columns      : Viz.Configuration.Columns.DynamicDisplayColorTemplate
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.grid.DynamicDisplayColorTemplate.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("DynamicDisplayColorTemplateChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy   : function() {
                Viz.util.MessageBusMgr.unsubscribe("DynamicDisplayColorTemplateChange", this.reloadStore, this);
                Viz.grid.DynamicDisplayColorTemplate.superclass.onDestroy.apply(this, arguments);
            }
        });
Ext.reg("vizGridDynamicDisplayColorTemplate", Viz.grid.DynamicDisplayColorTemplate);

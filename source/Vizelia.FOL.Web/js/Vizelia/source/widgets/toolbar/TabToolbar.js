﻿/**
 * A custom toolbar that acts like the Microsoft Office 2007 ribbon menu. Usage :
 * 
 * <pre><code>
 * var window = new Ext.Window({
 *             width  : 400,
 *             height : 200,
 *             tbar   : new Viz.TabToolbar([{
 *                         title : 'Module 1',
 *                         items : [{
 *                                     text : 'Button 1'
 *                                 }, {
 *                                     text : 'Button 2'
 *                                 }]
 *                     }, {
 *                         title : 'Module 2',
 *                         items : [{
 *                                     text : 'Button 2'
 *                                 }]
 *                     }])
 *         });
 * </code></pre>
 * 
 * @class Viz.TabToolbar
 * @extends Ext.TabPanel
 */
Viz.TabToolbar = Ext.extend(Ext.TabPanel, {
            activeTab         : 0,
            frame             : true,
            plain             : false,
            border            : false,
            layoutOnTabChange : true,
            // autoHeight : true,
            defaultType       : 'panel',
            // bodyStyle : 'border-bottom:none',
            style             : 'border:none',
            bodyStyle         : 'background-color:transparent;',
            /**
             * Constructor.
             * @param {Array} config The array of each tab toolbar configuration
             */
            constructor       : function(config) {
                config = [].concat(config); // forces config as an array
                Ext.each(config, function(c) {
                            c.defaults == c.defaults || {};
                            Ext.apply(c.defaults, {
                                        margins : '2'
                                    });
                            Ext.apply(c, {
                                        frame        : true,
                                        xtype        : 'container', // 'panel'
                                        layout       : 'hbox',
                                        layoutConfig : {
                                            align : 'stretchmax'
                                        },
                                        // cls : 'x-toolbar',
                                        defaultType  : 'button'
                                    });
                        });
                this.cls = "x-toolbar";
                this.items = config;
                Viz.TabToolbar.superclass.constructor.call(this);

                // corrects a bug in extjs that does not accept config option hidden on tabs.
                this.on("afterrender", function(tab) {
                            Ext.each(config, function(configItem, index) {
                                        if (configItem.hidden == true) {
                                            this.items.items[index].hide();
                                            this.hideTabStripItem(index);
                                        }
                                    }, this);
                        }, this);

            }
        });

Ext.reg("vizTabToolbar", Viz.TabToolbar);
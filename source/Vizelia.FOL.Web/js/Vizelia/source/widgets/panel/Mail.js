﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Mail
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.Mail = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_application_mail'),
                    iconCls : 'viz-icon-small-mail',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridMail'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Mail.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelMail', Viz.panel.Mail);
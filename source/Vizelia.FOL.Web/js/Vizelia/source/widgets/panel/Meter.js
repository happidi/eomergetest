﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Meter
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
// Viz.panel.Meter = Ext.extend(Viz.panel.Panel, {
//
// /**
// * Ctor.
// * @param {Object} config The configuration options
// */
// constructor : function(config) {
// config = config || {};
// var defaultConfig = {
// title : $lang('msg_meters'),
// iconCls : 'viz-icon-small-meter',
// items : [{
// border : Viz.DisplayAsDesktop ? false : true,
// xtype : 'vizGridMeter'
// }]
// };
//
// var forcedConfig = {};
// Ext.applyIf(config, defaultConfig);
// Ext.apply(config, forcedConfig);
//
// Viz.panel.Meter.superclass.constructor.call(this, config);
// }
// });
// Ext.reg('vizPanelMeter', Viz.panel.Meter);
Viz.panel.Meter = Ext.extend(Viz.panel.Panel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor      : function(config) {
                config = config || {};
                var defaultConfig = {};
                this._GridMeterData = Ext.id();
                var forcedConfig = {
                    title     : $lang('msg_meter'),
                    iconCls   : 'viz-icon-small-meter',
                    closable  : true,
                    bodyStyle : 'padding:1px',
                    layout    : 'fit',
                    items     : [{
                                xtype     : 'panel',
                                layout    : 'border',
                                bodyStyle : 'background-color:white;',
                                border    : false,
                                frame     : false,
                                items     : [{
                                            region    : 'center',
                                            split     : 'true',
                                            frame     : false,
                                            border    : false,
                                            layout    : 'fit',
                                            minHeight : 300,
                                            margins   : '1 1 0 1',
                                            items     : [{
                                                        title     : $lang('msg_meter'),
                                                        header    : false,
                                                        iconCls   : 'viz-icon-small-meter',
                                                        border    : true,
                                                        frame     : false,
                                                        xtype     : 'vizGridMeter',
                                                        listeners : {
                                                            render : function(comp) {
                                                                comp.getSelectionModel().on("rowselect", this.onMeterRowSelect, this);
                                                            },
                                                            scope  : this
                                                        }
                                                    }]
                                        }, {
                                            margins     : '0 1 1 1',
                                            split       : true,
                                            frame       : false,
                                            border      : false,
                                            height      : 250,
                                            region      : 'south',
                                            header      : false,
                                            collapsible : false,
                                            layout      : 'fit',
                                            items       : [{
                                                        id     : this._GridMeterData,
                                                        header : false,
                                                        // title : $lang('msg_meterdata'),
                                                        // iconCls : 'viz-icon-small-meterdata',
                                                        border : true,
                                                        frame  : false,
                                                        xtype  : 'vizGridMeterData'
                                                    }]
                                        }]
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.panel.MeterData.superclass.constructor.call(this, config);
            },
            /**
             * Handler for row click on the action request grid.
             * @param {Ext.grid.RowSelectionModel} selectModel
             * @param {Integer} rowIndex
             * @param {Ext.data.record} record
             */
            onMeterRowSelect : function(selectModel, rowIndex, record) {
                var gridMeterData = Ext.getCmp(this._GridMeterData);
                var meter = record.json;
                gridMeterData.setComboValue(meter.KeyMeter);
                gridMeterData.selectMeter(meter);
            }
        });
Ext.reg('vizPanelMeter', Viz.panel.Meter);
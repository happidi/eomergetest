﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.TraceSummary
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.TraceSummary = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_application_trace_summary'),
                    iconCls : 'viz-icon-small-tracesummary',
                    items   : [{
                                border  : Viz.DisplayAsDesktop ? false : true,
                                xtype   : 'vizGridTraceSummary',
                                stateId : 'MainViewPort.vizGridTraceSummary'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.TraceSummary.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelTraceSummary', Viz.panel.TraceSummary);
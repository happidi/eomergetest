﻿Ext.namespace('Viz.panel');

/**
* @class Viz.panel.EWSDataAcquisitionContainer
* @extends Ext.Panel
* <p>
* Summary
* </p>
*/
Viz.panel.EWSDataAcquisitionContainer = Ext.extend(Viz.panel.Panel, {

    /**
    * Ctor.
    * @param {Object} config The configuration options
    */
    constructor: function (config) {
        config = config || {};
        var defaultConfig = {};
        var forcedConfig = {
            title: $lang('msg_ewsdataacquisitioncontainer'),
            iconCls: 'viz-icon-small-ewsdataacquisitioncontainer',
            items: {
                border: Viz.DisplayAsDesktop ? false : true,
                xtype: 'vizGridEWSDataAcquisitionContainer'
            }
        };

        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.panel.EWSDataAcquisitionContainer.superclass.constructor.call(this, config);
    }
});
Ext.reg('vizPanelEWSDataAcquisitionContainer', Viz.panel.EWSDataAcquisitionContainer);

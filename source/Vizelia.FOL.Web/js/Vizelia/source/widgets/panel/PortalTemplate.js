﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.PortalTemplate
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.PortalTemplate = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    title   : $lang('msg_portaltemplate'),
                    iconCls : 'viz-icon-small-portaltemplate',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridPortalTemplate'
                            }]
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.PortalTemplate.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelPortalTemplate', Viz.panel.PortalTemplate);

﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.ReportViewer
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.ReportViewer = Ext.extend(Viz.panel.Panel, {

            /**
             * @cfg {string} path The report relative path.
             */
            path                         : '',
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                  : function(config) {
                config = config || {};
                var defaultConfig = {};

                this.bodyElementId = Ext.getCmp('viewport').el.id;
                var reportTitle = $lang('msg_reportviewer');

                if (config.path) {
                    var lastIndex = config.path.lastIndexOf('/');
                    if (lastIndex != -1) {
                        reportTitle = reportTitle + ': ' + config.path.substring(lastIndex + 1);
                    }
                }

                var forcedConfig = {
                    title     : reportTitle,
                    iconCls   : 'viz-icon-small-report',
                    layout    : 'fit',
                    bodyStyle : 'padding:0 0 0 0',
                    items     : [{
                                bodyStyle : 'padding:0 0 0 0',
                                // this is just in order for the loading mask to be positioned nicely
                                html      : '<div style="height:200px">&nbsp;</div>'
                            }],
                    listeners : {
                        afterrender : {
                            scope : this,
                            fn    : function() {
                                this.doLayout();
                                this.getEl().mask();
                                Viz.Services.ReportingWCF.GetReportParameters({
                                            key     : this.path,
                                            scope   : this,
                                            success : function(reportParameters) {
                                                if (Ext.isEmpty(reportParameters)) {
                                                    // No parameters - just draw the report in an IFRAME.
                                                    var iframePanel = new Ext.Panel({
                                                                bodyStyle : 'padding:0 0 0 0',
                                                                tbar      : [{
                                                                            text    : $lang("msg_report_subscribe"),
                                                                            iconCls : 'viz-icon-small-add',
                                                                            // hidden : !$authorized('ReportSubscription_write'),
                                                                            scope   : this,
                                                                            handler : this.onSubscribeWithoutParameters
                                                                        }, {
                                                                            text    : $lang("msg_report_manage_my_subscriptions"),
                                                                            iconCls : 'viz-icon-small-settings',
                                                                            handler : this.onManageReportSubscriptions,
                                                                            scope   : this
                                                                            // hidden : !$authorized('ReportSubscription_write'),
                                                                    }],
                                                                html      : '<iframe src="' + Viz.Const.Url.ReportViewer + '?reportpath=' + this.path + '" style="width:100%; height:100%" frameborder="0" />'
                                                            });
                                                    this.items.clear();
                                                    this.items.add(iframePanel);
                                                    this.doLayout();
                                                }
                                                else {
                                                    // Create a top panel for parameters and an IFRAME for the rendered report.
                                                    var columns = [[], []];

                                                    var extendedProperties = {};

                                                    for (var i = 0; i < reportParameters.length; i++) {
                                                        if (reportParameters[i] != null && reportParameters[i].Name == 'ExtendedProperties' && reportParameters[i].PromptUser === false && reportParameters[i].DefaultValues != null && reportParameters[i].DefaultValues.length > 0) {
                                                            Ext.apply(extendedProperties, Ext.decode(reportParameters[i].DefaultValues[0]));
                                                            break;
                                                        }
                                                    }

                                                    for (var i = 0; i < reportParameters.length; i++) {
                                                        if (reportParameters[i] != null && reportParameters[i].PromptUser === true) {
                                                            var fieldForParam = this.createField(reportParameters[i], extendedProperties);
                                                            columns[i % 2].push(Ext.apply({}, fieldForParam));
                                                        }
                                                    }

                                                    var defaults = this.extractDefaults(reportParameters);

                                                    var column1 = {
                                                        defaults : {
                                                            anchor : '100%'
                                                        },
                                                        items    : columns[0]
                                                    };

                                                    var column2 = {
                                                        defaults : {
                                                            anchor : '100%'
                                                        },
                                                        items    : columns[1]
                                                    };

                                                    if (!extendedProperties.width) {
                                                        // No width specified in the extended properties, make it two autofit columns.
                                                        Ext.apply(column1, {
                                                                    columnWidth : 0.5
                                                                });
                                                        Ext.apply(column2, {
                                                                    columnWidth : 0.5
                                                                });
                                                    }

                                                    var formColumns = [column1, column2];

                                                    this.formId = Ext.id();
                                                    this.iframeId = Ext.id();
                                                    var newPanel = new Ext.Panel({
                                                                layout : {
                                                                    type      : 'border',
                                                                    bodyStyle : 'padding:0 0 0 0'
                                                                },
                                                                items  : [{
                                                                            region      : 'north',
                                                                            split       : true,
                                                                            title       : $lang('msg_report_parameters'),
                                                                            collapsible : true,
                                                                            layout      : 'fit',
                                                                            bodyStyle   : 'padding:0 0 0 0',
                                                                            height      : ((reportParameters.length / 2) * 25) + 65,
                                                                            items       : [{
                                                                                        xtype      : 'form',
                                                                                        id         : this.formId,
                                                                                        frame      : true,
                                                                                        labelWidth : 170,
                                                                                        bodyStyle  : 'padding:0 0 0 0',
                                                                                        autoScroll : true,
                                                                                        listeners  : {
                                                                                            afterrender : {
                                                                                                scope : this,
                                                                                                fn    : function() {
                                                                                                    if (!Ext.isEmpty(defaults)) {
                                                                                                        var paramsForm = Ext.getCmp(this.formId).getForm();
                                                                                                        paramsForm.setValues.defer(100, paramsForm, [defaults]);
                                                                                                    }

                                                                                                }
                                                                                            },
                                                                                            resize      : {
                                                                                                scope : this,
                                                                                                fn    : function() {
                                                                                                    var paramsForm = Ext.getCmp(this.formId).getForm();
                                                                                                    paramsForm.items.each(function(item) {
                                                                                                                // Close possible open pickers since controls positions may change.
                                                                                                                if (item.collapse) {
                                                                                                                    item.collapse();
                                                                                                                }
                                                                                                            });
                                                                                                }
                                                                                            }
                                                                                        },
                                                                                        items      : [{
                                                                                                    layout   : 'column',
                                                                                                    defaults : {
                                                                                                        layout    : 'form',
                                                                                                        border    : false,
                                                                                                        xtype     : 'panel',
                                                                                                        bodyStyle : 'padding:0 40px 0 0'
                                                                                                    },
                                                                                                    items    : formColumns.concat([{
                                                                                                                defaults : {
                                                                                                                    anchor : '100%'
                                                                                                                },
                                                                                                                items    : [{
                                                                                                                            xtype   : 'splitbutton',
                                                                                                                            text    : $lang('msg_report_view') + '&nbsp;',
                                                                                                                            iconCls : 'viz-icon-small-display',
                                                                                                                            menu    : new Ext.menu.Menu({
                                                                                                                                        items : [{
                                                                                                                                                    text    : $lang('msg_report_view_and_subscribe'),
                                                                                                                                                    handler : this.onSubscribeWithParameters,
                                                                                                                                                    iconCls : 'viz-icon-small-add',
                                                                                                                                                    // hidden : !$authorized('ReportSubscription_write'),
                                                                                                                                                    scope   : this
                                                                                                                                                }, {
                                                                                                                                                    text    : $lang("msg_report_manage_my_subscriptions"),
                                                                                                                                                    iconCls : 'viz-icon-small-settings',
                                                                                                                                                    handler : this.onManageReportSubscriptions,
                                                                                                                                                    scope   : this
                                                                                                                                                }]
                                                                                                                                    }),
                                                                                                                            scope   : this,
                                                                                                                            handler : this.onViewReport
                                                                                                                        }]
                                                                                                            }])
                                                                                                }]
                                                                                    }]
                                                                        }, {
                                                                            region    : 'center',
                                                                            layout    : 'fit',
                                                                            bodyStyle : 'padding:0 0 0 0',
                                                                            html      : '<iframe id="' + this.iframeId + '" name="' + this.iframeId + '" style="width:100%; height:100%" frameborder="0" />'
                                                                        }]
                                                            })

                                                    this.items.clear();
                                                    this.items.add(newPanel);
                                                    this.doLayout();
                                                }

                                                this.getEl().unmask();
                                            }
                                        });

                            }
                        }
                    }
                };

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.ReportViewer.superclass.constructor.call(this, config);
            },

            /**
             * Returns a field created from the given Reporting Services parameter.
             * @param {Object} parameter The parameter
             */
            createField                  : function(parameter, extendedProperties) {
                var field;

                if (parameter.IsSelection === true) {
                    var fieldConfig = Ext.apply({
                                fieldLabel    : parameter.Prompt,
                                name          : parameter.Name,
                                allowBlank    : parameter.AllowBlank,
                                key           : this.path,
                                parameterName : parameter.Name
                            }, extendedProperties);
                    field = new Viz.combo.ReportParameterValue(fieldConfig);
                }
                else if (parameter.Prompt.indexOf('|') != -1) {
                    // this means the parameter was specified with an xtype for rendering its input box.
                    field = {
                        xtype       : parameter.Prompt.substring(parameter.Prompt.indexOf('|') + 1),
                        fieldLabel  : parameter.Prompt.substring(0, parameter.Prompt.indexOf('|')),
                        name        : parameter.Name,
                        submitValue : true,
                        allowBlank  : parameter.AllowBlank,
                        listeners   : {
                            autoSize : {
                                fn    : function() {
                                    // To prevent horizontal scrollbar and show only the vertical one.
                                    this.doLayout();
                                },
                                scope : this
                            }
                        }
                    };

                    Ext.apply(field, extendedProperties);
                }
                else if (parameter.ParameterTypeName === 'String') {
                    var fieldConfig = Ext.apply({
                                fieldLabel : parameter.Prompt,
                                name       : parameter.Name,
                                allowBlank : parameter.AllowBlank
                            }, extendedProperties);

                    field = new Ext.form.TextField(fieldConfig);
                }
                else if (parameter.ParameterTypeName === 'DateTime') {
                    var fieldConfig = Ext.apply({
                                fieldLabel : parameter.Prompt,
                                name       : parameter.Name,
                                allowBlank : parameter.AllowBlank
                            }, extendedProperties);

                    field = new Ext.form.DateField(fieldConfig);
                }
                else if (parameter.ParameterTypeName === 'Boolean') {
                    var fieldConfig = Ext.apply({
                                fieldLabel : parameter.Prompt,
                                name       : parameter.Name
                            }, extendedProperties);

                    field = new Viz.form.Checkbox(fieldConfig);
                }
                else if (parameter.ParameterTypeName === 'Integer') {
                    var fieldConfig = Ext.apply({
                                fieldLabel       : parameter.Prompt,
                                decimalPrecision : 0,
                                name             : parameter.Name,
                                allowBlank       : false
                            }, extendedProperties);

                    field = new Ext.form.NumberField(fieldConfig);
                }
                else if (parameter.ParameterTypeName === 'Float') {
                    var fieldConfig = Ext.apply({
                                fieldLabel : parameter.Prompt,
                                name       : parameter.Name,
                                allowBlank : false
                            }, extendedProperties);

                    field = new Ext.form.NumberField(fieldConfig);
                }

                return field;

            },

            extractDefaults              : function(reportParameters) {
                var defaults = [];

                for (var i = 0; i < reportParameters.length; i++) {
                    var parameter = reportParameters[i];
                    if (parameter.DefaultValues && Ext.isArray(parameter.DefaultValues) && parameter.DefaultValues[0]) {
                        var defaultValue = {
                            id    : parameter.Name,
                            value : parameter.DefaultValues[0]
                        };

                        defaults.push(defaultValue);
                    }
                }

                return defaults;
            },

            renderReport                 : function() {
                var paramsForm = Ext.getCmp(this.formId).getForm();
                // Post the parameters to refresh the report.
                paramsForm.submitAsPost({
                            action : Viz.Const.Url.ReportViewer + '?reportpath=' + this.path,
                            target : this.iframeId
                        });
                this.doLayout();
            },

            onViewReport                 : function() {
                this.renderReport();
            },

            onManageReportSubscriptions  : function() {
                var window = new Ext.Window({
                            layout  : 'fit',
                            title   : $lang('msg_report_manage_my_subscriptions'),
                            iconCls : 'viz-icon-small-settings',
                            width   : 740,
                            height  : 250,
                            items   : {
                                xtype      : 'vizGridReportSubscription',
                                reportPath : this.path
                            }
                        });
                window.show();
            },

            onSubscribeWithoutParameters : function() {
                this.subscribe({});
            },

            onSubscribeWithParameters    : function() {
                var paramsForm = Ext.getCmp(this.formId).getForm();

                if (!paramsForm.isValid()) {
                    return;
                }

                // Refresh the report in the iframe.
                this.renderReport();

                // Populate this variable for subscribing later.
                var item = paramsForm.getFieldValues();

                var parameterValues = [];

                for (var name in item) {
                    parameterValues.push({
                                Key   : name,
                                Value : item[name]
                            });
                }

                this.subscribe(parameterValues);
            },

            subscribe                    : function(parameterValues) {
                Viz.openFormCrud({
                            common : {
                                width            : 500,
                                height           : 250,
                                xtype            : 'vizFormReportSubscription',
                                messageSaveStart : 'ReportSubscriptionChangeStart',
                                messageSaveEnd   : 'ReportSubscriptionChange',
                                title            : $lang("msg_reportsubscription_add"),
                                iconCls          : 'viz-icon-small-report'
                            }
                        }, 'create', {
                            ParameterValues : parameterValues,
                            ReportPath      : this.path
                        });
            }

        });

Ext.reg('vizPanelReportViewer', Viz.panel.ReportViewer);
﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.FlipCard
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.FlipCard = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_flipcard'),
                    iconCls : 'viz-icon-small-flipcard',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridFlipCard'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.FlipCard.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelFlipCard', Viz.panel.FlipCard);
﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.WeatherLocation
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.WeatherLocation = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_weatherlocation'),
                    iconCls : 'viz-icon-small-weatherlocation',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridWeatherLocation'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.WeatherLocation.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelWeatherLocation', Viz.panel.WeatherLocation);
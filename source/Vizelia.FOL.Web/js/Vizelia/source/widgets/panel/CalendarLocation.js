﻿Ext.namespace('Viz.panel.CalendarLocation');

/**
 * @class Viz.panel.CalendarLocation.
 * @extends Ext.ensible.cal.CalendarPanel Summary.
 */
Viz.panel.CalendarLocation = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                   : function(config) {
                var store = new Viz.store.CalendarDefinition({
                            KeyLocation     : config.KeyLocation,
                            Category        : config.Category || '',
                            serviceHandler  : Viz.Services.CoreWCF.CalendarEvent_GetStoreOccurence,
                            deleteAllFields : true
                        });

                this._calendarComponentId = Ext.id();

                config = config || {};
                var defaultConfig = {
                    iconCls : 'viz-icon-small-calendar'
                };

                var forcedConfig = {
                    closable  : true,
                    bodyStyle : Viz.DisplayAsDesktop ? 'padding:0px' : 'padding:1px',
                    layout    : 'fit',
                    items     : [{
                                border      : Viz.DisplayAsDesktop ? false : true,
                                frame       : Viz.DisplayAsDesktop ? false : true,
                                id          : this._calendarComponentId,
                                xtype       : 'extensible.calendarpanel',
                                KeyLocation : config.KeyLocation,
                                eventStore  : store,
                                listeners   : {
                                    /**
                                     * Listeners to load the store when we first diplay the calendar.
                                     */
                                    viewchange : function(cal, view, info) {
                                        cal.getTopToolbar().find('id', this.id + '-view-local')[0].toggle(false);
                                        if (cal.store.hasLoaded === false) {
                                            view.reloadStore();
                                        }
                                    },
                                    /**
                                     * Add the EventCategory combobox filter to the top toolbar.
                                     */
                                    render     : function(cal) {
                                        cal.getTopToolbar().insert(0, {
                                                    xtype     : 'vizComboCalendarEventCategory',
                                                    height    : '40px',
                                                    listeners : {
                                                        select : this.onCalendarEventCategorySelect,
                                                        clear  : this.onCalendarEventCategoryClear,
                                                        scope  : this
                                                    }
                                                });
                                        cal.getTopToolbar().insert(1, {
                                                    id           : this.id + '-view-local',
                                                    iconCls      : 'viz-icon-small-world',
                                                    enableToggle : true,
                                                    text         : $lang('Timezone'),
                                                    handler      : this.viewOnLocalCalendar,
                                                    scope        : this
                                                });

                                        cal.getTopToolbar().insert(2, {
                                                    xtype : 'tbseparator'
                                                });
                                    },

                                    scope      : this
                                }
                            }]
                };

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.CalendarLocation.superclass.constructor.call(this, config);
                Viz.util.MessageBusMgr.subscribe("SpatialCalendarClick", this.reloadCalendarStore, this);

                this.calendarComponent = Ext.getCmp(this._calendarComponentId);

                this.calendarComponent.on("eventdelete", this.onCalendarLocationEventDelete, this);
                this.calendarComponent.on("eventadd", this.onCalendarLocationEventAdd, this);
            },
            /**
             * Destroy
             */
            onDestroy                     : function() {
                Viz.util.MessageBusMgr.unsubscribe("SpatialCalendarClick", this.reloadCalendarStore, this);
                Viz.panel.CalendarLocation.superclass.onDestroy.apply(this, arguments);
            },

            reloadCalendarStore           : function(node) {
                if (this.ownerCt.getActiveTab().id != this.id)
                    return;
                this.calendarComponent.store.serviceParams.KeyLocation = node.attributes.Key;
                this.calendarComponent.activeView.reloadStore();
                this.setTitle($lang('msg_calendar') + ' : ' + node.parentNode.text + ' / ' + node.text);
            },
            /**
             * Handler for deleting an event.
             */
            onCalendarLocationEventDelete : function() {
                this.calendarComponent.store.save();
            },

            /**
             * Handler for adding an event.
             */
            onCalendarLocationEventAdd    : function() {
                this.calendarComponent.store.save();
            },
            /**
             * Handler for the EventCategory Combobox filter select event.
             */
            onCalendarEventCategorySelect : function(combo, record, index) {
                this.calendarComponent.store.serviceParams.category = record.data.LocalId;
                this.calendarComponent.activeView.refresh(true);
            },
            /**
             * Handler for the EventCategory Combobox filter clear event.
             */
            onCalendarEventCategoryClear  : function(combo) {
                this.calendarComponent.store.serviceParams.category = '';
                this.calendarComponent.activeView.reloadStore();
            },
            viewOnLocalCalendar           : function(btn) {
                if (btn.pressed) {
                    this.calendarComponent.store.each(function(r) {
                                r.data.StartDate = Viz.date.convertToLocalDate(r.data.StartDate, r.data['TimeZoneOffset'][0]);
                                r.loalView = btn.pressed;
                                r.data.EndDate = Viz.date.convertToLocalDate(r.data.EndDate, r.data['TimeZoneOffset'][1]);
                            }, this);

                    this.calendarComponent.activeView.refresh();
                }
                else {
                    this.calendarComponent.activeView.refresh(true);
                }
            }
        });

Ext.reg('vizPanelCalendarLocation', Viz.panel.CalendarLocation);

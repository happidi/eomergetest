﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.WebFrame
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.WebFrame = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_webframe'),
                    iconCls : 'viz-icon-small-webframe',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridWebFrame'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.WebFrame.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelWebFrame', Viz.panel.WebFrame);
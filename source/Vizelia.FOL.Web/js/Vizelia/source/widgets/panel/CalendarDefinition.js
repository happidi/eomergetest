﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.CalendarDefinition
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.CalendarDefinition = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    iconCls : 'viz-icon-small-calendarevent',
                    items   : [{
                                border        : Viz.DisplayAsDesktop ? false : true,
                                xtype         : 'vizGridCalendarDefinition',
                                KeyLocation   : config.KeyLocation,
                                Category      : config.Category,
                                CategoryLabel : config.CategoryLabel
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.CalendarDefinition.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelCalendarDefinition', Viz.panel.CalendarDefinition);
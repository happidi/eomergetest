﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.RESTDataAcquisitionContainer
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.RESTDataAcquisitionContainer = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    title   : $lang('msg_restdataacquisitioncontainer'),
                    iconCls : 'viz-icon-small-restdataacquisitioncontainer',
                    items   : {
                        border : Viz.DisplayAsDesktop ? false : true,
                        xtype  : 'vizGridRESTDataAcquisitionContainer'
                    }
                };

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.RESTDataAcquisitionContainer.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelRESTDataAcquisitionContainer', Viz.panel.RESTDataAcquisitionContainer);

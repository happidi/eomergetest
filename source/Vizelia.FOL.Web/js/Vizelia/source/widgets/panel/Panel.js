﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Panel Base class for all Panels in order to have basic config.
 * @extends Ext.Panel
 * @xtype vizPanel
 */
Viz.panel.Panel = Ext.extend(Ext.Panel, {

            /**
             * @private Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    closable  : true,
                    bodyStyle : Viz.DisplayAsDesktop ? 'padding:0px' : 'padding:4px',
                    layout    : 'fit'
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Panel.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanel', Viz.panel.Panel);

﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Palette
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.Palette = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title     : $lang('msg_palette'),
                    iconCls   : 'viz-icon-small-palette',
                    items     : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizDataViewPalette'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Palette.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelPalette', Viz.panel.Palette);
﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.CalendarEventCategory
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.CalendarEventCategory = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};
               
                var forcedConfig = {
                    title     : $lang('msg_calendareventcategory'),
                    iconCls   : 'viz-icon-small-calendareventcategory',
                     items     : [{
                               border      : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridCalendarEventCategory'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.CalendarEventCategory.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelCalendarEventCategory', Viz.panel.CalendarEventCategory);
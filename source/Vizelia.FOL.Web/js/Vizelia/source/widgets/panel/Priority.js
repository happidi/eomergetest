﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Priority
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.Priority = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_priority_manage'),
                    iconCls : 'viz-icon-small-priority',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridPriority'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Priority.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelPriority', Viz.panel.Priority);
﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Job
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.Job = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title     : $lang('msg_application_job'),
                    iconCls   : 'viz-icon-small-job',
                    items     : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridJob'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Job.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelJob', Viz.panel.Job);
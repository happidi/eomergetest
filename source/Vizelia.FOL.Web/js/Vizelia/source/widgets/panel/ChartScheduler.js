﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.ChartScheduler
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.ChartScheduler = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    title   : $lang('msg_chartscheduler'),
                    iconCls : 'viz-icon-small-chartscheduler',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridChartScheduler'
                            }]
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.ChartScheduler.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelChartScheduler', Viz.panel.ChartScheduler);

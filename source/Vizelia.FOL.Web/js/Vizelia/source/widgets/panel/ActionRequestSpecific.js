﻿Ext.namespace('Viz.panel');
/**
 * @class Viz.panel.ActionRequestSpecific
 * @extends Ext.Panel
 * <p>
 * The panel for managing action requests.
 * </p>
 */
Viz.panel.ActionRequestSpecific = Ext.extend(Viz.panel.Panel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {
                    iconCls : 'viz-icon-small-' + Ext.util.Format.lowercase(config.panel || 'ActionRequest')
                };

                this._GridActionRequest = Ext.id();
                this._GridTask = Ext.id();
                this._GridApproval = Ext.id();

                var forcedConfig = {
                    title     : config.title || $lang('msg_actionrequest'),
                    closable  : true,
                    bodyStyle : 'padding:1px',
                    layout    : 'fit',
                    items     : []
                };

                forcedConfig.items = this.getContent(config.panel || 'ActionRequest');
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.panel.ActionRequestSpecific.superclass.constructor.call(this, config);
            },
            getContent              : function(content) {
                return {
                    ActionRequest : [{
                                xtype     : 'panel',
                                layout    : 'border',
                                iconCls   : 'viz-icon-small-actionrequest',
                                bodyStyle : 'background-color:white;',
                                border    : false,
                                frame     : false,
                                items     : [{
                                            region    : 'center',
                                            split     : 'true',
                                            frame     : false,
                                            border    : false,
                                            layout    : 'fit',
                                            minHeight : 300,
                                            margins   : '1 1 0 1',
                                            items     : [{
                                                        title             : $lang('msg_actionrequest'),
                                                        header            : false,
                                                        border            : true,
                                                        frame             : false,
                                                        xtype             : 'vizGridActionRequest',
                                                        listeners         : {
                                                            rowclick : this.onActionRequestRowClick,
                                                            scope    : this
                                                        },
                                                        filterBy          : function(role) {
                                                            switch (role) {
                                                                case 'Requestor' :
                                                                    return ['Requestor', Viz.App.Global.User.ProviderUserKey.KeyOccupant];
                                                                    break;
                                                                case 'Approver' :
                                                                    return ['KeyApprover', Viz.App.Global.User.ProviderUserKey.KeyUser];
                                                                    break;
                                                            };
                                                            return [];
                                                        },
                                                        onFilterAvailable : function() {
                                                            this.store.lastOptions.params.limit = this.pageSize;
                                                            var filters = this.findParentByType('vizPanelActionRequestSpecific').filters;
                                                            var filterName = this.filterBy(filters[0]);
                                                            var approverFilter = this.filters.getFilter(filterName[0]);
                                                            if (approverFilter == null) {
                                                                this.filters.addFilter({
                                                                            dataIndex : filterName[0],
                                                                            type      : 'string'
                                                                        });
                                                                approverFilter = this.filters.getFilter(filterName[0]);
                                                                approverFilter.setValue(filterName[1]);
                                                                approverFilter.setActive(true);
                                                            }

                                                        }
                                                    }]
                                        }, {
                                            margins : '0 1 1 1',
                                            split   : true,
                                            frame   : false,
                                            height  : 200,
                                            region  : 'south',
                                            header  : false,
                                            layout  : 'fit',
                                            items   : [{
                                                        title   : $lang('msg_task'),
                                                        iconCls : 'viz-icon-small-task',
                                                        border  : false,
                                                        id      : this._GridTask,
                                                        xtype   : 'vizGridTask'

                                                    }]
                                        }]
                            }],
                    Task          : [{
                                iconCls   : 'viz-icon-small-technician',
                                xtype     : 'panel',
                                layout    : 'border',
                                bodyStyle : 'background-color:white;',
                                border    : false,
                                frame     : false,
                                items     : [{
                                            region    : 'center',
                                            split     : 'true',
                                            border    : false,
                                            frame     : false,
                                            layout    : 'fit',
                                            minHeight : 200,
                                            margins   : '1 1 0 1',
                                            items     : [{
                                                        iconCls           : 'viz-icon-small-technician',
                                                        title             : $lang('msg_task'),
                                                        header            : false,
                                                        border            : true,
                                                        frame             : false,
                                                        store             : new Viz.store.Task({
                                                                    serviceHandler : Viz.Services.ServiceDeskWCF.Task_GetStore,
                                                                    autoLoad       : true,
                                                                    baseParams     : {
                                                                        filters : [{
                                                                                    data  : {
                                                                                        type       : 'string',
                                                                                        comparison : 'eq',
                                                                                        value      : Viz.App.Global.User.ProviderUserKey.KeyUser
                                                                                    },
                                                                                    field : 'KeyActor'
                                                                                }]
                                                                    }
                                                                }),

                                                        id                : this._GridTask,
                                                        xtype             : 'vizGridTask',
                                                        listeners         : {
                                                            rowclick : this.onTaskRowClick,
                                                            scope    : this
                                                        },
                                                        onFilterAvailable : function() {

                                                        }
                                                    }]
                                        }, {
                                            margins : '0 1 1 1',
                                            split   : true,
                                            frame   : false,
                                            border  : false,
                                            height  : 200,
                                            region  : 'south',
                                            header  : false,
                                            layout  : 'fit',
                                            items   : [{
                                                        id                : this._GridActionRequest,
                                                        iconCls           : 'viz-icon-small-actionrequest',
                                                        title             : $lang('msg_actionrequest'),
                                                        header            : true,
                                                        border            : true,
                                                        frame             : false,
                                                        xtype             : 'vizGridActionRequest',
                                                        onFilterAvailable : function() {

                                                        }

                                                    }]
                                        }]
                            }]
                }[content];
            },

            /**
             * Handler for row click on the action request grid.
             * @param {Ext.grid.GridPanel} grid
             * @param {Integer} rowIndex
             * @param {Ext.EventObject} e
             */
            onActionRequestRowClick : function(grid, rowIndex, e) {
                var record = grid.getSelectionModel().getSelected();
                var storeTask = Ext.getCmp(this._GridTask).store;
                var params = {
                    start : 0,
                    limit : grid.pageSize
                };
                var serviceParams = {
                    KeyActionRequest : record.get("KeyActionRequest")
                };
                storeTask.proxy.serviceParams = serviceParams;
                storeTask.serviceParams = storeTask.proxy.serviceParams;

                storeTask.load({
                            params   : params,
                            callback : function() {
                            }
                        });
            },

            /**
             * Handler for row click on the task grid.
             * @param {Ext.grid.GridPanel} grid
             * @param {Integer} rowIndex
             * @param {Ext.EventObject} e
             */
            onTaskRowClick          : function(grid, rowIndex, e) {
                grid.getSelectionModel().selectRow(rowIndex, false);
                var record = grid.getSelectionModel().getSelected();
                var gridActionRequest = Ext.getCmp(this._GridActionRequest);
                var storeActionRequest = gridActionRequest.store;

                var params = {
                    start : 0,
                    limit : grid.pageSize
                };
                var filterName = 'KeyActionRequest';

                var keyFilter = gridActionRequest.filters.getFilter(filterName);
                if (keyFilter == null) {
                    gridActionRequest.filters.addFilter({
                                dataIndex  : filterName,
                                comparison : 'eq',
                                type       : 'string'
                            });
                }
                keyFilter = gridActionRequest.filters.getFilter(filterName);
                keyFilter.setValue(record.get("KeyActionRequest"));
                keyFilter.setActive(true);
                storeActionRequest.load({
                            params : params
                        });
            }
        });
Ext.reg('vizPanelActionRequestSpecific', Viz.panel.ActionRequestSpecific);

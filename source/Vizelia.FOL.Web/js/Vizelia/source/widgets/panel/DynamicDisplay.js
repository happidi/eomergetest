﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.DynamicDisplay
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.DynamicDisplay = Ext.extend(Viz.panel.Panel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_dynamicdisplay'),
                    iconCls : 'viz-icon-small-dynamicdisplay',
                    items   : {
                        border : Viz.DisplayAsDesktop ? false : true,
                        xtype  : 'vizDataViewDynamicDisplay'
                    }

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.DynamicDisplay.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelDynamicDisplay', Viz.panel.DynamicDisplay);
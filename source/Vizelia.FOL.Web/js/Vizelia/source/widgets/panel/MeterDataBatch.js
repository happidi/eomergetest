﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.MeterDataBatch
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.MeterDataBatch = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_meterdata_batch'),
                    iconCls : 'viz-icon-small-site',
                    items   : {
                        border : Viz.DisplayAsDesktop ? false : true,
                        xtype  : 'vizFormMeterDataBatch'
                    }

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.MeterDataBatch.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelMeterDataBatch', Viz.panel.MeterDataBatch);

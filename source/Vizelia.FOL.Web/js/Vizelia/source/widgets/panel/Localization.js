﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Localization
 * @extends Ext.Panel
 * <p>
 * The panel for managing localizations.
 * </p>
 */
Viz.panel.Localization = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_localization'),
                    iconCls : 'viz-icon-small-localization',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridLocalization'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Localization.superclass.constructor.call(this, config);
            }
        });

Ext.reg('vizPanelLocalization', Viz.panel.Localization);

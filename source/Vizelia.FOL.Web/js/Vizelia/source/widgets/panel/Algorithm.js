﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Algorithm
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.Algorithm = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title: $lang('msg_algorithms'),
                    iconCls : 'viz-icon-small-python',
                    items   : [{
                                border  : Viz.DisplayAsDesktop ? false : true,
                                xtype: 'vizGridAlgorithm',
                                stateId: 'MainViewPort.vizGridAlgorithm'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.TraceEntry.superclass.constructor.call(this, config);
            }
        });
        Ext.reg('vizPanelAlgorithm', Viz.panel.Algorithm);
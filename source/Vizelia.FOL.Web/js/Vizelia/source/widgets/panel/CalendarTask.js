﻿Ext.namespace('Viz.panel.CalendarTask');

/**
 * @class Viz.panel.CalendarTask.
 * @extends Ext.ensible.cal.CalendarPanel Summary.
 */
Viz.panel.CalendarTask = Ext.extend(Viz.panel.Panel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                   : function(config) {

                Ext.apply(Ext.ensible.cal.EventMappings, {
                            EventId    : {
                                name    : 'KeyTask',
                                mapping : 'KeyTask',
                                type    : 'string'
                            },
                            CalendarId : {
                                name    : 'State',
                                mapping : 'State',
                                type    : 'string'
                            },
                            Title      : {
                                name    : 'Name',
                                mapping : 'Name'
                            },
                            StartDate  : {
                                name       : 'ActualStart',
                                mapping    : 'ActualStart',
                                type       : 'date',
                                dateFormat : 'c'
                            },
                            EndDate    : {
                                name       : 'ActualFinish',
                                mapping    : 'ActualFinish',
                                type       : 'date',
                                dateFormat : 'c'
                            },
                            RRule      : {
                                name    : 'RecurRule',
                                mapping : 'recur_rule'
                            },
                            Location   : {
                                name    : 'Location',
                                mapping : 'location'
                            },
                            Notes      : {
                                name    : 'Description',
                                mapping : 'Description'
                            }
                        });

                Ext.ensible.cal.CalendarMappings.CalendarId.name = 'State';
                Ext.ensible.cal.CalendarMappings.Title.name = 'State';
                Ext.ensible.cal.CalendarRecord.reconfigure();

                var store = new Viz.store.Task({
                            serviceHandler  : Viz.Services.ServiceDeskWCF.Task_GetStore,
                            deleteAllFields : true
                        });
                var calStore = new Ext.data.JsonStore({
                            storeId    : 'calStore',
                            idIndex    : 1,
                            root       : 'records',
                            idProperty : 'State',
                            fields     : [{
                                        name : 'id'
                                    }, {
                                        name : 'State'
                                    }, {
                                        name : 'ColorId',
                                        type : 'int'
                                    }, {
                                        name : 'hidden',
                                        type : 'bool'
                                    }]
                        });
                calStore.loadData({
                            records : [{
                                        "id"    : 1,
                                        "State" : "Scheduled",
                                        "ColorId" : 2
                                    }, {
                                        "id"    : 2,
                                        "State" : "Planned",
                                        "ColorId" : 22
                                    }, {
                                        "id"    : 3,
                                        "State" : "Completed",
                                        "ColorId" : 7
                                    }, {
                                        "id"     : 4,
                                        "State"  : "Initial",
                                        "hidden" : true,
                                        "ColorId"  : 26
                                    }]
                        });

                this._calendarComponentId = Ext.id();

                config = config || {};
                var defaultConfig = {
                    iconCls : 'viz-icon-small-calendar'
                };

                var forcedConfig = {
                    closable  : true,
                    bodyStyle : Viz.DisplayAsDesktop ? 'padding:0px' : 'padding:1px',
                    layout    : 'fit',
                    items     : [{
                                border        : Viz.DisplayAsDesktop ? false : true,
                                frame         : Viz.DisplayAsDesktop ? false : true,
                                id            : this._calendarComponentId,
                                xtype         : 'extensible.calendarpanel',
                                eventStore    : store,
                                calendarStore : calStore,
                                listeners     : {
                                    scope       : this,
                                    editdetails : function(cal, vw, rec, el) {
                                        var eventFormConfig = {
                                            'update' : {
                                                title               : $lang("msg_task_update"),
                                                iconCls             : 'viz-icon-small-update',
                                                titleEntity         : 'Name',
                                                serviceParamsEntity : [{
                                                            name  : 'Key',
                                                            value : 'KeyTask'
                                                        }],
                                                width               : 580,
                                                height              : 450,
                                                xtype               : 'vizFormTask',
                                                messageSaveStart    : 'TaskChangeStart',
                                                messageSaveEnd      : 'TaskChange'
                                            }
                                        };
                                        Viz.openFormCrud.apply(this, [eventFormConfig, 'update', rec.data]);
                                        return false;
                                    },
                                    /**
                                     * Listeners to load the store when we first diplay the calendar.
                                     */
                                    viewchange  : function(cal, view, info) {
                                        view.reloadStore = function(o) {
                                            o = Ext.isObject(o) ? o : {};
                                            o.params = o.params || {};
                                            var av = this;
                                            var dates = av.getStoreParams();
                                            dates.start = dates.start.format(av.dateParamFormat);
                                            dates.end = dates.end.format(av.dateParamFormat);
                                            var filters = [];
                                            filters.push(this.createFilter(Ext.ensible.cal.EventMappings.StartDate.name, dates.start, 'gt'));
                                            filters.push(this.createFilter(Ext.ensible.cal.EventMappings.StartDate.name, dates.end, 'lt'));
                                            filters.push(this.createFilter(Ext.ensible.cal.EventMappings.EndDate.name, dates.start, 'gt'));
                                            filters.push(this.createFilter(Ext.ensible.cal.EventMappings.EndDate.name, dates.end, 'lt'));
                                            var keyClassificationItem = this.ownerCalendarPanel.getTopToolbar().find('name', 'KeyClassificationItem')[0].value;
                                            if (keyClassificationItem != null)
                                                filters.push(this.createFilter('KeyClassificationItem', keyClassificationItem, 'eq', 'string'));

                                            Ext.apply(this.store, {
                                                        baseParams : {
                                                            filters : filters
                                                        }
                                                    });
                                            this.store.load(o);
                                        };
                                        view.createFilter = function(field, value, operator, type) {
                                            return {
                                                data  : {
                                                    type       : type != undefined ? type : 'date',
                                                    comparison : operator,
                                                    value      : value
                                                },
                                                field : field
                                            };
                                        };

                                        if (cal.store.hasLoaded === false) {
                                            view.reloadStore();
                                        }
                                    },
                                    /**
                                     * Add the EventCategory combobox filter to the top toolbar.
                                     */
                                    render      : function(cal) {
                                        cal.getTopToolbar().insert(0, {
                                                    xtype                : 'viztreecomboClassificationItemWork',
                                                    name                 : 'KeyClassificationItem',
                                                    originalDisplayValue : 'ClassificationLongPath',
                                                    root                 : {
                                                        id      : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("task"),
                                                        iconCls : 'viz-icon-small-task',
                                                        text    : ''
                                                    },
                                                    height               : '40px',
                                                    listeners            : {
                                                        select : this.onCalendarEventCategorySelect,
                                                        clear  : this.onCalendarEventCategoryClear,
                                                        scope  : this
                                                    }
                                                });
                                    },

                                    scope       : this
                                }
                            }]
                };

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.CalendarTask.superclass.constructor.call(this, config);

                this.calendarComponent = Ext.getCmp(this._calendarComponentId);

                this.calendarComponent.on("eventdelete", this.onCalendarTaskEventDelete, this);
                this.calendarComponent.on("eventadd", this.onCalendarTaskEventAdd, this);
            },

            /**
             * Handler for deleting an event.
             */
            onCalendarTaskEventDelete     : function() {
                this.calendarComponent.store.save();
            },

            /**
             * Handler for adding an event.
             */
            onCalendarTaskEventAdd        : function() {
                this.calendarComponent.store.save();
            },
            /**
             * Handler for the EventCategory Combobox filter select event.
             */
            onCalendarEventCategorySelect : function(field, node) {
                this.calendarComponent.store.serviceParams.category = node.attributes.Key;
                this.calendarComponent.activeView.refresh(true);
            },
            /**
             * Handler for the EventCategory Combobox filter clear event.
             */
            onCalendarEventCategoryClear  : function(combo) {
                this.calendarComponent.store.serviceParams.category = '';
                this.calendarComponent.activeView.reloadStore();
            }
        });

Ext.reg('vizPanelCalendarTask', Viz.panel.CalendarTask);

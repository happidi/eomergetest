﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.MeterValidationRule
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.MeterValidationRule = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                       : function(config) {
                config = config || {};
                var defaultConfig = {};
                this._GridAlarmInstance = Ext.id();
                this._GridMeterData = Ext.id();

                var forcedConfig = {
                    title     : $lang('msg_metervalidationrule'),
                    iconCls   : 'viz-icon-small-metervalidationrule',
                    closable  : true,
                    bodyStyle : 'padding:4px',
                    layout    : 'fit',
                    items     : [{
                                xtype     : 'panel',
                                layout    : 'border',
                                bodyStyle : 'background-color:white;',
                                border    : false,
                                frame     : false,
                                items     : [{
                                            region    : 'center',
                                            stateId   : 'MeterValidationRuleRegionCenter',
                                            split     : 'true',
                                            frame     : false,
                                            border    : false,
                                            layout    : 'fit',
                                            minHeight : 100,
                                            items     : [{
                                                        title     : $lang('msg_metervalidationrule'),
                                                        header    : false,
                                                        iconCls   : 'viz-icon-small-metervalidationrule',
                                                        border    : true,
                                                        frame     : false,
                                                        xtype     : 'vizGridMeterValidationRule',
                                                        listeners : {
                                                            // to allow reload of task from click or right click we have to switch to selectionModel
                                                            render : function(comp) {
                                                                comp.getSelectionModel().on("rowselect", this.onMeterValidationRuleRowSelect, this);
                                                            },
                                                            scope  : this
                                                        }
                                                    }]
                                        }, {
                                            split     : true,
                                            frame     : false,
                                            border    : false,
                                            height    : 500,
                                            region    : 'south',
                                            stateId   : 'MeterValidationRuleRegionSouth',
                                            header    : false,
                                            layout    : 'border',
                                            bodyStyle : 'background-color:white;',
                                            items     : [{
                                                        region  : 'center',
                                                        stateId : 'MeterValidationRuleRegionSouthCenter',
                                                        split   : 'true',
                                                        frame   : false,
                                                        border  : false,
                                                        layout  : 'fit',
                                                        items   : [{
                                                                    border              : true,
                                                                    // title : $lang('msg_alarminstance'),
                                                                    id                  : this._GridAlarmInstance,
                                                                    iconCls             : 'viz-icon-small-alarm',
                                                                    xtype               : 'vizGridAlarmInstance',
                                                                    hideChartViewButton : true,
                                                                    store               : new Viz.store.MeterValidationRuleAlarmInstance({
                                                                                KeyMeterValidation : ' '
                                                                            }),
                                                                    columns             : Viz.Configuration.Columns.MeterValidationRuleAlarmInstance,
                                                                    listeners           : {
                                                                        // to allow reload of task from click or right click we have to switch to selectionModel
                                                                        render : function(comp) {
                                                                            comp.getSelectionModel().on("rowselect", this.onAlarmInstanceRowSelect, this);
                                                                        },
                                                                        scope  : this
                                                                    }
                                                                }]
                                                    }, {
                                                        region    : 'east',
                                                        stateId   : 'MeterValidationRuleRegionSouthEast',
                                                        split     : 'true',
                                                        frame     : false,
                                                        border    : false,
                                                        width     : 500,
                                                        collapsed : false,
                                                        layout    : 'fit',
                                                        items     : [{
                                                                    iconCls : 'viz-icon-small-meterdata',
                                                                    border  : true,
                                                                    // title : $lang('msg_meterdata'),
                                                                    id      : this._GridMeterData,
                                                                    iconCls : 'viz-icon-small-meterdata',
                                                                    xtype   : 'vizGridMeterData'
                                                                }]
                                                    }]

                                        }]
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.MeterValidationRule.superclass.constructor.call(this, config);
            },

            /**
             * Handler for row click on the metervalidationrule grid.
             * @param {Ext.grid.RowSelectionModel} selectModel
             * @param {Integer} rowIndex
             * @param {Ext.data.record} record
             */
            onMeterValidationRuleRowSelect    : function(selectModel, rowIndex, record) {
                var grid = selectModel.grid;
                var storeAlarmInstance = Ext.getCmp(this._GridAlarmInstance).store;
                var params = {
                    start : 0,
                    limit : grid.pageSize
                };

                var serviceParams = {
                    KeyMeterValidationRule : record.get("KeyMeterValidationRule")
                };

                storeAlarmInstance.proxy.serviceParams = serviceParams;
                storeAlarmInstance.serviceParams = storeAlarmInstance.proxy.serviceParams;

                storeAlarmInstance.load({
                            params   : params,
                            callback : function() {
                            }
                        });
            },

            /**
             * Handler for row click on the alarminstance grid.
             * @param {Ext.grid.RowSelectionModel} selectModel
             * @param {Integer} rowIndex
             * @param {Ext.data.record} record
             */
            onAlarmInstanceRowSelect          : function(selectModel, rowIndex, record) {
                var grid = Ext.getCmp(this._GridMeterData);
                var storeMeterData = grid.store;
                var params = {
                    start : 0,
                    limit : grid.pageSize
                };
                var KeyMeter = record.get("KeyMeter");
                var serviceParams = {
                    KeyMeter : KeyMeter
                };
                grid.setComboValue(KeyMeter);
                storeMeterData.proxy.serviceParams = serviceParams;
                storeMeterData.serviceParams = storeMeterData.proxy.serviceParams;

                // we clear any prexisting listeners to the load event in the same scope
                var listenersToDelete = [];
                if (storeMeterData.events['load'] && storeMeterData.events['load'].listeners.length > 0) {
                    Ext.each(storeMeterData.events['load'].listeners, function(listener) {
                                if (listener.scope == this) {
                                    listenersToDelete.push(listener);
                                }
                            }, this);

                    Ext.each(listenersToDelete, function(listener) {
                                storeMeterData.un('load', listener.fn, listener.scope);
                            }, this);
                }

                var filter = grid.filters.getFilter("AcquisitionDateTime");
                if (filter) {
                    var startDate = record.get("MeterDataAcquisitionDateTimeStart");
                    var endDate = record.get("MeterDataAcquisitionDateTimeEnd");
                    if (Ext.isDate(startDate) && Ext.isDate(endDate)) {
                        filter.setValue({
                                    before : endDate.add(Date.DAY, 1),
                                    after  : startDate.add(Date.DAY, -1)
                                });
                        // we do this because there is no callback on the filter setValue that triggers the store relaod
                        storeMeterData.on({
                                    scope : this,
                                    // single : true,
                                    load  : function() {
                                        this.highlightRowByAcquisitionDateTime(startDate, grid, storeMeterData);
                                        this.highlightRowByAcquisitionDateTime(endDate, grid, storeMeterData);
                                    }
                                });
                    }
                    else {
                        var instanceDateTime = record.get("InstanceDateTime");
                        if (Ext.isDate(instanceDateTime)) {
                            filter.setValue({
                                        before : instanceDateTime.add(Date.DAY, 1)
                                    });
                            // we do this because there is no callback on the filter setValue that triggers the store relaod
                            storeMeterData.on({
                                        scope : this,
                                        // single : true,
                                        load  : function() {
                                            this.highlightRowByKeyMeterData(record.get("KeyMeterData"), grid, storeMeterData);
                                        }
                                    });
                        }
                    }
                    filter.setActive(true);
                }
                Viz.Services.EnergyWCF.Meter_GetItem({
                            Key     : KeyMeter,
                            success : function(meter) {
                                grid.setCurrentMeter(meter);
                            },
                            scope   : this
                        })
                /*
                 * storeMeterData.load({ params : params, callback : function() { this.highlightRow.defer(1000, this, [record, grid, storeMeterData]); }, scope : this });
                 */
            },

            /**
             * Highlight a row of the grid based on the MeterDataKey
             * @param {string} keyMeterData
             * @param {} grid
             * @param {} store
             */
            highlightRowByKeyMeterData        : function(keyMeterData, grid, store) {
                if (!Ext.isEmpty(keyMeterData)) {
                    var dex = store.find('KeyMeterData', keyMeterData);
                    if (dex >= 0) {
                        var row = grid.getView().getRow(dex);
                        Ext.get(row).addClass('highlighted-row');
                    }
                }
            },

            /**
             * Highlight a row of the grid based on the AcquisitionDateTime.
             * @param {} date
             * @param {} grid
             * @param {} store
             */
            highlightRowByAcquisitionDateTime : function(date, grid, store) {
                if (Ext.isDate(date)) {
                    var dex = store.find('AcquisitionDateTime', date);
                    if (dex >= 0) {
                        var row = grid.getView().getRow(dex);
                        Ext.get(row).addClass('highlighted-row');
                    }
                }

            }
        });
Ext.reg('vizPanelMeterValidationRule', Viz.panel.MeterValidationRule);

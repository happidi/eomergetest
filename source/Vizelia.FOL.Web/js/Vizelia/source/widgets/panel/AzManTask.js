﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.AzManTask
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.AzManTask = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_azmantask'),
                    iconCls : 'viz-icon-small-azmantask',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridAzManTask'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.AzManTask.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelAzManTask', Viz.panel.AzManTask);
﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Playlist
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.Playlist = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_playlist'),
                    iconCls : 'viz-icon-small-playlist',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridPlaylist'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Playlist.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelPlaylist', Viz.panel.Playlist);
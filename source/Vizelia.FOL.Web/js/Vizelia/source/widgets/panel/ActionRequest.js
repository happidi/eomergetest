﻿Ext.namespace('Viz.panel');
/**
 * @class Viz.panel.ActionRequest
 * @extends Ext.Panel
 * <p>
 * The panel for managing action requests.
 * </p>
 */
Viz.panel.ActionRequest = Ext.extend(Viz.panel.Panel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor              : function(config) {
                config = config || {};
                var defaultConfig = {};
                this._GridTask = Ext.id();
                // this._GridApproval = Ext.id();
                var forcedConfig = {
                    title     : $lang('msg_actionrequest'),
                    iconCls   : 'viz-icon-small-actionrequest',
                    closable  : true,
                    bodyStyle : 'padding:1px',
                    layout    : 'fit',
                    items     : [{
                                xtype     : 'panel',
                                layout    : 'border',
                                bodyStyle : 'background-color:white;',
                                border    : false,
                                frame     : false,
                                items     : [{
                                            region    : 'center',
                                            split     : 'true',
                                            frame     : false,
                                            border    : false,
                                            layout    : 'fit',
                                            minHeight : 300,
                                            margins   : '1 1 0 1',
                                            items     : [{
                                                        title     : $lang('msg_actionrequest'),
                                                        header    : false,
                                                        iconCls   : 'viz-icon-small-actionrequest',

                                                        border    : true,
                                                        frame     : false,
                                                        xtype     : 'vizGridActionRequest',
                                                        listeners : {
                                                            // to allow reload of task from click or right click we have to switch to selectionModel
                                                            render : function(comp) {
                                                                comp.getSelectionModel().on("rowselect", this.onActionRequestRowSelect, this);
                                                            },
                                                            scope  : this
                                                        }
                                                    }]
                                        }, {
                                            margins : '0 1 1 1',
                                            split   : true,
                                            frame   : false,
                                            border  : false,
                                            height  : 200,
                                            region  : 'south',
                                            header  : false,
                                            layout  : 'fit',
                                            items   : [{
                                                iconCls : 'viz-icon-small-technician',
                                                title   : $lang('msg_task'),
                                                id      : this._GridTask,
                                                iconCls : 'viz-icon-small-task',
                                                header  : true,
                                                border  : true,
                                                frame   : false,
                                                xtype   : 'vizGridTask'
                                                    // {
                                                    // xtype : 'tabpanel',
                                                    // activeTab : 0,
                                                    // items : [{
                                                    // title : $lang('msg_task'),
                                                    // border : false,
                                                    // id : this._GridTask,
                                                    // xtype : 'vizGridTask'
                                                    // }, {
                                                    // title : $lang('msg_approval'),
                                                    // border : false,
                                                    // id : this._GridApproval,
                                                    // xtype : 'vizGridApproval'
                                                    // }]
                                                    // }]
                                                }]
                                        }]
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.panel.ActionRequest.superclass.constructor.call(this, config);
            },
            /**
             * Handler for row click on the action request grid.
             * @param {Ext.grid.RowSelectionModel} selectModel
             * @param {Integer} rowIndex
             * @param {Ext.data.record} record
             */
            onActionRequestRowSelect : function(selectModel, rowIndex, record) {
                var grid = selectModel.grid;
                // var record = grid.getSelectionModel().getSelected();
                var storeTask = Ext.getCmp(this._GridTask).store;
                // var storeApproval = Ext.getCmp(this._GridApproval).store;
                var params = {
                    start : 0,
                    limit : grid.pageSize
                };
                var serviceParams = {
                    KeyActionRequest : record.get("KeyActionRequest")
                };
                // storeApproval.proxy.serviceParams = serviceParams;
                // storeApproval.serviceParams = storeApproval.proxy.serviceParams;
                storeTask.proxy.serviceParams = serviceParams;
                storeTask.serviceParams = storeTask.proxy.serviceParams;
                // storeApproval.load({
                // params : params,
                // callback : function() {
                // }
                // });
                storeTask.load({
                            params   : params,
                            callback : function() {
                            }
                        });
            }
        });
Ext.reg('vizPanelActionRequest', Viz.panel.ActionRequest);

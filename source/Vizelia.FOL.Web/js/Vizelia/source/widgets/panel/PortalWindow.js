﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.PortalWindow
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.PortalWindow = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor             : function(config) {
                config = config || {};
                var defaultConfig = {};
                this._GridPortalTab = Ext.id();
                this._GridPortalColumn = Ext.id();
                this._GridPortlet = Ext.id();

                var forcedConfig = {
                    title   : $lang('msg_portalwindows'),
                    iconCls : 'viz-icon-small-portalwindow',
                    items   : [{
                                xtype     : 'panel',
                                layout    : 'border',
                                bodyStyle : 'background-color:white;',
                                border    : false,
                                frame     : false,
                                items     : [{
                                            region    : 'north',
                                            stateId   : 'PortalWindowRegionNorth',
                                            split     : true,
                                            frame     : false,
                                            border    : false,
                                            layout    : 'fit',
                                            height    : 200,
                                            minHeight : 100,
                                            items     : [{
                                                        title     : $lang('msg_portalwindows'),
                                                        header    : false,
                                                        iconCls   : 'viz-icon-small-portalwindow',
                                                        border    : true,
                                                        frame     : false,
                                                        xtype     : 'vizGridPortalWindow',
                                                        listeners : {
                                                            // to allow reload of task from click or right click we have to switch to selectionModel
                                                            render : function(comp) {
                                                                comp.getSelectionModel().on("rowselect", this.onPortalWindowRowSelect, this);
                                                            },
                                                            scope  : this
                                                        }
                                                    }]
                                        }, {
                                            region    : 'center',
                                            stateId   : 'PortalTabRegionWest',
                                            split     : true,
                                            frame     : false,
                                            border    : false,
                                            height    : 300,
                                            width     : 400,
                                            layout    : 'fit',
                                            minHeight : 100,
                                            items     : [{
                                                        title     : $lang('msg_portaltabs'),
                                                        header    : false,
                                                        iconCls   : 'viz-icon-small-portaltab',
                                                        border    : true,
                                                        frame     : false,
                                                        id        : this._GridPortalTab,
                                                        xtype     : 'vizGridPortalTab',
                                                        listeners : {
                                                            // to allow reload of task from click or right click we have to switch to selectionModel
                                                            render : function(comp) {
                                                                comp.getSelectionModel().on("rowselect", this.onPortalTabRowSelect, this);
                                                            },
                                                            scope  : this
                                                        }
                                                    }]
                                        }, {
                                            region    : 'east',
                                            stateId   : 'PortalColumnRegionEast',
                                            split     : true,
                                            frame     : false,
                                            border    : false,
                                            layout    : 'fit',
                                            minHeight : 100,
                                            height    : 300,
                                            width     : 400,
                                            items     : [{
                                                        title     : $lang('msg_portalcolumns'),
                                                        header    : false,
                                                        iconCls   : 'viz-icon-small-portalcolumn',
                                                        border    : true,
                                                        frame     : false,
                                                        id        : this._GridPortalColumn,
                                                        xtype     : 'vizGridPortalColumn',
                                                        listeners : {
                                                            // to allow reload of task from click or right click we have to switch to selectionModel
                                                            render : function(comp) {
                                                                comp.getSelectionModel().on("rowselect", this.onPortalColumnRowSelect, this);
                                                            },
                                                            scope  : this
                                                        }
                                                    }]
                                        }, {
                                            region    : 'south',
                                            stateId   : 'PortletRegionSouth',
                                            split     : true,
                                            frame     : false,
                                            border    : false,
                                            layout    : 'fit',
                                            minHeight : 100,
                                            height    : 200,
                                            items     : [{
                                                        title           : $lang('msg_portlets'),
                                                        header          : false,
                                                        iconCls         : 'viz-icon-small-portlet',
                                                        border          : true,
                                                        frame           : false,
                                                        id              : this._GridPortlet,
                                                        xtype           : 'vizGridPortlet',
                                                        showEntityCombo : true
                                                    }]
                                        }]
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.PortalWindow.superclass.constructor.call(this, config);
            },

            /**
             * Handler for row click on the metervalidationrule grid.
             * @param {Ext.grid.RowSelectionModel} selectModel
             * @param {Integer} rowIndex
             * @param {Ext.data.record} record
             */
            onSelect                : function(selectModel, rowIndex, record, gridId, Key) {
                var grid = Ext.getCmp(gridId);
                grid[Key] = record.get(Key);

                var store = grid.store;
                var params = {
                    start : 0,
                    limit : grid.pageSize
                };

                var serviceParams = {};
                serviceParams[Key] = record.get(Key);

                store.proxy.serviceParams = serviceParams;
                store.serviceParams = store.proxy.serviceParams;

                store.load({
                            params   : params,
                            callback : function() {
                            }
                        });
            },

            onClear                 : function(gridId) {
                var grid = Ext.getCmp(gridId);
                var store = grid.store;
                store.removeAll();
            },

            /**
             * Handler for row click on the metervalidationrule grid.
             * @param {Ext.grid.RowSelectionModel} selectModel
             * @param {Integer} rowIndex
             * @param {Ext.data.record} record
             */
            onPortalWindowRowSelect : function(selectModel, rowIndex, record) {
                this.onSelect(selectModel, rowIndex, record, this._GridPortalTab, "KeyPortalWindow");
                this.onClear(this._GridPortalColumn);
                this.onClear(this._GridPortlet);
            },

            /**
             * Handler for row click on the metervalidationrule grid.
             * @param {Ext.grid.RowSelectionModel} selectModel
             * @param {Integer} rowIndex
             * @param {Ext.data.record} record
             */
            onPortalTabRowSelect    : function(selectModel, rowIndex, record) {
                this.onSelect(selectModel, rowIndex, record, this._GridPortalColumn, "KeyPortalTab");
                this.onClear(this._GridPortlet);
            },
            /**
             * Handler for row click on the metervalidationrule grid.
             * @param {Ext.grid.RowSelectionModel} selectModel
             * @param {Integer} rowIndex
             * @param {Ext.data.record} record
             */
            onPortalColumnRowSelect : function(selectModel, rowIndex, record) {
                this.onSelect(selectModel, rowIndex, record, this._GridPortlet, "KeyPortalColumn");

            }
        });
Ext.reg('vizPanelPortalWindow', Viz.panel.PortalWindow);
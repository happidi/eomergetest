﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.AzManFilter
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.AzManFilter = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_azmanfilter'),
                    iconCls : 'viz-icon-small-filter',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridAzManFilter',
                                entity : config.entity
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.AzManFilter.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelAzManFilter', Viz.panel.AzManFilter);
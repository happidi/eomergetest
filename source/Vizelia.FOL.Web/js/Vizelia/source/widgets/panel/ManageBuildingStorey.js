﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.ManageBuildingStorey
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.ManageBuildingStorey = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor  : function(config) {
                config = config || {};
                var defaultConfig = {};
                this._GridZoningLegendId = Ext.id();
                this._ViewerId = Ext.id();
                var forcedConfig = {
                    title   : $lang('msg_spacemanagement_managefloor') + ' : ' + config.title,
                    iconCls : 'viz-icon-small-managefloor',
                    items   : [{
                                border : true,
                                frame  : false,
                                xtype  : 'panel',
                                title  : config.title,
                                layout : 'border',
                                items  : [{
                                            region : 'center',
                                            split  : true,
                                            xtype  : 'container',
                                            layout : 'border',
                                            items  : [{
                                                        region    : 'north',
                                                        bodyStyle : 'background-color:white',
                                                        split     : true,
                                                        layout    : 'fit',
                                                        items     : [{
                                                                    id                : this._GridZoningLegendId,
                                                                    xtype             : 'vizGridZoningLegend',
                                                                    KeyBuildingStorey : config.entity.KeyBuildingStorey,
                                                                    listeners         : {
                                                                        render : function(grid) {
                                                                            grid.store.on("load", this.onLegendLoad.createDelegate(this, [grid.store]), this);
                                                                        },
                                                                        toogle : function(grid, KeyClassificationItem, display) {
                                                                            var viewer = Ext.getCmp(this._ViewerId);
                                                                            viewer.toggleSpaceAssignment(KeyClassificationItem);
                                                                        },
                                                                        scope  : this
                                                                    }
                                                                }],
                                                        border    : true,
                                                        margins   : '5 5 0 0',
                                                        height    : 200
                                                    }, {
                                                        region    : 'center',
                                                        split     : true,
                                                        border    : true,
                                                        margins   : '0 5 5 0',
                                                        bodyStyle : 'background-color:white',
                                                        layout    : 'fit',
                                                        items     : [{
                                                                    id                : this._ViewerId,
                                                                    layout            : 'fit',
                                                                    xtype             : 'vizSilverlightViewer',
                                                                    silverlightParams : "src=xaml/" + config.entity.CADFile + ".xaml" // P-0007-GM_CHAMBELLAND_BAT_A-A-1-RDC.xaml"
                                                                }]
                                                    }]
                                        }, {
                                            region  : 'west',
                                            layout  : 'fit',
                                            split   : true,
                                            margins : '5 0 5 5',

                                            items   : [{
                                                        xtype       : 'tabpanel',
                                                        activeTab   : 0,
                                                        border      : false,
                                                        tabPosition : 'bottom',
                                                        items       : [{
                                                                    title       : 'Zoning',
                                                                    xtype       : 'vizTreeClassificationItem',
                                                                    header      : false,
                                                                    rootVisible : true,
                                                                    border      : false,
                                                                    tbar        : [{
                                                                                text    : 'Show all',
                                                                                iconCls : 'viz-icon-small-classificationitem',
                                                                                handler : function() {
                                                                                    var grid = Ext.getCmp(this._GridZoningLegendId);
                                                                                    grid.store.load();
                                                                                },
                                                                                scope   : this
                                                                            }, {
                                                                                text    : 'Hide all',
                                                                                iconCls : 'viz-icon-small-classificationitem',
                                                                                handler : function() {
                                                                                    var viewer = Ext.getCmp(this._ViewerId);
                                                                                    var grid = Ext.getCmp(this._GridZoningLegendId);
                                                                                    grid.store.load({
                                                                                                params : {
                                                                                                    KeyClassificationItem : ''
                                                                                                }
                                                                                            });
                                                                                    viewer.resetSpaceAssignment();
                                                                                },
                                                                                scope   : this
                                                                            }],
                                                                    bodyStyle   : 'background-color:white',
                                                                    root        : {
                                                                        id      : '#2133',
                                                                        iconCls : 'viz-icon-small-classificationitem',
                                                                        text    : $lang('Zoning')
                                                                    },
                                                                    listeners   : {
                                                                        click : function(node) {
                                                                            Ext.getCmp(this._GridZoningLegendId).store.load({
                                                                                        params : {
                                                                                            KeyClassificationItem : node.attributes.id
                                                                                        }
                                                                                    });
                                                                        },
                                                                        scope : this
                                                                    }
                                                                }, {
                                                                    title : 'Dynamic',
                                                                    html  : 'not implemented'
                                                                }, {
                                                                    title : 'Organization',
                                                                    html  : 'not implemented'
                                                                }, {
                                                                    title : 'Segment',
                                                                    html  : 'not implemented'
                                                                }]

                                                    }],

                                            width   : 300
                                        }]
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.ManageBuildingStorey.superclass.constructor.call(this, config);
            },

            /**
             * @private Corrects the bug on Silverlight when showing / hiding
             */
            onHide       : function() {
                this.pHeight = this.getHeight();
                this.pWidth = this.getWidth();
                this.getVisibilityEl().setHeight(0);
                this.getVisibilityEl().setWidth(0);
            },

            /**
             * @private Corrects the bug on Silverlight when showing / hiding
             */
            onShow       : function() {
                Viz.panel.ManageBuildingStorey.superclass.onShow.apply(this, arguments);
                if (this.pWidth && this.pHeight)
                    this.getVisibilityEl().setSize(this.pWidth, this.pHeight);
            },

            /**
             * Handler for the legend store load.
             * @param {Ext.data.Store} store The legend store.
             */
            onLegendLoad : function(store) {
                var viewer = Ext.getCmp(this._ViewerId);
                var list = viewer.createManagedObject('List<Space>');
                Ext.each(store.data.items, function(item) {
                            list.push(viewer.createManagedObject('Space', item.json));
                        });

                viewer.displaySpaceAssignment(list);
            }
        });
Ext.reg('vizPanelManageBuildingStorey', Viz.panel.ManageBuildingStorey);
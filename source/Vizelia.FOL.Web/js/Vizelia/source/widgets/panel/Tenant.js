﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Tenant The tenant panel
 * @extends Viz.panel.Panel
 * @xtype vizPanelTenant
 */
Viz.panel.Tenant = Ext.extend(Viz.panel.Panel, {

            /**
             * @private Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    title   : $lang('msg_application_tenant'),
                    iconCls : 'viz-icon-small-tenant',
                    items   : {
                        xtype : 'vizGridTenant'
                    }
                };

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Chart.superclass.constructor.call(this, config);
            }
        });

Ext.reg('vizPanelTenant', Viz.panel.Tenant);
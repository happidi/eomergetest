﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.AzManOperation
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.AzManOperation = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_azmanoperation'),
                    iconCls : 'viz-icon-small-azmanoperation',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridAzManOperation'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.AzManOperation.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelAzManOperation', Viz.panel.AzManOperation);
﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.DynamicDisplayImage
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.DynamicDisplayImage = Ext.extend(Viz.panel.Panel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_dynamicdisplayimage'),
                    iconCls : 'viz-icon-small-dynamicdisplayimage',
                    items   : {
                        border : Viz.DisplayAsDesktop ? false : true,
                        xtype  : 'vizDataViewDynamicDisplayImage'
                    }
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.DynamicDisplayImage.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelDynamicDisplayImage', Viz.panel.DynamicDisplayImage);
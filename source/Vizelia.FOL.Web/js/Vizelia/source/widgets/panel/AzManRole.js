﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.AzManRole
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.AzManRole = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_azmanrole'),
                    iconCls : 'viz-icon-small-role',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizTreeAzManRole'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.AzManRole.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelAzManRole', Viz.panel.AzManRole);
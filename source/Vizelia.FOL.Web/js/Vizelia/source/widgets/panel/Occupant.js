﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Occupant
 * @extends Ext.Panel
 * <p>
 * The occupant panel.
 * </p>
 */
Viz.panel.Occupant = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_search_occupant'),
                    iconCls : 'viz-icon-small-search-occupant',
                    items   : [{
                                border : true,
                                xtype  : 'vizGridOccupant'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Occupant.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelOccupant', Viz.panel.Occupant);
﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.EnergyCertificate
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.EnergyCertificate = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_energycertificate'),
                    iconCls : 'viz-icon-small-energycertificate',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridEnergyCertificate'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.EnergyCertificate.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelEnergyCertificate', Viz.panel.EnergyCertificate);
﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.DynamicDisplay
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.DynamicDisplayColorTemplate = Ext.extend(Viz.panel.Panel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_dynamicdisplaycolortemplate'),
                    iconCls : 'viz-icon-small-dynamicdisplaycolortemplate',
                    items   : {
                        border : Viz.DisplayAsDesktop ? false : true,
                        xtype  : 'vizGridDynamicDisplayColorTemplate'
                    }
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.DynamicDisplayColorTemplate.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelDynamicDisplayColorTemplate', Viz.panel.DynamicDisplayColorTemplate);
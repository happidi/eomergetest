﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.AuditEntity
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.AuditEntity = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_application_auditentity'),
                    iconCls : 'viz-icon-small-auditentity',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridAuditEntity'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Mail.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelAuditEntity', Viz.panel.AuditEntity);
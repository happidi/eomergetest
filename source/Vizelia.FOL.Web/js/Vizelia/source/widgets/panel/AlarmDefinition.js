﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.AlarmDefinition
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.AlarmDefinition = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                : function(config) {

                this._GridAlarmInstance = Ext.id();
                config = config || {};
                var defaultConfig = {
                    title   : $lang('msg_alarmdefinition'),
                    iconCls : 'viz-icon-small-alarm',
                    items   : [{
                                xtype     : 'panel',
                                layout    : 'border',
                                bodyStyle : 'background-color:white;',
                                border    : false,
                                frame     : false,
                                items     : [{
                                            region    : 'center',
                                            split     : 'true',
                                            frame     : false,
                                            border    : false,
                                            layout    : 'fit',
                                            minHeight : 300,
                                            margins   : '1 1 0 1',
                                            items     : [{
                                                        title     : $lang('msg_alarmdefinition'),
                                                        header    : false,
                                                        iconCls   : 'viz-icon-small-alarm',
                                                        border    : true,
                                                        frame     : false,
                                                        xtype     : 'vizGridAlarmDefinition',
                                                        listeners : {
                                                            render : function(comp) {
                                                                comp.getSelectionModel().on("rowselect", this.onAlarmDefinitionRowSelect, this);
                                                            },
                                                            scope  : this
                                                        }
                                                    }]
                                        }, {
                                            margins     : '0 1 1 1',
                                            split       : true,
                                            frame       : false,
                                            border      : false,
                                            height      : 250,
                                            region      : 'south',
                                            header      : false,
                                            collapsible : false,
                                            layout      : 'fit',
                                            items       : [{
                                                        id     : this._GridAlarmInstance,
                                                        header : false,
                                                        // title : $lang('msg_meterdata'),
                                                        // iconCls : 'viz-icon-small-meterdata',
                                                        border : true,
                                                        frame  : false,
                                                        xtype  : 'vizGridAlarmInstance'
                                                    }]
                                        }]
                            }]
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.AlarmDefinition.superclass.constructor.call(this, config);
            },
            /**
             * Handler for row click on the action request grid.
             * @param {Ext.grid.RowSelectionModel} selectModel
             * @param {Integer} rowIndex
             * @param {Ext.data.record} record
             */
            onAlarmDefinitionRowSelect : function(selectModel, rowIndex, record) {
                var grid = Ext.getCmp(this._GridAlarmInstance);
                var store = grid.store;
                var params = {
                    start : 0,
                    limit : grid.pageSize
                };

                var serviceParams = {};
                serviceParams['KeyAlarmDefinition'] = record.get('KeyAlarmDefinition');

                store.proxy.serviceParams = serviceParams;
                store.serviceParams = store.proxy.serviceParams;

                store.load({
                            params   : params,
                            callback : function() {
                            }
                        });
            }
        });
Ext.reg('vizPanelAlarmDefinition', Viz.panel.AlarmDefinition);

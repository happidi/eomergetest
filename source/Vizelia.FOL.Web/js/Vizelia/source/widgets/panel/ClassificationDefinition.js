﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.ClassificationDefinition
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.ClassificationDefinition = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_application_classificationdefinition'),
                    iconCls : 'viz-icon-small-classificationdefinition',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridClassificationDefinition'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.ClassificationDefinition.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelClassificationDefinition', Viz.panel.ClassificationDefinition);
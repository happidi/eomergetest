﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.TraceEntry
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.TraceEntry = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_application_trace_viewer'),
                    iconCls : 'viz-icon-small-trace',
                    items   : [{
                                border       : Viz.DisplayAsDesktop ? false : true,
                                xtype        : 'vizGridTraceEntry',
                                storeFilters : this.storeFilters || config.storeFilters,
                                stateId      : 'MainViewPort.vizGridTraceEntry'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.TraceEntry.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelTraceEntry', Viz.panel.TraceEntry);

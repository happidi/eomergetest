﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.AlarmTable
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.AlarmTable = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    title   : $lang('msg_alarmtable'),
                    iconCls : 'viz-icon-small-alarmtable',
                    items   : {
                        border : Viz.DisplayAsDesktop ? false : true,
                        xtype  : 'vizGridAlarmTable'
                    }
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.AlarmTable.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelAlarmTable', Viz.panel.AlarmTable);

﻿Ext.namespace('Viz.panel');
/**
 * @class Viz.panel.LoginHistoryActive
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.LoginHistoryActive = Ext.extend(Viz.panel.Panel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    title   : $lang('msg_application_loginhistoryactive'),
                    iconCls : 'viz-icon-small-loginhistoryactive',
                    items   : [{
                                border  : Viz.DisplayAsDesktop ? false : true,
                                xtype   : 'vizGridLoginHistory',
                                columns : [{
                                            header     : $lang('msg_loginhistory_username'),
                                            dataIndex  : 'UserName',
                                            sortable   : true,
                                            filterable : true
                                        }, {
                                            header     : $lang('msg_profile_ip'),
                                            dataIndex  : 'IP',
                                            sortable   : true,
                                            filterable : true
                                        }, {
                                            header    : $lang('msg_loginhistory_logindate'),
                                            dataIndex : 'LoginDate',
                                            xtype     : 'datecolumn',
                                            align     : 'right',
                                            format    : Viz.getLocalizedDateTimeFormat(true),
                                            filter    : {
                                                type : 'date'
                                            },
                                            width     : 110,
                                            sortable  : true
                                        }],
                                store   : new Viz.store.LoginHistoryActive(),
                                tbar    : [{
                                            text    : '',
                                            iconCls : '',
                                            hidden  : true,
                                            scope   : this
                                        }]
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.panel.LoginHistoryActive.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelLoginHistoryActive', Viz.panel.LoginHistoryActive);
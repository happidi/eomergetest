﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Map
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.Map = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_map'),
                    iconCls : 'viz-icon-small-map',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridMap'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Map.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelMap', Viz.panel.Map);
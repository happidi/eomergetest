﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.User
 * @extends Ext.Panel
 * <p>
 * The main screen for managing users.
 * </p>
 */
Viz.panel.User = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_user_management'),
                    iconCls : 'viz-icon-small-user',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridUser'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.User.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelUser', Viz.panel.User);
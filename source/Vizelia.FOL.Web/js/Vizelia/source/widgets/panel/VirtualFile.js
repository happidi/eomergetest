﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.VirtualFile
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.VirtualFile = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_virtualfile'),
                    iconCls : 'viz-icon-small-virtualfile',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridVirtualFile'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.VirtualFile.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelVirtualFile', Viz.panel.VirtualFile);
﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.StruxurewareDataAcquisitionContainer
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.StruxurewareDataAcquisitionContainer = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    title   : $lang('msg_struxurewaredataacquisitioncontainer'),
                    iconCls : 'viz-icon-small-struxurewaredataacquisitioncontainer',
                    items   : {
                        border : Viz.DisplayAsDesktop ? false : true,
                        xtype  : 'vizGridStruxureWareDataAcquisitionContainer'
                    }
                };

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.StruxurewareDataAcquisitionContainer.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelStruxurewareDataAcquisitionContainer', Viz.panel.StruxurewareDataAcquisitionContainer);

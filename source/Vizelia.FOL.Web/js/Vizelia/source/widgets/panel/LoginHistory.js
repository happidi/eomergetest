﻿Ext.namespace('Viz.panel');
/**
 * @class Viz.panel.LoginHistory
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.LoginHistory = Ext.extend(Viz.panel.Panel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    title   : $lang('msg_application_loginhistory'),
                    iconCls : 'viz-icon-small-loginhistory',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridLoginHistory'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.panel.LoginHistory.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelLoginHistory', Viz.panel.LoginHistory);
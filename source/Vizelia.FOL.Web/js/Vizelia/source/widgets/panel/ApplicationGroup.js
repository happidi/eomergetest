﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.ApplicationGroup
 * @extends Ext.Panel
 * <p>
 * The panel for managing application groups.
 * </p>
 */
Viz.panel.ApplicationGroup = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_applicationgroup_root'),
                    iconCls : 'viz-icon-small-applicationgroup',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridApplicationGroup'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.ApplicationGroup.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelApplicationGroup', Viz.panel.ApplicationGroup);
﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.MachineInstance
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.MachineInstance = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_machineinstance'),
                    iconCls : 'viz-icon-small-machineinstance',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridMachineInstance'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.MachineInstance.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelMachineInstance', Viz.panel.MachineInstance);
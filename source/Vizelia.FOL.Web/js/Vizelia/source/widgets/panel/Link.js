﻿
Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Link
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.Link = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_application_link'),
                    iconCls : 'viz-icon-small-link',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridLink'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Link.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelLink', Viz.panel.Link);
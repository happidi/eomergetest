﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.DrawingCanvas
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.DrawingCanvas = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_drawingcanvas'),
                    iconCls : 'viz-icon-small-drawingcanvas',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridDrawingCanvas'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.DrawingCanvas.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelDrawingCanvas', Viz.panel.DrawingCanvas);
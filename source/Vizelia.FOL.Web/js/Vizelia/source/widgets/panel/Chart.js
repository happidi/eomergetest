﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.Chart
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.Chart = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    title   : config.isKPI ? $lang('msg_chart_kpi') : $lang('msg_charts'),
                    iconCls : config.isKPI ? 'viz-icon-small-chart-kpi' : 'viz-icon-small-chart',
                    items   : {
                        border : Viz.DisplayAsDesktop ? false : true,
                        xtype  : config.useDataView ? 'vizDataViewChart' : 'vizGridChart',
                        isKPI  : config.isKPI
                    }
                };

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.Chart.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelChart', Viz.panel.Chart);

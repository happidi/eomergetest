﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.EventLog
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.EventLog = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_eventlog'),
                    iconCls : 'viz-icon-small-eventlog',
                    items   : [{
                                border : Viz.DisplayAsDesktop ? false : true,
                                xtype  : 'vizGridEventLog'
                            }]
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.EventLog.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelEventLog', Viz.panel.EventLog);
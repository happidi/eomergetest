﻿Ext.namespace('Viz.panel');

/**
 * @class Viz.panel.MeterData
 * @extends Ext.Panel
 * <p>
 * Summary
 * </p>
 */
Viz.panel.MeterData = Ext.extend(Viz.panel.Panel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};

                var forcedConfig = {
                    title   : $lang('msg_meterdata'),
                    iconCls : 'viz-icon-small-meterdata',
                    items   : {
                        border : Viz.DisplayAsDesktop ? false : true,
                        xtype  : 'vizGridMeterData',
                        entity : config.entity
                    }

                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.panel.MeterData.superclass.constructor.call(this, config);
            }
        });
Ext.reg('vizPanelMeterData', Viz.panel.MeterData);

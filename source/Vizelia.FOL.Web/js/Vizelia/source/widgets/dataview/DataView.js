﻿Ext.namespace('Viz.dataview');

/**
 * @class Viz.dataview.DataViewPanel
 * <p>
 * A dataview exposing the business entity DataViewPanel
 * </p>
 * @extends Viz.Panel
 */

Viz.dataview.DataViewPanel = Ext.extend(Ext.Panel, {

            /**
             * @cfg {string} the name of the attribute of the entity
             * @type String
             */
            entityKey          : '',
            /**
             * @cfg {Number} pageSize The number of records per page. (default to 50).
             */
            pageSize           : 50,
            /**
             * z
             * @cfg {Boolean} hasPagingToolbar <tt>true</tt> to show a paging toolbar, <tt>false</tt> otherwise. (default to <tt>true</tt>).
             */
            hasPagingToolbar   : true,
            /**
             * @cfg {Boolean} hasToolbar <tt>true</tt> to add a toolbar with add, update and delete buttons, <tt>false</tt> otherwise. (default to <tt>true</tt>).
             */
            hasToolbar         : true,

            readonly           : false,

            allowEmpty         : true,

            invalidClass       : 'x-grid-invalid',

            invalidText        : '',

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor        : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.applyIf(config, this);
                Ext.apply(config, forcedConfig);

                this._viewId = Ext.id();

                Ext.apply(config, {
                            layout : 'fit',
                            // autoScroll : true,
                            items  : [new Ext.DataView({
                                        id           : this._viewId,
                                        store        : config.store,
                                        tpl          : config.tpl,
                                        autoScroll   : true,
                                        multiSelect  : false,
                                        singleSelect : true,
                                        trackOver    : true,
                                        // overClass : 'images-view-x-view-over',
                                        itemSelector : 'div.thumb-wrap',
                                        prepareData  : config.prepareData ? config.prepareData : Ext.emptyFn,
                                        listeners    : {
                                            dblclick    : config.readonly === true ? Ext.emptyFn : this.onUpdateRecord,
                                            contextmenu : (config.readonly === true || config.hasToolbar === false) ? Ext.emptyFn : this.onContextMenu,
                                            afterrender : this.onAfterRender,
                                            click       : Ext.isFunction(config.onClickItem) ? config.onClickItem : this.onClickItem,
                                            scope       : this
                                        }
                                    })]
                        });
                if (config.hasPagingToolbar) {
                    Ext.apply(config, {
                                bbar : {
                                    xtype          : 'paging',
                                    displayInfo    : true,
                                    enableOverflow : true,
                                    pageSize       : config.pageSize,
                                    store          : config.store
                                }
                            });
                }

                if (!config.hasToolbar) {
                    delete config.tbar;
                };
                Viz.dataview.DataViewPanel.superclass.constructor.call(this, config);

                this.addEvents(
                        /**
                         * @event beforecopyrecord Fires before the copy action on a record.
                         * @param {Viz.grid.GridPanel} this.
                         * @param {Array} records The array of records that are being copied. It could be changed by the handler.
                         */
                        'beforecopyrecord');
                this.on("beforecopyrecord", this.onBeforeCopyRecord, this);
            },

            /**
             * After render handler.
             */
            onAfterRender      : function() {
                var store = this.store;
                store.load.defer(100, store, [{
                                    params : {
                                        start : 0,
                                        limit : this.hasPagingToolbar ? this.pageSize : 0
                                    }

                                }]);
            },

            /**
             * Check if a grid is valid or not
             */
            isValid            : function() {
                var record = this.getSelectedRecord();
                if (this.allowEmpty === false && !record) {
                    this.markInvalid();
                    return false;
                }
                return true;
            },

            /**
             * Mark the grid as invalid (when it s used in a field for example
             * @param {} msg
             */
            markInvalid        : function(msg) {
                if (this.rendered) {
                    this.el.addClass(this.invalidClass)
                    if (this.invalidText) {
                        Ext.MessageBox.show({
                                    title    : $lang('msg_error_validation'),
                                    msg      : this.invalidText,
                                    buttons  : Ext.MessageBox.OK,
                                    minWidth : 250
                                });
                    }
                }
            },

            /**
             * Remove the invalid class.
             */
            clearInvalid       : function() {
                this.el.removeClass(this.invalidClass)
            },

            /**
             * Add a new record.
             */
            onAddRecord        : function() {
                Viz.openFormCrud(this.formConfig, 'create', {});
            },

            /**
             * Update an existing record.
             */
            onUpdateRecord     : function() {
                var record = this.getSelectedRecord();
                if (record) {
                    var entity = record.json;
                    Viz.openFormCrud(this.formConfig, 'update', entity);
                }
            },
            /**
             * Delete a record.
             */
            onDeleteRecord     : function() {
                var record = this.getSelectedRecord();
                if (record) {
                    var msgDelete = $lang("msg_record_delete_confirmation_single");
                    Ext.MessageBox.confirm($lang('msg_application_title'), msgDelete, function(button) {
                                if (button == 'yes') {
                                    this.store.remove(record);
                                    this.saveStore();
                                }
                            }, this);
                }
            },
            /**
             * Handler for the before copy event
             * @param {} record
             */
            onBeforeCopyRecord : function(record) {

            },

            /**
             * Copy a record.
             */
            onCopyRecord       : function() {
                var record = this.getSelectedRecord();
                if (record) {
                    if (this.fireEvent("beforecopyrecord", record)) {
                        var newdata = Viz.clone(record.data);
                        var reader = this.store.reader;
                        var idProperty = reader.meta.idProperty;
                        if (idProperty)
                            newdata[idProperty] = '';
                        newdata['LocalId'] = null;
                        newdata['Creator'] = null;
                        this.store.add(new this.store.recordType(newdata));

                        this.saveStore();
                    }

                }
            },

            /**
             * @param {} dataview
             * @param {} index
             * @param {} node
             * @param {} event
             */
            onContextMenu      : function(dataview, index, node, event) {
                dataview.select(index, false, true);
                event.stopEvent();
                if (this.hideMenu == true)
                    return;

                if (!this.ctxMenu && this.menu)
                    this.ctxMenu = new Ext.menu.Menu({
                                items : this.menu
                            });
                if (this.ctxMenu instanceof Ext.menu.Menu) {
                    this.ctxMenu.on("hide", function(menu) {
                                this.ctxMenu.destroy();
                                this.ctxMenu = null;
                            }, this);
                    this.ctxMenu.showAt(event.getXY());
                }

            },
            /**
             * Get the dataview object
             */
            getView            : function() {
                return Ext.getCmp(this._viewId);
            },

            /**
             * Get the selected record.
             * @return {}
             */
            getSelectedRecord  : function() {
                var record = this.getView().getSelectedRecords()[0];
                return record;
            },
            /**
             * Reload the store or a single element of the view.
             * @param {} entity
             */
            reloadStore        : function(entity) {
                var reader = this.store.reader;
                var idProperty = reader.meta.idProperty;

                if (entity && !Ext.isEmpty(idProperty)) {

                    if (this.store && this.store.data) {
                        Ext.each(this.store.data.items, function(item) {
                                    if (item.data[idProperty] == entity[idProperty])
                                        item.data = entity;
                                }, this)
                    }
                    var node = this.getView().getNode(entity[idProperty]);
                    if (node) {
                        var index = this.getView().indexOf(node);
                        this.getView().refreshNode(index);
                        return;
                    }
                }
                this.store.reload();
            },

            /**
             * Save the store.
             */
            saveStore          : function() {
                this.store.save();
            },

            onClickItem        : function(dv, index, item, e) {

            }
        });
Ext.reg("vizDataView", Viz.dataview.DataViewPanel);

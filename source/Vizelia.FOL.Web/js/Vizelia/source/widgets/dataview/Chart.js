﻿/**
 * @class Viz.dataview.Chart
 * <p>
 * A dataview exposing the business entity Chart
 * </p>
 * @extends Viz.Panel
 */

Viz.dataview.Chart = Ext.extend(Viz.dataview.DataViewPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor                   : function(config) {

                config = config || {};

                var isKPI = config.isKPI || false;

                var configMenuAndTbar = [{
                            text    : isKPI ? $lang("msg_chart_kpi_add") : $lang("msg_chart_add"), 
                            iconCls : isKPI ? 'viz-icon-small-chart-kpi-add' : 'viz-icon-small-chart-add',
                            hidden  : !$authorized('@@@@ Chart - Full Control') || config.readonly,
                            scope   : this,
                            handler : this.onAddRecord
                        }, {
                            text    : isKPI ? $lang("msg_chart_kpi_update") : $lang("msg_chart_update"),
                            iconCls : isKPI ? 'viz-icon-small-chart-kpi-update' : 'viz-icon-small-chart-update',
                            hidden  : !$authorized('@@@@ Chart - Full Control') || config.readonly,
                            scope   : this,
                            handler : this.onUpdateRecord
                        }, {
                            text    : isKPI ? $lang("msg_chart_kpi_delete") : $lang("msg_chart_delete"),
                            iconCls : isKPI ? 'viz-icon-small-chart-kpi-delete' : 'viz-icon-small-chart-delete',
                            hidden  : !$authorized('@@@@ Chart - Full Control') || config.readonly,
                            scope   : this,
                            handler : this.onDeleteRecord
                        }, {
                            text    : isKPI ? $lang("msg_chart_kpi_copy") : $lang("msg_chart_copy"),
                            iconCls : 'viz-icon-small-copy',
                            hidden  : !$authorized('@@@@ Chart - Full Control') || config.readonly,
                            scope   : this,
                            handler : this.onCopyRecord
                        }, '-', {
                            text    : isKPI ? $lang("msg_chart_kpi_view") : $lang("msg_chart_view"),
                            iconCls : 'viz-icon-small-display',
                            // hidden: !$authorized('@@@@ Chart - Full Control'),
                            scope   : this,
                            handler : this.onChartDisplay
                        }];

                var configTbarOnly = [{
                            xtype     : 'viztreecomboClassificationItemKPI',
                            width     : 250,
                            hidden    : !isKPI,
                            listeners : {
                                select : {
                                    fn    : this.onSelectClassificationItemKPI,
                                    scope : this
                                },
                                clear  : {
                                    fn    : this.onClearClassificationItemKPI,
                                    scope : this
                                }
                            }
                        }];
                var eqtip = Ext.isIE8 ? '{Title} <br> {Description} <br> {[(values[\'KPISetupInstructions\'] || "").replace(/\\"/g, \'\')]}' : '{Title} <br> {Description} <br> {KPISetupInstructions}';
                var defaultConfig = {
                    pageSize    : 10,
                    tpl         : new Ext.XTemplate('<tpl for=".">',
                            /**/
                            '<div class="thumb-wrap" id="{[Ext.util.Format.htmlEncode(values.KeyChart)]}" >',
                            /**/
                            isKPI ? '<div class="thumb"><a><img src="{[Ext.util.Format.htmlEncode(values.Url)]}"  ext:qtip="' + eqtip + '" ></a></div>' : '<div class="thumb"><a><img src="{[Ext.util.Format.htmlEncode(values.Url)]}" ext:qtip="{[Ext.util.Format.htmlEncode(values.Title)]} <br> {[Ext.util.Format.htmlEncode(values.Description)]}"></a></div>',
                            /**/
                            '<span >{[Ext.util.Format.htmlEncode(values.ShortTitle)]}</span></div>',

                            '</a>',
                            /**/
                            '</tpl>',
                            /**/
                            '<div class="x-clear"></div>'),
                    tbar        : configTbarOnly.concat(configMenuAndTbar),
                    menu        : configMenuAndTbar,
                    formConfig  : this.getFormConfig(isKPI),
                    store       : isKPI ? new Viz.store.ChartKPI({}) : new Viz.store.Chart({
                                isFavorite : true,
                                isKPI      : false
                            }),
                    prepareData : function(data) {
                        var keyImage = data.KeyKPIPreviewPicture || data.TenantKeyKPIPreviewPicture;
                        var url = keyImage ? Viz.Services.BuildUrlStream('Energy.svc', 'DynamicDisplayImage_GetStreamImage', {
                                    Key    : keyImage, 
                                    width  : 280,
                                    height : 200
                                }) : 'images/no_kpi_preview_picture.png';
                        data.Url = url;
                        data.ShortTitle = Ext.util.Format.ellipsis(data.Title, 38);

                        return data;
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.dataview.Chart.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("ChartChange", this.reloadStore, this);
            },

            /**
             * Destroy
             */
            onDestroy                     : function() {
                Viz.util.MessageBusMgr.unsubscribe("ChartChange", this.reloadStore, this);
                Viz.dataview.Chart.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * Get form config.
             * @param {} isKPI
             * @return {}
             */
            getFormConfig                 : function(isKPI) {
                var chartConfig = Viz.grid.Chart.prototype.getFormConfig();
                chartConfig.common.isKPI = isKPI;
                return chartConfig
            },

            /**
             * Handler for displaying a chart.
             * @method onChartDisplay
             */
            onChartDisplay                : function() {
                var record = this.getSelectedRecord();
                if (record) {
                    var entity = record.json;
                    Viz.grid.Chart.openChartDisplay(entity, 800, 550);
                }
            },

            /**
             * @private Handler for copying a chart.
             */
            onCopyRecord                  : function() {
                var record = this.getSelectedRecord();
                if (record) {
                    this.el.mask();
                    Viz.Services.EnergyWCF.Chart_Copy({
                                keys    : [record.data.KeyChart],
                                success : function(data) {
                                    this.el.unmask();
                                    this.reloadStore();
                                },
                                scope   : this
                            });
                }
            },

            onSelectClassificationItemKPI : function(tree, node) {
                this.store.serviceParams.KeyClassificationItemKPI = node.attributes.entity.KeyClassificationItem;
                this.reloadStore();
            },

            onClearClassificationItemKPI  : function() {
                this.store.serviceParams.KeyClassificationItemKPI = '';
                this.reloadStore();
            }
        });
Ext.reg("vizDataViewChart", Viz.dataview.Chart);

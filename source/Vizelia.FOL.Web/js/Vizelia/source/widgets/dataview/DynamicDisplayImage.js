﻿/**
 * @class Viz.dataview.DynamicDisplayImage
 * <p>
 * A dataview exposing the business entity DynamicDisplayImage
 * </p>
 * @extends Viz.Panel
 */

Viz.dataview.DynamicDisplayImage = Ext.extend(Viz.dataview.DataViewPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor                  : function(config) {
                this.imageType = Vizelia.FOL.BusinessEntities.DynamicDisplayImageType.Image;
                config = config || {};
                var configMenuAndTbar = [{
                            text    : $lang("msg_dynamicdisplayimage_add"),
                            iconCls : 'viz-icon-small-add',
                            hidden  : !$authorized('@@@@ Dynamic Display Image - Full Control') || config.readonly,
                            scope   : this,
                            handler : this.onAddRecord
                        }, {
                            text    : $lang("msg_dynamicdisplayimage_update"),
                            iconCls : 'viz-icon-small-update',
                            hidden  : !$authorized('@@@@ Dynamic Display Image - Full Control') || config.readonly,
                            scope   : this,
                            handler : this.onUpdateRecord
                        }, {
                            text    : $lang("msg_dynamicdisplayimage_delete"),
                            iconCls : 'viz-icon-small-delete',
                            hidden  : !$authorized('@@@@ Dynamic Display Image - Full Control') || config.readonly,
                            scope   : this,
                            handler : this.onDeleteRecord
                        }, {
                            text    : $lang("msg_dynamicdisplayimage_view"),
                            iconCls : 'viz-icon-small-display',
                            // hidden : !$authorized('DynamicDisplayImage_read'),
                            scope   : this,
                            handler : this.onDynamicDisplayImageDisplay
                        }];

                var configTbarOnly = [{
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.DynamicDisplayImageType,Vizelia.FOL.Common',
                            value        : Vizelia.FOL.BusinessEntities.DynamicDisplayImageType.Image,
                            width        : 250,
                            listeners    : {
                                select : {
                                    fn    : this.onSelectImageType,
                                    scope : this
                                }
                            }
                        }];

                var defaultConfig = {
                    tpl         : new Ext.XTemplate('<tpl for=".">',
                            /**/
                            '<div class="thumb-wrap" id="{[Ext.util.Format.htmlEncode(values.KeyDynamicDisplayImage)]}" >',
                            /**/
                            '<div class="thumb" style="width:200px"><img style="width:200px"  src="{[Ext.util.Format.htmlEncode(values.Url)]}" ext:qtip="{[Ext.util.Format.htmlEncode(values.Name)]} <br> {[Ext.util.Format.htmlEncode(values.KeyDynamicDisplayImage)]}"></div>',
                            /**/
                            '<span >{[Ext.util.Format.htmlEncode(values.ShortName)]}</span></div>',
                            /**/
                            '</tpl>',
                            /**/
                            '<div class="x-clear"></div>'),
                    tbar        : configTbarOnly.concat(configMenuAndTbar),
                    menu        : configMenuAndTbar,
                    formConfig  : Viz.grid.DynamicDisplayImage.prototype.getFormConfig(),
                    store       : new Viz.store.DynamicDisplayImage(),
                    prepareData : function(data) {
                        var url = Viz.Services.BuildUrlStream('Energy.svc', 'DynamicDisplayImage_GetStreamImage', {
                                    Key    : data.KeyDynamicDisplayImage,
                                    width  : 200,
                                    height : 200
                                });
                        data.Url = url;
                        data.ShortName = Ext.util.Format.ellipsis(data.Name, 38);
                        return data;
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.dataview.DynamicDisplayImage.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("DynamicDisplayImageChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy                    : function() {
                Viz.util.MessageBusMgr.unsubscribe("DynamicDisplayImageChange", this.reloadStore, this);
                Viz.dataview.DynamicDisplayImage.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * Handler for displaying a dynamicdisplayimage.
             * @method onChartDisplay
             */
            onDynamicDisplayImageDisplay : function() {
                var record = this.getSelectedRecord();
                if (record) {
                    var entity = record.json;
                    Viz.grid.DynamicDisplayImage.openDynamicDisplayImageDisplay(entity, 800, 550);
                }
            },

            onSelectImageType            : function(combo, record, value) {
                this.store.serviceParams.type = combo.getValue();
                var params = {
                    start : 0,
                    limit : this.pageSize
                };
                this.store.load({
                            params   : params,
                            callback : function() {
                            }
                        });
                this.imageType = combo.getValue();
            },

            reloadStore                  : function(entity) {
                this.store.reload();
            },

            /**
             * Add a new record.
             */
            onAddRecord                  : function() {
                Viz.openFormCrud(this.formConfig, 'create', {
                            Type : this.imageType
                        });
            }

        });
Ext.reg("vizDataViewDynamicDisplayImage", Viz.dataview.DynamicDisplayImage);

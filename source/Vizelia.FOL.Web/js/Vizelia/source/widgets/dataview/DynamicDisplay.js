﻿/**
 * @class Viz.dataview.DynamicDisplay
 * <p>
 * A dataview exposing the business entity DynamicDisplay
 * </p>
 * @extends Viz.Panel
 */

Viz.dataview.DynamicDisplay = Ext.extend(Viz.dataview.DataViewPanel, {
    /**
    * Ctor.
    * @param {Object} config
    */
    constructor: function (config) {

        config = config || {};

        var configMenuAndTbar = [{
            text: $lang("msg_dynamicdisplay_add"),
            iconCls: 'viz-icon-small-add',
            hidden: !$authorized('@@@@ Dynamic Display - Full Control') || config.readonly,
            scope: this,
            handler: this.onAddRecord
        }, {
            text: $lang("msg_dynamicdisplay_update"),
            iconCls: 'viz-icon-small-update',
            hidden: !$authorized('@@@@ Dynamic Display - Full Control') || config.readonly,
            scope: this,
            handler: this.onUpdateRecord
        }, {
            text: $lang("msg_dynamicdisplay_delete"),
            iconCls: 'viz-icon-small-delete',
            hidden: !$authorized('@@@@ Dynamic Display - Full Control') || config.readonly,
            scope: this,
            handler: this.onDeleteRecord
        }, {
            text: $lang("msg_dynamicdisplay_copy"),
            iconCls: 'viz-icon-small-copy',
            hidden: !$authorized('@@@@ Dynamic Display - Full Control') || config.readonly,
            scope: this,
            handler: this.onCopyRecord
        }, {
            text: $lang("msg_dynamicdisplay_view"),
            iconCls: 'viz-icon-small-display',
            // hidden : !$authorized('DynamicDisplay_read'),
            scope: this,
            handler: this.onDynamicDisplayDisplay
        }, {
            text: $lang("msg_dynamicdisplay_generatemodernui"),
            iconCls: 'viz-icon-small-modernui',
            // hidden : !$authorized('DynamicDisplay_read'),
            scope: this,
            handler: this.onDynamicDisplayGenerateModernUI
        }];

        var defaultConfig = {
            tpl: new Ext.XTemplate('<tpl for=".">',
            /**/
                            '<div class="thumb-wrap" id="{[Ext.util.Format.htmlEncode(values.KeyDynamicDisplay)]}" >',
            /**/
                            '<div class="thumb"><img src="{[Ext.util.Format.htmlEncode(values.Url)]}" ext:qtip="{[Ext.util.Format.htmlEncode(values.Name)]} <br> {[Ext.util.Format.htmlEncode(values.Description == null ? "" : values.Description)]}"></div>',
            /**/
                            '<span >{[Ext.util.Format.htmlEncode(values.ShortName)]}</span></div>',
            /**/
                            '</tpl>',
            /**/
                            '<div class="x-clear"></div>'),
            tbar: configMenuAndTbar,
            menu: configMenuAndTbar,
            formConfig: Viz.grid.DynamicDisplay.prototype.getFormConfig(),
            store: new Viz.store.DynamicDisplay(),
            prepareData: function (data) {
                // in case of copy dynamic display prepareData is called with a store with an empty KeyDynamicDisplay
                if (data.KeyDynamicDisplay != null && data.KeyDynamicDisplay != '') {

                    var url = Viz.Services.BuildUrlStream('Energy.svc', 'DynamicDisplay_GetStreamImage', {
                        Key: data.KeyDynamicDisplay,
                        width: 280,
                        height: 200,
                        displayBogusChart: true
                    });
                    data.Url = url;
                }
                data.ShortName = Ext.util.Format.ellipsis(data.Name, 38);
                return data;
            }
        };
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);
        Viz.dataview.DynamicDisplay.superclass.constructor.call(this, config);

        Viz.util.MessageBusMgr.subscribe("DynamicDisplayChange", this.reloadStore, this);
    },
    /**
    * Destroy
    */
    onDestroy: function () {
        Viz.util.MessageBusMgr.unsubscribe("DynamicDisplayChange", this.reloadStore, this);
        Viz.dataview.DynamicDisplay.superclass.onDestroy.apply(this, arguments);
    },

    onBeforeCopyRecord: function (record) {
        record.data.Name = $lang('msg_copyof') + ' ' + record.data.Name;
    },

    /**
    * Handler for displaying a dynamicdisplay.
    * @method onChartDisplay
    */
    onDynamicDisplayDisplay: function () {
        var record = this.getSelectedRecord();
        if (record) {
            var entity = record.json;
            Viz.grid.DynamicDisplay.openDynamicDisplayDisplay(entity, 800, 550);
        }
    },

    onDynamicDisplayGenerateModernUI: function () {
        this.el.mask();
        Viz.Services.EnergyWCF.DynamicDisplay_GenerateModernUI({
            success: function () {
                this.reloadStore();
                this.el.unmask();
            },
            scope: this
        });
    }
});
Ext.reg("vizDataViewDynamicDisplay", Viz.dataview.DynamicDisplay);

﻿/**
 * @class Viz.dataview.Palette
 * <p>
 * A dataview exposing the business entity Palette
 * </p>
 * @extends Viz.Panel
 */

Viz.dataview.Palette = Ext.extend(Viz.dataview.DataViewPanel, {
            /**
             * Ctor.
             * @param {Object} config
             */
            constructor      : function(config) {

                config = config || {};

                var configMenuAndTbar = [{
                            text    : $lang("msg_palette_add"),
                            iconCls : 'viz-icon-small-add',
                            hidden  : !$authorized('@@@@ Palette - Full Control') || config.readonly,
                            scope   : this,
                            handler : this.onAddRecord
                        }, {
                            text    : $lang("msg_palette_update"),
                            iconCls : 'viz-icon-small-update',
                            hidden  : !$authorized('@@@@ Palette - Full Control') || config.readonly,
                            scope   : this,
                            handler : this.onUpdateRecord
                        }, {
                            text    : $lang("msg_palette_delete"),
                            iconCls : 'viz-icon-small-delete',
                            hidden  : !$authorized('@@@@ Palette - Full Control') || config.readonly,
                            scope   : this,
                            handler : this.onDeleteRecord
                        }, {
                            text    : $lang("msg_palette_copy"),
                            iconCls : 'viz-icon-small-copy',
                            hidden  : !$authorized('@@@@ Palette - Full Control'),
                            scope   : this,
                            handler : this.onCopyRecord
                        }];
                var defaultConfig = {
                    tpl         : new Ext.XTemplate('<tpl for=".">',
                            /**/
                            '<div class="thumb-wrap" id="{[Ext.util.Format.htmlEncode(values.KeyPalette)]}" >',
                            /**/
                            '<div class="thumb" style="width:200px"><img style="width:200px;height:200px"  src="{[Ext.util.Format.htmlEncode(values.Url)]}" ext:qtip="{[Ext.util.Format.htmlEncode(values.Label)]}"></div>',
                            /**/
                            '<span >{[Ext.util.Format.htmlEncode(values.ShortLabel)]}</span></div>',
                            /**/
                            '</tpl>',
                            /**/
                            '<div class="x-clear"></div>'),
                    tbar        : configMenuAndTbar,
                    menu        : configMenuAndTbar,
                    formConfig  : Viz.grid.Palette.prototype.getFormConfig(),
                    store       : new Viz.store.Palette(),
                    prepareData : function(data) {
                        var url = Viz.Services.BuildUrlStream('Energy.svc', 'Palette_GetStreamImage', {
                                    Key    : data.KeyPalette,
                                    width  : 200,
                                    height : 10
                                });
                        data.Url = url;
                        data.ShortLabel = Ext.util.Format.ellipsis(data.Label, 38);
                        return data;
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.dataview.Palette.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("PaletteChange", this.reloadStore, this);
            },
            /**
             * Destroy
             */
            onDestroy        : function() {
                Viz.util.MessageBusMgr.unsubscribe("PaletteChange", this.reloadStore, this);
                Viz.dataview.Palette.superclass.onDestroy.apply(this, arguments);
            },
            /**
             * Handler for displaying a palette.
             * @method onChartDisplay
             */
            onPaletteDisplay : function() {
                var record = this.getSelectedRecord();
                if (record) {
                    var entity = record.json;
                    Viz.grid.Palette.openPaletteDisplay(entity, 800, 550);
                }
            },

            /**
             * @private Handler for copying a palette.
             */
            onCopyRecord     : function() {
                var record = this.getSelectedRecord();
                if (record) {
                    this.el.mask();
                    Viz.Services.EnergyWCF.Palette_Copy({
                                keys    : [record.data.KeyPalette],
                                success : function(data) {
                                    this.el.unmask();
                                    this.reloadStore();
                                },
                                scope   : this
                            });
                }
            }
        });
Ext.reg("vizDataViewPalette", Viz.dataview.Palette);

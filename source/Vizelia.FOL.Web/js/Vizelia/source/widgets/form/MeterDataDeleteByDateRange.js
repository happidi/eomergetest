﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.MeterDataDeleteByDateRange
 * @extends Ext.FormPanel.
 */
Viz.form.MeterDataDeleteByDateRange = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load               : 'local',
                    serviceHandlerCreate    : Viz.Services.EnergyWCF.MeterData_DeleteByDateRangeBegin,
                    serviceHandlerUpdate    : Viz.Services.EnergyWCF.MeterData_DeleteByDateRangeBegin,
                    items                   : this.configFormGeneral(config),
                    isLongRunningSubmit     : true,
                    longRunningSubmitConfig : {
                        allowCancel            : true,
                        showProgressPercentage : true,
                        modal                  : false
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.MeterDataGenerate.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                var now = new Date();
                return [{
                            name   : 'KeyMeter',
                            hidden : true,
                            value  : config.entity.KeyMeter
                        }, {
                            fieldLabel : $lang('msg_meterdata_startdate'),
                            name       : 'StartDate',
                            hideTime   : false,
                            xtype      : 'vizDateTime',
                            allowBlank : false,
                            hasSwitch  : false,
                            value      : new Date(now.getFullYear(), 0, 1, 0, 0, 0)
                        }, {
                            fieldLabel : $lang('msg_meterdata_enddate'),
                            name       : 'EndDate',
                            hideTime   : false,
                            xtype      : 'vizDateTime',
                            allowBlank : false,
                            hasSwitch  : false,
                            value      : new Date(now.getFullYear(), 11, 31, 23, 59, 59)
                        }];
            },

            saveItem          : function() {
                Ext.MessageBox.confirm($lang('msg_meterdata_deletebydaterange'), $lang('msg_meterdata_deletebydaterange_confirm'), function(button) {
                            if (button == 'yes') {
                                Viz.form.MeterDataGenerate.superclass.saveItem.apply(this,arguments);
                            }
                        }, this);
            }
        });

Ext.reg('vizFormMeterDataDeleteByDateRange', Viz.form.MeterDataDeleteByDateRange);
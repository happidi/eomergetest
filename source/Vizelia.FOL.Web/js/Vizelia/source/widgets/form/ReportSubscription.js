﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ReportSubscription
 * @extends Ext.FormPanel.
 */
Viz.form.ReportSubscription = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                this._scheduleFieldId = Ext.id();

                var defaultConfig = {
                    mode_load            : 'remote',
                    applyEnabled         : false,
                    serviceHandler       : Viz.Services.ReportingWCF.ReportSubscription_FormLoad,
                    serviceHandlerCreate : Viz.Services.ReportingWCF.ReportSubscription_FormCreate,
                    serviceHandlerUpdate : Viz.Services.ReportingWCF.ReportSubscription_FormUpdate,
                    items                : this.configFormGeneral(config),
                    listeners            : {
                        afterrender : {
                            scope : this,
                            fn    : function() {
                                var scheduleField = Ext.getCmp(this._scheduleFieldId);
                                scheduleField.validate();
                            }
                        }
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.ReportSubscription.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                var me = this;

                return [{
                            name   : 'KeyReportSubscription',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            maxLength  : 100,
                            allowBlank : false
                        }, {
                            fieldLabel      : $lang('msg_file_format'),
                            name            : 'FileFormat',
                            allowBlank      : false,
                            xtype           : 'vizComboEnum',
                            enumTypeName    : 'Vizelia.FOL.BusinessEntities.ExportType,Vizelia.FOL.Common',
                            sortByValues    : true,
                            displayIcon     : true,
                            valuesToInclude : ['Html', 'CSV', 'XML', 'Pdf', 'ImageTIFF', 'Word', 'ExcelBasic']
                        }, {
                            xtype : 'compositefield',
                            items : [{
                                        xtype      : 'textfield',
                                        id         : this._scheduleFieldId,
                                        fieldLabel : $lang('msg_schedule'),
                                        value      : $lang('msg_schedule_is_not_set'),
                                        msgTarget  : 'qtip',
                                        validator  : function(v) {
                                            // Is a schedule set?
                                            if (me.entity && me.entity.Schedule) {
                                                return true;
                                            }
                                            else {
                                                return $lang('error_schedule_must_be_set');
                                            }
                                        },
                                        readOnly   : true,
                                        flex       : 1
                                    }, {
                                        xtype   : 'button',
                                        text    : $lang('msg_schedule_set'),
                                        handler : function() {
                                            this.entity = this.entity || {};
                                            this.entity.Schedule = this.entity.Schedule || {};
                                            Ext.applyIf(this.entity.Schedule, {
                                                        // Default to hourly
                                                        RecurrencePattern : {
                                                            Frequency : 3,
                                                            Interval  : "1"
                                                        }
                                                    });

                                            Viz.openFormCrud({
                                                        common : {
                                                            title            : $lang("msg_schedule_update"),
                                                            iconCls          : 'viz-icon-small-schedule-update',
                                                            formModal        : true,
                                                            applyEnabled     : false, // the Viz function doesn't copy this to the form. need to fix Viz function.
                                                            width            : 740,
                                                            height           : 430,
                                                            xtype            : 'vizFormReportSubscriptionSchedule',
                                                            messageSaveStart : 'ReportSubscriptionScheduleChangeStart',
                                                            messageSaveEnd   : 'ReportSubscriptionScheduleChange',
                                                            listeners        : {
                                                                // change the standard behavior of the form in order to avoid server side save call.
                                                                beforesave : function(form, item, extraParam) {
                                                                    // prepare the entity as expected.
                                                                    var valid = form.onBeforeSave(form, item, extraParam);
                                                                    if (valid) {
                                                                        this.isDirty = true; // to make sure the form knows it has changed.
                                                                        this.entity.Schedule = item;
                                                                        Ext.getCmp(this._scheduleFieldId).setValue($lang('msg_schedule_is_set'));
                                                                        form.getParentWindow().close();
                                                                    }
                                                                    return false;
                                                                },
                                                                scope      : me
                                                            }
                                                        }
                                                    }, 'create', this.entity.Schedule);
                                        }
                                    }]
                        }, {
                            xtype      : 'textfield',
                            fieldLabel : $lang('msg_mail_to'),
                            name       : 'Recipients',
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description'
                        }];
            }
        });

Ext.reg('vizFormReportSubscription', Viz.form.ReportSubscription);
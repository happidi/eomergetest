﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ChartScheduler
 * @extends Ext.FormPanel.
 */
Viz.form.ChartScheduler = Ext.extend(Viz.form.CalendarDefinition, {

            eventHasDuration                : false,
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                     : function(config) {

                var Id = config.entity ? config.entity.KeyChartScheduler : null;

                this.storeChart = new Viz.store.ChartSchedulerChart({
                            KeyChartScheduler : Id
                        });
                this.storePortalWindow = new Viz.store.ChartSchedulerPortalWindow({
                            KeyChartScheduler : Id
                        });
                Ext.apply(config, {
                            showMinutely             : false,
                            showHourly               : true,
                            showDaily                : true,
                            showWeekly               : true,
                            showWeeklyRecurs         : true,
                            showMonthly              : true,
                            showYearly               : true,
                            showYearlyRecurs         : true,
                            showEndDate              : true,
                            showExplicitMonths       : false,
                            showNumberOccurrence     : true,
                            showHasRecurrencePattern : true,
                            showTitle                : true,
                            Category                 : 'ChartScheduler'
                        });
                this.generateIds();
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.ChartScheduler_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.ChartScheduler_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.ChartScheduler_FormUpdate,
                    hasValidationToolbar : true,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title   : $lang('msg_general'),
                                    iconCls : 'viz-icon-small-calendarevent',
                                    items   : this.configFormGeneral(config)
                                }, {
                                    title   : $lang('msg_calendar_recurrence'),
                                    iconCls : 'viz-icon-small-calendarevent-recurrence',
                                    items   : [this.configFormRecurrence(config)]
                                }, {
                                    title   : $lang('msg_chartscheduler'),
                                    iconCls : 'viz-icon-small-chartscheduler',
                                    items   : this.configFormGeneralChartScheduler(config)
                                }, {
                                    title      : $lang('msg_chart'),
                                    iconCls    : 'viz-icon-small-chart',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [Ext.apply(this.configGridDetail(this.configDetailGridChart()), { filterRemote : false })]
                                }, {
                                    title      : $lang('msg_portalwindow'),
                                    iconCls    : 'viz-icon-small-portalwindow',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridDetail(this.configDetailGridPortalWindow())]
                                }]
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.ChartScheduler.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneralChartScheduler : function(config) {
                return [{
                            name   : 'KeyChartScheduler',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_alarmdefinition_enabled'),
                            name       : 'Enabled',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel      : $lang('msg_chartscheduler_exporttype'),
                            name            : 'ExportType',
                            hiddenName      : 'ExportType',
                            allowBlank      : false,
                            xtype           : 'vizComboEnum',
                            enumTypeName    : 'Vizelia.FOL.BusinessEntities.ExportType,Vizelia.FOL.Common',
                            sortByValues    : true,
                            displayIcon     : true,
                            valuesToInclude : ['Pdf', 'Excel2007', 'ImagePNG']
                        }, {
                            xtype      : 'textarea',
                            allowBlank : false,
                            fieldLabel : $lang('msg_chartscheduler_emails'),
                            name       : 'Emails'
                        }, {
                            fieldLabel : $lang('msg_chartscheduler_subject'),
                            name       : 'Subject',
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            allowBlank : true,
                            fieldLabel : $lang('msg_chartscheduler_body'),
                            name       : 'Body',
                            height     : 140
                        }, {
                            fieldLabel : $lang('msg_chartscheduler_culture'),
                            name       : 'KeyLocalizationCulture',
                            hiddenName : 'KeyLocalizationCulture',
                            autoLoad   : config.mode == 'create',
                            xtype      : 'vizComboLocalizationCulture'
                        }];
            },

            /**
             * The configuration object for the detail grid Chart.
             * @return {Object}
             */
            configDetailGridChart           : function() {
                return {
                    name                : 'Chart',
                    windowTitle         : $lang('msg_chart'),
                    windowWidth         : 600,
                    windowHeight        : 400,
                    windowIconCls       : 'viz-icon-small-chart',
                    xtypeGridDetail     : 'vizGridChart',
                    columnsGridDetail   : Viz.Configuration.Columns.ChartSimple,
                    buttonAddTitle      : $lang('msg_chart_add'),
                    buttonAddIconCls    : 'viz-icon-small-add',
                    buttonDeleteTitle   : $lang('msg_chart_delete'),
                    buttonDeleteIconCls : 'viz-icon-small-delete',
                    storeDetail         : this.storeChart,
                    keyColumn           : 'KeyChart',
                    gridLoadMask        : false
                };
            },

            /**
             * The configuration object for the detail grid Chart.
             * @return {Object}
             */
            configDetailGridPortalWindow    : function() {
                return {
                    name                : 'PortalWindow',
                    windowTitle         : $lang('msg_portalwindow'),
                    windowWidth         : 600,
                    windowHeight        : 400,
                    windowIconCls       : 'viz-icon-small-portalwindow',
                    xtypeGridDetail     : 'vizGridPortalWindow',
                    buttonAddTitle      : $lang('msg_portalwindow_add'),
                    buttonAddIconCls    : 'viz-icon-small-add',
                    buttonDeleteTitle   : $lang('msg_portalwindow_delete'),
                    buttonDeleteIconCls : 'viz-icon-small-delete',
                    storeDetail         : this.storePortalWindow,
                    keyColumn           : 'KeyPortalWindow',
                    gridLoadMask        : false
                };
            },
            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                    : function(form, item, extraParams) {
                item.TimeZoneOffset = item.StartDate.getWCFGMTOffset();
                if (this.storeChart.hasLoaded)
                    this.storeChart.save();

                if (this.storePortalWindow.hasLoaded)
                    this.storePortalWindow.save();

                Ext.apply(extraParams, {
                            charts        : this.storeChart.crudStore,
                            portalwindows : this.storePortalWindow.crudStore
                        });

                return Viz.form.ChartScheduler.superclass.onBeforeSave.apply(this, arguments);
            }
        });

Ext.reg('vizFormChartScheduler', Viz.form.ChartScheduler);
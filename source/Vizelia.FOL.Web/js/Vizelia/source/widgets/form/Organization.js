﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Organization
 * @extends Ext.FormPanel.
 */
Viz.form.Organization = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.CoreWCF.Organization_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.Organization_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.Organization_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Organization.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            fieldLabel : $lang('msg_id'),
                            name       : 'Id',
                            allowBlank : false,
                            readOnly   : config.mode == "update" ? true : false
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description'
                        }];
            }
        });

Ext.reg('vizFormOrganization', Viz.form.Organization);
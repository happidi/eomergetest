﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ChartAxis
 * @extends Ext.FormPanel.
 */
Viz.form.ChartAxis = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Chart} entity The chart entity the axis belong to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.ChartAxis_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.ChartAxis_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.ChartAxis_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.ChartAxis.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyChartAxis',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name'
                        }, {
                            fieldLabel : $lang('msg_id'),
                            name       : 'LocalId',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_chartaxis_min'),
                            xtype      : 'vizSpinnerField',
                            name       : 'Min',
                            allowBlank : true
                        }, {
                            fieldLabel : $lang('msg_chartaxis_max'),
                            xtype      : 'vizSpinnerField',
                            name       : 'Max',
                            allowBlank : true
                        }, {
                            fieldLabel : $lang('msg_chartaxis_fullstacked'),
                            name       : 'FullStacked',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }];
            }
        });

Ext.reg('vizFormChartAxis', Viz.form.ChartAxis);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Mail
 * @extends Ext.FormPanel.
 */
Viz.form.Mail = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                            : function(config) {
                this._gridPriority = Ext.id();
                this._panelClassification = Ext.id();
                this._gridClassification = Ext.id();
                this._gridWorkflowActionElement = Ext.id();
                var Id = config.entity ? config.entity.KeyMail : null;

                this.storePriority = new Viz.store.MailPriority({
                            KeyMail : Id
                        });

                this.storeClassificationItem = new Viz.store.MailClassificationItem({
                            KeyMail : Id
                        });

                this.storeMailWorkflowActionElement = new Viz.store.MailWorkflowActionElement({
                            autoLoad      : true,
                            serviceParams : {
                                KeyMail : Id
                            },
                            listeners     : {
                                scope      : this,
                                beforeload : function(store, options) {

                                }
                            }

                        });

                this.storeLocation = new Viz.store.MailLocation({
                            KeyMail : Id
                        });
                config = config || {};
                config.texareawithintelissenseIdsArray = [Ext.id(), Ext.id(), Ext.id(), Ext.id(), Ext.id(), Ext.id()];
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.ServiceDeskWCF.Mail_FormLoad,
                    serviceHandlerCreate : Viz.Services.ServiceDeskWCF.Mail_FormCreate,
                    serviceHandlerUpdate : Viz.Services.ServiceDeskWCF.Mail_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        listeners      : {
                            scope           : this,
                            beforetabchange : function(tp, nTab, cTab) {
                                alert('beforetabchange');
                            }
                        },

                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)

                                }, {
                                    title      : $lang('msg_priority'),
                                    iconCls    : 'viz-icon-small-priority',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridDetail(this.configDetailGridPriority())],
                                    hidden     : config.entity != null && config.entity.ClassificationItemLocalIdPath != "Root / MAIL / MAIL_ServiceDesk"
                                }, {
                                    id         : this._panelClassification,
                                    title      : $lang('msg_classificationitem'),
                                    iconCls    : 'viz-icon-small-work',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridDetail(this.configDetailGridClassificationItem())],
                                    hidden     : config.entity != null && config.entity.ClassificationItemLocalIdPath != "Root / MAIL / MAIL_ServiceDesk"
                                }, {
                                    title      : $lang('msg_location_path'),
                                    iconCls    : 'viz-icon-small-site',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridDetail(this.configDetailGridLocation())],
                                    hidden     : config.entity != null && config.entity.ClassificationItemLocalIdPath != "Root / MAIL / MAIL_ServiceDesk"
                                }, {
                                    title      : $lang('msg_workflow_event'),
                                    iconCls    : 'viz-icon-small-event',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridDetail(this.configDetailGridWorkflowActionElements())],
                                    listeners  : {
                                        scope      : this,
                                        beforeshow : function(t) {
                                            var crec = Ext.getCmp(this._gridClassification).getSelectionModel().getSelected();
                                            /*
                                             * t.items.items[0].store.reload({ params : { KeyClassificationItem : crec.get('KeyClassificationItem'), KeyMail : Id, assemblyName : 'Vizelia.FOL.WF.Workflows.ActionRequestStandardStateMachine, Vizelia.FOL.WF.Workflows, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null' } });
                                             */
                                        }

                                    },
                                    hidden     : config.entity != null && config.entity.ClassificationItemLocalIdPath != "Root / MAIL / MAIL_ServiceDesk"
                                }]
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.Mail.superclass.constructor.call(this, config);

                this.on("beforeopendetail", this.onBeforeOpenDetail, this);

                // Inits the tree model once for all the Mail fields
                Viz.plugins.IntelliSense.synchronizeFields(config.texareawithintelissenseIdsArray);

            },

            /**
             * @private The configuration object for detail grid AzMan Role.
             * @return {Object}
             */
            configDetailGridClassificationItem     : function() {
                return {
                    id                  : this._gridClassification,
                    name                : 'ClassificationItem',
                    windowTitle         : $lang('msg_classificationitem'),
                    windowWidth         : 600,
                    windowHeight        : 400,
                    windowIconCls       : 'viz-icon-small-work',
                    xtypeGridDetail     : 'vizGrid',
                    buttonAddTitle      : $lang('msg_classificationitem_add'),
                    buttonAddIconCls    : 'viz-icon-small-add',
                    buttonDeleteTitle   : $lang('msg_classificationitem_delete'),
                    buttonDeleteIconCls : 'viz-icon-small-delete',
                    storeDetail         : this.storeClassificationItem,
                    keyColumn           : 'KeyClassificationItem',
                    columnsGridDetail   : [{
                                header     : $lang('msg_title'),
                                dataIndex  : 'LongPath',
                                sortable   : true,
                                filterable : true,
                                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                                    cell.css = 'viz-icon-cell ' + record.get('IconCls');
                                    return data;
                                }
                            }],
                    plugins             : [new Viz.plugins.GridExport({
                                listeners : {
                                    beforeExport : function(plugin) {
                                        plugin.title = $lang('msg_classificationitem') + ' : ' + this.entity.Id
                                    },
                                    scope        : this
                                }
                            })]
                };
            },

            /**
             * @private The configuration object for detail grid associated Locations.
             * @return {Object}
             */
            configDetailGridLocation               : function() {
                return {
                    id                  : this._gridLocation,
                    name                : 'Location',
                    windowTitle         : $lang('msg_location'),
                    windowWidth         : 600,
                    windowHeight        : 400,
                    windowIconCls       : 'viz-icon-small-site',
                    xtypeGridDetail     : 'vizGrid',
                    buttonAddTitle      : $lang('msg_add'),
                    buttonAddIconCls    : 'viz-icon-small-add',
                    buttonDeleteTitle   : $lang('msg_delete'),
                    buttonDeleteIconCls : 'viz-icon-small-delete',
                    storeDetail         : this.storeLocation,
                    keyColumn           : 'KeyLocation',
                    columnsGridDetail   : [{
                                header     : $lang('msg_location_path'),
                                dataIndex  : 'LongPath',
                                sortable   : true,
                                filterable : true,
                                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                                    cell.css = 'viz-icon-cell ' + record.get('IconCls');
                                    return data;
                                }
                            }],
                    plugins             : [new Viz.plugins.GridExport({
                                listeners : {
                                    beforeExport : function(plugin) {
                                        plugin.title = $lang('msg_location') + ' : ' + this.entity.Id
                                    },
                                    scope        : this
                                }
                            })]
                };
            },

            /**
             * Handler before opening a detail window that gives a change to override the open window configuration.
             * @param {Viz.form.FormPanel} form
             * @param {Object} config
             * @param {String} id
             */
            onBeforeOpenDetail                     : function(form, config, id) {
                switch (config.name) {
                    case 'ClassificationItem' :
                        this.addClassificationItem();
                        return false;
                        break;
                    case 'Location' :
                        this.addLocation();
                        return false;
                        break;
                    case 'WorkflowActionElement' :
                        var clsKeys = [];
                        this.storeClassificationItem.each(function(r) {
                                    clsKeys.push(r.get('KeyClassificationItem')); // this.getKey(r)
                                }, this.storeClassificationItem.data);

                        config.serviceParams = {
                            keys : clsKeys
                        };
                        return true;
                        break;

                    default :
                        return true;
                }
            },
            /**
             * Adds a list of classification items
             */
            addClassificationItem                  : function() {
                var componentId = Ext.id();
                var win = new Ext.Window({
                            title   : $lang('msg_classificationitem'),
                            width   : 450,
                            height  : 400,
                            modal   : true,
                            iconCls : 'viz-icon-small-work',
                            layout  : 'fit',
                            items   : [{
                                        xtype           : 'vizTreeClassificationItem',
                                        border          : false,
                                        id              : componentId,
                                        displayCheckbox : true,
                                        header          : false,
                                        entityKey       : 'KeyClassificationItem',
                                        rootVisible     : false,
                                        serviceParams   : {
                                            flgFilter           : false,
                                            excludePset         : true,
                                            excludeRelationship : true
                                        },
                                        root            : {
                                            id      : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("work"),
                                            iconCls : 'viz-icon-small-work',
                                            text    : ''
                                        }
                                    }],
                            buttons : [{
                                        text    : $lang('msg_add'),
                                        iconCls : 'viz-icon-small-add',
                                        handler : function() {
                                            /**
                                             * @type {Ext.tree.TreePanel} tree
                                             */
                                            var comp = Ext.getCmp(componentId);
                                            var nodes = comp.getChecked();
                                            Ext.each(nodes, function(node) {
                                                        var entity = node.attributes.entity;
                                                        if (entity && this.storeClassificationItem.find("KeyClassificationItem", entity.KeyClassificationItem) == -1) {
                                                            entity.LongPath = entity.Title;
                                                            this.isDirty = true;
                                                            this.storeClassificationItem.add(new this.storeClassificationItem.recordType(entity));
                                                        }
                                                    }, this);

                                            win.close();
                                        },
                                        scope   : this
                                    }]
                        });
                win.show();

            },
            /**
             * Adds a list of classification items
             */
            addLocation                            : function() {
                var componentId = Ext.id();
                var win = new Ext.Window({
                            title   : $lang('msg_location'),
                            width   : 450,
                            height  : 400,
                            modal   : true,
                            iconCls : 'viz-icon-small-site',
                            layout  : 'fit',
                            items   : [{
                                        xtype           : 'vizTreeSpatial',
                                        border          : false,
                                        id              : componentId,
                                        displayCheckbox : true,
                                        header          : false,
                                        entityKey       : 'KeyLocation',
                                        rootVisible     : false,
                                        serviceParams   : {
                                            flgFilter           : false,
                                            excludePset         : true,
                                            excludeRelationship : true
                                        },
                                        root            : {
                                            id      : ' ',
                                            iconCls : 'viz-icon-small-site',
                                            checked : undefined,
                                            text    : $lang("msg_tree_spatial_root")
                                        }
                                    }],
                            buttons : [{
                                        text    : $lang('msg_add'),
                                        iconCls : 'viz-icon-small-add',
                                        handler : function() {
                                            /**
                                             * @type {Ext.tree.TreePanel} tree
                                             */
                                            var comp = Ext.getCmp(componentId);
                                            var nodes = comp.getChecked();
                                            Ext.each(nodes, function(node) {
                                                        var entity = node.attributes.entity;
                                                        if (entity.KeySite == undefined && entity.KeyBuilding === undefined)
                                                            return;
                                                        if (entity && this.storeLocation.find("KeyLocation", entity.KeyLocation) == -1) {
                                                            entity.KeyLocation = (entity.KeySite != undefined ? entity.KeySite : entity.KeyBuilding);
                                                            entity.IconCls = node.attributes.iconCls;
                                                            entity.LongPath = entity.Name;
                                                            this.isDirty = true;
                                                            this.storeLocation.add(new this.storeLocation.recordType(entity));
                                                        }
                                                    }, this);

                                            win.close();
                                        },
                                        scope   : this
                                    }]
                        });
                win.show();

            },
            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                      : function(config) {
                return [{
                            xtype      : 'textfield',
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name'
                        }, {
                            fieldLabel           : $lang('msg_mail_classification'),
                            xtype                : 'viztreecomboClassificationItemMail',
                            name                 : 'KeyClassificationItem',
                            originalDisplayValue : 'ClassificationItemLongPath',
                            allowBlank           : false,
                            listeners            : {
                                changevalue : function(field, value, node) {
                                    if (node != null && node.attributes != null && node.attributes.entity != null && node.attributes.entity.LocalIdPath != null) {
                                        // Change the tree model once for all the Mail fields
                                        var serviceParams = {
                                            modelTypeNames                : [],
                                            classificationItemLocalIdPath : node.attributes.entity.LocalIdPath
                                        }

                                        Viz.plugins.IntelliSense.synchronizeFields(this.texareawithintelissenseIdsArray, serviceParams);

                                        var hidden = node.attributes.entity.LocalIdPath != "Root / MAIL / MAIL_ServiceDesk"
                                        var formTabPanel = this.getTabContainer();
                                        Ext.each(formTabPanel.items.items, function(tabItem) {
                                                    if (tabItem.title != "General")
                                                        if (hidden) {
                                                            formTabPanel.hideTabStripItem(tabItem.id);
                                                        }
                                                        else {
                                                            formTabPanel.unhideTabStripItem(tabItem.id);
                                                        }
                                                });
                                    }
                                },
                                scope       : this
                            }
                        }, {
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'System.Net.Mail.MailPriority, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089',
                            // xtype : 'vizSpinnerField',
                            // anchor : false,
                            fieldLabel   : $lang('msg_priority'),
                            name         : 'Priority',
                            // width : 48,
                            // minValue : 0,
                            // maxValue : 10,
                            // allowDecimals : false,
                            // incrementValue : 1,
                            // accelerate : true,
                            allowBlank   : false,
                            value        : 0

                        }, {
                            xtype      : 'vizcheckbox',
                            fieldLabel : $lang('msg_isactive'),
                            name       : 'IsActive',
                            value      : true
                        }, {
                            xtype      : 'textfield',
                            id         : config.texareawithintelissenseIdsArray[0],
                            fieldLabel : $lang('msg_mail_from'),
                            name       : 'From',
                            height     : 25,
                            plugins    : [new Viz.plugins.IntelliSense({
                                        serviceHandler : Viz.Services.CoreWCF.DataModel_GetTree,
                                        serviceParams  : {
                                            modelTypeNames                : [],
                                            classificationItemLocalIdPath : config.entity.ClassificationItemLocalIdPath
                                        }
                                    })]
                        }, {
                            xtype      : 'textfield',
                            id         : config.texareawithintelissenseIdsArray[1],
                            fieldLabel : $lang('msg_mail_to'),
                            name       : 'To',
                            height     : 25,
                            plugins    : [new Viz.plugins.IntelliSense({
                                        serviceHandler : Viz.Services.CoreWCF.DataModel_GetTree,
                                        serviceParams  : {
                                            modelTypeNames                : [],
                                            classificationItemLocalIdPath : config.entity.ClassificationItemLocalIdPath
                                        }
                                    })]
                        }, {
                            xtype      : 'textfield',
                            id         : config.texareawithintelissenseIdsArray[2],
                            fieldLabel : $lang('msg_mail_cc'),
                            name       : 'CC',
                            height     : 25,
                            plugins    : [new Viz.plugins.IntelliSense({
                                        serviceHandler : Viz.Services.CoreWCF.DataModel_GetTree,
                                        serviceParams  : {
                                            modelTypeNames                : [],
                                            classificationItemLocalIdPath : config.entity.ClassificationItemLocalIdPath
                                        }
                                    })]
                        }, {
                            xtype      : 'textfield',
                            id         : config.texareawithintelissenseIdsArray[3],
                            fieldLabel : $lang('msg_mail_bcc'),
                            name       : 'Bcc',
                            height     : 25,
                            plugins    : [new Viz.plugins.IntelliSense({
                                        serviceHandler : Viz.Services.CoreWCF.DataModel_GetTree,
                                        serviceParams  : {
                                            modelTypeNames                : [],
                                            classificationItemLocalIdPath : config.entity.ClassificationItemLocalIdPath
                                        }
                                    })]
                        }, {
                            xtype                         : 'textfield',
                            id                            : config.texareawithintelissenseIdsArray[4],
                            modelTypeNames                : [],
                            classificationItemLocalIdPath : config.entity.ClassificationItemLocalIdPath,
                            fieldLabel                    : $lang('msg_mail_subject'),
                            name                          : 'Subject',
                            height                        : 25,
                            plugins                       : [new Viz.plugins.IntelliSense({
                                        serviceHandler : Viz.Services.CoreWCF.DataModel_GetTree,
                                        serviceParams  : {
                                            modelTypeNames                : [],
                                            classificationItemLocalIdPath : config.entity.ClassificationItemLocalIdPath
                                        }
                                    })]
                        }, {
                            xtype      : 'textarea',
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description',
                            height     : 25
                        }, {
                            xtype      : 'vizcheckbox',
                            fieldLabel : $lang('msg_mail_ishtml'),
                            name       : 'IsHtml'
                        }, {
                            xtype      : 'htmleditor',
                            id         : config.texareawithintelissenseIdsArray[5],
                            fieldLabel : $lang('msg_mail_body'),
                            name       : 'Body',
                            height     : 180,
                            isHtml     : true,
                            plugins    : [new Viz.plugins.IntelliSense({
                                        serviceHandler : Viz.Services.CoreWCF.DataModel_GetTree,
                                        serviceParams  : {
                                            modelTypeNames                : [],
                                            classificationItemLocalIdPath : config.entity.ClassificationItemLocalIdPath
                                        }
                                    })]
                        }]
            },

            /**
             * The configuration object for the detail grid Priority.
             * @return {Object}
             */
            configDetailGridPriority               : function() {
                return {
                    name                : 'Priority',
                    windowTitle         : $lang('msg_priority'),
                    windowWidth         : 1000,
                    windowHeight        : 400,
                    windowIconCls       : 'viz-icon-small-priority',
                    xtypeGridDetail     : 'vizGridPriority',
                    buttonAddTitle      : $lang('msg_priority_add'),
                    buttonAddIconCls    : 'viz-icon-small-priority-add',
                    buttonDeleteTitle   : $lang('msg_priority_delete'),
                    buttonDeleteIconCls : 'viz-icon-small-priority-delete',
                    storeDetail         : this.storePriority,
                    keyColumn           : 'KeyPriority'
                }
            },

            /**
             * The configuration object for the detail grid Workflow events.
             * @return {Object}
             */
            configDetailGridWorkflowActionElements : function() {
                return {
                    id                  : this._gridWorkflowActionElement,
                    name                : 'WorkflowActionElement',
                    windowTitle         : $lang('msg_workflow_event'),
                    windowWidth         : 1000,
                    windowHeight        : 400,
                    windowIconCls       : 'viz-icon-small-event',
                    xtypeGridDetail     : 'vizGridWorkflowActionElement',
                    buttonAddTitle      : $lang('msg_add'),
                    buttonAddIconCls    : 'viz-icon-small-add',
                    buttonDeleteTitle   : $lang('msg_delete'),
                    buttonDeleteIconCls : 'viz-icon-small-delete',
                    storeDetail         : this.storeMailWorkflowActionElement,
                    keyColumn           : 'MsgCode'
                    /*
                     * , columnsGridDetail : [{ header : $lang('msg_title'), dataIndex : 'WorkflowActionElementName', sortable : true, filterable : false }, { header : $lang('msg_workflow'), dataIndex : 'WorkflowAssemblyDefinitionName', sortable : true, filterable : false }]
                     */
                }
            },
            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                           : function(form, item, extraParams) {
                if (this.storePriority.hasLoaded)
                    this.storePriority.save();
                if (this.storeClassificationItem.hasLoaded)
                    this.storeClassificationItem.save();
                if (this.storeMailWorkflowActionElement.hasLoaded)
                    this.storeMailWorkflowActionElement.save();
                if (this.storeLocation.hasLoaded)
                    this.storeLocation.save();
                Ext.apply(extraParams, {
                            priorities             : this.storePriority.crudStore,
                            classifications        : this.storeClassificationItem.crudStore,
                            workflowActionElements : this.storeMailWorkflowActionElement.crudStore,
                            locations              : this.storeLocation.crudStore
                        });
                return true;
            }
        });
Ext.reg('vizFormMail', Viz.form.Mail);

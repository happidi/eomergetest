﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.FilterSpatialPset
 * @extends Ext.FormPanel.
 */
Viz.form.FilterSpatialPset = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.FilterSpatialPset} entity The FilterSpatialPset entity.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                this._psetId = Ext.id();
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.FilterSpatialPset_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.FilterSpatialPset_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.FilterSpatialPset_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.FilterSpatialPset.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyFilterSpatialPset',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_id'),
                            name       : 'LocalId',
                            allowBlank : false
                        }, {
                            fieldLabel           : $lang('msg_pset'),
                            xtype                : 'viztreecomboPset',
                            name                 : 'PsetValueTree',
                            allowBlank           : false,
                            id                   : this._psetId,
                            originalDisplayValue : 'PropertySingleValueLongPath',
                            validator            : Ext.form.VTypes.spatialFilterPsetValidator,
                            listeners            : {
                                changevalue : function() {
                                    // we need to defer because the validator is called also with a defer
                                    this.onChangeValuePset.defer(500, this, arguments);
                                },
                                scope       : this
                            }
                        }, {
                            fieldLabel   : $lang('msg_filterspatialpset_condition'),
                            name         : 'Condition',
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.FilterCondition,Vizelia.FOL.Common',
                            allowBlank   : false,
                            listeners    : {
                                select : function() {
                                    // we need to defer because the validator is called also with a defer
                                    this.onChangeValuePset.defer(500, this, arguments);
                                },
                                scope  : this
                            }
                        }, {
                            fieldLabel       : $lang('msg_filterspatialpset_order'),
                            name             : 'Order',
                            minValue         : 1,
                            decimalPrecision : 0,
                            allowBlank       : false,
                            value            : config.entity ? config.entity.Order || 1 : 1
                        }, {
                            fieldLabel   : $lang('msg_filterspatialpset_operator'),
                            name         : 'Operator',
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.LogicalOperator,Vizelia.FOL.Common',
                            allowBlank   : false,
                            value        : 0
                        }];
            },

            /**
             * Handler for updating fiels based on the form values.
             * @param {Viz.form.FormPanel} the form panel
             * @param {Object} values
             */
            onAfterLoadValues : function(form, values) {
                Ext.getCmp(this._psetId).setValue(values.PropertySingleValueLongPath);
                return true;
            },
            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave      : function(form, item, extraParams) {
                // TODO: cast date to correct format, boolean also
                if (item.Condition != Vizelia.FOL.BusinessEntities.FilterCondition.Between && item.Condition != Vizelia.FOL.BusinessEntities.FilterCondition.Outside) {
                    item.Value2 = null;
                }
                return true;
            },

            /**
             * Handler for updating PropertySingleValueLongPath when a the KeyPropertySingleValue is changed.
             */
            onChangeValuePset : function() {
                var combo = Ext.getCmp(this._psetId);
                var node = combo.getTree().getSelectionModel().getSelectedNode();
                var previousField = this.getForm().findField('Value');
                if (previousField) {
                    this.remove(previousField);
                }
                previousField = this.getForm().findField('Value2');
                if (previousField) {
                    this.remove(previousField);
                }
                if (node && Ext.isEmpty(combo.getActiveError())) {
                    this.entity.PropertySingleValueLongPath = node.getPath('text');
                    this.entity.PsetName = node.parentNode.attributes.entity.PsetName;
                    this.entity.AttributeName = node.attributes.entity.AttributeName;
                    this.isDirty = true;

                    var field = this.getFieldFromPsetAttribute(node.attributes.entity);
                    Ext.apply(field, {
                                fieldLabel : $lang('msg_value'),
                                name       : 'Value',
                                allowBlank : false,
                                value      : this.entity.Value
                            });
                    if (field.store) {
                        field.store.on('load', function() {
                                    var c = this.getForm().findField('Value');
                                    c.setValue(c.value);
                                    this.getForm()._async_components = [];
                                }, this);
                    }
                    if (this.getForm().findField('Condition').getValue() == Vizelia.FOL.BusinessEntities.FilterCondition.Between || this.getForm().findField('Condition').getValue() == Vizelia.FOL.BusinessEntities.FilterCondition.Outside) {
                        Ext.apply(field, {
                                    fieldLabel : $lang('msg_filterspatialpset_value')
                                });
                        this.insert(4, field);

                        Ext.apply(field, {
                                    fieldLabel : $lang('msg_filterspatialpset_value1'),
                                    name       : 'Value2',
                                    value      : this.entity.Value2
                                });
                        this.insert(5, field);
                    }
                    else
                        this.insert(4, field);

                }
                this.doLayout();

            }
        });

Ext.reg('vizFormFilterSpatialPset', Viz.form.FilterSpatialPset);
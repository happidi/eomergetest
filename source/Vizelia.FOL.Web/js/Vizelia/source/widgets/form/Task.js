﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Task
 * @extends Ext.FormPanel.
 * @xtype vizFormTask
 */
Viz.form.Task = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                this._id = Ext.id();
                var defaultConfig = {
                    mode_load                         : 'remote',
                    psetConfig                        : {
                        IfcType                    : 'IfcTask',
                        entityKey                  : 'KeyTask',
                        filterByClassificationItem : true
                    },
                    messageBusAfterUpdate             : 'TaskChange',
                    serviceHandlerWorkflowTransitions : Viz.Services.ServiceDeskWCF.Workflow_GetStateMachineEventsTransitions,
                    serviceHandlerWorkflowRaiseEvent  : Viz.Services.ServiceDeskWCF.Workflow_RaiseEvent,
                    serviceHandler                    : Viz.Services.ServiceDeskWCF.Task_FormLoad,
                    serviceHandlerCreate              : Viz.Services.ServiceDeskWCF.Task_FormCreate,
                    serviceHandlerUpdate              : Viz.Services.ServiceDeskWCF.Task_FormUpdate,
                    items                             : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.Task.superclass.constructor.call(this, config);

                if (this.mode == "update") {
                    this.on('beforerender', this.onBeforeRender, this);
                }

            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyTask',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            xtype      : 'textfield',
                            name       : 'Name'
                        }, {
                            fieldLabel           : $lang('msg_actionrequest_classificationlongpath'),
                            xtype                : 'viztreecomboClassificationItemWork',
                            name                 : 'KeyClassificationItem',
                            originalDisplayValue : 'ClassificationLongPath',
                            serviceParams        : {
                                fflgFilter            : config.mode == "update" ? false : true,
                                excludePset         : true,
                                excludeRelationship : true,
                                entity               : Viz.Configuration.GetClassificationItemDefinitionEntityFromCategory("task")
                            },
                            root                 : {
                                id      : Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory("task"),
                                iconCls : 'viz-icon-small-task',
                                text    : ''
                            },

                            listeners            : {
                                changevalue : function(field, value, node) {
                                    if (node == null) {
                                        this.psetConfig.KeyClassificationItem = this.entity.KeyClassificationItem;
                                    }
                                    else {
                                        this.psetConfig.KeyClassificationItem = node.attributes.entity.KeyClassificationItem;
                                        this.getForm().findField("Name").setValue(node.attributes.entity.LongPath);
                                    }
                                    this.loadExtraTabs();
                                },
                                scope       : this
                            }
                        }, {
                            fieldLabel     : $lang('msg_task_priority'),
                            name           : 'Priority',
                            xtype          : 'vizSpinnerField',
                            width          : 48,
                            minValue       : 0,
                            allowDecimals  : false,
                            incrementValue : 1,
                            accelerate     : true,
                            anchor         : null
                        }, {
                            fieldLabel : $lang('msg_status'),
                            xtype      : 'displayfield',
                            name       : 'StateLabel',
                            value      : 'Initial'
                        }, {
                            xtype         : 'compositefield',
                            fieldLabel    : $lang('msg_task_duration'),
                            combineErrors : true,
                            defaults      : {
                                hideLabel  : true,
                                allowBlank : true
                            },
                            items         : [{
                                        name           : 'DurationHours',
                                        xtype          : 'vizSpinnerField',
                                        width          : 60,
                                        minValue       : 0,
                                        allowDecimals  : false,
                                        incrementValue : 1,
                                        accelerate     : true,
                                        allowBlank     : true
                                    }, {
                                        xtype : 'displayfield',
                                        value : $lang('msg_enum_axistimeinterval_hours')
                                    }, {
                                        name           : 'DurationMinutes',
                                        xtype          : 'vizSpinnerField',
                                        width          : 48,
                                        minValue       : 0,
                                        maxValue       : 59,
                                        allowDecimals  : false,
                                        incrementValue : 1,
                                        accelerate     : true,
                                        allowBlank     : true
                                    }, {
                                        xtype : 'displayfield',
                                        value : $lang('msg_enum_axistimeinterval_minutes')
                                    }]
                        }, {
                            fieldLabel : $lang('msg_task_schedulestart'),
                            name       : 'ScheduleStart',
                            hideTime   : false,
                            xtype      : 'vizDateTime',
                            vvalidator : Ext.form.VTypes.daterangesimple,
                            width      : 200,
                            hasSwitch  : false,
                            allowBlank : true
                        }, {
                            fieldLabel : $lang('msg_task_startdate'),
                            name       : 'ActualStart',
                            hideTime   : false,
                            xtype      : 'vizDateTime',
                            vvalidator : Ext.form.VTypes.daterangesimple,
                            width      : 200,
                            hasSwitch  : false,
                            allowBlank : true
                        }, {
                            fieldLabel : $lang('msg_task_enddate'),
                            name       : 'ActualFinish',
                            hideTime   : false,
                            xtype      : 'vizDateTime',
                            vvalidator : Ext.form.VTypes.daterangesimple,
                            width      : 200,
                            hasSwitch  : false,
                            allowBlank : true
                        }, {
                            fieldLabel : $lang('msg_description'),
                            xtype      : 'textarea',
                            name       : 'Description'
                        }];
            },
            /**
             * Implementation for the virtual function in order to correct the issue with getFieldValues and composite fields.
             * @param {Viz.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave      : function(form, item, extraParams) {
                var isValid = true;
                item.Duration = item.DurationHours * 60 + item.DurationMinutes;
            },
            /**
             * Handler for updating action buttons responding to workflow.
             */
            onBeforeRender    : function() {
                var workflowUtil = new Viz.util.Workflow({
                            comp      : this,
                            listeners : {
                                success            : function(o, actions) {
                                    this.fbar.insert(0, {
                                                text    : $lang('msg_otheractions'),
                                                iconCls : 'viz-icon-small-action',
                                                menu    : actions
                                            });
                                    this.fbar.doLayout();

                                },
                                beforeaction       : function() {
                                    var form = this.getForm();
                                    if (form.isValid()) {// && (form.isDirty() || this.isDirty)
                                        if (this.ownerCt)
                                            this.ownerCt.getEl().mask();
                                        return true;
                                    }
                                    else
                                        return false;
                                },
                                afteractionsuccess : function() {
                                    if (this.ownerCt)
                                        this.ownerCt.getEl().unmask();
                                    this.publishMessage("TaskChange");
                                    this.publishMessage("ActionRequestChange");
                                    this.closeParentWindow(false);
                                },
                                afteractionfailure : function() {
                                    if (this.ownerCt)
                                        this.ownerCt.getEl().unmask();
                                },
                                scope              : this
                            }
                        });

                workflowUtil.getStateMachineActions();
                return true;
            }
        });
Ext.reg('vizFormTask', Viz.form.Task);
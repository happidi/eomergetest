﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Priority
 * @extends Ext.FormPanel.
 */
Viz.form.Priority = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.ServiceDeskWCF.Priority_FormLoad,
                    serviceHandlerCreate : Viz.Services.ServiceDeskWCF.Priority_FormCreate,
                    serviceHandlerUpdate : Viz.Services.ServiceDeskWCF.Priority_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Priority.superclass.constructor.call(this, config);
            },
            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name       : 'LocalId',
                            fieldLabel : $lang('msg_localid'),
                            hidden     : false
                        }, {
                            fieldLabel : $lang('msg_value'),
                            name       : 'Value',
                            xtype      : 'viznumberfield'
                        }, Viz.combo.Localization.factory({})];
            }
        });

Ext.reg('vizFormPriority', Viz.form.Priority);
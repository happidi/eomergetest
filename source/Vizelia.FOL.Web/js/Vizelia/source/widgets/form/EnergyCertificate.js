﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.EnergyCertificate
 * @extends Ext.FormPanel.
 */
Viz.form.EnergyCertificate = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor        : function(config) {
                this._gridEnergyCertificateCategory = Ext.id();
                this._tabEnergyCertificateCategory = Ext.id();

                var Id = config.entity ? config.entity.KeyEnergyCertificate : null;
                this.storeCategory = new Viz.store.EnergyCertificateCategory({
                            KeyEnergyCertificate : Id
                        });
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.EnergyCertificate_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.EnergyCertificate_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.EnergyCertificate_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_energycertificate_categories'),
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    hidden     : config.mode == 'create',
                                    id         : this._tabEnergyCertificateCategory,
                                    items      : [this.configGridPanelTab(config)]
                                }]
                    },
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.EnergyCertificate.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral  : function(config) {
                return [{
                            name   : 'KeyEnergyCertificate',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_energycertificate_startcolor'),
                            name       : 'StartColor',
                            xtype      : 'colorpickerfield',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_energycertificate_endcolor'),
                            name       : 'EndColor',
                            xtype      : 'colorpickerfield',
                            allowBlank : false
                        }]
            },

            /**
             * The configuration for the grid priority
             * @return {object}
             */
            configGridPanelTab : function(config) {
                return {
                    xtype                : 'vizGridEnergyCertificateCategory',
                    loadMask             : false,
                    id                   : this._gridEnergyCertificateCategory,
                    store                : this.storeCategory,
                    KeyEnergyCertificate : config.entity.KeyEnergyCertificate,
                    columns              : Viz.Configuration.Columns.EnergyCertificateCategory,
                    border               : true,
                    listeners            : {
                        beforedeleterecord : this.onDeletedRecordTab,
                        scope              : this
                    }
                };
            },

            /**
             * Handler for the columns grid delete.
             */
            onDeletedRecordTab : function() {
                this.deleteSelectedRowsFromGrid(this._gridEnergyCertificateCategory);
                return false;
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave       : function(form, item, extraParams) {
                if (this.storeCategory.hasLoaded)
                    this.storeCategory.save();
                Ext.apply(extraParams, {
                            categories : this.storeCategory.crudStore
                        });
                return true;
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess      : function(form, action) {
                Viz.form.EnergyCertificate.superclass.onSaveSuccess.apply(this, arguments);

                var tabpanel = this.getTabContainer();
                tabpanel.unhideTabStripItem(this._tabEnergyCertificateCategory);

                var grid = Ext.getCmp(this._gridEnergyCertificateCategory);
                grid.KeyEnergyCertificate = grid.store.serviceParams.KeyEnergyCertificate = this.entity.KeyEnergyCertificate;

            }
        });
Ext.reg('vizFormEnergyCertificate', Viz.form.EnergyCertificate);

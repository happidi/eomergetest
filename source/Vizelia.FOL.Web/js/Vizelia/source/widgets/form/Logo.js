﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Logo
 * @extends Ext.FormPanel.
 */
Viz.form.Logo = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                this.storeDynamicDisplayImageArray = [new Viz.store.DynamicDisplayImage({
                            type : Viz.Configuration.GetEnumValue('DynamicDisplayImageType', 'KPIPreviewPicture')
                        })];

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.CoreWCF.Logo_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.Logo_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.Logo_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Logo.superclass.constructor.call(this, config);
            },

            /*
             * saveItem : function() { if (this.form.getFieldValues().KeyImage === null) { this.closeParentWindow(); } else { Viz.form.Logo.superclass.saveItem.call(this, arguments); } },
             */

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyLogo',
                            hidden : true
                        }, {
                            fieldLabel             : $lang('msg_logo_image'),
                            name                   : 'KeyImage',
                            xtype                  : 'vizFileUpload',
                            acceptedFileExtensions : ['png'],
                            listeners              : {
                                onAfterDeleteClick : function() {
                                    Viz.App.Global.ViewPort.setDesktopLogo();
                                },
                                scope              : this
                            }
                        }, Viz.combo.DynamicDisplayImage.factory({
                                    fieldLabel : $lang('msg_tenant_kpi_previewpicture'),
                                    name       : 'KeyKPIPreviewPicture',
                                    store      : this.storeDynamicDisplayImageArray[0]
                                })];
            },

            onSaveSuccess     : function(form, action) {
                Viz.App.Global.ViewPort.setDesktopLogo();
                Viz.form.Logo.superclass.onSaveSuccess(form, action);
            }
        });

Ext.reg('vizFormLogo', Viz.form.Logo);
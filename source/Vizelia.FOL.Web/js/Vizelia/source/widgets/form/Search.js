﻿Ext.namespace("Viz.form");
/**
 * @class Viz.form.Search
 * @extends Ext.form.FormPanel The base search form. To take advantage of the builder, a field should expose the optional properties :
 * <ul>
 * <li><tt>filterIgnore</tt> to be excluded from the filter </li>
 * <li><tt>filterComparison</tt> the comparison ('eq', 'ne', 'gt', 'lt') </li>
 * <li><tt>filterType</tt> the data type ('string', 'numeric', 'boolean', 'date', 'list') </li>
 * </ul>
 */
Viz.form.Search = Ext.extend(Ext.form.FormPanel, {

            /**
             * Builds the filters
             */
            buildFilters : function() {
                var filters = [];
                this.getForm().items.each(function(f) {

                            if (f.isDirty() && f.filterIgnore != true) {

                                filters.push({
                                            field : f.getName(),
                                            data  : {
                                                comparison : f.filterComparison || null,
                                                type       : f.filterType || "string",
                                                value      : f.getValue()
                                            }
                                        });
                            }
                        });

                return filters;
            }

        });

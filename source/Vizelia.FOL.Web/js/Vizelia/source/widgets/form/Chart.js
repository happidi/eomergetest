﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Chart
 * @extends Ext.ChartFormPanel.
 */
Viz.form.Chart = Ext.extend(Viz.form.ChartFormPanel, {
    /**
             * Ctor.
             * @param {Object} config The configuration options
             */
    constructor                                                 : function(config) {
        this._gridDataSerie = Ext.id();
        this._gridCalculatedSerie = Ext.id();
        this._gridStatisticalSerie = Ext.id();
        this._gridChartAxis = Ext.id();
        this._gridChartPsetAttributeHistorical = Ext.id();
        this._gridChartMarker = Ext.id();
        this._gridChartHistoricalAnalysis = Ext.id();
        this._gridChartAlgorithm = Ext.id();
        this._gridFilterSpatialPset = Ext.id();
        this._gridFilterKPIAzmanRole = Ext.id();

        this._fieldsetHeatMap = Ext.id();
        this._fieldsetCorrelation = Ext.id();
        this._fieldsetEnergyCertificate = Ext.id();
        this._fieldsetDifferenceHighlight = Ext.id();
        this._fieldsetMicroChart = Ext.id();
        this._fieldsetCalendarView = Ext.id();

        this._tabMeterAll = Ext.id();
        this._tabDataSeriesAndAxis = Ext.id();
        this._tabCalculatedSerie = Ext.id();
        this._tabStatisticalSerie = Ext.id();
        this._tabSpecificAnalysis = Ext.id();

        this._displayModeId = Ext.id();

        this.storeDynamicDisplayImageArray = [new Viz.store.DynamicDisplayImage({
            type : Viz.Configuration.GetEnumValue('DynamicDisplayImageType', 'KPIPreviewPicture')
        })];

        var Id = config.entity ? config.entity.KeyChart : null;
        this._KeyChart = Id || "";
        if (!Ext.isArray(config.uniqueTabs))
            config.uniqueTabs = [];

        if ((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'datasource' || Array.contains(config.uniqueTabs, 'datasource')) {

            this.storeFilterSpatial = new Viz.store.ChartFilterSpatial({
                KeyChart : Id
            });
            this.storeFilterMeterClassification = new Viz.store.ChartFilterMeterClassification({
                KeyChart : Id
            });
            this.storeFilterEventLogClassification = new Viz.store.ChartFilterEventLogClassification({
                KeyChart : Id
            });
            this.storeMeter = new Viz.store.ChartMeter({
                KeyChart : Id
            });
            this.storeFilterAlarmDefinitionClassification = new Viz.store.ChartFilterAlarmDefinitionClassification({
                KeyChart : Id
            });
            this.storeFilterSpatialPset = new Viz.store.ChartFilterSpatialPset({
                KeyChart : Id
            });
            this.storeChartPsetAttributeHistorical = new Viz.store.ChartPsetAttributeHistorical({
                KeyChart : Id
            });
        }

        if (((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'meterall') || Array.contains(config.uniqueTabs, 'meterall')) {
            this.storeMeterAll = new Viz.store.ChartMeterAll({
                KeyChart : Id
            });
        }
        if (((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'dataseries') || Array.contains(config.uniqueTabs, 'dataseries')) {
            this.storeDataSerie = new Viz.store.DataSerie({
                KeyChart : Id
            });
            this.storeChartAxis = new Viz.store.ChartAxis({
                KeyChart : Id
            });

            this.storeChartMarker = new Viz.store.ChartMarker({
                KeyChart : Id
            });
        }
        if (((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'calculatedserie') || Array.contains(config.uniqueTabs, 'calculatedserie')) {
            this.storeCalculatedSerie = new Viz.store.CalculatedSerie({
                KeyChart : Id
            });
        }
        if (((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'statisticalserie') || Array.contains(config.uniqueTabs, 'statisticalserie')) {
            this.storeStatisticalSerie = new Viz.store.StatisticalSerie({
                KeyChart : Id
            });
        }
        if (((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'analysis') || Array.contains(config.uniqueTabs, 'analysis')) {
            this.storeChartHistoricalAnalysis = new Viz.store.ChartHistoricalAnalysis({
                KeyChart : Id
            });
            if ($authorized('@@@@ Algorithm')) {
                this.storeChartAlgorithm = new Viz.store.ChartAlgorithm({
                    KeyChart : Id
                });
            }
        }
        if (((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'kpi') || Array.contains(config.uniqueTabs, 'kpi')) {
            this.storeFilterKPIAzManRole = new Viz.store.ChartKPIAzManRole({
                KeyChart : Id
            });
        }
        /*
                 * this.storeChartAvailableDataSerieLocalId = new Viz.store.ChartAvailableDataSerieLocalId({ KeyChart : Id, includeExisting : true });
                 */
        /*
                 * this.storeDynamicDisplayImage = new Viz.store.DynamicDisplayImage({});
                 */

        config = config || {};
        config.texareawithintelissenseIdsArray = [Ext.id(), Ext.id(), Ext.id()];
        var defaultConfig = {
            mode_load            : 'remote',
            serviceHandler       : Viz.Services.EnergyWCF.Chart_FormLoad,
            serviceHandlerCreate : Viz.Services.EnergyWCF.Chart_FormCreate,
            serviceHandlerUpdate : Viz.Services.EnergyWCF.Chart_FormUpdate,
            hasValidationToolbar : true,
            entityType           : 'Chart',
            tabConfig            : {
                deferredRender : Ext.isEmpty(config.deferredRender) ? true : config.deferredRender,
                activeTab      : 0,
                items          : []
            },
            closeonsave          : false
        };
        if ((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'general' || Array.contains(config.uniqueTabs, 'general'))
            defaultConfig.tabConfig.items.push({
                title      : $lang('msg_general'),
                iconCls    : 'viz-icon-small-form-update',
                items      : this.configFormGeneral(config),
                labelWidth : 150
            });

        if ((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'datasource' || Array.contains(config.uniqueTabs, 'datasource'))
            defaultConfig.tabConfig.items.push({
                title      : $lang('msg_chart_datasource'),
                iconCls    : 'viz-icon-small-chart-datasource',
                defaults   : {
                    anchor   : Viz.Const.UI.Anchor,
                    hideMode : 'offsets'
                },
                labelWidth : 200,
                items      : [this.configGridFilterSpatial(config), this.configGridFilterSpatialPset(config), this.configGridFilterMeterClassification(config), this.configDetailGridMeter(config), this.configGridFilterEventLogClassification(config), this.configGridChartPsetAttributeHistorical(config), this.configGridFilterAlarmDefinitionClassification(config), this.configFormDataSource(config), this.configFormMaxMeter(config), this.configFormHideMeterDataValidity(config)],
                listeners  : {
                    activate : {
                        fn    : function() {
                            // we need this for the MeterClassification test on
                            // SpatialFilter.
                            if (!this.storeFilterSpatial.hasLoaded)
                                this.storeFilterSpatial.load();
                        },
                        scope : this
                    }
                }
            });
        if (config.mode != 'create' && ((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'meterall') || Array.contains(config.uniqueTabs, 'meterall'))
            defaultConfig.tabConfig.items.push({
                id         : this._tabMeterAll,
                title      : $lang('msg_chart_meterall'),
                iconCls    : 'viz-icon-small-meter-all',
                labelAlign : 'top',
                layout     : 'fit',
                items      : [this.configGridMeterAll(config)]
            });
        if ((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'display' || Array.contains(config.uniqueTabs, 'display'))
            defaultConfig.tabConfig.items.push({
                title      : $lang('msg_chart_display'),
                iconCls    : 'viz-icon-small-chart-display',
                labelWidth : 200,
                defaults   : {
                    anchor   : Viz.Const.UI.Anchor,
                    hideMode : 'offsets'
                },
                items      : this.configFormDisplay(config)

            });
        if ((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'dynamicdisplay' || Array.contains(config.uniqueTabs, 'dynamicdisplay'))
            defaultConfig.tabConfig.items.push({
                title      : $lang('msg_chart_dynamicdisplay'),
                iconCls    : 'viz-icon-small-dynamicdisplay',
                labelWidth : 200,
                defaults   : {
                    anchor   : Viz.Const.UI.Anchor,
                    hideMode : 'offsets'
                },
                items      : this.configFormDynamicDisplay(config)

            });
        if (config.mode != 'create' && ((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'dataseries') || Array.contains(config.uniqueTabs, 'dataseries'))
            defaultConfig.tabConfig.items.push({
                id: this._tabDataSeriesAndAxis,
                title: $lang('msg_chart_dataseries') + ' & ' + $lang('msg_chartaxis'),
                iconCls: 'viz-icon-small-chart-hbar',
                labelWidth: 200,
                defaults: {
                    anchor: Viz.Const.UI.Anchor,
                    hideMode: 'offsets'
                },
                items: [this.configFormDataSeries(config)]
            });
        if (config.mode != 'create' && ((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'calculatedserie') || Array.contains(config.uniqueTabs, 'calculatedserie'))
            defaultConfig.tabConfig.items.push({
                id: this._tabCalculatedSerie,
                title: $lang('msg_chart_calculatedseries'),
                iconCls: 'viz-icon-small-calculatedserie',
                labelAlign: 'top',
                layout: 'fit',
                items: [this.configGridCalculatedSerie(config)]
            });
        if (config.mode != 'create' && ((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'statisticalserie') || Array.contains(config.uniqueTabs, 'statisticalserie'))
            defaultConfig.tabConfig.items.push({
                id: this._tabStatisticalSerie,
                title: $lang('msg_chart_statisticalseries'),
                iconCls: 'viz-icon-small-statisticalserie',
                labelAlign: 'top',
                layout: 'fit',
                items: [this.configGridStatisticalSerie(config)]
            });
        if (config.mode != 'create' && ((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'analysis') || Array.contains(config.uniqueTabs, 'analysis'))
            defaultConfig.tabConfig.items.push({
                id: this._tabSpecificAnalysis,
                title: $lang('msg_chart_analysis'),
                iconCls: 'viz-icon-small-chart-analysis',
                labelWidth: 200,
                defaults: {
                    anchor: Viz.Const.UI.Anchor,
                    hideMode: 'offsets'
                },
                items: this.configFormAnalysis(config) 
            });

        if (((Ext.isEmpty(config.uniqueTab) && config.uniqueTabs.length == 0) || config.uniqueTab == 'kpi') && $authorized('@@@@ Chart KPI') || Array.contains(config.uniqueTabs, 'kpi'))
            defaultConfig.tabConfig.items.push({
                title: $lang('msg_chart_kpi'),
                iconCls: 'viz-icon-small-chart-kpi',
                labelWidth: 300,
                defaults: {
                    anchor: Viz.Const.UI.Anchor,
                    hideMode: 'offsets'
                },
                items: this.configFormKPI(config)
            });

        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        if (!Ext.isEmpty(config.uniqueTab)) {
            config.items = config.tabConfig.items[0].items;
            config.labelWidth = config.tabConfig.items[0].labelWidth || 200;
            config.auditEnabled = false;
            if (config.uniqueTab == 'meterall')
                config.layout = 'fit';
            delete config.tabConfig;
        }

        Viz.util.MessageBusMgr.subscribe("ChartHistoricalAnalysisChange", this.onChartHistoricalAnalysisChange, this);
        Viz.util.MessageBusMgr.subscribe("DataSerieChange", this.onDataSerieChange, this);
        Viz.util.MessageBusMgr.subscribe("CalculatedSerieChange", this.onCalculatedSerieChange, this);
        Viz.util.MessageBusMgr.subscribe("StatisticalSerieChange", this.onStatisticalSerieChange, this);
        Viz.util.MessageBusMgr.subscribe("ChartChange", this.onChartChange, this);

        Viz.form.Chart.superclass.constructor.call(this, config);

        // Inits the tree model once for all the DynamicDisplay fields
        if (Ext.isEmpty(this.uniqueTab) || this.uniqueTab == 'dynamicdisplay') {
            Viz.plugins.IntelliSense.synchronizeFields(config.texareawithintelissenseIdsArray);
        }
    },

    /**
             * Destroy
             */
    onDestroy: function() {
        Viz.util.MessageBusMgr.unsubscribe("ChartHistoricalAnalysisChange", this.onChartHistoricalAnalysisChange, this);
        Viz.util.MessageBusMgr.unsubscribe("DataSerieChange", this.onDataSerieChange, this);
        Viz.util.MessageBusMgr.unsubscribe("CalculatedSerieChange", this.onCalculatedSerieChange, this);
        Viz.util.MessageBusMgr.unsubscribe("ChartChange", this.onChartChange, this);
        Viz.util.MessageBusMgr.unsubscribe("StatisticalSerieChange", this.onStatisticalSerieChange, this);
        Viz.form.Chart.superclass.onDestroy.apply(this, arguments);
    },

    /**
    * @param {Object} config The configuration options of the form
    */
    configFormGeneral: function(config) {
        var now = new Date();
        var ret = [
            {
                name: 'KeyChart',
                hidden: true
            }, {
                fieldLabel: $lang('msg_localid'),
                allowBlank: true,
                name: 'LocalId'
            }, {
                fieldLabel: $lang('msg_chart_title'),
                name: 'Title',
                allowBlank: false
            }, {
                xtype: 'textarea',
                fieldLabel: $lang('msg_description'),
                name: 'Description'
            }, {
                fieldLabel: $lang('msg_chart_favorite'),
                name: 'IsFavorite',
                xtype: 'vizcheckbox',
                checked: true
            }, {
                fieldLabel: $lang('msg_portalwindow_openonstartup'),
                name: 'OpenOnStartup',
                xtype: 'vizcheckbox',
                checked: false
            }, {
                xtype: 'vizcompositefield',
                fieldLabel: $lang('msg_chart_localisation'),
                items: [
                    {
                        // fieldLabel : $lang('msg_chart_localisation'),
                        name: 'Localisation',
                        allowBlank: false,
                        xtype: 'vizComboEnum',
                        enumTypeName: 'Vizelia.FOL.BusinessEntities.ChartLocalisation,Vizelia.FOL.Common',
                        sortByValues: true,
                        displayIcon: true,
                        value: Vizelia.FOL.BusinessEntities.ChartLocalisation.ByMeter,
                        listeners: {
                            select: this.onSelectLocalisation,
                            scope: this
                        }
                    }, {
                        xtype: 'displayfield',
                        submitValue: false,
                        cls: 'viz-form-display-field-inside-composite',
                        value: $lang('msg_chart_localisationsitelevel') + ':'
                    }, {
                        fieldLabel: $lang('msg_chart_localisationsitelevel'),
                        xtype: 'vizSpinnerField',
                        name: 'LocalisationSiteLevel',
                        value: 0,
                        minValue: 0,
                        maxValue: 10,
                        allowBlank: false,
                        width: 60
                    }, {
                        xtype: 'displayfield',
                        submitValue: false,
                        cls: 'viz-form-display-field-inside-composite',
                        value: $lang('msg_chart_usespatialpath') + ':'
                    }, {
                        fieldLabel: $lang('msg_chart_usespatialpath'),
                        xtype: 'vizcheckbox',
                        name: 'UseSpatialPath',
                        checked: true
                    }
                ]
            }, {
                fieldLabel: $lang('msg_chart_timeinterval'),
                name: 'TimeInterval',
                allowBlank: false,
                xtype: 'vizComboEnum',
                enumTypeName: 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common',
                value: Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months,
                sortByValues: true,
                displayIcon: true
            }, {
                xtype: 'vizcompositefield',
                fieldLabel: $lang('msg_chart_classificationlevel'),
                items: [
                    {
                        fieldLabel: $lang('msg_chart_classificationlevel'),
                        name: 'ClassificationLevel',
                        allowBlank: false,
                        xtype: 'vizComboChartClassificationLevel'
                    }, {
                        xtype: 'displayfield',
                        submitValue: false,
                        cls: 'viz-form-display-field-inside-composite',
                        value: $lang('msg_chart_useclassificationpath') + ':'
                    }, {
                        fieldLabel: $lang('msg_chart_useclassificationpath'),
                        xtype: 'vizcheckbox',
                        name: 'UseClassificationPath',
                        checked: false
                    }
                ]
            }, {
                fieldLabel: $lang('msg_chart_timezone'),
                name: 'TimeZoneId',
                allowBlank: false,
                autoLoad: true,
                xtype: 'vizComboTimeZone',
                listeners: {
                    scope: this,
                    select: function(combo) {
                        var form = this.getForm();
                        form.findField('EndDate').setTimeZoneValue(combo.value);
                        form.findField('StartDate').setTimeZoneValue(combo.value);
                    }
                }
            }, {
                xtype: 'fieldset',
                title: $lang('msg_chart_timerange'),
                labelWidth: 180,
                defaults: {
                    msgTarget: 'side',
                    anchor: Viz.Const.UI.Anchor
                },
                items: [
                    {
                        fieldLabel: $lang('msg_chart_startdate'),
                        name: 'StartDate',
                        hideTime: false,
                        xtype: 'vizDateTime',
                        allowBlank: false,
                        hasSwitch: false,
                        hasTimeZone: true,
                        value: new Date(now.getFullYear(), 0, 1, 0, 0, 0)
                    }, {
                        fieldLabel: $lang('msg_chart_enddate'),
                        name: 'EndDate',
                        hideTime: false,
                        xtype: 'vizDateTime',
                        allowBlank: false,
                        hasSwitch: false,
                        hasTimeZone: true,
                        value: new Date(now.getFullYear(), 11, 31, 23, 59, 59)
                    }, {
                        fieldLabel: $lang('msg_chart_dynamic_timescale'),
                        name: 'DynamicTimeScaleEnabled',
                        xtype: 'vizcheckbox',
                        listeners: {
                            check: this.onCheckDynamicTimeScaleEnabled,
                            scope: this
                        }

                    }, {
                        xtype: 'vizcompositefield',
                        fieldLabel: $lang('msg_chart_dynamic_timescale_uselast'),
                        items: [
                            {
                                fieldLabel: $lang('msg_chart_dynamic_timescale_uselast'),
                                name: 'DynamicTimeScaleFrequency',
                                xtype: 'vizSpinnerField',
                                // minValue : 1,
                                allowBlank: true,
                                width: 70,
                                value: 1,
                                validator: Ext.form.VTypes.numberfieldNotZeroValidator
                            }, {
                                xtype: 'displayfield',
                                cls: 'viz-form-display-field-inside-composite',
                                value: $lang('msg_chart_dynamic_timescale_interval') + ':'
                            }, {
                                fieldLabel: $lang('msg_chart_dynamic_timescale_interval'),
                                name: 'DynamicTimeScaleInterval',
                                xtype: 'vizComboEnum',
                                enumTypeName: 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common',
                                sortByValues: true,
                                value: "4",
                                flex: 1,
                                valuesToRemove: ['None', 'Global']
                            }, {
                                xtype: 'displayfield',
                                cls: 'viz-form-display-field-inside-composite',
                                value: $lang('msg_chart_dynamic_timescale_calendar') + ':'
                            }, {
                                name: 'DynamicTimeScaleCalendar',
                                xtype: 'vizcheckbox',
                                checked: true
                            }
                        ]
                    }, {
                        xtype: 'vizcompositefield',
                        fieldLabel: $lang('msg_chart_dynamic_timescale_completeinterval'),
                        items: [
                            {
                                name: 'DynamicTimeScaleCompleteInterval',
                                xtype: 'vizcheckbox',
                                listeners: {
                                    check: this.onCheckDynamicTimeScaleCompleteInterval,
                                    scope: this
                                }
                            }, {
                                xtype: 'displayfield',
                                cls: 'viz-form-display-field-inside-composite',
                                value: $lang('msg_chart_dynamic_timescale_calendarendtonow') + ':'
                            }, {
                                name: 'DynamicTimeScaleCalendarEndToNow',
                                xtype: 'vizcheckbox',
                                listeners: {
                                    check: this.onCheckDynamicTimeScaleCalendarEndToNow,
                                    scope: this
                                }
                            }
                        ]
                    }, {
                        xtype: 'vizcompositefield',
                        fieldLabel: $lang('msg_calendar'),
                        items: [
                            {
                                fieldLabel: $lang('msg_calendar'),
                                name: 'KeyCalendarEventCategory',
                                valueField: 'KeyCalendarEventCategory',
                                allowBlank: true,
                                xtype: 'vizComboCalendarEventCategory',
                                flex: 1
                            }, {
                                xtype: 'displayfield',
                                cls: 'viz-form-display-field-inside-composite',
                                value: $lang('msg_chart_calendarmode') + ':'
                            }, {
                                name: 'CalendarMode',
                                xtype: 'vizComboEnum',
                                enumTypeName: 'Vizelia.FOL.BusinessEntities.ChartCalendarMode,Vizelia.FOL.Common',
                                // sortByValues : true,
                                value: 0,
                                flex: 1,
                                allowBlank: false
                            }
                        ]
                    }
                ]
            }, {
                fieldLabel: $lang('msg_chart_hidedatagridinexport'),
                name: 'HideDatagridInExport',
                xtype: 'vizcheckbox'
            }
        ];
        this.addHiddenMaxMeter(config, ret);
        return ret;
    },

    configFormKPI: function(config) {
        var ret = [
            {
                fieldLabel: $lang('msg_chart_kpi_define'),
                name: 'IsKPI',
                xtype: 'vizcheckbox',
                checked: config.initialConfigFromGrid ? config.initialConfigFromGrid.common.isKPI : false
            }, {
                xtype: 'fieldset',
                title: $lang('msg_chart_kpi_libraryinformations'),
                iconCls: 'viz-icon-small-chart-kpi',
                collapsible: true,
                collapsed: false,
                autoHeight: true,
                labelWidth: 280,
                defaults: {
                    msgTarget: 'side',
                    anchor: Viz.Const.UI.Anchor
                },
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: $lang('msg_chart_kpi_id'),
                        allowBlank: true,
                        name: 'KPIId'
                    }, {
                        xtype: 'textfield',
                        fieldLabel: $lang('msg_chart_kpi_build'),
                        allowBlank: true,
                        name: 'KPIBuild'
                    }, Viz.combo.DynamicDisplayImage.factory({
                        fieldLabel: $lang('msg_chart_kpi_previewpicture'),
                        name: 'KeyKPIPreviewPicture',
                        store: this.storeDynamicDisplayImageArray[0]
                    }), {
                        fieldLabel: $lang('msg_chart_kpi_copydate'),
                        name: 'KPICopyDate',
                        hideTime: false,
                        xtype: 'vizDateTime',
                        allowBlank: true,
                        hasSwitch: false
                    }, {
                        fieldLabel: $lang('msg_chart_kpi_classification'),
                        xtype: 'viztreecomboClassificationItemKPI',
                        name: 'KeyClassificationItemKPI',
                        originalDisplayValue: 'ClassificationItemKPILongPath',
                        allowBlank: true
                    }, {
                        xtype: 'textarea',
                        fieldLabel: $lang('msg_chart_kpi_setupinstructions'),
                        name: 'KPISetupInstructions'
                    }
                ]
            }, {
                xtype: 'fieldset',
                title: $lang('msg_azmanrole'),
                iconCls: 'viz-icon-small-role',
                collapsible: true,
                collapsed: true,
                autoHeight: true,
                defaults: {
                    anchor: Viz.Const.UI.GridAnchor
                },
                items: {
                    xtype: 'vizGridFilterAzManRole',
                    id: this._gridFilterKPIAzmanRole,
                    store: this.storeFilterKPIAzManRole,
                    border: true,
                    loadMask: false,
                    height: 200
                }
            }, {
                xtype: 'fieldset',
                title: $lang('msg_chart_kpi_wizardselections'),
                iconCls: 'viz-icon-small-wizard',
                collapsible: true,
                collapsed: true,
                autoHeight: true,
                labelWidth: 280,
                labelWidth: 280,
                defaults: {
                    msgTarget: 'side',
                    anchor: Viz.Const.UI.Anchor
                },
                items: [
                    {
                        fieldLabel: $lang('msg_chart_kpi_disablemeterselection'),
                        name: 'KPIDisableMeterSelection',
                        xtype: 'vizcheckbox',
                        checked: false
                    }, {
                        fieldLabel: $lang('msg_chart_kpi_disablespatialselection'),
                        name: 'KPIDisableSpatialSelection',
                        xtype: 'vizcheckbox',
                        checked: false
                    }, {
                        fieldLabel: $lang('msg_chart_kpi_disablemeterclassificationselection'),
                        name: 'KPIDisableMeterClassificationSelection',
                        xtype: 'vizcheckbox',
                        checked: false
                    }, {
                        fieldLabel: $lang('msg_chart_kpi_disabledaterangemenu'),
                        name: 'KPIDisableDateRangeMenu',
                        xtype: 'vizcheckbox',
                        checked: false
                    }
                ]
            }, {
                xtype: 'fieldset',
                iconCls: 'viz-icon-small-chart',
                collapsible: true,
                collapsed: true,
                autoHeight: true,
                labelWidth: 280,
                title: $lang('msg_chart_kpi_usercontrols'),
                labelWidth: 280,
                defaults: {
                    msgTarget: 'side',
                    anchor: Viz.Const.UI.Anchor
                },
                items: [
                    {
                        fieldLabel: $lang('msg_chart_kpi_disablelocalisationslider'),
                        name: 'KPIDisableLocalisationSlider',
                        xtype: 'vizcheckbox',
                        checked: false
                    }, {
                        fieldLabel: $lang('msg_chart_kpi_disabletimeintervalslider'),
                        name: 'KPIDisableTimeIntervalSlider',
                        xtype: 'vizcheckbox',
                        checked: false
                    }, {
                        fieldLabel: $lang('msg_chart_kpi_disableclassificationlevelslider'),
                        name: 'KPIDisableClassificationLevelSlider',
                        xtype: 'vizcheckbox',
                        checked: false
                    }, {
                        fieldLabel: $lang('msg_chart_kpi_disabledisplaymodeslider'),
                        name: 'KPIDisableDisplayModeSlider',
                        xtype: 'vizcheckbox',
                        checked: false
                    }, {
                        fieldLabel: $lang('msg_chart_kpi_disablelimitseriesnumberslider'),
                        name: 'KPIDisableLimitSeriesNumberSlider',
                        xtype: 'vizcheckbox',
                        checked: false
                    }
                ]
            }
        ];
       
        this.addHiddenMaxMeter(config, ret);
        return ret;
    },
    /**
             * The configuration object for the filter spatial grid.
             * @return {Object}
             */
    configGridFilterSpatial: function(config) {
        return {
            xtype: 'fieldset',
            title: $lang('msg_chart_filterspatial'),
            iconCls: 'viz-icon-small-site',
            collapsible: true,
            collapsed: true,
            autoHeight: true,
            defaults: {
                anchor: Viz.Const.UI.GridAnchor
            },
            items: {
                xtype: 'vizGridFilterSpatial',
                store: this.storeFilterSpatial,
                border: true,
                loadMask: false,
                height: 200,
                disableCheckboxForAncestors: true
            }
        };
    },

    /**
             * The configuration for the grid filter spatial.
             * @return {object}
             */
    configGridFilterSpatialPset: function(config) {
        return {
            xtype: 'fieldset',
            title: $lang('msg_filterspatialpset'),
            iconCls: 'viz-icon-small-gridfilter',
            collapsible: true,
            collapsed: true,
            autoHeight: true,
            defaults: {
                anchor: Viz.Const.UI.GridAnchor
            },
            items: {
                xtype: 'vizGridFilterSpatialPset',
                id: this._gridFilterSpatialPset,
                hidden: config.mode == 'create',
                border: true,
                height: 200,
                store: this.storeFilterSpatialPset,
                loadMask: false,
                KeyChart: config.entity ? config.entity.KeyChart : '',
                listeners: {
                    beforedeleterecord: function() {
                        this.deleteSelectedRowsFromGrid(this._gridFilterSpatialPset);
                        return false;
                    },
                    scope: this
                }
            }
        };
    },

    /**
             * The configuration object for the filter meter classification.
             * @return {Object}
             */
    configGridFilterMeterClassification: function(config) {
        return {
            xtype: 'fieldset',
            title: $lang('msg_meter_classification'),
            iconCls: 'viz-icon-small-classificationitem',
            collapsible: true,
            collapsed: true,
            autoHeight: true,
            defaults: {
                anchor: Viz.Const.UI.GridAnchor
            },
            items: {
                xtype: 'vizGridFilterMeterClassification',
                store: this.storeFilterMeterClassification,
                border: true,
                loadMask: false,
                listeners: {
                    beforeaddrecord: this.onBeforeAddFilterMeterClassification,
                    scope: this
                },
                height: 200
            }
        };
    },

    /**
             * The configuration object for the filter meter classification.
             * @return {Object}
             */
    configGridFilterEventLogClassification: function(config) {
        return {
            xtype: 'fieldset',
            title: $lang('msg_chart_filtereventlog'),
            iconCls: 'viz-icon-small-eventlog',
            collapsible: true,
            collapsed: true,
            autoHeight: true,
            defaults: {
                anchor: Viz.Const.UI.GridAnchor
            },
            items: {
                xtype: 'vizGridFilterEventLogClassification',
                store: this.storeFilterEventLogClassification,
                height: 200,
                border: true
            }
        };
    },

    /**
             * The configuration object for the filter meter classification.
             * @return {Object}
             */
    configGridFilterAlarmDefinitionClassification: function(config) {
        return {
            xtype: 'fieldset',
            title: $lang('msg_alarmdefinition'),
            iconCls: 'viz-icon-small-alarm',
            collapsible: true,
            collapsed: true,
            autoHeight: true,
            defaults: {
                anchor: Viz.Const.UI.GridAnchor
            },
            items: {
                xtype: 'vizGridFilterAlarmDefinitionClassification',
                store: this.storeFilterAlarmDefinitionClassification,
                height: 200,
                border: true
            }
        };
    },

    /**
             * The configuration object for the detail grid Meter.
             * @return {Object}
             */
    configDetailGridMeter: function(config) {
        var retVal = this.configGridDetail({
            name: 'Meter',
            windowTitle: $lang('msg_meter'),
            windowWidth: 1000,
            windowHeight: 400,
            windowIconCls: 'viz-icon-small-meter',
            xtypeGridDetail: 'vizGridMeter',
            columnsGridDetail: Viz.Configuration.Columns.ChartMeter,
            buttonAddTitle: $lang('msg_meter_add'),
            buttonAddIconCls: 'viz-icon-small-add',
            buttonDeleteTitle: $lang('msg_meter_delete'),
            buttonDeleteIconCls: 'viz-icon-small-delete',
            storeDetail: this.storeMeter,
            keyColumn: 'KeyMeter',
            ddGroupGridDetail: 'SpatialDD',
            enableDropFromTreeGridDetail: true
        });
        Ext.apply(retVal, {
            height: 200,
            loadMask: false
        });
        return {
            xtype: 'fieldset',
            title: $lang('msg_meters'),
            iconCls: 'viz-icon-small-meter',
            collapsible: true,
            collapsed: true,
            autoHeight: true,
            defaults: {
                anchor: Viz.Const.UI.GridAnchor
            },
            items: retVal
        };
    },

    /**
             * The configuration for the grid ChartPsetAttributeHistorical.
             * @return {object}
             */
    configGridChartPsetAttributeHistorical: function(config) {
        return {
            xtype: 'fieldset',
            title: $lang('msg_chartpsetattributehistorical'),
            iconCls: 'viz-icon-small-psetattributehistorical',
            collapsible: true,
            collapsed: true,
            autoHeight: true,
            defaults: {
                anchor: Viz.Const.UI.GridAnchor
            },
            items: {
                id: this._gridChartPsetAttributeHistorical,
                border: true,
                xtype: 'vizGridChartPsetAttributeHistorical',
                store: this.storeChartPsetAttributeHistorical,
                loadMask: false,
                height: 200,
                KeyChart: config.entity ? config.entity.KeyChart : '',
                listeners: {
                    beforedeleterecord: function() {
                        this.deleteSelectedRowsFromGrid(this._gridChartPsetAttributeHistorical);
                        return false;
                    },
                    scope: this
                }
            }
        };
    },
    /**
             * Return the extra options for the Chart DataSource form tab.
             * @param {} config
             * @return {}
             */
    configFormDataSource: function(config) {
        return {
            xtype: 'fieldset',
            title: $lang('msg_chart_usedataacquisitionendpoint'),
            iconCls: 'viz-icon-small-dataacquisitioncontainer',
            collapsible: true,
            collapsed: true,
            autoHeight: true,
            defaults: {
                anchor: Viz.Const.UI.GridAnchor
            },
            items: [
                {
                    fieldLabel: $lang('msg_chart_usedataacquisitionendpoint'),
                    name: 'UseDataAcquisitionEndPoint',
                    xtype: 'vizcheckbox',
                    checked: config.UseDataAcquisitionEndPoint
                }
            ]
        };
    },

    configFormMaxMeter: function(config) {
        return {
            xtype: 'fieldset',
            title: $lang('msg_chart_maxmeter'),
            iconCls: 'viz-icon-small-warning',
            collapsible: true,
            collapsed: true,
            autoHeight: true,
            listeners: {

                expand: function (panel) {
/* Due to hidden spinners setting the wrapping div tag's width to 0,
the errorIcon is incorrectly positioned. Calling resize when the spinners are visible
let's it update the div tag correctly.
 */
                    var spinFld = panel.findByType("vizSpinnerField", false)[0];
                    spinFld.onResize.call(spinFld, spinFld.width, spinFld.height);
                },

            },
            defaults: {
                anchor: Viz.Const.UI.Anchor
            },
            items: [
                {
                    fieldLabel: $lang('msg_chart_maxmeter'),
                    name: 'MaxMeter',
                    xtype: 'vizSpinnerField',
                    value: 100,
                    minValue: 0,
                    allowBlank: false
                }
            ]
        }
    },

    addHiddenMaxMeter: function (config, ret) {
        if (config.uniqueTab) {
            ret.push({
                fieldLabel: $lang('msg_chart_maxmeter'),
                name: 'MaxMeter',
                xtype: 'vizSpinnerField',
                value: 100,
                minValue: 0,
                allowBlank: false,
                hidden: true
            });
        }
        return ret;
    },

    configFormHideMeterDataValidity: function(config) {
        return {
            xtype: 'fieldset',
            title: $lang('msg_metervalidationrule'),
            iconCls: 'viz-icon-small-metervalidationrule',
            collapsible: true,
            collapsed: true,
            autoHeight: true,
            defaults: {
                anchor: Viz.Const.UI.Anchor
            },
            items: [
                {
                    fieldLabel: $lang('msg_chart_hidemeterdatavalidity'),
                    name: 'HideMeterDataValidity',
                    xtype: 'vizcheckbox'
                }
            ]
        }
    },

    /**
             * Return the configuration object for the grid containing all the meters.(readonly grid)
             * @param {} config
             * @return {}
             */
    configGridMeterAll: function(config) {
        return {
            xtype: 'vizGridMeter',
            store: this.storeMeterAll,
            border: true,
            readonly: true,
            alloweditbyclick: true,
            columns: Viz.Configuration.Columns.ChartMeter
        };
    },

    configFormDataSeries: function (config) {
        var ret = [this.configFormChartAxis(config), this.configGridDataSerie(config), this.configFormChartMarker(config)];
        this.addHiddenMaxMeter(config, ret);
        return ret;
    },

    configFormAnalysis: function (config) {
        var ret = [this.configGridChartHistoricalAnalysis(config), this.configFormSpecificAnalysis(config), this.configFormDrillDown(config), this.configGridAlgorithm(config)];
        this.addHiddenMaxMeter(config, ret);
        return ret;
    },

    configFormStatisticalSerie:function(config) {
        var ret = [this.configGridStatisticalSerie(config)];
        this.addHiddenMaxMeter(config, ret);
        return ret;
    },
            /**
             * The configuration for the display tab.
             * @return {object}
             */
            configFormDisplay                             : function(config) {
                var ret =  [{
                            id             : this._displayModeId,
                            fieldLabel     : $lang('msg_chart_displaymode'),
                            name           : 'DisplayMode',
                            allowBlank     : false,
                            xtype          : 'vizComboEnum',
                            enumTypeName   : 'Vizelia.FOL.BusinessEntities.ChartDisplayMode,Vizelia.FOL.Common',
                            sortByValues   : true,
                            displayIcon    : true,
                            value          : Vizelia.FOL.BusinessEntities.ChartDisplayMode.Image,
                            valuesToRemove : config.entity && config.entity.AlgorithmsCount == 0 ? ['Html'] : []
                        }, {
                            fieldLabel   : $lang('msg_chart_type'),
                            name         : 'ChartType',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartType,Vizelia.FOL.Common',
                            displayIcon  : true,
                            value        : Vizelia.FOL.BusinessEntities.ChartType.Combo,
                            listeners    : {
                                select : this.onSelectChartType,
                                scope  : this
                            }
                        }, {
                            fieldLabel   : $lang('msg_dataserie_type'),
                            name         : 'DataSerieType',
                            allowBlank   : false,
                            displayIcon  : true,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.DataSerieType,Vizelia.FOL.Common',
                            value        : Vizelia.FOL.BusinessEntities.DataSerieType.Column
                        }, {
                            fieldLabel   : $lang('msg_gauge_type'),
                            name         : 'GaugeType',
                            allowBlank   : false,
                            displayIcon  : true,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.GaugeType,Vizelia.FOL.Common',
                            value        : Vizelia.FOL.BusinessEntities.GaugeType.Circular
                        }, {
                            fieldLabel : $lang('msg_chart_displayclassifications'),
                            name       : 'DisplayClassifications',
                            xtype      : 'vizcheckbox'
                        }, {
                            fieldLabel : $lang('msg_chart_gridtranspose'),
                            name       : 'GridTranspose',
                            xtype      : 'vizcheckbox'
                        }, {
                            fieldLabel : $lang('msg_chart_gridthideunit'),
                            name       : 'GridHideUnit',
                            xtype      : 'vizcheckbox'
                        }, {
                            fieldLabel : $lang('msg_chart_gridaggregatebycalculatedserieclassification'),
                            name       : 'GridAggregateByCalculatedSerieClassification',
                            xtype      : 'vizcheckbox',
                            checked    : true

                        }, {
                            fieldLabel : $lang('msg_chart_gridtransposelocationwidth'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : true,
                            minValue   : 1,
                            maxValue   : 500,
                            name       : 'GridTransposeLocationWidth',
                            width      : 60
                        }, {
                            fieldLabel : $lang('msg_chart_gridtransposeclassificationwidth'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : true,
                            minValue   : 1,
                            maxValue   : 500,
                            name       : 'GridTransposeClassificationWidth',
                            width      : 60
                        }, {
                            fieldLabel : $lang('msg_chart_customgridrowcount'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : false,
                            minValue   : 1,
                            maxValue   : 60,
                            name       : 'CustomGridRowCount',
                            width      : 60,
                            value      : 5
                        }, {
                            fieldLabel : $lang('msg_chart_customgridcolumncount'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : false,
                            minValue   : 1,
                            maxValue   : 40,
                            name       : 'CustomGridColumnCount',
                            width      : 60,
                            value      : 5
                        }, {
                            fieldLabel : $lang('msg_chart_customgridhidelines'),
                            name       : 'CustomGridHideLines',
                            xtype      : 'vizcheckbox'
                        }, {
                            fieldLabel : $lang('msg_chart_display_3d'),
                            name       : 'Display3D',
                            xtype      : 'vizcheckbox'
                        }, {
                            fieldLabel : $lang('msg_chart_display_stackseries'),
                            xtype      : 'vizcheckbox',
                            name       : 'DisplayStackSeries',
                            checked    : true
                        }, {
                            fieldLabel : $lang('msg_chart_display_fontfamily'),
                            name       : 'DisplayFontFamily',
                            xtype      : 'vizComboFontFamily',
                            value      : 'Verdana'
                        }, {
                            fieldLabel : $lang('msg_chart_display_fontsize'),
                            xtype      : 'vizSpinnerField',
                            name       : 'DisplayFontSize',
                            minValue   : 1,
                            maxValue   : 200,
                            allowBlank : false,
                            width      : 60,
                            value      : 9
                        }, {
                            fieldLabel : $lang('msg_chart_display_fontcolor'),
                            name       : 'DisplayFontColor',
                            allowBlank : true,
                            xtype      : 'colorpickerfield',
                            value      : '000000'
                        }, {
                            fieldLabel : $lang('msg_chart_display_startdateenddate'),
                            name       : 'DisplayStartEndDate',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel : $lang('msg_chart_display_startdateenddate_fontsize'),
                            xtype      : 'vizSpinnerField',
                            name       : 'DisplayStartEndDateFontSize',
                            minValue   : 1,
                            maxValue   : 200,
                            allowBlank : false,
                            width      : 60,
                            value      : 9
                        }, {
                            fieldLabel   : $lang('msg_chart_display_infoicon_position'),
                            name         : 'InfoIconPosition',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartInfoIconPosition,Vizelia.FOL.Common',
                            value        : Vizelia.FOL.BusinessEntities.ChartInfoIconPosition.BottomRight
                        }, {
                            fieldLabel : $lang('msg_chart_detailed_info_icon'),
                            name       : 'DetailedStatusIconEnabled',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel : $lang('msg_chart_display_legend_visible'),
                            name       : 'DisplayLegendVisible',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel   : $lang('msg_chart_display_legend_position'),
                            name         : 'LegendPosition',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartLegendPosition,Vizelia.FOL.Common',
                            value        : Vizelia.FOL.BusinessEntities.ChartLegendPosition.ChartTitle
                        }, {
                            fieldLabel : $lang('msg_chart_display_legend_fontsize'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : false,
                            minValue   : 1,
                            maxValue   : 200,
                            name       : 'DisplayLegendFontSize',
                            width      : 60,
                            value      : 9
                        }, {
                            fieldLabel : $lang('msg_chart_display_legend_plain'),
                            name       : 'DisplayLegendPlain',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel   : $lang('msg_chart_display_legend_sort'),
                            name         : 'DisplayLegendSort',
                            allowBlank   : false,
                            displayIcon  : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartLegendSort,Vizelia.FOL.Common',
                            value        : Vizelia.FOL.BusinessEntities.ChartLegendSort.ByNameAsc
                        },
                         {
                             fieldLabel: $lang('msg_chart_hide_full_spatial_path'),
                             name: 'HideFullSpatialPath',
                             xtype: 'vizcheckbox',
                             checked: true
                         },
                        {
                            fieldLabel : $lang('msg_chart_display_values'),
                            name       : 'DisplayValues',
                            xtype      : 'vizcheckbox'
                        }, {
                            fieldLabel : $lang('msg_chart_display_labeltruncation'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : true,
                            minValue   : 1,
                            maxValue   : 200,
                            name       : 'DisplayLabelTruncationLength',
                            width      : 60,
                            value      : 20
                        }, {
                            fieldLabel : $lang('msg_chart_display_decimalprecision'),
                            xtype      : 'vizSpinnerField',
                            name       : 'DisplayDecimalPrecision',
                            allowBlank : false,
                            minValue   : 0,
                            maxValue   : 5,
                            width      : 60,
                            value      : 0
                        }, {
                            fieldLabel : $lang('msg_chart_display_markersize'),
                            name       : 'DisplayMarkerSize',
                            xtype      : 'vizSpinnerField',
                            minValue   : 0,
                            maxValue   : 200,
                            allowBlank : false,
                            width      : 60,
                            value      : 5
                        }, {
                            fieldLabel : $lang('msg_chart_display_line_thickness'),
                            xtype      : 'vizSpinnerField',
                            name       : 'DisplayLineThickness',
                            minValue   : 0,
                            maxValue   : 200,
                            allowBlank : false,
                            width      : 60,
                            value      : 3
                        }, {
                            fieldLabel   : $lang('msg_chart_display_shaddingeffect'),
                            name         : 'DisplayShaddingEffect',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartShaddingEffect,Vizelia.FOL.Common',
                            sortByValues : true,
                            value        : Vizelia.FOL.BusinessEntities.ChartShaddingEffect.One
                        }, {
                            fieldLabel : $lang('msg_chart_display_transparency'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : false,
                            name       : 'DisplayTransparency',
                            minValue   : 0,
                            maxValue   : 100,
                            width      : 60,
                            value      : 30
                        }, Viz.combo.Palette.factory({
                                    name       : 'KeyPalette',
                                    allowBlank : true,
                                    fieldLabel : $lang('msg_chart_palette')
                                }), {
                            fieldLabel   : $lang('msg_chart_datagridpercentage'),
                            name         : 'DataGridPercentage',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartDataGridPercentage,Vizelia.FOL.Common',
                            sortByValues : true,
                            displayIcon  : true,
                            value        : Vizelia.FOL.BusinessEntities.ChartDataGridPercentage.None
                        }, {
                            fieldLabel : $lang('msg_chart_background'),
                            name       : 'DisplayBackground',
                            allowBlank : true,
                            xtype      : 'colorpickerfield',
                            value      : 'ffffff'
                        }, {
                            fieldLabel : $lang('msg_chart_areastartcolor'),
                            name       : 'DisplayChartAreaBackgroundStart',
                            allowBlank : true,
                            xtype      : 'colorpickerfield',
                            value      : 'ffffff'
                        }, {
                            fieldLabel : $lang('msg_chart_areaendcolor'),
                            name       : 'DisplayChartAreaBackgroundEnd',
                            allowBlank : true,
                            xtype      : 'colorpickerfield',
                            value      : 'cff499'
                        }, Viz.combo.DynamicDisplayImage.factory({
                            fieldLabel : $lang('msg_chart_areapicture'),
                            name       : 'ChartAreaPicture'// ,
                                // store : this.storeDynamicDisplayImage
                        })];
                this.addHiddenMaxMeter(config, ret);
                return ret;

            },
            /**
             * The configuration for the dynamicdisplay tab.
             * @return {object}
             */
            configFormDynamicDisplay                      : function(config) {
                var ret = [{
                            fieldLabel : $lang('msg_chart_use_dynamicdisplay'),
                            name       : 'UseDynamicDisplay',
                            xtype      : 'vizcheckbox',
                            checked    : false
                },
                          Viz.combo.DynamicDisplay.factory({}), {
                    
                              fieldLabel: $lang('msg_dynamicdisplay_culture'),
                              name: 'DynamicDisplayKeyLocalizationCulture',
                              hiddenName: 'DynamicDisplayKeyLocalizationCulture',
                              autoLoad: config.mode == 'create',
                              forceSelection: false,
                              html : '<div></div>',
                              listeners: {
                                  render: function (c) {
                                      new Ext.ToolTip({
                                          target       : c.getEl(),
                                          mouseOffset  : [0, 0],
                                          dismissDelay : 0,
                                          html         : $lang('msg_dynamicdisplayculture_explanation')
                                      });
                                  }
                              },
                              displayFlagForEmptyValue : false,
                              xtype: 'vizComboLocalizationCulture'
                }, {
                            xtype       : 'fieldset',
                            title       : $lang('msg_dynamicdisplay_header'),
                            iconCls     : 'viz-icon-small-dynamicdisplay-header',
                            collapsible : true,
                            collapsed   : true,
                            autoHeight  : true,
                            defaults    : {
                                anchor : Viz.Const.UI.Anchor,
                                xtype  : 'textfield'
                            },
                            items       : [{
                                        id               : config.texareawithintelissenseIdsArray[0],
                                        fieldLabel       : $lang('msg_dynamicdisplay_headertext'),
                                        name             : 'DynamicDisplayHeaderText',
                                        xtype            : 'htmleditor',
                                        enableAlignments : false,
                                        enableLinks      : false,
                                        height           : 120,
                                        plugins          : [new Viz.plugins.IntelliSense({
                                                    delimiterChar  : ',',
                                                    serviceHandler : Viz.Services.EnergyWCF.ChartModel_GetMenuTree,
                                                    serviceParams  : {
                                                        KeyChart : this._KeyChart
                                                    }
                                                })]

                                    }, Viz.combo.DynamicDisplayImage.factory({
                                        fieldLabel : $lang('msg_chart_dynamicdisplayheaderpicture'),
                                        name       : 'DynamicDisplayHeaderPicture'// ,
                                            // store : this.storeDynamicDisplayImage
                                        })]
                        }, {
                            xtype       : 'fieldset',
                            title       : $lang('msg_dynamicdisplay_main'),
                            iconCls     : 'viz-icon-small-dynamicdisplay-main',
                            collapsible : true,
                            collapsed   : true,
                            autoHeight  : true,
                            defaults    : {
                                anchor : Viz.Const.UI.Anchor,
                                xtype  : 'textfield'
                            },
                            items       : [{
                                        id               : config.texareawithintelissenseIdsArray[1],
                                        fieldLabel       : $lang('msg_dynamicdisplay_maintext'),
                                        name             : 'DynamicDisplayMainText',
                                        xtype            : 'htmleditor',
                                        enableAlignments : false,
                                        enableLinks      : false,
                                        height           : 120,
                                        plugins          : [new Viz.plugins.IntelliSense({
                                                    delimiterChar  : ',',
                                                    serviceHandler : Viz.Services.EnergyWCF.ChartModel_GetMenuTree,
                                                    serviceParams  : {
                                                        KeyChart : this._KeyChart
                                                    }
                                                })]
                                    }, Viz.combo.DynamicDisplayImage.factory({
                                        fieldLabel : $lang('msg_chart_dynamicdisplaymainpicture'),
                                        name       : 'DynamicDisplayMainPicture'// ,
                                            // store : this.storeDynamicDisplayImage
                                        })]
                        }, {
                            xtype       : 'fieldset',
                            title       : $lang('msg_dynamicdisplay_footer'),
                            iconCls     : 'viz-icon-small-dynamicdisplay-footer',
                            collapsible : true,
                            collapsed   : true,
                            autoHeight  : true,
                            defaults    : {
                                anchor : Viz.Const.UI.Anchor,
                                xtype  : 'textfield'
                            },
                            items       : [{
                                        id               : config.texareawithintelissenseIdsArray[2],
                                        fieldLabel       : $lang('msg_dynamicdisplay_footertext'),
                                        name             : 'DynamicDisplayFooterText',
                                        xtype            : 'htmleditor',
                                        enableAlignments : false,
                                        enableLinks      : false,
                                        height           : 120,
                                        plugins          : [new Viz.plugins.IntelliSense({
                                                    delimiterChar  : ',',
                                                    serviceHandler : Viz.Services.EnergyWCF.ChartModel_GetMenuTree,
                                                    serviceParams  : {
                                                        KeyChart : this._KeyChart
                                                    }
                                                })]
                                    }, Viz.combo.DynamicDisplayImage.factory({
                                        fieldLabel : $lang('msg_chart_dynamicdisplayfooterpicture'),
                                        name       : 'DynamicDisplayFooterPicture'// ,
                                            // store : this.storeDynamicDisplayImage
                                        })]
                        }];
                this.addHiddenMaxMeter(config, ret);
                return ret;

            },
            /**
             * The configuration for the grid dataseries.
             * @return {object}
             */
            configGridDataSerie                           : function(config) {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_chart_dataseries'),
                    iconCls     : 'viz-icon-small-dataserie-line',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.Anchor
                    },
                    items       : [{
                                fieldLabel : $lang('msg_chart_maxdatapointsperdataserie'),
                                xtype      : 'vizSpinnerField',
                                allowBlank : false,
                                name       : 'MaxDataPointPerDataSerie',
                                minValue   : 1,
                                maxValue   : 5000,
                                width      : 60,
                                value      : 500
                            }, {
                                xtype     : 'vizGridDataSerie',
                                id        : this._gridDataSerie,
                                height    : 200,
                                border    : true,
                                store     : this.storeDataSerie,
                                loadMask  : false,
                                KeyChart  : config.entity ? config.entity.KeyChart : '',
                                listeners : {
                                    beforedeleterecord : function() {
                                        this.deleteSelectedRowsFromGrid(this._gridDataSerie);
                                        return false;
                                    },
                                    scope              : this
                                }
                            }]
                };
            },

            /**
             * The configuration for the grid calculatedseries.
             * @return {object}
             */
            configGridCalculatedSerie                     : function(config) {
                return {
                    xtype     : 'vizGridCalculatedSerie',
                    id        : this._gridCalculatedSerie,
                    border    : true,
                    store     : this.storeCalculatedSerie,
                    loadMask  : false,
                    KeyChart  : config.entity ? config.entity.KeyChart : '',
                    listeners : {
                        beforedeleterecord : function() {
                            this.deleteSelectedRowsFromGrid(this._gridCalculatedSerie);
                            return false;
                        },
                        scope              : this
                    }
                };
            },

            /**
             * The configuration for the grid statisticalseries.
             * @return {object}
             */
            configGridStatisticalSerie                    : function(config) {
                return {
                    xtype     : 'vizGridStatisticalSerie',
                    id        : this._gridStatisticalSerie,
                    border    : true,
                    store     : this.storeStatisticalSerie,
                    loadMask  : false,
                    KeyChart  : config.entity ? config.entity.KeyChart : '',
                    listeners : {
                        beforedeleterecord : function() {
                            this.deleteSelectedRowsFromGrid(this._gridStatisticalSerie);
                            return false;
                        },
                        scope              : this
                    }
                };
            },

            /**
             * The configuration for the form of Axis.
             * @return {object}
             */
            configFormChartAxis                           : function(config) {
                var ret = {
                    xtype       : 'fieldset',
                    title       : $lang('msg_chartaxis'),
                    iconCls     : 'viz-icon-small-chart-axis',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.Anchor,
                        xtype  : 'textfield'
                    },
                    items       : [{
                                fieldLabel     : $lang('msg_chart_xaxisformat'),
                                name           : 'XAxisFormat',
                                hiddenName     : 'XAxisFormat',
                                xtype          : 'combo',
                                mode           : 'local',
                                triggerAction  : 'all',
                                forceSelection : false,
                                editable       : true,
                                displayField   : 'name',
                                valueField     : 'value',
                                store          : new Ext.data.JsonStore({
                                            fields : ['name', 'value'],
                                            data   : [{
                                                        name  : 'dd/MM/yy HH:mm:ss',
                                                        value : 'dd/MM/yy HH:mm:ss'
                                                    }, {
                                                        name  : 'dd/MM/yy',
                                                        value : 'dd/MM/yy'
                                                    }, {
                                                        name  : 'MMMM',
                                                        value : 'MMM'
                                                    }, {
                                                        name  : 'HH:mm',
                                                        value : 'HH:mm'
                                                    }]
                                        })
                            }, {
                                fieldLabel : $lang('msg_chart_yaxisfullstacked'),
                                name       : 'YAxisFullStacked',
                                xtype      : 'vizcheckbox',
                                checked    : false
                            }, {
                                fieldLabel : $lang('msg_chart_xaxis_fontsize'),
                                xtype      : 'vizSpinnerField',
                                allowBlank : true,
                                minValue   : 1,
                                maxValue   : 200,
                                name       : 'DisplayXAxisFontSize',
                                width      : 60
                            }, {
                                fieldLabel : $lang('msg_chart_xaxis_startdayofweek'),
                                name       : 'XAxisStartDayOfWeek',
                                xtype      : 'vizComboCalendarDayName'
                            }, this.configGridChartAxis(config)]
                }
                return ret;
            },
            /**
             * The configuration for the grid axis.
             * @return {object}
             */
            configGridChartAxis                           : function(config) {
                return {
                    xtype     : 'vizGridChartAxis',
                    height    : 200,
                    id        : this._gridChartAxis,
                    border    : true,
                    store     : this.storeChartAxis,
                    loadMask  : false,
                    KeyChart  : config.entity ? config.entity.KeyChart : '',
                    listeners : {
                        beforedeleterecord : function() {
                            this.deleteSelectedRowsFromGrid(this._gridChartAxis);
                            return false;
                        },
                        scope              : this
                    }
                };
            },

            /**
             * The configuration for the grid of charthistoricalanalysis.
             * @return {object}
             */
            configGridChartHistoricalAnalysis             : function(config) {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_charthistoricalanalysis'),
                    iconCls     : 'viz-icon-small-chart-historicalanalysis',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : {
                        xtype     : 'vizGridChartHistoricalAnalysis',
                        id        : this._gridChartHistoricalAnalysis,
                        border    : true,
                        store     : this.storeChartHistoricalAnalysis,
                        loadMask  : false,
                        KeyChart  : config.entity ? config.entity.KeyChart : '',
                        listeners : {
                            beforeaddrecord    : function(grid, entity) {
                                if (this.entity && this.entity.LocalId) {
                                    // This will cause the new entity to have this text
                                    // as default in the Local ID field.
                                    entity.LocalId = this.entity.LocalId + '_';
                                }
                            },
                            beforedeleterecord : function() {
                                this.deleteSelectedRowsFromGrid(this._gridChartHistoricalAnalysis);
                                return false;
                            },
                            scope              : this
                        },
                        height    : 200
                    }
                };
            },

            configFormSpecificAnalysis                    : function(config) {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_chart_specificanalysis'),
                    iconCls     : 'viz-icon-small-chart-specificanalysis',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.Anchor
                    },
                    items       : [{
                                fieldLabel   : $lang('msg_chart_specificanalysis'),
                                name         : 'SpecificAnalysis',
                                allowBlank   : false,
                                xtype        : 'vizComboEnum',
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartSpecificAnalysis,Vizelia.FOL.Common',
                                displayIcon  : true,
                                value        : Vizelia.FOL.BusinessEntities.ChartSpecificAnalysis.None,
                                listeners    : {
                                    select : this.onSelectSpecificAnalysis,
                                    scope  : this
                                }
                            }, {
                                xtype      : 'fieldset',
                                id         : this._fieldsetHeatMap,
                                hidden     : true,
                                title      : $lang('msg_enum_chartspecificanalysis_heatmap'),
                                labelWidth : 150,
                                defaults   : {
                                    msgTarget : 'side',
                                    anchor    : Viz.Const.UI.Anchor
                                },
                                items      : [{
                                            fieldLabel : $lang('msg_chart_heatmapmincolor'),
                                            name       : 'HeatMapMinColor',
                                            xtype      : 'colorpickerfield',
                                            value      : '00ff00'
                                        }, {
                                            fieldLabel : $lang('msg_chart_heatmapmaxcolor'),
                                            name       : 'HeatMapMaxColor',
                                            xtype      : 'colorpickerfield',
                                            value      : 'ff0000'
                                        }]
                            }, {
                                xtype      : 'fieldset',
                                id         : this._fieldsetCorrelation,
                                title      : $lang('msg_enum_chartspecificanalysis_correlation'),
                                hidden     : true,
                                labelWidth : 150,
                                defaults   : {
                                    msgTarget : 'side',
                                    anchor    : Viz.Const.UI.Anchor
                                },
                                items      : [{
                                            fieldLabel      : $lang('msg_chart_correlationxaxisserie'),
                                            name            : 'CorrelationXSerieLocalId',
                                            xtype           : 'vizComboChartAvailableDataSerieLocalId',
                                            KeyChart        : config.entity ? config.entity.KeyChart : '',
                                            // store : this.storeChartAvailableDataSerieLocalId,
                                            includeExisting : true
                                        }, {
                                            fieldLabel      : $lang('msg_chart_correlationyaxisserie'),
                                            name            : 'CorrelationYSerieLocalId',
                                            xtype           : 'vizComboChartAvailableDataSerieLocalId',
                                            KeyChart        : config.entity ? config.entity.KeyChart : '',
                                            // store : this.storeChartAvailableDataSerieLocalId,
                                            includeExisting : true
                                        }, {
                                            fieldLabel      : $lang('msg_chart_correlationxaxisserie') + ' 2',
                                            name            : 'CorrelationXSerieLocalId2',
                                            xtype           : 'vizComboChartAvailableDataSerieLocalId',
                                            KeyChart        : config.entity ? config.entity.KeyChart : '',
                                            // store : this.storeChartAvailableDataSerieLocalId,
                                            includeExisting : true
                                        }, {
                                            fieldLabel      : $lang('msg_chart_correlationyaxisserie') + ' 2',
                                            name            : 'CorrelationYSerieLocalId2',
                                            xtype           : 'vizComboChartAvailableDataSerieLocalId',
                                            KeyChart        : config.entity ? config.entity.KeyChart : '',
                                            // store : this.storeChartAvailableDataSerieLocalId,
                                            includeExisting : true
                                        }]
                            }, {
                                xtype      : 'fieldset',
                                id         : this._fieldsetEnergyCertificate,
                                title      : $lang('msg_enum_chartspecificanalysis_energycertificate'),
                                hidden     : true,
                                labelWidth : 150,
                                defaults   : {
                                    msgTarget : 'side',
                                    anchor    : Viz.Const.UI.Anchor
                                },
                                items      : [{
                                            fieldLabel : $lang('msg_energycertificate'),
                                            name       : 'KeyEnergyCertificate',
                                            allowBlank : true,
                                            xtype      : 'vizComboEnergyCertificate',
                                            flex       : 1
                                        }, {
                                            fieldLabel : $lang('msg_chart_energycertificate_target'),
                                            name       : 'EnergyCertificateTarget',
                                            xtype      : 'vizSpinnerField',
                                            allowBlank : true
                                        }, {
                                            fieldLabel : $lang('msg_chart_energycertificate_displayelements'),
                                            name       : 'EnergyCertificateDisplayElements',
                                            xtype      : 'vizcheckbox',
                                            checked    : false
                                        }]
                            }, {
                                xtype      : 'fieldset',
                                id         : this._fieldsetDifferenceHighlight,
                                title      : $lang('msg_enum_chartspecificanalysis_differencehighlight'),
                                hidden     : true,
                                labelWidth : 150,
                                defaults   : {
                                    msgTarget : 'side',
                                    anchor    : Viz.Const.UI.Anchor
                                },
                                items      : [{
                                            fieldLabel : $lang('msg_chart_differencehighlightthreshold'),
                                            xtype      : 'vizSpinnerField',
                                            name       : 'DifferenceHighlightSerieThreshold',
                                            minValue   : 0,
                                            maxValue   : 100,
                                            allowBlank : false,
                                            width      : 60,
                                            value      : 0
                                        }, {
                                            fieldLabel      : $lang('msg_chart_differencehighlightserie1'),
                                            name            : 'DifferenceHighlightSerie1LocalId',
                                            xtype           : 'vizComboChartAvailableDataSerieLocalId',
                                            KeyChart        : config.entity ? config.entity.KeyChart : '',
                                            // store : this.storeChartAvailableDataSerieLocalId,
                                            includeExisting : true
                                        }, {
                                            fieldLabel      : $lang('msg_chart_differencehighlightserie2'),
                                            name            : 'DifferenceHighlightSerie2LocalId',
                                            xtype           : 'vizComboChartAvailableDataSerieLocalId',
                                            KeyChart        : config.entity ? config.entity.KeyChart : '',
                                            // store : this.storeChartAvailableDataSerieLocalId,
                                            includeExisting : true
                                        }]
                            }, {
                                xtype      : 'fieldset',
                                id         : this._fieldsetMicroChart,
                                title      : $lang('msg_enum_chartspecificanalysis_microchart'),
                                hidden     : true,
                                labelWidth : 150,
                                defaults   : {
                                    msgTarget : 'side',
                                    anchor    : Viz.Const.UI.Anchor
                                },
                                items      : [{
                                            fieldLabel : $lang('msg_chart_microcharttarget'),
                                            xtype      : 'vizSpinnerField',
                                            name       : 'MicroChartTarget',
                                            allowBlank : true,
                                            width      : 60,
                                            value      : 0
                                        }, {
                                            fieldLabel : $lang('msg_chart_microcharttargetelement'),
                                            xtype      : 'vizSpinnerField',
                                            name       : 'MicroChartTargetElement',
                                            allowBlank : true,
                                            width      : 60,
                                            value      : 0
                                        }, {
                                            fieldLabel : $lang('msg_chart_microchart_displayelements'),
                                            name       : 'MicroChartDisplayElements',
                                            xtype      : 'vizcheckbox',
                                            checked    : false
                                        }]
                            }, {
                                xtype      : 'fieldset',
                                id         : this._fieldsetCalendarView,
                                title      : $lang('msg_enum_chartspecificanalysis_calendarview'),
                                hidden     : true,
                                labelWidth : 150,
                                defaults   : {
                                    msgTarget : 'side',
                                    anchor    : Viz.Const.UI.Anchor
                                },
                                items      : [{
                                            fieldLabel     : $lang('msg_chart_timeinterval'),
                                            name           : 'CalendarViewTimeInterval',
                                            allowBlank     : false,
                                            xtype          : 'vizComboEnum',
                                            enumTypeName   : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common',
                                            value          : Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days,
                                            sortByValues   : true,
                                            displayIcon    : true,
                                            valuesToRemove : ['Global', 'Weeks', 'Years', 'Weeks', 'Minutes', 'Seconds', 'Semesters']
                                        }]
                            }]
                };
            },

            configFormDrillDown                           : function(config) {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_chartdrilldown'),
                    hidden      : !$authorized('@@@@ ChartDrillDown - Full Control'),
                    iconCls     : 'viz-icon-small-chartviewer',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.Anchor
                    },
                    items       : [{
                                fieldLabel   : $lang('msg_chartdrilldown'),
                                name         : 'DrillDownMode',
                                allowBlank   : false,
                                sortByValues : true,
                                xtype        : 'vizComboEnum',
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartDrillDownMode,Vizelia.FOL.Common',
                                value        : Vizelia.FOL.BusinessEntities.ChartDrillDownMode.Autonomous
                            }]
                };
            },

            configGridAlgorithm                           : function(config) {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_algorithm'),
                    hidden      : !$authorized('@@@@ Chart'),
                    iconCls     : 'viz-icon-small-python',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.Anchor
                    },
                    items       : $authorized('@@@@ Chart') ? {
                        xtype     : 'vizGridChartAlgorithm',
                        id        : this._gridChartAlgorithm,
                        border    : true,
                        store     : this.storeChartAlgorithm,
                        loadMask  : false,
                        KeyChart  : config.entity ? config.entity.KeyChart : '',
                        listeners : {
                            beforeaddrecord    : function(grid, entity) {
                                if (this.entity && this.entity.LocalId) {
                                    // This will cause the new entity to have this text
                                    // as default in the Local ID field.
                                    entity.LocalId = this.entity.LocalId + '_';
                                }
                            },
                            beforedeleterecord : function() {
                                this.deleteSelectedRowsFromGrid(this._gridChartAlgorithm);
                                return false;
                            },
                            scope              : this
                        },
                        height    : 200
                    } : null
                };
            },

            /**
             * Handler for the ChartType combo select event.
             */
            onSelectSpecificAnalysis                      : function(combo, record, index) {
                this.setSpecificAnalysisFrom(combo.getValue());
            },

            /**
             * Update the visible fieldset in the SpecificAnalysis form
             * @param {} specificanalysis
             */
            setSpecificAnalysisFrom                       : function(specificanalysis) {
                Ext.getCmp(this._fieldsetHeatMap).setVisible(false);
                Ext.getCmp(this._fieldsetCorrelation).setVisible(false);
                Ext.getCmp(this._fieldsetEnergyCertificate).setVisible(false);
                Ext.getCmp(this._fieldsetDifferenceHighlight).setVisible(false);
                Ext.getCmp(this._fieldsetMicroChart).setVisible(false);
                Ext.getCmp(this._fieldsetCalendarView).setVisible(false);
                switch (specificanalysis) {
                    case Vizelia.FOL.BusinessEntities.ChartSpecificAnalysis.HeatMap :
                        Ext.getCmp(this._fieldsetHeatMap).setVisible(true);
                        break;
                    case Vizelia.FOL.BusinessEntities.ChartSpecificAnalysis.Correlation :
                        Ext.getCmp(this._fieldsetCorrelation).setVisible(true);
                        break;
                    case Vizelia.FOL.BusinessEntities.ChartSpecificAnalysis.EnergyCertificate :
                        Ext.getCmp(this._fieldsetEnergyCertificate).setVisible(true);
                        break;
                    case Vizelia.FOL.BusinessEntities.ChartSpecificAnalysis.DifferenceHighlight :
                        Ext.getCmp(this._fieldsetDifferenceHighlight).setVisible(true);
                        break;
                    case Vizelia.FOL.BusinessEntities.ChartSpecificAnalysis.MicroChart :
                        Ext.getCmp(this._fieldsetMicroChart).setVisible(true);
                        break;
                    case Vizelia.FOL.BusinessEntities.ChartSpecificAnalysis.CalendarView :
                        Ext.getCmp(this._fieldsetCalendarView).setVisible(true);
                        break;
                }
            },

            /**
             * The configuration for the grid of chartmarkers..
             * @return {object}
             */
            configFormChartMarker                         : function(config) {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_chartmarker'),
                    iconCls     : 'viz-icon-small-chart-marker',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    labelWidth  : 120,
                    defaults    : {
                        anchor : Viz.Const.UI.Anchor,
                        xtype  : 'textfield'
                    },
                    items       : [{
                                xtype      : 'vizcompositefield',
                                fieldLabel : $lang('msg_chart_night_starttime'),
                                items      : [{
                                            fieldLabel : $lang('msg_chart_night_starttime'),
                                            name       : 'NightStartTime',
                                            xtype      : 'timefield',
                                            format     : 'H:i',
                                            value      : new Date(0, 0, 1, 22, 0, 0),
                                            width      : 80
                                        }, {
                                            xtype : 'displayfield',
                                            cls   : 'viz-form-display-field-inside-composite',
                                            value : $lang('msg_chart_night_endtime') + ':'
                                        }, {
                                            fieldLabel : $lang('msg_chart_night_endtime'),
                                            name       : 'NightEndTime',
                                            xtype      : 'timefield',
                                            format     : 'H:i',
                                            value      : new Date(0, 0, 1, 6, 0, 0),
                                            width      : 80
                                        }, {
                                            xtype : 'displayfield',
                                            cls   : 'viz-form-display-field-inside-composite',
                                            value : $lang('msg_color') + ':'
                                        }, {
                                            fieldLabel : $lang('msg_color'),
                                            name       : 'NightColor',
                                            xtype      : 'colorpickerfield',
                                            value      : '',
                                            /*
                                             * listeners : { afterrender : function(field) { if (!Ext.isEmpty(this.entity.NightColorR)) { field.setValue(new Viz.RGBColor(String.format("rgb({0},{1},{2})", this.entity.NightColorR, this.entity.NightColorG, this.entity.NightColorB)).toHex()); } }, scope : this },
                                             */
                                            flex       : 1
                                        }]
                            }, this.configGridChartMarker(config)]
                };
            },

            /**
             * The configuration for the grid axis.
             * @return {object}
             */
            configGridChartMarker                         : function(config) {
                return {
                    xtype     : 'vizGridChartMarker',
                    height    : 200,
                    id        : this._gridChartMarker,
                    border    : true,
                    store     : this.storeChartMarker,
                    loadMask  : false,
                    KeyChart  : config.entity ? config.entity.KeyChart : '',
                    listeners : {
                        beforedeleterecord : function() {
                            this.deleteSelectedRowsFromGrid(this._gridChartMarker);
                            return false;
                        },
                        scope              : this
                    }
                };
            },
            /**
             * Handler when a dataserie has been changed.
             * @param {Object} item
             * @return {Boolean}
             */
            onDataSerieChange                             : function(item) {
                if (item.KeyChart == this.entity.KeyChart) {
                    if (this.storeDataSerie)
                        this.storeDataSerie.reload();
                    Viz.util.MessageBusMgr.publish('ChartChange', this.entity);
                }
                return true;
            },

            /**
             * Handler when a calculatedserie has been changed.
             * @param {Object} item
             * @return {Boolean}
             */
            onCalculatedSerieChange                       : function(item) {
                if (item.KeyChart == this.entity.KeyChart) {
                    if (this.storeCalculatedSerie)
                        this.storeCalculatedSerie.reload();
                    // this.storeChartAvailableDataSerieLocalId.reload();
                    Viz.util.MessageBusMgr.publish('ChartChange', this.entity);
                }
                return true;
            },

            /**
             * Handler when a calculatedserie has been changed.
             * @param {Object} item
             * @return {Boolean}
             */
            onStatisticalSerieChange                      : function(item) {
                if (item.KeyChart == this.entity.KeyChart) {
                    if (this.storeStatisticalSerie)
                        this.storeStatisticalSerie.reload();
                    // this.storeChartAvailableDataSerieLocalId.reload();
                    Viz.util.MessageBusMgr.publish('ChartChange', this.entity);
                }
                return true;
            },
            /**
             * Handler when a historicalanalysis has been changed.
             * @param {Object} item
             * @return {Boolean}
             */
            onChartHistoricalAnalysisChange               : function(item) {
                if (item.KeyChart == this.entity.KeyChart) {
                    if (this.storeChartHistoricalAnalysis)
                        this.storeChartHistoricalAnalysis.reload();
                    Viz.util.MessageBusMgr.publish('ChartChange', this.entity);
                }
                return true;
            },

            /**
             * Handler when a chart has been changed.
             * @param {Object} item
             * @return {Boolean}
             */
            onChartChange                                 : function(item) {
                if (item.KeyChart == this.entity.KeyChart) {
                    if (displayMode) {
                        var displayMode = Ext.getCmp(this._displayModeId);
                        displayMode.store.valuesToRemove = item.AlgorithmsCount == 0 ? ['Html'] : [];
                        displayMode.store.removeAll();
                        displayMode.store.load();
                        displayMode.ownerCt.doLayout();
                    }
                    this.entity = item;
                    this.getForm().setValues(this.entity);
                }
                return true;
            },
            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                                  : function(form, item, extraParams) {
                var storeArray = this.getExtraStoreArray();

                Ext.each(storeArray, function(store) {
                            if (store.hasLoaded)
                                store.save();
                        }, this);

                Ext.apply(extraParams, {
                            filterSpatial                       : this.storeFilterSpatial ? this.storeFilterSpatial.crudStore : null,
                            filterMeterClassification           : this.storeFilterMeterClassification ? this.storeFilterMeterClassification.crudStore : null,
                            filterEventLogClassification        : this.storeFilterEventLogClassification ? this.storeFilterEventLogClassification.crudStore : null,
                            filterAlarmDefinitionClassification : this.storeFilterAlarmDefinitionClassification ? this.storeFilterAlarmDefinitionClassification.crudStore : null,
                            filterAzManRoles                    : this.storeFilterKPIAzManRole ? this.storeFilterKPIAzManRole.crudStore : null,
                            meters                              : this.storeMeter ? this.storeMeter.crudStore : null,
                            dataseries                          : this.storeDataSerie ? this.storeDataSerie.crudStore : null,
                            calculatedseries                    : this.storeCalculatedSerie ? this.storeCalculatedSerie.crudStore : null,
                            statisticalseries                   : this.storeStatisticalSerie ? this.storeStatisticalSerie.crudStore : null,
                            chartaxis                           : this.storeChartAxis ? this.storeChartAxis.crudStore : null,
                            chartmarker                         : this.storeChartMarker ? this.storeChartMarker.crudStore : null,
                            charthistoricalanalysis             : this.storeChartHistoricalAnalysis ? this.storeChartHistoricalAnalysis.crudStore : null,
                            chartfilterpset                     : this.storeFilterSpatialPset ? this.storeFilterSpatialPset.crudStore : null,
                            historicals                         : this.storeChartPsetAttributeHistorical ? this.storeChartPsetAttributeHistorical.crudStore : null,
                            algorithms                          : this.storeChartAlgorithm ? this.storeChartAlgorithm.crudStore : null
                        });

                var nightColor = this.getForm().findField('NightColor');
                if (nightColor) {
                    var value = nightColor.getValue();
                    if (value) {
                        var rgbArray = new Viz.RGBColor(value).toRGBArray();
                        item.NightColorR = rgbArray[0];
                        item.NightColorG = rgbArray[1];
                        item.NightColorB = rgbArray[2];
                    }
                    else {
                        item.NightColorR = null;
                        item.NightColorG = null;
                        item.NightColorB = null;
                    }
                }
                if (item.EnergyCertificateTarget === '')
                    item.EnergyCertificateTarget = null;
                if (item.MicroChartTargetElement === '')
                    item.MicroChartTargetElement = null;
                if (item.MicroChartTarget === '')
                    item.MicroChartTarget = null;
                if (item.DisplayXAxisFontSize === '')
                    item.DisplayXAxisFontSize = null;
                if (item.DisplayLabelTruncationLength === '')
                    item.DisplayLabelTruncationLength = null;
                if (!Ext.isNumber(item.MaxDataPointPerDataSerie))
                    item.MaxDataPointPerDataSerie = 500;

                if (item.IsKPI && Ext.isEmpty(item.KeyClassificationItemKPI)) {
                    var field = form.getForm().findField('KeyClassificationItemKPI');
                    field.markInvalid(field.blankText);
                    return false;
                }

                return true;
            },

            /**
             * Handler on the beforeadd event of the grid FilterMeterClassification
             */
            onBeforeAddFilterMeterClassification          : function(grid, entity) {
                if (this.storeFilterSpatial.hasLoaded)
                    this.storeFilterSpatial.save();

                if (!this.storeFilterSpatial.data || this.storeFilterSpatial.data.length == 0) {
                    Ext.MessageBox.alert($lang("msg_chart"), $lang("msg_chart_wizard_spatial_empty"));
                    return false;
                }

                entity.KeyChart = this.entity.KeyChart;
                entity.filterSpatial = this.storeFilterSpatial.crudStore;

                return true;
            },

            /**
             * Handler for updating fiels based on the form values.
             * @param {Viz.form.FormPanel} the form panel
             * @param {Object} values
             */
            onAfterLoadValues                             : function(formpanel, values) {
                var form = this.getForm();

                if (Ext.isEmpty(this.uniqueTab) || this.uniqueTab == 'display') {
                    this.updateFieldEnableorDisable('DataSerieType', values.ChartType, [Vizelia.FOL.BusinessEntities.ChartType.Combo, Vizelia.FOL.BusinessEntities.ChartType.ComboHorizontal, Vizelia.FOL.BusinessEntities.ChartType.Radar]);
                    this.updateFieldEnableorDisable('GaugeType', values.ChartType, Vizelia.FOL.BusinessEntities.ChartType.Gauge);
                }
                if (Ext.isEmpty(this.uniqueTab) || this.uniqueTab == 'general') {
                    this.updateFieldEnableorDisable('LocalisationSiteLevel', values.Localisation, Vizelia.FOL.BusinessEntities.ChartLocalisation.BySite);
                    this.onCheckDynamicTimeScaleEnabled(form.findField("DynamicTimeScaleEnabled"), values.DynamicTimeScaleEnabled);
                }
                if (this.mode == 'update' && Ext.isEmpty(this.uniqueTab) || this.uniqueTab == 'analysis') {
                    this.setSpecificAnalysisFrom(values.SpecificAnalysis);
                }
                if (Ext.isEmpty(this.uniqueTab) || this.uniqueTab == 'dataseries') {
                    var night = this.getForm().findField('NightColor');
                    if (night && !Ext.isEmpty(values.NightColorR))
                        night.setValue(new Viz.RGBColor(String.format("rgb({0},{1},{2})", values.NightColorR, values.NightColorG, values.NightColorB)).toHex());
                }
                var endDateField = form.findField('EndDate');
                var startDateField = form.findField('StartDate');
                var timeZoneField = form.findField('TimeZoneId');

                if (endDateField) {
                    endDateField.setTimeZoneValue(timeZoneField.value);
                    endDateField.disableTimeZoneComboBox();
                }

                if (startDateField) {
                    startDateField.setTimeZoneValue(timeZoneField.value);
                    startDateField.disableTimeZoneComboBox();
                }

                if (this.entity) {
                    if (this.entity.StartDate && this.entity.TimeZoneId) {
                        this.ConvertDateToTimeZone(this, this.entity.StartDate, this.entity.TimeZoneId, startDateField);
                    }
                    if (this.entity.EndDate && this.entity.TimeZoneId) {
                        this.ConvertDateToTimeZone(this, this.entity.EndDate, this.entity.TimeZoneId, endDateField);
                    }
                }
                if (this.entity.MaxMeterOverflowError) {
                    var maxMeter = form.findField("MaxMeter");
                    if (maxMeter) {
                        maxMeter.markInvalid(this.entity.MaxMeterOverflowError);
                    } 
                }
            },

            ConvertDateToTimeZone                         : function(scope, date, timeZone, fieldToChange) {
                Viz.Services.EnergyWCF.ConvertDateToTimeZone({
                            scope      : scope,
                            date       : date,
                            timeZoneId : timeZone,
                            success    : function(response) {
                                if (fieldToChange) {
                                    // var userOffset = new Date().getTimezoneOffset() * 60000;
                                    var dateOffset = new Date(response.getTime()).getTimezoneOffset() * 60000;
                                    var time = new Date(response.getTime() + dateOffset); // changing the date recieve to ignore browser time zone offset
                                    fieldToChange.setValue(time);
                                }

                            }
                        });
            },

            /**
             * Handler for the ChartType combo select event.
             */
            onSelectChartType                             : function(combo, record, index) {
                this.updateFieldEnableorDisable('DataSerieType', record.data.Value, [Vizelia.FOL.BusinessEntities.ChartType.Combo, Vizelia.FOL.BusinessEntities.ChartType.ComboHorizontal, Vizelia.FOL.BusinessEntities.ChartType.Radar]);
                this.updateFieldEnableorDisable('GaugeType', record.data.Value, Vizelia.FOL.BusinessEntities.ChartType.Gauge);
            },

            /**
             * Handler for the ChartType combo select event.
             */
            onSelectLocalisation                          : function(combo, record, index) {
                this.updateFieldEnableorDisable('LocalisationSiteLevel', record.data.Value, Vizelia.FOL.BusinessEntities.ChartLocalisation.BySite);
            },

            updateFieldEnableorDisable                    : function(fieldName, currentValue, enableValues) {
                if (!Ext.isArray(enableValues)) {
                    enableValues = [enableValues];
                }

                var form = this.getForm();
                var field = form.findField(fieldName);
                if (field) {
                    if (enableValues.indexOf(currentValue) < 0) {
                        // field.setValue(0);
                        field.disable();
                    }
                    else {
                        field.enable();
                    }
                }
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                                 : function(form, action) {
                if (this.closeonsave !== true) {
                    Viz.form.Chart.superclass.onSaveSuccess.apply(this, arguments);
                    var tabpanel = this.getTabContainer();

                    // if (!Ext.isEmpty(this.getForm().findField("StartDate"))) {
                    // this.getForm().findField("StartDate").setValue(Viz.date.convertToLocalDate(this.entity.StartDate,
                    // this.entity.TimeZoneOffset[0]));
                    // this.getForm().findField("EndDate").setValue(Viz.date.convertToLocalDate(this.entity.EndDate,
                    // this.entity.TimeZoneOffset[1]));
                    // }

                    this._KeyChart = this.entity.KeyChart;
                    var storeArray = this.getExtraStoreArray();
                    if (this.storeMeterAll)
                        storeArray.push(this.storeMeterAll);

                    Ext.each(storeArray, function(store) {
                                store.serviceParams.KeyChart = this.entity.KeyChart;
                                if (store.hasLoaded)
                                    store.reload();
                            }, this);

                    var gridArray = this.getExtraGridArray();
                    Ext.each(gridArray, function(gridId) {
                                var grid = Ext.getCmp(gridId);
                                if (grid)
                                    grid.KeyChart = this.entity.KeyChart;
                            }, this);

                    if (Ext.isEmpty(this.uniqueTab) || this.uniqueTab == 'dynamicdisplay') {
                        Viz.plugins.IntelliSense.synchronizeFields(this.texareawithintelissenseIdsArray);
                    }
                    if (Ext.isEmpty(this.uniqueTab)) {
                        tabpanel.unhideTabStripItem(this._tabMeterAll);
                        tabpanel.unhideTabStripItem(this._tabDataSeriesAndAxis);
                        tabpanel.unhideTabStripItem(this._tabCalculatedSerie);
                        tabpanel.unhideTabStripItem(this._tabStatisticalSerie);
                        tabpanel.unhideTabStripItem(this._tabSpecificAnalysis);
                    }

                    var gridSpatialPset = Ext.getCmp(this._gridFilterSpatialPset);
                    if (gridSpatialPset) {
                        gridSpatialPset.show();
                    }
                    var chartForm = form.getForm();
                    var startDateField = chartForm.findField('StartDate');
                    var endDateField = chartForm.findField('EndDate');
                    var timeZoneField = chartForm.findField('TimeZoneId');

                    if (endDateField) {
                        endDateField.setTimeZoneValue(timeZoneField.value);
                        endDateField.disableTimeZoneComboBox();
                    }
                    if (startDateField) {
                        startDateField.setTimeZoneValue(timeZoneField.value);
                        startDateField.disableTimeZoneComboBox();
                    }

                    this.ConvertDateToTimeZone(this, this.entity.StartDate, this.entity.TimeZoneId, startDateField);
                    this.ConvertDateToTimeZone(this, this.entity.EndDate, this.entity.TimeZoneId, endDateField);
                }
                return true;
            },

            /**
             * Return an array of all the extra store of the Chart.
             * @return {Array}
             */
            getExtraStoreArray                            : function() {
                var stores = [];
                if (this.storeFilterSpatial)
                    stores.push(this.storeFilterSpatial);
                if (this.storeFilterMeterClassification)
                    stores.push(this.storeFilterMeterClassification);
                if (this.storeFilterEventLogClassification)
                    stores.push(this.storeFilterEventLogClassification);
                if (this.storeFilterAlarmDefinitionClassification)
                    stores.push(this.storeFilterAlarmDefinitionClassification);
                if (this.storeMeter)
                    stores.push(this.storeMeter);
                if (this.storeDataSerie)
                    stores.push(this.storeDataSerie);
                if (this.storeCalculatedSerie)
                    stores.push(this.storeCalculatedSerie);
                if (this.storeStatisticalSerie)
                    stores.push(this.storeStatisticalSerie);
                if (this.storeChartAxis)
                    stores.push(this.storeChartAxis);
                if (this.storeChartMarker)
                    stores.push(this.storeChartMarker);
                if (this.storeChartHistoricalAnalysis)
                    stores.push(this.storeChartHistoricalAnalysis);
                if (this.storeChartAlgorithm)
                    stores.push(this.storeChartAlgorithm);
                if (this.storeFilterSpatialPset)
                    stores.push(this.storeFilterSpatialPset);
                if (this.storeChartPsetAttributeHistorical)
                    stores.push(this.storeChartPsetAttributeHistorical);
                if (this.storeFilterKPIAzManRole)
                    stores.push(this.storeFilterKPIAzManRole);

                return stores; // , this.storeChartAvailableDataSerieLocalId
            },
            /**
             * Return an array of all the extra grid of the Chart.
             * @return {Array}
             */
            getExtraGridArray                             : function() {
                return [this._gridDataSerie, this._gridCalculatedSerie, this._gridStatisticalSerie, this._gridChartAxis, this._gridChartPsetAttributeHistorical, this._gridChartMarker, this._gridChartHistoricalAnalysis, this._gridChartAlgorithm, this._gridFilterSpatialPset, this._gridFilterKPIAzManRole];
            },
            /**
             * Handler for check DynamicTimeScaleEnabled checkbox.
             * @param {Object} comp
             * @param {Bool} value
             */
            onCheckDynamicTimeScaleEnabled                : function(comp, value) {
                var form = this.getForm();
                if (value) {
                    var f = form.findField("StartDate");
                    if (f)
                        f.disable();
                    f = form.findField("EndDate");
                    if (f)
                        f.disable();

                    f = form.findField("DynamicTimeScaleFrequency");
                    if (f)
                        f.enable();
                    f = form.findField("DynamicTimeScaleInterval");
                    if (f)
                        f.enable();
                    f = form.findField("DynamicTimeScaleCalendar");
                    if (f)
                        f.enable();
                    f = form.findField("DynamicTimeScaleCompleteInterval");
                    if (f)
                        f.enable();

                    f = form.findField("DynamicTimeScaleCalendarEndToNow").disable();
                    if (f)
                        f.enable();
                }
                else {
                    form.findField("StartDate").enable();
                    form.findField("EndDate").enable();

                    form.findField("DynamicTimeScaleFrequency").disable();
                    form.findField("DynamicTimeScaleInterval").disable();
                    form.findField("DynamicTimeScaleCalendar").disable();
                    form.findField("DynamicTimeScaleCompleteInterval").disable();
                    form.findField("DynamicTimeScaleCalendarEndToNow").disable();
                }
            },

            /**
             * Handler for check DynamicTimeScaleCalendarEndToNow checkbox.
             * @param {Object} comp
             * @param {Bool} value
             */
            onCheckDynamicTimeScaleCalendarEndToNow       : function(comp, value) {
                var form = this.getForm();
                var f = form.findField("DynamicTimeScaleCompleteInterval");
                if (value && f.checked)
                    f.setValue(false);
            },

            /**
             * Handler for check DynamicTimeScaleCompleteInterval checkbox.
             * @param {Object} comp
             * @param {Bool} value
             */
            onCheckDynamicTimeScaleCompleteInterval       : function(comp, value) {
                var form = this.getForm();
                var f = form.findField("DynamicTimeScaleCalendarEndToNow");
                if (value && f.checked)
                    f.setValue(false);
            }

        });
Ext.reg('vizFormChart', Viz.form.Chart);

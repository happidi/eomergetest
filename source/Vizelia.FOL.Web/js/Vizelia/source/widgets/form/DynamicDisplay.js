﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.DynamicDisplay
 * @extends Ext.FormPanel.
 */
Viz.form.DynamicDisplay = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                  : function(config) {
                config = config || {};
                config.texareawithintelissenseIdsArray = [Ext.id(), Ext.id(), Ext.id()];

                this.storeDynamicDisplayImageArray = [new Viz.store.DynamicDisplayImage({}), new Viz.store.DynamicDisplayImage({}), new Viz.store.DynamicDisplayImage({})];

                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.DynamicDisplay_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.DynamicDisplay_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.DynamicDisplay_FormUpdate,
                    tabConfig            : {
                        deferredRender : true,
                        items          : [{
                                    title      : $lang('msg_general'),
                                    iconCls    : 'viz-icon-small-dynamicdisplay',
                                    items      : this.configFormGeneral(config),
                                    labelWidth : 250
                                }, {
                                    title   : $lang('msg_dynamicdisplay_header'),
                                    iconCls : 'viz-icon-small-dynamicdisplay-header',
                                    items   : this.configFormHeader(config)
                                }, {
                                    title   : $lang('msg_dynamicdisplay_main'),
                                    iconCls : 'viz-icon-small-dynamicdisplay-main',
                                    items   : this.configFormMain(config)
                                }, {
                                    title   : $lang('msg_dynamicdisplay_footer'),
                                    iconCls : 'viz-icon-small-dynamicdisplay-footer',
                                    items   : this.configFormFooter(config)
                                }, {
                                    title   : $lang('msg_dynamicdisplay_boguschart'),
                                    iconCls : 'viz-icon-small-chart',
                                    items   : this.configFormBogusChart(config)
                                }]
                    },
                    labelWidth           : 180,
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.DynamicDisplay.superclass.constructor.call(this, config);

                // Inits the tree model once for all the DynamicDisplay fields
                Viz.plugins.IntelliSense.synchronizeFields(config.texareawithintelissenseIdsArray);

            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral            : function(config) {
                return [{
                            name   : 'KeyDynamicDisplay',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_dynamicdisplay_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_id'),
                            name       : 'LocalId',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description',
                            xtype      : 'textarea',
                            allowBlank : true
                        }, {
                            fieldLabel   : $lang('msg_dynamicdisplay_type'),
                            name         : 'Type',
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.DynamicDisplayType,Vizelia.FOL.Common',
                            sortByValues : true,
                            value        : "0"
                        }, {
                            fieldLabel : $lang('msg_dynamicdisplay_iscustomcolortemplate'),
                            name       : 'IsCustomColorTemplate',
                            xtype      : 'vizcheckbox',
                            checked    : false,
                            listeners  : {
                                check : this.onCheckIsCustomColorTemplate,
                                scope : this
                            }
                        }, {
                            fieldLabel   : $lang('msg_dynamicdisplay_colortemplate'),
                            name         : 'ColorTemplate',
                            xtype        : 'vizComboColor',
                            sortByValues : true,
                            value        : 'GreenYellow'
                        }, {
                            fieldLabel : $lang('msg_dynamicdisplay_customcolortemplate'),
                            name       : 'KeyCustomColorTemplate',
                            hiddenName : 'KeyCustomColorTemplate',
                            xtype      : 'vizComboDynamicDisplayColorTemplate',
                            allowBlank : true,
                            disabled   : true
                        }, {
                            fieldLabel : $lang('msg_dynamicdisplay_roundedcorners'),
                            name       : 'RoundedCorners',
                            hidden     : true,
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }]
            },

            /**
             * @param {Object} config The configuration options of the Header.
             */
            configFormHeader             : function(config) {
                return [{
                            fieldLabel : $lang('msg_dynamicdisplay_headervisible'),
                            name       : 'HeaderVisible',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, Viz.combo.DynamicDisplayImage.factory({
                                    fieldLabel : $lang('msg_dynamicdisplay_headerpicture'),
                                    name       : 'HeaderPicture',
                                    store      : this.storeDynamicDisplayImageArray[0]
                                }), {
                            xtype      : 'vizcompositefield',
                            fieldLabel : $lang('msg_dynamicdisplay_defaultfontstyle'),
                            items      : [{
                                        name       : 'HeaderTextDefaultFontFamily',
                                        xtype      : 'vizComboFontFamily',
                                        hiddenName : 'HeaderTextDefaultFontFamily'
                                    }, {
                                        xtype       : 'displayfield',
                                        submitValue : false,
                                        cls         : 'viz-form-display-field-inside-composite',
                                        value       : $lang('msg_dynamicdisplay_size') + ':'
                                    }, {
                                        xtype    : 'vizSpinnerField',
                                        name     : 'HeaderTextDefaultFontSize',
                                        value    : 2,
                                        minValue : 0,
                                        maxValue : 99999,
                                        width    : 40
                                    }, {
                                        xtype       : 'displayfield',
                                        submitValue : false,
                                        cls         : 'viz-form-display-field-inside-composite',
                                        value       : $lang('msg_dynamicdisplay_color') + ':'
                                    }, {
                                        name  : 'HeaderTextDefaultFontColor',
                                        xtype : 'colorpickerfield'
                                    }]
                        }, {
                            fieldLabel       : $lang('msg_dynamicdisplay_headeremptytext'),
                            name             : 'HeaderEmptyText',
                            xtype            : 'htmleditor',
                            enableAlignments : false,
                            enableLinks      : false,
                            height           : 100

                        }, {
                            id               : config.texareawithintelissenseIdsArray[0],
                            fieldLabel       : $lang('msg_dynamicdisplay_headertext'),
                            name             : 'HeaderText',
                            xtype            : 'htmleditor',
                            enableAlignments : false,
                            enableLinks      : false,
                            height           : 100,
                            anchor           : Viz.Const.UI.Anchor, // + ' -110',
                            plugins          : [new Viz.plugins.IntelliSense({
                                        delimiterChar  : ',',
                                        serviceHandler : Viz.Services.EnergyWCF.ChartModel_GetMenuTree,
                                        serviceParams  : {
                                            KeyChart : null
                                        }
                                    })]
                        }]

            },

            /**
             * @param {Object} config The configuration options of the Main area.
             */
            configFormMain               : function(config) {
                return [{
                            fieldLabel : $lang('msg_dynamicdisplay_mainchartvisible'),
                            name       : 'MainChartVisible',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, Viz.combo.DynamicDisplayImage.factory({
                                    fieldLabel : $lang('msg_dynamicdisplay_mainpicture'),
                                    name       : 'MainPicture',
                                    store      : this.storeDynamicDisplayImageArray[1]
                                }), {
                            fieldLabel : $lang('msg_dynamicdisplay_mainpicturesize') + ' %',
                            name       : 'MainPictureSize',
                            xtype      : 'vizSpinnerField',
                            minValue   : 0,
                            maxValue   : 100,
                            value      : 10
                        }, {
                            xtype      : 'vizcompositefield',
                            fieldLabel : $lang('msg_dynamicdisplay_defaultfontstyle'),
                            items      : [{
                                        name       : 'MainTextDefaultFontFamily',
                                        xtype      : 'vizComboFontFamily',
                                        hiddenName : 'MainTextDefaultFontFamily'
                                    }, {
                                        xtype       : 'displayfield',
                                        submitValue : false,
                                        cls         : 'viz-form-display-field-inside-composite',
                                        value       : $lang('msg_dynamicdisplay_size') + ':'
                                    }, {
                                        xtype    : 'vizSpinnerField',
                                        name     : 'MainTextDefaultFontSize',
                                        value    : 2,
                                        minValue : 0,
                                        maxValue : 99999,
                                        width    : 40
                                    }, {
                                        xtype       : 'displayfield',
                                        submitValue : false,
                                        cls         : 'viz-form-display-field-inside-composite',
                                        value       : $lang('msg_dynamicdisplay_color') + ':'
                                    }, {
                                        name  : 'MainTextDefaultFontColor',
                                        xtype : 'colorpickerfield'
                                    }]
                        }, {
                            fieldLabel       : $lang('msg_dynamicdisplay_mainemptytext'),
                            name             : 'MainEmptyText',
                            xtype            : 'htmleditor',
                            enableAlignments : false,
                            enableLinks      : false,
                            height           : 100

                        }, {
                            id               : config.texareawithintelissenseIdsArray[1],
                            fieldLabel       : $lang('msg_dynamicdisplay_maintext'),
                            name             : 'MainText',
                            xtype            : 'htmleditor',
                            enableAlignments : false,
                            enableLinks      : false,
                            height           : 100,
                            anchor           : Viz.Const.UI.Anchor, // + ' -140',
                            plugins          : [new Viz.plugins.IntelliSense({
                                        delimiterChar  : ',',
                                        serviceHandler : Viz.Services.EnergyWCF.ChartModel_GetMenuTree,
                                        serviceParams  : {
                                            KeyChart : null
                                        }
                                    })]
                        }]

            },

            /**
             * @param {Object} config The configuration options of the Footer.
             */
            configFormFooter             : function(config) {
                return [{
                            fieldLabel : $lang('msg_dynamicdisplay_footervisible'),
                            name       : 'FooterVisible',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, Viz.combo.DynamicDisplayImage.factory({
                                    fieldLabel : $lang('msg_dynamicdisplay_footerpicture'),
                                    name       : 'FooterPicture',
                                    store      : this.storeDynamicDisplayImageArray[2]
                                }), {
                            xtype      : 'vizcompositefield',
                            fieldLabel : $lang('msg_dynamicdisplay_defaultfontstyle'),
                            items      : [{
                                        name       : 'FooterTextDefaultFontFamily',
                                        xtype      : 'vizComboFontFamily',
                                        hiddenName : 'FooterTextDefaultFontFamily'
                                    }, {
                                        xtype       : 'displayfield',
                                        submitValue : false,
                                        cls         : 'viz-form-display-field-inside-composite',
                                        value       : $lang('msg_dynamicdisplay_size') + ':'
                                    }, {
                                        xtype    : 'vizSpinnerField',
                                        name     : 'FooterTextDefaultFontSize',
                                        value    : 2,
                                        minValue : 0,
                                        maxValue : 99999,
                                        width    : 40
                                    }, {
                                        xtype       : 'displayfield',
                                        submitValue : false,
                                        cls         : 'viz-form-display-field-inside-composite',
                                        value       : $lang('msg_dynamicdisplay_color') + ':'
                                    }, {
                                        name  : 'FooterTextDefaultFontColor',
                                        xtype : 'colorpickerfield'
                                    }]
                        }, {
                            fieldLabel       : $lang('msg_dynamicdisplay_footeremptytext'),
                            name             : 'FooterEmptyText',
                            xtype            : 'htmleditor',
                            enableAlignments : false,
                            enableLinks      : false,
                            height           : 100

                        }, {
                            id               : config.texareawithintelissenseIdsArray[2],
                            fieldLabel       : $lang('msg_dynamicdisplay_footertext'),
                            name             : 'FooterText',
                            xtype            : 'htmleditor',
                            enableAlignments : false,
                            enableLinks      : false,
                            height           : 100,
                            anchor           : Viz.Const.UI.Anchor, // + ' -110',
                            plugins          : [new Viz.plugins.IntelliSense({
                                        delimiterChar  : ',',
                                        serviceHandler : Viz.Services.EnergyWCF.ChartModel_GetMenuTree,
                                        serviceParams  : {
                                            KeyChart : null
                                        }
                                    })]
                        }];
            },

            configFormBogusChart         : function(config) {
                return [{
                            fieldLabel   : $lang('msg_chart_type'),
                            name         : 'BogusChartType',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartType,Vizelia.FOL.Common',
                            displayIcon  : true,
                            value        : Vizelia.FOL.BusinessEntities.ChartType.Combo
                        }];
            },

            /**
             * Handler for reinjecting recurrence pattern into the fields of the form.
             * @param {Viz.form.FormPanel} form
             * @param {Object} values
             */
            onAfterLoadValues            : function(form, values) {
                this.checkColorTemplate(values.IsCustomColorTemplate);
            },

            /**
             * Handler for the IsCustomColorTemplate checkbox check event.
             */
            onCheckIsCustomColorTemplate : function(field, fieldValue) {
                this.checkColorTemplate(fieldValue);
            },
            /**
             * Update the status of the KeyCustomColorTemplate field based on the value of the ColorTemplate
             * @param {String} colorTemplateValue The ColorTemplate value
             */
            checkColorTemplate           : function(isCustomColorTemplate) {
                var form = this.getForm();
                var fieldToEnable;
                var fieldToDisable;
                if (isCustomColorTemplate) {
                    fieldToEnable = form.findField('KeyCustomColorTemplate');
                    fieldToDisable = form.findField('ColorTemplate');
                }
                else {
                    fieldToDisable = form.findField('KeyCustomColorTemplate');
                    fieldToEnable = form.findField('ColorTemplate');
                }

                fieldToEnable.enable();
                fieldToDisable.clearInvalid();
                fieldToDisable.disable();
            }

        });

Ext.reg('vizFormDynamicDisplay', Viz.form.DynamicDisplay);
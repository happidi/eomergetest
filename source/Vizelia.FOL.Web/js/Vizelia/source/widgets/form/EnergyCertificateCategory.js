﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.EnergyCertificateCategory
 * @extends Ext.FormPanel.
 */
Viz.form.EnergyCertificateCategory = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.EnergyCertificate} entity The EnergyCertificate entity the category page belongs to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                this._minValueFieldId = Ext.id();
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.EnergyCertificateCategory_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.EnergyCertificateCategory_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.EnergyCertificateCategory_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.EnergyCertificateCategory.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyEnergyCertificateCategory',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_energycertificatecategory_minvalue'),
                            id         : this._minValueFieldId,
                            xtype      : 'vizSpinnerField',
                            allowBlank : true,
                            name       : 'MinValue'
                        }, {
                            fieldLabel      : $lang('msg_energycertificatecategory_maxvalue'),
                            xtype           : 'vizSpinnerField',
                            minValueFieldId : this._minValueFieldId,
                            allowBlank      : true,
                            name            : 'MaxValue',
                            validator       : function(value, field) {
                                var minValue = Ext.getCmp(this.minValueFieldId).getValue();
                                if (minValue != null && !Ext.isNumber(value) && !Ext.isNumber(minValue)) {
                                    var msg = $lang('error_msg_invalid_energycertificate_category');
                                    Ext.getCmp(this.minValueFieldId).markInvalid(msg);
                                    return msg;
                                }
                                return true;
                            }
                        }];
            }

        });

Ext.reg('vizFormEnergyCertificateCategory', Viz.form.EnergyCertificateCategory);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.AlarmTable
 * @extends Ext.FormPanel.
 */
Viz.form.AlarmTable = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                                    : function(config) {

                var Id = config.entity ? config.entity.KeyAlarmTable : null;
                this.storeFilterSpatial = new Viz.store.AlarmTableFilterSpatial({
                            KeyAlarmTable : Id
                        });
                this.storeFilterAlarmDefinitionClassification = new Viz.store.AlarmTableFilterAlarmDefinitionClassification({
                            KeyAlarmTable : Id
                        });
                this.storeAlarmDefinition = new Viz.store.AlarmTableAlarmDefinition({
                            KeyAlarmTable : Id
                        });
                this.storeAlarmDefinitionAll = new Viz.store.AlarmTableAlarmDefinitionAll({
                            KeyAlarmTable : Id
                        });

                this._tabAlarmDefinitionAll = Ext.id();

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.AlarmTable_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.AlarmTable_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.AlarmTable_FormUpdate,
                    tabConfig            : {
                        deferredRender : true,
                        items          : [{
                                    title   : $lang('msg_general'),
                                    iconCls : 'viz-icon-small-form-update',
                                    items   : this.configFormGeneral(config)
                                }, {
                                    title    : $lang('msg_alarmtable_datasource'),
                                    iconCls  : 'viz-icon-small-chart-datasource',
                                    defaults : {
                                        anchor   : Viz.Const.UI.Anchor,
                                        hideMode : 'offsets'
                                    },
                                    items    : [this.configGridFilterSpatial(config), this.configGridFilterAlarmDefinitionClassification(config), this.configDetailGridAlarmDefinition(config)]
                                }, {
                                    id         : this._tabAlarmDefinitionAll,
                                    title      : $lang('msg_alarmtable_alarmdefinitionall'),
                                    iconCls    : 'viz-icon-small-meter-all',
                                    labelAlign : 'top',
                                    hidden     : config.mode == 'create',
                                    layout     : 'fit',
                                    items      : [this.configGridAlarmDefinitionAll(config)]
                                }]
                    },
                    // labelWidth : 180,
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.AlarmTable.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                              : function(config) {
                return [{
                            name   : 'KeyAlarmTable',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_localid'),
                            allowBlank : false,
                            name       : 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_title'),
                            name       : 'Title',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_chart_favorite'),
                            name       : 'IsFavorite',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel   : $lang('msg_alarminstance_status'),
                            name         : 'AlarmInstanceStatus',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.AlarmInstanceStatus,Vizelia.FOL.Common',
                            displayIcon  : false,
                            value        : Vizelia.FOL.BusinessEntities.AlarmInstanceStatus.Open
                        }, {
                            fieldLabel : $lang('msg_portalwindow_openonstartup'),
                            name       : 'OpenOnStartup',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }];

            },

            /**
             * The configuration object for the filter spatial grid.
             * @return {Object}
             */
            configGridFilterSpatial                        : function() {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_chart_filterspatial'),
                    iconCls     : 'viz-icon-small-site',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : {
                        xtype                       : 'vizGridFilterSpatial',
                        store                       : this.storeFilterSpatial,
                        border                      : true,
                        loadMask                    : false,
                        height                      : 200,
                        disableCheckboxForAncestors : true
                    }
                };
            },

            /**
             * The configuration object for the filter meter classification.
             * @return {Object}
             */
            configGridFilterAlarmDefinitionClassification  : function(config) {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_alarmdefinition_classification'),
                    iconCls     : 'viz-icon-small-classificationitem',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : {
                        xtype     : 'vizGridFilterAlarmDefinitionClassification',
                        store     : this.storeFilterAlarmDefinitionClassification,
                        border    : true,
                        loadMask  : false,
                        listeners : {
                            beforeaddrecord : this.onBeforeAddFilterAlarmDefinitionClassification,
                            scope           : this
                        },
                        height    : 200
                    }
                };
            },
            /**
             * The configuration object for the detail grid AlarmDefinition .
             * @return {Object}
             */
            configDetailGridAlarmDefinition                : function() {
                var retVal = this.configGridDetail({
                            name                : 'AlarmDefinition',
                            windowTitle         : $lang('msg_alarmdefinition'),
                            windowWidth         : 1000,
                            windowHeight        : 400,
                            windowIconCls       : 'viz-icon-small-alarm',
                            xtypeGridDetail     : 'vizGridAlarmDefinition',
                            columnsGridDetail   : Viz.Configuration.Columns.AlarmDefinitionSimple,
                            buttonAddTitle      : $lang('msg_alarmdefinition_add'),
                            buttonAddIconCls    : 'viz-icon-small-add',
                            buttonDeleteTitle   : $lang('msg_alarmdefinition_delete'),
                            buttonDeleteIconCls : 'viz-icon-small-delete',
                            storeDetail         : this.storeAlarmDefinition,
                            keyColumn           : 'KeyAlarmDefinition'
                        });

                Ext.apply(retVal, {
                            height   : 200,
                            loadMask : false
                        });
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_alarmdefinition'),
                    iconCls     : 'viz-icon-small-alarm',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : retVal
                };
            },

            /**
             * Return the configuration object for the grid containing all the AlarmDefinitions.(readonly grid)
             * @return {}
             */
            configGridAlarmDefinitionAll                   : function() {
                return {
                    xtype            : 'vizGridAlarmDefinition',
                    store            : this.storeAlarmDefinitionAll,
                    border           : true,
                    readonly         : true,
                    alloweditbyclick : true,
                    columns          : Viz.Configuration.Columns.AlarmDefinition
                };
            },

            /**
             * Handler on the beforeadd event of the grid FilterAlarmDefinitionClassification
             */
            onBeforeAddFilterAlarmDefinitionClassification : function(grid, entity) {
                if (this.storeFilterSpatial.hasLoaded)
                    this.storeFilterSpatial.save();

                entity.KeyAlarmTable = this.entity.KeyAlarmTable;
                entity.filterSpatial = this.storeFilterSpatial.crudStore;

                return true;
            },

            /**
             * Before save handler.
             * @param {Viz.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                                   : function(form, item, extraParams) {
                var storeArray = this.getExtraStoreArray();

                Ext.each(storeArray, function(store) {
                            if (store.hasLoaded)
                                store.save();
                        }, this);

                Ext.apply(extraParams, {
                            filterSpatial                       : this.storeFilterSpatial.crudStore,
                            filterAlarmDefinitionClassification : this.storeFilterAlarmDefinitionClassification.crudStore,
                            alarmdefinitions                    : this.storeAlarmDefinition.crudStore
                        });
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                                  : function(form, action) {
                Viz.form.AlarmDefinition.superclass.onSaveSuccess.apply(this, arguments);

                var storeArray = this.getExtraStoreArray();
                storeArray.push(this.storeAlarmDefinitionAll);

                Ext.each(storeArray, function(store) {
                            store.serviceParams.KeyAlarmTable = this.entity.KeyAlarmTable;
                            if (store.hasLoaded)
                                store.reload();
                        }, this);

                var tabpanel = this.getTabContainer();
                tabpanel.unhideTabStripItem(this._tabAlarmDefinitionAll);

                return true;
            },

            /**
             * Return an array of all the extra store of the Chart.
             * @return {Array}
             */
            getExtraStoreArray                             : function() {
                return [this.storeFilterSpatial, this.storeFilterAlarmDefinitionClassification, this.storeAlarmDefinition];
            }
        });

Ext.reg('vizFormAlarmTable', Viz.form.AlarmTable);
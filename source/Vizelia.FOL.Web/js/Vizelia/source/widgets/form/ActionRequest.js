﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ActionRequest
 * @extends Ext.FormPanel.
 * @xtype vizFormActionRequest
 */
Viz.form.ActionRequest = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor         : function(config) {
                config = config || {};
                this._id = Ext.id();
                var defaultConfig = {
                    mode_load                         : 'remote',
                    psetConfig                        : {
                        IfcType                    : 'IfcActionRequest',
                        entityKey                  : 'KeyActionRequest',
                        filterByClassificationItem : true
                    },
                    messageBusAfterUpdate             : 'ActionRequestChange',
                    serviceHandlerWorkflowTransitions : Viz.Services.ServiceDeskWCF.Workflow_GetStateMachineEventsTransitions,
                    serviceHandlerWorkflowRaiseEvent  : Viz.Services.ServiceDeskWCF.Workflow_RaiseEvent,
                    serviceHandler                    : Viz.Services.ServiceDeskWCF.ActionRequest_FormLoad,
                    serviceHandlerCreate              : Viz.Services.ServiceDeskWCF.ActionRequest_FormCreate,
                    serviceHandlerUpdate              : Viz.Services.ServiceDeskWCF.ActionRequest_FormUpdate,
                    items                             : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.ActionRequest.superclass.constructor.call(this, config);
                if (this.mode == "update") {
                    this.readonly = config.entity.ReadOnly;
                    this.on('beforerender', this.onBeforeRender, this);
                }
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral   : function(config) {
                var rootEntity = Viz.Configuration.GetClassificationItemDefinitionEntityFromCategory("work");
                rootEntity.Level = 1;
                return [{
                            fieldLabel : $lang('msg_actionrequest_id'),
                            hidden     : config.mode == 'create',
                            xtype      : 'displayfield',
                            name       : 'RequestID'
                        }, {
                            fieldLabel : $lang('msg_status'),
                            hidden     : config.mode == 'create',
                            xtype      : 'vizdisplayformatfield',
                            renderer   : function(field, value) {
                                var form = field.findParentByType('form');
                                var entity = form.entity;
                                if (entity != null) {
                                    field.addClass('viz-icon-displayfield');
                                    var cssIcon = Viz.Configuration.StateCss(entity.State);
                                    field.addClass(cssIcon);
                                }
                                return value;
                            },
                            name       : 'StateLabel'
                        }, {
                            fieldLabel : $lang('msg_actionrequest_requestor'),
                            xtype      : 'vizComboOccupant',
                            displayFn  : function(entity) {
                                return !Ext.isEmpty(entity.RequestorFirstName) && !Ext.isEmpty(entity.RequestorLastName) ? entity.RequestorFirstName + " " + entity.RequestorLastName : '';
                            },
                            hiddenName : 'Requestor',
                            name       : 'Requestor',
                            allowBlank : false,
                            listeners  : {
                                select : function(combo, record, index) {
                                },
                                scope  : this
                            }
                        }, {
                            fieldLabel           : $lang('msg_location_path'),
                            xtype                : 'viztreecomboSpatial',
                            name                 : 'KeyLocation',
                            originalDisplayValue : 'LocationLongPath',
                            allowBlank           : false,
                            validator            : Ext.form.VTypes.actionRequestLocationValidator,
                            listeners            : {
                                select : this.updateScheduleStart,
                                scope  : this
                            }
                        }, {
                            fieldLabel           : $lang('msg_actionrequest_classificationlongpath'),
                            xtype                : 'viztreecomboClassificationItemWork',
                            name                 : 'ClassificationKeyChildren',
                            originalDisplayValue : 'ClassificationLongPath',
                            serviceParams        : {
                                flgFilter           : config.mode == "update" ? false : true,
                                excludePset         : true,
                                excludeRelationship : true,
                                entity              : rootEntity
                            },
                            allowBlank           : false,
                            validator            : Ext.form.VTypes.actionRequestClassificationItemValidator,
                            listeners            : {
                                changevalue : function(field, value, node) {
                                    if (node == null) {
                                        this.psetConfig.KeyClassificationItem = this.entity.ClassificationKeyParent;
                                    }
                                    else {
                                        if (!node.attributes.entity)
                                            return;
                                        if (node.attributes.entity.Category == 'task')
                                            return;
                                        if (node.attributes.entity.Category == 'work')
                                            this.psetConfig.KeyClassificationItem = node.attributes.entity.KeyClassificationItem;
                                        if (node.attributes.entity.Category == 'actionrequest') {
                                            this.psetConfig.KeyClassificationItem = node.parentNode.attributes.entity.KeyClassificationItem;
                                            this.loadPriority(node.parentNode.attributes.entity.KeyClassificationItem, node.attributes.entity.KeyClassificationItem);
                                        }
                                    }
                                    this.loadExtraTabs();
                                },
                                scope       : this
                            }
                        }, {
                            xtype      : 'vizComboPriority',
                            fieldLabel : $lang('msg_priority'),
                            name       : 'PriorityID',
                            hiddenName : 'PriorityID',
                            allowBlank : false,
                            validator  : Ext.form.VTypes.comboTestValidator,
                            listeners  : {
                                select : this.updateScheduleStart,
                                scope  : this
                            }
                        }, {
                            fieldLabel : $lang('msg_actionrequest_schedulestart'),
                            name       : 'ScheduleStart',
                            hideTime   : false,
                            id         : this.dateend_id,
                            xtype      : 'vizDateTime',
                            vvalidator : Ext.form.VTypes.daterangesimple,
                            width      : 200,
                            hasSwitch  : false
                        }, {
                            fieldLabel : $lang('msg_description'),
                            xtype      : 'textarea',
                            height     : 150,
                            name       : 'Description'
                        }];
            },

            /**
             * Finds the priority value of the current classification item and assign it to the combo.
             * @param {String} keyParent the Key of the parent of the current ClassificationItem.
             * @param {String} keyChildren the Key of the current ClassificationItem.
             */
            loadPriority        : function(keyParent, keyChildren) {
                Viz.Services.CoreWCF.Priority_GetItemByClassificationAscendant({
                            KeyClassificationChildren : keyChildren,
                            KeyClassificationParent   : keyParent,
                            success                   : function(/* Viz.BusinessEntity.Priority */priority) {
                                if (priority != null) {
                                    this.getForm().findField("PriorityID").setValue(priority.KeyPriority);
                                    this.updateScheduleStart();
                                }
                            },
                            scope                     : this
                        })

            },

            /**
             * Implementation for the virtual function in order to correct the issue with getFieldValues and composite fields.
             * @param {Viz.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave        : function(form, item, extraParams) {
                var isValid = true;
                var listboxtree = form.getForm().findField("ClassificationKeyChildren");
                var node = listboxtree.getSelectedNode();
                if (node)
                    item.ClassificationKeyParent = node.parentNode.attributes.Key;
                return isValid;
            },
            /**
             * Handler for updating schedule start date when a the priority is changed.
             */
            updateScheduleStart : function() {
                var priorityValue = null;
                var priorityField = this.getForm().findField("PriorityID");
                var keyPriority = priorityField.getValue();
                var storePriority = priorityField.getStore();
                var r = storePriority.getById(keyPriority);
                if (r) {
                    priorityValue = r.data["Value"];
                }
                var keyLocation = this.getForm().findField("KeyLocation").getValue();
                if (priorityValue != null && keyLocation != null) {
                    Viz.Services.ServiceDeskWCF.Calendar_CalculateScheduleDate({
                                start       : new Date(),
                                minutes     : priorityValue,
                                KeyLocation : keyLocation,
                                success     : function(value) {
                                    if (value) {
                                        this.getForm().findField("ScheduleStart").setValue(value);
                                    }
                                },
                                scope       : this
                            });
                }
            },
            /**
             * Handler for updating action buttons responding to workflow.
             */
            onBeforeRender      : function() {
                var workflowUtil = new Viz.util.Workflow({
                            comp      : this,
                            listeners : {
                                success            : function(o, actions) {
                                    this.fbar.insert(0, {
                                                text    : $lang('msg_otheractions'),
                                                iconCls : 'viz-icon-small-action',
                                                menu    : actions
                                            });
                                    this.fbar.doLayout();

                                },
                                beforeaction       : function() {
                                    var form = this.getForm();
                                    if (form.isValid()) {// && (form.isDirty() || this.isDirty)
                                        if (this.ownerCt)
                                            this.ownerCt.getEl().mask();
                                        return true;
                                    }
                                    else
                                        return false;
                                },
                                afteractionsuccess : function() {
                                    if (this.ownerCt)
                                        this.ownerCt.getEl().unmask();
                                    this.publishMessage("ActionRequestChange");
                                    this.publishMessage("TaskChange");
                                    this.closeParentWindow(false);
                                },
                                afteractionfailure : function() {
                                    if (this.ownerCt)
                                        this.ownerCt.getEl().unmask();
                                },
                                scope              : this
                            }
                        });

                workflowUtil.getStateMachineActions();
                return true;
            }
        });
Ext.reg('vizFormActionRequest', Viz.form.ActionRequest);
﻿Ext.namespace('Viz.form');

/**
 * @class Viz.form.Furniture
 * @extends Ext.FormPanel.
 */
Viz.form.Furniture = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                this._tabContainerId = Ext.id();
                var defaultConfig = {
                    mode_load            : 'remote',
                    psetConfig           : {
                        IfcType                    : 'IfcFurniture',
                        entityKey                  : 'KeyFurniture',
                        filterByClassificationItem : false
                    },
                    serviceHandler       : Viz.Services.CoreWCF.Furniture_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.Furniture_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.Furniture_FormUpdate,
                    hasValidationToolbar : true,
                    items                : this.configFormGeneral(config)

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Furniture.superclass.constructor.call(this, config);

            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral : function(config) {
                return [{
                    name       : 'LocalId',
                    fieldLabel : $lang('msg_localid'),
                    allowBlank : false
                        // ,
                        // readOnly : config.mode == "update" ? true : false
                    }, {
                    fieldLabel : $lang('msg_name'),
                    name       : 'Name'
                }, {
                    fieldLabel           : $lang('msg_equipment_classification'),
                    xtype                : 'viztreecomboClassificationItemEquipment',
                    name                 : 'KeyClassificationItem',
                    originalDisplayValue : 'ClassificationItemLongPath',
                    allowBlank           : false,
                    listeners            : {
                        changevalue : function(field, value) {
                            return;
                        },
                        scope       : this
                    }
                }, {
                    xtype      : 'textarea',
                    anchor     : Viz.Const.UI.Anchor + ' -90',
                    fieldLabel : $lang('msg_description'),
                    name       : 'Description'
                }]
            }
        });

Ext.reg('vizFormFurniture', Viz.form.Furniture);

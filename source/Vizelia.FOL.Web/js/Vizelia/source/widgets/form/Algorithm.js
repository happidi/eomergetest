﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Algorithm
 * @extends Ext.FormPanel.
 */
Viz.form.Algorithm = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                        : function(config) {
                this._codeId = Ext.id();
                this._binaryId = Ext.id();
                this._generateButtonId = Ext.id();
                this._nameId = Ext.id();
                config = config || {};

                this._gridAlgorithmInputDefinition = Ext.id();
                this._tabAlgorithmInputDefinition = Ext.id();

                var Id = config.entity ? config.entity.KeyAlgorithm : null;
                this.storeInput = new Viz.store.AlgorithmInputDefinition({
                            KeyAlgorithm : Id
                        });

                var defaultConfig = {
                    mode_load            : 'remote',
                    errorMessageBoxWidth : 600,
                    serviceHandler       : Viz.Services.EnergyWCF.Algorithm_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.Algorithm_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.Algorithm_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_algorithminputdefinition'),
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    iconCls    : 'viz-icon-small-python',
                                    hidden     : config.mode == 'create',
                                    id         : this._tabAlgorithmInputDefinition,
                                    items      : [this.configGridAlgorithmInputDefinition(config)]
                                }]
                    },
                    closeonsave          : false,
                    extraButtons         : [{
                                id       : this._generateButtonId,
                                text     : $lang('msg_algorithm_generatecode'),
                                width    : 80,
                                hidden   : config.mode != 'create',
                                cls      : 'x-toolbar-standardbutton',
                                iconCls  : 'viz-icon-small-python',
                                handler  : this.onGenerateCode,
                                scope    : this
                            }]
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.Algorithm.superclass.constructor.call(this, config);

                Viz.util.MessageBusMgr.subscribe("AlgorithmInputDefinitionChange", this.onAlgorithmInputDefinitionChange, this);

            },

            /**
             * Destroy
             * @private
             */
            onDestroy                          : function() {
                Viz.util.MessageBusMgr.unsubscribe("AlgorithmInputDefinitionChange", this.onAlgorithmInputDefinitionChange, this);
                Viz.form.Algorithm.superclass.onDestroy.apply(this, arguments);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                  : function(config) {
                return [{
                            name   : 'KeyAlgorithm',
                            hidden : true
                        }, {
                            xtype      : 'fieldset',
                            title      : $lang('msg_algorithm_general'),
                            labelWidth : 140,
                            defaults   : {
                                msgTarget : 'side',
                                anchor    : Viz.Const.UI.Anchor
                            },
                            items      : [{
                                        fieldLabel : $lang('msg_name'),
                                        xtype      : 'textfield',
                                        name       : 'Name',
                                        id         : this._nameId,
                                        allowBlank : false
                                    }, {
                                        xtype      : 'textarea',
                                        anchor     : Viz.Const.UI.Anchor,
                                        fieldLabel : $lang('msg_description'),
                                        name       : 'Description',
                                        height     : 50
                                    }, {
                                        fieldLabel     : $lang('msg_algorithm_source'),
                                        name           : 'Source',
                                        allowBlank     : false,
                                        displayIcon    : false,
                                        xtype          : 'vizComboEnum',
                                        enumTypeName   : 'Vizelia.FOL.BusinessEntities.AlgorithmSource,Vizelia.FOL.Common',
                                        value: Vizelia.FOL.BusinessEntities.AlgorithmSource.ExternalLibrary,
                                        valuesToRemove : ['BuiltIn', 'Unknown'] //,
//                                        listeners      : {
//                                            select : this.onSelectSource,
//                                            scope  : this
//                                        }
                                    }]
                        }, {
                            xtype      : 'fieldset',
                            title      : $lang('msg_algorithm_binary'),
                            labelWidth : 140,
                            defaults   : {
                                msgTarget : 'side',
                                anchor    : Viz.Const.UI.Anchor
                            },
                            items      : [{
                                        fieldLabel             : $lang('msg_algorithm_binary'),
                                        id                     : this._binaryId,
                                        name                   : 'KeyBinary',
                                        xtype                  : 'vizFileUpload',
                                        deleteButtonDisabled   : true,
                                        acceptedFileExtensions : ['dll', 'zip']
                                    }]
                        }, {
                            xtype      : 'fieldset',
                            title      : $lang('msg_algorithm_editor'),
                            labelWidth : 140,
                            anchor     : Viz.Const.UI.Anchor + ' -215',
                            defaults   : {
                                msgTarget : 'side'
                            },
                            items      : [{
                                        fieldLabel   : $lang('msg_algorithm_language'),
                                        name         : 'Language',
                                        anchor       : Viz.Const.UI.Anchor,
                                        allowBlank   : false,
                                        displayIcon  : false,
                                        xtype        : 'vizComboEnum',
                                        enumTypeName : 'Vizelia.FOL.BusinessEntities.AlgorithmLanguage,Vizelia.FOL.Common',
                                        value        : Vizelia.FOL.BusinessEntities.AlgorithmLanguage.CSharp,
                                        valuesToRemove : ['Unknown', 'NotApplicable'],
                                        listeners    : {
                                            select : this.onSelectLanguage,
                                            scope  : this
                                        }
                                    }, {
                                        xtype      : 'codemirror',
                                        id         : this._codeId,
                                        fieldLabel : $lang('msg_algorithm_code'),
                                        anchor     : Viz.Const.UI.Anchor + ' -40',
                                        name       : 'Code',
                                        allowBlank : true,
                                        plugins    : [new Viz.plugins.IntelliSense({
                                                    useParentEl    : true,
                                                    startChar      : '',
                                                    serviceHandler : Viz.Services.EnergyWCF.ScriptModel_GetTree,
                                                    serviceParams  : {
                                                        KeyScript : config.entity.KeyAlgorithm
                                                    }
                                                })]
                                        // ,
                                    // listeners : {
                                    // fileselected : function(field, value) {
                                    // if (!Ext.isEmpty(value)) {
                                    // var form = this.getForm();
                                    // var nameField = form.findField('Name');
                                    // if (Ext.isEmpty(nameField.getValue())) {
                                    // var name = value.substring(0, value.indexOf("."));
                                    // if (name.lastIndexOf('\\') >= 0) {
                                    // name = name.substring(name.lastIndexOf('\\') + 1);
                                    // }
                                    // nameField.setValue(name);
                                    // }
                                    // }
                                    // },
                                    // scope : this
                                    // }
                                }]
                        }];
            },

            onSelectLanguage                   : function(combo, record, index) {
                var editor = Ext.getCmp(this._codeId);
                switch (record.data.Value) {
                    case Vizelia.FOL.BusinessEntities.AlgorithmLanguage.Python :
                        editor.setMode({
                                    name                   : 'python',
                                    version                : 2,
                                    singleLineStringErrors : false
                                });
                        Ext.getCmp(this._generateButtonId).disable();
                        break;

                    case Vizelia.FOL.BusinessEntities.AlgorithmLanguage.CSharp :
                        editor.setMode('text/x-csharp');
                        Ext.getCmp(this._generateButtonId).enable();
                        break;
                }
            },

//            onSelectSource                     : function(combo, record, index) {
//                var editor = Ext.getCmp(this._codeId);
//                var binaryField = Ext.getCmp(this._binaryId);
//
//                switch (record.data.Value) {
//                    case Vizelia.FOL.BusinessEntities.AlgorithmSource.ExternalLibrary :
//                        //console.log('external library!');
//                        // what about the generate button?
//                        break;
//
//                    case Vizelia.FOL.BusinessEntities.AlgorithmSource.Editable :
//                        //console.log('editable!');
//                        // what about the generate button?
//                        break;
//
//                    default :
//                        console.log('defaaaaaullllttttt!');
//                        break;
//                }
//            },

            onGenerateCode                     : function() {
                var name = Ext.getCmp(this._nameId);
                if (name.isValid()) {
                    Viz.Services.EnergyWCF.Algorithm_GetCode({
                                className : name.getValue(),
                                success   : function(response) {
                                    var editor = Ext.getCmp(this._codeId);
                                    editor.setValue(response);
                                },
                                scope     : this
                            });
                }

            },

            onAlgorithmInputDefinitionChange   : function(entity) {
                if (entity.KeyAlgorithm == this.entity.KeyAlgorithm) {
                    var editor = Ext.getCmp(this._codeId);
                    if (editor.intelliSense && editor.intelliSense.field)
                        delete editor.intelliSense.field.nodes;
                }
            },

            /**
             * The configuration for the grid priority
             * @return {object}
             */
            configGridAlgorithmInputDefinition : function(config) {
                return {
                    xtype        : 'vizGridAlgorithmInputDefinition',
                    loadMask     : false,
                    id           : this._gridAlgorithmInputDefinition,
                    store        : this.storeInput,
                    KeyAlgorithm : config.entity.KeyAlgorithm,
                    columns      : Viz.Configuration.Columns.AlgorithmInputDefinition,
                    border       : true,
                    listeners    : {
                        beforedeleterecord : this.onDeletedRecordTab,
                        scope              : this
                    }
                };
            },

            /**
             * Handler for the columns grid delete.
             */
            onDeletedRecordTab                 : function() {
                this.deleteSelectedRowsFromGrid(this._gridAlgorithmInputDefinition);
                return false;
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                       : function(form, item, extraParams) {
                if (this.storeInput.hasLoaded)
                    this.storeInput.save();
                Ext.apply(extraParams, {
                            inputs : this.storeInput.crudStore
                        });
                return true;
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                      : function(form, action) {
                Viz.form.Algorithm.superclass.onSaveSuccess.apply(this, arguments);

                var tabpanel = this.getTabContainer();
                tabpanel.unhideTabStripItem(this._tabAlgorithmInputDefinition);

                var grid = Ext.getCmp(this._gridAlgorithmInputDefinition);
                grid.KeyAlgorithm = grid.store.serviceParams.KeyAlgorithm = this.entity.KeyAlgorithm;

                var editor = Ext.getCmp(this._codeId);
                if (editor.intelliSense && editor.intelliSense.serviceParams) {
                    editor.intelliSense.serviceParams.KeyAlgorithm = this.entity.KeyAlgorithm;
                }
            }

        });

Ext.reg('vizFormAlgorithm', Viz.form.Algorithm);

﻿Ext.ns('Viz.form');

/**
 * @class Viz.form.CompositeField
 * @extends Ext.form.CompositeField
 */
Viz.form.CompositeField = Ext.extend(Ext.form.CompositeField, {
            bodyStyle      : 'background-color:transparent;',
            defaultMargins : '0 0 0 0',
            defaults       : {
                height : 23
            },
            
            /* @return {String} The xtype
			*/
			getXType : function(){
				return 'compositefield';
			}
        });

Ext.reg('vizcompositefield', Viz.form.CompositeField);

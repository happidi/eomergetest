﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Playlist
 * @extends Ext.FormPanel.
 */
Viz.form.Playlist = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor        : function(config) {
                this._gridPlaylistPage = Ext.id();
                this._tabPlaylistPage = Ext.id();

                var Id = config.entity ? config.entity.KeyPlaylist : null;
                this.storePage = new Viz.store.PlaylistPage({
                            KeyPlaylist : Id
                        });
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.Playlist_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.Playlist_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.Playlist_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_playlistpage'),
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    hidden     : config.mode == 'create',
                                    id         : this._tabPlaylistPage,
                                    items      : [this.configGridPanelTab(config)]
                                }]
                    },
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.Playlist.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral  : function(config) {
                return [{
                            name   : 'KeyPlaylist',
                            hidden : true
                        }, {
                            fieldLabel: $lang('msg_localid'),
                            allowBlank: false,
                            name: 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_playlist_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_playlist_culture'),
                            name       : 'KeyLocalizationCulture',
                            hiddenName : 'KeyLocalizationCulture',
                            autoLoad   : config.mode == 'create',
                            xtype      : 'vizComboLocalizationCulture'
                        }, {
                            fieldLabel : $lang('msg_playlist_width'),
                            name       : 'Width',
                            xtype      : 'vizSpinnerField',
                            minValue   : 640,
                            maxValue   : 4096,
                            allowBlank : false,
                            value      : 1920
                        }, {
                            fieldLabel : $lang('msg_playlist_height'),
                            name       : 'Height',
                            xtype      : 'vizSpinnerField',
                            minValue   : 480,
                            maxValue   : 4096,
                            allowBlank : false,
                            value      : 1080
                        }]
            },

            /**
             * The configuration for the grid priority
             * @return {object}
             */
            configGridPanelTab : function(config) {
                return {
                    xtype       : 'vizGridPlaylistPage',
                    loadMask    : false,
                    id          : this._gridPlaylistPage,
                    store       : this.storePage,
                    KeyPlaylist : config.entity.KeyPlaylist,
                    columns     : Viz.Configuration.Columns.PlaylistPage,
                    border      : true,
                    listeners   : {
                        beforedeleterecord : this.onDeletedRecordTab,
                        scope              : this
                    }
                };
            },

            /**
             * Handler for the columns grid delete.
             */
            onDeletedRecordTab : function() {
                this.deleteSelectedRowsFromGrid(this._gridPlaylistPage);
                return false;
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave       : function(form, item, extraParams) {
                if (this.storePage.hasLoaded)
                    this.storePage.save();
                Ext.apply(extraParams, {
                            pages : this.storePage.crudStore
                        });
                return true;
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess      : function(form, action) {
                Viz.form.Playlist.superclass.onSaveSuccess.apply(this, arguments);

                var tabpanel = this.getTabContainer();
                tabpanel.unhideTabStripItem(this._tabPlaylistPage);

                var grid = Ext.getCmp(this._gridPlaylistPage);
                grid.KeyPlaylist = grid.store.serviceParams.KeyPlaylist = this.entity.KeyPlaylist;

            }
        });
Ext.reg('vizFormPlaylist', Viz.form.Playlist);

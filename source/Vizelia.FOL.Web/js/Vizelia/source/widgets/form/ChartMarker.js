﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ChartMarker
 * @extends Ext.FormPanel.
 */
Viz.form.ChartMarker = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Chart} entity The chart entity the chartmarker belong to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.ChartMarker_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.ChartMarker_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.ChartMarker_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.ChartMarker.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyChartMarker',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_id'),
                            name       : 'LocalId',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_chartmarker_min'),
                            xtype      : 'vizSpinnerField',
                            name       : 'Min',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_chartmarker_max'),
                            xtype      : 'vizSpinnerField',
                            name       : 'Max',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_color'),
                            name       : 'Color',
                            xtype      : 'colorpickerfield'
                        }, {
                            fieldLabel : $lang('msg_chartaxis'),
                            name       : 'KeyChartAxis',
                            xtype      : 'vizComboChartAxis',
                            KeyChart   : config.entity.KeyChart,
                            allowBlank : false
                        }];
            },

            /**
             * Override the loadValues function to update the Color field based on ColorR,ColorG,ColorB
             */
            loadValues        : function() {
                if (!Ext.isEmpty(this.entity.ColorR)) {
                    this.getForm().findField('Color').setValue(new Viz.RGBColor(String.format("rgb({0},{1},{2})", this.entity.ColorR, this.entity.ColorG, this.entity.ColorB)).toHex());
                }
                Viz.form.ChartMarker.superclass.loadValues.apply(this, arguments)
            },

            /**
             * Override the onBeforeSave function to update ColorR,ColorG and ColorB fields based on the Color component
             */
            onBeforeSave      : function(form, item, extraParams) {
                var value = this.getForm().findField('Color').getValue();
                if (value) {
                    var rgbArray = new Viz.RGBColor(value).toRGBArray();
                    item.ColorR = rgbArray[0];
                    item.ColorG = rgbArray[1];
                    item.ColorB = rgbArray[2];
                }
                else {
                    item.ColorR = null;
                    item.ColorG = null;
                    item.ColorB = null;
                }
                return true;
            }
        });

Ext.reg('vizFormChartMarker', Viz.form.ChartMarker);
﻿Ext.namespace('Viz.form');

/**
 * @class Viz.form.DisplayFormatField
 * @extends Ext.form.DisplayField Summary.
 */
Viz.form.DisplayFormatField = Ext.extend(Ext.form.DisplayField, {

            /**
             * @cfg {Function} renderer A renderer is an 'interceptor' method which can be used transform data (value, appearance, etc.) before it is rendered). The parameter passed to renderer are the field, and the value.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.DisplayFormatField.superclass.constructor.call(this, config);
                this.innerRawValue = null;
            },

            getRawValue : function() {
                var v = Ext.value(this.value, '');
                if (v === this.emptyText) {
                    v = '';
                }

                return v;
            },

            setRawValue : function(v) {
                var value = v;
                if (this.renderer && Ext.isFunction(this.renderer)) {
                    v = this.renderer(this, value);
                }
                if (this.htmlEncode) {
                    v = Ext.util.Format.htmlEncode(v);
                }
                return this.rendered ? (this.el.dom.innerHTML = (Ext.isEmpty(v) ? '' : v)) : (this.value = value);
            }
        });

Ext.reg('vizdisplayformatfield', Viz.form.DisplayFormatField);

﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Palette
 * @extends Ext.FormPanel.
 */
Viz.form.Palette = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor        : function(config) {
                this._gridPaletteColor = Ext.id();
                this._tabPaletteColor = Ext.id();

                var Id = config.entity ? config.entity.KeyPalette : null;
                this.storeColor = new Viz.store.PaletteColor({
                            KeyPalette : Id
                        });
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.Palette_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.Palette_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.Palette_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_palettecolor'),
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    hidden     : config.mode == 'create',
                                    id         : this._tabPaletteColor,
                                    items      : [this.configGridPanelTab(config)]
                                }]
                    },
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.Palette.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral  : function(config) {
                return [{
                            name   : 'KeyPalette',
                            hidden : true
                        }, Viz.combo.Localization.factory({})]
            },

            /**
             * The configuration for the grid priority
             * @return {object}
             */
            configGridPanelTab : function(config) {
                return {
                    xtype      : 'vizGridPaletteColor',
                    loadMask   : false,
                    id         : this._gridPaletteColor,
                    store      : this.storeColor,
                    KeyPalette : config.entity.KeyPalette,
                    columns    : Viz.Configuration.Columns.PaletteColor,
                    border     : true,
                    listeners  : {
                        beforedeleterecord : this.onDeletedRecordTab,
                        scope              : this
                    }
                };
            },

            /**
             * Handler for the columns grid delete.
             */
            onDeletedRecordTab : function() {
                this.deleteSelectedRowsFromGrid(this._gridPaletteColor);
                return false;
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave       : function(form, item, extraParams) {
                if (this.storeColor.hasLoaded)
                    this.storeColor.save();
                Ext.apply(extraParams, {
                            palettecolor : this.storeColor.crudStore
                        });
                return true;
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess      : function(form, action) {
                Viz.form.Palette.superclass.onSaveSuccess.apply(this, arguments);

                var tabpanel = this.getTabContainer();
                tabpanel.unhideTabStripItem(this._tabPaletteColor);

                var grid = Ext.getCmp(this._gridPaletteColor);
                grid.KeyPalette = grid.store.serviceParams.KeyPalette = this.entity.KeyPalette;

            }
        });
Ext.reg('vizFormPalette', Viz.form.Palette);

﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.PlaylistPage
 * @extends Ext.FormPanel.
 */
Viz.form.PlaylistPage = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Playlist} entity The Playlist entity the playlist page belongs to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.PlaylistPage_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.PlaylistPage_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.PlaylistPage_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.PlaylistPage.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyPlaylistPage',
                            hidden : true
                        }, {
                            fieldLabel: $lang('msg_localid'),
                            allowBlank: false,
                            name: 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_playlistpage_order'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : false,
                            name       : 'Order',
                            minValue   : 1
                        }, {
                            fieldLabel : $lang('msg_portaltab'),
                            name       : 'KeyPortalTab',
                            hiddenName : 'KeyPortalTab',
                            allowBlank : false,
                            xtype      : 'vizComboPortalTab'
                        }, {
                            fieldLabel : $lang('msg_playlistpage_duration'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : false,
                            name       : 'Duration',
                            minValue   : 5,
                            value      : 30
                        }];
            }

        });

Ext.reg('vizFormPlaylistPage', Viz.form.PlaylistPage);
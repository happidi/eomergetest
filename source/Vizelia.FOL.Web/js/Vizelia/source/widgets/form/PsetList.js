﻿Ext.namespace("Viz.form");

/**
 * @class Viz.form.PsetList
 * @extends Ext.FormPanel.
 */
Viz.form.PsetList = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                : function(config) {

                this._gridList = Ext.id();
                var listName = config.entity ? config.entity.listName : null;

                this.storeList = new Viz.store.PsetList({
                            listName           : config.entity.listName,
                            serviceHandlerCrud : null
                        });

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'local',
                    serviceHandlerCreate : Viz.Services.CoreWCF.PsetListElement_SaveStore,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.PsetListElement_SaveStore,
                    tabConfig            : {
                        deferredRender : true,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }]

                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.ApplicationGroup.superclass.constructor.call(this, config);

            },

            /**
             * Handler before save that gives a chance to send the 2 stores as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave               : function(form, item, extraParams) {
                if (!form.isDirty){
                	if(form.closeonsave)
                		form.closeParentWindow(false);
                    return false;
                }
                if (this.storeList.hasLoaded)
                    this.storeList.save();
                Ext.apply(extraParams, {
                            listName : item.listName,
                            store    : this.storeList.crudStore
                        });
                return true;
            },
            /**
             * Handler for save success. This is a virtual function, and each sub class could provide implementation.
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess              : function(form, action) {
                Viz.form.PsetList.superclass.onSaveSuccess.apply(this, arguments);
                form.storeList.commitChanges();
            },
            /**
             * The configuration for the form general.
             */
            configFormGeneral          : function(config) {
                return [{
                            fieldLabel     : $lang('msg_name'),
                            name           : 'listName',
                            id             : 'fieldPsetListDefinition',
                            fieldLabel     : $lang('msg_component_combo'),
                            xtype          : 'vizComboListElement',
                            listName       : 'PsetListDefinition',
                            hiddenName     : 'listName',
                            disabled       : config.mode == "update" ? true : false,
                            allowBlank     : false,
                            typeAhead      : false,
                            forceSelection : false,
                            lastQuery      : ''
                        },
                        // { fieldLabel : $lang('msg_name'), name : 'listName', disabled : config.mode == "update" ? true : false, allowBlank : false },
                        {
                            xtype            : 'vizGrid',
                            id               : this._gridList,
                            stateId          : 'vizFormPsetList.PsetListElement',
                            hasRowEditor     : false,
                            hasToolbar       : false,
                            hasPagingToolbar : true,
                            border           : true,
                            stripeRows       : false,
                            anchor           : Viz.Const.UI.Anchor + ' -30',
                            tbar             : [{
                                        text    : $lang("msg_listelement_add"),
                                        iconCls : 'viz-icon-small-list-add',
                                        handler : this.ManageListElement.createDelegate(this, ['create']),
                                        scope   : this
                                    }, {
                                        text    : $lang("msg_listelement_update"),
                                        iconCls : 'viz-icon-small-list-update',
                                        handler : this.ManageListElement.createDelegate(this, ['update']),
                                        scope   : this
                                    }, {
                                        text    : $lang("msg_listelement_delete"),
                                        iconCls : 'viz-icon-small-list-delete',
                                        handler : this.deleteSelectedRowsFromGrid.createDelegate(this, [this._gridList]),
                                        scope   : this
                                    }],
                            store            : this.storeList,
                            columns          : Viz.Configuration.Columns.PsetListElement
                        }];
            },

            /**
             * Adds or updates a element of the list.
             * @param {String} mode the mode of the form (create, or update).
             */
            ManageListElement          : function(mode) {
                var entity = {};
                var mainform = this;
                if (mode == 'update') {
                    var record = Ext.getCmp(this._gridList).getSelectionModel().getSelected();
                    if (record)
                        entity = record.json;
                }
                this.idMsgCode = Ext.id();
                var win = new Ext.Window({
                            title   : mode == 'create' ? $lang('msg_listelement_add') : $lang('msg_listelement_update'),
                            width   : 500,
                            height  : 150,
                            modal   : true,
                            iconCls : 'viz-icon-small-list',
                            layout  : 'fit',
                            items   : [{
                                        xtype        : 'vizForm',
                                        mode         : mode,
                                        entity       : entity,
                                        gridId       : this._gridList,
                                        items        : [Viz.combo.Localization.factory({
                                                            fieldLabel : $lang('msg_localization_key'),
                                                            id         : this.idMsgCode,
                                                            name       : 'MsgCode',
                                                            hiddenName : 'MsgCode',
                                                            allowBlank : false,
                                                            listeners  : {
                                                                select : function(field) {
                                                                    field.clearInvalid();
                                                                }
                                                            }
                                                        })/*
                                                             * { fieldLabel : $lang('msg_localization_key'), xtype : 'vizComboLocalization', id : idMsgCode, name : 'MsgCode', hiddenName : 'MsgCode', allowBlank : false, listeners : { select : function(field) { field.clearInvalid(); } } }
                                                             */, {
                                                    fieldLabel : $lang('msg_value'),
                                                    name       : 'Value',
                                                    allowBlank : false
                                                }],
                                        onBeforeSave : function(form, item, extraParams) {
                                            mainform.isDirty = true;
                                            if (form.mode == "update") {
                                                var record = Ext.getCmp(form.gridId).getSelectionModel().getSelected();
                                                record.set("MsgCode", item.MsgCode);
                                                record.set("Value", item.Value);
                                            }
                                            else {
                                                var store = Ext.getCmp(form.gridId).getStore();
                                                if (store.find("MsgCode", item.MsgCode) == -1)
                                                    store.add(new store.recordType(item));
                                                else {
                                                    var loCombo = form.findByType('vizComboLocalization')[0];
                                                    if (loCombo)
                                                        loCombo.markInvalid($lang('error_msg_uniquefield'));
                                                    return false;
                                                }
                                            }
                                            win.close();
                                            return false;
                                        }
                                    }]
                        });
                win.show();

            },

            /**
             * Deletes the selected rows from the grid.
             * @param {String} gridId the id of the grid.
             */
            deleteSelectedRowsFromGrid : function(gridId) {
                this.isDirty = true;
                var grid = Ext.getCmp(gridId);
                grid.deleteSelectedRows();
            }
        });

Ext.reg("vizFormPsetList", Viz.form.PsetList);

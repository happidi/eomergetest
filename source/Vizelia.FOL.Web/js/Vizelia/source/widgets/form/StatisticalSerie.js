﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.StatisticalSerie
 * @extends Ext.FormPanel.
 */
Viz.form.StatisticalSerie = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Chart} entity The chart entity the dataserie belong to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                this._formulaId = Ext.id();
                this._nameId = Ext.id();
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.StatisticalSerie_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.StatisticalSerie_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.StatisticalSerie_FormUpdate,
                    items                : this.configFormGeneral(config),
                    labelWidth           : 200,
                    closeonsave          : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.StatisticalSerie.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyStatisticalSerie',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_id'),
                            name       : 'LocalId',
                            allowBlank : false
                        }, {
                            fieldLabel   : $lang('msg_statisticalserie_operator'),
                            name         : 'Operator',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.MathematicOperator,Vizelia.FOL.Common',
                            flex         : 1,
                            value        : Vizelia.FOL.BusinessEntities.MathematicOperator.SUM
                        }, {
                            fieldLabel           : $lang('msg_statisticalserie_classificationlongpath'),
                            xtype                : 'viztreecomboClassificationItemMeter',
                            name                 : 'KeyClassificationItem',
                            originalDisplayValue : 'ClassificationItemLongPath',
                            allowBlank           : false,
                            listeners            : {
                                changevalue : function(field, value) {
                                    return;
                                },
                                scope       : this
                            }
                        }, {
                            fieldLabel : $lang('msg_statisticalserie_includehiddenseries'),
                            name       : 'IncludeHiddenSeries',
                            xtype      : 'vizcheckbox',
                            flex       : 1
                        }, {
                            fieldLabel : $lang('msg_statisticalserie_runaftercalculatedseries'),
                            name       : 'RunAfterCalculatedSeries',
                            xtype      : 'vizcheckbox',
                            flex       : 1
                        }];
            }

        });

Ext.reg('vizFormStatisticalSerie', Viz.form.StatisticalSerie);
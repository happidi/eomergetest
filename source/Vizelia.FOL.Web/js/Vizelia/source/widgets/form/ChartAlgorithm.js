﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ChartAlgorithm
 * @extends Ext.FormPanel.
 */
Viz.form.ChartAlgorithm = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                   : function(config) {
                config = config || {};

                this._gridAlgorithmInputValue = Ext.id();
                this._tabAlgorithmInputValue = Ext.id();

                var KeyAlgorithm = config.entity ? config.entity.KeyAlgorithm : null;
                var KeyChart = config.entity ? config.entity.KeyChart : null;

                this.storeInput = new Viz.store.AlgorithmInputValue({
                            KeyAlgorithm : KeyAlgorithm,
                            KeyChart     : KeyChart
                        });

                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.ChartAlgorithm_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.ChartAlgorithm_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.ChartAlgorithm_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_algorithminputvalue'),
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    hidden     : config.mode == 'create',
                                    iconCls    : 'viz-icon-small-python',
                                    id         : this._tabAlgorithmInputValue,
                                    items      : [this.configGridAlgorithmInputValue(config)]
                                }]
                    },
                    closeonsave          : false
                };

                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.ChartAlgorithm.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral             : function(config) {
                return [{
                            name   : 'KeyChartAlgorithm',
                            hidden : true
                        }, {
                            name   : 'KeyChart',
                            hidden : true
                        }, {
                            fieldLabel       : $lang('msg_order'),
                            name             : 'Order',
                            xtype            : 'vizSpinnerField',
                            minValue         : 1,
                            decimalPrecision : 0,
                            allowBlank       : false
                        }, {
                            fieldLabel : $lang('msg_algorithm'),
                            name       : 'KeyAlgorithm',
                            xtype      : 'vizComboAlgorithm',
                            allowBlank : false,
                            listeners  : {
                                select : this.onSelectAlgorithm,
                                scope  : this
                            }

                        }];
            },

            /**
             * The configuration for the grid priority
             * @return {object}
             */
            configGridAlgorithmInputValue : function(config) {
                return {
                    xtype        : 'vizGridAlgorithmInputValue',
                    loadMask     : false,
                    id           : this._gridAlgorithmInputValue,
                    store        : this.storeInput,
                    KeyAlgorithm : config.entity.KeyAlgorithm,
                    KeyChart     : config.entity.KeyChart,
                    columns      : Viz.Configuration.Columns.AlgorithmInputValue,
                    border       : true,
                    listeners    : {
                        beforedeleterecord : this.onDeletedRecordTab,
                        scope              : this
                    }
                };
            },

            /**
             * Handler for the columns grid delete.
             */
            onDeletedRecordTab            : function() {
                this.deleteSelectedRowsFromGrid(this._gridAlgorithmInputValue);
                return false;
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                  : function(form, item, extraParams) {
                if (this.storeInput.hasLoaded)
                    this.storeInput.save();
                Ext.apply(extraParams, {
                            inputs : this.storeInput.crudStore
                        });
                return true;
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                 : function(form, action) {

                var useWizard = false;
                var mode = this.mode;
                Viz.form.ChartAlgorithm.superclass.onSaveSuccess.apply(this, arguments);
                if (mode == 'create') {
                    // we sending the incremental change. 
                    Viz.util.MessageBusMgr.publish('ChartAlgorithmCountChange', this.entity.KeyChart, 1);
                    var tabpanel = this.getTabContainer();
                    tabpanel.unhideTabStripItem(this._tabAlgorithmInputValue);

                    var grid = Ext.getCmp(this._gridAlgorithmInputValue);

                    grid.KeyAlgorithm = grid.store.serviceParams.KeyAlgorithm = this.entity.KeyAlgorithm;
                    // grid.KeyChart = grid.store.serviceParams.KeyChart = this.entity.KeyChart;

                    this.el.mask();

                    Viz.Services.EnergyWCF.AlgorithmInputDefinition_GetStoreByKeyAlgorithm({
                                KeyAlgorithm : this.entity.KeyAlgorithm,
                                paging       : {
                                    start : 0,
                                    limit : 50
                                },
                                success      : function(store) {
                                    var items = [];
                                    var scriptinputvalues = [];
                                    if (store && store.records) {
                                        Ext.each(store.records, function(r) {
                                                    if (useWizard) {
                                                        items.push({
                                                                    title      : r.Name,// _welcome'),
                                                                    iconCls    : 'viz-icon-small-python',
                                                                    labelWidth : 150,
                                                                    items      : [{
                                                                                xtype      : 'displayfield',
                                                                                hideLabel  : true,
                                                                                value      : r.Description,
                                                                                fieldClass : 'x-wizard-displayfield'
                                                                            }, {
                                                                                fieldLabel : r.Name,
                                                                                name       : r.Name
                                                                            }]
                                                                });
                                                    }
                                                    else {
                                                        items.push({
                                                                    fieldLabel : r.Name,
                                                                    qtip       : r.Description,
                                                                    name       : r.Name,
                                                                    labelStyle : 'text-overflow : ellipsis; overflow : hidden',
                                                                    xtype      : 'textfield',
                                                                    listeners  : {
                                                                        render : function(p) {
                                                                            var theLabel = Ext.get(p.label.id);
                                                                            var theTip = Ext.create({
                                                                                        xtype  : 'quicktip',
                                                                                        html   : r.Name,
                                                                                        margin : '0 0 0 200',
                                                                                        shadow : false
                                                                                    });

                                                                            theLabel.on('mouseover', function(event, eventObject) {
                                                                                        if (eventObject.scrollWidth > eventObject.clientWidth) {
                                                                                            var xy = theLabel.getXY();
                                                                                            xy[1] += -10;
                                                                                            theTip.showAt(xy);
                                                                                        }
                                                                                    });

                                                                            theLabel.on('mouseleave', function() {
                                                                                        theTip.hide();
                                                                                    });
                                                                        }
                                                                    }
                                                                });

                                                    }
                                                    scriptinputvalues.push({
                                                                KeyChart                    : this.entity.KeyChart,
                                                                KeyAlgorithm                : this.entity.KeyAlgorithm,
                                                                KeyAlgorithmInputDefinition : r.KeyAlgorithmInputDefinition,
                                                                KeyAlgorithmInputValue      : r.KeyAlgorithmInputValue,
                                                                Name                        : r.Name
                                                            })
                                                }, this);

                                    }
                                    if (items.length > 0) {
                                        var wizardConfig = {
                                            title        : $lang('msg_algorithminputvalue'),
                                            iconCls      : 'viz-icon-small-python',
                                            modal        : true,
                                            width        : 400,
                                            height       : Math.min(25 * items.length + 100, 700),
                                            collapsible  : true,
                                            hideMode     : 'offsets',
                                            animCollapse : false,
                                            layout       : 'fit',
                                            items        : {
                                                xtype                : 'vizForm',
                                                mode_load            : 'local',
                                                isWizard             : useWizard,
                                                hasValidationToolbar : false,
                                                closeOnSave          : true,
                                                scriptinputvalues    : scriptinputvalues,
                                                storeInput           : this.storeInput,
                                                entity               : this.entity,
                                                saveItem             : function() {
                                                    var form = Ext.getCmp(this._formId).getForm();
                                                    if (form.isValid()) {
                                                        var item = form.getFieldValues();

                                                        for (var propertyName in item) {
                                                            Ext.each(this.scriptinputvalues, function(s) {
                                                                        if (s.Name == propertyName) {
                                                                            s.Value = item[propertyName];

                                                                            if (!s.Value) {
                                                                                s.Value = '';
                                                                            }
                                                                        }
                                                                    }, this)
                                                        }
                                                        Viz.Services.EnergyWCF.AlgorithmInputValue_SaveStore({
                                                                    store   : {
                                                                        create : this.scriptinputvalues
                                                                    },
                                                                    success : function() {
                                                                        this.storeInput.reload();
                                                                        this.closeParentWindow();

                                                                        Viz.util.MessageBusMgr.publish('ChartAlgorithmChange', this.entity);
                                                                    },
                                                                    scope   : this
                                                                })
                                                    }
                                                }
                                            }
                                        };
                                        if (useWizard) {
                                            Ext.apply(wizardConfig.items, {
                                                        tabConfig : {
                                                            deferredRender : true,
                                                            items          : items,
                                                            defaults       : {
                                                                layout   : 'fit',
                                                                hideMode : 'offsets'
                                                            }
                                                        }
                                                    });
                                        }
                                        else {
                                            Ext.apply(wizardConfig.items, {
                                                        items : items
                                                    });
                                        }

                                        var wizard = new Ext.Window(wizardConfig);
                                        wizard.show();
                                    }
                                    else {
                                        Viz.util.MessageBusMgr.publish('ChartAlgorithmChange', this.entity);
                                    }
                                    this.el.unmask();
                                },
                                scope        : this
                            });
                }
                else {
                    Viz.util.MessageBusMgr.publish('ChartAlgorithmChange', this.entity);
                }
            },

            /**
             * listener for the Algorithm change event
             * @param {} combo
             * @param {} record
             * @param {} index
             */
            onSelectAlgorithm             : function(combo, record, index) {
                var grid = Ext.getCmp(this._gridAlgorithmInputValue);
                grid.KeyAlgorithm = grid.store.serviceParams.KeyAlgorithm = record.data.KeyAlgorithm;
                grid.store.reload();
            },

            publishMessage                : function(message, messageParams) {
                // we disable the publish message to publish it after the input wizard.
            }

        });

Ext.reg('vizFormChartAlgorithm', Viz.form.ChartAlgorithm);

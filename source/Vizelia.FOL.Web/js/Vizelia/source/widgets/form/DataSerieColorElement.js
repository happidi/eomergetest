﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.DataSerieColorElement
 * @extends Ext.FormPanel.
 */
Viz.form.DataSerieColorElement = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.DataSerieColorElement} entity The DataSerieColorElement entity.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                this._psetId = Ext.id();

                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.DataSerieColorElement_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.DataSerieColorElement_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.DataSerieColorElement_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.DataSerieColorElement.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyDataSerieColorElement',
                            hidden : true
                        }, {
                            fieldLabel       : $lang('msg_dataseriecolorelement_minvalue'),
                            xtype            : 'vizSpinnerField',
                            allowBlank       : true,
                            name             : 'MinValue',
                            decimalPrecision : 5
                        }, {
                            fieldLabel       : $lang('msg_dataseriecolorelement_maxvalue'),
                            xtype            : 'vizSpinnerField',
                            allowBlank       : true,
                            name             : 'MaxValue',
                            decimalPrecision : 5
                        }, {
                            fieldLabel   : $lang('msg_dataseriecolorelement_highlight'),
                            name         : 'Highlight',
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.DataPointHighlight,Vizelia.FOL.Common',
                            allowBlank   : false
                        }, {
                            fieldLabel : $lang('msg_color'),
                            name       : 'Color',
                            xtype      : 'colorpickerfield',
                            allowBlank : false
                        }];
            }

        });

Ext.reg('vizFormDataSerieColorElement', Viz.form.DataSerieColorElement);
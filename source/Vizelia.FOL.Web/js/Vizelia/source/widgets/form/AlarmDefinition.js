﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.AlarmDefinition
 * @extends Ext.FormPanel.
 */
Viz.form.AlarmDefinition = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                          : function(config) {
                this._gridFilterSpatialPset = Ext.id();

                this._tabMeterAll = Ext.id();
                this._tabInstances = Ext.id();

                var Id = config.entity ? config.entity.KeyAlarmDefinition : null;
                this.storeFilterSpatial = new Viz.store.AlarmDefinitionFilterSpatial({
                            KeyAlarmDefinition : Id
                        });
                this.storeFilterMeterClassification = new Viz.store.AlarmDefinitionFilterMeterClassification({
                            KeyAlarmDefinition : Id
                        });
                this.storeMeter = new Viz.store.AlarmDefinitionMeter({
                            KeyAlarmDefinition : Id
                        });
                this.storeMeterAll = new Viz.store.AlarmDefinitionMeterAll({
                            KeyAlarmDefinition : Id
                        });
                this.storeFilterSpatialPset = new Viz.store.AlarmDefinitionFilterSpatialPset({
                            KeyAlarmDefinition : Id
                        });
                this.storeAlarmInstance = new Viz.store.AlarmInstance({
                            KeyAlarmDefinition : Id
                        });

                this.storeChart = new Viz.store.AlarmDefinitionChart({
                            KeyAlarmDefinition : Id
                        });
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.AlarmDefinition_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.AlarmDefinition_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.AlarmDefinition_FormUpdate,
                    hasValidationToolbar : true,
                    tabConfig            : {
                        deferredRender : true,
                        items          : [{
                                    title   : $lang('msg_general'),
                                    iconCls : 'viz-icon-small-form-update',
                                    items   : this.configFormGeneral(config)
                                }, {
                                    title    : $lang('msg_chart_datasource'),
                                    iconCls  : 'viz-icon-small-chart-datasource',
                                    defaults : {
                                        anchor   : Viz.Const.UI.Anchor,
                                        hideMode : 'offsets'
                                    },
                                    items    : [this.configGridFilterSpatial(config), this.configGridFilterSpatialPset(config), this.configGridFilterMeterClassification(config), this.configDetailGridMeter(config), this.configDetailGridChart(config)]
                                }, {
                                    title      : $lang('msg_chart_meterall'),
                                    id         : this._tabMeterAll,
                                    iconCls    : 'viz-icon-small-meter-all',
                                    hidden     : config.mode == 'create',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridMeterAll(config)]
                                }, {
                                    title   : $lang('msg_alarmdefinition_meteraggregation'),
                                    iconCls : 'viz-icon-small-meter',
                                    items   : this.configFormMeterAggregation(config)
                                }, {
                                    title   : $lang('msg_alarmdefinition_condition'),
                                    iconCls : 'viz-icon-small-calculatedserie',
                                    items   : this.configFormCondition(config)
                                }, {
                                    title   : $lang('msg_alarmdefinition_action'),
                                    iconCls : 'viz-icon-small-actionrequest',
                                    items   : this.configFormActionRequest(config)
                                }, {
                                    title      : $lang('msg_alarminstance'),
                                    id         : this._tabInstances,
                                    iconCls    : 'viz-icon-small-alarminstance',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    hidden     : config.mode == 'create',
                                    items      : [this.configGridAlarmInstance(config)]
                                }]
                    },
                    labelWidth           : 180,
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.AlarmDefinition.superclass.constructor.call(this, config);
            },
            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                    : function(config) {
                return [{
                    name   : 'KeyAlarmDefinition',
                    hidden : true
                }/*
                     * , { fieldLabel : $lang('msg_localid'), name : 'LocalId' }
                     */
                , {
                    fieldLabel : $lang('msg_localid'),
                    allowBlank : false,
                    name       : 'LocalId'
                }, {
                    fieldLabel : $lang('msg_title'),
                    name       : 'Title',
                    allowBlank : false
                }, {
                    xtype      : 'textarea',
                    fieldLabel : $lang('msg_description'),
                    name       : 'Description'
                }, {
                    fieldLabel           : $lang('msg_alarmdefinition_classification'),
                    xtype                : 'viztreecomboClassificationItemAlarmDefinition',
                    name                 : 'KeyClassificationItem',
                    originalDisplayValue : 'ClassificationItemLongPath',
                    allowBlank           : false
                }, {
                    fieldLabel : $lang('msg_alarmdefinition_enabled'),
                    name       : 'Enabled',
                    xtype      : 'vizcheckbox',
                    checked    : true
                }, {
                    fieldLabel : $lang('msg_alarmdefinition_processmanually'),
                    name       : 'ProcessManually',
                    xtype      : 'vizcheckbox',
                    checked    : false
                }, {
                    fieldLabel : $lang('msg_alarmdefinition_startdate'),
                    name       : 'StartDate',
                    hideTime   : false,
                    xtype      : 'vizDateTime',
                    allowBlank : true,
                    hasSwitch  : false
                }, {
                    fieldLabel : $lang('msg_alarmdefinition_enddate'),
                    name       : 'EndDate',
                    hideTime   : false,
                    xtype      : 'vizDateTime',
                    allowBlank : true,
                    hasSwitch  : false
                }, {
                    fieldLabel : $lang('msg_alarmdefinition_lastprocessdatetime'),
                    name       : 'LastProcessDateTime',
                    hideTime   : false,
                    xtype      : 'vizDateTime',
                    allowBlank : true,
                    hasSwitch  : false,
                    hidden     : config.mode != 'update'
                }, {
                    fieldLabel             : $lang('msg_location_path'),
                    xtype                  : 'viztreecomboSpatial',
                    validator              : Ext.form.VTypes.locationValidator,
                    name                   : 'KeyLocation',
                    originalDisplayValue   : 'LocationLongPath',
                    ExcludeMetersAndAlarms : true,
                    allowBlank             : false,
                    listeners              : {

                        changevalue : function(field, value, node) {
                            var keyLocation;
                            if ((node == null) || (this.mode == 'update'))
                                return;
                            this.getForm().findField("ClassificationKeyChildren").serviceHandler = Viz.Services.CoreWCF.ClassificationItem_GetTreeByRelatedObject;
                            this.getForm().findField("ClassificationKeyChildren").serviceParams.KeyRelatedObject = keyLocation;
                            this.getForm().findField("ClassificationKeyChildren").clearValue();
                            this.getForm().findField("ClassificationKeyChildren").getTree().getRootNode().reload();

                        },
                        scope       : this
                    }
                }];
            },

            /**
             * The configuration object for the filter spatial grid.
             * @return {Object}
             */
            /**
             * The configuration object for the filter spatial grid.
             * @return {Object}
             */
            configGridFilterSpatial              : function(config) {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_chart_filterspatial'),
                    iconCls     : 'viz-icon-small-site',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : {
                        xtype                       : 'vizGridFilterSpatial',
                        store                       : this.storeFilterSpatial,
                        border                      : true,
                        loadMask                    : false,
                        height                      : 200,
                        disableCheckboxForAncestors : true
                    }
                };
            },

            /**
             * The configuration for the grid axis.
             * @return {object}
             */
            configGridFilterSpatialPset          : function(config) {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_filterspatialpset'),
                    iconCls     : 'viz-icon-small-gridfilter',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : {
                        xtype              : 'vizGridFilterSpatialPset',
                        id                 : this._gridFilterSpatialPset,
                        hidden             : config.mode == 'create',
                        border             : true,
                        store              : this.storeFilterSpatialPset,
                        loadMask           : false,
                        KeyAlarmDefinition : config.entity ? config.entity.KeyAlarmDefinition : '',
                        listeners          : {
                            beforedeleterecord : function() {
                                this.deleteSelectedRowsFromGrid(this._gridFilterSpatialPset);
                                return false;
                            },
                            scope              : this
                        },
                        height             : 200
                    }
                };
            },
            /**
             * The configuration object for the filter meter classification.
             * @return {Object}
             */
            configGridFilterMeterClassification  : function(config) {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_meter_classification'),
                    iconCls     : 'viz-icon-small-classificationitem',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : {
                        xtype     : 'vizGridFilterMeterClassification',
                        store     : this.storeFilterMeterClassification,
                        border    : true,
                        listeners : {
                            beforeaddrecord : this.onBeforeAddFilterMeterClassification,
                            load            : {
                                fn    : function() {
                                    if (!this.storeFilterSpatial.hasLoaded)
                                        this.storeFilterSpatial.load();
                                },
                                scope : this
                            },
                            scope           : this
                        },
                        height    : 200
                    }
                };
            },
            /**
             * The configuration object for the detail grid Meter.
             * @return {Object}
             */
            configDetailGridMeter                : function(config) {
                var retVal = this.configGridDetail({
                            name                         : 'Meter',
                            windowTitle                  : $lang('msg_meter'),
                            windowWidth                  : 1000,
                            windowHeight                 : 400,
                            windowIconCls                : 'viz-icon-small-meter',
                            xtypeGridDetail              : 'vizGridMeter',
                            columnsGridDetail            : Viz.Configuration.Columns.ChartMeter,
                            buttonAddTitle               : $lang('msg_meter_add'),
                            buttonAddIconCls             : 'viz-icon-small-add',
                            buttonDeleteTitle            : $lang('msg_meter_delete'),
                            buttonDeleteIconCls          : 'viz-icon-small-delete',
                            storeDetail                  : this.storeMeter,
                            keyColumn                    : 'KeyMeter',
                            ddGroupGridDetail            : 'SpatialDD',
                            enableDropFromTreeGridDetail : true
                        });
                Ext.apply(retVal, {
                            height   : 200,
                            loadMask : false
                        });
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_meters'),
                    iconCls     : 'viz-icon-small-meter',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : retVal
                };
            },

            /**
             * The configuration object for the detail grid Chart.
             * @return {Object}
             */
            configDetailGridChart                : function() {
                var retVal = this.configGridDetail({
                            name                : 'Chart',
                            windowTitle         : $lang('msg_chart'),
                            windowWidth         : 600,
                            windowHeight        : 400,
                            windowIconCls       : 'viz-icon-small-chart',
                            xtypeGridDetail     : 'vizGridChart',
                            columnsGridDetail   : Viz.Configuration.Columns.ChartSimple,
                            buttonAddTitle      : $lang('msg_chart_add'),
                            buttonAddIconCls    : 'viz-icon-small-add',
                            buttonDeleteTitle   : $lang('msg_chart_delete'),
                            buttonDeleteIconCls : 'viz-icon-small-delete',
                            storeDetail         : this.storeChart,
                            keyColumn           : 'KeyChart'
                        });
                Ext.apply(retVal, {
                            height   : 200,
                            loadMask : false
                        });
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_chart'),
                    iconCls     : 'viz-icon-small-chart',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : retVal
                };
            },

            /**
             * Return the configuration object for the grid containing all the meters.(readonly grid)
             * @param {} config
             * @return {}
             */
            configGridMeterAll                   : function(config) {
                return {
                    xtype            : 'vizGridMeter',
                    store            : this.storeMeterAll,
                    border           : true,
                    readonly         : true,
                    alloweditbyclick : true,
                    columns          : Viz.Configuration.Columns.ChartMeter
                };
            },

            /**
             * Return the configuration object for the grid containing all the instances.(readonly grid)
             * @param {} config
             * @return {}
             */
            configGridAlarmInstance              : function(config) {
                return {
                    xtype   : 'vizGridAlarmInstance',
                    store   : this.storeAlarmInstance,
                    border  : true,
                    columns : Viz.Configuration.Columns.AlarmInstance
                };
            },

            /**
             * config of the form containing the meter aggregation parameters.
             * @param {} config
             * @return {}
             */
            configFormMeterAggregation           : function(config) {
                return [{
                            xtype      : 'vizcompositefield',
                            fieldLabel : $lang('msg_calendar'),
                            items      : [{
                                        fieldLabel : $lang('msg_calendar'),
                                        name       : 'KeyCalendarEventCategory',
                                        valueField : 'KeyCalendarEventCategory',
                                        allowBlank : true,
                                        xtype      : 'vizComboCalendarEventCategory',
                                        flex       : 1
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_chart_calendarmode') + ':'
                                    }, {
                                        name           : 'CalendarMode',
                                        xtype          : 'vizComboEnum',
                                        enumTypeName   : 'Vizelia.FOL.BusinessEntities.ChartCalendarMode,Vizelia.FOL.Common',
                                        sortByValues   : true,
                                        value          : 0,
                                        flex           : 1,
                                        allowBlank     : false,
                                        valuesToRemove : ['SplitByEvent']
                                    }]

                        }, {
                            fieldLabel   : $lang('msg_alarmdefinition_timeinterval'),
                            name         : 'TimeInterval',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common',
                            value        : Vizelia.FOL.BusinessEntities.AxisTimeInterval.None,
                            sortByValues : true,
                            displayIcon  : true
                            // ,
                        // valuesToRemove : ['None']
                    }   , {
                            fieldLabel : $lang('msg_chart_timezone'),
                            name       : 'TimeZoneId',
                            allowBlank : false,
                            xtype      : 'vizComboTimeZone',
                            autoLoad   : true
                        }];
            },
            /**
             * config of the form containing the alarm condition
             * @param {} config
             * @return {}
             */
            configFormCondition                  : function(config) {
                return [{
                            fieldLabel     : $lang('msg_alarmdefinition_condition'),
                            name           : 'Condition',
                            xtype          : 'vizComboEnum',
                            enumTypeName   : 'Vizelia.FOL.BusinessEntities.FilterCondition,Vizelia.FOL.Common',
                            valuesToRemove : ['Like'],
                            allowBlank     : false,
                            listeners      : {
                                select : function(combo, record) {
                                    this.updateThresholds(record.data.Value);

                                },
                                scope  : this
                            }
                        }, {
                            fieldLabel       : ' = ', // $lang('msg_alarmdefinition_threshold'),
                            xtype            : 'viznumberfield',
                            allowBlank       : true,
                            hidden           : true,
                            name             : 'Threshold',
                            decimalPrecision : 5
                        }, {
                            fieldLabel       : $lang('msg_alarmdefinition_threshold2'),
                            xtype            : 'viznumberfield',
                            allowBlank       : true,
                            hidden           : true,
                            name             : 'Threshold2',
                            decimalPrecision : 5
                        }, {
                            fieldLabel : $lang('msg_alarmdefinition_usepercentagedelta'),
                            name       : 'UsePercentageDelta',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }];

            },

            /**
             * config of the form containing the alarm actionrequest
             * @param {} config
             * @return {}
             */
            configFormActionRequest              : function(config) {
                return [{
                            fieldLabel           : $lang('msg_actionrequest'),
                            xtype                : 'viztreecomboClassificationItemWork',
                            name                 : 'ClassificationKeyChildren',
                            hidden               : true,
                            originalDisplayValue : 'ClassificationLongPath',
                            serviceParams        : {
                                flgFilter           : config.mode == "update" ? false : true,
                                excludePset         : true,
                                excludeRelationship : true,
                                entity              : Viz.Configuration.GetClassificationItemDefinitionEntityFromCategory("work")
                            },
                            allowBlank           : true,
                            validator            : Ext.form.VTypes.actionRequestClassificationItemValidator
                        }, {
                            xtype      : 'textarea',
                            allowBlank : true,
                            fieldLabel : $lang('msg_chartscheduler_emails'),
                            name       : 'Emails'
                        }, {
                            fieldLabel : $lang('msg_playlist_culture'),
                            name       : 'KeyLocalizationCulture',
                            hiddenName : 'KeyLocalizationCulture',
                            autoLoad   : config.mode == 'create',
                            xtype      : 'vizComboLocalizationCulture'
                        }];

            },

            /**
             * Update the ThresHold fields enable/disable status
             * @param {} conditionValue
             */
            updateThresholds                     : function(conditionValue) {
                var f = this.getForm();
                f.findField('Threshold').setVisible(true);

                switch (conditionValue) {
                    case Vizelia.FOL.BusinessEntities.FilterCondition.Between :
                    case Vizelia.FOL.BusinessEntities.FilterCondition.Outside :
                        f.findField('Threshold').setVisible(true);
                        f.findField('Threshold').enable();
                        f.findField('Threshold2').enable();
                        f.findField('Threshold2').setVisible(true);
                        f.findField('UsePercentageDelta').setVisible(true);
                        break;
                    case Vizelia.FOL.BusinessEntities.FilterCondition.Equals :
                    case Vizelia.FOL.BusinessEntities.FilterCondition.NotEquals :
                    case Vizelia.FOL.BusinessEntities.FilterCondition.Higher :
                    case Vizelia.FOL.BusinessEntities.FilterCondition.Lower :
                        f.findField('Threshold').setVisible(true);
                        f.findField('Threshold').enable();
                        f.findField('Threshold2').disable();
                        f.findField('Threshold2').setVisible(false);
                        f.findField('UsePercentageDelta').setVisible(true);
                        break;

                    case Vizelia.FOL.BusinessEntities.FilterCondition.Script :
                        f.findField('Threshold').disable();
                        f.findField('Threshold').setVisible(false);
                        f.findField('Threshold2').disable();
                        f.findField('Threshold2').setVisible(false);
                        f.findField('UsePercentageDelta').setVisible(false);
                        break;

                }
                // We update the label as well
                switch (conditionValue) {
                    case Vizelia.FOL.BusinessEntities.FilterCondition.Between :
                        if (f.findField('Threshold').label)
                            f.findField('Threshold').label.update(' >= ');
                        else
                            f.findField('Threshold').fieldLabel = ' >= ';

                        if (f.findField('Threshold2').label)
                            f.findField('Threshold2').label.update(' <= ');
                        else
                            f.findField('Threshold2').fieldLabel = ' <= ';
                        break;

                    case Vizelia.FOL.BusinessEntities.FilterCondition.Outside :
                        if (f.findField('Threshold').label)
                            f.findField('Threshold').label.update(' <= ');
                        else
                            f.findField('Threshold').fieldLabel = ' <= ';

                        if (f.findField('Threshold2').label)
                            f.findField('Threshold2').label.update(' >= ');
                        else
                            f.findField('Threshold2').fieldLabel = ' >= ';
                        break;
                    case Vizelia.FOL.BusinessEntities.FilterCondition.Equals :
                        if (f.findField('Threshold').label)
                            f.findField('Threshold').label.update(' = ');
                        else
                            f.findField('Threshold').fieldLabel = ' = ';
                        break;
                    case Vizelia.FOL.BusinessEntities.FilterCondition.NotEquals :
                        if (f.findField('Threshold').label)
                            f.findField('Threshold').label.update(' <> ');
                        else
                            f.findField('Threshold').fieldLabel = ' <> ';
                        break;
                    case Vizelia.FOL.BusinessEntities.FilterCondition.Higher :
                        if (f.findField('Threshold').label)
                            f.findField('Threshold').label.update(' >= ');
                        else
                            f.findField('Threshold').fieldLabel = ' >= ';
                        break;

                    case Vizelia.FOL.BusinessEntities.FilterCondition.Lower :
                        if (f.findField('Threshold').label)
                            f.findField('Threshold').label.update(' <= ');
                        else
                            f.findField('Threshold').fieldLabel = ' <= ';
                        break;
                }
            },

            /**
             * Handler on the beforeadd event of the grid FilterMeterClassification
             */
            onBeforeAddFilterMeterClassification : function(grid, entity) {
                if (this.storeFilterSpatial.hasLoaded)
                    this.storeFilterSpatial.save();

                if (!this.storeFilterSpatial.data || this.storeFilterSpatial.data.length == 0) {
                    Ext.MessageBox.alert($lang("msg_alarmdefinition"), $lang("msg_chart_wizard_spatial_empty"));
                    return false;
                }

                entity.KeyAlarmDefinition = this.entity.KeyAlarmDefinition;
                entity.filterSpatial = this.storeFilterSpatial.crudStore;

                return true;
            },

            /**
             * Handler for updating fiels based on the form values.
             * @param {Viz.form.FormPanel} the form panel
             * @param {Object} values
             */
            onAfterLoadValues                    : function(formpanel, values) {
                this.updateThresholds(values.Condition);
            },

            /**
             * Before save handler.
             * @param {Viz.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                         : function(form, item, extraParams) {
                var storeArray = this.getExtraStoreArray();

                Ext.each(storeArray, function(store) {
                            if (store.hasLoaded)
                                store.save();
                        }, this);

                Ext.apply(extraParams, {
                            filterSpatial             : this.storeFilterSpatial.crudStore,
                            filterMeterClassification : this.storeFilterMeterClassification.crudStore,
                            meters                    : this.storeMeter.crudStore,
                            filterSpatialPset         : this.storeFilterSpatialPset.crudStore,
                            charts                    : this.storeChart.crudStore
                        });

                if (Ext.isEmpty(item.Threshold))
                    item.Threshold = null;
                if (Ext.isEmpty(item.Threshold2))
                    item.Threshold2 = null;
                var listboxtree = form.getForm().findField("ClassificationKeyChildren");
                var node = listboxtree.getSelectedNode();
                if (node)
                    item.ClassificationKeyParent = node.parentNode.attributes.Key;
                return true;
            },

            /**
             * Return an array of all the extra store of the Chart.
             * @return {Array}
             */
            getExtraStoreArray                   : function() {
                return [this.storeFilterSpatial, this.storeFilterMeterClassification, this.storeMeter, this.storeFilterSpatialPset, this.storeChart];
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                        : function(form, action) {
                Viz.form.AlarmDefinition.superclass.onSaveSuccess.apply(this, arguments);
                var tabpanel = this.getTabContainer();

                var storeArray = this.getExtraStoreArray();
                storeArray.push(this.storeMeterAll);

                Ext.each(storeArray, function(store) {
                            store.serviceParams.KeyAlarmDefinition = this.entity.KeyAlarmDefinition;
                            if (store.hasLoaded)
                                store.reload();
                        }, this);

                tabpanel.unhideTabStripItem(this._tabMeterAll);
                tabpanel.unhideTabStripItem(this._tabInstances);

                var gridSpatialPset = Ext.getCmp(this._gridFilterSpatialPset);
                if (gridSpatialPset) {
                    gridSpatialPset.KeyAlarmDefinition = this.entity.KeyAlarmDefinition;
                    gridSpatialPset.show();
                }

                return true;
            }
        });
Ext.reg('vizFormAlarmDefinition', Viz.form.AlarmDefinition);

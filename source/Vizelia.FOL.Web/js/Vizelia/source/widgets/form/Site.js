﻿Ext.namespace('Viz.form');

/**
 * @class Viz.form.Site
 * @extends Ext.FormPanel.
 */
Viz.form.Site = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                this._tabContainerId = Ext.id();
                var defaultConfig = {
                    name                 : 'Site',
                    mode_load            : 'remote',
                    psetConfig           : {
                        IfcType                    : 'IfcSite',
                        entityKey                  : 'KeySite',
                        filterByClassificationItem : false
                    },
                    serviceHandler       : Viz.Services.CoreWCF.Site_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.Site_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.Site_FormUpdate,
                    hasValidationToolbar : true,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Site.superclass.constructor.call(this, config);

            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral : function(config) {
                return [{
                            fieldLabel : $lang('msg_localid'),
                            allowBlank : false,
                            name       : 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_name'),
                            allowBlank : false,
                            name       : 'Name'
                        }, {
                            xtype      : 'textarea',
                            anchor     : Viz.Const.UI.Anchor,// + ' -60',
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description'
                        }]
            }
        });

Ext.reg('vizFormSite', Viz.form.Site);

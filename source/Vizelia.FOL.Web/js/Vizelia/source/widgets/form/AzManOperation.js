﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.AzManOperation
 * @extends Ext.FormPanel.
 */
Viz.form.AzManOperation = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};

                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.AuthenticationWCF.AzManOperation_FormLoad,
                    serviceHandlerCreate : Viz.Services.AuthenticationWCF.AzManOperation_FormCreate,
                    serviceHandlerUpdate : Viz.Services.AuthenticationWCF.AzManOperation_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {
                    auditEntityName : 'AuthorizationItem.Operation'
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.AzManOperation.superclass.constructor.call(this, config);

            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral : function(config) {
                return [{
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            anchor     : Viz.Const.UI.Anchor + ' -40',
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description'
                        }];
            }
        });

Ext.reg('vizFormAzManOperation', Viz.form.AzManOperation);
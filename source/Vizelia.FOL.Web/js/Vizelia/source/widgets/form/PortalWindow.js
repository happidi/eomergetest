﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.PortalWindow
 * @extends Ext.FormPanel.
 */
Viz.form.PortalWindow = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                   : function(config) {
                this._gridPortalTab = Ext.id();
                this._tabPortalTab = Ext.id();

                var Id = config.entity ? config.entity.KeyPortalWindow : null;
                this.storeTab = new Viz.store.PortalTab({
                            KeyPortalWindow : Id
                        });
                config = config || {};

                var items = [];
                items.push({
                            title      : $lang('msg_general'),
                            iconCls    : 'viz-icon-small-form-update',
                            items      : this.configFormGeneral(config),
                            labelWidth : 200
                        });
                if (config.isWizard !== true) {
                    items.push({
                                title      : $lang('msg_portalwindow_tabs'),
                                labelAlign : 'top',
                                layout     : 'fit',
                                hidden     : config.mode == 'create',
                                id         : this._tabPortalTab,
                                items      : [this.configGridPanelTab(config)]
                            });
                }
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.PortalWindow_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.PortalWindow_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.PortalWindow_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : items
                    },
                    closeonsave          : false

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.PortalWindow.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral             : function(config) {
                return [{
                            name   : 'KeyPortalWindow',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_localid'),
                            name       : 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_portalwindow_title'),
                            name       : 'Title',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_portalwindow_icon'),
                            name       : 'IconCls',
                            xtype      : 'vizComboCss',
                            selector   : '.viz-portal-large-',
                            allowBlank : true,
                            listeners  : {
                                select : function(combo, record, index) {
                                    this.form.findField('KeyImage').clearValue();
                                    this.form.findField('KeyImage').clearInvalid();                                    
                                },
                                scope  : this
                            }
                        }, Viz.combo.DynamicDisplayImage.factory({
                                    fieldLabel : $lang('msg_portalwindow_customicon'),
                                    name       : 'KeyImage',
                                    type       : Vizelia.FOL.BusinessEntities.DynamicDisplayImageType.Icon,
                                    allowBlank : true,
                                    listeners  : {
                                        select : function(combo, record, index) {
                                            this.form.findField('IconCls').clearValue();
                                            this.form.findField('IconCls').clearInvalid();
                                        },
                                        scope  : this
                                    }
                                }), {
                            fieldLabel : $lang('msg_portalwindow_displayintoolbar'),
                            name       : 'DisplayInToolbar',
                            xtype      : 'vizcheckbox',
                            checked    : true,
                            hidden     : true
                        }, {
                            fieldLabel   : $lang('msg_portalwindow_defaulttabcolumnconfig'),
                            name         : 'PortalTabColumnConfig',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.PortalTabColumnConfig,Vizelia.FOL.Common',
                            displayIcon  : true,
                            hidden       : config.mode == 'update',
                            value        : Vizelia.FOL.BusinessEntities.PortalTabColumnConfig.Column2_5050
                        }, {
                            fieldLabel : $lang('msg_portalwindow_openonstartup'),
                            name       : 'OpenOnStartup',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }, {
                            fieldLabel : $lang('msg_portalwindow_preloadtab'),
                            name       : 'PreloadTab',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }, {
                            fieldLabel : $lang('msg_portalwindow_printwidth'),
                            name       : 'PrintWidth',
                            xtype      : 'vizSpinnerField',
                            minValue   : 0,
                            maxValue   : 4096,
                            allowBlank : true
                        }, {
                            fieldLabel : $lang('msg_portalwindow_printheight'),
                            name       : 'PrintHeight',
                            xtype      : 'vizSpinnerField',
                            minValue   : 0,
                            maxValue   : 4096,
                            allowBlank : true
                        }, {
                            fieldLabel : $lang('msg_creator'),
                            name       : 'CreatorLogin',
                            readOnly   : true,
                            hidden     : config.mode == 'create'
                        }]
            },

            /**
             * The configuration for the grid priority
             * @return {object}
             */
            configGridPanelTab            : function(config) {
                return {
                    xtype           : 'vizGridPortalTab',
                    border          : true,
                    loadMask        : false,
                    id              : this._gridPortalTab,
                    store           : this.storeTab,
                    KeyPortalWindow : config.entity.KeyPortalWindow,
                    columns         : Viz.Configuration.Columns.PortalTab,
                    listeners       : {
                        beforedeleterecord : this.onDeletedRecordTab,
                        scope              : this
                    }
                };
            },
            /**
             * Handler for the columns grid delete.
             */
            onDeletedRecordTab            : function() {
                this.deleteSelectedRowsFromGrid(this._gridPortalTab);
                return false;
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                  : function(form, item, extraParams) {
                // validate
                if (!this.validateIconClsKeyImageFields())
                    return false;

                if (this.storeTab.hasLoaded)
                    this.storeTab.save();
                // this.form.findField('PortalTabColumnConfig').getValue()
                Ext.apply(extraParams, {
                            portaltab : this.storeTab.crudStore
                        });
                return true;
            },

            /**
             * Validates that either IconCls or KeyImage is picked.
             * @return {Boolean}
             */
            validateIconClsKeyImageFields : function() {
                var fieldIcon = this.getForm().findField("IconCls");
                var fieldImage = this.getForm().findField("KeyImage");
                if (!fieldIcon.value && !fieldImage.value) {
                    var errorMsg = $lang('error_msg_pick_icon_or_custom_icon');
                    fieldIcon.markInvalid(errorMsg);
                    fieldImage.markInvalid(errorMsg);
                    return false;
                }

                return true;
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                 : function(form, action) {
                Viz.form.PortalWindow.superclass.onSaveSuccess.apply(this, arguments);

                this.entity = action.result.data;
                if (this.isWizard) {
                    Viz.App.Global.ViewPort.openModule({
                                xtype           : 'vizPortalPanel',
                                KeyPortalWindow : this.entity.KeyPortalWindow,
                                title           : Viz.util.Portal.localizeTitle(this.entity.Title),
                                iconCls         : Ext.isEmpty(this.entity.IconCls) ? Viz.util.Portal.getCssFromImage(this.entity.KeyImage, 16, 16) : this.entity.IconCls.replace('-large-', '-small-')

                            }, true, true);
                }
                if (this.mode == "create")
                    this.closeonsave = true;
                return true;
                /*
                 * var tabpanel = this.getTabContainer(); tabpanel.unhideTabStripItem(this._tabPortalTab); var grid = Ext.getCmp(this._gridPortalTab); grid.KeyPortalWindow = grid.store.serviceParams.KeyPortalWindow = this.entity.KeyPortalWindow; grid.store.reload();
                 */

            }
        });
Ext.reg('vizFormPortalWindow', Viz.form.PortalWindow);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.StruxureWareDataAcquisitionContainer
 * @extends Ext.FormPanel.
 */
Viz.form.StruxureWareDataAcquisitionContainer = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                       : function(config) {
                this._gridDataAcquisitionEndpoint = Ext.id();
                this._tabEndpoint = Ext.id();

                var Id = config.entity ? config.entity.KeyDataAcquisitionContainer : null;
                this.storeDataAcquisitionEndpoint = new Viz.store.StruxureWareDataAcquisitionEndpoint({
                            KeyDataAcquisitionContainer : Id
                        });

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.StruxureWareDataAcquisitionContainer_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.StruxureWareDataAcquisitionContainer_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.StruxureWareDataAcquisitionContainer_FormUpdate,
                    tabConfig            : {
                        deferredRender : true,
                        items          : [{
                                    title   : $lang('msg_general'),
                                    iconCls : 'viz-icon-small-form-update',
                                    items   : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_struxurewaredataacquisitionendpoint'),
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    id         : this._tabEndpoint,
                                    hidden     : config.mode == 'create',
                                    iconCls    : 'viz-icon-small-struxurewaredataacquisitionendpoint',
                                    items      : [this.configGridDataAcquisitionEndpoint(config)]
                                }]
                    },
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.StruxureWareDataAcquisitionContainer.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                 : function(config) {
                return [{
                            name   : 'KeyDataAcquisitionContainer',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_localid'),
                            name       : 'LocalId',
                            readOnly   : config.mode == 'update' ? true : false,
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_title'),
                            name       : 'Title',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_struxurewaredataacquisitioncontainer_username'),
                            name       : 'Username',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_struxurewaredataacquisitioncontainer_password'),
                            name       : 'Password',
                            inputType  : 'password',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_struxurewaredataacquisitioncontainer_ip'),
                            name       : 'Url',
                            allowBlank : false
                        }];

            },

            /**
             * The configuration for the grid DataAcquisitionEndpoint
             * @return {object}
             */
            configGridDataAcquisitionEndpoint : function(config) {
                return {
                    id                          : this._gridDataAcquisitionEndpoint,
                    xtype                       : 'vizGridStruxureWareDataAcquisitionEndpoint',
                    loadMask                    : true,
                    store                       : this.storeDataAcquisitionEndpoint,
                    KeyDataAcquisitionContainer : config.entity ? config.entity.KeyDataAcquisitionContainer : null,
                    entityContainer             : config.entity,
                    border                      : true,
                    listeners                   : {
                        beforedeleterecord : this.onDeletedRecordTab,
                        scope              : this
                    }
                };
            },

            /**
             * Handler for the columns grid delete.
             */
            onDeletedRecordTab                : function() {
                this.deleteSelectedRowsFromGrid(this._gridDataAcquisitionEndpoint);
                return false;
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                      : function(form, item, extraParams) {
                if (this.storeDataAcquisitionEndpoint.hasLoaded)
                    this.storeDataAcquisitionEndpoint.save();
                return true;
            },

            /**
             * Handler for save success.
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                     : function(form, action) {
                Viz.form.StruxureWareDataAcquisitionContainer.superclass.onSaveSuccess.apply(this, arguments);

                var tabpanel = this.getTabContainer();
                tabpanel.unhideTabStripItem(this._tabEndpoint);

                var grid = Ext.getCmp(this._gridDataAcquisitionEndpoint);
                grid.KeyDataAcquisitionContainer = grid.store.serviceParams.KeyDataAcquisitionContainer = this.entity.KeyDataAcquisitionContainer;

            }
        });

Ext.reg('vizFormStruxureWareDataAcquisitionContainer', Viz.form.StruxureWareDataAcquisitionContainer);
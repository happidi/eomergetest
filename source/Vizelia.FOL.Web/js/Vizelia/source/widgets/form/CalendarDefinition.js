﻿Ext.namespace('Viz.form');

/**
 * @class Viz.form.CalendarDefinition
 * @extends Ext.FormPanel.
 */
Viz.form.CalendarDefinition = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                 : function(config) {
                config = config || {};

                this.generateIds();

                var defaultConfig = {
                    mode_load                : 'remote',
                    serviceHandler           : Viz.Services.CoreWCF.CalendarEvent_FormLoad,
                    serviceHandlerCreate     : Viz.Services.CoreWCF.CalendarEvent_FormCreate,
                    serviceHandlerUpdate     : Viz.Services.CoreWCF.CalendarEvent_FormUpdate,
                    serviceParams            : {
                        KeyLocation : config.KeyLocation
                    },
                    /**
                     * @cfg {Boolean} eventHasDuration <tt>true</tt> if the event has an end datetime, <tt>false</tt> otherwise.
                     */
                    eventHasDuration         : true,
                    /**
                     * @cfg {Boolean} showMinutely <tt>true</tt> if the minutely option should appear, <tt>false</tt> otherwise.
                     */
                    showMinutely             : false,
                    /**
                     * @cfg {Boolean} showHourly <tt>true</tt> if the hourly option should appear, <tt>false</tt> otherwise.
                     */
                    showHourly               : false,
                    /**
                     * @cfg {Boolean} showDaily <tt>true</tt> if the daily option should appear, <tt>false</tt> otherwise.
                     */
                    showDaily                : true,
                    /**
                     * @cfg {Boolean} showWeekly <tt>true</tt> if the weekly option should appear, <tt>false</tt> otherwise.
                     */
                    showWeekly               : true,
                    /**
                     * @cfg {Boolean} showWeeklyRecurs <tt>true</tt> if the weekly recurs option should appear, <tt>false</tt> otherwise.
                     */
                    showWeeklyRecurs         : true,
                    /**
                     * @cfg {Boolean} showMonthly <tt>true</tt> if the monthly option should appear, <tt>false</tt> otherwise.
                     */
                    showMonthly              : true,
                    /**
                     * @cfg {Boolean} showYearly <tt>true</tt> if the yearly option should appear, <tt>false</tt> otherwise.
                     */
                    showYearly               : true,
                    /**
                     * @cfg {Boolean} showYearlyRecurs <tt>true</tt> if the yearly recurs option should appear, <tt>false</tt> otherwise.
                     */
                    showYearlyRecurs         : true,
                    /**
                     * @cfg {Boolean} showEndDate <tt>true</tt> if the showEndDate option should appear, <tt>false</tt> otherwise.
                     */
                    showEndDate              : true,
                    /**
                     * @cfg {Boolean} fullEndDate <tt>true</tt> if the full date time until should appear, <tt>false</tt> otherwise.
                     */
                    fullEndDate              : false,
                    /**
                     * @cfg {Boolean} showExplictMonths <tt>true</tt> if the months should be chosen explicitly, <tt>false</tt> otherwise.
                     */
                    showExplicitMonths       : false,
                    /**
                     * @cfg {Boolean} showNumberOccurrence <tt>true</tt> if the showNumberOccurrence option should appear, <tt>false</tt> otherwise.
                     */
                    showNumberOccurrence     : true,
                    /**
                     * @cfg {Boolean} showTitle <tt>true</tt> if the title should appear in the form, <tt>false</tt> otherwise.
                     */
                    showTitle                : true,
                    /**
                     * @cfg {Boolean} showHasRecurrencePattern <tt>true</tt> if the HasRecurrencePattern checkbox should appear in the form, <tt>false</tt> otherwise.
                     */
                    showHasRecurrencePattern : true,
                    /**
                     * @cfg {String} Category The category of the event.
                     */
                    Category                 : config.serviceParams ? config.serviceParams.Category || 'Holiday' : 'Holiday'

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.applyIf(config, {
                            hasValidationToolbar : true,
                            tabConfig            : {
                                deferredRender : false,
                                items          : [{
                                            title   : $lang('msg_general'),
                                            iconCls : 'viz-icon-small-calendarevent',
                                            items   : this.configFormGeneral(config)
                                        }, {
                                            title   : $lang('msg_calendar_recurrence'),
                                            iconCls : 'viz-icon-small-calendarevent-recurrence',
                                            items   : [this.configFormRecurrence(config)]
                                        }]
                            }
                        });
                Viz.form.CalendarDefinition.superclass.constructor.call(this, config);

            },
            /**
             * Generate internal ids for some form fields.
             */
            generateIds                 : function() {
                if (!this.frequency_daily_section_id) {
                    this.frequency_minutely_section_id = Ext.id();
                    this.frequency_hourly_section_id = Ext.id();
                    this.frequency_daily_section_id = Ext.id();
                    this.frequency_weekly_section_id = Ext.id();
                    this.frequency_monthly_section_id = Ext.id();
                    this.frequency_yearly_section_id = Ext.id();
                    this.frequency_detail_id = Ext.id();
                    this.checkboxgroup_dayofweek_id = Ext.id();
                    this.checkboxgroup_month_id = Ext.id();
                    this.datestart_id = Ext.id();
                    this.dateend_id = Ext.id();
                }
            },

            /**
             * The general tab
             * @param {Object} config The configuration options of the tab.
             */
            configFormGeneral           : function(config) {
                return [{
                            fieldLabel : $lang('msg_localid'),
                            name       : 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_calendarevent_title'),
                            name       : 'Title',
                            allowBlank : !config.showTitle,
                            hidden     : !config.showTitle
                        }, {

                            fieldLabel   : $lang('msg_calendarevent_startdate'),
                            name         : 'StartDate',
                            xtype        : 'vizDateTime',
                            firstLoad    : true,
                            id           : this.datestart_id,
                            width        : 200,
                            validator    : Ext.form.VTypes.daterangesimple,
                            hideTime     : false,
                            endDateField : this.dateend_id,
                            switchName   : 'IsAllDayUI',
                            hasSwitch    : config.eventHasDuration,
                            listeners    : {
                                scope       : this,
                                checkallday : function(comp, value) {
                                    var startDate = comp.getValue();
                                    var endDate = Ext.getCmp(this.dateend_id).getValue();
                                    if (value && this.mode === 'update' && comp.firstLoad) {
                                        endDate = endDate.add(Date.DAY, -1);
                                        if (endDate >= startDate)
                                            Ext.getCmp(this.dateend_id).setValue(endDate);
                                        comp.firstLoad = false;
                                    }
                                    else if (endDate == null)
                                        Ext.getCmp(this.dateend_id).setValue(comp.getValue());
                                    Ext.getCmp(this.dateend_id).disableTime(value);
                                },
                                selectdate  : function(comp, datefield, value) {
                                    comp.setValue(value); // to force time
                                    var endfield = Ext.getCmp(this.dateend_id);
                                    var end = endfield.getValue();
                                    if ((!end) || (end && Ext.isDate(end) && end <= comp.getValue())) {
                                        if (comp.isAllDay())
                                            endfield.setValue(value.add(Date.DAY, 1));
                                        else
                                            endfield.setValue(value.add(Date.MINUTE, 30));
                                    }
                                },
                                selecttime  : function(comp, timefield, record, index, value) {
                                    if (value) {
                                        var endfield = Ext.getCmp(this.dateend_id);
                                        var end = endfield.getValue();

                                        if ((!end) || (end && Ext.isDate(end) && end <= comp.getValue()))
                                            endfield.setValue(value.add(Date.MINUTE, 30));

                                        if (this.eventHasDuration === false) {
                                            endfield.setValue(value.add(Date.MINUTE, 1));
                                        }
                                    }
                                }
                            }
                            // listeners : {
                        // select : this.onStartDateSelect,
                        // scope : this
                        // }
                    }   , {
                            fieldLabel     : $lang('msg_calendarevent_enddate'),
                            name           : 'EndDate',
                            hideTime       : false,
                            id             : this.dateend_id,
                            xtype          : 'vizDateTime',
                            validator      : Ext.form.VTypes.daterangesimple,
                            startDateField : this.datestart_id,
                            width          : 200,
                            hasSwitch      : false,
                            hidden         : !config.eventHasDuration,
                            hideLabel      : !config.eventHasDuration,
                            height         : config.eventHasDuration ? 22 : 0

                        }, {
                            fieldLabel : $lang('msg_calendarevent_location'),
                            name       : 'Location',
                            hidden     : true,
                            hideLabel  : true
                        }, {
                            fieldLabel : $lang('msg_calendarevent_hasrecurrencepattern'),
                            name       : 'HasRecurrencePattern',
                            inputValue : true,
                            checked    : true,
                            xtype      : 'checkbox',
                            hidden     : !config.showHasRecurrencePattern,
                            listeners  : {
                                check : this.onCheckHasRecurrencePattern,
                                scope : this
                            }
                        }, {
                            fieldLabel : $lang('msg_chart_timezone'),
                            name       : 'TimeZoneId',
                            allowBlank : false,
                            xtype      : 'vizComboTimeZone',
                            autoLoad   : true
                        }, {
                            fieldLabel       : $lang('msg_calendarevent_factor'),
                            name             : 'Factor',
                            xtype            : 'vizSpinnerField',
                            allowBlank       : true,
                            value            : 1,
                            decimalPrecision : 5,
                            hidden           : config.Category == 'ChartScheduler' || config.Category == 'ReportSubscriptionSchedule'
                        }, {
                            fieldLabel : $lang('msg_calendarevent_unit'),
                            name       : 'Unit',
                            hidden     : true,
                            allowBlank : true
                        }];
            },

            /**
             * The recurrence tab.
             * @param {Object} config The configuration options of the recurrence tab of the form.
             */
            configFormRecurrence        : function(config) {
                return [{
                            xtype  : 'fieldset',
                            title  : $lang('msg_calendarevent_recurrencepattern'),
                            layout : 'column',
                            items  : [this.configFormFrequency(config), this.configFormFrequencyDetail(config)]
                        }, {
                            xtype : 'fieldset',
                            title : $lang('msg_calendarevent_recurrencerange'),
                            // layout : 'form',
                            items : [this.configFormEnd(config)]

                        }];
            },

            /**
             * Configuration for Frequency detail panel
             * @param {Object} config The configuration options of the form
             * @return {Object}
             */
            configFormFrequencyDetail   : function(config) {
                return {
                    layout             : 'card',
                    columnWidth        : '1',
                    height             : 125,
                    activeItem         : 3,
                    border             : false,
                    layoutOnCardChange : true,
                    bodyStyle          : 'background-color:transparent;padding:0px',
                    defaults           : {
                        border : false
                    },
                    id                 : this.frequency_detail_id,
                    items              : [this.configFormMinutely(config), this.configFormHourly(config), this.configFormDaily(config), this.configFormWeekly(config), this.configFormMonthly(config), this.configFormYearly(config)]
                };

            },

            /**
             * Config for Frequency options
             * @private
             * @param {Object} config The configuration option of the form
             */
            configFormFrequency         : function(config) {
                return {
                    border      : false,
                    layout      : 'form',
                    width       : 120,
                    defaultType : 'radio',
                    bodyStyle   : 'background-color:transparent;padding:4px 20px 4px 4px;border-right:#B5B8C8 1px solid',
                    defaults    : {
                        hideLabel      : true,
                        fieldLabel     : '',
                        labelSeparator : '',
                        labelStyle     : 'white-space:nowrap;overflow:hidden',
                        name           : 'Frequency',
                        listeners      : {
                            check : this.onCheckFrequency,
                            scope : this
                        }
                    },
                    items       : [{
                                boxLabel   : $lang('msg_calendarevent_minutely'),
                                inputValue : Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Minutely,
                                hidden     : !config.showMinutely
                            }, {
                                boxLabel   : $lang('msg_calendarevent_hourly'), // $lang('msg_calendarevent_daily'),
                                inputValue : Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Hourly,
                                hidden     : !config.showHourly
                            }, {
                                boxLabel   : $lang('msg_calendarevent_daily'),
                                inputValue : Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Daily,
                                hidden     : !config.showDaily
                            }, {
                                boxLabel   : $lang('msg_calendarevent_weekly'),
                                inputValue : Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Weekly,
                                hidden     : !config.showWeekly
                            }, {
                                boxLabel   : $lang('msg_calendarevent_monthly'),
                                inputValue : Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Monthly,
                                hidden     : !config.showMonthly

                            }, {
                                boxLabel   : $lang('msg_calendarevent_yearly'),
                                inputValue : Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Yearly,
                                hidden     : !config.showYearly,
                                listeners  : {
                                    scope       : this,
                                    check       : this.onCheckFrequency,
                                    afterrender : function(comp) {
                                        comp.setValue(true);
                                    }
                                }
                            }]
                };
            },

            /**
             * Config for Minutely options
             * @private
             * @param {Object} config The configuration option of the form
             */
            configFormMinutely          : function(config) {
                return {
                    id         : this.frequency_minutely_section_id,
                    layout     : 'form',
                    border     : false,
                    defaults   : {
                        anchor : Viz.Const.UI.Anchor,
                        style  : 'background-color:transparent;'
                    },
                    bodyStyle  : 'background-color:transparent;padding:10px;',
                    labelWidth : 100,
                    items      : [{
                                xtype      : 'compositefield',
                                fieldLabel : $lang('msg_calendarevent_recursevery'),
                                items      : [{
                                            xtype         : 'viznumberfield',
                                            name          : 'frequency_minutely_interval',
                                            fieldLabel    : $lang('msg_calendarevent_interval'),
                                            minValue      : 1,
                                            allowDecimals : false,
                                            hideLabel     : true,
                                            width         : 40,
                                            flex          : 1,
                                            value         : 1
                                        }, {
                                            xtype : 'displayfield',
                                            value : $lang('msg_calendarevent_minutes'),
                                            flex  : 1
                                        }]
                            }]
                };
            },
            /**
             * Config for Hourly options
             * @private
             * @param {Object} config The configuration option of the form
             */
            configFormHourly            : function(config) {
                return {
                    id         : this.frequency_hourly_section_id,
                    layout     : 'form',
                    border     : false,
                    defaults   : {
                        anchor : Viz.Const.UI.Anchor,
                        style  : 'background-color:transparent;'
                    },
                    bodyStyle  : 'background-color:transparent;padding:10px;',
                    labelWidth : 100,
                    items      : [{
                                xtype      : 'compositefield',
                                fieldLabel : $lang('msg_calendarevent_recursevery'),
                                items      : [{
                                            xtype         : 'viznumberfield',
                                            name          : 'frequency_hourly_interval',
                                            fieldLabel    : $lang('msg_calendarevent_interval'),
                                            minValue      : 1,
                                            allowDecimals : false,
                                            hideLabel     : true,
                                            width         : 40,
                                            flex          : 1,
                                            value         : 1
                                        }, {
                                            xtype : 'displayfield',
                                            value : $lang('msg_calendarevent_hours'),
                                            flex  : 1
                                        }]
                            }]
                };
            },

            /**
             * Config for Daily options
             * @private
             * @param {Object} config The configuration option of the form
             */
            configFormDaily             : function(config) {
                return {
                    id         : this.frequency_daily_section_id,
                    layout     : 'form',
                    border     : false,
                    defaults   : {
                        anchor : Viz.Const.UI.Anchor,
                        style  : 'background-color:transparent;'
                    },
                    bodyStyle  : 'background-color:transparent;padding:10px;',
                    labelWidth : 100,
                    items      : [{
                                xtype      : 'compositefield',
                                fieldLabel : $lang('msg_calendarevent_recursevery'),
                                items      : [{
                                            xtype         : 'viznumberfield',
                                            name          : 'frequency_daily_interval',
                                            fieldLabel    : $lang('msg_calendarevent_interval'),
                                            minValue      : 1,
                                            allowDecimals : false,
                                            hideLabel     : true,
                                            width         : 40,
                                            flex          : 1,
                                            value         : 1
                                        }, {
                                            xtype : 'displayfield',
                                            value : $lang('msg_calendarevent_days'),
                                            flex  : 1
                                        }]
                            }]
                };
            },

            /**
             * Config for Monthly options
             * @private
             * @param {Object} config The configuration option of the form
             */
            configFormMonthly           : function(config) {
                return {
                    id         : this.frequency_monthly_section_id,
                    layout     : 'form',
                    border     : false,
                    labelWidth : 20,
                    defaults   : {
                        anchor : Viz.Const.UI.Anchor,
                        style  : 'background-color:transparent;'
                    },
                    bodyStyle  : 'background-color:transparent;padding:10px;',
                    items      : [{
                                xtype     : 'compositefield',
                                hideLabel : true,
                                items     : [{
                                            fieldLabel     : '',
                                            hideLabel      : true,
                                            labelSeparator : '',
                                            xtype          : 'radio',
                                            checked        : true,
                                            width          : 40,
                                            name           : 'frequency_monthly_choice',
                                            inputValue     : 'bymonthday',
                                            boxLabel       : $lang('msg_calendarevent_the'),
                                            listeners      : {
                                                check : this.onCheckMonthlyChoice,
                                                scope : this
                                            }
                                        }, {
                                            xtype         : 'viznumberfield',
                                            width         : 40,
                                            minValue      : 1,
                                            maxValue      : 31,
                                            allowDecimals : false,
                                            name          : 'frequency_monthly_day',
                                            fieldLabel    : $lang('msg_calendarevent_day'),
                                            hideLabel     : true,
                                            value         : 1
                                        }, {
                                            xtype : 'displayfield',
                                            value : $lang('msg_calendarevent_ofevery')
                                        }, {
                                            xtype         : 'viznumberfield',
                                            width         : 40,
                                            minValue      : 1,
                                            allowDecimals : false,
                                            name          : 'frequency_monthly_bymonthday_interval',
                                            fieldLabel    : $lang('msg_calendarevent_interval'),
                                            hideLabel     : true,
                                            hidden        : config.showExplicitMonths,
                                            value         : 1
                                        }, {
                                            xtype  : 'displayfield',
                                            value  : $lang('msg_calendarevent_months'),
                                            hidden : config.showExplicitMonths
                                        }]
                            }, {
                                xtype     : 'compositefield',
                                hideLabel : true,
                                items     : [{
                                            fieldLabel     : '',
                                            hideLabel      : true,
                                            labelSeparator : '',
                                            name           : 'frequency_monthly_choice',
                                            inputValue     : 'bydayspecifier',
                                            xtype          : 'radio',
                                            width          : 40,
                                            boxLabel       : $lang('msg_calendarevent_the'),
                                            listeners      : {
                                                check : this.onCheckMonthlyChoice,
                                                scope : this
                                            }
                                        }, {
                                            xtype      : 'vizComboCalendarFrequencyOccurrence',
                                            width      : 100,
                                            name       : 'frequency_monthly_occurrence',
                                            hiddenName : 'frequency_monthly_occurrence',
                                            disabled   : true,
                                            fieldLabel : $lang('msg_calendarevent_occurrence'),
                                            hideLabel  : true
                                        }, {
                                            xtype      : 'vizComboCalendarDayName',
                                            width      : 100,
                                            name       : 'frequency_monthly_weekday',
                                            hiddenName : 'frequency_monthly_weekday',
                                            disabled   : true,
                                            fieldLabel : $lang('msg_calendarevent_weekday'),
                                            hideLabel  : true
                                        }, {
                                            xtype : 'displayfield',
                                            value : $lang('msg_calendarevent_ofevery')
                                        }, {
                                            xtype      : 'viznumberfield',
                                            width      : 40,
                                            name       : 'frequency_monthly_bydayspecifier_interval',
                                            disabled   : true,
                                            fieldLabel : $lang('msg_calendarevent_interval'),
                                            hideLabel  : true,
                                            hidden     : config.showExplicitMonths
                                        }, {
                                            xtype  : 'displayfield',
                                            value  : $lang('msg_calendarevent_months'),
                                            hidden : config.showExplicitMonths
                                        }]
                            },

                            {
                                // Use the default, automatic layout to distribute the controls evenly
                                // across a single row
                                xtype      : 'checkboxgroup',
                                id         : this.checkboxgroup_month_id,
                                name       : 'frequency_monthly_cbg',
                                fieldLabel : $lang('msg_calendarevent_month'),
                                hideLabel  : true,
                                hidden     : !config.showExplicitMonths,
                                columns    : 6,
                                defaults   : {
                                    // name: 'frequency_weekly_weekdays'
                                    name : 'frequency_monthly_months'
                                },
                                items      : [{
                                            boxLabel   : Date.monthNames[0],
                                            id         : this.checkboxgroup_month_id + Date.monthNames[0],
                                            inputValue : 1
                                        }, {
                                            boxLabel   : Date.monthNames[1],
                                            id         : this.checkboxgroup_month_id + Date.monthNames[1],
                                            inputValue : 2
                                        }, {
                                            boxLabel   : Date.monthNames[2],
                                            id         : this.checkboxgroup_month_id + Date.monthNames[2],
                                            inputValue : 3
                                        }, {
                                            boxLabel   : Date.monthNames[3],
                                            id         : this.checkboxgroup_month_id + Date.monthNames[3],
                                            inputValue : 4
                                        }, {
                                            boxLabel   : Date.monthNames[4],
                                            id         : this.checkboxgroup_month_id + Date.monthNames[4],
                                            inputValue : 5
                                        }, {
                                            boxLabel   : Date.monthNames[5],
                                            id         : this.checkboxgroup_month_id + Date.monthNames[5],
                                            inputValue : 6
                                        }, {
                                            boxLabel   : Date.monthNames[6],
                                            id         : this.checkboxgroup_month_id + Date.monthNames[6],
                                            inputValue : 7
                                        }, {
                                            boxLabel   : Date.monthNames[7],
                                            id         : this.checkboxgroup_month_id + Date.monthNames[7],
                                            inputValue : 8
                                        }, {
                                            boxLabel   : Date.monthNames[8],
                                            id         : this.checkboxgroup_month_id + Date.monthNames[8],
                                            inputValue : 9
                                        }, {
                                            boxLabel   : Date.monthNames[9],
                                            id         : this.checkboxgroup_month_id + Date.monthNames[9],
                                            inputValue : 10
                                        }, {
                                            boxLabel   : Date.monthNames[10],
                                            id         : this.checkboxgroup_month_id + Date.monthNames[10],
                                            inputValue : 11
                                        }, {
                                            boxLabel   : Date.monthNames[11],
                                            id         : this.checkboxgroup_month_id + Date.monthNames[11],
                                            inputValue : 12
                                        }]
                            }

                    ]
                };
            },

            /**
             * Config for Yearly options
             * @private
             * @param {Object} config The configuration option of the form
             */
            configFormYearly            : function(config) {
                return {
                    // hidden : true,
                    // columnWidth : '1',
                    id         : this.frequency_yearly_section_id,
                    layout     : 'form',
                    border     : false,
                    labelWidth : 20,
                    defaults   : {
                        anchor : Viz.Const.UI.Anchor,
                        style  : 'background-color:transparent;'
                    },
                    bodyStyle  : 'background-color:transparent;padding:10px;',
                    items      : [{
                                xtype     : 'compositefield',
                                hidden    : !config.showYearlyRecurs,
                                hideLabel : true,
                                items     : [{
                                            xtype : 'displayfield',
                                            value : $lang('msg_calendarevent_recursevery')
                                        }, {
                                            xtype         : 'viznumberfield',
                                            width         : 40,
                                            value         : 1,
                                            minValue      : 1,
                                            allowDecimals : false,
                                            fieldLabel    : $lang('msg_calendarevent_interval'),
                                            hideLabel     : true,
                                            name          : 'frequency_yearly_interval'
                                        }, {
                                            xtype : 'displayfield',
                                            value : $lang('msg_calendarevent_years')
                                        }]
                            }, {
                                xtype     : 'compositefield',
                                hideLabel : true,
                                items     : [{
                                            fieldLabel     : '',
                                            name           : 'frequency_yearly_choice',
                                            inputValue     : 'bymonthday',
                                            hideLabel      : true,
                                            labelSeparator : '',
                                            xtype          : 'radio',
                                            checked        : true,
                                            width          : 40,
                                            boxLabel       : $lang('msg_calendarevent_the'),
                                            listeners      : {
                                                check : this.onCheckYearlyChoice,
                                                scope : this
                                            }
                                        }, {
                                            xtype         : 'viznumberfield',
                                            width         : 40,
                                            name          : 'frequency_yearly_day',
                                            fieldLabel    : $lang('msg_calendarevent_day'),
                                            minValue      : 1,
                                            maxValue      : 31,
                                            allowDecimals : false,
                                            hideLabel     : true
                                        }, {
                                            xtype      : 'vizComboCalendarMonthName',
                                            width      : 100,
                                            hiddenName : 'frequency_yearly_bymonthday_month',
                                            name       : 'frequency_yearly_bymonthday_month',
                                            fieldLabel : $lang('msg_calendarevent_month'),
                                            hideLabel  : true
                                        }]
                            }, {
                                xtype     : 'compositefield',
                                hideLabel : true,
                                items     : [{
                                            fieldLabel     : '',
                                            name           : 'frequency_yearly_choice',
                                            inputValue     : 'bydayspecifier',
                                            hideLabel      : true,
                                            labelSeparator : '',
                                            xtype          : 'radio',
                                            width          : 40,
                                            boxLabel       : $lang('msg_calendarevent_the'),
                                            listeners      : {
                                                check : this.onCheckYearlyChoice,
                                                scope : this
                                            }
                                        }, {
                                            xtype      : 'vizComboCalendarFrequencyOccurrence',
                                            width      : 100,
                                            name       : 'frequency_yearly_occurrence',
                                            hiddenName : 'frequency_yearly_occurrence',
                                            fieldLabel : $lang('msg_calendarevent_occurrence'),
                                            hideLabel  : true,
                                            disabled   : true
                                        }, {
                                            xtype      : 'vizComboCalendarDayName',
                                            width      : 100,
                                            name       : 'frequency_yearly_weekday',
                                            hiddenName : 'frequency_yearly_weekday',
                                            fieldLabel : $lang('msg_calendarevent_day'),
                                            hideLabel  : true,
                                            disabled   : true
                                        }, {
                                            xtype : 'displayfield',
                                            value : $lang('msg_calendarevent_of')
                                        }, {
                                            xtype      : 'vizComboCalendarMonthName',
                                            width      : 100,
                                            name       : 'frequency_yearly_bydayspecifier_month',
                                            hiddenName : 'frequency_yearly_bydayspecifier_month',
                                            fieldLabel : $lang('msg_calendarevent_month'),
                                            hideLabel  : true,
                                            disabled   : true
                                        }]
                            }]
                };
            },

            /**
             * Config for Weekly options
             * @private
             * @param {Object} config The configuration option of the form
             */
            configFormWeekly            : function(config) {
                return {
                    // hidden : true,
                    // columnWidth : '1',
                    id         : this.frequency_weekly_section_id,
                    layout     : 'form',
                    border     : false,
                    defaults   : {
                        anchor : Viz.Const.UI.Anchor,
                        style  : 'background-color:transparent;'
                    },
                    bodyStyle  : 'background-color:transparent;padding:10px;',
                    labelWidth : 100,
                    items      : [{
                                xtype      : 'compositefield',
                                fieldLabel : $lang('msg_calendarevent_recurseveryweek'),
                                hidden     : !config.showWeeklyRecurs,
                                items      : [{
                                            xtype      : 'viznumberfield',
                                            name       : 'frequency_weekly_interval',
                                            fieldLabel : $lang('msg_calendarevent_interval'),
                                            hideLabel  : true,
                                            width      : 40,
                                            value      : 1
                                        }, {
                                            xtype      : 'displayfield',
                                            fieldLabel : 'Last',
                                            value      : $lang('msg_calendarevent_weekson'),
                                            flex       : 1
                                        }]
                            }, {
                                // Use the default, automatic layout to distribute the controls evenly
                                // across a single row
                                xtype      : 'checkboxgroup',
                                id         : this.checkboxgroup_dayofweek_id,
                                name       : 'frequency_weekly_cbg',
                                fieldLabel : $lang('msg_calendarevent_weekday'),
                                hideLabel  : true,
                                columns    : 4,
                                defaults   : {
                                    name : 'frequency_weekly_weekdays'
                                },
                                items      : [{
                                            boxLabel   : Date.dayNames[1],
                                            id         : this.checkboxgroup_dayofweek_id + 'Monday',
                                            inputValue : 'Monday'
                                        }, {
                                            boxLabel   : Date.dayNames[2],
                                            id         : this.checkboxgroup_dayofweek_id + 'Tuesday',
                                            inputValue : 'Tuesday'
                                        }, {
                                            boxLabel   : Date.dayNames[3],
                                            id         : this.checkboxgroup_dayofweek_id + 'Wednesday',
                                            inputValue : 'Wednesday'
                                        }, {
                                            boxLabel   : Date.dayNames[4],
                                            id         : this.checkboxgroup_dayofweek_id + 'Thursday',
                                            inputValue : 'Thursday'
                                        }, {
                                            boxLabel   : Date.dayNames[5],
                                            id         : this.checkboxgroup_dayofweek_id + 'Friday',
                                            inputValue : 'Friday'
                                        }, {
                                            boxLabel   : Date.dayNames[6],
                                            id         : this.checkboxgroup_dayofweek_id + 'Saturday',
                                            inputValue : 'Saturday'
                                        }, {
                                            boxLabel   : Date.dayNames[0],
                                            id         : this.checkboxgroup_dayofweek_id + 'Sunday',
                                            inputValue : 'Sunday'
                                        }]
                            }]
                };
            },

            /**
             * Configuration for End recurrence
             * @param {Object} config The configuration option of the form
             * @return {Object}
             */
            configFormEnd               : function(config) {
                return {
                    // columnWidth : '1',
                    layout    : 'form',
                    border    : false,
                    bodyStyle : 'background-color:transparent;padding:4px;',
                    defaults  : {
                        style  : 'background-color:transparent;',
                        anchor : Viz.Const.UI.Anchor
                    },
                    items     : [{
                                xtype     : 'compositefield',
                                hideLabel : true,
                                width     : 300,
                                items     : [{
                                            fieldLabel     : '',
                                            name           : 'frequency_end',
                                            hideLabel      : true,
                                            height         : 30,
                                            inputValue     : 'none',
                                            labelSeparator : '',
                                            xtype          : 'radio',
                                            checked        : true,
                                            boxLabel       : $lang('msg_calendarevent_noenddate'),
                                            listeners      : {
                                                check : this.onCheckFrequencyEnd,
                                                scope : this
                                            }
                                        }]
                            }, {
                                xtype     : 'compositefield',
                                hidden    : !config.showNumberOccurrence,
                                hideLabel : true,
                                width     : 300,
                                items     : [{
                                            fieldLabel     : '',
                                            width          : 100,
                                            name           : 'frequency_end',
                                            hideLabel      : true,
                                            inputValue     : 'count',
                                            labelSeparator : '',
                                            xtype          : 'radio',
                                            checked        : false,
                                            boxLabel       : $lang('msg_calendarevent_endafter'),
                                            listeners      : {
                                                check : this.onCheckFrequencyEnd,
                                                scope : this
                                            }
                                        }, {
                                            xtype      : 'viznumberfield',
                                            width      : 40,
                                            disabled   : true,
                                            name       : 'Count',
                                            allowBlank : false
                                        }, {
                                            xtype : 'displayfield',
                                            value : $lang('msg_calendarevent_occurrences')
                                        }]
                            }, {
                                xtype     : 'compositefield',
                                hidden    : !config.showEndDate,
                                hideLabel : true,
                                width     : 400,
                                items     : [{
                                            fieldLabel     : '',
                                            width          : 100,
                                            name           : 'frequency_end',
                                            hideLabel      : true,
                                            inputValue     : 'until',
                                            labelSeparator : '',
                                            xtype          : 'radio',
                                            checked        : false,
                                            boxLabel       : $lang('msg_calendarevent_endby'),
                                            listeners      : {
                                                check : this.onCheckFrequencyEnd,
                                                scope : this
                                            }
                                        }, {
                                            xtype          : config.fullEndDate ? 'vizDateTime' : 'datefield',
                                            width          : config.fullEndDate ? 250 : 100,
                                            disabled       : true,
                                            hideTime       : false,
                                            id             : this.datestart_id + '_until_date',
                                            validator      : Ext.form.VTypes.daterangesimple,
                                            startDateField : this.datestart_id,
                                            hasSwitch      : false,
                                            format         : config.fullEndDate ? '' : Viz.getLocalizedDateFormat(),
                                            name           : 'Until'
                                        }]
                            }]
                };
            },

            /**
             * Handler for forcing enddate value to be one day ahead of startdate
             * @param {Ext.form.Field} field
             * @param {Date} value
             */
            onStartDateSelect           : function(field, value) {
                var fieldEndDate = this.getForm().findField(field.endDateField);
                fieldEndDate.setValue(value.add(Date.DAY, 1));
            },

            /**
             * Handler for check all day checkbox.
             * @private
             * @param {Object} comp
             * @param {Bool} value
             */
            onCheckAllDay               : function(comp, value) {
                var rowFieldNames = [[], ['startdate_time', 'enddate_date', 'enddate_time']];
                var rowFields = this.makeRowFields(rowFieldNames);

                if (value)
                    this.makeDisable(rowFields, 0);
                else
                    this.makeDisable(rowFields, 1);
            },

            /**
             * Handler for check hasrecurrencepattern checkbox.
             * @param {Object} comp
             * @param {Bool} value
             */
            onCheckHasRecurrencePattern : function(comp, value) {
                if (value)
                    Ext.getCmp(this._tabContainerId).unhideTabStripItem(1);
                else
                    Ext.getCmp(this._tabContainerId).hideTabStripItem(1);
            },

            /**
             * Handler for check one of frequency radio
             * @param {Object} comp
             * @param {Boolean} value
             */
            onCheckFrequency            : function(comp, value) {
                if (value) {
                    var card = Ext.getCmp(this.frequency_detail_id);
                    var frequency = "";
                    switch (comp.inputValue) {
                        case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Minutely :
                            frequency = "Minutely";
                            break;
                        case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Hourly :
                            frequency = "Hourly";
                            break;
                        case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Daily :
                            frequency = "Daily";
                            break;
                        case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Weekly :
                            frequency = "Weekly";
                            break;
                        case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Monthly :
                            frequency = "Monthly";
                            break;
                        case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Yearly :
                            frequency = "Yearly";
                            break;
                    }
                    card.layout.setActiveItem(this['frequency_' + frequency.toLowerCase() + '_section_id']);
                    return;
                }
            },

            /**
             * Handler for check one of monthly choice radio
             * @param {Object} comp
             * @param {Boolean} value
             */
            onCheckMonthlyChoice        : function(comp, value) {
                if (!value)
                    return;
                var rowFieldNames = [['frequency_monthly_day', 'frequency_monthly_bymonthday_interval'], ['frequency_monthly_occurrence', 'frequency_monthly_weekday', 'frequency_monthly_bydayspecifier_interval']];
                var rowFields = this.makeRowFields(rowFieldNames);

                switch (comp.inputValue) {
                    case 'bymonthday' :
                        this.makeDisable(rowFields, 0);
                        break;
                    case 'bydayspecifier' :
                        this.makeDisable(rowFields, 1);
                        break;
                }
            },

            /**
             * Handler for check one of yearly choice radio
             * @param {Object} comp
             * @param {Boolean} value
             */
            onCheckYearlyChoice         : function(comp, value) {
                if (!value)
                    return;
                var rowFieldNames = [['frequency_yearly_day', 'frequency_yearly_bymonthday_month'], ['frequency_yearly_occurrence', 'frequency_yearly_weekday', 'frequency_yearly_bydayspecifier_month']];
                var rowFields = this.makeRowFields(rowFieldNames);

                switch (comp.inputValue) {
                    case 'bymonthday' :
                        this.makeDisable(rowFields, 0);
                        break;
                    case 'bydayspecifier' :
                        this.makeDisable(rowFields, 1);
                        break;
                }
            },

            /**
             * Handler for check one of frequency end choice radio
             * @param {Object} comp
             * @param {Boolean} value
             */
            onCheckFrequencyEnd         : function(comp, value) {
                if (!value)
                    return;
                var rowFieldNames = [[], ['Count'], ['Until']];
                var rowFields = this.makeRowFields(rowFieldNames);

                switch (comp.inputValue) {
                    case 'none' :
                        this.makeDisable(rowFields, 0);
                        break;
                    case 'count' :
                        this.makeDisable(rowFields, 1);
                        break;
                    case 'until' :
                        this.makeDisable(rowFields, 2);

                        break;
                }
            },
            /**
             * Handler for reinjecting recurrence pattern into the fields of the form.
             * @param {Viz.form.FormPanel} form
             * @param {Object} values
             */
            onAfterLoadValues           : function(form, values) {
                // Ajusting local dates
                if (!Ext.isEmpty(values.StartDate)) {
                    this.getForm().findField("StartDate").setValue(Viz.date.convertToLocalDate(values.StartDate, values.TimeZoneOffset[0]));
                    this.getForm().findField("EndDate").setValue(Viz.date.convertToLocalDate(values.EndDate, values.TimeZoneOffset[1]));
                }
                var rp = values.RecurrencePattern;
                var rp_flat = {
                    HasRecurrencePattern : rp != null
                    // || this.Category == 'Schedule' // || this.Category == 'ChartScheduler'
                };
                if (Ext.isObject(values)) {
                    var chkAllDay = Ext.getCmp(this.getForm().findField("StartDate").allday_id);
                    chkAllDay.setValue(false);
                    chkAllDay.setValue(values['IsAllDay']);
                }

                if (rp != null) {
                    Ext.apply(rp_flat, {
                                Frequency : rp.Frequency
                            });
                    try {
                        switch (rp.Frequency) {
                            case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Yearly :
                                rp_flat.frequency_yearly_interval = rp.Interval;
                                rp_flat.frequency_yearly_choice = 'bymonthday';
                                if (this.HasValue(rp.ByDay)) {
                                    rp_flat.frequency_yearly_choice = 'bydayspecifier';
                                    rp_flat.frequency_yearly_occurrence = rp.BySetPosition[0];
                                    rp_flat.frequency_yearly_weekday = rp.ByDay[0].DayOfWeek;
                                    rp_flat.frequency_yearly_bydayspecifier_month = rp.ByMonth[0];
                                }
                                else if (this.HasValue(rp.ByMonthDay)) {
                                    rp_flat.frequency_yearly_choice = 'bymonthday';
                                    rp_flat.frequency_yearly_bymonthday_month = rp.ByMonth[0];
                                    rp_flat.frequency_yearly_day = rp.ByMonthDay[0];
                                }
                                else {
                                    rp_flat.frequency_yearly_day = values.StartDate.getDate();
                                    rp_flat.frequency_yearly_bymonthday_month = values.StartDate.getMonth();
                                    rp_flat.frequency_yearly_bydayspecifier_month = values.StartDate.getMonth();
                                }
                                break;

                            case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Monthly :
                                if (this.HasValue(rp.ByDay)) {
                                    rp_flat.frequency_monthly_choice = 'bydayspecifier';
                                    rp_flat.frequency_monthly_occurrence = rp.BySetPosition[0];
                                    rp_flat.frequency_monthly_weekday = rp.ByDay[0].DayOfWeek;
                                    if (!this.showExplicitMonths) {
                                        rp_flat.frequency_monthly_bydayspecifier_interval = rp.Interval;
                                    }
                                }
                                else if (this.HasValue(rp.ByMonthDay)) {
                                    rp_flat.frequency_monthly_choice = 'bymonthday';
                                    rp_flat.frequency_monthly_day = rp.ByMonthDay[0];
                                    if (!this.showExplicitMonths) {
                                        rp_flat.frequency_monthly_bymonthday_interval = rp.Interval;
                                    }
                                }
                                Ext.each(rp.ByMonth, function(value) {
                                            rp_flat[this.checkboxgroup_month_id + Date.monthNames[value - 1]] = true;
                                        }, this);
                                break;

                            case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Weekly :
                                if (this.showWeeklyRecurs) {
                                    rp_flat.frequency_weekly_interval = rp.Interval;
                                }
                                Ext.each(rp.ByDay, function(value) {
                                            rp_flat[this.checkboxgroup_dayofweek_id + this.convertValueToDay(value.DayOfWeek)] = true;
                                        }, this);
                                break;

                            case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Daily :
                                rp_flat.frequency_daily_interval = rp.Interval;
                                break;

                            case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Hourly :
                                rp_flat.frequency_hourly_interval = rp.Interval;
                                break;
                            case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Minutely :
                                rp_flat.frequency_minutely_interval = rp.Interval;
                                break;

                        }
                    }
                    catch (ex) {
                        console.log('Error');
                    }
                    rp_flat.frequency_end = 'none';
                    if (rp.Count > 0) {
                        rp_flat.Count = rp.Count;
                        rp_flat.frequency_end = 'count';
                    }
                    if (!Ext.isEmpty(rp.Until)) {
                        rp.Until = Viz.date.convertToLocalDate(rp.Until, values.TimeZoneOffset[2])
                        rp_flat.Until = rp.Until;
                        rp_flat.frequency_end = 'until';
                    }
                }
                this.getForm().setValues(rp_flat);
            },
            /**
             * Handler for save success. This is a virtual function, and each sub class could provide implementation.
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess               : function(form, action) {
                var retVal = Viz.form.CalendarDefinition.superclass.onSaveSuccess.apply(this, arguments);
                if (retVal) {
                    var startDate = this.entity.StartDate;
                    this.entity.StartDate = null;
                    this.onAfterLoadValues(form, this.entity);
                    this.entity.StartDate = startDate;
                }
            },
            /**
             * Implementation for the virtual function in order to correct the issue with getFieldValues and composite fields.
             * @param {Viz.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                : function(form, item, extraParams) {
                var isValid = true;

                Viz.clearObject(item, ['Key', 'KeySchedule', 'StartDate', 'EndDate', 'Until', 'IsAllDay', 'TimeZoneId', 'TimeZoneOffset']);
                form.getForm().clearInvalid();
                Ext.applyIf(item, form.getForm().getValues());
                item.TimeZoneOffset = [];
                item.TimeZoneOffset[0] = item.StartDate.getWCFGMTOffset();
                item.TimeZoneOffset[1] = item.EndDate.getWCFGMTOffset();
                // apply the category because it is not exposed through a form field.
                Ext.applyIf(item, {
                            Category : this.Category
                        });

                item.Factor = this.getForm().findField('Factor').getValue();

                if (item.Factor === '')
                    item.Factor = null;
                var IsAllDay = item.IsAllDay || false;
                var IsAllDayField = this.getForm().findField('IsAllDayUI');

                var IsAllDayUI = false;
                if (IsAllDayField)
                    IsAllDayUI = IsAllDayField.getValue();
                if (IsAllDayUI) {
                    item.IsAllDay = true;
                }
                else {
                    if (form.getForm().findField("StartDate").isDirty() || form.getForm().findField("EndDate").isDirty())
                        item.IsAllDay = false;
                    else
                        item.IsAllDay = IsAllDay || IsAllDayUI;
                }

                if (item.HasRecurrencePattern) {
                    var rp = {
                        Frequency : parseInt(item.Frequency),
                        Count     : item.Count

                    };

                    switch (item.frequency_end) {
                        case 'none' :
                            break;
                        case 'count' :
                            isValid = this.MarkInvalidEmptyField(item, "Count") && isValid;
                            break;
                        case 'until' :
                            isValid = this.MarkInvalidEmptyField(item, "Until") && isValid;
                            rp.Until = form.getForm().findField("Until").getValue();
                            item.TimeZoneOffset[2] = rp.Until.getWCFGMTOffset();
                            break;
                    }

                    switch (rp.Frequency) {

                        case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Minutely :
                            rp.Interval = item.frequency_minutely_interval;
                            isValid = this.MarkInvalidEmptyField(item, "frequency_minutely_interval") && isValid;
                            break;
                        case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Hourly :
                            rp.Interval = item.frequency_hourly_interval;
                            isValid = this.MarkInvalidEmptyField(item, "frequency_hourly_interval") && isValid;
                            break;

                        case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Daily :
                            rp.Interval = item.frequency_daily_interval;
                            isValid = this.MarkInvalidEmptyField(item, "frequency_daily_interval") && isValid;
                            break;

                        case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Weekly :
                            if (this.showWeeklyRecurs) {
                                rp.Interval = item.frequency_weekly_interval;
                                isValid = this.MarkInvalidEmptyField(item, "frequency_weekly_interval") && isValid;
                            }
                            isValid = this.MarkInvalidEmptyField(item.frequency_weekly_weekdays, this.checkboxgroup_dayofweek_id) && isValid;
                            if (!Ext.isEmpty(item.frequency_weekly_weekdays))
                                item.frequency_weekly_weekdays = [].concat(item.frequency_weekly_weekdays);
                            rp.ByDay = [];
                            Ext.each(item.frequency_weekly_weekdays, function(value) {
                                        rp.ByDay.push({
                                                    DayOfWeek : System.DayOfWeek[value],
                                                    Offset    : -2147483648
                                                });
                                    });

                            break;

                        case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Monthly :

                            if (this.showExplicitMonths) {
                                isValid = this.MarkInvalidEmptyField(item.frequency_monthly_months, this.checkboxgroup_month_id) && isValid;
                                if (!Ext.isEmpty(item.frequency_monthly_months))
                                    item.frequency_monthly_months = [].concat(item.frequency_monthly_months);
                                rp.ByMonth = item.frequency_monthly_months;
                            }

                            switch (item.frequency_monthly_choice) {
                                case 'bymonthday' :
                                    isValid = this.MarkInvalidEmptyField(item, "frequency_monthly_day") && isValid;
                                    if (!this.showExplicitMonths) {
                                        isValid = this.MarkInvalidEmptyField(item, "frequency_monthly_bymonthday_interval") && isValid;
                                        rp.Interval = item.frequency_monthly_bymonthday_interval;
                                    }
                                    rp.ByMonthDay = [item.frequency_monthly_day];
                                    break;
                                case 'bydayspecifier' :
                                    isValid = this.MarkInvalidEmptyField(item, "frequency_monthly_occurrence") && isValid;
                                    isValid = this.MarkInvalidEmptyField(item, "frequency_monthly_weekday") && isValid;
                                    if (!this.showExplicitMonths) {
                                        isValid = this.MarkInvalidEmptyField(item, "frequency_monthly_bydayspecifier_interval") && isValid;
                                        rp.Interval = item.frequency_monthly_bydayspecifier_interval;
                                    }
                                    rp.ByDay = [{
                                                DayOfWeek : item.frequency_monthly_weekday,
                                                Offset    : -2147483648
                                            }];
                                    rp.BySetPosition = [item.frequency_monthly_occurrence];
                                    break;
                            }
                            break;

                        case Vizelia.FOL.BusinessEntities.CalendarFrequencyType.Yearly :
                            rp.Interval = item.frequency_yearly_interval;
                            isValid = this.MarkInvalidEmptyField(item, "frequency_yearly_interval") && isValid;

                            switch (item.frequency_yearly_choice) {
                                case 'bymonthday' :
                                    isValid = this.MarkInvalidEmptyField(item, "frequency_yearly_day") && isValid;
                                    isValid = this.MarkInvalidEmptyField(item, "frequency_yearly_bymonthday_month") && isValid;

                                    rp.ByMonthDay = [item.frequency_yearly_day];
                                    rp.ByMonth = [item.frequency_yearly_bymonthday_month];
                                    break;

                                case 'bydayspecifier' :
                                    isValid = this.MarkInvalidEmptyField(item, "frequency_yearly_occurrence") && isValid;
                                    isValid = this.MarkInvalidEmptyField(item, "frequency_yearly_weekday") && isValid;
                                    isValid = this.MarkInvalidEmptyField(item, "frequency_yearly_bydayspecifier_month") && isValid;
                                    rp.ByMonth = [item.frequency_yearly_bydayspecifier_month];
                                    rp.ByDay = [{
                                                DayOfWeek : item.frequency_yearly_weekday,
                                                Offset    : -2147483648
                                            }];
                                    rp.BySetPosition = [item.frequency_yearly_occurrence];
                                    break;
                            }
                            break;
                    }
                    item.RecurrencePattern = rp;
                }

                return isValid;
            },

            /**
             * Check if the value of the field is empty and mark it as invalid.
             * @param {Object} value The value either an object or a plain value
             * @param {String} field The field name or id
             * @param {String} error The error message
             * @return {Boolean}
             */
            MarkInvalidEmptyField       : function(value, field, error) {
                error = error || Ext.form.Field.prototype.invalidText;
                var test_value = Ext.isObject(value) ? value[field] : value;
                if (Ext.isEmpty(test_value)) {
                    this.getForm().findField(field).markInvalid(error);
                    return false;
                }
                return true;
            },

            /**
             * Checks if an array value exists (This is the way parameters are stored in RecurrencePattern)
             * @param {Array} o
             * @return {Boolean}
             */
            HasValue                    : function(o) {
                if (o == null)
                    return false;
                if (Ext.isArray(o) && o.length > 0)
                    return true;
                return false;
            },

            /**
             * @private Find the day name from an integer (based 0)
             * @param {Integer} value
             * @return {String}
             */
            convertValueToDay           : function(value) {
                for (p in System.DayOfWeek) {
                    if (System.DayOfWeek[p] == value)
                        return p;
                }
            }
        });

Ext.reg('vizFormCalendarDefinition', Viz.form.CalendarDefinition);

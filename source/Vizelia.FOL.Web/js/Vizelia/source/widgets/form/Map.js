﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Map
 * @extends Ext.FormPanel.
 */
Viz.form.Map = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor               : function(config) {
                this._psetId = Ext.id();
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.Map_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.Map_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.Map_FormUpdate,
                    items                : this.configFormGeneral(config),
                    labelWidth           : 210
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.Map.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral         : function(config) {
                return [{
                            name   : 'KeyMap',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_localid'),
                            allowBlank : false,
                            name       : 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_title'),
                            name       : 'Title',
                            allowBlank : false
                        }, {
                            fieldLabel   : $lang('msg_map_maptype'),
                            name         : 'MapType',
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.MapType,Vizelia.FOL.Common',
                            flex         : 1,
                            value        : 1,
                            allowBlank   : false
                        }, {
                            fieldLabel           : $lang('msg_pset'),
                            name                 : 'Pset',
                            xtype                : 'viztreecomboPset',
                            id                   : this._psetId,
                            originalDisplayValue : 'PropertySingleValueLongPath',
                            validator            : Ext.form.VTypes.mapPsetValidator,
                            allowBlank           : true,
                            listeners            : {
                                select : this.updatePropertySingleValue,
                                clear  : this.clearPropertySingleValue,
                                scope  : this
                            }
                        }, {
                            fieldLabel           : $lang('msg_alarm'),
                            name                 : 'KeyClassificationItemAlarmDefinition',
                            xtype                : 'viztreecomboClassificationItemAlarmDefinition',
                            originalDisplayValue : 'ClassificationItemAlarmDefinitionLongPath'
                        }, {
                            fieldLabel : $lang('msg_map_showdashboard'),
                            name       : 'ShowDashboard',
                            xtype      : 'vizcheckbox'
                        }, {
                            fieldLabel : $lang('msg_map_usespatialicons'),
                            name       : 'UseSpatialIcons',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }, {
                            fieldLabel : $lang('msg_chart_favorite'),
                            name       : 'IsFavorite',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel : $lang('msg_portalwindow_openonstartup'),
                            name       : 'OpenOnStartup',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }, {
                            fieldLabel             : $lang('msg_location_path'),
                            xtype                  : 'viztreecomboSpatial',
                            validator              : Ext.form.VTypes.locationValidator,
                            name                   : 'KeyLocation',
                            ExcludeMetersAndAlarms : true,
                            originalDisplayValue   : 'LocationLongPath',
                            allowBlank             : true
                        }];

            },

            onAfterLoadValues         : function(form, values) {
                Ext.getCmp(this._psetId).setValue(values.PropertySingleValueLongPath);
            },
            /**
             * Handler for updating PropertySingleValueLongPath when a the KeyPropertySingleValue is changed.
             */
            updatePropertySingleValue : function(combo, node) {
                if (node && node.attributes && node.attributes.entity && node.attributes.entity.IfcType == "PsetAttributeDefinition") {
                    this.entity.PsetName = node.parentNode.attributes.entity.PsetName;
                    this.entity.AttributeName = node.attributes.entity.AttributeName;
                    this.entity.PropertySingleValueLongPath = node.getPath('text');
                    // this.entity.PropertySingleValueLongPath = this.entity.PsetName + ' / ' + this.entity.AttributeName;
                    this.isDirty = true;
                }
            },

            clearPropertySingleValue  : function(combo) {
                combo.clearInvalid();

                this.entity.PsetName = null;
                this.entity.AttributeName = null;
                this.entity.PropertySingleValueLongPath = null;
                this.isDirty = true;
            }
        });

Ext.reg('vizFormMap', Viz.form.Map);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ClassificationDefinition
 * @extends Ext.FormPanel.
 */
Viz.form.ClassificationDefinition = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
    constructor: function (config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.CoreWCF.ClassificationDefinition_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.ClassificationDefinition_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.ClassificationDefinition_FormUpdate,
                    labelWidth           : 250,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.ClassificationDefinition.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            fieldLabel : $lang('msg_classificationdefinition_category'),
                            name       : 'Category',
                            readOnly   : config.mode == "update" ? true : false,
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_classificationdefinition_localid'),
                            name       : 'LocalId',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_classificationdefinition_iconcls'),
                            name       : 'IconCls'
                        }, {
                            fieldLabel : $lang('msg_classificationdefinition_iconrootonly'),
                            name       : 'IconRootOnly',
                            xtype      : 'vizcheckbox'
                        }];
            }
        });

Ext.reg('vizFormClassificationDefinition', Viz.form.ClassificationDefinition);
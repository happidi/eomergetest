﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ChartHistoricalAnalysis
 * @extends Ext.FormPanel.
 */
Viz.form.ChartHistoricalAnalysis = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Chart} entity The chart entity the axis belong to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.ChartHistoricalAnalysis_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.ChartHistoricalAnalysis_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.ChartHistoricalAnalysis_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.ChartHistoricalAnalysis.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyChartHistoricalAnalysis',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_id'),
                            name       : 'LocalId',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_charthistoricalanalysis_enabled'),
                            name       : 'Enabled',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel : $lang('msg_charthistoricalanalysis_offset'),
                            name       : 'Frequency',
                            xtype      : 'vizSpinnerField',
                            minValue   : 1,
                            allowBlank : false,
                            width      : 60,
                            value      : 1
                        }, {
                            fieldLabel     : $lang('msg_charthistoricalanalysis_interval'),
                            name           : 'Interval',
                            xtype          : 'vizComboEnum',
                            enumTypeName   : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common',
                            sortByValues   : true,
                            value          : "4",
                            flex           : 1,
                            valuesToRemove : ['None', 'Global']
                        }, {
                            fieldLabel : $lang('msg_charthistoricalanalysis_extrachartarea'),
                            name       : 'ExtraChartArea',
                            xtype      : 'vizcheckbox'
                        }, {
                            fieldLabel   : $lang('msg_charthistoricalanalysis_dataserietype'),
                            name         : 'DataSerieType',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.DataSerieType,Vizelia.FOL.Common',
                            value        : Vizelia.FOL.BusinessEntities.DataSerieType.None
                        }, {
                            fieldLabel : $lang('msg_charthistoricalanalysis_includeinmaintimeline'),
                            name       : 'IncludeInMainTimeline',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }, {
                            fieldLabel : $lang('msg_charthistoricalanalysis_xaxisvisible'),
                            name       : 'XAxisVisible',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel : $lang('msg_charthistoricalanalysis_setxaxismin'),
                            name       : 'SetXAxisMin',
                            xtype      : 'vizcheckbox',
                            checked    : true,
                            hidden     : true
                        }, {
                            fieldLabel : $lang('msg_charthistoricalanalysis_setxaxismax'),
                            name       : 'SetXAxisMax',
                            xtype      : 'vizcheckbox',
                            checked    : true,
                            hidden     : true
                        }];
            }
        });

Ext.reg('vizFormChartHistoricalAnalysis', Viz.form.ChartHistoricalAnalysis);
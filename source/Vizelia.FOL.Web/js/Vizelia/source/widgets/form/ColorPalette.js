﻿Ext.namespace('Viz');

/**
 * @class Viz.ColorPalette
 * @extends Ext.ColorPalette Color Palette form element that can be populated with ColorR, ColorG and ColorB values
 */
Viz.ColorPalette = Ext.extend(Ext.ColorPalette, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                // TODO construction

                Viz.ColorPalette.superclass.constructor.call(this, config);
                if (Ext.isArray(this.value))
                    this.value = new Viz.RGBColor(String.format('rgb({0},{1},{2})', this.value[0], this.value[1], this.value[2])).toHex();
            },

            /**
             * Selects the specified color in the palette (fires the {@link #select} event)
             * @param {String} color A valid 6-digit color hex code (# will be stripped if included)
             */
            select      : function(color) {
                color = color.replace('#', '');
                if (color != this.value || this.allowReselect) {
                    var el = this.el;
                    if (this.value) {
                        el.child('a.color-' + this.value).removeClass('x-color-palette-sel');
                    }
                    el.child('a.color-' + color).addClass('x-color-palette-sel');
                    this.value = color;
                    this.fireEvent('select', this, color);
                }
            }

        });

Ext.reg('vizColorPalette', Viz.ColorPalette);

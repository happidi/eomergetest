﻿Ext.namespace('Viz.form');

/**
 * @class Viz.form.BuildingStorey
 * @extends Ext.FormPanel.
 */
Viz.form.BuildingStorey = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                this._tabContainerId = Ext.id();
                var defaultConfig = {
                    mode_load            : 'remote',
                    psetConfig           : {
                        IfcType                    : 'IfcBuildingStorey',
                        entityKey                  : 'KeyBuildingStorey',
                        filterByClassificationItem : false
                    },
                    serviceHandler       : Viz.Services.CoreWCF.BuildingStorey_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.BuildingStorey_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.BuildingStorey_FormUpdate,
                    hasValidationToolbar : true,
                    items                : this.configFormGeneral(config)

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.BuildingStorey.superclass.constructor.call(this, config);

            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral : function(config) {
                return [{
                    name       : 'LocalId',
                    fieldLabel : $lang('msg_localid'),
                    allowBlank : false
                        // ,
                        // readOnly : config.mode == "update" ? true : false
                    }, {
                    fieldLabel : $lang('msg_name'),
                    allowBlank : false,
                    name       : 'Name'
                }, {
                    fieldLabel : $lang('msg_elevation'),
                    name       : 'Elevation',
                    xtype      : 'viznumberfield'
                }, {
                    xtype      : 'textarea',
                    anchor     : Viz.Const.UI.Anchor + ' -90',
                    fieldLabel : $lang('msg_description'),
                    name       : 'Description'
                }]
            }
        });

Ext.reg('vizFormBuildingStorey', Viz.form.BuildingStorey);

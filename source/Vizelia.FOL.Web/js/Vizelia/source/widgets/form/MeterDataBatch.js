﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.MeterDataBatch
 * @extends Ext.FormPanel.
 */
Viz.form.MeterDataBatch = Ext.extend(Ext.Panel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                           : function(config) {
                this._fieldAcquisitionDateTime = Ext.id();
                this._fieldAcquisitionHour = Ext.id();
                this._fieldAcquisitionMinute = Ext.id();
                this._fieldAcquisitionDay = Ext.id();
                this._fieldAcquisitionMonthYear = Ext.id();
                this.currentDisplayedDateTimeFieldId = this._fieldAcquisitionDateTime;

                this._fieldLocation = Ext.id();
                this._fieldClassification = Ext.id();
                this._fieldsetMeterData = Ext.id();
                this._fieldsetMessage = Ext.id();
                this._panelMessage = Ext.id();

                this._formId = Ext.id();
                this.currentSelectedDate = null;
                config = config || {};
                var defaultConfig = {
                    layout    : 'fit',
                    items     : this.configFormGeneral(config),
                    bodyStyle : 'padding: 5px;background-color: transparent;',
                    buttons   : [{
                                id      : this._buttonSaveId,
                                text    : $lang('msg_save'),
                                iconCls : 'viz-icon-small-save',
                                handler : this.saveItem,
                                scope   : this
                            }]
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.MeterDataBatch.superclass.constructor.call(this, config);

                this.meterDataCollection = new Ext.util.MixedCollection();
            },
            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                     : function(config) {
                return {
                    xtype       : 'form',
                    layout      : 'form',
                    id          : this._formId,
                    border      : false,
                    bodyStyle   : 'padding: 5px;background-color: transparent;',
                    defaultType : 'textfield',
                    labelWidth  : 300,
                    autoScroll  : true,
                    defaults    : {
                        msgTarget   : 'side',
                        preventMark : false
                        // anchor : '-30'
                    },
                    items       : [{
                                xtype : 'fieldset',
                                width : 1200,
                                title : $lang('msg_meterdatabatch_filterdata'),
                                items : [{
                                            xtype         : 'vizcompositefield',
                                            fieldLabel    : $lang('msg_location_path'),
                                            combineErrors : false,
                                            items         : [{
                                                        id                     : this._fieldLocation,
                                                        fieldLabel             : $lang('msg_location_path'),
                                                        xtype                  : 'viztreecomboSpatial',
                                                        validator              : Ext.form.VTypes.locationValidator,
                                                        name                   : 'KeyLocation',
                                                        flex                   : 1,
                                                        margins                : '0 20 0 0',
                                                        originalDisplayValue   : 'LocationLongPath',
                                                        allowBlank             : false,
                                                        stateId                : 'viztreecomboSpatial_MeterData',
                                                        stateful               : true,
                                                        ExcludeMetersAndAlarms : true,
                                                        listeners              : {
                                                            changevalue : function() {
                                                                this.updateTreeClassificationSpatialFilter();
                                                            },
                                                            scope       : this
                                                        }
                                                    }, {
                                                        xtype : 'displayfield',
                                                        cls   : 'viz-form-display-field-inside-composite',
                                                        value : $lang('msg_meter_classification')
                                                    }, {
                                                        id                   : this._fieldClassification,
                                                        fieldLabel           : $lang('msg_meter_classification'),
                                                        xtype                : 'viztreecomboClassificationItemMeter',
                                                        name                 : 'KeyClassificationItem',
                                                        allowBlank           : false,
                                                        rootVisible          : true,
                                                        flex                 : 1,
                                                        margins              : '0 20 0 0',
                                                        originalDisplayValue : 'ClassificationItemLongPath',
                                                        stateId              : 'viztreecomboClassificationItemMeter_MeterData',
                                                        stateful             : true
                                                    }, {
                                                        xtype   : 'button',
                                                        text    : $lang('msg_search'),
                                                        iconCls : 'viz-icon-small-search',
                                                        style   : 'padding-left:15px;',
                                                        scope   : this,
                                                        handler : function() {
                                                            var locationField = Ext.getCmp(this._fieldLocation);
                                                            var classificationField = Ext.getCmp(this._fieldClassification);

                                                            if (!classificationField.value) {
                                                                classificationField.setValue(classificationField.root.id);
                                                                classificationField.clearInvalid();
                                                            }

                                                            var locationValid = locationField.validate();

                                                            if (locationValid) {
                                                                this.buildBatchForm();
                                                            }
                                                        }
                                                    }]
                                        }]
                            }, {
                                xtype : 'fieldset',
                                title : $lang('msg_meterdatabatch_batchdate'),
                                width : 1200,
                                items : [{
                                            fieldLabel   : $lang('msg_meter_manual_input_time_interval'),
                                            name         : 'ManualInputTimeInterval',
                                            xtype        : 'vizComboEnum',
                                            enumTypeName : 'Vizelia.FOL.BusinessEntities.ManualInputTimeInterval,Vizelia.FOL.Common',
                                            sortByValues : true,
                                            displayIcon  : true,
                                            value        : Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Seconds,
                                            listeners    : {
                                                select : {
                                                    fn    : this.onSelectManualInput,
                                                    scope : this
                                                }
                                            }
                                        }, {
                                            id             : this._fieldAcquisitionDateTime,
                                            fieldLabel     : $lang('msg_meterdata_acquisitiondatetime'),
                                            name           : 'AcquisitionDateTime',
                                            hideTime       : false,
                                            xtype          : 'vizDateTime',
                                            hidden         : false,
                                            allowBlank     : true,
                                            hasSwitch      : false,
                                            displaySeconds : true,
                                            timeConfig     : {
                                                width : 80
                                            },
                                            listeners      : {
                                                scope      : this,
                                                selectdate : function(comp, datefield, value) {
                                                    this.currentSelectedDate = value;
                                                },
                                                selecttime : function(comp, timefield, record, index, value) {
                                                    this.currentSelectedDate = value;
                                                }
                                            }
                                        }, {
                                            name           : 'AcquisitionHour',
                                            id             : this._fieldAcquisitionHour,
                                            fieldLabel     : $lang('msg_meterdata_acquisitiondatetime'),
                                            xtype          : 'vizDateTime',
                                            hidden         : true,
                                            hideTime       : false,
                                            displaySeconds : false,
                                            allowBlank     : true,
                                            hasSwitch      : false,
                                            timeConfig     : {
                                                width     : 80,
                                                increment : 60,
                                                minValue  : '00:00',
                                                maxValue  : '23:00'

                                            },
                                            listeners      : {
                                                scope      : this,
                                                selectdate : function(comp, datefield, value) {
                                                    this.currentSelectedDate = value;
                                                },
                                                selecttime : function(comp, timefield, record, index, value) {
                                                    this.currentSelectedDate = value;
                                                }
                                            }
                                        }, {
                                            name           : 'AcquisitionMinute',
                                            id             : this._fieldAcquisitionMinute,
                                            fieldLabel     : $lang('msg_meterdata_acquisitiondatetime'),
                                            xtype          : 'vizDateTime',
                                            hidden         : true,
                                            hideTime       : false,
                                            displaySeconds : false,
                                            allowBlank     : true,
                                            hasSwitch      : false,
                                            timeConfig     : {
                                                width     : 80,
                                                increment : 1,
                                                minValue  : '00:00',
                                                maxValue  : '00:59'
                                            },
                                            listeners      : {
                                                scope      : this,
                                                selectdate : function(comp, datefield, value) {
                                                    this.currentSelectedDate = value;
                                                },
                                                selecttime : function(comp, timefield, record, index, value) {
                                                    this.currentSelectedDate = value;
                                                }
                                            }
                                        }, {
                                            name           : 'AcquisitionDay',
                                            id             : this._fieldAcquisitionDay,
                                            fieldLabel     : $lang('msg_meterdata_acquisitiondatetime'),
                                            xtype          : 'vizDateTime',
                                            hidden         : true,
                                            hideTime       : true,
                                            displaySeconds : false,
                                            allowBlank     : true,
                                            hasSwitch      : false,
                                            listeners      : {
                                                scope      : this,
                                                selectdate : function(comp, datefield, value) {
                                                    this.currentSelectedDate = value;
                                                }
                                            }
                                        }, {
                                            xtype       : 'vizMonthYear',
                                            id          : this._fieldAcquisitionMonthYear,
                                            fieldLabel  : $lang('msg_meterdata_acquisitiondatetime'),
                                            hidden      : true,
                                            monthConfig : {
                                                id    : this._fieldAcquisitionMonth,
                                                width : 100
                                                // value : 1 + new Date().getMonth()
                                            },
                                            yearConfig  : {
                                                id    : this._fieldAcquisitionYear,
                                                width : 100
                                                // value : new Date().getFullYear()
                                            },
                                            listeners   : {
                                                scope       : this,
                                                selectmonth : function(comp, record, index, date) {
                                                    this.currentSelectedDate = date;
                                                },
                                                selectyear  : function(comp, newValue, oldValue, date) {
                                                    this.currentSelectedDate = date;
                                                }
                                            }
                                        }]
                            },

                            {
                                xtype    : 'fieldset',
                                id       : this._fieldsetMeterData,
                                title    : $lang('msg_meterdata'),
                                width    : 1200,
                                defaults : {
                                    msgTarget : 'side',
                                    anchor    : Viz.Const.UI.Anchor
                                }
                            }, {
                                xtype    : 'fieldset',
                                id       : this._fieldsetMessage,
                                width    : 1200,
                                hidden   : true,
                                defaults : {
                                    msgTarget : 'side',
                                    anchor    : Viz.Const.UI.Anchor
                                },
                                items    : [{
                                            xtype  : "panel",
                                            id     : this._panelMessage,
                                            border : false,
                                            width  : '100%'
                                        }]
                            }]
                };
            },

            /**
             * Update the TreeClassfication Combo based on the Location selected.
             */
            updateTreeClassificationSpatialFilter : function() {
                var fiedLocation = Ext.getCmp(this._fieldLocation);
                var treeLocation = fiedLocation.getTree();
                var node = treeLocation.getNodeById(fiedLocation.getValue());
                if (Ext.isDefined(node)) {
                    var comboMeterClassification = Ext.getCmp(this._fieldClassification);
                    var treeClassification = comboMeterClassification.getTree();
                    treeClassification.serviceParams.filterSpatial = {
                        create : [Viz.util.Location.getLocationEntityFromSpatialNode(node)]
                    };
                    comboMeterClassification.clearValue();
                    treeClassification.getLoader().load(treeClassification.getRootNode());
                    // root need to be set as loaded to prevent the expand() from loading it againg.
                    treeClassification.root.loaded = true;
                    // we expand the root because of a bug causing the root to be selected if being expanded in UI.
                    treeClassification.root.expand();
                }
            },

            /**
             * Build the form based on the Location selected and Classification selected.
             */
            buildBatchForm                        : function() {
                var keylocation = Ext.getCmp(this._fieldLocation).getValue();
                var keyclassificationitem = Ext.getCmp(this._fieldClassification).getValue();

                if (Ext.isDefined(keylocation) && Ext.isDefined(keyclassificationitem)) {
                    this.el.mask();
                    Viz.Services.EnergyWCF.Meter_GetStoreByLocationAndClassification({
                                KeyLocation           : keylocation,
                                KeyClassificationItem : keyclassificationitem,

                                paging                : {
                                    start : 0,
                                    limit : 200,
                                    sort  : 'LocationLongPath,Name',
                                    dir   : "ASC"
                                },
                                success               : function(meterStore) {
                                    var message = null;
                                    if (meterStore.records.length < meterStore.recordCount) {
                                        message = String.format($lang('msg_meterdatabatch_limitexceeded'), meterStore.records.length, meterStore.recordCount);
                                    }
                                    this.buildMeterDataForm(meterStore, message);
                                },
                                scope                 : this
                            });
                }
            },
            /**
             * Build one row
             * @param {} meterStore
             */
            buildMeterDataForm                    : function(meterStore, message) {
                var fieldset = Ext.getCmp(this._fieldsetMeterData);
                var form = Ext.getCmp(this._formId).getForm();
                // var width = Math.min(500, Ext.getCmp(this._formId).getWidth() - 700);

                fieldset.removeAll(true);
                form.cleanDestroyed();
                this.meterDataCollection = new Ext.util.MixedCollection();
                var field = {
                    xtype      : 'vizcompositefield',
                    fieldLabel : $lang('msg_meters'),
                    // labelStyle : 'width:' + width + 'px',
                    items      : [{
                                xtype : 'displayfield',
                                cls   : 'viz-form-display-field-inside-composite',
                                width : 150,
                                value : $lang('msg_meterdata_lastacquisitiondate')
                            }, {
                                xtype : 'displayfield',
                                cls   : 'viz-form-display-field-inside-composite',
                                width : 120,
                                value : $lang('msg_meterdata_lastvalue')
                            }, {
                                xtype : 'displayfield',
                                cls   : 'viz-form-display-field-inside-composite',
                                width : 120,
                                value : $lang('msg_meterdata_inputnewvalue')
                            }]
                };
                fieldset.add(field);

                for (var i = 0; i < meterStore.records.length; i++) {
                    var previousDate = '';
                    var previousValue = '';
                    var meter = meterStore.records[i];
                    if (!meter.IsVirtual) {
                        if (meter.LastData) {
                            previousDate = meter.LastData.AcquisitionDateTime.format(Viz.getLocalizedDateTimeFormat(true));
                            previousValue = meter.LastData.Value;
                        }
                        var field = {
                            xtype      : 'vizcompositefield',
                            fieldLabel : meter.LocationShortPath + ' / ' + meter.Name,
                            // labelStyle : 'width:' + width + 'px',
                            items      : [{
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        width : 150,
                                        value : previousDate
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        width : 120,
                                        value : previousValue
                                    }, {
                                        xtype      : 'numberfield',
                                        fieldLabel : $lang('msg_meterdata_value'),
                                        name       : meter.KeyMeter,
                                        width      : 120,
                                        allowBlank : true,
                                        listeners  : {
                                            // we use this in order to get all the numberfield with the find function of the form, because the findbytype of the formpanel doesnt work with compositefield (no cascade method)
                                            change : function(field, newValue, oldValue) {
                                                var key = field.getName();
                                                if (this.meterDataCollection.containsKey(key)) {
                                                    this.meterDataCollection.key(key).Value = newValue;
                                                }
                                                else {
                                                    var md = {
                                                        KeyMeter : key,// .replace('#', '') - 0,
                                                        Value    : newValue
                                                    };
                                                    this.meterDataCollection.add(key, md);
                                                }
                                            },
                                            scope  : this
                                        }
                                    }]
                        };

                        fieldset.add(field);

                    }
                }

                var messageFieldset = Ext.getCmp(this._fieldsetMessage);

                if (message) {
                    var messagePanel = Ext.getCmp(this._panelMessage);
                    var bodyMessage = '<div align="center"><b>' + message + '<b></span>';
                    if (messagePanel.body) {
                        messagePanel.body.update(bodyMessage);
                    }
                    else {
                        messagePanel.html = bodyMessage;
                    }
                    messageFieldset.doLayout();
                    messageFieldset.show();
                }
                else {
                    messageFieldset.hide();
                }

                try {
                    this.doLayout();
                }
                catch (e) {
                    ;
                }
                this.el.unmask();
            },

            saveItem                              : function() {
                var form = Ext.getCmp(this._formId).getForm();

                if (form.isValid() && this.currentSelectedDate) {
                    var batchAcquisitiondatetime = this.currentSelectedDate;
                    var meterDatas = new Array();
                    this.meterDataCollection.each(function(md) {
                                md.AcquisitionDateTime = batchAcquisitiondatetime;
                                if (Ext.isNumber(md.Value)) {
                                    meterDatas.push(md);
                                }
                            });

                    if (meterDatas.length > 0) {
                        Viz.Services.EnergyWCF.MeterData_SaveStore({
                                    store   : {
                                        create  : meterDatas,
                                        destroy : [],
                                        update  : []
                                    },
                                    success : function() {
                                        this.buildBatchForm();
                                    },
                                    scope   : this
                                });
                    }
                }
                else {
                    // form.isValid will not mark currentDateField as invalid because it has no allowBlank : true (it can't have because of this form's date fields setVisible(true), setVisible(false) model)
                    var currentDateField = Ext.getCmp(this.currentDisplayedDateTimeFieldId);
                    if (!currentDateField.getValue()) {
                        // TODO: implement items.markInvalid inside compositefield
                        currentDateField.items.each(function(f) {
                                    if (!f.hidden && !f.getValue()) {
                                        f.markInvalid($lang('error_msg_fieldrequired'));
                                    }
                                });
                        currentDateField.markInvalid($lang('error_msg_fieldrequired'));
                    }
                }
            },

            onSelectManualInput                   : function(combo, record, index) {
                var previousDisplayedDateTimeField = Ext.getCmp(this.currentDisplayedDateTimeFieldId);
                previousDisplayedDateTimeField.clearInvalid();
                previousDisplayedDateTimeField.setValue(null);
                previousDisplayedDateTimeField.setVisible(false);
                this.currentSelectedDate = null;

                switch (record.data.Value) {
                    case Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Seconds :
                        this.currentDisplayedDateTimeFieldId = this._fieldAcquisitionDateTime;
                        break;
                    case Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Minutes :
                        this.currentDisplayedDateTimeFieldId = this._fieldAcquisitionMinute;
                        break;
                    case Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Hours :
                        this.currentDisplayedDateTimeFieldId = this._fieldAcquisitionHour;
                        break;
                    case Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Days :
                        this.currentDisplayedDateTimeFieldId = this._fieldAcquisitionDay;
                        break;
                    case Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Months :
                        this.currentDisplayedDateTimeFieldId = this._fieldAcquisitionMonthYear;
                        Ext.getCmp(this._fieldAcquisitionMonthYear).disableMonth(false);
                        break;
                    case Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Years :
                        this.currentDisplayedDateTimeFieldId = this._fieldAcquisitionMonthYear;
                        Ext.getCmp(this._fieldAcquisitionMonthYear).disableMonth(true);
                        break;
                    default :
                }
                var currentDisplayedDateTimeField = Ext.getCmp(this.currentDisplayedDateTimeFieldId);
                currentDisplayedDateTimeField.setVisible(true);
                currentDisplayedDateTimeField.doLayout();
            }

        });
Ext.reg('vizFormMeterDataBatch', Viz.form.MeterDataBatch);
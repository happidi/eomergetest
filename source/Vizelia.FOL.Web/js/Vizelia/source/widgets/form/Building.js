﻿

Ext.namespace('Viz.form');
/**
 * @class Viz.form.Building
 * @extends Ext.FormPanel.
 */
Viz.form.Building = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    name                 : 'Building',
                    mode_load            : 'remote',
                    psetConfig           : {
                        IfcType                    : 'IfcBuilding',
                        entityKey                  : 'KeyBuilding',
                        filterByClassificationItem : false
                    },
                    serviceHandler       : Viz.Services.CoreWCF.Building_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.Building_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.Building_FormUpdate,
                    hasValidationToolbar : true,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Building.superclass.constructor.call(this, config);

            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral : function(config) {
                return [{
                            name       : 'LocalId',
                            fieldLabel : $lang('msg_localid'),
                            allowBlank : false,
                            hidden     : false
                        }, {
                            fieldLabel : $lang('msg_name'),
                            allowBlank : false,
                            name       : 'Name'
                        }, {
                            xtype      : 'textarea',
                            anchor     : Viz.Const.UI.Anchor,
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description'
                        }]
            }
        });

Ext.reg('vizFormBuilding', Viz.form.Building);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.MeterValidationRule
 * @extends Ext.FormPanel.
 */
Viz.form.MeterValidationRule = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                          : function(config) {
                this._gridFilterSpatialPset = Ext.id();

                var Id = config.entity ? config.entity.KeyMeterValidationRule : null;
                this.storeFilterSpatial = new Viz.store.MeterValidationRuleFilterSpatial({
                            KeyMeterValidationRule : Id
                        });
                this.storeFilterMeterClassification = new Viz.store.MeterValidationRuleFilterMeterClassification({
                            KeyMeterValidationRule : Id
                        });
                this.storeMeter = new Viz.store.MeterValidationRuleMeter({
                            KeyMeterValidationRule : Id
                        });
                this.storeMeterAll = new Viz.store.MeterValidationRuleMeterAll({
                            KeyMeterValidationRule : Id
                        });

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.MeterValidationRule_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.MeterValidationRule_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.MeterValidationRule_FormUpdate,
                    hasValidationToolbar : true,
                    tabConfig            : {
                        deferredRender : true,
                        items          : [{
                                    title   : $lang('msg_general'),
                                    iconCls : 'viz-icon-small-form-update',
                                    items   : this.configFormGeneral(config)
                                }, {
                                    title    : $lang('msg_chart_datasource'),
                                    iconCls  : 'viz-icon-small-chart-datasource',
                                    hidden   : config.readonly,
                                    defaults : {
                                        anchor   : Viz.Const.UI.Anchor,
                                        hideMode : 'offsets'
                                    },
                                    items    : [this.configGridFilterSpatial(config), this.configGridFilterMeterClassification(config), this.configDetailGridMeter(config)]
                                }, {
                                    title      : $lang('msg_chart_meterall'),
                                    iconCls    : 'viz-icon-small-meter-all',
                                    hidden     : config.mode == 'create',
                                    disabled   : config.isWizard,
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridMeterAll(config)]
                                }, {
                                    title      : $lang('msg_alarmdefinition_condition'),
                                    iconCls    : 'viz-icon-small-calculatedserie',
                                    items      : this.configFormCondition(config),
                                    labelWidth : 250
                                }]
                    },
                    labelWidth           : 180,
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.MeterValidationRule.superclass.constructor.call(this, config);
            },
            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                    : function(config) {
                return [{
                    name   : 'KeyMeterValidationRule',
                    hidden : true
                }/*
                     * , { fieldLabel : $lang('msg_localid'), name : 'LocalId' }
                     */
                , {
                    fieldLabel : $lang('msg_title'),
                    name       : 'Title',
                    allowBlank : false
                }, {
                    xtype      : 'textarea',
                    fieldLabel : $lang('msg_description'),
                    name       : 'Description'
                }, {
                    fieldLabel : $lang('msg_alarmdefinition_enabled'),
                    name       : 'Enabled',
                    xtype      : 'vizcheckbox',
                    checked    : true
                }, {
                    fieldLabel : $lang('msg_alarmdefinition_startdate'),
                    name       : 'StartDate',
                    hideTime   : false,
                    xtype      : 'vizDateTime',
                    allowBlank : true,
                    hasSwitch  : false
                }, {
                    fieldLabel : $lang('msg_alarmdefinition_enddate'),
                    name       : 'EndDate',
                    hideTime   : false,
                    xtype      : 'vizDateTime',
                    allowBlank : true,
                    hasSwitch  : false
                }, {
                    fieldLabel : $lang('msg_alarmdefinition_lastprocessdatetime'),
                    name       : 'LastProcessDateTime',
                    hideTime   : false,
                    xtype      : 'vizDateTime',
                    allowBlank : true,
                    hasSwitch  : false,
                    hidden     : config.mode != 'update'
                }, {
                    xtype      : 'textarea',
                    allowBlank : true,
                    fieldLabel : $lang('msg_chartscheduler_emails'),
                    name       : 'Emails'
                }, {
                    fieldLabel : $lang('msg_playlist_culture'),
                    name       : 'KeyLocalizationCulture',
                    hiddenName : 'KeyLocalizationCulture',
                    autoLoad   : config.mode == 'create',
                    xtype      : 'vizComboLocalizationCulture'
                }];
            },

            /**
             * The configuration object for the filter spatial grid.
             * @return {Object}
             */
            /**
             * The configuration object for the filter spatial grid.
             * @return {Object}
             */
            configGridFilterSpatial              : function(config) {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_chart_filterspatial'),
                    iconCls     : 'viz-icon-small-site',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : {
                        xtype                       : 'vizGridFilterSpatial',
                        store                       : this.storeFilterSpatial,
                        border                      : true,
                        loadMask                    : false,
                        height                      : 200,
                        disableCheckboxForAncestors : true
                    }
                };
            },

            /**
             * The configuration object for the filter meter classification.
             * @return {Object}
             */
            configGridFilterMeterClassification  : function(config) {
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_meter_classification'),
                    iconCls     : 'viz-icon-small-classificationitem',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : {
                        xtype     : 'vizGridFilterMeterClassification',
                        store     : this.storeFilterMeterClassification,
                        border    : true,
                        listeners : {
                            beforeaddrecord: this.onBeforeAddFilterMeterClassification,
                            load: {
                                fn: function () {
                                    if (!this.storeFilterSpatial.hasLoaded)
                                        this.storeFilterSpatial.load();
                                },
                                scope: this
                            },
                            scope           : this
                        },
                        height    : 200
                    }
                };
            },
            /**
             * The configuration object for the detail grid Meter.
             * @return {Object}
             */
            configDetailGridMeter                : function(config) {
                var retVal = this.configGridDetail({
                            name                         : 'Meter',
                            windowTitle                  : $lang('msg_meter'),
                            windowWidth                  : 1000,
                            windowHeight                 : 400,
                            windowIconCls                : 'viz-icon-small-meter',
                            xtypeGridDetail              : 'vizGridMeter',
                            columnsGridDetail            : Viz.Configuration.Columns.ChartMeter,
                            buttonAddTitle               : $lang('msg_meter_add'),
                            buttonAddIconCls             : 'viz-icon-small-add',
                            buttonDeleteTitle            : $lang('msg_meter_delete'),
                            buttonDeleteIconCls          : 'viz-icon-small-delete',
                            storeDetail                  : this.storeMeter,
                            keyColumn                    : 'KeyMeter',
                            ddGroupGridDetail            : 'SpatialDD',
                            enableDropFromTreeGridDetail : true
                        });
                Ext.apply(retVal, {
                            height   : 200,
                            loadMask : false
                        });
                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_meters') + ' (' + $lang('msg_meter_novirtual') + ')',
                    iconCls     : 'viz-icon-small-meter',
                    collapsible : true,
                    collapsed   : true,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : retVal
                };
            },

            /**
             * Return the configuration object for the grid containing all the meters.(readonly grid)
             * @param {} config
             * @return {}
             */
            configGridMeterAll                   : function(config) {
                return {
                    xtype            : 'vizGridMeter',
                    store            : this.storeMeterAll,
                    border           : true,
                    readonly         : true,
                    alloweditbyclick : true,
                    columns          : Viz.Configuration.Columns.ChartMeter
                };
            },

            /**
             * config of the form containing the alarm condition
             * @param {} config
             * @return {}
             */
            configFormCondition                  : function(config) {
                return [{
                            xtype      : 'vizcompositefield',
                            fieldLabel : $lang('msg_meterdata_frequency'),
                            items      : [{
                                        name       : 'AcquisitionFrequency',
                                        xtype      : 'vizSpinnerField',
                                        minValue   : 1,
                                        allowBlank : true
                                    }, {
                                        name           : 'AcquisitionInterval',
                                        style          : 'margin-left:5px',
                                        allowBlank     : false,
                                        xtype          : 'vizComboEnum',
                                        enumTypeName   : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common',
                                        sortByValues   : true,
                                        width          : 90,
                                        value          : "0",
                                        valuesToRemove : ['Global']
                                    }]
                        }, {
                            xtype      : 'vizcompositefield',
                            fieldLabel : $lang('msg_metervalidationrule_delay'),
                            items      : [{
                                        name       : 'Delay',
                                        xtype      : 'vizSpinnerField',
                                        minValue   : 1,
                                        allowBlank : true
                                    }, {
                                        name           : 'DelayInterval',
                                        style          : 'margin-left:5px',
                                        allowBlank     : false,
                                        xtype          : 'vizComboEnum',
                                        enumTypeName   : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common',
                                        sortByValues   : true,
                                        width          : 90,
                                        value          : "0",
                                        valuesToRemove : ['Global']
                                    }]
                        }, {
                            fieldLabel : $lang('msg_metervalidationrule_increaseonly'),
                            name       : 'IncreaseOnly',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }, {
                            fieldLabel       : $lang('msg_metervalidationrule_spike'),
                            xtype            : 'viznumberfield',
                            allowBlank       : true,
                            name             : 'SpikeDetection',
                            decimalPrecision : 5
                        }, {
                            fieldLabel       : $lang('msg_metervalidationrule_spikeabsthreshold'),
                            xtype            : 'viznumberfield',
                            allowNegative    : false,
                            allowBlank       : true,
                            name             : 'SpikeDetectionAbsThreshold',
                            decimalPrecision : 5
                        }, {
                            fieldLabel       : $lang('msg_metervalidationrule_maxrecurringvalues'),
                            xtype            : 'viznumberfield',
                            allowBlank       : true,
                            name             : 'MaxRecurringValues',
                            decimalPrecision : 0
                        }, {
                            fieldLabel       : $lang('msg_metervalidationrule_minvalue') + ' > ',
                            xtype            : 'viznumberfield',
                            allowBlank       : true,
                            name             : 'MinValue',
                            decimalPrecision : 5
                        }, {
                            fieldLabel       : $lang('msg_metervalidationrule_maxvalue') + ' < ',
                            xtype            : 'viznumberfield',
                            allowBlank       : true,
                            name             : 'MaxValue',
                            decimalPrecision : 5
                        }, {
                            fieldLabel     : $lang('msg_meterdata_validity'),
                            name           : 'Validity',
                            allowBlank     : true,
                            xtype          : 'vizComboEnum',
                            enumTypeName   : 'Vizelia.FOL.BusinessEntities.MeterDataValidity,Vizelia.FOL.Common',
                            sortByValues   : true,
                            displayIcon    : false,
                            valuesToRemove : ['Invalid']
                        }];
            },

            /**
             * Handler on the beforeadd event of the grid FilterMeterClassification
             */
            onBeforeAddFilterMeterClassification : function(grid, entity) {
                if (this.storeFilterSpatial.hasLoaded)
                    this.storeFilterSpatial.save();

                if (!this.storeFilterSpatial.data || this.storeFilterSpatial.data.length == 0) {
                    Ext.MessageBox.alert($lang("msg_metervalidationrule"), $lang("msg_chart_wizard_spatial_empty"));
                    return false;
                }
                entity.KeyMeterValidationRule = this.entity.KeyMeterValidationRule;
                entity.filterSpatial = this.storeFilterSpatial.crudStore;

                return true;
            },

            /**
             * Before save handler.
             * @param {Viz.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                         : function(form, item, extraParams) {
                var storeArray = this.getExtraStoreArray();

                Ext.each(storeArray, function(store) {
                            if (store.hasLoaded)
                                store.save();
                        }, this);

                Ext.apply(extraParams, {
                            filterSpatial             : this.storeFilterSpatial.crudStore,
                            filterMeterClassification : this.storeFilterMeterClassification.crudStore,
                            meters                    : this.storeMeter.crudStore
                        });

                if (Ext.isEmpty(item.AcquisitionFrequency))
                    item.AcquisitionFrequency = null;

                if (Ext.isEmpty(item.Delay))
                    item.Delay = null;

                if (Ext.isEmpty(item.SpikeDetection))
                    item.SpikeDetection = null;

                if (Ext.isEmpty(item.SpikeDetectionAbsThreshold))
                    item.SpikeDetectionAbsThreshold = null;

                if (Ext.isEmpty(item.MaxRecurringValues))
                    item.MaxRecurringValues = null;

                if (Ext.isEmpty(item.MinValue))
                    item.MinValue = null;

                if (Ext.isEmpty(item.MaxValue))
                    item.MaxValue = null;

                if (Ext.isEmpty(item.Validity))
                    item.Validity = null;
                return true;
            },

            /**
             * Return an array of all the extra store of the Chart.
             * @return {Array}
             */
            getExtraStoreArray                   : function() {
                return [this.storeFilterSpatial, this.storeFilterMeterClassification, this.storeMeter];
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                        : function(form, action) {
                Viz.form.MeterValidationRule.superclass.onSaveSuccess.apply(this, arguments);

                var storeArray = this.getExtraStoreArray();
                storeArray.push(this.storeMeterAll);

                Ext.each(storeArray, function(store) {
                            store.serviceParams.KeyMeterValidationRule = this.entity.KeyMeterValidationRule;
                            if (store.hasLoaded)
                                store.reload();
                        }, this);

                return true;
            }
        });
Ext.reg('vizFormMeterValidationRule', Viz.form.MeterValidationRule);

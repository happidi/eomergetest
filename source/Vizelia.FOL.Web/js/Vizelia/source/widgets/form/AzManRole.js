﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.AzManRole
 * @extends Ext.FormPanel.
 */
Viz.form.AzManRole = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var filterId = config.entity ? config.entity.Id : null;

                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.AuthenticationWCF.AzManRole_FormLoad,
                    serviceHandlerCreate : Viz.Services.AuthenticationWCF.AzManRole_FormCreate,
                    serviceHandlerUpdate : Viz.Services.AuthenticationWCF.AzManRole_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {
                    auditEntityName : 'AuthorizationItem.Role'
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.AzManRole.superclass.constructor.call(this, config);

            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral : function(config) {
                return [{
                            // fieldLabel : $lang('msg_parent'),
                            name      : 'ParentId',
                            hidden    : true,
                            hideLabel : true,
                            readOnly  : true
                        }, {
                            // fieldLabel : 'Id',
                            name      : 'Id',
                            hidden    : true,
                            hideLabel : true,
                            readOnly  : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            anchor     : Viz.Const.UI.Anchor + ' -40',
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description'
                        }];
            }
        });

Ext.reg('vizFormAzManRole', Viz.form.AzManRole);
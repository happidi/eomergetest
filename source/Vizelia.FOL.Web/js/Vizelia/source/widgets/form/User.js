﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.User
 * @extends Ext.FormPanel.
 */
Viz.form.User = Ext.extend(Viz.form.FormPanel, {
    /**
             * Ctor.
             * @param {Object} config The configuration options
             */
    constructor: function(config) {
        config = config || {};
        this._formUserId = Ext.id();
        this._fieldpasswordId = Ext.id();
        this._fieldEmailId = Ext.id();
        this._fieldTimeZoneIdId = Ext.id();

        var username = config.entity ? config.entity.UserName : null;
        if (username == null)
            username = "";
        this._gridAuthorization = Ext.id();
        this._gridApplicationGroup = Ext.id();
        this._gridProjects = Ext.id();

        this.storeAuthorization = new Viz.store.AuthorizationItem({
            username: username,
            authorizationFilter: 'Role',
            IsFilterOnly: false
        });

        this.storeApplicationGroup = new Viz.store.UserApplicationGroup({
            username: username
        });

        this.storeProjects = new Viz.store.AuthorizationItem({
            username: username,
            authorizationFilter: 'Role',
            IsFilterOnly: true
        });

        var defaultConfig = {
            applyEnabled: false,
            trackResetOnLoad: true,
            mode_load: 'remote',
            serviceHandler: Viz.Services.AuthenticationWCF.User_FormLoad,
            serviceHandlerCreate: Viz.Services.AuthenticationWCF.User_FormCreate,
            serviceHandlerUpdate: Viz.Services.AuthenticationWCF.User_FormUpdate,
            tabConfig: {
                deferredRender: true,
                items: [
                    {
                        title: $lang('msg_general'),
                        iconCls: 'viz-icon-small-user',
                        items: this.configFormGeneral(config)
                    }, {
                        title: $lang('msg_authorization'),
                        iconCls: 'viz-icon-small-role',
                        labelAlign: 'top',
                        layout: 'fit',
                        items: [this.configGridAuthorization(config)]
                    }, {
                        title: $lang('msg_applicationgroup_root'),
                        iconCls: 'viz-icon-small-applicationgroup',
                        labelAlign: 'top',
                        layout: 'fit',
                        items: [this.configGridApplicationGroup(config)]
                    }, {
                        title: $lang('msg_azmanfilter_root'),
                        iconCls: 'viz-icon-small-filter',
                        labelAlign: 'top',
                        layout: 'fit',
                        items: [this.configGridProjects(config)]
                    }, {
                        title: $lang('msg_preferences'),
                        iconCls: 'viz-icon-small-azmanoperation',
                        defaultType: 'displayfield',
                        labelWidth: 250,
                        items: [
                            {
                                name: 'Preferences_KeyUser',
                                hidden: true
                            },
                            {
                                fieldLabel: $lang('msg_enum_dynamicdisplayimagetype_desktopbackground'),
                                name: 'Preferences_DesktopBackground',
                                xtype: 'vizComboDesktopBackground',
                                allowBlank: true
                            },
                            {
                                fieldLabel: $lang('msg_desktopbackground_custom'),
                                name: 'Preferences_CustomDesktopBackground',
                                xtype: 'vizComboDynamicDisplayImage',
                                type: Vizelia.FOL.BusinessEntities.DynamicDisplayImageType.DesktopBackground,
                                store: new Viz.store.DynamicDisplayImage({
                                    type: Vizelia.FOL.BusinessEntities.DynamicDisplayImageType.DesktopBackground
                                }),
                                allowBlank: true
                            }, {
                                fieldLabel: $lang('msg_desktopshortcuttextcolor'),
                                name: 'Preferences_DesktopTextColor',
                                xtype: 'colorpickerfield',
                                allowBlank: true,
                                id: this._colorPickerId
                            },
                            {
                                name: 'Preferences_TimeZoneId',
                                hidden: true
                            },
                            {
                                fieldLabel: $lang('msg_chart_preloadall'),
                                name: 'Preferences_PreloadCharts',
                                xtype: 'vizcheckbox',
                                hidden: !$authorized('@@@@ Chart - PreLoad'),
                                allowBlank: true
                            },
                            {
                                fieldLabel: $lang('msg_chart_kpi'),
                                name: 'Preferences_DefaultChartKPI',
                                hiddenName: 'Preferences_DefaultChartKPI',
                                xtype: 'vizComboChart',
                                store: new Viz.store.ChartKPI(),
                                allowBlank: true
                            },
                            {
                                fieldLabel: $lang('msg_user_grid_page_size'),
                                name: 'Preferences_GridsPageSize',
                                xtype: 'viznumberfield',
                                allowBlank: false,
                                value: Viz.Const.UI.DefaultGridPageSize,
                                allowDecimals: false,
                                width: 50,
                                minValue: 1,
                                maxValue: 1000
                            }
                        ]
                    }
                ]
            }
        };
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.form.User.superclass.constructor.call(this, config);

        if (this.entity.ProviderUserKey) {
            this.entity.UserLocationLongPath = this.entity.ProviderUserKey.UserLocationLongPath;
        }

        this.on("beforesave", function(form, item) {
            form.getForm().findField('Preferences_TimeZoneId').setValue(form.getForm().findField('TimeZoneId').getValue());
            Ext.apply(item, this.buildUser());
            if (form.mode == 'create')
                delete item.LastLoginDate;

            this.serviceParams = {
                password: this.getPassword()
            };
            return true;
        }, this);
    },

    /**
             * Builds the general form.
             */
    configFormGeneral: function(config) {
        return [
            {
                fieldLabel: $lang('msg_password'),
                id: this._fieldpasswordId + "_fake",
                name: 'passwordFake',
                inputType: 'password',
                emptyText: 'This information is protected',
                allowBlank: true,
                hidden: true
            }, {
                fieldLabel: 'Guid',
                name: 'Guid',
                hideLabel: true,
                hidden: true
            }, {
                fieldLabel: $lang('msg_login'),
                name: 'UserName',
                id: this._formUserId,
                allowBlank: false
            }, {
                fieldLabel: $lang('msg_user_firstname'),
                name: 'FirstName'
            }, {
                fieldLabel: $lang('msg_user_lastname'),
                name: 'LastName'
            },
            {
                xtype: 'vizComboOccupant',
                displayFn: function(entity) {
                    return entity.OccupantFullName;
                }
            }, {
                fieldLabel: $lang('msg_password'),
                id: this._fieldpasswordId,
                name: 'password',
                inputType: 'password',
                emptyText: 'This information is protected',
                allowBlank: this.mode == "create" ? false : true
            }, {
                fieldLabel: $lang('msg_password_confirm'),
                name: 'confirmPassword',
                validator: Ext.form.VTypes.passwordconfirmValidator,
                inputType: 'password',
                initialPasswordField: this._fieldpasswordId,
                userIdField: this._formUserId,
                emptyText: 'This information is protected',
                allowBlank: this.mode == "create" ? false : true
            }, {
                fieldLabel: $lang('msg_occupant_email'),
                id: this._fieldEmailId,
                name: 'Email',
                vtype: 'email',
                allowBlank: false
            }, {
                xtype: 'vizComboTimeZone',
                fieldLabel: $lang('msg_user_time_zone'),
                id: this._fieldTimeZoneIdId,
                name: 'TimeZoneId',
                autoLoad: false,
                allowBlank: false,
                editable: true
            }, {
                fieldLabel: $lang('msg_occupant_location'),
                xtype: 'viztreecomboSpatial',
                name: 'KeyLocation',
                validator: Ext.form.VTypes.locationValidator,
                ExcludeMetersAndAlarms: true,
                originalDisplayValue: 'UserLocationLongPath'
            }, {
                fieldLabel: $lang('msg_comment'),
                xtype: 'textarea',
                name: 'Comment',
                allowBlank: true
            }, {
                fieldLabel: $lang('msg_user_lastlogindate'),
                name: 'LastLoginDate',
                xtype: 'datefield',
                format: Viz.getLocalizedDateTimeFormat(false),
                readOnly: true,
                hideTrigger: true,
                allowBlank: true
            }, {
                fieldLabel: $lang('msg_user_isapproved'),
                name: 'IsApproved',
                xtype: 'vizcheckbox'
            }, {
                fieldLabel: $lang('msg_user_isonline'),
                name: 'IsOnline',
                xtype: 'vizcheckbox',
                disabled: true,
                allowBlank: true
            }, {
                fieldLabel: $lang('msg_user_islocked'),
                name: 'IsLockedOut',
                xtype: 'vizcheckbox',
                disabled: false,
                allowBlank: true
            }, {
                fieldLabel: $lang('msg_user_neverexpires'),
                name: 'NeverExpires',
                xtype: 'vizcheckbox',
                disabled: false,
                allowBlank: true
            }
        ];
    },
    /**
             * Sets the non linear properties for the user.
             * @param {Viz.BusinessEntity.FOLMembershipUser} user
             */
    setUser: function(user) {
        this.getForm().setValues({
            Guid: user.ProviderUserKey.Guid,
            KeyOccupant: user.ProviderUserKey.KeyOccupant,
            FirstName: user.ProviderUserKey.FirstName,
            LastName: user.ProviderUserKey.LastName,
            NeverExpires: user.ProviderUserKey.NeverExpires,
            KeyLocation: user.ProviderUserKey.KeyLocation,
            Preferences_KeyUser: user.Preferences.KeyUser,
            Preferences_DesktopBackground: user.Preferences.DesktopBackground,
            Preferences_CustomDesktopBackground: user.Preferences.CustomDesktopBackground,
            Preferences_DesktopTextColor: user.Preferences.DesktopTextColor,
            Preferences_TimeZoneId: user.Preferences.TimeZoneId,
            Preferences_PreloadCharts: user.Preferences.PreloadCharts,
            Preferences_DefaultChartKPI: user.Preferences.DefaultChartKPI,
            Preferences_GridsPageSize: user.Preferences.GridsPageSize,
        });
    },

    /**
             * Gets the password, returns null if no password was entered.
             * @return {String}
             */
    getPassword: function() {
        var passwordField = Ext.getCmp(this._fieldpasswordId);
        if (passwordField.isDirty())
            return passwordField.getValue();
        else
            return null;

    },

    /**
             * Builds a user object from the form.
             * @return {Viz.BusinessEntity.FOLMembershipUser}
             */
    buildUser: function () {
        var form = this.getForm();
        var values = form.getFieldValues(); // do not use here getValues as it does not return correctly datefield.

        // ie8 bug. can't handle empty string
        values.Preferences_DesktopTextColor = values.Preferences_DesktopTextColor || null;
        if (values.Preferences_DesktopTextColor !== null) {
            if (!values.Preferences_DesktopTextColor.trim()) {
                values.Preferences_DesktopTextColor = null;
            }
        }

        Ext.apply(values, {
            ProviderUserKey: {
                KeyOccupant: values.KeyOccupant,
                Guid: values.Guid != "" ? values.Guid : null,
                NeverExpires: values.NeverExpires,
                KeyLocation: values.KeyLocation,
                FirstName: values.FirstName,
                LastName: values.LastName
            },
            Preferences: {
                KeyUser: values.Preferences_KeyUser,
                DesktopBackground: values.Preferences_DesktopBackground,
                CustomDesktopBackground: values.Preferences_CustomDesktopBackground,
                DesktopTextColor: values.Preferences_DesktopTextColor,
                TimeZoneId: values.Preferences_TimeZoneId,
                PreloadCharts: values.Preferences_PreloadCharts,
                DefaultChartKPI: values.Preferences_DefaultChartKPI,
                GridsPageSize: values.Preferences_GridsPageSize,
            }
        });

        delete values.KeyOccupant;
        delete values.Guid;
        delete values.Preferences_KeyUser;
        delete values.Preferences_DesktopBackground;
        delete values.Preferences_CustomDesktopBackground;
        delete values.Preferences_DesktopTextColor;
        delete values.Preferences_TimeZoneId;
        delete values.Preferences_PreloadCharts;
        delete values.Preferences_DefaultChartKPI;
        delete values.Preferences_GridsPageSize;

        return values;
    },

    /**
             * The configuration for the grid authorization.
             * @return {object}
             */
    configGridAuthorization: function(config) {
        return {
            xtype: 'vizGrid',
            stateId: 'vizFormUser.gridAuthorization',
            id: this._gridAuthorization,
            hasRowEditor: false,
            hasToolbar: false,
            hasPagingToolbar: true,
            border: true,
            stripeRows: false,
            plugins: [
                new Viz.plugins.GridExport({
                    listeners: {
                        beforeExport: function(plugin) {
                            plugin.title = $lang('msg_authorization') + ' : ' + this.entity.UserName;
                        },
                        scope: this
                    },
                    paging: {
                        filters: this.storeAuthorization.baseParams.filters
                    }
                })
            ],
            store: this.storeAuthorization,
            columns: Viz.Configuration.Columns.AzManItem
        };
    },

    /**
             * The configuration for the grid authorization.
             * @return {object}
             */
    configGridApplicationGroup: function(config) {
        return {
            xtype: 'vizGrid',
            stateId: 'vizFormUser.gridApplicationGroup',
            id: this._gridApplicationGroup,
            hasRowEditor: false,
            hasToolbar: false,
            hasPagingToolbar: true,
            border: true,
            stripeRows: false,
            tbar: [
                {
                    text: $lang("msg_applicationgroup_update"),
                    iconCls: 'viz-icon-small-applicationgroup-update',
                    // hidden : !$authorized('@@@@ Application Groups - Full Control'),
                    scope: this,
                    handler: function(a, b, c) {
                        var grid = Ext.getCmp(this._gridApplicationGroup);
                        grid.onUpdateRecord();
                    }
                }
            ],
            plugins: [
                new Viz.plugins.GridExport({
                    listeners: {
                        beforeExport: function(plugin) {
                            plugin.title = $lang('msg_applicationgroup_root') + ' : ' + this.entity.UserName;
                        },
                        scope: this
                    }
                })
            ],
            store: this.storeApplicationGroup,
            columns: Viz.Configuration.Columns.AzManItem,
            formConfig: {
                common: {
                    width: 500,
                    height: 350,
                    xtype: 'vizFormApplicationGroup',
                    messageSaveStart: 'ApplicationGroupChangeStart',
                    messageSaveEnd: 'ApplicationGroupChange'
                },
                update: {
                    title: $lang("msg_applicationgroup_update"),
                    iconCls: 'viz-icon-small-filter-update',
                    titleEntity: 'Name',
                    serviceParamsEntity: [
                        {
                            name: 'Key',
                            value: 'Id'
                        }
                    ]
                }
            }
        };
    },
    configGridProjects: function(config) {
        return {
            xtype: 'vizGrid',
            stateId: 'vizFormUser.vizGridFilterAzManRole',
            id: this._gridProjects,
            hasRowEditor: false,
            hasToolbar: false,
            hasPagingToolbar: true,
            border: true,
            stripeRows: false,
            tbar: [
                {
                    text: $lang("msg_azmanfilter_update"),
                    iconCls: 'viz-icon-small-filter-update',
                    // hidden : !$authorized('@@@@ Application Groups - Full Control'),
                    scope: this,
                    handler: function() {
                        var gridProjects = Ext.getCmp(this._gridProjects);
                        gridProjects.onUpdateRecord();
                    }
                }
            ],
            plugins: [
                new Viz.plugins.GridExport({
                    listeners: {
                        beforeExport: function(plugin) {
                            plugin.title = $lang('msg_azmanfilter_root') + ' : ' + this.entity.UserName;
                        },
                        scope: this
                    },
                    paging: {
                        filters: this.storeProjects.baseParams.filters
                    }
                })
            ],
            store: this.storeProjects,
            columns: Viz.Configuration.Columns.AzManItem,
            formConfig: {
                common: {
                    width: 500,
                    height: 350,
                    xtype: 'vizFormAzManFilter',
                    messageSaveStart: 'AzManChangeStart',
                    messageSaveEnd: 'AzManChange'
                },
                update: {
                    title: $lang("msg_azmanfilter_update"),
                    iconCls: 'viz-icon-small-filter-update',
                    titleEntity: 'Name',
                    serviceParamsEntity: [
                        {
                            name: 'Key',
                            value: 'Id'
                        }
                    ]
                }
            }
        };
    },
    onAfterLoadValues: function (form) {
        if (form.mode === 'update') {
            var user = this.entity;
            this.setUser(user);
            form.getForm().findField('TimeZoneId').setValue(user.Preferences.TimeZoneId);

            if (Viz.App.Global.User.KeyUser === this.entity.Preferences.KeyUser) {
                Ext.getCmp(this._fieldTimeZoneIdId).loadDefaultTimeZone();
            }
        }
    },
    onSaveSuccess: function() {
        if (Viz.App.Global.User.KeyUser === this.entity.Preferences.KeyUser) {
            new Viz.form.UserInfo().updateUserPreferences(this.entity.Preferences);
        }
    }
});

Ext.reg('vizFormUser', Viz.form.User);
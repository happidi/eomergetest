﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.DataSeriePsetRatio
 * @extends Ext.FormPanel.
 */
Viz.form.DataSeriePsetRatio = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.DataSeriePsetRatio} entity The DataSeriePsetRatio entity.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                this._psetId = Ext.id();

                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.DataSeriePsetRatio_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.DataSeriePsetRatio_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.DataSeriePsetRatio_FormUpdate,
                    items                : this.configFormGeneral(config),
                    labelWidth           : 200
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.DataSeriePsetRatio.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyDataSeriePsetRatio',
                            hidden : true
                        }, {
                            name                 : 'Pset',
                            fieldLabel           : $lang('msg_pset'),
                            xtype                : 'viztreecomboPset',
                            id                   : this._psetId,
                            originalDisplayValue : 'PropertySingleValueLongPath',
                            validator            : Ext.form.VTypes.psetAttributeSelectedValidator,
                            listeners            : {
                                select : function() {
                                    // we need to defer because the validator is called also with a defer
                                    this.onChangeValuePset.defer(500, this, arguments);
                                },
                                scope  : this
                            }
                        }, {
                            fieldLabel       : $lang('msg_chartfilterspatialpset_order'),
                            name             : 'Order',
                            minValue         : 1,
                            decimalPrecision : 0,
                            allowBlank       : false,
                            value            : config.entity ? config.entity.Order || 1 : 1
                        }, {
                            fieldLabel     : $lang('msg_chartfilterspatialpset_operator'),
                            name           : 'Operator',
                            xtype          : 'vizComboEnum',
                            enumTypeName   : 'Vizelia.FOL.BusinessEntities.ArithmeticOperator,Vizelia.FOL.Common',
                            displayIcon    : true,
                            allowBlank     : false,
                            valuesToRemove : ['None', 'Add', 'Subtract', 'Power']
                        }, {
                            fieldLabel   : $lang('msg_meter_groupoperation'),
                            name         : 'GroupOperation',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.MathematicOperator,Vizelia.FOL.Common'
                        }, {
                            fieldLabel : $lang('msg_dataseriepsetratio_fillmissingvalues'),
                            name       : 'FillMissingValues',
                            xtype      : 'vizcheckbox'
                        }, {
                            fieldLabel : $lang('msg_dataseriepsetratio_stopatfirstlevelencountered'),
                            name       : 'StopAtFirstLevelEncountered',
                            xtype      : 'vizcheckbox'
                        }];
            },

            /**
             * Handler for updating fiels based on the form values.
             * @param {Viz.form.FormPanel} the form panel
             * @param {Object} values
             */
            onAfterLoadValues : function(form, values) {
                Ext.getCmp(this._psetId).setValue(values.PropertySingleValueLongPath);
            },

            /**
             * Handler for updating PropertySingleValueLongPath when a the KeyPropertySingleValue is changed.
             */
            onChangeValuePset : function() {
                var combo = Ext.getCmp(this._psetId);
                var node = combo.getTree().getSelectionModel().getSelectedNode();
                if (node && Ext.isEmpty(combo.getActiveError())) {
                    this.entity.PropertySingleValueLongPath = node.getPath('text');
                    this.entity.PsetName = node.parentNode.attributes.entity.PsetName;
                    this.entity.AttributeName = node.attributes.entity.AttributeName;
                    this.isDirty = true;
                }
            }
        });

Ext.reg('vizFormDataSeriePsetRatio', Viz.form.DataSeriePsetRatio);
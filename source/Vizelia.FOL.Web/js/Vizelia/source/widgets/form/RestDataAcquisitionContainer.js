﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.RESTDataAcquisitionContainer
 * @extends Ext.FormPanel.
 */
Viz.form.RESTDataAcquisitionContainer = Ext.extend(Viz.form.FormPanel, {

    scriptUrl: window.location.protocol + '//ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx',

    /**
     * The current version of bing map
     * @cfg {String}
     */
    version: '7.0',

    /**
     * List of supported culture for the map.
     * @cfg {String}
     */
    supportedCultures: ['nl-BE', 'en-CA', 'en-IN', 'en-GB', 'en-US', 'fr-CA', 'fr-FR', 'de-DE', 'it-IT', 'ja-JP', 'es-MX', 'es-ES', 'es-US'],

    /**
     * Infobox associated with a Telvent Weather Station on a Bing map (if provider is Telvent)
     */
    stationInfoBox: undefined,

    /**
     * Enumeration for LocationType used for Telvent Weather Service
     */
    locationType: {
        Address: 0,
        LatLon: 1,
        StationId: 2
    },

    /**
     * @cfg {String} the loading class
     */
    loadingClass: 'viz-chart-loading',

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                       : function(config) {
                this._gridDataAcquisitionEndpoint = Ext.id();
                this._tabEndpoint = Ext.id();
                this._locationTypeFieldSet = Ext.id();
                this._restUrl = Ext.id();
                this._address = Ext.id();
                this._latitude = Ext.id();
                this._longitude = Ext.id();
                this._stationId = Ext.id();
                this._mapPanel = Ext.id();
                var Id = config.entity ? config.entity.KeyDataAcquisitionContainer : null;
                
                this.storeDataAcquisitionEndpoint = new Viz.store.RESTDataAcquisitionEndpoint({ 
                            KeyDataAcquisitionContainer : Id
                });
                

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.RESTDataAcquisitionContainer_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.RESTDataAcquisitionContainer_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.RESTDataAcquisitionContainer_FormUpdate,
                    tabConfig            : {
                        deferredRender : true,
                        items          : [{
                                    title      : $lang('msg_general'),
                                    iconCls    : 'viz-icon-small-form-update',
                                    items      : this.configFormGeneral(config),
                                    labelWidth : 250
                                }, {
                                    title      : $lang('msg_restdataacquisitionendpoint'),
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    id         : this._tabEndpoint,
                                    hidden     : config.mode == 'create',
                                    iconCls    : 'viz-icon-small-restdataacquisitionendpoint',
                                    items      : [this.configGridDataAcquisitionEndpoint(config)]
                                }]
                    },
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.RESTDataAcquisitionContainer.superclass.constructor.call(this, config);

                this.loadMapCount = 0;
                this.on('beforedestroy', this.onMapBeforeDestroy, this);
                this.on('afterrender', this.onAfterRender, this);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                 : function(config) {
                return [
                    {
                        fieldLabel: $lang('msg_localid'),
                        name: 'LocalId',
                        readOnly: config.mode == 'update' ? true : false,
                        allowBlank: false
                    }, {
                        fieldLabel: $lang('msg_restdataacquisitioncontainer_providertype'),
                        name: 'ProviderType',
                        xtype: 'vizComboRESTDataAcquisitionProvider',
                        allowBlank: false,
                        listeners: {
                            select: function(combo, record, index) {
                                this.updateFormFields(record.data.Id);
                            },
                            scope: this
                        }
                    }, {
                        name: 'KeyDataAcquisitionContainer',
                        hidden: true
                    }, {
                        fieldLabel: $lang('msg_title'),
                        name: 'Title',
                        allowBlank: false
                    }, {
                        fieldLabel: $lang('msg_restdataacquisitioncontainer_url'),
                        name: 'Url',
                        id: this._restUrl,
                        allowBlank: true // This may change to true at load time. Some providers don't require it
                    }, {
                        fieldLabel: $lang('msg_restdataacquisitioncontainer_xpathmeterid'),
                        name: 'XPathMeterId',
                        hidden: true,
                        disable: true,
                        allowBlank: true
                    }, {
                        fieldLabel: $lang('msg_restdataacquisitioncontainer_attributemeterid'),
                        name: 'AttributeMeterId',
                        hidden: true,
                        disable: true,
                        allowBlank: true
                    }, {
                        fieldLabel: $lang('msg_restdataacquisitioncontainer_xpathacquisitiondatetime'),
                        name: 'XPathAcquisitionDateTime',
                        hidden: true,
                        disable: true,
                        allowBlank: true
                    }, {
                        fieldLabel: $lang('msg_restdataacquisitioncontainer_attributeacquisitiondatetime'),
                        name: 'AttributeAcquisitionDateTime',
                        hidden: true,
                        disable: true,
                        allowBlank: true
                    }, {
                        fieldLabel: $lang('msg_restdataacquisitioncontainer_formatacquisitiondatetime'),
                        name: 'FormatAcquisitionDateTime',
                        hidden: true,
                        disable: true,
                        allowBlank: true
                    }, {
                        fieldLabel: $lang('msg_restdataacquisitioncontainer_xpathvalue'),
                        name: 'XPathValue',
                        disable: true,
                        hidden: true,
                        allowBlank: true
                    }, {
                        fieldLabel: $lang('msg_restdataacquisitioncontainer_attributevalue'),
                        name: 'AttributeValue',
                        hidden: true,
                        disable: true,
                        allowBlank: true
                    }, {
                        name: 'CustomData',
                        hidden: true,
                        disable: true,
                        allowBlank: true
                    }, {
                        xtype: 'fieldset',
                        title: $lang('msg_telventdataacquisitioncontainer_location_type'),
                        name: 'LocationTypeFieldSet',
                        id: this._locationTypeFieldSet,
                        style: 'padding-top:5px;',
                        hidden: true,
                        style: 'height:73%',
                        items: [
                            {
                                layout: 'form',
                                border: false,
                                bodyStyle: 'background-color:transparent;padding:4px;margin-left:121px;',
                                defaults: {
                                    style: 'background-color:transparent;',
                                },
                                items: [
                                {
                                    xtype: 'compositefield',
                                    hideLabel: true,
                                    width: 415,
                                    combineErrors: false,
                                    items: [
                                        {
                                            xtype: 'radio',
                                            name: 'LocationType',
                                            checked: true,
                                            inputValue: 0,
                                            listeners: {
                                                check: this.onLocationTypeChange,
                                                scope: this
                                            }
                                        }, {
                                            xtype: 'displayfield',
                                            width: 70,
                                            value: $lang('msg_telventdataacquisitioncontainer_address') + ':',
                                        }, {
                                            xtype: 'textfield',
                                            name: 'Address',
                                            id: this._address,
                                            width: 312,
                                            allowBlank: true
                                        }
                                    ]
                                }, {
                                            xtype: 'compositefield',
                                            hideLabel: true,
                                            width: 415,
                                            combineErrors: false,
                                            items: [
                                                {
                                                    xtype: 'radio',
                                                    name: 'LocationType',
                                                    inputValue: 1,
                                                    listeners: {
                                                        check: this.onLocationTypeChange,
                                                        scope: this
                                                    }
                                                }, {
                                                    xtype: 'displayfield',
                                                    value: $lang('msg_telventdataacquisitioncontainer_latitude') + ':',
                                                    width: 70,
                                                }, {
                                                    xtype: 'viznumberfield',
                                                    width: 101,
                                                    minValue: -90,
                                                    maxValue: 90,
                                                    allowDecimals: true,
                                                    id: this._latitude,
                                                    name: 'Latitude'
                                                }, {
                                                    xtype: 'displayfield',
                                                    value: '/',
                                                    style: 'margin-left:13px'
                                                }, {
                                                    xtype: 'displayfield',
                                                    style: 'margin-left:19px',
                                                    value: $lang('msg_telventdataacquisitioncontainer_longitude') + ':',
                                                    width: 70,
                                                }, {
                                                    xtype: 'viznumberfield',
                                                    width: 101,
                                                    style: 'margin-left:19px',
                                                    minValue: -180,
                                                    maxValue: 180,
                                                    allowDecimals: true,
                                                    id: this._longitude,
                                                    name: 'Longitude'
                                                }]
                                            }, {
                                                xtype: 'compositefield',
                                                hideLabel: true,
                                                width: 415,
                                                combineErrors: false,
                                                items: [
                                                    {
                                                        xtype: 'radio',
                                                        name: 'LocationType',
                                                        hidden: false,
                                                        inputValue: 2,
                                                        listeners: {
                                                            check: this.onLocationTypeChange,
                                                            scope: this
                                                        }

                                                    }, {
                                                        xtype: 'displayfield',
                                                        value: $lang('msg_telventdataacquisitioncontainer_station_id') + ':',
                                                        width: 70,
                                                    }, {
                                                        xtype: 'textfield',
                                                        name: 'StationId',
                                                        id: this._stationId,
                                                        width: 312,
                                                        allowBlank: true
                                                    }
                                                ]
                                        
                                            }
                                        ]
                            }, {
                                xtype: 'panel',
                                style: 'position:absolute; height:46%; margin-top:10px; margin-bottom:0px',
                                name: 'MapPanel',
                                border: false,
                                id: this._mapPanel
                            }
                       ]
                    }, {
                        fieldLabel: $lang('msg_chartscheduler_culture'),
                        name: 'KeyLocalizationCulture',
                        hiddenName: 'KeyLocalizationCulture',
                        autoLoad: config.mode == 'create',
                        xtype: 'vizComboLocalizationCulture'
                    }
                ];
            },

            onMapBeforeDestroy: function () {
                try {
                    if (this._map)
                        this._map.dispose();
                }
                finally {
                    this._map = null;
                }
                return true;
            },

            /**
             * Handler for the afterrender event. Go and Fetch the BingKey and the script for the first load.
             */
            onAfterRender: function () {
                Ext.getCmp(this._mapPanel).addClass(this.loadingClass);

                if (Ext.isEmpty(Viz.form.RESTDataAcquisitionContainer.prototype.bingKey)) {
                    Viz.Services.CoreWCF.Versions_GetItem({
                        success: function(versions) {
                            Viz.Services.EnergyWCF.Map_GetKey({
                                success: function(response) {
                                    Viz.form.RESTDataAcquisitionContainer.prototype.bingKey = response;

                                    var params = {
                                        v: this.version
                                    };
                                    if (this.supportedCultures.indexOf(versions.Culture) >= 0) {
                                        params.mkt = versions.Culture;
                                    }
                                    if (Ext.isSecure) {
                                        params.s = 1;
                                    }

                                    Ext.ux.ScriptMgr.load({
                                        jsonp: true,
                                        scripts: [this.scriptUrl],
                                        params: params,
                                        timeout: 2,
                                        expectResult: false,
                                        scope: this
                                    });
                                },
                                scope: this
                            });
                        },
                        scope: this
                    });
                }
            },

            /**
             * Load the Map.
             */
            loadMap: function () {

                var customDataString = this.form.findField("CustomData").getValue();
                if (!customDataString)
                    return;

                var customData = JSON.parse(customDataString);
                if (this.destroying !== true && this.isDestroyed !== true && typeof Microsoft !== 'undefined' && Microsoft.Maps && Microsoft.Maps.Map) {
                    var me = this;
                    Microsoft.Maps.loadModule('Microsoft.Maps.Themes.BingTheme', {
                        callback: function () {
                            if (me._map)
                                me._map.dispose();
                            me._map = new Microsoft.Maps.Map(Ext.getDom(me._mapPanel), {
                                credentials: Viz.form.RESTDataAcquisitionContainer.prototype.bingKey,
                                enableClickableLogo: false,
                                enableSearchLogo: false,
                                showBreadcrumb: false,
                                showMapTypeSelector: false,
                                showCopyright: false,
                                theme: new Microsoft.Maps.Themes.BingTheme()
                            });
                                
                            if (me.locationIsValid(customData)) {
                                me.applyPushPins(customData);
                            } else {
                                if (!me.isInitialLocationInfo(customData)) {
                                    me.raiseLocationError($lang('error_telventdataacquisitioncontainer_invalid_location'));
                                }
                            }
                                
                            Ext.getCmp(me._mapPanel).removeClass(me.loadingClass);
                                
                        }
                    });
                }
                else {
                    this.loadMapCount += 1;
                    if (this.loadMapCount < 3) {
                        this.loadMap.defer(1000, this);
                           
                    }
                }
            },

            applyPushPins: function(customData) {
                var locations = [];
                if (customData.Type != this.locationType.StationId) {
                    // Id the location type is a Station ID, then there is no location to place on the map
                    var location = new Microsoft.Maps.Location(customData.Latitude, customData.Longitude);
                    var locationPin = new Microsoft.Maps.Pushpin(location,
                    {
                        typeName: Ext.id(),
                    });
                    if (customData.Address) {
                        locationPin.setOptions({ title: customData.Address });
                    }
                    this._map.entities.push(locationPin);
                    locations.push(location);
                }

                if (this.weatherInformationFound(customData)) {
                    var station = new Microsoft.Maps.Location(customData.StationLatitude, customData.StationLongitude);
                    var stationPin = new Microsoft.Maps.Pushpin(station,
                    {
                        typeName: Ext.id(),
                        icon: 'images/rsz_se_pictogram_windfarm_darkest.jpg',
                        width: 25,
                        height: 34
                    });

                    this.stationInfoBox = null;
                    var me = this;
                    Microsoft.Maps.Events.addHandler(stationPin, 'click', function (e) {
                        if (e.targetType == 'pushpin') {
                            if (!me.stationInfoBox) {
                                me.stationInfoBox = new Microsoft.Maps.Infobox(e.target.getLocation(),
                                {
                                    title: $lang('msg_telventdataacquisitioncontainer_station') + ' - ' + customData.StationId,
                                    description: $lang('msg_telventdataacquisitioncontainer_station_description'),
                                    width: 200,
                                    height: 100

                                });
                                me._map.entities.push(me.stationInfoBox);
                            } else {
                                me.stationInfoBox.setOptions({ visible: !me.stationInfoBox.getVisible() });
                            }

                        }
                    });

                    this._map.entities.push(stationPin);
                    locations.push(station);

                } else {
                    this.raiseLocationError($lang('error_telventdataacquisitioncontainer_no_weather_available'));
                }

                this._map.setView({ bounds: Microsoft.Maps.LocationRect.fromLocations(locations), padding: 100 });

                if (locations.length == 1) {
                    this._map.setView({ zoom: 10 });
                }
            },

            onLocationTypeChange : function() {
                var locationType = this.form.getValues()['LocationType'];
                Ext.getCmp(this._address).setDisabled(locationType != this.locationType.Address);
                Ext.getCmp(this._latitude).setDisabled(locationType != this.locationType.LatLon);
                Ext.getCmp(this._longitude).setDisabled(locationType != this.locationType.LatLon);
                Ext.getCmp(this._stationId).setDisabled(locationType != this.locationType.StationId);

                Ext.apply(Ext.getCmp(this._address), { allowBlank: locationType != this.locationType.Address }, {});
                Ext.apply(Ext.getCmp(this._latitude), { allowBlank: locationType != this.locationType.LatLon }, {});
                Ext.apply(Ext.getCmp(this._longitude), { allowBlank: locationType != this.locationType.LatLon }, {});
                Ext.apply(Ext.getCmp(this._stationId), { allowBlank: locationType != this.locationType.StationId }, {});

                if (locationType == this.locationType.Address) {
                    Ext.getCmp(this._latitude).clearInvalid();
                    Ext.getCmp(this._longitude).clearInvalid();
                    Ext.getCmp(this._stationId).clearInvalid();
                }
                else if (locationType == this.locationType.LatLon) {
                    Ext.getCmp(this._address).clearInvalid();
                    Ext.getCmp(this._stationId).clearInvalid();
                } else {
                    Ext.getCmp(this._address).clearInvalid();
                    Ext.getCmp(this._latitude).clearInvalid();
                    Ext.getCmp(this._longitude).clearInvalid();
                }
            },

            raiseLocationError: function (error) {
                var locationType = this.form.getValues()['LocationType'];
                if (locationType == this.locationType.Address) {
                    Ext.getCmp(this._address).markInvalid(error);
                }
                else if (locationType == this.locationType.LatLon) {
                    Ext.getCmp(this._latitude).markInvalid(error);
                    Ext.getCmp(this._longitude).markInvalid(error);
                } else {
                    Ext.getCmp(this._stationId).markInvalid(error);
                }
            },


            /**
             * The configuration for the grid DataAcquisitionEndpoint
             * @return {object}
             */
            configGridDataAcquisitionEndpoint : function(config) {
                return {
                    id                          : this._gridDataAcquisitionEndpoint,
                    xtype                       : 'vizGridRESTDataAcquisitionEndpoint',
                    loadMask                    : true,
                    store                       : this.storeDataAcquisitionEndpoint,
                    KeyDataAcquisitionContainer : config.entity ? config.entity.KeyDataAcquisitionContainer : null,
                    entityContainer             : config.entity,
                    border                      : true,
                    listeners                   : {
                        beforedeleterecord : this.onDeletedRecordTab,
                        scope              : this
                    }
                };
            },

            /**
             * Handler for the columns grid delete.
             */
            onDeletedRecordTab                : function() {
                this.deleteSelectedRowsFromGrid(this._gridDataAcquisitionEndpoint);
                return false;
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                      : function(form, item, extraParams) {
                if (this.storeDataAcquisitionEndpoint.hasLoaded) {
                    this.storeDataAcquisitionEndpoint.save();
                }
                var type = this.getProviderType(item.ProviderType);
                if (type == 'TelventRESTDataAcquisitionProvider') {
                    if (item.LocationType[this.locationType.Address]) {
                        item.CustomData = JSON.stringify({
                            Type: this.locationType.Address,
                            Address: item.Address,
                        });
                    }
                    else if (item.LocationType[this.locationType.LatLon]) {
                        item.CustomData = JSON.stringify({
                            Type: this.locationType.LatLon,
                            Latitude: item.Latitude,
                            Longitude: item.Longitude
                        });
                    }
                    else if (item.LocationType[this.locationType.StationId]) {
                        item.CustomData = JSON.stringify({
                            Type: this.locationType.StationId,
                            StationId: item.StationId,
                        });
                    }
                }
                    
                return true;
            },
            /**
             * Handler for updating fiels based on the form values.
             * @param {Viz.form.FormPanel} the form panel
             * @param {Object} values
             */
            onAfterLoadValues                 : function(formpanel, values) {
                this.updateFormFields(values.ProviderType);
            },

            /**
             * Handler for save success.
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                     : function(form, action) {
                Viz.form.RESTDataAcquisitionContainer.superclass.onSaveSuccess.apply(this, arguments);

                var tabpanel = this.getTabContainer();
                tabpanel.unhideTabStripItem(this._tabEndpoint);

                var grid = Ext.getCmp(this._gridDataAcquisitionEndpoint);
                grid.KeyDataAcquisitionContainer = grid.store.serviceParams.KeyDataAcquisitionContainer = this.entity.KeyDataAcquisitionContainer;

                if (this.closeonsave !== true) {
                    if (this.getProviderType(this.form.findField("ProviderType").getValue()) == 'TelventRESTDataAcquisitionProvider') {
                        this.updateTelventFields();
                    }
                }
                

            },

            updateFormFields                  : function(type) {
                if (!Ext.isEmpty(type)) {
                    type = this.getProviderType(type);
                    this.form.findField('Url').label.update($lang('msg_restdataacquisitioncontainer_url'));

                    this.form.findField('XPathMeterId').setVisible(type == 'XPathRESTDataAcquisitionProvider');
                    this.form.findField('XPathAcquisitionDateTime').setVisible(type == 'XPathRESTDataAcquisitionProvider');
                    this.form.findField('XPathValue').setVisible(type == 'XPathRESTDataAcquisitionProvider');
                    this.form.findField('AttributeMeterId').setVisible(type == 'XPathRESTDataAcquisitionProvider');
                    this.form.findField('AttributeAcquisitionDateTime').setVisible(type == 'XPathRESTDataAcquisitionProvider');
                    this.form.findField('FormatAcquisitionDateTime').setVisible(type == 'XPathRESTDataAcquisitionProvider');
                    this.form.findField('AttributeValue').setVisible(type == 'XPathRESTDataAcquisitionProvider');
                    this.form.findField('KeyLocalizationCulture').setVisible(type == 'XPathRESTDataAcquisitionProvider');
                    var locationTypeFieldSet = Ext.getCmp(this._locationTypeFieldSet);
                    locationTypeFieldSet.setVisible(type == 'TelventRESTDataAcquisitionProvider');
                    Ext.apply(Ext.getCmp(this._restUrl), { allowBlank: (type == 'TelventRESTDataAcquisitionProvider') }, {});
                    this.form.findField('Url').setVisible(type != 'TelventRESTDataAcquisitionProvider');

                    switch (type) {
                        case 'WolframAlphaRESTDataAcquisitionProvider' :
                            this.form.findField('Url').setValue('http://www.wolfram.com');
                            this.form.findField('Title').setValue('http://www.wolfram.com');
                            break;

                        case 'WeatherDotComRESTDataAcquisitionProvider' :
                            this.form.findField('Url').setValue('http://www.weather.com');
                            this.form.findField('Title').setValue('http://www.weather.com');
                            break;
                        case 'TelventRESTDataAcquisitionProvider':
                            this.updateTelventFields();
                            this.doLayout();
                            break;

                        case 'XPathRESTDataAcquisitionProvider' :
                            // this.form.findField('Url').setValue('');
                            break;

                    }
                }
            },
            getProviderType: function (providerValue) {
                var type = providerValue;
                if (type.contains(","))
                    type = type.substring(0, type.indexOf(","));
                type = type.replace("Vizelia.FOL.ConcreteProviders.", "");
                return type;
            },
            updateTelventFields: function() {
                var customData = this.form.findField('CustomData').getValue();
                if (!customData) {
                    var initialAddress = this.form.findField('Url').getValue(); //Use this as initial to migrate old data that used to be stored in URL
                    customData = JSON.stringify( {
                        Type: this.locationType.Address, //Default to address type
                        Address: initialAddress || "",
                        Latitude: '',
                        Longitude: '',
                        StationId: '',
                        StationLatitude: '',
                        StationLongitude: ''
                    });
                    this.form.findField('CustomData').setValue(customData);
                }
                if (customData.indexOf('{"Type":') == 0) {
                    var locationInfo = JSON.parse(customData);
                    this.form.findField('LocationType').setValue(locationInfo.Type);
                    if (locationInfo.Type == this.locationType.Address) {
                        this.form.findField(this._address).setValue(locationInfo.Address);
                        this.form.findField(this._latitude).setValue(locationInfo.Latitude);
                        this.form.findField(this._longitude).setValue(locationInfo.Longitude);
                        this.form.findField(this._stationId).setValue(locationInfo.StationId);

                    } else if (locationInfo.Type == this.locationType.LatLon) {
                        this.form.findField(this._address).setValue('');
                        this.form.findField(this._latitude).setValue(locationInfo.Latitude);
                        this.form.findField(this._longitude).setValue(locationInfo.Longitude);
                        this.form.findField(this._stationId).setValue(locationInfo.StationId);

                    } else if (locationInfo.Type == this.locationType.StationId) {
                        this.form.findField(this._address).setValue('');
                        this.form.findField(this._latitude).setValue('');
                        this.form.findField(this._longitude).setValue('');
                        this.form.findField(this._stationId).setValue(locationInfo.StationId);
                    }
                    this.onLocationTypeChange();
                }
                this.loadMap();
                
            },
            locationIsValid: function(locationInfo) {
                if (locationInfo.Type == this.locationType.Address) {
                    if (!locationInfo.Address || locationInfo.Latitude == null || locationInfo.Longitude == null) {
                        return false;
                    }
                }
                if (locationInfo.Type == this.locationType.LatLon) {
                    if (locationInfo.Latitude == null || locationInfo.Longitude == null) {
                        return false;
                    }
                }
                if (locationInfo.Type == this.locationType.StationId) {
                    if (!locationInfo.StationId || !locationInfo.StationLatitude || !locationInfo.StationLongitude) {
                        return false;
                    }
                }
                return true;
            },
            weatherInformationFound: function (locationInfo) {
                return locationInfo.StationLatitude && locationInfo.StationLongitude;
            },
            isInitialLocationInfo: function (locationInfo) {
                return locationInfo.Type == this.locationType.Address && locationInfo.Address === '';

            }
            
        });

Ext.reg('vizFormRESTDataAcquisitionContainer', Viz.form.RESTDataAcquisitionContainer);
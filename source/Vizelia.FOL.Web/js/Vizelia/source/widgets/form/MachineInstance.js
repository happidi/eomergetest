﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.MachineInstance
 * @extends Ext.FormPanel.
 */
Viz.form.MachineInstance = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandlerCreate : Viz.Services.EnergyWCF.MachineInstance_FormCreate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.MachineInstance.superclass.constructor.call(this, config);

            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral : function(config) {
                return [{
                            name       : 'MachinePurpose',
                            fieldLabel : $lang('msg_machineinstance_machinepurpose'),
                            allowBlank : false,
                            readOnly   : true,
                            value      : 'Vizelia.FOL.EnergyAggregatorService'
                        }, {
                            name       : 'EnvironmentName',
                            fieldLabel : $lang('msg_machineinstance_environmentname'),
                            allowBlank : false,
                            vtype      : 'virtualfilePath'
                        }, {
                            fieldLabel   : $lang('msg_machineinstance_size'),
                            name         : 'Size',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.MachineInstanceSize,Vizelia.FOL.Common'
                        }]
            }
        });

Ext.reg('vizFormMachineInstance', Viz.form.MachineInstance);
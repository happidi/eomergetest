﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.MeterData
 * @extends Ext.FormPanel.
 */

Viz.form.MeterData = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor         : function(config) {
                this.isManualIntervalSeconds = config.entity.ManualInputTimeInterval == Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Seconds;
                this.isManualIntervalMinutes = config.entity.ManualInputTimeInterval == Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Minutes;
                this.isManualIntervalHours = config.entity.ManualInputTimeInterval == Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Hours;
                this.isManualIntervalDays = config.entity.ManualInputTimeInterval == Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Days;
                this.isManualIntervalMonths = config.entity.ManualInputTimeInterval == Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Months;
                this.isManualIntervalYears = config.entity.ManualInputTimeInterval == Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Years;

                this._fieldAcquisitionHourOrMinute = Ext.id();
                this._fieldAcquisitionDay = Ext.id();
                this._fieldAcquisitionMonthYear = Ext.id();

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.MeterData_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.MeterData_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.MeterData_FormUpdate,
                    items                : this.configFormGeneral(config),
                    entityType           : 'MeterData',
                    closeonsave          : false,
                    extraButtons         : [{
                                text    : $lang('msg_validate'),
                                width   : 80,
                                hidden  : true,
                                cls     : 'x-toolbar-standardbutton',
                                iconCls : 'viz-icon-small-metervalidationrule',
                                handler : this.onValidateMeterData,
                                scope   : this
                            }]
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.MeterData.superclass.constructor.call(this, config);
            },
            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral   : function(config) {
                return [{
                            name   : 'KeyMeterData',
                            hidden : true
                        }, {
                            fieldLabel     : $lang('msg_meterdata_acquisitiondatetime'),
                            name           : 'AcquisitionDateTime',
                            xtype          : 'vizDateTime',
                            hidden         : !this.isManualIntervalSeconds,
                            hideTime       : false,
                            displaySeconds : true,
                            allowBlank     : !this.isManualIntervalSeconds,
                            hasSwitch      : false
                        }, {
                            fieldLabel     : this.isManualIntervalMinutes ? $lang('msg_meterdata_acquisitionminutes') : $lang('msg_meterdata_acquisitionhours'),
                            name           : 'AcquisitionHourOrMinute',
                            id             : this._fieldAcquisitionHourOrMinute,
                            xtype          : 'vizDateTime',
                            hidden         : !(this.isManualIntervalMinutes || this.isManualIntervalHours),
                            hideTime       : false,
                            displaySeconds : false,
                            allowBlank     : !(this.isManualIntervalMinutes || this.isManualIntervalHours),
                            hasSwitch      : false,
                            timeConfig     : this.isManualIntervalMinutes ? {
                                increment : 1,
                                minValue  : '00:00',
                                maxValue  : '00:59'
                            } : {
                                increment : 60,
                                minValue  : '00:00',
                                maxValue  : '23:00'
                            }
                        }, {
                            fieldLabel     : $lang('msg_meterdata_acquisitionday'),
                            name           : 'AcquisitionDay',
                            id             : this._fieldAcquisitionDay,
                            xtype          : 'vizDateTime',
                            hidden         : !this.isManualIntervalDays,
                            hideTime       : true,
                            displaySeconds : false,
                            allowBlank     : !this.isManualIntervalDays,
                            hasSwitch      : false
                        }, {
                            xtype       : 'vizMonthYear',
                            name        : 'AcquisitionMonthYear',
                            id          : this._fieldAcquisitionMonthYear,
                            fieldLabel  : this.isManualIntervalMonths ? $lang('msg_meterdata_acquisitionmonth') : $lang('msg_meterdata_acquisitionyear'),
                            hideMonth   : !this.isManualIntervalMonths,
                            hidden      : !(this.isManualIntervalYears || this.isManualIntervalMonths),
                            monthConfig : {
                                allowBlank : !this.isManualIntervalMonths,
                                width      : 115
                            },
                            yearConfig  : {
                                allowBlank : !(this.isManualIntervalYears || this.isManualIntervalMonths),
                                value      : new Date().getFullYear()
                            }
                        }, {
                            xtype            : 'viznumberfield',
                            fieldLabel       : $lang('msg_meterdata_value'),
                            name             : 'Value',
                            decimalPrecision : 5,
                            allowBlank       : false
                        }, {
                            fieldLabel   : $lang('msg_meterdata_validity'),
                            name         : 'Validity',
                            allowBlank   : false,
                            disabled     : !$authorized('@@@@ Meter Data - Validator'),
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.MeterDataValidity,Vizelia.FOL.Common',
                            sortByValues : true,
                            displayIcon  : true,
                            value        : Viz.ApplicationSettings.DefaultMeterDataValidity
                        }];
            },

            /**
             * Handler for save success. We clear the form to input new values
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess       : function(panel, action) {
                this.form.findField('KeyMeterData').setValue('');
                this.mode = 'create';
                return true;
            },

            onBeforeSave        : function(form, item, extraParams) {
                if (this.mode == 'create') {
                    // if (typeof(item.KeyMeter) == "string")
                    // item.KeyMeter = item.KeyMeter.replace('#', '') - 0;
                    item.KeyMeterData = null;
                }
                // we have to put some bogus values otherwise WCF cannot deserialize the object.
                if (this.mode == 'updatebatch') {
                    item.AcquisitionDateTime = new Date();
                    item.Value = 0;
                }

                if (this.successFillDateTimeFields || this.mode == 'create') {
                    if (this.isManualIntervalMinutes) {
                        item.AcquisitionDateTime = new Date(item.AcquisitionHourOrMinute.getFullYear(), item.AcquisitionHourOrMinute.getMonth(), item.AcquisitionHourOrMinute.getDate(), item.AcquisitionHourOrMinute.getHours(), item.AcquisitionHourOrMinute.getMinutes());
                    }
                    if (this.isManualIntervalHours) {
                        item.AcquisitionDateTime = new Date(item.AcquisitionHourOrMinute.getFullYear(), item.AcquisitionHourOrMinute.getMonth(), item.AcquisitionHourOrMinute.getDate(), item.AcquisitionHourOrMinute.getHours());
                    }
                    if (this.isManualIntervalDays) {
                        item.AcquisitionDateTime = new Date(item.AcquisitionDay.getFullYear(), item.AcquisitionDay.getMonth(), item.AcquisitionDay.getDate(), Viz.Const.Date.MeterDataHoursOffset);
                    }
                    if (this.isManualIntervalMonths || this.isManualIntervalYears) {
                        item.AcquisitionDateTime = item.AcquisitionMonthYear;
                    }
                }
            },

            /**
             * Validate the MeterData
             */
            onValidateMeterData : function() {
                var thisForm = Ext.getCmp(this._formId);
                var form = thisForm.getForm();

                if (form.isValid()) {
                    this.el.mask();
                    var item = form.getFieldValues();
                    this.entity = thisForm.entity || {};
                    item = Ext.apply(thisForm.entity, item);
                    Viz.Services.EnergyWCF.MeterData_Validate({
                                item    : item,
                                success : function(response) {
                                    this.el.unmask();
                                    var message = $lang('msg_ok');
                                    var icon = Ext.MessageBox.INFO;
                                    if (response.length > 0) {
                                        message = "";
                                        icon = Ext.MessageBox.ERROR;
                                        Ext.each(response, function(instance) {
                                                    message += instance.Description;
                                                }, this);
                                    }

                                    Ext.Msg.show({
                                                title   : $lang('msg_validate'),
                                                msg     : message,
                                                buttons : Ext.Msg.OK,
                                                icon    : icon
                                            });
                                },
                                scope   : this
                            });
                }

            },

            onAfterLoadValues   : function(form, values) {
                if (values.AcquisitionDateTime) {
                    Ext.getCmp(this._fieldAcquisitionHourOrMinute).setValue(values.AcquisitionDateTime);
                    Ext.getCmp(this._fieldAcquisitionDay).setValue(values.AcquisitionDateTime);
                    Ext.getCmp(this._fieldAcquisitionMonthYear).setValue(values.AcquisitionDateTime);
                    this.successFillDateTimeFields = true;
                }
                else if (this.mode == 'create') {
                    Ext.getCmp(this._fieldAcquisitionMonthYear).setValue(new Date());
                }
            }
        });
Ext.reg('vizFormMeterData', Viz.form.MeterData);
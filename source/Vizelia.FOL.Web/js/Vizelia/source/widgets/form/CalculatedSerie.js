﻿Ext.namespace('Viz.form');
/**
* @class Viz.form.CalculatedSerie
* @extends Ext.FormPanel.
*/
Viz.form.CalculatedSerie = Ext.extend(Viz.form.FormPanel, {

    /**
    * @cfg {Viz.BusinessEntity.Chart} entity The chart entity the dataserie belong to.
    */

    /**
    * Ctor.
    * @param {Object} config The configuration options
    */
    constructor: function (config) {
        this._formulaId = Ext.id();
        this._nameId = Ext.id();
        config = config || {};
        var defaultConfig = {
            mode_load: 'remote',
            serviceHandler: Viz.Services.EnergyWCF.CalculatedSerie_FormLoad,
            serviceHandlerCreate: Viz.Services.EnergyWCF.CalculatedSerie_FormCreate,
            serviceHandlerUpdate: Viz.Services.EnergyWCF.CalculatedSerie_FormUpdate,
            items: this.configFormGeneral(config),
            labelWidth: 200,
            closeonsave: true
            /*
            * , tbar : [{ id : this._combometer_id, xtype : 'vizComboChartAvailableDataSerieName', KeyChart : config.entity.KeyChart, width : 350 }]
            */
        };
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);

        Viz.form.CalculatedSerie.superclass.constructor.call(this, config);
    },

    /**
    * TODO : define here correct fields General form configuration
    * @param {Object} config The configuration options of the form
    */
    configFormGeneral: function (config) {
        return [{
            name: 'KeyCalculatedSerie',
            hidden: true
        }, {
            fieldLabel: $lang('msg_localid'),
            name: 'LocalId',
            allowBlank: false,
            listeners: {
                change: function (field, newValue, oldValue) {
                    Ext.getCmp(this._nameId).setValue(newValue);
                },
                scope: this
            }
        }, {
            fieldLabel: $lang('msg_name'),
            name: 'Name',
            id: this._nameId,
            allowBlank: false
        }, {
            fieldLabel: $lang('msg_dataserie_unit'),
            name: 'Unit'
        }, {
            fieldLabel: $lang('msg_calculatedserie_order'),
            name: 'Order',
            xtype: 'vizSpinnerField',
            minValue: 1,
            allowBlank: false
        }, {
            fieldLabel: $lang('msg_calculatedserie_hideusedseries'),
            name: 'HideUsedSeries',
            xtype: 'vizcheckbox',
            flex: 1
        }, {
            fieldLabel: $lang('msg_playlist_culture'),
            name: 'KeyLocalizationCulture',
            hiddenName: 'KeyLocalizationCulture',
            autoLoad: config.mode == 'create',
            xtype: 'vizComboLocalizationCulture'
        }, {
            fieldLabel: $lang('msg_meter_classification'),
            xtype: 'viztreecomboClassificationItemMeter',
            name: 'KeyClassificationItem',
            originalDisplayValue: 'ClassificationItemLongPath',
            rootVisible: true
        }, /*
                             * { xtype : 'displayfield', fieldLabel : $img('viz-icon-small-information') + ' ' + $lang('msg_form_info'), value : $lang('msg_calculatedserie_usepoint') }
                             */{
                             id: this._formulaId,
                             fieldLabel: $lang('msg_calculatedserie_formula'),
                             name: 'Formula',
                             xtype: 'textarea',
                             anchor: Viz.Const.UI.Anchor + ' -220',
                             // enableKeyEvents : true,
                             allowBlank: false,
                             /*
                             * plugins : [{ ptype : 'vizRemoteValidator', serviceWCF : Viz.Services.EnergyWCF.CalculatedSerie_CheckFormula, params : { KeyChart : config.entity.KeyChart } }],
                             */
                             listeners: {
                                 render: function () {
                                     Ext.get(this._formulaId).on('contextmenu', function (e) {
                                         this.displayMenu(e);
                                     }, this);
                                 },
                                 /*
                                 * keypress : function(text, event) { // we prevent from typing commas. decimal separator needs to be . if (event.getKey() == 44) { return false; } },
                                 */
                                 scope: this
                             }
                         }, {
                             fieldLabel: $lang('msg_calculatedserie_runonlyinmaintimeline'),
                             name: 'RunOnlyInMainTimeline',
                             xtype: 'vizcheckbox',
                             checked: false
                         }];
    },

    /**
    * Display the formula menu
    * @param {} e
    */
    displayMenu: function (e) {
        var seriesStore = new Viz.store.ChartAvailableDataSerieLocalId({
            KeyChart: this.entity.KeyChart,
            includeExisting: true
        });

        var menuConfig = {
            defaults: {
                hideOnClick: false
            },
            items: [{
                text: $lang('msg_calculatedserie_function'),
                iconCls: 'viz-icon-small-calculatedserie',
                menu: {
                    xtype: 'menustore',
                    store: {
                        xtype: 'vizStoreEnum',
                        serviceParams: {
                            enumTypeName: 'Vizelia.FOL.BusinessEntities.AlgebricFunction,Vizelia.FOL.Common'
                        }
                    },
                    createMenuItemFromRecord: function (record) {
                        if (record.data.MsgCode == 'msg_enum_algebricfunction_none') {
                            return null
                        }
                        else {
                            return {
                                text: record.data.Label,
                                iconCls: record.data.IconCls,
                                handler: this.addFormulaValue.createDelegate(this, [record.data.Id.toLowerCase() + '(' + ')'])
                            }
                        }
                    } .createDelegate(this)
                }
            }, {
                text: $lang('msg_calculatedserie_functiononeparameter'),
                iconCls: 'viz-icon-small-calculatedserie',
                menu: {
                    xtype: 'menustore',
                    store: {
                        xtype: 'vizStoreEnum',
                        serviceParams: {
                            enumTypeName: 'Vizelia.FOL.BusinessEntities.OneParameterFunction,Vizelia.FOL.Common'
                        }
                    },
                    createMenuItemFromRecord: function (record) {
                        return {
                            text: record.data.Label,
                            iconCls: record.data.IconCls,
                            handler: this.addFormulaValue.createDelegate(this, [record.data.Id.toLowerCase() + '(' + ' , 0)'])
                        }
                    } .createDelegate(this)
                }
            }, {
                text: $lang('msg_enum_mathematicoperator'),
                iconCls: 'viz-icon-small-calculatedserie',
                menu: {
                    xtype: 'menustore',
                    store: {
                        xtype: 'vizStoreEnum',
                        serviceParams: {
                            enumTypeName: 'Vizelia.FOL.BusinessEntities.DataSerieProjection,Vizelia.FOL.Common'
                        }
                    },
                    createMenuItemFromRecord: function (record) {
                        return {
                            text: record.data.Label,
                            iconCls: record.data.IconCls,
                            handler: this.addFormulaValue.createDelegate(this, [record.data.Id.toLowerCase() + '(' + ')'])
                        }
                    } .createDelegate(this)
                }
            }, {
                text: $lang('msg_localization'),
                iconCls: 'viz-icon-small-localization',
                handler: this.addLocalization.createDelegate(this)
            }, {
                text: $lang('msg_chart_dataseries'),
                iconCls: 'viz-icon-small-dataserie',
                menu: {
                    xtype: 'menustore',
                    iconCls: 'viz-icon-small-dataserie',
                    store: seriesStore,
                    createMenuItemFromRecord: function (record) {
                        return {
                            text: record.data.MsgCode,
                            iconCls: record.data.IconCls || 'viz-icon-small-dataserie',
                            handler: this.addFormulaValue.createDelegate(this, ['(' + record.data.Value + ')'])
                        }
                    } .createDelegate(this)
                }
            }, {
                text: $lang('msg_dataserie_classification'),
                iconCls: 'viz-icon-small-classificationitem',
                handler: function (menu, events) {
                    var treeId = Ext.id();
                    var win = new Ext.Window({
                        width: 300,
                        height: 400,
                        layout: 'fit',
                        title: $lang('msg_dataserie_classification'),
                        modal: true,
                        items: {
                            id: treeId,
                            header: false,
                            border: false,
                            xtype: 'vizTreeMeterClassification',
                            KeyChart: events.ctrlKey ? null : this.entity.KeyChart,
                            existing: true
                        },
                        buttons: [{
                            text: $lang('msg_add'),
                            iconCls: 'viz-icon-small-add',
                            handler: function () {
                                var tree = Ext.getCmp(treeId);
                                var node = tree.getSelectionModel().getSelectedNode();
                                if (node != null) {
                                    win.close();
                                    if (node.isRoot)
                                        this.addFormulaValue('#Root / ' + node.attributes.entity.LocalId + '#')
                                    else
                                        this.addFormulaValue('#' + node.attributes.entity.LocalIdPath + '#')
                                }
                            },
                            scope: this
                        }]
                    });
                    win.show();
                },
                scope: this
            }]
        };
        var menu = new Ext.menu.Menu(menuConfig);
        menu.showAt(e.xy);
    },

    /**
    * Add the current tag to the formula
    * @param {} value
    */
    addFormulaValue: function (value) {
        var formula = Ext.getCmp(this._formulaId);
        formula.insertAtCursor(value);
        // var currentValue = formula.getValue();
        // formula.setValue((currentValue ? currentValue : '') + ' ' + value);
    },

    /**
    * Add localization tags
    */
    addLocalization: function () {
        var formula = Ext.getCmp(this._formulaId);
        formula.insertAtCursor('%%');
    }

});

Ext.reg('vizFormCalculatedSerie', Viz.form.CalculatedSerie);
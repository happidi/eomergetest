﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.FlipCardEntry
 * @extends Ext.FormPanel.
 */
Viz.form.FlipCardEntry = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Playlist} entity The Playlist entity the playlist page belongs to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.FlipCardEntry_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.FlipCardEntry_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.FlipCardEntry_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.FlipCardEntry.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyFlipCardEntry',
                            hidden : true
                        }, {
                            fieldLabel: $lang('msg_localid'),
                            allowBlank: false,
                            name: 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_order'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : false,
                            name       : 'Order',
                            minValue   : 1
                        }, {
                            fieldLabel           : $lang('msg_portlet'),
                            name                 : 'KeyEntity',
                            hiddenName           : 'KeyEntity',
                            xtype                : 'viztreecomboPortlet',
                            originalDisplayValue : 'EntityLongPath',
                            allowBlank           : false,
                            listeners            : {
                                'select' : {
                                    fn    : function(tree, node) {
                                        if (!node.leaf || !node.attributes.entity) {
                                            tree.setRawValue("");
                                            this.entity.KeyEntity = null;
                                            this.entity.TypeEntity = null;
                                        }
                                        else {
                                            var type = Viz.getEntityType(node.attributes.entity.__type);
                                            if (type != 'Vizelia.FOL.BusinessEntities.FlipCard') {
                                                this.entity.TypeEntity = type;
                                                this.entity.KeyEntity = node.id;
                                            }
                                            else {
                                                tree.setRawValue("");
                                                this.entity.KeyEntity = null;
                                                this.entity.TypeEntity = null;
                                            }
                                        }
                                    },
                                    scope : this
                                }
                            }
                        }];
            }

        });

Ext.reg('vizFormFlipCardEntry', Viz.form.FlipCardEntry);
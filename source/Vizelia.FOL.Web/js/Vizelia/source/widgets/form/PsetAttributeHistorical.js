﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.PsetAttributeHistorical
 * @extends Ext.FormPanel.
 */
Viz.form.PsetAttributeHistorical = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.CoreWCF.PsetAttributeHistorical_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.PsetAttributeHistorical_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.PsetAttributeHistorical_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : false,
                    modal                : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.PsetAttributeHistorical.superclass.constructor.call(this, config);
            },
            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                this.psetDataType = config.initialConfigFromGrid.common.psetDataType;
                if (this.psetDataType == "hourly") {
                    var timeIncrement = 60;
                }
                else {
                    var timeIncrement = 15;
                }
                return [{
                            name   : 'KeyPsetAttributeHistorical',
                            hidden : true

                        }, {
                            name         : 'year',
                            xtype        : 'vizSpinnerField',
                            fieldLabel   : $lang('msg_psetattributehistorical_attributeacquisitionyear'),
                            defaultValue : (new Date()).getFullYear(),
                            value        : (new Date()).getFullYear(),
                            hidden       : (!((this.psetDataType == "monthly") || (this.psetDataType == "yearly"))),
                            disabled     : (!((this.psetDataType == "monthly") || (this.psetDataType == "yearly")))
                        }, {
                            name       : 'month',
                            xtype      : 'vizCombo',
                            store      : Date.monthNames,
                            hidden     : (!(this.psetDataType == "monthly")),
                            disabled   : (!(this.psetDataType == "monthly")),
                            pageSize   : null,
                            listHeight : null,
                            fieldLabel : $lang('msg_psetattributehistorical_attributeacquisitionmonth')
                        }, {
                            fieldLabel    : $lang('msg_psetattributehistorical_attributeacquisitiondatetime'),
                            name          : 'AttributeAcquisitionDateTime',
                            hideTime      : (this.psetDataType == "daily"),
                            hasTime       : (!(this.psetDataType == "daily")),
                            xtype         : 'vizDateTime',
                            allowBlank    : false,
                            hasSwitch     : false,
                            timeIncrement : timeIncrement,
                            hidden        : ((this.psetDataType == "monthly") || (this.psetDataType == "yearly")),
                            disabled      : ((this.psetDataType == "monthly") || (this.psetDataType == "yearly"))
                        }, {
                            xtype            : 'numberfield',
                            fieldLabel       : $lang('msg_psetattributehistorical_attributevalue'),
                            name             : 'AttributeValue',
                            allowBlank       : false,
                            decimalPrecision : 9
                        }, {
                            fieldLabel : $lang('msg_psetattributehistorical_attributecomment'),
                            name       : 'AttributeComment'
                        }];
            },

            onAfterLoadValues : function(form, values) {
                if (form.mode == 'update') {
                    values.year = values.AttributeAcquisitionDateTime.getFullYear();
                    values.month = Date.monthNames[values.AttributeAcquisitionDateTime.getMonth()];
                    this.getForm().setValues(values);
                }

            },
            /**
             * Handler before save
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave      : function(form, item, extraParams) {

                if (form.psetDataType == 'monthly') {
                    var month = Date.monthNames.indexOf(item.month) + 1;
                    var year = item.year;
                    var date = new Date();
                    date.setFullYear(year);
                    date.setMonth(month - 1);
                    date.setDate(1);
                    date.setHours(0);
                    date.setMinutes(0);
                    date.setSeconds(0);
                    date.setMilliseconds(0);
                    item.AttributeAcquisitionDateTime = date;
                }

                if (form.psetDataType == 'yearly') {

                    var year = item.year;

                    var date = new Date();
                    date.setFullYear(year);
                    date.setMonth(0);
                    date.setDate(1);
                    date.setHours(0);
                    date.setMinutes(0);
                    date.setSeconds(0);
                    date.setMilliseconds(0);
                    item.AttributeAcquisitionDateTime = date;
                }

                return true;
            },

            /**
             * Handler for save success. We clear the form to input new values
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess     : function(panel, action) {
                // we do this to allow batch pset historical data insertion.
                if (this.mode == "create") {
                    this.form.findField('KeyPsetAttributeHistorical').setValue('');
                    this.mode = 'create';
                }
                return true;
            }
        });
Ext.reg('vizFormPsetAttributeHistorical', Viz.form.PsetAttributeHistorical);

﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.DateTime
 * @extends Ext.form.CompositeField A custom datetime field component..
 */
Viz.form.DateTime = Ext.extend(Ext.form.CompositeField, {
            /**
             * @cfg {String} all day text.
             */
            alldayText              : $lang('msg_calendarevent_isallday'),

            /**
             * @cfg {Boolean} hideTime either <tt>true</tt> to hide time component or false to disable it. <tt>true</tt> by default.
             */
            hideTime                : false,

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor             : function(config) {
                this.allday_id = Ext.id();
                config = config || {};
                var defaultConfig = {

                    /**
                     * @cfg {Boolean} hasSwitch indicates if the component exposes a checkbox to enable/disable time field. <tt>false</tt> by default.
                     */
                    hasSwitch        : false,
                    /**
                     * @cfg {String} the name of the field indicating if the range is an all day range.
                     */
                    switchName       : 'IsAllDay',

                    displaySeconds   : false,

                    hasTimeZone      : false,

                    TimeZoneReadOnly : true

                };
                var forcedConfig = {
                    style : 'background-color:transparent'
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                config.width = config.hasSwitch ? 450 : 200;

                /**
                 * @cfg {Object} dateConfig Config for DateField constructor.
                 */
                var dateConfig = {
                    xtype           : 'datefield',
                    width           : 115,
                    enableKeyEvents : true,
                    fieldLabel      : $lang('msg_datatype_date'),
                    format          : Viz.getLocalizedDateFormat(), // Ext.form.DateField.prototype.format,//
                    allowBlank      : config.allowBlank || false,
                    listeners       : {
                        select : this.onSelectDate,
                        change : this.onSelectDate,
                        keyup  : this.onKeyUpDate,
                        scope  : this
                    }
                };

                Ext.apply(dateConfig, {
                            id   : Ext.isEmpty(config['id']) ? this.allday_id + 'date' : config['id'] + 'date',
                            name : config['name'] + 'date'
                        });

                /**
                 * @cfg {Object} timeConfig Config for TimeField constructor.
                 */
                var timeConfig = {
                    xtype           : 'timefield',
                    width           : 90,
                    enableKeyEvents : true,
                    fieldLabel      : $lang('msg_datatype_time'),
                    hidden          : config.hideTime,
                    format          : Viz.getLocalizedTimeFormat(config.displaySeconds),// Ext.form.TimeField.prototype.format,
                    allowBlank      : config.allowBlank || config.hideTime || false,
                    listeners       : {
                        select : this.onSelectTime,
                        change : this.onSelectTime,
                        keyup  : this.onKeyUpTime,
                        scope  : this
                    }
                };

                Ext.apply(timeConfig, {
                            id   : Ext.isEmpty(config['id']) ? this.allday_id + 'time' : config['id'] + 'time',
                            name : config['name'] + 'time'
                        });

                /**
                 * @cfg {Object} alldayConfig Config for AllDay field constructor.
                 */
                var alldayConfig = {
                    id         : this.allday_id,
                    xtype      : 'checkbox',
                    width      : 150,
                    name       : config.hasSwitch ? config.switchName : this.allday_id,
                    boxLabel   : this.alldayText,
                    inputValue : true,
                    hidden     : !config.hasSwitch,
                    hideLabel  : !config.hasSwitch,
                    listeners  : {
                        check : this.onCheckAllDay,
                        scope : this
                    },
                    scope      : this
                };

                var timezoneCombo = {
                    fieldLabel : $lang('msg_chart_timezone'),
                    name       : 'TimeZoneId',
                    allowBlank : false,
                    xtype      : 'vizComboTimeZone',
                    hidden     : !config.hasTimeZone,
                    readOnly   : config.TimeZoneReadOnly,
                    width      : 250
                };

                Ext.apply(timeConfig, config.timeConfig);
                Ext.apply(dateConfig, config.dateConfig);
                Ext.apply(alldayConfig, config.alldayConfig);
                Ext.apply(timezoneCombo, config.timezoneCombo);
                this.items = [dateConfig, timeConfig, alldayConfig, timezoneCombo];
                Viz.form.DateTime.superclass.constructor.call(this, config);
                this.addEvents(
                        /**
                         * @event checkallday Fires when allday checkbox changes value.
                         * @param {Viz.form.DateTime} this
                         * @param {Boolean} checked The value of allday checkbox.
                         */
                        'checkallday',
                        /**
                         * @event selectdate Fires when a date is selected via the date picker.
                         * @param {Viz.form.DateTime} this
                         * @param {Ext.form.DateField} comp
                         * @param {Date} value
                         */
                        'selectdate',
                        /**
                         * @event selectdate Fires when a date is selected via the date picker.
                         * @param {Viz.form.DateTime} this
                         * @param {Ext.form.TimeField} comp
                         * @param {Object} record
                         * @param {Number} index
                         * @param {Date} value
                         */
                        'selecttime',

                        /**
                         * @event selectdate Fires when a keyup is generated on either time or date cmp
                         * @param {Viz.form.DateTime} this
                         */
                        'keyup');
            },

            /**
             * @private
             */
            onRender                : function(ct, position) {
                Viz.form.DateTime.superclass.onRender.apply(this, arguments);
                this.df = this.items.items[0];
                this.tf = this.items.items[1];
                this.timeZoneField = this.items.items[3];
            },

            /**
             * @private Handler for AllDay checkbox.
             * @param {Object} comp
             * @param {Boolean} value
             */
            onCheckAllDay           : function(comp, value) {
                if (this.getValue() == null)
                    this.setValue(new Date());
                this.disableTime(value);
                this.fireEvent('checkallday', this, value);
            },

            /**
             * @private Handler for select date.
             * @param {Ext.form.DateField} comp
             * @param {Date} value
             */
            onSelectDate            : function(comp, value) {
                this.fireEvent('selectdate', this, comp, this.getValue());
            },
            /**
             * Handler for select time.
             * @param {Ext.form.TimeField} comp
             * @param {Object} record
             * @param {Number} index
             */
            onSelectTime            : function(comp, record, index) {
                this.fireEvent('selecttime', this, comp, record, index, this.getValue());
            },

            /**
             * Handler for keyup date.
             */
            onKeyUpDate             : function() {
                this.isDirty = function() {
                    return true
                };
                this.fireEvent('keyup', this);
            },
            /**
             * Handler for keyup time.
             */
            onKeyUpTime             : function() {
                this.isDirty = function() {
                    return true
                };
                this.fireEvent('keyup', this);
            },

            /**
             * Sets the value of the field.
             * @param {Date} v
             */
            setValue                : function(v) {
                if (v && Ext.isDate(v)) {
                    if (this.df)
                        this.df.setValue(v);
                    if (this.tf) {
                        if (this.hideTime != true) // && !this.tf.disabled
                            this.tf.setValue(v.format(this.tf.format));
                        else
                            this.tf.setValue('');
                    }
                    // we are here if the field isnt rendered yet, so we put the value into the value attribute.
                    if (!this.df && !this.tf) {
                        this.value = v;
                    }
                }
                else {
                    if (this.df)
                        this.df.setValue('');
                    if (this.tf)
                        this.tf.setValue('');
                }
            },

            /**
             * Gets the value of the field.
             * @return {Date}
             */
            getValue                : function() {
                if (this.tf == null && this.df == null)
                    return null;
                var time = this.tf.getValue();
                var dt = this.df.getValue();
                if (Ext.isDate(dt)) {
                    dt = dt.format(this.df.format);
                }
                else {
                    return null;
                };
                if (time != '') {// && !this.tf.disabled
                    return Date.parseDate(dt + ' ' + time, this.df.format + ' ' + this.tf.format);
                }
                return Date.parseDate(dt, this.df.format);
            },

            /**
             * Disables or Enables time field.
             * @param {Boolean} checked
             */
            disableTime             : function(checked) {
                this.tf.setDisabled(checked);
                if (checked) {
                    this.tf.clearValue();
                    this.tf.clearInvalid();
                }
                if (this.hideTime)
                    this.tf.setVisible(!checked);
                this.doLayout();
            },

            /**
             * Gets the value for all day checkbox.
             * @return {Boolean}
             */
            isAllDay                : function() {
                return Ext.getCmp(this.allday_id).getValue();
            },

            /**
             * @method getRawValue
             * @hide
             */
            getRawValue             : Ext.emptyFn,

            /**
             * @method setRawValue
             * @hide
             */
            setRawValue             : Ext.emptyFn,

            getErrors               : function(value) {
                var errors = Viz.form.DateTime.superclass.getErrors.apply(this, arguments);
                if (Ext.isFunction(this.validator)) {
                    var msg = this.validator(value);
                    if (msg !== true) {
                        errors.push(msg);
                    }
                }
                return errors;
            },

            validateValue           : function(value) {
                // currently, we only show 1 error at a time for a field, so just use the first one
                var error = this.getErrors(value)[0];
                if (error == undefined) {
                    return Viz.form.DateTime.superclass.validateValue.apply(this, arguments);
                }
                else {
                    this.markInvalid(error);
                    return false;
                }
            },

            /**
             * Override because otherwise the field would always appear as dirty.
             * @return {Boolean}
             */
            isDirty                 : function() {
                if (this.disabled || !this.rendered) {
                    return false;
                }
                return String(this.getValue()) !== String(this.originalValue);
            },

            disableTimeZoneComboBox : function() {
                this.timeZoneField.disable();
            },

            setTimeZoneValue        : function(date) {
                if (date) {
                    if (this.timeZoneField) {
                        this.timeZoneField.setValue(date);
                    }
                } else {
                    this.timeZoneField.loadDefaultTimeZone();
                }
            }

        });
Ext.reg('vizDateTime', Viz.form.DateTime);

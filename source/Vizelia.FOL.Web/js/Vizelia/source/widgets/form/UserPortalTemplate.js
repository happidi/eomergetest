﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.UserPortalTemplate
 * @extends Viz.panel.Panel
 */
Viz.form.UserPortalTemplate = Ext.extend(Viz.panel.Panel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor     : function(config) {

                var keyPortalTemplate = config.keyPortalTemplate;
                var storeFOLMembershipUser = new Viz.store.PortalTemplateFOLMembershipUser({
                            KeyPortalTemplate : keyPortalTemplate
                        });

                var gridId = Ext.id();
                var labelId = Ext.id();
                var btnOkId = Ext.id();

                var myCboxSelModel = new Viz.grid.CheckboxSelectionModel({
                            statusLabelId              : labelId,
                            updateSelectionHookHandler : function(selModel) {
                                var isDisabled;
                                var selectionCount = Object.size(selModel.multiSelectState);
                                if (selectionCount === 0) {
                                    isDisabled = true;
                                }
                                else {
                                    isDisabled = false;
                                }
                                Ext.getCmp(btnOkId).setDisabled(isDisabled);
                            }
                        });

                var columnsCheckbox = Viz.clone(Viz.Configuration.Columns.User);
                columnsCheckbox.splice(0, 0, myCboxSelModel);

                storeFOLMembershipUser.on('load', myCboxSelModel.reloadSelection, myCboxSelModel);

                var defaultConfig = {
                    layout    : {
                        type    : 'vbox',
                        padding : '5 5 5 5',
                        align   : 'stretch'
                    },
                    bodyStyle : 'padding: 5px;background-color: transparent;',
                    items     : [{
                                xtype : 'label',
                                html  : Ext.util.Format.htmlEncode($lang('msg_portaltemplate_pushupdates_advanced_info')) + '<br/><br/>'
                            }, {
                                id                 : gridId,
                                flex               : 1,
                                xtype              : 'vizGridUser',
                                stateId            : 'vizGridUserPortalTemplate',
                                store              : storeFOLMembershipUser,
                                border             : true,
                                tbar               : [{
                                            text    : $lang('msg_portaltemplate_pushupdates_advanced_deselect_all'),
                                            iconCls : 'viz-icon-small-delete',
                                            handler : function() {
                                                var grid = Ext.getCmp(gridId);
                                                grid.selModel.clearSelection();
                                            },
                                            scope   : this
                                        }, {
                                            text    : $lang('msg_portaltemplate_pushupdates_advanced_select_all'),
                                            iconCls : 'viz-icon-small-apply',
                                            handler : function() {
                                                var grid = Ext.getCmp(gridId);
                                                grid.ownerCt.getEl().mask();
                                                Viz.Services.EnergyWCF.FOLMembershipUser_GetStoreByKeyPortalTemplate({
                                                            KeyPortalTemplate : keyPortalTemplate,
                                                            success           : function(data) {
                                                                for (key in data.records) {
                                                                    var keyUser = data.records[key].KeyUser;
                                                                    if (keyUser) {
                                                                        grid.selModel.multiSelectState[keyUser] = data.records[key];
                                                                    }
                                                                }
                                                                grid.selModel.reloadSelection();
                                                                grid.ownerCt.getEl().unmask();
                                                            }
                                                        });
                                            },
                                            scope   : this
                                        }, {
                                            text    : $lang('msg_portaltemplate_pushupdates_advanced_new_users'),
                                            iconCls : 'viz-icon-small-user-add',
                                            handler : function() {
                                                var grid = Ext.getCmp(gridId);
                                                grid.ownerCt.getEl().mask();
                                                Viz.Services.EnergyWCF.FOLMembershipUser_GetNewUserKeysByKeyPortalTemplate({
                                                            keyPortalTemplate : keyPortalTemplate,
                                                            success           : function(data) {
                                                                for (var i = 0; i < data.length; i++) {
                                                                    grid.selModel.multiSelectState[data[i]] = data[i];
                                                                }
                                                                grid.selModel.reloadSelection();
                                                                grid.ownerCt.getEl().unmask();
                                                            }
                                                        });
                                            },
                                            scope   : this
                                        }, '-', {
                                            xtype : 'label',
                                            text  : $lang('msg_portaltemplate_pushupdates_advanced_user_count')
                                        }, {
                                            xtype : 'label',
                                            id    : labelId,
                                            text  : '0'
                                        }],
                                plugins            : [],
                                columns            : columnsCheckbox,
                                skipCloningColumns : true,
                                sm                 : myCboxSelModel
                            }],
                    buttons   : [{
                                text     : $lang('msg_ok'),
                                width    : 80,
                                id       : btnOkId,
                                disabled : true,
                                cls      : 'x-toolbar-standardbutton',
                                iconCls  : 'viz-icon-small-save',
                                handler  : function() {
                                    var grid = Ext.getCmp(gridId);
                                    var selectedUsers = [];
                                    for (key in grid.selModel.multiSelectState) {
                                        selectedUsers.push(key);
                                    }

                                    var keys = [keyPortalTemplate];

                                    var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter({
                                                pollingInterval        : 1000,
                                                serviceStart           : Viz.Services.EnergyWCF.PortalTemplate_PushUpdatesBegin,
                                                serviceParams          : {
                                                    Keys     : keys,
                                                    userKeys : selectedUsers
                                                },
                                                showProgress           : true,
                                                showProgressPercentage : true,
                                                modal                  : false,
                                                progressWindowTitle    : $lang('msg_portaltemplate_pushupdates')
                                            });
                                    submitter.start();
                                    this.getParentWindow().close();
                                },
                                scope    : this
                            }, {
                                text    : $lang('msg_cancel'),
                                width   : 80,
                                cls     : 'x-toolbar-standardbutton',
                                iconCls : 'viz-icon-small-cancel',
                                handler : function() {
                                    this.getParentWindow().close();
                                },
                                scope   : this
                            }]
                };

                Ext.applyIf(config, defaultConfig);
                Viz.form.UserPortalTemplate.superclass.constructor.call(this, config);
            },

            getParentWindow : function() {
                return this.findParentByType('window');
            }
        });

Ext.reg('vizFormUserPortalTemplate', Viz.form.UserPortalTemplate);
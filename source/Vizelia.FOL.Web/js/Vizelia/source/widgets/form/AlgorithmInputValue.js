﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.AlgorithmInputValue
 * @extends Ext.FormPanel.
 */
Viz.form.AlgorithmInputValue = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Playlist} entity The Playlist entity the playlist page belongs to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor        : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.AlgorithmInputValue_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.AlgorithmInputValue_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.AlgorithmInputValue_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.AlgorithmInputValue.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral  : function(config) {
                return [{
                            name   : 'KeyAlgorithmInputValue',
                            hidden : true
                        }, {
                            fieldLabel   : $lang('msg_algorithminputdefinition'),
                            name         : 'KeyAlgorithmInputDefinition',
                            KeyAlgorithm : config.entity.KeyAlgorithm,
                            hideLabel    : config.mode == 'update' && Ext.isEmpty(config.entity.KeyAlgorithmInputDefinition),
                            hidden       : config.mode == 'update' && Ext.isEmpty(config.entity.KeyAlgorithmInputDefinition),
                            xtype        : 'vizComboAlgorithmInputDefinition',
                            allowBlank   : config.mode == 'update' && Ext.isEmpty(config.entity.KeyAlgorithmInputDefinition),
                            listeners    : {
                                select : this.onSelectDefinition,
                                scope  : this
                            }
                        }, {
                            fieldLabel : $lang('msg_name'),
                            value      : config.entity.Name,
                            hideLabel  : config.mode == 'create' || !Ext.isEmpty(config.entity.KeyAlgorithmInputDefinition),
                            hidden     : config.mode == 'create' || !Ext.isEmpty(config.entity.KeyAlgorithmInputDefinition),
                            xtype      : 'textfield',
                            readOnly   : true,
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_value'),
                            name       : 'Value'
                        }];
            },

            onSelectDefinition : function(combo, record, index) {
                this.entity.Name = record.data.Name;
            },

            onBeforeSave       : function(form, item, extraParams) {
                if (item.KeyAlgorithmInputDefinition === '')
                    item.KeyAlgorithmInputDefinition = null;
                    
                if(!item.Value) {
                	item.Value = '';
                }
            }

        });

Ext.reg('vizFormAlgorithmInputValue', Viz.form.AlgorithmInputValue);
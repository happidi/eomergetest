﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.DrawingCanvasImage
 * @extends Ext.FormPanel.
 */
Viz.form.DrawingCanvasImage = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.DrawingCanvasImage_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.DrawingCanvasImage_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.DrawingCanvasImage_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.DrawingCanvasImage.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyDrawingCanvasImage',
                            hidden : true
                        }, {
                            fieldLabel: $lang('msg_localid'),
                            allowBlank: false,
                            name: 'LocalId'
                        }, {
                            name   : 'KeyDrawingCanvas',
                            hidden : true
                        },/*
                             * { fieldLabel : $lang('msg_drawingcanvaschart_title'), name : 'Title', allowBlank : false },
                             */{
                            fieldLabel : $lang('msg_drawingcanvasimage'),
                            name       : 'KeyImage',
                            xtype      : 'vizComboDynamicDisplayImage',
                            allowBlank : false
                        }, {
                            fieldLabel       : $lang('msg_drawingcanvaschart_width'),
                            name             : 'Width',
                            xtype            : 'vizSpinnerField',
                            minValue         : 10,
                            decimalPrecision : 0,
                            allowBlank       : false
                        }, {
                            fieldLabel       : $lang('msg_drawingcanvaschart_height'),
                            name             : 'Height',
                            xtype            : 'vizSpinnerField',
                            minValue         : 10,
                            decimalPrecision : 0,
                            allowBlank       : false
                        }, {
                            fieldLabel       : $lang('msg_drawingcanvaschart_x'),
                            name             : 'X',
                            xtype            : 'vizSpinnerField',
                            // minValue : 0,
                            decimalPrecision : 0,
                            value            : 0,
                            allowBlank       : false
                        }, {
                            fieldLabel       : $lang('msg_drawingcanvaschart_y'),
                            name             : 'Y',
                            xtype            : 'vizSpinnerField',
                            // minValue : 0,
                            decimalPrecision : 0,
                            value            : 0,
                            allowBlank       : false
                        }, {
                            fieldLabel       : $lang('msg_drawingcanvaschart_zindex'),
                            name             : 'ZIndex',
                            xtype            : 'vizSpinnerField',
                            minValue         : 0,
                            decimalPrecision : 0,
                            value            : 0,
                            allowBlank       : false
                        }];
            }
        });

Ext.reg('vizFormDrawingCanvasImage', Viz.form.DrawingCanvasImage);
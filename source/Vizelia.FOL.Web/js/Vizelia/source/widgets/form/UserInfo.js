﻿Ext.namespace("Viz.form");

/**
 * @class Viz.form.UserInfo
 * <p>
 * The user info is presented in east region of the main viewPort
 * </p>
 * @extends Ext.form.FormPanel
 * <p>
 * The UserInfo form.
 * </p>
 */
Viz.form.UserInfo = Ext.extend(Viz.form.FormPanel, {
    /**
     * Ctor.
     * @param {Object} config The configuration options
     */
    constructor: function (config) {
        this._fieldTimeZoneIdId = Ext.id();

        config = config || {};
        var defaultConfig = {
            closeonsave: false,
            mode_load: 'remote',
            serviceHandler: Viz.Services.CoreWCF.UserPreferences_FormLoad,
            serviceHandlerUpdate: Viz.Services.CoreWCF.UserPreferences_FormUpdate,
            items: this.configFormGeneral.apply(this)
    };
        var forcedConfig = {};
        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, forcedConfig);
        Viz.form.UserInfo.superclass.constructor.call(this, config);
        
        Viz.util.MessageBusMgr.subscribe("UpdateUserInfo", this.onUpdateUserInfo, this);
    },
    onUpdateUserInfo: function (user) {

        this.getForm().setValues([{
            id: 'UserName',
            value: user.UserName
        }, {
            id: 'Email',
            value: user.Email
        }, {
            id: 'LastLoginDate',
            value: user.LastLoginDate.format(Viz.getLocalizedDateTimeFormat(false))
        }, {
            id: 'FullName',
            value: user.ProviderUserKey.Occupant.FullName
        }, {
            id: 'IP',
            value: user.ProviderUserKey.Profile.IP
        }]);
    },
    configFormGeneral: function () {
        return [
            {
                fieldLabel: $lang('msg_login'),
                xtype: 'displayfield',
                name: 'UserName'
            }, {
                fieldLabel: $lang('msg_occupant_fullname'),
                xtype: 'displayfield',
                name: 'FullName'
            }, {
                fieldLabel: $lang('msg_occupant_email'),
                xtype: 'displayfield',
                name: 'Email'
            }, {
                fieldLabel: $lang('msg_user_lastlogindate'),
                xtype: 'displayfield',
                name: 'LastLoginDate'
            }, {
                fieldLabel: $lang('msg_user_ip'),
                xtype: 'displayfield',
                name: 'IP'
            },
            {
                name: 'KeyUser',
                hidden: true
            },
            {
                fieldLabel: $lang('msg_enum_dynamicdisplayimagetype_desktopbackground'),
                name: 'DesktopBackground',
                xtype: 'vizComboDesktopBackground',
                allowBlank: true
            },
            {
                fieldLabel: $lang('msg_desktopbackground_custom'),
                hiddenName: 'CustomDesktopBackground',
                name: 'CustomDesktopBackground',
                xtype: 'vizComboDynamicDisplayImage',
                type: Vizelia.FOL.BusinessEntities.DynamicDisplayImageType.DesktopBackground,
                store: new Viz.store.DynamicDisplayImage({
                    type: Vizelia.FOL.BusinessEntities.DynamicDisplayImageType.DesktopBackground
                }),
                allowBlank: true
            }, {
                fieldLabel: $lang('msg_desktopshortcuttextcolor'),
                name: 'DesktopTextColor',
                xtype: 'colorpickerfield',
                allowBlank: true,
                id: this._colorPickerId
            },
            {
                fieldLabel: $lang('msg_chart_timezone'),
                name: 'TimeZoneId',
                id : this._fieldTimeZoneIdId,
                xtype: 'vizComboTimeZone',
                allowBlank: false
            },
            {
                fieldLabel: $lang('msg_chart_preloadall'),
                name: 'PreloadCharts',
                xtype: 'vizcheckbox',
                hidden: !$authorized('@@@@ Chart - PreLoad'),
                allowBlank: true
            },
            {
                fieldLabel: $lang('msg_chart_kpi'),
                hiddenName: 'DefaultChartKPI',
                xtype: 'vizComboChart',
                store: new Viz.store.ChartKPI(),
                allowBlank: true
            },
            {
                fieldLabel: $lang('msg_user_grid_page_size'),
                name: 'GridsPageSize',
                xtype: 'viznumberfield',
                allowBlank: false,
                value: Viz.App.Global.User.Preferences.GridsPageSize,
                allowDecimals: false,
                width: 50,
                minValue: 1,
                maxValue: 1000
            }
        ];
    },

    /**
     * Handler for save success. We reload the store of meters to unmask the grid
     * @param {Ext.form.FormPanel} form
     * @param {Object} action
     * @return {Boolean}
     */
    onRender: function () {
        Viz.form.UserInfo.superclass.onRender.apply(this, arguments);
        Viz.util.MessageBusMgr.publish("UpdateUserInfo", Viz.App.Global.User);
    },
    onSaveSuccess: function () {
        this.updateUserPreferences(this.entity);
    },
    onAfterLoadValues: function () {
        Ext.getCmp(this._fieldTimeZoneIdId).loadDefaultTimeZone();
    },
    updateUserPreferences : function(preferences) {
        Viz.App.Global.User.Preferences = preferences;
        var url = Viz.util.Desktop.getBackgroundUrl(preferences.CustomDesktopBackground);
        Viz.App.Global.ViewPort.setDesktopBackground(preferences.DesktopBackground || url || '');

        Viz.App.Global.ViewPort.updateApplicationToolbarPortalWindows();
        Viz.App.Global.ViewPort.setDesktopShortcutTextColor(preferences.DesktopTextColor);

        if (preferences.PreloadCharts) {
            Viz.App.Global.ViewPort.loadChartsInCache();
        }
    }
});
Ext.reg('vizFormUserInfo', Viz.form.UserInfo);
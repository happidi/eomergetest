﻿Ext.namespace("Viz.form");
Viz.form.SpinnerField = Ext.extend(Ext.ux.form.SpinnerField, {
            /**
             * @cfg useAnchor {Boolean} Determines whether to use an anchor given in the config or not
             */
            useAnchor   : false,

            constructor : function(config) {
                var defaultConfig = {
                    width : 100
                };
                var forcedConfig = {};

                Ext.applyIf(config, defaultConfig);

                if (config.useAnchor !== true) {
                    delete config.anchor;
                }

                Ext.apply(config, forcedConfig);

                Viz.form.SpinnerField.superclass.constructor.call(this, config);
            },
            // for some reason onBlur event in extjs 3.4 Ext.ux.form.SpinnerField is set to Ext.emptyFn. That Causes the change event to never be fired.
            // reference http://www.sencha.com/forum/archive/index.php/t-97878.html
            onBlur      : function() {
                var v = this.getValue();
                if (String(v) !== String(this.startValue)) {
                    this.fireEvent('change', this, v, this.startValue);
                }
            },
            // debugging this function causes endless loop
            setRawValue : function(v) {
                var newVal = Ext.ux.form.SpinnerField.superclass.setRawValue.call(this, v);
                var val = this.getValue();
                if (String(val) !== String(this.startValue)) {
                    this.fireEvent('change', this, val, this.startValue);
                }
                return (newVal);
            }
        });

Ext.reg('vizSpinnerField', Viz.form.SpinnerField);

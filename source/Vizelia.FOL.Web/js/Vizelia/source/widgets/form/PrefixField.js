﻿Ext.namespace("Viz.form");

/**
 * @class Viz.form.PrefixField A text field with prefix.
 * @extends Ext.form.TextField
 * @xtype vizprefixfield
 */

Viz.form.PrefixField = Ext.extend(Ext.form.TextField, {
            /**
             * @cfg {String} prefix The prefix for this field
             */
            prefix        : null,

            previousValue : null,

            /**
             * CTor.
             * @param {Object} config The config for this object.
             */
            constructor   : function(config) {
                config = config || {};

                var initialValue = config.value || config.prefix || '';
                var prefixValue = config.prefix || '';

                // Escape regex-related characaters from prefix since we use a regex to validate.
                var regexValue = '^' + prefixValue.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&") + '.*';

                var defaultConfig = {};
                var forcedConfig = {
                    prefix          : prefixValue,
                    value           : initialValue,
                    previousValue   : initialValue,
                    regex           : new RegExp(regexValue),
                    regexText       : 'must begin with prefix ' + prefixValue,
                    enableKeyEvents : true,
                    listeners       : {
                        keydown : {
                            fn    : function(field, e) {
                                // We have to defer the handler by 1ms in order for the field value to update.
                                Ext.defer(function(field, e) {
                                            var value = field.getValue();

                                            if (!value || value == '') {
                                                this.setValue(this.prefix);
                                                this.previousValue = this.prefix;
                                            }
                                            else if (value.indexOf(this.prefix) != 0) {
                                                this.setValue(this.previousValue);
                                            }
                                            else {
                                                this.previousValue = value;
                                            }
                                        }, 1, this, arguments)
                            },
                            scope : this
                        }
                    }
                };

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.PrefixField.superclass.constructor.call(this, config);
            }

        });

Ext.reg('vizprefixfield', Viz.form.PrefixField);
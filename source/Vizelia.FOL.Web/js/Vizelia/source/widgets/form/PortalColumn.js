﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.PortalColumn
 * @extends Ext.FormPanel.
 */
Viz.form.PortalColumn = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.PortalTab} entity The portal tab entity the column belong to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor            : function(config) {
                this._tabPortlet = Ext.id();
                this._gridPortlet = Ext.id();
                var Id = config.entity ? config.entity.KeyPortalColumn : null;
                this.storePortlet = new Viz.store.Portlet({
                            KeyPortalColumn : Id
                        });
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.PortalColumn_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.PortalColumn_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.PortalColumn_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_portlet'),
                                    id         : this._tabPortlet,
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    hidden     : true,// config.mode == 'create',
                                    items      : [this.configGridPanelPortlet(config)]
                                }]
                    },
                    extraButtons         : [{
                                text    : $lang('msg_portaltab_viewflex'),
                                width   : 80,
                                cls     : 'x-toolbar-standardbutton',
                                iconCls : 'viz-icon-small-display',
                                handler : this.displayTotalFlex,
                                scope   : this
                            }],
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.PortalColumn.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral      : function(config) {
                return [{
                            name   : 'KeyPortalColumn',
                            hidden : true
                        }, {
                            fieldLabel: $lang('msg_localid'),
                            allowBlank: false,
                            name: 'LocalId'
                        }, {
                            fieldLabel       : $lang('msg_portalcolumn_flex'),
                            name             : 'Flex',
                            xtype            : 'vizSpinnerField',
                            minValue         : 1,
                            maxValue         : 100,
                            decimalPrecision : 0,
                            allowBlank       : false
                        }, {
                            fieldLabel       : $lang('msg_portalcolumn_order'),
                            name             : 'Order',
                            xtype            : 'vizSpinnerField',
                            minValue         : 1,
                            decimalPrecision : 0,
                            allowBlank       : false,
                            value            : config.entity.Order || 1
                        }, {
                            fieldLabel   : $lang('msg_portalcolumn_type'),
                            name         : 'Type',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.PortalColumnType,Vizelia.FOL.Common',
                            value        : Vizelia.FOL.BusinessEntities.PortalColumnType.Main
                        }];
            },

            /**
             * The configuration for the grid priority
             * @return {object}
             */
            configGridPanelPortlet : function(config) {
                return {
                    xtype           : 'vizGridPortlet',
                    loadMask        : false,
                    border          : true,
                    id              : this._gridPortlet,
                    store           : this.storePortlet,
                    KeyPortalColumn : config.entity.KeyPortalColumn,
                    columns         : Viz.Configuration.Columns.Portlet,
                    listeners       : {
                        beforedeleterecord : this.onDeletedRecordPortlet,
                        scope              : this
                    }
                };
            },

            /**
             * Handler for the columns grid delete.
             */
            onDeletedRecordPortlet : function() {
                this.deleteSelectedRowsFromGrid(this._gridPortlet);
                return false;
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave           : function(form, item, extraParams) {
                if (this.storePortlet.hasLoaded)
                    this.storePortlet.save();
                Ext.apply(extraParams, {
                            portlet : this.storePortlet.crudStore
                        });
                return true;
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess          : function(form, action) {
                Viz.form.PortalColumn.superclass.onSaveSuccess.apply(this, arguments);

                /*
                 * var tabpanel = this.getTabContainer(); tabpanel.unhideTabStripItem(this._tabPortlet); var grid = Ext.getCmp(this._gridPortlet); grid.KeyPortalColumn = grid.store.serviceParams.KeyPortalColumn = this.entity.KeyPortalColumn;
                 */
            },

            /**
             * Handler for the display total flex button
             */
            displayTotalFlex       : function() {
                var type = this.getForm().findField('Type').getValue();
                if (Ext.isDefined(type)) {
                    Viz.Services.EnergyWCF.PortalTab_GetFlex({
                                KeyPortalTab : this.entity.KeyPortalTab,
                                type         : type,
                                success      : function(flex) {
                                    Ext.Msg.show({
                                                title   : $lang("msg_portaltab_viewflex"),
                                                width   : 200,
                                                msg     : flex,
                                                buttons : Ext.Msg.OK,
                                                icon    : Ext.MessageBox.INFO
                                            })
                                }
                            })
                }
            }
        });

Ext.reg('vizFormPortalColumn', Viz.form.PortalColumn);
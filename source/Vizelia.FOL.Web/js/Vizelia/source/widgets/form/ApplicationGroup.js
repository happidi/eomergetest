﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ApplicationGroup
 * @extends Viz.form.FormPanel
 */
Viz.form.ApplicationGroup = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                   : function(config) {
                var Id = config.entity ? config.entity.Id : null;

                this.storeAuthorization = new Viz.store.ApplicationGroupAuthorization({
                            applicationGroupId : Id
                        });

                this.storeAzManRole = new Viz.store.ApplicationGroupAzManRole({
                            applicationGroupId : Id
                        });

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.AuthenticationWCF.ApplicationGroup_FormLoad,
                    serviceHandlerCreate : Viz.Services.AuthenticationWCF.ApplicationGroup_FormCreate,
                    serviceHandlerUpdate : Viz.Services.AuthenticationWCF.ApplicationGroup_FormUpdate,
                    tabConfig            : {
                        deferredRender : true,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_user_management'),
                                    iconCls    : 'viz-icon-small-user',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridDetail(this.configDetailGridAuthorization())]
                                    // [this.configGridAuthorization(config)]
                            }   , {
                                    title      : $lang('msg_azmanrole'),
                                    iconCls    : 'viz-icon-small-role',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configDetailGridRole()]
                                    // [this.configGridAzManRole(config)]
                            }]
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.ApplicationGroup.superclass.constructor.call(this, config);

                this.on("beforeopendetail", this.onBeforeOpenDetail, this);
            },

            /**
             * Handler before save that gives a chance to send the 2 stores as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                  : function(form, item, extraParams) {
                if (this.storeAzManRole.hasLoaded)
                    this.storeAzManRole.save();
                if (this.storeAuthorization.hasLoaded)
                    this.storeAuthorization.save();
                Ext.apply(extraParams, {
                            authorizations : this.storeAuthorization.crudStore,
                            roles          : this.storeAzManRole.crudStore
                        });
                return true;
            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral             : function(config) {
                return [{
                            name      : 'Id',
                            hidden    : true,
                            hideLabel : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            anchor     : Viz.Const.UI.Anchor + ' -53',
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description'
                        }];
            },

            /**
             * @private The configuration object for detail grid of Users.
             * @return {Object}
             */
            configDetailGridAuthorization : function() {
                return {
                    name                : 'User',
                    windowTitle         : $lang('msg_user_management'),
                    windowWidth         : 1000,
                    windowHeight        : 400,
                    windowIconCls       : 'viz-icon-small-user',
                    xtypeGridDetail     : 'vizGridUser',
                    buttonAddTitle      : $lang('msg_user_add'),
                    buttonAddIconCls    : 'viz-icon-small-user-add',
                    buttonDeleteTitle   : $lang('msg_user_delete'),
                    buttonDeleteIconCls : 'viz-icon-small-user-delete',
                    gridLoadMask        : false,
                    storeDetail         : this.storeAuthorization,
                    keyColumn           : 'UserName',
                    columnsGridDetail   : Viz.Configuration.Columns.ApplicationGroupAuthorization,
                    gridPlugins         : [new Viz.plugins.GridExport({
                                listeners : {
                                    beforeExport : function(plugin) {
                                        plugin.title = $lang('msg_authorization') + ' : ' + this.entity.Id
                                    },
                                    scope        : this
                                }
                            })]
                };
            },
            /**
             * @private The configuration object for detail grid AzMan Role.
             * @return {Object}
             */
            configDetailGridRole          : function() {
                return {
                    xtype    : 'vizGridFilterAzManRole',
                    loadMask : false,
                    store    : this.storeAzManRole
                }
            }

            ,

            /**
             * Handler for save success. We reload the store to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                 : function(form, action) {
                if (this.closeonsave !== true) {
                    Viz.form.ApplicationGroup.superclass.onSaveSuccess.apply(this, arguments);
                    var stores = [];
                    if (this.storeAuthorization)
                        stores.push(this.storeAuthorization);
                    if (this.storeAzManRole)
                        stores.push(this.storeAzManRole);

                    Ext.each(stores, function(store) {
                                store.serviceParams.applicationGroupId = this.entity.Id;
                                if (store.hasLoaded)
                                    store.reload();
                            }, this);

                }
                return true;
            }

        });

Ext.reg('vizFormApplicationGroup', Viz.form.ApplicationGroup);
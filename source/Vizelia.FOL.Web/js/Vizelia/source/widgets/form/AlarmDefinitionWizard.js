﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.AlarmDefinitionWizard
 * @extends Ext.FormPanel.
 */
Viz.form.AlarmDefinitionWizard = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                         : function(config) {
                config = config || {};
                var Id = config.entity ? config.entity.KeyAlarmDefinition : null;
                this._const_grid_anchor = ' -80';

                this._gridMeter = Ext.id();
                this._gridSpatial = Ext.id();
                this._gridMeterClassification = Ext.id();

                this.storeFilterSpatial = new Viz.store.AlarmDefinitionFilterSpatial({
                            KeyAlarmDefinition : Id
                        });

                this.storeFilterMeterClassification = new Viz.store.AlarmDefinitionFilterMeterClassification({
                            KeyAlarmDefinition : Id
                        });

                this.storeMeter = new Viz.store.AlarmDefinitionMeter({
                            KeyAlarmDefinition : Id,
                            listeners          : {
                                add    : this.onMeterStoreChange,
                                remove : this.onMeterStoreChange,
                                scope  : this
                            }
                        });

                this.storeChart = new Viz.store.AlarmDefinitionChart({
                            KeyAlarmDefinition : Id
                        });

                var items = [];
                if (config.entity.type != 'Chart') {
                    items = [this.configFormGeneral(config), this.configDetailGridMeter(config), this.configGridFilterSpatial(config), this.configGridFilterMeterClassification(config), this.configFormMeterAggregation(config), this.configFormCondition(config), this.configFormActionRequest(config)];
                }
                else {
                    items = [this.configFormGeneral(config), this.configDetailGridChart(config), this.configFormCondition(config), this.configFormActionRequest(config)];

                }
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.AlarmDefinition_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.AlarmDefinition_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.AlarmDefinition_FormUpdate,
                    messageSaveStart     : 'AlarmDefinitionChangeStart',
                    messageSaveEnd       : 'AlarmDefinitionChange',
                    isWizard             : true,
                    hasValidationToolbar : true,
                    closeOnSave          : true,
                    tabConfig            : {
                        deferredRender : true,
                        items          : items,
                        defaults       : {
                            layout   : 'fit',
                            hideMode : 'offsets'
                        }
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.AlarmDefinitionWizard.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                   : function(config) {
                return {
                    title      : $lang('msg_alarmdefinition_wizard_welcome'),
                    iconCls    : 'viz-icon-small-form-update',
                    labelWidth : 150,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_alarmdefinition_wizard_titlecard_description'),
                                fieldClass : 'x-wizard-displayfield'

                            }, {
                                name   : 'KeyAlarmDefinition',
                                hidden : true
                            }, {
                                fieldLabel : $lang('msg_title'),
                                name       : 'Title',
                                allowBlank : false
                            }, {
                                fieldLabel           : $lang('msg_alarmdefinition_classification'),
                                xtype                : 'viztreecomboClassificationItemAlarmDefinition',
                                name                 : 'KeyClassificationItem',
                                originalDisplayValue : 'ClassificationItemLongPath',
                                allowBlank           : false
                            }, {
                                fieldLabel             : $lang('msg_location_path'),
                                xtype                  : 'viztreecomboSpatial',
                                validator              : Ext.form.VTypes.locationValidator,
                                name                   : 'KeyLocation',
                                originalDisplayValue   : 'LocationLongPath',
                                ExcludeMetersAndAlarms : true,
                                allowBlank             : false
                            }, {
                                xtype      : 'textarea',
                                fieldLabel : $lang('msg_description'),
                                name       : 'Description',
                                anchor     : Viz.Const.UI.Anchor + ' -140'
                            }]
                };
            },

            /**
             * The configuration object for the detail grid Meter.
             * @return {Object}
             */
            configDetailGridMeter               : function(config) {
                var retVal = this.configGridDetail({
                            name                         : 'Meter',
                            windowTitle                  : $lang('msg_meters'),
                            windowWidth                  : 1000,
                            windowHeight                 : 400,
                            windowIconCls                : 'viz-icon-small-meter',
                            xtypeGridDetail              : 'vizGridMeter',
                            columnsGridDetail            : Viz.Configuration.Columns.ChartMeter,
                            buttonAddTitle               : $lang('msg_meter_add'),
                            buttonAddIconCls             : 'viz-icon-small-add',
                            buttonDeleteTitle            : $lang('msg_meter_delete'),
                            buttonDeleteIconCls          : 'viz-icon-small-delete',
                            storeDetail                  : this.storeMeter,
                            keyColumn                    : 'KeyMeter',
                            ddGroupGridDetail            : 'SpatialDD',
                            enableDropFromTreeGridDetail : true
                        });
                Ext.apply(retVal, {
                            anchor : Viz.Const.UI.GridAnchor + this._const_grid_anchor
                        });
                return {
                    title      : $lang('msg_meters'),
                    iconCls    : 'viz-icon-small-meter',
                    labelWidth : 150,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_chart_wizard_metercard_description'),
                                fieldClass : 'x-wizard-displayfield'
                            }, retVal]
                };
            },

            /**
             * The configuration object for the detail grid Chart.
             * @return {Object}
             */
            configDetailGridChart               : function() {
                var retVal = this.configGridDetail({
                            name                : 'Chart',
                            windowTitle         : $lang('msg_chart'),
                            windowWidth         : 600,
                            windowHeight        : 400,
                            windowIconCls       : 'viz-icon-small-chart',
                            xtypeGridDetail     : 'vizGridChart',
                            columnsGridDetail   : Viz.Configuration.Columns.ChartSimple,
                            buttonAddTitle      : $lang('msg_chart_add'),
                            buttonAddIconCls    : 'viz-icon-small-add',
                            buttonDeleteTitle   : $lang('msg_chart_delete'),
                            buttonDeleteIconCls : 'viz-icon-small-delete',
                            storeDetail         : this.storeChart,
                            keyColumn           : 'KeyChart'
                        });
                Ext.apply(retVal, {
                            anchor      : Viz.Const.UI.GridAnchor + this._const_grid_anchor,
                            allowEmpty  : false,
                            invalidText : $lang('msg_alarmdefinition_wizard_chart_empty')
                        });
                return {
                    title      : $lang('msg_chart'),
                    iconCls    : 'viz-icon-small-chart',
                    labelWidth : 150,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_alarmdefinition_wizard_chartdescription'),
                                fieldClass : 'x-wizard-displayfield'
                            }, retVal]
                };
            },

            onMeterStoreChange                  : function(store) {
                if (store.getCount() > 0) {
                    Ext.getCmp(this._gridSpatial).allowEmpty = true;
                    Ext.getCmp(this._gridMeterClassification).allowEmpty = true;
                }
                else {
                    Ext.getCmp(this._gridSpatial).allowEmpty = false;
                    Ext.getCmp(this._gridMeterClassification).allowEmpty = false;
                }
            },

            /**
             * The configuration object for the filter spatial grid.
             * @return {Object}
             */
            configGridFilterSpatial             : function(config) {
                return {
                    title      : $lang('msg_chart_filterspatial'),
                    iconCls    : 'viz-icon-small-site',
                    labelWidth : 150,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_chart_wizard_spatialcard_description'),
                                fieldClass : 'x-wizard-displayfield'
                            }, {
                                id                          : this._gridSpatial,
                                anchor                      : Viz.Const.UI.GridAnchor + this._const_grid_anchor,
                                xtype                       : 'vizGridFilterSpatial',
                                store                       : this.storeFilterSpatial,
                                border                      : true,
                                loadMask                    : false,
                                allowEmpty                  : false,
                                invalidText                 : $lang('msg_chart_wizard_spatial_empty'),
                                disableCheckboxForAncestors : true
                            }]
                };
            },

            configGridFilterMeterClassification : function(config) {
                return {
                    title      : $lang('msg_meter_classification'),
                    iconCls    : 'viz-icon-small-classificationitem',
                    labelWidth : 150,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_chart_wizard_meterclassificationcard_description'),
                                fieldClass : 'x-wizard-displayfield'
                            }, {
                                anchor      : Viz.Const.UI.GridAnchor + this._const_grid_anchor,
                                xtype       : 'vizGridFilterMeterClassification',
                                store       : this.storeFilterMeterClassification,
                                border      : true,
                                loadMask    : false,
                                id          : this._gridMeterClassification,
                                listeners   : {
                                    beforeaddrecord : function(grid, entity) {
                                        if (this.storeFilterSpatial.hasLoaded)
                                            this.storeFilterSpatial.save();
                                        entity.KeyChart = this.entity.KeyChart;
                                        entity.filterSpatial = this.storeFilterSpatial.crudStore;
                                        return true;
                                    },
                                    scope           : this
                                },
                                allowEmpty  : false,
                                invalidText : $lang('msg_chart_wizard_meterclassification_empty')
                            }]
                };

            },

            /**
             * config of the form containing the meter aggregation parameters.
             * @param {} config
             * @return {}
             */
            configFormMeterAggregation          : function(config) {
                return {
                    title      : $lang('msg_alarmdefinition_meteraggregation'),
                    iconCls    : 'viz-icon-small-meter',
                    labelWidth : 150,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_alarmdefinition_wizard_meteraggregation_description'),
                                fieldClass : 'x-wizard-displayfield'
                            }, {
                                fieldLabel     : $lang('msg_alarmdefinition_timeinterval'),
                                name           : 'TimeInterval',
                                allowBlank     : false,
                                xtype          : 'vizComboEnum',
                                enumTypeName   : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common',
                                sortByValues   : true,
                                displayIcon    : true,
                                valuesToRemove : ['None']
                            }, {
                                fieldLabel : $lang('msg_chart_timezone'),
                                name       : 'TimeZoneId',
                                allowBlank : false,
                                xtype      : 'vizComboTimeZone',
                                autoLoad   : true
                            }]
                };
            },

            configFormCondition                 : function(config) {
                return {
                    title      : $lang('msg_alarmdefinition_condition'),
                    iconCls    : 'viz-icon-small-calculatedserie',
                    labelWidth : 180,
                    items      : [{
                                fieldLabel : $lang('msg_alarmdefinition_usepercentagedelta'),
                                name       : 'UsePercentageDelta',
                                xtype      : 'vizcheckbox',
                                checked    : false
                            }, {
                                fieldLabel     : $lang('msg_alarmdefinition_condition'),
                                name           : 'Condition',
                                xtype          : 'vizComboEnum',
                                enumTypeName   : 'Vizelia.FOL.BusinessEntities.FilterCondition,Vizelia.FOL.Common',
                                valuesToRemove : ['Like'],
                                allowBlank     : false,
                                listeners      : {
                                    select : function(combo, record) {
                                        Viz.form.AlarmDefinition.prototype.updateThresholds.apply(this, [record.data.Value]);

                                    },
                                    scope  : this
                                }
                            }, {
                                fieldLabel       : ' = ',// $lang('msg_alarmdefinition_threshold'),
                                xtype            : 'viznumberfield',
                                allowBlank       : false,
                                name             : 'Threshold',
                                decimalPrecision : 5
                            }, {
                                fieldLabel       : $lang('msg_alarmdefinition_threshold2'),
                                xtype            : 'viznumberfield',
                                allowBlank       : true,
                                hidden           : true,
                                name             : 'Threshold2',
                                decimalPrecision : 5
                            }]
                };
            },

            /**
             * config of the form containing the alarm actionrequest
             * @param {} config
             * @return {}
             */
            configFormActionRequest             : function(config) {
                return {
                    title      : $lang('msg_alarmdefinition_action'),
                    iconCls    : 'viz-icon-small-actionrequest',
                    labelWidth : 180,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_alarmdefinition_wizard_actioncard_description'),
                                fieldClass : 'x-wizard-displayfield'
                            }, {
                                xtype      : 'textarea',
                                allowBlank : true,
                                fieldLabel : $lang('msg_chartscheduler_emails'),
                                name       : 'Emails'
                            }, {
                                fieldLabel : $lang('msg_playlist_culture'),
                                name       : 'KeyLocalizationCulture',
                                hiddenName : 'KeyLocalizationCulture',
                                autoLoad   : config.mode == 'create',
                                xtype      : 'vizComboLocalizationCulture'
                            }]
                };
            },
            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                        : function(form, item, extraParams) {
                var storeArray = this.getExtraStoreArray();
                Ext.each(storeArray, function(store) {
                            if (store.hasLoaded)
                                store.save();
                        }, this);
                Ext.apply(extraParams, {
                            filterSpatial             : this.storeFilterSpatial.crudStore,
                            filterMeterClassification : this.storeFilterMeterClassification.crudStore,
                            meters                    : this.storeMeter.crudStore,
                            charts                    : this.storeChart.crudStore
                        });

                item.Enabled = true;
                if (!item.TimeZoneId) {
                    var updateTimeZone = function (timezoneid) {
                        item.TimeZoneId = timezoneid;
                    };
                    Viz.util.Chart.getTimeZoneId(updateTimeZone.createDelegate(this));
                }
                return true;
            },

            /**
             * Return an array of all the extra store of the AlarmDefinition.
             * @return {Array}
             */
            getExtraStoreArray                  : function() {
                return [this.storeFilterSpatial, this.storeFilterMeterClassification, this.storeMeter, this.storeChart];

            },

            /*
             * we orrive the standard AlarmDefinition method here because not all fields are existing.
             */
            onAfterLoadValues                   : function() {

            },
            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                       : function(form, action) {
            }
        });

Ext.reg('vizFormAlarmDefinitionWizard', Viz.form.AlarmDefinitionWizard);
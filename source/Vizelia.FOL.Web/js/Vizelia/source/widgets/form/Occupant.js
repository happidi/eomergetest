Ext.namespace('Viz.form');
/**
 * @class Viz.form.Occupant
 * @extends Ext.FormPanel.
 */
Viz.form.Occupant = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    psetConfig           : {
                        IfcType                    : 'IfcOccupant',
                        entityKey                  : 'KeyOccupant',
                        filterByClassificationItem : true
                    },
                    serviceHandler       : Viz.Services.CoreWCF.Occupant_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.Occupant_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.Occupant_FormUpdate,
                    entityType           : 'Occupant',
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Occupant.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyOccupant',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_occupant_id'),
                            name       : 'Id',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_occupant_firstname'),
                            name       : 'FirstName',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_occupant_lastname'),
                            name       : 'LastName',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_occupant_jobtitle'),
                            name       : 'JobTitle',
                            xtype      : 'textarea'
                        }, {
                            name       : 'TelephoneNumbers',
                            fieldLabel : $lang('msg_occupant_telephone')
                        }, {
                            fieldLabel             : $lang('msg_occupant_location'),
                            xtype                  : 'viztreecomboSpatial',
                            name                   : 'KeyLocation',
                            validator              : Ext.form.VTypes.locationValidator,
                            ExcludeMetersAndAlarms : true,
                            originalDisplayValue   : 'LocationLongPath'
                        }, {
                            fieldLabel           : $lang('msg_occupant_company'),
                            xtype                : 'viztreecomboOrganization',
                            name                 : 'KeyOrganization',
                            originalDisplayValue : 'OrganizationLongPath'
                        }, {
                            fieldLabel           : $lang('msg_occupant_worktype'),
                            xtype                : 'viztreecomboClassificationItemWorktype',
                            name                 : 'KeyClassificationItemWorktype',
                            originalDisplayValue : 'ClassificationItemWorktypeLongPath',
                            listeners            : {
                                changevalue : function(field, value) {
                                    this.psetConfig.KeyClassificationItem = value;
                                    this.loadExtraTabs();
                                },
                                scope       : this
                            }
                        }, {
                            fieldLabel           : $lang('msg_occupant_organization'),
                            xtype                : 'viztreecomboClassificationItemOrganization',
                            name                 : 'KeyClassificationItemOrganization',
                            originalDisplayValue : 'ClassificationItemOrganizationLongPath'
                        }];
            }
        });

Ext.reg('vizFormOccupant', Viz.form.Occupant);

/**
 * @class Viz.form.Occupant2
 * @extends Ext.form.FormPanel
 * <p>
 * The Occupant form.
 * </p>
 */
Viz.form.Occupant2 = Ext.extend(Ext.form.FormPanel, {
            border      : false,
            // constructor function
            constructor : function(config) {
                Ext.apply(this, {
                            // pre-configured config options here
                            defaultType : 'textfield',
                            frame       : true,
                            defaults    : {
                                anchor : '-5'
                            },
                            bodyStyle   : 'padding: 6px 6px 6px 6px',
                            items       : [{
                                        xtype       : 'fieldset',
                                        defaultType : 'textfield',
                                        defaults    : {
                                            msgTarget : 'side',
                                            anchor    : Viz.Const.UI.Anchor
                                        },
                                        title       : $lang('msg_info'),
                                        items       : [{
                                                    fieldLabel : $lang('msg_occupant_id'),
                                                    name       : 'Id',
                                                    allowBlank : false
                                                }, {
                                                    fieldLabel : $lang('msg_occupant_firstname'),
                                                    name       : 'FirstName',
                                                    allowBlank : false
                                                }, {
                                                    fieldLabel : $lang('msg_occupant_lastname'),
                                                    name       : 'LastName',
                                                    allowBlank : false
                                                }, {
                                                    fieldLabel : $lang('msg_occupant_jobtitle'),
                                                    name       : 'JobTitle',
                                                    xtype      : 'textarea',
                                                    readOnly   : false
                                                }, {
                                                    fieldLabel : $lang('msg_occupant_organization'),
                                                    name       : 'OrganizationName',
                                                    readOnly   : true
                                                }]
                                    }, {
                                        xtype       : 'fieldset',
                                        title       : $lang('msg_occupant_contact'),
                                        defaultType : 'textfield',
                                        defaults    : {
                                            msgTarget : 'side',
                                            anchor    : Viz.Const.UI.Anchor
                                        },
                                        items       : [{
                                                    fieldLabel : $lang('msg_occupant_email'),
                                                    name       : 'ElectronicMailAddresses'
                                                }, {
                                                    fieldLabel : $lang('msg_occupant_telephone'),
                                                    name       : 'TelephoneNumbers'
                                                }, {
                                                    fieldLabel : $lang('msg_occupant_fax'),
                                                    name       : 'FacsimileNumbers'
                                                }, {
                                                    fieldLabel : $lang('msg_occupant_pager'),
                                                    name       : 'PagerNumber'
                                                }]
                                    }],
                            buttons     : [{
                                        text    : 'Update',
                                        scope   : this,
                                        handler : function() {
                                            var currentRecord = this.ownerCt.grid.getSelectionModel().getSelected();
                                            if (currentRecord)
                                                this.getForm().updateRecord(currentRecord);
                                        }
                                    }, {
                                        text    : 'Cancel',
                                        scope   : this,
                                        handler : function() {
                                            var currentRecord = this.ownerCt.grid.getSelectionModel().getSelected();
                                            if (currentRecord) {
                                                currentRecord.reject();
                                                this.getForm().loadRecord(currentRecord);
                                            }
                                        }
                                    }]
                        });

                Viz.form.Occupant2.superclass.constructor.apply(this, arguments);

            }

        });

Ext.reg("vizFormOccupant2", Viz.form.Occupant2);
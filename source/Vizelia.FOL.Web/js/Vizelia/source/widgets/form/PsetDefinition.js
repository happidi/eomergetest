﻿Ext.namespace("Viz.form");

/**
 * @class Viz.form.PsetDefinition
 * @extends Viz.FormPanel The PsetDefinition form.
 */
Viz.form.PsetDefinition = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.PsetDefinition} entity The entity to load the form with.
             */

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);

                Ext.apply(config, {
                            mode_load            : 'local',
                            serviceHandlerCreate : Viz.Services.CoreWCF.PsetDefinition_FormCreate,
                            serviceHandlerUpdate : Viz.Services.CoreWCF.PsetDefinition_FormUpdate,
                            items                : this.configFormGeneral(config)
                        });
                Viz.form.PsetDefinition.superclass.constructor.call(this, config);
            },

            /**
             * Handler for beforesave event.
             * @param {Viz.form.FormPanel} form
             */
            onBeforeSave      : function(form) {
                Viz.util.MessageBusMgr.publish("PsetChangeStart");
            },

            /**
             * Handler for savesuccess event.
             * @param {Viz.form.FormPanel} form
             * @param {Ext.form.Action} action
             */
            onSaveSuccess     : function(form, action) {
                Viz.util.MessageBusMgr.publish("PsetChangeEnd");

            },

            /**
             * Handler for savefailure event.
             * @param {Viz.form.FormPanel} form
             * @param {Ext.form.Action} action
             */
            onSaveFailure     : function(form, action) {
                Viz.util.MessageBusMgr.publish("PsetChangeEnd");
            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral : function(config) {
                return [{
                            fieldLabel : $lang('msg_id'),
                            name       : 'KeyPropertySet',
                            readOnly   : true,
                            hidden     : true,
                            hideLabel  : true
                        }, {
                            id         : this._fieldPsetId,
                            fieldLabel : $lang('msg_localid'),
                            readOnly   : config.mode == "update" ? true : false,
                            name       : 'PsetName',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang("msg_pset_usagename"),
                            name       : 'UsageName',
                            hiddenName : 'UsageName',
                            xtype      : 'vizComboPsetUsageName',
                            hidden     : (!(config.entity === null))
                        }, {
                            fieldLabel : $lang('msg_description'),
                            name       : 'PsetDescription',
                            xtype      : 'textarea'// ,
                            // anchor : Viz.Const.UI.Anchor + ' -60'
                    }   , Viz.combo.Localization.factory({})];
            }

        });

Ext.reg("vizFormPsetDefinition", Viz.form.PsetDefinition);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.VirtualFile
 * @extends Ext.FormPanel.
 */
Viz.form.VirtualFile = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor        : function(config) {
                this._fieldType = Ext.id();
                this._fieldChart = Ext.id();
                this._fieldChartParent = Ext.id();
                // this._fieldPortalTab = Ext.id();
                this._fieldPlaylist = Ext.id();
                this._fieldMeter = Ext.id();
                this._fieldWidth = Ext.id();
                this._fieldHeight = Ext.id();
                this._fieldMaxRecordCount = Ext.id();

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.VirtualFile_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.VirtualFile_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.VirtualFile_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.VirtualFile.superclass.constructor.call(this, config);

                this.on('afterrender', this.onAfterRender, this);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral  : function(config) {
                return [{
                            name   : 'KeyVirtualFile',
                            hidden : true
                        }, {
                            fieldLabel: $lang('msg_localid'),
                            allowBlank: false,
                            name: 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_virtualfile_entitytype'),
                            xtype      : 'vizComboVirtualFileType',
                            id         : this._fieldType,
                            value      : 'Vizelia.FOL.BusinessEntities.Chart',
                            allowBlank : false,
                            listeners  : {
                                select : function(combo, record, index) {
                                    var type = record.get('Value');
                                    this.updateFormFromType(type);
                                },
                                scope  : this
                            }
                        }, Viz.combo.Chart.factory({
                                    fieldLabel : $lang('msg_chart'),
                                    id         : this._fieldChart,
                                    parentid   : this._fieldChartParent,
                                    allowBlank : true,
                                    hidden     : false,
                                    listeners  : {
                                        select : function(combo, record, index) {
                                            this.entity.EntityType = 'Vizelia.FOL.BusinessEntities.Chart';
                                            this.entity.KeyEntity = record.get('KeyChart');
                                            // Ext.getCmp(this._fieldPortalTab).clearValue();
                                            Ext.getCmp(this._fieldPlaylist).clearValue();
                                            Ext.getCmp(this._fieldMeter).clearValue();

                                            this.updateFormFromType(this.entity.EntityType);
                                        },
                                        scope  : this
                                    }
                                }), /*
                                     * { fieldLabel : $lang('msg_portaltab'), xtype : 'vizComboPortalTab', allowBlank : true, hidden : false, id : this._fieldPortalTab, listeners : { select : function(combo, record, index) { this.entity.EntityType = 'Vizelia.FOL.BusinessEntities.PortalTab'; this.entity.KeyEntity = record.get('KeyPortalTab'); Ext.getCmp(this._fieldChart).clearValue(); Ext.getCmp(this._fieldPlaylist).clearValue(); Ext.getCmp(this._fieldMeter).clearValue(); this.updateFormFromType(this.entity.EntityType); }, scope : this } },
                                     */
                        {
                            fieldLabel : $lang('msg_playlist'),
                            xtype      : 'vizComboPlaylist',
                            allowBlank : true,
                            hidden     : false,
                            id         : this._fieldPlaylist,
                            listeners  : {
                                select : function(combo, record, index) {
                                    this.entity.EntityType = 'Vizelia.FOL.BusinessEntities.Playlist';
                                    this.entity.KeyEntity = record.get('KeyPlaylist');
                                    Ext.getCmp(this._fieldWidth).setValue(record.get('Width'));
                                    Ext.getCmp(this._fieldHeight).setValue(record.get('Height'));

                                    Ext.getCmp(this._fieldChart).clearValue();
                                    // Ext.getCmp(this._fieldPortalTab).clearValue();
                                    Ext.getCmp(this._fieldMeter).clearValue();

                                    this.updateFormFromType(this.entity.EntityType);
                                },
                                scope  : this
                            }
                        }, {
                            fieldLabel : $lang('msg_meter'),
                            xtype      : 'vizComboMeter',
                            allowBlank : true,
                            hidden     : false,
                            id         : this._fieldMeter,
                            listeners  : {
                                select : function(combo, record, index) {
                                    this.entity.EntityType = 'Vizelia.FOL.BusinessEntities.Meter';
                                    this.entity.KeyEntity = record.get('KeyMeter');
                                    Ext.getCmp(this._fieldChart).clearValue();
                                    Ext.getCmp(this._fieldPlaylist).clearValue();
                                    // Ext.getCmp(this._fieldPortalTab).clearValue();

                                    this.updateFormFromType(this.entity.EntityType);

                                },
                                scope  : this
                            }
                        }, {
                            fieldLabel : $lang('msg_virtualfile_path'),
                            allowBlank : false,
                            name       : 'Path',
                            vtype      : 'virtualfilePath'
                        }, {
                            fieldLabel : $lang('msg_virtualfile_width'),
                            name       : 'Width',
                            xtype      : 'vizSpinnerField',
                            hidden     : false,
                            id         : this._fieldWidth,
                            minValue   : 640,
                            maxValue   : 4096,
                            allowBlank : false,
                            value      : 800
                        }, {
                            fieldLabel : $lang('msg_virtualfile_height'),
                            name       : 'Height',
                            id         : this._fieldHeight,
                            xtype      : 'vizSpinnerField',
                            hidden     : false,
                            minValue   : 480,
                            maxValue   : 4096,
                            allowBlank : false,
                            value      : 600
                        }, {
                            fieldLabel : $lang('msg_virtualfile_maxrecordcount'),
                            name       : 'MaxRecordCount',
                            id         : this._fieldMaxRecordCount,
                            xtype      : 'vizSpinnerField',
                            hidden     : true,
                            minValue   : 0,
                            maxValue   : 10000,
                            allowBlank : false,
                            value      : 1000
                        }];
            },

            onAfterRender      : function() {
                this.updateFormFromType('Vizelia.FOL.BusinessEntities.Chart');
            },

            updateFormFromType : function(type) {
                Ext.getCmp(this._fieldType).setValue(type);
                switch (type) {
                    case 'Vizelia.FOL.BusinessEntities.Meter' :
                        Ext.getCmp(this._fieldWidth).disable();
                        Ext.getCmp(this._fieldHeight).disable();
                        Ext.getCmp(this._fieldMaxRecordCount).enable();

                        Ext.getCmp(this._fieldWidth).hide();
                        Ext.getCmp(this._fieldHeight).hide();
                        Ext.getCmp(this._fieldMaxRecordCount).show();
                        break;

                    case 'Vizelia.FOL.BusinessEntities.Playlist' :
                    case 'Vizelia.FOL.BusinessEntities.Chart' :
                    case 'Vizelia.FOL.BusinessEntities.PortalTab' :
                        Ext.getCmp(this._fieldWidth).enable();
                        Ext.getCmp(this._fieldHeight).enable();
                        Ext.getCmp(this._fieldMaxRecordCount).disable();

                        Ext.getCmp(this._fieldWidth).show();
                        Ext.getCmp(this._fieldHeight).show();
                        Ext.getCmp(this._fieldMaxRecordCount).hide();
                        break;

                }
                switch (type) {
                    case 'Vizelia.FOL.BusinessEntities.Meter' :
                        Ext.getCmp(this._fieldMeter).show();
                        Ext.getCmp(this._fieldMeter).syncSize();
                        Ext.getCmp(this._fieldMeter).setWidth(Ext.getCmp(this._fieldType).getWidth());
                        Ext.getCmp(this._fieldChartParent).hide();
                        // Ext.getCmp(this._fieldPortalTab).hide();
                        Ext.getCmp(this._fieldPlaylist).hide();
                        break;

                    case 'Vizelia.FOL.BusinessEntities.Playlist' :
                        Ext.getCmp(this._fieldMeter).hide();
                        Ext.getCmp(this._fieldChartParent).hide();
                        // Ext.getCmp(this._fieldPortalTab).hide();
                        Ext.getCmp(this._fieldPlaylist).show();
                        break;
                    case 'Vizelia.FOL.BusinessEntities.Chart' :
                        Ext.getCmp(this._fieldMeter).hide();
                        Ext.getCmp(this._fieldChartParent).show();
                        //Ext.getCmp(this._fieldPortalTab).hide();
                        Ext.getCmp(this._fieldPlaylist).hide();
                        break;
                    case 'Vizelia.FOL.BusinessEntities.PortalTab' :
                        Ext.getCmp(this._fieldMeter).hide();
                        Ext.getCmp(this._fieldChartParent).hide();
                        // Ext.getCmp(this._fieldPortalTab).show();
                        Ext.getCmp(this._fieldPlaylist).hide();
                        break;
                }
                this.doLayout();
            },

            onAfterLoadValues  : function(form, values) {
                var form = this.getForm();

                if (values.EntityType) {
                    if (values.EntityType == 'Vizelia.FOL.BusinessEntities.Chart') {
                        Ext.getCmp(this._fieldChart).setValue(values.KeyEntity);
                    }
                    // else if (values.EntityType == 'Vizelia.FOL.BusinessEntities.PortalTab') {
                    // Ext.getCmp(this._fieldPortalTab).setValue(values.KeyEntity);
                    // }
                    else if (values.EntityType == 'Vizelia.FOL.BusinessEntities.Playlist') {
                        Ext.getCmp(this._fieldPlaylist).setValue(values.KeyEntity);
                    }
                    else if (values.EntityType == 'Vizelia.FOL.BusinessEntities.Meter') {
                        Ext.getCmp(this._fieldMeter).setValue(values.KeyEntity);
                    }
                    this.updateFormFromType(values.EntityType);
                }
            },

            onBeforeSave       : function(form, item) {
                if (Ext.isEmpty(item.KeyEntity) || Ext.isEmpty(item.EntityType)) {
                    Ext.getCmp(this._fieldChart).markInvalid();
                    // Ext.getCmp(this._fieldPortalTab).markInvalid();
                    Ext.getCmp(this._fieldPlaylist).markInvalid();
                    Ext.getCmp(this._fieldMeter).markInvalid();
                    return false;
                }
                return true;
            }
        });

Ext.reg('vizFormVirtualFile', Viz.form.VirtualFile);
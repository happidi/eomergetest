﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.PaletteColor
 * @extends Ext.FormPanel.
 */
Viz.form.DynamicDisplayImage = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.DynamicDisplayImage_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.DynamicDisplayImage_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.DynamicDisplayImage_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.DynamicDisplayImage.superclass.constructor.call(this, config);

                this.form.markInvalid = this.markInvalid;
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyDynamicDisplayImage',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_dynamicdisplayimage_name'),
                            name       : 'Name',
                            allowBlank : false

                        }, {
                            fieldLabel             : $lang('msg_dynamicdisplayimage_image'),
                            name                   : 'KeyImage',
                            xtype                  : 'vizFileUpload',
                            deleteButtonDisabled   : true,
                            acceptedFileExtensions : ['bmp', 'png', 'jpg'],
                            listeners              : {
                                fileselected : function(field, value) {
                                    if (!Ext.isEmpty(value)) {
                                        var form = this.getForm();
                                        var nameField = form.findField('Name');
                                        if (Ext.isEmpty(nameField.getValue())) {
                                            var name = value.substring(0, value.indexOf("."));
                                            if (name.lastIndexOf('\\') >= 0) {
                                                name = name.substring(name.lastIndexOf('\\') + 1);
                                            }
                                            nameField.setValue(name);
                                        }
                                    }
                                },
                                scope        : this
                            }
                        }, {
                            fieldLabel   : $lang('msg_dynamicdisplayimage_type'),
                            name         : 'Type',
                            allowBlank   : false,
                            sortByValues : true,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.DynamicDisplayImageType,Vizelia.FOL.Common'                            
                        }];
            },

            markInvalid       : function(errors) {
                if (Ext.isArray(errors)) {
                    for (var i = 0, len = errors.length; i < len; i++) {
                        var fieldError = errors[i];
                        if (fieldError.id === 'LocalId') {
                            fieldError.id = 'Name';
                        }
                        var f = this.findField(fieldError.id);
                        if (f) {
                            f.markInvalid(fieldError.msg);
                        }
                    }
                }
                else {
                    var field, id;
                    for (id in errors) {
                        if (!Ext.isFunction(errors[id]) && (field = this.findField(id))) {
                            field.markInvalid(errors[id]);
                        }
                    }
                }

                return this;
            }
        });

Ext.reg('vizFormDynamicDisplayImage', Viz.form.DynamicDisplayImage);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.MappingTask
 * @extends Ext.FormPanel.
 */
Viz.form.MappingTask = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                : function(config) {
                this.fileSourceTypeComboId = Ext.id();
                this.ftpSecurityTypeComboId = Ext.id();
                this.usernameTextFieldId = Ext.id();
                this.passwordTextFieldId = Ext.id();
                this.useCustomCredentialsFieldId = Ext.id();
                this.autoCreatedMetersKeyLocationId = Ext.id();
                this.autoCreatedMetersOptionsId = Ext.id();

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.MappingWCF.MappingTask_FormLoad,
                    serviceHandlerCreate : Viz.Services.MappingWCF.MappingTask_FormCreate,
                    serviceHandlerUpdate : Viz.Services.MappingWCF.MappingTask_FormUpdate,
                    items                : this.configFormGeneral(config),
                    listeners            : {
                        afterloadvalues : {
                            fn    : function() {
                                var t = Ext.getCmp(this.fileSourceTypeComboId);

                                if (this.mode == 'create') {
                                    t.setValue(Vizelia.FOL.BusinessEntities.FileSourceType.FTP);
                                    this.handleFileSourceTypeChange(Vizelia.FOL.BusinessEntities.FileSourceType.FTP);
                                }
                                else {
                                    var v = t.getValue();
                                    this.handleFileSourceTypeChange(v);
                                }

                                var autoCreatedMetersOptions = Ext.getCmp(this.autoCreatedMetersOptionsId);
                                this.toggleAutoCreateMetersCustomLocationAvailability(autoCreatedMetersOptions.getValue());
                            },
                            scope : this
                        }
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.MappingTask.superclass.constructor.call(this, config);
            },

            /**
             * General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral          : function(config) {
                return [{
                            name   : 'KeyMappingTask',
                            hidden : true
                        }, {
                            xtype      : 'fieldset',
                            title      : $lang('msg_mappingtask_general'),
                            labelWidth : 140,
                            defaults   : {
                                msgTarget : 'side',
                                anchor    : Viz.Const.UI.Anchor
                            },
                            items      : [{
                                        fieldLabel : $lang('msg_name'),
                                        xtype      : 'textfield',
                                        name       : 'Name',
                                        width      : 300

                                    }, {
                                        xtype      : 'textarea',
                                        anchor     : '-20',
                                        fieldLabel : $lang('msg_description'),
                                        name       : 'Description',
                                        height     : 40
                                    }, {
                                        fieldLabel : $lang('msg_email_results_to'),
                                        name       : 'Emails',
                                        allowBlank : true,
                                        xtype      : 'textarea',
                                        height     : 40
                                    }]
                        },
                             {
                    xtype: 'fieldset',
                    title: $lang('msg_meter_import_options'),
                    labelWidth: 140,
                    defaults: {
                        msgTarget: 'side',
                        anchor: Viz.Const.UI.Anchor
                    },
                    items: [
                        {
                            fieldLabel: $lang('msg_auto_create_meters'),
                            name: 'AutoCreateMeterOptions',
                            id: this.autoCreatedMetersOptionsId,
                            allowBlank: false,
                            xtype        : 'vizComboEnum',
                            enumTypeName: 'Vizelia.FOL.BusinessEntities.AutoCreateMeterOptions,Vizelia.FOL.Common',
                            value : Vizelia.FOL.BusinessEntities.AutoCreateMeterOptions.Off,
                            listeners    : {
                                select: function (sender, record) {
                                    this.toggleAutoCreateMetersCustomLocationAvailability(record.data.Value);
                                },
                                scope  : this
                            }
                        },
                        {
                            fieldLabel: $lang('msg_custom_location'),
                            xtype: 'viztreecomboSpatial',
                            validator: Ext.form.VTypes.locationValidator,
                            id: this.autoCreatedMetersKeyLocationId,
                            name: 'AutoCreatedMetersKeyLocation',
                            allowBlank: false,
                            ExcludeMetersAndAlarms: true,
                            originalDisplayValue: 'AutoCreatedMetersLocationLongPath',
                            disabled: true
                        }
                    ]
                },

                        {
                            xtype      : 'fieldset',
                            title      : $lang('msg_mappingtask_connection'),
                            labelWidth : 140,
                            defaults   : {
                                msgTarget : 'side',
                                anchor    : Viz.Const.UI.Anchor
                            },
                            items      : [{
                                        fieldLabel   : $lang('msg_file_source_type'),
                                        name         : 'FileSourceType',
                                        id           : this.fileSourceTypeComboId,
                                        hiddenName   : 'FileSourceType',
                                        allowBlank   : false,
                                        xtype        : 'vizComboEnum',
                                        enumTypeName : 'Vizelia.FOL.BusinessEntities.FileSourceType,Vizelia.FOL.Common',
                                        listeners    : {
                                            select : function(sender, record, index) {
                                                if (record && record.data && Ext.isInt(record.data.Value)) {
                                                    this.handleFileSourceTypeChange(record.data.Value);
                                                }
                                            },
                                            scope  : this
                                        }
                                    }, {
                                        fieldLabel : $lang('msg_address'),
                                        xtype      : 'textfield',
                                        name       : 'Address',
                                        allowBlank : false
                                    }]
                        }, {
                            xtype      : 'fieldset',
                            title      : $lang('msg_mappingtask_authentication'),
                            labelWidth : 140,
                            defaults   : {
                                msgTarget : 'side',
                                anchor    : Viz.Const.UI.Anchor
                            },
                            items      : [{
                                        fieldLabel        : $lang('msg_use_custom_credentials'),
                                        name              : 'UseCustomCredentials',
                                        xtype             : 'checkbox',
                                        id                : this.useCustomCredentialsFieldId,
                                        listeners         : {
                                            check : {
                                                fn    : function(sender, isChecked) {
                                                    var usernameBox = Ext.getCmp(this.usernameTextFieldId);
                                                    var passwordBox = Ext.getCmp(this.passwordTextFieldId);

                                                    if (isChecked) {
                                                        usernameBox.enable();
                                                        passwordBox.enable();
                                                    }
                                                    else {
                                                        usernameBox.disable();
                                                        passwordBox.disable();
                                                    }
                                                },
                                                scope : this
                                            }
                                        },
                                        fileSourceRelated : [Vizelia.FOL.BusinessEntities.FileSourceType.UNC, Vizelia.FOL.BusinessEntities.FileSourceType.FTP]
                                    }, {
                                        fieldLabel        : $lang('msg_sftp_authentication_mode'),
                                        name              : 'SftpAuthenticationMode',
                                        allowBlank        : true,
                                        xtype             : 'vizComboEnum',
                                        enumTypeName      : 'Vizelia.FOL.BusinessEntities.SftpAuthenticationMode,Vizelia.FOL.Common',
                                        value             : Vizelia.FOL.BusinessEntities.SftpAuthenticationMode.Password,
                                        sortByValues      : true,
                                        fileSourceRelated : Vizelia.FOL.BusinessEntities.FileSourceType.SFTP
                                    }, {
                                        fieldLabel : $lang('msg_username'),
                                        xtype      : 'textfield',
                                        id         : this.usernameTextFieldId,
                                        name       : 'Username',
                                        disabled   : true
                                    }, {
                                        fieldLabel : $lang('msg_password'),
                                        xtype: 'textfield',
                                        id         : this.passwordTextFieldId,
                                        name       : 'Password',
                                        disabled   : true
                                    }, {
                                        fieldLabel        : $lang('msg_ftp_security_type'),
                                        name              : 'FtpSecurityType',
                                        id                : this.ftpSecurityTypeComboId,
                                        allowBlank        : false,
                                        xtype             : 'vizComboEnum',
                                        enumTypeName      : 'Vizelia.FOL.BusinessEntities.FtpSecurityType,Vizelia.FOL.Common',
                                        value             : Vizelia.FOL.BusinessEntities.FtpSecurityType.None,
                                        sortByValues      : true,
                                        fileSourceRelated : Vizelia.FOL.BusinessEntities.FileSourceType.FTP
                                    }, {
                                        fieldLabel        : $lang('msg_private_key'),
                                        name              : 'PrivateKey',
                                        xtype             : 'textarea',
                                        anchor            : '-20',
                                        height            : 40,
                                        fileSourceRelated : Vizelia.FOL.BusinessEntities.FileSourceType.SFTP
                                    }, {
                                        fieldLabel        : $lang('msg_private_key_password'),
                                        xtype             : 'textfield',
                                        name              : 'PrivateKeyPassword',
                                        fileSourceRelated : Vizelia.FOL.BusinessEntities.FileSourceType.SFTP
                                    }]
                        }];
            },

            handleFileSourceTypeChange : function(fileSourceValue) {
                Ext.each(this.form.items.items, function(item) {
                            if (item && item.fileSourceRelated) {
                                if (item.fileSourceRelated === fileSourceValue || ((Ext.isArray(item.fileSourceRelated)) && (item.fileSourceRelated.indexOf(fileSourceValue) != -1))) {
                                    item.show();
                                }
                                else {
                                    item.hide();
                                }
                            }
                        });

                switch (fileSourceValue) {
                    case Vizelia.FOL.BusinessEntities.FileSourceType.UNC :
                        // Do nothing for now.
                        break;
                    case Vizelia.FOL.BusinessEntities.FileSourceType.FTP :
                        // Do nothing for now.
                        break;
                    case Vizelia.FOL.BusinessEntities.FileSourceType.SFTP :
                        Ext.getCmp(this.useCustomCredentialsFieldId).setValue(true);
                        break;
                }
            },
            toggleAutoCreateMetersCustomLocationAvailability: function (selectedAutoCreateOption) {
                if (selectedAutoCreateOption == null) {
                    return;
                }

                var autoCreatedMetersKeyLocation = Ext.getCmp(this.autoCreatedMetersKeyLocationId);

                switch (selectedAutoCreateOption) {
                    case Vizelia.FOL.BusinessEntities.AutoCreateMeterOptions.AutoCreateInCustomLocation:
                        autoCreatedMetersKeyLocation.enable();
                        autoCreatedMetersKeyLocation.setReadOnly(false);
                        break;
                    case Vizelia.FOL.BusinessEntities.AutoCreateMeterOptions.Off:
                    case Vizelia.FOL.BusinessEntities.AutoCreateMeterOptions.AutoCreateInDefaultLocation:
                    default:
                        autoCreatedMetersKeyLocation.disable();
                        autoCreatedMetersKeyLocation.setReadOnly(true);
                       
                }
            }
        });

Ext.reg('vizFormMappingTask', Viz.form.MappingTask);
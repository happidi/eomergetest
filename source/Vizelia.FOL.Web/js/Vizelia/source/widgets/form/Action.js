﻿
/**
 * @class Ext.form.Action.SubmitJson
 * @extends Ext.form.ActionSubmit A class which handles submission of JSON data from {@link Ext.form.BasicForm Form}s to a WCF service endpoint and processes the returned response. <br>
 * <br>
 * Instances of this class are only created by a {@link Ext.form.BasicForm Form} when submitting using the <b>submitJson</b> method. <br>
 * <br>
 * A response packet must contain a boolean <tt style="font-weight:bold">success</tt> property, and, optionally an <tt style="font-weight:bold">errors</tt> property. The <tt style="font-weight:bold">errors</tt> property contains error messages for invalid fields. <br>
 * <br>
 * By default, response packets are assumed to be JSON, so a typical response packet may look like this: <br>
 * <br>
 * 
 * <pre><code>
 * {
 *     success: false,
 *     errors: {
 *         clientCode: &quot;Client not found&quot;,
 *         portOfLoading: &quot;This field must not be null&quot;
 *     }
 * }
 * </code></pre>
 * 
 * <br>
 * <br>
 * Other data may be placed into the response for processing the the {@link Ext.form.BasicForm}'s callback or event handler methods. The object decoded from this JSON is available in the {@link #result} property.
 */
Ext.form.Action.SubmitJson = Ext.extend(Ext.form.Action.Submit, {
            type : 'submitjson',

            // private
            run  : function() {
                var o = this.options;
                if (o.clientValidation === false || this.form.isValid()) {
                    Ext.Ajax.request(Ext.apply(this.createCallback(o), {
                                url      : this.getUrl(false),
                                method   : 'POST',
                                headers  : {
                                    'Content-Type' : 'application/json'
                                },
                                callback : o.callback,
                                params   : Ext.encode(Ext.applyIf(this.form.getValues(), o.extraParams)),
                                isUpload : this.form.fileUpload
                            }));
                }
                else if (o.clientValidation !== false) { // client validation
                    // failed
                    this.failureType = Ext.form.Action.CLIENT_INVALID;
                    this.form.afterAction(this, false);
                }
            }
        });

/**
 * @class Ext.form.Action.LoadWCF
 * @extends Ext.form.Action
 * <p>
 * A class which handles loading of data from a WCF service call into the Fields of an {@link Ext.form.BasicForm}.
 * </p>
 * <p>
 * Instances of this class are only created by a {@link Ext.form.BasicForm Form} when {@link Ext.form.BasicForm#load load}ing.
 * </p>
 * <p>
 * <u><b>Response Packet Criteria</b></u>
 * </p>
 * <p>
 * A response packet <b>must</b> contain: <div class="mdetail-params">
 * <ul>
 * <li><b><code>success</code></b> property : Boolean</li>
 * <li><b><code>data</code></b> property : Object</li>
 * <div class="sub-desc">The <code>data</code> property contains the values of Fields to load. The individual value object for each Field is passed to the Field's {@link Ext.form.Field#setValue setValue} method.</div></li>
 * </ul>
 * </div>
 * <p>
 * <u><b>JSON Packets</b></u>
 * </p>
 * <p>
 * By default, response packets are assumed to be JSON, so for the following form load call:
 * 
 * <pre><code>
 * var myFormPanel = new Ext.form.FormPanel({
 * title: 'Client and routing info',
 * items: [{
 * fieldLabel: 'Client',
 * name: 'clientName'
 * }, {
 * fieldLabel: 'Port of loading',
 * name: 'portOfLoading'
 * }, {
 * fieldLabel: 'Port of discharge',
 * name: 'portOfDischarge'
 * }]
 * });
 * myFormPanel.{@link Ext.form.FormPanel#getForm getForm}().{@link Ext.form.BasicForm#load load}({
 * serviceHandler: Viz.Services.TestWCF.GetFormResponse,
 * extraParams: {
 * consignmentRef: myConsignmentRef
 * },
 * failure: function(form, action() {
 * Ext.Msg.alert(&quot;Load failed&quot;, action.result.errorMessage);
 * }
 * });
 * </code></pre>
 */
Ext.form.Action.LoadWCF = Ext.extend(Ext.form.Action, {
            // private
            type              : 'loadwcf',

            /**
             * Ctor. We override the contructor to allow passing the argument waitMsg with 'true' value and get the default loading message.
             * @param {Ext.form.BasicForm} form
             * @param {Object} options
             */
            constructor       : function(form, options) {
                options = options || {};
                if (options.waitMsg === true)
                    options.waitMsg = Ext.LoadMask.prototype.msg;
                Ext.form.Action.LoadWCF.superclass.constructor.call(this, form, options);
            },

            // utility functions used internally
            getServiceHandler : function() {
                return this.options.serviceHandler || this.form.serviceHandler;
            },

            // private
            run               : function() {
                var o = this.options;
                var a = this.createCallback(o);
                Ext.applyIf(a, this.form.serviceParams);
                Ext.apply(a, Ext.apply(this.form.getFieldValues(), o.extraParams));
                this.getServiceHandler().call(this, a);
            },

            // private
            success           : function(response) {
                var result = response;
                if (result.errors || result.success === false) {
                    this.form.markInvalid(result.errors);
                    this.failureType = Ext.form.Action.LOAD_FAILURE;
                    this.form.afterAction(this, false);
                }
                else {
                    // this means that the server sent us back a valid form response.
                    this.result = response;
                    if (result.success === true && result.data) {
                        Ext.apply(this.form.entity, result.data);
                        this.form.clearInvalid();
                        this.form.setValues(result.data);
                        this.form.afterAction(this, true);
                    }
                    else {
                        // submit was successful but we didn't get a form response. Usually we will use the response in the success callback of the submitWCF.
                        if (Ext.isObject(result)) {
                            // if result is an object we try to set values field from it's properties.
                            this.form.setValues(result);
                        }
                        this.form.afterAction(this, true);
                    }
                }
            }

        });

/**
 * @class Ext.form.Action.SubmitWCF
 * @extends Ext.form.ActionSubmit A class which handles submission of JSON data from {@link Ext.form.BasicForm Form}s to a WCF service endpoint and processes the returned response. <br>
 * <p>
 * Instances of this class are only created by a {@link Ext.form.BasicForm Form} when submitting using the <b>submitJson</b> method. <br>
 * </p>
 * <p>
 * A response packet must contain a boolean <tt style="font-weight:bold">success</tt> property, and, optionally an <tt style="font-weight:bold">errors</tt> property. The <tt style="font-weight:bold">errors</tt> property contains error messages for invalid fields. <br>
 * </p>
 * By default, response packets are assumed to be JSON, so a typical response packet may look like this: <br>
 * 
 * <pre><code>
 * {
 *     success: false,
 *     errors: {
 *         clientCode: &quot;Client not found&quot;,
 *         portOfLoading: &quot;This field must not be null&quot;
 *     }
 * }
 * </code></pre>
 * 
 * <br>
 * Example:
 * 
 * <pre><code>
 * form.submitWCF({
 *             waitMsg     : true,
 *             extraParams : {
 *                 languagePair : form.findField('language_from').getValue() + '|' + form.findField('language_to').getValue()
 *             },
 *             success     : function(form, action) {
 *                 Ext.getCmp(this._outputid).setValue(action.result);
 *             },
 *             failure     : function(form, action) {
 *                 Ext.getCmp(this._outputid).setValue(action.response._message);
 *             },
 *             scope       : this
 *         });
 * </pre></code> Other data may be placed into the response for processing the the {@link Ext.form.BasicForm}'s callback or event handler methods. The object decoded from this JSON is available in the {@link #result} property.
 */
Ext.form.Action.SubmitWCF = Ext.extend(Ext.form.Action.Submit, {
            type              : 'submitwcf',

            /**
             * Ctor. We override the contructor to allow passing the argument waitMsg with 'true' value and get the default loading message.
             * @param {Ext.form.BasicForm} form
             * @param {Object} options
             */
            constructor       : function(form, options) {
                options = options || {};
                if (options.waitMsg === true)
                    options.waitMsg = Ext.LoadMask.prototype.msg;
                Ext.form.Action.SubmitWCF.superclass.constructor.call(this, form, options);
            },

            // utility functions used internally
            getServiceHandler : function() {
                return this.options.serviceHandler || this.form.serviceHandler;
            },

            // private
            run               : function() {
                var o = this.options;
                if (o.clientValidation === false || this.form.isValid()) {
                    var root;
                    var a = this.createCallback(o);
                    if (o.root) {
                        a[o.root] = {};
                        root = a[o.root];
                    }
                    else {
                        root = a;
                    }
                    Ext.applyIf(a, this.form.serviceParams);
                    if (o.bypassGetValues == true)
                        Ext.apply(root, o.extraParams);
                    else
                        Ext.apply(root, Ext.apply(this.form.getFieldValues(), o.extraParams));

                    this.callServiceMethod(a);
                }
                else if (o.clientValidation !== false) { // client validation
                    // failed
                    this.failureType = Ext.form.Action.CLIENT_INVALID;
                    this.form.afterAction(this, false);
                }
            },

            callServiceMethod : function(serviceMethodConfig) {
                this.getServiceHandler().call(this, serviceMethodConfig);
            },

            // private
            success           : function(response) {
                // case of IsOneWay WCF web service call.
                if (Ext.isEmpty(response)) {
                    this.form.afterAction(this, true);
                    return;
                }

                var result = response;
                this.result = result;
                if (result.errors || result.success === false) {
                    this.form.markInvalid(result.errors);
                    this.failureType = Ext.form.Action.SERVER_INVALID;
                    this.form.afterAction(this, false);
                }
                else {
                    // this means that the server sent us back a valid form response.
                    this.result = response;
                    if (result.success === true && result.data) {
                        this.form.entity = result.data;
                        this.form.clearInvalid();
                        this.form.setValues(result.data);
                        this.form.afterAction(this, true);
                    }
                    else {
                        // submit was successfull but we didn't get a form response. Usually we will use the response in the success callback of the submitWCF.
                        if (Ext.isObject(result)) {
                            // if result is an object we try to set values field from it's properties.
                            this.form.setValues(result);
                        }
                        this.form.afterAction(this, true);
                    }
                }

            }
        });

/**
 * @class Ext.form.Action.SubmitLongRunning
 * @extends Ext.form.Action.SubmitWCF A class which handles submission of mutlipart data from {@link Ext.form.BasicForm Form} to a WCF service endpoint and processes the returned response stream asynchronously. <br>
 * <br>
 * Instances of this class are only created by a {@link Ext.form.BasicForm Form} when submitting using the <b>submitLongRunning</b> method. <br>
 * A file (upload) can be submitted to the server. In addition the operationId variable is added to the list of parameters so a separate thread can check the status of the operation <br>
 * The response packet can be anything (binary).
 */
Ext.form.Action.SubmitLongRunning = Ext.extend(Ext.form.Action.SubmitWCF, {
            type              : 'submitlongrunning',

            constructor       : function(form, options) {
                Ext.form.Action.SubmitLongRunning.superclass.constructor.call(this, form, options);
            },

            /**
             * An override function that creates a callback that fits the LongRunningOperationSubmitter class.
             * @param {Object} opts Options
             * @return {Object} The callback object
             */
            createCallback    : function(opts) {
                // Create an empty callback since we don't use success and failure members, and we don't
                // mix them with parameters in the same object. We'll create our callbacks in the callServiceMethod function.
                return {};
            },

            // override
            callServiceMethod : function(serviceMethodConfig) {
                var submitterConfig = {
                    serviceStart       : this.getServiceHandler(),
                    completionHandler  : function(progressObject) {
                        // In case the result is a file download, only this handler will be called.
                        var result = {
                            success : true
                        };
                        this.success(result);
                    }.createDelegate(this),
                    resultHandler      : function(result) {
                        // In case the result is a business entity, only this handler will be called.
                        this.success(result);
                    }.createDelegate(this),
                    failureHandler     : function(response, options, exceptionMessage) {
                        if (response) {
                            Ext.MessageBox.alert('Error', response);
                        }
                        else if (exceptionMessage) {
                            Ext.MessageBox.alert('Error', exceptionMessage);
                        }
                        this.failure();
                    }.createDelegate(this),
                    cancelationHandler : function() {
                        this.failure();
                    }.createDelegate(this),
                    serviceParams      : serviceMethodConfig
                };

                Ext.apply(submitterConfig, this.options);
                Ext.apply(submitterConfig, this.form.longRunningSubmitConfig);

                // Send the form by using the MS AJAX proxy.
                var submitter = new Viz.Services.ObjectLongRunningOperationSubmitter(submitterConfig);

                submitter.start();
            }

        });

/**
 * @class Ext.form.Action.SubmitByForm
 * @extends Ext.form.Action.Submit A class which handles submission of of a {@link Ext.form.BasicForm Form} to a WCF service endpoint, by choosing an action depending on form configuration.<br>
 * If the form is specified as isUpload or isLongRunninSubmit, then a long-running operation submitting will take place. Otherwise, regular WCF submit will take place.<br>
 * Instances of this class are only created by a {@link Ext.form.BasicForm Form} when submitting using the <b>submitByForm</b> method. <br>
 * This is actually a kind of a factory and wrapper for form submit actions.
 */
Ext.form.Action.SubmitByForm = Ext.extend(Ext.form.Action.Submit, {
            type         : 'submitbyform',
            submitAction : null,

            constructor  : function(form, options) {
                options = options || {};

                // Choose a form submitting action by the form configuration.
                var actionName = (form.isLongRunningSubmit === true) ? 'submitlongrunning' : 'submitwcf';
                // Create the member form action so we can call it later.
                this.submitAction = new Ext.form.Action.ACTION_TYPES[actionName](form, options);

                Ext.form.Action.SubmitByForm.superclass.constructor.call(this, form, options);
            },

            // private
            run          : function() {
                // Call the member form action.
                this.submitAction.run();
            }
        });

/**
 * @class Ext.form.Action.SubmitDocument
 * @extends Ext.form.Action.Submit A class which handles submission of a file from {@link Ext.form.BasicForm Form} to a WCF service endpoint and processes the returned response.<br>
 * <br>
 * Instances of this class are only created by a {@link Ext.form.BasicForm Form} when submitting using the <b>submitDocument</b> method. <br>
 * A file (upload) can be submitted to the server. <br>
 * The response packet should have the guid for the next request to find the file(binary).
 */
Ext.form.Action.SubmitDocument = Ext.extend(Ext.form.Action.Submit, {
            type           : 'submitdocument',
            handleResponse : function(response) {
                var result = Ext.form.Action.SubmitDocument.superclass.handleResponse.call(this, response);
                return result.d;
            }
        });

/**
 * @class Ext.form.Action.SubmitAsPost
 * @extends Ext.form.Action.Submit
 */
Ext.form.Action.SubmitAsPost = Ext.extend(Ext.form.Action.Submit, {
            type        : 'submitaspost',

            /**
             * Ctor. We override the contructor to allow passing the argument waitMsg with 'true' value and get the default loading message.
             * @param {Ext.form.BasicForm} form
             * @param {Object} options
             */
            constructor : function(form, options) {
                options = options || {};
                if (options.waitMsg === true)
                    options.waitMsg = Ext.LoadMask.prototype.msg;
                Ext.form.Action.SubmitAsPost.superclass.constructor.call(this, form, options);
            },

            // private
            run         : function() {
                var o = this.options;

                if (o.clientValidation === false || this.form.isValid()) {
                    // We take the object values and create a form with fields.
                    var item = this.form.getFieldValues();
                    var formFields = [];

                    for (key in item) {
                        var value = item[key];

                        if (Ext.isDate(value)) {
                            // Format the date for sending
                            value = value.format(Date.patterns.SortableDateTime);
                        }

                        formFields.push({
                                    inputType : 'hidden',
                                    name      : key,
                                    value     : value
                                });
                    }

                    var tempForm = new Ext.form.FormPanel({
                                renderTo       : Ext.getBody(),
                                hidden         : true,
                                standardSubmit : true,
                                labelWidth     : 150,
                                defaultType    : 'textfield',
                                items          : formFields
                            });

                    var baseForm = tempForm.getForm();
                    var formDom = baseForm.getEl().dom;

                    formDom.method = 'POST';
                    formDom.action = o.action;
                    formDom.target = o.target;

                    baseForm.submit();

                    Ext.getBody().remove(tempForm);

                    this.form.afterAction(this, true);
                }
                else if (o.clientValidation !== false) { // client validation failed
                    this.failureType = Ext.form.Action.CLIENT_INVALID;
                    this.form.afterAction(this, false);
                }
            }
        });

Ext.override(Ext.form.BasicForm, {
            /**
             * Submits form fields as json
             */
            submitJson        : function(options) {
                this.doAction('submitjson', options);
                return this;
            },

            /**
             * Submits form fields as a long running operation
             */
            submitLongRunning : function(options) {
                this.doAction('submitlongrunning', options);
                return this;
            },

            /**
             * Submits form fields as a form POST to the URL of the given action and to the frame of the given target
             */
            submitAsPost      : function(options) {
                this.doAction('submitaspost', options);
                return this;
            },

            /**
             * Submits the form by its configuration - either by WCF or by long-running
             */
            submitByForm      : function(options) {
                this.doAction('submitbyform', options);
                return this;
            },

            /**
             * Submits a file
             */
            submitDocument    : function(options) {
                this.doAction('submitdocument', options);
                return this;
            },

            /**
             * Loads form fields from a WCF service
             */
            loadWCF           : function(options) {
                this.doAction('loadwcf', options);
            },
            /**
             * Submits form fields to WCF service.
             */
            submitWCF         : function(options) {
                this.doAction('submitwcf', options);
                return this;
            }
        });

Ext.apply(Ext.form.Action.ACTION_TYPES, {
            'submitjson'        : Ext.form.Action.SubmitJson,
            'submitlongrunning' : Ext.form.Action.SubmitLongRunning,
            'submitwcf'         : Ext.form.Action.SubmitWCF,
            'submitbyform'      : Ext.form.Action.SubmitByForm,
            'loadwcf'           : Ext.form.Action.LoadWCF,
            'submitdocument'    : Ext.form.Action.SubmitDocument,
            'submitaspost'      : Ext.form.Action.SubmitAsPost
        });

﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Meter
 * @extends Ext.FormPanel.
 */
Viz.form.Meter = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                    : function(config) {
                this._tabGeneral = Ext.id();
                this._tabMeterOperation = Ext.id();
                this._tabMeterOperationWizard = Ext.id();
                this._gridMeterOperation = Ext.id();
                this._comboCalendarEventCategoryTitleId = Ext.id();
                this._checkboxIsEndOfTimeIntervalId = Ext.id();

                var Id = config.entity ? config.entity.KeyMeter : null;
                this._psetId = Ext.id();

                this.storeMeterOperation = new Viz.store.MeterOperation({
                            KeyMeter : Id
                        });

                // we need an empty meter store for the meteroperation wizard.
                this.storeMeterWizard = new Viz.store.Meter({
                            serviceHandlerCrud : null,
                            baseParams         : {
                                filters : [{
                                            data  : {
                                                dataIndex : 'KeyMeter',
                                                type      : 'innerjoin',
                                                value     : ' '
                                            },
                                            field : 'KeyMeter'

                                        }]
                            }
                        });
                this.storeCalendarEventCategoryTitle = new Viz.store.CalendarEventCategoryTitle({
                            KeyMeter                 : Id,
                            KeyCalendarEventCategory : ''
                        });

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.Meter_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.Meter_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.Meter_FormUpdate,
                    psetConfig           : {
                        IfcType                    : 'IfcMeter',
                        entityKey                  : 'KeyMeter',
                        filterByClassificationItem : false
                    },
                    hasValidationToolbar : true,
                    tabConfig            : {
                        // TODO : Fix this. because otherwise the datetime component doesnt work.
                        deferredRender : false,
                        items          : [{
                                    title      : $lang('msg_general'),
                                    id         : this._tabGeneral,
                                    iconCls    : 'viz-icon-small-meter',
                                    items      : this.configFormGeneral(config),
                                    labelWidth : 180
                                }, {
                                    title   : $lang('msg_meter_meteroperations_wizard'),
                                    hidden  : config.mode == 'update',
                                    id      : this._tabMeterOperationWizard,
                                    iconCls : 'viz-icon-small-meteroperation',
                                    items   : this.configGridMeterOperationWizard(config)
                                }, {
                                    title      : $lang('msg_advanced'),
                                    iconCls    : 'viz-icon-small-advanced',
                                    items      : this.configFormAdvanced(config),
                                    labelWidth : 250
                                }, {
                                    title      : $lang('msg_meter_meteroperations'),
                                    hidden     : config.mode == 'create',
                                    id         : this._tabMeterOperation,
                                    iconCls    : 'viz-icon-small-meteroperation',
                                    items      : this.configGridMeterOperation(config),
                                    labelWidth : 150
                                }]
                    },
                    labelWidth           : 180,
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.Meter.superclass.constructor.call(this, config);
            },
            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral              : function(config) {
                return [{
                            name   : 'KeyMeter',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_localid'),
                            allowBlank : false,
                            name       : 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_name'),
                            allowBlank : false,
                            name       : 'Name'
                        }, {
                            xtype      : 'textarea',
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description'
                        }, {
                            fieldLabel             : $lang('msg_location_path'),
                            xtype                  : 'viztreecomboSpatial',
                            validator              : Ext.form.VTypes.locationValidator,
                            name                   : 'KeyLocation',
                            allowBlank             : false,
                            ExcludeMetersAndAlarms : true,
                            originalDisplayValue   : 'LocationLongPath'
                        }, {
                            fieldLabel           : $lang('msg_meter_classification'),
                            xtype                : 'viztreecomboClassificationItemMeter',
                            name                 : 'KeyClassificationItem',
                            originalDisplayValue : 'ClassificationItemLongPath',
                            pathField            : 'KeyClassificationItemPath',
                            pathStepField        : 'Key',
                            allowBlank           : false,
                            listeners            : {
                                changevalue : function(field, value) {
                                    return;
                                },
                                scope       : this
                            }
                        }, {
                            xtype      : 'vizcompositefield',
                            fieldLabel : $lang('msg_meter_expected_interval'),
                            items      : [{
                                        fieldLabel    : $lang('msg_meter_expected_interval'),
                                        name          : 'FrequencyInput',
                                        xtype         : 'spinnerfield',
                                        flex          : 1,
                                        allowBlank    : true,
                                        allowDecimals : false,
                                        allowNegative : false,
                                        maxValue      : 2147483647,
                                        minValue      : 1
                                    }, {
                                        name         : 'ManualInputTimeInterval',
                                        xtype        : 'vizComboEnum',
                                        enumTypeName : 'Vizelia.FOL.BusinessEntities.ManualInputTimeInterval,Vizelia.FOL.Common',
                                        flex         : 4,
                                        sortByValues : true,
                                        displayIcon: true,
                                        allowBlank: false,
                                        // hidden : for non admins
                                value: Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Minutes
                                    }]
                        }, {
                            fieldLabel   : $lang('msg_meter_groupoperation'),
                            name         : 'GroupOperation',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.MathematicOperator,Vizelia.FOL.Common',
                            flex         : 1,
                            value        : Vizelia.FOL.BusinessEntities.MathematicOperator.SUM
                        }, {
                            xtype      : 'vizcompositefield',
                            fieldLabel : $lang('msg_meter_factor'),
                            items      : [{
                                        fieldLabel       : $lang('msg_meter_factor'),
                                        xtype            : 'viznumberfield',
                                        name             : 'Factor',
                                        flex             : 1,
                                        value            : 1,
                                        decimalPrecision : 5,
                                        width            : 50

                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_meter_offset') + ':'
                                    }, {
                                        fieldLabel       : $lang('msg_meter_offset'),
                                        xtype            : 'viznumberfield',
                                        name             : 'Offset',
                                        flex             : 1,
                                        value            : 0,
                                        decimalPrecision : 5,
                                        width            : 50
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_meter_unitinput') + ':'
                                    }, {
                                        fieldLabel : $lang('msg_meter_unitinput'),
                                        xtype      : 'textfield',
                                        name       : 'UnitInput',
                                        flex       : 1
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_meter_unitoutput') + ':'
                                    }, {
                                        fieldLabel : $lang('msg_meter_unitoutput'),
                                        xtype      : 'textfield',
                                        name       : 'UnitOutput',
                                        flex       : 1
                                    }]
                        }, {
                            xtype      : 'vizcompositefield',
                            fieldLabel : $lang('msg_meter_psetfactor'),
                            items      : [{

                                        fieldLabel           : $lang('msg_meter_psetfactor'),
                                        name                 : 'PsetFactor',
                                        xtype                : 'viztreecomboPset',
                                        allowBlank           : true,
                                        id                   : this._psetId,
                                        flex                 : 3,
                                        originalDisplayValue : 'PropertySingleValueLongPath',
                                        validator            : Ext.form.VTypes.psetAttributeSelectedValidator,
                                        listeners            : {
                                            select : function() {
                                                // we need to defer because the validator is called also with a defer
                                                this.onChangeValuePset.defer(500, this, arguments);
                                            },
                                            clear  : function() {
                                                this.onClearValuePset();
                                            },
                                            scope  : this
                                        }
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_chartfilterspatialpset_operator') + ':'
                                    }, {
                                        fieldLabel     : $lang('msg_chartfilterspatialpset_operator'),
                                        name           : 'PsetFactorOperator',
                                        xtype          : 'vizComboEnum',
                                        enumTypeName   : 'Vizelia.FOL.BusinessEntities.ArithmeticOperator,Vizelia.FOL.Common',
                                        displayIcon    : true,
                                        allowBlank     : false,
                                        flex           : 1,
                                        value          : Vizelia.FOL.BusinessEntities.ArithmeticOperator.Multiply,
                                        valuesToRemove : ['None', 'Add', 'Subtract', 'Power']
                                    }]
                        }, {
                            fieldLabel : $lang('msg_meter_isaccumulated'),
                            name       : 'IsAccumulated',
                            xtype      : 'vizcheckbox',
                            flex       : 1
                        }, {
                            fieldLabel : $lang('msg_meter_isacquisitiondatetimeendinterval'),
                            name       : 'IsAcquisitionDateTimeEndInterval',
                            xtype      : 'vizcheckbox',
                            checked    : true,
                            id         : this._checkboxIsEndOfTimeIntervalId,
                            hidden     : config.entity.IsVirtual === true,
                            flex       : 1
                        }];
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormAdvanced             : function(config) {
                return [{
                            fieldLabel : $lang('msg_meter_endpoint'),
                            hiddenName : 'KeyEndpoint',
                            name       : 'KeyEndpoint',
                            xtype      : 'vizComboDataAcquisitionEndpoint',
                            listeners  : {
                                select : function(combo, record, index) {
                                    this.entity.EndpointType = Viz.getEntityType(record.json.__type);
                                },
                                scope  : this
                            }
                        }, {
                            fieldLabel : $lang('msg_meter_endpoint_frequency'),
                            xtype      : 'viznumberfield',
                            name       : 'EndpointFrequency',
                            flex       : 1,
                            allowBlank : true
                        }, {
                            fieldLabel : $lang('msg_meter_calibrationdatetime'),
                            name       : 'CalibrationDateTime',
                            hidden     : true,
                            hideTime   : false,
                            xtype      : 'vizDateTime',
                            allowBlank : true,
                            hasSwitch  : false
                        }, {
                            fieldLabel : $lang('msg_meter_calibrationvalue'),
                            xtype      : 'viznumberfield',
                            hidden     : true,
                            name       : 'CalibrationValue'
                        }, {
                            fieldLabel : $lang('msg_meter_rollovervalue'),
                            xtype      : 'viznumberfield',
                            name       : 'RolloverValue',
                            maxValue   : 10000000000
                        }, {
                            fieldLabel : $lang('msg_meter_acceptmobileinput'),
                            name       : 'AcceptMobileInput',
                            xtype      : 'vizcheckbox',
                            hidden     : true
                        }, {
                            fieldLabel   : $lang('msg_meter_degreeday_type'),
                            name         : 'DegreeDayType',
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.DegreeDayType,Vizelia.FOL.Common',
                            sortByValues : true,
                            displayIcon  : false,
                            value        : 0
                            // Vizelia.FOL.BusinessEntities.DegreeDayType.None
                    }   , {
                            fieldLabel : $lang('msg_meter_degreeday_base'),
                            name       : 'DegreeDayBase',
                            xtype      : 'viznumberfield',
                            allowBlank : true
                        }];
            },

            /**
             * The configuration for the grid calculatedseries.
             * @return {object}
             */
            configGridMeterOperation       : function(config) {
                return [{

                    fieldLabel   : $lang('msg_meter_operation_timescale_interval'),
                    name         : 'MeterOperationTimeScaleInterval',
                    xtype        : 'vizComboEnum',
                    enumTypeName : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common',
                    sortByValues : true,
                    displayIcon  : true,
                    value        : Vizelia.FOL.BusinessEntities.AxisTimeInterval.None
                        // ,
                        // valuesToRemove : ['None']
                    }, {
                    fieldLabel : $lang('msg_chart_timezone'),
                    name       : 'TimeZoneId',
                    allowBlank : true,
                    xtype      : 'vizComboTimeZone'
                }, {
                    xtype     : 'vizGridMeterOperation',
                    anchor    : Viz.Const.UI.Anchor + ' -100',
                    cls       : 'x-form-item',
                    id        : this._gridMeterOperation,
                    border    : true,
                    store     : this.storeMeterOperation,
                    loadMask  : false,
                    KeyMeter  : config.entity.KeyMeter,
                    listeners : {
                        beforedeleterecord : function(grid, records) {
                            this.deleteSelectedRowsFromGrid(this._gridMeterOperation);
                            var currentKeyMeterOperationResult = this.form.findField("KeyMeterOperationResult").value;

                            // delete MeterOperationResult field value in case its corresponding meter operation has been deleted.
                            Ext.each(records, function(r) {
                                if (r.data.KeyMeterOperation == currentKeyMeterOperationResult) {
                                    this.form.findField("KeyMeterOperationResult").clearValue();
                                }
                            }, this);
                            return false;
                        },
                        scope              : this
                    }
                }, {
                    fieldLabel        : $lang('msg_meter_operation_result'),
                    xtype             : 'vizComboMeterOperation',
                    store             : this.storeMeterOperation,
                    name              : 'KeyMeterOperationResult',
                    hiddenName        : 'KeyMeterOperationResult',
                    allowBlank        : true,
                    KeyMeter          : config.entity.KeyMeter,
                    KeyMeterOperation : ''
                }, {
                    xtype      : 'vizcompositefield',
                    fieldLabel : $lang('msg_calendar'),
                    items      : [{
                                fieldLabel : $lang('msg_calendar'),
                                name       : 'KeyCalendarEventCategory',
                                valueField : 'KeyCalendarEventCategory',
                                allowBlank : true,
                                xtype      : 'vizComboCalendarEventCategory',
                                flex       : 1,
                                listeners  : {
                                    select : {
                                        fn    : this.onSelectCalendarEventCategory,
                                        scope : this
                                    }
                                }
                            }, {
                                id         : this._comboCalendarEventCategoryTitleId,
                                fieldLabel : $lang('msg_calendar'),
                                name       : 'CalendarEventCategoryTitle',
                                disabled   : !(config.entity.CalendarMode == Vizelia.FOL.BusinessEntities.ChartCalendarMode.SplitByEvent),
                                allowBlank : false,
                                xtype      : 'vizComboCalendarEventCategoryTitle',
                                store      : this.storeCalendarEventCategoryTitle,
                                flex       : 1
                            }, {
                                name         : 'CalendarMode',
                                xtype        : 'vizComboEnum',
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartCalendarMode,Vizelia.FOL.Common',
                                value        : 0,
                                flex         : 1,
                                allowBlank   : false,
                                listeners    : {
                                    select : {
                                        fn    : this.onSelectCalendarMode,
                                        scope : this
                                    }
                                }
                            }]
                }];
            },

            /**
             * Listener for the Calendar Event Category select event.
             * @param {} combo
             * @param {} record
             * @param {} index
             */
            onSelectCalendarEventCategory  : function(combo, record, index) {
                var category = record.data;
                var combo = Ext.getCmp(this._comboCalendarEventCategoryTitleId);

                combo.clearValue();

                this.updateCalendarEventTitleStore(combo.store, this.entity.KeyMeter, category.KeyCalendarEventCategory);
                combo.store.reload();
            },

            updateCalendarEventTitleStore  : function(store, KeyMeter, KeyCalendarEventCategory) {
                store.proxy.serviceParams.KeyCalendarEventCategory = KeyCalendarEventCategory;
                store.serviceParams.KeyCalendarEventCategory = KeyCalendarEventCategory;
                store.proxy.serviceParams.KeyMeter = KeyMeter;
                store.serviceParams.KeyMeter = KeyMeter;
            },

            /**
             * Listener for the Calendar mode select event.
             * @param {} combo
             * @param {} record
             * @param {} index
             */
            onSelectCalendarMode           : function(combo, record, index) {
                var mode = record.data;
                var combo = Ext.getCmp(this._comboCalendarEventCategoryTitleId);
                if (mode.Value == Vizelia.FOL.BusinessEntities.ChartCalendarMode.SplitByEvent) {
                    combo.enable();
                }
                else {
                    combo.disable();
                }
            },
            /**
             * The configuration object for the detail grid Meter for the MeterOperation Wizard.
             * @return {Object}
             */
            configGridMeterOperationWizard : function(config) {
                var retVal = this.configGridDetail({
                            name                         : 'Meter',
                            windowTitle                  : $lang('msg_meter'),
                            windowWidth                  : 1000,
                            windowHeight                 : 400,
                            windowIconCls                : 'viz-icon-small-meter',
                            xtypeGridDetail              : 'vizGridMeter',
                            columnsGridDetail            : Viz.Configuration.Columns.ChartMeter,
                            buttonAddTitle               : $lang('msg_meter_add'),
                            buttonAddIconCls             : 'viz-icon-small-add',
                            buttonDeleteTitle            : $lang('msg_meter_delete'),
                            buttonDeleteIconCls          : 'viz-icon-small-delete',
                            storeDetail                  : this.storeMeterWizard,
                            keyColumn                    : 'KeyMeter',
                            ddGroupGridDetail            : 'SpatialDD',
                            enableDropFromTreeGridDetail : true
                        });

                Ext.apply(retVal, {
                            anchor : Viz.Const.UI.GridAnchor + ' -80'
                        });
                return [{
                            xtype     : 'displayfield',
                            hideLabel : true,
                            value     : $img('viz-icon-small-information') + $lang('msg_meter_meteroperation_wizard_description')
                        }, retVal];
            },

            /**
             * Handler for updating PropertySingleValueLongPath when a the KeyPropertySingleValue is changed.
             */
            onChangeValuePset              : function() {
                var combo = Ext.getCmp(this._psetId);
                var node = combo.getTree().getSelectionModel().getSelectedNode();
                if (node && Ext.isEmpty(combo.getActiveError())) {
                    this.entity.PropertySingleValueLongPath = node.getPath('text');
                    this.entity.PsetName = node.parentNode.attributes.entity.PsetName;
                    this.entity.AttributeName = node.attributes.entity.AttributeName;
                    this.isDirty = true;
                }
            },

            onClearValuePset               : function() {
                this.entity.PropertySingleValueLongPath = null;
                this.entity.PsetName = null;
                this.entity.AttributeName = null;
                this.isDirty = true;
            },

            /**
             * Handler for updating fiels based on the form values.
             * @param {Viz.form.FormPanel} the form panel
             * @param {Object} values
             */
            onAfterLoadValues              : function(form, values) {
                if (!Ext.isEmpty(values.KeyCalendarEventCategory)) {
                    this.updateCalendarEventTitleStore(this.storeCalendarEventCategoryTitle, values.KeyMeter, values.KeyCalendarEventCategory);
                    this.storeCalendarEventCategoryTitle.reload();
                }
                if (!Ext.isEmpty(values.PropertySingleValueLongPath)) {
                    Ext.getCmp(this._psetId).setValue(values.PropertySingleValueLongPath);
                }
            },

            /**
             * Before save handler.
             * @param {Viz.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                   : function(form, item, extraParams) {
                if (this.mode == 'create') {
                    if (this.storeMeterWizard.hasLoaded)
                        this.storeMeterWizard.save();
                    Ext.apply(extraParams, {
                                meterOperationsWizard : this.storeMeterWizard.crudStore
                            });
                }
                if (this.mode == 'update') {
                    if (this.storeMeterOperation.hasLoaded)
                        this.storeMeterOperation.save();
                    Ext.apply(extraParams, {
                                meterOperations : this.storeMeterOperation.crudStore
                            });
                }

                return true;
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                  : function(form, action) {
                Viz.form.Meter.superclass.onSaveSuccess.apply(this, arguments);
                var tabpanel = this.getTabContainer();

                if (tabpanel.getActiveTab() == Ext.getCmp(this._tabMeterOperationWizard))
                    tabpanel.setActiveTab(Ext.getCmp(this._tabGeneral));

                tabpanel.hideTabStripItem(this._tabMeterOperationWizard);
                tabpanel.unhideTabStripItem(this._tabMeterOperation);

                var grid = Ext.getCmp(this._gridMeterOperation);
                grid.KeyMeter = grid.store.serviceParams.KeyMeter = this.entity.KeyMeter;
                grid.store.reload();

                if (this.entity.IsVirtual) {
                    Ext.getCmp(this._checkboxIsEndOfTimeIntervalId).hide();
                }

            }
        });
Ext.reg('vizFormMeter', Viz.form.Meter);

﻿Ext.namespace("Viz.form");

/**
 * @class Viz.form.FileUploadField
 * @extends Ext.ux.form.FileUploadField Creates a file upload field with download/delete buttons.
 * @xtype vizFileUpload
 */

Viz.form.FileUploadField = Ext.extend(Ext.ux.form.FileUploadField, {
            /**
             * @cfg {String} buttonText The button text to display on the upload button (defaults to 'Browse...'). Note that if you supply a value for {@link #buttonCfg}, the buttonCfg.text value will be used instead if available.
             */
            buttonText             : $lang('msg_document_browse'),
            /**
             * @cfg {String} downloadButtonText The button text to display on the download button (defaults to 'Download File').
             */
            downloadButtonText     : '',

            /**
             * @cfg {String} deleteButtonText The button text to display on the delete button (defaults to 'Delete File').
             */
            deleteButtonText       : '',
            /**
             * @cfg {Boolean} deleteButtonDisabled Controls if the delete button is disabled, we need this for forms where the image is mandatory (defaults to false).
             */
            deleteButtonDisabled   : false,

            /**
             * @cfg {Array} acceptedFileExtensions An array of accepted files extension in lower case without the point (if empty all files are accepted). For example :['bmp','png','jpg']
             */
            acceptedFileExtensions : [],

            constructor            : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    validator : this.validateFileExtension
                };
                // if the detected browser is Internet Explorer 9.x or lower
                if (Ext.isIE9m) {
                    forcedConfig.height = 3;
                }
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.FileUploadField.superclass.constructor.call(this, config);
            },

            // private
            onRender               : function(ct, position) {
                Ext.ux.form.FileUploadField.superclass.onRender.call(this, ct, position);

                this.wrap = this.el.wrap({
                            cls : 'x-form-field-wrap x-form-file-wrap'
                        });

                this.wrap.applyStyles({
                            display : 'inline-block'
                        });

                this.downloadWrap = this.wrap.wrap({
                            tag : 'span'
                        });

                this.el.addClass('x-form-file-text');
                this.el.dom.removeAttribute('name');
                this.createFileInput();

                var btnCfg = Ext.applyIf(this.buttonCfg || {}, {
                            text : this.buttonText
                        });

                var downloadBtnCfg = Ext.applyIf(this.downloadButtonCfg || {}, {
                            text     : this.downloadButtonText,
                            handler  : this.onDownloadClick,
                            scope    : this,
                            renderTo : this.downloadWrap,
                            disabled : true,
                            iconCls  : 'viz-icon-small-download-document',
                            tooltip  : $lang('msg_document_download')

                        });

                var deleteBtnCfg = Ext.applyIf(this.deleteButtonCfg || {}, {
                            text     : this.deleteButtonText,
                            handler  : this.onDeleteClick,
                            scope    : this,
                            renderTo : this.downloadWrap,
                            disabled : true,
                            iconCls  : 'viz-icon-small-delete-document',
                            tooltip  : $lang('msg_document_delete')

                        });

                this.button = new Ext.Button(Ext.apply(btnCfg, {
                            renderTo : this.wrap,
                            cls      : 'x-form-file-btn' + (btnCfg.iconCls ? ' x-btn-icon' : '')
                        }));

                this.downloadButton = new Ext.Button(Ext.apply(downloadBtnCfg, {}));

                this.downloadButton.getEl().applyStyles({
                            display : 'inline-block'
                        });

                this.deleteButton = new Ext.Button(Ext.apply(deleteBtnCfg, {}));

                this.deleteButton.getEl().applyStyles({
                            display : 'inline-block'
                        });

                this.bindListeners();
                this.resizeEl = this.positionEl = this.wrap;

            },

            // private
            onResize               : function(w, h) {
                Ext.ux.form.FileUploadField.superclass.onResize.call(this, w, h);

                this.wrap.setWidth(w);

                if (!this.buttonOnly) {
                    this.wrap.setWidth(this.wrap.getWidth() - this.downloadButton.getEl().getWidth() - this.deleteButton.getEl().getWidth());
                    var w = this.wrap.getWidth() - this.button.getEl().getWidth() - this.buttonOffset;
                    this.el.setWidth(w);
                }
            },

            // private
            onDestroy              : function() {
                Ext.ux.form.FileUploadField.superclass.onDestroy.call(this);
                Ext.destroy(this.fileInput, this.button, this.downloadButton, this.deleteButton, this.wrap);
            },

            // private
            doDisable              : function(disabled) {
                this.fileInput.dom.disabled = disabled;
                this.button.setDisabled(disabled);
                this.downloadButton.setDisabled(disabled);
                this.deleteButton.setDisabled(disabled);
            },

            // private
            preFocus               : Ext.emptyFn,

            // private
            alignErrorIcon         : function() {
                this.errorIcon.alignTo(this.wrap, 'tl-tr', [this.downloadButton.getWidth() + this.deleteButton.getWidth(), 0]); // Should be 45
            },
            // msgTarget : 'under',

            setValue               : function(v) {
                if (v == null || isNaN(v) || (v == '')) {
                    Viz.form.FileUploadField.superclass.setValue.apply(this, arguments);
                    this.documentMetadata = null;
                    this.downloadButton.setDisabled(true);
                    this.deleteButton.setDisabled(true);
                }
                else {
                    Viz.Services.CoreWCF.DocumentMetadata_GetItem.call(this, {
                                documentId : v,
                                success    : function(docMetadata) {
                                    // The reasons we check this is that it could be that the form the field is in has closed already by the time we get to setting the value.
                                    if ((this.el.dom) && (docMetadata)) {
                                        this.documentMetadata = docMetadata;
                                        Viz.form.FileUploadField.superclass.setValue.apply(this, [docMetadata.DocumentName]);
                                        this.downloadButton.setDisabled(false);
                                        this.deleteButton.setDisabled(false || this.deleteButtonDisabled);
                                    }
                                },
                                scope      : this
                            });
                }
                return this;
            },

            /**
             * @cfg {String} onDownloadClick The method to execute when you click the download button.
             */
            onDownloadClick        : function() {

                var func = Viz.Services.CoreWCF.Document_GetStream;
                var info = func.getInfo();
                var url = info.url + '?documentId=' + this.documentMetadata.KeyDocument;

                Viz.downloadFile(url);
            },
            /**
             * @cfg {String} onDeleteClick The method to execute when you click the delete button.
             */
            onDeleteClick          : function() {

                Ext.MessageBox.confirm($lang('msg_deleteconfirm_title'), $lang('msg_deleteconfirm_body'), function(value) {
                            if ((value) && (value == 'yes')) {
                                var func = Viz.Services.CoreWCF.Document_Delete.call(this, {
                                            item    : {
                                                KeyDocument : this.documentMetadata.KeyDocument
                                            },
                                            success : function() {
                                                Ext.MessageBox.alert($lang('msg_deleteconfirm_title'), $lang('msg_document_deleted'));
                                                this.setValue('');
                                                this.downloadButton.setDisabled(true);
                                                this.deleteButton.setDisabled(true);

                                                this.fireEvent('onAfterDeleteClick');
                                            },
                                            scope   : this
                                        });
                            }
                        }, this)

            },

            validateFileExtension  : function(field) {
                var valid = true;
                if (!this.acceptedFileExtensions || !Ext.isArray(this.acceptedFileExtensions) || this.acceptedFileExtensions.length == 0 || !field)
                    return valid;
                if (field) {
                    var extension = field.substring(field.lastIndexOf(".") + 1).toLowerCase();
                    valid = this.acceptedFileExtensions.indexOf(extension) >= 0;
                }
                if (!valid)
                    return $lang('msg_fileupload_incorrectextension') + ' :' + this.acceptedFileExtensions.join(";");
                return valid

            }

        });
Ext.reg('vizFileUpload', Viz.form.FileUploadField);

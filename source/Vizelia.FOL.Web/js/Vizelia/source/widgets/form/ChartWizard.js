﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ChartWizard
 * @extends Ext.ChartFormPanel.
 */
Viz.form.ChartWizard = Ext.extend(Viz.form.ChartFormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                         : function(config) {
                config = config || {};
                this._gridMeter = Ext.id();
                this._gridSpatial = Ext.id();
                this._gridMeterClassification = Ext.id();
                this._dvKPI = Ext.id();

                this._tabDatesId = Ext.id();
                this._tabMeterId = Ext.id();
                this._tabFilterSpatialId = Ext.id();
                this._tabFilterMeterClassificationId = Ext.id();

                this._TitleId = Ext.id();
                this._DescriptionId = Ext.id();

                this._const_grid_anchor = ' -80';

                var Id = config.entity ? config.entity.KeyChart : null;

                this.storeMeter = new Viz.store.ChartMeter({
                            KeyChart  : Id,
                            listeners : {
                                add    : this.onMeterStoreChange,
                                remove : this.onMeterStoreChange,
                                scope  : this
                            }
                        });
                this.storeFilterSpatial = new Viz.store.ChartFilterSpatial({
                            KeyChart : Id
                        });
                this.storeFilterMeterClassification = new Viz.store.ChartFilterMeterClassification({
                            KeyChart : Id
                        });

                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandlerCreate : Viz.Services.EnergyWCF.Chart_FormCreate,
                    messageSaveStart     : 'ChartChangeStart',
                    messageSaveEnd       : 'ChartChange',
                    isWizard             : true,
                    hasValidationToolbar : false,
                    closeOnSave          : true,
                    tabConfig            : {
                        deferredRender : true,
                        items          : [this.configKPI(config), this.configFormGeneral(config), this.configFormDates(config), this.configDetailGridMeter(config), this.configGridFilterSpatial(config), this.configGridFilterMeterClassification(config), this.configFormPortalColumn(config)], // this.configFormDisplay(config)
                        defaults       : {
                            layout   : 'fit',
                            hideMode : 'offsets'
                        }
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.ChartWizard.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                   : function(config) {
                return {
                    title      : $lang('msg_description'), // _welcome'),
                    iconCls    : 'viz-icon-small-form-update',
                    labelWidth : 150,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_chart_wizard_titlecard_description'),
                                fieldClass : 'x-wizard-displayfield'

                            }, {
                                name   : 'KeyChart',
                                hidden : true
                            }, {
                                fieldLabel : $lang('msg_chart_title'),
                                name       : 'Title',
                                id         : this._TitleId,
                                allowBlank : false
                            }, {
                                fieldLabel : $lang('msg_chart_favorite'),
                                name       : 'IsFavorite',
                                xtype      : 'vizcheckbox',
                                checked    : false
                            }, {
                                xtype      : 'textarea',
                                fieldLabel : $lang('msg_description'),
                                name       : 'Description',
                                id         : this._DescriptionId,
                                anchor     : Viz.Const.UI.Anchor + ' -110'
                            }]
                };
            },

            configKPI                           : function(config) {
                return {
                    title      : $lang('msg_chart_wizard_welcome') + ' ' + $lang('msg_chart_wizard_kpi'),
                    iconCls    : 'viz-icon-small-chart-kpi',
                    labelWidth : 150,
                    items      : [{
                                xtype       : 'vizDataViewChart',
                                id          : this._dvKPI,
                                isKPI       : true,
                                invalidText : $lang('msg_chart_wizard_kpi_empty'),
                                readonly    : true,
                                allowEmpty  : false,
                                anchor      : '-2 -2',
                                onClickItem : function(dv, index, item, e) {
                                    var chartKPI = dv.store.getAt(index).json;
                                    Ext.getCmp(this._tabDatesId).setDisabled(chartKPI.DynamicTimeScaleEnabled || chartKPI.KPIDisableDateRangeMenu);
                                    if (chartKPI.KPIDisableMeterSelection == false && chartKPI.KPIDisableSpatialSelection == false && chartKPI.KPIDisableMeterClassificationSelection == false) {
                                        Ext.getCmp(this._gridMeter).allowEmpty = true;
                                        Ext.getCmp(this._gridSpatial).allowEmpty = false;
                                        Ext.getCmp(this._gridMeterClassification).allowEmpty = false;
                                        Ext.getCmp(this._tabMeterId).setDisabled(chartKPI.KPIDisableMeterSelection);
                                        Ext.getCmp(this._tabFilterSpatialId).setDisabled(chartKPI.KPIDisableSpatialSelection);
                                        Ext.getCmp(this._tabFilterMeterClassificationId).setDisabled(chartKPI.KPIDisableMeterClassificationSelection);
                                    }
                                    else {
                                        Ext.getCmp(this._tabMeterId).setDisabled(chartKPI.KPIDisableMeterSelection);
                                        Ext.getCmp(this._gridMeter).allowEmpty = !(chartKPI.KPIDisableSpatialSelection && chartKPI.KPIDisableMeterClassificationSelection);

                                        Ext.getCmp(this._tabFilterSpatialId).setDisabled(chartKPI.KPIDisableSpatialSelection);
                                        Ext.getCmp(this._gridSpatial).allowEmpty = chartKPI.KPIDisableSpatialSelection;

                                        Ext.getCmp(this._tabFilterMeterClassificationId).setDisabled(chartKPI.KPIDisableMeterClassificationSelection);
                                        Ext.getCmp(this._gridMeterClassification).allowEmpty = chartKPI.KPIDisableMeterClassificationSelection;
                                    }
                                    Ext.getCmp(this._TitleId).setValue(chartKPI.Title);
                                    Ext.getCmp(this._DescriptionId).setValue(chartKPI.Description);
                                    this.isDirty = true;
                                }.createDelegate(this)
                            }]
                };
            },

            configFormDates                     : function(config) {
                var now = new Date();
                return {
                    title      : $lang('msg_chart_timeinterval'),
                    iconCls    : 'viz-icon-small-calendar',
                    labelWidth : 150,
                    id         : this._tabDatesId,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_chart_wizard_dates_description'),
                                fieldClass : 'x-wizard-displayfield'
                            }, {
                                fieldLabel : $lang('msg_chart_startdate'),
                                name       : 'StartDate',
                                hideTime   : false,
                                xtype      : 'vizDateTime',
                                allowBlank : true,
                                hasSwitch  : false,
                                value      : new Date(now.getFullYear(), 0, 1, 0, 0, 0)
                            }, {
                                fieldLabel : $lang('msg_chart_enddate'),
                                name       : 'EndDate',
                                hideTime   : false,
                                xtype      : 'vizDateTime',
                                allowBlank : true,
                                hasSwitch  : false,
                                value      : new Date(now.getFullYear(), 11, 31, 23, 59, 59)
                            }, {
                                fieldLabel : $lang('msg_chart_timezone'),
                                name       : 'TimeZoneId',
                                allowBlank : false,
                                xtype      : 'vizComboTimeZone',
                                autoLoad   : true
                            }]
                };
            },

            /**
             * The configuration object for the detail grid Meter.
             * @return {Object}
             */
            configDetailGridMeter               : function(config) {
                var retVal = this.configGridDetail({
                            name                         : 'Meter',
                            id                           : this._gridMeter,
                            windowTitle                  : $lang('msg_meters'),
                            windowWidth                  : 1000,
                            windowHeight                 : 400,
                            windowIconCls                : 'viz-icon-small-meter',
                            xtypeGridDetail              : 'vizGridMeter',
                            columnsGridDetail            : Viz.Configuration.Columns.ChartMeter,
                            buttonAddTitle               : $lang('msg_meter_add'),
                            buttonAddIconCls             : 'viz-icon-small-add',
                            buttonDeleteTitle            : $lang('msg_meter_delete'),
                            buttonDeleteIconCls          : 'viz-icon-small-delete',
                            storeDetail                  : this.storeMeter,
                            keyColumn                    : 'KeyMeter',
                            ddGroupGridDetail            : 'SpatialDD',
                            enableDropFromTreeGridDetail: true
                        });
                Ext.apply(retVal, {
                    anchor: Viz.Const.UI.GridAnchor + this._const_grid_anchor,
                    height   : 200,
                    loadMask : false

                        });
                return {
                    title      : $lang('msg_meters'),
                    iconCls    : 'viz-icon-small-meter',
                    id         : this._tabMeterId,
                    labelWidth : 150,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_chart_wizard_metercard_description'),
                                fieldClass: 'x-wizard-displayfield'
                            }, retVal]
                };
            },

            onMeterStoreChange                  : function(store) {
                var allowEmpty = store.getCount() > 0;   
                Ext.getCmp(this._gridSpatial).allowEmpty = allowEmpty;
                Ext.getCmp(this._gridMeterClassification).allowEmpty = allowEmpty;
            },

            /**
             * The configuration object for the filter spatial grid.
             * @return {Object}
             */
            configGridFilterSpatial             : function(config) {
                return {
                    title      : $lang('msg_chart_filterspatial'),
                    iconCls    : 'viz-icon-small-site',
                    id         : this._tabFilterSpatialId,
                    labelWidth : 150,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_chart_wizard_spatialcard_description'),
                                fieldClass : 'x-wizard-displayfield'
                            }, {
                                id                          : this._gridSpatial,
                                anchor                      : Viz.Const.UI.GridAnchor + this._const_grid_anchor,
                                xtype                       : 'vizGridFilterSpatial',
                                store                       : this.storeFilterSpatial,
                                border                      : true,
                                loadMask                    : false,
                                allowEmpty                  : false,
                                invalidText                 : $lang('msg_chart_wizard_spatial_empty'),
                                ddGroup                     : 'SpatialDD',
                                enableDropFromTree          : true,
                                disableCheckboxForAncestors : true
                            }]
                };
            },

            configGridFilterMeterClassification : function(config) {
                return {
                    title      : $lang('msg_meter_classification'),
                    iconCls    : 'viz-icon-small-classificationitem',
                    id         : this._tabFilterMeterClassificationId,
                    labelWidth : 150,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_chart_wizard_meterclassificationcard_description'),
                                fieldClass : 'x-wizard-displayfield'
                            }, {
                                anchor      : Viz.Const.UI.GridAnchor + this._const_grid_anchor,
                                xtype       : 'vizGridFilterMeterClassification',
                                store       : this.storeFilterMeterClassification,
                                border      : true,
                                loadMask    : false,
                                id          : this._gridMeterClassification,
                                listeners   : {
                                    beforeaddrecord : function(grid, entity) {
                                        if (this.storeFilterSpatial.hasLoaded)
                                            this.storeFilterSpatial.save();
                                        entity.KeyChart = this.entity.KeyChart;
                                        entity.filterSpatial = this.storeFilterSpatial.crudStore;
                                        return true;
                                    },
                                    scope           : this
                                },
                                allowEmpty  : false,
                                invalidText : $lang('msg_chart_wizard_meterclassification_empty')
                            }]
                };

            },

            configFormDisplay                   : function(config) {
                return {
                    title      : $lang('msg_chart_display'),
                    iconCls    : 'viz-icon-small-chart-display',
                    labelWidth : 180,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_chart_wizard_displaycard_description'),
                                fieldClass : 'x-wizard-displayfield'
                            }, {
                                fieldLabel   : $lang('msg_chart_type'),
                                name         : 'ChartType',
                                allowBlank   : false,
                                xtype        : 'vizComboEnum',
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartType,Vizelia.FOL.Common',
                                displayIcon  : true,
                                value        : Vizelia.FOL.BusinessEntities.ChartType.Combo
                            }, {
                                fieldLabel   : $lang('msg_chart_localisation'),
                                name         : 'Localisation',
                                allowBlank   : false,
                                xtype        : 'vizComboEnum',
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartLocalisation,Vizelia.FOL.Common',
                                sortByValues : true,
                                displayIcon  : true,
                                value        : Vizelia.FOL.BusinessEntities.ChartLocalisation.ByBuilding
                            }, {
                                fieldLabel   : $lang('msg_chart_timeinterval'),
                                name         : 'TimeInterval',
                                allowBlank   : false,
                                xtype        : 'vizComboEnum',
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common',
                                value        : Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months,
                                sortByValues : true,
                                displayIcon  : true
                            }, {
                                fieldLabel : $lang('msg_chart_classificationlevel'),
                                name       : 'ClassificationLevel',
                                allowBlank : false,
                                xtype      : 'vizComboChartClassificationLevel'
                            }, Viz.combo.DynamicDisplay.factory({}), {
                                fieldLabel : $lang('msg_portalcolumn'),
                                name       : 'KeyPortalColumn',
                                hiddenName : 'KeyPortalColumn',
                                xtype      : 'vizComboPortalColumn',
                                value      : config.entity ? config.entity.KeyPortalColumn : null,
                                allowBlank : true
                            }]
                };
            },

            configFormPortalColumn              : function(config) {
                return {
                    title      : $lang('msg_portalcolumn'),
                    iconCls    : 'viz-icon-small-portalwindow',
                    labelWidth : 180,
                    items      : [{
                                xtype      : 'displayfield',
                                hideLabel  : true,
                                value      : $img('viz-icon-small-information') + $lang('msg_chart_wizard_portalcolumn_description'),
                                fieldClass : 'x-wizard-displayfield'
                            }, {
                                fieldLabel : $lang('msg_portalcolumn'),
                                name       : 'KeyPortalColumn',
                                hiddenName : 'KeyPortalColumn',
                                xtype      : 'vizComboPortalColumn',
                                value      : config.entity ? config.entity.KeyPortalColumn : null,
                                allowBlank : true
                            }]
                }
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                        : function(form, item, extraParams) {
                var storeArray = this.getExtraStoreArray();
                Ext.each(storeArray, function(store) {
                            if (store.hasLoaded)
                                store.save();
                        }, this);

                var KeyChartToCopy = null;
                var chartKPI = Ext.getCmp(this._dvKPI).getSelectedRecord();
                if (chartKPI) {
                    KeyChartToCopy = chartKPI.data.KeyChart;
                    item.MaxMeter = chartKPI.data.MaxMeter;
                }

                Ext.apply(extraParams, {
                            filterSpatial             : this.storeFilterSpatial.crudStore,
                            filterMeterClassification : this.storeFilterMeterClassification.crudStore,
                            meters                    : this.storeMeter.crudStore,
                            KeyChartToCopy            : KeyChartToCopy
                        });

                item.DisplayFontFamily = 'Verdana';
                item.DisplayFontSize = 9;
                item.DisplayLegendFontSize = item.DisplayFontSize;
                item.DisplayStartEndDateFontSize = item.DisplayFontSize;
                item.DisplayLegendVisible = true;
                item.DisplayStackSeries = true;
                item.DisplayTransparency = 30;
                item.DisplayShaddingEffect = Vizelia.FOL.BusinessEntities.ChartShaddingEffect.One;
                item.CustomGridColumnCount = 5;
                item.CustomGridRowCount = 5;
                item.MaxDataPointPerDataSerie = 500;
                item.DisplayChartAreaBackgroundStart = 'ffffff';
                item.DisplayChartAreaBackgroundEnd = 'cff499';
                item.UseSpatialPath = false;
                item.UseClassificationPath = true;
                if (!Ext.isDate(item.StartDate))
                    item.StartDate = new Date();
                if (!Ext.isDate(item.EndDate))
                    item.EndDate = new Date();
                // var KeyDynamicDisplay = this.form.findField('KeyDynamicDisplay').getValue();
                // item.UseDynamicDisplay = !Ext.isEmpty(KeyDynamicDisplay);
                
                return true;
            },

            /**
             * Return an array of all the extra store of the Chart.
             * @return {Array}
             */
            getExtraStoreArray                  : function() {
                return [this.storeMeter, this.storeFilterSpatial, this.storeFilterMeterClassification];

            },

            /*
             * we orrive the standard Chart method here because not all fields are existing.
             */
            onAfterLoadValues                   : function() {

            },
            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                       : function(form, action) {
                Ext.apply(this.entity, action.result.data);

                var KeyPortalColumn = this.form.findField('KeyPortalColumn').getValue();
                // if the KeyPortalColumn has been defined, we create the portlet
                if (!Ext.isEmpty(KeyPortalColumn)) {
                    this.el.mask($lang('msg_chart_wizard_associatingcharttoportal'));
                    var portlet = {
                        Collapsed               : false,
                        Flex                    : 1,
                        KeyEntity               : this.entity.KeyChart,
                        TypeEntity              : 'Vizelia.FOL.BusinessEntities.Chart',
                        // EntityLongPath : $lang('msg_chart') + ' / ' + this.entity.Title,
                        KeyPortalColumn         : KeyPortalColumn,
                        HeaderAndToolbarVisible : true,
                        KeyPortlet              : null,
                        Order                   : 1,
                        Title                   : this.entity.Title

                    };
                    Viz.Services.EnergyWCF.Portlet_FormCreate({
                                item    : portlet,
                                success : function(response) {
                                    this.el.unmask();
                                    this.closeParentWindow(false);
                                    var portlet = response.data;

                                    // we open or create the portal
                                    var created = Viz.App.Global.ViewPort.openModule({
                                                xtype           : 'vizPortalPanel',
                                                KeyPortalWindow : portlet.KeyPortalWindow,
                                                title           : portlet.PortalWindowTitle,
                                                iconCls         : portlet.PortalWindowIconCls.replace('-large-', '-small-')
                                            }, true);
                                    if (created === false) {
                                        Viz.util.MessageBusMgr.fireEvent('PortalWindowChange', {
                                                    KeyPortalWindow : portlet.KeyPortalWindow,
                                                    KeyActiveTab    : portlet.KeyPortalTab,
                                                    Title           : portlet.PortalWindowTitle
                                                });
                                    }
                                },
                                scope   : this
                            });
                    return false;
                }
                else {
                    Viz.grid.Chart.openChartDisplay(this.entity);
                }
            }
        });

Ext.reg('vizFormChartWizard', Viz.form.ChartWizard);

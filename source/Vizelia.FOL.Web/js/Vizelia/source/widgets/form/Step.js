﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Step
 * @extends Ext.FormPanel.
 */
Viz.form.Step = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};

                this.modelDataKeys = config.entity.ModelDataKeys;
                this._gridContainerId = Ext.id();
                this._comboMethodDefinitionId = Ext.id();
                this._lastMethodDefinition = '';

                var defaultConfig = {
                    mode_load            : 'local',
                    serviceHandler       : null,
                    serviceHandlerCreate : null,
                    serviceHandlerUpdate : null,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Step.superclass.constructor.call(this, config);
            },

            /**
             * General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            allowBlank : false,
                            anchor     : '-40'
                        }, {
                            fieldLabel     : $lang('msg_step_methodname'),
                            xtype          : 'vizComboMethodDefinition',
                            typeName       : 'Vizelia.FOL.WCFService.JobWCF',
                            name           : 'MethodQualifiedName',
                            hiddenName     : 'MethodQualifiedName',
                            allowBlank     : false,
                            anchor         : '-40',
                            forceLoadStore : true,
                            id             : this._comboMethodDefinitionId,
                            listeners      : {
                                setvalue : function(combo, record) {
                                    if (record.data.MethodName != this._lastMethodDefinition){
                                        this._lastMethodDefinition = record.data.MethodName; 
                                        this.setDynamicGrid(record);
                                    }
                                },
                                scope    : this
                            }
                        }, {
                            xtype  : 'container',
                            layout : 'fit',
                            anchor : '-40 -60',
                            id     : this._gridContainerId,
                            border : false
                        }];
            },

            /**
             * Handler for updating fiels based on the form values.
             * @param {Viz.form.FormPanel} the form panel
             * @param {Object} values
             */
            onAfterLoadValues : function(formpanel, values) {
            },

            /**
             * Get the current value of the combo and display the approprieted grid.
             */
            applyComboValue   : function() {
                var combo = Ext.getCmp(this._comboMethodDefinitionId);
                var idx = combo.store.find("MethodQualifiedName", combo.getValue());
                if (idx > -1) {
                    this.setDynamicGrid(combo.store.getAt(idx));
                }
            },

            /**
             * Set the dynamic detail grid
             * @param {} record
             */
            setDynamicGrid    : function(record) {
                var gridContainer = Ext.getCmp(this._gridContainerId);
                gridContainer.removeAll();

                var gridXType = record.get("GridXType");
                var storeXType = record.get("StoreXType");
                var KeyEntity = record.get("KeyEntity");

                // grid is optional
                if (Ext.isEmpty(gridXType)) {
                    delete this._gridId;
                    return;
                }
                // initializa the modelDataKeys with a value that will return no row for store.
                var modelDataKeys = " ";
                if (this.entity.ModelDataKeys != null && Ext.isArray(this.entity.ModelDataKeys) && this.entity.ModelDataKeys.length > 0)
                    modelDataKeys = this.entity.ModelDataKeys.join(",");

                var gridDetailConfig = {
                    name            : 'ModelDataGrid',
                    windowTitle     : $lang('msg_add'),
                    windowWidth     : 1000,
                    windowHeight    : 400,
                    xtypeGridDetail : gridXType,
                    gridReadOnly    : false,
                    keyColumn       : KeyEntity
                };

                if (storeXType) {
                    Ext.apply(gridDetailConfig, {
                                storeDetail : {
                                    xtype   : storeXType,
                                    keyStep : this.entity.KeyStep,
                                    keyJob  : this.entity.KeyJob
                                }
                            });
                }

                var gridPanel = Ext.apply(this.configGridDetail(gridDetailConfig), {
                            filterRemote    : false,
                            storeBaseParams : {
                                filters : [{
                                            data  : {
                                                dataIndex : KeyEntity,
                                                type      : 'innerjoin',
                                                value     : modelDataKeys
                                            },
                                            field : KeyEntity
                                        }]
                            }

                        });

                this._gridId = gridPanel.id;

                gridContainer.add(gridPanel);
                gridContainer.doLayout();
            }

        });

Ext.reg('vizFormStep', Viz.form.Step);

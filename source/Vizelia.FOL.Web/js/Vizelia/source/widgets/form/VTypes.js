﻿
/**
 * @class Ext.form.VTypes
 * <p>
 * This is the extension of the singleton object which contains a set of commonly used field validation functions.
 * </p>
 * @singleton
 */
Ext.apply(Ext.form.VTypes, {

    /**
     * The validator to validate xml file extension.
     * @param {String} value
     * @return {Boolean}
     */
    extensionXML: function (value) {
        var exp = /^.*.(xml|XML)$/;
        return exp.test(value);
    },

    /**
     * The function used to validate passwords. The second password field should have an initialPassword property containing the value of the id of the first field.
     * @param {String} val the value of the field (second password)
     * @param {Ext.form.TextField} field (the field)
     * @return {Boolean} true if the passwords are identical, false otherwise.
     */
    passwordconfirm: function (val, field) {
        if (field.initialPasswordField) {
            var passwordField = Ext.getCmp(field.initialPasswordField);
            return (val == passwordField.getValue());
        }
    },

    /**
     * The error text to display when the password validation function returns false.
     * @type String
     */
    passwordconfirmText: '',

    /**
     * The validator to validate passwords. The second password field should have an initialPassword property containing the value of the id of the first field.
     * <p>
     * The validator is preferably used over the vtype because it is executed before the allowBlank validation.
     * </p>
     * @param {String} value
     * @return {String}
     */
    passwordconfirmValidator: function (value) {
        var passwordField = Ext.getCmp(this.initialPasswordField);
        var userField = Ext.getCmp(this.userIdField);
        var username;
        if (userField)
            username = userField.getRawValue();
        if ((!userField || !username) && (Viz && Viz.App && Viz.App.Global && Viz.App.Global.User && Viz.App.Global.User.UserName)) {
            username = Viz.App.Global.User.UserName;
        }
        if (!this.isDirty() && !passwordField.isDirty()) {
            // passwordField.clearInvalid();
            return; // true;
        }
        if (passwordField.getRawValue() != value) {
            var msg = $lang('error_msg_invalid_password_confirm');
            passwordField.markInvalid(msg);
            return msg;
        }
        if (passwordField.getRawValue() == username) {
            var msg = $lang('msg_username_same_as_password');
            passwordField.markInvalid(msg);
            return msg;
        }
        passwordField.clearInvalid();
        return true;
    },

    /**
     * The validator to validate ActionRequest location.
     * <p>
     * The validator is preferably used over the vtype because it is executed before the allowBlank validation.
     * </p>
     * @param {String} value
     * @return {String}
     */
    actionRequestLocationValidator: function (value) {
        var form = this.findParentByType('form').getForm();
        var node = this.getTree().getSelectionModel().getSelectedNode();
        if ((node == null && this.allowBlank) || (node == null && !Ext.isEmpty(this.getValue()))) {
            return true;
        }
        if (node == null) {
            var msg = Ext.form.TextField.prototype.blankText;
            this.markInvalid(msg);
            return msg;
        }
        if (node.attributes.entity.IfcType == 'IfcSite') {
            var msg = $lang('error_msg_invalid_location_site');
            this.markInvalid(msg);
            return msg;
        }
        this.clearInvalid();
        return true;
    },

    /**
     * The validator to validate that ClassificationItem of the ActionRequest is of category actionrequest.
     * @param {String} value
     * @return {String}
     */
    actionRequestClassificationItemValidator: function (value) {
        var node = this.getTree().getSelectionModel().getSelectedNode();
        if ((node == null && this.allowBlank) || (node == null && !Ext.isEmpty(this.getValue()))) {
            return true;
        }
        if (node == null) {
            var msg = Ext.form.TextField.prototype.blankText;
            this.markInvalid(msg);
            return msg;
        }
        if (node.attributes.entity.Category != 'actionrequest') {
            var msg = $lang('error_msg_invalid_classification');
            this.markInvalid(msg);
            return msg;
        }
        this.clearInvalid();
        return true;
    },

    comboTestValidator: function (value) {
        if (value == null) {
            var msg = Ext.form.TextField.prototype.blankText;
            this.markInvalid(msg);
            return msg;
        }
        this.clearInvalid();
        return true;
    },

    /**
     * The function used to validation localization message code.
     * @param {String} val the value of the field
     * @return {Boolean} true if the value is valid, false otherwise.
     */
    localization: function (val) {
        var mask = /^MSG_[A-Z0-9_]+$/;
        return mask.test(val);
    },

    /**
     * The error text to display when the validation function returns false.
     */
    localizationText: $lang('msg_component'),

    /**
     * The function used to validation virtualfile path.
     * @param {String} val the value of the field
     * @return {Boolean} true if the value is valid, false otherwise.
     */
    virtualfilePath: function (val) {
        var mask = /^[a-z0-9_]+$/;
        return mask.test(val);
    },

    /**
     * The error text to display when the validation function returns false.
     * @type String
     */
    virtualfilePathText: $lang('error_msg_invalid_virtualfile_path'),

    /**
    * The function used to validation virtualfile path.
    * @param {String} val the value of the field
    * @return {Boolean} true if the value is valid, false otherwise.
    */
    gridViewFilterName: function (val) {
        var mask = /^[a-zA-Z0-9_]+$/;
        return mask.test(val);
    },

    /**
    * The error text to display when the validation function returns false.
    * @type String
    */
    gridViewFilterNameText: $lang('error_msg_invalid_gridview_path'),

    /**
     * The function used to validation simple URL (for tenant provisioning, localhost is allowed, which is not the case in sencha).
     * @param {String} val the value of the field
     * @return {Boolean} true if the value is valid, false otherwise.
     */
    simpleUrlValidator: function (val) {
        var mask = /^[\-A-Za-z0-9_\.]+$/;
        return mask.test(val);
    },
    simpleUrlValidatorText: $lang('error_msg_invalid_url'),
    /**
     * The function allow validation between 2 dates, to ensure that enddate will be greater than startdate.
     * @param {Date} val
     * @param {Ext.form.Field} field
     * @return {Boolean}
     */
    daterange: function (val, field) {
        var date = field.parseDate(val);

        if (!date) {
            return false;
        }
        var form = field.findParentByType('form').getForm();

        if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
            var start = form.findField(field.startDateField);
            start.setMaxValue(date);
            // start.validate();
            this.dateRangeMax = date;
        }
        else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
            var end = form.findField(field.endDateField);
            end.setMinValue(date);
            // end.validate();
            this.dateRangeMin = date;
        }

        return true;
    },

    datetimerange: function (val) {
        var field = this;
        var date = field.parseDate(val);

        if (!date) {
            return false;
        }
        var form = field.findParentByType('form').getForm();

        if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
            var start = form.findField(field.startDateField);
            start.df.setMaxValue(date);
            start.df.validate();
            this.dateRangeMax = date;
        }
        else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
            var end = form.findField(field.endDateField);
            end.df.setMinValue(date);
            end.df.validate();
            this.dateRangeMin = date;
        }

        return true;
    },

    daterangesimple: function (val) {
        var field = this; // .ownerCt;
        // var form = field.findParentByType('form').getForm();
        if (field.startDateField) {
            var startField = Ext.getCmp(field.startDateField);
            var start = startField.getValue();
            var end = field.getValue();
            if (start > end) {
                var msg = $lang('error_msg_invalid_daterange');
                // startField.markInvalid(msg);
                return msg;
            }
            else {
                // startField.clearInvalid();
                return true;
            }
        }
        if (field.endDateField) {
            var endField = Ext.getCmp(field.endDateField);
            var start = field.getValue();
            var end = endField.getValue();
            if (start > end) {
                var msg = $lang('error_msg_invalid_daterange');
                // endField.markInvalid(msg);
                return msg;
            }
            else {
                // endField.clearInvalid();
                return true;
            }
        }

    },

    /**
     * The validator to validate Map pset.
     * <p>
     * The validator is preferably used over the vtype because it is executed before the allowBlank validation.
     * </p>
     * @param {String} value
     * @return {String}
     */
    mapPsetValidator: function (value) {
        var node = this.getTree().getSelectionModel().getSelectedNode();
        if ((node == null && this.allowBlank) || (node == null && !Ext.isEmpty(this.getValue()))) {
            return true;
        }
        if (node == null) {
            var msg = Ext.form.TextField.prototype.blankText;
            this.markInvalid(msg);
            return msg;
        }
        if (node.attributes.entity.IfcType != 'PsetAttributeDefinition' || node.attributes.entity.AttributeComponent != 'textfield') {
            var msg = $lang('error_msg_invalid_map_pset');
            this.markInvalid(msg);
            return msg;
        }
        this.clearInvalid();
        return true;
    },

    /**
     * The validator to validate Historical pset.
     * <p>
     * The validator is preferably used over the vtype because it is executed before the allowBlank validation.
     * </p>
     * @param {String} value
     * @return {String}
     */
    historicalPsetValidator: function (value) {
        var node = this.getTree().getSelectionModel().getSelectedNode();
        if ((node == null && this.allowBlank) || (node == null && !Ext.isEmpty(this.getValue()))) {
            return true;
        }
        if (node == null) {
            var msg = Ext.form.TextField.prototype.blankText;
            this.markInvalid(msg);
            return msg;
        }
        if (node.attributes.entity.IfcType != 'PsetAttributeDefinition' || node.attributes.entity.AttributeComponent != 'vizGridPsetAttributeHistorical') {
            var msg = $lang('error_msg_invalid_historical_pset');
            this.markInvalid(msg);
            return msg;
        }
        this.clearInvalid();
        return true;
    },

    /**
     * The validator to validate spatialFilterPset.
     * <p>
     * The validator is preferably used over the vtype because it is executed before the allowBlank validation.
     * </p>
     * @param {String} value
     * @return {String}
     */
    spatialFilterPsetValidator: function (value) {
        var node = this.getTree().getSelectionModel().getSelectedNode();
        if ((node == null && this.allowBlank) || (node == null && !Ext.isEmpty(this.getValue()))) {
            return true;
        }
        if (node == null) {
            var msg = Ext.form.TextField.prototype.blankText;
            this.markInvalid(msg);
            return msg;
        }
        if (node.attributes.entity.IfcType != 'PsetAttributeDefinition' || node.attributes.entity.AttributeComponent == 'label' || node.attributes.entity.AttributeComponent == 'vizGridPsetAttributeHistorical' || node.attributes.entity.AttributeComponent == 'vizFileUpload' || node.attributes.entity.AttributeComponent == 'vizImageUpload') {
            var msg = $lang('error_msg_invalid_spatialfilter_pset');
            this.markInvalid(msg);
            return msg;
        }
        var usageName = node.parentNode.attributes.entity.UsageName;
        if (usageName != 'IfcSite' && usageName != 'IfcBuilding' && usageName != 'IfcBuildingStorey' && usageName != 'IfcSpace' && usageName != 'IfcMeter') {
            var msg = $lang('error_msg_invalid_spatialfilter_pset');
            this.markInvalid(msg);
            return msg;
        }
        this.clearInvalid();
        return true;
    },

    /**
     * The validator to validate PsetAttributeSelected.
     * <p>
     * The validator is preferably used over the vtype because it is executed before the allowBlank validation.
     * </p>
     * @param {String} value
     * @return {String}
     */
    psetAttributeSelectedValidator: function (value) {
        var node = this.getTree().getSelectionModel().getSelectedNode();
        if ((node == null && this.allowBlank) || (node == null && !Ext.isEmpty(this.getValue()))) {
            return true;
        }
        if (node == null) {
            var msg = Ext.form.TextField.prototype.blankText;
            this.markInvalid(msg);
            return msg;
        }
        if (!node.attributes.entity || node.attributes.entity.IfcType != 'PsetAttributeDefinition') {
            var msg = $lang('error_msg_invalid_psetattribute');
            this.markInvalid(msg);
            return msg;
        }

        // Validation moved to DB.
        /*
         * var usageName = node.parentNode.attributes.entity.UsageName; if (node.attributes.entity.IfcType != 'PsetAttributeDefinition' || node.attributes.entity.AttributeComponent != 'vizGridPsetAttributeHistorical' || (usageName != 'IfcSite' && usageName != 'IfcBuilding' && usageName != 'IfcBuildingStorey' && usageName != 'IfcSpace' && usageName != 'IfcFurniture' && usageName != 'IfcMeter')) { var msg = $lang('error_msg_invalid_dataseriepsetratio_pset'); this.markInvalid(msg); return msg; }
         */
        this.clearInvalid();
        return true;
    },

    /**
     * The validator to validate spatial node (IfcSite,IfcBuilding...,IfcSpace, IfcFurniture).
     * <p>
     * The validator is preferably used over the vtype because it is executed before the allowBlank validation.
     * </p>
     * @param {String} value
     * @return {String}
     */
    locationValidator: function (value) {
        var node = this.getTree().getSelectionModel().getSelectedNode();
        if ((node == null && this.allowBlank) || (node == null && !Ext.isEmpty(this.getValue()))) {
            return true;
        }
        if (node == null) {
            var msg = Ext.form.TextField.prototype.blankText;
            this.markInvalid(msg);
            return msg;
        }

        var validTypes = ['IfcSite', 'IfcBuilding', 'IfcBuildingStorey', 'IfcSpace', 'IfcFurniture'];

        if (validTypes.indexOf(node.attributes.entity.IfcType) < 0) {
            var msg = $lang('error_msg_invalid_location');
            this.markInvalid(msg);
            return msg;
        }

        this.clearInvalid();
        return true;
    },

    /**
     * The validator to validate input mask for pset attribute. Should only be used with a delegate passing the mask
     * @param {String} value
     * @return {Boolean}
     */
    maskValidator: function (value, mask) {
        if (!mask)
            mask = this.mask;
        var regmask = new RegExp(mask);
        if (!regmask.test(value)) {
            var msg = $lang('error_msg_field_inputmask') + ' "' + mask + '"';
            this.markInvalid(msg);
            return msg;
        }
        return true;
    },

    /**
     * Validate that the number is not zero.
     * @param {} value
     * @return {Boolean}
     */
    numberfieldNotZeroValidator: function (value) {
        if (value == 0) {
            var msg = $lang('error_msg_invalid_zerovalue');
            this.markInvalid(msg);
            return msg;
        }
        return true;
    },
    /**
     * Validate that the text is a valid regular expression value.
     * @param {} value
     * @return {Boolean}
     */
    regexValidator: function (value) {
        try {
            new RegExp(value);
        } catch (e) {
            this.markInvalid(e.message);
            return e.message;
        }
        return true;
    }

});

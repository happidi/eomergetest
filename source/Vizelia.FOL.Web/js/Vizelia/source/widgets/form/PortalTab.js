﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.PortalTab
 * @extends Ext.FormPanel.
 */
Viz.form.PortalTab = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Portal} entity The portal entity the tab belong to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor           : function(config) {
                this._tabPortalColumn = Ext.id();
                this._gridPortalColumn = Ext.id();
                var Id = config.entity ? config.entity.KeyPortalTab : null;
                this.storeColumn = new Viz.store.PortalColumn({
                            KeyPortalTab : Id
                        });
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.PortalTab_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.PortalTab_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.PortalTab_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_portaltab_columns'),
                                    id         : this._tabPortalColumn,
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    hidden     : config.mode == 'create',
                                    items      : [this.configGridPanelColumn(config)]
                                }]
                    },
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.PortalTab.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral     : function(config) {
                return [{
                            name   : 'KeyPortalTab',
                            hidden : true
                        }, {
                            fieldLabel: $lang('msg_localid'),
                            allowBlank: false,
                            name: 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_portaltab_title'),
                            name       : 'Title',
                            allowBlank : false

                        }, {
                            fieldLabel : $lang('msg_portaltab_background'),
                            name       : 'BackgroundCls',
                            xtype      : 'vizComboCss',
                            selector   : '.viz-playlist-background-',
                            allowBlank : true
                        }, {
                            fieldLabel       : $lang('msg_portaltab_order'),
                            name             : 'Order',
                            xtype            : 'vizSpinnerField',
                            minValue         : 1,
                            decimalPrecision : 0,
                            allowBlank       : false,
                            width            : 80,
                            value            : config.entity.Order || ''
                        }, {
                            fieldLabel   : $lang('msg_portaltab_columnconfig'),
                            name         : 'ColumnConfig',
                            allowBlank   : false,
                            hidden       : config.mode != 'create',
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.PortalTabColumnConfig,Vizelia.FOL.Common',
                            displayIcon  : true
                        }, {
                            fieldLabel       : $lang('msg_portaltab_headerheight'),
                            name             : 'HeaderHeight',
                            xtype            : 'vizSpinnerField',
                            decimalPrecision : 0,
                            width            : 80,
                            allowBlank       : true
                        }, {
                            fieldLabel       : $lang('msg_portaltab_footerheight'),
                            name             : 'FooterHeight',
                            xtype            : 'vizSpinnerField',
                            decimalPrecision : 0,
                            width            : 80,
                            allowBlank       : true
                        }];
            },

            /**
             * The configuration for the grid priority
             * @return {object}
             */
            configGridPanelColumn : function(config) {
                return {
                    xtype        : 'vizGridPortalColumn',
                    loadMask     : false,
                    border       : true,
                    id           : this._gridPortalColumn,
                    store        : this.storeColumn,
                    KeyPortalTab : config.entity.KeyPortalTab,
                    columns      : Viz.Configuration.Columns.PortalColumn,
                    listeners    : {
                        beforedeleterecord : this.onDeletedRecordColumn,
                        scope              : this
                    }
                };
            },

            /**
             * Handler for the columns grid delete.
             */
            onDeletedRecordColumn : function() {
                this.deleteSelectedRowsFromGrid(this._gridPortalColumn);
                return false;
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave          : function(form, item, extraParams) {
                if (this.storeColumn.hasLoaded)
                    this.storeColumn.save();
                Ext.apply(extraParams, {
                            portalcolumn : this.storeColumn.crudStore
                        });
                return true;
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess         : function(form, action) {
                Viz.form.PortalTab.superclass.onSaveSuccess.apply(this, arguments);

                var tabpanel = this.getTabContainer();
                tabpanel.unhideTabStripItem(this._tabPortalColumn);

                var grid = Ext.getCmp(this._gridPortalColumn);
                grid.KeyPortalTab = grid.store.serviceParams.KeyPortalTab = this.entity.KeyPortalTab;
                grid.store.reload();
            }
        });

Ext.reg('vizFormPortalTab', Viz.form.PortalTab);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Job
 * @extends Ext.FormPanel.
 */
Viz.form.Job = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor        : function(config) {
                config = config || {};
                var Id = config.entity ? config.entity.KeyJob : null;
                this._scheduleGridId = Ext.id();

                this.storeSchedule = new Ext.data.JsonStore({
                            idProperty : "KeySchedule",
                            root       : 'records',
                            load       : function() {
                            },
                            fields     : [{
                                        mapping : 'KeySchedule',
                                        name    : 'KeySchedule',
                                        type    : 'auto'

                                    }, {
                                        mapping : 'Title',
                                        name    : 'Title',
                                        type    : 'auto'
                                    }, {
                                        mapping : 'StartDate',
                                        name    : 'StartDate',
                                        type    : 'auto'
                                    }]
                        });
                this.storeStep = new Ext.data.JsonStore({
                            idProperty : "KeyStep",
                            root       : 'records',
                            load       : function() {
                            },
                            fields     : [{
                                        mapping : 'KeyStep',
                                        name    : 'KeyStep',
                                        type    : 'auto'

                                    }, {
                                        mapping : 'Name',
                                        name    : 'Name',
                                        type    : 'auto'
                                    }, {
                                        mapping : 'MethodQualifiedName',
                                        name    : 'MethodQualifiedName',
                                        type    : 'auto'
                                    }, {
                                        mapping : 'MethodMsgCode',
                                        name    : 'MethodMsgCode',
                                        type    : 'auto'
                                    }]
                        });
                if (!config.entity.Schedules)
                    config.entity.Schedules = [];
                if (!config.entity.Steps)
                    config.entity.Steps = [];

                this.bindStoreSchedule(config.entity);
                this.bindStoreStep(config.entity);

                var defaultConfig = {
                    mode_load            : 'local',
                    serviceHandler       : Viz.Services.CoreWCF.Job_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.Job_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.Job_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_schedules'),
                                    iconCls    : 'viz-icon-small-schedule',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridSchedule()]
                                }, {
                                    title      : $lang('msg_steps'),
                                    iconCls    : 'viz-icon-small-step',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridStep()]
                                }]
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Job.superclass.constructor.call(this, config);
            },

            /**
             * @private Binds storeSchedule to the Schedules (list) property of the Job.
             * @param {Viz.BusinessEntity.Job} entity
             */
            bindStoreSchedule  : function(entity) {
                if (entity.Schedules) {
                    this.storeSchedule.loadData({
                                records : Viz.clone(entity.Schedules)
                            });
                }
            },

            /**
             * @private Binds storeStep to the Steps (list) property of the Job.
             * @param {Viz.BusinessEntity.Job} entity
             */
            bindStoreStep      : function(entity) {
                if (entity.Steps) {
                    this.storeStep.loadData({
                                records : Viz.clone(entity.Steps)
                            });
                }
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral  : function(config) {
                return [{
                            name   : 'KeyJob',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            anchor     : '-20 -40',
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description'
                        }];
            },

            /**
             * @private The configuration object for detail grid Schedule.
             * @return {Object}
             */
            configGridSchedule : function() {
                var gridId = this._scheduleGridId;
                var paramConfig = {
                    id               : gridId,
                    name             : 'Schedule',
                    xtype            : 'vizGridSchedule',
                    store            : this.storeSchedule,
                    filterRemote     : false,
                    loadMask         : false,
                    hasPagingToolbar : false,
                    border           : true,
                    listeners        : {
                        deleterecord : function() {
                            this.isDirty = true;
                        },
                        scope        : this
                    },
                    formConfig       : {
                        common : {
                            width            : 740,
                            height           : 430,
                            xtype            : 'vizFormSchedule',
                            messageSaveStart : 'ScheduleChangeStart',
                            messageSaveEnd   : 'ScheduleChange',
                            listeners        : {
                                // change the standard behavior of the form in order to avoid server side save call.
                                beforesave : function(form, item, extraParam) {
                                    // prepare the entity as expected.
                                    var valid = form.onBeforeSave(form, item, extraParam);
                                    if (valid) {
                                        this.isDirty = true; // to make sure Job form knows it has changed.
                                        if (item.KeySchedule == null) {
                                            item.KeySchedule = Ext.id();
                                            this.entity.Schedules.push(item);
                                        }
                                        else {
                                            Ext.each(this.entity.Schedules, function(schedule) {
                                                        if (schedule.KeySchedule == item.KeySchedule) {
                                                            if (!item.RecurrencePattern) {
                                                                delete schedule.RecurrencePattern;
                                                            }
                                                            
                                                            Ext.apply(schedule, item);
                                                        }
                                                    });

                                        }

                                        this.bindStoreSchedule(this.entity);
                                        if (form.closeonsave) form.getParentWindow().close();
                                    }
                                    return false;
                                },
                                scope      : this
                            }
                        },
                        create : {
                            title   : $lang("msg_schedule_add"),
                            iconCls : 'viz-icon-small-schedule-add'
                        },
                        update : {
                            title               : $lang("msg_schedule_update"),
                            iconCls             : 'viz-icon-small-schedule-update',
                            titleEntity         : 'Title',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeySchedule'
                                    }]
                        }

                    }

                };
                return paramConfig;

            },

            /**
             * @private The configuration object for detail grid Step.
             * @return {Object}
             */
            configGridStep     : function() {
                var gridId = this._stepGridId;
                var paramConfig = {
                    id               : gridId,
                    name             : 'Step',
                    xtype            : 'vizGridStep',
                    store            : this.storeStep,
                    filterRemote     : false,
                    loadMask         : false,
                    hasPagingToolbar : false,
                    border           : true,
                    listeners        : {
                        deleterecord : function() {
                            this.isDirty = true;
                        },
                        scope        : this
                    },
                    formConfig       : {
                        common : {
                            width            : 740,
                            height           : 430,
                            xtype            : 'vizFormStep',
                            messageSaveStart : 'StepChangeStart',
                            messageSaveEnd   : 'StepChange',
                            listeners        : {
                                // change the standard behavior of the form in order to avoid server side save call.
                                beforesave : function(form, item, extraParam) {
                                    var modelDataKeys = null;
                                    var methodDefinitionCombo = Ext.getCmp(form._comboMethodDefinitionId);
                                    var selectedMethodDefinition = methodDefinitionCombo.store.getAt(methodDefinitionCombo.store.find("KeyMethodDefinition", methodDefinitionCombo.getValue()));

                                    var grid = Ext.getCmp(form._gridId);
                                    // grid is optional, so we test for existence.
                                    if (grid) {
                                        var storeDataModel = grid.store;
                                        modelDataKeys = Ext.pluck(Ext.pluck(storeDataModel.data.items, "data"), selectedMethodDefinition.get("KeyEntity"));
                                    }

                                    Ext.apply(item, {
                                                MethodName          : selectedMethodDefinition.data.MethodName,
                                                MethodQualifiedName : selectedMethodDefinition.data.MethodQualifiedName,
                                                MethodMsgCode       : selectedMethodDefinition.data.MsgCode

                                            });
                                    item.ModelDataKeys = modelDataKeys;
                                    this.isDirty = true; // we make sure Job form knows it has changed.
                                    if (item.KeyStep == null) {
                                        item.KeyStep = Ext.id();
                                        this.entity.Steps.push(item);
                                    }
                                    else {
                                        Ext.each(this.entity.Steps, function(step) {
                                                    if (step.KeyStep == item.KeyStep)
                                                        Ext.apply(step, item);
                                                });

                                    }

                                    this.bindStoreStep(this.entity);
                                    form.getParentWindow().close();

                                    return false;
                                },
                                scope      : this
                            }
                        },
                        create : {
                            title   : $lang("msg_step_add"),
                            iconCls : 'viz-icon-small-step-add'
                        },
                        update : {
                            title               : $lang("msg_step_update"),
                            iconCls             : 'viz-icon-small-step-update',
                            titleEntity         : 'Name',
                            serviceParamsEntity : [{
                                        name  : 'Key',
                                        value : 'KeyStep'
                                    }]
                        }

                    }

                };
                return paramConfig;

            },

            /**
             * @private Extract the list of the entity (Schedules and Steps) from the corresponding stores.
             */
            onBeforeSave       : function(form, entity, extraParam) {
                entity.Schedules = Ext.pluck(this.storeSchedule.data.items, "json");
                entity.Steps = Ext.pluck(this.storeStep.data.items, "json");
                return true;
            }
        });

Ext.reg('vizFormJob', Viz.form.Job);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.DataSerie
 * @extends Ext.FormPanel.
 */
Viz.form.DataSerie = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Chart} entity The chart entity the dataserie belong to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                     : function(config) {
                this._gridDataSeriePsetRatio = Ext.id();
                this._gridDataSerieColorElement = Ext.id();
                this._tabDataSerieColorElement = Ext.id();
                this._tabDataSeriePsetRatio = Ext.id();

                var Id = config.entity ? config.entity.KeyDataSerie : null;
                this.storeDataSeriePsetRatio = new Viz.store.DataSeriePsetRatio({
                            KeyDataSerie : Id
                        });
                this.storeDataSerieColorElement = new Viz.store.DataSerieColorElement({
                            KeyDataSerie : Id
                        });
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.DataSerie_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.DataSerie_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.DataSerie_FormUpdate,
                    tabConfig            : {
                        deferredRender : true,
                        items          : [{
                                    title      : $lang('msg_general'),
                                    iconCls    : 'viz-icon-small-form-update',
                                    items      : this.configFormGeneral(config),
                                    labelWidth : 180
                                }, {
                                    id         : this._tabDataSerieColorElement,
                                    title      : $lang('msg_dataseriecolorelement'),
                                    iconCls    : 'viz-icon-small-palette',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    hidden     : config.mode == 'create',
                                    items      : [this.configGridDataSerieColorElement(config)]
                                }, {
                                    id         : this._tabDataSeriePsetRatio,
                                    title      : $lang('msg_dataseriepsetratio'),
                                    iconCls    : 'viz-icon-small-dataseriepsetratio',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    hidden     : config.mode == 'create',
                                    items      : [this.configGridDataSeriePsetRatio(config)]
                                }]
                    },
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.DataSerie.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the general tab
             */
            configFormGeneral               : function(config) {
                return [{
                            name   : 'KeyDataSerie',
                            hidden : true
                        }, {
                            fieldLabel      : $lang('msg_localid'),
                            name            : 'LocalId',
                            KeyChart        : config.entity.KeyChart,
                            includeExisting : false,
                            xtype           : 'vizComboChartAvailableDataSerieLocalId',
                            displayField 	: 'Value',
                            allowBlank      : false,
                            listeners       : {
                                select : this.onSelectLocalId,
                                scope  : this
                            },
                            hideLabel       : config.entity.IsClassificationItem,
                            hidden          : config.entity.IsClassificationItem,
                            disabled        : config.mode == 'update'
                        }, {
                            fieldLabel           : $lang('msg_meter_classification'),
                            xtype                : 'viztreecomboClassificationItemMeter',
                            name                 : 'KeyClassificationItem',
                            originalDisplayValue : 'ClassificationItemLongPath',
                            KeyChart             : config.entity.KeyChart,
                            existing             : true,
                            rootVisible          : true,
                            listeners            : {
                                changevalue : this.onSelectClassificationItem,
                                clear       : this.onClearClassificationItem,
                                scope       : this
                            },
                            hideLabel            : !config.entity.IsClassificationItem,
                            hidden               : !config.entity.IsClassificationItem,
                            disabled             : config.mode == 'update'
                        }, {
                            xtype       : 'fieldset',
                            title       : $lang('msg_chart_display'),
                            iconCls     : 'viz-icon-small-chart-display',
                            collapsible : true,
                            collapsed   : true,
                            autoHeight  : true,
                            defaults    : {
                                anchor : Viz.Const.UI.Anchor,
                                xtype  : 'textfield'
                            },
                            items       : [{
                                        fieldLabel : $lang('msg_name'),
                                        name       : 'Name',
                                        allowBlank : false
                                        //hideLabel  : config.entity.IsClassificationItem,
                                        //hidden     : config.entity.IsClassificationItem
                                    }, {
                                        fieldLabel : $lang('msg_dataserie_hidden'),
                                        name       : 'Hidden',
                                        xtype      : 'vizcheckbox',
                                        flex       : 1
                                    }, {
                                        fieldLabel : $lang('msg_dataserie_hidevalues'),
                                        name       : 'HideLabel',
                                        xtype      : 'vizcheckbox',
                                        flex       : 1
                                    }, {
                                        fieldLabel   : $lang('msg_dataserie_type'),
                                        name         : 'Type',
                                        allowBlank   : false,
                                        xtype        : 'vizComboEnum',
                                        enumTypeName : 'Vizelia.FOL.BusinessEntities.DataSerieType,Vizelia.FOL.Common',
                                        value        : "" + Vizelia.FOL.BusinessEntities.DataSerieType.None,
                                        displayIcon  : true
                                    }, {
                                        fieldLabel   : $lang('msg_dataserie_linedashstyle'),
                                        name         : 'LineDashStyle',
                                        allowBlank   : false,
                                        xtype        : 'vizComboEnum',
                                        enumTypeName : 'Vizelia.FOL.BusinessEntities.LineDashStyle,Vizelia.FOL.Common',
                                        value        : "" + Vizelia.FOL.BusinessEntities.LineDashStyle.Solid,
                                        displayIcon  : false
                                    }, {
                                        fieldLabel : $lang('msg_dataserie_linewidth'),
                                        xtype      : 'vizSpinnerField',
                                        allowBlank : true,
                                        name       : 'LineWidth',
                                        minValue   : 0,
                                        maxValue   : 40,
                                        width      : 60
                                    }, {
                                        fieldLabel   : $lang('msg_dataserie_markertype'),
                                        name         : 'MarkerType',
                                        allowBlank   : false,
                                        xtype        : 'vizComboEnum',
                                        enumTypeName : 'Vizelia.FOL.BusinessEntities.MarkerType,Vizelia.FOL.Common',
                                        value        : "" + Vizelia.FOL.BusinessEntities.MarkerType.Circle,
                                        displayIcon  : false
                                    }, {
                                        fieldLabel : $lang('msg_dataserie_markersize'),
                                        xtype      : 'vizSpinnerField',
                                        allowBlank : true,
                                        name       : 'MarkerSize',
                                        minValue   : 0,
                                        maxValue   : 40,
                                        width      : 60
                                    }, {
                                        fieldLabel : $lang('msg_dataserie_unit'),
                                        name       : 'Unit'
                                    }, {
                                        fieldLabel : $lang('msg_chart_display_transparency'),
                                        xtype      : 'vizSpinnerField',
                                        allowBlank : true,
                                        name       : 'DisplayTransparency',
                                        // value : 0,
                                        minValue   : 0,
                                        maxValue   : 100,
                                        width      : 60
                                    }, {
                                        fieldLabel   : $lang('msg_dataserie_legendentry'),
                                        name         : 'DisplayLegendEntry',
                                        xtype        : 'vizComboEnum',
                                        enumTypeName : 'Vizelia.FOL.BusinessEntities.DataSerieLegendEntry,Vizelia.FOL.Common',
                                        allowBlank   : false,
                                        value        : "" + Vizelia.FOL.BusinessEntities.DataSerieLegendEntry.SUM
                                    }, {
                                        fieldLabel : $lang('msg_color'),
                                        name       : 'Color',
                                        xtype      : 'colorpickerfield'
                                    }, {
                                        fieldLabel : $lang('msg_chartaxis'),
                                        name       : 'KeyYAxis',
                                        xtype      : 'vizComboChartAxis',
                                        KeyChart   : config.entity.KeyChart,
                                        allowBlank : true
                                    }, {
                                        fieldLabel : $lang('msg_drawingcanvaschart_zindex'),
                                        xtype      : 'vizSpinnerField',
                                        allowBlank : true,
                                        name       : 'ZIndex',
                                        width      : 60
                                    }]
                        }, {
                            xtype       : 'fieldset',
                            title       : $lang('msg_dataserie_calculations'),
                            iconCls     : 'viz-icon-small-calculatedserie',
                            collapsible : true,
                            collapsed   : true,
                            autoHeight  : true,
                            defaults    : {
                                anchor : Viz.Const.UI.Anchor
                            },
                            items       : [{
                                        fieldLabel       : $lang('msg_dataserie_ratio'),
                                        xtype            : 'vizSpinnerField',
                                        allowBlank       : true,
                                        name             : 'Ratio',
                                        value            : 1,
                                        decimalPrecision : 5
                                    }, {
                                        fieldLabel       : $lang('msg_dataserie_limitbylegendentryminvalue') + ' >=',
                                        xtype            : 'vizSpinnerField',
                                        allowBlank       : true,
                                        name             : 'LimitByLegendEntryMinValue',
                                        decimalPrecision : 5
                                    }, {
                                        fieldLabel       : $lang('msg_dataserie_limitbylegendentrymaxvalue') + ' <=',
                                        xtype            : 'vizSpinnerField',
                                        allowBlank       : true,
                                        name             : 'LimitByLegendEntryMaxValue',
                                        decimalPrecision : 5
                                    }, {
                                        fieldLabel       : $lang('msg_enum_chartspecificanalysis_microchart') + ' - ' + $lang('msg_dataserie_target'),
                                        xtype            : 'vizSpinnerField',
                                        allowBlank       : true,
                                        name             : 'Target',
                                        // value : 1,
                                        decimalPrecision : 5
                                    }, {
                                        fieldLabel       : $lang('msg_enum_chartspecificanalysis_microchart') + ' - ' + $lang('msg_dataserie_targetelement'),
                                        xtype            : 'vizSpinnerField',
                                        allowBlank       : true,
                                        name             : 'TargetElement',
                                        // value : 1,
                                        decimalPrecision : 5
                                    }, {
                                        fieldLabel       : $lang('msg_dataserie_limitelementminvalue') + ' >=',
                                        xtype            : 'vizSpinnerField',
                                        allowBlank       : true,
                                        name             : 'LimitElementMinValue',
                                        decimalPrecision : 5
                                    }, {
                                        fieldLabel       : $lang('msg_dataserie_limitelementmaxvalue') + ' <=',
                                        xtype            : 'vizSpinnerField',
                                        allowBlank       : true,
                                        name             : 'LimitElementMaxValue',
                                        decimalPrecision : 5
                                    }, {
                                        fieldLabel : $lang('msg_dataserie_limitelementcount'),
                                        name       : 'LimitElementCount',
                                        xtype      : 'vizcheckbox',
                                        flex       : 1
                                    }, {
                                        fieldLabel : $lang('msg_dataserie_limitelementrawdata'),
                                        name       : 'LimitElementRawData',
                                        xtype      : 'vizcheckbox',
                                        flex       : 1
                                    }]
                        }];
            },

            /**
             * The configuration for the grid axis.
             * @return {object}
             */
            configGridDataSeriePsetRatio    : function(config) {
                return {
                    xtype        : 'vizGridDataSeriePsetRatio',
                    id           : this._gridDataSeriePsetRatio,
                    border       : true,
                    store        : this.storeDataSeriePsetRatio,
                    loadMask     : false,
                    KeyDataSerie : config.entity ? config.entity.KeyDataSerie : '',
                    listeners    : {
                        beforedeleterecord : function() {
                            this.deleteSelectedRowsFromGrid(this._gridDataSeriePsetRatio);
                            return false;
                        },
                        scope              : this
                    }
                };
            },

            /**
             * The configuration for the grid axis.
             * @return {object}
             */
            configGridDataSerieColorElement : function(config) {
                return {
                    xtype        : 'vizGridDataSerieColorElement',
                    id           : this._gridDataSerieColorElement,
                    border       : true,
                    store        : this.storeDataSerieColorElement,
                    loadMask     : false,
                    KeyDataSerie : config.entity ? config.entity.KeyDataSerie : '',
                    listeners    : {
                        beforedeleterecord : function() {
                            this.deleteSelectedRowsFromGrid(this._gridDataSerieColorElement);
                            return false;
                        },
                        scope              : this
                    }
                };
            },

            /**
             * Handler for the ChartType combo select event.
             */
            onSelectLocalId                 : function(combo, record, index) {
                var form = this.getForm();
                var field = form.findField('Name');
                var currentValue = field.getValue();
                if (Ext.isEmpty(currentValue)) {
                    field.setValue(record.data.MsgCode);
                }
            },
            /**
             * Handler for the ClassificationItem tree change event
             * @param {} field
             * @param {} value
             * @param {} node
             */
            onSelectClassificationItem      : function(field, value, node) {
                var form = this.getForm();
                if (node) {
                    var field = form.findField('Name');
                    field.setValue(node.attributes.entity.LongPath || node.attributes.entity.Title);
                    //field.disable();

                    field = form.findField('LocalId');
                    field.setValue(node.attributes.entity.LocalId);
                    field.disable();
                }
            },

            /**
             * Handler for the ClassificationItem tree clear event
             */
            onClearClassificationItem       : function() {
                var form = this.getForm();

                var field = form.findField('Name');
                field.setValue('');
                field.enable();

                field = form.findField('LocalId');
                field.setValue('');
                field.enable();
            },
            /**
             * Override the loadValues function to update the Color field based on ColorR,ColorG,ColorB
             */
            loadValues                      : function() {
                var form = this.getForm();
                if (!Ext.isEmpty(this.entity.ColorR)) {
                    form.findField('Color').setValue(new Viz.RGBColor(String.format("rgb({0},{1},{2})", this.entity.ColorR, this.entity.ColorG, this.entity.ColorB)).toHex());
                }
                if (this.mode == "update")
                    form.findField('LocalId').disable();

                Viz.form.DataSerie.superclass.loadValues.apply(this, arguments)
            },

            /**
             * Override the onBeforeSave function to update ColorR,ColorG and ColorB fields based on the Color component
             */
            onBeforeSave                    : function(form, item, extraParams) {
                var value = this.getForm().findField('Color').getValue();
                if (value) {
                    var rgbArray = new Viz.RGBColor(value).toRGBArray();
                    item.ColorR = rgbArray[0];
                    item.ColorG = rgbArray[1];
                    item.ColorB = rgbArray[2];
                }
                else {
                    item.ColorR = null;
                    item.ColorG = null;
                    item.ColorB = null;
                }
                if (Ext.isEmpty(item.KeyClassificationItem))
                    item.KeyClassificationItem = null;

                if (this.storeDataSeriePsetRatio.hasLoaded)
                    this.storeDataSeriePsetRatio.save();
                if (this.storeDataSerieColorElement.hasLoaded)
                    this.storeDataSerieColorElement.save();

                Ext.apply(extraParams, {
                            ratios        : this.storeDataSeriePsetRatio.crudStore,
                            colorelements : this.storeDataSerieColorElement.crudStore
                        });
                return true;
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                   : function(form, action) {
                Viz.form.DataSerie.superclass.onSaveSuccess.apply(this, arguments);

                if (this.closeonsave) return;
                
                var keyDataSerie = this.form.entity.KeyDataSerie;
                this.getForm().findField('LocalId').disable();
                this.getForm().findField('KeyClassificationItem').disable();

                var tabpanel = this.getTabContainer();

                tabpanel.unhideTabStripItem(this._tabDataSerieColorElement);
                var grid = Ext.getCmp(this._gridDataSerieColorElement);
                grid.KeyDataSerie = grid.store.serviceParams.KeyDataSerie = keyDataSerie;
                grid.store.reload();

                tabpanel.unhideTabStripItem(this._tabDataSeriePsetRatio);
                grid = Ext.getCmp(this._gridDataSeriePsetRatio);
                grid.KeyDataSerie = grid.store.serviceParams.KeyDataSerie = keyDataSerie;
                grid.store.reload();
            }
        });

Ext.reg('vizFormDataSerie', Viz.form.DataSerie);
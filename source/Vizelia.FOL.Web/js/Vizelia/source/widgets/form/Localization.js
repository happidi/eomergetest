﻿Ext.namespace("Viz.form");

/**
 * @class Viz.form.Localization
 * @extends Viz.form.FormPanel
 */
Viz.form.Localization = Ext.extend(Viz.form.FormPanel, {

    /**
     * Ctor.
     * @param {Object} config
     */
    constructor : function(config) {
        config = config || {};
        var defaultConfig = {};
        var forcedConfig = {};

        Ext.applyIf(config, defaultConfig);
        Ext.apply(config, {
                    serviceHandlerCreate : Viz.Services.CoreWCF.LocalizationResource_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.LocalizationResource_FormUpdate,
                    messageSaveEnd       : 'LocalizationChange',
                    onSaveSuccess        : function(form, action) {
                        // in case the form didn't open throgh a grid
                        if (!this.initialConfigFromGrid) {
                            this.initialConfigFromGrid = {
                                update : {
                                    title       : $lang("msg_update"),
                                    titleEntity : 'Key',
                                    iconCls     : 'viz-icon-small-localization-update'
                                }
                            };
                        }
                        Viz.form.Localization.superclass.onSaveSuccess.apply(this, arguments);
                        if (this.onSaveSuccessCombo) {
                            this.onSaveSuccessCombo(form, action);
                        }
                        return true;
                    },
                    items                : [{
                                fieldLabel : $lang('msg_localization_key'),
                                name       : 'Key',
                                // vtype : 'localization',
                                disabled   : config.mode == 'create' ? false : true,
                                allowBlank : false
                            }, {
                                fieldLabel : $lang('msg_localization_language'),
                                allowBlank : false,
                                name       : 'Culture',
                                // hiddenName : 'language',
                                xtype      : 'vizComboLocalizationCulture',
                                autoLoad   : config.mode == 'create' ? true : false,
                                disabled   : config.mode == 'create' ? false : true
                                // disabled : config.mode == 'create' ? false : false
                        }   , {
                                fieldLabel : $lang('msg_localization_value'),
                                xtype      : 'textarea',
                                name       : 'Value',
                                allowBlank : false,
                                anchor     : Viz.Const.UI.Anchor + ' -60'
                            }]
                });
        Viz.form.Localization.superclass.constructor.call(this, config);

    }

        /**
         * Handler before save that gives a chance to send the 2 stores as extra parameters.
         * @param {Ext.form.FormPanel} form
         * @param {Object} item
         * @param {Object} extraParams
         * @return {Boolean}
         */
        /*
         * onBeforeSave : function(form, item, extraParams) { Ext.apply(extraParams, { language : item.language }); return true; }
         */
    });

Ext.reg("vizFormLocalization", Viz.form.Localization);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Link
 * @extends Ext.FormPanel.
 */
Viz.form.Link = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                            : function(config) {
                config = config || {};

                var defaultConfig = {
                    mode_load: 'remote',
                    serviceHandler: Viz.Services.CoreWCF.Link_FormLoad,
                    serviceHandlerCreate: Viz.Services.CoreWCF.Link_FormCreate,
                    serviceHandlerUpdate: Viz.Services.CoreWCF.Link_FormUpdate,
                    items: this.configFormGeneral(config)
                };
                var forcedConfig = { };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.Link.superclass.constructor.call(this, config);
            },



            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                      : function(config) {
                return [{
                            name   : 'KeyLink',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_localid'),
                            name: 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_link_value'),
                            name       : 'LinkValue',
                            allowBlank : false
                        }, 
                        {
                            fieldLabel : $lang('msg_portalwindow_icon'),
                            name       : 'IconCls',
                            xtype      : 'vizComboCss',
                            selector   : '.viz-portal-large-',
                            allowBlank : true,
                            listeners  : {
                                select : function(combo, record, index) {
                                    this.form.findField('KeyImage').clearValue();
                                    this.form.findField('KeyImage').clearInvalid();
                                },
                                scope  : this
                            }
                        }, Viz.combo.DynamicDisplayImage.factory({
                                    fieldLabel : $lang('msg_portalwindow_customicon'),
                                    name       : 'KeyImage',
                                    type       : Vizelia.FOL.BusinessEntities.DynamicDisplayImageType.Icon,
                                    allowBlank : true,
                                    listeners  : {
                                        select : function(combo, record, index) {
                                            this.form.findField('IconCls').clearValue();
                                            this.form.findField('IconCls').clearInvalid();
                                        },
                                        scope  : this
                                    }
                                }),
                    {
                        fieldLabel: $lang('msg_link_parent_link'),
                        name: 'KeyParent',
                        xtype: 'vizComboLink',
                        serviceHandler: Viz.Services.CoreWCF.Link_GetStoreRootOnly,
                        currentId: config.entity.KeyLink
                        
                    },
                    Viz.combo.Localization.factory({
                        fieldLabel: $lang('msg_link_name')
                    }),
                    Viz.combo.Localization.factory({
                        fieldLabel: $lang('msg_link_group_name'),
                        name: 'GroupNameMsgCode'
                    })
                    
                ]
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                           : function(form, item, extraParams) {
            	if (!this.validateIconClsKeyImageFields()) return false;
           
                Viz.util.MessageBusMgr.fireEvent.defer(300, Viz.util.MessageBusMgr, ['LinkChange', {}]);
                return true;
            },
            
            /**
             * Validates that either IconCls or KeyImage is picked.
             * @return {Boolean}
             */
            validateIconClsKeyImageFields : function() {
                var fieldIcon = this.getForm().findField("IconCls");
                var fieldImage = this.getForm().findField("KeyImage");
                if (!fieldIcon.value && !fieldImage.value) {
                    var errorMsg = $lang('error_msg_pick_icon_or_custom_icon');
                    fieldIcon.markInvalid(errorMsg);
                    fieldImage.markInvalid(errorMsg);
                    return false;
                }

                return true;
            }
        });
Ext.reg('vizFormLink', Viz.form.Link);

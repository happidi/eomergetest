﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Tenant
 * @extends Ext.FormPanel.
 */
Viz.form.Tenant = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                this._fieldTenantName = Ext.id();
                this._fieldTenantUrl = Ext.id();
                this._fieldpasswordId = Ext.id();

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.TenancyWCF.Tenant_FormLoad,
                    serviceHandlerCreate : Viz.Services.TenancyWCF.Tenant_FormCreate,
                    serviceHandlerUpdate : Viz.Services.TenancyWCF.Tenant_FormUpdate,
                    isLongRunningSubmit  : true,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Tenant.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            fieldLabel : $lang('msg_tenant_admin_password'),
                            id         : this._fieldpasswordId + '_fake',
                            inputType  : 'password',
                            name       : 'AdminPasswordFake',
                            allowBlank : true,
                            hidden     : true,
                            emptyText  : 'This information is protected'
                        }, {
                            name   : 'KeyTenant',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_tenant_name'),
                            name       : 'Name',
                            allowBlank : false,
                            id         : this._fieldTenantName,
                            disabled   : config && config.mode == 'update'
                        }, {
                            fieldLabel : $lang('msg_tenant_admin_email'),
                            name       : 'AdminEmail',
                            allowBlank : false,
                            vtype      : 'email'
                        }, {
                            fieldLabel : $lang('msg_tenant_url'),
                            name       : 'Url',
                            // vtype: 'simpleUrlValidator',
                            allowBlank : false,
                            id         : this._fieldTenantUrl
                        }, {
                            fieldLabel : $lang('msg_tenant_admin_password'),
                            id         : this._fieldpasswordId,
                            inputType  : 'password',
                            name       : 'AdminPassword',
                            allowBlank : config && config.mode == 'update',
                            hidden     : config && config.mode == 'update',
                            emptyText  : 'This information is protected'
                        }, {
                            fieldLabel           : $lang('msg_password_confirm'),
                            name                 : 'confirmPassword',
                            validator            : Ext.form.VTypes.passwordconfirmValidator,
                            inputType            : 'password',
                            initialPasswordField : this._fieldpasswordId,
                            emptyText            : 'This information is protected',
                            allowBlank           : config && config.mode == 'update',
                            hidden               : config && config.mode == 'update'
                            // }, {
                        // fieldLabel : $lang('msg_tenant_isolated'),
                        // name : 'IsIsolated',
                        // xtype : 'vizcheckbox',
                        // checked : false,
                        // hidden : true
                    }];
            }
        });

Ext.reg('vizFormTenant', Viz.form.Tenant);

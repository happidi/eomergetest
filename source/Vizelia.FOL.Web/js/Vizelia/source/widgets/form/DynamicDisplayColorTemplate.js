﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.PaletteColor
 * @extends Ext.FormPanel.
 */
Viz.form.DynamicDisplayColorTemplate = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor         : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.DynamicDisplayColorTemplate_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.DynamicDisplayColorTemplate_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.DynamicDisplayColorTemplate_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.DynamicDisplayColorTemplate.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral   : function(config) {
                return [{
                            name   : 'KeyDynamicDisplayColorTemplate',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_dynamicdisplaycolortemplate_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_id'),
                            name       : 'LocalId',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_dynamicdisplaycolortemplate_startcolor'),
                            name       : 'StartColor',
                            allowBlank : false,
                            xtype      : 'colorpickerfield'
                        }, {
                            fieldLabel : $lang('msg_dynamicdisplaycolortemplate_endcolor'),
                            name       : 'EndColor',
                            allowBlank : false,
                            xtype      : 'colorpickerfield'
                        }, {
                            fieldLabel : $lang('msg_dynamicdisplaycolortemplate_bordercolor'),
                            name       : 'BorderColor',
                            allowBlank : false,
                            xtype      : 'colorpickerfield'
                        }];
            },

            /**
             * Override the loadValues function to update the Color fields based on ColorR,ColorG,ColorB
             */
            loadValues          : function() {
                var form = this.getForm();
                var colorFields = this.getColorFieldsArray();
                Ext.each(colorFields, function(colorField) {
                            if (!Ext.isEmpty(this.entity[colorField + 'R'])) {
                                form.findField(colorField).setValue(new Viz.RGBColor(String.format("rgb({0},{1},{2})", this.entity[colorField + 'R'], this.entity[colorField + 'G'], this.entity[colorField + 'B'])).toHex());
                            }
                        }, this);
                Viz.form.DynamicDisplayColorTemplate.superclass.loadValues.apply(this, arguments)
            },

            /**
             * @private Return an Array with all the ColorFields.
             * @return {Array}
             */
            getColorFieldsArray : function() {
                return ['StartColor', 'EndColor', 'BorderColor'];

            },
            /**
             * Override the onBeforeSave function to update ColorR,ColorG and ColorB fields based on the Color component
             */
            onBeforeSave        : function(form, item, extraParams) {

                var colorFields = this.getColorFieldsArray();

                Ext.each(colorFields, function(colorField) {
                            var value = this.getForm().findField(colorField).getValue();
                            if (value) {
                                var rgbArray = new Viz.RGBColor(value).toRGBArray();
                                item[colorField + 'R'] = rgbArray[0];
                                item[colorField + 'G'] = rgbArray[1];
                                item[colorField + 'B'] = rgbArray[2];
                            }
                            else {
                                item[colorField + 'R'] = null;
                                item[colorField + 'G'] = null;
                                item[colorField + 'B'] = null;
                            }
                        }, this);

                return true;
            }
        });

Ext.reg('vizFormDynamicDisplayColorTemplate', Viz.form.DynamicDisplayColorTemplate);
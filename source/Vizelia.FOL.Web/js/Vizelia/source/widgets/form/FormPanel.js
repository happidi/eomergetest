﻿Ext.namespace("Viz.form");
/**
 * @class Viz.form.FormPanel
 * <p>
 * Standard Vizelia form. No border, transparent, msg on side.
 * </p>
 * <p>
 * Standard form for Vizjs with save and cancel buttons.
 * <p>
 * <p>
 * For a full functionnal form that loads and saves (create,update) data the following properties should be defined in the extend:
 * </p>
 * <ul>
 * <li><b>entityType</b> : The type name of the entity</li>
 * <li><b>serviceHandler</b> : the service used to load existing data. Usually the parameter is <tt>Key</tt></li>
 * <li><b>serviceHandlerCreate</b> : the service used to create a new item. Usually the parameter is <tt>item</tt></li>
 * <li><b>serviceHandlerUpdate</b> : the service used to update data. Usually the parameter is <tt>item</tt></li>
 * <li><b>serviceHandlerUpdateBatch</b> : the service used to update batch data. Usually the parameter is <tt>keys</tt> and <tt>item</tt></li>
 * <li><b>loadValues</b> : this function could be overrided</li>
 * <li><b>saveItem</b> : this function could be overrided</li>
 * <li><b>beforesave</b> : this event gives an opportunity to change the item before it is sent to serviceHandlerCreate or serviceHandlerUpdate, or to simply cancel the save.</li>
 * <li><b>mode_load</b> ('remote', or 'local') : indicates if the form in mode == 'update' should loads its data from entity (locally) or should use serviceHandler and serviceParams.</li>
 * <li><b>hasValidationToolbar</b> true to display a validation toolbar.</li>
 * <li><b>isWizard</b> true to display the form has a wizard (card layout), false to display as Tabpanel. </li>
 * <li><b>cancelable</b> true to display the cancel button, false to hide it. </li>
 * </ul>
 * <p>
 * The the following properties should be defined for an instance:
 * <ul>
 * <li>mode ('create', or 'update') : this indicates which is the serviceHandler that will be used when saving.</li>
 * <li>serviceParams : passed for load (and saves although it doesnt matter for save)
 * 
 * <pre><code>
 * serviceParams : {
 *     Key : ''
 * }
 * </code></pre>
 * 
 * </li>
 * <li>entity : represents the entity that should be loaded. Can be left null if mode == 'create' or mode_load == 'remote'.</li>
 * <li>entityParent : represents a parent for the entity. Can be left null if mode == 'create' or mode_load == 'remote'.</li>
 * <li>psetConfig : the configuration for the psets of the entity.</li>
 * <li>tabConfig : the configuration if the form content should be injected into a tab.</li>
 * <ul>
 * <p>
 * The complete list of parameter for loadWCF is:
 * <p class="sub-desc">
 * 
 * <pre><code>
 * this.getForm().loadWCF({
 *             serviceHandler : Viz.Services.AuthenticationWCF.User_GetByUserName,
 *             scope          : this,
 *             extraParams    : {
 *                 username : this.user.UserName
 *             },
 *             waitMsg        : true,
 *             success        : function(form, action) {
 *                 // Viz.Services.AuthenticationWCF.User_GetByUserName does not return a FormResponse object. Instead of looking for action.result.data , we access the user loaded via action.result.
 *                 var user = action.result;
 *                 this.setUser(user);
 *             }
 *         });
 * </code></pre>
 * 
 * </p>
 * </p>
 * @extends Ext.FormPanel
 * @xtype vizForm
 */
Viz.form.FormPanel = Ext.extend(Ext.FormPanel, {
            /**
             * @cfg {Object} entity (optional) The entity is used when loading the form in mode <tt>update</tt>
             */
            /**
             * Ctor. Defines the standard configuration.
             * @param {Object} config
             */
            /**
             * @cfg {Integer} errorMessageBoxWidth Controls how wide the error message box is. (defaults to 400).
             */
            errorMessageBoxWidth       : 400,
            constructor                : function(config) {
                this.isDirty = false;
                config = config || {};
                this._formId = config.id || Ext.id();
                this._tabContainerId = Ext.id();
                this._auditPanelId = Ext.id();
                this._auditTabPanelId = Ext.id();
                this._buttonSaveId = Ext.id();
                this._buttonApplyId = Ext.id();
                this._buttonWizardNextId = Ext.id();
                this._buttonWizardPreviousId = Ext.id();
                this._imgWizardStepId = Ext.id();
                this._validationStatusId = Ext.id();
                this._auditView = {
                    currentView    : 'DetailedView',
                    detailedViewId : Ext.id(),
                    gridViewId     : Ext.id()
                };
                var defaultConfig = {
                    id                   : this._formId,
                    /**
                     * @cfg {String} mode The mode of the form, either 'create' or 'update'.
                     */
                    mode                 : 'create',
                    groupPset            : true,
                    trackResetOnLoad     : true,
                    border               : false,
                    bodyStyle            : 'padding: 5px;background-color: transparent;',
                    defaultType          : 'textfield',
                    labelWidth           : 150,
                    autoScroll           : true,
                    defaults             : {
                        msgTarget : 'side',
                        anchor    : Viz.Const.UI.Anchor
                    },
                    /**
                     * @cfg {Boolean} closeonsave Indicates if the form should autoclose when save is successful. Default to <tt>true</tt>
                     */
                    closeonsave          : false,
                    /**
                     * @cfg {Boolean} hasValidationToolbar <tt>true</tt> to display a validation toolbar. Default to <tt>true</tt>
                     */
                    hasValidationToolbar : true,
                    /**
                     * @cfg {String} mode_load Acceptable values are:
                     * <ul>
                     * <li><b><tt>'remote'</tt></b> :
                     * <p class="sub-desc">
                     * Automatically loads the entity from server via the serviceHandler function.
                     * <p>
                     * Usually the serviceHandler function has the following signature : <tt>FormResponse function(string Key)</tt>
                     * </p>
                     * <p>
                     * serviceHandler needs a serviceParam object that will contains
                     * </p>
                     * <p>
                     * <code>
                     * serviceParam : {
                     *     Key : entity.Id
                     * }
                     * </code>
                     * </p>
                     * </p>
                     * </li>
                     * <li><b><tt>'local'</tt></b> : <b>Default</b>
                     * <p class="sub-desc">
                     * Loads the entity without calling the server.
                     * </p>
                     * </li>
                     * </ul>
                     */
                    mode_load            : 'local',
                    /**
                     * @cfg {String} messageSaveStart The name of the message that should to publish when save begins.
                     */
                    /**
                     * @cfg {String} messageSaveEnd the name of the message to publish when save ends.
                     */
                    /**
                     * @cfg {Object} psetConfig the pset configuration if the form should expose pset. <code>
                     * psetConfig : {
                     *      IfcType   : 'IfcBuilding',
                     *      entityKey : 'KeyBuilding',
                     *      filterByClassificationItem : true
                     * }
                     * </code>
                     */
                    /**
                     * @cfg updatebatchkeys the array of keys to update in case mode="updatebatch"
                     */
                    /**
                     * @cfg {Object} tabConfig the tab configuration if the form should expose a tab. <code>
                     * tabConfig : {
                     *      deferredRender : true,
                     *      title   : $lang('msg_general'),
                     *      items : [{                     <- optional
                     *              title : 'tab 1',
                     *              items : []
                     *      }, {
                     *              title : 'tab 2',
                     *              items : []
                     *      }]
                     * }
                     * </code>
                     */
                    /**
                     * @cfg {Boolean} applyEnabled Determines whether the Apply button is enabled or not. Default is true.
                     */
                    applyEnabled         : true,
                    buttons              : [{
                                id       : this._buttonWizardPreviousId,
                                text     : $lang('msg_wizard_previous'),
                                width    : 80,
                                cls      : 'x-toolbar-standardbutton',
                                iconCls  : 'viz-icon-small-wizard-previous',
                                hidden   : !(config.isWizard === true && config.isWizardSingleTab !== true),
                                handler  : this.onWizardPrevious,
                                disabled : true,
                                scope    : this
                            }, {
                                id      : this._buttonWizardNextId,
                                text    : $lang('msg_wizard_next'),
                                width   : 80,
                                cls     : 'x-toolbar-standardbutton',
                                iconCls : 'viz-icon-small-wizard-next',
                                hidden  : !(config.isWizard === true && config.isWizardSingleTab !== true),
                                handler : this.onWizardNext,
                                scope   : this
                            }, {
                                id      : this._buttonSaveId,
                                text    : $lang('msg_ok'),
                                width   : 80,
                                cls     : 'x-toolbar-standardbutton',
                                iconCls : 'viz-icon-small-save',
                                handler : function() {
                                    this.closeonsave = true;
                                    this.saveItem();
                                },
                                hidden  : config.isWizard === true && config.isWizardSingleTab !== true,
                                scope   : this
                            }, {
                                id      : this._buttonApplyId,
                                text    : $lang('msg_apply'),
                                width   : 80,
                                cls     : 'x-toolbar-standardbutton',
                                iconCls : 'viz-icon-small-apply',
                                handler : function() {
                                    this.closeonsave = false;
                                    this.saveItem();
                                },
                                // we show this button on the render event if the config closeonsave was set to true.
                                hidden  : true,
                                scope   : this
                            }, {
                                text    : $lang('msg_cancel'),
                                width   : 80,
                                cls     : 'x-toolbar-standardbutton',
                                iconCls : 'viz-icon-small-cancel',
                                hidden  : config.cancelable === false || (config.isWizard === true && config.isWizardSingleTab !== true),
                                handler : function() {
                                     this.closeParentWindow(true);
                                },
                                scope   : this
                            }],
                    /**
                     * @cfg {Boolean} auditEnabled Indicates if the form should create a audit tab. Default to <tt>true</tt>
                     */
                    auditEnabled         : true,
                    /**
                     * @cfg {Boolean} isLongRunningSubmit Indicates whether the form submits as a long running operation or not
                     */
                    isLongRunningSubmit  : false,
                    /**
                     * @cfg {Boolean} readonly Indicates whether the form should be readonly. Default to <tt>false</tt>
                     */
                    readonly             : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                if (Ext.isArray(config.buttons) && Ext.isArray(config.extraButtons)) {
                    config.buttons = config.extraButtons.concat(config.buttons);
                }
                if (config.hasValidationToolbar) {
                    this._validationStatusPlugin = new Ext.ux.ValidationStatus({
                                id   : this._validationStatusId,
                                form : this._formId
                            });
                    config.bbar = new Ext.ux.StatusBar({
                                defaultText : $lang('msg_ready'),
                                plugins     : this._validationStatusPlugin,
                                items       : config.buttons
                            });
                    delete config.buttons;
                }

                // Audit Module
                if (config.entity && config.entity.__type && config.auditEnabled) {
                    var entityName = config.auditEntityName || Viz.getEntityName(config.entity.__type);
                    if ($isAuditEnabled(entityName)) {
                        if (!config.auditConfig) {
                            config.auditConfig = {
                                entityName : entityName,
                                entityKey  : Viz.getServiceParamsKey(entityName, config.serviceParams)
                            };
                        }
                        else {
                            config.auditConfig.entityKey = config.serviceParams.Key;
                        }
                    }
                    else {
                        config.auditEnabled = false;
                        config.auditConfig = null;
                    }
                }
                else {
                    config.auditEnabled = false;
                    config.auditConfig = null;
                }

                this.buildDetailedAudit(config);

                this.buildTab(config);

                Viz.form.FormPanel.superclass.constructor.call(this, config);

                this.entity = this.entity || {};
                this.addEvents(
                        /**
                         * @event beforesave Fires before the save action.
                         * @param {Viz.form.FormPanel} this
                         * @param {Object} item the item passed as a parameter of crud handler service (FormCreate or FormUpdate)
                         * @param {Object} extraParams the extraParams so it could be built from the outside.
                         */
                        'beforesave',
                        /**
                         * @event savesuccess Fires after save in case the operation succeeded.
                         * @param {Viz.form.FormPanel} this
                         * @param {Object} item The item beeing saved.
                         * @param {Ext.form.Action} action
                         */
                        'savesuccess',
                        /**
                         * @event savefailure Fires after save in case the operation failed.
                         * @param {Viz.form.FormPanel} this
                         * @param {Ext.form.Action} action
                         */
                        'savefailure',
                        /**
                         * @event beforeloadvalues Fires before loadValues.
                         * @param {Viz.form.FormPanel} this
                         * @param {Object} value
                         */
                        'beforeloadvalues',
                        /**
                         * @event afterloadvalues Fires after loadValues.
                         * @param {Viz.form.FormPanel} this
                         * @param {Object} value
                         */
                        'afterloadvalues',

                        /**
                         * @event beforeopendetail Fires before opening a detail window.
                         * @param {Viz.form.FormPanel) this
                         * @param [Object} config the configuration of the detail window.
                         * @param {String} Id the id of the compenent.
                         */
                        'beforeopendetail');

                this.on("beforesave", this.onBeforeSave, this);
                this.on("savesuccess", this.onSaveSuccess, this);
                this.on("savefailure", this.onSaveFailure, this);
                this.on("beforeloadvalues", this.onBeforeLoadValues, this);
                this.on("afterloadvalues", function() {
                            // add support for readonly configuration
                            this.loadExtraTabs();

                            if (this.readonly)
                                this.setReadOnly(this.readonly);
                            this.onAfterLoadValues.apply(this, arguments);
                        }, this);
                this.on('afterrender', function() {
                            this.mask();
                            this.loadValues.defer(100, this);
                        }, this);
                this.on("beforeopendetail", this.onBeforeOpenDetail, this);
            },
            /**
             * Builds the detailed audit data view based by config.auditConfig
             * @param {} config
             */
            buildDetailedAudit         : function(config) {
                if (config.auditEnabled && config.mode == "update") {
                    var auditStore = new Viz.store.DetailedAudit({
                                // xtype : 'vizStoreDetailedAudit',
                                entityName : config.auditConfig.entityName,
                                entityKey  : config.auditConfig.entityKey
                            });
                    auditStore.load();

                    var auditTemplate = new Ext.XTemplate(
                            /**/
                            '<tpl for=".">'
                            /**/
                            , '<div class="audit-template">'
                            /**/
                            , '<div class="x-grid3-summary-row" {[this.getToolTip(values)]}>'
                            /**/
                            , '<tpl if="xindex &lt;= 3">'
                            /**/
                            , '<span class="viz-icon-small-arrowgray expanded"></span>'
                            /**/
                            , '</tpl>'
                            /**/
                            , '<tpl if="xindex &gt; 3">'
                            /**/
                            , '<span class="viz-icon-small-arrowgray "></span>'
                            /**/
                            , '</tpl>'
                            /**/
                            , '<span class="viz-audit-row-span "><b>{[this.headerText(values)]}</b></span>'
                            /**/
                            , '</div>'
                            /**/
                            , '<tpl if="xindex &lt;= 3">'
                            /**/
                            , '<div class="audit-detail">'
                            /**/
                            , '</tpl>'
                            /**/
                            , '<tpl if="xindex &gt; 3">'
                            /**/
                            , '<div class="audit-detail x-hidden">'
                            /**/
                            , '</tpl>'
                            /**/
                            , '<table class="x-grid3-summary-table" width="100%" border="0" cellspacing="5" cellpadding="0">'
                            /**/
                            , '<thead><tr><th width="20%"><b>' + $lang('msg_audit_fieldname') + '</b></th><th width="40%"><b>' + $lang('msg_audit_oldvalue') + '</b></th><th width="40%"><b>' + $lang('msg_audit_newvalue') + '</b></th></tr></thead>'
                            /**/
                            , '<tpl for="ChangedFields">'
                            /**/
                            , '<tbody><tr><td>{[Ext.util.Format.htmlEncode(values.FieldName)]}</td><td>{[this.notNullDisplay(values.OldValue)]}</td><td>{[this.notNullDisplay(values.NewValue)]}</td></tr></tbody>'
                            /**/
                            , '</tpl>'
                            /**/
                            , '</table>'
                            /**/
                            , '</div>'
                            /**/
                            , '</div>'
                            /**/
                            , '</tpl>', {
                                getToolTip : function(values) {
                                    var retVal = 'ext:qtip="' + $lang('msg_audit_changedon') + ' ' + values.ChangeTime.format(Viz.getLocalizedDateTimeFormat(true)) + '"';
                                    return Ext.util.Format.htmlEncode(retVal);
                                }
                            }, {
                                headerText : function(values) {
                                    return Ext.util.Format.htmlEncode(String.format($lang('msg_audit_headertext'), Viz.date.prettyDate(values.ChangeTime), values.ModifiedBy));
                                }
                            }, {
                                notNullDisplay : function(value) {
                                    return Ext.util.Format.htmlEncode(value != null ? value : "");
                                }
                            });

                    var auditDataView = {
                        xtype      : 'dataview',
                        store      : auditStore,
                        tpl        : auditTemplate,
                        autoScroll : true,
                        emptyText  : $lang('msg_audit_nohistory'),
                        listeners  : {
                            // set the click handler of the array icon
                            click : function(a, b, target) {
                                var cls = target.attributes['class'];
                                if (cls != undefined && cls.nodeValue.indexOf('viz-icon-small-arrowgray') != -1) {
                                    Ext.get(target).toggleClass('expanded');
                                    Ext.get(target).parent().next('div').toggleClass('x-hidden');
                                }
                            }
                        }
                    };

                    config.auditConfig.detailedView = {
                        id          : this._auditView.detailedViewId,
                        xtype       : 'container',
                        layout      : 'fit',
                        width       : 600,
                        height      : 400,
                        forceLayout : true,
                        title       : $lang('msg_audit_detailedview'),
                        items       : [auditDataView]
                    };
                }
                else {
                    config.auditConfig = null;
                }
            },
            /**
             * Injects the content of the form into a tab in case a tabConfuig or a psetConfig was provided.
             * @param {Object} config
             */
            buildTab                   : function(config) {
                if (config.tabConfig || config.psetConfig || (config.auditConfig && config.auditEnabled)) {
                    config.layout = 'fit';

                    config.bodyStyle = config.isWizard ? 'padding:2px;background-color: transparent;' : 'padding:0px;background-color: transparent;';
                    var deferredRender = false;
                    if (config.tabConfig && config.tabConfig.deferredRender)
                        deferredRender = config.tabConfig.deferredRender;

                    var activeTab = 0;
                    if (config.tabConfig && config.tabConfig.activeTab)
                        activeTab = config.tabConfig.activeTab;

                    // simple case of injection of the form content into a tab
                    var items = [{
                                // tbar : config.mode == 'update' ? [] : null,
                                title : config.tabConfig ? config.tabConfig.title || $lang('msg_general') : $lang('msg_general'),
                                items : config.items
                            }];
                    // more complex case of injection of multiple tabs provided through tabConfig.items.
                    if (config.tabConfig) {
                        if (config.tabConfig.items) {
                            items = [].concat(config.tabConfig.items);
                        }
                    }
                    config.items = [{
                                id                 : this._tabContainerId,
                                xtype              : config.isWizard ? 'container' : 'tabpanel',
                                layout             : config.isWizard ? 'card' : null,
                                layoutConfig       : config.isWizard ? {
                                    animate        : true,
                                    deferredRender : deferredRender
                                } : null,
                                layoutOnCardChange : true,
                                activeItem         : config.isWizard ? 0 : null,
                                activeTab          : activeTab,
                                enableTabScroll    : true,
                                deferredRender     : deferredRender,
                                bodyStyle          : 'padding:0px;background-color: transparent;',
                                border             : false,
                                defaults           : {
                                    layout      : 'form',
                                    border      : config.isWizard ? true : false,
                                    hideMode    : 'offsets', // to prevent mark invalid icon to show over the fieldLabel
                                    autoScroll  : true,
                                    defaultType : 'textfield',
                                    bodyStyle   : 'padding: 10px;background-color: transparent;',
                                    defaults    : {
                                        anchor : Viz.Const.UI.Anchor
                                    }
                                },
                                items              : items
                            }];

                    if (config.isWizard) {

                        var stepImage = new Ext.BoxComponent({
                                    autoEl : {
                                        tag : 'img',
                                        id  : this._imgWizardStepId,
                                        src : '~/step.asmx?C=0&T=' + this.getWizardTotalLength(items)
                                    }
                                });
                        config.tbar = new Ext.Toolbar({
                                    height : 35,
                                    items  : [stepImage]
                                });
                    }

                }
            },

            getWizardTotalLength       : function(items) {
                var length = 0;
                Ext.each(items, function(item) {
                            if (!item.disabled)
                                length += 1;
                        }, this);
                return length;
            },
            /**
             * Handler for the wizard next button
             */
            onWizardNext               : function() {
                this.onWizardChangeActiveItem(1);
            },

            /**
             * Handler for the wizard previous button
             */
            onWizardPrevious           : function() {
                this.onWizardChangeActiveItem(-1);
            },

            /**
             * private function to handle wizard navigation.
             * @param {int} incr : the increment (+1 or -1)
             */
            onWizardChangeActiveItem   : function(incr) {
                var wizard = Ext.getCmp(this._tabContainerId);
                var wizardLayout = wizard.getLayout();
                var valid = true;
                if (incr > 0) {
                    wizardLayout.activeItem.items.each(function(f) {
                                if (f.isValid && !f.isValid()) {
                                    valid = false;
                                }
                            });
                }
                if (valid) {

                    var currentActiveIndex = wizard.items.indexOf(wizardLayout.activeItem);
                    var nextIndex = Math.min(wizard.items.length - 1, currentActiveIndex + incr);
                    while (wizard.items.get(nextIndex).disabled === true && ((nextIndex < wizard.items.length - 1 && incr > 0) || (nextIndex > 0 && incr < 0))) {
                        nextIndex += incr;
                    }
                    wizardLayout.setActiveItem(nextIndex);

                    Ext.getCmp(this._buttonWizardNextId).setDisabled(nextIndex == (wizard.items.length - 1));
                    Ext.getCmp(this._buttonWizardPreviousId).setDisabled(nextIndex == 0);
                    Ext.getCmp(this._buttonSaveId).setVisible(nextIndex == (wizard.items.length - 1));

                    Ext.get(this._imgWizardStepId).dom.src = '~/step.asmx?C=' + nextIndex + '&T=' + this.getWizardTotalLength(wizard.items.items);
                }
            },

            /**
             * Override to expose pset tab based on the value of hasPset configuration option.
             * @param {Object} ct
             * @param {Object} position
             */
            onRender                   : function(ct, position) {
                Viz.form.FormPanel.superclass.onRender.apply(this, arguments);
                // this.loadExtraTabs();

                if (this.closeonsave == false && !this.isWizard && this.applyEnabled) {
                    var button = Ext.getCmp(this._buttonApplyId);
                    if (button) {
                        button.setVisible(true);
                    }
                }
            },

            /**
             * Disables all fields (mark them as readonly as well).
             */
            disableAllFields           : function() {
                // we set to readonly the fields
                this.getForm().items.each(function(f) {
                            if (f.isFormField) {
                                if (f.rendered) {
                                    f.disable();
                                    // this.setReadOnlyField(f, true);
                                }
                                else {
                                    f.on('render', function() {
                                        f.disable();
                                            // this.setReadOnlyField(f, true);
                                        }, this);
                                }
                            }
                        }, this);

                // we set to readonly the historical grids.
                Ext.each(this.findByType('vizGrid'), function(grid) {
                            grid.setReadOnly(true);
                        });

            },

            /**
             * Marks all fields as read only
             * @param {Boolean} bReadOnly
             */
            setReadOnly                : function(bReadOnly) {
                // we set to readonly the buttons (not used anymore since buttons have been move to the toolbar)
                Ext.each(this.buttons, function(b) {
                            if (bReadOnly)
                                b.hide();
                            else
                                b.show();
                        }, this);
                // buttons are now in the bottom toolbar
                var bottomToolbar = this.getBottomToolbar();
                if (bottomToolbar) {
                    if (bReadOnly)
                        bottomToolbar.hide();
                }

                // we set to readonly the fields
                this.getForm().items.each(function(f) {
                            if (f.isFormField) {
                                if (f.rendered) {
                                    this.setReadOnlyField(f, bReadOnly);
                                }
                                else {
                                    f.on('render', function() {
                                                this.setReadOnlyField(f, bReadOnly);
                                            }, this);
                                }
                            }
                        }, this);

                // we set to readonly the historical grids.
                Ext.each(this.findByType('vizGrid'), function(grid) {
                            grid.setReadOnly(bReadOnly);
                        });
            },

            /**
             * Set individual field to ReadIN
             * @param {} f
             */
            setReadOnlyField           : function(f, bReadOnly) {
                f.getEl().dom.readOnly = bReadOnly;
                // for example TriggerFields need to have the "trigger" button disabled
                if (Ext.isFunction(f.setReadOnly))
                    f.setReadOnly(bReadOnly);
            },

            /**
             * Enable the list of fields that referres to the properties.
             * @param {Object} properties The list of property name of the entity that are batch updatable.
             */
            enableUpdatableBatchFields : function(properties) {
                Ext.each(properties, function(property) {
                            var field = this.getForm().findField(property);
                            if (Ext.isObject(field)) {
                                field.setDisabled(false);
                                // this.setReadOnlyField(field, false);
                            }
                        }, this);
            },
            insertHistoryTab           : function() {
                // injecting audit panel into the tab panel
                if (this.auditConfig && this.auditEnabled && Ext.getCmp(this._auditPanelId) == null) {
                    var tabContainer = this.getTabContainer();
                    var auditPanel = {
                        id          : this._auditPanelId,
                        xtype       : 'panel',
                        bbar        : [{
                                    text    : $lang('msg_refresh'),
                                    iconCls : 'x-tbar-loading',
                                    handler : function() {
                                        this.auditConfig.detailedView.items[0].store.reload();
                                    },
                                    scope   : this
                                }],
                        autoScroll  : true,
                        bodyStyle   : 'background-color : white; border-bottom-width: 1px',
                        items       : [this.auditConfig.detailedView],
                        iconCls     : 'viz-icon-small-audit',
                        layout      : 'fit',
                        forceLayout : true,
                        title       : $lang('msg_audit')
                    };
                    tabContainer.add(auditPanel);
                }
            },
            /**
             * Loads extra tabs.
             */
            loadExtraTabs              : function() {
                // First Pset Definitions
                if (this.psetConfig != null) {
                    this.getForm()._async_components = null;
                    this.mask();
                    Viz.Services.CoreWCF.PsetDefinition_GetListByUsageName({
                                UsageName             : this.psetConfig.IfcType,
                                KeyClassificationItem : this.psetConfig.KeyClassificationItem,
                                success               : this.buildPset,
                                scope                 : this
                            });
                }
                else {
                    this.insertHistoryTab();
                }
            },
            /**
             * Destroys the pset tabs.
             */
            destroyPset                : function() {
                var tabContainer = this.getTabContainer();

                // Make sure the tab container still exists and was not destroyed.
                if (!tabContainer)
                    return;

                var tabsToDelete = tabContainer.findBy(function(item) {
                            if (item.getId().startsWith('_pset_') && item.isXType('panel')) {
                                return true;
                            }
                            return false;
                        }, this);
                var form = Ext.getCmp(this._formId).getForm();
                form.items.each(function(field) {
                            var n = field.getName();
                            if (n && n.startsWith('_pset_')) {
                                field.destroy();
                            }
                        }, this);
                Ext.each(tabsToDelete, function(item) {
                            item.destroy();
                        });
                tabContainer.doLayout();
            },
            /**
             * Returns the tabpanel of the form.
             * @return {Ext.TabPanel}
             */
            getTabContainer            : function() {
                return Ext.getCmp(this._tabContainerId);
            },
            /**
             * Builds the pset tabs.
             * @param {Viz.BusinessEntity.PsetDefinition[]} psetDefinitionList
             */
            buildPset                  : function(list) {
                var tabContainer = this.getTabContainer();

                // Make sure the tab container still exists and wasn't closed before the ajax request returned.
                if (!tabContainer)
                    return;
                this.destroyPset();
                this.psetDefinitionList = list;
                var fields_noextratab = [];
                var historical_tabs = [];
                var extra_tabs = [];

                Ext.each(list, function(item) {
                            var tab = {
                                title    : item.Label || item.PsetName,
                                id       : '_pset_' + item.PsetName + '_' + Ext.id(),
                                defaults : {
                                    anchor : '-40'
                                },
                                layout   : 'form',
                                iconCls  : 'viz-icon-small-pset'
                            };
                            var fields = [];
                            Ext.each(item.Attributes, function(attribute) {
                                        var field = this.getFieldFromPsetAttribute(attribute, item.PsetName);
                                        if (attribute.AttributeComponent == "vizGridPsetAttributeHistorical") {
                                            historical_tabs.push({
                                                        title      : (item.Label || item.PsetName) + ' - ' + ($lang(attribute.AttributeMsgCode) || attribute.AttributeName),
                                                        labelAlign : 'top',
                                                        layout     : 'fit',
                                                        iconCls    : 'viz-icon-small-psetattributehistorical',
                                                        items      : field,
                                                        hidden     : this.mode != 'update'
                                                    });
                                        }
                                        else {
                                            if (attribute.AttributeHasTab != true)
                                                fields_noextratab.push(field);
                                            else
                                                fields.push(field);
                                        }

                                    }, this);
                            if (fields.length > 0) {
                                extra_tabs.push(Ext.apply(tab, {
                                            items : fields
                                        }))
                            }

                        }, this);
                if (fields_noextratab.length > 0) {
                    var lastTab = tabContainer.get(this.groupPset ? 0 : tabContainer.items.length - 1);
                    Ext.each(fields_noextratab, function(field) {
                                lastTab.insert(lastTab.items.length, field);
                            }, this);
                }
                if (historical_tabs.length > 0) {
                    tabContainer.add(historical_tabs);
                }
                if (extra_tabs.length) {
                    tabContainer.add(extra_tabs);
                }
                this.insertHistoryTab();

                tabContainer.doLayout();

                if (this.readonly) {
                    this.setReadOnly(this.readonly);
                }
                this.loadPsetValues.defer(200, this);
                this.unMask(true);
            },

            /**
             * After the saveSuccess we show the hidden historical grid that were hidden if mode was create.
             */
            showHiddenHistoricalGrids  : function() {
                var grids = this.findByType('vizGridPsetAttributeHistorical');
                Ext.each(grids, function(grid) {
                            grid.KeyObject = this.entity[this.psetConfig.entityKey];
                            if (grid.store)
                                grid.store.serviceParams.KeyObject = this.entity[this.psetConfig.entityKey];

                            var panel = grid.findParentByType('panel');
                            if (panel) {
                                var tabPanel = panel.findParentByType('tabpanel');
                                if (tabPanel)
                                    tabPanel.unhideTabStripItem(panel.id);
                            }
                        }, this);
            },
            /**
             * Return the field config base on PsetAttributeDefinition entity
             * @param {Viz.BusinessEntity.PsetAttributeDefinition} attribute
             * @return {}
             */
            getFieldFromPsetAttribute  : function(attribute, psetName) {
                var xtype = attribute.AttributeComponent;
                var dataType = attribute.AttributeDataType;
                var supDefinition = {};
                if (xtype == "vizGridPsetAttributeHistorical") {
                    var field = {
                        xtype                  : 'vizGridPsetAttributeHistorical',
                        border                 : true,
                        KeyObject              : this.entity[this.psetConfig.entityKey],
                        KeyPropertySingleValue : attribute.KeyPropertySingleValue,
                        psetDataType           : attribute.AttributeDataType
                    };

                }
                else {
                    var width;
                    if (xtype == "combo")
                        xtype = "vizComboListElement";

                    var store = null;
                    switch (xtype) {
                        case 'datefield' :
                            width = 100;
                            supDefinition.format = Viz.getLocalizedDateFormat();
                            break;
                        case 'numberfield' :
                        case 'viznumberfield' :
                            width = 90;
                            if (dataType == 'int')
                                supDefinition.allowDecimals = false;
                            if (attribute.AttributeMinValue)
                                supDefinition.minValue = attribute.AttributeMinValue;
                            if (attribute.AttributeMaxValue)
                                supDefinition.maxValue = attribute.AttributeMaxValue;
                            break;
                        case 'timefield' :
                            width = 100;
                            supDefinition.format = Viz.getLocalizedTimeFormat(true);
                            break;
                        case 'xdatetime' :
                        case 'vizDateTime' :
                            width = 200;
                            break;
                        case 'vizComboListElement' :
                            store = new Viz.store.PsetList({
                                    listName : attribute.AttributeList,
                                    autoLoad : true,
                                    sortInfo : {
                                        field     : 'Value',
                                        direction : 'ASC'
                                    }
                                })
                            break;   
                        case 'vizComboOccupant' :
                            store = new Viz.store.Occupant();
                            break;
                    }

                    width = attribute.AttributeWidth > 0 ? attribute.AttributeWidth : width;

                    var allowBlank = attribute.AttributeAllowBlank == true;
                    var field = {
                        xtype          : xtype,
                        store          : store,
                        name           : '_pset_' + psetName + '_' + attribute.AttributeName,
                        attributeName  : attribute.AttributeName,
                        keyPropertySet : attribute.KeyPropertySet,
                        fieldLabel     : $lang(attribute.AttributeMsgCode) || attribute.AttributeName,
                        labelStyle     : 'word-wrap:break-word',
                        allowBlank     : allowBlank
                    };

                    if (attribute.AttributeDefaultValue)
                        supDefinition.value = attribute.AttributeDefaultValue;
                    if (width)
                        Ext.apply(field, {
                                    width  : width,
                                    anchor : null
                                });
                    if (attribute.AttributeVType && attribute.AttributeVType.toLowerCase().endsWith('validator'))
                        supDefinition.validator = Ext.form.VTypes[attribute.AttributeVType];

                    if (attribute.AttributeVType && !attribute.AttributeVType.toLowerCase().endsWith('validator'))
                        supDefinition.vtype = attribute.AttributeVType;

                    if (attribute.AttributeMask) {
                        supDefinition.validator = function(value) {
                            return Ext.form.VTypes.maskValidator.call(this, value, attribute.AttributeMask);
                        }
                    }
                    Ext.apply(field, supDefinition);
                }
                // We have to manually add the fields to the validation plugin because the plugin attached itself on the render event of the form
                // and at this stage the pset fields do not exist yet.
                if (this.hasValidationToolbar) {
                    Ext.apply(field, {
                                listeners : {
                                    added : function(f) {
                                        var plugin = Ext.getCmp(this._validationStatusId);
                                        plugin.startMonitoringField(f);
                                    },
                                    scope : this
                                }
                            });
                }
                return field;

            },
            /**
             * Loads the pset values.
             */
            loadPsetValues             : function() {
                if (Ext.isObject(this.psetConfig) && this.entity[this.psetConfig.entityKey]) {
                    Viz.Services.CoreWCF.Pset_GetList({
                                KeyObject : this.entity[this.psetConfig.entityKey],
                                success   : this.setPsetValues,
                                scope     : this
                            });
                }
            },
            /**
             * Set the values of the field from the pset data.
             * @param {Viz.BusinessEntity.Pset} psetList
             */
            setPsetValues: function (psetList) {
                // Make sure the form still exists, it will not be if the form was closed before the response returned
                if (!Ext.getCmp(this._formId))
                    return;

                var values = {};
                Ext.each(psetList, function(pset) {
                            Ext.each(pset.Attributes, function(attribute) {
                                        values["_pset_" + pset.PsetName + "_" + attribute.Key] = attribute.Value;
                                    }, this);
                        }, this);
                Ext.getCmp(this._formId).getForm().setValues(values);
            },
            /**
             * Handler before save that gives a chance to send the 2 stores as extra parameters. This is a virtual function, and each sub class could provide implementation.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave               : function(form, item, extraParams) {
                return true;
            },
            /**
             * Handler for save success. This is a virtual function, and each sub class could provide implementation.
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess              : function(form, action) {
                if (this.closeonsave == false) {

                    Ext.apply(this.entity, action.result.data);
                    this.getForm().setValues(this.entity);
                    this.loadPsetValues();
                    this.showHiddenHistoricalGrids();
                    this.mode = 'update';

                    var win = this.getParentWindow();
                    if (win && this.initialConfigFromGrid && this.initialConfigFromGrid.update) {
                        var config = this.initialConfigFromGrid.update;
                        win.setIconClass(config.iconCls);
                        win.setTitle(config.title + ' : ' + this.entity[config.titleEntity]);
                    }
                }
                return true;
            },
            /**
             * Handler for save failure. This is a virtual function, and each sub class could provide implementation.
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveFailure              : function(form, action) {
                return true;
            },
            /**
             * Handler for beforeloadvalues event. This is a virtual function, and each sub class could provide implementation.
             * @param {Ext.form.FormPanel} form
             * @param {Object} Values
             * @return {Boolean}
             */
            onBeforeLoadValues         : function(form, values) {
                return true;
            },
            /**
             * Handler for afterloadvalues event. This is a virtual function, and each sub class could provide implementation.
             * @param {Ext.form.FormPanel} form
             * @param {Object} Values
             * @return {Boolean}
             */
            onAfterLoadValues          : function(form, values) {
                return true;
            },

            /**
             * Handler before opening a detail window that gives a change to override the open window configuration.
             * @param {Viz.form.FormPanel} form
             * @param {Object} config
             * @param {String} id
             */
            onBeforeOpenDetail         : function(form, config, id) {
                return true;
            },
            /**
             * Loads and display an existing entity.
             */
            loadValues                 : function() {
                var values = this.entity; // here we are sure that entity is an object, it was initialized after the call to the parent ctor.
                Ext.apply(values, this.entityParent);
                if (this.fireEvent("beforeloadvalues", this, values) != false) {
                    if (this.mode == "update" && this.mode_load == "remote") {
                        this.getForm().loadWCF({
                                    waitMsg : true,
                                    success : function(form, action) {
                                        Ext.apply(this.entity, action.result.data);
                                        this.fireEvent("afterloadvalues", this, this.entity);
                                    },
                                    failure : function() {
                                        this.unMask.defer(100, this);
                                    },
                                    scope   : this
                                });
                    }
                    else {
                        this.getForm().setValues(values);
                        this.fireEvent("afterloadvalues", this, values);
                        if (this.mode == "updatebatch") {
                            Viz.Services.CoreWCF.Entity_GetUpdatableBatchProperties({
                                        entityType : this.entityType,
                                        failure    : function() {
                                            this.unMask.defer(100, this);
                                        },
                                        success    : function(properties) {
                                            this.disableAllFields();
                                            this.enableUpdatableBatchFields(properties);
                                            this.isDirty = true;
                                            this.unMask.defer(100, this);
                                        },
                                        scope      : this
                                    });
                        }
                        else
                            this.unMask.defer(100, this);
                    }
                }

                if (this.name && this.mode =="update") {

                    //Finding the localId field and setting it readonly if no permissions
                    var localIdField = this.getForm().findField('LocalId');
                    if (localIdField) {
                        if(!$authorized('@@@@ ' + this.name + ' - LocalId Update')){
                            localIdField.setDisabled(true);
                        }
                    }
                }

            },
            mask                       : function() {
                if (!this.waitMsgTarget) {
                    if (this.ownerCt) {
                        this.getForm().waitMsgTarget = this.ownerCt.getEl();
                        this.getForm().waitMsgTarget.mask();
                    }
                }
            },
            /**
             * @private Unmask the ownerCt of the form.
             */
            unMask                     : function(forced) {
                var asynComp = this.getForm()._async_components;
                asynComp = Ext.isEmpty(asynComp) || !Ext.isArray(asynComp) ? [] : asynComp;
                if ((asynComp.length == 0) || this.mode == "create" || (forced == true)) {
                    if (this.ownerCt)
                        this.ownerCt.getEl().unmask();
                    this.syncSize();
                }
            },

            fileUpload                 : true,

            /**
             * Does the real saving, works both with file uploads and without
             */
            doSaveItem                 : function(config) {
                var form = config.form;
                var fileGuids = config.fileGuids;
                var thisForm = Ext.getCmp(this._formId)
                form._async_components = null;
                if (form.isValid() && (1 == 1 || form.isDirty() || this.isDirty)) {
                    var item = form.getFieldValues();
                    this.entity = thisForm.entity || {};
                    item = Ext.apply(thisForm.entity, item);
                    var extraParams = {};

                    if (fileGuids) {

                        Ext.each(fileGuids, function(fileGuid) {
                                    item[fileGuid.Id] = fileGuid.Value;
                                });
                    }

                    if (form.psetConfig) {
                        var psets = thisForm.getPsetsValues(fileGuids);
                        Ext.apply(extraParams, {
                                    psets : psets
                                });
                        item.PropertySetList = psets;

                    }

                    if (thisForm.fireEvent("beforesave", thisForm, item, extraParams) != false) {
                        extraParams = Ext.apply(extraParams, {
                                    item : item
                                });
                        thisForm.publishMessage(thisForm.messageSaveStart);
                        var serviceHandler;
                        switch (thisForm.mode) {
                            case 'create' :
                                serviceHandler = thisForm.serviceHandlerCreate;
                                break;
                            case 'update' :
                                serviceHandler = thisForm.serviceHandlerUpdate;
                                break;
                            case 'updatebatch' :
                                serviceHandler = thisForm.serviceHandlerUpdateBatch;
                                Ext.apply(extraParams, {
                                            keys : thisForm.updatebatchkeys
                                        });
                                break;
                        }
                        thisForm.form.submitByForm({
                                    waitMsg        : true,
                                    serviceHandler : serviceHandler,
                                    extraParams    : extraParams,
                                    success        : function(form, action) {
                                        this.isDirty = false;
                                        this.publishMessage(this.messageSaveEnd, [this.messageSaveEndParams || action.result.data, action.form ? action.form.mode : '']);
                                        if (this.fireEvent("savesuccess", this, action) != false) {
                                            this.closeParentWindow(false);
                                        }
                                    },
                                    failure        : function(form, action) {
                                        if (this.fireEvent("savefailure", this, action) != false) {
                                            if (action.result && action.result.message) {
                                                Ext.Msg.show({
                                                            width   : this.errorMessageBoxWidth,
                                                            title   : $lang("error_ajax"),
                                                            msg     : action.result.message,
                                                            buttons : Ext.Msg.OK,
                                                            icon    : Ext.MessageBox.ERROR
                                                        });
                                            }

                                            if (action.result && action.result.msg) {
                                                Ext.Msg.show({
                                                            width   : this.errorMessageBoxWidth,
                                                            title   : $lang("error_ajax"),
                                                            msg     : action.result.msg,
                                                            buttons : Ext.Msg.OK,
                                                            icon    : Ext.MessageBox.ERROR
                                                        });
                                            }

                                        }
                                    },
                                    scope          : this
                                });
                    }

                }

            },
            /**
             * Saves the item with dates converted to chart time zone
             */

            saveItemWithDateTimeFixedToTimeZone: function (dateTimeArray, timeZoneId, callbackMethod) {
                if (dateTimeArray.length == 0) {
                    callbackMethod.apply(this, arguments);
                } else {
                    this.timeAdjust(dateTimeArray, timeZoneId, callbackMethod);
                }

            },
            
            timeAdjust    :       function (controllers, timeZoneId, callbackMethod) {
                if (this.getForm().findField(controllers[0]) && this.getForm().isValid()) {
                    var item = this.getForm().getFieldValues();
                    if (item.IsKPI && item.KeyClassificationItemKPI == null) {
                        callbackMethod.apply(this, arguments);
                        return;
                    }

                    var dateOffset = new Date(this.getForm().findField(controllers[0]).getValue().getTime()).getTimezoneOffset() * 60000; //getting the offset in the correct date
                    var adjustedDate = new Date(this.getForm().findField(controllers[0]).getValue().getTime() - dateOffset); //changing the date sent to ignore browser time zone offset
                    Viz.Services.EnergyWCF.ConvertDateToUtcFromTimeZone({
                        scope: this,
                        date: adjustedDate,
                        timeZoneId: timeZoneId,
                        success: function (response) {
                            this.entity.StartDate = response;
                            this.getForm().findField(controllers[0]).setValue(response);
                            this.saveItemWithDateTimeFixedToTimeZone(controllers.slice(1, controllers.length), timeZoneId, callbackMethod);
                        }
                    });
                }
                else {
                    callbackMethod.apply(this, arguments);
                }   
            },

            /**
             * Saves the item.
             */
            saveItem                   : function() {
                /**
                 * @ignore
                 * @type {Ext.form.BasicForm} form
                 */
                // var thisForm = this;
                var form = Ext.getCmp(this._formId).getForm();
                // if (form.isValid() && (form.isDirty() || this.isDirty) && (form.psetConfig)) {
                if (form.isValid()) {
                    if (1 == 1 || form.isDirty() || this.isDirty) {
                        // var documents = thisForm.getDocuments();
                        var documents = this.getDocuments();
                        var hasValue = function(document) {
                            return document.value;
                        };

                        if (!Ext.isEmpty(documents) && (Ext.partition(documents, hasValue)[0][0])) {
                            // if (!Ext.isEmpty(documents)){
                            var messageBox = Ext.MessageBox.wait($lang('msg_fileuploading'), $lang('msg_save'));

                            this.getForm().submitDocument({
                                        url     : Viz.Services.CoreWCF.EntityDocument_FormPrepare.getInfo().url + '?PAGEID=' + Viz.pageId,
                                        success : function(fp, o) {
                                            messageBox.hide();
                                            this.doSaveItem({
                                                        form      : fp,
                                                        fileGuids : o.result.data.FieldsAndGuids
                                                    });
                                        },
                                        failure : function(value, error) {
                                            messageBox.hide();
                                            // If we want to show error
                                            var errorMsg = error.result.msg;
                                            messageBox.alert($lang('msg_document_error'), errorMsg);
                                        },
                                        scope   : this
                                    });
                        }
                        else {
                            this.doSaveItem({
                                        form : this.getForm()
                                    });
                        }
                    }
                    // we close the form if it s valid, not dirty and closeonsave=true. V2
                    else {
                        if (this.closeonsave === true) {
                            this.closeParentWindow();
                        }
                    }
                }
            },

            /**
             * Gets the value of a pset field, automatically replacing a guid instead of a document name if needed
             */
            getPsetFieldValue          : function(config) {
                var psetField = config.field;
                var fileGuidsObj = config.guids;

                if (!psetField || !psetField.getValue) {
                    return null;
                }

                if (Ext.isObject(fileGuidsObj) && fileGuidsObj.containsKey(psetField.name)) {
                    if (psetField.documentMetadata && psetField.documentMetadata.KeyDocument)
                        return psetField.documentMetadata.KeyDocument;
                    else
                        return fileGuidsObj.get(psetField.name);
                }
                else {
                    return psetField.getValue();
                }

            },
            /**
             * Gets the value of psets.
             */
            getPsetsValues             : function(fileGuids) {
                var psets = [];
                var formPanel = Ext.getCmp(this._formId);
                /*
                 * if (fileGuids) { var fileGuidsObj = { }; Ext.each(fileGuids, function(fileGuid) { this[fileGuid.Key] = fileGuid.Value; }, fileGuidsObj); }
                 */

                if (fileGuids) {
                    var fileGuidsObj = new Ext.util.MixedCollection();
                    Ext.each(fileGuids, function(fileGuid) {
                        fileGuidsObj.add(fileGuid.Id, fileGuid.Value);
                            // this[fileGuid.Key] = fileGuid.Value;
                        });
                }
                Ext.each(this.psetDefinitionList, function(psetDefinition) {
                            var psetFields = formPanel.findBy(function(component, container) {
                                        return component.keyPropertySet && (component.keyPropertySet == psetDefinition.KeyPropertySet);
                                    }, this);
                            var pset = {
                                KeyObject  : this.entity[this.psetConfig.entityKey],
                                KeyPset    : psetDefinition.KeyPropertySet,
                                PsetName   : psetDefinition.PsetName,
                                Attributes : []
                            };
                            Ext.each(psetFields, function(psetField) {
                                        pset.Attributes.push({
                                                    Key   : psetField.attributeName,
                                                    Value : formPanel.getPsetFieldValue({
                                                                field : psetField,
                                                                guids : fileGuidsObj
                                                            })
                                                });
                                    });
                            psets.push(pset);
                        }, this);
                return psets;
            },

            /**
             * @private Gets all the documents in the form
             * @return {Array} documents
             */
            getDocuments               : function() {

                var formPanel = Ext.getCmp(this._formId);
                var documents = formPanel.findBy(function(component, container) {
                            return component.name && (component.inputType == 'file' || component.xtype == 'fileuploadfield' || component.xtype == 'vizFileUpload' || component.xtype == 'vizImageUpload');
                        }, this);
                return documents;
            },

            /**
             * Publishes a mesageBus message.
             * @param {String} message The message to publish.
             * @param {Array} messageParams The parameters of the publication.
             */
            publishMessage             : function(message, messageParams) {
                var params;
                if (message) {
                    params = [message];
                    if (messageParams) {
                        params = params.concat(messageParams);
                    }
                    Viz.util.MessageBusMgr.publish.apply(Viz.util.MessageBusMgr, params);
                }
            },

            /**
             * @Private Return the parent Window if exists.
             * @return {Ext.Window}
             */

            getParentWindow            : function() {
                return this.findParentByType('window');
            },

            /**
             * @private Closes the parent window if exists. Is called if closeonsave config property is true.
             * @param {Boolean} force Forces the form to close ifself.
             */
            closeParentWindow          : function(force) {
                var parentWin = this.getParentWindow();
                if (parentWin) {
                    if ((this.getForm().isDirty() || this.isDirty) && force == true) {
                        Ext.MessageBox.confirm($lang('msg_application_title'), $lang('msg_form_close_dirty'), function(button) {
                                    if (button == 'yes') {
                                        parentWin.close();
                                    }
                                }, this);
                    }
                    else {
                        if (this.closeonsave == true || force == true) {
                            parentWin.close();
                        }
                    }
                }
            },
            /**
             * Generic function that converts an array of array of fields names into an array of array of fields.
             * @param {Object} rowFieldNames
             * @return {Object}
             */
            makeRowFields              : function(rowFieldNames) {
                var fields = [];
                var form = this.getForm();
                Ext.each(rowFieldNames, function(names) {
                            var row = [];
                            Ext.each(names, function(name) {
                                        row.push(form.findField(name));
                                    }, this);
                            fields.push(row);
                        }, this);
                return fields;
            },
            /**
             * Generic function that enables the array of fields at index and disables the other elements
             * @param {Object} rowFields A array of array of fields
             * @param {Integer} index The index in the rowFields array that should be enabled
             */
            makeDisable                : function(rowFields, index) {
                var fields = rowFields[index];
                Ext.each(fields, function(f) {
                            f.setDisabled(false);
                        });
                // rowFields[index].pop();
                rowFields.remove(rowFields[index]);
                Ext.each(rowFields, function(row) {
                            Ext.each(row, function(f) {
                                        f.setDisabled(true);
                                        f.reset();
                                    });
                        });
            },
            /**
             * Deletes the selected rows from the grid.
             * @param {String} gridId the id of the grid.
             */
            deleteSelectedRowsFromGrid : function(gridId) {
                this.isDirty = true;
                var grid = Ext.getCmp(gridId);
                grid.deleteSelectedRows();
            },

            /**
             * Opens the window for adding detail records.
             * @param {Object} paramConfig The configuration object.
             */
            openAddDetail              : function(paramConfig) {

                var componentId = Ext.id();

                if (this.fireEvent("beforeopendetail", this, paramConfig, id) != false) {
                    var win = new Ext.Window({
                                title   : paramConfig.windowTitle,
                                width   : paramConfig.windowWidth,
                                height  : paramConfig.windowHeight,
                                modal   : true,
                                iconCls : paramConfig.windowIconCls || paramConfig.buttonAddIconCls,
                                layout  : 'fit',
                                items   : [{
                                            xtype         : paramConfig.xtypeGridDetail,
                                            id            : componentId,
                                            readonly      : paramConfig.gridReadOnly === false ? false : true,
                                            serviceParams : paramConfig.serviceParams,
                                            stateful      : false
                                        }],
                                buttons : [{
                                            text    : paramConfig.buttonAddTitle,
                                            iconCls : paramConfig.buttonAddIconCls,
                                            handler : function() {
                                                var comp = Ext.getCmp(componentId);
                                                var s = comp.getSelectionModel().getSelections();
                                                var storeDetail = Ext.getCmp(paramConfig.gridId).store;
                                                for (var i = 0, r; r = s[i]; i++) {
                                                    if (storeDetail.findExact(paramConfig.keyColumn, r.id) == -1) {
                                                        this.isDirty = true;
                                                        storeDetail.add(new storeDetail.recordType(r.data));
                                                    }
                                                }
                                                win.close();
                                            },
                                            scope   : this
                                        }]
                            });
                    win.show();
                }
            },

            /**
             * The configuration function for a detail grid appearing as a tab.
             * @param {Object} paramConfig the param object with the following propoerties :
             * <ul>
             * <li><b>name</b> {String} The name of the detail window. Usefull when used in conjonction with beforeopendetail event so we know what grid is beeing opened (optional). </li>
             * <li><b>windowTitle</b> {String} The title of the detail window.</li>
             * <li><b>windowIconCls</b> {String} The iconCls of the detail window(optional).</li>
             * <li><b>windowWidth</b> {Number} The width of the detail window (optional).</li>
             * <li><b>windowHeight</b> {Number} The height of the detail window (optional).</li>
             * <li><b>buttonAddTitle</b> {Number} The title of the add button (optional).</li>
             * <li><b>buttonAddIconCls</b> {Number} The iconCls of the add button (optional).</li>
             * <li><b>buttonDeleteTitle</b> {Number} The title of the delete button (optional).</li>
             * <li><b>buttonDeleteIconCls</b> {Number} The iconCls of the delete button (optional).</li>
             * <li><b>xtypeGridDetail</b> {String} The xtype of the grid detail.</li>
             * <li><b>columnsGridDetail</b> {Object} The columns for the grid appearing in detail tab (optional).</li>
             * <li><b>keyColumn</b> {String} The Key column in the store.</li>
             * <li><b>storeDetail</b> {Ext.data.Store} The store for detail records.</li>
             * <li><b>gridPlugins</b> {Object} The detail grid plugins (optional)</li>
             * <li><b>gridHasToolbar</b> {Bool} true to indicate that the grid has a toolbar (for crud), false otherwise (optional default to true).</li>
             * <li><b>gridHasPagingToolbar</b> {Bool} true to indicate that the grid has a paging toolbar false otherwise (optional default to true).</li>
             * <li><b>gridFilterRemote</b> {Bool} true to indicate that the filter plugins should filter on the server, false if the filter should be client side (optional default to true).</li>
             * </ul>
             * @return {Object}
             */
            configGridDetail           : function(paramConfig) {
                var gridId = paramConfig.id || Ext.id();

                Ext.applyIf(paramConfig, {
                            windowTitle          : '',
                            windowWidth          : 1000,
                            windowHeight         : 500,
                            buttonAddTitle       : $lang('msg_add'),
                            buttonAddIconCls     : 'viz-icon-small-add',
                            buttonDeleteTitle    : $lang('msg_delete'),
                            buttonDeleteIconCls  : 'viz-icon-small-delete',
                            gridHasToolbar       : true,
                            gridHasPagingToolbar : true,
                            gridFilterRemote     : true,
                            gridLoadMask         : true,
                            gridId               : gridId
                        });

                var result = {
                    xtype              : paramConfig.xtypeGridDetail,
                    filterRemote       : paramConfig.gridFilterRemote,
                    loadMask           : paramConfig.gridLoadMask,
                    columns            : paramConfig.columnsGridDetail,
                    hasPagingToolbar   : paramConfig.gridHasPagingToolbar,
                    plugins            : paramConfig.gridPlugins,
                    id                 : paramConfig.gridId,
                    ddGroup            : paramConfig.ddGroupGridDetail,
                    enableDropFromTree : paramConfig.enableDropFromTreeGridDetail,
                    border             : true,
                    enableRowDblClick  : false, // this is to avoid that a dblclick on a row would open the update form.
                    store              : paramConfig.storeDetail,
                    stateId            : paramConfig.stateId
                };
                if (paramConfig.gridHasToolbar == true) {
                    Ext.apply(result, {
                                tbar : [/*
                                         * { text : $lang('msg_refresh'), iconCls : 'x-tbar-loading', handler : function() { paramConfig.storeDetail.reload(); }, scope : this },
                                         */{
                                            text    : paramConfig.buttonAddTitle,
                                            iconCls : paramConfig.buttonAddIconCls,
                                            handler : this.openAddDetail.createDelegate(this, [paramConfig]),
                                            scope   : this
                                        }, {
                                            text    : paramConfig.buttonDeleteTitle,
                                            iconCls : paramConfig.buttonDeleteIconCls,
                                            handler : this.deleteSelectedRowsFromGrid.createDelegate(this, [gridId]),
                                            scope   : this
                                        }]
                            });
                }
                return result;
            },

            //isLocalIdReadOnlyOption: function () {
            //    return false;
            //},

            // Overriden to fix an issue with some fields wrongly appearing in the collection this.items (for exemple paging field of a grid)
            isField                    : function(c) {
                return (!!c.name || c.isXType('compositefield')) && !!c.setValue && !!c.getValue && !!c.markInvalid && !!c.clearInvalid;
            }
        });

Ext.reg('vizForm', Viz.form.FormPanel);

﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.MeterDataExportTask
 * @extends Ext.FormPanel.
 */
Viz.form.MeterDataExportTask = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor           : function(config) {
                var Id = config.entity ? config.entity.KeyMeterDataExportTask : null;
                this.storeMeter = new Viz.store.MeterDataExportTaskMeter({
                            KeyMeterDataExportTask : Id
                        });

                this._tabContainerId = Ext.id();

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.MappingWCF.MeterDataExportTask_FormLoad,
                    serviceHandlerCreate : Viz.Services.MappingWCF.MeterDataExportTask_FormCreate,
                    serviceHandlerUpdate : Viz.Services.MappingWCF.MeterDataExportTask_FormUpdate,
                    tabConfig            : {
                        id             : this._tabContainerId,
                        deferredRender : true,
                        activeTab      : 0,
                        items          : [{
                                    title      : $lang('msg_general'),
                                    iconCls    : 'viz-icon-small-form-update',
                                    items      : this.configFormGeneral(config),
                                    labelWidth : 150
                                }, {
                                    title      : $lang('msg_meterdataexporttask_datasource'),
                                    iconCls    : 'viz-icon-small-chart-datasource',
                                    defaults   : {
                                        anchor   : Viz.Const.UI.Anchor,
                                        hideMode : 'offsets'
                                    },
                                    labelWidth : 150,
                                    items      : [this.configDetailGridMeter(config)]
                                }]
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.MeterDataExportTask.superclass.constructor.call(this, config);

            },

            /**
             * General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral     : function(config) {
                return [{
                            name   : 'KeyMeterDataExportTask',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            width      : 300,
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_filename'),
                            name       : 'Filename'
                        }, {
                            xtype      : 'vizcompositefield',
                            fieldLabel : $lang('msg_all_meters'),
                            items      : [{
                                        fieldLabel : $lang('msg_all_meters'),
                                        name       : 'ExportAllMeters',
                                        xtype      : 'checkbox',
                                        width      : 70,
                                        listeners  : {
                                            check : {
                                                fn    : function(sender, isChecked) {
                                                    if (isChecked) {
                                                        Ext.getCmp(this._tabContainerId).hideTabStripItem(1);
                                                    }
                                                    else {
                                                        Ext.getCmp(this._tabContainerId).unhideTabStripItem(1);
                                                    }
                                                },
                                                scope : this
                                            }
                                        }
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_singleoutputfile') + ':'
                                    }, {
                                        fieldLabel : $lang('msg_singleoutputfile'),
                                        name       : 'SingleOutputFile',
                                        xtype      : 'checkbox',
                                        flex       : 1
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_compress') + ':'
                                    }, {
                                        fieldLabel : $lang('msg_compress'),
                                        name       : 'Compress',
                                        xtype      : 'checkbox',
                                        flex       : 1
                                    }]
                        }, {
                            xtype      : 'vizcompositefield',
                            fieldLabel : $lang('msg_timerange'),
                            items      : [{
                                        fieldLabel : $lang('msg_timerange'),
                                        name       : 'TimeRange',
                                        xtype      : 'vizSpinnerField',
                                        minValue   : 1,
                                        allowBlank : false,
                                        width      : 70,
                                        value      : 1
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_timerange_interval') + ':'
                                    }, {
                                        fieldLabel     : $lang('msg_timerange_interval'),
                                        name           : 'TimeRangeInterval',
                                        xtype          : 'vizComboEnum',
                                        enumTypeName   : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common',
                                        sortByValues   : true,
                                        value          : "7",
                                        flex           : 1,
                                        valuesToRemove : ['None', 'Global'],
                                        allowBlank     : false
                                    }]
                        }, {
                            fieldLabel : $lang('msg_mappingtask'),
                            xtype      : 'vizComboMappingTask',
                            name       : 'KeyMappingTask',
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            anchor     : '-20',
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description'
                        }];
            },

            /**
             * The configuration object for the detail grid Meter.
             * @return {Object}
             */
            configDetailGridMeter : function(config) {
                var retVal = this.configGridDetail({
                            name                         : 'Meter',
                            windowTitle                  : $lang('msg_meter'),
                            windowWidth                  : 1000,
                            windowHeight                 : 400,
                            windowIconCls                : 'viz-icon-small-meter',
                            xtypeGridDetail              : 'vizGridMeter',
                            columnsGridDetail            : Viz.Configuration.Columns.MeterDataExportTaskMeter,
                            buttonAddTitle               : $lang('msg_meter_add'),
                            buttonAddIconCls             : 'viz-icon-small-add',
                            buttonDeleteTitle            : $lang('msg_meter_delete'),
                            buttonDeleteIconCls          : 'viz-icon-small-delete',
                            storeDetail                  : this.storeMeter,
                            keyColumn                    : 'KeyMeter',
                            ddGroupGridDetail            : 'SpatialDD',
                            enableDropFromTreeGridDetail : true
                        });
                Ext.apply(retVal, {
                            height   : 200,
                            loadMask : false
                        });

                return {
                    xtype       : 'fieldset',
                    title       : $lang('msg_meters'),
                    iconCls     : 'viz-icon-small-meter',
                    collapsible : false,
                    collapsed   : false,
                    autoHeight  : true,
                    defaults    : {
                        anchor : Viz.Const.UI.GridAnchor
                    },
                    items       : retVal
                };
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave          : function(form, item, extraParams) {
                var storeArray = [this.storeMeter];

                Ext.each(storeArray, function(store) {
                            if (store.hasLoaded)
                                if (item.ExportAllMeters === true) {
                                    store.removeAll();
                                }
                            store.save();
                        }, this);

                Ext.apply(extraParams, {
                            meters : this.storeMeter ? this.storeMeter.crudStore : null
                        });

                return true;
            }

        }

);

Ext.reg('vizFormMeterDataExportTask', Viz.form.MeterDataExportTask);
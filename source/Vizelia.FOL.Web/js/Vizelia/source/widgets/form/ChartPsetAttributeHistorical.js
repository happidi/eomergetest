﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ChartPsetAttributeHistorical
 * @extends Ext.FormPanel.
 */
Viz.form.ChartPsetAttributeHistorical = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Chart} entity The chart entity the axis belong to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                this._psetId = Ext.id();
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.ChartPsetAttributeHistorical_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.ChartPsetAttributeHistorical_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.ChartPsetAttributeHistorical_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.ChartPsetAttributeHistorical.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyChartPsetAttributeHistorical',
                            hidden : true
                        }, {
                            fieldLabel           : $lang('msg_pset'),
                            xtype                : 'viztreecomboPset',
                            id                   : this._psetId,
                            originalDisplayValue : 'PropertySingleValueLongPath',
                            validator            : Ext.form.VTypes.historicalPsetValidator,
                            listeners            : {
                                select : this.updatePropertySingleValue,
                                scope  : this
                            }
                        }, {
                            fieldLabel   : $lang('msg_meter_groupoperation'),
                            name         : 'GroupOperation',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.MathematicOperator,Vizelia.FOL.Common',
                            flex         : 1,
                            value        : Vizelia.FOL.BusinessEntities.MathematicOperator.SUM
                        }, {
                            fieldLabel : $lang('msg_dataseriepsetratio_fillmissingvalues'),
                            name       : 'FillMissingValues',
                            xtype      : 'vizcheckbox'
                        }];
            },
            
            
            onAfterLoadValues         : function(form, values) {
                Ext.getCmp(this._psetId).setValue(values.PropertySingleValueLongPath);
            },
            /**
             * Handler for updating PropertySingleValueLongPath when a the KeyPropertySingleValue is changed.
             */
            updatePropertySingleValue : function(combo, node) {
                if (node && node.attributes && node.attributes.entity && node.attributes.entity.IfcType == "PsetAttributeDefinition") {
                    this.entity.PropertySingleValueLongPath = node.getPath('text');
                    this.entity.PsetName = node.parentNode.attributes.entity.PsetName;
                    this.entity.AttributeName = node.attributes.entity.AttributeName;
                    this.isDirty = true;
                }
            }
        });

Ext.reg('vizFormChartPsetAttributeHistorical', Viz.form.ChartPsetAttributeHistorical);
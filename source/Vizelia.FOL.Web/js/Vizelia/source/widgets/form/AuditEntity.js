﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.AuditEntity
 * @extends Ext.FormPanel.
 */
Viz.form.AuditEntity = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                var Id = config.entity ? config.entity.KeyAuditEntity : null;

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.CoreWCF.AuditEntity_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.AuditEntity_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.AuditEntity_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.AuditEntity.superclass.constructor.call(this, config);

            },
            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            xtype      : 'textfield',
                            value      : Viz.App.Global.User.ApplicationName,
                            fieldLabel : $lang('msg_auditentity_application'),
                            name       : 'Application',
                            hidden     : true
                        }, {
                            xtype      : 'vizComboAuditableEntity',
                            fieldLabel : $lang('msg_auditentity_entityname'),
                            name       : 'EntityName',
                            height     : 25
                        }, {
                            xtype      : 'vizcheckbox',
                            fieldLabel : $lang('msg_auditentity_isauditenabled'),
                            name       : 'IsAuditEnabled',
                            checked    : true
                        }]
            },
            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess     : function(form, action) {
                Viz.form.AuditEntity.superclass.onSaveSuccess.apply(this, arguments);
                Viz.App.loadAuditEntity();
            }
        });
Ext.reg('vizFormAuditEntity', Viz.form.AuditEntity);

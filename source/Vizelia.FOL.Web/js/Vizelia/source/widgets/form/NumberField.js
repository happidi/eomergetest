﻿Ext.namespace('Viz.form');

/**
 * @class Viz.form.NumberField The Vizelia number field.
 * @extends Ext.form.NumberField
 * @xtype viznumberfield
 */
Viz.form.NumberField = Ext.extend(Ext.form.NumberField, {
			/**
			 * @cfg useAnchor {Boolean} Determines whether to use an anchor given in the config or not
			 */
			useAnchor : false,
			
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                var defaultConfig = {
                    width : 115
                };
                var forcedConfig = {};

                Ext.applyIf(config, defaultConfig);

                if (config.useAnchor !== true) {
                    delete config.anchor;
                }

                Ext.apply(config, forcedConfig);

                Viz.form.NumberField.superclass.constructor.call(this, config);
            }
        });

Ext.reg('viznumberfield', Viz.form.NumberField);
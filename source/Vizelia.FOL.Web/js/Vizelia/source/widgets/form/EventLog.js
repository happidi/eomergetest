﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.EventLog
 * @extends Ext.FormPanel.
 */
Viz.form.EventLog = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.EventLog_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.EventLog_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.EventLog_FormUpdate,
                    items                : this.configFormGeneral(config),
                    labelWidth           : 170
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.EventLog.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyEventLog',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_localid'),
                            allowBlank : false,
                            name       : 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_eventlog_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            fieldLabel : $lang('msg_eventlog_description'),
                            name       : 'Description'
                        }, {
                            fieldLabel : $lang('msg_eventlog_startdate'),
                            name       : 'StartDate',
                            xtype      : 'vizDateTime',
                            hasSwitch  : false,
                            hideTime   : false
                        }, {
                            fieldLabel : $lang('msg_eventlog_enddate'),
                            name       : 'EndDate',
                            xtype      : 'vizDateTime',
                            hasSwitch  : false,
                            hideTime   : false
                        }, {
                            fieldLabel           : $lang('msg_eventlog_classificationlongpath'),
                            xtype                : 'viztreecomboClassificationItemEventLog',
                            name                 : 'KeyClassificationItem',
                            originalDisplayValue : 'ClassificationItemLongPath',
                            listeners            : {
                                changevalue : function(field, value) {
                                    return;
                                },
                                scope       : this
                            },
                            allowBlank           : false
                        }, {
                            fieldLabel             : $lang('msg_location_path'),
                            xtype                  : 'viztreecomboSpatial',
                            name                   : 'KeyLocation',
                            validator              : Ext.form.VTypes.locationValidator,
                            ExcludeMetersAndAlarms : true,
                            originalDisplayValue   : 'LocationLongPath',
                            allowBlank             : false
                        }];
            }
        });

Ext.reg('vizFormEventLog', Viz.form.EventLog);
﻿Ext.namespace('Viz.form');

/**
 * @class Viz.form.ClassificationItem
 * @extends Ext.FormPanel.
 */
Viz.form.ClassificationItem = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                this._tabContainerId = Ext.id();
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.CoreWCF.ClassificationItem_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.ClassificationItem_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.ClassificationItem_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }]
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.ClassificationItem.superclass.constructor.call(this, config);

            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral : function(config) {
                return [{
                            fieldLabel : $lang('msg_localid'),
                            name       : 'LocalId',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Title',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_color'),
                            name       : 'Color',
                            xtype      : 'colorpickerfield'
                        }]
            },

            /**
             * Override the loadValues function to update the Color field based on ColorR,ColorG,ColorB
             */
            loadValues        : function() {
                if (Ext.isEmpty(this.entity.ColorR)) {
                    this.entity.ColorR = Math.floor(Math.random() * 255);
                    this.entity.ColorG = Math.floor(Math.random() * 255);
                    this.entity.ColorB = Math.floor(Math.random() * 255);
                }
                this.getForm().findField('Color').setValue(new Viz.RGBColor(String.format("rgb({0},{1},{2})", this.entity.ColorR, this.entity.ColorG, this.entity.ColorB)).toHex());
                Viz.form.ClassificationItem.superclass.loadValues.apply(this, arguments)
            },

            /**
             * Override the onBeforeSave function to update ColorR,ColorG and ColorB fields based on the Color component
             */
            onBeforeSave      : function(form, item, extraParams) {
                var rgbArray = new Viz.RGBColor(this.getForm().findField('Color').getValue()).toRGBArray();
                item.ColorR = rgbArray[0];
                item.ColorG = rgbArray[1];
                item.ColorB = rgbArray[2];
                return true;
            }

        });

Ext.reg('vizFormClassificationItem', Viz.form.ClassificationItem);

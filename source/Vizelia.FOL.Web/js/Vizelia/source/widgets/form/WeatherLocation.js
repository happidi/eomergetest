﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.WeatherLocation
 * @extends Ext.FormPanel.
 */
Viz.form.WeatherLocation = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                          : function(config) {
                this._displayFontFamilyId = Ext.id();
                this._textColorId = Ext.id();
                this._mainBackgroundColorId = Ext.id();
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    labelWidth           : 200,
                    serviceHandler       : Viz.Services.EnergyWCF.WeatherLocation_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.WeatherLocation_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.WeatherLocation_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.WeatherLocation.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                    : function(config) {
                return [{
                            name   : 'KeyWeatherLocation',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_localid'),
                            allowBlank : false,
                            name       : 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_title'),
                            name       : 'Title',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_weatherlocation_search'),
                            name       : 'Name',
                            xtype      : 'vizComboWeatherLocationSearch',
                            allowBlank : false,
                            hiddenName : 'Name',
                            listeners  : {
                                select : function(combo, record, index) {
                                    this.getForm().findField('LocationId').setValue(record.get('LocationId'));
                                },
                                scope  : this
                            }
                        }, {
                            hidden     : true,
                            fieldLabel : $lang('msg_weatherlocation_locationid'),
                            name       : 'LocationId'
                        }, {
                            fieldLabel   : $lang('msg_weatherlocation_displaytype'),
                            name         : 'DisplayType',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.WeatherDisplayType,Vizelia.FOL.Common',
                            value        : Vizelia.FOL.BusinessEntities.WeatherDisplayType.Horizontal,
                            sortByValues : true,
                            listeners    : {
                                select : function(combo, record) {
                                    this.onDynamicDisplayTypeChange([record.data.Value]);

                                },
                                scope  : this
                            }

                        }, {
                            fieldLabel   : $lang('msg_weatherlocation_unit'),
                            name         : 'Unit',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.WeatherUnit,Vizelia.FOL.Common',
                            sortByValues : true
                        }, {
                            id         : this._displayFontFamilyId,
                            fieldLabel : $lang('msg_weatherlocation_displayfontfamily'),
                            name       : 'DisplayFontFamily',
                            xtype      : 'vizComboFontFamily',
                            hiddenName : 'DisplayFontFamily'
                        }, {
                            id         : this._textColorId,
                            fieldLabel : $lang('msg_weatherlocation_textcolor'),
                            name       : 'TextColor',
                            xtype      : 'colorpickerfield'
                        }, {
                            id         : this._mainBackgroundColorId,
                            fieldLabel : $lang('msg_weatherlocation_mainbackgroundcolor'),
                            name       : 'MainBackgroundColor',
                            xtype      : 'colorpickerfield'
                        }, {
                            fieldLabel : $lang('msg_weatherlocation_alternativebackgroundcolor'),
                            name       : 'AlternativeBackgroundColor',
                            xtype      : 'colorpickerfield'
                        }, {
                            fieldLabel : $lang('msg_chart_favorite'),
                            name       : 'IsFavorite',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel : $lang('msg_portalwindow_openonstartup'),
                            name       : 'OpenOnStartup',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }];
            },
            /**
             * Handler for updating fiels based on the form values.
             * @param {Viz.form.FormPanel} the form panel
             * @param {Object} values
             */
            onAfterLoadValues                    : function(formpanel, values) {
            },
            onDynamicDisplayTypeChange           : function(value) {
                var weatherDisplayType = value[0];
                switch (weatherDisplayType) {
                    case Vizelia.FOL.BusinessEntities.WeatherDisplayType.Horizontal :
                        break;
                    case Vizelia.FOL.BusinessEntities.WeatherDisplayType.Veritcal :
                        break;
                    case Vizelia.FOL.BusinessEntities.WeatherDisplayType.MetroOneDay :
                    case Vizelia.FOL.BusinessEntities.WeatherDisplayType.MetroFullforecast :
                        this.onWeatherDisplayTypeMetro();
                        break;

                }
            },
            onWeatherDisplayTypeMetro : function() {
                var displayFontFamily = Ext.getCmp(this._displayFontFamilyId);
                if (displayFontFamily && (!displayFontFamily.value || displayFontFamily.value == ""))
                    displayFontFamily.setValue("Segoe UI");

                var textColorId = Ext.getCmp(this._textColorId);
                if (textColorId && (!textColorId.value || textColorId.value == ""))
                    textColorId.setValue("FFFFFF");

                var mainBackgroundColorId = Ext.getCmp(this._mainBackgroundColorId);
                if (mainBackgroundColorId && (!mainBackgroundColorId.value || mainBackgroundColorId.value == ""))
                    mainBackgroundColorId.setValue("256CA1");
            }
        });

Ext.reg('vizFormWeatherLocation', Viz.form.WeatherLocation);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ChartFormPanel
 * @extends Ext.FormPanel.
 * @this class includes the save item function of chart\chartwizard that manipulates the chart time zone
 */

Viz.form.ChartFormPanel = Ext.extend(Viz.form.FormPanel, {

    constructor                         : function(config) {
        Viz.form.ChartFormPanel.superclass.constructor.call(this, config);
    },

    saveItem: function () {
        if (this.getForm().findField('StartDate') && this.getForm().findField('StartDate').getValue() && this.getForm().findField('EndDate') && this.getForm().findField('EndDate').getValue() && this.getForm().isValid()) {
            var controllers = ['StartDate', 'EndDate'];
            var timeZoneValue = this.getForm().findField('TimeZoneId').value;
            this.saveItemWithDateTimeFixedToTimeZone(controllers, timeZoneValue, Viz.form.ChartFormPanel.superclass.saveItem);
        } else {
            Viz.form.ChartFormPanel.superclass.saveItem.apply(this, arguments);
        }
       
    }

});


Ext.reg('vizFormChartFormPanel', Viz.form.ChartFormPanel);
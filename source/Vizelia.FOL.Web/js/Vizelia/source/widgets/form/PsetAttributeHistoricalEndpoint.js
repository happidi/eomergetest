﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.PsetAttributeHistoricalEndpoint
 * @extends Ext.FormPanel.
 */
Viz.form.PsetAttributeHistoricalEndpoint = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'local',
                    serviceHandler       : Viz.Services.EnergyWCF.PsetAttributeHistoricalEndpoint_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.PsetAttributeHistoricalEndpoint_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.PsetAttributeHistoricalEndpoint_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.PsetAttributeHistoricalEndpoint.superclass.constructor.call(this, config);
            },
            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyPsetAttributeHistoricalEndpoint',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_meter_endpoint'),
                            hiddenName : 'KeyEndpoint',
                            name       : 'KeyEndpoint',
                            xtype      : 'vizComboDataAcquisitionEndpoint',
                            listeners  : {
                                select : function(combo, record, index) {
                                    this.entity.EndpointType = Viz.getEntityType(record.json.__type);
                                },
                                scope  : this
                            }
                        }, {
                            fieldLabel : $lang('msg_meter_endpoint_frequency'),
                            xtype      : 'numberfield',
                            name       : 'EndpointFrequency',
                            flex       : 1,
                            allowBlank : true
                        }];
            },

            /**
             * Handler for save success. We clear the form to input new values
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess     : function(panel, action) {
            }
        });
Ext.reg('vizFormPsetAttributeHistoricalEndpoint', Viz.form.PsetAttributeHistoricalEndpoint);
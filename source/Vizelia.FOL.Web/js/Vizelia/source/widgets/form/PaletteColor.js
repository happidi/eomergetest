﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.PaletteColor
 * @extends Ext.FormPanel.
 */
Viz.form.PaletteColor = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Palette} entity The Palette entity the dataserie belong to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.PaletteColor_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.PaletteColor_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.PaletteColor_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.PaletteColor.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyPaletteColor',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_palettecolor_order'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : false,
                            name       : 'Order',
                            minValue   : 1
                        }, {
                            fieldLabel : $lang('msg_color'),
                            name       : 'Color',
                            xtype      : 'colorpickerfield'
                        }];
            },

            /**
             * Override the loadValues function to update the Color field based on ColorR,ColorG,ColorB
             */
            loadValues        : function() {
                var form = this.getForm();
                if (!Ext.isEmpty(this.entity.ColorR)) {
                    form.findField('Color').setValue(new Viz.RGBColor(String.format("rgb({0},{1},{2})", this.entity.ColorR, this.entity.ColorG, this.entity.ColorB)).toHex());
                }
                Viz.form.PaletteColor.superclass.loadValues.apply(this, arguments)
            },

            /**
             * Override the onBeforeSave function to update ColorR,ColorG and ColorB fields based on the Color component
             */
            onBeforeSave      : function(form, item, extraParams) {
                var value = this.getForm().findField('Color').getValue();
                if (value) {
                    var rgbArray = new Viz.RGBColor(value).toRGBArray();
                    item.ColorR = rgbArray[0];
                    item.ColorG = rgbArray[1];
                    item.ColorB = rgbArray[2];
                }
                else {
                    item.ColorR = null;
                    item.ColorG = null;
                    item.ColorB = null;
                }
                return true;
            },

            /**
             * Handler for save success. We clear the form to input new values
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess     : function(panel, action) {
                this.form.findField('KeyPaletteColor').setValue('');

                var order = this.form.findField('Order');
                order.setValue(order.getValue() + 1);

                this.mode = 'create';
                return true;
            }
        });

Ext.reg('vizFormPaletteColor', Viz.form.PaletteColor);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.WebFrame
 * @extends Ext.FormPanel.
 */
Viz.form.WebFrame = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                this._fieldBody = Ext.id();
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.WebFrame_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.WebFrame_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.WebFrame_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.WebFrame.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyWebFrame',
                            hidden : true
                        }, {
                            fieldLabel: $lang('msg_localid'),
                            allowBlank: false,
                            name: 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_title'),
                            name       : 'Title',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_webframe_url'),
                            name       : 'Url',
                            allowBlank : true
                        }, {
                            fieldLabel : $lang('msg_chart_favorite'),
                            name       : 'IsFavorite',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel : $lang('msg_portalwindow_openonstartup'),
                            name       : 'OpenOnStartup',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }, {
                            xtype      : 'vizComboDynamicDisplayImage',
                            fieldLabel : $lang('msg_dynamicdisplayimage'),
                            listeners  : {
                                select : function(combo, record, index) {
                                    var bodyTextArea = Ext.getCmp(this._fieldBody);
                                    var body = '<img src="' + document.URL.replaceAll('#', '') + '/Public.svc/DynamicDisplayImage_GetStreamImage?applicationName=' + Viz.App.Global.User.ApplicationName + '&width=300&height=300&Key=' + record.data.KeyDynamicDisplayImage + '" width="100%" height="100%" />';
                                    bodyTextArea.setValue(body);
                                },
                                scope  : this
                            }
                        }, {
                            fieldLabel : $lang('msg_webframe_body'),
                            name       : 'Body',
                            id         : this._fieldBody,
                            xtype      : 'textarea',
                            anchor     : Viz.Const.UI.Anchor + ' -140',
                            allowBlank : true
                        }];

            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave      : function(form, item, extraParams) {
                if (item.Body == null)
                    item.Body = '';
                return true;
            }
        });

Ext.reg('vizFormWebFrame', Viz.form.WebFrame);
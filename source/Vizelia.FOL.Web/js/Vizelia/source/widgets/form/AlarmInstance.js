﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.AlarmInstance
 * @extends Ext.FormPanel.
 */
Viz.form.AlarmInstance = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.AlarmInstance_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.AlarmInstance_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.AlarmInstance_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.AlarmInstance.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyAlarmInstance',
                            hidden : true
                        }, {
                            fieldLabel   : $lang('msg_alarminstance_status'),
                            name         : 'Status',
                            allowBlank   : false,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.AlarmInstanceStatus,Vizelia.FOL.Common',
                            sortByValues : true
                        }];
            }
        });
Ext.reg('vizFormAlarmInstance', Viz.form.AlarmInstance);
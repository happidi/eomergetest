﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.RESTDataAcquisitionEndpoint
 * @extends Ext.FormPanel.
 */
Viz.form.RESTDataAcquisitionEndpoint = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                       : function(config) {
                config = config || {};
                this._telventDaily = Ext.id();
                this._telventHourly = Ext.id();
                this._telventDailyForecast = Ext.id();
                this._telventHourlyForecast = Ext.id();
                this._telvantHiddenValidation = Ext.id();

                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.RESTDataAcquisitionEndpoint_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.RESTDataAcquisitionEndpoint_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.RESTDataAcquisitionEndpoint_FormUpdate,
                    tabConfig            : {
                        deferredRender : true,
                        items          : [{
                                    title   : $lang('msg_general'),
                                    iconCls : 'viz-icon-small-form-update',
                                    items   : this.configFormGeneral(config)
                                }]
                    },
                    closeonsave          : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.RESTDataAcquisitionEndpoint.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral                 : function(config) {
                var queryField = [{
                            fieldLabel : $lang('msg_restdataacquisitioncontainer_querystring'),
                            name       : 'QueryString',
                            allowBlank : true
                        }];
                if (config.entity.ProviderType.contains('WeatherDotComRESTDataAcquisitionProvider')) {
                    queryField = [{
                                fieldLabel : $lang('msg_restdataacquisitioncontainer_querystring'),
                                name       : 'QueryString',
                                allowBlank : false,
                                xtype      : 'vizComboWeatherLocationSearch',
                                hiddenName : 'QueryString',
                                valueField : 'LocationId'
                            }];
                }
                if (config.entity.ProviderType.contains('TelventRESTDataAcquisitionProvider')) {
                    queryField = [{
                                fieldLabel     : $lang('msg_telvent_hourly'),
                                xtype          : 'vizComboEnum',
                                id             : this._telventHourly,
                                name           : "RestDataAcquisitionEndPointTelvant_Hourly",
                                resizable      : true,
                                forceSelection : true,
                                enumTypeName   : 'Vizelia.FOL.BusinessEntities.TelventHourlyAttributes,Vizelia.FOL.Common',
                                listeners      : {
                                    select : function(combo, record, index) {
                                        this.valueSelected(record, 'HourlyObservation');
                                    },
                                    scope  : this
                                }
                            }, {
                                fieldLabel     : $lang('msg_telvent_daily'),
                                xtype          : 'vizComboEnum',
                                id             : this._telventDaily,
                                name           : "RestDataAcquisitionEndPointTelvant_Daily",
                                resizable      : true,
                                forceSelection : true,
                                enumTypeName   : 'Vizelia.FOL.BusinessEntities.TelventDailyAttributes,Vizelia.FOL.Common',
                                listeners      : {
                                    select : function(combo, record, index) {
                                        this.valueSelected(record, 'DailyObservation');
                                    },

                                    scope  : this
                                }
                            }, {
                                fieldLabel     : $lang('msg_telvent_hourlyforecast'),
                                xtype          : 'vizComboEnum',
                                id             : this._telventHourlyForecast,
                                name           : "RestDataAcquisitionEndPointTelvant_HourlyForecast",
                                resizable      : true,
                                forceSelection : true,
                                enumTypeName   : 'Vizelia.FOL.BusinessEntities.TelventHourlyForecastAttributes,Vizelia.FOL.Common',
                                listeners      : {
                                    select : function(combo, record, index) {
                                        this.valueSelected(record, 'HourlyForecast');
                                    },
                                    scope  : this
                                }
                            }, {
                                fieldLabel     : $lang('msg_telvent_dailyforecast'),
                                xtype          : 'vizComboEnum',
                                id             : this._telventDailyForecast,
                                name           : "RestDataAcquisitionEndPointTelvant_DailyForecast",
                                resizable      : true,
                                forceSelection : true,
                                enumTypeName   : 'Vizelia.FOL.BusinessEntities.TelventDailyForecastAttributes,Vizelia.FOL.Common',
                                listeners      : {
                                    select : function(combo, record, index) {
                                        this.valueSelected(record, 'DailyForecast');
                                    },
                                    scope  : this
                                }
                            },
                            // TODO: Switch the combo based on the selected Attribute
                            {
                                fieldLabel   : $lang('msg_restdataacquisitionendpoint_unit'),
                                name         : 'Unit',
                                allowBlank   : true,
                                xtype        : 'vizComboEnum',
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.WeatherUnit,Vizelia.FOL.Common',
                                sortByValues : true,
                                valueField   : 'Id'
                            }];
                }
                return [{
                            fieldLabel : $lang('msg_localid'),
                            name       : 'LocalId',
                            readOnly   : config.mode == 'update' ? true : false,
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            hidden     : false,
                            allowBlank : false
                        }].concat(queryField.concat([{
                            name   : 'KeyDataAcquisitionEndpoint',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description',
                            hidden     : true,
                            hidden     : config.entity.ProviderType.contains('TelventRESTDataAcquisitionProvider'),
                            allowBlank : true
                        }, {
                            fieldLabel : $lang('msg_restdataacquisitionendpoint_path'),
                            name       : 'Path',
                            allowBlank : true,
                            hidden     : config.entity.ProviderType.contains('XPathRESTDataAcquisitionProvider') || config.entity.ProviderType.contains('TelventRESTDataAcquisitionProvider') || config.entity.ProviderType.contains('WolframAlphaRESTDataAcquisitionProvider') || config.entity.ProviderType.contains('WeatherDotComRESTDataAcquisitionProvider')
                        }]));

            },

            valueSelected                     : function(record, fieldName) {
                this.form.findField("Path").setValue(fieldName);
                this.entity.QueryString = record.data.Value;

                var telventDaily = this.getForm().findField("RestDataAcquisitionEndPointTelvant_Daily");
                var telventHourly = this.getForm().findField("RestDataAcquisitionEndPointTelvant_Hourly");
                var telventHourlyForecast = this.getForm().findField("RestDataAcquisitionEndPointTelvant_HourlyForecast");
                var telventDailyForecast = this.getForm().findField("RestDataAcquisitionEndPointTelvant_DailyForecast");

                if (fieldName !== "DailyObservation") {
                    telventDaily.clearValue();
                    telventDaily.clearInvalid();
                }

                if (fieldName !== "HourlyObservation") {
                    telventHourly.clearValue();
                    telventHourly.clearInvalid();
                }

                if (fieldName !== "HourlyForecast") {
                    telventHourlyForecast.clearValue();
                    telventHourlyForecast.clearInvalid();
                }

                if (fieldName !== "DailyForecast") {
                    telventDailyForecast.clearValue();
                    telventDailyForecast.clearInvalid();
                }
            },

            onAfterLoadValues                 : function(formpanel, values) {
                if (values.ProviderType.contains('TelventRESTDataAcquisitionProvider')) {
                    switch (values.Path) {
                        case 'DailyObservation' :
                            Ext.getCmp(this._telventDaily).setValue(values.QueryString);
                            break;

                        case 'DailyForecast' :
                            Ext.getCmp(this._telventDailyForecast).setValue(values.QueryString);
                            break;

                        case 'HourlyObservation' :
                            Ext.getCmp(this._telventHourly).setValue(values.QueryString);
                            break;

                        case 'HourlyForecast' :
                            Ext.getCmp(this._telventHourlyForecast).setValue(values.QueryString);
                            break;
                    }
                }

            },
            onBeforeSave                      : function(form, item, extraParams) {
                var retVal = true;
                if (this.entity.ProviderType.contains('TelventRESTDataAcquisitionProvider') && !this.validateObservationForecastFields()) {
                    retVal = false;
                }

                return retVal;
            },

            validateObservationForecastFields : function() {

                var telventDaily = this.getForm().findField("RestDataAcquisitionEndPointTelvant_Daily");
                var telventHourly = this.getForm().findField("RestDataAcquisitionEndPointTelvant_Hourly");
                var telventDailyForecast = this.getForm().findField("RestDataAcquisitionEndPointTelvant_DailyForecast");
                var telventHourlyForecast = this.getForm().findField("RestDataAcquisitionEndPointTelvant_HourlyForecast");

                if ((isNaN(telventDaily.value) || telventDaily.value === "") && (isNaN(telventHourly.value) || telventHourly.value === "") && (isNaN(telventHourlyForecast.value) || telventHourlyForecast.value === "") && (isNaN(telventDailyForecast.value) || telventDailyForecast.value === "")) {
                    var errorMsg = $lang('error_msg_pickobservationorforecast');
                    telventDaily.markInvalid(errorMsg);
                    telventHourly.markInvalid(errorMsg);
                    telventHourlyForecast.markInvalid(errorMsg);
                    telventDailyForecast.markInvalid(errorMsg);
                    return false;
                }

                return true;
            }

        });

Ext.reg('vizFormRESTDataAcquisitionEndpoint', Viz.form.RESTDataAcquisitionEndpoint);
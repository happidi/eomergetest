﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.FlipCard
 * @extends Ext.FormPanel.
 */
Viz.form.FlipCard = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor        : function(config) {
                this._gridFlipCardEntry = Ext.id();
                this._tabFlipCardEntry = Ext.id();

                var Id = config.entity ? config.entity.KeyFlipCard : null;
                this.storeEntry = new Viz.store.FlipCardEntry({
                            KeyFlipCard : Id
                        });
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.FlipCard_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.FlipCard_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.FlipCard_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title      : $lang('msg_general'),
                                    items      : this.configFormGeneral(config),
                                    labelWidth : 200
                                }, {
                                    title      : $lang('msg_flipcardentry'),
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    hidden     : config.mode == 'create',
                                    id         : this._tabFlipCardEntry,
                                    items      : [this.configGridPanelTab(config)]
                                }]
                    },
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.FlipCard.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral  : function(config) {
                return [{
                            name   : 'KeyFlipCard',
                            hidden : true
                        }, {
                            fieldLabel: $lang('msg_localid'),
                            allowBlank: false,
                            name: 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_title'),
                            name       : 'Title',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_flipcard_random'),
                            name       : 'Random',
                            xtype      : 'vizcheckbox'
                        }, {
                            fieldLabel : $lang('msg_flipcard_fliponrefresh'),
                            name       : 'FlipOnRefresh',
                            xtype      : 'vizcheckbox'
                        }, {
                            fieldLabel : $lang('msg_chart_favorite'),
                            name       : 'IsFavorite',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel : $lang('msg_portalwindow_openonstartup'),
                            name       : 'OpenOnStartup',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }]
            },

            /**
             * The configuration for the grid priority
             * @return {object}
             */
            configGridPanelTab : function(config) {
                return {
                    xtype       : 'vizGridFlipCardEntry',
                    loadMask    : false,
                    id          : this._gridFlipCardEntry,
                    store       : this.storeEntry,
                    KeyFlipCard : config.entity.KeyFlipCard,
                    columns     : Viz.Configuration.Columns.FlipCardEntry,
                    border      : true,
                    listeners   : {
                        beforedeleterecord : this.onDeletedRecordTab,
                        scope              : this
                    }
                };
            },

            /**
             * Handler for the columns grid delete.
             */
            onDeletedRecordTab : function() {
                this.deleteSelectedRowsFromGrid(this._gridFlipCardEntry);
                return false;
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave       : function(form, item, extraParams) {
                if (this.storeEntry.hasLoaded)
                    this.storeEntry.save();
                Ext.apply(extraParams, {
                            cards : this.storeEntry.crudStore
                        });
                return true;
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess      : function(form, action) {
                Viz.form.FlipCard.superclass.onSaveSuccess.apply(this, arguments);

                var tabpanel = this.getTabContainer();
                tabpanel.unhideTabStripItem(this._tabFlipCardEntry);

                var grid = Ext.getCmp(this._gridFlipCardEntry);
                grid.KeyFlipCard = grid.store.serviceParams.KeyFlipCard = this.entity.KeyFlipCard;
                grid.store.reload();

            }
        });
Ext.reg('vizFormFlipCard', Viz.form.FlipCard);

﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.DrawingCanvasChart
 * @extends Ext.FormPanel.
 */
Viz.form.DrawingCanvasChart = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.DrawingCanvasChart_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.DrawingCanvasChart_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.DrawingCanvasChart_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.DrawingCanvasChart.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name   : 'KeyDrawingCanvasChart',
                            hidden : true
                        }, {
                            fieldLabel: $lang('msg_localid'),
                            allowBlank: false,
                            name: 'LocalId'
                        }, {
                            name   : 'KeyDrawingCanvas',
                            hidden : true
                        }, /*
                             * { fieldLabel : $lang('msg_drawingcanvaschart_title'), name : 'Title', allowBlank : false },
                             */Viz.combo.Chart.factory({
                                    fieldLabel : $lang('msg_chart'),
                                    name       : 'KeyChart',
                                    allowBlank : false
                                }), {
                            fieldLabel       : $lang('msg_drawingcanvaschart_width'),
                            name             : 'Width',
                            xtype            : 'vizSpinnerField',
                            minValue         : 10,
                            decimalPrecision : 0,
                            allowBlank       : false
                        }, {
                            fieldLabel       : $lang('msg_drawingcanvaschart_height'),
                            name             : 'Height',
                            xtype            : 'vizSpinnerField',
                            minValue         : 10,
                            decimalPrecision : 0,
                            allowBlank       : false
                        }, {
                            fieldLabel       : $lang('msg_drawingcanvaschart_x'),
                            name             : 'X',
                            xtype            : 'vizSpinnerField',
                            // minValue : 0,
                            decimalPrecision : 0,
                            value            : 0,
                            allowBlank       : false
                        }, {
                            fieldLabel       : $lang('msg_drawingcanvaschart_y'),
                            name             : 'Y',
                            xtype            : 'vizSpinnerField',
                            // minValue : 0,
                            decimalPrecision : 0,
                            value            : 0,
                            allowBlank       : false
                        }, {
                            fieldLabel       : $lang('msg_drawingcanvaschart_zindex'),
                            name             : 'ZIndex',
                            xtype            : 'vizSpinnerField',
                            minValue         : 0,
                            decimalPrecision : 0,
                            value            : 0,
                            allowBlank       : false
                        }, {
                            fieldLabel : $lang('msg_drawingcanvaschart_disableallsliders'),
                            name       : 'DisableAllSliders',
                            xtype      : 'vizcheckbox',
                            checked    : false,
                            hidden     : true
                        }];
            }
        });

Ext.reg('vizFormDrawingCanvasChart', Viz.form.DrawingCanvasChart);
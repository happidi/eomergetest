﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.AzManFilter
 * @extends Ext.FormPanel.
 */
Viz.form.AzManFilter = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                              : function(config) {
                config = config || {};

                var filterId = config.entity ? config.entity.Id : null;

                this.storeFilterSpatial = new Viz.store.AzManFilterSpatial({
                            filterId : filterId
                        });

                this.storeFilterReport = new Viz.store.AzManFilterReport({
                            filterId : filterId
                        });

                this.storeFilterChart = new Viz.store.AzManFilterChart({
                            filterId : filterId
                        });

                this.storeFilterAlarmDefinition = new Viz.store.AzManFilterAlarmDefinition({
                            filterId : filterId
                        });

                this.storeFilterDynamicDisplay = new Viz.store.AzManFilterDynamicDisplay({
                            filterId : filterId
                        });

                this.storeFilterPlaylist = new Viz.store.AzManFilterPlaylist({
                            filterId : filterId
                        });

                this.storeFilterLink = new Viz.store.AzManFilterLink({
                        filterId: filterId
                });

                this.storeFilterChartScheduler = new Viz.store.AzManFilterChartScheduler({
                            filterId : filterId
                        });

                this.storeFilterPortalTemplate = new Viz.store.AzManFilterPortalTemplate({
                            filterId : filterId
                        });

                this.storeFilterPortalWindow = new Viz.store.AzManFilterPortalWindow({
                            filterId : filterId
                        });
                this.storeUsersAuthorization = new Viz.store.AzManFilterUserAuthorization({
                            filterId : filterId
                        });

                this.storeAuthorization = new Viz.store.AzManFilterAuthorization({
                            filterId : filterId
                        });
                this.storeFilterClassificationItem = new Viz.store.AzManFilterClassificationItem({
                            filterId : filterId
                        });

                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.AuthenticationWCF.AzManFilter_FormLoad,
                    serviceHandlerCreate : Viz.Services.AuthenticationWCF.AzManFilter_FormCreate,
                    serviceHandlerUpdate : Viz.Services.AuthenticationWCF.AzManFilter_FormUpdate,
                    tabConfig            : {
                        deferredRender : true,
                        items          : [{
                                    title   : $lang('msg_general'),
                                    iconCls : 'viz-icon-small-filter',
                                    items   : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_applicationgroup'),
                                    iconCls    : 'viz-icon-small-applicationgroup',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridDetail(this.configDetailGridApplicationGroup())]
                                    // [this.configGridAuthorization(config)]
                            }   , {
                                    title      : $lang('msg_user_management'),
                                    iconCls    : 'viz-icon-small-user',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridDetail(this.configDetailGridAuthorization())]
                                    // [this.configGridAuthorization(config)]
                            }   , {
                                    title      : $lang('msg_azman_filterspatial'),
                                    iconCls    : 'viz-icon-small-site',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : this.configDetailGridFilterSpatial()
                                    // [this.configGridFilterSpatial(config)]

                            }   , {
                                    title      : $lang('msg_report'),
                                    iconCls    : 'viz-icon-small-report',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : this.configDetailGridFilterReport()
                                    // [this.configGridFilterSpatial(config)]
                            }   , {
                                    title      : $lang('msg_chart'),
                                    iconCls    : 'viz-icon-small-chart',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : this.configDetailGridFilterChart()
                                    // [this.configGridFilterSpatial(config)]
                            }   , {
                                    title      : $lang('msg_alarmdefinition'),
                                    iconCls    : 'viz-icon-small-alarm',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : this.configDetailGridFilterAlarmDefinition()
                                    // [this.configGridFilterSpatial(config)]
                            }   , {
                                    title      : $lang('msg_dynamicdisplay'),
                                    iconCls    : 'viz-icon-small-dynamicdisplay',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : this.configDetailGridFilterDynamicDisplay()
                                    // [this.configGridFilterSpatial(config)]
                            }   , {
                                    title      : $lang('msg_playlist'),
                                    iconCls    : 'viz-icon-small-playlist',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : this.configDetailGridFilterPlaylist()
                                    // [this.configGridFilterSpatial(config)]
                            }   , {
                                    title      : $lang('msg_chartscheduler'),
                                    iconCls    : 'viz-icon-small-chartscheduler',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : this.configDetailGridFilterChartScheduler()
                                    // [this.configGridFilterSpatial(config)]
                            }   , {
                                    title      : $lang('msg_portaltemplate'),
                                    iconCls    : 'viz-icon-small-portaltemplate',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : this.configDetailGridFilterPortalTemplate()
                                    // [this.configGridFilterSpatial(config)]
                            }   , {
                                    title      : $lang('msg_portalwindow'),
                                    iconCls    : 'viz-icon-small-portalwindow',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : this.configDetailGridFilterPortalWindow()
                                    // [this.configGridFilterSpatial(config)]
                            }   , {
                                    title      : $lang('msg_classificationitem'),
                                    iconCls    : 'viz-icon-small-classificationitem',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : this.configDetailGridFilterClassificationItem()
                                    // [this.configGridFilterSpatial(config)]

                                }, {
                                    title: $lang('msg_application_link'),
                                    iconCls: 'viz-icon-small-link',
                                    labelAlign: 'top',
                                    layout: 'fit',
                                    items: this.configDetailGridFilterLink()
                                    // [this.configGridFilterSpatial(config)]

                                }]
                    }
                };
                var forcedConfig = {
                    auditEntityName : 'AuthorizationItem.Filter'
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.AzManFilter.superclass.constructor.call(this, config);

            },

            /**
             * Handler before save that gives a chance to send the 2 stores as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                             : function(form, item, extraParams) {
                Ext.apply(extraParams, {
                            filterSpatial            : this.storeFilterSpatial.getArray(),
                            filterReport             : this.storeFilterReport.getArray(),
                            filterChart              : this.storeFilterChart.getArray(),
                            filterAlarmDefinition    : this.storeFilterAlarmDefinition.getArray(),
                            filterDynamicDisplay     : this.storeFilterDynamicDisplay.getArray(),
                            filterPlaylist           : this.storeFilterPlaylist.getArray(),
                            filterLink               : this.storeFilterLink.getArray(),
                            filterChartScheduler     : this.storeFilterChartScheduler.getArray(),
                            filterPortalTemplate     : this.storeFilterPortalTemplate.getArray(),
                            filterPortalWindow       : this.storeFilterPortalWindow.getArray(),
                            filterClassificationItem : this.storeFilterClassificationItem.getArray()

                        });

                if (this.storeAuthorization.hasLoaded)
                    this.storeAuthorization.save();
                if (this.storeUsersAuthorization.hasLoaded)
                    this.storeUsersAuthorization.save();
                Ext.apply(extraParams, {
                            authorizations : this.storeAuthorization.crudStore,
                            users          : this.storeUsersAuthorization.crudStore
                        });

                return true;
            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral                        : function(config) {
                return [{
                            // fieldLabel : $lang('msg_parent'),
                            name      : 'ParentId',
                            hidden    : true,
                            hideLabel : true,
                            readOnly  : true
                        }, {
                            fieldLabel : $lang('msg_id'),
                            name      : 'Id',
                            hidden  : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            anchor     : Viz.Const.UI.Anchor + ' -90',
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description',
                            allowBlank : true
                        }];
            },

            /**
             * @private The configuration object for detail grid filter spatial.
             * @return {Object}
             */
            configDetailGridFilterSpatial            : function() {
                return {
                    name                        : 'FilterSpatial',
                    xtype                       : 'vizGridFilterSpatial',
                    isSecurableEntity           : true,
                    store                       : this.storeFilterSpatial,
                    border                      : true,
                    disableCheckboxForAncestors : true,
                    gridPlugins                 : [new Viz.plugins.GridExport({
                                listeners : {
                                    beforeExport : function(plugin) {
                                        plugin.title = $lang('msg_azman_filterspatial') + ' : ' + this.entity.Id
                                    },
                                    scope        : this
                                }
                            })]
                };
            },
            /**
             * @private The configuration object for detail grid filter report.
             * @return {Object}
             */

            configDetailGridFilterReport             : function() {
                return {
                    name              : 'FilterReport',
                    xtype             : 'vizGridFilterReport',
                    isSecurableEntity : true,
                    store             : this.storeFilterReport,
                    border            : true
                }
            },

            /**
             * @private The configuration object for detail grid filter chart.
             * @return {Object}
             */
            configDetailGridFilterChart              : function() {
                return {
                    name              : 'FilterChart',
                    xtype             : 'vizGridFilterChart',
                    isSecurableEntity : true,
                    store             : this.storeFilterChart,
                    border            : true
                }
            },

            /**
             * @private The configuration object for detail grid filter chart.
             * @return {Object}
             */
            configDetailGridFilterAlarmDefinition    : function() {
                return {
                    name              : 'FilterAlarmDefinition',
                    xtype             : 'vizGridFilterAlarmDefinition',
                    isSecurableEntity : true,
                    store             : this.storeFilterAlarmDefinition,
                    border            : true
                }
            },

            /**
             * @private The configuration object for detail grid filter DynamicDisplay.
             * @return {Object}
             */
            configDetailGridFilterDynamicDisplay     : function() {
                return {
                    name              : 'FilterDynamicDisplay',
                    xtype             : 'vizGridFilterDynamicDisplay',
                    isSecurableEntity : true,
                    store             : this.storeFilterDynamicDisplay,
                    border            : true
                }
            },

            /**
             * @private The configuration object for detail grid filter Playlist.
             * @return {Object}
             */
            configDetailGridFilterPlaylist           : function() {
                return {
                    name              : 'FilterPlaylist',
                    xtype             : 'vizGridFilterPlaylist',
                    isSecurableEntity : true,
                    store             : this.storeFilterPlaylist,
                    border            : true
                }
            },

            /**
            * @private The configuration object for detail grid filter Link.
            * @return {Object}
            */
            configDetailGridFilterLink: function () {
                return {
                    name: 'FilterLink',
                    xtype: 'vizGridFilterLink',
                    isSecurableEntity: true,
                    store: this.storeFilterLink,
                    border: true
                }
            },

            /**
             * @private The configuration object for detail grid filter ChartScheduler.
             * @return {Object}
             */
            configDetailGridFilterChartScheduler     : function() {
                return {
                    name              : 'FilterChartScheduler',
                    xtype             : 'vizGridFilterChartScheduler',
                    isSecurableEntity : true,
                    store             : this.storeFilterChartScheduler,
                    border            : true
                }
            },

            /**
             * @private The configuration object for detail grid filter PortalTemplate.
             * @return {Object}
             */
            configDetailGridFilterPortalTemplate     : function() {
                return {
                    name              : 'FilterPortalTemplate',
                    xtype             : 'vizGridFilterPortalTemplate',
                    isSecurableEntity : true,
                    store             : this.storeFilterPortalTemplate,
                    border            : true
                }
            },

            /**
             * @private The configuration object for detail grid filter classification item.
             * @return {Object}
             */
            configDetailGridFilterClassificationItem : function() {
                return {
                    name              : 'FilterClassificationItem',
                    xtype             : 'vizGridFilterClassificationItem',
                    isSecurableEntity : true,
                    store             : this.storeFilterClassificationItem,
                    border            : true,
                    gridPlugins       : [new Viz.plugins.GridExport({
                                listeners : {
                                    beforeExport : function(plugin) {
                                        plugin.title = $lang('msg_azman_filterclassificationitem') + ' : ' + this.entity.Id
                                    },
                                    scope        : this
                                }
                            })]
                };
            },

            /**
             * @private The configuration object for detail grid filter spatial.
             * @return {Object}
             */

            configDetailGridFilterPortalWindow       : function() {
                return {
                    name              : 'FilterPortalWindow',
                    xtype             : 'vizGridFilterPortalWindow',
                    isSecurableEntity : true,
                    store             : this.storeFilterPortalWindow,
                    border            : true
                }
            },

            /**
             * @private The configuration object for detail grid AzMan Authorization.
             * @return {Object}
             */
            configDetailGridAuthorization            : function() {
                return {
                    name                : 'User',
                    windowTitle         : $lang('msg_user_management'),
                    windowWidth         : 1000,
                    windowHeight        : 400,
                    windowIconCls       : 'viz-icon-small-user',
                    xtypeGridDetail     : 'vizGridUser',
                    buttonAddTitle      : $lang('msg_user_add'),
                    buttonAddIconCls    : 'viz-icon-small-user-add',
                    buttonDeleteTitle   : $lang('msg_user_delete'),
                    buttonDeleteIconCls : 'viz-icon-small-user-delete',
                    gridLoadMask        : false,
                    storeDetail         : this.storeUsersAuthorization,
                    keyColumn           : 'UserName',
                    columnsGridDetail   : Viz.Configuration.Columns.ApplicationGroupAuthorization,
                    gridPlugins         : [new Viz.plugins.GridExport({
                                listeners : {
                                    beforeExport : function(plugin) {
                                        plugin.title = $lang('msg_authorization') + ' : ' + this.entity.Id
                                    },
                                    scope        : this
                                }
                            })]
                };
            },

            /**
             * @private The configuration object for detail grid AzMan Authorization.
             * @return {Object}
             */
            configDetailGridApplicationGroup         : function() {
                return {
                    name                : 'ApplicationGroup',
                    windowTitle         : $lang('msg_applicationgroup'),
                    windowWidth         : 1000,
                    windowHeight        : 400,
                    windowIconCls       : 'viz-icon-small-user',
                    xtypeGridDetail     : 'vizGridApplicationGroup',
                    buttonAddTitle      : $lang('msg_add'),
                    buttonAddIconCls    : 'viz-icon-small-applicationgroup-add',
                    buttonDeleteTitle   : $lang('msg_delete'),
                    buttonDeleteIconCls : 'viz-icon-small-applicationgroup-delete',
                    storeDetail         : this.storeAuthorization,
                    keyColumn           : 'Name',
                    gridPlugins         : [new Viz.plugins.GridExport({
                                listeners : {
                                    beforeExport : function(plugin) {
                                        plugin.title = $lang('msg_authorization') + ' : ' + this.entity.Id
                                    },
                                    scope        : this
                                }
                            })]
                };
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                            : function(form, action) {
                if (this.closeonsave !== true) {
                    Viz.form.AzManFilter.superclass.onSaveSuccess.apply(this, arguments);

                    var storeArray = this.getExtraStoreArray();

                    Ext.each(storeArray, function(store) {
                                store.serviceParams.filterId = this.entity.Id;
                                if (store.hasLoaded)
                                    store.reload();
                            }, this);
                }
                return true;
            },

            /**
             * Return an array of all the extra store of the Chart.
             * @return {Array}
             */
            getExtraStoreArray                       : function() {
                var stores = [];

                if (this.storeFilterSpatial)
                    stores.push(this.storeFilterSpatial);
                if (this.storeFilterReport)
                    stores.push(this.storeFilterReport);
                if (this.storeFilterChart)
                    stores.push(this.storeFilterChart);
                if (this.storeFilterAlarmDefinition)
                    stores.push(this.storeFilterAlarmDefinition);
                if (this.storeFilterDynamicDisplay)
                    stores.push(this.storeFilterDynamicDisplay);
                if (this.storeFilterPlaylist)
                    stores.push(this.storeFilterPlaylist);
                if (this.storeFilterLink)
                    stores.push(this.storeFilterLink);
                if (this.storeFilterChartScheduler)
                    stores.push(this.storeFilterChartScheduler);
                if (this.storeFilterPortalTemplate)
                    stores.push(this.storeFilterPortalTemplate);
                if (this.storeFilterPortalWindow)
                    stores.push(this.storeFilterPortalWindow);
                if (this.storeFilterPortalWindow)
                    stores.push(this.storeFilterPortalWindow);
                if (this.storeFilterPortalWindow)
                    stores.push(this.storeFilterPortalWindow);
                if (this.storeUsersAuthorization)
                    stores.push(this.storeUsersAuthorization);
                if (this.storeAuthorization)
                    stores.push(this.storeAuthorization);
                if (this.storeFilterClassificationItem)
                    stores.push(this.storeFilterClassificationItem);

                return stores; // , this.storeChartAvailableDataSerieLocalId
            }

        });

Ext.reg('vizFormAzManFilter', Viz.form.AzManFilter);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Schedule
 * @extends Viz.form.CalendarDefinition.
 */
Viz.form.Schedule = Ext.extend(Viz.form.CalendarDefinition, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'local',
                    Category             : 'Schedule',
                    eventHasDuration     : false,
                    serviceHandler       : null,
                    serviceHandlerCreate : null,
                    serviceHandlerUpdate : null,
                    showHourly           : true,
                    showMinutely         : true,
                    showWeeklyRecurs     : false,
                    showEndDate          : true,
                    showNumberOccurrence : false,
                    showDaily            : false,
                    showExplicitMonths   : true
                    
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Schedule.superclass.constructor.call(this, config);
            }

        });

Ext.reg('vizFormSchedule', Viz.form.Schedule);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.AzManTask
 * @extends Ext.FormPanel.
 */
Viz.form.AzManTask = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor               : function(config) {
                config = config || {};
                var Id = config.entity ? config.entity.Id : null;

                this.storeOperation = new Viz.store.AzManTaskOperation({
                            KeyAzManTask : Id
                        });

                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.AuthenticationWCF.AzManTask_FormLoad,
                    serviceHandlerCreate : Viz.Services.AuthenticationWCF.AzManTask_FormCreate,
                    serviceHandlerUpdate : Viz.Services.AuthenticationWCF.AzManTask_FormUpdate,
                    // items : this.configFormGeneral(config),
                    tabConfig            : {
                        deferredRender : true,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_azmanoperation'),
                                    iconCls    : 'viz-icon-small-azmanoperation',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridDetail(this.configDetailGridOperation())]
                                }]

                    }
                };
                var forcedConfig = {
                    auditEntityName : 'AuthorizationItem.Task'
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.AzManTask.superclass.constructor.call(this, config);

            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral         : function(config) {
                return [{
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            readOnly   : config.mode == 'update' ? true : false,
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            anchor     : Viz.Const.UI.Anchor,
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description'
                        }];
            },

            /**
             * @private The configuration object for detail grid AzMan Operation.
             * @return {Object}
             */
            configDetailGridOperation : function() {
                return {
                    name                : 'Operation',
                    windowTitle         : $lang('msg_azmanoperation'),
                    windowWidth         : 1000,
                    windowHeight        : 400,
                    windowIconCls       : 'viz-icon-small-azmanoperation',
                    xtypeGridDetail     : 'vizGridAzManOperation',
                    buttonAddTitle      : $lang('msg_azmanoperation_add'),
                    buttonAddIconCls    : 'viz-icon-small-azmanoperation-add',
                    buttonDeleteTitle   : $lang('msg_azmanoperation_delete'),
                    buttonDeleteIconCls : 'viz-icon-small-azmanoperation-delete',
                    storeDetail         : this.storeOperation,
                    keyColumn           : 'Id'
                };
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave              : function(form, item, extraParams) {
                if (this.storeOperation.hasLoaded)
                    this.storeOperation.save();

                Ext.apply(extraParams, {
                            operations : this.storeOperation.crudStore
                        });
                return true;
            }
        });

Ext.reg('vizFormAzManTask', Viz.form.AzManTask);
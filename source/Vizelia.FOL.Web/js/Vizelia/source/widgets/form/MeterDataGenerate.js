﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.MeterDataGenerate
 * @extends Ext.FormPanel.
 */
Viz.form.MeterDataGenerate = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load               : 'local',
                    serviceHandlerCreate    : Viz.Services.EnergyWCF.MeterData_GenerateBegin,
                    serviceHandlerUpdate    : Viz.Services.EnergyWCF.MeterData_GenerateBegin,
                    items                   : this.configFormGeneral(config),
                    isLongRunningSubmit     : true,
                    longRunningSubmitConfig : {
                        allowCancel            : true,
                        showProgressPercentage : true,
                        modal                  : false
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.MeterDataGenerate.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                var now = new Date();
                return [{
                            name   : 'KeyMeter',
                            hidden : true,
                            value  : config.entity.KeyMeter
                        }, {
                            fieldLabel : $lang('msg_meterdata_startdate'),
                            name       : 'StartDate',
                            hideTime   : false,
                            xtype      : 'vizDateTime',
                            allowBlank : false,
                            hasSwitch  : false,
                            value      : new Date(now.getFullYear(), 0, 1, 0, 0, 0)
                        }, {
                            fieldLabel : $lang('msg_meterdata_enddate'),
                            name       : 'EndDate',
                            hideTime   : false,
                            xtype      : 'vizDateTime',
                            allowBlank : false,
                            hasSwitch  : false,
                            value      : new Date(now.getFullYear(), 11, 31, 23, 59, 59)
                        }, {
                            xtype      : 'vizcompositefield',
                            fieldLabel: $lang('msg_meterdata_frequency'),
                            items      : [{
                                        name       : 'FrequencyNumber',
                                        xtype      : 'vizSpinnerField',
                                        minValue   : 1,
                                        allowBlank : false,
                                        value: config.entity.MeterTimeInterval || 1
                                    }, {
                                        name           : 'Frequency',
                                        style          : 'margin-left:5px',
                                        allowBlank     : false,
                                        xtype          : 'vizComboEnum',
                                        enumTypeName   : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common',
                                        sortByValues   : true,
                                        width          : 90,
                                        value          : Viz.toAxisTimeInterval(config.entity.MeterTimeIntervalUnit),
                                        valuesToRemove : ['None', 'Global']
                                    }]
                        }, {
                            fieldLabel : $lang('msg_meterdata_minimum') + ' >= ',
                            name       : 'Minimum',
                            xtype      : 'vizSpinnerField',
                            allowBlank : false,
                            value      : 0
                        }, {
                            fieldLabel : $lang('msg_meterdata_maximum') + '  < ',
                            name       : 'Maximum',
                            xtype      : 'vizSpinnerField',
                            allowBlank : false,
                            value      : 100
                        }];
            },

            saveItem          : function() {
                Ext.MessageBox.confirm($lang('msg_meterdata_generate'), $lang('msg_meterdata_generate_confirm'), function (button) {
                            if (button == 'yes') {
                                Viz.form.MeterDataGenerate.superclass.saveItem.apply(this, arguments);
                            }
                        }, this);
            }
            
        });

Ext.reg('vizFormMeterDataGenerate', Viz.form.MeterDataGenerate);

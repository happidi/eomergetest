﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.PortalTemplate
 * @extends Ext.FormPanel.
 */
Viz.form.PortalTemplate = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.PortalTemplate} entity The portal template entity.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                    : function(config) {
                this._gridFilterSpatialPset = Ext.id();

                var Id = config.entity ? config.entity.KeyPortalTemplate : null;

                this.storeFilterSpatial = new Viz.store.PortalTemplateFilterSpatial({
                            KeyPortalTemplate : Id
                        });
                this.storeFOLMembershipUser = new Viz.store.PortalTemplateFOLMembershipUser({
                            KeyPortalTemplate : Id
                        });
                this.storePortalWindow = new Viz.store.PortalTemplatePortalWindow({
                            KeyPortalTemplate : Id
                        });

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.PortalTemplate_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.PortalTemplate_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.PortalTemplate_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title   : $lang('msg_general'),
                                    iconCls : 'viz-icon-small-portaltemplate',
                                    items   : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_module_users'),
                                    iconCls    : 'viz-icon-small-user',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridDetail(this.configDetailGridUser())]
                                }, {
                                    title      : $lang('msg_portalwindow'),
                                    iconCls    : 'viz-icon-small-portalwindow',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridDetail(this.configDetailGridPortalWindow())]
                                }, {
                                    title      : $lang('msg_chart_filterspatial'),
                                    iconCls    : 'viz-icon-small-site',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    items      : [this.configGridFilterSpatial(config)]
                                }]
                    }
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.PortalTemplate.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral              : function(config) {
                return [{
                            name   : 'KeyPortalTemplate',
                            hidden : true
                        }, {
                            fieldLabel: $lang('msg_localid'),
                            allowBlank: false,
                            name: 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_title'),
                            name       : 'Title',
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            fieldLabel : $lang('msg_description'),
                            height     : 150,
                            name       : 'Description'
                        }, {
                            fieldLabel : $lang('msg_portaltemplate_autoupdate'),
                            name       : 'AutoUpdate',
                            xtype      : 'vizcheckbox',
                            checked    : true,
                            hidden     : true
                        }, {
                            fieldLabel          : $lang('msg_chart_timezone'),
                            name                : 'TimeZoneId',
                            value               : null,
                            loadDefaultTimeZone : Ext.emptyFn,
                            allowBlank          : true,
                            xtype               : 'vizComboTimeZone'
                        },{
                            fieldLabel : $lang('msg_portaltemplate_numberofcolumns'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : true,
                            name       : 'NumberOfColumns',
                            minValue   : 1
                        },{
                            fieldLabel : $lang('msg_portaltemplate_startx'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : true,
                            name       : 'StartX',
                            minValue   : 0
                        },{
                            fieldLabel : $lang('msg_portaltemplate_starty'),
                            xtype      : 'vizSpinnerField',
                            allowBlank : true,
                            name       : 'StartY',
                            minValue   : 0
                        }];
            },

            /**
             * The configuration object for the detail grid User.
             * @return {Object}
             */
            configDetailGridUser           : function() {
                return {
                    name                : 'User',
                    windowTitle         : $lang('msg_module_users'),
                    windowWidth         : 600,
                    windowHeight        : 400,
                    windowIconCls       : 'viz-icon-small-user',
                    xtypeGridDetail     : 'vizGridUser',
                    columnsGridDetail   : Viz.Configuration.Columns.User,
                    buttonAddTitle      : $lang('msg_add'),
                    buttonAddIconCls    : 'viz-icon-small-add',
                    buttonDeleteTitle   : $lang('msg_delete'),
                    buttonDeleteIconCls : 'viz-icon-small-delete',
                    storeDetail         : this.storeFOLMembershipUser,
                    keyColumn           : 'KeyUser'
                };
            },

            /**
             * The configuration object for the detail grid Portal Window.
             * @return {Object}
             */
            configDetailGridPortalWindow   : function() {
                return {
                    name                : 'PortalWindow',
                    windowTitle         : $lang('msg_portalwindow'),
                    windowWidth         : 600,
                    windowHeight        : 400,
                    windowIconCls       : 'viz-icon-small-portalwindow',
                    xtypeGridDetail     : 'vizGridPortalWindow',
                    buttonAddTitle      : $lang('msg_portalwindow_add'),
                    buttonAddIconCls    : 'viz-icon-small-add',
                    buttonDeleteTitle   : $lang('msg_portalwindow_delete'),
                    buttonDeleteIconCls : 'viz-icon-small-delete',
                    storeDetail         : this.storePortalWindow,
                    keyColumn           : 'KeyPortalWindow'
                };
            },

            /**
             * The configuration object for the filter spatial grid.
             * @return {Object}
             */
            configGridFilterSpatial        : function(config) {
                return {
                    xtype                       : 'vizGridFilterSpatial',
                    store                       : this.storeFilterSpatial,
                    border                      : true,
                    loadMask                    : false,
                    height                      : 200,
                    disableCheckboxForAncestors : true
                };
            },

            /**
             * Handler before save that gives a chance to send the store meter as extra parameters.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                   : function(form, item, extraParams) {
                if (this.storeFOLMembershipUser.hasLoaded)
                    this.storeFOLMembershipUser.save();

                if (this.storePortalWindow.hasLoaded)
                    this.storePortalWindow.save();

                if (this.storeFilterSpatial.hasLoaded)
                    this.storeFilterSpatial.save();

                Ext.apply(extraParams, {
                            users         : this.reconstructComplexEntity(this.storeFOLMembershipUser.crudStore),
                            portalwindows : this.storePortalWindow.crudStore,
                            filterSpatial : this.storeFilterSpatial.crudStore
                        });

                return Viz.form.PortalTemplate.superclass.onBeforeSave.apply(this, arguments);
            },

            reconstructComplexEntity       : function(crudStore) {
                if (Ext.isArray(crudStore.create)) {
                    this.reconstructComplexEntityRecord(crudStore.create);
                }
                if (Ext.isArray(crudStore.destroy)) {
                    this.reconstructComplexEntityRecord(crudStore.destroy);
                }
                return crudStore;
            },

            reconstructComplexEntityRecord : function(records) {
                Ext.each(records, function(record) {
                            for (var i in record) {
                                if (i.contains(".")) {
                                    var subs = i.split('.');

                                    var obj = record;

                                    for (var j = 0; j < subs.length; j++) {
                                        if (j == subs.length - 1) {
                                            obj[subs[j]] = record[i];
                                        }
                                        else {
                                            if (!obj[subs[j]])
                                                obj[subs[j]] = {};

                                            obj = obj[subs[j]];
                                        }
                                    }
                                    delete record[i];
                                }
                            }
                        }, this);
            },

            /**
             * Handler for save success. We reload the store of meters to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                  : function(form, action) {
                 if (this.closeonsave !== true) {
                    Viz.form.PortalTemplate.superclass.onSaveSuccess.apply(this, arguments);
                    var storeArray = this.getExtraStoreArray();
                    Ext.each(storeArray, function(store) {
                                store.serviceParams.KeyPortalTemplate = this.entity.KeyPortalTemplate;
                                if (store.hasLoaded)
                                    store.reload();
                            }, this);
                }
            },
            /**
             * Return an array of all the extra store.
             * @return {Array}
             */
            getExtraStoreArray             : function() {
                var stores = [];
                if (this.storeFOLMembershipUser)
                    stores.push(this.storeFOLMembershipUser);

                if (this.storePortalWindow)
                    stores.push(this.storePortalWindow);

                if (this.storeFilterSpatial)
                    stores.push(this.storeFilterSpatial);
                return stores;
            }
        });

Ext.reg('vizFormPortalTemplate', Viz.form.PortalTemplate);

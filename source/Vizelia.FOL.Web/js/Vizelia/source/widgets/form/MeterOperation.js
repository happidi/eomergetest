﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.MeterOperation
 * @extends Ext.FormPanel.
 */
Viz.form.MeterOperation = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Meter} entity The Meter entity the meteroperation belong to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor             : function(config) {
                config = config || {};

                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.MeterOperation_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.MeterOperation_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.MeterOperation_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.MeterOperation.superclass.constructor.call(this, config);
            },

            /**
             * The config of the form
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral       : function(config) {
                return [{
                            name   : 'KeyMeterOperation',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_localid'),
                            name       : 'LocalId',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_meteroperation_order'),
                            name       : 'Order',
                            xtype      : 'vizSpinnerField',
                            minValue   : 1,
                            allowBlank : false,
                            value      : config.entity.Order || 1,
                            listeners  : {
                                change : this.onChangeOrder,
                                spin   : this.onChangeOrder,
                                scope  : this
                            }
                        }, {
                            xtype          : 'vizcompositefield',
                            fieldLabel     : $lang('msg_meteroperation_factor1'),
                            defaultMargins : '0 5 0 0',
                            items          : [{
                                        fieldLabel       : $lang('msg_meteroperation_factor1'),
                                        name             : 'Factor1',
                                        xtype            : 'numberfield',
                                        decimalPrecision : 10,
                                        allowBlank       : false,
                                        value            : 1,
                                        flex             : 1
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_meteroperation_function1') + ':'
                                    }, {
                                        fieldLabel     : $lang('msg_meteroperation_function1'),
                                        name           : 'Function1',
                                        hiddenName     : 'Function1',
                                        allowBlank     : true,
                                        xtype          : 'vizComboEnum',
                                        enumTypeName   : 'Vizelia.FOL.BusinessEntities.AlgebricFunction,Vizelia.FOL.Common',
                                        flex           : 1,
                                        value          : "0",
                                        valuesToRemove : ['Percentage', 'Delta', 'YTD']
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_meteroperation_offset1') + ':'
                                    }, {
                                        name             : 'Offset1',
                                        xtype            : 'numberfield',
                                        decimalPrecision : 10,
                                        allowBlank       : true,
                                        flex             : 1
                                    }]
                        }, {
                            xtype          : 'vizcompositefield',
                            fieldLabel     : $lang('msg_meteroperation_meter1'),
                            defaultMargins : '0 5 0 0',
                            items          : [{
                                        fieldLabel : $lang('msg_meteroperation_meter1'),
                                        name       : 'KeyMeter1',
                                        hiddenName : 'KeyMeter1',
                                        xtype      : 'vizComboMeter',
                                        allowBlank : true,
                                        flex       : 1,
                                        listeners  : {
                                            select : this.onSelectMeter1,
                                            scope  : this
                                        }
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_meteroperation_1') + ':'
                                    }, {
                                        fieldLabel        : $lang('msg_meteroperation_meter1'),
                                        xtype             : 'vizComboMeterOperation',
                                        name              : 'KeyMeterOperation1',
                                        hiddenName        : 'KeyMeterOperation1',
                                        allowBlank        : true,
                                        KeyMeter          : config.entity.KeyMeter,
                                        KeyMeterOperation : config.entity.KeyMeterOperation,
                                        Order             : config.entity.Order || 1,
                                        flex              : 1,
                                        listeners         : {
                                            select : this.onSelectMeterOperation1,
                                            scope  : this
                                        }
                                    }]
                        }, {
                            fieldLabel   : $lang('msg_meteroperation_operator'),
                            name         : 'Operator',
                            hiddenName   : 'Operator',
                            allowBlank   : true,
                            xtype        : 'vizComboEnum',
                            enumTypeName : 'Vizelia.FOL.BusinessEntities.ArithmeticOperator,Vizelia.FOL.Common',
                            displayIcon  : true,
                            sortByValues : true,
                            value        : "0",
                            listeners    : {
                                select : this.onSelectOperator,
                                scope  : this
                            }
                        }, {
                            xtype          : 'vizcompositefield',
                            fieldLabel     : $lang('msg_meteroperation_factor2'),
                            defaultMargins : '0 5 0 0',
                            items          : [{
                                        fieldLabel       : $lang('msg_meteroperation_factor2'),
                                        name             : 'Factor2',
                                        xtype            : 'numberfield',
                                        allowBlank       : false,
                                        decimalPrecision : 10,
                                        value            : 1,
                                        flex             : 1
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_meteroperation_function2') + ':'
                                    }, {
                                        fieldLabel     : $lang('msg_meteroperation_function2'),
                                        name           : 'Function2',
                                        hiddenName     : 'Function2',
                                        allowBlank     : true,
                                        xtype          : 'vizComboEnum',
                                        enumTypeName   : 'Vizelia.FOL.BusinessEntities.AlgebricFunction,Vizelia.FOL.Common',
                                        flex           : 1,
                                        value          : "0",
                                        valuesToRemove : ['Percentage', 'Delta', 'YTD']
                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_meteroperation_offset2') + ':'
                                    }, {
                                        name             : 'Offset2',
                                        xtype            : 'numberfield',
                                        decimalPrecision : 10,
                                        allowBlank       : true,
                                        flex             : 1
                                    }]
                        }, {
                            xtype          : 'vizcompositefield',
                            fieldLabel     : $lang('msg_meteroperation_meter2'),
                            defaultMargins : '0 5 0 0',
                            items          : [{
                                        fieldLabel : $lang('msg_meteroperation_meter2'),
                                        xtype      : 'vizComboMeter',
                                        hiddenName : 'KeyMeter2',
                                        name       : 'KeyMeter2',
                                        allowBlank : true,
                                        flex       : 1,
                                        listeners  : {
                                            select : this.onSelectMeter2,
                                            scope  : this
                                        }

                                    }, {
                                        xtype : 'displayfield',
                                        cls   : 'viz-form-display-field-inside-composite',
                                        value : $lang('msg_meteroperation_2') + ':'
                                    }, {
                                        xtype             : 'vizComboMeterOperation',
                                        name              : 'KeyMeterOperation2',
                                        hiddenName        : 'KeyMeterOperation2',
                                        allowBlank        : true,
                                        KeyMeter          : config.entity.KeyMeter,
                                        KeyMeterOperation : config.entity.KeyMeterOperation,
                                        Order             : config.entity.Order || 1,
                                        flex              : 1,
                                        listeners         : {
                                            select : this.onSelectMeterOperation2,
                                            scope  : this
                                        }
                                    }]
                        }];
            },
            /**
             * Handler for reinjecting recurrence pattern into the fields of the form.
             * @param {Viz.form.FormPanel} form
             * @param {Object} values
             */
            onAfterLoadValues       : function(form, values) {
                this.checkOperator(values.Operator);
            },

            /**
             * Handler for the Operator combo select event.
             */
            onSelectOperator        : function(combo, record, index) {
                this.checkOperator(record.data.Value);
            },

            /**
             * Handler for the Order field change event.
             */
            onChangeOrder           : function() {
                this.checkOrder();
            },

            /**
             * Handler for the Meter1 combo select event.
             */
            onSelectMeter1          : function(combo, record, index) {
                var form = this.getForm();
                form.findField("KeyMeterOperation1").clearValue();
            },

            /**
             * Handler for the Meter1 combo select event.
             */
            onSelectMeterOperation1 : function(combo, record, index) {
                var form = this.getForm();
                form.findField("KeyMeter1").clearValue();
            },

            /**
             * Handler for the Meter2 combo select event.
             */
            onSelectMeter2          : function(combo, record, index) {
                var form = this.getForm();
                form.findField("KeyMeterOperation2").clearValue();
            },

            /**
             * Handler for the Meter1 combo select event.
             */
            onSelectMeterOperation2 : function(combo, record, index) {
                var form = this.getForm();
                form.findField("KeyMeter2").clearValue();
            },
            /**
             * Update the status of the ****2 fields based on the value of the Operator
             * @param {String} operatorValue The Operator value
             */
            checkOperator           : function(operatorValue) {
                var clear = false;
                if (Ext.isEmpty(operatorValue) || operatorValue == "0")
                    clear = true;
                var fieldNames = ['KeyMeter2', 'KeyMeterOperation2', 'Function2', 'Factor2'];
                var form = this.getForm();

                Ext.each(fieldNames, function(fieldName) {
                            var field = form.findField(fieldName);
                            if (clear) {
                                // if (field.clearValue)
                                // form.findField(fieldName).clearValue();
                                field.clearInvalid();
                                field.setDisabled(true);
                            }
                            else {
                                field.setDisabled(false);
                            }
                        }, this);
            },
            /**
             * Update the store of the KeyMeterOperation1 and KeyMeterOperation1 based on the current Order.
             */
            checkOrder              : function() {
                var form = this.getForm();
                var orderValue = form.findField('Order').getValue();

                var fieldNames = ['KeyMeterOperation1', 'KeyMeterOperation2'];
                Ext.each(fieldNames, function(fieldName) {
                            var field = form.findField(fieldName);
                            field.store.serviceParams.order = orderValue;
                            field.store.reload();
                        }, this);

            },

            /**
             * Check the status of the form.
             * @param {} form
             * @param {} item
             */
            onBeforeSave            : function(form, item) {
                var form = this.getForm();
                if (item.Operator != 0 && Ext.isEmpty(item.KeyMeter2) && Ext.isEmpty(item.KeyMeterOperation2)) {
                    var msg = Ext.form.TextField.prototype.blankText;
                    form.findField("KeyMeter2").markInvalid(msg);
                    return false;
                }
                if (item.KeyMeterOperation2 == "0")
                    item.KeyMeterOperation2 = null;

                if (item.KeyMeterOperation1 == "0")
                    item.KeyMeterOperation1 = null;

                return true;
            }

        });

Ext.reg('vizFormMeterOperation', Viz.form.MeterOperation);
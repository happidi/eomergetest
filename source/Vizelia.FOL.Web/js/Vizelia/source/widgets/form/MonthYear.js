﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.MonthYear
 * @extends Ext.form.CompositeField A custom month year field component..
 */
Viz.form.MonthYear = Ext.extend(Ext.form.CompositeField, {

            hideMonth     : false,

            constructor   : function(config) {
                this.monthId = Ext.id(), this.yearId = Ext.id(), config = config || {};
                var defaultConfig = {};
                var forcedConfig = {
                    style : 'background-color:transparent'
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                config.width = config.hideMonth ? 120 : 210;
                var monthConfig = {
                    xtype      : 'vizComboCalendarMonthName',
                    width      : 115,
                    fieldLabel : $lang('msg_calendarevent_month'),
                    allowBlank : config.hidden || config.allowBlank || config.hideMonth || false,
                    hidden     : config.hideMonth,
                    listeners  : {
                        select : this.onSelectMonth,
                        keyup  : this.onKeyUpMonth,
                        scope  : this
                    }
                };

                Ext.apply(monthConfig, {
                            id   : Ext.isEmpty(config['id']) ? this.monthId + 'month' : config['id'] + 'month',
                            name : config['name'] + 'month'
                        });

                var yearConfig = {
                    xtype      : 'vizSpinnerField',
                    width      : 90,
                    fieldLabel : $lang('msg_calendarevent_year'),
                    allowBlank : config.hidden || config.allowBlank || false,
                    minValue   : 1990,
                    maxValue   : 3000,
                    listeners  : {
                        // select: this.onSelectYear,
                        change : this.onChangeYear,
                        keyup  : this.onKeyUpYear,
                        scope  : this
                    }
                };

                Ext.apply(yearConfig, {
                            id   : Ext.isEmpty(config['id']) ? this.yearId + 'year' : config['id'] + 'year',
                            name : config['name'] + 'year'
                        });

                Ext.apply(monthConfig, config.monthConfig);
                Ext.apply(yearConfig, config.yearConfig);
                this.items = [monthConfig, yearConfig];
                Viz.form.MonthYear.superclass.constructor.call(this, config);
                this.addEvents('selectmonth', 'selectyear', 'keyup');
            },

            onRender      : function(ct, position) {
                Viz.form.MonthYear.superclass.onRender.apply(this, arguments);
                this.mf = this.items.items[0];
                this.yf = this.items.items[1];
            },

            onSelectMonth : function(combo, record, index) {
                this.fireEvent('selectmonth', this, record, index, this.getValue());
            },

            // onSelectYear: function (comp, record, index) {
            // this.fireEvent('selectyear', this, comp, record, index, this.getValue());
            // },

            onChangeYear  : function(comp, newValue, oldValue) {
                this.fireEvent('selectyear', this, newValue, oldValue, this.getValue());
            },

            onKeyUpMonth  : function() {
                this.isDirty = function() {
                    return true;
                };
                this.fireEvent('keyup', this);
            },

            onKeyUpYear   : function() {
                this.isDirty = function() {
                    return true;
                };
                this.fireEvent('keyup', this);
            },

            setValue      : function(v) {
                if (v && Ext.isDate(v)) {
                    if (this.mf) {
                        if (!this.hideMonth) {
                            this.mf.setValue(1 + v.getMonth());
                        }
                        else {
                            this.mf.setValue(null);
                        }
                    }
                    if (this.yf) {
                        this.yf.setValue(v.getFullYear());
                    }

                    // we are here if the field isnt rendered yet, so we put the value into the value attribute.
                    if (!this.mf && !this.yf) {
                        this.value = v;
                    }
                }
                else {
                    if (this.mf)
                        this.mf.setValue(null);
                    if (this.yf)
                        this.yf.setValue(null);
                }
            },

            getValue      : function() {
                if ((this.yf == null && this.mf == null) || this.yf && !this.yf.getValue() && this.mf && !this.mf.getValue())
                    return null;
                var year = this.yf.getValue();
                var month = this.mf.getValue() || 1;

                return new Date(year, month - 1, Viz.Const.Date.MeterDataDaysOffset);
            },

            disableMonth  : function(checked) {
                this.mf.setDisabled(checked);
                if (checked) {
                    this.mf.clearValue();
                    this.mf.clearInvalid();
                }
                this.mf.setVisible(!checked);
                this.doLayout();
            },

            getRawValue   : Ext.emptyFn,

            setRawValue   : Ext.emptyFn,

            getErrors     : function(value) {
                var errors = Viz.form.MonthYear.superclass.getErrors.apply(this, arguments);
                if (Ext.isFunction(this.validator)) {
                    var msg = this.validator(value);
                    if (msg !== true) {
                        errors.push(msg);
                    }
                }
                return errors;
            },

            validateValue : function(value) {
                // currently, we only show 1 error at a time for a field, so just use the first one
                var error = this.getErrors(value)[0];
                if (error == undefined) {
                    return Viz.form.MonthYear.superclass.validateValue.apply(this, arguments);
                }
                else {
                    this.markInvalid(error);
                    return false;
                }
            },

            isDirty       : function() {
                if (this.disabled || !this.rendered) {
                    return false;
                }
                return String(this.getValue()) !== String(this.originalValue);
            }

        });
Ext.reg('vizMonthYear', Viz.form.MonthYear);

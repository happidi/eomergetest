﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.DrawingCanvas
 * @extends Ext.FormPanel.
 */
Viz.form.DrawingCanvas = Ext.extend(Viz.form.FormPanel, {

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor                  : function(config) {
                this._gridDrawingCanvasChart = Ext.id();
                this._tabDrawingCanvasChart = Ext.id();
                this._gridDrawingCanvasImage = Ext.id();
                this._tabDrawingCanvasImage = Ext.id();

                var Id = config.entity ? config.entity.KeyDrawingCanvas : null;
                this.storeDrawingCanvasChart = new Viz.store.DrawingCanvasChart({
                            KeyDrawingCanvas : Id
                        });
                this.storeDrawingCanvasImage = new Viz.store.DrawingCanvasImage({
                            KeyDrawingCanvas : Id
                        });

                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.DrawingCanvas_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.DrawingCanvas_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.DrawingCanvas_FormUpdate,
                    tabConfig            : {
                        deferredRender : false,
                        items          : [{
                                    title : $lang('msg_general'),
                                    items : this.configFormGeneral(config)
                                }, {
                                    title      : $lang('msg_chart'),
                                    iconCls    : 'viz-icon-small-chart',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    hidden     : config.mode == 'create',
                                    id         : this._tabDrawingCanvasChart,
                                    items      : [this.configGridDrawingCanvasChart(config)]
                                }, {
                                    title      : $lang('msg_drawingcanvasimage'),
                                    iconCls    : 'viz-icon-small-image',
                                    labelAlign : 'top',
                                    layout     : 'fit',
                                    hidden     : config.mode == 'create',
                                    id         : this._tabDrawingCanvasImage,
                                    items      : [this.configGridDrawingCanvasImage(config)]
                                }]
                    },
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.DrawingCanvas.superclass.constructor.call(this, config);
            },

            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral            : function(config) {
                return [{
                            name   : 'KeyDrawingCanvas',
                            hidden : true
                        }, {
                            fieldLabel: $lang('msg_localid'),
                            allowBlank: false,
                            name: 'LocalId'
                        }, {
                            fieldLabel : $lang('msg_title'),
                            name       : 'Title',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_chart_favorite'),
                            name       : 'IsFavorite',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            fieldLabel : $lang('msg_portalwindow_openonstartup'),
                            name       : 'OpenOnStartup',
                            xtype      : 'vizcheckbox',
                            checked    : false
                        }, {
                            fieldLabel : $lang('msg_drawingcanvas_displaysliders'),
                            name       : 'DisplaySliders',
                            xtype      : 'vizcheckbox',
                            hidden:true,
                            checked    : false
                        }];

            },

            /**
             * The configuration for the grid chart
             * @return {object}
             */
            configGridDrawingCanvasChart : function(config) {
                return {
                    xtype            : 'vizGridDrawingCanvasChart',
                    border           : true,
                    loadMask         : false,
                    id               : this._gridDrawingCanvasChart,
                    store            : this.storeDrawingCanvasChart,
                    KeyDrawingCanvas : config.entity.KeyDrawingCanvas,
                    columns          : Viz.Configuration.Columns.DrawingCanvasChart,
                    listeners        : {
                        beforedeleterecord : this.onDeletedRecordTab.createDelegate(this, [this._gridDrawingCanvasChart]),
                        scope              : this
                    }
                };
            },

            /**
             * The configuration for the grid image
             * @return {object}
             */
            configGridDrawingCanvasImage : function(config) {
                return {
                    xtype            : 'vizGridDrawingCanvasImage',
                    border           : true,
                    loadMask         : false,
                    id               : this._gridDrawingCanvasImage,
                    store            : this.storeDrawingCanvasImage,
                    KeyDrawingCanvas : config.entity.KeyDrawingCanvas,
                    columns          : Viz.Configuration.Columns.DrawingCanvasImage,
                    listeners        : {
                        beforedeleterecord : this.onDeletedRecordTab.createDelegate(this, [this._gridDrawingCanvasImage]),
                        scope              : this
                    }
                };
            },

            /**
             * Handler before save that gives a chance to send the store of deleted drawing canvas chart.
             * @param {Ext.form.FormPanel} form
             * @param {Object} item
             * @param {Object} extraParams
             * @return {Boolean}
             */
            onBeforeSave                 : function(form, item, extraParams) {
                if (this.storeDrawingCanvasChart.hasLoaded)
                    this.storeDrawingCanvasChart.save();
                if (this.storeDrawingCanvasImage.hasLoaded)
                    this.storeDrawingCanvasImage.save();
                Ext.apply(extraParams, {
                            charts : this.storeDrawingCanvasChart.crudStore,
                            images : this.storeDrawingCanvasImage.crudStore
                        });
                return true;
            },

            /**
             * Handler for the columns grid delete.
             */
            onDeletedRecordTab           : function(gridId) {
                this.deleteSelectedRowsFromGrid(gridId);
                return false;
            },

            /**
             * Handler for save success. We reload the store of DrawingCanvasChart to unmask the grid
             * @param {Ext.form.FormPanel} form
             * @param {Object} action
             * @return {Boolean}
             */
            onSaveSuccess                : function(form, action) {
                Viz.form.DrawingCanvas.superclass.onSaveSuccess.apply(this, arguments);

                var tabpanel = this.getTabContainer();
                tabpanel.unhideTabStripItem(this._tabDrawingCanvasChart);
                tabpanel.unhideTabStripItem(this._tabDrawingCanvasImage);

                var grid = Ext.getCmp(this._gridDrawingCanvasChart);
                grid.KeyDrawingCanvas = grid.store.serviceParams.KeyDrawingCanvas = this.entity.KeyDrawingCanvas;
                grid.store.reload();

                grid = Ext.getCmp(this._gridDrawingCanvasImage);
                grid.KeyDrawingCanvas = grid.store.serviceParams.KeyDrawingCanvas = this.entity.KeyDrawingCanvas;
                grid.store.reload();
            }
        });

Ext.reg('vizFormDrawingCanvas', Viz.form.DrawingCanvas);
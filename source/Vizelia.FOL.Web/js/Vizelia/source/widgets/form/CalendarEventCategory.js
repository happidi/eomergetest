﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.CalendarEventCategory
 * @extends Ext.FormPanel.
 */
Viz.form.CalendarEventCategory = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.CoreWCF.CalendarEventCategory_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.CalendarEventCategory_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.CalendarEventCategory_FormUpdate,
                    items                : this.configFormGeneral(config)
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.CalendarEventCategory.superclass.constructor.call(this, config);
            },
            /**
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                            name       : 'LocalId',
                            fieldLabel : $lang('msg_localid'),
                            hidden     : false
                        }, Viz.combo.Localization.factory({}),
                    {
                            fieldLabel : $lang('msg_color'),
                            name       : 'ColorId',
                            xtype      : 'extensible.calendarcolorpalettefield'
                        }];
            },

            /**
             * Override the loadValues function to update the Color field based on ColorR,ColorG,ColorB
             */
            loadValues        : function() {
                var form = this.getForm();
                /*
                 * if (!Ext.isEmpty(this.entity.ColorR)) { form.findField('Color').setValue(new Viz.RGBColor(String.format("rgb({0},{1},{2})", this.entity.ColorR, this.entity.ColorG, this.entity.ColorB)).toHex()); }
                 */
                Viz.form.CalendarEventCategory.superclass.loadValues.apply(this, arguments)
            },

            /**
             * Override the onBeforeSave function to update ColorR,ColorG and ColorB fields based on the Color component
             */
            onBeforeSave      : function(form, item, extraParams) {
                var rgb = this.getForm().findField('ColorId').getValue();
                var rgbArray = [0,0,0];
                if (rgb != null)
                	rgbArray = this.getForm().findField('ColorId').getRGBArray(rgb);
                	
                item.ColorR = rgbArray[0];
                item.ColorG = rgbArray[1];
                item.ColorB = rgbArray[2];
                /*
                 * if (value) { var rgbArray = new Viz.RGBColor(value).toRGBArray(); item.ColorR = rgbArray[0]; item.ColorG = rgbArray[1]; item.ColorB = rgbArray[2]; } else { item.ColorR = null; item.ColorG = null; item.ColorB = null; }
                 */
                return true;
            }
        });

Ext.reg('vizFormCalendarEventCategory', Viz.form.CalendarEventCategory);

﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.Portlet
 * @extends Ext.FormPanel.
 */
Viz.form.Portlet = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Portlet} entity : the portlet.
             */
            entity            : null,

            showEntityCombo   : false,
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler       : Viz.Services.EnergyWCF.Portlet_FormLoad,
                    serviceHandlerCreate : Viz.Services.EnergyWCF.Portlet_FormCreate,
                    serviceHandlerUpdate : Viz.Services.EnergyWCF.Portlet_FormUpdate,
                    items                : this.configFormGeneral(config),
                    messageSaveStart     : 'PortletChangeStart',
                    messageSaveEnd       : 'PortletChange',
                    labelWidth           : 200
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Viz.form.Portlet.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {

                var showEntityCombo = false;
                if (config && config.initialConfigFromGrid && config.initialConfigFromGrid.common)
                    showEntityCombo = config.initialConfigFromGrid.common.showEntityCombo;
                return [{
                            name   : 'KeyPortlet',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_portlet_title'),
                            name       : 'Title',
                            allowBlank : false
                        }, {
                            fieldLabel : $lang('msg_portlet_collapsed'),
                            name       : 'Collapsed',
                            xtype      : 'vizcheckbox'
                        }, {
                            fieldLabel : $lang('msg_portlet_headerandtoolbarvisible'),
                            name       : 'HeaderAndToolbarVisible',
                            xtype      : 'vizcheckbox',
                            allowBlank : false
                        }, {
                            fieldLabel           : $lang('msg_portlet'),
                            name                 : 'KeyEntity',
                            hiddenName           : 'KeyEntity',
                            xtype                : 'viztreecomboPortlet',
                            originalDisplayValue : 'EntityLongPath',
                            hidden               : !showEntityCombo,
                            allowBlank           : false,
                            listeners            : {
                                'select' : {
                                    fn    : function(tree, node) {
                                        if (!node.leaf || !node.attributes.entity) {
                                            tree.setRawValue("");
                                            this.entity.KeyEntity = null;
                                            this.entity.TypeEntity = null;
                                        }
                                        else {
                                            this.entity.TypeEntity = Viz.getEntityType(node.attributes.entity.__type);
                                            this.entity.EntityLongPath = Viz.util.Location.getLongPathFromNode(node);
                                        }
                                    },
                                    scope : this
                                }
                            }
                        }, {
                            fieldLabel       : $lang('msg_portlet_flex'),
                            name             : 'Flex',
                            xtype            : 'vizSpinnerField',
                            minValue         : 1,
                            maxValue         : 100,
                            decimalPrecision : 0,
                            allowBlank       : false,
                            value            : 1
                        }, {
                            fieldLabel       : $lang('msg_portlet_fixedheight'),
                            name             : 'FixedHeight',
                            xtype            : 'vizSpinnerField',
                            minValue         : 0,
                            decimalPrecision : 0,
                            allowBlank       : false,
                            value            : 0
                        }, {
                            // fieldLabel : $lang('msg_portlet_order'),
                            hidden : true,
                            name   : 'Order',
                            // xtype : 'vizSpinnerField',
                            // minValue : 1,
                            // decimalPrecision : 0,
                            // allowBlank : false,
                            value  : config.entity.Order || 1
                        }, {
                            fieldLabel       : $lang('msg_portlet_refreshfrequency'),
                            name             : 'RefreshFrequency',
                            xtype            : 'vizSpinnerField',
                            decimalPrecision : 0,
                            allowBlank       : true
                        }, {
                            fieldLabel    : $lang('msg_portlet_refreshanimationdirection'),
                            name          : 'RefreshAnimationDirection',
                            mode          : 'local',
                            triggerAction : 'all',
                            hidden        : config.entity && config.entity.TypeEntity == 'Vizelia.FOL.BusinessEntities.FlipCard',
                            displayField  : 'Display',
                            valueField    : 'Value',
                            store         : new Ext.data.ArrayStore({
                                        fields : ['Value', 'Display'],
                                        data   : [['r', $lang('msg_right')], ['l', $lang('msg_left')], ['t', $lang('msg_top')], ['b', $lang('msg_bottom')], ['f', $lang('msg_flip')]]
                                    }),
                            xtype         : 'combo',
                            allowBlank    : true
                        }, {
                            fieldLabel : $lang('msg_portlet_associateportaltab'),
                            name       : 'AssociatedPortalTabKey',
                            hiddenName : 'AssociatedPortalTabKey',
                            xtype      : 'vizComboPortalTab',
                            // hidden : config.entity && config.entity.TypeEntity == 'Vizelia.FOL.BusinessEntities.FlipCard',
                            allowBlank : true
                        }];
            }
        });

Ext.reg('vizFormPortlet', Viz.form.Portlet);
﻿Ext.namespace("Viz.form");

Viz.form.PsetAttributeDefinition = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.PsetAttributeDefinition} entity The entity to load the form with.
             */

            /**
             * @cfg {Viz.BusinessEntity.PsetDefinition} entityParent The entity parent (PsetDefinition) to load the form with.
             */

            /**
             * Ctor.
             * @param {Object} config
             */
            constructor           : function(config) {
                config = config || {};
                var defaultConfig = {};
                var forcedConfig = {};

                this._fieldPsetDataType = Ext.id();
                this._fieldPsetComponent = Ext.id();
                this._componentComboId = Ext.id();
                this._fieldPsetListDefinition = Ext.id();

                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, {
                            serviceHandlerCreate : Viz.Services.CoreWCF.PsetAttributeDefinition_FormCreate,
                            serviceHandlerUpdate : Viz.Services.CoreWCF.PsetAttributeDefinition_FormUpdate,
                            layout               : 'vbox',
                            layoutConfig         : {
                                align : 'stretch'
                            },
                            items                : [this.configFormGeneral(config), this.configFormTab(config)]
                        });
                Viz.form.PsetAttributeDefinition.superclass.constructor.call(this, config);

            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral     : function(config) {
                return {
                    xtype       : 'container',
                    layout      : 'form',
                    defaultType : 'textfield',
                    defaults    : {
                        anchor : Viz.Const.UI.Anchor
                    },
                    items       : [{
                                name      : 'KeyPropertySingleValue',
                                readOnly  : true,
                                hidden    : true,
                                hideLabel : true
                            }, {
                                name      : 'KeyPropertySet',
                                readOnly  : true,
                                hidden    : true,
                                hideLabel : true
                            }, {
                                fieldLabel : $lang('msg_localid'),
                                readOnly   : config.mode == "update" ? true : false,
                                name       : 'AttributeName',
                                allowBlank : false
                            }, {
                                fieldLabel : $lang('msg_psetattribute_order'),
                                name       : 'AttributeOrder',
                                xtype      : 'numberfield',
                                width      : 50,
                                anchor     : ' ',
                                value      : 1,
                                minValue   : 1,
                                allowBlank : false
                            }, {
                                fieldLabel : $lang('msg_description'),
                                name       : 'AttributeDescription',
                                xtype      : 'textarea'
                            }, /*
                                 * { fieldLabel : $lang('msg_label'), xtype : 'vizComboLocalization', name : 'AttributeMsgCode', hiddenName : 'AttributeMsgCode' },
                                 */
                            Viz.combo.Localization.factory({
                                        name : 'AttributeMsgCode'
                                    })]
                };
            },

            configFormTab         : function(config) {
                return {
                    xtype             : 'tabpanel',
                    activeTab         : 0,
                    flex              : 1,
                    margins           : '40 0 0 0',
                    bodyStyle         : 'background-color:transparent',
                    border            : true,
                    deferredRender    : false,
                    layoutOnTabChange : true,
                    defaults          : {
                        // to prevent mark invalid icon to show over the fieldLabel
                        hideMode  : 'offsets',
                        bodyStyle : 'background-color:transparent;padding: 5px',
                        defaults  : {
                            defaults : {
                                anchor : Viz.Const.UI.Anchor
                            }
                        }
                    },
                    items             : [{
                                title      : $lang('msg_component'),
                                layout     : 'form',
                                labelWidth : 150,
                                iconCls    : 'viz-icon-small-component',
                                items      : this.configFormComponent(config)
                            }, {
                                title      : $lang('msg_validation'),
                                layout     : 'form',
                                labelWidth : 150,
                                iconCls    : 'viz-icon-small-validation',
                                items      : this.configFormValidation(config)
                            }]
                };
            },

            configFormComponent   : function(config) {
                return [{
                            xtype    : 'container',
                            layout   : 'form',
                            defaults : {
                                anchor : Viz.Const.UI.Anchor
                            },
                            items    : [{
                                        id         : this._fieldPsetComponent,
                                        fieldLabel : $lang("msg_psetattribute_component"),
                                        name       : 'AttributeComponent',
                                        hiddenName : 'AttributeComponent',
                                        xtype      : 'vizComboListElement',
                                        listName   : 'PsetComponent',
                                        lastQuery  : '',
                                        listeners  : {
                                            // select : this.onSelectPsetComponent,
                                            setvalue : this.onSelectPsetComponent,
                                            scope    : this
                                        }

                                    }, {
                                        id         : this._fieldPsetDataType,
                                        fieldLabel : $lang("msg_psetattribute_datatype"),
                                        name       : 'AttributeDataType',
                                        hiddenName : 'AttributeDataType',
                                        xtype      : 'vizComboListElement',
                                        lastQuery  : '',
                                        listName   : 'PsetDataType'
                                    }, {
                                        fieldLabel     : $lang('msg_width'),
                                        name           : 'AttributeWidth',
                                        xtype          : 'vizSpinnerField',
                                        width          : 48,
                                        minValue       : 0,
                                        maxValue       : 800,
                                        allowDecimals  : false,
                                        incrementValue : 1,
                                        accelerate     : true,
                                        allowBlank     : true
                                    }, {
                                        fieldLabel : $lang('msg_psetattribute_hastab'),
                                        name       : 'AttributeHasTab',
                                        xtype      : 'vizcheckbox'
                                    }]

                        }, {
                            xtype  : 'container',
                            layout : 'column',
                            anchor : Viz.Const.UI.Anchor,
                            hidden : true,
                            id     : this._componentComboId,
                            items  : [{
                                        xtype       : 'container',
                                        layout      : 'form',
                                        defaults    : {
                                            anchor : Viz.Const.UI.Anchor
                                        },
                                        columnWidth : 1,
                                        items       : [{
                                                    id         : this._fieldPsetListDefinition,
                                                    name       : 'psetListDefinition',
                                                    fieldLabel : $lang('msg_component_combo'),
                                                    xtype      : 'vizComboListElement',
                                                    listName   : 'PsetListDefinition',
                                                    hiddenName : 'AttributeList',
                                                    allowBlank : true,
                                                    lastQuery  : ''
                                                }]
                                    }, {
                                        // width : 40,
                                        xtype   : 'button',
                                        tooltip : 'Manage',
                                        iconCls : 'viz-icon-small-list',
                                        handler : this.onManageList,
                                        scope   : this
                                    }]
                        }];
            },

            configFormValidation  : function(config) {
                return [{
                            fieldLabel : $lang('msg_psetattribute_defaultvalue'),
                            name       : 'AttributeDefaultValue',
                            width      : 150,
                            anchor     : Viz.Const.UI.Anchor,
                            xtype      : 'textfield'
                        }, {
                            xtype  : 'container',
                            layout : 'column',
                            anchor : ' ',
                            items  : [{
                                        xtype       : 'container',
                                        layout      : 'form',
                                        defaults    : {
                                            anchor : Viz.Const.UI.Anchor,
                                            width  : 150
                                        },
                                        columnWidth : 0.5,
                                        items       : [{
                                                    fieldLabel : $lang('msg_psetattribute_minvalue'),
                                                    name       : 'AttributeMinValue',
                                                    xtype      : 'textfield'
                                                }]
                                    }, {
                                        xtype       : 'container',
                                        layout      : 'form',
                                        defaults    : {
                                            anchor : Viz.Const.UI.Anchor
                                        },
                                        columnWidth : 0.5,
                                        items       : [{
                                                    fieldLabel : $lang('msg_psetattribute_maxvalue'),
                                                    name       : 'AttributeMaxValue',
                                                    xtype      : 'textfield'
                                                }]
                                    }]
                        }, {
                            fieldLabel : $lang('msg_psetattribute_allowblank'),
                            name       : 'AttributeAllowBlank',
                            xtype      : 'vizcheckbox',
                            checked    : true
                        }, {
                            xtype  : 'container',
                            layout : 'column',
                            anchor : ' ',
                            items  : [{
                                        xtype       : 'container',
                                        layout      : 'form',
                                        defaults    : {
                                            anchor : Viz.Const.UI.Anchor
                                        },
                                        columnWidth : 0.5,
                                        items       : [{
                                                    fieldLabel : $lang('msg_psetattribute_mask'),
                                                    name: 'AttributeMask',
                                                    validator  : Ext.form.VTypes.regexValidator,
                                                    xtype      : 'textfield'
                                                }]
                                    }, {
                                        xtype       : 'container',
                                        layout      : 'form',
                                        defaults    : {
                                            anchor : Viz.Const.UI.Anchor
                                        },
                                        columnWidth : 0.5,
                                        items       : [{
                                                    fieldLabel    : $lang('msg_psetattribute_vtype'),
                                                    name          : 'AttributeVType',
                                                    xtype         : 'combo',
                                                    triggerAction : 'all',
                                                    resizable     : true,
                                                    listWidth     : 250,
                                                    store         : this.buildArrayVTypes()
                                                }]
                                    }]
                        }];
            },

            /**
             * Builds an array of available VTypes.
             */
            buildArrayVTypes      : function() {
                var result = [];
                for (var vtype in Ext.form.VTypes) {
                    if (!vtype.endsWith("Text") && !vtype.endsWith("Mask") && vtype != 'maskValidator') {
                        switch (vtype) {
                            case 'alpha' :
                            case 'alphanum' :
                            case 'email' :
                            case 'url' :
                                result.push(vtype);
                                break;

                        }

                    }
                }
                // we sort alphabetically ignoring the case.
                return result.sort(function charOrdA(a, b) {
                            a = a.toLowerCase();
                            b = b.toLowerCase();
                            if (a > b)
                                return 1;
                            if (a < b)
                                return -1;
                            return 0;
                        });
            },

            /**
             * Event handler when the PsetComponent combo box is changed.
             * @param {Ext.form.ComboBox} combo
             * @param {Ext.data.Record} record
             * @param {int} index
             */
            onSelectPsetComponent : function(combo, record, index) {
                var psetDataType = Ext.getCmp(this._fieldPsetDataType);
                var value = record.get("Value");
                var filterBy = null;
                var dataTypeValue = psetDataType.getValue();

                psetDataType.clearValue();
                psetDataType.store.clearFilter();

                var section = Ext.getCmp(this._componentComboId);
                section.setVisible(false)

                switch (value) {
                    case "textfield" :
                    case "textarea" :
                        filterBy = this.filterByTextField;
                        break;
                    case "numberfield" :
                        filterBy = this.filterByNumberField;
                        break;
                    case "checkbox" :
                        filterBy = this.filterByCheckbox;
                        break;

                    case "combo" :
                        filterBy = this.filterByTextField;
                        section.setVisible(true);
                        break;

                    case "datefield" :
                    case "xdatetime" :
                    case "timefield" :
                        filterBy = this.filterByDate;
                        break;
                    case "vizImageUpload" :
                    case "vizFileUpload" :
                        filterBy = this.filterByDocument;
                        break;
                    case "vizGridPsetAttributeHistorical" :
                        filterBy = this.filterByHistorical;
                        break;
                    default :
                        filterBy = this.filterByTextField;
                        break;
                }
                if (filterBy != null)
                    psetDataType.store.filterBy(filterBy, this);

                if (psetDataType.store.find("Value", dataTypeValue) > -1)
                    psetDataType.setValue(dataTypeValue);

            },

            filterByDocument      : function(record, id) {
                switch (record.get("Value")) {
                    case "document" :
                        return true;
                    default :
                        return false;
                }
            },

            filterByHistorical    : function(record, id) {
                switch (record.get("Value")) {
                    case 'yearly' :
                    case 'monthly' :
                    case 'daily' :
                    case 'hourly' :
                    case 'minutly' :
                        return true;
                    default :
                        return false;
                }
            },

            filterByTextField     : function(record, id) {
                switch (record.get("Value")) {
                    case "string" :
                        return true;
                    default :
                        return false;
                }
            },

            filterByCheckbox      : function(record, id) {
                switch (record.get("Value")) {
                    case "boolean" :
                    case "bool" :
                        return true;
                    default :
                        return false;
                }
            },

            filterByNumberField   : function(record, id) {
                switch (record.get("Value")) {
                    case "int" :
                    case "float" :
                        return true;
                    default :
                        return false;
                }
            },

            filterByDate          : function(record, id) {
                switch (record.get("Value")) {
                    case "date" :

                        return true;
                    default :
                        return false;
                }
            },

            onManageList          : function() {
                var listName = Ext.getCmp(this._fieldPsetListDefinition).getValue();
                var mode = !Ext.isEmpty(listName) ? "update" : "create";
                var entity = {
                    listName : listName
                };
                var win = new Ext.Window({
                            title   : $lang('msg_listmanagement'),
                            iconCls : 'viz-icon-small-list',
                            width   : 650,
                            height  : 350,
                            layout  : 'fit',
                            items   : [{
                                        xtype     : 'vizFormPsetList',
                                        mode      : mode,
                                        entity    : entity,
                                        listeners : {
                                            savefailure : function(f) {
                                                var listName = f.findByType('combo')[0];
                                                if (listName)
                                                    listName.markInvalid($lang('error_msg_uniquefield'));
                                                return false;
                                            },
                                            savesuccess : function(f, o) {
                                                var newlistName = o.options.extraParams.listName;
                                                var listCombo = Ext.getCmp(this._fieldPsetListDefinition)
                                                listCombo.store.load();
                                                listCombo.setValue(newlistName);
                                            },
                                            scope       : this
                                        }
                                    }]
                        });
                win.show();

            }

        });

Ext.reg("vizFormPsetAttributeDefinition", Viz.form.PsetAttributeDefinition);
﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.AlgorithmInputDefinition
 * @extends Ext.FormPanel.
 */
Viz.form.AlgorithmInputDefinition = Ext.extend(Viz.form.FormPanel, {

            /**
             * @cfg {Viz.BusinessEntity.Playlist} entity The Playlist entity the playlist page belongs to.
             */

            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                var defaultConfig = {
                    mode_load            : 'remote',
                    serviceHandler: Viz.Services.EnergyWCF.AlgorithmInputDefinition_FormLoad,
                    serviceHandlerCreate: Viz.Services.EnergyWCF.AlgorithmInputDefinition_FormCreate,
                    serviceHandlerUpdate: Viz.Services.EnergyWCF.AlgorithmInputDefinition_FormUpdate,
                    items                : this.configFormGeneral(config),
                    closeonsave          : false
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.AlgorithmInputDefinition.superclass.constructor.call(this, config);
            },

            /**
             * TODO : define here correct fields General form configuration
             * @param {Object} config The configuration options of the form
             */
            configFormGeneral : function(config) {
                return [{
                    name: 'KeyAlgorithmInputDefinition',
                            hidden : true
                        }, {
                            fieldLabel : $lang('msg_name'),
                            name       : 'Name',
                            allowBlank : false
                        }, {
                            xtype      : 'textarea',
                            anchor     : Viz.Const.UI.Anchor,
                            fieldLabel : $lang('msg_description'),
                            name       : 'Description',
                            height     : 50
                        }];
            }

        });

                Ext.reg('vizFormAlgorithmInputDefinition', Viz.form.AlgorithmInputDefinition);
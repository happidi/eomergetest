﻿Ext.namespace('Viz.form');

/**
 * @class Viz.form.Space
 * @extends Ext.FormPanel.
 */
Viz.form.Space = Ext.extend(Viz.form.FormPanel, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor       : function(config) {
                config = config || {};
                this._tabContainerId = Ext.id();
                var defaultConfig = {
                    mode_load            : 'remote',
                    psetConfig           : {
                        IfcType                    : 'IfcSpace',
                        entityKey                  : 'KeySpace',
                        filterByClassificationItem : false
                    },
                    serviceHandler       : Viz.Services.CoreWCF.Space_FormLoad,
                    serviceHandlerCreate : Viz.Services.CoreWCF.Space_FormCreate,
                    serviceHandlerUpdate : Viz.Services.CoreWCF.Space_FormUpdate,
                    hasValidationToolbar : true,
                    items                : this.configFormGeneral(config)

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Space.superclass.constructor.call(this, config);

            },

            /**
             * The configuration for the form general.
             */
            configFormGeneral : function(config) {
                return [{
                    name       : 'LocalId',
                    fieldLabel : $lang('msg_localid'),
                    allowBlank : false
                        // ,
                        // readOnly : config.mode == "update" ? true : false
                    }, {
                    fieldLabel : $lang('msg_name'),
                    allowBlank : false,
                    name       : 'Name'
                }, {
                    fieldLabel : $lang('msg_space_areavalue'),
                    name       : 'AreaValue',
                    xtype      : 'viznumberfield',
                    value      : 0
                }, {
                    fieldLabel : $lang('msg_space_elevationwithflooring'),
                    name       : 'ElevationWithFlooring',
                    xtype      : 'viznumberfield',
                    value      : 0
                }, {
                    xtype      : 'textarea',
                    anchor     : Viz.Const.UI.Anchor + ' -150',
                    fieldLabel : $lang('msg_description'),
                    name       : 'Description'
                }]
            }
        });

Ext.reg('vizFormSpace', Viz.form.Space);

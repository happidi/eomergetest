﻿Ext.namespace('Viz.form');
/**
 * @class Viz.form.ReportSubscriptionSchedule
 * @extends Viz.form.CalendarDefinition.
 */
Viz.form.ReportSubscriptionSchedule = Ext.extend(Viz.form.CalendarDefinition, {
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                var defaultConfig = {
                    serviceHandler           : null,
                    serviceHandlerCreate     : null,
                    serviceHandlerUpdate     : null,
                    mode_load                : 'local',
                    Category                 : 'ReportSubscriptionSchedule',
                    showHasRecurrencePattern : false,
                    showTitle                : false,
                    eventHasDuration         : false,
                    showMinutely             : false,
                    showHourly               : true,
                    showDaily                : false,
                    showWeekly               : true,
                    showWeeklyRecurs         : false,
                    showMonthly              : true,
                    showYearly               : false,
                    showYearlyRecurs         : false,
                    showEndDate              : false,
                    showExplicitMonths       : true,
                    showNumberOccurrence     : false

                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Viz.form.Schedule.superclass.constructor.call(this, config);
            }

        });

Ext.reg('vizFormReportSubscriptionSchedule', Viz.form.ReportSubscriptionSchedule);
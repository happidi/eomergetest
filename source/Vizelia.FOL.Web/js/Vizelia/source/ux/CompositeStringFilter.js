﻿/**
 * A special kind of StringFilter that takes a dataIndexGeneric config parameter that can be anything inclunding a formula to allow complex search criteria <br/>
 * <p>
 * <i>Example Usage:</i>
 * </p>
 * 
 * <pre><code>
 * var filters = new Ext.ux.grid.GridFilters({
 *     ...
 *     filters: [{
 *         // required configs
 *         type: 'compositeString',
 *         dataIndexGeneric: 'FirstName + \' \' + LastName'
 *     }]
 * });
 * </code></pre>
 * 
 * @class Ext.ux.grid.filter.CompositeStringFilter
 * @extends Ext.ux.grid.filter.StringFilter Filter by a configurable Ext.Component
 */
Ext.ux.grid.filter.CompositeStringFilter = Ext.extend(Ext.ux.grid.filter.StringFilter, {

            constructor   : function(config) {
                config.type = "string";
                Ext.ux.grid.filter.CompositeStringFilter.superclass.constructor.call(this, config);
            },

            /**
             * @private Template method that is to get and return serialized filter data for transmission to the server.
             * @return {Object/Array} An object or collection of objects containing key value pairs representing the current configuration of the filter.
             */
            getSerialArgs : function() {
                return {
                    type      : 'string',
                    dataIndex : this.dataIndexGeneric ? this.dataIndexGeneric : this.dataIndex,
                    value     : this.inputItem.getValue()
                };
            }
        });
﻿Ext.ns('Ext.ux.form.HtmlEditor');

/**
 * @class Ext.ux.form.HtmlEditor.Divider
 * @extends Ext.util.Observable
 * <p>
 * A plugin that creates a divider on the HtmlEditor. Used for separating additional buttons.
 * </p>
 */
Ext.ux.form.HtmlEditor.Divider = Ext.extend(Ext.util.Observable, {
            // private
            init     : function(cmp) {
                this.cmp = cmp;
                this.cmp.on('render', this.onRender, this);
            },
            // private
            onRender : function() {
                this.cmp.getToolbar().addButton([new Ext.Toolbar.Separator()]);
            }
        });

/**
 * @class Ext.ux.form.HtmlEditor.DynamicDisplayImage
 * @extends Ext.util.Observable
 * <p>
 * A plugin that insert a tag to render a DynamicDisplayImage.
 * </p>
 */
Ext.ux.form.HtmlEditor.DynamicDisplayImage = Ext.extend(Ext.util.Observable, {

            // private
            init     : function(cmp) {
                this.cmp = cmp;
                this.cmp.on('render', this.onRender, this);
            },
            // private
            onRender : function() {
                var cmp = this.cmp;
                var btn = this.cmp.getToolbar().addButton({
                            iconCls      : 'x-edit-dynamicdisplayimage',
                            handler      : function() {
                                if (!this.imageWindow) {
                                    this.imageWindow = new Ext.Window({
                                                title       : $lang('msg_htmleditor_insertimage'),
                                                width       : 400,
                                                height      : 300,
                                                closeAction : 'hide',
                                                items       : [{
                                                            itemId     : 'insert-image',
                                                            xtype      : 'form',
                                                            border     : false,
                                                            plain      : true,
                                                            bodyStyle  : 'padding: 10px;background-color:transparent',
                                                            labelWidth : 60,
                                                            labelAlign : 'right',
                                                            items      : [{
                                                                        xtype         : 'numberfield',
                                                                        allowBlank    : false,
                                                                        allowDecimals : false,
                                                                        fieldLabel    : $lang('msg_virtualfile_width'),
                                                                        name          : 'Width',
                                                                        width         : 60
                                                                    }, {
                                                                        xtype         : 'numberfield',
                                                                        allowBlank    : false,
                                                                        allowDecimals : false,
                                                                        fieldLabel    : $lang('msg_virtualfile_height'),
                                                                        name          : 'Height',
                                                                        width         : 60
                                                                    }, Viz.combo.DynamicDisplayImage.factory({
                                                                                name       : 'Picture',
                                                                                allowBlank : false
                                                                            })]
                                                        }],
                                                buttons     : [{
                                                            text    : $lang('msg_htmleditor_insert'),
                                                            iconCls : 'viz-icon-small-save',
                                                            handler : function() {
                                                                var frm = this.imageWindow.getComponent('insert-image').getForm();
                                                                if (frm.isValid()) {
                                                                    var width = frm.findField('Width').getValue();
                                                                    var height = frm.findField('Height').getValue();
                                                                    var key = frm.findField('Picture').getValue();
                                                                    var html = String.format("<image src='{4}Public.svc/DynamicDisplayImage_GetStreamImage?Key={2}&width={0}&height={1}&applicationName={3}' style='width:{0}px;height:{1}px;'>", width, height, key, Viz.App.Global.User.ApplicationName, document.URL.replaceAll('#', ''));
                                                                    this.cmp.onFirstFocus();
                                                                    this.cmp.insertAtCursor(html);
                                                                    this.imageWindow.hide();
                                                                }

                                                            },
                                                            scope   : this
                                                        }, {
                                                            text    : $lang('msg_cancel'),
                                                            iconCls : 'viz-icon-small-cancel',
                                                            handler : function() {
                                                                this.imageWindow.hide();
                                                            },
                                                            scope   : this
                                                        }]
                                            });

                                }
                                else {
                                    this.imageWindow.getEl().frame();
                                }
                                this.imageWindow.show();
                            },
                            scope        : this,
                            tooltip      : {
                                title : $lang('msg_htmleditor_insertimage')
                            },
                            overflowText : $lang('msg_htmleditor_image')
                        });
            }
        });

/**
 * @class Ext.ux.form.HtmlEditor.Table
 * @extends Ext.util.Observable
 * <p>
 * A plugin that creates a button on the HtmlEditor for making simple tables.
 * </p>
 */
Ext.ux.form.HtmlEditor.Table = Ext.extend(Ext.util.Observable, {
            /**
             * @cfg {Array} tableBorderOptions A nested array of value/display options to present to the user for table border style. Defaults to a simple list of 5 varrying border types.
             */
            tableBorderOptions : [['none', 'None'], ['1px solid #000', 'Sold Thin'], ['2px solid #000', 'Solid Thick'], ['1px dashed #000', 'Dashed'], ['1px dotted #000', 'Dotted']],
            // private
            init               : function(cmp) {
                this.cmp = cmp;
                this.cmp.on('render', this.onRender, this);
            },
            // private
            onRender           : function() {
                var cmp = this.cmp;
                var btn = this.cmp.getToolbar().addButton({
                            iconCls      : 'x-edit-table',
                            handler      : function() {
                                if (!this.tableWindow) {
                                    this.tableWindow = new Ext.Window({
                                                title       : $lang('msg_htmleditor_inserttable'),
                                                closeAction : 'hide',
                                                items       : [{
                                                            itemId     : 'insert-table',
                                                            xtype      : 'form',
                                                            border     : false,
                                                            plain      : true,
                                                            bodyStyle  : 'padding: 10px;',
                                                            labelWidth : 60,
                                                            labelAlign : 'right',
                                                            items      : [{
                                                                        xtype         : 'numberfield',
                                                                        allowBlank    : false,
                                                                        allowDecimals : false,
                                                                        fieldLabel    : $lang('msg_htmleditor_rows'),
                                                                        name          : 'row',
                                                                        width         : 60
                                                                    }, {
                                                                        xtype         : 'numberfield',
                                                                        allowBlank    : false,
                                                                        allowDecimals : false,
                                                                        fieldLabel    : $lang('msg_htmleditor_columns'),
                                                                        name          : 'col',
                                                                        width         : 60
                                                                    }, {
                                                                        xtype          : 'combo',
                                                                        fieldLabel     : $lang('msg_htmleditor_border'),
                                                                        name           : 'border',
                                                                        forceSelection : true,
                                                                        mode           : 'local',
                                                                        store          : new Ext.data.ArrayStore({
                                                                                    autoDestroy : true,
                                                                                    fields      : ['spec', 'val'],
                                                                                    data        : this.tableBorderOptions
                                                                                }),
                                                                        triggerAction  : 'all',
                                                                        value          : 'none',
                                                                        displayField   : 'val',
                                                                        valueField     : 'spec',
                                                                        width          : 90
                                                                    }]
                                                        }],
                                                buttons     : [{
                                                            text    : $lang('msg_htmleditor_insert'),
                                                            iconCls : 'viz-icon-small-save',
                                                            handler : function() {
                                                                var frm = this.tableWindow.getComponent('insert-table').getForm();
                                                                if (frm.isValid()) {
                                                                    var border = frm.findField('border').getValue();
                                                                    var rowcol = [frm.findField('row').getValue(), frm.findField('col').getValue()];
                                                                    if (rowcol.length == 2 && rowcol[0] > 0 && rowcol[0] < 10 && rowcol[1] > 0 && rowcol[1] < 10) {
                                                                        var html = "<table>";
                                                                        for (var row = 0; row < rowcol[0]; row++) {
                                                                            html += "<tr>";
                                                                            for (var col = 0; col < rowcol[1]; col++) {
                                                                                html += "<td width='20%' style='border: " + border + ";'>" + row + "-" + col + "</td>";
                                                                            }
                                                                            html += "</tr>";
                                                                        }
                                                                        html += "</table>";
                                                                        this.cmp.insertAtCursor(html);
                                                                    }
                                                                    this.tableWindow.hide();
                                                                }
                                                                else {
                                                                    if (!frm.findField('row').isValid()) {
                                                                        frm.findField('row').getEl().frame();
                                                                    }
                                                                    else if (!frm.findField('col').isValid()) {
                                                                        frm.findField('col').getEl().frame();
                                                                    }
                                                                }
                                                            },
                                                            scope   : this
                                                        }, {
                                                            text    : $lang('msg_cancel'),
                                                            iconCls : 'viz-icon-small-cancel',

                                                            handler : function() {
                                                                this.tableWindow.hide();
                                                            },
                                                            scope   : this
                                                        }]
                                            });

                                }
                                else {
                                    this.tableWindow.getEl().frame();
                                }
                                this.tableWindow.show();
                            },
                            scope        : this,
                            tooltip      : {
                                title : $lang('msg_htmleditor_inserttable')
                            },
                            overflowText : $lang('msg_htmleditor_table')
                        });
            }
        });

Ext.form.HtmlEditor.prototype.fontFamilies = ['Arial', 'Courier New', 'Tahoma', 'Segoe UI', 'Times New Roman', 'Verdana'];
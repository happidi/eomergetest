﻿Ext.namespace('Ext.ux.menu');

Ext.ux.menu.StoreMenu = function(config) {
    Ext.ux.menu.StoreMenu.superclass.constructor.call(this, config);
    if (!this.store.loadData) {
        this.store = Ext.create(this.store);
    }

    // Keep track of what menu items have been added
    this.storeMenus = [];

    // List of handlers for menu and submenus
    this.handlerTypes = {};

    if (this.itemsOffset === undefined)
        this.itemsOffset = 0;

    this.on('beforeshow', this.onMenuBeforeShow, this);
    this.on('show', this.onMenuLoad, this);
    this.store.on('beforeload', this.onBeforeLoad, this);
    this.store.on('load', this.onLoad, this);
};

Ext.extend(Ext.ux.menu.StoreMenu, Ext.menu.Menu, {
    loadingText: Ext.LoadMask.prototype.msg || $lang('msg_loading'),
    loaded: false,
    repositioned: false,
    enableScrolling: true,
    maxHeight: 400,

    onMenuLoad: function () {
        if (this.parentMenu.activeItem)
            this.parentActiveItem = this.parentMenu.activeItem;
        if (!this.loaded || this.autoReload) {
            if (this.options) {
                this.store.load(this.options);
            }
            else {
                this.store.load();
            }
        }
    },

    onDestroy: function () {
        if (this.store) {
            this.store.un('beforeload', this.onBeforeLoad, this);
            this.store.un('load', this.onLoad, this);
        }

        if (this.storeMenus) {
            for (var i = 0; i < this.storeMenus.length; i++) {
                this.storeMenus[i].destroy();
                this.remove(this.storeMenus[i]);
            }
        }

        this.un('beforeshow', this.onMenuBeforeShow, this);
        this.un('show', this.onMenuLoad, this);

        Ext.ux.menu.StoreMenu.superclass.onDestroy.apply(this, arguments);
    },

    updateMenuItems: function (loadedState, records) {
        // var visible = this.isVisible();
        // this.hide(false);

        for (var i = 0; i < this.storeMenus.length; i++) {
            this.storeMenus[i].destroy();
            this.remove(this.storeMenus[i]);
        }
        this.storeMenus = [];

        // to sync the height of the shadow
        if (this.el && this.el.sync)
            this.el.sync();

        if (loadedState) {
            for (var i = 0, len = records.length; i < len; i++) {
                // create a real function if a handler or menu is given as a string (because a function cannot really be encoded in JSON
                var json = this.createMenuItemFromRecord(records[i]);
                if (!Ext.isEmpty(json)) {
                    if (!Ext.isArray(json)) {
                        json = [json];
                    }
                    for (j = 0; j < json.length; j++) {
                        this.storeMenus.push(this.insert(this.itemsOffset + i + j, json[j]));
                    }
                }
            }
        }
        else {
            this.storeMenus.push(this.insert(this.itemsOffset + i, '<span class="loading-indicator">' + this.loadingText + '</span>'));
        }
        this.loaded = loadedState;
    },

    createMenuItemFromRecord: function (record) {
        return null;
    },

    onBeforeLoad: function (store) {
        this.updateMenuItems(false);
    },

    onLoad: function (store, records) {
        // Resort the underlying store, if it sorts in client-side.
        if (store.sortInfo && !store.remoteSort) {
            store.sort(store.sortInfo.field, store.sortInfo.direction);
            records = store.data.items;
        }
        
        this.updateMenuItems(true, records);
        if (!this.repositioned && this.el && this.el.dom) { // reshow the menu so as to calculate it's correct position once it has been populated by the store. This fixes the issue where the menu may get clipped by the viewport
            if (this.parentMenu) {
                this.addListener('beforeshow', function () {
                    this.loaded = true;
                }); // cancel out the beforeshow this.loaded=false function to stop the store reloading unneccesarily`
                if (this.storeMenus.length > 0 || (this.items && this.items.length > 0)) {
                    this.showAt([this.el.getXY()[0], this.el.getXY()[1]], this.parentMenu);
                    if (this.parentMenu && this.parentActiveItem) {
                        this.parentMenu.setActiveItem(this.parentActiveItem);
                    }
                    // this.showAt(this.el.getAlignToXY(this.activeItem.el, 'tl-tr?', [0, 0]), this.parentMenu);
                }
                else
                    this.hide();
            }
            else {
                if (this.storeMenus.length > 0)
                    this.showAt([this.el.getXY()[0], this.el.getXY()[1]]);

            };
            this.repositioned = true;
        };
        if ((!this.storeMenus || this.storeMenus.length <= 0) && (!this.items || this.items.length <= 0))
            this.hide();
    },

    onMenuBeforeShow: function () {
        if (this.loaded && this.items.length == 0 && (!this.storeMenus || this.storeMenus.length <= 0))
            return false;
        return true;
    },

    setItemsHandler: function (handler) {
        this.itemsHandler = handler;
    },

    setSubMenuHandler: function (handler, handlerType) {
        this.handlerTypes[handlerType] = handler;
    },

    setOffset: function (offset) {
        this.itemsOffset = offset;
    },

    setAutoReload: function (autoReload) {
        this.autoReload = autoReload;
    },

    setBaseParam: function (param, value) {
        this.store.setBaseParam(param, value);
    },

    setStore: function (store) {
        this.store = store;
    }
});
Ext.reg('menustore', Ext.ux.menu.StoreMenu);
﻿/**
 * @class Ext.ux.grid.filter.TreeFilter
 * @extends Ext.ux.grid.filter.Filter Filter by a configurable Ext.form.TextField and a TreeComboMultiSelect
 */
Ext.ux.grid.filter.TreecombomultiselectFilter = Ext.extend(Ext.ux.grid.filter.Filter, {
            /**
             * @cfg {String} iconCls The iconCls to be applied to the menu item. Defaults to <tt>'ux-gridfilter-text-icon'</tt>.
             */
            iconCls         : 'ux-gridfilter-text-icon',
            selectOnFocus   : true,
            width           : 290,
            dataIndexTree   : null,
            /**
             * @cfg {String} queryType The type of query that is performed, either an standard innerjoin (default) or a innerpath for better performance with hierarchies.
             */
            queryType       : 'innerjoin',
            /**
             * @private Template method that is to initialize the filter and install required menu items.
             */
            init            : function(config) {
                var width = 290;
                var configCombo = {
                    iconCls         : this.iconCls,
                    width           : Ext.isIE ? width - 38 : width,
                    listeners       : {
                        autosize   : function() {
                            this.menu.doLayout();
                        },
                        select     : function() {
                            this.menu.doLayout();
                            this.inputItem.autoSize();
                            this.onSelect();
                        },
                        clear      : function() {
                            this.menu.doLayout();
                            this.inputItem.autoSize();
                            this.onSelect();
                        },
                        removeitem : function() {
                            this.menu.doLayout();
                            this.inputItem.autoSize();
                            this.onSelect();
                        },
                        scope      : this
                    },
                    getListParent   : function() {
                        if (this.el)
                            return this.el.up('.x-menu');
                        else
                            return document.body;
                    },
                    getAlignElement : function() {
                        return this.el.up('.x-menu');
                    },
                    getAlignOffset  : function() {
                        return [28, 2];
                    },
                    getWidthOffset  : function() {
                        return -28;
                    }
                };
                Ext.apply(config, configCombo);
                this.inputItem = new Ext.ComponentMgr.types[config.xtype](config);
                this.menu.destroy();
                this.menu = new Ext.menu.Menu({
                            style : {
                                overflow : 'visible'
                            }
                        });
                this.inputText = new Ext.form.TextField({
                            width           : width,
                            enableKeyEvents : true,
                            iconCls         : this.iconCls,
                            style           : {
                                marginLeft   : '2px',
                                marginBottom : '2px'
                            },
                            listeners       : {
                                scope : this,
                                keyup : this.onInputKeyUp
                            }
                        });
                this.menu.add(this.inputText);
                this.menu.add(this.inputItem);
                this.menu.doLayout();
                this.updateTask = new Ext.util.DelayedTask(this.fireUpdate, this);
            },
            /**
             * @private Template method that is to get and return the value of the filter.
             * @return {String} The value of this filter
             */
            getValue        : function() {
                if (!Ext.isEmpty(this.inputItem.treePanel)) {
                    return this.inputItem.getValue();
                }
                else {
                    return this.inputItem.value;
                }
            },
            /**
             * @private Template method that is to set the value of the filter.
             * @param {Object} value The value to set the filter
             */
            setValue        : function(value) {
                this.inputItem.removeAllItems();
                this.inputItem.setValue(value);
                this.fireEvent('update', this);
            },
            /**
             * Returns an Array of the paths of the checked nodes of the treeview.
             * @return {String} The path values of this filter
             */
            getPathAsArray  : function() {
                return this.inputItem.getPathAsArray();
            },
            selectPathArray : function(pathArray) {
                this.inputItem.selectedPath = pathArray;
                for (var i = 0; i < pathArray.length; i++) {
                    this.inputItem.selectPath(pathArray[i]);
                }
            },
            /**
             * @private Template method that is to return <tt>true</tt> if the filter has enough configuration information to be activated.
             * @return {Boolean}
             */
            isActivatable   : function() {
                return (this.inputItem.getValue().length > 0) || (this.inputText.getValue() != null && this.inputText.getValue().length > 0);
            },
            /**
             * @private Template method that is to get and return serialized filter data for transmission to the server.
             * @return {Object/Array} An object or collection of objects containing key value pairs representing the current configuration of the filter.
             */
            getSerialArgs   : function() {
                return [{
                            type      : this.queryType,
                            dataIndex : this.dataIndexTree,
                            value     : this.getValue()
                        }, {
                            type  : 'string',
                            value : this.inputText.getValue()
                        }];
            },
            /**
             * Template method that is to validate the provided Ext.data.Record against the filters configuration.
             * @param {Ext.data.Record} record The record to validate
             * @return {Boolean} true if the record is valid within the bounds of the filter, false otherwise.
             */
            validateRecord  : function(record) {
                var val = record.get(this.dataIndex);
                if (typeof val != 'string') {
                    return (this.getValue().length === 0);
                }
                return val.toLowerCase().indexOf(this.getValue().toLowerCase()) > -1;
            },
            /**
             * @private Handler method called when there is a keyup event on this.inputItem
             */
            onInputKeyUp    : function(field, e) {
                var k = e.getKey();
                if (k == e.RETURN && field.isValid()) {
                    e.stopEvent();
                    this.menu.hide(true);
                    return;
                }
                // restart the timer
                this.updateTask.delay(this.updateBuffer);
            },
            /**
             * @private Handler method called when an element in the combo is selected.
             * @param {} combo
             * @param {} record
             * @param {} index
             */
            onSelect        : function() {
                this.updateTask.delay(this.updateBuffer);
            }
        });
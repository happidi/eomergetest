﻿/**
 * A  filter allowing a dataIndexGeneric config parameter and accepts an enumTypeName to create a vizComboEnum as the exposed filter field. <br/>
 * <p>
 * <i>Example Usage:</i>
 * </p>
 * 
 * <pre><code>
 * var filters = new Ext.ux.grid.GridFilters({
 *     ...
 *     filters: [{
 *         // required configs
 *         type: 'comboEnum',
 *         dataIndexGeneric: 'DisplayMode',
 *         enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartDisplayMode,Vizelia.FOL.Common'
 *     }]
 * });
 * </code></pre>
 * 
 * @class Ext.ux.grid.filter.ComboEnumFilter
 * @extends Ext.ux.grid.filter.GenericFilter Filter for an enum
 */
Ext.ux.grid.filter.ComboEnumFilter = Ext.extend(Ext.ux.grid.filter.GenericFilter, {
           
            constructor   : function(config) {
            	
            	var defaultConfig = {
                  	width            : 200,
                    xtype            : 'vizComboEnum',
                    valueField       : 'Id'
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
           	
                Ext.ux.grid.filter.GenericFilter.superclass.constructor.call(this, config);
            }
       });
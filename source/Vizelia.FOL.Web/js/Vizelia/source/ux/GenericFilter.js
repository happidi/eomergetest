﻿/**
 * A generic filter allowing a dataIndexGeneric config parameter and accepts any xtype (mainly combos) as the exposed filter field. <br/>
 * <p>
 * <i>Example Usage:</i>
 * </p>
 * 
 * <pre><code>
 * var filters = new Ext.ux.grid.GridFilters({
 *     ...
 *     filters: [{
 *         // required configs
 *         type: 'generic',
 *         dataIndexGeneric: 'FirstName + \' \' + LastName',
 *         xtype : 'vizComboOccupant'
 *     }]
 * });
 * </code></pre>
 * 
 * @class Ext.ux.grid.filter.GenericFilter
 * @extends Ext.ux.grid.filter.CompositeStringFilter Filter by a configurable Ext.Component
 */
Ext.ux.grid.filter.GenericFilter = Ext.extend(Ext.ux.grid.filter.CompositeStringFilter, {
            width         : 200,

            constructor   : function(config) {
                Ext.ux.grid.filter.GenericFilter.superclass.constructor.call(this, config);
            },

            /**
             * @cfg {String} dataIndexGeneric The column thath should be filtered (can be different from the column where the filter is ).
             */
            /**
             * @private Template method that is to initialize the filter and install required menu items.
             */
            init          : function(config) {
                var width = config.width || this.width;
                Ext.apply(config, {
                            enableKeyEvents : false,
                            iconCls         : this.iconCls,
                            width           : width - 38,
                            allowBlank      : true,
                            style           : {
                                marginLeft   : '2px',
                                marginBottom : '2px'
                            },
                            listeners       : {
                                scope  : this,
                                select : this.onSelectCombo
                            },
                            getListParent   : function() {
                                return this.el.up('.x-menu');
                            },
                            getAlignElement : function() {
                                return this.el.up('.x-menu');
                            },
                            getAlignOffset  : function() {
                                return [28, 2];
                            },
                            getWidthOffset  : function() {
                                return -28;
                            }
                        });
                this.menu.destroy();
                this.menu = new Ext.menu.Menu({
                            width : width,
                            style : {
                                overflow : 'visible'
                            }
                        });
                this.inputItem = Ext.ComponentMgr.create(config);
                this.menu.add(this.inputItem);
                this.menu.doLayout();
                this.updateTask = new Ext.util.DelayedTask(this.fireUpdate, this);
            },
            onSelectCombo : function() {
                this.updateTask.delay(this.updateBuffer);
            }
        });
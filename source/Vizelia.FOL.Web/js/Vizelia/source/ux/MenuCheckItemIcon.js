﻿Ext.namespace('Ext.ux.menu');
/**
 * @class Ext.ux.menu.CheckItemIcon
 * @extends Ext.menu.CheckItem Adds a menu item that contains a checkbox and icon in the text.
 * @constructor Creates a new CheckItemIcon
 * @param {Object} config Configuration options
 * @xtype menucheckitemicon
 */
Ext.ux.menu.CheckItemIcon = Ext.extend(Ext.menu.CheckItem, {
            /**
             * @cfg {String} textIconCls The CSS class to use for icon displayed in the menu text.
             */

            hideOnClick     : false,
            
            initComponent   : function() {
                Ext.ux.menu.CheckItemIcon.superclass.initComponent.call(this);
            },
			
            // private
            onRender        : function(container, position) {
                this.itemTpl = new Ext.XTemplate(
                        /**/
                        '<a id="{[Ext.util.Format.htmlEncode(values.id)]}" class="{[Ext.util.Format.htmlEncode(values.cls)]}" hidefocus="true" unselectable="on" href="{[Ext.util.Format.htmlEncode(values.href)]}"',
                        /**/
                        '<tpl if="hrefTarget">',
                        /**/
                        ' target="{[Ext.util.Format.htmlEncode(values.hrefTarget)]}"',
                        /**/
                        '</tpl>',
                        /**/
                        '>',
                        /**/
                        '<img src="{[Ext.util.Format.htmlEncode(values.icon)]}" class="x-menu-item-icon {[Ext.util.Format.htmlEncode(values.iconCls)]}"/>',
                        /**/
                        '<span class="x-menu-item-text">',
                        /**/
                        '<div class="viz-menu-checkitem-icon {[Ext.util.Format.htmlEncode(values.textIconCls)]}"></div>',
                        /**/
                        '<div class="viz-menu-checkitem-icon-text">{[Ext.util.Format.htmlEncode(values.text)]}</div>',
                        /**/
                        '</span>',
                        /**/
                        '</a>');
                Ext.ux.menu.CheckItemIcon.superclass.onRender.call(this, container, position);
            },

            /**
             * Update the templates arguments.
             * @return {}
             */
            getTemplateArgs : function() {
                var retVal = Ext.ux.menu.CheckItemIcon.superclass.getTemplateArgs.apply(this, arguments);
                retVal.textIconCls = this.textIconCls;
                return retVal;
            }

        });
Ext.reg('menucheckitemicon', Ext.ux.menu.CheckItemIcon);
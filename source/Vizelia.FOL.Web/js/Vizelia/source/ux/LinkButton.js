﻿/**
 * @class Ext.LinkButton
 * @extends Ext.Button A Button which encapsulates an &lt;a> element to enable navigation, or downloading of files.
 * @constructor Creates a new LinkButton
 */
Ext.LinkButton = Ext.extend(Ext.Button, {
            template       : new Ext.Template(
                    /**/
                    '<table cellspacing="0" class="x-btn {3}"><tbody class="{1}">',
                    /**/
                    '<tr>',
                    /**/
                    '<td>',
                    /**/
                    '<a style="display:block" " class="x-form-item">{0}</a>',
                    /**/
                    '</td>',
                    /**/
                    '</tr>',
                    /**/
                    '</tbody></table>').compile(),

            buttonSelector : 'a:first',

            /**
             * @cfg String href The URL to create a link for.
             */
            /**
             * @cfg String target The target for the &lt;a> element.
             */
            /**
             * @cfg Object A set of parameters which are always passed to the URL specified in the href
             */
            baseParams     : {},

            // private
            params         : {},

            onClick        : function(e) {
                if (e.button != 0) {
                    return;
                }
                if (this.disabled) {
                    this.stopEvent(e);
                }
                else {
                    if (this.fireEvent("click", this, e) !== false) {
                        if (this.handler) {
                            this.handler.call(this.scope || this, this, e);
                        }
                    }
                }
            }
        });
Ext.reg('linkbutton', Ext.LinkButton);
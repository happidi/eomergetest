﻿// This code was taked from http://stackoverflow.com/a/6691294/601179.
// It was modified a bit to support <iframe> thus the second parameter-window.
Viz.util.pasteHtmlAtCaret = function (html, win) {
    var theWindow = win || window;
    var sel, range;
    if (theWindow.getSelection) {
        // IE >=9 and non-IE
        sel = theWindow.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();

            var el = theWindow.document.createElement("div");
            el.innerHTML = html;
            var frag = theWindow.document.createDocumentFragment(), node, lastNode;
            while ((node = el.firstChild)) {
                lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);

            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    } else if (theWindow.document.selection && theWindow.document.selection.type != "Control") {
        // IE < 9
        theWindow.document.selection.createRange().pasteHTML(html);
    }
}
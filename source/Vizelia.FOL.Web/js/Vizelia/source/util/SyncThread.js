﻿Ext.namespace('Viz.util');

/**
 * Enables monitoring of several asynchronous action with a single end point.
 * <p>
 * Usage:
 * </p>
 * 
 * <pre><code>
 * var sync = new Viz.util.SyncThread(); 
 * // or
 * var sync = new Viz.util.SyncThread({items : [&quot;loading 1&quot; , &quot;loading 2&quot;]}); 
 * sync.onReady(EndPoint);
 * sync.startThread(&quot;loading 1&quot;);
 * sync.startThread(&quot;loading 2&quot;);
 * ...
 * sync.endThread(&quot;loading 1&quot;);
 * ..
 * sync.endThread(&quot;loading 2&quot;);
 * 
 * </code></pre>
 * 
 * @class Viz.util.SyncThread
 * @extends Ext.util.Observable
 */
Viz.util.SyncThread = Ext.extend(Ext.util.Observable, {

            /**
             * @cfg {Boolean} isReady Status of the SyncThread.
             */
            isReady     : false,
            /**
             * Ctor.
             * @param {Object} config The configuration options
             */
            constructor : function(config) {
                config = config || {};
                defaultConfig = {};
                forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

                Ext.applyIf(config, this);
               
                // TODO construction
                if (!config.items)
                    config.items = [];
                    
                Viz.util.SyncThread.superclass.constructor.call(this, config);
                 this.addEvents({
                            'ready' : true
                        });
            },

            /**
             * Starts a thread.
             * @param {String} name Name of the thread.
             */
            startThread : function(name) {
                if (this.isReady)
                    throw "Cannot start a new thread on a Viz.SyncThread that is already ready";
                this.items.push(item);
            },

            /**
             * Ends a thread.
             * @param {String} name Name of the thread.
             */
            endThread   : function(name) {
                try {
                    this.items.remove(item);
                }
                catch (e) {
                }
                if (this.items.length === 0) {
                    this.afterLoad();
                }
            },

            // private
            afterLoad   : function() {
                this.fireEvent('ready', this);
                this.isReady = true;
            },

            /**
             * The function to execute when all threads have terminated.
             * @param {Function} fn
             * @param {Object} scope
             */
            onReady     : function(fn, scope) {
                if (!this.isReady) {
                    this.on('ready', fn, scope);
                }
                else {
                    fn.call(scope, this);
                }
            }
        });

Ext.reg('vizSyncThread', Viz.util.SyncThread);

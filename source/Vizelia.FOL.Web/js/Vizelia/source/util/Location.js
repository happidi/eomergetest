﻿Ext.namespace('Viz.util.Location');

/**
 * Returns the LongPath from a Spatial tree node because Site, Building, BuildingStorey doesnt have the LocationPath attribute.
 * @param {Ext.tree.TreeNode} node
 */
Viz.util.Location.getLongPathFromNode = function(node) {
    var retVal = node.getPath('text').replace(node.ownerTree.root.text + node.ownerTree.pathSeparator, '');
    return retVal;
}

/**
 * Return the site level from a Chart Localisation store record.
 * @param {} data
 * @return {}
 */
Viz.util.Location.getSiteLevel = function(data) {
    return data.Id.replace('BySite', '') - 0
}
/**
 * Return a Location entity from a spatial tree node (Site, Building etc...)
 * @param {} node
 * @return {}
 */
Viz.util.Location.getLocationEntityFromSpatialNode = function(node) {
    var retVal = {
        KeyLocation : node.id,
        Name        : node.text,
        TypeName    : Vizelia.FOL.BusinessEntities.HierarchySpatialTypeName[node.attributes.entity.IfcType],
        IconCls     : node.attributes.iconCls,
        LongPath    : Viz.util.Location.getLongPathFromNode(node)
    };
    return retVal;

}
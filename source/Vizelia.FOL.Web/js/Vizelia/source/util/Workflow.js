﻿Ext.namespace('Viz.util');
/**
 * A workflow helper class
 * @class Viz.util.Workflow
 * @extends Ext.util.Observable
 */
Viz.util.Workflow = Ext.extend(Ext.util.Observable, {
            /**
             * The ctor.
             */
            constructor             : function(config) {
                // this.comp = config.comp;
                // this.listeners = config.listeners;
                this.addEvents(
                        /**
                         * @event beforeaction Fires before an action is called on a worflow entity.
                         */
                        'beforeaction',
                        /**
                         * @event afteractionsuccess Fires after an action is successfully called on a workflow entity.
                         */
                        'afteractionsuccess',
                        /**
                         * @event afteractionfailure Fires after an action is called unsuccessfully on a workflow entity.
                         */
                        'afteractionfailure',
                        /**
                         * @event success Fires after the component retreives the possible actions on a workflow entity.
                         */
                        'success',
                        /**
                         * @event failure Fires if the component cannot retreive the possible actions on a workflow entity.
                         */
                        'failure');
                var defaultConfig = {
                    /**
                     * @cfg {Boolean} includeClientActions <tt>true</tt> to include client actions, <tt>false</tt> otherwise.
                     */
                    includeClientActions : true
                };
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                // this.includeClientActions = true;
                Ext.apply(this, config);
                Viz.util.Workflow.superclass.constructor.call(this, config);
            },

            /**
             * Builds a waiting menu.
             * @return {Ext.menu.Menu} The waiting menu.
             */
            buildWaitContextMenu    : function() {
                var ctxMenu = new Ext.menu.Menu({
                            items : [{
                                        text    : "...",
                                        scope   : this,
                                        iconCls : 'loading-indicator'
                                    }]
                        });
                ctxMenu.on("hide", function(menu) {
                            menu.destroy();
                        });
                return ctxMenu;
            },

            /**
             * Gets the workflow entity.
             * @param {Object} comp The component (grid or form) exposing the workflow entity.
             * @return {Object} The workflow entity.
             */
            getWorkflowEntity       : function(comp) {
                var item = {};
                if (comp.isXType('form')) {
                    var form = comp.getForm();
                    try {
                        item = form.getFieldValues();
                    }
                    catch (e) {
                        ;
                    }
                    var entity = comp.entity || {};
                    Ext.applyIf(item, entity);
                }
                else if (comp.isXType('grid')) {
                    var rec = comp.getSelectionModel().getSelected();
                    item = Viz.clone(rec.json);
                }
                return item;
            },

            /**
             * Retreives the list of possible actions on a workflow entity.
             * @param {Function} serviceHandler The service handler for retreiving the list of possible actions.
             */
            getStateMachineActions  : function(serviceHandler) {
                if (serviceHandler == null)
                    serviceHandler = Viz.Services.ServiceDeskWCF.Workflow_GetStateMachineEventsTransitions;
                var item = this.getWorkflowEntity(this.comp);
                if (item != null) {
                    serviceHandler.call(this.comp, {
                                entity  : item,
                                success : function(values) {
                                    var actions = this.buildWorkflowActions(values);
                                    this.fireEvent("success", this, actions);

                                },
                                failure : function() {
                                    this.fireEvent("failure", this);
                                },
                                scope   : this
                            });
                }

            },

            /**
             * Builds the list of actions
             * @param {Object} values
             * @return {Object}
             */
            buildWorkflowActions    : function(values) {
                var actions = [];
                var item = Ext.each(values, function(/* Viz.BusinessEntity.WorkflowActionElement */listElement) {
                            var serviceHandlerName;
                            var serviceHandlerParams;

                            switch (listElement.Id) {
                                case 'workflow_update_definition' :
                                    serviceHandlerName = "Workflow_UpdateInstanceDefinition";
                                    break;

                                case 'workflow_restart' :
                                    serviceHandlerName = "Workflow_Restart";
                                    break;

                                default :
                                    serviceHandlerName = "Workflow_RaiseEvent";

                            }
                            var handler = function() {
                                var comp = this.comp;
                                var item = this.getWorkflowEntity(comp);
                                serviceHandlerParams = {
                                    entity     : null,
                                    instanceId : item.InstanceId,
                                    eventName: listElement.Value,
                                    workflowQualifiedName: listElement.AssemblyQualifiedName
                                };

                                Ext.apply(serviceHandlerParams, {
                                            entity  : item,
                                            scope   : this,
                                            success : function(result) {
                                                if (result.msg != null) {
                                                    Ext.Msg.show({
                                                                width   : 400,
                                                                title   : $lang("error_ajax"),
                                                                msg     : result.msg,
                                                                buttons : Ext.Msg.OK,
                                                                icon    : Ext.MessageBox.ERROR
                                                            });
                                                    this.fireEvent("afteractionfailure", comp);

                                                }
                                                else
                                                    this.fireEvent("afteractionsuccess", comp);

                                            }
                                        });

                                var action = Viz.Services.ServiceDeskWCF[serviceHandlerName].createInterceptor(function() {
                                            return this.fireEvent("beforeaction", comp);

                                        }, this);

                                if (!Ext.isEmpty(listElement.PreAction)) {
                                    // we clear the subscriptions to avoid multiple running into the same handler.
                                    Viz.util.MessageBusMgr.clearsubscription("EndPreActionWorkflow" + item.InstanceId);
                                    // we suscribe to the EndPreActionWorkflow
                                    Viz.util.MessageBusMgr.subscribe("EndPreActionWorkflow" + item.InstanceId, action, comp, {
                                                single : true
                                            });

                                    // we call the prehandler function in the scope of the grid. The PreAction params are on this.PreActionParams.
                                    this.PreActionParams = serviceHandlerParams;
                                    this.listElement = listElement;

                                    // Ext.applyIf(this, Viz.util.Workflow);
                                    var callPreAction = listElement.PreAction.replace('(', '.call(this,');
                                    (new Function("with(this) {" + listElement.PreAction + "}")).call(this);
                                }
                                else {
                                    action(serviceHandlerParams);
                                }
                            };

                            var actionItem = {
                                text    : listElement.Label,
                                scope   : this,
                                iconCls : listElement.IconCls,
                                handler : handler
                            }
                            actions.push(actionItem);

                        }, this);
                if (this.includeClientActions)
                    actions = actions.concat(this.buildClientActions());

                return actions;

            },

            /**
             * Builds the list of client actions.
             * @return {Object}
             */
            buildClientActions      : function() {
                var clientActions = [];
                var item = this.getWorkflowEntity(this.comp);
                clientActions.push({
                            text    : $lang('msg_workflow_openviewer'),
                            scope   : this,
                            iconCls : 'viz-icon-small-workflow',
                            handler : this.openWorkflowViewer.createDelegate(this, [item])
                        });
                return clientActions;

            },

            /**
             * Opens the workflow viewer.
             * @param {Viz.BusinessEntity.ActionRequest} item
             */
            openWorkflowViewer      : function() {
                var comp = this.comp;
                var item = this.getWorkflowEntity(comp);
                var win = new Ext.Window({
                            title       : $lang('msg_workflow_viewer'),
                            iconCls     : 'viz-icon-small-workflow',
                            maximizable : true,
                            width       : 600,
                            height      : 600,
                            layout      : 'fit',
                            items       : [{
                                        xtype  : 'vizWorkflowViewer',
                                        entity : item
                                    }]
                        });
                win.show();
            },

            /**
             * Assign a user to an ActionRequest entity.
             * @private
             */
            ActionRequestAssignUser : function() {
                var roles = [].splice.call(arguments, 0);
                this.AssignUser(roles, "KeyApprover");
            },

            /**
             * Assign a user to a Task.
             * @private
             */
            TaskAssignUser          : function() {
                var roles = [];
                this.AssignUser(roles, "KeyActor");
            },

            /**
             * Assign a user to an workflow entity.
             * @private
             * @param {Array} roles An array of authorized roles.
             * @param {String} actorField The name of the field on the entity that will be updated.
             */
            AssignUser              : function(roles, actorField) {
                var serviceHandlerParams = this.PreActionParams;
                delete this.PreActionParams;
                var item = serviceHandlerParams.entity;

                var _gridId = Ext.id();

                var win = new Ext.Window({
                            title   : $lang('msg_user_assign'),
                            width   : 500,
                            height  : 350,
                            iconCls : 'viz-icon-small-user',
                            layout  : 'fit',
                            items   : [{
                                        id        : _gridId,
                                        xtype     : 'vizGridUserFiltered',
                                        roles     : roles,
                                        locations : [item.KeyLocation]
                                    }],
                            buttons : [{
                                        text    : Ext.MessageBox.buttonText.ok,
                                        handler : function() {
                                            var gridUser = Ext.getCmp(_gridId);
                                            var recordUser = gridUser.getSelectionModel().getSelected();
                                            if (recordUser) {
                                                var keyUser = recordUser.json.ProviderUserKey.KeyUser;
                                                item[actorField] = keyUser;
                                                this.comp.publishMessage("EndPreActionWorkflow" + item.InstanceId, [serviceHandlerParams]);
                                                win.close();
                                            }
                                        },
                                        scope   : this
                                    }, {
                                        text    : Ext.MessageBox.buttonText.cancel,
                                        handler : function() {

                                            win.close();
                                        },
                                        scope   : this
                                    }]
                        });
                win.show();
            }

        });

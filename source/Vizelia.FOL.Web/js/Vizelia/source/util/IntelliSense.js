﻿Ext.namespace('Viz.util');
/**
 * A intelliSense helper class
 * @class Viz.util.IntelliSense
 * @extends Ext.util.Observable
 */
Viz.util.IntelliSense = Ext.extend(Ext.util.Observable, {
            startChar            : '@',
            delimiterChar        : '.',

            serviceHandler       : Ext.emptyFn,
            serviceParams        : null,

            menuHandler          : Ext.emptyFn,
            menuHandlerScope     : null,
            /**
             * The ctor.
             */
            constructor          : function(config) {
                this.addEvents(
                        /**
                         * @event success Fires after the component retreives the possible actions on a workflow entity.
                         */
                        'success',
                        /**
                         * @event failure Fires if the component cannot retreive the possible actions on a workflow entity.
                         */
                        'failure');
                var defaultConfig = {};
                var forcedConfig = {};
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.apply(this, config);
                Viz.util.IntelliSense.superclass.constructor.call(this, config);
            },

            /**
             * Builds a waiting menu.
             * @return {Ext.menu.Menu} The waiting menu.
             */
            buildWaitContextMenu : function() {
                var ctxMenu = new Ext.menu.Menu({
                            items : [{
                                        text    : "...",
                                        scope   : this,
                                        iconCls : 'loading-indicator'
                                    }]
                        });
                ctxMenu.on("hide", function(menu) {
                            menu.destroy();
                        });
                return ctxMenu;
            },

            /**
             * Retreives the menu items from service side.
             * @param {Function} serviceHandler The service handler for retreiving the list of possible actions.
             */
            getMenu              : function(config) {
                config = config || {};
                if (this.serviceHandler != null) {
                    Ext.apply(this.serviceParams, config);
                    this.serviceHandler.call(this, Ext.applyIf(this.serviceParams, {
                                        success : function(values) {

                                            var actions = this.buildMenu(values);
                                            this.fireEvent("success", this, values);
                                        },
                                        failure : function() {
                                            this.fireEvent("failure", this);
                                        },
                                        scope   : this
                                    }));
                }

            },

            /**
             * Recursive function that builds the menu items.
             * @param {Viz.BusinessEntity.TreeNode[]} values
             * @param {string} path should be null for the first call
             * @return {}
             */
            buildMenu            : function(values, path) {
                var localPath;
                var retVal = [];
                Ext.each(values, function(/* Viz.BusinessEntity.TreeNode */node) {

                            if (node.Key == "Formula")
                                localPath = "##";
                            else if (node.Key == "LocalizedText")
                                localPath = "%%";
                            else if (path == "@FormulaParent")
                                localPath = node.Key;
                            else if (node.text == 'ModelDataScriptPython')
                                localPath = null;
                            else if (path == null)
                                localPath = this.startChar + (node.Key || node.text);
                            else
                                localPath = path + this.delimiterChar + (node.Key || node.text);
                            var menuItem = {
                                key         : node.Key,
                                text        : node.text,
                                iconCls     : node.iconCls,
                                handler     : node.leaf ? this.menuHandler.createDelegate(this.menuHandlerScope, [localPath]) : null,
                                scope       : this.menuHandlerScope,
                                hideOnClick : node.leaf
                            };
                            if (node.Key == "SEPARATOR") {
                                menuItem = "-";
                            }
                            if (!node.leaf) {
                                var items = this.buildMenu(node.children, localPath);
                                menuItem.menu = {
                                    items           : items,
                                    maxHeight       : 600,
                                    enableScrolling : true
                                };
                            }

                            retVal.push(menuItem);
                        }, this)

                return retVal;

            },

            createMenu           : function(nodes) {
                var actions = this.buildMenu(nodes);
                if (actions && Ext.isArray(actions) && actions.length > 0) {

                    var ctxMenu = new Ext.menu.Menu({
                                items           : actions,
                                maxHeight       : 600,
                                enableScrolling : true
                            });
                    return ctxMenu;
                }
                else
                    return null;
            },

            /**
             * Shows the menu.
             */
            showMenu             : function(nodes, xy) {
                var ctxMenu = this.createMenu(nodes);
                if (ctxMenu) {
                    ctxMenu.on("hide", function(menu) {
                                menu.destroy();
                            });
                    ctxMenu.showAt(xy);
                }
            }

        });

﻿Ext.namespace('Viz.util.Fullscreen');

/**
 * Requests the browser to enter fullscreen mode
 */
Viz.util.Fullscreen.toggle = function() {
    if ((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreenElement && !document.webkitFullScreenElement)) { // current working methods
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        }
        else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        }
        else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    }
    else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        }
        else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}
/**
 * Check if fullscreen toggle is supported
 */
Viz.util.Fullscreen.isSupported = function() {
    // Exit full screen
    return Ext.isFunction(document.documentElement.requestFullscreen) || Ext.isFunction(document.documentElement.mozRequestFullScreen) || Ext.isFunction(document.documentElement.webkitRequestFullScreen) || Ext.isFunction(document.exitFullscreen) || Ext.isFunction(document.mozCancelFullScreen) || Ext.isFunction(document.webkitCancelFullScreen);

}
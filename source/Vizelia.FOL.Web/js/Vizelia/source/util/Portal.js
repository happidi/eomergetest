﻿Ext.namespace('Viz.util.Portal');

/**
 * Build the differents tabs based on the PortalColumn entity.
 * @param {Viz.BusinessEntity.PortalColumn} entity
 */
Viz.util.Portal.getPortalColumnConfig = function(entity, totalFlex, insertIndex, total) {
    // default inner padding for columns (left and right set to 2px)
    var padding = Viz.util.Portal.getPortalColumnPadding(insertIndex, total);
    var column = {
        columnWidth : entity.Flex / totalFlex,
        style       : 'padding :' + padding,
        entity      : entity,
        items       : []
    };
    if (entity.Portlets && entity.Portlets.length > 0) {
        Ext.each(entity.Portlets, function(portlet) {
                    column.items.push(Viz.util.Portal.getPortletConfig(portlet));
                }, this);
    }
    return column;
};
/**
 * Return the padding style for a specific column based on its position
 * @param {} insertIndex
 * @param {} total
 * @return {}
 */
Viz.util.Portal.getPortalColumnPadding = function(insertIndex, total) {
    var style = '4px 2px 0px 2px';
    if (Ext.isNumber(insertIndex) && Ext.isNumber(total)) {
        // there is only one column
        if (total == 1) {
            style = '4px 4px 0px 4px';
        }
        // it s the first column of many so padding right is set to 2px
        else if (insertIndex == 0) {
            style = '4px 2px 0px 4px';
        }
        // it s the last column of many so padding left is set to 2px
        else if (insertIndex == total - 1) {
            style = '4px 4px 0px 2px';
        }
    }
    return style;
}
/**
 * Build the portlet based on the Portlet entity.
 * @param {Viz.BusinessEntity.Portlet} entity
 */
Viz.util.Portal.getPortletConfig = function(entity) {
    var portlet = {
        title         : entity.Title,
        entityPortlet : entity,
        collapsed     : entity.Collapsed,
        entity        : entity.Entity
    };
    if (entity.KeyEntity && entity.Entity) {

        switch (entity.TypeEntity) {
            case 'Vizelia.FOL.BusinessEntities.WeatherLocation' :
                Ext.apply(portlet, {
                            xtype : 'vizPortletWeather'
                        });
                break;
            case 'Vizelia.FOL.BusinessEntities.Chart' :
                Ext.apply(portlet, {
                            xtype : 'vizChartViewer'
                        });
                break;
            default :
                Ext.apply(portlet, {
                            xtype : 'vizPortlet' + entity.TypeEntity.replace('Vizelia.FOL.BusinessEntities.', '')
                        });
                break;
        }
    }
    return portlet;
};

/**
 * Build the differents tabs based on the PortalWindow entity.
 * @param {Viz.BusinessEntity.PortalWindow} entity
 */
Viz.util.Portal.getPortalWindowConfig = function(entity) {
    var items = [];
    if (entity && entity.Tabs.length > 0) {
        Ext.each(entity.Tabs, function(tab) {
                    items.push(Viz.util.Portal.getPortalTabConfig(tab));
                }, this);
    }
    return items;
};
/**
 * Build the differents tabs based on the PortalTab entity.
 * @param {Viz.BusinessEntity.PortalTab} entity
 */
Viz.util.Portal.getPortalTabConfig = function(entity) {
    var tab = {
        xtype  : 'vizPortalTab',
        entity : entity
    };
    return tab;
};

/**
 * Return the proper authorization string based on the portlet type.
 * @param {} type
 * @param {} fullcontrol
 * @return {}
 */
Viz.util.Portal.getPortletAuthorization = function(type, fullcontrol) {

    var retVal = '@@@@ ';
    switch (type) {
        case 'Vizelia.FOL.BusinessEntities.Chart' :
            retVal += 'Chart';
            break;
        case 'Vizelia.FOL.BusinessEntities.DrawingCanvas' :
            retVal += 'Drawing Canvas';
            break;
        case 'Vizelia.FOL.BusinessEntities.Map' :
            retVal += 'Map';
            break;
        case 'Vizelia.FOL.BusinessEntities.WeatherLocation' :
            retVal += 'Weather Location';
            break;
        case 'Vizelia.FOL.BusinessEntities.AlarmTable' :
            retVal += 'Alarm Table';
            break;
        case 'Vizelia.FOL.BusinessEntities.WebFrame' :
            retVal += 'Web Frame';
            break;
        case 'Vizelia.FOL.BusinessEntities.FlipCard' :
            retVal += 'FlipCard';
            break;
        default :
            retVal += type.replace('Vizelia.FOL.BusinessEntities.', '');
            break;
    }

    if (fullcontrol)
        retVal += ' - Full Control';

    return retVal;

}
/**
 * Return the Key attribute of a Portlet entity based on the type.
 * @param {} type
 * @return {}
 */
Viz.util.Portal.getPortletKey = function(type) {

    var retVal = 'Key';
    switch (type) {
        case 'Vizelia.FOL.BusinessEntities.Chart' :
            retVal += 'Chart';
            break;
        case 'Vizelia.FOL.BusinessEntities.DrawingCanvas' :
            retVal += 'DrawingCanvas';
            break;
        case 'Vizelia.FOL.BusinessEntities.Map' :
            retVal += 'Map';
            break;
        case 'Vizelia.FOL.BusinessEntities.WeatherLocation' :
            retVal += 'WeatherLocation';
            break;
        case 'Vizelia.FOL.BusinessEntities.AlarmTable' :
            retVal += 'AlarmTable';
            break;
        case 'Vizelia.FOL.BusinessEntities.WebFrame' :
            retVal += 'WebFrame';
            break;
        case 'Vizelia.FOL.BusinessEntities.DynamicDisplay' :
            retVal += 'DynamicDisplay';
            break;
        case 'Vizelia.FOL.BusinessEntities.FlipCard' :
            retVal += 'FlipCard';
            break;
        default :
            retVal += type.replace('Vizelia.FOL.BusinessEntities.', '');
            break;
    }
    return retVal;
}

/**
 * Open the PortletWindow for the passed entity.
 * @param {} entity
 */
Viz.util.Portal.openDisplay = function(entity) {
    var type = Viz.getEntityType(entity.__type);
    switch (type) {
        case 'Vizelia.FOL.BusinessEntities.Chart' :
            Viz.grid.Chart.openChartDisplay(entity);
            break;
        case 'Vizelia.FOL.BusinessEntities.DrawingCanvas' :
            Viz.grid.DrawingCanvas.openDrawingCanvasDisplay(entity);
            break;
        case 'Vizelia.FOL.BusinessEntities.Map' :
            Viz.grid.Map.openMapDisplay(entity);
            break;
        case 'Vizelia.FOL.BusinessEntities.WeatherLocation' :
            Viz.grid.WeatherLocation.openWeatherLocationDisplay(entity);
            break;
        case 'Vizelia.FOL.BusinessEntities.AlarmTable' :
            Viz.grid.AlarmTable.openAlarmTableDisplay(entity);
            break;
        case 'Vizelia.FOL.BusinessEntities.WebFrame' :
            Viz.grid.WebFrame.openWebFrameDisplay(entity);
            break;
        case 'Vizelia.FOL.BusinessEntities.FlipCard' :
            Viz.grid.FlipCard.openFlipCardDisplay(entity)
    }
}

/**
 * Generic function used in every "Portlet" grid on render to load the store empty.
 */
Viz.util.Portal.gridRender = function() {
    this.store.load({
                params : {
                    start : 0,
                    limit : -1
                }
            });
}

/**
 * Generic function used every "Portlet" grid to preload the store with a specific isFavorite filter set to true.
 */
Viz.util.Portal.gridFilterAvailable = function(preferIsFavoriteFilter) {
    // we reset the paging parameter
    if (this.store.lastOptions.params) {
        this.store.lastOptions.params.limit = this.pageSize;
    }
    // we make the list show only last 2 months data.
    var isFavoriteFilter = preferIsFavoriteFilter === true || !this.savedView ? this.filters.getFilter("IsFavorite") : null;

    if (isFavoriteFilter) {
        isFavoriteFilter.setValue({
                    equals : true
                });
        isFavoriteFilter.setActive(true);
    }
    else {
        this.store.load();
    }
}

/**
 * Generic function used every "Portlet" grid to set the state of the favorite button;
 * @param {} buttonId
 * @param {} state
 */
Viz.util.Portal.toggleFavoriteButton = function(buttonId, state) {
    var button = Ext.getCmp(buttonId);
    if (button) {
        button.toggle(state, true);
    }
}

/**
 * Generic function used every "Portlet" grid that handle the listeners for the toggling of the button
 * @param {} buttonId
 * @param {} state
 */
Viz.util.Portal.onToggleFavoriteButton = function(button, pressed) {
    if (!pressed) {
        this.onGridFilterClear();
    }
    else {
        Viz.util.Portal.gridFilterAvailable.createDelegate(this, [true]).call();
    }

    this.reloadStore();
}

/**
 * Display a grid with the list of portlets for a specific entity (Used in Chart grid, etc...)
 */
Viz.util.Portal.displayPortletGridFromEntity = function() {
    var selections = this.getSelectionModel().getSelections();
    if (selections.length == 0)
        return;
    var record = this.getSelectionModel().getSelected();
    if (record) {
        var entity = record.json;
        var KeyEntity = Viz.getEntityKey(entity);
        var TypeEntity = Viz.getEntityType(entity);

        var win = new Ext.Window({
                    title       : entity.Title + ' - ' + $lang('msg_portlets'),
                    iconCls     : 'viz-icon-small-portlet',
                    maximizable : true,
                    width       : 500,
                    height      : 300,
                    layout      : 'fit',
                    items       : [{
                                xtype    : 'vizGridPortlet',
                                readonly : true,
                                store    : new Viz.store.EntityPortlet({
                                            KeyEntity  : KeyEntity,
                                            TypeEntity : TypeEntity
                                        })
                            }]
                });
        win.show();

    }

}

/**
 * Localise a portal (tab, portlet etc...) title
 * @param {string} title
 * @return {}
 */
Viz.util.Portal.localizeTitle = function(title) {
    var re = /%(\w*)%/;
    var array = [];
    var match = {};
    while (match = re.exec(title)) {
        title = title.replace(match[0], $lang(match[1]));
    }

    /*
     * if (!Ext.isEmpty(title) && title.length > 0) { if (title.startsWith('%') && title.endsWith('%')) { title = $lang(title.substring(1, title.length - 1)); } }
     */
    return title;

}

Viz.util.Portal.getCssFromImage = function(KeyImage, width, height) {
    if (Ext.isEmpty(KeyImage) || KeyImage == 'null')
        return null;

    var ruleName = 'viz-icon-dynamicimage-' + KeyImage + '-' + width + '-' + height;
    var url = Viz.Services.BuildUrlStream("Energy.svc", "DynamicDisplayImage_GetStreamImage", {
                Key    : KeyImage,
                width  : width,
                height : height
            });

    if (Ext.util.CSS.getArrayMatch('.' + ruleName).length == 0) {
        var css = '.' + ruleName + " {";
        css += "  background-image: url('" + url + "') !important;";
        css += "  background-repeat: no-repeat;";
        css += "}";

        Ext.util.CSS.createStyleSheet(css);
    }
    return ruleName;
}

Ext.namespace('Viz.util.Desktop');

Viz.util.Desktop.getBackgroundUrl = function(KeyImage) {
    if (Ext.isEmpty(KeyImage) || KeyImage == 'null')
        return null;

    var url = '../../' + Viz.Services.BuildUrlStream("Energy.svc", "DynamicDisplayImage_GetStreamImage", {
                Key    : KeyImage,
                width  : 0,
                height : 0
            });
    return url;
}

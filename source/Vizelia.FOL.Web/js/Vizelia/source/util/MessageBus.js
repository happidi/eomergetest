Ext.namespace("Viz.util");
/**
 * @class Viz.util.MessageBus
 * @extends Ext.util.Observable Do not create this class directly but use Viz.util.MessageBusMgr instead
 */
Viz.util.MessageBus = Ext.extend(Ext.util.Observable, {
            constructor : function() {
                this.addEvents({});
                Viz.util.MessageBus.superclass.constructor.apply(this, arguments);
            },

            /**
             * Fires the specified event with the passed parameters (minus the event name).
             * @param {String} eventName
             * @param {Object...} args Variable number of parameters are passed to handlers
             * @return {Boolean} returns false if any of the handlers return false otherwise it returns true
             */
            publish     : function(eventName) {
                var arr = arguments;
                this.events[eventName] || (this.addEvents(eventName));
                return this.fireEvent.apply(this, arguments);
            },

            /**
             * Appends an subscriber
             * @param {String} eventName The type of event to listen for
             * @param {Function} fn The handler function the event invokes
             * @param {Object} scope (optional) The scope in which to execute the handler function. The handler function's "this" context.
             * @param {Object} o (optional) An object containing handler configuration properties. This may contain any of the following properties:
             * <ul>
             * <li><b>scope</b> : Object
             * <p class="sub-desc">
             * The scope in which to execute the handler function. The handler function's "this" context.
             * </p>
             * </li>
             * <li><b>delay</b> : Number
             * <p class="sub-desc">
             * The number of milliseconds to delay the invocation of the handler after the event fires.
             * </p>
             * </li>
             * <li><b>single</b> : Boolean
             * <p class="sub-desc">
             * True to add a handler to handle just the next firing of the event, and then remove itself.
             * </p>
             * </li>
             * <li><b>buffer</b> : Number
             * <p class="sub-desc">
             * Causes the handler to be scheduled to run in an {@link Ext.util.DelayedTask} delayed by the specified number of milliseconds. If the event fires again within that time, the original handler is <em>not</em> invoked, but the new handler is scheduled in its place.
             * </p>
             * </li>
             * </ul>
             * <br>
             */
            subscribe   : function(eventName, fn, scope, o) {
                this.on(eventName, fn, scope, o);
            },

            unsubscribe : function(eventName, fn, scope) {
                this.removeListener(eventName, fn, scope);
            },

            /**
             * Clear any subscription for the specified event.
             * @param {String} eventName
             */
            clearsubscription       : function(eventName) {
                var ce = this.events[eventName.toLowerCase()];

                if (Ext.isObject(ce)) {
                    ce.clearListeners();
                }
            }
        });

/**
 * @class Viz.util.MessageBusMgr
 * @extends Ext.util.Observable
 * @singleton Use this class to publish and subscibe to events
 */
Viz.util.MessageBusMgr = new Viz.util.MessageBus();

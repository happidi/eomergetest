﻿Ext.namespace('Viz.util.Chart');

/**
 * Return the number format to use based on the Chart entity.
 * @param {Viz.BusinessEntity.Chart} entity
 */
Viz.util.Chart.getNumberFormat = function(entity) {
    var retVal = "0,000";
    if (Ext.form.NumberField.prototype.decimalSeparator != ".")
        retVal = "0.000";

    if (entity && entity.DisplayDecimalPrecision > 0) {
        retVal += Ext.form.NumberField.prototype.decimalSeparator;
        for (var i = 0; i < entity.DisplayDecimalPrecision; i++) {
            retVal += "0";
        }
    }
    if (Ext.form.NumberField.prototype.decimalSeparator != ".")
        return retVal + '/i';
    else
        return retVal;
}
/**
 * Returns the current TimeZoneId
 * @param {} callback
 */
Viz.util.Chart.getTimeZoneId = function(callback) {
    var store = new Viz.store.TimeZone({
                autoLoad : false
            });
    store.on('load', function(store) {
                var timezoneid = Viz.App.Global.User.Preferences.TimeZoneId;
                if (!Ext.isEmpty(timezoneid)) {
                    var idx = store.findExact("Id", timezoneid);
                }
                else {
                    var v = "" + (-new Date().getTimezoneOffset());
                    idx = store.findExact("Value", v);
                }
                if (idx >= 0) {
                    var retVal = store.getAt(idx).data['Id'];
                }
                if (Ext.isFunction(callback))
                    callback(retVal);
            }, this);
    store.load();
}

/**
 * Returns the background [0,0] color.
 * @param {dom} img : the dom image
 * @return {String}
 */
Viz.util.Chart.getBackgroundColor = function(img) {
    try {
        var canvas = $('<canvas/>').css({
                    width  : img.width + 'px',
                    height : img.height + 'px'
                })[0];
        canvas.getContext('2d').drawImage(img, 0, 0, img.width, img.height);
        var pixelData = canvas.getContext('2d').getImageData(0, 0, 1, 1).data;
        var retVal = new Viz.RGBColor(String.format('rgb({0},{1},{2})', pixelData[0], pixelData[1], pixelData[2])).toHex();
        if (retVal == '000000')
            return 'transparent';
        return '#' + retVal;
    }
    catch (e) {
    }
    return 'transparent';
}

/**
 * Cancel a started webrequest
 * @param {} webRequest
 */
Viz.util.Chart.cancelRequest = function(webRequest) {
    try {
        if (webRequest && webRequest.conn) {
            var executor = webRequest.conn;
            if (executor.readyState < 4) {
                executor.abort();
            }
        }
    }
    catch (e) {
        ;
    }
}

/**
 * Return the timerange menu config. Used in chart menu and in portal tab menu.
 * @param {} entity
 * @param {} scope
 * @param {} startDateMenuItemId
 * @param {} endDateMenuItemId
 * @return {}
 */
Viz.util.Chart.getTimeRangeMenu = function(entity, scope, startDateMenuItemId, endDateMenuItemId) {
    return [{
                text    : (entity && entity.DynamicTimeScaleEnabled == false) ? $lang('msg_chart_startdate').toBold() : $lang('msg_chart_startdate'),
                id      : startDateMenuItemId,
                iconCls : 'viz-icon-small-calendar',
                menu    : {
                    hideOnClick : false,
                    xtype       : 'datemenu',
                    handler     : scope.onChartUpdateStartDate,
                    hideOnClick : false,
                    value       : entity ? entity.StartDate : null,
                    scope       : scope
                }
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled == false) ? $lang('msg_chart_enddate').toBold() : $lang('msg_chart_enddate'),
                id      : endDateMenuItemId,
                iconCls : 'viz-icon-small-calendar',
                menu    : {
                    hideOnClick : false,
                    xtype       : 'datemenu',
                    handler     : scope.onChartUpdateEndDate,
                    hideOnClick : false,
                    value       : entity ? entity.EndDate : null,
                    scope       : scope
                }
            }, '-', {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days && entity.DynamicTimeScaleCalendar && !entity.DynamicTimeScaleCompleteInterval && !entity.DynamicTimeScaleCompleteInterval) ? $lang("msg_chart_dynamic_timescale_today").toBold() : $lang("msg_chart_dynamic_timescale_today"),
                iconCls : 'viz-icon-small-chart-timeinterval-day',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days && entity.DynamicTimeScaleCalendar && entity.DynamicTimeScaleCompleteInterval && !entity.DynamicTimeScaleCompleteInterval) ? $lang("msg_chart_dynamic_timescale_yesterday").toBold() : $lang("msg_chart_dynamic_timescale_yesterday"),
                iconCls : 'viz-icon-small-chart-timeinterval-day',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true, true, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Weeks && entity.DynamicTimeScaleCalendar && !entity.DynamicTimeScaleCompleteInterval) ? $lang("msg_chart_dynamic_timescale_current_week").toBold() : $lang("msg_chart_dynamic_timescale_current_week"),
                iconCls : 'viz-icon-small-chart-timeinterval-week',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Weeks, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months && entity.DynamicTimeScaleCalendar && !entity.DynamicTimeScaleCompleteInterval) ? $lang("msg_chart_dynamic_timescale_current_month").toBold() : $lang("msg_chart_dynamic_timescale_current_month"),
                iconCls : 'viz-icon-small-chart-timeinterval-month',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months && entity.DynamicTimeScaleCalendar && entity.DynamicTimeScaleCompleteInterval) ? $lang("msg_chart_dynamic_timescale_previous_month").toBold() : $lang("msg_chart_dynamic_timescale_previous_month"),
                iconCls : 'viz-icon-small-chart-timeinterval-month',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true, true, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, false);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Years && entity.DynamicTimeScaleCalendar && !entity.DynamicTimeScaleCalendarEndToNow) ? $lang("msg_chart_dynamic_timescale_current_year").toBold() : $lang("msg_chart_dynamic_timescale_current_year"),
                iconCls : 'viz-icon-small-chart-timeinterval-year',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Years, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true);
                },
                scope   : scope
            }, '-', {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months && entity.DynamicTimeScaleCalendar && entity.DynamicTimeScaleCalendarEndToNow) ? $lang("msg_chart_dynamic_timescale_current_mtd").toBold() : $lang("msg_chart_dynamic_timescale_current_mtd"),
                iconCls : 'viz-icon-small-chart-timeinterval-month',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    // frequency, interval, iscalendar, completeinterval, timeinterval, endtonow
                    scope.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Years && entity.DynamicTimeScaleCalendar && entity.DynamicTimeScaleCalendarEndToNow) ? $lang("msg_chart_dynamic_timescale_current_ytd").toBold() : $lang("msg_chart_dynamic_timescale_current_ytd"),
                iconCls : 'viz-icon-small-chart-timeinterval-year',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    // frequency, interval, iscalendar, completeinterval, timeinterval, endtonow
                    scope.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Years, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true);
                },
                scope   : scope
            }, '-', {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours && !entity.DynamicTimeScaleCalendar) ? $lang("msg_chart_dynamic_timescale_last_hour").toBold() : $lang("msg_chart_dynamic_timescale_last_hour"),
                iconCls : 'viz-icon-small-chart-timeinterval-hour',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Minutes, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days && !entity.DynamicTimeScaleCalendar) ? $lang("msg_chart_dynamic_timescale_last_24_hours").toBold() : $lang("msg_chart_dynamic_timescale_last_24_hours"),
                iconCls : 'viz-icon-small-chart-timeinterval-day',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(24, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 3 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days && !entity.DynamicTimeScaleCalendar) ? $lang("msg_chart_dynamic_timescale_last_3_days").toBold() : $lang("msg_chart_dynamic_timescale_last_3_days"),
                iconCls : 'viz-icon-small-chart-timeinterval-day',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(3, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Hours, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Weeks && !entity.DynamicTimeScaleCalendar) ? $lang("msg_chart_dynamic_timescale_last_7_days").toBold() : $lang("msg_chart_dynamic_timescale_last_7_days"),
                iconCls : 'viz-icon-small-chart-timeinterval-week',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(7, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 15 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days && !entity.DynamicTimeScaleCalendar) ? $lang("msg_chart_dynamic_timescale_last_15_days").toBold() : $lang("msg_chart_dynamic_timescale_last_15_days"),
                iconCls : 'viz-icon-small-chart-timeinterval-week',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(15, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 30 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days && !entity.DynamicTimeScaleCalendar) ? $lang("msg_chart_dynamic_timescale_last_30_days").toBold() : $lang("msg_chart_dynamic_timescale_last_30_days"),
                iconCls : 'viz-icon-small-chart-timeinterval-month',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(30, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 3 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months && !entity.DynamicTimeScaleCalendar) ? $lang("msg_chart_dynamic_timescale_last_3_months").toBold() : $lang("msg_chart_dynamic_timescale_last_3_months"),
                iconCls : 'viz-icon-small-chart-timeinterval-month',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(3, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 6 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months && !entity.DynamicTimeScaleCalendar) ? $lang("msg_chart_dynamic_timescale_last_6_months").toBold() : $lang("msg_chart_dynamic_timescale_last_6_months"),
                iconCls : 'viz-icon-small-chart-timeinterval-month',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(6, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 12 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months && !entity.DynamicTimeScaleCalendar) ? $lang("msg_chart_dynamic_timescale_last_12_months").toBold() : $lang("msg_chart_dynamic_timescale_last_12_months"),
                iconCls : 'viz-icon-small-chart-timeinterval-year',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(12, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == 24 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months && !entity.DynamicTimeScaleCalendar) ? $lang("msg_chart_dynamic_timescale_last_24_months").toBold() : $lang("msg_chart_dynamic_timescale_last_24_months"),
                iconCls : 'viz-icon-small-chart-timeinterval-year',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    scope.onChartUpdateDynamicTimeScale(24, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true);
                },
                scope   : scope
            }, '-', {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == -1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Weeks && entity.DynamicTimeScaleCalendar && !entity.DynamicTimeScaleCalendarEndToNow) ? $lang("msg_chart_dynamic_timescale_next_week").toBold() : $lang("msg_chart_dynamic_timescale_next_week"),
                iconCls : 'viz-icon-small-chart-timeinterval-day',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    // frequency, interval, iscalendar, completeinterval, timeinterval, endtonow
                    scope.onChartUpdateDynamicTimeScale(-1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Weeks, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, false);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == -1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months && entity.DynamicTimeScaleCalendar && !entity.DynamicTimeScaleCalendarEndToNow) ? $lang("msg_chart_dynamic_timescale_next_month").toBold() : $lang("msg_chart_dynamic_timescale_next_month"),
                iconCls : 'viz-icon-small-chart-timeinterval-month',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    // frequency, interval, iscalendar, completeinterval, timeinterval, endtonow
                    scope.onChartUpdateDynamicTimeScale(-1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Days, false);
                },
                scope   : scope
            }, {
                text    : (entity && entity.DynamicTimeScaleEnabled && entity.DynamicTimeScaleFrequency == -1 && entity.DynamicTimeScaleInterval == Vizelia.FOL.BusinessEntities.AxisTimeInterval.Years && entity.DynamicTimeScaleCalendar && !entity.DynamicTimeScaleCalendarEndToNow) ? $lang("msg_chart_dynamic_timescale_next_year").toBold() : $lang("msg_chart_dynamic_timescale_next_year"),
                iconCls : 'viz-icon-small-chart-timeinterval-year',
                handler : function(b, e) {
                    if (Ext.isFunction(scope.markDynamicTimeScaleMenuItemToRegularText))
                        scope.markDynamicTimeScaleMenuItemToRegularText(b);
                    // frequency, interval, iscalendar, completeinterval, timeinterval, endtonow
                    scope.onChartUpdateDynamicTimeScale(-1, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Years, true, false, Vizelia.FOL.BusinessEntities.AxisTimeInterval.Months, false);
                },
                scope   : scope
            }];

}

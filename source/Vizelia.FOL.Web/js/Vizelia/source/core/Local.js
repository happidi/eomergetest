﻿/*
 * Overrides GridFilters for translation.
 */
if (Ext.ux.grid && Ext.ux.grid.GridFilters) {
    Ext.apply(Ext.ux.grid.GridFilters.prototype, {
                menuFilterText : $lang('msg_filters')
            });
    Ext.apply(Ext.ux.grid.filter.DateFilter.prototype, {
                afterText  : $lang('msg_filters_filterdate_after'),
                beforeText : $lang('msg_filters_filterdate_before'),
                onText     : $lang('msg_filters_filterdate_on'),
                dateFormat : 'd/m/Y'
            });
    Ext.apply(Ext.ux.grid.filter.BooleanFilter.prototype, {
                yesText : $lang('msg_filters_filterboolean_yes'),
                noText  : $lang('msg_filters_filterboolean_no')
            });
}
if (Ext.ux.ValidationStatus) {
    Ext.apply(Ext.ux.ValidationStatus.prototype, {
                showText   : $lang('msg_validationstatus_showtext'),
                hideText   : $lang('msg_validationstatus_hidetext'),
                submitText : $lang('msg_validationstatus_submitText')
            });
}
Ext.namespace("Viz.DomQuery");

/**
 * @class Viz.DomQuery (singleton) A specific implementation of DomQuery that supports namespace and serializes XML docs.
 * @singleton
 * @param no
 *            parameters required
 */
Viz.DomQuery = {
	/**
	 * Selects a group of elements.
	 * 
	 * @param {String}
	 *            PATH The selector/xpath query (can be a comma separated list of selectors)
	 * @param {Node}
	 *            root The start of the query (defaults to document).
	 * @param {Object}
	 *            (optional) The namespace
	 * @return {Array}
	 */
	select : function(path, root, ns) {
		try {
			if (window.ActiveXObject) {
				return root.selectNodes(path);
			} else {
				

				var oXpe = new XPathEvaluator();
				var oNsResolver = ns ? ns : oXpe.createNSResolver(root.ownerDocument == null ? root.documentElement : root.ownerDocument.documentElement);
				var oResult = oXpe.evaluate(path, root, oNsResolver, 0, null);
				var aFound = [];
				var oRes;
				while (oRes = oResult.iterateNext()) {
					aFound[aFound.length] = oRes;
				}
				if (aFound.length > 0)
					return aFound;
			}
		} catch (e) {
			alert(e);
		}
	},

	/**
	 * Selects the value of a node.
	 * 
	 * @param {String}
	 *            path The selector/xpath query
	 * @param {Node}
	 *            root The start of the query (defaults to document).
	 * @param {Object}
	 *            ns (optional) The namespace
	 * @return {String}
	 */
	selectValue : function(path, root, ns) {
		var res = this.select(path, root, ns);
		if (res)
			return res[0].value;
	},

	selectNumber : this.selectValue,

	/**
	 * Selects a single element.
	 * 
	 * @param {String}
	 *            selector The selector/xpath query (can be a comma separated list of selectors)
	 * @param {Node}
	 *            root The start of the query (defaults to document).
	 * @param {Object}
	 *            ns (optional) The namespace
	 * @return {Node}
	 */
	selectNode : function(path, root, ns) {
		var res = this.select(path, root, ns);
		if (res)
			return res[0];
	},

	/**
	 * Selects a the text content of an element.
	 * 
	 * @param {String}
	 *            path The selector/xpath query (can be a comma separated list of selectors)
	 * @param {Node}
	 *            root The start of the query (defaults to document).
	 * @param {Object}
	 *            ns (optional) The namespace
	 * @return {String}
	 */
	selectNodeText : function(path, root, ns) {
		// look for 'text' property for IE and 'textContent' for Mozilla
		var res = this.selectNode(path, root, ns);
		if (res) {
			if (window.ActiveXObject)
				return res.text;
			else
				return res.textContent;
		}
	},

	/**
	 * Set the text content of an element.
	 * 
	 * @param {Node}
	 *            root The element
	 * @param {String}
	 *            text The text content
	 */
	setNodeText : function(oNode, text) {
		// If the current browser is Internet Explorer...
		if (window.ActiveXObject)
			oNode.text = text;
		else
			oNode.textContent = text;
	},

	/**
	 * Creates an XML document following the browser implementation.
	 * 
	 * @param {String}
	 *            sXml (optional) The initial xml string the document should be loaded with. String is loaded synchronously
	 * @return {Object} XmlDom
	 */
	createXMLDoc : function(sXml) {
		// If the current browser is Internet Explorer...
		if (window.ActiveXObject) {
			oXml = new ActiveXObject("Microsoft.XMLDOM");
			oXml.async = "false";
			if (sXml)
				oXml.loadXML(sXml);
		} else { // Current browser is Mozilla, Firefox, Opera, etc.
			var oParser = new DOMParser();
			if (sXml)
				oXml = oParser.parseFromString(sXml, "text/xml");
		}
		// Return the reference to the XML document.
		return oXml;
	},

	/**
	 * Serializes to a string an XML document following the browser implementation.
	 * 
	 * @param {Object}
	 *            oXML The XML document to be serialized.
	 * @return {String} The serialized string
	 */
	serializeXML : function(oXML) {
		var strXML = '';
		strXML = oXML.xml;
		if (!strXML) {
			var serializer = new XMLSerializer();
			strXML = serializer.serializeToString(oXML);
		}
		return strXML;
	}
};

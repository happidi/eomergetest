Ext.namespace("Viz");
Ext.namespace("Viz.Authorization");

Viz.version = "1.0.0";

/**
 * True to indicate that user has logged in.
 * @type Boolean
 */
Viz.HasLogged = false;

/**
 * True to display the application as a desktop.
 * @type Boolean
 */
Viz.DisplayAsDesktop = false;
/**
 * Returns <tt>true</tt> if the operation is authorized for the current user, <tt>false</tt> otherwise.
 * @param {String} operation
 * @return {Boolean}
 */
$authorized = function(operation) {
    if (Viz.ApplicationSettings.DisablePermissions)
        return true;
    if (Ext.isArray(operation)) {
        var approved = Ext.partition(operation, function(singleOperation) {
                    return Viz.getOperationAuthorization(singleOperation);
                })[0];
        return (approved.length > 0);
    }
    return Viz.getOperationAuthorization(operation);
};

Viz.getOperationAuthorization = function(operation) {
    var msg = null;
    var result;
    if (Viz.Authorization && Viz.Authorization.Store) {
        var resource = Viz.Authorization.Store.getById(operation);
        if (resource)
            return resource.data.ItemType == "Operation" || resource.data.ItemType == "Task";
        else
            return false;
    }
    return false;
}

/**
 * Returns the localized message resource.
 * @param {String} code
 * @return {String}
 */
$lang = function(code) {
    var msg = null;
    var result;
    if (Viz.Localization && Viz.Localization.Store) {
        var resource = Viz.Localization.Store.getById(code);
        if (resource)
            msg = resource.data.Value;
    }
    if (!msg)
        msg = Viz.Localization.Langue[code]

    return (msg != null ? msg : code);
};

/**
 * Returns an img tag in grid header.
 * @param {String} iconCls the icon css class
 * @return {String}
 */
$img = function(iconCls) {
    return '<span class="viz-grid-img-header ' + iconCls + '" ></span>';
    // return '<img class="viz-grid-img-header" src="' + imgPath + '" />';
}

$imgSrc = function(iconCls) {
    var retVal = "";
    var rule = Ext.util.CSS.getRule("." + iconCls);
    if (rule && rule.style && rule.style['backgroundImage'])
        retVal = rule.style['backgroundImage'].replace('url("', '').replace('")', '').replace('url(', '').replace(')', '');
    return retVal;
}
/**
 * Returns the localized date time format
 * @param {} isWithSeconds
 * @return {}
 */
Viz.getLocalizedDateTimeFormat = function(isWithSeconds) {
    var format = $lang('langue_sencha_date_format') + ' ' + $lang('langue_sencha_time_format');
    var retVal = isWithSeconds ? format + $lang('langue_sencha_seconds_format') : format;

    return retVal;
}
/**
 * Returns the localized date format
 * @return {}
 */
Viz.getLocalizedDateFormat = function() {
    return $lang('langue_sencha_date_format');
}
/**
 * Returns the localized date format
 * @param {} isWithSeconds
 * @return {}
 */
Viz.getLocalizedTimeFormat = function(isWithSeconds) {
    var format = $lang('langue_sencha_time_format')
    var retVal = isWithSeconds ? format + $lang('langue_sencha_seconds_format') : format;
    return retVal;
}
/**
 * Returns the localized acquisition date time format
 * @param {} intervalType
 * @return {}
 */
Viz.getLocalizedMeterDataAcquisitionDateTimeFormat = function(intervalType) {
    switch (intervalType) {
        case Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Minutes :
        case Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Hours :
            return $lang('langue_sencha_meterdata_minuteshours_format');
        case Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Days :
            return $lang('langue_sencha_meterdata_days_format');
        case Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Months :
            return $lang('langue_sencha_meterdata_months_format');
        case Vizelia.FOL.BusinessEntities.ManualInputTimeInterval.Years :
            return $lang('langue_sencha_meterdata_years_format');
        default :
            return Viz.getLocalizedDateTimeFormat(true);
    }
}
/**
 * Clones an object. When working with arborescent objects, the Ext.apply does only copy the first level. Any deeper level is in fact a reference.
 * @param {Object} o
 * @return {Object}
 */
Viz.clone = function(o) {
    if (!o || 'object' !== typeof o) {
        return o;
    }
    if ('function' === typeof o.clone) {
        return o.clone();
    }
    var c = '[object Array]' === Object.prototype.toString.call(o) ? [] : {};
    var p, v;
    for (p in o) {
        if (o.hasOwnProperty(p)) {
            v = o[p];
            // We don't want to duplicate a scope property since it may contain cyclic references.
            if (v && 'object' === typeof v && p != 'scope') {
                c[p] = Viz.clone(v);
            }
            else {
                c[p] = v;
            }
        }
    }
    return c;
};

/*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************
 * Returns copied data records without references to the store that retrieved them.
 * @param {} records
 * @return {}
 */
Viz.copyDataRecords = function(records) {
    // We want to cache the retreived results, but we don't want the store property.
    // By copying the record, we lose that property, enabling the original store to
    // not be refferred by anything and the garbage collector will collect it.
    // NOTE: Similar code in Enum store and WCFSimpleStore for caching. Update both for now. Refactor later.

    var copiedRecords = new Array();

    Ext.each(records, function(r) {
                var c = r.copy();
                Ext.apply(c, {
                            json : r.json
                        });

                copiedRecords.push(c);
            }, this);

    return copiedRecords;
};

/**
 * Clears all properties from an object.
 * @param {Object} o
 */
Viz.clearObject = function(o, preserve) {
    for (p in o) {
        var flgPreserve = Ext.isArray(preserve) && (preserve.indexOf(p) > -1);
        if (!flgPreserve)
            delete o[p];
    }
};

/**
 * The number of the current download frame. We iterate downloads between IFRAMEs.
 * @type Number
 */
Viz.currentDownloadFrameIndex = 0;

/**
 * Downloads a file from the given URL using an HTTP GET
 * @param {String} url The URL of the file to download
 * @param {Bool} disableCaching Determines whether to add a parameter to the request in order to force the browser to not use cache.
 */
Viz.downloadFile = function(url, disableCaching) {
    // Constants
    var numberOfDownloadFrames = 5;
    var downloadFrameIdPrefix = 'viz_download_iframe_';

    // The IFRAME we are going to work with now:
    var currentDownloadFrame = downloadFrameIdPrefix + Viz.currentDownloadFrameIndex;

    // Calculate next frame number we are going to work with. This is modulation since the frame
    // number we calculate must be between 0 and (numberOfDownloadFrames-1).
    Viz.currentDownloadFrameIndex = (Viz.currentDownloadFrameIndex + 1) % numberOfDownloadFrames;

    try {
        Ext.destroy(Ext.get(currentDownloadFrame));
    }
    catch (e) {
    }

    var requestUrl = url;
    requestUrl += (url.indexOf('?') != -1 ? '&' : '?') + 'PAGEID=' + Viz.pageId;
    if (disableCaching === true) {
        requestUrl += (url.indexOf('?') != -1 ? '&' : '?') + '_dc=' + (new Date().getTime());
    }

    /*
     * //window.open('test.aspx?url=' + requestUrl, null, 'toolbar=0,location=no,directories=0,status=0,scrollbars=yes,resizable=1,width=1,height=1,top=0,left=0'); //return; var linkId = 'downloadLink' + Ext.id(); Ext.DomHelper.append(Ext.getBody(), { tag : 'a', id : linkId, frameBorder : 0, css : 'display:none;visibility:hidden;height:0px;', href : 'test.aspx?url=' + requestUrl, target : '' }); Ext.get(linkId).dom.click(); Ext.get(linkId).remove();
     */
    Ext.DomHelper.append(Ext.getBody(), {
                tag         : 'iframe',
                id          : currentDownloadFrame,
                frameBorder : 0,
                width       : 0,
                height      : 0,
                css         : 'display:none;visibility:hidden;height:0px;',
                src         : requestUrl
            });
}

// This is expected by AJAX.NET
Viz.__namespace = true;

Viz.Configuration = {};

/**
 * Repository for component xtype constants
 * @singleton
 */
Viz.xtype = {
    panel    : 'panel',
    field    : 'field',
    textarea : 'textarea',
    combo    : 'combo',
    window   : 'window',
    tabpanel : 'tabpanel'
};

/**
 * Opens a form crud.
 * @param {Object} config The configuration of the form.
 * @param {String} mode The mode of the form, either 'create' or 'update'.
 * @param {Object} entity The entity object is not null when mode == 'update'
 */

Viz.openFormCrud = function(config, mode, entity, updatebatchkeys, entityParent) {
    // console.log('Viz.openFormCrud ' + new Date().format('h:m:s.u'));
    var initialConfigFromGrid = config;
    if (Ext.isObject(config)) {
        var configForm = Viz.clone(config);
        configForm = Ext.applyIf(configForm[mode] || {}, configForm.common);
        var serviceParams = {};
        if (Ext.isArray(configForm.serviceParamsSimple)) {
            Ext.each(configForm.serviceParamsSimple, function(item) {
                        serviceParams[item.name] = item.value;
                    }, this);
        }
        if (mode == 'update' && Ext.isObject(entity) && Ext.isArray(configForm.serviceParamsEntity)) {
            Ext.each(configForm.serviceParamsEntity, function(item) {
                        serviceParams[item.name] = entity[item.value];
                    }, this);
        }
        if (mode == 'updatebatch' && (!config.updatebatch || config.updatebatch.serviceHandlerUpdateBatch == null))
            return;
        var title;
        switch (mode) {
            case 'create' :
                title = configForm.title;
                break;
            case 'update' :
                title = configForm.title + ' : ' + entity[configForm.titleEntity];
                break;
            case 'updatebatch' :
                title = String.format(configForm.title, updatebatchkeys.length);
                break;
        }

        var stateId = null;
        if (mode == 'update' && !Ext.isEmpty(entity.__type) && !Ext.isEmpty(Viz.getEntityKey(entity))) {
            stateId = 'updateForm' + '_' + Viz.getEntityType(entity.__type) + '_' + Viz.getEntityKey(entity);
            if (!Ext.isEmpty(configForm.uniqueTab))
                stateId += '_' + configForm.uniqueTab;
        }
        else
            stateId = configForm.stateId;
        var existingWindows = Ext.WindowMgr.getBy(function(win) {
                    if (win.items == null || win.items.get(0) == null || win.items.get(0).mode != mode || win.items.get(0).entity != entity)
                        return false;
                    return true;
                }, this);
        if (existingWindows.length > 0) {
            Ext.each(existingWindows, function(win) {
                        Ext.WindowMgr.bringToFront(win);
                    }, this);
        }
        else {
            // we deactivate DD targets when a form crud is opened to prevent multiple dropss
            Viz.App.Global.ViewPort.activeTabLockDropTargets();
            // console.log(' window creation ' + new Date().format('h:m:s.u'));
            var win = new Ext.Window({
                        title        : title,
                        iconCls      : configForm.iconCls,
                        modal        : configForm.formModal,
                        width        : configForm.width || 400,
                        height       : configForm.height || 300,
                        x            : configForm.x,
                        y            : configForm.y,
                        collapsible  : true,
                        hideMode     : 'offsets',
                        animCollapse : false,
                        layout       : 'fit',
                        stateId      : stateId,
                        // animateTarget : grid.getView().focusEl,
                        items        : [{
                                    xtype                     : configForm.xtype,
                                    mode                      : mode,
                                    listeners                 : configForm.listeners,
                                    readonly                  : configForm.readonly,
                                    isWizard                  : configForm.isWizard,
                                    isWizardSingleTab         : configForm.isWizardSingleTab,
                                    messageSaveStart          : configForm.messageSaveStart,
                                    messageSaveEnd            : configForm.messageSaveEnd,
                                    messageSaveEndParams      : configForm.messageSaveEndParams,
                                    serviceParams             : serviceParams,
                                    entity                    : entity,
                                    serviceHandlerUpdateBatch : configForm.serviceHandlerUpdateBatch,
                                    updatebatchkeys           : updatebatchkeys,
                                    initialConfigFromGrid     : initialConfigFromGrid,
                                    historyConfig             : configForm.historyConfig,
                                    entityParent              : entityParent,
                                    uniqueTab                 : configForm.uniqueTab,
                                    uniqueTabs                : configForm.uniqueTabs,
                                    deferredRender            : configForm.deferredRender
                                }],
                        listeners    : {
                            close : function() {
                                // we reactivate DD targets
                                Viz.App.Global.ViewPort.activeTabUnlockDropTargets();
                            }
                        }
                    });
            // console.log(' window creation end ' + new Date().format('h:m:s.u'));
            win.show();
            // console.log(' window show end ' + new Date().format('h:m:s.u'));

        }
        return false;
    }
}

/**
 * Repository for layout constants
 * @singleton
 */
Viz.layout = {
    fit       : 'fit',
    accordion : 'accordion',
    absolute  : 'absolute',
    container : 'container',
    anchor    : 'anchor'
};

Ext.Ajax.timeout = 120000;

Ext.Ajax.on("requestexception", function(con, response, options) {
            // delete options.failure;
            // delete options.callback;
            var error;
            var url = options.url;
            try {
                var errorResponse = response.responseText || "";
                if (errorResponse.length > 0)
                    error = Ext.decode(errorResponse);
            }
            catch (e) {
                ;
            }

            // new code

            var const_exception_type = "ExceptionType";
            // Accepted stands for IsOneWay WCF operation.
            if (response.statusText != "OK" && response.statusText != "Accepted") {
                if (response.status === 500) {
                    // cancel any other handler

                    var exceptionType = response.getResponseHeader(const_exception_type);
                    if (exceptionType === "Vizelia.FOL.Common.VizeliaSessionTimeoutException") {
                        // we do not show the message box if user has not logged. This prevent double display of message.
                        delete options.failure;
                        delete options.callback;
                        if (Viz.HasLogged == true) {
                            Ext.Msg.show({
                                        fn      : function() {
                                            window.location.reload();
                                            // window.location.href = window.location.protocol + "//" + window.location.host + window.location.pathname.substring(0, window.location.pathname.indexOf('/', 1)) + '/default.aspx';
                                        },
                                        width   : 400,
                                        title   : $lang("error_ajax"),
                                        msg     : $lang("msg_session_timeout"),
                                        buttons : Ext.Msg.OK,
                                        icon    : Ext.MessageBox.ERROR
                                    });
                        }
                        else {
                            window.location.reload();
                        }
                    }

                    if (exceptionType === "Vizelia.FOL.Common.VizeliaSecurityException") {
                 
                        Ext.Msg.show({
                                    width   : 400,
                                    title   : $lang("msg_access_denied"),
                                    msg     : $lang("msg_access_denied_detailed"),
                                    buttons : Ext.Msg.OK,
                                    icon    : Ext.MessageBox.ERROR
                                });
                    }

                    return false;
                }

                if (error)
                    Ext.Msg.show({
                                width   : 400,
                                title   : $lang("error_ajax"),
                                msg     : String.format($lang("error_msg_ajax"), url) + '<br><br>' + String.format($lang("error_msg_ajax_type"), error.ExceptionType) + '<br>' + String.format($lang("error_msg_ajax_description"), error.Message),
                                buttons : Ext.Msg.OK,
                                icon    : Ext.MessageBox.ERROR
                            });
                else {
                    Ext.Msg.show({
                                width   : 400,
                                title   : $lang("error_ajax"),
                                msg     : $lang("error_ajax_timeout"),
                                details : String.format($lang("error_msg_ajax"), url),
                                buttons : Ext.Msg.OK,
                                icon    : Ext.MessageBox.ERROR
                            });
                }
                return false;
            }

        });

/**
 * Forces POST method over GET when not specified (for exemple in a dataStore({url : url})
 */
Ext.Ajax.on("beforerequest", function(con, options) {
    options.method = options.method ? options.method : "POST";
        // Ext.MessageBox.wait(options.waitMsg, options.waitTitle || this.waitTitle || 'Please Wait...');
        // Ext.getBody().mask('Please Wait...', 'x-mask-loading');
    });

Ext.Ajax.on("requestcomplete", function(con, response, options) {
        });

/**
 * Returns a usable type.
 * @param {string/Object} __type The type in the format : Entity:#Vizelia.FOL.BusinessEntities or a Entity
 * @return {string} the usable type in the format : Vizelia.FOL.BusinessEntities.Entity
 */
Viz.getEntityType = function(value) {
    var __type = value;
    if (Ext.isObject(value)) {
        __type = value.__type;
    }
    var retVal = __type.split('#')[1] + "." + Viz.getEntityName(__type);
    return retVal;
}
/**
 * Returns entity name.
 * @param {string/Object} __type The type in the format : Entity:#Vizelia.FOL.BusinessEntities or a Entity
 * @return {string} the entity name in the format : Entity
 */
Viz.getEntityName = function(value) {
    return value.split(':')[0];
}
/**
 * Return the key property value of an entity.
 * @param {} entity
 */
Viz.getEntityKey = function(entity) {
    var retVal = entity[Viz.BusinessEntity[Viz.getEntityType(entity.__type).replace('Vizelia.FOL.BusinessEntities.', '')].prototype.__key];
    return retVal;
}
/**
 * Return the key property value of an entity.
 * @param {} entity
 */
Viz.getServiceParamsKey = function(entityName, serviceParams) {
    var retVal = serviceParams.Key;
    if (entityName == 'FOLMembershipUser')
        retVal = serviceParams.username;
    return retVal;
}
/**
 * Takes an Date and returns a string representing how long ago the date represents. Returns a string of date in a "time ago" format.
 * @param {Date} date, The date to convert
 * @return {string}, the formated string "xxx time ago"
 */
Viz.date = {
    /**
     * Convert a local datetime for a specific timezone.
     * @param {Date} date
     * @param {Number} timeZoneOffset
     * @return {Date}
     */
    convertToLocalDate : function(date, timeZoneOffset) {
        var mult = timeZoneOffset.indexOf('-') == 0 ? -1 : 1;
        var hour = mult * timeZoneOffset.substring(timeZoneOffset.indexOf('T') + 1, timeZoneOffset.indexOf('H'));
        var minute = mult * timeZoneOffset.substring(timeZoneOffset.indexOf('H') + 1, timeZoneOffset.indexOf('M'));
        hour = isNaN(hour) ? 0 : hour;
        minute = isNaN(minute) ? 0 : minute;
        date = date.add(Date.MINUTE, date.getTimezoneOffset()).add(Date.MINUTE, hour * 60 + minute);
        return date;
    },
    prettyDate         : function(date) {
        var retVal;
        var dateLevel = 999;
        var diff = (((new Date()).getTime() - date.getTime()) / 1000);
        var day_diff = Math.floor(diff / 86400);

        if (isNaN(day_diff) || day_diff < 0)
            return;

        var dateArgs = new Array();
        if (day_diff > 365) {
            dateLevel = 1;
            var years = Math.floor(day_diff / 365);
            day_diff -= years * 365;
            diff -= years * 365 * 86400;
            dateArgs.push(years);
            if (years > 1) {
                dateArgs.push($lang('msg_years'));
            }
            else {
                dateArgs.push($lang('msg_year'));
            }
        }
        if (day_diff > 30) {
            dateLevel = 2;
            var months = Math.floor(day_diff / 30);
            day_diff -= months * 30;
            diff -= months * 30 * 86400;
            dateArgs.push(months);
            if (months > 1) {
                dateArgs.push($lang('msg_months'));
            }
            else {
                dateArgs.push($lang('msg_month'));
            }
        }
        if (day_diff > 7 && dateArgs.length < 4 && dateLevel > 1) {
            dateLevel = 3;
            var weeks = Math.floor(day_diff / 7);
            day_diff -= weeks * 7;
            diff -= weeks * 7 * 86400;
            dateArgs.push(weeks);
            if (weeks > 1) {
                dateArgs.push($lang('msg_weeks'));
            }
            else {
                dateArgs.push($lang('msg_week'));
            }
        }
        if (day_diff > 1 && dateArgs.length < 4 && dateLevel > 2) {
            dateLevel = 4;
            dateArgs.push(day_diff);
            dateArgs.push($lang('msg_days'));
            diff -= day_diff * 86400;
        }
        if (day_diff == 1 && dateArgs.length < 4 && dateLevel > 3) {
            dateLevel = 5;
            dateArgs.push(day_diff);
            dateArgs.push($lang('msg_day'));
            diff -= 86400;
        }
        if (dateArgs.length < 4 && dateLevel > 4) {
            if (diff > 3600) {
                dateLevel = 6;
                var hours = Math.floor(diff / 3600);
                diff -= hours * 3600;
                dateArgs.push(hours);
                if (hours > 1) {
                    dateArgs.push($lang('msg_hours'));
                }
                else {
                    dateArgs.push($lang('msg_hour'));
                }
            }
            if (diff > 60 && dateArgs.length < 4 && dateLevel > 5) {
                dateLevel = 7;
                var minutes = Math.floor(diff / 60);
                diff -= minutes * 60;
                dateArgs.push(minutes);
                if (minutes > 1) {
                    dateArgs.push($lang('msg_minutes'));
                }
                else {
                    dateArgs.push($lang('msg_minute'));
                }
            }
            if (dateArgs.length < 4 && dateLevel > 6) {
                dateLevel = 8;
                var seconds = Math.round(diff);
                dateArgs.push(seconds);
                if (seconds > 1) {
                    dateArgs.push($lang('msg_seconds'));
                }
                else {
                    dateArgs.push($lang('msg_second'));
                }
            }
        }

        if (dateArgs.length == 2)
            retVal = String.format($lang("msg_single_time_ago"), dateArgs[0], dateArgs[1]);
        else if (dateArgs.length == 4)
            retVal = String.format($lang("msg_double_time_ago"), dateArgs[0], dateArgs[1], dateArgs[2], dateArgs[3]);

        return retVal;
    }
}
/**
 * Returns true if Audit is enabled for this entity, false if not.
 * @param {String} entityName
 * @return {String}
 */
$isAuditEnabled = function(entityName) {
    var retval = null;
    if (Viz.AuditEntityStore) {
        var index = Viz.AuditEntityStore.findExact("EntityName", entityName);
        if (index >= 0)
            retval = Viz.AuditEntityStore.getAt(index).get("IsAuditEnabled");
    }

    return (retval || false);
};

/*
* Mapping function between Vizelia.FOL.BusinessEntities.ManualInputTimeInterval(Meter enum) 
* and Vizelia.FOL.BusinessEntities.AxisTimeInterval(Form and Service enum)
* @param {Number} manualMeterTimeInterval
* @return {Number}
*/
Viz.toAxisTimeInterval = function (manualMeterTimeInterval) {
    var axisTimeInterval = 8; //Default to minutes
    switch(manualMeterTimeInterval) {
        case 0:
            //Seconds
            axisTimeInterval = 9;
            break;
        case 1:
            //Minutes
            axisTimeInterval = 8;
            break;
        case 2:
            //Hours
            axisTimeInterval = 7;
            break;
        case 3:
            //Days
            axisTimeInterval = 6;
            break;
        case 4:
            //Months
            axisTimeInterval = 4;
            break;
        case 5:
            //Years
            axisTimeInterval = 2;
            break;
        default:
            axisTimeInterval = 8;
    }
    return axisTimeInterval;
}

       
Viz.activateGoogleAnalytics = function(tenantName) {
    if (Viz.ApplicationSettings.EnableGoogleAnalytics == true) {
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', Viz.ApplicationSettings.GoogleAnalyticsAccountId, 'auto', { 'userId': tenantName});
        ga('require', 'linkid', 'linkid.js');
        ga('set', 'dimension1', tenantName);
        ga('send', 'pageview');
    }
}

Viz.writeEventToGoogleAnalytics = function (category, action, label) {
    if (typeof ga == 'function') {
        ga('send', 'event', category, action, label);
    } else {
        if (Viz.ApplicationSettings.EnableGoogleAnalytics == true) {
            Viz.activateGoogleAnalytics(Viz.App.Global.User.ApplicationName);
            ga('send', 'event', category, action, label);
        }
    }
}
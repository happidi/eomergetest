﻿Ext.namespace("Viz");
// Shortcut to F3 in Spket
Viz.overrides = {};
/**
 * Determines whether the beginning of this string matches the specified string
 * @param {String} value
 * @return {Boolean}
 */
String.prototype.startsWith = function(value) {
    var index = this.indexOf(value);
    if (index == 0)
        return true;
    else
        return false;
};
/**
 * Determines whether the string contains the specified string
 * @param {String} value
 * @return {Boolean}
 */
String.prototype.contains = function(value) {
    var index = this.indexOf(value);
    if (index >= 0)
        return true;
    else
        return false;
};
/**
 * Determines whether the end of this string matches the specified string
 * @param {String} value
 * @return {Boolean}
 */
String.prototype.endsWith = function(value) {
    var start = this.length - value.length;
    return (start > -1) && (this.substr(start) == value);
};

/**
 * Replace all instances of s1 by s2 in the specifieds string.
 * @param {String} s1 the value to remove
 * @param {String} s2 the value to insert
 * @return {String}
 */
String.prototype.replaceAll = function(s1, s2) {
    return this.replace(new RegExp(s1, "g"), s2);
};

/**
 * Return HTML tag to render the text as bold.
 * @param {} value
 * @return {}
 */
String.prototype.toBold = function() {
    return '<b>' + this + '</b>';
}

Ext.apply(String, {

            /**
             * Override because MsAjax function has a bug when { is used for other purpose than argument formating 'myclass : {border : 0, color : {0}}'
             */
            format : function(format) {
                var args = Ext.toArray(arguments, 1);
                return format.replace(/\{(\d+)\}/g, function(m, i) {
                            return args[i];
                        });
            }
        });

/**
 * Creates a new array with all of the elements of this array for which the provided filtering function returns true. Should be removed in Extjs 4.0 and laters.
 * @param {Array} array
 * @param {Function} fn Callback function for each item
 * @param {Object} scope Callback function scope
 * @return {Array} results
 */
if (!Array.prototype.filter) {
    Array.prototype.filter = function(fun /* , thisp */) {
        "use strict";

        if (this == null)
            throw new TypeError();

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun != "function")
            throw new TypeError();

        var res = [];
        var thisp = arguments[1];
        for (var i = 0; i < len; i++) {
            if (i in t) {
                var val = t[i]; // in case fun mutates this
                if (fun.call(thisp, val, i, t))
                    res.push(val);
            }
        }

        return res;
    };
}

/**
 * Date patterns
 * @type Date.patterns
 */
Date.patterns = {
    ISO8601Long               : "Y-m-d H:i:s",
    ISO8601Short              : "Y-m-d",
    ShortDate                 : "n/j/Y",
    LongDate                  : "l, F d, Y",
    FullDateTime              : "l, F d, Y g:i:s A",
    MonthDay                  : "F d",
    ShortTime                 : "g:i A",
    LongTime                  : "g:i:s A",
    SortableDateTime          : "Y-m-d\\TH:i:s",
    UniversalSortableDateTime : "Y-m-d H:i:sO",
    YearMonth                 : "F, Y",
    FrenchShortDate           : 'd/m/Y',
    FrenchDateTimeNoSecond    : 'd/m/Y H:i',
    FrenchDateTimeSecond      : 'd/m/Y H:i:s'
};
Date.prototype.getWCFGMTOffset = function() {
    var h = Math.floor(Math.abs(this.getTimezoneOffset()) / 60);
    var m = Math.abs(this.getTimezoneOffset() % 60);
    return (this.getTimezoneOffset() > 0 ? "-" : "") + 'PT' + h + 'H' + (m > 0 ? m + 'M' : '');
};

Ext.overrideBase = Ext.override;
Ext.apply(Ext, {
            /**
             * Adds a list of functions to the prototype of an existing class, overwriting any existing methods with the same name. The version is different from the standard Extjs one, because we add a baseclass property to allow simple access to the former functions before the override. Usage:
             * 
             * <pre><code>
             * Ext.override(MyClass, {
             *             newMethod1 : function() {
             *                 // before code.
             *                 // calling the former implementation
             *                 MyClass.baseclass.newMethod1.apply(this, arguments);
             *                 // after code
             *             },
             *             newMethod2 : function(foo) {
             *                 // etc.
             *             }
             *         });
             * </code></pre>
             * 
             * @param {Object} origclass The class to override
             * @param {Object} overrides The list of functions to add to origClass. This should be specified as an object literal containing one or more methods.
             * @method override
             */
            override : function(origclass, overrides) {
                if (overrides) {
                    var oc = Object.prototype.constructor; // to make sure overrides containes a real constructor and not the Object one.
                    var p = origclass.prototype;
                    // we provide a baseclass property containing the former
                    // functions
                    origclass.baseclass = {};
                    // we copy also the constructor from the prototype
                    origclass.baseclass.constructor = origclass.prototype.constructor;
                    if (overrides.constructor && overrides.constructor != oc) {
                        origclass.prototype.constructor = overrides.constructor;
                        origclass.constructor = overrides.constructor;
                    }

                    // we copy all remaining functions
                    for (var method in overrides) {
                        if (method != "constructor") {
                            origclass.baseclass[method] = p[method];
                            // p[method] = overrides[method];
                        }
                    }

                    Ext.apply(p, overrides);
                    if (Ext.isIE && overrides.toString != origclass.toString) {
                        p.toString = overrides.toString;
                    }
                }
            }
        });

var wrappedElementSetStyle = Ext.Element.prototype.setStyle;

Ext.override(Ext.Element, {
            /**
             * Override because sometime in IE this.dom is null; Returns the region of the given element. The element must be part of the DOM tree to have a region (display:none or elements not appended return false).
             * @return {Region} A Ext.lib.Region containing "top, left, bottom, right" member data.
             */
            getRegion : function() {
                if (this.dom)
                    return Ext.lib.Dom.getRegion(this.dom);
                else
                    return null;
            },
            /**
             * Overrides the original Extjs function so that it accepts no parameters or one parameter When parameters are missing we use Ext.LoadMask.prototype.msg and Ext.LoadMask.prototype.msgClass Puts a mask over this element to disable user interaction. Requires core.css. This method can only be applied to elements which accept child nodes.
             * @param {String} msg (optional) A message to display in the mask
             * @param {String} msgCls (optional) A css class to apply to the msg element
             * @return {Element} The mask element
             */
            mask      : function() {
                // Make sure the dom still exists and wasn't removed in the meanwhile.
                if (!this.dom)
                    return;

                if (arguments.length == 0) {
                    return Ext.Element.baseclass.mask.apply(this, [Ext.LoadMask.prototype.msg, Ext.LoadMask.prototype.msgCls]);
                }
                else if (arguments.length == 1) {
                    return Ext.Element.baseclass.mask.apply(this, [arguments[0], Ext.LoadMask.prototype.msgCls]);
                }
                else {
                    return Ext.Element.baseclass.mask.apply(this, arguments);
                }
            },
            /**
             * Overrides the original Extjs function so that "this.dom.style[chkCache(style)] = value" will not throw invalid property in IE8 for selected styles.
             */
            setStyle  : function(prop, value) {
                var delegate = Ext.createDelegate(wrappedElementSetStyle, this);
                if (Ext.isIE8 || Ext.isIE7) {
                    switch (prop) {
                        case 'background-image' :
                            prop = 'backgroundImage';
                            break;
                        case 'overflow-x' :
                            prop = 'overflowX';
                            break;
                    }
                    if (typeof value == "string")
                        value = value.replace(" !important", "");
                }
                return delegate(prop, value);
            }
        });

Ext.apply(Ext.util.Format, {
            Number : function(v) {
                v = (Math.round((v - 0) * 100)) / 100;
                v = (v == Math.floor(v)) ? v + "" : ((v * 10 == Math.floor(v * 10)) ? v + "0" : v);
                v = String(v);
                var ps = v.split('.');
                var whole = ps[0];
                var sub = ps[1] ? '.' + ps[1] : '';
                var r = /(\d+)(\d{3})/;
                while (r.test(whole)) {
                    whole = whole.replace(r, '$1' + ' ' + '$2');
                }
                v = whole + sub;
                if (v.charAt(0) == '-') {
                    return '-' + v.substr(1);
                }
                return "" + v;
            }
        });

Ext.chromeVersion = Ext.isChrome && navigator && navigator.userAgent ? parseInt((/chrome\/(\d{2})/).exec(navigator.userAgent.toLowerCase())[1], 10) : NaN;
try {
    Ext.safariVersion = Ext.isSafari && navigator && navigator.userAgent ? parseInt((/version\/(\d{1})/).exec(navigator.userAgent.toLowerCase())[1], 10) : NaN;
}
catch (e) {
    Ext.safariVersion = NaN;
}
Ext.override(Ext.grid.ColumnModel, {
            /**
             * Overrides Ext.grid.ColumnModel.setConfig
             * <p>
             * The new version allows us to define a column via an xtype.
             * </p>
             * <p>
             * example:
             * </p>
             * 
             * <pre><code>
             * cols : [{xtype : 'viz-grid-rownumberer'} , ...]
             * </code></pre>
             */
            setConfig         : function(config, initial) {
                // we replace each config defining a xtype with the
                // correspondant object
                Ext.each(config, function(c, index, arr) {
                            if (c.xtype && Ext.ComponentMgr.isRegistered(c.xtype)) {
                                arr[index] = Ext.ComponentMgr.create(c);
                            }
                        });
                // we then call the original implementation
                Ext.grid.ColumnModel.baseclass.setConfig.apply(this, arguments);
            },
            /**
             * Returns an array of column config objects for the visibility given.
             * @param {Boolean} hidden True returns visible columns; False returns hidden columns (defaults to True).
             * @param {String} cfg Specify the config property to return (defaults to the config).
             * @return {Array} result
             * 
             * <pre><code>
             * //get array of visible column config objects
             * var visible1 = grid.getColumnModel().getColumnsVisible();
             * var visible2 = grid.getColumnModel().getColumnsVisible(true);
             * //get array of hidden column config objects
             * var hidden1 = grid.getColumnModel().getColumnsVisible(false);
             * //get array of hidden column config objects (only return the
             * //dataIndex property of each)   
             * var hidden2 = grid.getColumnModel().getColumnsVisible(false, 'dataIndex');
             * </code></pre>
             */
            getColumnsVisible : function(visibility, cfg) {
                var visible = (visibility === false) ? false : true;
                var r = [];
                for (var i = 0, len = this.config.length; i < len; i++) {
                    var c = this.config[i];
                    var hidden = c.hidden ? true : false;
                    if (hidden !== visible) {
                        r[r.length] = c[cfg] || c;
                    }
                }
                return r;
            },

            /**
             * Override to fix Column sizing with Chorme 19 and above. Should be fixed in 3.4.1...
             */

            getTotalWidth     : function(includeHidden) {
                if (!this.totalWidth) {
                    var boxsizeadj = ((Ext.isChrome && Ext.chromeVersion > 18) || (Ext.isSafari && Ext.safariVersion > 5) ? 2 : 0);
                    this.totalWidth = 0;
                    for (var i = 0, len = this.config.length; i < len; i++) {
                        if (includeHidden || !this.isHidden(i)) {
                            this.totalWidth += (this.getColumnWidth(i) + boxsizeadj);
                        }
                    }
                }
                return this.totalWidth;
            }
        });

/**
 * Overrides after a bug when rendering error tooltip on form as standard tooltip instead of the white expected.
 * @param {} field
 * @param {} msg
 */
Ext.form.MessageTargets.qtip.mark = function(field, msg) {
    field.el.addClass(field.invalidClass);
    field.el.dom.qtip = msg;
    field.el.set({
                "ext:qclass" : 'x-form-invalid-tip'
            }); // <-- change here
    if (Ext.QuickTips) {
        Ext.QuickTips.enable();
    }
};
/**
 * Overrides after a bug when rendering error tooltip on form as standard tooltip instead of the white expected.
 * @param {} field
 * @param {} msg
 */
Ext.form.MessageTargets.side.mark = function(field, msg) {
    field.el.addClass(field.invalidClass);
    if (!field.errorIcon) {
        var elp = field.getErrorCt();

        if (!elp) {
            field.el.dom.title = msg;
            return;
        }
        field.errorIcon = elp.createChild({
                    cls : 'x-form-invalid-icon'
                });
        if (field.ownerCt) {
            field.ownerCt.on('afterlayout', field.alignErrorIcon, field);
            field.ownerCt.on('expand', field.alignErrorIcon, field);
        }
        field.on('resize', field.alignErrorIcon, field);
        field.on('destroy', function() {
                    Ext.destroy(this.errorIcon);
                }, field);
    }
    field.alignErrorIcon();
    field.errorIcon.dom.qtip = msg;
    field.errorIcon.set({
                "ext:qclass" : 'x-form-invalid-tip'
            }); // <-- change here
    field.errorIcon.show();
};

/**
 * Corrects a bug in onDestroyRecords. Adds beforesave and save event. Change so that when paging through the store previous modification will be kept and display to the user.
 */
var singleSortMethod = Ext.data.Store.prototype.singleSort;

Ext.override(Ext.data.Store, {
            saveStatus         : 0,
            constructor        : function(config) {
                /**
                 * @property hasLoaded <tt>true</tt> if the store was loaded, <tt>false</tt> otherwise.
                 */
                this.hasLoaded = false;
                Ext.data.Store.baseclass.constructor.apply(this, arguments);
                this.addEvents('beforesave', 'save');
                if ((this.writer) && (this.proxy)) {
                    this.proxy.on({
                                scope       : this,
                                beforewrite : this.onProxyBeforeWrite,
                                write       : this.onProxyWrite
                            });
                }
                // So we can keep track of modified records through paging
                this.on("load", function(store) {
                            // loop through modified records
                            this.hasLoaded = true;
                            this.removed = [];
                            var modified = Ext.apply([], store.getModifiedRecords());
                            for (var i = 0; i < modified.length; i++) {
                                // see if we have a record with same id and apply changes if yes
                                var r = store.getById(modified[i].id);
                                if (r) {
                                    var changes = modified[i].getChanges();
                                    // before we apply change we remove the record in the modified collection so that it is not added twice.
                                    store.modified.remove(modified[i]);
                                    for (p in changes) {
                                        if (changes.hasOwnProperty(p)) {
                                            r.set(p, changes[p]);
                                        }
                                    } // eo changes loop
                                    this.fireEvent('update', this, r, Ext.data.Record.EDIT);
                                }
                            } // eo modified loop
                        }, this);

                this.on("add", function(store, records, index) {
                            //
                        });
            },
            // Each time we are about to write check to see if this is the first
            // time of the batch and then fire the before save event.
            onProxyBeforeWrite : function(proxy, action, rs, params) {
                if (this.saveStatus == 0)
                    this.fireEvent("beforesave", this);
                this.saveStatus += 1;
            },
            // Each time we finished writing, decrease the saveStatus. If equals
            // 0 it means the batch is finished and we fire the save event.
            onProxyWrite       : function(proxy, action, rs, params) {
                this.saveStatus -= 1;
                if (this.saveStatus == 0) {
                    this.fireEvent("save", this);
                }
            },

            /**
             * Gets an array of data from a store.
             * @param {Number} startIndex (optional) The starting index (defaults to 0)
             * @param {Number} endIndex (optional) The ending index (defaults to the last Record in the Store)
             * @return {[]} An array of data
             */
            getArray           : function(start, end) {
                if (!this.hasLoaded)
                    return null;
                var range = this.getRange(start, end);
                var result = [];
                Ext.each(range, function(rec) {
                            result.push(rec.data);
                        });
                return result;
            },

            singleSort         : function(fieldName, dir) {
                if (!this.fields)
                    return false;
                return singleSortMethod.apply(this, arguments);
            }
        });
/**
 * For each JsonStore we send from WCF we always include meta. This is an enhancement to the JsonReader so that when same meta is sent again, the reader does not need to recreate itself.
 */
Ext.override(Ext.data.JsonReader, {
            readRecords       : function(o) {
                this.jsonData = o;
                if (o.metaData) {
                    if (o.metaData.__type && (this.meta.__type == o.metaData.__type)) {
                        delete o.metaData;
                    }
                }
                return Ext.data.JsonReader.baseclass.readRecords.apply(this, arguments);
            },

            /**
             * Override of the createAccessor because when a complex object is send we need to check if the child object is not null( case when i>0)
             */
            createAccessorOld : function() {
                var re = /[\[\.]/;
                return function(expr) {
                    if (Ext.isEmpty(expr)) {
                        return Ext.emptyFn;
                    }
                    if (Ext.isFunction(expr)) {
                        return expr;
                    }
                    var i = String(expr).search(re);
                    if (i >= 0) {
                        // /this is our change
                        if (i > 0) {
                            return new Function('obj', ' if(obj.' + expr.replace(expr.substring(i), '') + '){  return obj.' + expr + ';} else {return null;}');
                        }
                        else {
                            return new Function('obj', 'return obj' + expr);
                        }
                    }
                    return function(obj) {
                        return obj[expr];
                    };
                };
            }(),

            /**
             * Override of the createAccessor because when a complex object is send we need to check if the child object is not null( case when i>0)
             */
            createAccessor    : function() {
                var re = /[\[\.]/;
                return function(expr) {
                    if (Ext.isEmpty(expr)) {
                        return Ext.emptyFn;
                    }
                    if (Ext.isFunction(expr)) {
                        return expr;
                    }

                    var i = String(expr).search(re);
                    if (i >= 0) {
                        // /this is our change
                        if (i > 0) {
                            var split = String(expr).split(re);
                            var lastVar = 'obj.' + split[0];
                            var ifCondition = 'if(!Ext.isEmpty(' + lastVar + ')';
                            for (var j = 1; j < split.length; j++) {
                                lastVar += '.' + split[j]
                                ifCondition += ' && !Ext.isEmpty(' + lastVar + ')';
                            }
                            ifCondition += ')';
                            return new Function('obj', ifCondition + '{  return obj.' + expr + ';} else {return null;}');
                        }
                        else {
                            return new Function('obj', 'return obj' + expr);
                        }
                    }

                    return function(obj) {
                        return obj[expr];
                    };
                };
            }()

        });
/**
 * Overrides the constructor for detecting that a mask should be applied when a writer is defined on the store.
 * @class Ext.LoadMask
 * @extends Ext.LoadMask
 */
Ext.LoadMask2 = Ext.extend(Ext.LoadMask, {
            /**
             * @cfg {String} msgCrud The text to display after a crud operation in a centered loading message box (defaults to 'Updating...')
             */
            msgCrud      : 'Updating...',
            /**
             * @constructor Create a new LoadMask
             * @param {Mixed} el The element or DOM node, or its id
             * @param {Object} config The config object
             */
            constructor  : function(el, config) {
                Ext.LoadMask.superclass.constructor.apply(this, arguments);
                if (this.store) {
                    this.store.on('beforesave', this.onBeforeSave, this);
                    this.store.on('save', this.onLoad, this);
                }
            },
            // private
            onBeforeSave : function() {
                if (!this.disabled) {
                    this.el.mask(this.msg, this.msgCls);
                }
            },
            destroy      : function() {
                Ext.LoadMask.superclass.destroy.apply(this, arguments);
                if (this.store) {
                    this.store.un('beforesave', this.onBeforeLoad, this);
                    this.store.un('save', this.onLoad, this);
                }
            }
        });
Ext.LoadMask = Ext.LoadMask2;

Ext.override(Ext.form.DateField, {
            /**
             * Override because we want to change the error message that is displayed when data is inputed in a wrong format
             */
            getErrors : function(value) {
                var errors = Ext.form.TriggerField.prototype.getErrors.apply(this, arguments);

                value = this.formatDate(value || this.processValue(this.getRawValue()));

                if (value.length < 1) { // if it's blank and textfield didn't flag it then it's valid
                    return errors;
                }

                var svalue = value;
                value = this.parseDate(value);
                if (!value) {
                    errors.push(String.format(this.invalidText, svalue, this.formatDate(new Date(new Date().getFullYear(), 0, 31, 0, 0, 0))));
                    return errors;
                }

                var time = value.getTime();
                if (this.minValue && time < this.minValue.clearTime().getTime()) {
                    errors.push(String.format(this.minText, this.formatDate(this.minValue)));
                }

                if (this.maxValue && time > this.maxValue.clearTime().getTime()) {
                    errors.push(String.format(this.maxText, this.formatDate(this.maxValue)));
                }

                if (this.disabledDays) {
                    var day = value.getDay();

                    for (var i = 0; i < this.disabledDays.length; i++) {
                        if (day === this.disabledDays[i]) {
                            errors.push(this.disabledDaysText);
                            break;
                        }
                    }
                }

                var fvalue = this.formatDate(value);
                if (this.disabledDatesRE && this.disabledDatesRE.test(fvalue)) {
                    errors.push(String.format(this.disabledDatesText, fvalue));
                }

                return errors;
            }
        });
/**
 * Overrides the findField so it works with pset field of type combo.
 */
Ext.override(Ext.form.BasicForm, {
            /**
             * Overrides the findField so it works with pset field of type combo. Find a {@link Ext.form.Field} in this form.
             * @param {String} id The value to search for (specify either a {@link Ext.Component#id id}, {@link Ext.grid.Column#dataIndex dataIndex}, {@link Ext.form.Field#getName name or hiddenName}).
             * @return Field
             */
            findFieldOLD   : function(id) {
                var field = this.items.get(id);
                if (!Ext.isObject(field)) {
                    this.items.each(function(f) {
                                if (f.items) {
                                    field = this.findField.apply(f, [id]);
                                    return field == null;
                                }
                                // we added here the condition f.name == id because when using a combobox f.getName() would return the hidden value.
                                if (f.isFormField && (f.dataIndex == id || f.id == id || f.getName() == id || f.name == id)) {
                                    field = f;
                                    return false;
                                }
                            }, this);
                }
                return field || null;
            },
            /**
             * Overrides the findField so it works with pset field of type combo. Find a {@link Ext.form.Field} in this form.
             * @param {String} id The value to search for (specify either a {@link Ext.Component#id id}, {@link Ext.grid.Column#dataIndex dataIndex}, {@link Ext.form.Field#getName name or hiddenName}).
             * @return Field
             */
            findField      : function(id) {
                var field = this.items.get(id);
                if (!Ext.isObject(field)) {
                    // searches for the field corresponding to the given id. Used recursively for composite fields
                    var findMatchingField = function(f) {
                        if (f.isFormField) {
                            // we added here the condition f.name == id because when using a combobox f.getName() would return the hidden value.
                            if (f.dataIndex == id || f.id == id || f.getName() == id || f.name == id) {
                                field = f;
                                return false;
                            }
                            else if (f.isComposite && f.items) {
                                if (f.items.each) {
                                    return f.items.each(findMatchingField)
                                }
                                else {
                                    Ext.each(f.items, findMatchingField, this);
                                    return false;
                                }
                                // return f.items.each(findMatchingField);
                            }
                            else if (f.items) {
                                if (f.items.each) {
                                    return f.items.each(findMatchingField);
                                }
                                else {
                                    Ext.each(f.items, findMatchingField, this);
                                    return false;
                                }
                                // return f.items.each(findMatchingField);
                            }
                        }
                    };
                    this.items.each(findMatchingField);
                }
                return field || null;
            },
            /**
             * Overrides to delay the after action if the form contains async combobox.
             * @param {} action
             * @param {} success
             */
            afterAction    : function(action, success) {
                if (this._async_components && (this._async_components.length > 0)) {
                    this._async_components_action = action;
                    return;
                }
                // if (this._async_components_action) {
                // this._async_components_action = null;
                // }
                // from here it's a copy of afterAction from BasicForm.js
                this.activeAction = null;
                var o = action.options;
                if (o.waitMsg) {
                    if (this.waitMsgTarget === true) {
                        this.el.unmask();
                    }
                    else if (this.waitMsgTarget) {
                        this.waitMsgTarget.unmask();
                    }
                    else {
                        Ext.MessageBox.updateProgress(1);
                        Ext.MessageBox.hide();
                    }
                }
                if (success) {
                    if (o.reset) {
                        this.reset();
                    }
                    Ext.callback(o.success, o.scope, [this, action]);
                    this.fireEvent('actioncomplete', this, action);
                }
                else {
                    Ext.callback(o.failure, o.scope, [this, action]);
                    this.fireEvent('actionfailed', this, action);
                }
            },
            /**
             * Overrides to work nicely with composite fields
             * @param {} dirtyOnly
             * @return {}
             */
            getFieldValues : function(dirtyOnly) {
                var o = {}, n, key, val;
                this.items.each(function(f) {
                            if (dirtyOnly !== true || f.isDirty()) {
                                // disabled because even the compositefield are inside the main collection....
                                if (f.getXType() == 'compositefield') {
                                    f.items.each(function(ff) {
                                                n = ff.getName();
                                                key = o[n];
                                                val = ff.getValue();
                                                if (key != null && Ext.isDefined(key)) {
                                                    if (Ext.isArray(key)) {
                                                        o[n].push(val);
                                                    }
                                                    else {
                                                        o[n] = [key, val];
                                                    }
                                                }
                                                else {
                                                    o[n] = val;
                                                }
                                            });
                                }
                                else {
                                    n = f.getName();
                                    key = o[n];
                                    val = f.getValue();
                                    if (key != null && Ext.isDefined(key)) {
                                        if (Ext.isArray(key)) {
                                            o[n].push(val);
                                        }
                                        else {
                                            o[n] = [key, val];
                                        }
                                    }
                                    else {
                                        o[n] = val;
                                    }
                                }
                            }
                        });
                return o;
            }
        });
/**
 * <p>
 * Overrides the setValue function. Defer the setValue call until after the next load. writer is defined on the store.
 * <p>
 * <p>
 * Adds a listHeight property.
 * </p>
 * @class Ext.form.ComboBox
 * @extends Ext.form.ComboBox
 */
Ext.override(Ext.form.ComboBox, {
            /**
             * Overrided to ensure some space exists between the textfield and the drop down box when expanded.
             */
            listAlign      : ['tl-bl?', [0, 2]],
            /**
             * @cfg {Number} listHeight Fixes the heigth of the dropdown list (optional).
             */
            /**
             * <p>
             * Overrided to defer the setValue call until after the next load. writer is defined on the store.
             * </p>
             * Sets the specified value into the field. If the value finds a match, the corresponding record text will be displayed in the field. If the value does not match the data value of an existing item, and the valueNotFoundText config option is defined, it will be displayed as the default field text. Otherwise the field will be blank (although the value will still be set).
             * @param {String} value The value to match
             * @return {Ext.form.Field} this
             */
            setValue       : function(v) {
                var idx = this.store.find(this.valueField, v);
                // if (v && this.mode == 'remote' && idx == -1) {
                if (v != null && v !== '' && idx == -1) {
                    var p = {};;
                    p["filter"] = this.valueField;
                    p[this.queryParam] = v;
                    // we add the component to the queue of async components
                    if ((this.mode == "remote" || this.store.hasLoaded == false) && (this.store.proxy != null)) {

                        var formEl = this.findParentByType('form');
                        if (formEl) {
                            var form = formEl.getForm();
                            if ((formEl.readonly == true || this.readOnly == true) && (!this.forceLoadStore)) {
                                if (Ext.isFunction(this.displayFn))
                                    var displayValue = this.displayFn.call(this, formEl.entity);
                                else
                                    displayValue = v;

                                Ext.form.ComboBox.baseclass.setValue.call(this, displayValue);
                                this.value = v;
                                this.fireEvent.defer(200, this, ["setvalue", this, displayValue, 1]);
                                return;
                            }

                            if (form._async_components == null)
                                form._async_components = [];
                            // concurrency here****
                            form._async_components.push(this.id);
                        }
                        this.store.load({
                                    scope    : this,
                                    params   : p,
                                    callback : function() {
                                        // we delete the componenent from the queue of async components, and only if the queue is empty we launch afterAction (or unMask)
                                        if (form) {
                                            form._async_components.remove(this.id);
                                            if (form._async_components.length <= 0) {
                                                // concurrency here****
                                                form._async_components = [];
                                                if (form._async_components_action) {
                                                    var action = form._async_components_action;
                                                    form._async_components_action = null;
                                                    form.afterAction(action, true);
                                                }
                                                else {
                                                    if (Ext.isFunction(formEl.unMask)) // unMask only exists for Viz.form.FormPanel
                                                        formEl.unMask();
                                                }
                                            }
                                        }
                                        // we need to check if the store still exists, the form could have been closed in the meantime.
                                        if (this.store) {
                                            idx = this.store.find(this.valueField, v);
                                            // if the value was still not found after store load, we display the initial display value.
                                            if (idx == -1) {
                                                if (Ext.isFunction(this.displayFn))
                                                    var displayValue = this.displayFn.call(this, formEl.entity);
                                                else
                                                    displayValue = v;
                                                Ext.form.ComboBox.baseclass.setValue.call(this, displayValue);
                                                this.value = v;
                                            }
                                            else
                                                Ext.form.ComboBox.baseclass.setValue.call(this, v);

                                            if (this.ownerCt instanceof Ext.form.FormPanel && this.ownerCt.trackResetOnLoad)
                                                this.originalValue = v;

                                            // custom setvalue event. We use it for example in Viz.panel.Localization
                                            this.fireEvent.defer(200, this, ["setvalue", this, this.store.getAt(idx), idx]);
                                            this.collapse();
                                        }
                                    }
                                });
                    }
                    else {
                        Ext.form.ComboBox.baseclass.setValue.call(this, v);
                        // custom setvalue event. We use it for example in Viz.panel.Localization
                        this.fireEvent.defer(200, this, ["setvalue", this, this.store.getAt(idx), idx]);
                    }
                }
                else {
                    Ext.form.ComboBox.baseclass.setValue.call(this, v);
                    // custom setvalue event. We use it for example in Viz.panel.Localization
                    this.fireEvent.defer(200, this, ["setvalue", this, this.store.getAt(idx), idx]);
                }
            },

            /**
             * Overrided to return null if the value does not exist instead of a blank string.
             */
            getValue       : function() {
                if (this.valueField) {
                    return this.value;
                }
                else {
                    return Ext.form.ComboBox.baseclass.getValue.call(this);
                }
            },

            /**
             * Overrided to make use of listHeight property
             */
            restrictHeight : function() {
                this.innerList.dom.style.height = '';
                var inner = this.innerList.dom;
                var pad = this.list.getFrameWidth('tb') + (this.resizable ? this.handleHeight : 0) + this.assetHeight;
                var h = Math.max(inner.clientHeight, inner.offsetHeight, inner.scrollHeight);
                var ha = this.getPosition()[1] - Ext.getBody().getScroll().top;
                var hb = Ext.lib.Dom.getViewHeight() - ha - this.getSize().height;
                var space = Math.max(ha, hb, this.minHeight || 0) - this.list.shadowOffset - pad - 5;
                h = Math.min(h, space, this.maxHeight);
                if (this.listHeight || this.listHeight == 0)
                    h = this.listHeight;
                this.innerList.setHeight(h);
                this.list.beginUpdate();
                this.list.setHeight(h + pad);
                this.list.alignTo.apply(this.list, [this.el].concat(this.listAlign));
                // this.list.alignTo(this.wrap, this.listAlign);
                this.list.endUpdate();
            },
            /**
             * Overrided to make use of loadMask property instead of the standard loadingText that does not look nice.
             */
            onBeforeLoad   : function() {
                if (!this.hasFocus) {
                    return;
                }
                if (this.loadMask)
                    this.innerList.update("");
                else
                    this.innerList.update(this.loadingText ? '<div class="loading-indicator">' + this.loadingText + '</div>' : '');
                this.restrictHeight();
                if (this.loadMask)
                    this.innerList.mask(this.loadingText);
                this.selectedIndex = -1;
            },
            /**
             * Overrided to make use of loadMask property instead of the standard loadingText that does not look nice.
             */
            onLoad         : function() {
                if (this.loadMask)
                    this.innerList.unmask();
                Ext.form.ComboBox.baseclass.onLoad.apply(this, arguments);
            }
        });

/**
 * Overrides to augment the maxWidth of tooltips.
 */
Ext.override(Ext.Tip, {
            maxWidth : 500
        });

Ext.override(Ext.form.Field, {
            /**
             * Overrides to return empty value as null. This is necessay when we expose nullable field. For example Elevation (Nullable double) on BuildingStorey
             * @return {}
             */
            getValue      : function() {
                if (!this.rendered) {
                    return this.value;
                }
                var v = this.el.getValue();
                if (v === this.emptyText || v === undefined || v === '') {
                    v = null;
                }
                return v;
            },
            /**
             * Overrides to render a tooltip when value is longer than form field.
             */
            initComponent : function() {
                Ext.form.Field.baseclass.initComponent.apply(this, arguments);

                this.on("render", function() {
                            this.tip = this.createToolTip();

                            // tooltip of the field itself to display more info
                            var qt = this.qtip;
                            if (qt) {
                                Ext.QuickTips.register({
                                            target    : this,
                                            title     : '',
                                            text      : qt,
                                            enabled   : true,
                                            showDelay : 20
                                        });
                            }

                        }, this);
            },

            /**
             * @private New function to create a tooltip.
             * @return {Ext.ToolTip}
             */
            createToolTip : function() {
                return new Ext.ToolTip({
                            target       : this.getEl(),
                            anchor       : 'top',
                            anchorOffset : 0,
                            renderTo     : document.body,
                            html         : '',
                            listeners    : {
                                beforeshow : function updateTipBody(tip) {
                                    try {
                                        var value = this.getRawValue();
                                        var xtype = this.getXType();
                                        if (this.el && this.el.dom && this.el.dom.type == 'password')
                                            return false;
                                        if ((xtype == 'checkbox') || (xtype == 'radio'))
                                            return false;
                                        if (this.getEl().getTextWidth(value) > this.getEl().getWidth()) {
                                            tip.setInfo(value);
                                            return true;
                                        }
                                        else
                                            return false;
                                    }
                                    catch (e) {
                                        return false;
                                    }
                                },

                                scope      : this
                            }
                        });
            },

            /**
             * Overrides to destroy the tooltip added in initComponent.
             */
            onDestroy     : function() {
                if (this.tip) {
                    this.tip.destroy();
                }
                Ext.form.Field.baseclass.onDestroy.call(this);
            }
        });

Ext.override(Ext.form.NumberField, {
            /**
             * Overrides to return empty value as null. This is necessay when we expose nullable field. For example Elevation (Nullable double) on BuildingStorey
             * @return {}
             */
            getValue : function() {
                var v = Ext.form.NumberField.superclass.getValue.call(this);
                if (v != null)
                    return this.fixPrecision(this.parseValue(Ext.form.NumberField.superclass.getValue.call(this)));
                else
                    return v;
            }

        });
/**
 * Overrides Ext.ToolTip in order to provide a helper function that preserves the carriage return of the text display in the tooltip.
 * @class Ext.ToolTip
 * @extends Ext.ToolTip
 */
Ext.override(Ext.ToolTip, {
            /**
             * Sets the tooltip value.
             * @param {String} value The value to display in the tooltip
             */
            setInfo : function(value) {
                this.body.dom.innerHTML = Ext.util.Format.nl2br(value); // value.replace(/\n/g, '<br />');
            }
        });
/**
 * Overrides Ext.Window in order so that when an animateTarget is in use, when closing the window (true close, not hide) the animateTarget is used also.
 * @class Ext.Window
 * @extends Ext.Window
 */
Ext.override(Ext.Window, {
            close     : function() {
                if (this.fireEvent('beforeclose', this) !== false) {
                    if (this.hidden) {
                        this.doClose();
                    }
                    else {
                        this.hide(this.animateTarget, this.doClose, this); // <-- change is here
                    }
                }
            },

            /**
             * Overrides to force any window to fit into available screen.
             */
            constrain : true,

            /**
             * Overrides to hide the shadow.
             */
            shadow    : false,

            saveState : function() {
                if (Ext.state.Manager && this.stateful !== false) {
                    var id = this.getStateId();
                    if (id) {
                        var state = this.getState();
                        if (this.collapsed) {
                            delete state.height;
                        }
                        if (this.fireEvent('beforestatesave', this, state) !== false) {
                            Ext.state.Manager.set(id, state);
                            this.fireEvent('statesave', this, state);
                        }
                    }
                }
            }
        });

Ext.override(Ext.grid.GridPanel, {
            getState : function() {
                var o = {
                    columns : []
                }, store = this.store, ss, gs;

                for (var i = 0, c; (c = this.colModel.config[i]); i++) {
                    o.columns[i] = {
                        id    : c.id,
                        width : c.width
                    };
                    if (c.hidden) {
                        o.columns[i].hidden = true;
                    }
                    if (c.sortable) {
                        o.columns[i].sortable = true;
                    }
                }
                if (store) {
                    ss = store.getSortState();
                    if (ss) {
                        o.sort = ss;
                    }
                    if (store.getGroupState) {
                        gs = store.getGroupState();
                        if (gs) {
                            o.group = gs;
                        }
                    }
                }
                if (this.savedView) {
                    o.savedView = this.savedView;
                }
                if (this.savedViewGlobal) {
                    o.savedViewGlobal = this.savedViewGlobal;
                }
                if (this.showAllItems) {
                    o.showAllItems = this.showAllItems;
                }
                return o;
            }
        });
/**
 * Overrides Ext.ux.GridFilters in order to correct a few bugs and gives access to the filters parameter in a PagingParamater object.
 * @class Ext.ux.grid.GridFilters
 * @extend Ext.ux.grid.GridFilters
 */
Ext.override(Ext.ux.grid.GridFilters, {
            /**
             * Override to work with auto configured store, so we need to delay this.addFilters untill the store is loaded.
             */
            init           : function(grid) {
                if (grid instanceof Ext.grid.GridPanel) {
                    this.grid = grid;
                    this.grid.addEvents({
                                'filteravailable' : true
                            });
                    this.bindStore(this.grid.getStore(), true);
                    // assumes no filters were passed in the constructor, so try and use ones from the colModel
                    // ****** begin change ******
                    // this.addFilters(this.grid.getColumnModel()); <-- change here
                    if (this.filters.getCount() == 0) { // <-- change here
                        if (this.store && this.store.hasLoaded === true) {
                            this.addFilters(this.grid.getColumnModel());
                        }
                        else {
                            this.store.on("load", function() {
                                        this.addFilters(this.grid.getColumnModel());
                                        if (this.grid.defaultFilter) {
                                            var curState = Ext.state.Manager.get(this.grid.stateId);
                                            if (curState.lastFilters) {
                                                this.grid.loadSerializedFilter(curState.lastFilters);
                                            }
                                        }
                                        /*
                                         * this.applyStateAfterload(this.grid, { filters : this.gridStateFilters });
                                         */
                                        this.grid.fireEvent('filteravailable', this);
                                    }, this, {
                                        single : true
                                    });
                            // this.store.on("load", this.addFilters.createDelegate(this, [this.grid.getColumnModel()]), this, { // <-- change here
                            // single : true
                            // });
                        }
                    }
                    // ****** end change ******
                    this.grid.filters = this;
                    this.grid.addEvents({
                                'filterupdate' : true
                            });

                    this.grid.on({
                                scope              : this,
                                beforestaterestore : this.applyState,
                                beforestatesave    : this.saveState,
                                beforedestroy      : this.destroy,
                                reconfigure        : this.onReconfigure
                            });
                    if (grid.rendered) {
                        this.onRender();
                    }
                    else {
                        grid.on({
                                    scope  : this,
                                    single : true,
                                    render : this.onRender
                                });
                    }
                }
                else if (grid instanceof Ext.PagingToolbar) {
                    this.toolbar = grid;
                }
            },
            /*
             * applyState : function(grid, state) { if (state.filters) { this.gridStateFilters = state.filters; } }, applyStateAfterload : function(grid, state) { var key, filter; this.applyingState = true; this.clearFilters(); if (state.filters) { for (key in state.filters) { filter = this.filters.get(key); if (filter) { filter.setValue(state.filters[key]); filter.setActive(true); } } this.deferredUpdate.cancel(); // if (this.local) { <-- we need to reload the store after applying the filter. this.reload.defer(200, this); // } delete this.applyingState; delete state.filters; } }, onStateChange : function(event, filter) { if (event === 'serialize') { return; } if (filter == this.getMenuFilter()) { this.menu.setChecked(filter.active, false); } if ((this.autoReload || this.local) &&
             * !this.applyingState) { this.deferredUpdate.delay(this.updateBuffer); // <- change here because the treemulticomselect is seting the value via an event. } this.updateColumnHeadings(); if (!this.applyingState) { this.grid.saveState.defer(200, this.grid); } this.grid.fireEvent('filterupdate', this, filter); },
             */
            /**
             * Override because there is a bug when trying to get the type of the fields (this.store.fields.get(dI).type.type instead of this.store.fields.get(dI).type)
             */
            addFilters     : function(filters) {
                if (filters) {
                    var i, len, filter, cm = false, dI;
                    if (filters instanceof Ext.grid.ColumnModel) {
                        filters = filters.config;
                        cm = true;
                    }
                    for (i = 0, len = filters.length; i < len; i++) {
                        filter = false;
                        if (cm) {
                            dI = filters[i].dataIndex;
                            filter = filters[i].filter || filters[i].filterable;
                            if (filter) {
                                filter = (filter === true) ? {} : filter;
                                Ext.apply(filter, {
                                            dataIndex : dI
                                        });
                                // filter type is specified in order of preference:
                                // filter type specified in config
                                // type specified in store's field's type config
                                filter.type = filter.type || (this.store.fields ? this.store.fields.get(dI).type.type : 'string'); // <-- chenge here
                            }
                        }
                        else {
                            filter = filters[i];
                        }
                        // if filter config found add filter for the column
                        if (filter) {
                            this.addFilter(filter);
                        }
                    }
                }
            },
            /**
             * Override because we want to apply the filter as filters property of PagingParameter.
             */
            buildQuery     : function(filters) {
                var p = {}, i, f, root, dataPrefix, key, tmp, len = filters.length;
                tmp = [];
                for (i = 0; i < len; i++) {
                    f = filters[i];
                    tmp.push(Ext.apply({}, {
                                field : f.field,
                                data  : f.data
                            }));
                }
                // only build if there is active filter
                if (tmp.length > 0) {
                    // p[this.paramPrefix] = Ext.util.JSON.encode(tmp); // <-- change here
                    p[this.paramPrefix] = tmp; // Ext.util.JSON.encode(tmp);
                }
                return p;
            },
            /**
             * Override because with metadata for a store, a field boolean appears with type bool instead
             */
            getFilterClass : function(type) {
                // map the supported Ext.data.Field type values into a supported filter
                switch (type) {
                    case 'auto' :
                        type = 'string';
                        break;
                    case 'int' :
                        type = 'int';
                        break;
                    case 'float' :
                        type = 'numeric';
                        break;
                    case 'bool' :
                        type = 'boolean';
                        break;
                }
                return Ext.ux.grid.filter[type.substr(0, 1).toUpperCase() + type.substr(1) + 'Filter'];
            },
            /**
             * Override because when using a combo filter we need to expose 2 differents dataIndex
             * @return {}
             */
            getFilterData  : function() {
                var filters = [], i, len;
                this.filters.each(function(f) {
                            if (f.active) {
                                var d = [].concat(f.serialize());
                                for (i = 0, len = d.length; i < len; i++) {
                                    filters.push({
                                                field : d[i].dataIndex || f.dataIndex, // <-- change here
                                                data  : d[i]
                                            });
                                }
                            }
                        });
                return filters;
            },
            /**
             * Overrides because this function seems to be call by the DelayedTask after the grid was destroyed, so we need to make sure store exists
             */
            reload         : function() {
                if (this.local) {
                    this.grid.store.clearFilter(true);
                    this.grid.store.filterBy(this.getRecordFilter());
                }
                else {
                    var start, store = this.grid.store;
                    this.deferredUpdate.cancel();
                    if (store) {// <-- change here
                        if (this.toolbar) {
                            start = store.paramNames.start;
                            if (store.lastOptions && store.lastOptions.params && store.lastOptions.params[start]) {
                                store.lastOptions.params[start] = 0;
                            }
                        }
                        store.reload();
                    }
                }
            },
            /**
             * Saves the state of all active filters
             * @param {Object} grid
             * @param {Object} state
             * @return {Boolean}
             */
            saveState      : function(grid, state) {
                var filters = {};
                this.filters.each(function(filter) {
                            if (filter.active) {
                                filters[filter.dataIndex] = filter.getValue();
                                var colIndex = grid.getColumnModel().findColumnIndex(filter.dataIndex);
                                var column = state.columns.filter(function(el, i, arr) {
                                            return el.id == filter.dataIndex || el.id == colIndex;
                                        })[0];
                                switch (filter.type) {
                                    case 'treecombomultiselect' :
                                        if (column)
                                            column['path'] = filter.getPathAsArray();
                                        filters[filter.dataIndex + 'Path'] = filter.getPathAsArray();
                                        break;
                                    default :
                                }
                            }
                        }, this);
                return (state.filters = filters);
            }

        });

Ext.override(Ext.ux.grid.filter.StringFilter, {
            /**
             * Overrides because in our implementation getValue() for an empty textfield will return null and not ''
             */
            isActivatable : function() {
                return !Ext.isEmpty(this.inputItem.getValue());
            }

        });

Ext.override(Ext.grid.Column, {
            /**
             * Overrides because we may have an editor configuration for a column that was not instanctiated and so we don't want to execute ed.destroy()
             */
            setEditor : function(editor) {
                var ed = this.editor;
                if (ed && (ed instanceof Ext.Component)) { // <- change here
                    if (ed.gridEditor) {
                        ed.gridEditor.destroy();
                        delete ed.gridEditor;
                    }
                    else {
                        ed.destroy();
                    }
                }
                this.editor = null;
                if (editor) {
                    // not an instance, create it
                    if (!editor.isXType) {
                        editor = Ext.create(editor, 'textfield');
                    }
                    this.editor = editor;
                }
            }
        });

// Ext.override(Ext.grid.GroupingView, {
/**
 * Overrides because we want to fireevents on group summaries.
 * @param {} name
 * @param {} e
 */
/*
 * processEvent : function(name, e) { var hd = e.getTarget('.x-grid-group-hd', this.mainBody); if (hd) { // group value is at the end of the string var field = this.getGroupField(), prefix = this.getPrefix(field), groupValue = hd.id.substring(prefix.length), emptyRe = new RegExp('gp-' + Ext.escapeRe(field) + '--hd'); // remove trailing '-hd' groupValue = groupValue.substr(0, groupValue.length - 3); // also need to check for empty groups if (groupValue || emptyRe.test(hd.id)) { this.grid.fireEvent('group' + name, this.grid, field, groupValue, e); } if (name == 'mousedown' && e.button == 0) { this.toggleGroup(hd.parentNode); } } } });
 */
Ext.override(Ext.tree.TreeNode, {
            /**
             * Finds the first parent by a custom function. The parent matches if the function passed returns true.
             * @param {Function} fn A function which must return true if the passed Node is the required Node.
             * @param {Object} scope (optional) The scope (this reference) in which the function is executed. Defaults to the Node being tested.
             * @return {Node} The found parent or null if none was found
             */
            findParentBy    : function(fn, scope) {
                var n = this.parentNode;
                var res;
                if (fn.call(scope || n, n) === true) {
                    return n;
                }
                else {
                    res = n.findParentBy(fn, scope);
                    if (res != null) {
                        return res;
                    }
                }
                return null;
            },
            /**
             * Overrides the getPath original function (Ext.data.Node) to exclude hidden node from beeing exposed in the path (espacially root node)
             * @param {} attr
             * @return {}
             */
            getPath         : function(attr) {
                attr = attr || "id";
                var p = this.parentNode;
                var b = [this.attributes[attr]];
                while (p && !p.excludeFromPath()) {
                    b.unshift(p.attributes[attr]);
                    p = p.parentNode;
                }
                var sep = this.getOwnerTree().pathSeparator;
                return b.join(sep);
            },

            /**
             * Original GetPath function used in tree state
             * @param {} attr
             * @return {}
             */
            getPathOriginal : function(attr) {
                attr = attr || "id";
                var p = this.parentNode;
                var b = [this.attributes[attr]];
                while (p) {
                    b.unshift(p.attributes[attr]);
                    p = p.parentNode;
                }
                var sep = this.getOwnerTree().pathSeparator;
                return sep + b.join(sep);
            },

            /**
             * Overrides to provide this new function
             * @return {Boolean}
             */
            excludeFromPath : function() {
                if (this == this.getOwnerTree().root && this.getOwnerTree().rootVisible == false)
                    return true;
                else
                    return false;
            }
        });

Ext.override(Ext.form.CompositeField, {
            /**
             * Overrides to avoid that errors are sorted by the field name
             */
            sortErrors    : Ext.emptyFn,
            /**
             * Fix for the compositefield that was rendered too late when the deferedRender was set to true.
             */
            initComponent : function() {
                // Ext.form.CompositeField.baseclass.initComponent.apply(this, arguments);
                var layout = this.layoutCT == undefined ? 'hbox' : this.layoutCT;
                var layoutConfig = this.layoutConfigCT == undefined ? {} : this.layoutConfigCT;

                // var layoutConfig = this.layoutConfigCT || {};
                var labels = [], items = this.items, item;

                for (var i = 0, j = items.length; i < j; i++) {
                    item = items[i];

                    if (!Ext.isEmpty(item.ref)) {
                        item.ref = '../' + item.ref;
                    }

                    labels.push(item.fieldLabel);

                    // apply any defaults
                    Ext.applyIf(item, this.defaults);

                    // apply default margins to each item except the last
                    if (!(i == j - 1 && this.skipLastItemMargin)) {
                        Ext.applyIf(item, {
                                    margins : this.defaultMargins
                                });
                    }
                }

                this.fieldLabel = this.fieldLabel || this.buildLabel(labels);

                /**
                 * @property fieldErrors
                 * @type Ext.util.MixedCollection MixedCollection of current errors on the Composite's subfields. This is used internally to track when to show and hide error messages at the Composite level. Listeners are attached to the MixedCollection's add, remove and replace events to update the error icon in the UI as errors are added or removed.
                 */
                this.fieldErrors = new Ext.util.MixedCollection(true, function(item) {
                            return item.field;
                        });

                this.fieldErrors.on({
                            scope   : this,
                            add     : this.updateInvalidMark,
                            remove  : this.updateInvalidMark,
                            replace : this.updateInvalidMark
                        });

                Ext.form.CompositeField.superclass.initComponent.apply(this, arguments);

                /**
                 * @property innerCt
                 * @type Ext.Container A container configured with hbox layout which is responsible for laying out the subfields
                 */
                this.innerCt = new Ext.Container({
                            layout         : layout,
                            layoutConfig   : layoutConfig,
                            items          : this.items,
                            cls            : 'x-form-composite',
                            defaultMargins : '0 3 0 0',
                            ownerCt        : this
                        });

                // this.innerCt.ownerCt = undefined;

                var fields = this.innerCt.findBy(function(c) {
                            return c.isFormField;
                        }, this);

                /**
                 * @property items
                 * @type Ext.util.MixedCollection Internal collection of all of the subfields in this Composite
                 */
                this.items = new Ext.util.MixedCollection();
                this.items.addAll(fields);
            }

        });

if (Ext.version == "3.2.0") {
    Ext.override(Ext.Component, {
                /**
                 * Overrides to correct a temporary bug in v 3.2.0 (corrected in v 3.3.0).
                 * @param {} xtype
                 * @param {} shallow
                 * @return {}
                 */
                findParentByType : function(xtype, shallow) {
                    return this.findParentBy(function(c) {
                                return c.isXType(xtype, shallow);
                            });
                }
            });
    Ext.override(Ext.form.Radio, {
                /**
                 * To be removed if version is 3.3.1 or higher Sets either the checked/unchecked status of this Radio, or, if a string value is passed, checks a sibling Radio of the same name whose value is the value specified.
                 * @param value {String/Boolean} Checked value, or the value of the sibling radio button to check.
                 * @return {Ext.form.Field} this
                 */
                setValue : function(v) {
                    var checkEl, els, radio;
                    if (typeof v == 'boolean') {
                        Ext.form.Radio.superclass.setValue.call(this, v);
                    }
                    else if (this.rendered) {
                        checkEl = this.getCheckEl();
                        radio = checkEl.child('input[name=' + this.el.dom.name + '][value=' + v + ']', true);
                        if (radio) {
                            Ext.getCmp(radio.id).setValue(true);
                        }
                    }
                    if (this.rendered && this.checked) {
                        checkEl = checkEl || this.getCheckEl();
                        els = this.getCheckEl().select('input[name=' + this.el.dom.name + ']');
                        els.each(function(el) {
                                    if (el.dom.id != this.id) {
                                        Ext.getCmp(el.dom.id).setValue(false);
                                    }
                                }, this);
                    }
                    return this;
                }
            });

    Ext.override(Ext.form.CompositeField, {
                /**
                 * To be removed if version is 3.3.1 or higher Overrides the way the combined message is built so we do not expose fieldName to the user but instead fieldLabel. This is specific to version 3.2.0 and should be deleted in above version as it is already integrated.
                 * @param {Ext.form.Field} field The field that was marked invalid
                 * @param {String} message The error message
                 */
                onFieldMarkInvalid : function(field, message) {
                    var name = field.getName(), error = {
                        field : field.fieldLabel || name,
                        error : message
                    };
                    this.fieldErrors.replace(name, error);
                    field.el.addClass(field.invalidClass);
                },

                /**
                 * Fix for the compositefield that was rendered too late when the deferedRender was set to true.
                 */
                initComponent      : Ext.form.CompositeField.prototype.initComponent.createSequence(function() {
                            var layout = this.layoutCT == undefined ? 'hbox' : this.layoutCT;
                            var layoutConfig = this.layoutConfigCT == undefined ? {} : this.layoutConfigCT;

                            // var layoutConfig = this.layoutConfigCT || {};
                            /**
                             * @property innerCt
                             * @type Ext.Container A container configured with hbox layout which is responsible for laying out the subfields
                             */
                            this.innerCt = new Ext.Container({
                                        layout         : layout,
                                        layoutConfig   : layoutConfig,
                                        items          : this.items,
                                        cls            : 'x-form-composite',
                                        defaultMargins : '0 3 0 0'
                                    });

                            var fields = this.innerCt.findBy(function(c) {
                                        return c.isFormField;
                                    });

                            /**
                             * @property items
                             * @type Ext.util.MixedCollection Internal collection of all of the subfields in this Composite
                             */
                            this.items = new Ext.util.MixedCollection();
                            this.items.addAll(fields);
                        }),

                /**
                 * @private Creates an internal container using hbox and renders the fields to it
                 */
                onRender           : function(ct, position) {
                    if (!this.el) {
                        var innerCt = this.innerCt;
                        innerCt.render(ct);

                        this.el = innerCt.getEl();

                        // if we're combining subfield errors into a single message, override the markInvalid and clearInvalid
                        // methods of each subfield and show them at the Composite level instead
                        if (this.combineErrors) {
                            this.eachItem(function(field) {
                                        Ext.apply(field, {
                                                    markInvalid  : this.onFieldMarkInvalid.createDelegate(this, [field], 0),
                                                    clearInvalid : this.onFieldClearInvalid.createDelegate(this, [field], 0)
                                                });
                                    });
                        }

                        // set the label 'for' to the first item
                        var l = this.el.parent().parent().child('label', true);
                        if (l) {
                            l.setAttribute('for', this.items.items[0].id);
                        }
                    }

                    Ext.form.CompositeField.superclass.onRender.apply(this, arguments);
                }
            });
}

Ext.override(Ext.TabPanel, {
            // Make sure the tabpanel header is hidden if an item is set to hidden.
            initTab : Ext.TabPanel.prototype.initTab.createSequence(function(item, index) {
                        if (item.hidden)
                            this.hideTabStripItem(index);
                    })
        });
/**
 * Override for changing the send parameters from the calendar so they are real dates.
 */
Ext.override(Ext.ensible.cal.CalendarView, {
            getStoreParams : function() {
                return {
                    start : this.viewStart,
                    end   : this.viewEnd
                };
            },
            reloadStore    : function(o) {
                o = Ext.isObject(o) ? o : {};
                o.params = o.params || {};
                // Ext.apply(o.params, this.getStoreParams());
                var serviceParams = this.store.serviceParams;
                Ext.apply(serviceParams, this.getStoreParams());
                this.store.proxy.serviceParams = serviceParams;
                this.store.serviceParams = serviceParams;
                this.store.load(o);
            }
        });
/**
 * Override to add a tooltip to the day representation
 */
Ext.override(Ext.ensible.cal.DayBodyView, {
            getEventTemplate : function() {
                var qtip = '{[Ext.util.Format.htmlEncode(values.qtip)]}<br/>{StartDate:date("d/m/Y H:i:s")} - {EndDate:date("d/m/Y H:i:s")}';
                if (!this.eventTpl) {
                    this.eventTpl = !(Ext.isIE || Ext.isOpera) ? new Ext.XTemplate('<div ext:qtip="' + qtip + '" id="{[Ext.util.Format.htmlEncode(values._elId)]}" class="{[Ext.util.Format.htmlEncode(values._extraCls)]} ext-cal-evt ext-cal-evr" style="left: {[Ext.util.Format.htmlEncode(values._left)]}%; width: {[Ext.util.Format.htmlEncode(values._width)]}%; top: {[Ext.util.Format.htmlEncode(values._top)]}px; height: {[Ext.util.Format.htmlEncode(values._height)]}px;">', '<div class="ext-evt-bd">', this.getEventBodyMarkup(), '</div>', this.enableEventResize ? '<div class="ext-evt-rsz"><div class="ext-evt-rsz-h">&#160;</div></div>' : '', '</div>') : new Ext.XTemplate('<div ext:qtip="' + qtip
                                    + '" id="{[Ext.util.Format.htmlEncode(values._elId)]}" class="ext-cal-evt {[Ext.util.Format.htmlEncode(values._extraCls)]}" style="left: {[Ext.util.Format.htmlEncode(values._left)]}%; width: {[Ext.util.Format.htmlEncode(values._width)]}%; top: {[Ext.util.Format.htmlEncode(values._top)]}px;">', '<div class="ext-cal-evb">&#160;</div>', '<dl style="height: {[Ext.util.Format.htmlEncode(values._height)]}px;" class="ext-cal-evdm">', '<dd ext:qtip="' + qtip + '" class="ext-evt-bd">', this.getEventBodyMarkup(), '</dd>', this.enableEventResize ? '<div class="ext-evt-rsz"><div class="ext-evt-rsz-h">&#160;</div></div>' : '', '</dl>', '<div class="ext-cal-evb">&#160;</div>', '</div>');
                    this.eventTpl.compile();
                }
                return this.eventTpl;
            }

        });
/**
 * Override to add a tooltip to the month representation
 */
Ext.override(Ext.ensible.cal.MonthView, {
            getEventTemplate : function() {
                if (!this.eventTpl) {
                    var tpl, body = this.getEventBodyMarkup();
                    var qtip = '{[Ext.util.Format.htmlEncode(values.qtip)]}<br/>{StartDate:date("d/m/Y H:i:s")} - {EndDate:date("d/m/Y H:i:s")}';
                    tpl = !(Ext.isIE || Ext.isOpera) ? new Ext.XTemplate('<div ext:qtip="' + qtip + '" class="{[Ext.util.Format.htmlEncode(values._extraCls)]} {[Ext.util.Format.htmlEncode(values.spanCls)]} ext-cal-evt ext-cal-evr">', body, '</div>') : new Ext.XTemplate('<tpl if="_renderAsAllDay">', '<div ext:qtip="' + qtip + '" class="{[Ext.util.Format.htmlEncode(values._extraCls)]} {[Ext.util.Format.htmlEncode(values.spanCls)]} ext-cal-evt ext-cal-evo">', '<div class="ext-cal-evm">', '<div class="ext-cal-evi">', '</tpl>', '<tpl if="!_renderAsAllDay">', '<div ext:qtip="' + qtip + '" class="{[Ext.util.Format.htmlEncode(values._extraCls)]} ext-cal-evt ext-cal-evr">', '</tpl>', body, '<tpl if="_renderAsAllDay">', '</div>', '</div>', '</tpl>', '</div>');
                    tpl.compile();
                    this.eventTpl = tpl;
                }
                return this.eventTpl;
            }
        });

/**
 * Override to map the business entity event with the calendar.
 */
Ext.ensible.cal.EventMappings = {
    EventId          : {
        name    : 'Key',
        mapping : 'Key',
        type    : 'string'
    },
    CalendarId       : {
        name    : 'CalendarId',
        mapping : 'CalendarId',
        type    : 'string'
    },
    Title            : {
        name    : 'Title',
        mapping : 'Title',
        type    : 'string'
    },
    StartDate        : {
        name       : 'StartDate',
        mapping    : 'StartDate',
        type       : 'date',
        dateFormat : 'c'
    },
    InitialStartDate : {
        name       : 'InitialStartDate',
        mapping    : 'InitialStartDate',
        type       : 'date',
        dateFormat : 'c'
    },
    EndDate          : {
        name       : 'EndDate',
        mapping    : 'EndDate',
        type       : 'date',
        dateFormat : 'c'
    },
    RRule            : {
        name    : 'RecurRule',
        mapping : 'RecurRule'
    }, // not currently used
    Location         : {
        name    : 'Location',
        mapping : 'Location',
        type    : 'string'
    },
    Notes            : {
        name    : 'Notes',
        mapping : 'Notes',
        type    : 'string'
    },
    Url              : {
        name    : 'Url',
        mapping : 'Url',
        type    : 'string'
    },
    IsAllDay         : {
        name    : 'IsAllDay',
        mapping : 'IsAllDay',
        type    : 'boolean'
    },
    Reminder         : {
        name    : 'Reminder',
        mapping : 'Reminder',
        type    : 'string'
    },
    Category         : {
        name    : 'Category',
        mapping : 'Category',
        type    : 'string'
    },
    TimeZoneId       : {
        name    : 'TimeZoneId',
        mapping : 'TimeZoneId',
        type    : 'string'
    },
    TimeZoneOffset   : {
        name    : 'TimeZoneOffset',
        mapping : 'TimeZoneOffset',
        type    : 'string'
    }
};
// Don't forget to reconfigure!
Ext.ensible.cal.EventRecord.reconfigure();

/**
 * Add a new ptype for Ext.slider.Tip
 */
Ext.preg('slidertip', Ext.slider.Tip);

/**
 * IE 9 Fix
 */
if ((typeof Range !== "undefined") && !Range.prototype.createContextualFragment) {
    Range.prototype.createContextualFragment = function(html) {
        var frag = document.createDocumentFragment(), div = document.createElement("div");
        frag.appendChild(div);
        div.outerHTML = html;
        return frag;
    };
}

/**
 * IE 9 Fix for Slider not beeing draggable.
 */
/*Ext.override(Ext.dd.DragTracker, {
            onMouseMove : function(e, target) {
                var isIE9 = Ext.isIE && (/msie 9/.test(navigator.userAgent.toLowerCase())) && document.documentMode != 6;
                if (this.active && Ext.isIE && !isIE9 && !e.browserEvent.button) {
                    e.preventDefault();
                    this.onMouseUp(e);
                    return;
                }
                e.preventDefault();
                var xy = e.getXY(), s = this.startXY;
                this.lastXY = xy;
                if (!this.active) {
                    if (Math.abs(s[0] - xy[0]) > this.tolerance || Math.abs(s[1] - xy[1]) > this.tolerance) {
                        this.triggerStart(e);
                    }
                    else {
                        return;
                    }
                }
                this.fireEvent('mousemove', this, e);
                this.onDrag(e);
                this.fireEvent('drag', this, e);
            }
        });*/

/**
 * Create a dynamic style
 * @param {String} cssText The CSS text.
 * @param {Boolean} id element Id.
 * @return {styleSheet} CSS.
 */
Ext.util.CSS.createStyleSheet = function(cssText, id) {
    var doc = document;
    var ss;
    var head = doc.getElementsByTagName("head")[0];
    var rules;
    var newlyCreatedRule = false;
    if (cssText.indexOf("dynamicimage") != -1) {
        rules = doc.getElementById("dynamicImageStyle");
        if (!rules) {
            rules = doc.createElement("style");
            rules.setAttribute("type", "text/css");
            rules.setAttribute("id", "dynamicImageStyle");
            newlyCreatedRule = true;
        }
    }
    else {
        rules = doc.createElement("style");
        rules.setAttribute("type", "text/css");
        if (id) {
            rules.setAttribute("id", id);
        }
        newlyCreatedRule = true;
    }
    if (Ext.isIE8 || Ext.isIE7) {
        if (newlyCreatedRule === true) {
            head.appendChild(rules);
        }
        ss = rules.styleSheet;
        if (!ss.disabled) {
            ss.addRule(cssText.substring(0, cssText.indexOf('{') - 1), cssText.substring(cssText.indexOf('{') + 1, cssText.indexOf('}')).trim(), 0);
        }
    }
    else {
        try {
            if (newlyCreatedRule === true) {
                rules.appendChild(doc.createTextNode(cssText));
                head.appendChild(rules);
            }
            else {
                rules.sheet.insertRule(cssText, 0);
            }
        }
        catch (e) {

        }
        ss = rules.styleSheet ? rules.styleSheet : (rules.sheet || doc.styleSheets[doc.styleSheets.length - 1]);
    }
    this.cacheStyleSheet(ss);
    return ss;
};

/**
 * Gets an an individual CSS rule by selector(s)
 * @param {String} selector The CSS selector beginning.
 * @param {Boolean} refreshCache true to refresh the internal cache if you have recently updated any rules or added styles dynamically.
 * @return {Array} An array of CSS rules matching the selector.
 */
Ext.util.CSS.getArrayMatch = function(selector, refreshCache, start, limit) {
    var rs = this.getRules(refreshCache);
    var retVal = [];

    var count = 0;
    for (var i in rs) {
        if ((i + '').startsWith(selector)) {
            var rule = rs[i];
            var label = $lang(i.replaceAll(selector, 'msg_icon_label_').replaceAll('-', '_'));
            if (i.startsWith('.'))
                i = i.substring(1);
            if ((!Ext.isNumber(start) || !Ext.isNumber(limit) || (count >= start && count < limit)))
                retVal.push([i, label, rule]);

            count += 1;
        }
    }
    return retVal;
};

/**
 * Override the TwinTriggerField getTriggerWidth to fix a bug when in readonly mode.
 */
Ext.override(Ext.form.TwinTriggerField, {
            // Fix for readonly mode
            getTriggerWidth : function() {
                var tw = 0;
                Ext.each(this.triggers, function(t, index) {
                            var triggerIndex = 'Trigger' + (index + 1), w = t.getWidth();
                            // this is where we added a check for readonly
                            if (w === 0 && !this.readOnly && !this['hidden' + triggerIndex]) {
                                tw += this.defaultTriggerWidth;
                            }
                            else {
                                tw += w;
                            }
                        }, this);
                return tw;
            }
        });
/**
 * Override to fix GroupingView when grouping is disabled and column are resized.
 */

Ext.grid.GroupingView.override({
            hasGroupRows   : function() {
                var fc = this.mainBody.dom.firstChild;
                return fc && fc.nodeType == 1 && fc.className.indexOf("x-grid-group") > -1;
            },
            getGroups      : function() {
                return this.hasGroupRows() ? this.mainBody.dom.childNodes : [];
            },
            beforeMenuShow : function() {
                var field = this.getGroupField();
                var g = this.hmenu.items.get('groupBy');
                if (g) {
                    g.setDisabled(this.cm.config[this.hdCtxIndex].groupable === false);
                }
                var s = this.hmenu.items.get('showGroups');
                if (s) {
                    s.setDisabled(!field && this.cm.config[this.hdCtxIndex].groupable === false);
                    s.setChecked(!!field, true);
                }
            }
        });
/**
 * Override because of bug in PivotGrid
 */
Ext.grid.GridView.override({
            scrollToTop : function() {
                var dom = this.scroller.dom;
                if (dom) {
                    dom.scrollTop = 0;
                    dom.scrollLeft = 0;
                }
            }
        })
/**
 * CardLayout override which fades the cards in/out
 */
/*
 * Ext.override(Ext.layout.CardLayout, { setActiveItem : function(item) { var ai = this.activeItem, ct = this.container; item = ct.getComponent(item); if (ai != item) { if (item.rendered && this.animate) { Ext.Fx.syncFx(); // Correct top position of card because they have to occupy the same space // during animation. var n = item.getEl(); n.setStyle({ position : 'absolute', top : (ai.findParentByType('panel').body.getPadding('t') + this.container.getLayoutTarget().getPadding('t')) + 'px' }); if (ai) { ai.getEl().fadeOut({ useDisplay : true, callback : Ext.Component.prototype.hide, scope : ai }); } ai = item; item.show(); this.container.doLayout(); n.fadeIn({ callback : function() { n.setStyle({ position : '' }); } }); Ext.Fx.sequenceFx(); } else { if (ai) { ai.hide(); if (ai.hidden !==
 * true) { return false; } ai.fireEvent('deactivate', ai); } var layout = item.doLayout && (this.layoutOnCardChange || !item.rendered); this.activeItem = item; delete item.deferLayout; item.show(); this.layout(); if (layout) { item.doLayout(); } item.fireEvent('activate', item); } } } });
 */

/**
 * Override to add the insertAtCursor function to TextArea
 */
Ext.override(Ext.form.TextArea, {
            insertAtCursor : function(v) {
                if (Ext.isIE) {
                    this.el.focus();
                    var sel = document.selection.createRange();
                    sel.text = v;
                    sel.moveEnd('character', v.length);
                    sel.moveStart('character', v.length);
                }
                else {
                    var startPos = this.el.dom.selectionStart;
                    var endPos = this.el.dom.selectionEnd;
                    this.el.dom.value = this.el.dom.value.substring(0, startPos) + v + this.el.dom.value.substring(endPos, this.el.dom.value.length);

                    this.el.focus();
                    this.el.dom.setSelectionRange(endPos + v.length, endPos + v.length);
                }
            }
        });

/**
 * Override to handle empty node in treeloader exception
 */
Ext.override(Ext.tree.TreeLoader, {
            handleFailure : function(response, params) {
                this.transId = false;
                var a = response.argument || params;// <- this is the change
                this.fireEvent("loadexception", this, a.node, response);
                this.runCallback(a.callback, a.scope || a.node, [a.node]);
            }
        });
/**
 * Override Pivot Axis to handle renderer
 */
Ext.override(Ext.grid.PivotAxis, {
            getRecordInfo : function(record) {
                var dimensions = this.dimensions, length = dimensions.length, data = {}, dimension, dataIndex, i, renderers = {};

                // get an object containing just the data we are interested in based on the configured dimensions
                for (i = 0; i < length; i++) {
                    dimension = dimensions[i];
                    dataIndex = dimension.dataIndex;

                    data[dataIndex] = record.get(dataIndex);
                    if (dimension.renderer) {// <- Change here
                        data[dataIndex] = dimension.renderer(data[dataIndex])
                        renderers[dataIndex] = dimension.renderer;
                    }
                }

                // creates a specialised matcher function for a given tuple. The returned function will return
                // true if the record passed to it matches the dataIndex values of each dimension in this axis
                var createMatcherFunction = function(data, renderers) {
                    return function(record) {
                        for (var dataIndex in data) {
                            if (!Ext.isFunction(renderers[dataIndex]) && record.get(dataIndex) != data[dataIndex]) {
                                return false;
                            }
                            else if (Ext.isFunction(renderers[dataIndex]) && renderers[dataIndex](record.get(dataIndex)) != data[dataIndex]) {
                                return false;
                            }
                        }

                        return true;
                    };
                };

                return {
                    data    : data,
                    matcher : createMatcherFunction(data, renderers)
                };
            }
        });

/**
 * Override Pivot Grid to handle destroying of axis.
 */
Ext.override(Ext.grid.PivotGrid, {
            onDestroy : function() {
                if (this.topAxis) {
                    this.topAxis.destroy();
                    this.topAxis = null;
                }

                if (this.leftAxis) {
                    this.leftAxis.destroy();
                    this.leftAxis = null;
                }

                Ext.grid.PivotGrid.baseclass.onDestroy.call(this);
            }

        });

Ext.override(Ext.slider.Thumb, {
            /**
             * @private Destroys the thumb
             */
            destroy : function() {
                // Memory leak fixes.

                // Unhook createDelegate usages coming from Thumb's constructor.
                if (this.tracker) {
                    this.tracker.onBeforeDragStart = null;
                    this.tracker.onDragStart = null;
                    this.tracker.onDrag = null;
                    this.tracker.onDragEnd = null;
                }

                // Release the slider.
                this.slider = null;

                // Original code:
                Ext.destroyMembers(this, 'tracker', 'el');
            }
        });

Ext.override(Ext.Component, {
            onDestroy : function() {
                if (this.initialConfig) {
                    // Memory leak fix.
                    this.initialConfig = null;
                }
            }
        });

Ext.override(Ext.TabPanel, {
            // Memory leak fix.
            beforeDestroy : function() {
                Ext.destroy(this.leftRepeater, this.rightRepeater);
                this.deleteMembers('strip', 'edge', 'scrollLeft', 'scrollRight', 'stripWrap');
                this.activeTab = null;
                Ext.TabPanel.superclass.beforeDestroy.apply(this);
                this.getState = null; // killing a closure. memory leak fix.
            }
        });

/**
 * Fix for IE 8 drag and drop issues
 * @param {} g
 * @return {}
 */
Ext.lib.Region.getRegion = function(g) {
    if (g) {
        var i = Ext.lib.Dom.getXY(g), f = i[1], h = i[0] + g.offsetWidth, d = i[1] + g.offsetHeight, e = i[0];
        return new Ext.lib.Region(f, h, d, e)
    }
    else {
        return null;
    }
};

/**
 * Original Ext getXY error
 * @type
 */
// Ext.lib.Dom.getXYOriginal = Ext.lib.Dom.getXY;
/**
 * Fix for firefox errors in getXY
 * @param {} el
 * @return {}
 */
/*
 * Ext.lib.Dom.getXY = function(el) { el = Ext.getDom(el); if (el) return Ext.lib.Dom.getXYOriginal(el); else return [0, 0]; }
 */

/**
 * Fix Spiner UI glitches
 */
Ext.override(Ext.ux.Spinner, {

            doRender : function(ct, position) {
                var el = this.el = this.field.getEl();
                var f = this.field;

                if (!f.wrap) {
                    f.wrap = this.wrap = el.wrap({
                                cls : "x-form-field-wrap"
                            });
                }
                else {
                    this.wrap = f.wrap.addClass('x-form-field-wrap');
                }

                this.trigger = this.wrap.createChild({
                            tag : "img",
                            src : Ext.BLANK_IMAGE_URL,
                            cls : "x-form-trigger " + this.triggerClass
                        });

                if (!f.width) {
                    this.wrap.setWidth(el.getWidth() + this.trigger.getWidth());
                }

                this.splitter = this.wrap.createChild({
                            tag   : 'div',
                            cls   : this.splitterClass,
                            // this fixes UI bug on IE and some of the instances of spinnerfield
                            // style : 'width:13px; height:2px;',
                            style : 'width:13px; height:2px; position:relative; display:inline-block'
                        });

                // this fixes UI bug on IE and some of the instances of spinnerfield
                // this.splitter.setRight((Ext.isIE) ? 1 : 2).setTop(10).show();
                this.splitter.setLeft((Ext.isIE) ? 1 : 2).setBottom((Ext.isIE) ? 3 : 2).show();

                this.proxy = this.trigger.createProxy('', this.splitter, true);
                this.proxy.addClass("x-form-spinner-proxy");
                this.proxy.setStyle('left', '0px');
                this.proxy.setSize(14, 1);
                this.proxy.hide();
                this.dd = new Ext.dd.DDProxy(this.splitter.dom.id, "SpinnerDrag", {
                            dragElId : this.proxy.id
                        });

                this.initTrigger();
                this.initSpinner();
            }
        });

Ext.grid.PivotAggregatorMgr.registerType('concat', function(records, measure) {
            var length = records.length, total = "", i;
            for (i = 0; i < length; i++) {
                total += records[i].get(measure);
            }
            return total;
        });

/**
 * Overrides to fix DragDropManager with multiple targers that wasnt checking for the zindex of the element.
 */
/*
 * Ext.Element.prototype.getZ = function(cached) { if (cached && this.zcache) return this.zcache; var z = this.getStyle('z-index'); if (z == 'auto') { var n = 0; var el = this; // auto no good & elements with x-window-bwrap have a hard coded z-index of 1 but it's no good. while ((z == 'auto' || el.hasClass('x-window-bwrap')) && n++ < 30) { el = el.parent(); if (el != null) z = el.getStyle('z-index'); else break; } } this.zcache = z; return z; } Ext.Element.prototype.clearZCache = function() { this.zcache = false; } Ext.apply(Ext.dd.DragDropMgr, { useZ : true , startDrag : function(x, y) { clearTimeout(this.clickTimeout); if (this.useZ && this.dragCurrent) { // Make a sortable array of the objects var tmp = []; var ids = this.ids[this.dragCurrent.ddGroup]; for (var i in ids) {
 * tmp.push(ids[i]); ids[i].el.clearZCache(); } tmp.sort(function(a, b) { return b.el.getZ(true) < a.el.getZ(true) ? -1 : 1; }); this.sortedIds = tmp; } if (this.dragCurrent) { this.dragCurrent.b4StartDrag(x, y); this.dragCurrent.startDrag(x, y); } this.dragThreshMet = true; } , fireEvents : function(e, isDrop) { var dc = this.dragCurrent; // If the user did the mouse up outside of the window, we could // get here even though we have ended the drag. if (!dc || dc.isLocked()) { return; } var pt = e.getPoint(); // cache the previous dragOver array var oldOvers = []; var outEvts = []; var overEvts = []; var dropEvts = []; var enterEvts = []; // Check to see if the object(s) we were hovering over is no longer // being hovered over so we can fire the onDragOut event for (var i in
 * this.dragOvers) { var ddo = this.dragOvers[i]; if (!this.isTypeOfDD(ddo)) { continue; } if (!this.isOverTarget(pt, ddo, this.mode)) { outEvts.push(ddo); } oldOvers[i] = true; delete this.dragOvers[i]; } for (var sGroup in dc.groups) { if ("string" != typeof sGroup) { continue; } if (this.useZ) ids = this.sortedIds; else ids = this.ids[sGroup]; for (i in ids) { var oDD = ids[i]; if (!this.isTypeOfDD(oDD)) { continue; } if (oDD.isTarget && !oDD.isLocked() && oDD != dc) { if (this.isOverTarget(pt, oDD, this.mode)) { // look for drop interactions if (isDrop) { if (this.useZ && dropEvts.length > 0) continue; dropEvts.push(oDD); // look for drag enter and drag over interactions } else { // initial drag over: dragEnter fires if (!oldOvers[oDD.id]) { if (this.useZ && enterEvts.length > 0)
 * continue; enterEvts.push(oDD); // subsequent drag overs: dragOver fires } else { if (this.useZ && overEvts.length > 0) continue; overEvts.push(oDD); } this.dragOvers[oDD.id] = oDD; } } } } } if (this.useZ && enterEvts.length > 0 && overEvts.length > 0) { // Prevent Enter event to targets below the current one if (enterEvts[0].el.zcache < overEvts[0].el.zcache) enterEvts = []; } if (this.mode) { if (outEvts.length) { dc.b4DragOut(e, outEvts); dc.onDragOut(e, outEvts); } if (enterEvts.length) { dc.onDragEnter(e, enterEvts); } if (overEvts.length) { dc.b4DragOver(e, overEvts); dc.onDragOver(e, overEvts); } if (dropEvts.length) { dc.b4DragDrop(e, dropEvts); dc.onDragDrop(e, dropEvts); } } else { // fire dragout events var len = 0; for (i = 0, len = outEvts.length; i < len; ++i) {
 * dc.b4DragOut(e, outEvts[i].id); dc.onDragOut(e, outEvts[i].id); } // fire enter events for (i = 0, len = enterEvts.length; i < len; ++i) { // dc.b4DragEnter(e, oDD.id); dc.onDragEnter(e, enterEvts[i].id); } // fire over events for (i = 0, len = overEvts.length; i < len; ++i) { dc.b4DragOver(e, overEvts[i].id); dc.onDragOver(e, overEvts[i].id); } // fire drop events for (i = 0, len = dropEvts.length; i < len; ++i) { dc.b4DragDrop(e, dropEvts[i].id); dc.onDragDrop(e, dropEvts[i].id); } } // notify about a drop that did not find a target if (isDrop && !dropEvts.length) { dc.onInvalidDrop(e); } } });
 */

/**
 * This override is for Ext 3.4 The HtmlEditor has a known bug on IE 9 The bug is - the second instance appears ReadOnly Also Overrides to fix HtmlEditor grow text bug in Chrome and Safari browsers. Also add support for markInvalid.
 */
Ext.override(Ext.form.HtmlEditor, {
            addClearInvalidListener : true,
            allowBlank              : true,
            defaultValue            : '',
            markInvalid             : function(msg) {
                // if (!this.rendered || this.preventMark) {
                if (this.initialized && !this.preventMark) {
                    if (this.addClearInvalidListener) {
                        this.on('sync', this.clearInvalid);
                        this.addClearInvalidListener = false;
                    }
                    Ext.form.HtmlEditor.superclass.markInvalid.apply(this, arguments);
                    Ext.get(this.iframe).addClass(this.invalidClass);
                }
                else
                    this.setActiveError(msg);
            },

            alignErrorIcon          : function() {
                try {
                    this.errorIcon.alignTo(this.wrap, 'tl-tr', [2, 0]);
                }
                catch (err) {
                }
            },

            clearInvalid            : function() {
                // if (!this.rendered || this.preventMark) {
                if (this.initialized && !this.preventMark) {

                    Ext.form.HtmlEditor.superclass.clearInvalid.apply(this, arguments);
                    Ext.get(this.iframe).removeClass(this.invalidClass);
                }
                else
                    this.unsetActiveError();
            },

            isEmpty                 : function() {
                var value = this.getValue();
                value = value.replace(/&nbsp;/gi, "");
                value = value.replace(/<p>/gi, "");
                value = value.replace(/<p align=left>/gi, "");
                value = value.replace(/<p align=right>/gi, "");
                value = value.replace(/<p align=center>/gi, "");
                value = value.replace(/<.p>/gi, "");
                value = value.replace(/<br>/gi, "");
                value = value.trim();
                if (value != '')
                    return false;
                return true;
            },

            getErrors               : function(value) {
                var errors = Ext.form.HtmlEditor.superclass.getErrors.apply(this, arguments);

                value = Ext.isDefined(value) ? value : this.processValue(this.getRawValue());

                if (Ext.isFunction(this.validator)) {
                    var msg = this.validator(value);
                    if (msg !== true) {
                        errors.push(msg);
                    }
                }

                if (value.length < 1 || value === this.emptyText) {
                    if (this.allowBlank) {
                        // if value is blank and allowBlank is true, there cannot be any additional errors
                        return errors;
                    }
                    else {
                        errors.push(Ext.form.TextField.prototype.blankText);
                    }
                }

                if (!this.allowBlank && (value.length < 1 || value === this.emptyText)) { // if it's blank
                    errors.push(Ext.form.TextField.prototype.blankText);
                }

                if (value.length < this.minLength) {
                    errors.push(String.format(Ext.form.TextField.prototype.minLengthText, this.minLength));
                }

                if (value.length > this.maxLength) {
                    errors.push(String.format(Ext.form.TextField.prototype.maxLengthText, this.maxLength));
                }

                if (this.vtype) {
                    var vt = Ext.form.VTypes;
                    if (!vt[this.vtype](value, this)) {
                        errors.push(this.vtypeText || vt[this.vtype + 'Text']);
                    }
                }

                if (this.regex && !this.regex.test(value)) {
                    errors.push(this.regexText);
                }

                return errors;
            },
            iframePad               : (Ext.isIE9) ? 0 : 3,
            // private
            onRelayedEvent          : function(event) {
                // relay event from the iframe's document to the document that owns the iframe...

                var iframeEl = Ext.fly(this.iframe), iframeXY = iframeEl.getXY(), eventXY = event.getXY();

                // the event from the inner document has XY relative to that document's origin,
                // so adjust it to use the origin of the iframe in the outer document:
                event.xy = [iframeXY[0] + eventXY[0], iframeXY[1] + eventXY[1]];

                // event.injectEvent(iframeEl); // blame the iframe for the event...
                this.deferFocus();
                event.xy = eventXY; // restore the original XY (just for safety)
            },
            // private
            initEditor              : function() {
                // Destroying the component during/before initEditor can cause issues.
                try {
                    var dbody = this.getEditorBody(), ss = this.el.getStyles('font-size', 'font-family', 'background-image', 'background-repeat', 'background-color', 'color'), doc, fn;

                    ss['background-attachment'] = 'fixed'; // w3c
                    dbody.bgProperties = 'fixed'; // ie

                    Ext.DomHelper.applyStyles(dbody, ss);

                    doc = this.getDoc();

                    if (doc) {
                        try {
                            Ext.EventManager.removeAll(doc);
                        }
                        catch (e) {
                        }
                    }

                    /*
                     * We need to use createDelegate here, because when using buffer, the delayed task is added as a property to the function. When the listener is removed, the task is deleted from the function. Since onEditorEvent is shared on the prototype, if we have multiple html editors, the first time one of the editors is destroyed, it causes the fn to be deleted from the prototype, which causes errors. Essentially, we're just anonymizing the function.
                     */
                    fn = this.onEditorEvent.createDelegate(this);
                    Ext.EventManager.on(doc, {
                                mousedown : fn,
                                dblclick  : fn,
                                click     : fn,
                                keyup     : fn,
                                buffer    : 100
                            });

                    fn = this.onRelayedEvent;
                    Ext.EventManager.on(doc, {
                                mousedown : fn, // menu dismisal (MenuManager) and Window onMouseDown (toFront)
                                mousemove : fn, // window resize drag detection
                                mouseup   : fn, // window resize termination
                                click     : fn, // not sure, but just to be safe
                                dblclick  : fn, // not sure again
                                scope     : this
                            });

                    if (Ext.isGecko) {
                        Ext.EventManager.on(doc, 'keypress', this.applyCommand, this);
                    }
                    // if (Ext.isIE || Ext.isWebKit || Ext.isOpera) {
                    if (this.fixKeys) {
                        Ext.EventManager.on(doc, 'keydown', this.fixKeys, this);
                    }
                    // We need to be sure we remove all our events from the iframe on unload or we're going to LEAK!
                    Ext.EventManager.on(window, 'unload', this.beforeDestroy, this);
                    doc.editorInitialized = true;
                    this.initialized = true;
                    this.pushValue();
                    this.setReadOnly(this.readOnly);
                    this.fireEvent('initialize', this);
                }
                catch (e) {
                }
            },

            adjustFont              : function(btn) {
                var adjust = btn.getItemId() == 'increasefontsize' ? 1 : -1, doc = this.getDoc(), v = parseInt(doc.queryCommandValue('FontSize') || 2, 10);
                if (Ext.isAir) {
                    if (v <= 10) {
                        v = 1 + adjust;
                    }
                    else if (v <= 13) {
                        v = 2 + adjust;
                    }
                    else if (v <= 16) {
                        v = 3 + adjust;
                    }
                    else if (v <= 18) {
                        v = 4 + adjust;
                    }
                    else if (v <= 24) {
                        v = 5 + adjust;
                    }
                    else {
                        v = 6 + adjust;
                    }
                    v = v.constrain(1, 6);
                }
                else {
                    v = Math.max(1, v + adjust);
                }
                this.execCmd('FontSize', v);
            },
            /**
             * Protected method that will not generally be called directly. If you need/want custom HTML cleanup, this is the method you should override.
             * @param {String} html The HTML to be cleaned
             * @return {String} The cleaned HTML
             */
            cleanHtml               : function(html) {
                html = String(html);
                if (Ext.isWebKit) { // strip safari nonsense
                    html = html.replace(/\sclass="(?:Apple-style-span|khtml-block-placeholder)"/gi, '');
                }

                /*
                 * Neat little hack. Strips out all the non-digit characters from the default value and compares it to the character code of the first character in the string because it can cause encoding issues when posted to the server.
                 */
                if (html.charCodeAt(0) == this.defaultValue.replace(/\D/g, '')) {
                    html = html.substring(1);
                }

                // Cleaning html tags and annoying <br> tag.
                // only html tags
                // html = (Ext.isEmpty(html.replace(/<[a-zA-Z/][^>]*>/igm, ' ').trim())) ? '' : html;
                // ending <br>
                if (html == "<br>")
                    html = "";
                // html = html.replace(/<br>$/im, '');

                return html;
            },

            /**
             * Fixes the display of htmleditor after submit form.
             */
            fixDisplay              : function() {
                if (this.iframe && this.iframe.style && this.iframe.style.height) {
                    var elHeight;
                    elHeight = parseInt(this.iframe.style.height, 10);
                    elHeight--;
                    this.setIframeHeight(elHeight);
                    elHeight++;
                    this.setIframeHeight.defer(100, this, [elHeight]);
                }
            },
            setIframeHeight         : function(height) {
                this.iframe.style.height = height + 'px';
            },
            setValue                : function(v) {
                Ext.form.HtmlEditor.superclass.setValue.call(this, v);
                this.pushValue();
                this.fixDisplay();
                return this;
            },

            beforeDestroy           : function() {
                if (this.monitorTask) {
                    Ext.TaskMgr.stop(this.monitorTask);
                }
                if (this.rendered) {
                    Ext.destroy(this.tb);
                    var doc = this.getDoc();
                    if (doc) {
                        try {
                            Ext.EventManager.removeAll(doc);
                            for (var prop in doc) {
                                delete doc[prop];
                            }
                        }
                        catch (e) {
                        }
                    }
                    if (this.wrap) {
                        // fix this
                        if (this.wrap.dom)
                            this.wrap.dom.innerHTML = '';
                        this.wrap.remove();
                    }
                }
                Ext.form.HtmlEditor.superclass.beforeDestroy.call(this);
            },
            // Resolved concerned Chrome for bug 1353
            initFrame               : function() {
                Ext.TaskMgr.stop(this.monitorTask);
                var doc = this.getDoc();
                this.win = this.getWin();

                doc.open();
                doc.write(this.getDocMarkup());
                // Chrome hates this for somes circumstances
                if (!Ext.isChrome)
                    doc.close();

                var task = { // must defer to wait for browser to be ready
                    run      : function() {
                        var doc = this.getDoc();
                        if (doc.body || doc.readyState == 'complete') {
                            Ext.TaskMgr.stop(task);
                            this.setDesignMode(true);
                            this.initEditor.defer(10, this);
                        }
                    },
                    interval : 10,
                    duration : 10000,
                    scope    : this
                };
                Ext.TaskMgr.start(task);
            }
        });

/**
 * @class Ext.ux.grid.filter.NumericFilter
 * @extends Ext.ux.grid.filter.Filter Filters using an Ext.ux.menu.RangeMenu.
 * <p>
 * <b><u>Example Usage:</u></b>
 * </p>
 * 
 * <pre><code>
 *     
 *  var filters = new Ext.ux.grid.GridFilters({
 *  ...
 *  filters: [{
 *  type: 'numeric',
 *  dataIndex: 'price'
 *  }]
 *  });
 * </code></pre>
 */
Ext.ux.grid.filter.IntFilter = Ext.extend(Ext.ux.grid.filter.NumericFilter, {

            /**
             * @private Template method that is to get and return serialized filter data for transmission to the server.
             * @return {Object/Array} An object or collection of objects containing key value pairs representing the current configuration of the filter.
             */
            getSerialArgs : function() {
                var key, args = [], values = this.menu.getValue();
                for (key in values) {
                    args.push({
                                type       : 'int',
                                comparison : key,
                                value      : values[key]
                            });
                }
                return args;
            }
        });

/**
 * Return true if the number is an int.
 * @param {} n
 * @return {}
 */
Ext.isInt = function(n) {
    return typeof n == 'number' && parseFloat(n) == parseInt(n) && !isNaN(n);
};

// Override the min width of messageboxes
Ext.MessageBox.minWidth = 360;

// Override to make portlet draggable by body
Ext.Panel.DD = Ext.extend(Ext.dd.DragSource, {

            constructor : function(panel, cfg) {
                this.panel = panel;
                this.dragData = {
                    panel : panel
                };
                this.proxy = new Ext.dd.PanelProxy(panel, cfg);
                Ext.Panel.DD.superclass.constructor.call(this, panel.el, cfg);
                var h = panel.header, el = panel.body;
                var useBodyForDrag = false;

                if (panel.isXType('portlet') && !panel.isXType('vizPortletMap') && !panel.isXType('vizPortletWebFrame') && !panel.isXType('vizPortletAlarmTable'))
                    useBodyForDrag = true;

                if (panel.isXType('vizChartViewer') && panel.entity) {
                    switch (panel.entity.DisplayMode) {
                        case Vizelia.FOL.BusinessEntities.ChartDisplayMode.CustomGrid :
                        case Vizelia.FOL.BusinessEntities.ChartDisplayMode.Grid :
                        case Vizelia.FOL.BusinessEntities.ChartDisplayMode.Html :
                            useBodyForDrag = false;
                    }
                }

                if (useBodyForDrag) {
                    if (el) {
                        this.setHandleElId(el.id);
                    }
                }

                if (h) {
                    this.setHandleElId(h.id);
                    h.setStyle('cursor', 'move');
                }

                this.scroll = false;
            },

            showFrame   : Ext.emptyFn,
            startDrag   : Ext.emptyFn,
            b4StartDrag : function(x, y) {
                this.proxy.show();
            },
            b4MouseDown : function(e) {
                var x = e.getPageX(), y = e.getPageY();
                this.autoOffset(x, y);
            },
            onInitDrag  : function(x, y) {
                this.onStartDrag(x, y);
                return true;
            },
            createFrame : Ext.emptyFn,
            getDragEl   : function(e) {
                return this.proxy.ghost.dom;
            },
            endDrag     : function(e) {
                this.proxy.hide();
                this.panel.saveState();
            },

            autoOffset  : function(x, y) {
                x -= this.startPageX;
                y -= this.startPageY;
                this.setDelta(x, y);
            }
        });

Ext.override(Ext.EventObjectImpl, {
            stopEvent       : function() {
                try {
                    var me = this;
                    if (me.browserEvent) {
                        if (me.browserEvent.type == 'mousedown') {
                            Ext.EventManager.stoppedMouseDownEvent.fire(me);
                        }
                        var E = Ext.lib.Event;
                        E.stopEvent(me.browserEvent);
                    }
                }
                catch (e) {
                    // ie 8 bug. do nothing.
                }
            },
            stopPropagation : function() {
                try {
                    var me = this;
                    if (me.browserEvent) {
                        if (me.browserEvent.type == 'mousedown') {
                            Ext.EventManager.stoppedMouseDownEvent.fire(me);
                        }
                        var E = Ext.lib.Event;
                        E.stopPropagation(me.browserEvent);
                    }
                }
                catch (e) {
                    // ie 8 bug. do nothing.
                }
            }
        });

Ext.override(Ext.menu.Menu, {
            constrainScroll : function(y) {
                var max, full = this.ul.setHeight('auto').getHeight(), returnY = y, normalY, parentEl, scrollTop, viewHeight;
                if (this.floating) {
                    parentEl = Ext.fly(this.el.dom.parentNode);
                    scrollTop = parentEl.getScroll().top;
                    viewHeight = parentEl.getViewSize().height;
                    // Normalize y by the scroll position for the parent element. Need to move it into the coordinate space
                    // of the view.
                    normalY = y - scrollTop;
                    max = this.maxHeight ? Math.min(this.maxHeight, viewHeight - normalY) : viewHeight - normalY;
                    if (full > viewHeight) {
                        max = viewHeight;
                        // Set returnY equal to (0,0) in view space by reducing y by the value of normalY
                        returnY = y - normalY;
                    }
                    else if (max < full) {
                        returnY = y - (full - max);
                        max = full;
                    }
                }
                else {
                    max = this.getHeight();
                }
                // Always respect maxHeight
                if (this.maxHeight) {
                    max = Math.min(this.maxHeight, max);
                }
                if (full > max && max > 0) {
                    this.activeMax = max - this.scrollerHeight * 2 - this.el.getFrameWidth('tb') - Ext.num(this.el.shadowOffset, 0);
                    this.ul.setHeight(this.activeMax);
                    this.createScrollers();
                    this.el.select('.x-menu-scroller').setDisplayed('');
                }
                else {
                    this.ul.setHeight(full);
                    this.el.select('.x-menu-scroller').setDisplayed('none');
                }
                this.ul.dom.scrollTop = 0;
                this.el.setY(returnY);
                return returnY;
            }
        });
// Moved the the next apply from a file called "extension.js"
Ext.apply(Ext.form.VTypes, {
            /**
             * The function used to validate passwords. The second password field should have an initialPassword property containing the value of the id of the first field.
             * @param {String} val the value of the field (second password)
             * @param {Ext.form.TextField} field (the field)
             * @return {Boolean} true if the passwords are identical, false otherwise.
             */
            test          : function(val, field) {
                if (field.initialPasswordField) {
                    var passwordField = Ext.getCmp(field.initialPasswordField);
                    return (val == passwordField.getValue());
                }
            },

            /**
             * The error text to display when the password validation function returns false.
             * @type String
             */
            testText      : '',

            /**
             * The validator to validate passwords. The second password field should have an initialPassword property containing the value of the id of the first field.
             * <p>
             * The validator is preferably used over the vtype because it is executed before the allowBlank validation.
             * </p>
             * @param {String} value
             * @return {String}
             */
            testValidator : function(value) {
                var passwordField = Ext.getCmp(this.initialPasswordField);
                if (passwordField.getValue() != value) {
                    var msg = $lang('error_msg_invalid_password_confirm');
                    passwordField.markInvalid(msg);
                    return msg;
                }
                return true;
            }
        });

// Fixes the sort by zIndex.
Ext.dd.DragDropMgr.byZIndex = function(d1, d2) {
    return d2.zIndex - d1.zIndex;
};

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
};
﻿Ext.namespace("Viz");

/**
 * Repository for constants
 * @singleton
 */
Viz.Const = {

    /**
     * User interface constants
     * @type
     */
    UI    : {
        /**
         * Anchor value for form fields.
         */
        Anchor     : '-20',
        /**
         * Anchor value for grid in form fields.
         */
        GridAnchor : '-5',
        /**
         * Default Value for Page Size in Grid.
        */
        DefaultGridPageSize: 50
    },

    /**
     * Url constants
     */
    Url   : {
        /**
         * End point to localized messages
         */
        GetLangue              : 'Core.svc/GetLangue',

        /**
         * End point to execute login service
         */
        Login                  : 'Authentication_JSON_AppService.axd/Login',

        /**
         * End point to get application's dataset
         */
        GetApplication         : 'Core.svc/GetApplication',

        /**
         * Url to blank gif
         */
        BlankImage             : 'Resources/images/default/s.gif',

        /**
         * Url to the chart loading background gif
         */
        ChartLoadingBackground: '/Resources/images/default/chart/loading_background.gif',

        /**
         * Url to the report viewer aspx file
         */
        ReportViewer           : 'reporting/reportingservices.aspx'
    },

    /**
     * State constants
     */
    State : {
        /**
         * Store the user desktop background
         */
        DesktopBackground               : 'DesktopBackground',
        /**
         * Store the user custom desktop background key image
         */
        DesktopBackgroundCustomKeyImage : 'DesktopBackgroundCustomKeyImage',
        /**
         * Store the user desktop shortcut text color
         */
        DesktopShortcutTextColor        : 'DesktopShortcutTextColor',
        /**
         * Store the user timezone id
         */
        TimeZoneId                      : 'TimeZoneId',
        /**
         * Store the user active slider for each portlet
         */
        PortletActiveSlider             : 'PortletActiveSlider_',
        /**
         * State id that manages the chart preload all option.
         */
        ChartPreLoadAll                 : 'ChartPreLoadAll',
        /**
         * Store the user default KPI to be used when creating charts by drag and drop
         */
        DefaultChartKPI                 : 'DefaultChartKPI',
        /**
         * Store the user's page size in the grids
         */
        GridsPageSize: 'GridsPageSize'

    },
    Date  : {
        /**
         * Days offset to prevent wrong month in new meter data creation duo to time zone offsets.
         */
        MeterDataDaysOffset  : 2,
        /**
         * Hours offset to prevent wrong day in new meter data creation duo to time zone offsets.
         */
        MeterDataHoursOffset : 12
    }
};
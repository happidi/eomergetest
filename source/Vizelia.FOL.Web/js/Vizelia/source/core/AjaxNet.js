﻿// / <reference name="MicrosoftAjax.js" />
Ext.namespace("Viz");
/**
 * @class Viz.AjaxNet (singleton) Extends the AJAX.NET infrastructure.
 * @singleton
 * @param no parameters required
 */
Viz.AjaxNet = {
    init: function () {
        Ext.BLANK_IMAGE_URL = Viz.Const.Url.BlankImage;
        // disable right click on the page.
        Ext.getBody().on("contextmenu", Ext.emptyFn, null, {
            preventDefault: true
        });
        
        Ext.QuickTips.init();
        Viz.Configuration.Columns.init();
        Ext.form.Field.prototype.msgTarget = 'side';
        // we need to override Sys.Net.XMLHttpExecutor from AJAX.NET because of
        // the block finally that causes IE to break on it
        // Sys.Net.XMLHttpExecutor = Ext.extend(Sys.Net.XMLHttpExecutor, {
        // constructor : function() {
        // if (arguments.length !== 0)
        // throw Error.parameterCount();
        // Sys.Net.XMLHttpExecutor.initializeBase(this);
        // var _this = this;
        // this._xmlHttpRequest = null;
        // this._webRequest = null;
        // this._responseAvailable = false;
        // this._timedOut = false;
        // this._timer = null;
        // this._aborted = false;
        // this._started = false;
        // this._onReadyStateChange = (function() {
        // if (_this._xmlHttpRequest.readyState === 4) {
        // try {
        // if (typeof(_this._xmlHttpRequest.status) === "undefined") {
        // return;
        // }
        // }
        // catch (ex) {
        // return;
        // }
        // _this._clearTimer();
        // _this._responseAvailable = true;
        // try {
        // _this._webRequest.completed(Sys.EventArgs.Empty);
        // }
        // catch (e) {
        // ;
        // }
        // if (_this._xmlHttpRequest != null) {
        // _this._xmlHttpRequest.onreadystatechange = Function.emptyMethod;
        // _this._xmlHttpRequest = null;
        // }
        // }
        // });
        // this._clearTimer = (function() {
        // if (_this._timer != null) {
        // window.clearTimeout(_this._timer);
        // _this._timer = null;
        // }
        // });
        // this._onTimeout = (function() {
        // if (!_this._responseAvailable) {
        // _this._clearTimer();
        // _this._timedOut = true;
        // _this._xmlHttpRequest.onreadystatechange = Function.emptyMethod;
        // _this._xmlHttpRequest.abort();
        // _this._webRequest.completed(Sys.EventArgs.Empty);
        // _this._xmlHttpRequest = null;
        // }
        // });
        // }
        // });
        // try {
        // Sys.__registeredTypes['Sys.Net.XMLHttpExecutor'] = null;
        // }
        // catch (e) {
        // ;
        // }
        // Sys.Net.XMLHttpExecutor.registerClass('Sys.Net.XMLHttpExecutor', Sys.Net.WebRequestExecutor);
        // Add a changing parameter to WeGet AJAX.NET requests
        Sys.Net.WebRequestManager.add_invokingRequest(function (executer, eventArgs) {
            if (Viz.pageId) {
                var currentRequest = eventArgs.get_webRequest();
                //currentRequest.get_headers()[Vizelia.FOL.Providers.SessionService.const_CSRFHeader] = Viz.CSRFGuid;
                var const_page_id = "PAGEID";
                currentRequest.get_headers()[const_page_id] = Viz.pageId;

            }
            var url = eventArgs.get_webRequest().get_url();
            var method = eventArgs.get_webRequest().get_httpVerb();
            if (method == 'GET') {
                var dcp = "_dc";
                url += (url.indexOf('?') != -1 ? '&' : '?') + dcp + '=' + (new Date().getTime());
                eventArgs.get_webRequest().set_url(url);
            }
        });
        // Exception handling
        Sys.Net.WebRequestManager.add_completedRequest(function (sender, eventArgs) {
            var const_exception_type = "ExceptionType";
            // Accepted stands for IsOneWay WCF operation.
            if (sender.get_statusText() != "OK" && sender.get_statusText() != "Accepted") {
                if (sender.get_statusCode() === 500) {
                    var exceptionType = sender._xmlHttpRequest.getResponseHeader(const_exception_type);
                    if (exceptionType === "Vizelia.FOL.Common.VizeliaSessionTimeoutException") {
                        // we do not show the message box if user has not logged. This prevent double display of message.
                        if (Viz.HasLogged == true) {
                            Ext.Msg.show({
                                fn: function () {
                                    window.location.reload();
                                    // window.location.href = window.location.protocol + "//" + window.location.host + window.location.pathname.substring(0, window.location.pathname.indexOf('/', 1)) + '/default.aspx';
                                },
                                width: 400,
                                title: $lang("error_ajax"),
                                msg: $lang("msg_session_timeout"),
                                buttons: Ext.Msg.OK,
                                icon: Ext.MessageBox.ERROR
                            });
                        }
                        else {
                            window.location.reload();
                        }
                    }

                    if (exceptionType === "Vizelia.FOL.Common.VizeliaSecurityException") {
                        Ext.Msg.show({
                            width: 400,
                            title: $lang("msg_access_denied"),
                            msg: $lang("msg_access_denied_detailed"),
                            buttons: Ext.Msg.OK,
                            icon: Ext.MessageBox.ERROR
                        });
                    }

                    return false;
                }
                try {
                    var url = sender.get_webRequest().get_url();
                    var error = sender.get_object();
                    Ext.Msg.show({
                        width: 400,
                        title: $lang("error_ajax"),
                        msg: String.format($lang("error_msg_ajax"), url) + '<br><br>' + String.format($lang("error_msg_ajax_type"), error.ExceptionType) + '<br>' + String.format($lang("error_msg_ajax_description"), error.Message),
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                catch (e) {
                    Ext.Msg.show({
                        width: 400,
                        title: $lang("error_ajax"),
                        msg: $lang("error_msg_unknown_detail"),
                        details: String.format($lang("error_msg_ajax"), url) + '<br><br>' + String.format($lang("error_msg_ajax_description"), $lang("error_msg_unknown") + '<br>' + e.message),
                        buttons: Ext.Msg.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                }
                finally {
                    return false;
                }
            }
        });
        // we need to override this function because gmt date are not sent properly by msajax script.
        Sys.Serialization.JavaScriptSerializer._serializeWithBuilder = function Sys$Serialization$JavaScriptSerializer$_serializeWithBuilder(object, stringBuilder, sort, prevObjects) {
            var i;
            switch (typeof object) {
                case 'object':
                    if (object) {
                        if (prevObjects) {
                            for (var j = 0; j < prevObjects.length; j++) {
                                if (prevObjects[j] === object) {
                                    throw Error.invalidOperation(Sys.Res.cannotSerializeObjectWithCycle);
                                }
                            }
                        }
                        else {
                            prevObjects = new Array();
                        }
                        try {
                            Array.add(prevObjects, object);
                            if (Number.isInstanceOfType(object)) {
                                Sys.Serialization.JavaScriptSerializer._serializeNumberWithBuilder(object, stringBuilder);
                            }
                            else if (Boolean.isInstanceOfType(object)) {
                                Sys.Serialization.JavaScriptSerializer._serializeBooleanWithBuilder(object, stringBuilder);
                            }
                            else if (String.isInstanceOfType(object)) {
                                Sys.Serialization.JavaScriptSerializer._serializeStringWithBuilder(object, stringBuilder);
                            }
                            else if (Array.isInstanceOfType(object)) {
                                stringBuilder.append('[');
                                for (i = 0; i < object.length; ++i) {
                                    if (i > 0) {
                                        stringBuilder.append(',');
                                    }
                                    Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(object[i], stringBuilder, false, prevObjects);
                                }
                                stringBuilder.append(']');
                            }
                            else {
                                if (Date.isInstanceOfType(object)) {
                                    stringBuilder.append('"\\/Date(');
                                    stringBuilder.append(object.getTime() + object.getGMTOffset()); // <---CHANGE HERE + object.getGMTOffset()
                                    stringBuilder.append(')\\/"');
                                    break;
                                }
                                var properties = [];
                                var propertyCount = 0;
                                for (var name in object) {
                                    if (name.startsWith('$')) {
                                        continue;
                                    }
                                    if (name === Sys.Serialization.JavaScriptSerializer._serverTypeFieldName && propertyCount !== 0) {
                                        properties[propertyCount++] = properties[0];
                                        properties[0] = name;
                                    }
                                    else {
                                        properties[propertyCount++] = name;
                                    }
                                }
                                if (sort)
                                    properties.sort();
                                stringBuilder.append('{');
                                var needComma = false;
                                for (i = 0; i < propertyCount; i++) {
                                    var value = object[properties[i]];
                                    if (typeof value !== 'undefined' && typeof value !== 'function') {
                                        if (needComma) {
                                            stringBuilder.append(',');
                                        }
                                        else {
                                            needComma = true;
                                        }
                                        Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(properties[i], stringBuilder, sort, prevObjects);
                                        stringBuilder.append(':');
                                        Sys.Serialization.JavaScriptSerializer._serializeWithBuilder(value, stringBuilder, sort, prevObjects);
                                    }
                                }
                                stringBuilder.append('}');
                            }
                        }
                        finally {
                            Array.removeAt(prevObjects, prevObjects.length - 1);
                        }
                    }
                    else {
                        stringBuilder.append('null');
                    }
                    break;
                case 'number':
                    Sys.Serialization.JavaScriptSerializer._serializeNumberWithBuilder(object, stringBuilder);
                    break;
                case 'string':
                    Sys.Serialization.JavaScriptSerializer._serializeStringWithBuilder(object, stringBuilder);
                    break;
                case 'boolean':
                    Sys.Serialization.JavaScriptSerializer._serializeBooleanWithBuilder(object, stringBuilder);
                    break;
                default:
                    stringBuilder.append('null');
                    break;
            }
        };
    }
};
﻿// create namespace for plugins
Ext.namespace("Viz.plugins");

/**
 * @class Viz.plugins.RemoteValidator
 * @extends Ext.util.Observable Viz.plugins.RemoteValidator plugin for Ext.form.Field. This plugin males a remote validation of a field.
 * 
 * <pre><code>
 * Ext.field.TextField({
 *             plugins : [new Viz.plugins.RemoteValidator({
 *                         serviceWCF            : Viz.Services.EnergyWCF.Chart_ValidateFormula,
 *                         params                : {
 *                             KeyChart : '1'
 *                         },
 *                         remoteValidationDelay : true
 *                     })]
 *         });
 * ,
 * </code></pre>
 */

Viz.plugins.RemoteValidator = Ext.extend(Ext.util.Observable, {
            /**
             * The ctor.
             * @param {Object} config
             */
            constructor : function(config) {

                Ext.apply(this, config);
                Viz.plugins.RemoteValidator.superclass.constructor.apply(this, arguments);

            },

            /**
             * The init function of the plugin
             * @method init
             * @param {Ext.form.Field} field The field the plugin is applied to.
             * @private
             */
            init        : function(field) {
                // save original functions
                var isValid = field.isValid;
                var validate = field.validate;

                // apply remote validation to field
                Ext.apply(field, {
                            remoteValid            : true,
                            remoteValidationDelay  : this.remoteValidationDelay || 500,
                            remoteValidWCF         : this.serviceWCF,
                            remoteValidParams      : this.params,

                            isValid                : function(preventMark) {
                                if (this.disabled) {
                                    return true;
                                }
                                return isValid.call(this, preventMark) && this.remoteValid;
                            }

                            // private
                            ,
                            validate               : function() {
                                var clientValid = validate.call(this);
                                if (!this.disabled && !clientValid) {
                                    return false;
                                }
                                if (this.disabled || (clientValid && this.remoteValid)) {
                                    this.clearInvalid();
                                    return true;
                                }
                                if (!this.remoteValid) {
                                    this.markInvalid(this.reason);
                                    return false;
                                }
                                return false;
                            }

                            // private - remote validation request
                            ,
                            validateRemote         : function() {
                                var options = {
                                    field   : this.name,
                                    value   : this.getValue(),
                                    success : function(value) {

                                        this.remoteValid = value.success;
                                        this.reason = value.msg;
                                        this.validate();
                                    },

                                    failure : function() {
                                        this.remoteValid = false;
                                        this.reason = "";
                                    },
                                    scope   : this
                                };
                                if (this.remoteValidParams) {
                                    Ext.applyIf(options, this.remoteValidParams);
                                }
                                this.remoteValidWCF(options);
                            }

                            // private - runs from keyup event handler
                            ,
                            filterRemoteValidation : function(e) {
                                if (!e.isNavKeyPress()) {
                                    this.remoteValidationTask.delay(this.remoteValidationDelay);
                                }
                            }
                        });

                // install event handlers on field render
                field.on({
                            render : {
                                single : true,
                                scope  : field,
                                fn     : function() {
                                    this.remoteValidationTask = new Ext.util.DelayedTask(this.validateRemote, this);
                                    this.el.on(this.validationEvent, this.filterRemoteValidation, this);
                                }
                            }
                        });
            }
        });

Ext.preg('vizRemoteValidator', Viz.plugins.RemoteValidator);
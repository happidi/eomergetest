﻿Ext.namespace("Viz.plugins");
/**
 * @class Viz.plugins.RapidSearch
 * @extends Ext.util.Observable Viz.plugins.RapidSearch plugin for Ext.form.Field. This plugin creates an RapidSearch menu when right clicking on the field.
 */
Viz.plugins.RapidSearch = Ext.extend(Ext.util.Observable, {
            serviceHandler   : Ext.emptyFn,
            serviceParams    : null,
            byLocalId        : false,
            pathTypeString   : '',
            searchName       : '',           
            /**
             * The ctor.
             * @param {Object} config
             */
            constructor      : function(config) {
                pathTypeString = config.pathTypeString || $lang('msg_location_path');
                searchName = config.searchName || '';
                this.addEvents(
                        /**
                         * @event select Fires when the result is selected in the list view.
                         */
                        'select',
                        /**
                         * @event update Fires if the component updates its found results.
                         */
                        'update');
                Ext.apply(this, config);
                Viz.plugins.RapidSearch.superclass.constructor.apply(this, arguments);
            },

            /**
             * The init function of the plugin
             * @method init
             * @param {Ext.grid.GridPanel} grid The grid the plugin is applied to.
             * @private
             */
            init             : function(panel) {
                this.panel = panel;
                panel.RapidSearch = this;
                panel.tools.push({
                            id      : 'search',
                            qtip    : $lang('msg_search'),
                            handler : function(event) {
                                if (!this.ctxMenu) {
                                    this.ctxMenu = new Ext.menu.Menu({
                                                defaults : {
                                                    hideOnClick : false
                                                }
                                            });
                                    this.inputItem = new Viz.combo.RapidSearch({
                                                iconCls        : 'ux-gridfilter-text-icon',
                                                listClass      : 'x-menu',
                                                hiddenName     : this.panel.id + 'KeyChildren',
                                                name           : this.panel.id + 'KeyChildren',
                                                displayField   : this.displayField || 'Name',
                                                valueField     : this.valueField || 'KeyChildren',
                                                width          : 300,
                                                pathTypeString : this.pathTypeString,
                                                searchName     : this.searchName,
                                                listeners      : {
                                                    select : {
                                                        fn    : this.onSelectLocation,
                                                        scope : this
                                                    },
                                                    expand : {
                                                        fn    : function(combo) {                                                           
                                                            this.writeEventToGoogleAnalytics('Search Text Box', 'Search', combo.searchName);
                                                            if (combo.pageTb.find('id', 'btn-apply-all').length == 0) {
                                                                combo.pageTb.insert(13, {
                                                                            id           : 'btn-apply-all',
                                                                            iconCls      : 'viz-icon-small-select',
                                                                            enableToggle : false,
                                                                            text         : $lang('msg_tree_checkallnodes'),
                                                                            handler      : this.onSelectAll,
                                                                            scope        : this
                                                                        });
                                                                combo.pageTb.doLayout();                                                                
                                                            }
                                                        },
                                                        scope : this
                                                    }
                                                },
                                                store          : this.store
                                            });
                                    this.ctxMenu.add(this.inputItem);
                                }

                                this.ctxMenu.showAt(event.getXY());
                                if (this.inputItem.store.getCount() > 0) {
                                    this.inputItem.onFocus({});
                                    this.inputItem.expand();
                                }
                            },
                            scope   : this
                        });
            },
            /**
             * @private Handler method called when there is a keyup event on this.inputItem
             */
            onSelectLocation : function(combo, record, index) {
                if (this.fireEvent("select", this, record) != false) {
                    this.ctxMenu.hide(true);
                    this.inputItem.collapse();
                    if (!Ext.isEmpty(record.data['Code'])) {
                        Viz.Configuration.ClassificationItemDefinition.each(function(r) {
                                    if (record.data['Code'].indexOf(r.data['Code']) == 0) {
                                        var const_root_header = String.format(' / {0} / {1} / ', this.root.text, this.root.firstChild.text);
                                        this.selectPath(const_root_header + record.data.LongPath, 'text');
                                        this.selectPath(const_root_header + r.data['Title'] + ' / ' + record.data.LongPath, 'text');
                                        const_root_header = String.format(' / {0} / ', this.root.text);
                                        this.selectPath(const_root_header + record.data.LongPath, 'text');
                                    }
                                }, this.panel);
                    }
                    else {
                        var explore = function(bSuccess, oSelNode) {
                            if (!bSuccess)
                                return;
                            oSelNode.checked = true;
                            oSelNode.ui.toggleCheck(true);
                        };
                        var const_root_header = !this.byLocalId ? String.format(' / {0} / ', this.panel.root.text) : ' /  / ';
                        this.panel.root.attributes.LocalId = this.panel.root.attributes.LocalId || "";
                        var path = const_root_header + (this.byLocalId ? record.data.LocalIdPath : record.data.LongPath);
                        this.panel.selectPath(path, !this.byLocalId ? 'text' : 'LocalId', index == null ? explore : null);
                    }
                    this.inputItem.setRawValue(this.inputItem.lastQuery);
                    this.inputItem.lastSelectionText = this.inputItem.lastQuery;
                }
            },
            onSelectAll      : function(btn) {
                this.inputItem.store.each(function(r) {
                            this.onSelectLocation(this.inputItem, r, null);
                        }, this);
            },
            writeEventToGoogleAnalytics: function (category, action, label) {                
                Viz.writeEventToGoogleAnalytics(category, action, label);
            }
        });
Ext.preg('vizRapidSearch', Viz.plugins.RapidSearch);
﻿Ext.namespace("Viz.plugins");

/**
 * @class Viz.plugins.IntelliSense
 * @extends Ext.util.Observable Viz.plugins.IntelliSense plugin for Ext.form.Field. This plugin creates an intelliSense menu when right clicking on the field.
 */
Viz.plugins.IntelliSense = Ext.extend(Ext.util.Observable, {
            serviceHandler : Ext.emptyFn,
            serviceParams  : null,
            startChar      : '@',
            delimiterChar  : '.',
            useParentEl    : false,
            /**
             * The ctor.
             * @param {Object} config
             */
            constructor    : function(config) {
                this.addEvents(
                        /**
                         * @event success Fires after the component retreives the possible actions on a workflow entity.
                         */
                        'success',
                        /**
                         * @event failure Fires if the component cannot retreive the possible actions on a workflow entity.
                         */
                        'failure');
                Ext.apply(this, config);
                Viz.plugins.IntelliSense.superclass.constructor.apply(this, arguments);
            },

            /**
             * The init function of the plugin
             * @method init
             * @param {Ext.grid.GridPanel} grid The grid the plugin is applied to.
             * @private
             */
            init           : function(field) {
                this.field = field;
                field.intelliSense = this;
                this.intelliSenseUtil = new Viz.util.IntelliSense({
                            startChar        : this.startChar,
                            delimiterChar    : this.delimiterChar,
                            serviceHandler   : this.serviceHandler,
                            serviceParams    : this.serviceParams,
                            menuHandler      : this.menuHandler,
                            menuHandlerScope : this,
                            listeners        : {
                                success : function(o, nodes) {
                                    this.field.nodes = nodes;
                                    // this.field.actions = actions;
                                    if (this.fireEvent("success", this, nodes) != false) {
                                        ctxMenuWait.destroy();
                                        this.intelliSenseUtil.showMenu(this.field.nodes, this.xy);
                                    }
                                },
                                failure : function() {
                                    ctxMenuWait.destroy();
                                },
                                scope   : this
                            }
                        });
                var ctxMenuWait = this.intelliSenseUtil.buildWaitContextMenu();

                this.field.on('initialize', function(f) {
                            if (f.getXType && f.getXType() == "htmleditor") {
                                var body = Ext.get(f.getEditorBody());
                                body.on('contextmenu', function(e) {
                                            e.stopEvent();
                                            e.stopPropagation();
                                        });

                                var opts = {
                                    buffer : 200
                                };
                                if (!Ext.isIE) {
                                    Ext.apply(opts, {
                                                stopEvent       : true,
                                                stopPropagation : true,
                                                preventDefault  : true
                                            })
                                }
                                body.on('contextmenu', function(e) {
                                            this.onContextMenu(e)
                                        }, this, opts);
                            }
                        }, this);
                this.field.on('afterrender', function(f) {
                            var el = f.getEl();
                            if (this.useParentEl)
                                el = el.parent();
                            el.on('contextmenu', this.onContextMenu, this);
                        }, this);

            },

            onContextMenu  : function(e) {
                e.stopEvent();
                e.stopPropagation();
                this.startPos = this.field.el.dom.selectionStart;
                this.endPos = this.field.el.dom.selectionEnd;
                this.xy = e.xy;
                var iframe = Ext.get(this.field.iframe);
                if (iframe && this.field.sourceEditMode === false) {
                    this.xy[0] += iframe.getX();
                    this.xy[1] += iframe.getY();
                }
                if (!this.field.nodes) {
                    if (typeof ctxMenuWait !== 'undefined')
                        ctxMenuWait.showAt(this.xy);
                    this.getMenu();
                }
                else
                    this.intelliSenseUtil.showMenu(this.field.nodes, this.xy);
            },

            getMenu        : function(config) {
                this.intelliSenseUtil.getMenu(config);
            },
            /*
             * Handles the menu click
             */
            menuHandler: function (path) {
                this.field.focus();
                Viz.util.pasteHtmlAtCaret(path, this.field.win);
            }
});

/**
 * Static Takes an array of field ids, select the first one to build the intellisense menu and then mark the other fields as already loaded.
 * @param {string[]} arrFieldIds
 */
Viz.plugins.IntelliSense.synchronizeFields = function(arrFieldIds, config) {
    if (arrFieldIds && Ext.isArray(arrFieldIds) && arrFieldIds.length > 0) {
        var field = Ext.getCmp(arrFieldIds[0]);
        if (field) {
            field.intelliSense.on("success", function(plugin, nodes) {
                        Ext.each(arrFieldIds, function(id) {
                                    var cmp = Ext.getCmp(id);
                                    if (cmp)
                                        cmp.nodes = nodes;
                                });
                        return false;
                    });
            field.intelliSense.getMenu(config);
        }
    }
};

Ext.preg('vizIntelliSense', Viz.plugins.IntelliSense);
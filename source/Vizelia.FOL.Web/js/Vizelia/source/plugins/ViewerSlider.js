﻿// create namespace for plugins
Ext.namespace("Viz.plugins");

/**
 * @class Viz.plugins.ChartViewerSlider
 * @extends Ext.util.Observable Viz.plugins.ChartViewerSlider plugin for Ext.chart.Viewer. This plugin creates / updates a tbar in a Chart Viewer and adds to this toolbar an button with menus for handling multiple sliders.
 * 
 * <pre><code>
 * new Ext.chart.Viewer({
 *             title   : 'Title',
 *             store   : store,
 *             columns : columns,
 *             plugins : [new Viz.plugins.ChartViewerSlider({
 *                         sliders     : [{
 *                 				text      : text to be displayed in the menu,
 * 			                    iconCls   : icon to be displayed in the menu and on the button,
 * 			                    entityAttribute : attribute of the Viewer entity to be updated,
 * 			                    store     : store that defines the slider
 * 			                }]
 *                     })]
 *         });
 * ,
 * </code></pre>
 */

Viz.plugins.ChartViewerSlider = Ext.extend(Ext.util.Observable, {

    /**
    * @cfg {Object} sliders An array of sliders to be added to the Panel [{ text : text to be displayed in the menu, iconCls : icon to be displayed in the menu and on the button, entityAttribute : attribute of the Viewer entity to be updated, store : store that defines the slider }]
    */
    sliders: null,

    /**
    * The ctor.
    * @param {Object} config
    */
    constructor: function (config) {
        Ext.apply(this, config);
        Viz.plugins.ChartViewerSlider.superclass.constructor.apply(this, arguments);
        Viz.util.MessageBusMgr.subscribe("ChartChange", this.onChartChange, this);
        Viz.util.MessageBusMgr.subscribe("ChartAlgorithmCountChange", this.onChartAlgorithmCountChange, this);
    },

    /**
    * Destroy
    */
    onDestroy: function () {
        if (this.panel) {
            this.panel.un('destroy', this.onDestroy, this);
            this.panel.un("afterrender", this.afterPanelRender, this);
        }

        if (this.sliders && this.sliders.length > 0) {
            Ext.each(this.sliders, function (item) {
                if (item.storeHandler && item.store) {
                    item.store.un('load', item.storeHandler);
                }
                item.storeHandler = null;
                item.store = null;
            }, this);
        }

        Viz.util.MessageBusMgr.unsubscribe("ChartAlgorithmCountChange", this.onChartAlgorithmCountChange, this);
        Viz.util.MessageBusMgr.unsubscribe("ChartChange", this.onChartChange, this);
        // No use in calling as it doesn't exist! Viz.plugins.ChartViewerSlider.superclass.onDestroy.apply(this, arguments);
    },

    /**
    * The init function of the plugin
    * @method init
    * @param {Ext.Panel} panel The panel the plugin is applied to.
    * @private
    */
    init: function (panel) {
        panel.on('destroy', this.onDestroy, this);

        this._sliderid = Ext.id();
        this._buttonid = Ext.id();

        this.panel = panel;
        // we have to make sure there is a placeholder for top toolbar.
        // we examine elements property of the grid and add to it tbar
        // if missing.
        if (panel.elements.indexOf("bbar") < 0) {
            panel.elements += ",bbar";

            if (this.sliders && this.sliders.length > 0) {

                var count = 0;
                Ext.each(this.sliders, function (item) {
                    if (item.hidden != true)
                        count += 1;
                }, this);

                var disabled = false;
                if (panel.entity && panel.entity.DrillDownEnabled == true)
                    disabled = true;

                if (count > 0) {
                    panel.bottomToolbar = panel.createToolbar({
                        xtype: 'statusbar',
                        disabled: disabled,
                        items: [{
                            iconCls: 'viz-icon-small-refresh',
                            tooltip: $lang("msg_portlet_refresh"),
                            handler: function () {
                                if (this.panel && this.panel.onRefresh)
                                    this.panel.onRefresh(true);
                            },
                            scope: this
                        }, {
                            xtype: 'tbfill'
                        }]
                    });

                    panel.bbar = null;

                    Ext.each(this.sliders, function (item) {
                        if (item.hidden != true) {
                            var handler = this.appendSlider.createDelegate(this, [item]);
                            item.storeHandler = handler;
                            item.store.on("load", handler);
                        }
                    }, this);
                }
            }
        }

        panel.on("afterrender", this.afterPanelRender, this);
        panel.doLayout();
    },

    afterPanelRender: function () {
        this.loadStores.defer(1000, this);
    },

    /**
    * Returns a single or the 1st master entity
    */
    getEntity: function () {
        switch (Viz.getEntityType(this.panel.entity)) {
            case 'Vizelia.FOL.BusinessEntities.DrawingCanvas':
                var charts = this.panel.getChartEntities(true);
                if (!Ext.isArray(charts) || charts.length == 0) {
                    return {};
                }
                return charts[0];

            case 'Vizelia.FOL.BusinessEntities.Chart':
            default:
                return this.panel.entity;
        }
    },

    setEntity: function (entity) {
        switch (Viz.getEntityType(this.panel.entity)) {
            case 'Vizelia.FOL.BusinessEntities.DrawingCanvas':
                var charts = this.panel.getChartEntities(true);
                if (Ext.isArray(charts) && charts.length > 0) {
                    charts[0] = entity;
                }
                break;
            case 'Vizelia.FOL.BusinessEntities.Chart':
            default:
                this.panel.entity = entity;
        }
    },

    /**
    * Returns the chart entity when used on a ChartViewer or all enabled Chart entities on a DrawingCanvas.
    */
    getEntities: function () {
        switch (Viz.getEntityType(this.panel.entity)) {
            case 'Vizelia.FOL.BusinessEntities.DrawingCanvas':
                var charts = this.panel.getChartEntities(true);
                if (!Ext.isArray(charts) || charts.length == 0) {
                    return [];
                }
                return charts;

            case 'Vizelia.FOL.BusinessEntities.Chart':
            default:
                return [this.panel.entity];
        }
    },

    /**
    * Load each store.
    * @method loadStores
    * @private
    */
    loadStores: function () {
        if (this.sliders && this.sliders.length > 0) {
            Ext.each(this.sliders, function (item) {
                if (item.store) {
                    item.store.load();
                }
            }, this);
        }
    },

    /**
    * Once a store has been loaded, we create or append the slider to the panel
    * @method appendSlider
    * @private
    */
    appendSlider: function (item) {
        var toolbar = this.panel.getBottomToolbar();

        // Check to see if the slider exist or if we have to create it
        if (!Ext.getCmp(this._sliderid)) {
            var menu = new Ext.menu.Menu();

            menu.addMenuItem(this.createMenuItem(item.text, item.iconCls, item));

            toolbar.add(new Ext.Button({
                id: this._buttonid,
                tooltip: item.text,
                iconCls: item.iconCls,
                menu: menu
            }));

            toolbar.add(this.createSlider(item));
        }
        else {
            var button = Ext.getCmp(this._buttonid);
            button.menu.addMenuItem(this.createMenuItem(item.text, item.iconCls, item));
            button.menu.doLayout();
        }

        if (this.panel && this.panel.entityPortlet) {
            var activeSliderName = Ext.state.Manager.get(Viz.Const.State.PortletActiveSlider + this.panel.entityPortlet.KeyPortlet);

            //Set default active slider if one doesn't exist
            if (!activeSliderName) {
                activeSliderName = item.entityAttribute;
                Ext.state.Manager.set(Viz.Const.State.PortletActiveSlider + this.panel.entityPortlet.KeyPortlet, activeSliderName);
            }
            if (item.entityAttribute == activeSliderName) {
                this.currentItem = item;
            }
        }
        


        toolbar.doLayout();

    },

    /**
    * Create the first slider based on the first item to be loaded
    * @method createSlider
    * @private
    */
    createSlider: function (item) {
        var entity = this.getEntity();
        if (item.entityAttribute == 'DisplayMode') {
            if (entity.AlgorithmsCount == 0) {
                item.store.remove(item.store.getById('Html'));
            }
        }
        return {
            xtype: 'slider',
            id: this._sliderid,
            width: 150,
            topThumbZIndex: 8000,
            store: item.store,
            entityAttribute: item.entityAttribute,
            increment: 1,
            minValue: item.minValue ? item.minValue : 0,
            maxValue: item.minValue ? item.minValue + item.store.getCount() - 1 : item.store.getCount() - 1,
            value: item.store.getAt(item.store.find('Value', entity[item.entityAttribute])).data.Order + (item.entityAttribute == 'Localisation' ? (entity['LocalisationSiteLevel']) : 0),
            clickToChange: false,
            getSiteLevel: Viz.util.Location.getSiteLevel,
            plugins: [{
                ptype: 'slidertip',
                getText: function (thumb) {
                    var retVal = '';
                    if (!isNaN(thumb.value)) {
                        var store = thumb.slider.store;
                        var data = store.getAt(store.find('Order', thumb.value)).data;
                        retVal = data.Label || data.Id;
                        // if (thumb.slider.entityAttribute == 'Localisation' && data.Id.startsWith('BySite')) {
                        // retVal += ' ' + thumb.slider.getSiteLevel(data);
                        // }
                        var iconCls = data.IconCls;
                        if (!Ext.isEmpty(iconCls)) {
                            retVal = '<div class=" x-icon-combo-item ' + iconCls + '" />' + retVal + '</div>';
                        }

                    }
                    return retVal;

                }
            }],
            listeners: {
                dragend: function (slider, evt) {
                    var value = slider.getValue();
                    var entities = this.getEntities();
                    Ext.each(entities, function (entity) {
                        if (!isNaN(value)) {
                            var data = slider.store.getAt(slider.store.find('Order', value)).data;
                            if (slider.entityAttribute == 'Localisation' && data.Id.startsWith('BySite')) {
                                var siteLevel = slider.getSiteLevel(data);
                                if (entity.Localisation != Vizelia.FOL.BusinessEntities.ChartLocalisation.BySite || this.getEntity().LocalisationSiteLevel != siteLevel) {
                                    entity.Localisation = Vizelia.FOL.BusinessEntities.ChartLocalisation.BySite;
                                    entity.LocalisationSiteLevel = siteLevel;
                                    this.panel.fireEvent("afterentityupdate", this.panel, entity);
                                }
                            }
                            else {
                                var enumValue = slider.store.getAt(slider.store.find('Order', value)).data.Value;
                                if (entity[slider.entityAttribute] != enumValue) {
                                    entity[slider.entityAttribute] = enumValue;
                                    this.panel.fireEvent("afterentityupdate", this.panel, entity);
                                }
                            }
                        }
                    }, this);
                },
                scope: this
            }
        };
    },

    /**
    * When a menu item is selected, we update the slider with the new store
    * @method updateSlider
    * @private
    */
    updateSlider: function (item) {
        var button = Ext.getCmp(this._buttonid);
        button.setIconClass(item.iconCls);
        button.setTooltip(item.text);
        var slider = this.getSlider();
        slider.entityAttribute = item.entityAttribute;
        var entity = this.getEntity();
        if (item.entityAttribute == 'DisplayMode') {
            if (entity.AlgorithmsCount == 0) {
                item.store.remove(item.store.getById('Html'));
            }
        }
        slider.store = item.store;
        slider.minValue = item.minValue || 0;
        slider.maxValue = slider.minValue + item.store.getCount() - 1;

        this.sliderSetValue();

        if (this.panel && this.panel.entityPortlet) {
            Ext.state.Manager.set(Viz.Const.State.PortletActiveSlider + this.panel.entityPortlet.KeyPortlet, item.entityAttribute);
        }

        this.currentItem = item;
    },

    /**
    * Gets the slider
    * @method getSlider
    * @private
    * @return {Ext.Slider}
    */
    getSlider: function () {
        return Ext.getCmp(this._sliderid);
    },

    /**
    * Creates a menu item.
    * @method createMenuItem
    * @private
    * @return {Object}
    */
    createMenuItem: function (text, iconCls, item) {
        var menuItem = {
            text: text,
            tooltip: text,
            iconCls: iconCls,
            hideDelay: 0,
            handler: this.updateSlider.createDelegate(this, [item])
        };
        return menuItem;
    },

    sliderSetValue: function () {
        var slider = this.getSlider();
        if (slider.entityAttribute == 'Localisation' && this.getEntity().Localisation == Vizelia.FOL.BusinessEntities.ChartLocalisation.BySite) {
            slider.setValue(Vizelia.FOL.BusinessEntities.ChartLocalisation.BySite + this.getEntity().LocalisationSiteLevel);
        }
        else
            slider.setValue(slider.store.getAt(slider.store.find('Value', this.getEntity()[slider.entityAttribute])).data.Order);
        slider.syncThumb();
    },

    /**
    * Listener for the afterentityupdate event.
    * @param {Viz.BusinessEntity.Chart} entity
    */
    onChartChange: function (entity) {
        var currentEntity = this.getEntity();
        if (currentEntity.KeyChart == entity.KeyChart) {
            var sliderShouldRefresh = (currentEntity.AlgorithmsCount == 0 && entity.AlgorithmsCount != 0) || (currentEntity.AlgorithmsCount != 0 && entity.AlgorithmsCount == 0);

            this.setEntity(entity);

            var slider = this.getSlider();
            // Check to see if the slider exist or if we have to recreate it
            if (slider) {
                if (sliderShouldRefresh) {
                    this.refreshSlider(slider);
                }
                if (this.currentItem)
                    this.updateSlider(this.currentItem);
            }
        }
    },
    onChartAlgorithmCountChange: function (keyChart, algorithmsCountIncrement) {
        var currentEntity = this.getEntity();
        var algorithmsCount = currentEntity.AlgorithmsCount + algorithmsCountIncrement;
        if (currentEntity.KeyChart == keyChart) {
            var sliderShouldRefresh = (currentEntity.AlgorithmsCount == 0 && algorithmsCount != 0) || (currentEntity.AlgorithmsCount != 0 && algorithmsCount == 0);
            currentEntity.AlgorithmsCount = algorithmsCount;
            var slider = this.getSlider();
            // Check to see if the slider exist or if we have to recreate it
            if (slider) {
                if (sliderShouldRefresh) {
                    this.refreshSlider(slider);
                }
            }
        }
    },
    refreshSlider: function (slider) {
        var toolbar = this.panel.getBottomToolbar();
        var button = Ext.getCmp(this._buttonid);
        toolbar.remove(button);
        toolbar.remove(slider);
        this.loadStores();
    }

});

Ext.preg('vizChartViewerSlider', Viz.plugins.ChartViewerSlider);

﻿// create namespace for plugins
Ext.namespace("Viz.plugins");

/**
 * @class Viz.plugins.GridExport
 * @extends Ext.util.Observable Viz.plugins.GridExport plugin for Ext.grid.GridPanel. This plugin creates / updates a tbar in a GridPanel and adds to this toolbar an export button with menus for different export format.
 * 
 * <pre><code>
 * new Ext.grid.GridPanel({
 *             title   : 'Title',
 *             store   : store,
 *             columns : columns,
 *             plugins : [new Viz.plugins.GridExport({
 *                         title     : 'this is my title',
 *                         listeners : {
 *                             beforeExport : function(plugin) {
 *                                 plugin.title = 'new title'
 *                             }
 *                         }
 *                     })]
 *         });
 * ,
 * </code></pre>
 */
Viz.plugins.GridExport = Ext.extend(Ext.util.Observable, {

            /**
             * @cfg {Object} paging A paging parameter.
             */
            paging                     : {
                start : 0,
                limit : 1000
            },

            /**
             * @cfg {Number} maxRecords Indicated the max number of lines to export.
             */
            maxRecords                 : 1000,
            /**
             * @cfg {String} align Indicates where the export button should appear in the toolbar (defaults to 'right').
             */
            align                      : 'right',

            /**
             * @cfg {Boolean} canExportPDF Indicates if PDF export is available (defaults to <tt>true</tt>).
             */
            canExportPDF               : true,

            /**
             * @cfg {Boolean} canExportExcel Indicates if Excel export is available (defaults to <tt>true</tt>).
             */
            canExportExcel             : true,

            /**
             * @cfg {Boolean} canExportCSV Indicates if CSV export is available (defaults to <tt>true</tt>).
             */
            canExportCSV               : true,

            /**
             * @cfg {Boolean} canExportImage Indicates if Image export is available (defaults to <tt>false</tt>).
             */
            canExportImage             : false,

            /**
             * @cfg {Boolean} canExportHTML Indicates if HTML export is available (defaults to <tt>false</tt>).
             */
            canExportHTML              : false,

            /**
             * @cfg {Boolean} promptMaxRecords Indicates if we want to prompt for the number of records to export.
             */
            promptMaxRecord            : false,

            /**
             * @private Submits a long-running operation to render the report
             */
            submitLongRunningOperation : function(extraParams) {

                var submitter = new Viz.Services.StreamLongRunningOperationSubmitter({
                            pollingInterval     : 2000,
                            serviceStart        : Viz.Services.ReportingWCF.BeginRenderReport,
                            serviceParams       : extraParams,
                            isFileResult        : true,
                            showProgress        : true,
                            progressWindowTitle : $lang('msg_application_export_in_progress'),
                            allowCancel         : true,
                            allowEmailResult    : true
                        });

                submitter.start();
            },

            /**
             * @cfg {String} title Set the title of the report.
             */

            /**
             * if .
             * @method exportData
             * @param {Vizelia.FOL.BusinessEntities.ExportType} exportType The export format.
             * @param {Ext.grid.GridPanel} grid The grid that is being exported.
             * @private
             */
            exportData                 : function(exportType, grid) {
                if (this.fireEvent("beforeExport", this, exportType, grid)) {
                    if (this.promptMaxRecord) {
                        var win = this.createWinMaxRecords(exportType, grid);
                        win.show();
                    }
                    else
                        this.doExportData(exportType, grid);
                }
            },

            /**
             * Creates the win for allowing the user to enter the max number of records.
             * @param {Vizelia.FOL.BusinessEntities.ExportType} exportType The export format.
             * @param {Ext.grid.GridPanel} grid The grid that is being exported.
             */
            createWinMaxRecords        : function(exportType, grid) {
                var spinnerfieldId = Ext.id();
                var win = new Ext.Window({
                            title   : $lang('msg_gridexport_maxrecordinput'),
                            width   : 400,
                            height  : 105,
                            layout  : 'fit',
                            items   : [{
                                        xtype      : 'form',
                                        border     : false,
                                        labelWidth : 200,
                                        defaults   : {
                                            msgTarget : 'side',
                                            anchor    : Viz.Const.UI.Anchor
                                        },
                                        bodyStyle  : 'padding: 5px;background-color: transparent;',
                                        items      : [{
                                                    fieldLabel : $lang('msg_gridexport_maxrecordinput'),
                                                    xtype      : 'vizSpinnerField',
                                                    id         : spinnerfieldId,
                                                    name       : 'maxRecord',
                                                    value      : this.maxRecords,
                                                    minValue   : 1,
                                                    maxValue   : 10000,
                                                    allowBlank : false,
                                                    width      : 60
                                                }]
                                    }],
                            buttons : [{
                                        text    : Ext.MessageBox.buttonText.ok,
                                        handler : function() {
                                            var f = Ext.getCmp(spinnerfieldId);
                                            if (f.isValid()) {
                                                this.maxRecords = f.getValue();
                                                win.close();
                                                this.doExportData(exportType, grid);
                                            }
                                        },
                                        scope   : this
                                    }, {
                                        text    : Ext.MessageBox.buttonText.cancel,
                                        handler : function() {
                                            win.close();
                                        },
                                        scope   : this
                                    }]
                        });
                return win;
            },

            /**
             * Exports data of a grid to the selected format.
             * @method exportData
             * @param {Vizelia.FOL.BusinessEntities.ExportType} exportType The export format.
             * @param {Ext.grid.GridPanel} grid The grid that is being exported.
             * @private
             */
            doExportData               : function(exportType, grid) {
                var paging = Ext.apply(this.paging, {
                            limit : this.maxRecords
                        });
                // Ext.apply(paging, grid.store.sortInfo);
                if (grid.store.sortInfo) {
                    Ext.apply(paging, {
                                sort : grid.store.sortInfo.field,
                                dir  : grid.store.sortInfo.direction
                            });
                }
                var filters = {
                    filters : null
                };
                if (grid.filters) {
                    filters = grid.filters.buildQuery(grid.filters.getFilterData());
                    Ext.apply(paging, filters);
                }

                var extraParams = {
                    exportType : exportType,
                    paging     : paging
                };
                // Ext.apply(extraParams, filters);

                var info = grid.store.serviceHandler.getInfo();
                var gridColumns = grid.getColumnModel().getColumnsVisible(true);
                extraParams = Ext.apply({
                            serviceTypeName : info.serviceTypeName,
                            serviceHandler  : info.serviceHandler,
                            columns         : gridColumns,
                            reportTitle     : this.title || grid.title || grid.ownerCt.title

                        }, extraParams);

                // we need to add the extraParams that where used to load the current grid store
                Ext.applyIf(extraParams, grid.store.serviceParams);

                this.submitLongRunningOperation(extraParams);
            },

            /**
             * The ctor.
             * @param {Object} config
             */
            constructor                : function(config) {

                this.addEvents(
                        /**
                         * @event beforeExport Fires before a report is exported.
                         * @param {Viz.plugins.GridExport} this
                         * @param {Vizelia.FOL.BusinessEntities.ExportType} exportType The export type of the report
                         * @param {Ext.grid.GridPanel} grid The grid that is being exported
                         */
                        "beforeExport");

                Ext.apply(this, config);

                Viz.plugins.GridExport.superclass.constructor.apply(this, arguments);

            },

            /**
             * The init function of the plugin
             * @method init
             * @param {Ext.grid.GridPanel} grid The grid the plugin is applied to.
             * @private
             */
            init                       : function(grid) {

                this.grid = grid;
                // we have to make sure there is a placeholder for top toolbar.
                // we examine elements property of the grid and add to it tbar
                // if missing.
                var elements = grid.elements;
                if (elements.indexOf("tbar") < 0) {
                    elements += elements + ",tbar";
                    grid.elements = elements;
                }
                grid.on("render", this.appendToolbar, this);
            },

            /**
             * Appends the export button to the toolbar and align it.
             * @method appendToolbar
             * @private
             */
            appendToolbar              : function() {
                var toolbar = this.grid.getTopToolbar();
                if (!toolbar) {
                    toolbar = new Ext.Toolbar({
                                renderTo : this.grid.tbar
                            });
                }
                var hasFill = false
                toolbar.items.each(function(item) {
                            if (item.isFill == true)
                                hasFill = true;

                        });
                if (this.align == 'right' && !hasFill)
                    toolbar.add("->");

                toolbar.add(new Ext.Button({
                            // xtype : 'button',
                            text    : $lang("msg_application_export"),
                            tooltip : $lang("msg_application_export"),
                            iconCls : 'viz-icon-small-report',
                            menu    : this.createMenu(),
                            hidden  : !$authorized('@@@@ Reports')
                        }));
                try {
                    toolbar.doLayout();
                }
                catch (e) {
                    ;
                }
            },

            /**
             * Creates the export menus based on the configuration of the plugin.
             * @method createMenu
             * @private
             * @return {Ext.menu.Menu}
             */
            createMenu                 : function() {
                var menu = new Ext.menu.Menu();
                var ExportType = Vizelia.FOL.BusinessEntities.ExportType;
                if (this.canExportPDF) {
                    menu.add(this.createMenuItem('PDF', 'viz-icon-small-pdf', ExportType.Pdf));
                }
                if (this.canExportHTML) {
                    menu.add(this.createMenuItem('HTML', 'viz-icon-small-html', ExportType.Html));
                }
                if (this.canExportExcel) {
                    menu.add(this.createMenuItem('Excel', 'viz-icon-small-excel', ExportType.Excel2007));
                }
                if (this.canExportCSV) {
                    menu.add(this.createMenuItem('CSV', 'viz-icon-small-csv', ExportType.CSV));
                }
                if (this.canExportImage) {
                    menu.add(this.createMenuItem('Image', 'viz-icon-small-image', ExportType.ImagePNG));
                }
                return menu;
            },

            /**
             * Creates a menu item.
             * @method createMenuItem
             * @private
             * @return {Object}
             */
            createMenuItem             : function(text, iconCls, exportType) {
                var menuItem = {
                    text      : text,
                    iconCls   : iconCls,
                    hideDelay : 0,
                    handler   : this.exportData.createDelegate(this, [exportType, this.grid])
                };
                return menuItem;
            }

        });

Ext.preg('vizGridExport', Viz.plugins.GridExport);
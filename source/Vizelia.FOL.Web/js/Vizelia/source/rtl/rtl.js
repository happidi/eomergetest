﻿if ($lang('langue_direction') == 'rtl') {
    Ext.override(Ext.tree.TreeNodeUI, {
                // private
                renderElements : function(n, a, targetNode, bulkRender) {
                    // add some indent caching, this helps performance when rendering a large tree
                    this.indentMarkup = n.parentNode ? n.parentNode.ui.getChildIndent() : '';

                    var cb = Ext.isBoolean(a.checked), nel, href = this.getHref(a.href), buf = ['<li class="x-tree-node"><div ext:tree-node-id="', n.id, '" class="x-tree-node-el x-tree-node-leaf x-unselectable ', a.cls, '" unselectable="on">',
                            //
                            '<span class="x-tree-node-indent">', this.indentMarkup, "</span>",
                            //
                            '<img alt="" src="', this.emptyIcon, '" class="x-tree-ec-icon x-tree-elbow" />',
                            //
                            cb ? ('<input class="x-tree-node-cb" type="checkbox" ' + (a.checked ? 'checked="checked" />' : '/>')) : '',

                            //
                            '<img alt="" src="', a.icon || this.emptyIcon, '" class="x-tree-node-icon', (a.icon ? " x-tree-node-inline-icon" : ""), (a.iconCls ? " " + a.iconCls : ""), '" unselectable="on" />',
                            //
                            '<a hidefocus="on" class="x-tree-node-anchor" href="', href, '" tabIndex="1" ',
                            //
                            a.hrefTarget ? ' target="' + a.hrefTarget + '"' : "", '><span unselectable="on">', n.text, "</span></a></div>",
                            //
                            '<ul class="x-tree-node-ct" style="display:none;"></ul>',
                            //
                            "</li>"].join('');
                    if (bulkRender !== true && n.nextSibling && (nel = n.nextSibling.ui.getEl())) {
                        this.wrap = Ext.DomHelper.insertHtml("beforeBegin", nel, buf);
                    }
                    else {
                        this.wrap = Ext.DomHelper.insertHtml("beforeEnd", targetNode, buf);
                    }

                    this.elNode = this.wrap.childNodes[0];
                    this.ctNode = this.wrap.childNodes[1];
                    var cs = this.elNode.childNodes;
                    this.indentNode = cs[0];
                    this.ecNode = cs[1];
                    this.iconNode = cs[3];
                    var index = 3;
                    if (cb) {
                        this.checkbox = cs[2];
                        // fix for IE6
                        this.checkbox.defaultChecked = this.checkbox.checked;
                        index++;
                    }
                    this.anchor = cs[index];
                    this.textNode = cs[index].firstChild;
                }
            });

    Ext.form.ComboBox.prototype.listAlign = ['tr-br?', [0, 2]];

    Ext.override(Ext.ux.Spinner, {

                doRender : function(ct, position) {
                    var el = this.el = this.field.getEl();
                    var f = this.field;

                    if (!f.wrap) {
                        f.wrap = this.wrap = el.wrap({
                                    cls : "x-form-field-wrap"
                                });
                    }
                    else {
                        this.wrap = f.wrap.addClass('x-form-field-wrap');
                    }

                    this.trigger = this.wrap.createChild({
                                tag : "img",
                                src : Ext.BLANK_IMAGE_URL,
                                cls : "x-form-trigger " + this.triggerClass
                            });

                    if (!f.width) {
                        this.wrap.setWidth(el.getWidth() + this.trigger.getWidth());
                    }

                    this.splitter = this.wrap.createChild({
                                tag   : 'div',
                                cls   : this.splitterClass,
                                style : 'width:13px; height:2px;'
                            });
                    this.splitter.setLeft((Ext.isIE) ? 1 : 2).setTop(10).show(); // <-- Change here : this.splitter.setLeft((Ext.isIE) ? 1 : 2).setTop(10).show();

                    this.proxy = this.trigger.createProxy('', this.splitter, true);
                    this.proxy.addClass("x-form-spinner-proxy");
                    this.proxy.setStyle('left', '0px');
                    this.proxy.setSize(14, 1);
                    this.proxy.hide();
                    this.dd = new Ext.dd.DDProxy(this.splitter.dom.id, "SpinnerDrag", {
                                dragElId : this.proxy.id
                            });

                    this.initTrigger();
                    this.initSpinner();
                }

            });
}
﻿Ext.namespace('Viz.data');

/**
 * @class Viz.data.AdoNetStore
 * @extends Ext.ux.WriteStore
 * <p>Creates a new Viz.data.AdoNetStore</p>
 * <p>Example</p>
 * <pre><code>
ds = new Viz.data.AdoNetStore({
    proxy: new Ext.data.HttpProxy({
        url: 'Core.svc/GetLangue',
        method: 'GET'
    }),
    meta: { idColumn: 'MsgCode' }
});
 * </code></pre>
 * @cfg {Object} meta A config object in the format {idColumn : "fieldname"} 
 */
Viz.data.AdoNetStore = Ext.extend(Ext.ux.WriteStore, {

    constructor: function(config) {
        config = config || {};
        Ext.apply(this, config);
        // reader has to be Ext.viz.AdoNetXmlReader so we override it here
        Ext.apply(this, {
            reader: new Viz.data.AdoNetXmlReader(this.meta)
        });
        Viz.data.AdoNetStore.superclass.constructor.apply(this, arguments);
    },

    // private
    createBeforeNode: function(oBeforeNode, oParentNode) {
        if (!oBeforeNode) {
            oBeforeNode = oParentNode.ownerDocument.createElement("diffgr:before");
            oParentNode.parentNode.appendChild(oBeforeNode);
        }
        return oBeforeNode;
    },

    // private
    // shortcut to Viz.DomQuery.selectNode where we pass the namespaceResolver of the reader's store
    selectNode: function(path, node) {
        return Viz.DomQuery.selectNode(path, node, this.reader.namespaceResolver)
    },

    saveChanges: function() {
        var data = this.writeRecords({});
        var q = Viz.DomQuery; // shortcut
        var doc = this.reader.xmlData.documentElement.cloneNode(true); // we clone so we don't modify the original document
        doc = doc.ownerDocument;
        var oParentNode = this.selectNode("//NewDataSet", doc);
        var oBeforeNode;
        if (!oParentNode) {
            // we are here if the transmitted DataSet was empty, so we have to explicitly create NewDataSet node
            oParentNode = doc.createElement("NewDataSet");
            this.selectNode("//diffgr:diffgram", doc).appendChild(oParentNode);
        }
        // could not find the oBeforeNode because namespace issue, so we go directly to the second child of diffgram
        if (oParentNode.parentNode.childNodes.length == 2)
            oBeforeNode = oParentNode.parentNode.childNodes[1];
        for (var i = 0; i < data.length; i++) {
            var entry = data[i];
            switch (entry.type) {
                // ADD  
                case this.types.add:
                    var oNode = doc.createElement("Result");
                    oNode.setAttribute('diffgr:id', 'Result' + (entry.rid + 1));
                    // setAttributeNS does not exist in IE so we must check before using it
                    if (typeof oNode.setAttributeNS == 'function')
                        oNode.setAttributeNS(this.reader.namespace['msdata'], 'msdata:rowOrder', entry.rid);
                    else
                        oNode.setAttribute('msdata:rowOrder', entry.rid);
                    oNode.setAttribute('diffgr:hasChanges', 'inserted');
                    for (var p in entry.data) {
                        var oFieldNode = doc.createElement(p);
                        q.setNodeText(oFieldNode, entry.data[p]);
                        oNode.appendChild(oFieldNode);
                    }
                    oParentNode.appendChild(oNode);
                    break;
                // DELETE	  
                case this.types.remove:
                    oBeforeNode = this.createBeforeNode(oBeforeNode, oParentNode);
                    var oNode = this.selectNode("Result[@msdata:rowOrder=" + entry.rid + "]", oParentNode);
                    if (oNode.getAttribute("diffgr:hasChanges") == 'inserted') {
                        // we are here if the node we are removing was previously inserted
                        oParentNode.removeChild(oNode);
                    }
                    else {
                        oBeforeNode.appendChild(oNode);
                    }
                    break;
                // UPDATE	  
                case this.types.change:
                    oBeforeNode = this.createBeforeNode(oBeforeNode, oParentNode);
                    var oNode = this.selectNode("Result[@msdata:rowOrder=" + entry.rid + "]", oParentNode);
                    if (oNode.getAttribute("diffgr:hasChanges") != 'inserted') {
                        if (!this.selectNode("Result[@msdata:rowOrder=" + entry.rid + "]", oBeforeNode)) {
                            var oNewNode = oNode.cloneNode(true);
                            oBeforeNode.appendChild(oNewNode);
                        }
                        oNode.setAttribute("diffgr:hasChanges", "modified");
                    }
                    for (var j = 0; j < entry.data.length; j++) {
                        var oFieldNode = this.selectNode(entry.data[j].name, oNode);
                        if (!oFieldNode) {
                            // in the case of a null value from database, the field would not appear in xml, so we create it
                            oFieldNode = doc.createElement(entry.data[j].name);
                            oNode.appendChild(oFieldNode);
                        }
                        q.setNodeText(oFieldNode, entry.data[j]["new"]);
                    }
                    break;
            }
        }
        // TODO : à remplacer par un élément de config
        Ext.Ajax.request({
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            url: 'applications/core/service/test.svc/SaveTable',
            params: Ext.encode({ p: q.serializeXML(doc) }),
            scope: this,
            success: function(response, options) {
                this.reload();
            },
            failure: function(response, options) {
                alert("failure");
            }
        });
        // TODO : traiter le journal
    }
});
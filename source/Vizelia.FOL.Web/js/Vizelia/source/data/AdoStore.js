﻿
Ext.namespace('Viz.data');
/**
 * @class Viz.data.AdoStore
 * @extends Ext.ux.WriteStore
 * @param {Object} config The configuration options for this object.
 */   
Viz.data.AdoStore = Ext.extend(Ext.ux.WriteStore , {
	
	constructor : function(config) {
		Ext.apply(this , config);
		// reader has to be Ext.viz.AdoNetXmlReader so we override it here
		Ext.apply (this , {
			reader : new Viz.data.AdoXmlReader()
		});
		Viz.data.AdoStore.superclass.constructor.apply(this ,arguments);
	},
	
	// private
	// shortcut to Viz.DomQuery.selectNode where we pass the namespaceResolver of the reader's store
	selectNode : function(path , node) {
		return Viz.DomQuery.selectNode(path , node , this.reader.namespaceResolver)
	},
	
	/**
	 * Creates the rs:delete Node if not already exist in the dom
	 * @param {Node} oDeleteNode
	 * @param {Node} oParentNode the parent node that will contain the created node
	 * @return {Node} the rs:delete node
	 */
	createDeleteNode : function (oDeleteNode , oParentNode) {
		if(!oDeleteNode) {
			oDeleteNode = oParentNode.ownerDocument.createElement("rs:delete");
			oParentNode.appendChild(oDeleteNode);
		}
		return oDeleteNode;
	},
	
	/**
	 * Creates the rs:insert Node if not already exist in the dom
	 * @param {Node} oInsertNode
	 * @param {Node} oParentNode the parent node that will contain the created node
	 * @return {Node} the rs:insert node
	 */
	createInsertNode : function (oInsertNode, oParentNode) {
		if(!oInsertNode) {
			oInsertNode = oParentNode.ownerDocument.createElement("rs:insert");
			oParentNode.appendChild(oInsertNode);
		}
		return oInsertNode;
	},
	
	saveChanges : function() {
		var data = this.writeRecords({});
		var q = Viz.DomQuery; // shortcut
		var doc = this.reader.xmlData.documentElement.cloneNode(true); // we clone so we don't modify the original document
		doc = doc.ownerDocument;
		var oParentNode = this.selectNode( "//rs:data", doc);
		var oDeleteNode = this.selectNode( "//rs:delete", doc);
		var oInsertNode = this.selectNode( "//rs:insert" , doc);
		for(var i = 0 ; i < data.length ; i++) {
			var entry = data[i];
			switch (entry.type) {
				// ADD
				case this.types.add :
					oInsertNode = this.createInsertNode(oInsertNode , oParentNode );
					var oNode = doc.createElement("z:row");
					oNode.setAttribute('_order' , entry.rid );
					for(var p in entry.data) {
						oNode.setAttribute(p , entry.data[p]);
					}
					oInsertNode.appendChild(oNode);
					break;
				// DELETE	
				case this.types.remove :
					oDeleteNode = this.createDeleteNode(oDeleteNode , oParentNode );
					var oNode = this.selectNode("*[@_order=" + entry.rid + "]" , oParentNode);
					// here we check if the removed node was inserted before, in this case we simply get rid of it
					if((!oNode) && oInsertNode ) {
						// we are here if the node we are deleting was previously inserted
						oNode = this.selectNode("*[@_order=" + entry.rid + "]" , oInsertNode);
						oInsertNode.removeChild(oNode);
					}
					else {
						oDeleteNode.appendChild(oNode);
					}
					break;
				// UPDATE	
				case this.types.change:
					var oNewNode;
					var oUpdateNode = this.selectNode("rs:update/rs:original/z:row[@_order=" + entry.rid + "]" , oParentNode);
					var oNode = this.selectNode("*[@_order=" + entry.rid + "]" , oParentNode);
					if((!oNode) && (!oUpdateNode)) {
						// we are here if the node we are changing was previously inserted
						oNode = this.selectNode("*[@_order=" + entry.rid + "]" , oInsertNode);
						for(var j = 0; j<entry.data.length ; j++) {
							oNode.setAttribute(entry.data[j].name , entry.data[j]["new"]);
						}
						break;
					}
					if(!oUpdateNode)  {
						// first time the row is update
						var oUpdate;
						var oOriginal;
						if (typeof doc.createElementNS == 'function') {
							oUpdate = doc.createElementNS(this.reader.namespace['rs'] , "rs:update");
							oOriginal = doc.createElementNS(this.reader.namespace['rs'] , "rs:original");
							oNewNode = doc.createElementNS(this.reader.namespace['z'] , "z:row");
						}
						else {
							oUpdate = doc.createElement("rs:update");
							oOriginal = doc.createElement("rs:original");
							oNewNode = doc.createElement("z:row");
						}
						oParentNode.appendChild(oUpdate);
						oUpdate.appendChild(oOriginal);
						oOriginal.appendChild(oNode);
						
						oNewNode.setAttribute("_order" , entry.rid);
						oUpdate.appendChild(oNewNode);
					}
					else {
						oNewNode = this.selectNode("rs:update/z:row[@_order=" + entry.rid + "]" , oParentNode);
					}
					
					for(var j = 0; j<entry.data.length ; j++) {
						oNewNode.setAttribute(entry.data[j].name , entry.data[j]["new"]);
					}
					break;
			}
		}
		
		// TODO : à remplacer par un élément de config
		Ext.Ajax.request({
			method : 'POST',
			headers : {'Content-Type' : 'application/json'},
			url : 'applications/core/service/test.svc/SaveAdo',
			params : Ext.encode({p : q.serializeXML(doc)}),
			scope : this,
			success : function (response , options) {
				this.reload();
			},
			failure : function (response , options) {
				alert("failure");
			}
		});
		// TODO : traiter le journal
	}	
});


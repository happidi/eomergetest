﻿Ext.namespace('Viz.data');

/**
 * @class Viz.data.JsonWriter
 * @extends Ext.data.JsonWriter DataWriter extension for writing an array {@link Ext.data.Record} object(s) in preparation for executing a remote CRUD action.
 */
Viz.data.JsonWriter = Ext.extend(Ext.data.JsonWriter, {

            /**
             * The ctor.
             * @method constructor
             * @param {Object} config The config object.
             */
            constructor   : function(config) {
                config = config || {};
                Ext.apply(config, {
                            listful        : true,
                            encode         : false,
                            writeAllFields : true
                        });
                Viz.data.JsonWriter.superclass.constructor.call(this, config);

                config = config || {};
                var defaultConfig = {
                    /**
                     * @cfg {Boolean} <tt>true to send every field in a delete batch</tt>, <tt>false</tt> otherwise. Default to <tt>false</tt>
                     */
                    deleteAllFields : false
                };
                var forcedConfig = {
                    listful        : true,
                    encode         : false,
                    writeAllFields : true
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);

            },

            /**
             * Return deleted records as a hash so we have access on server side to the whole row.
             * @method destroyRecord
             * @protected
             * @param {Ext.data.Record} rec
             */
            destroyRecord : function(rec) {
                // if an id has been defined we just need to send it back instead of the entire record.
                var idProperty = this.meta.idProperty;
                if (idProperty && !this.deleteAllFields) {
                    var data = {};
                    data[idProperty] = rec.id;
                    return data;
                }
                else
                    return this.toHash(rec);
            }
        });
 
Ext.namespace('Viz.data');
/**
 * @class Viz.data.AdoXmlReader
 * @extends Viz.data.XmlReader
 * @constructor
 * create a new Viz.data.AdoXmlReader
 * Data reader class to create an Array of Ext.data.Record objects from a XML response formated as a ADODB Recordset
 */
Viz.data.AdoXmlReader = function() {
	Viz.data.AdoXmlReader.superclass.constructor.apply(this , arguments);
};
   
Ext.extend(Viz.data.AdoXmlReader  ,Viz.data.XmlReader , {
          
	schema : null, 
   
	// private function a store will implement
	onMetaChange : function(meta, recordType, o){

	},
	
	createRecord : function() {
    	var q = Viz.DomQuery;
        var fields = [];
        var cols = q.select('//s:AttributeType' , this.schema , this.namespaceResolver);
        for(var i = 0, len = cols.length; i < len; i++) {
        	var col = cols[i];
			var fieldname = q.selectValue('@name' , col , this.namespaceResolver);
			var type = this.convertType(q.selectValue('s:datatype/@dt:type' , col , this.namespaceResolver)); 
            fields[fields.length] = {
            	name : fieldname,
                mapping : '@' + fieldname,
                type : type,
                location : 'attribute',
                dateFormat : type == "date" ? "Y-m-d\\TH:i:s" : undefined,
                convert : type == "boolean" ? function(v){ return v === true || v === "true" || v === "True" || v == 1; } : undefined
            };
          }
          return Ext.data.Record.create(fields);
	},
            
    readSchema : function() {
		var q = Viz.DomQuery;
		var o = {
   			record : '//rs:data/z:' + q.selectValue('//s:ElementType/@name' , this.schema , this.namespaceResolver),
			id : '@_order',  
			idlocation : 'attribute'
 			///id : q.selectValue("//s:AttributeType[@rs:keycolumn='true']/@name" , this.schema , this.namespaceResolver)
		}
 		return o;
	},
          
	readRecords : function(doc) {
		this.namespace = {
			's' : 'uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882',
			'dt' : 'uuid:C2F41010-65B3-11d1-A29F-00AA00C14882',
			'rs' : 'urn:schemas-microsoft-com:rowset',
			'z' : '#RowsetSchema',
			'env' : 'urn:vizelia-com:soap'
		};
		this.setNamespaceResolver();
		this.xmlData = doc;
		var root = doc.documentElement || doc;
		var q = Viz.DomQuery;
		this.schema = q.selectNode("//s:Schema[@id='RowsetSchema']" , root , this.namespaceResolver);      
		if(this.schema){
   			this.meta = this.readSchema();
   			this.recordType = this.createRecord();
   			this.onMetaChange(this.meta, this.recordType, doc);
    	}      
     	return Viz.data.AdoXmlReader.superclass.readRecords.apply(this , arguments);
	}
});
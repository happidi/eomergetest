﻿Ext.namespace('Viz.data');
/**
 * @class Viz.data.WCFArraySimpleStore
 * @extends Ext.data.Store Small helper class to make creating Stores from WCF proxy function easier.
 * <p>
 * The service is expected to return a IList or an Array.<p/>
 * <p>
 * Example:
 * </p>
 * 
 * <pre><code>
 * var store = new Viz.data.WCFArraySimpleStore({
 *     remoteSort     : true,
 *     fields         : [{
 *         name    : 'KeySite',
 *         mapping : 'KeySite';
 *     }],
 *     serviceHandler : Viz.Services.CoreWCF.Site_GetChildren,
 *     serviceParams  : {
 *         KeySite : '#124877'
 *     }
 * });
 * store.load();
 * </code></pre>
 */
Viz.data.WCFArraySimpleStore = Ext.extend(Ext.data.Store, {
            /**
             * @cfg {Function} serviceHandler The service endpoint to execute.
             */
            serviceHandler : null,
            /**
             * @cfg {Object} serviceParams The param object to pass to serviceHandler. Each parameter of the service appears as a property of serviceParams.
             */
            serviceParams  : null,
            /**
             * The ctor.
             * @method constructor
             * @param {Object} config The config object.
             */
            constructor    : function(config) {
                Viz.data.WCFArraySimpleStore.superclass.constructor.call(this, Ext.apply(config, {
                                    proxy  : config.proxy || (!config.data ? new Viz.data.WCFHttpProxy({
                                                serviceHandler : config.serviceHandler,
                                                serviceParams  : config.serviceParams
                                            }) : undefined),
                                    reader : new Ext.data.JsonReader({
                                                id : config.id
                                            }, Ext.data.Record.create(config.fields))
                                }));
                // this.on("exception", function(proxy, type, action, options, response, e) {
                // // Ext.Msg.show({
                // // width : 400,
                // // title : $lang("error_ajax"),
                // // msg : String.format($lang("error_msg_ajax"), action) + '<br><br>' + String.format($lang("error_msg_ajax_type"), e._exceptionType) + '<br>' + String.format($lang("error_msg_ajax_description"), e._message),
                // // buttons : Ext.Msg.OK,
                // // icon : Ext.MessageBox.ERROR
                // // });
                // });
            }
        });
/**
 * @class Viz.data.WCFJsonSimpleStore
 * @extends Ext.data.Store Small helper class to make creating Stores from WCF proxy function easier.
 * <p>
 * The service is expected to return a JsonStore<BaseBusinessEntity>.
 * </p>
 * <p>
 * Example:
 * </p>
 * 
 * <pre><code>
 * var store = new Viz.data.WCFJsonSimpleStore({
 *             remoteSort     : true,
 *             serviceHandler : Viz.Services.CoreWCF.Occupant_GetStore,
 *             serviceParams  : {
 *                 flgServer : true
 *             }
 *         });
 * </code></pre>
 * 
 * if the service handler has a parameter of type Vizelia.FOL.BusinessEntities.PagingParameter then we can load the store via paging.
 * 
 * <pre><code>
 * store.load({
 *             params : {
 *                 start : 0,
 *                 limit : PAGING
 *             }
 *         });
 * </code></pre>
 * 
 * if the store should be used in a ComboBox the filter property should be initialized like this:
 * 
 * <pre><code>
 * baseParams : {
 *     filter : 'FirstName LastName Id OrganizationName FirstName+LastName LastName+FirstName'
 * }
 * </code></pre>
 */
Viz.data.WCFJsonSimpleStore = Ext.extend(Ext.data.GroupingStore, {
            autoSave             : false,
            batch                : true,
            restful              : false,
            remoteSort           : true,
            /**
             * @cfg {Boolean} isCrud True if the store should populate the crudStore property with the changes but calling the server is left to do manually if no serviceHandlerCrud was defined, false to call the standard proxy to save.
             */
            isCrud               : true,
            pruneModifiedRecords : false,
            /**
             * @cfg {Function} serviceHandler The service endpoint to execute.
             */
            serviceHandler       : null,
            /**
             * @cfg {Function} serviceHandlerCrud The service endpoint to execute when saving the store.
             */
            serviceHandlerCrud   : null,
            /**
             * @cfg {Object} serviceParams The param object to pass to serviceHandler. Each parameter of the service is a property of serviceParams.
             */
            serviceParams        : null,
            /**
             * @cfg {Boolean} <tt>true to send every field in a delete batch</tt>, <tt>false</tt> otherwise. Default to <tt>false</tt>
             */
            deleteAllFields      : false,
            /**
             * The ctor.
             * @method constructor
             * @param {Object} config The config object.
             */
            constructor          : function(config) {
                config = config || {};
                var defaultConfig = {
                    autoDestroy : true
                };
                var forcedConfig = {
                    crudStore            : {},
                    pruneModifiedRecords : false
                };
                Ext.applyIf(config, defaultConfig);
                Ext.apply(config, forcedConfig);
                Ext.applyIf(config, this);
                var reader;
                var writer;
                // we check the config object
                // if a fields attribute has been defined, we use it
                if (!config.fields) {
                    // if not we except a metaData configuration from the server
                    reader = new Ext.data.JsonReader();
                }
                else {
                    reader = new Ext.data.JsonReader({
                                id            : config.id,
                                totalProperty : "recordCount",
                                root          : "records"
                            }, Ext.data.Record.create(config.fields));
                }
                if (config.serviceHandlerCrud || config.isCrud) {
                    writer = new Viz.data.JsonWriter({
                                deleteAllFields : config.deleteAllFields
                            });
                }
                Ext.apply(config, {
                            proxy  : config.proxy || (!config.data ? new Viz.data.WCFHttpProxy({
                                        serviceHandler     : config.serviceHandler,
                                        serviceHandlerCrud : config.serviceHandlerCrud,
                                        serviceParams      : config.serviceParams
                                    }) : undefined),
                            reader : reader,
                            writer : writer
                        });
                Viz.data.WCFJsonSimpleStore.superclass.constructor.call(this, config);
                // if (this.proxy)
                // this.relayEvents(this.proxy, ["loadexception", "exception"]);
                // to make sure the writer meta property is synchronised with the reader meta property.
                // this.on("exception", function(proxy, type, action, options, response, e) {
                // // Ext.Msg.show({
                // // width : 400,
                // // title : $lang("error_ajax"),
                // // msg : String.format($lang("error_msg_ajax"), action) + '<br><br>' + String.format($lang("error_msg_ajax_type"), e._exceptionType) + '<br>' + String.format($lang("error_msg_ajax_description"), e._message),
                // // buttons : Ext.Msg.OK,
                // // icon : Ext.MessageBox.ERROR
                // // });
                // });
                this.on("metachange", this.onWCFMetaChange, this);

                if (this.isCrud == true) {
                    this.on("beforewrite", this.onWCFBeforeWrite, this);
                }
            },

            onWCFMetaChange      : function(store, meta) {
                if (store.writer) {
                    store.writer.meta = meta;
                }
            },

            onWCFBeforeWrite     : function(store, action, rs, options) {
                // we test the presence of apply function on the writer for version Extjs >= 3.2.0
                if (Ext.isFunction(store.writer.apply)) {
                    store.writer.apply(options.params, store.baseParams, action, rs);
                    this.crudStore[action] = options.params.jsonData.records;
                }
                // previous version of Extjs
                else {
                    store.writer.write(action, options.params, rs);
                    this.crudStore[action] = options.params.records;
                }
                return false;
            },

            /**
             * Overrides save to initialize the crudStore property before saving if isCrud is <tt>true</tt>.
             */
            save                 : function() {
                if (this.isCrud) {
                    this.crudStore = {};
                }
                if (!this.hasLoaded) {
                    return;
                }
                Viz.data.WCFJsonSimpleStore.superclass.save.apply(this, arguments);
                if (Ext.isFunction(this.serviceHandlerCrud) && Ext.isObject(this.crudStore)) {
                    var o = {
                        store   : this.crudStore,
                        success : function(newstore) {
                            if (!newstore.success) {
                                Ext.Msg.show({
                                            width   : 400,
                                            title   : $lang("error_ajax"),
                                            msg     : $lang("error_save") + ":<br/>" + newstore.message,
                                            buttons : Ext.Msg.OK,
                                            icon    : Ext.MessageBox.ERROR
                                        });
                                this.reload();
                            }
                            else {
                                this.removed = [];
                                this.reload();
                            }
                            // this will unmask the grid.
                            this.fireEvent("save", this);
                        },
                        failure : function(e, o) {
                            this.fireEvent("save", this);
                            Ext.Msg.show({
                                        width   : 400,
                                        title   : $lang("error_ajax"),
                                        msg     : $lang("error_msg_unknown_detail"),
                                        details : String.format($lang("error_msg_ajax"), o.url) + '<br><br>' + String.format($lang("error_msg_ajax_description"), $lang("error_msg_unknown") + '<br>' + e.Message),
                                        buttons : Ext.Msg.OK,
                                        icon    : Ext.MessageBox.ERROR
                                    });
                        },
                        scope   : this
                    };
                    Ext.apply(o, this.serviceParams);
                    this.serviceHandlerCrud(o);
                }
            },

            sort                 : function(fieldName, dir) {
                if (!Ext.isEmpty(fieldName)) {
                    Viz.data.WCFJsonSimpleStore.superclass.sort.apply(this, arguments);
                }
            },

            /**
             * function to be called in the beforeload event in order to cache the result and not to reload the store everytime.
             * @param {} cachedStorageObject
             * @return {Boolean}
             */
            beforeLoadWithCache  : function(store, options, cachedStorageName) {
                // TODO: Fix the stack overflow bug, currently overriding to no cache.
                // return true;

                if (!Ext.isEmpty(Viz.Configuration[cachedStorageName])) {
                    this.add(Viz.Configuration[cachedStorageName]);
                    this.hasLoaded = true;

                    if (options.callback) {
                        options.callback.call(options.scope || this);
                    }
                    this.fireEvent('load', this, Viz.Configuration[cachedStorageName], options);
                    return false;
                }
                return true;
            },

            /**
             * function to be called in the load event in order to cache the result and not to reload the store everytime.
             */
            loadWithCache        : function(store, records, options, cachedStorageName) {
            	var cachedRecords = Viz.copyDataRecords(records);
                Viz.Configuration[cachedStorageName] = cachedRecords;

                if (Ext.isArray(this.valuesToRemove)) {
                    Ext.each(this.valuesToRemove, function(valueToRemove) {
                                this.remove(this.getById(valueToRemove));
                            }, this);
                }
                return true;
            }
        });

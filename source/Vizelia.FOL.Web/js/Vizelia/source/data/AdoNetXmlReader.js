Ext.namespace('Viz.data');   
       
Viz.data.AdoNetXmlReader = function() {
	Viz.data.AdoNetXmlReader.superclass.constructor.apply(this , arguments);
};
       
Ext.extend(Viz.data.AdoNetXmlReader  , Viz.data.XmlReader , {
          
   	schema : null, 
    
   	/**
   	 * Parses the XML document to build the fields definition
   	 * <p>To create a new Record :</p>
   	 * <p>
   	 * <code>
   	 * var rec = new this.recordType({
   	 * 		field1 : value1, field2 : value2 , ...
   	 * } , id);
   	 * </code>
   	 * </p>
   	 * @return {Ext.data.Record} The Ext.data.Record returned is stored in recordType property
   	 */
	createRecord : function() {
    	var q = Viz.DomQuery;
        var fields = [];
        var cols = q.select('xs:element/xs:complexType/xs:choice/xs:element/xs:complexType/xs:sequence/xs:element' , this.schema , this.namespaceResolver);
		for(var i = 0, len = cols.length; i < len; i++) {
			var col = cols[i];
			var fieldname =  q.selectValue('@name' , col , this.namespaceResolver);
			var type = this.convertType(q.selectValue('@type' , col , this.namespaceResolver).replace('xs:' , ''));
		    fields[fields.length] = {
		    	name : fieldname,
		        mapping :  fieldname,
		        type : type,
		        location : 'content',
		        dateFormat : type == "date" ? "c" : undefined
		    };
		}
		var recordTypeFactory =   Ext.data.Record.create(fields);
	    var recordType = Ext.extend(recordTypeFactory , {
	    	constructor : function (data, id) {
	    		this.id = (id || id === 0) ? id : ++Ext.data.Record.AUTO_ID;
				this.data = data;		
	    	}
   		});
		return recordType;
	},
 
	/**
	 * Create the meta object from the XML document.
	 * @return {Object} meta 
	 */
	readSchema : function() {
		var q = Viz.DomQuery;
		var o = {
			record : "//" + q.selectValue('//xs:element/xs:complexType/xs:choice/xs:element/@name' , this.schema , this.namespaceResolver),
			id : this.meta.idColumn ?  this.meta.idColumn : '@msdata:rowOrder',
			idlocation : this.meta.idColumn ? 'content' : 'attribute'
		}
		return o;
	},

	// private function a store will implement
	onMetaChange : function(meta, recordType, o){
	
	},

	/**
	 * Create a data block containing Ext.data.Records from an XML document.
	 * @param {Object} doc A parsed XML document.
	 * @return {Object} records A data block which is used by an {@link Ext.data.Store} as
	 * a cache of Ext.data.Records.
	 */
	readRecords : function(doc) {
		this.namespace = {
		   'xs' : 'http://www.w3.org/2001/XMLSchema',
		   'msdata' : 'urn:schemas-microsoft-com:xml-msdata',
		   'diffgr' : 'urn:schemas-microsoft-com:xml-diffgram-v1'
		};
		this.setNamespaceResolver();
		this.xmlData = doc;
		var root = doc.documentElement || doc;
		var q = Viz.DomQuery;
		this.schema = q.selectNode("//xs:schema[@id]" , root , this.namespaceResolver);      
			if(this.schema){
	   			this.meta = this.readSchema();
	   			this.recordType = this.createRecord();
	   			this.onMetaChange(this.meta, this.recordType, doc);
	    	}      
	     return Viz.data.AdoNetXmlReader.superclass.readRecords.apply(this , arguments);    	
	}
});

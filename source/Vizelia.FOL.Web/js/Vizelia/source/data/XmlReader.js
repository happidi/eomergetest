
Ext.namespace('Viz.data');
	
Viz.data.XmlReader = function(meta , recordtype) {
	Viz.data.XmlReader.superclass.constructor.apply(this , arguments);
	if(meta)	
		if(meta.namespace)
			this.namespace = meta.namespace
};
       
Ext.extend(Viz.data.XmlReader  , Ext.data.XmlReader , {
 
 
	read: function(response) {
    
		var doc;
		// We examine the response type
		// if we get json we assume that the xml is in the property d sent back by the server
		if (response.getResponseHeader["Content-Type"].toLowerCase().indexOf("application/json") >= 0)
		    doc = Viz.DomQuery.createXMLDoc((Ext.decode(response.responseText)).d);
		else
		    doc = response.responseXML; 
				
		if (!doc) {
		    throw { message: "XmlReader.read: XML Document not available" };
		}
		return this.readRecords(doc);
	},
 
	convertType : function(xmltype){
		switch(xmltype) {
			case 'short' :
    	case 'i2'  :
        	return 'int';
		case 'dateTime' : 
			return 'date';
		case 'uuid' :
			return 'string'
		case 'double' :
			return 'float';
		default :
			return xmltype;
		}
	},
	
	setNamespaceResolver : function() {
		if(this.namespace)
			this.namespaceResolver = (function(prefix){
				return this.namespace[prefix] || null;
		}).createDelegate(this);			
	},
	
	readRecords : function(doc) {
		this.setNamespaceResolver();
		this.xmlData = doc;
		var root = doc.documentElement || doc;
		var q = Viz.DomQuery; // INSTEAD OF DomQuery
    	var recordType = this.recordType, fields = recordType.prototype.fields;
    	var sid = this.meta.id;
    	var sidlocation = this.meta.idlocation;
    	var totalRecords = 0, success = true;
    	if(this.meta.totalRecords){
    	    totalRecords = q.selectNumber(this.meta.totalRecords, root, 0);
    	}
        if(this.meta.success){
            var sv = q.selectValue(this.meta.success, root, true);
            success = sv !== false && sv !== 'false';
    	}
    	var records = [];
    	var nsResolver = this.namespaceResolver;
    	var ns = q.select( this.meta.record, root , nsResolver);
    	if(!ns) 
    		return {
    			success : true,
    			records :[],
    			totalRecords : 0
    		};
        for(var i = 0, len = ns.length; i < len; i++) {
	        var n = ns[i];
	        var values = {};
	        if(sidlocation == "attribute")
	        	var id = sid ? q.selectValue(sid, n , nsResolver) : undefined;
	        else if (sidlocation == "content")
	       		 var id = sid ? q.selectNodeText(sid, n , nsResolver) : undefined;
	        for(var j = 0, jlen = fields.length; j < jlen; j++){
	            var f = fields.items[j];
	            if(f.location == "attribute")
	            	var v = q.selectValue( (f.mapping || f.name), n, nsResolver);
                else if(f.location == "content")
                	var v =  q.selectNodeText( (f.mapping || f.name), n, nsResolver);
                v = f.convert(v, n);
	            values[f.name] = v;
	        }
	        var record = new recordType(values, id);
	        record.node = n;
	        records[records.length] = record;
	    }
	    return {
	        success : success,
	        records : records,
	        totalRecords : totalRecords || records.length
	    };
	}
});
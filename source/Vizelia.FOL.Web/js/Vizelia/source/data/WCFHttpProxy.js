﻿Ext.namespace('Viz.data');

/**
 * @class Viz.data.WCFHttpProxy
 * @extends Ext.data.DataProxy An implementation of {@link Ext.data.DataProxy} that reads the result from a WCF script proxy function<br>
 * <p>
 * <b>Note that this class cannot be used to retrieve data from a domain other than the domain from which the running page was served.<br>
 * @constructor
 * @param {Object} containing the script proxy function and parameters options passed to the function.
 */
Viz.data.WCFHttpProxy = function(config) {
    Ext.data.HttpProxy.superclass.constructor.call(this);
    /**
     * The Connection object (Or options parameter to {@link Ext.Ajax#request}) which this HttpProxy uses to make requests to the server. Properties of this object may be changed dynamically to change the way data is requested.
     * @property
     */
    this.serviceHandler = config.serviceHandler;
    this.serviceHandlerCrud = config.serviceHandlerCrud;
    this.serviceParams = config.serviceParams;

};

Ext.extend(Viz.data.WCFHttpProxy, Ext.data.DataProxy, {

            doRequest      : function(action, rs, params, reader, callback, scope, options) {
                // default implementation of doRequest for backwards
                // compatibility with 2.0 proxies.
                // If we're executing here, the action is probably "load".
                // Call with the pre-3.0 method signature.
                if (action === Ext.data.Api.actions.read) {
                    this.load(params, reader, callback, scope, options);
                }
                else {
                    var o = {
                        action   : action,
                        records  : params[reader.meta.root],
                        argument : {
                            callback : callback,
                            rs       : rs,
                            scope    : scope,
                            reader   : reader
                        },
                        success  : this.createCallback(action, rs),
                        failure  : this.createCallback(action, rs),
                        /*
                         * success : function(o, arg) { arg.callback.call(arg.scope, o); }, failure : function() { this.fireEvent("exception", this, 'response', action); },
                         */
                        scope    : this
                    };
                    Ext.applyIf(o, this.serviceParams);
                    this.serviceHandlerCrud.call(scope || this, o);
                }
            },

            /**
             * Returns a callback function for a request. Note a special case is made for the read action vs all the others.
             * @param {String} action [create|update|delete|load]
             * @param {Ext.data.Record[]} rs The Store-recordset being acted upon
             * @private
             */
            createCallback : function(action, rs) {
                return function(o, arg) {
                    if (!o.success) {
                        if (action === Ext.data.Api.actions.read) {
                            // @deprecated: fire loadexception for backwards compat.
                            this.fireEvent('loadexception', this, o, arg);
                        }
                        this.fireEvent('exception', this, 'response', action, o, arg);
                        arg.callback.call(arg.scope, null, arg, false);
                        return;
                    }
                    if (action === Ext.data.Api.actions.read) {
                        // we should never be here
                        throw new Error('Viz.data.WCFHttpProxy is not supposed to read via an api configuration.')
                    }
                    else {
                        this.onWrite(action, o, arg, rs);
                    }
                }
            },

            /**
             * Callback for write actions
             * @param {String} action [Ext.data.Api.actions.create|read|update|destroy]
             * @param {Object} trans The request transaction object
             * @param {Object} res The server response
             * @private
             */
            onWrite        : function(action, o, arg, rs) {
                if (o[arg.reader.meta.successProperty] === false) {
                    this.fireEvent('exception', this, 'remote', action, o, arg, rs);
                }
                else {
                    this.fireEvent('write', this, action, o[arg.reader.meta.root], o, rs, arg);
                }
                arg.callback.call(arg.scope, o[arg.reader.meta.root], o, o[arg.reader.meta.successProperty]);
            },
            /**
             * Load data from the configured {@link Ext.data.Connection}, read the data object into a block of Ext.data.Records using the passed {@link Ext.data.DataReader} implementation, and process that block using the passed callback.
             * @method load
             * @param {Object} params An object containing properties which are to be used as HTTP parameters for the request to the remote server.
             * @param {Ext.data.DataReader} reader The Reader object which converts the data object into a block of Ext.data.Records.
             * @param {Function} callback The function into which to pass the block of Ext.data.Records. The function must be passed
             * <ul>
             * <li>The Record block object</li>
             * <li>The "arg" argument from the load function</li>
             * <li>A boolean success indicator</li>
             * </ul>
             * @param {Object} scope The scope in which to call the callback
             * @param {Object} arg An optional argument which is passed to the callback as its second parameter.
             */
            load           : function(params, reader, callback, scope, arg) {
                if (this.fireEvent("beforeload", this, params) !== false) {

                    var o = {
                        paging   : params || {},
                        argument : {
                            callback : callback,
                            scope    : scope,
                            arg      : arg,
                            reader   : reader
                        },
                        reader   : reader,
                        success  : this.loadResponse,
                        failure  : this.loadError,
                        scope    : this
                    };
                    Ext.applyIf(o, this.serviceParams);
                    Ext.applyIf(o, this.baseParams);
                    Ext.applyIf(o, params);
                    if (this.lastRequest != null && this.lastRequest.conn != null) {
                        var executor = this.lastRequest.conn;
                        if (executor.readyState < 4) {
                            try {
                                executor.abort();
                            }
                            catch (e) {
                                ;
                            }
                        }
                    }
                    this.lastRequest = this.serviceHandler.call(scope || this, o);
                }
                else {
                    callback.call(scope || this, null, arg, false);
                }
            },

            // private
            loadResponse   : function(o, arg) {
                try {
                    var result = arg.reader.readRecords(o);
                    if (!result.success) {
                        this.fireEvent("exception", this, "remote", "", arg, o, null);
                        return;
                    }
                }
                catch (err) {
                    this.fireEvent("loadexception", this, arg, null, err);
                    this.fireEvent("exception", this, "response", "", arg, null, err);
                    return;
                }
                this.fireEvent("load", this, o, arg);
                arg.callback.call(arg.scope, result, arg.arg, true);
            },

            // private
            loadError      : function(err, arg, methodName) {
                this.fireEvent("loadexception", this, arg, null, err);
                this.fireEvent("exception", this, 'remote', methodName, arg, null, err);
            }
        });

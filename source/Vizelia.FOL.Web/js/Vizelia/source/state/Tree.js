﻿// vim: ts=4:sw=4:nu:fdc=4:nospell
/* global Ext */
/**
 * @class Ext.ux.state.TreePanel Ext.tree.TreePanel State Plugin Usage: var tree = new Ext.tree.TreePanel({ root:{ nodeType:'async' ,id:'root' ,text:'Root' } ,loader: { url:'get-tree.php' } ,plugins:[new Ext.ux.state.TreePanel()] });
 * @author Ing. Jozef Sakáloš
 * @copyright (c) 2009, by Ing. Jozef Sakáloš
 * @date 5. April 2009
 * @version 1.0
 * @revision $Id: Ext.ux.state.TreePanel.js 676 2009-04-07 13:03:20Z jozo $
 * @license Ext.ux.state.TreePanel.js is licensed under the terms of the Open Source LGPL 3.0 license. Commercial use is permitted to the extent that the code/component(s) do NOT become part of another Open Source or Commercially licensed development library or toolkit without explicit permission. License details: http://www.gnu.org/licenses/lgpl.html
 * @forum 64714
 * @demo http://examples.extjs.eu/?ex=treestate
 * @download Ext.ux.state.TreePanel.js.bz2 Ext.ux.state.TreePanel.js.gz Ext.ux.state.TreePanel.js.zip
 * @donate
 */

Ext.ns('Ext.ux.state');

// dummy constructor
Ext.ux.state.TreePanel = function() {
};

Ext.override(Ext.ux.state.TreePanel, {
            /**
             * Initializes the plugin
             * @param {Ext.tree.TreePanel} tree
             * @private
             */
            init : function(tree) {
                // update state on node expand or collapse
                tree.stateEvents = tree.stateEvents || [];
                tree.stateEvents.push('expandnode', 'collapsenode', 'collapse', 'expand', 'click');

                // install event handlers on TreePanel
                tree.on({
                            // add path of expanded node to stateHash
                            beforeexpandnode   : function(n) {
                                if (n.getPathOriginal)
                                    this.stateHash[n.id] = n.getPathOriginal();
                                else
                                    this.stateHash[n.id] = n.getPath();
                            }

                            // delete path and all subpaths of collapsed node from stateHash
                            ,
                            beforecollapsenode : function(n) {
                                delete this.stateHash[n.id];
                                var cPath = "";
                                if (n.getPathOriginal)
                                    cPath = n.getPathOriginal();
                                else
                                    cPath = n.getPath();
                                for (var p in this.stateHash) {
                                    if (this.stateHash.hasOwnProperty(p)) {
                                        if (-1 !== this.stateHash[p].indexOf(cPath)) {
                                            delete this.stateHash[p];
                                        }
                                    }
                                }
                            },
                            click              : function(n, e) {                                
                                this.stateSelectNode = n.getPathOriginal('text');
                            },
                            checkchange        : function(n, checked) {
                                var pathAttr = n.getOwnerTree().pathAttribute;
                                if (checked == true)
                                    this.stateCheckedNodes[n.id] = n.getPathOriginal(pathAttr);
                                else
                                    delete this.stateCheckedNodes[n.id];
                            }
                        });

                // add state related props to the tree
                Ext.apply(tree, {
                            // keeps expanded nodes paths keyed by node.ids
                            stateHash         : {},
                            stateCheckedNodes : {},
                            stateSelectNode   : null,
                            // apply state on tree initialization
                            applyState        : function(state) {
                                if (state) {
                                    Ext.apply(this, state);

                                    // it is too early to expand paths here
                                    // so do it once on root load
                                    this.root.on({
                                                load : {
                                                    single : true,
                                                    scope  : this,
                                                    fn     : function() {
                                                        for (var p in this.stateHash) {
                                                            if (this.stateHash.hasOwnProperty(p)) {
                                                                this.expandPath(this.stateHash[p]);
                                                            }
                                                        }
                                                        if (this.checkedState) {
                                                            var explore = function(bSuccess, oSelNode) {
                                                                if (!bSuccess)
                                                                    return;
                                                                oSelNode.checked = true;
                                                                oSelNode.ui.toggleCheck(true);
                                                            };
                                                            for (var p in this.stateCheckedNodes) {
                                                                if (this.stateCheckedNodes.hasOwnProperty(p)) {
                                                                    this.selectPath(this.stateCheckedNodes[p], null, explore);
                                                                }
                                                            }
                                                        }
                                                        
                                                        if (this.stateSelectNode){
                                                        	 var selectNode = function(bSuccess, oSelNode) {
                                                                if (!bSuccess)
                                                                    return;                                                                
                                                                oSelNode.fireEvent("click", oSelNode, null);
                                                            };
                                                        	
                                                        	this.selectPath(this.stateSelectNode,'text', selectNode);
                                                        }
                                                    }
                                                }
                                            });
                                }
                            } // eo function applyState

                            // returns stateHash for save by state manager
                            ,
                            getState          : function() {
                                return {
                                    collapsed         : this.collapsed,
                                    stateHash         : this.stateHash,
                                    stateCheckedNodes : this.stateCheckedNodes,
                                    stateSelectNode   : this.stateSelectNode
                                };
                            } // eo function getState
                        });
            } // eo function init

        }); // eo override

// eof

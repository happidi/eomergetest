﻿/**
 * Provider implementation that stores preferences using a RESTful Resource.
 */
Ext.state.RESTfulProvider = Ext.extend(Ext.state.Provider, {

            constructor              : function(config) {
                config = config || {};

                var defaultConfig = {
                    serviceHandlerRead         : Viz.Services.CoreWCF.UserProfilePreference_GetListByUser,
                    serviceHandlerUpdateGlobal : Viz.Services.CoreWCF.UserProfilePreference_UpdateGlobal,
                    serviceHandlerUpdate       : Viz.Services.CoreWCF.UserProfilePreference_Update
                };

                Ext.applyIf(config, defaultConfig);

                Ext.state.RESTfulProvider.superclass.constructor.call(this);

                Ext.apply(this, config);
                this.state = {};
                this.readPreferenceDB(this);
            },

            set                      : function(name, value) {
                if (typeof value == "undefined" || value === null) {
                    this.clear(name);
                    return;
                }

                var globalUpdate = false;
                if (name.endsWith('_global')) {
                    globalUpdate = true;
                    this.setPreference(name.substr(0, name.length - 7), value, globalUpdate);
                }
                else {
                    this.setPreference(name, value, globalUpdate);
                }

                // this.setPreference(name, value, globalUpdate);
                Ext.state.RESTfulProvider.superclass.set.call(this, name, value);
            },

            clear                    : function(name) {
                this.clearPreference(name);
                Ext.state.RESTfulProvider.superclass.clear.call(this, name);
            },

            readPreferenceDB         : function(provider) {
                this.serviceHandlerRead({
                            userName : this.impersonateUserName, // used to impersonate another user (in PrintScreen.aspx).
                            success  : function(response) {
                                this.state = this.handlePreferenceResponse(response);
                            },
                            scope    : this
                        });

            },

            handlePreferenceResponse : function(response) {
                var prefs = {};
                Ext.each(response, function(node) {
                            var prefKey = node.KeyUser === '' ? node.PreferenceKey + '_global' : node.PreferenceKey;
                            var decodedValue = this.decodeValue(unescape(node.PreferenceValue));
                            prefs[prefKey] = decodedValue;
                        }, this);
                return prefs;
            },

            setPreference            : function(name, value, isGlobalUpdate) {
                // we only send the preference value if it is different from the one cached on the client.
                if (isGlobalUpdate) {
                    if (escape(this.encodeValue(this.state[name])) != escape(this.encodeValue(value))) {
                        this.serviceHandlerUpdateGlobal({
                                    preferenceKey   : escape(name),
                                    preferenceValue : escape(this.encodeValue(value)),
                                    success         : function(response) {
                                    },
                                    scope           : this
                                });
                    }
                }
                else {
                    if (escape(this.encodeValue(this.state[name])) != escape(this.encodeValue(value))) {
                        this.serviceHandlerUpdate({
                                    preferenceKey   : escape(name),
                                    preferenceValue : escape(this.encodeValue(value)),
                                    success         : function(response) {
                                    },
                                    scope           : this
                                });
                    }
                }
            },

            clearPreference          : function(name) {
                /*
                 * Ext.Ajax.request({ // makes an async request url : this.url + "/" + escape(name), method : 'DELETE', success : function(response, options) { // call back for the async request console.log('value asynchronously deleted in database' + response.responseText); }, failure : function(response, options) { // console.log('defaulting to cookies.'); } });
                 */
            }
        });

﻿Ext.namespace('Ext.ux.state.TabPanel');
Ext.ux.state.TabPanel = function() {
};

// provide a custom implementation of tabpanel state in order to save the activated tab.
Ext.override(Ext.ux.state.TabPanel, {
            /**
             * Initializes the plugin
             * @param {Ext.TabPanel} tabpanel
             * @private
             */
            init : function(tabpanel) {

                // update state on node expand or collapse
                tabpanel.stateEvents = tabpanel.stateEvents || [];
                tabpanel.stateEvents.push('tabchange');
                tabpanel.stateEvents.push('collapse');
                tabpanel.stateEvents.push('expand');

                // add state related props to the tree
                Ext.apply(tabpanel, {

                            // returns activeTab
                            getState : function(s) {
                                var state = Ext.TabPanel.prototype.getState.call(this) || {};
                                Ext.apply(state, {
                                            activeTab : this.items.indexOf(this.getActiveTab()),
                                            collapsed : this.collapsed
                                        });
                                return state;
                            } // eo function getState

                        });
            } // eo function init

        }); // eo override

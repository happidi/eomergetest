﻿Ext.namespace("Viz.Configuration.Columns", "Viz.Configuration.Editors");
/**
 * Returns the category of a classification item from its notation value
 * @param {string} code The notation value of the classification category
 * @return {string}
 */
Viz.Configuration.GetCategoryFromClassificationItemDefinition = function(code) {
    var store = Viz.Configuration.ClassificationItemDefinition;
    var index = store.findBy(function(record, id) {
                if (code.startsWith(record.get('Code')))
                    return true;
                return false;
            });
    if (index >= 0)
        return store.getAt(index).get("Category");
};
/**
 * Returns the Key of a classification item from its Category
 * @param {string} category The category of the ClassificationItem
 * @return {string}
 */
Viz.Configuration.GetClassificationItemDefinitionKeyFromCategory = function(category) {
    var store = Viz.Configuration.ClassificationItemDefinition;
    var index = store.findExact("Category", category);
    if (index >= 0)
        return store.getAt(index).get("KeyClassificationItem");
};
/**
 * Returns the code of a classification item from its Category
 * @param {string} category The category of the ClassificationItem
 * @return {string}
 */
Viz.Configuration.GetClassificationItemDefinitionCodeFromCategory = function(category) {
    var store = Viz.Configuration.ClassificationItemDefinition;
    var index = store.findExact("Category", category);
    if (index >= 0)
        return store.getAt(index).get("Code");
};
/**
 * Returns the classification item entity from its Category
 * @param {string} category The category of the ClassificationItem
 * @return {string}
 */
Viz.Configuration.GetClassificationItemDefinitionEntityFromCategory = function(category) {
    var retVal = null;
    var store = Viz.Configuration.ClassificationItemDefinition;
    var index = store.findExact("Category", category);
    if (index >= 0) {
        retVal = store.getAt(index).data;
        retVal.__type = 'ClassificationItem:#Vizelia.FOL.BusinessEntities';
    }
    return retVal;
};

/**
 * Returns the enum value from the enum name and item id.
 * @param {string} enumName The enum name
 * @param {string} id The enum item id
 * @return {string}
 */
Viz.Configuration.GetEnumValue = function(enumName, id) {
    var enums = Viz.Configuration.EnumRecords['Vizelia.FOL.BusinessEntities.' + enumName + ',Vizelia.FOL.Common'];
    if (enums == null)
        return null;
    var store = enums[0].store;

    var retVal = null;
    var index = store.findExact("Id", id);
    if (index >= 0) {
        retVal = store.getAt(index).data.Value;
    }
    return retVal;
};

/**
 * Returns the enum label from the enum name and value.
 * @param {string} enumName The enum name
 * @param {int} value The enum item value
 * @return {string}
 */
Viz.Configuration.GetEnumLabel = function(enumName, value) {
    var enums = Viz.Configuration.EnumRecords['Vizelia.FOL.BusinessEntities.' + enumName + ',Vizelia.FOL.Common'];
    if (enums == null)
        return null;
    var store = enums[0].store;

    var retVal = '';
    var index = store.findExact("Value", value);
    if (index >= 0) {
        retVal = store.getAt(index).data.Label;
    }
    return retVal;
}

/**
 * Standard text editor.
 */
Viz.Configuration.Editors.StandardTextField = new Ext.form.TextField();
/**
 * Organization ComBox.
 */
Viz.Configuration.Editors.ComboBoxOrganization = new Ext.form.ComboBox({
            pageSize          : 50,
            mode              : 'remote',
            triggerAction     : 'all',
            displayField      : 'Name',
            valueField        : 'KeyOrganization',
            listHeight        : 300,
            loadMask          : true,
            valueNotFoundText : '',
            forceSelection    : true,
            typeAhead         : true,
            minChars          : 1,
            lazyRender        : true,
            store             : new Viz.data.WCFJsonSimpleStore({
                        serviceHandler : Viz.Services.CoreWCF.Organization_GetStore,
                        sortInfo       : {
                            field     : 'Name',
                            direction : 'ASC'
                        },
                        baseParams     : {
                            filter : 'Name'
                        }
                    })
        });
Viz.Configuration.Editors.ComboBoxOrganizationId = new Ext.form.ComboBox({
            pageSize          : 50,
            mode              : 'remote',
            triggerAction     : 'all',
            displayField      : 'Id',
            valueField        : 'KeyOrganization',
            valueNotFoundText : '',
            listHeight        : 300,
            loadMask          : true,
            forceSelection    : true,
            query             : 'all',
            typeAhead         : true,
            minChars          : 1,
            lazyRender        : true,
            store             : new Viz.data.WCFJsonSimpleStore({
                        serviceHandler : Viz.Services.CoreWCF.Organization_GetStore,
                        sortInfo       : {
                            field     : 'Id',
                            direction : 'ASC'
                        },
                        baseParams     : {
                            filter : 'Id'
                        }
                    })
        });
/**
 * Combobox renderer
 */
Ext.ux.comboBoxRenderer = function(combo, fieldName) {
    return function(value, meta, rec) {
        var idx = combo.store.find(combo.valueField, value);
        if (idx == -1) {
            return rec.get(fieldName);
        }
        else {
            var recCombo = combo.store.getAt(idx);
            return recCombo.get(combo.displayField);
        }
    };
};
/**
 * Enum renderer
 * @param {Viz.store.Enum} store
 * @param {Array} excludedValues
 * @return {string}
 */
Ext.ux.comboBoxEnumRenderer = function(store, excludedValues) {
    return {
        loadStore : function() {
            store.load();
        },
        fn        : function(value, cell, rec) {
            var retVal = value;
            var index = store.find("Value", value);
            if (index >= 0) {
                retVal = store.getAt(index).get("Label");
                var iconCss = store.getAt(index).get("IconCls");
                if (!Ext.isEmpty(iconCss))
                    cell.css = 'viz-icon-cell ' + iconCss;
            }
            if (!Ext.isArray(excludedValues) || excludedValues.indexOf(retVal) == -1)
                return retVal;
            return '';
        }
    };

};

/**
 * IconCls renderer
 */
Ext.ux.iconClsRenderer = function() {
    return function(data, cell, record) {
        var iconCss = record.get("IconCls");
        cell.css = 'viz-icon-cell ' + iconCss;
        return data;
    };
};

/**
 * Location Path renderer
 */
Ext.ux.locationPathRenderer = function(field) {
    return function(data, cell, record) {
        var locationType = record.get(field || "LocationTypeName");
        var iconCss = Viz.Configuration.LocationCss(locationType);
        cell.css = 'viz-icon-cell ' + iconCss;
        return data;
    };
};

/**
 * Workflow state renderer
 */
Ext.ux.workflowStateRenderer = function() {
    return function(data, cell, record) {
        var state = record.get("State");
        var cssIcon = Viz.Configuration.StateCss(state);
        cell.css = 'viz-icon-cell ' + cssIcon;
        return data;
        // return $lang('msg_workflow_state_' + state.toLowerCase());
    };
};

/**
 * Logging renderer
 */
Ext.ux.LoggingRenderer = function() {
    return function(data, cell, record) {
        var isSuccess = record.get("IsSuccess");
        if (isSuccess)
            cell.css = 'viz-icon-cell ' + 'viz-icon-small-success';
        else
            cell.css = 'viz-icon-cell ' + 'viz-icon-small-exception';
        return data;
    };
};

/**
 * TraceEntry renderer
 */
Ext.ux.TraceEntryRenderer = function() {
    return function(data, cell, record) {
        var severity = record.get("Severity");
        switch (severity) {
            case Vizelia.FOL.BusinessEntities.TraceEntrySeverity.Information :
                cell.css = 'viz-icon-cell ' + 'viz-icon-small-information';
                break;

            case Vizelia.FOL.BusinessEntities.TraceEntrySeverity.Warning :
                cell.css = 'viz-icon-cell ' + 'viz-icon-small-warning';
                break;

            case Vizelia.FOL.BusinessEntities.TraceEntrySeverity.Error :
                cell.css = 'viz-icon-cell ' + 'viz-icon-small-exception';
                break;

        }

        return data;
    };
};

Ext.ux.LongTextRenderer = function(value) {
    var str = '<div class="viz-grid-nowrap-text">' + value + '</div>';
    return str;
};
/**
 * Color renderer.
 * @param {String} prefix : the prefix of the ColorR,ColorG,ColorB fields.
 * @param {String} fieldName: an extra Field to display.
 * @return {String}
 */
Ext.ux.colorRenderer = function(prefix, fieldName) {
    prefix = prefix || '';
    return function(data, cell, record) {
        var r = record.get(prefix + "ColorR");
        var g = record.get(prefix + "ColorG");
        var b = record.get(prefix + "ColorB");
        var retVal = "";
        if (Ext.isNumber(r) && Ext.isNumber(g) && Ext.isNumber(b)) {
            var img = Ext.ux.colorImg(r, g, b);
            retVal = img;
        }
        if (fieldName) {
            retVal += record.get(fieldName);
        }
        return retVal;
    };
}
Ext.ux.colorImg = function(r, g, b) {
    var src = Ext.ux.colorImgSrc(r, g, b, true);
    return '<img class="viz-grid-img-row" src="' + src + '" />';
}

Ext.ux.colorImgSrc = function(r, g, b, grid) {
    var src = '~/color.asmx?';
    if (grid === true)
        src += 'grid=true&';
    src += 'R=' + r + '&G=' + g + '&B=' + b;
    return src;
}

Viz.Configuration.LocationCss = function(locationType) {
    switch (locationType) {
        case Vizelia.FOL.BusinessEntities.HierarchySpatialTypeName.IfcSite :
        case 'IfcSite' :
            return 'viz-icon-small-site';
        case Vizelia.FOL.BusinessEntities.HierarchySpatialTypeName.IfcBuilding :
        case 'IfcBuilding' :
            return 'viz-icon-small-building';
        case Vizelia.FOL.BusinessEntities.HierarchySpatialTypeName.IfcBuildingStorey :
        case 'IfcBuildingStorey' :
            return 'viz-icon-small-buildingstorey';
        case Vizelia.FOL.BusinessEntities.HierarchySpatialTypeName.IfcSpace :
        case 'IfcSpace' :
            return 'viz-icon-small-space';
        case Vizelia.FOL.BusinessEntities.HierarchySpatialTypeName.IfcFurniture :
        case 'IfcFurniture' :
            return 'viz-icon-small-furniture';
        default :
            return null;
    }
};

Viz.Configuration.StateCss = function(state) {
    switch (state) {
        case 'Initial' :
            return 'viz-icon-small-flag-purple';

        case 'Opened' :
            return 'viz-icon-small-flag-green';

        case 'Assigned' :
            return 'viz-icon-small-flag-blue';

        case 'Dispatched' :
            return 'viz-icon-small-flag-orange';

        case 'Rejected' :
            return 'viz-icon-small-flag-pink';

        case 'Completed' :
            return 'viz-icon-small-flag-finish';

        default :
            return 'viz-icon-small-flag-black';
    }
};

Viz.Configuration.ArithmeticOperatorSymbol = function(operator) {
    switch (operator) {
        case Vizelia.FOL.BusinessEntities.ArithmeticOperator.Add :
            return '+';
        case Vizelia.FOL.BusinessEntities.ArithmeticOperator.Subtract :
            return '-';
        case Vizelia.FOL.BusinessEntities.ArithmeticOperator.Multiply :
            return '*';
        case Vizelia.FOL.BusinessEntities.ArithmeticOperator.Divide :
            return '/';
        case Vizelia.FOL.BusinessEntities.ArithmeticOperator.Substract :
            return '-';
        case Vizelia.FOL.BusinessEntities.ArithmeticOperator.Power :
            return '^';
        default :
            return null;
    }
};

Viz.Configuration.AlgebricFunctionSymbol = function(funct) {
    switch (funct) {
        case Vizelia.FOL.BusinessEntities.AlgebricFunction.Cos :
            return 'cos';
        case Vizelia.FOL.BusinessEntities.AlgebricFunction.Sin :
            return 'sin';
        case Vizelia.FOL.BusinessEntities.AlgebricFunction.Tan :
            return 'tan';
        case Vizelia.FOL.BusinessEntities.AlgebricFunction.Sqrt :
            return '√';
        case Vizelia.FOL.BusinessEntities.AlgebricFunction.Abs :
            return 'abs';
        case Vizelia.FOL.BusinessEntities.AlgebricFunction.Log :
            return 'log';
        case Vizelia.FOL.BusinessEntities.AlgebricFunction.Inverse :
            return '1/';
        case Vizelia.FOL.BusinessEntities.AlgebricFunction.Count :
            return 'count';
        case Vizelia.FOL.BusinessEntities.AlgebricFunction.Delta :
            return 'delta';
        case Vizelia.FOL.BusinessEntities.AlgebricFunction.YTD :
            return 'ytd';
        case Vizelia.FOL.BusinessEntities.AlgebricFunction.Percentage :
            return 'ytd';
        default :
            return null;
    }
};

/**
 * The Viz.Configuration.Columns object contains columns for grids in the system. If one of your columns uses a renderer with a store (such as an enum renderer), you must put your column collection here. Only columns located here will have their stores loaded!!!
 */
Viz.Configuration.Columns.init = function() {
    /**
     * Configuration columns for grid Occupant.
     */
    // Viz.Configuration.Columns.OccupantOld = [
    // /*
    // * { dataIndex : 'KeyOccupant', header : 'Icon', renderer : function(data, cell, record, rowIndex, colIndex, store) { if (rowIndex % 2) cell.css = "viz-icon-small-site"; else cell.css = "viz-icon-small-building viz-test"; return data; } },
    // */
    // {
    // dataIndex : 'KeyOccupant',
    // header : 'Key',
    // sortable : true,
    // hidden : true
    // }, {
    // dataIndex : 'Id',
    // header : $lang('msg_occupant_id'),
    // sortable : true,
    // filterable : true,
    // width : 100
    // // editor : new Ext.form.NumberField()
    // }, {
    // dataIndex : 'FirstName',
    // header : $lang('msg_occupant_firstname'),
    // sortable : true,
    // filterable : true,
    // editor : Viz.Configuration.Editors.StandardTextField
    // }, {
    // dataIndex : 'LastName',
    // header : $lang('msg_occupant_lastname'),
    // sortable : true,
    // filterable : true,
    // editor : Viz.Configuration.Editors.StandardTextField
    // }, {
    // dataIndex : 'ElectronicMailAddresses',
    // header : $lang('msg_occupant_email'),
    // sortable : true,
    // filterable : true,
    // editor : Viz.Configuration.Editors.StandardTextField
    // }, {
    // dataIndex : 'JobTitle',
    // header : $lang('msg_occupant_jobtitle'),
    // sortable : true,
    // filterable : true,
    // editor : new Ext.form.TextArea()
    // }, {
    // dataIndex : 'TelephoneNumbers',
    // header : $lang('msg_occupant_telephone'),
    // sortable : true,
    // editor : Viz.Configuration.Editors.StandardTextField
    // }, {
    // dataIndex : 'PagerNumber',
    // header : $lang('msg_occupant_pager'),
    // sortable : true,
    // editor : Viz.Configuration.Editors.StandardTextField
    // }, {
    // dataIndex : 'KeyOrganization',
    // header : $lang('msg_occupant_organization'),
    // sortable : true,
    // // editor : Viz.Configuration.Editors.ComboBoxOrganization,
    // renderer : Ext.ux.comboBoxRenderer(new Ext.form.ComboBox({
    // pageSize : 50,
    // mode : 'remote',
    // triggerAction : 'all',
    // displayField : 'Name',
    // valueField : 'KeyOrganization',
    // listHeight : 300,
    // loadMask : true,
    // valueNotFoundText : '',
    // forceSelection : true,
    // typeAhead : true,
    // minChars : 1,
    // lazyRender : true,
    // store : new Viz.data.WCFJsonSimpleStore({
    // serviceHandler : Viz.Services.TestWCF.GetStoreOrganization,
    // sortInfo : {
    // field : 'Name',
    // direction : 'ASC'
    // },
    // baseParams : {
    // filter : 'Name'
    // }
    // })
    // }), "OrganizationName")
    // }, {
    // dataIndex : 'KeyOrganization',
    // header : $lang('msg_occupant_organization_id'),
    // hidden : true,
    // sortable : true,
    // // editor : Viz.Configuration.Editors.ComboBoxOrganizationId,
    // renderer : Ext.ux.comboBoxRenderer(new Ext.form.ComboBox({
    // pageSize : 50,
    // mode : 'remote',
    // triggerAction : 'all',
    // displayField : 'Id',
    // valueField : 'KeyOrganization',
    // valueNotFoundText : '',
    // listHeight : 300,
    // loadMask : true,
    // forceSelection : true,
    // query : 'all',
    // typeAhead : true,
    // minChars : 1,
    // lazyRender : true,
    // store : new Viz.data.WCFJsonSimpleStore({
    // serviceHandler : Viz.Services.TestWCF.GetStoreOrganization,
    // sortInfo : {
    // field : 'Id',
    // direction : 'ASC'
    // },
    // baseParams : {
    // filter : 'Id'
    // }
    // })
    // }), "OrganizationId")
    // }, {
    // dataIndex : 'OrganizationName',
    // header : $lang('msg_occupant_organization'),
    // hidden : true,
    // sortable : true,
    // editor : Viz.Configuration.Editors.StandardTextField
    // }, {
    // dataIndex : 'LocationLongPath',
    // header : $lang('msg_location_path'),
    // sortable : true,
    // renderer : Ext.ux.locationPathRenderer(),
    // filter : {
    // type : 'treecombomultiselect',
    // dataIndexTree : 'KeyLocation',
    // xtype : 'viztreecombomultiselectSpatial'
    // }
    // }, {
    // dataIndex : 'ClassificationItemWorktypeLongPath',
    // header : $lang('msg_occupant_worktype'),
    // sortable : true,
    // filter : {
    // type : 'treecombomultiselect',
    // dataIndexTree : 'KeyClassificationItemWorktype',
    // xtype : 'viztreecombomultiselectClassificationItemWorktype'
    // }
    // }, {
    // dataIndex : 'ClassificationItemOrganizationLongPath',
    // header : $lang('msg_occupant_organization'),
    // sortable : true,
    // filter : {
    // type : 'treecombomultiselect',
    // dataIndexTree : 'KeyClassificationItemOrganization',
    // xtype : 'viztreecombomultiselectClassificationItemOrganization'
    // }
    // }, {
    // dataIndex : 'OrganizationLongPath',
    // header : $lang('msg_occupant_organization'),
    // sortable : true,
    // filter : {
    // type : 'treecombomultiselect',
    // dataIndexTree : 'KeyOrganization',
    // xtype : 'viztreecombomultiselectOrganization'
    // }
    // }];
    Viz.Configuration.Columns.Occupant = [
            /*
             * { dataIndex : 'KeyOccupant', header : 'Icon', renderer : function(data, cell, record, rowIndex, colIndex, store) { if (rowIndex % 2) cell.css = "viz-icon-small-site"; else cell.css = "viz-icon-small-building viz-test"; return data; } },
             */
            {
        dataIndex  : 'Id',
        header     : $lang('msg_occupant_id'),
        sortable   : true,
        filterable : true,
        width      : 100
    }, {
        dataIndex  : 'FirstName',
        header     : $lang('msg_occupant_firstname'),
        sortable   : true,
        filterable : true
    }, {
        dataIndex  : 'LastName',
        header     : $lang('msg_occupant_lastname'),
        sortable   : true,
        filterable : true
    }, {
        dataIndex  : 'JobTitle',
        header     : $lang('msg_occupant_jobtitle'),
        sortable   : true,
        filterable : true
    }, {
        dataIndex  : 'TelephoneNumbers',
        header     : $img('viz-icon-small-phone') + $lang('msg_occupant_telephone'),
        headerText : $lang('msg_occupant_telephone'),
        sortable   : true,
        filterable : true
    }, {
        dataIndex  : 'PagerNumber',
        header     : $lang('msg_occupant_pager'),
        sortable   : true,
        hidden     : true,
        filterable : true
    }, {
        dataIndex  : 'OrganizationName',
        header     : $img('viz-icon-small-organization') + $lang('msg_occupant_organization'),
        headerText : $lang('msg_occupant_organization'),
        hidden     : true,
        sortable   : true,
        filterable : true
    }, {
        dataIndex  : 'LocationLongPath',
        header     : $img('viz-icon-small-site') + $lang('msg_location_path'),
        headerText : $lang('msg_location_path'),
        sortable   : true,
        renderer   : Ext.ux.locationPathRenderer(),
        filter     : {
            type          : 'treecombomultiselect',
            dataIndexTree : 'KeyLocationPath',
            queryType     : 'innerpath',
            xtype         : 'viztreecombomultiselectSpatial'

        }
    }, {
        dataIndex  : 'ClassificationItemWorktypeLongPath',
        header     : $img('viz-icon-small-worktype') + $lang('msg_occupant_worktype'),
        headerText : $lang('msg_occupant_worktype'),
        sortable   : true,
        filter     : {
            type          : 'treecombomultiselect',
            dataIndexTree : 'KeyClassificationItemWorktypePath',
            queryType     : 'innerpath',
            xtype         : 'viztreecombomultiselectClassificationItemWorktype'
        }
    }, {
        dataIndex : 'ClassificationItemOrganizationLongPath',
        header    : $lang('msg_occupant_organization'),
        sortable  : true,
        filter    : {
            type          : 'treecombomultiselect',
            dataIndexTree : 'KeyClassificationItemOrganizationPath',
            queryType     : 'innerpath',
            xtype         : 'viztreecombomultiselectClassificationItemOrganization'
        }
    }, {
        dataIndex  : 'OrganizationLongPath',
        header     : $img('viz-icon-small-organization') + $lang('msg_occupant_company'),
        headerText : $lang('msg_occupant_company'),
        sortable   : true,
        filter     : {
            type          : 'treecombomultiselect',
            dataIndexTree : 'KeyOrganizationPath',
            queryType     : 'innerpath',
            xtype         : 'viztreecombomultiselectOrganization'
        }
    }];
    /**
     * Configuration columns for AzManFilterSpatial grid.
     */
    Viz.Configuration.Columns.FilterSpatial = [{
                dataIndex  : 'Name',
                header     : $lang('msg_name'),
                width      : 30,
                sortable   : true,
                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                    cell.css = 'viz-icon-cell ' + record.get('IconCls');
                    return data;
                },
                filterable : true
            }, {
                dataIndex : 'LongPath',
                id        : 'LongPath',
                header    : $lang('msg_location_path'),
                sortable  : true,
                renderer  : Ext.ux.locationPathRenderer("TypeName")
            }];
    /**
     * Configuration columns for AzManFilterSpatial grid.
     */
    Viz.Configuration.Columns.FilterClassificationItem = [{
                dataIndex  : 'Name',
                header     : $lang('msg_name'),
                sortable   : true,
                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                    var iconCls = record.get('IconCls') || 'viz-icon-small-icon-classificationitem';
                    cell.css = 'viz-icon-cell ' + iconCls;
                    return data;
                },
                filterable : true
            }];
    /**
     * Configuration columns for ApplicationGroup Authorization grid.
     */
    Viz.Configuration.Columns.ApplicationGroupAuthorization = [{
                dataIndex  : 'UserName',
                header     : $lang('msg_login'),
                sortable   : true,
                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                    cell.css = 'viz-icon-cell ' + 'viz-icon-small-user';
                    return data;
                },
                filterable : true
            }, {
                dataIndex  : 'Email',
                header     : $lang('msg_occupant_email'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'OccupantFullName',
                header     : $lang('msg_occupant_fullname'),
                sortable   : true,
                filterable : true
            }];
    /**
     * Configuration columns for ApplicationGroup Roles grid.
     */
    Viz.Configuration.Columns.AzManItem = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                    cell.css = 'viz-icon-cell ' + record.get('IconCls');
                    return data;
                }
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }];
    /**
     * Configuration columns for ApplicationGroup Roles grid.
     */
    Viz.Configuration.Columns.AzManFilter = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                    cell.css = 'viz-icon-cell ' + record.get('IconCls');
                    return data;
                }
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_azmanfilter_path'),
                dataIndex  : 'LongPath',
                sortable   : false,
                filterable : true
            }, {
                header: $lang('msg_username_list'),
                dataIndex: 'UserNames',
                sortable: false,
                filterable: true
            }];
    /**
     * Configuration columns for Localization grid.
     */
    Viz.Configuration.Columns.Localization = [{
                dataIndex  : 'Key',
                header     : $lang('msg_localization_key'),
                sortable   : true,
                filterable : true,
                editor     : {
                    xtype      : 'textfield',
                    allowBlank : false,
                    vtype      : 'localization'
                }
            }, {
                dataIndex  : 'Value',
                header     : $lang('msg_localization_value'),
                sortable   : true,
                filterable : true,
                editor     : {
                    xtype      : 'textfield',
                    allowBlank : false
                }
            }];
    /**
     * Configuration columns for User grid.
     */
    Viz.Configuration.Columns.User = [{
                header     : $lang('msg_login'),
                dataIndex  : 'UserName',
                filterable : true,
                sortable   : true,
                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                    cell.css = 'viz-icon-cell ' + record.get('iconCls');
                    // cell.attr = 'ext:qtip="Heloo"';
                    return data;
                }
            }, {
                header     : $lang('msg_user_firstname'),
                dataIndex  : 'FirstName',
                filterable : true,
                sortable   : true
            }, {
                header     : $lang('msg_user_lastname'),
                dataIndex  : 'LastName',
                filterable : true,
                sortable   : true
            }, {
                header     : $lang('msg_occupant_email'),
                dataIndex  : 'Email',
                sortable   : true,
                filterable : true
            }, {
                header    : $lang("msg_user_lastlogindate"),
                dataIndex : 'LastLoginDate',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(false),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 90,
                sortable  : true
            }, {
                header     : $lang('msg_user_isapproved'),
                dataIndex  : 'IsApproved',
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                // trueText : '<span class="xcheckbox-on" style="background-position:50% -2px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>',
                // falseText : '<span class="xcheckbox-off" style="background-position:50% -2px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>',
                width      : 70,
                align      : "center",
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_user_islocked'),
                dataIndex  : 'IsLockedOut',
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 70,
                align      : "center",
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_user_time_zone'),
                dataIndex  : 'Preferences.TimeZoneId',
                sortable   : true,
                filterable : true,
                renderer   : function(data) {
                    var tzl = data;
                    Ext.each(Viz.Configuration.TimeZones, function(element) {
                                if (element.data['Id'] == data) {
                                    tzl = element.data['Label'];
                                    return false;
                                }
                            });
                    return tzl;
                }
            }, {
                header     : $lang('msg_user_applicationgroups'),
                dataIndex  : 'ProviderUserKey.ApplicationGroups',
                sortable   : false,
                filterable : false
            }, {
                dataIndex  : 'ProviderUserKey.UserLocationLongPath',
                header     : $img('viz-icon-small-site') + $lang('msg_location_path'),
                headerText : $lang('msg_location_path'),
                sortable   : true,
                renderer   : Ext.ux.locationPathRenderer(),
                filter     : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'ProviderUserKey.UserKeyLocationPath',
                    queryType     : 'innerpath',
                    xtype         : 'viztreecombomultiselectSpatial'
                }
            }, {
                header     : $lang('msg_user_projectscount'),
                dataIndex  : 'ProviderUserKey.ProjectsCount',
                xtype      : 'numbercolumn',
                sortable   : true,
                filterable : true,
                format     : Viz.util.Chart.getNumberFormat(),
                align      : 'right'
            }];
    /**
     * Configuration columns for PsetList grid.
     */
    Viz.Configuration.Columns.PsetList = [{
                dataIndex : 'MsgCode',
                header    : $lang('msg_name'),
                sortable  : true,
                renderer  : function(data, cell, record, rowIndex, colIndex, store) {
                    cell.css = 'viz-icon-cell ' + 'viz-icon-small-list';
                    return data;
                }
            }];
    Viz.Configuration.Columns.PsetListElement = [{
                header    : $lang('msg_localization_key'),
                dataIndex : 'MsgCode',
                sortable  : true,
                editor    : {
                    xtype      : 'vizComboLocalization',
                    allowBlank : false
                }
            }, {
                header    : $lang('msg_value'),
                dataIndex : 'Value',
                sortable  : true,
                editor    : {
                    xtype      : 'textfield',
                    allowBlank : false
                }
            }];
    Viz.Configuration.Columns.ClassificationDefinition = [{
                header     : $lang('msg_classificationdefinition_category'),
                dataIndex  : 'Category',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_classificationdefinition_localid'),
                dataIndex  : 'LocalId',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_classificationdefinition_iconcls'),
                dataIndex  : 'IconCls',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                    cell.css = 'viz-icon-cell ' + data;
                    return data;
                }
            }, {
                header     : $lang('msg_classificationdefinition_iconrootonly'),
                dataIndex  : 'IconRootOnly',
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 70,
                align      : "center",
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.ClassificationItem = [{
                header     : $lang('msg_classificationitem_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true,
                hidden     : true
            }, {
                header     : $lang('msg_classificationditem_localid'),
                dataIndex  : 'LocalId',
                sortable   : true,
                filterable : true,
                hidden     : true
            }, {
                header     : $lang('msg_classificationitem_longpath'),
                renderer   : Ext.ux.colorRenderer('', 'LongPath'),
                dataIndex  : 'LongPath',
                sortable   : true,
                filterable : true,
                width      : 200
            }];
    /**
     * Configuration columns for ActionRequest grid.
     */
    Viz.Configuration.Columns.ActionRequest = [{
                dataIndex  : 'RequestID',
                header     : $lang('msg_actionrequest_id'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'RequestorLastName',
                header     : $img('viz-icon-small-occupant') + $lang('msg_actionrequest_requestor'),
                headerText : $lang('msg_actionrequest_requestor'),
                sortable   : true,
                renderer   : function(data, cell, record) {
                    return record.get('RequestorFirstName') + ' ' + record.get('RequestorLastName');

                },
                filter     : {
                    type             : 'generic',
                    dataIndexGeneric : 'Requestor',
                    width            : 300,
                    xtype            : 'vizComboOccupant'
                }
            }, {
                dataIndex  : 'ApproverLastName',
                header     : $img('viz-icon-small-user') + $lang('msg_approval_approver'),
                headerText : $lang('msg_approval_approver'),
                renderer   : function(data, cell, record) {
                    return record.get('ApproverFirstName') + ' ' + record.get('ApproverLastName');
                },
                sortable   : true,
                filter     : {
                    type             : 'compositeString',
                    dataIndexGeneric : "ApproverFirstName + ' ' + ApproverLastName"
                }
            }, {
                dataIndex  : 'ClassificationLongPath',
                header     : $img('viz-icon-small-work') + $lang('msg_actionrequest_classificationlongpath'),
                headerText : $lang('msg_actionrequest_classificationlongpath'),
                sortable   : false,
                filter     : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'KeyClassificationItemPath',
                    queryType     : 'innerpath',
                    xtype         : 'viztreecombomultiselectClassificationItemWork',
                    listeners     : {
                        serialize : function(args, filter) {
                            // we always concatenate with the parentnode to allow search on cti request level.
                            if (Ext.isEmpty(filter.inputItem.treePanel)) {
                                args[0].value = filter.inputItem.value;
                                return;
                            }
                            var tree = filter.inputItem.getTree();
                            var multiSelect = filter.inputItem;
                            var values = multiSelect.getValueAsArray();
                            var nodes = tree.getChecked();
                            var newValues = [];
                            Ext.each(values, function(v) {
                                        var node = tree.getRootNode().findChild("Key", v, true);
                                        var parentNode = node.parentNode;
                                        if (parentNode != null && parentNode.attributes && !Ext.isEmpty(parentNode.attributes.Key)) {
                                            newValues.push(parentNode.attributes.Key + '  / ' + node.attributes.Key);
                                        }
                                        else
                                            newValues.push(node.attributes.Key);
                                    });

                            if (multiSelect.formatAsArray)
                                args[0].value = newValues;
                            else
                                args[0].value = newValues.join(multiSelect.valueDelimiter);
                        }

                    }
                }
            }, {
                dataIndex  : 'PriorityMsgCode',
                header     : $img('viz-icon-small-priority') + $lang('msg_priority'),
                headerText : $lang('msg_priority'),
                sortable   : true,
                filterable : true,
                renderer   : function(value) {
                    return $lang(value);
                },
                filter     : {
                    type       : 'generic',
                    valueField : 'MsgCode',
                    xtype      : 'vizComboPriority'
                }
            }, {
                dataIndex  : 'StateLabel',
                header     : $lang('msg_status'),
                renderer   : Ext.ux.workflowStateRenderer(),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'Description',
                header     : $lang('msg_comment'),
                sortable   : true,
                filterable : true
            }, {
                header    : $lang("msg_actionrequest_actualstart"),
                dataIndex : 'ActualStart',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 94,
                sortable  : true
            }, {
                header    : $lang("msg_actionrequest_schedulestart"),
                dataIndex : 'ScheduleStart',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 94,
                sortable  : true
            }, {
                header    : $lang("msg_actionrequest_closedatetime"),
                dataIndex : 'CloseDateTime',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 94,
                sortable  : true
            }, {
                dataIndex : 'LocationLongPath',
                header    : $lang('msg_location_path'),
                sortable  : true,
                renderer  : Ext.ux.locationPathRenderer(),
                filter    : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'KeyLocationPath',
                    queryType     : 'innerpath',
                    xtype         : 'viztreecombomultiselectSpatial'
                }
            }, {
                dataIndex : 'AuditTimeLastModified',
                header    : $lang("msg_audit_timelastmodified"),
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 94,
                sortable  : true
            }, {
                dataIndex  : 'AuditModifiedByLastName',
                header     : $img('viz-icon-small-user') + $lang('msg_audit_modifiedby'),
                headerText : $lang('msg_audit_modifiedby'),
                renderer   : function(data, cell, record) {
                    if (!Ext.isEmpty(record.get('AuditModifiedByLogin')))
                        return record.get('AuditModifiedByFirstName') + ' ' + record.get('AuditModifiedByLastName') + ' - ' + record.get('AuditModifiedByLogin');
                    else
                        return null;
                },
                sortable   : true,
                filter     : {
                    type             : 'compositeString',
                    dataIndexGeneric : "AuditModifiedByFirstName + ' ' + AuditModifiedByLastName + ' ' + AuditModifiedByLogin"
                }
            }];

    /**
     * Configuration columns for Task grid.
     */
    Viz.Configuration.Columns.Task = [{
                dataIndex  : 'Priority',
                header     : $lang('msg_task_priority'),
                sortable   : true,
                filterable : true,
                width      : 40
            }, {
                dataIndex  : 'ActorLastName',
                header     : $img('viz-icon-small-user') + $lang('msg_task_assigned_to'),
                headerText : $lang('msg_task_assigned_to'),
                renderer   : function(data, cell, record) {
                    return record.get('ActorFirstName') + ' ' + record.get('ActorLastName');
                },
                sortable   : true,
                filter     : {
                    type             : 'compositeString',
                    dataIndexGeneric : "ActorFirstName + ' ' + ActorLastName"
                },
                width      : 200
            }, {
                dataIndex  : 'Name',
                header     : $lang('msg_name'),
                sortable   : true,
                filterable : true,
                width      : 200
            }, {
                dataIndex  : 'StateLabel',
                header     : $lang('msg_status'),
                renderer   : Ext.ux.workflowStateRenderer(),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'Description',
                header     : $lang('msg_comment'),
                sortable   : true,
                filterable : true
            }, {
                header    : $lang("msg_task_schedulestart"),
                dataIndex : 'ScheduleStart',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 90,
                sortable  : true
            }, {
                header    : $lang("msg_task_startdate"),
                dataIndex : 'ActualStart',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 90,
                sortable  : true
            }, {
                header    : $lang("msg_task_enddate"),
                dataIndex : 'ActualFinish',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 90,
                sortable  : true
            }, {
                dataIndex  : 'Duration',
                header     : $lang('msg_task_duration'),
                sortable   : true,
                filterable : true,
                align      : 'right',
                renderer   : function(data, cell, record) {
                    if (data != null) {
                        var durationHours = Math.floor(data / 60);
                        var durationMinutes = data - durationHours * 60;
                        return String.leftPad(durationHours, 2, "0") + ":" + String.leftPad(durationMinutes, 2, "0");
                    }
                    else
                        return null;
                },
                width      : 60
            }];
    /**
     * Configuration columns for Approval grid.
     */
    Viz.Configuration.Columns.Approval = [{
                dataIndex  : 'Status',
                header     : $lang('msg_status'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'Description',
                header     : $lang('msg_description'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'ApproverLastName',
                header     : $lang('msg_approval_approver'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex : 'ApprovalDateTime',
                header    : $lang('msg_approval_approvaldatetime'),
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                sortable  : true,
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                }
            }];
    Viz.Configuration.Columns.CalendarEvent = [{
                header     : $lang('msg_calendarevent_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_calendarevent_startdate'),
                dataIndex  : 'StartDate',
                xtype      : 'datecolumn',
                align      : 'right',
                format     : Viz.getLocalizedDateTimeFormat(false),
                filterable : true,
                width      : 90,
                sortable   : true,
                filter     : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                }
            }, {
                header     : $lang('msg_calendarevent_enddate'),
                dataIndex  : 'EndDate',
                xtype      : 'datecolumn',
                align      : 'right',
                format     : Viz.getLocalizedDateTimeFormat(false),
                filterable : true,
                width      : 90,
                sortable   : true,
                filter     : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                }
            }, {
                header     : $lang('msg_calendarevent_category'),
                dataIndex  : 'CategoryEntity.Label',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_calendarevent_isallday'),
                dataIndex  : 'IsAllDay',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 70,
                align      : "center"
            }, {
                header    : $lang('msg_chart_timezone'),
                dataIndex : 'TimeZoneId',
                sortable  : true,
                renderer  : function(data, cell, record) {
                    /*
                     * var tzl = Viz.Configuration.TimeZones.filter(function(element, i, array) { return element.data['Id'] == data; }, this)
                     */
                    var tzl = null;
                    Ext.each(Viz.Configuration.TimeZones, function(element) {
                                if (element.data['Id'] == data) {
                                    tzl = element;
                                    return false;
                                }
                            });

                    if (tzl)
                        return tzl.data['Label'];
                },
                width     : 200
            }];
    Viz.Configuration.Columns.Priority = [{
                header    : $lang('msg_key'),
                dataIndex : 'KeyPriority',
                sortable  : true
            }, {
                header    : $lang('msg_localid'),
                dataIndex : 'LocalId',
                sortable  : true
            }, {
                header    : $lang('msg_value'),
                dataIndex : 'Value',
                align     : 'right',
                sortable  : true
            }, {
                header    : $lang('msg_localization_value'),
                dataIndex : 'MsgCode',
                sortable  : true
            }];

    Viz.Configuration.Columns.Palette = [{
                header    : $lang('msg_label'),
                dataIndex : 'Label',
                sortable  : true,
                width     : 100
            }, {
                header    : $lang('msg_localization_value'),
                dataIndex : 'MsgCode',
                sortable  : true,
                hidden    : true,
                width     : 100
            }, {
                dataIndex  : 'KeyPalette',
                header     : $lang('msg_palette_preview'),
                sortable   : false,
                filterable : false,
                renderer   : function(value, cell) {
                    var url = Viz.Services.BuildUrlStream('Energy.svc', 'Palette_GetStreamImage', {
                                Key    : value,
                                height : 12
                            })
                    return '<img src="' + url + '">';
                }
            }];

    Viz.Configuration.Columns.PaletteColor = [{
                header     : $lang('msg_palettecolor_order'),
                dataIndex  : 'Order',
                sortable   : true,
                filterable : true,
                width      : 50
            }, {
                header    : $lang("msg_color"),
                dataIndex : 'Color',
                renderer  : Ext.ux.colorRenderer()
            }];

    Viz.Configuration.Columns.Playlist = [{
                header    : $lang('msg_key'),
                dataIndex : 'KeyPlaylist',
                sortable  : true,
                hidden    : true
            }, {
                header    : $lang('msg_label'),
                dataIndex : 'Name',
                sortable  : true
            }, {
                header     : $lang('msg_playlist_width'),
                dataIndex  : 'Width',
                xtype      : 'numbercolumn',
                sortable   : true,
                align      : 'right',
                filterable : true
            }, {
                header     : $lang('msg_playlist_height'),
                dataIndex  : 'Height',
                xtype      : 'numbercolumn',
                sortable   : true,
                align      : 'right',
                filterable : true
            }, {
                header     : $lang('msg_playlistpage_duration'),
                dataIndex  : 'TotalDuration',
                xtype      : 'numbercolumn',
                sortable   : false,
                width      : 30,
                align      : 'right',
                filterable : false
            }, {
                header     : $lang("msg_virtualfile"),
                dataIndex  : 'VirtualFilePath',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return '<a href="direct/' + record.data.Application + '/' + data + '" target="_blank">' + data + '</a>';
                }
            }];

    Viz.Configuration.Columns.PlaylistPage = [{
                header     : $lang('msg_playlistpage_order'),
                dataIndex  : 'Order',
                sortable   : true,
                filterable : true,
                width      : 50
            }, {
                header     : $lang('msg_portalwindow'),
                dataIndex  : 'PortalWindowTitle',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_portaltab'),
                dataIndex  : 'PortalTabTitle',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_playlistpage_duration'),
                dataIndex  : 'Duration',
                xtype      : 'numbercolumn',
                sortable   : true,
                width      : 60,
                align      : 'right',
                filterable : true
            }];

    Viz.Configuration.Columns.CalendarEventCategory = [{
                header     : $lang('msg_key'),
                dataIndex  : 'KeyCalendarEventCategory',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_localid'),
                dataIndex  : 'LocalId',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_localization_value'),
                dataIndex  : 'MsgCode',
                sortable   : true,
                filterable : true
            }, {
                header    : $lang('msg_label'),
                dataIndex : 'Label'
            }, {
                header    : $lang("msg_color"),
                dataIndex : 'Color',
                renderer  : Ext.ux.colorRenderer()
            }];

    Viz.Configuration.Columns.Meter = [{
                header     : $lang('msg_localid'),
                dataIndex  : 'LocalId',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    cell.css = 'viz-icon-cell ' + record.get('IconCls');
                    return data;
                }
            }, {
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true,
                hidden     : true
            }, {
                dataIndex : 'ClassificationItemLongPath',
                id        : 'ClassificationItemLongPath',
                header    : $lang('msg_meter_classification'),
                sortable  : true,
                renderer  : Ext.ux.colorRenderer('ClassificationItem', 'ClassificationItemLongPath'),
                filter    : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'KeyClassificationItemPath',
                    queryType     : 'innerpath',
                    xtype         : 'viztreecombomultiselectClassificationItemMeter'
                }
            }, {
                header     : $lang('msg_meter_groupoperation'),
                dataIndex  : 'GroupOperation',
                sortable   : true,
                filterable : true,
                width      : 40,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.MathematicOperator,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'GroupOperation',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.MathematicOperator,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang('msg_meter_expected_interval'),
                dataIndex  : 'FrequencyInput',
                xtype      : 'numbercolumn',
                sortable   : true,
                width      : 30,
                align      : 'right',
                hidden     : true,
                filterable : true
            }, {
                header     : $lang('msg_meter_expected_interval_time_unit'),
                dataIndex  : 'ManualInputTimeInterval',
                sortable   : true,
                filterable : true,
                width      : 50,
                hidden     : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.ManualInputTimeInterval,Vizelia.FOL.Common'
                    }
                })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'GroupOperation',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.ManualInputTimeInterval,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang('msg_meter_factor'),
                dataIndex  : 'Factor',
                xtype      : 'numbercolumn',
                sortable   : true,
                width      : 30,
                align      : 'right',
                filterable : true
            }, {
                header     : $lang('msg_meter_unitinput'),
                dataIndex  : 'UnitInput',
                width      : 20,
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_meter_unitoutput'),
                dataIndex  : 'UnitOutput',
                width      : 20,
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_meter_isaccumulated'),
                dataIndex  : 'IsAccumulated',
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 20,
                align      : "center",
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_meter_isacquisitiondatetimeendinterval'),
                dataIndex  : 'IsAcquisitionDateTimeEndInterval',
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 20,
                align      : "center",
                sortable   : true,
                filterable : true
            }, {
                dataIndex : 'LocationLongPath',
                id        : 'LocationLongPath',
                header    : $lang('msg_location_path'),
                sortable  : true,
                renderer  : Ext.ux.locationPathRenderer(),
                filter    : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'KeyLocationPath',
                    queryType     : 'innerpath',
                    xtype         : 'viztreecombomultiselectSpatial'
                }
            }, {
                header     : $lang('msg_meter_isvirtual'),
                dataIndex  : 'IsVirtual',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 70,
                align      : "center"
            }, {
                header     : $lang('msg_meter_metervalidationrulealarminstances'),
                dataIndex  : 'AlarmInstanceFromMeterValidationRuleCount',
                sortable   : true,
                width      : 30,
                filterable : true,
                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                    if (data == 0) {
                        cell.css = 'viz-icon-cell viz-icon-small-success';
                        return ''
                    }
                    else {
                        cell.css = 'viz-icon-cell viz-icon-small-exception';
                        return Ext.util.Format.number(data, Viz.util.Chart.getNumberFormat());
                    }
                }
            }, {
                header     : $lang('msg_meterdata'),
                dataIndex  : 'MeterDataCount',
                xtype      : 'numbercolumn',
                format     : Viz.util.Chart.getNumberFormat(),
                sortable   : true,
                width      : 30,
                align      : 'right',
                filterable : true
            }, {
                header    : $lang("msg_meter_acquisitiondatetimemin"),
                dataIndex : 'AcquisitionDateTimeMin',
                // xtype : 'datecolumn',
                align     : 'right',
                // hidden : true,
                // format : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 120,
                sortable  : true,
                renderer  : function(value, cell, model, rowIndex, colIndex, store) {
                    if (value == null)
                        return null;

                    var data = new Date();
                    data.setTime(value);
                    return Ext.util.Format.date(data, Viz.getLocalizedMeterDataAcquisitionDateTimeFormat(model.data.ManualInputTimeInterval));
                }
            }, {
                header    : $lang("msg_meter_acquisitiondatetimemax"),
                dataIndex : 'AcquisitionDateTimeMax',
                // xtype : 'datecolumn',
                align     : 'right',
                // hidden : true,
                // format : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 120,
                sortable  : true,
                renderer  : function(value, cell, model, rowIndex, colIndex, store) {
                    if (value == null)
                        return null;

                    var data = new Date();
                    data.setTime(value);
                    return Ext.util.Format.date(data, Viz.getLocalizedMeterDataAcquisitionDateTimeFormat(model.data.ManualInputTimeInterval));
                }
            }, {
                header     : $lang('msg_lastrawmetervalue'),
                dataIndex  : 'LastRawValue',
                xtype      : 'numbercolumn',
                format     : Viz.util.Chart.getNumberFormat({
                            DisplayDecimalPrecision : 4
                        }), // '0,000.0000',
                sortable   : true,
                align      : 'right',
                filterable : true,
                hidden     : true,
                width      : 100,
                filter     : {
                    type      : 'float',
                    dataIndex : 'Value',
                    fieldCls  : Ext.extend(Ext.form.NumberField, {
                                decimalPrecision : 4
                            })
                }
            }, {
                header     : $lang('msg_lastmetervalidity'),
                dataIndex  : 'LastValidity',
                hidden     : true,
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.MeterDataValidity,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'LastValidity',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.MeterDataValidity,Vizelia.FOL.Common'
                }
            }];

    Viz.Configuration.Columns.MeterDataExportTaskMeter = [{
                header     : $lang('msg_localid'),
                dataIndex  : 'LocalId',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }, {
                dataIndex : 'ClassificationItemLongPath',
                header    : $lang('msg_meter_classification'),
                renderer  : Ext.ux.colorRenderer('ClassificationItem', 'ClassificationItemLongPath'),
                sortable  : true,
                filter    : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'KeyClassificationItem',
                    xtype         : 'viztreecombomultiselectClassificationItemMeter'
                }
            }, {
                header     : $lang('msg_meter_unitoutput'),
                dataIndex  : 'UnitOutput',
                sortable   : true,
                filterable : true
            }, {
                dataIndex : 'LocationLongPath',
                header    : $lang('msg_location_path'),
                sortable  : true,
                renderer  : Ext.ux.locationPathRenderer(),
                filter    : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'KeyLocation',
                    xtype         : 'viztreecombomultiselectSpatial'
                }
            }, {
                header     : $lang('msg_meter_metervalidationrulealarminstances'),
                dataIndex  : 'AlarmInstanceFromMeterValidationRuleCount',
                sortable   : true,
                width      : 30,
                filterable : true,
                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                    if (data == 0) {
                        cell.css = 'viz-icon-cell viz-icon-small-success';
                        return ''
                    }
                    else {
                        cell.css = 'viz-icon-cell viz-icon-small-exception';
                        return Ext.util.Format.number(data, Viz.util.Chart.getNumberFormat());
                    }
                }
            }, {
                header     : $lang('msg_meterdata'),
                dataIndex  : 'MeterDataCount',
                xtype      : 'numbercolumn',
                format     : Viz.util.Chart.getNumberFormat(),
                sortable   : true,
                width      : 30,
                align      : 'right',
                filterable : true
            }];

    Viz.Configuration.Columns.ChartMeter = [{
                header     : $lang('msg_localid'),
                dataIndex  : 'LocalId',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    cell.css = 'viz-icon-cell ' + record.get('IconCls');
                    return data;
                }
            }, {
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }, {
                dataIndex : 'ClassificationItemLongPath',
                header    : $lang('msg_meter_classification'),
                renderer  : Ext.ux.colorRenderer('ClassificationItem', 'ClassificationItemLongPath'),
                sortable  : true,
                filter    : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'KeyClassificationItem',
                    xtype         : 'viztreecombomultiselectClassificationItemMeter'
                }
            }, {
                header     : $lang('msg_meter_unitoutput'),
                dataIndex  : 'UnitOutput',
                sortable   : true,
                filterable : true
            }, {
                dataIndex : 'LocationLongPath',
                header    : $lang('msg_location_path'),
                sortable  : true,
                renderer  : Ext.ux.locationPathRenderer(),
                filter    : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'KeyLocation',
                    xtype         : 'viztreecombomultiselectSpatial'
                }
            }, {
                header     : $lang('msg_meter_metervalidationrulealarminstances'),
                dataIndex  : 'AlarmInstanceFromMeterValidationRuleCount',
                sortable   : true,
                width      : 30,
                filterable : true,
                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                    if (data == 0) {
                        cell.css = 'viz-icon-cell viz-icon-small-success';
                        return ''
                    }
                    else {
                        cell.css = 'viz-icon-cell viz-icon-small-exception';
                        return Ext.util.Format.number(data, Viz.util.Chart.getNumberFormat());
                    }
                }
            }, {
                header     : $lang('msg_meterdata'),
                dataIndex  : 'MeterDataCount',
                xtype      : 'numbercolumn',
                format     : Viz.util.Chart.getNumberFormat(),
                sortable   : true,
                width      : 30,
                align      : 'right',
                filterable : true
            }];

    Viz.Configuration.Columns.MeterOperation = [{
                header     : $lang('msg_meteroperation_order'),
                dataIndex  : 'Order',
                sortable   : true,
                filterable : true,
                width      : 50
            }, /*
                 * { header : $lang('msg_localid'), dataIndex : 'LocalId', sortable : true, filterable : true, width : 150, hidden : true }, { header : $lang('msg_meteroperation_factor1'), dataIndex : 'Factor1', sortable : true, align : 'right', filterable : false, width : 80, hidden : true, renderer : function(data, cell, record) { var key = record.get("KeyMeter1"); var keyOp = record.get("KeyMeterOperation1"); if (Ext.isEmpty(key) && Ext.isEmpty(keyOp)) return ''; return Ext.util.Format.number(data, Ext.grid.NumberColumn.prototype.format); } }, { header : $lang('msg_meteroperation_function1'), dataIndex : 'Function1', sortable : true, filterable : false, width : 80, hidden : true, renderer : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({ autoLoad : true, serviceParams : { enumTypeName :
                 * 'Vizelia.FOL.BusinessEntities.AlgebricFunction,Vizelia.FOL.Common' } }), ['None']) }, { header : $lang('msg_meteroperation_meter1'), dataIndex : 'NameMeter1', sortable : true, filterable : false, width : 80, hidden : true, renderer : function(data, cell, record) { var key = record.get("KeyMeter1"); if (Ext.isEmpty(key)) return record.get("LocalIdMeterOperation1") else return record.get("NameMeter1"); } }, { header : $lang('msg_meteroperation_operator'), dataIndex : 'Operator', sortable : true, filterable : false, width : 80, hidden : true, renderer : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({ autoLoad : true, serviceParams : { enumTypeName : 'Vizelia.FOL.BusinessEntities.ArithmeticOperator,Vizelia.FOL.Common' } }), ['None']) }, { header :
                 * $lang('msg_meteroperation_function2'), dataIndex : 'Function2', sortable : true, filterable : false, width : 80, hidden : true, renderer : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({ autoLoad : true, serviceParams : { enumTypeName : 'Vizelia.FOL.BusinessEntities.AlgebricFunction,Vizelia.FOL.Common' } }), ['None']) }, { header : $lang('msg_meteroperation_factor2'), dataIndex : 'Factor2', sortable : true, align : 'right', filterable : false, width : 80, hidden : true, renderer : function(data, cell, record) { var key = record.get("KeyMeter2"); var keyOp = record.get("KeyMeterOperation2"); if (Ext.isEmpty(key) && Ext.isEmpty(keyOp)) return ''; return Ext.util.Format.number(data, Ext.grid.NumberColumn.prototype.format); } }, { header :
                 * $lang('msg_meteroperation_meter2'), dataIndex : 'NameMeter2', sortable : true, filterable : true, width : 80, hidden : true, renderer : function(data, cell, record) { var key = record.get("KeyMeter2"); if (Ext.isEmpty(key)) return record.get("LocalIdMeterOperation2") else return record.get("NameMeter2"); } },
                 */{
                header   : $lang('msg_meteroperation_formula'),
                id       : 'Formula',
                width    : 300,
                renderer : function(data, cell, record) {

                    var retVal = record.get("LocalId") + ' = (';
                    if (record.get("Factor1") != 1) {
                        retVal += Ext.isInt(record.get("Factor1")) ? record.get("Factor1").toString() : Ext.util.Format.number(record.get("Factor1"), Ext.grid.NumberColumn.prototype.format + '00000');
                        retVal += ' * ';
                    }
                    var func = Viz.Configuration.AlgebricFunctionSymbol(record.get("Function1"));
                    if (func)
                        retVal += func + '(';

                    var key = record.get("KeyMeter1");
                    if (Ext.isEmpty(key))
                        retVal += record.get("LocalIdMeterOperation1");
                    else
                        retVal += record.get("NameMeter1");

                    if (func)
                        retVal += ')';

                    var offset1 = record.get("Offset1");
                    if (Ext.isNumber(offset1)) {
                        if (offset1 >= 0)
                            retVal += ' +';
                        retVal += Ext.isInt(offset1) ? offset1.toString() : Ext.util.Format.number(offset1, Ext.grid.NumberColumn.prototype.format + '00000');
                    }
                    retVal += ')';

                    var op = Viz.Configuration.ArithmeticOperatorSymbol(record.get("Operator"));
                    if (op) {
                        retVal += ' ' + op + ' (';

                        key = record.get("KeyMeter2");
                        var meter2 = "";
                        if (Ext.isEmpty(key))
                            meter2 = record.get("LocalIdMeterOperation2");
                        else
                            meter2 = record.get("NameMeter2");
                        if (!Ext.isEmpty(meter2)) {
                            if (record.get("Factor2") != 1) {
                                retVal += Ext.isInt(record.get("Factor2")) ? record.get("Factor2").toString() : Ext.util.Format.number(record.get("Factor2"), Ext.grid.NumberColumn.prototype.format + '00000');
                                retVal += ' * ';
                            }
                            func = Viz.Configuration.AlgebricFunctionSymbol(record.get("Function2"));
                            if (func)
                                retVal += func + '(';
                            retVal += meter2;
                            if (func)
                                retVal += ')';

                            var offset2 = record.get("Offset2");
                            if (Ext.isNumber(offset2)) {
                                if (offset2 >= 0)
                                    retVal += ' +';
                                retVal += Ext.isInt(offset2) ? offset2.toString() : Ext.util.Format.number(offset2, Ext.grid.NumberColumn.prototype.format + '00000');
                            }

                        }
                        retVal += ')';
                    }
                    return retVal;
                }
            }];

    Viz.Configuration.Columns.MeterData = [{
                header    : $lang("msg_meterdata_acquisitiondatetime"),
                dataIndex : 'AcquisitionDateTime',
                // xtype : 'datecolumn',
                align     : 'left',
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 120,
                sortable  : true,
                renderer  : function(value, cell, model, rowIndex, colIndex, store) {
                    if (value == null)
                        return null;

                    var data = new Date();
                    data.setTime(value);
                    return Ext.util.Format.date(data, Viz.getLocalizedMeterDataAcquisitionDateTimeFormat(store.ManualInputTimeInterval));
                }
            }, {
                header     : $lang('msg_meterdata_acquisitiondatetimelocal'),
                dataIndex  : 'AcquisitionDateTimeLocal',
                sortable   : false,
                align      : 'left',
                width      : 120,
                filterable : false
            }, {
                header     : $lang('msg_chart_timezone'),
                dataIndex  : 'TimeZoneLocal',
                sortable   : false,
                filterable : false,
                hidden     : true,
                width      : 100
            }, {
                header     : $lang('msg_meterdata_value'),
                dataIndex  : 'Value',
                xtype      : 'numbercolumn',
                format     : Viz.util.Chart.getNumberFormat({
                            DisplayDecimalPrecision : 4
                        }), // '0,000.0000',
                sortable   : true,
                align      : 'left',
                filterable : true,
                width      : 100,
                filter     : {
                    type      : 'float',
                    dataIndex : 'Value',
                    fieldCls  : Ext.extend(Ext.form.NumberField, {
                                decimalPrecision : 4
                            })
                }
            }, {
                header     : $lang('msg_meterdata_validity'),
                dataIndex  : 'Validity',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.MeterDataValidity,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Validity',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.MeterDataValidity,Vizelia.FOL.Common'
                }
            }/*
                 * , { header : $lang('msg_meterdata_source'), dataIndex : 'Source', sortable : true, filterable : true }
                 */];
    Viz.Configuration.Columns.Chart = [{
                header     : $lang('msg_chart_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                filterable : true,
                sortable   : true
            }, {
                header     : $lang('msg_chart_favorite'),
                dataIndex  : 'IsFavorite',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }, {
                header     : $lang('msg_portalwindow_openonstartup'),
                dataIndex  : 'OpenOnStartup',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }, {
                header     : $lang('msg_chart_displaymode'),
                dataIndex  : 'DisplayMode',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartDisplayMode,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'DisplayMode',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.ChartDisplayMode,Vizelia.FOL.Common'
                }
            }, {
                header    : $lang('msg_chart_startdate'),
                dataIndex : 'StartDate',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 90,
                sortable  : true
            }, {
                header    : $lang('msg_chart_enddate'),
                dataIndex : 'EndDate',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 90,
                sortable  : true
            }, {
                header     : $lang("msg_chart_localisation"),
                dataIndex  : 'Localisation',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartLocalisation,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Localisation',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.ChartLocalisation,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang("msg_chart_timeinterval"),
                dataIndex  : 'TimeInterval',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'TimeInterval',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang("msg_chart_type"),
                dataIndex  : 'ChartType',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartType,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'ChartType',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.ChartType,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang("msg_dataserie_type"),
                dataIndex  : 'DataSerieType',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.DataSerieType,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'DataSerieType',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.DataSerieType,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang("msg_chart_specificanalysis"),
                dataIndex  : 'SpecificAnalysis',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.ChartSpecificAnalysis,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'SpecificAnalysis',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.ChartSpecificAnalysis,Vizelia.FOL.Common'
                }
            }, /*
                 * { header : $lang("msg_dynamicdisplay"), dataIndex : 'DynamicDisplay.Name', sortable : true, filterable : true, filter : { type : 'generic', dataIndexGeneric : 'KeyDynamicDisplay', xtype : 'vizComboDynamicDisplay' } },
                 */{
                header     : $lang("msg_virtualfile"),
                dataIndex  : 'VirtualFilePath',
                sortable   : false,
                filterable : false,
                renderer   : function(data, cell, record) {
                    return '<a href="direct/' + record.data.Application + '/' + data + '" target="_blank">' + data + '</a>';
                }
            }, {
                header     : $lang('msg_chart_used'),
                dataIndex  : 'UsedCount',
                sortable   : true,
                xtype      : 'numbercolumn',
                format     : Viz.util.Chart.getNumberFormat({
                            DisplayDecimalPrecision : 0
                        }),
                align      : 'right',
                filterable : true,
                width      : 100
            }];

    Viz.Configuration.Columns.ChartSimple = [{
                header     : $lang('msg_chart_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                filterable : true,
                sortable   : true
            }];
    Viz.Configuration.Columns.DataSerie = [{
                header     : $lang('msg_localid'),
                dataIndex  : 'LocalId',
                sortable   : true,
                filterable : true,
                width      : 100,
                renderer   : function(data, cell, record) {
                    cell.css = 'viz-icon-cell x-icon-combo-icon ' + record.get('IconCls');
                    return data;
                }
            }, {
                header     : $lang('msg_dataserie_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true,
                width      : 150
            }, {
                header     : $lang('msg_dataserie_unit'),
                dataIndex  : 'Unit',
                sortable   : true,
                filterable : true,
                width      : 30
            }, {
                header    : $lang("msg_color"),
                dataIndex : 'Color',
                width     : 50,
                renderer  : Ext.ux.colorRenderer()
            }, {
                header     : $lang("msg_chartaxis"),
                dataIndex  : 'YAxisName',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_dataserie_hidden'),
                dataIndex  : 'Hidden',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }];

    Viz.Configuration.Columns.CalculatedSerie = [{
                header     : $lang('msg_calculatedserie_order'),
                dataIndex  : 'Order',
                sortable   : true,
                filterable : true,
                width      : 50
            }, {
                header     : $lang('msg_calculatedserie_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true,
                width      : 150
            }, {
                header     : $lang('msg_dataserie_unit'),
                dataIndex  : 'Unit',
                sortable   : true,
                filterable : true,
                width      : 30
            }, {
                header     : $lang("msg_calculatedserie_formula"),
                dataIndex  : 'Formula',
                sortable   : true,
                filterable : true,
                width      : 300
            }];

    Viz.Configuration.Columns.StatisticalSerie = [{
                header     : $lang("msg_id"),
                dataIndex  : 'LocalId',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_statisticalserie_operator"),
                dataIndex  : 'Operator',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.MathematicOperator,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Operator',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.MathematicOperator,Vizelia.FOL.Common'
                }
            }, {
                dataIndex : 'ClassificationItemLongPath',
                header    : $lang('msg_statisticalserie_classificationlongpath'),
                renderer  : Ext.ux.colorRenderer('ClassificationItem', 'ClassificationItemLongPath'),
                sortable  : true,
                filter    : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'KeyClassificationItemPath',
                    queryType     : 'innerpath',
                    xtype         : 'viztreecombomultiselectClassificationItemMeter'
                }
            }, {
                header     : $lang('msg_statisticalserie_includehiddenseries'),
                dataIndex  : 'IncludeHiddenSeries',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }, {
                header     : $lang('msg_statisticalserie_runaftercalculatedseries'),
                dataIndex  : 'RunAfterCalculatedSeries',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }];

    Viz.Configuration.Columns.DataPoint = [{
                header     : $lang('msg_datapoint_seriesname'),
                dataIndex  : 'SeriesName',
                sortable   : true,
                filterable : true,
                width      : 100
            }, {
                header    : $lang('msg_datapoint_xdatetime'),
                dataIndex : 'XDateTime',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 100,
                sortable  : true
            }, {
                header     : $lang('msg_datapoint_yvalue'),
                dataIndex  : 'YValue',
                sortable   : true,
                xtype      : 'numbercolumn',
                align      : 'right',
                filterable : true,
                width      : 100
            }];
    Viz.Configuration.Columns.ChartAxis = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true,
                width      : 300
            }, {
                header     : $lang("msg_chartaxis_min"),
                dataIndex  : 'Min',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_chartaxis_max"),
                dataIndex  : 'Max',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.DataSerieColorElement = [{
                header     : $lang("msg_dataseriecolorelement_minvalue"),
                dataIndex  : 'MinValue',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_dataseriecolorelement_maxvalue"),
                dataIndex  : 'MaxValue',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_dataseriecolorelement_highlight"),
                dataIndex  : 'Highlight',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.DataPointHighlight,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Highlight',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.DataPointHighlight,Vizelia.FOL.Common'
                }
            }, {
                header    : $lang("msg_color"),
                dataIndex : 'Color',
                renderer  : function(data, cell, record) {
                    cell.attr = 'style="background-color:#' + data + ';"';
                    return '';
                }
            }];

    Viz.Configuration.Columns.ChartHistoricalAnalysis = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header    : $lang('msg_chart_startdate'),
                dataIndex : 'StartDate',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 90,
                sortable  : true
            }, {
                header    : $lang('msg_chart_enddate'),
                dataIndex : 'EndDate',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 90,
                sortable  : true
            }, {
                header     : $lang("msg_charthistoricalanalysis_offset"),
                dataIndex  : 'Frequency',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_charthistoricalanalysis_interval"),
                dataIndex  : 'Interval',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Interval',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang('msg_charthistoricalanalysis_extrachartarea'),
                dataIndex  : 'ExtraChartArea',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 130,
                align      : "center",
                hidden     : true
            }, {
                header     : $lang("msg_dataserie_type"),
                dataIndex  : 'DataSerieType',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.DataSerieType,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'DataSerieType',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.DataSerieType,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang('msg_charthistoricalanalysis_enabled'),
                dataIndex  : 'Enabled',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 70,
                align      : "center"
            }, {
                header     : $lang('msg_charthistoricalanalysis_xaxisvisible'),
                dataIndex  : 'XAxisVisible',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 70,
                align      : "center"
            }];

    Viz.Configuration.Columns.ChartMarker = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_chartmarker_min"),
                dataIndex  : 'Min',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_chartmarker_max"),
                dataIndex  : 'Max',
                sortable   : true,
                filterable : true
            }, {
                header    : $lang("msg_color"),
                dataIndex : 'Color',
                renderer  : Ext.ux.colorRenderer()
            }, {
                header     : $lang("msg_chartaxis"),
                dataIndex  : 'ChartAxisName',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.Mail = [{
                header     : $lang('msg_id'),
                dataIndex  : 'KeyMail',
                sortable   : true,
                filterable : true,
                width      : 20
            }, {
                header     : $lang('msg_mail_classification'),
                dataIndex  : 'ClassificationItemTitle',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_mail_subject'),
                dataIndex  : 'Subject',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_mail_from'),
                dataIndex  : 'From',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_mail_to'),
                dataIndex  : 'To',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_mail_cc'),
                dataIndex  : 'CC',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_mail_body'),
                dataIndex  : 'Body',
                width      : 250,
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_isactive'),
                dataIndex  : 'IsActive',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 40,
                align      : "center"
            }];

    Viz.Configuration.Columns.Link = [{
                header     : $lang('msg_id'),
                dataIndex  : 'LocalId',
                sortable   : true,
                filterable : true,
                width      : 20
            }, {
                header     : $lang('msg_link_value'),
                dataIndex  : 'LinkValue',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.LoginHistory = [{
                header     : $lang('msg_loginhistory_username'),
                dataIndex  : 'UserName',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_profile_ip'),
                dataIndex  : 'IP',
                sortable   : true,
                filterable : true
            }, {
                header    : $lang('msg_loginhistory_logindate'),
                dataIndex : 'LoginDate',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 110,
                sortable  : true
            }, {
                header    : $lang('msg_loginhistory_logoutdate'),
                dataIndex : 'LogoutDate',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 110,
                sortable  : true
            }];

    Viz.Configuration.Columns.PortalWindow = [{
                header     : $lang('msg_portalwindow_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true,
                width      : 300,
                renderer   : function(data, cell, record) {
                    if (!Ext.isEmpty(record.get('IconClsSmall')))
                        cell.css = 'viz-icon-cell x-icon-combo-icon ' + record.get('IconClsSmall');
                    else if (!Ext.isEmpty(record.get('KeyImage')))
                        cell.css = 'viz-icon-cell x-icon-combo-icon ' + Viz.util.Portal.getCssFromImage(record.get('KeyImage'), 16, 16);

                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_portalwindow_tabnumber'),
                dataIndex  : 'PortalWindowTabNumber',
                sortable   : true,
                filterable : true
            }, /*
                 * { header : $lang('msg_portalwindow_defaulttabcolumnconfig'), dataIndex : 'PortalTabColumnConfig', sortable : true, filterable : true, width : 40, renderer : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({ serviceParams : { enumTypeName : 'Vizelia.FOL.BusinessEntities.PortalTabColumnConfig,Vizelia.FOL.Common' } })), filter : { type : 'comboEnum', dataIndexGeneric : 'PortalTabColumnConfig', enumTypeName : 'Vizelia.FOL.BusinessEntities.PortalTabColumnConfig,Vizelia.FOL.Common' } }
                 */{
                header     : $lang('msg_portalwindow_openonstartup'),
                dataIndex  : 'OpenOnStartup',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 40,
                align      : "center"
            }, {
                header    : $lang('msg_creator'), // $lang('msg_portaltemplate') + ' - ' + $lang('msg_loginhistory_username'),
                dataIndex : 'CreatorLogin'
            }];

    Viz.Configuration.Columns.PortalTab = [{
                header     : $lang('msg_portaltab_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true,
                width      : 200,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_portaltab_order'),
                dataIndex  : 'Order',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.PortalColumn = [{
                header     : $lang('msg_portalcolumn_flex'),
                dataIndex  : 'Flex',
                sortable   : true,
                filterable : true,
                width      : 200
            }, {
                header     : $lang('msg_portalcolumn_order'),
                dataIndex  : 'Order',
                sortable   : true,
                filterable : true,
                width      : 100
            }, {
                header     : $lang('msg_portalcolumn_type'),
                dataIndex  : 'Type',
                sortable   : true,
                filterable : true,
                width      : 40,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.PortalColumnType,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Type',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.PortalColumnType,Vizelia.FOL.Common'
                }
            }];

    Viz.Configuration.Columns.Portlet = [{
                header     : $lang('msg_portlet_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : false,
                width      : 100,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_portlet_order'),
                dataIndex  : 'Order',
                sortable   : true,
                filterable : false,
                width      : 100
            }, {
                header     : $lang('msg_portalwindow'),
                dataIndex  : 'PortalWindowTitle',
                sortable   : true,
                filterable : false,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_portaltab'),
                dataIndex  : 'PortalTabTitle',
                sortable   : true,
                filterable : false,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_portlet_keyentity'),
                dataIndex  : 'KeyEntity',
                hidden     : true,
                sortable   : true,
                filterable : false,
                width      : 100
            }, {
                header     : $lang('msg_portlet_typeentity'),
                dataIndex  : 'TypeEntity',
                hidden     : true,
                sortable   : true,
                filterable : false,
                width      : 100
            }];

    /**
     * Configuration columns for EventLog grid.
     */
    Viz.Configuration.Columns.EventLog = [{
                dataIndex  : 'Name',
                header     : $lang('msg_eventlog_name'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'Description',
                header     : $lang('msg_eventlog_description'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex : 'ClassificationItemLongPath',
                header    : $lang('msg_eventlog_classificationlongpath'),
                renderer  : Ext.ux.colorRenderer('ClassificationItem', 'ClassificationItemLongPath'),
                sortable  : false,
                filter    : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'ClassificationKeyParent',
                    xtype         : 'viztreecombomultiselectClassificationItemEventLog'
                }
            }, {
                header    : $lang("msg_eventlog_startdate"),
                dataIndex : 'StartDate',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 90,
                sortable  : true
            }, {
                header    : $lang("msg_eventlog_enddate"),
                dataIndex : 'EndDate',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 90,
                sortable  : true
            }, {
                dataIndex : 'LocationLongPath',
                header    : $lang('msg_location_path'),
                sortable  : true,
                renderer  : Ext.ux.locationPathRenderer(),
                filter    : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'KeyLocation',
                    xtype         : 'viztreecombomultiselectSpatial'
                }
            }];

    Viz.Configuration.Columns.DynamicDisplay = [{
                dataIndex  : 'Name',
                header     : $lang('msg_dynamicdisplay_name'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'KeyDynamicDisplay',
                header     : ' ',
                sortable   : false,
                filterable : false,
                renderer   : function(value, metaData, record) {
                    var url = Viz.Services.BuildUrlStream('Energy.svc', 'DynamicDisplay_GetStreamImage', {
                                Key    : value,
                                width  : 250,
                                height : record.get('Type') == 3 ? 80 : 200
                            })
                    return '<img src="' + url + '">';
                }
            }];

    Viz.Configuration.Columns.DynamicDisplayColorTemplate = [{
                dataIndex  : 'Name',
                header     : $lang('msg_dynamicdisplaycolortemplate_name'),
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.DynamicDisplayImage = [{
                dataIndex  : 'Name',
                header     : $lang('msg_dynamicdisplayimage_name'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'KeyDynamicDisplayImage',
                header     : ' ',
                sortable   : false,
                filterable : false,
                renderer   : function(value, metaData) {
                    var url = Viz.Services.BuildUrlStream('Energy.svc', 'DynamicDisplayImage_GetStreamImage', {
                                Key    : value,
                                width  : 40,
                                height : 40
                            })
                    return '<img src="' + url + '"  title="' + value + '">';
                }
            }];

    Viz.Configuration.Columns.VirtualFile = [{
                dataIndex  : 'Path',
                header     : $lang('msg_virtualfile_path'),
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    switch (record.get('EntityType')) {
                        case 'Vizelia.FOL.BusinessEntities.Chart' :
                            cell.css = 'viz-icon-cell viz-icon-small-chart';
                            break;
                        case 'Vizelia.FOL.BusinessEntities.PortalTab' :
                            cell.css = 'viz-icon-cell viz-icon-small-portalwindow';
                            break;

                        case 'Vizelia.FOL.BusinessEntities.Playlist' :
                            cell.css = 'viz-icon-cell viz-icon-small-playlist';
                            break;
                        case 'Vizelia.FOL.BusinessEntities.Meter' :
                            cell.css = 'viz-icon-cell viz-icon-small-meter';
                            break;
                    }
                    return data;
                }
            }, {
                header     : $lang('msg_virtualfile_keyentity'),
                dataIndex  : 'KeyEntity',
                sortable   : true,
                hidden     : true,
                filterable : true
            }, {
                header     : $lang('msg_virtualfile_entitytype'),
                dataIndex  : 'EntityType',
                sortable   : true,
                hidden     : true,
                filterable : true
            }, {
                header     : $lang('msg_virtualfile_width'),
                dataIndex  : 'Width',
                sortable   : true,
                align      : 'right',
                filterable : true,
                renderer   : function(data, cell, record) {
                    switch (record.get('EntityType')) {
                        case 'Vizelia.FOL.BusinessEntities.Meter' :
                            return 'N/A';
                            break;
                    }
                    return data;
                }
            }, {
                header     : $lang('msg_virtualfile_height'),
                dataIndex  : 'Height',
                sortable   : true,
                align      : 'right',
                filterable : true,
                renderer   : function(data, cell, record) {
                    switch (record.get('EntityType')) {
                        case 'Vizelia.FOL.BusinessEntities.Meter' :
                            return 'N/A';
                            break;
                    }
                    return data;
                }
            }, {
                header     : $lang("msg_virtualfile"),
                dataIndex  : 'Path',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return '<a href="direct/' + record.data.Application + '/' + data + '" target="_blank">' + data + '</a>';
                }
            }];

    Viz.Configuration.Columns.SecurableEntity = [{
                header     : $lang('msg_name'),
                dataIndex  : 'SecurableName',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    var iconCls = record.get('SecurableIconCls') || 'viz-icon-small-classificationitem';
                    cell.css = 'viz-icon-cell ' + iconCls;
                    return data;
                }
            }];

    Viz.Configuration.Columns.Report = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.ChartScheduler = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_calendarevent_startdate'),
                dataIndex  : 'StartDate',
                xtype      : 'datecolumn',
                align      : 'right',
                format     : Viz.getLocalizedDateTimeFormat(false),
                width      : 90,
                sortable   : false,
                filterable : false
                /*
                 * filter : { type : 'date', dateFormat : Date.patterns.UniversalSortableDateTime }
                 */
        }   , {
                header     : $lang('msg_chartscheduler_nextdate'),
                dataIndex  : 'NextOccurenceDate',
                xtype      : 'datecolumn',
                align      : 'right',
                format     : Viz.getLocalizedDateTimeFormat(false),
                width      : 90,
                sortable   : false,
                filterable : false
                /*
                 * filter : { type : 'date', dateFormat : Date.patterns.UniversalSortableDateTime }
                 */
        }   , {
                header     : $lang('msg_alarmdefinition_enabled'),
                dataIndex  : 'Enabled',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 70,
                align      : "center"
            }, {
                header     : $lang('msg_chartscheduler_subject'),
                dataIndex  : 'Subject',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_chartscheduler_body'),
                dataIndex  : 'Body',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_chartscheduler_emails'),
                dataIndex  : 'Emails',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.PortalTemplate = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.WeatherLocation = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_weatherlocation_unit'),
                dataIndex  : 'Unit',
                sortable   : true,
                filterable : true,
                width      : 40,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.WeatherUnit,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Unit',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.WeatherUnit,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang('msg_chart_favorite'),
                dataIndex  : 'IsFavorite',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }, {
                header     : $lang('msg_portalwindow_openonstartup'),
                dataIndex  : 'OpenOnStartup',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }];

    Viz.Configuration.Columns.WebFrame = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_webframe_url'),
                dataIndex  : 'Url',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_webframe_body'),
                dataIndex  : 'Body',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_chart_favorite'),
                dataIndex  : 'IsFavorite',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }, {
                header     : $lang('msg_portalwindow_openonstartup'),
                dataIndex  : 'OpenOnStartup',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }];

    Viz.Configuration.Columns.Map = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_chart_favorite'),
                dataIndex  : 'IsFavorite',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }, {
                header     : $lang('msg_portalwindow_openonstartup'),
                dataIndex  : 'OpenOnStartup',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }];

    Viz.Configuration.Columns.Tenant = [{
                header    : $lang('msg_key'),
                dataIndex : 'KeyTenant',
                hidden    : true,
                sortable  : true
            }, {
                header    : $lang('msg_name'),
                dataIndex : 'Name',
                sortable  : true
            }, {
                header    : $lang('msg_tenant_url'),
                dataIndex : 'Url',
                sortable  : true
            }, {
                header    : $lang('msg_tenant_admin_email'),
                dataIndex : 'AdminEmail',
                sortable  : true
                // }, {
            // header : $lang('msg_tenant_isolated'),
            // dataIndex : 'IsIsolated',
            // sortable : true,
            // xtype : 'booleancolumn',
            // trueText : "&#10003;",
            // falseText : ''
        }   , {
                header    : $lang('msg_tenant_is_initialized'),
                dataIndex : 'IsInitialized',
                sortable  : true,
                xtype     : 'booleancolumn',
                trueText  : "&#10003;",
                falseText : ''
            }, {
                header    : $lang('msg_tenant_hosting_container'),
                dataIndex : 'HostingContainer',
                sortable  : true
            }];

    Viz.Configuration.Columns.MachineInstance = [{
                header     : $lang('msg_key'),
                dataIndex  : 'KeyMachineInstance',
                hidden     : true,
                sortable   : true,
                filterable : true
            }, {
                header          : $lang('msg_title'),
                dataIndex       : 'InstanceName',
                sortable        : true,
                filterable      : true,
                summaryType     : 'count',
                summaryRenderer : function(v, params, data) {
                    return v + ' ' + $lang('msg_machineinstance') + '(s)';
                }
            }, {
                header     : $lang('msg_machineinstance_ipaddress'),
                dataIndex  : 'AbsoluteUri',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_machineinstance_status'),
                dataIndex  : 'Status',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_machineinstance_machinepurpose'),
                dataIndex  : 'MachinePurpose',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_machineinstance_environmentname'),
                dataIndex  : 'EnvironmentName',
                sortable   : true,
                filterable : true
            }, {
                header      : $lang('msg_machineinstance_inmemoryobjectcount'),
                dataIndex   : 'InMemoryObjectCount',
                xtype       : 'numbercolumn',
                align       : 'right',
                sortable    : true,
                filterable  : true,
                summaryType : 'sum'
            }, {
                header     : $lang("msg_machineinstance_size"),
                dataIndex  : 'Size',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.MachineInstanceSize,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Size',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.MachineInstanceSize,Vizelia.FOL.Common'
                }
            }];

    Viz.Configuration.Columns.DrawingCanvas = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_chart_favorite'),
                dataIndex  : 'IsFavorite',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }, {
                header     : $lang('msg_portalwindow_openonstartup'),
                dataIndex  : 'OpenOnStartup',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }];

    Viz.Configuration.Columns.DrawingCanvasChart = [/*
                                                     * { header : $lang('msg_title'), dataIndex : 'Title', sortable : true, filterable : true },
                                                     */{
                header     : $lang('msg_chart_title'),
                dataIndex  : 'ChartTitle',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_drawingcanvaschart_zindex'),
                dataIndex  : 'ZIndex',
                sortable   : true,
                filterable : true,
                width      : 70
            }];

    Viz.Configuration.Columns.DrawingCanvasImage = [/*
                                                     * { header : $lang('msg_title'), dataIndex : 'Title', sortable : true, filterable : true },
                                                     */{
                header     : $lang('msg_drawingcanvasimage_title'),
                dataIndex  : 'ImageTitle',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_drawingcanvaschart_zindex'),
                dataIndex  : 'ZIndex',
                sortable   : true,
                filterable : true,
                width      : 70
            }];
    Viz.Configuration.Columns.FilterSpatialPset = [{
                header     : $lang('msg_filterspatialpset_psetname'),
                dataIndex  : 'PsetName',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                    cell.css = 'viz-icon-cell ' + record.get('IconCls');
                    return data;
                }
            }, {
                header     : $lang('msg_filterspatialpset_attributename'),
                dataIndex  : 'AttributeName',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_filterspatialpset_condition"),
                dataIndex  : 'Condition',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.FilterCondition,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'FilterCondition',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.FilterCondition,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang('msg_filterspatialpset_value'),
                dataIndex  : 'Value',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_filterspatialpset_value1'),
                dataIndex  : 'Value2',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_filterspatialpset_operator"),
                dataIndex  : 'Operator',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.LogicalOperator,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Operator',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.LogicalOperator,Vizelia.FOL.Common'
                }
            }];

    Viz.Configuration.Columns.DataSeriePsetRatio = [{
                header     : $lang('msg_dataseriepsetratio_psetname'),
                dataIndex  : 'PsetName',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record, rowIndex, colIndex, store) {
                    cell.css = 'viz-icon-cell ' + record.get('IconCls');
                    return data;
                }
            }, {
                header     : $lang('msg_dataseriepsetratio_attributename'),
                dataIndex  : 'AttributeName',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_dataseriepsetratio_operator"),
                dataIndex  : 'Operator',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.ArithmeticOperator,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Operator',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.ArithmeticOperator,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang("msg_meter_groupoperation"),
                dataIndex  : 'GroupOperation',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.MathematicOperator,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'GroupOperation',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.MathematicOperator,Vizelia.FOL.Common'
                }
            }];

    Viz.Configuration.Columns.PsetAttributeHistorical = [{
                header    : $lang('msg_psetattributehistorical_attributeacquisitiondatetime'),
                dataIndex : 'AttributeAcquisitionDateTime',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                width     : 120,
                sortable  : true,
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                }
            }, {
                header     : $lang('msg_psetattributehistorical_attributevalue'),
                dataIndex  : 'AttributeValue',
                xtype      : 'numbercolumn',
                format     : Viz.util.Chart.getNumberFormat({
                            DisplayDecimalPrecision : 9
                        }),
                sortable   : true,
                align      : 'right',
                filterable : true,
                filter     : {
                    type      : 'float',
                    dataIndex : 'AttributeValue',
                    fieldCls  : Ext.extend(Ext.form.NumberField, {
                                decimalPrecision : 9
                            })
                }
            }, {
                header     : $lang('msg_psetattributehistorical_attributecomment'),
                dataIndex  : 'AttributeComment',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.Job = [{
                header    : $lang('msg_is_paused'),
                dataIndex : 'IsPaused',
                renderer  : function(value, metaData) {
                    if (value === true) {
                        metaData.css = 'viz-icon-cell ' + 'viz-icon-small-job-pause';
                    }
                    else {
                        metaData.css = 'viz-icon-cell ' + 'viz-icon-small-job-resume';
                    }
                    return '';
                },
                align     : "center",
                resizable : true,
                width     : 5
            }, {
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_job_triggers'),
                dataIndex  : 'Triggers',
                sortable   : false,
                filterable : false,
                renderer   : Ext.ux.LongTextRenderer
            }, {
                dataIndex  : 'LastRunTime',
                header     : $lang('msg_job_lastruntime'),
                format     : Viz.getLocalizedDateTimeFormat(true),
                renderer   : {
                    loadStore : function() {
                        store = new Viz.store.Enum({
                                    serviceParams : {
                                        enumTypeName : 'Vizelia.FOL.BusinessEntities.TraceEntrySeverity,Vizelia.FOL.Common'
                                    }
                                });
                        store.load();
                    },
                    fn        : function(value, cell, rec) {
                        var retVal = value;
                        retVal = Ext.util.Format.date(retVal, this.format);

                        value = rec.get('LastRunSeverity');
                        var index = store.find("Value", value);
                        if (index >= 0) {
                            var iconCss = store.getAt(index).get("IconCls");
                            if (!Ext.isEmpty(iconCss))
                                cell.css = 'viz-icon-cell ' + iconCss;
                        }
                        return retVal;
                    }
                },
                sortable   : false,
                filterable : false,
                width      : 40
            }, {
                dataIndex  : 'NextRunTime',
                header     : $lang('msg_job_nextruntime'),
                xtype      : 'datecolumn',
                format     : Viz.getLocalizedDateTimeFormat(true),
                /*
                 * filter : { type : 'date', dateFormat : Date.patterns.UniversalSortableDateTime },
                 */
                sortable   : false,
                filterable : false,
                width      : 35
            }];

    Viz.Configuration.Columns.JobInstance = [{
                dataIndex  : 'KeyJobInstance',
                header     : $lang('msg_jobinstance_key'),
                // align: 'left',
                sortable   : false,
                filterable : false,
                width      : 40,
                hidden     : true
            }, {
                header     : $lang('msg_trace_severity'),
                dataIndex  : 'Severity',
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.TraceEntrySeverity,Vizelia.FOL.Common'
                            }
                        })),
                sortable   : true,
                filterable : false,
                width      : 150
            }, {
                dataIndex  : 'StartDate',
                header     : $lang('msg_jobinstance_startdate'),
                xtype      : 'datecolumn',
                format     : Viz.getLocalizedDateTimeFormat(true),
                // align: 'left',
                sortable   : true,
                filterable : false,
                width      : 150
            }, {
                dataIndex  : 'EndDate',
                header     : $lang('msg_jobinstance_enddate'),
                xtype      : 'datecolumn',
                format     : Viz.getLocalizedDateTimeFormat(true),
                // align: 'left',
                sortable   : true,
                filterable : false,
                width      : 150
            }, {
                dataIndex  : 'Duration',
                header     : $lang('msg_jobinstance_duration'),
                // align: 'left',
                sortable   : false,
                filterable : false,
                width      : 150
            }, {
                header     : $lang('msg_jobinstance_jobruninstancerequestid'),
                dataIndex  : 'JobRunInstanceRequestId',
                id         : 'jobRunInstanceRequestIdColumn',
                sortable   : false,
                filterable : false,
                // align: 'left',
                width      : 150
            }];

    Viz.Configuration.Columns.Schedule = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true
            }, {
                header    : $lang('msg_calendarevent_startdate'),
                dataIndex : 'StartDate',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(false),
                width     : 90,
                sortable  : true,
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                }
            }];

    Viz.Configuration.Columns.Step = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_step_methodname'),
                dataIndex  : 'MethodMsgCode',
                sortable   : false,
                filterable : false,
                renderer   : function(data, cell, record) {
                    return $lang(data);
                }
            }];

    Viz.Configuration.Columns.Logging = [{
                dataIndex  : 'KeyLogging',
                header     : $lang('msg_key'),
                // renderer : Ext.ux.LoggingRenderer(),
                filterable : true,
                sortable   : true,
                width      : 40
            }, {
                dataIndex : 'Timestamp',
                header    : $lang('msg_logging_timestamp'),
                xtype     : 'datecolumn',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                width     : 70,
                sortable  : true
            }, {
                header     : $lang('msg_trace_severity'),
                dataIndex  : 'Severity',
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.TraceEntrySeverity,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Severity',
                    displayIcon      : true,
                    insideFilter     : true,
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.TraceEntrySeverity,Vizelia.FOL.Common'
                },
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'MachineName',
                header     : $lang('msg_logging_machinename'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'Duration',
                header     : $lang('msg_logging_duration'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'Exception',
                header     : $lang('msg_logging_message'),
                // align : 'right',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.LongTextRenderer
            }];

    Viz.Configuration.Columns.AlarmDefinition = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true
            }, {
                header    : $lang('msg_alarmdefinition_startdate'),
                dataIndex : 'StartDate',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(false),
                width     : 90,
                sortable  : true,
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                }
            }, {
                header    : $lang('msg_alarmdefinition_enddate'),
                dataIndex : 'EndDate',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(false),
                width     : 90,
                sortable  : true,
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                }
            }, {
                header     : $lang('msg_alarmdefinition_enabled'),
                dataIndex  : 'Enabled',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 70,
                align      : "center"
            }, {
                header     : $lang('msg_alarmdefinition_processmanually'),
                dataIndex  : 'ProcessManually',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 70,
                align      : "center"
            }, {
                dataIndex : 'ClassificationItemLongPath',
                header    : $lang('msg_alarmdefinition_classification'),
                sortable  : true,
                renderer  : Ext.ux.colorRenderer('ClassificationItem', 'ClassificationItemLongPath'),
                filter    : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'KeyClassificationItemPath',
                    queryType     : 'innerpath',
                    xtype         : 'viztreecombomultiselectClassificationItemAlarmDefinition'

                }
            }, {
                header     : $lang('msg_alarmdefinition_usepercentagedelta'),
                dataIndex  : 'UsePercentageDelta',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 70,
                align      : "center"
            }, {
                header     : $lang("msg_alarmdefinition_condition"),
                dataIndex  : 'Condition',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.FilterCondition,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Condition',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.FilterCondition,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang('msg_alarmdefinition_threshold'),
                dataIndex  : 'Threshold',
                xtype      : 'numbercolumn',
                align      : 'right',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_alarmdefinition_threshold2'),
                dataIndex  : 'Threshold2',
                xtype      : 'numbercolumn',
                sortable   : true,
                align      : 'right',
                filterable : true
            }];

    Viz.Configuration.Columns.MeterDataExportTask = [{
                dataIndex : 'KeyMeterDataExportTask',
                hidden    : true
            }, {
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_timerange'),
                dataIndex  : 'TimeRange',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_compress'),
                dataIndex  : 'Compress',
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                align      : "center",
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_singleoutputfile'),
                dataIndex  : 'SingleOutputFile',
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                align      : "center",
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.MappingTask = [{
                dataIndex : 'KeyMappingTask',
                hidden    : true
            }, {
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_file_source_type'),
                dataIndex  : 'FileSourceType',
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.FileSourceType,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'FileSourceType',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.FileSourceType,Vizelia.FOL.Common'
                },
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_address'),
                dataIndex  : 'Address',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_use_custom_credentials'),
                dataIndex  : 'UseCustomCredentials',
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                align      : "center",
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_username'),
                dataIndex  : 'Username',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.TraceSummary = [{
                header     : $lang('msg_trace_severity'),
                dataIndex  : 'Severity',
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.TraceEntrySeverity,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Severity',
                    displayIcon      : true,
                    insideFilter     : true,
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.TraceEntrySeverity,Vizelia.FOL.Common'
                },
                sortable   : false,
                filterable : false
            }, {
                dataIndex  : 'TraceCategory',
                id         : 'traceCategoryColumn',
                header     : $lang('msg_trace_category'),
                align      : 'left',
                groupable  : false,
                sortable   : false,
                filterable : false
            }, {
                dataIndex   : 'LastHour',
                id          : 'lastHourColumn',
                header      : $lang('msg_trace_summary_last_hour'),
                align       : 'left',
                groupable   : false,
                sortable    : false,
                filterable  : false,
                summaryType : 'sum',
                renderer    : function(v) {
                    return v;
                }
            }, {
                dataIndex   : 'LastDay',
                id          : 'lastDayColumn',
                header      : $lang('msg_trace_summary_last_day'),
                align       : 'left',
                groupable   : false,
                sortable    : false,
                filterable  : false,
                summaryType : 'sum',
                renderer    : Ext.ux.LongTextRenderer
            }, {
                dataIndex   : 'LastWeek',
                id          : 'lastWeekColumn',
                header      : $lang('msg_trace_summary_last_week'),
                align       : 'left',
                groupable   : false,
                sortable    : false,
                filterable  : false,
                summaryType : 'sum',
                renderer    : Ext.ux.LongTextRenderer
            }];

    Viz.Configuration.Columns.TraceEntry = [{
                dataIndex  : 'KeyTraceEntry',
                header     : $lang('msg_key'),
                // renderer : Ext.ux.TraceEntryRenderer(),
                filterable : true,
                sortable   : true,
                width      : 40
            }, {
                dataIndex : 'Timestamp',
                header    : $lang('msg_logging_timestamp'),
                xtype     : 'datecolumn',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                sortable  : true,
                width     : 150
            }, {
                header     : $lang('msg_trace_severity'),
                dataIndex  : 'Severity',
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.TraceEntrySeverity,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Severity',
                    displayIcon      : true,
                    insideFilter     : true,
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.TraceEntrySeverity,Vizelia.FOL.Common'
                },
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'MachineName',
                header     : $lang('msg_logging_machinename'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'TraceCategory',
                header     : $lang('msg_trace_category'),
                align      : 'left',
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'RequestId',
                header     : $lang('msg_logging_requestid'),
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'RelatedKeyEntity',
                header     : $lang('msg_trace_related_entity_key'),
                align      : 'left',
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'Message',
                id         : 'messageColumn',
                header     : $lang('msg_logging_message'),
                align      : 'left',
                sortable   : false,
                filterable : true
                // renderer : Ext.ux.LongTextRenderer
        }];

    Viz.Configuration.Columns.AlarmDefinitionSimple = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                filterable : true,
                sortable   : true
            }];

    Viz.Configuration.Columns.AlarmInstance = [{
                dataIndex  : 'ClassificationItemLongPath',
                header     : $lang('msg_alarmdefinition_classification'),
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.colorRenderer('AlarmDefinition.ClassificationItem', 'AlarmDefinition.ClassificationItemLongPath'),
                filter     : {
                    type          : 'treecombomultiselect',
                    dataIndexTree : 'KeyClassificationItemPath',
                    queryType     : 'innerpath',
                    xtype         : 'viztreecombomultiselectClassificationItemAlarmDefinition'
                }
            }, {
                header     : $lang('msg_alarminstance_ischartinstance'),
                dataIndex  : 'IsChartInstance',
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 20,
                align      : "center",
                sortable   : false,
                filterable : true
            }, {
                header     : $lang('msg_chart_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_alarminstance_status"),
                dataIndex  : 'Status',
                sortable   : true,
                filterable : false,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.AlarmInstanceStatus,Vizelia.FOL.Common'
                            }
                        }))
            }, {
                header     : $lang('msg_alarminstance_value'),
                dataIndex  : 'Value',
                xtype      : 'numbercolumn',
                align      : 'right',
                sortable   : true,
                filterable : true
            }, {
                header    : $lang('msg_alarminstance_datetime'),
                dataIndex : 'InstanceDateTime',
                // xtype : 'datecolumn',
                align     : 'right',
                // format : Viz.getLocalizedDateTimeFormat(true),
                width     : 120,
                sortable  : true,
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                renderer  : {
                    fn    : function(data, cell, record) {
                        if (record.data.UTCInstanceDateTime) {
                            data = record.data.UTCInstanceDateTime;
                        }
                        return Ext.util.Format.date(data, Viz.getLocalizedDateTimeFormat(true));
                    },
                    scope : this
                }
            }, {
                header    : $lang('msg_alarminstance_chartdatetime'),
                dataIndex : 'ChartInstanceDateTime',
                align     : 'right',
                sortable  : false,
                width     : 140
            }, {
                header     : $lang('msg_alarmdefinition_threshold'),
                dataIndex  : 'Threshold',
                xtype      : 'numbercolumn',
                align      : 'right',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_alarmdefinition_threshold2'),
                dataIndex  : 'Threshold2',
                xtype      : 'numbercolumn',
                sortable   : true,
                align      : 'right',
                filterable : true
            }, {
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'LocationLongPath',
                id         : 'LocationLongPath',
                header     : $lang('msg_location_path'),
                sortable   : false,
                filterable : false,
                renderer   : Ext.ux.locationPathRenderer()
                /*
                 * filter : { type : 'treecombomultiselect', dataIndexTree : 'KeyLocationPath', queryType : 'innerpath', xtype : 'viztreecombomultiselectSpatial' }
                 */
        }];

    Viz.Configuration.Columns.MeterValidationRuleAlarmInstance = [{
                header     : $lang('msg_metervalidationrule'),
                dataIndex  : 'MeterValidationRule.Title',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_meter'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                dataIndex  : 'LocationLongPath',
                id         : 'LocationLongPath',
                header     : $lang('msg_location_path'),
                sortable   : false,
                filterable : false,
                renderer   : Ext.ux.locationPathRenderer()
                /*
                 * filter : { type : 'treecombomultiselect', dataIndexTree : 'KeyLocationPath', queryType : 'innerpath', xtype : 'viztreecombomultiselectSpatial' }
                 */
        }   , {
                header     : $lang("msg_alarminstance_metervalidationrulealarmtype"),
                dataIndex  : 'MeterValidationRuleAlarmType',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.MeterValidationRuleAlarmType,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Status',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.MeterValidationRuleAlarmType,Vizelia.FOL.Common'
                }
            }, {
                header    : $lang('msg_alarminstance_datetime'),
                dataIndex : 'InstanceDateTime',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                width     : 120,
                sortable  : true,
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                }
            }, {
                header    : $lang('msg_alarminstance_startdate'),
                dataIndex : 'MeterDataAcquisitionDateTimeStart',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                width     : 120,
                sortable  : true,
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                }
            }, {
                header    : $lang('msg_alarminstance_enddate'),
                dataIndex : 'MeterDataAcquisitionDateTimeEnd',
                xtype     : 'datecolumn',
                align     : 'right',
                format    : Viz.getLocalizedDateTimeFormat(true),
                width     : 120,
                sortable  : true,
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                }
            }, {
                header     : $lang('msg_metervalidationrule_minvalue'),
                dataIndex  : 'Threshold',
                xtype      : 'numbercolumn',
                align      : 'right',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_metervalidationrule_maxvalue'),
                dataIndex  : 'Threshold2',
                xtype      : 'numbercolumn',
                sortable   : true,
                align      : 'right',
                filterable : true
            }, {
                header     : $lang('msg_alarminstance_value'),
                dataIndex  : 'Value',
                xtype      : 'numbercolumn',
                align      : 'right',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_alarminstance_status"),
                dataIndex  : 'Status',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.AlarmInstanceStatus,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Status',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.AlarmInstanceStatus,Vizelia.FOL.Common'
                }
            }];

    Viz.Configuration.Columns.AlarmTable = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_chart_favorite'),
                dataIndex  : 'IsFavorite',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }, {
                header     : $lang('msg_portalwindow_openonstartup'),
                dataIndex  : 'OpenOnStartup',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }];

    Viz.Configuration.Columns.StruxureWareDataAcquisitionContainer = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_struxurewaredataacquisitioncontainer_ip'),
                dataIndex  : 'IP',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.StruxureWareDataAcquisitionEndpoint = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_struxurewaredataacquisitionendpoint_value'),
                dataIndex  : 'Value',
                xtype      : 'numbercolumn',
                align      : 'right',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.EWSDataAcquisitionContainer = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Url',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.EWSDataAcquisitionEndpoint = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_path'),
                dataIndex  : 'Path',
                sortable   : true,
                filterable : true
            }, {
                header    : $lang('msg_ewsdataacquisitionendpoint_value'),
                dataIndex : 'ValueString',
                align     : 'right'
            }];

    Viz.Configuration.Columns.RESTDataAcquisitionContainer = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Url',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.RESTDataAcquisitionEndpoint = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header    : $lang('msg_restdataacquisitionendpoint_value'),
                dataIndex : 'ValueString',
                align     : 'right'
            }, {
                header     : $lang('msg_restdataacquisitionendpoint_unit'),
                dataIndex  : 'Unit',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.ChartPsetAttributeHistorical = [{
                header     : $lang('msg_dataseriepsetratio_psetname'),
                dataIndex  : 'PsetName',
                sortable   : true,
                filterable : true,
                width      : 150
            }, {
                header     : $lang('msg_dataseriepsetratio_attributename'),
                dataIndex  : 'AttributeName',
                sortable   : true,
                filterable : true,
                width      : 150
            }, {
                header     : $lang("msg_meter_groupoperation"),
                dataIndex  : 'GroupOperation',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.MathematicOperator,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'GroupOperation',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.MathematicOperator,Vizelia.FOL.Common'
                }
            }];

    Viz.Configuration.Columns.EnergyCertificate = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }]

    Viz.Configuration.Columns.EnergyCertificateCategory = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_energycertificatecategory_minvalue'),
                dataIndex  : 'MinValue',
                xtype      : 'numbercolumn',
                align      : 'right',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_energycertificatecategory_maxvalue'),
                dataIndex  : 'MaxValue',
                xtype      : 'numbercolumn',
                align      : 'right',
                sortable   : true,
                filterable : true
            }]

    Viz.Configuration.Columns.MeterValidationRule = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_alarmdefinition_enabled'),
                dataIndex  : 'Enabled',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 70,
                align      : "center"
            }, {
                header     : $lang('msg_meterdata_frequency'),
                dataIndex  : 'AcquisitionFrequency',
                xtype      : 'numbercolumn',
                format     : Viz.util.Chart.getNumberFormat({
                            DisplayDecimalPrecision : 0
                        }),
                align      : 'right',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_meterdata_frequency"),
                dataIndex  : 'AcquisitionInterval',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'AcquisitionInterval',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang('msg_metervalidationrule_delay'),
                dataIndex  : 'Delay',
                xtype      : 'numbercolumn',
                format     : Viz.util.Chart.getNumberFormat({
                            DisplayDecimalPrecision : 0
                        }),
                align      : 'right',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang("msg_metervalidationrule_delay"),
                dataIndex  : 'DelayInterval',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'AcquisitionInterval',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.AxisTimeInterval,Vizelia.FOL.Common'
                }
            }, {
                header     : $lang('msg_metervalidationrule_increaseonly'),
                dataIndex  : 'IncreaseOnly',
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                align      : "center",
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_metervalidationrule_spike'),
                dataIndex  : 'SpikeDetection',
                xtype      : 'numbercolumn',
                align      : 'right',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_metervalidationrule_maxrecurringvalues'),
                dataIndex  : 'MaxRecurringValues',
                xtype      : 'numbercolumn',
                align      : 'right',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_metervalidationrule_minvalue'),
                dataIndex  : 'MinValue',
                xtype      : 'numbercolumn',
                align      : 'right',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_metervalidationrule_maxvalue'),
                dataIndex  : 'MaxValue',
                xtype      : 'numbercolumn',
                align      : 'right',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_meterdata_validity'),
                dataIndex  : 'Validity',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.MeterDataValidity,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Validity',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.MeterDataValidity,Vizelia.FOL.Common'
                }
            }];

    Viz.Configuration.Columns.Audit = [{
                header    : $lang('msg_audit_changetime'),
                dataIndex : 'ChangeTime',
                xtype     : 'datecolumn',
                format    : Viz.getLocalizedDateTimeFormat(true),
                filter    : {
                    type       : 'date',
                    dateFormat : Date.patterns.UniversalSortableDateTime
                },
                sortable  : true
            }, {
                header    : $lang('msg_audit_modifiedby'),
                dataIndex : 'ModifiedBy',
                sortable  : true
            }, {
                header     : $lang('msg_audit_operation'),
                dataIndex  : 'Operation',
                sortable   : true,
                filterable : true,
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.AuditOperation,Vizelia.FOL.Common'
                            }
                        })),
                filter     : {
                    type             : 'comboEnum',
                    dataIndexGeneric : 'Operation',
                    enumTypeName     : 'Vizelia.FOL.BusinessEntities.AuditOperation,Vizelia.FOL.Common'
                }
            }];

    Viz.Configuration.Columns.ReportSubscription = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : false,
                filterable : false
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : false,
                filterable : false
            }, {
                header     : $lang('msg_file_format'),
                dataIndex  : 'FileFormat',
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.ExportType,Vizelia.FOL.Common'
                            }
                        })),
                sortable   : false,
                filterable : false
            }, {
                header     : $lang('msg_recipients'),
                dataIndex  : 'Recipients',
                sortable   : false,
                filterable : false
            }, {
                header     : $lang('msg_last_run'),
                xtype      : 'datecolumn',
                format     : Viz.getLocalizedDateTimeFormat(true),
                dataIndex  : 'LastRun',
                sortable   : false,
                filterable : false
            }, {
                header     : $lang('msg_status'),
                dataIndex  : 'Status',
                sortable   : false,
                filterable : false
            }];

    Viz.Configuration.Columns.Algorithm = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_algorithm_source'),
                dataIndex  : 'Source',
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.AlgorithmSource,Vizelia.FOL.Common'
                            }
                        })),
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_algorithm_language'),
                dataIndex  : 'Language',
                renderer   : Ext.ux.comboBoxEnumRenderer(new Viz.store.Enum({
                            serviceParams : {
                                enumTypeName : 'Vizelia.FOL.BusinessEntities.AlgorithmLanguage,Vizelia.FOL.Common'
                            }
                        })),
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.ChartAlgorithm = [{
                header     : $lang('msg_order'),
                dataIndex  : 'Order',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_name'),
                dataIndex  : 'AlgorithmName',
                width      : 200,
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.FlipCard = [{
                header     : $lang('msg_title'),
                dataIndex  : 'Title',
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_flipcard_random'),
                dataIndex  : 'Random',
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                align      : "center",
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_flipcard_fliponrefresh'),
                dataIndex  : 'FlipOnRefresh',
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                align      : "center",
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_chart_favorite'),
                dataIndex  : 'IsFavorite',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }, {
                header     : $lang('msg_portalwindow_openonstartup'),
                dataIndex  : 'OpenOnStartup',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 50,
                align      : "center"
            }];

    Viz.Configuration.Columns.FlipCardEntry = [{
                header     : $lang('msg_portlet_order'),
                dataIndex  : 'Order',
                sortable   : true,
                filterable : false,
                width      : 100
            }, {
                header     : $lang('msg_title'),
                dataIndex  : 'TitleEntity',
                sortable   : true,
                filterable : false,
                renderer   : function(data, cell, record) {
                    cell.css = 'viz-icon-cell ' + record.get('IconClsEntity');
                    return Viz.util.Portal.localizeTitle(data);
                }
            }, {
                header     : $lang('msg_portlet_keyentity'),
                dataIndex  : 'KeyEntity',
                hidden     : true,
                sortable   : true,
                filterable : false,
                width      : 100
            }, {
                header     : $lang('msg_portlet_typeentity'),
                dataIndex  : 'TypeEntity',
                hidden     : true,
                sortable   : true,
                filterable : false,
                width      : 100
            }];

    Viz.Configuration.Columns.AuditEntity = [{
                header     : $lang('msg_id'),
                dataIndex  : 'KeyAuditEntity',
                sortable   : true,
                filterable : true,
                width      : 20
            }, /*
                 * { header : $lang('msg_auditentity_application'), dataIndex : 'Application', sortable : true, filterable : true },
                 */{
                header     : $lang('msg_auditentity_entityname'),
                dataIndex  : 'EntityName',
                width      : 250,
                sortable   : true,
                filterable : true,
                renderer   : function(data, cell, record) {
                    cell.css = 'viz-icon-cell ' + record.get("IconCls");
                    return record.get("EntityNameLocalized");
                }

            }, {
                header     : $lang('msg_auditentity_isauditenabled'),
                dataIndex  : 'IsAuditEnabled',
                sortable   : true,
                filterable : true,
                xtype      : 'booleancolumn',
                trueText   : "&#10003;",
                falseText  : '',
                width      : 40,
                align      : "center"
            }];

    Viz.Configuration.Columns.AlgorithmInputDefinition = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.AlgorithmInputValue = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Name',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_description'),
                dataIndex  : 'Description',
                sortable   : true,
                filterable : true
            }, {
                header     : $lang('msg_value'),
                dataIndex  : 'Value',
                sortable   : true,
                filterable : true
            }];

    Viz.Configuration.Columns.EntityCount = [{
                header     : $lang('msg_name'),
                dataIndex  : 'Label',
                sortable   : false,
                filterable : false,
                renderer   : function(data, cell, record) {
                    cell.css = 'viz-icon-cell ' + record.get("IconCls");
                    return record.get("Label") || record.get("Id");
                }
            }, {
                header     : $lang('msg_value'),
                dataIndex  : 'Value',
                sortable   : false,
                filterable : false,
                xtype      : 'numbercolumn',
                format     : Viz.util.Chart.getNumberFormat({
                            DisplayDecimalPrecision : 0
                        }),
                align      : 'right'
            }];
}

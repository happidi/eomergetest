﻿<?xml version="1.0" encoding="utf-8"?>
<project path="" name="Viz-Ext - JS Lib" author="Vizelia" version="1.0" copyright="Viz-Ext JS Library $version&#xD;&#xA;Copyright(c) 2010-2012, $author.&#xD;&#xA;licensing@vizelia.com&#xD;&#xA;&#xD;&#xA;http://www.vizelia.com" output="$project" source="False" source-dir="$output\deploy" minify="False" min-dir="$output\build" doc="False" doc-dir="$output\docs" master="true" master-file="$output\yui-ext.js" zip="true" zip-file="$output\yuo-ext.$version.zip">
  <directory name="source" />
  <target name="viz-ext-all" file="$output\viz-ext-all.js" debug="True" shorthand="False" shorthand-list="YAHOO.util.Dom.setStyle&#xD;&#xA;YAHOO.util.Dom.getStyle&#xD;&#xA;YAHOO.util.Dom.getRegion&#xD;&#xA;YAHOO.util.Dom.getViewportHeight&#xD;&#xA;YAHOO.util.Dom.getViewportWidth&#xD;&#xA;YAHOO.util.Dom.get&#xD;&#xA;YAHOO.util.Dom.getXY&#xD;&#xA;YAHOO.util.Dom.setXY&#xD;&#xA;YAHOO.util.CustomEvent&#xD;&#xA;YAHOO.util.Event.addListener&#xD;&#xA;YAHOO.util.Event.getEvent&#xD;&#xA;YAHOO.util.Event.getTarget&#xD;&#xA;YAHOO.util.Event.preventDefault&#xD;&#xA;YAHOO.util.Event.stopEvent&#xD;&#xA;YAHOO.util.Event.stopPropagation&#xD;&#xA;YAHOO.util.Event.stopEvent&#xD;&#xA;YAHOO.util.Anim&#xD;&#xA;YAHOO.util.Motion&#xD;&#xA;YAHOO.util.Connect.asyncRequest&#xD;&#xA;YAHOO.util.Connect.setForm&#xD;&#xA;YAHOO.util.Dom&#xD;&#xA;YAHOO.util.Event">
    <include name="source\services\AssemblyVersion.js" />
    <include name="source\core\Const.js" />
    <include name="source\services\BusinessEntity.js" />
    <include name="source\ux\CodeMirror.js" />
    <include name="source\ux\Calendar.js" />
    <include name="source\ux\CalendarColorPaletteField.js" />
    <include name="source\core\Overrides.js" />
    <include name="source\core\Fx.js" />
    <include name="source\core\Viz.js" />
    <include name="source\plugins\ValidationStatus.js" />
    <include name="source\plugins\MonthPicker.js" />
    <include name="source\plugins\RapidSearch.js" />
    <include name="source\plugins\Flip.js" />
    <include name="source\core\Local.js" />
    <include name="source\widgets\silverlight\Silverlight.js" />
    <include name="source\widgets\silverlight\SilverlightComponent.js" />
    <include name="source\widgets\silverlight\Viewer.js" />
    <include name="source\widgets\silverlight\ChartViewer.js" />
    <include name="source\ux\RowLayout.js" />
    <include name="source\core\RgbColor.js" />
    <include name="source\core\AjaxNet.js" />
    <include name="source\services\Services.js" />
    <include name="source\util\SyncThread.js" />
    <include name="source\util\MessageBus.js" />
    <include name="source\util\Workflow.js" />
    <include name="source\util\Portal.js" />
    <include name="source\util\Location.js" />
    <include name="source\util\Chart.js" />
    <include name="source\util\IntelliSense.js" />
    <include name="source\util\Fullscreen.js" />
    <include name="source\util\SelectionManipulation.js" />
    <include name="source\widgets\form\Checkbox.js" />
    <include name="source\widgets\form\Action.js" />
    <include name="source\widgets\form\NumberField.js" />
    <include name="source\widgets\form\SpinnerField.js" />
    <include name="source\widgets\form\PrefixField.js" />
    <include name="source\core\DomQuery.js" />
    <include name="source\data\PagingStore.js" />
    <include name="source\data\JsonWriter.js" />
    <include name="source\data\XmlReader.js" />
    <include name="source\data\AdoXmlReader.js" />
    <include name="source\data\AdoNetXmlReader.js" />
    <include name="source\widgets\panel\Panel.js" />
    <include name="source\widgets\tabpanel\TabPanel.js" />
    <include name="source\ux\WriteStore.js" />
    <include name="source\ux\DateTime.js" />
    <include name="source\ux\GUID.js" />
    <include name="source\ux\JSONP.js" />
    <include name="source\ux\ScriptLoader.js" />
    <include name="source\ux\SuperboxSelect.js" />
    <include name="source\ux\MenuStore.js" />
    <include name="source\ux\MenuCheckItemIcon.js" />
    <include name="source\ux\GridSummary.js" />
    <include name="source\ux\ColorPicker.js" />
    <include name="source\ux\ColorPickerField.js" />
    <include name="source\ux\CompositeStringFilter.js" />
    <include name="source\ux\GenericFilter.js" />
    <include name="source\ux\ComboEnumFilter.js" />
    <include name="source\ux\TreecombomultiselectFilter.js" />
    <include name="source\ux\VerticalTabPanel.js" />
    <include name="source\ux\Ribbon.js" />
    <include name="source\ux\Notification.js" />
    <include name="source\ux\StatusBar.js" />
    <include name="source\state\RESTfulProvider.js" />
    <include name="source\ux\RowExpander.js" />
    <include name="source\ux\HtmlEditor.js" />
    <include name="source\ux\LinkButton.js" />
    <include name="source\state\TabPanel.js" />
    <include name="source\state\Tree.js" />
    <include name="source\data\AdoStore.js" />
    <include name="source\data\AdoNetStore.js" />
    <include name="source\data\WCFHttpProxy.js" />
    <include name="source\data\WCFSimpleStore.js" />
    <include name="source\widgets\store\AuthenticationProvider.js" />
    <include name="source\widgets\store\Authorization.js" />
    <include name="source\widgets\store\Organization.js" />
    <include name="source\widgets\store\Occupant.js" />
    <include name="source\widgets\store\LocalizationCulture.js" />
    <include name="source\widgets\store\AzManFilterSpatial.js" />
    <include name="source\widgets\store\AzManFilterAuthorization.js" />
    <include name="source\widgets\store\AzManFilterUserAuthorization.js" />
    <include name="source\widgets\store\AzManFilterChart.js" />
    <include name="source\widgets\store\AzManFilterAlarmDefinition.js" />
    <include name="source\widgets\store\AzManFilterPortalWindow.js" />
    <include name="source\widgets\store\AzManFilterReport.js" />
    <include name="source\widgets\store\AzManFilterDynamicDisplay.js" />
    <include name="source\widgets\store\AzmanFilterPortalTemplate.js" />
    <include name="source\widgets\store\AzmanFilterPlaylist.js" />
    <include name="source\widgets\store\AzmanFilterLink.js" />
    <include name="source\widgets\store\AzmanFilterChartScheduler.js" />
    <include name="source\widgets\store\PsetUsageName.js" />
    <include name="source\widgets\store\ListElement.js" />
    <include name="source\widgets\store\PsetList.js" />
    <include name="source\widgets\store\PsetAttributeHistorical.js" />
    <include name="source\widgets\store\Localization.js" />
    <include name="source\widgets\store\User.js" />
    <include name="source\widgets\store\UserFiltered.js" />
    <include name="source\widgets\store\Site.js" />
    <include name="source\widgets\store\ApplicationGroup.js" />
    <include name="source\widgets\store\ApplicationGroupAuthorization.js" />
    <include name="source\widgets\store\ApplicationGroupAzManRole.js" />
    <include name="source\widgets\store\AzManTask.js" />
    <include name="source\widgets\store\UserApplicationGroup.js" />
    <include name="source\widgets\store\ClassificationItemDefinition.js" />
    <include name="source\widgets\store\ClassificationDefinition.js" />
    <include name="source\widgets\store\ActionRequest.js" />
    <include name="source\widgets\store\Task.js" />
    <include name="source\widgets\store\Approval.js" />
    <include name="source\widgets\store\CalendarMonthName.js" />
    <include name="source\widgets\store\CalendarDayName.js" />
    <include name="source\widgets\store\CalendarFrequencyOccurrence.js" />
    <include name="source\widgets\store\CalendarEventCategory.js" />
    <include name="source\widgets\store\CalendarDefinition.js" />
    <include name="source\widgets\store\TimeZone.js" />
    <include name="source\widgets\store\Priority.js" />
    <include name="source\widgets\store\Meter.js" />
    <include name="source\widgets\store\MeterData.js" />
    <include name="source\widgets\store\MeterOperation.js" />
    <include name="source\widgets\store\Palette.js" />
    <include name="source\widgets\store\PaletteColor.js" />
    <include name="source\widgets\store\PlaylistPage.js" />
    <include name="source\widgets\store\Playlist.js" />
    <include name="source\widgets\store\Chart.js" />
    <include name="source\widgets\store\ChartKPI.js" />
    <include name="source\widgets\store\ChartSchedulerPortalWindow.js" />
    <include name="source\widgets\store\ChartSchedulerChart.js" />
    <include name="source\widgets\store\ChartScheduler.js" />
    <include name="source\widgets\store\PortalTemplatePortalWindow.js" />
    <include name="source\widgets\store\PortalTemplateFOLMembershipUser.js" />
    <include name="source\widgets\store\PortalTemplate.js" />
    <include name="source\widgets\store\DynamicDisplayColorTemplate.js" />
    <include name="source\widgets\store\DynamicDisplay.js" />
    <include name="source\widgets\store\DynamicDisplayImage.js" />
    <include name="source\widgets\store\EnergyCertificateCategory.js" />
    <include name="source\widgets\store\EnergyCertificate.js" />
    <include name="source\widgets\store\Enum.js" />
    <include name="source\widgets\store\AlarmDefinition.js" />
    <include name="source\widgets\store\AlarmDefinitionMeterAll.js" />
    <include name="source\widgets\store\AlarmDefinitionMeter.js" />
    <include name="source\widgets\store\AlarmDefinitionFilterSpatial.js" />
    <include name="source\widgets\store\AlarmDefinitionFilterMeterClassification.js" />
    <include name="source\widgets\store\AlarmInstance.js" />
    <include name="source\widgets\store\MeterAlarmInstance.js" />
    <include name="source\widgets\store\MeterDataAlarmInstance.js" />
    <include name="source\widgets\store\ChartPsetAttributeHistorical.js" />
    <include name="source\widgets\store\ChartLocalisation.js" />
    <include name="source\widgets\store\ChartClassificationLevel.js" />
    <include name="source\widgets\store\ChartMeter.js" />
    <include name="source\widgets\store\ChartMeterAll.js" />
    <include name="source\widgets\store\ChartKPIAzManRole.js" />
    <include name="source\widgets\store\DataSerie.js" />
    <include name="source\widgets\store\DataSeriePsetRatio.js" />
    <include name="source\widgets\store\DataSerieColorElement.js" />
    <include name="source\widgets\store\CalculatedSerie.js" />
    <include name="source\widgets\store\StatisticalSerie.js" />
    <include name="source\widgets\store\ChartAxis.js" />
    <include name="source\widgets\store\ChartFilterSpatialPset.js" />
    <include name="source\widgets\store\ChartFilterSpatialPsetLocation.js" />
    <include name="source\widgets\store\ChartFilterAlarmDefinitionClassification.js" />
    <include name="source\widgets\store\AlarmDefinitionFilterSpatialPset.js" />
    <include name="source\widgets\store\AlarmDefinitionFilterSpatialPsetLocation.js" />
    <include name="source\widgets\store\AlarmDefinitionChart.js" />
    <include name="source\widgets\store\AlarmTable.js" />
    <include name="source\widgets\store\AlarmTableFilterSpatial.js" />
    <include name="source\widgets\store\AlarmTableFilterAlarmDefinitionClassification.js" />
    <include name="source\widgets\store\AlarmTableAlarmDefinitionAll.js" />
    <include name="source\widgets\store\AlarmTableAlarmDefinition.js" />
    <include name="source\widgets\store\AlarmTableAlarmInstance.js" />
    <include name="source\widgets\store\MeterValidationRuleMeterAll.js" />
    <include name="source\widgets\store\MeterValidationRuleMeter.js" />
    <include name="source\widgets\store\MeterValidationRuleFilterSpatial.js" />
    <include name="source\widgets\store\MeterValidationRuleFilterMeterClassification.js" />
    <include name="source\widgets\store\MeterValidationRule.js" />
    <include name="source\widgets\store\MeterValidationRuleAlarmInstance.js" />
    <include name="source\widgets\store\ChartHistoricalAnalysis.js" />
    <include name="source\widgets\store\ChartMarker.js" />
    <include name="source\widgets\store\ChartAvailableDataSerieLocalId.js" />
    <include name="source\widgets\store\FontFamily.js" />
    <include name="source\widgets\store\WorkflowAssemblyDefinition.js" />
    <include name="source\widgets\store\Mail.js" />
    <include name="source\widgets\store\Link.js" />
    <include name="source\widgets\store\MailPriority.js" />
    <include name="source\widgets\store\MailClassificationItem.js" />
    <include name="source\widgets\store\MailLocation.js" />
    <include name="source\widgets\store\MailWorkflowActionElement.js" />
    <include name="source\widgets\store\LoginHistory.js" />
    <include name="source\widgets\store\LoginHistoryActive.js" />
    <include name="source\widgets\store\PortalWindow.js" />
    <include name="source\widgets\store\PortalTab.js" />
    <include name="source\widgets\store\PortalColumn.js" />
    <include name="source\widgets\store\Portlet.js" />
    <include name="source\widgets\store\DataPoint.js" />
    <include name="source\widgets\store\AzManOperation.js" />
    <include name="source\widgets\store\AzManTaskOperation.js" />
    <include name="source\widgets\store\EventLog.js" />
    <include name="source\widgets\store\ChartFilterSpatial.js" />
    <include name="source\widgets\store\ChartFilterMeterClassification.js" />
    <include name="source\widgets\store\ChartFilterEventLogClassification.js" />
    <include name="source\widgets\store\VirtualFile.js" />
    <include name="source\widgets\store\Audit.js" />
    <include name="source\widgets\store\DetailedAudit.js" />
    <include name="source\widgets\store\WeatherLocation.js" />
    <include name="source\widgets\store\WeatherLocationSearch.js" />
    <include name="source\widgets\store\WebFrame.js" />
    <include name="source\widgets\store\Map.js" />
    <include name="source\widgets\store\DrawingCanvas.js" />
    <include name="source\widgets\store\DrawingCanvasChart.js" />
    <include name="source\widgets\store\DrawingCanvasImage.js" />
    <include name="source\widgets\store\WorkflowActionElement.js" />
    <include name="source\widgets\store\MachineInstance.js" />
    <include name="source\widgets\store\Job.js" />
    <include name="source\widgets\store\JobSchedule.js" />
    <include name="source\widgets\store\MethodDefinition.js" />
    <include name="source\widgets\store\StruxureWareDataAcquisitionContainer.js" />
    <include name="source\widgets\store\StruxureWareDataAcquisitionEndpoint.js" />
    <include name="source\widgets\store\EWSDataAcquisitionContainer.js" />
    <include name="source\widgets\store\EWSDataAcquisitionEndpoint.js" />
    <include name="source\widgets\store\RestDataAcquisitionContainer.js" />
    <include name="source\widgets\store\RestDataAcquisitionEndpoint.js" />
    <include name="source\widgets\store\DataAcquisitionEndpoint.js" />
    <include name="source\widgets\store\MappingTask.js" />
    <include name="source\widgets\store\AzManFilterClassificationItem.js" />
    <include name="source\widgets\store\RestDataAcquisitionProvider.js" />
    <include name="source\widgets\store\ObjectClassificationItem.js" />
    <include name="source\widgets\store\Color.js" />
    <include name="source\widgets\store\TraceEntry.js" />
    <include name="source\widgets\store\TraceSummary.js" />
    <include name="source\widgets\store\ReportParameterValue.js" />
    <include name="source\widgets\store\Tenant.js" />
    <include name="source\widgets\store\DesktopBackground.js" />
    <include name="source\widgets\store\RapidSearch.js" />
    <include name="source\widgets\store\StepAlarmDefinition.js" />
    <include name="source\widgets\store\StepPlaylist.js" />
    <include name="source\widgets\store\StepPortalTemplate.js" />
    <include name="source\widgets\store\PsetDefinition.js" />
    <include name="source\widgets\store\AuditEntity.js" />
    <include name="source\widgets\store\EntityPortlet.js" />
    <include name="source\widgets\store\Algorithm.js" />
    <include name="source\widgets\store\ReportSubscription.js" />
    <include name="source\widgets\store\ChartAlgorithm.js" />
    <include name="source\widgets\store\CalendarEventCategoryTitle.js" />
    <include name="source\widgets\store\FlipCard.js" />
    <include name="source\widgets\store\FlipCardEntry.js" />
    <include name="source\widgets\store\MeterDataExportTaskMeter.js" />
    <include name="source\widgets\store\MeterDataExportTask.js" />
    <include name="source\widgets\store\AuditableEntity.js" />
    <include name="source\widgets\store\PortalTemplateFilterSpatial.js" />
    <include name="source\widgets\store\JobInstance.js" />
    <include name="source\widgets\store\AlgorithmInputDefinition.js" />
    <include name="source\widgets\store\AlgorithmInputValue.js" />
    <include name="source\widgets\store\EntityCount.js" />
    <include name="source\widgets\store\AzManFilterByLocation.js" />
    <include name="source\widgets\form\VTypes.js" />
    <include name="source\widgets\form\DateTime.js" />
    <include name="source\widgets\form\DisplayFormatField.js" />
    <include name="source\widgets\grid\PagingRowNumberer.js" />
    <include name="source\widgets\grid\GridPanel.js" />
    <include name="source\widgets\grid\PsetList.js" />
    <include name="source\widgets\grid\User.js" />
    <include name="source\widgets\grid\UserFiltered.js" />
    <include name="source\widgets\grid\ApplicationGroup.js" />
    <include name="source\widgets\grid\Localization.js" />
    <include name="source\widgets\grid\ZoningLegend.js" />
    <include name="source\widgets\grid\Occupant.js" />
    <include name="source\widgets\grid\ClassificationDefinition.js" />
    <include name="source\widgets\grid\ActionRequest.js" />
    <include name="source\widgets\grid\Task.js" />
    <include name="source\widgets\grid\Approval.js" />
    <include name="source\widgets\grid\CalendarEventCategory.js" />
    <include name="source\widgets\grid\CalendarDefinition.js" />
    <include name="source\widgets\grid\Priority.js" />
    <include name="source\widgets\grid\Meter.js" />
    <include name="source\widgets\grid\MeterData.js" />
    <include name="source\widgets\grid\MeterOperation.js" />
    <include name="source\widgets\grid\Palette.js" />
    <include name="source\widgets\grid\PaletteColor.js" />
    <include name="source\widgets\grid\Playlist.js" />
    <include name="source\widgets\grid\PlaylistPage.js" />
    <include name="source\widgets\grid\AlarmDefinition.js" />
    <include name="source\widgets\grid\AlarmInstance.js" />
    <include name="source\widgets\grid\AlarmTable.js" />
    <include name="source\widgets\grid\MeterValidationRule.js" />
    <include name="source\widgets\grid\Chart.js" />
    <include name="source\widgets\grid\ChartScheduler.js" />
    <include name="source\widgets\grid\ChartAxis.js" />
    <include name="source\widgets\grid\ChartPsetAttributeHistorical.js" />
    <include name="source\widgets\grid\FilterSpatialPset.js" />
    <include name="source\widgets\grid\FilterSpatialPsetLocation.js" />
    <include name="source\widgets\grid\FilterAzmanRole.js" />
    <include name="source\widgets\grid\ChartHistoricalAnalysis.js" />
    <include name="source\widgets\grid\ChartMarker.js" />
    <include name="source\widgets\grid\PortalTemplate.js" />
    <include name="source\widgets\grid\DataSerie.js" />
    <include name="source\widgets\grid\DataSeriePsetRatio.js" />
    <include name="source\widgets\grid\DataSerieColorElement.js" />
    <include name="source\widgets\grid\CalculatedSerie.js" />
    <include name="source\widgets\grid\StatisticalSerie.js" />
    <include name="source\widgets\grid\EnergyCertificateCategory.js" />
    <include name="source\widgets\grid\EnergyCertificate.js" />
    <include name="source\widgets\grid\DynamicDisplayColorTemplate.js" />
    <include name="source\widgets\grid\DynamicDisplay.js" />
    <include name="source\widgets\grid\DynamicDisplayImage.js" />
    <include name="source\widgets\grid\Mail.js" />
    <include name="source\widgets\grid\Link.js" />
    <include name="source\widgets\grid\LoginHistory.js" />
    <include name="source\widgets\grid\PortalWindow.js" />
    <include name="source\widgets\grid\PortalTab.js" />
    <include name="source\widgets\grid\PortalColumn.js" />
    <include name="source\widgets\grid\Portlet.js" />
    <include name="source\widgets\grid\Algorithm.js" />
    <include name="source\widgets\grid\DataPoint.js" />
    <include name="source\widgets\grid\AzManOperation.js" />
    <include name="source\widgets\grid\AzManTask.js" />
    <include name="source\widgets\grid\EventLog.js" />
    <include name="source\widgets\grid\FilterSpatial.js" />
    <include name="source\widgets\grid\FilterPortalWindow.js" />
    <include name="source\widgets\grid\FilterAlarmDefinition.js" />
    <include name="source\widgets\grid\FilterChart.js" />
    <include name="source\widgets\grid\FilterReport.js" />
    <include name="source\widgets\grid\FilterMeterClassification.js" />
    <include name="source\widgets\grid\FilterAlarmDefinitionClassification.js" />
    <include name="source\widgets\grid\FilterEventLog.js" />
    <include name="source\widgets\grid\FilterDynamicDisplay.js" />
    <include name="source\widgets\grid\FilterPortalTemplate.js" />
    <include name="source\widgets\grid\FilterPlaylist.js" />
    <include name="source\widgets\grid\FilterLink.js" />
    <include name="source\widgets\grid\FilterChartScheduler.js" />
    <include name="source\widgets\grid\VirtualFile.js" />
    <include name="source\widgets\grid\WeatherLocation.js" />
    <include name="source\widgets\grid\WebFrame.js" />
    <include name="source\widgets\grid\Map.js" />
    <include name="source\widgets\grid\DrawingCanvas.js" />
    <include name="source\widgets\grid\DrawingCanvasChart.js" />
    <include name="source\widgets\grid\DrawingCanvasImage.js" />
    <include name="source\widgets\grid\PsetAttributeHistorical.js" />
    <include name="source\widgets\grid\WorkflowActionElement.js" />
    <include name="source\widgets\grid\MachineInstance.js" />
    <include name="source\widgets\grid\Schedule.js" />
    <include name="source\widgets\grid\Step.js" />
    <include name="source\widgets\grid\Job.js" />
    <include name="source\widgets\grid\StruxureWareDataAcquisitionContainer.js" />
    <include name="source\widgets\grid\StruxureWareDataAcquisitionEndpoint.js" />
    <include name="source\widgets\grid\EWSDataAcquisitionContainer.js" />
    <include name="source\widgets\grid\EWSDataAcquisitionEndPoint.js" />
    <include name="source\widgets\grid\RestDataAcquisitionContainer.js" />
    <include name="source\widgets\grid\MappingTask.js" />
    <include name="source\widgets\grid\RestDataAcquisitionEndpoint.js" />
    <include name="source\widgets\grid\FilterClassificationItem.js" />
    <include name="source\widgets\grid\TraceEntry.js" />
    <include name="source\widgets\grid\TraceSummary.js" />
    <include name="source\widgets\grid\ReportSubscription.js" />
    <include name="source\widgets\grid\ChartAlgorithm.js" />
    <include name="source\widgets\grid\FlipCard.js" />
    <include name="source\widgets\grid\FlipCardEntry.js" />
    <include name="source\widgets\grid\MeterDataExportTask.js" />
    <include name="source\widgets\grid\AuditEntity.js" />
    <include name="source\widgets\grid\EntityCount.js" />
    <include name="source\widgets\grid\JobInstance.js" />
    <include name="source\widgets\grid\AlgorithmInputDefinition.js" />
    <include name="source\widgets\grid\AlgorithmInputValue.js" />
    <include name="source\widgets\grid\AzManFilter.js" />
    <include name="source\widgets\grid\CheckboxSelectionModel.js" />
    <include name="source\widgets\dataview\DataView.js" />
    <include name="source\widgets\dataview\Chart.js" />
    <include name="source\widgets\dataview\DynamicDisplay.js" />
    <include name="source\widgets\dataview\DynamicDisplayImage.js" />
    <include name="source\widgets\dataview\Palette.js" />
    <include name="source\widgets\grid\Tenant.js" />
    <include name="source\widgets\button\ButtonGroup.js" />
    <include name="source\widgets\tree\TreeSorter.js" />
    <include name="source\widgets\tree\TreeLoaderWCF.js" />
    <include name="source\widgets\tree\TreePanel.js" />
    <include name="source\widgets\tree\Spatial.js" />
    <include name="source\widgets\tree\User.js" />
    <include name="source\widgets\tree\ApplicationGroup.js" />
    <include name="source\widgets\tree\AzManFilter.js" />
    <include name="source\widgets\tree\AzManRole.js" />
    <include name="source\widgets\tree\AzManOperation.js" />
    <include name="source\widgets\tree\Pset.js" />
    <include name="source\widgets\tree\Report.js" />
    <include name="source\widgets\tree\Portlet.js" />
    <include name="source\widgets\tree\ClassificationItem.js" />
    <include name="source\widgets\tree\MeterClassification.js" />
    <include name="source\widgets\tree\AlarmDefinitionClassification.js" />
    <include name="source\widgets\tree\Organization.js" />
    <include name="source\widgets\tree\DataAcquisition.js" />
    <include name="source\widgets\toolbar\TabToolbar.js" />
    <include name="source\widgets\LoadingIndicator.js" />
    <include name="source\widgets\combo\ComboBox.js" />
    <include name="source\widgets\combo\ClearableComboBox.js" />
    <include name="source\widgets\form\ColorPalette.js" />
    <include name="source\widgets\form\Search.js" />
    <include name="source\widgets\form\FormPanel.js" />
    <include name="source\widgets\combo\TreeCombo.js" />
    <include name="source\widgets\combo\TreeComboMultiSelect.js" />
    <include name="source\widgets\combo\AuthenticationProvider.js" />
    <include name="source\widgets\combo\LocalizationCulture.js" />
    <include name="source\widgets\combo\Css.js" />
    <include name="source\widgets\combo\CalendarEventCategory.js" />
    <include name="source\widgets\combo\Occupant.js" />
    <include name="source\widgets\combo\Meter.js" />
    <include name="source\widgets\combo\MeterOperation.js" />
    <include name="source\widgets\combo\PortalTab.js" />
    <include name="source\widgets\combo\PortalColumn.js" />
    <include name="source\widgets\combo\Palette.js" />
    <include name="source\widgets\combo\Playlist.js" />
    <include name="source\widgets\combo\EnergyCertificate.js" />
    <include name="source\widgets\combo\DynamicDisplayColorTemplate.js" />
    <include name="source\widgets\combo\DynamicDisplay.js" />
    <include name="source\widgets\combo\DynamicDisplayImage.js" />
    <include name="source\widgets\combo\ChartAvailableDataSerieLocalId.js" />
    <include name="source\widgets\combo\ChartClassificationLevel.js" />
    <include name="source\widgets\combo\PsetUsageName.js" />
    <include name="source\widgets\combo\ListElement.js" />
    <include name="source\widgets\combo\FontFamily.js" />
    <include name="source\widgets\combo\CalendarDayName.js" />
    <include name="source\widgets\combo\CalendarMonthName.js" />
    <include name="source\widgets\combo\AzManTask.js" />
    <include name="source\widgets\combo\Link.js" />
    <include name="source\widgets\combo\CalendarFrequencyOccurrence.js" />
    <include name="source\widgets\combo\TreeComboSpatial.js" />
    <include name="source\widgets\combo\TreeComboOrganization.js" />
    <include name="source\widgets\combo\TreeComboClassificationItemWork.js" />
    <include name="source\widgets\combo\TreeComboClassificationItemWorktype.js" />
    <include name="source\widgets\combo\TreeComboClassificationItemOrganization.js" />
    <include name="source\widgets\combo\TreeComboClassificationItemMeter.js" />
    <include name="source\widgets\combo\TreeComboClassificationItemEventLog.js" />
    <include name="source\widgets\combo\TreeComboClassificationItemAlarmDefinition.js" />
    <include name="source\widgets\combo\TreeComboClassificationItemEquipment.js" />
    <include name="source\widgets\combo\TreeComboClassificationItemMail.js" />
    <include name="source\widgets\combo\TreeComboClassificationItemKPI.js" />
    <include name="source\widgets\combo\TreeComboPset.js" />
    <include name="source\widgets\combo\TreeComboPortlet.js" />
    <include name="source\widgets\combo\TreeComboMultiSelectSpatial.js" />
    <include name="source\widgets\combo\TreeComboMultiSelectOrganization.js" />
    <include name="source\widgets\combo\TreeComboMultiSelectClassificationItemWorktype.js" />
    <include name="source\widgets\combo\TreeComboMultiSelectClassificationItemWork.js" />
    <include name="source\widgets\combo\TreeComboMultiSelectClassificationItemOrganization.js" />
    <include name="source\widgets\combo\TreeComboMultiSelectClassificationItemMeter.js" />
    <include name="source\widgets\combo\TreeComboMultiSelectClassificationItemEventLog.js" />
    <include name="source\widgets\combo\TreeComboMultiSelectClassificationItemAlarmDefinition.js" />
    <include name="source\widgets\combo\TimeZone.js" />
    <include name="source\widgets\combo\Enum.js" />
    <include name="source\widgets\ComboTheme.js" />
    <include name="source\widgets\ComboLangue.js" />
    <include name="source\widgets\combo\Localization.js" />
    <include name="source\widgets\combo\Priority.js" />
    <include name="source\widgets\combo\WorkflowAssemblyDefinition.js" />
    <include name="source\widgets\combo\ChartAxis.js" />
    <include name="source\widgets\combo\Chart.js" />
    <include name="source\widgets\combo\WeatherLocationSearch.js" />
    <include name="source\widgets\combo\UserFiltered.js" />
    <include name="source\widgets\combo\WorkflowActionElement.js" />
    <include name="source\widgets\combo\MethodDefinition.js" />
    <include name="source\widgets\combo\AlgorithmInputDefinition.js" />
    <include name="source\widgets\combo\DataAcquisitionEndpoint.js" />
    <include name="source\widgets\combo\RestDataAcquisitionProvider.js" />
    <include name="source\widgets\combo\Color.js" />
    <include name="source\widgets\combo\ReportParameterValue.js" />
    <include name="source\widgets\combo\DesktopBackground.js" />
    <include name="source\widgets\combo\RapidSearch.js" />
    <include name="source\widgets\combo\Algorithm.js" />
    <include name="source\widgets\combo\CalendarEventCategoryTitle.js" />
    <include name="source\widgets\combo\VirtualFileType.js" />
    <include name="source\widgets\combo\MappingTask.js" />
    <include name="source\widgets\combo\AuditableEntity.js" />
    <include name="source\widgets\combo\TreeComboAzmanFilter.js" />
    <include name="source\widgets\Desktop.js" />
    <include name="source\plugins\GridExport.js" />
    <include name="source\plugins\GridValidator.js" />
    <include name="source\plugins\RemoteValidator.js" />
    <include name="source\plugins\DragDropTabs.js" />
    <include name="source\plugins\ViewerSlider.js" />
    <include name="source\plugins\IntelliSense.js" />
    <include name="source\widgets\window\ForgotPasswordDialog.js" />
    <include name="source\widgets\window\LoginDialog.js" />
    <include name="source\widgets\window\AboutBox.js" />
    <include name="source\widgets\window\Translator.js" />
    <include name="source\widgets\window\Occupant.js" />
    <include name="source\widgets\window\ImportXml.js" />
    <include name="source\widgets\window\AzmanImportXml.js" />
    <include name="source\widgets\window\ChangePassword.js" />
    <include name="source\widgets\window\LoginAsUser.js" />
    <include name="source\widgets\portal\PortalWindow.js" />
    <include name="source\widgets\portal\PortalPanel.js" />
    <include name="source\widgets\portal\PortalTab.js" />
    <include name="source\widgets\portal\PortalColumn.js" />
    <include name="source\widgets\portal\Portlet.js" />
    <include name="source\widgets\portal\PortletWeather.js" />
    <include name="source\widgets\portal\PortletWebFrame.js" />
    <include name="source\widgets\portal\PortletMap.js" />
    <include name="source\widgets\portal\PortletAlarmTable.js" />
    <include name="source\widgets\portal\PortletDrawingCanvas.js" />
    <include name="source\widgets\portal\PortletFlipCard.js" />
    <include name="source\widgets\portal\PortletWindow.js" />
    <include name="source\config\Columns.js" />
    <include name="source\widgets\form\CompositeField.js" />
    <include name="source\widgets\form\FileUploadField.js" />
    <include name="source\widgets\form\ImageUploadField.js" />
    <include name="source\widgets\form\UserInfo.js" />
    <include name="source\widgets\form\Occupant.js" />
    <include name="source\widgets\form\User.js" />
    <include name="source\widgets\form\AzManFilter.js" />
    <include name="source\widgets\form\AzManRole.js" />
    <include name="source\widgets\form\AzManTask.js" />
    <include name="source\widgets\form\AzManOperation.js" />
    <include name="source\widgets\form\PsetDefinition.js" />
    <include name="source\widgets\form\PsetAttributeDefinition.js" />
    <include name="source\widgets\form\PsetList.js" />
    <include name="source\widgets\form\PsetAttributeHistorical.js" />
    <include name="source\widgets\form\PsetAttributeHistoricalEndpoint.js" />
    <include name="source\widgets\form\ClassificationItem.js" />
    <include name="source\widgets\form\Site.js" />
    <include name="source\widgets\form\Building.js" />
    <include name="source\widgets\form\BuildingStorey.js" />
    <include name="source\widgets\form\Space.js" />
    <include name="source\widgets\form\Furniture.js" />
    <include name="source\widgets\form\Localization.js" />
    <include name="source\widgets\form\ApplicationGroup.js" />
    <include name="source\widgets\form\Organization.js" />
    <include name="source\widgets\form\ClassificationDefinition.js" />
    <include name="source\widgets\form\CalendarEventCategory.js" />
    <include name="source\widgets\form\CalendarDefinition.js" />
    <include name="source\widgets\form\Priority.js" />
    <include name="source\widgets\form\ActionRequest.js" />
    <include name="source\widgets\form\Meter.js" />
    <include name="source\widgets\form\MeterData.js" />
    <include name="source\widgets\form\MeterDataGenerate.js" />
    <include name="source\widgets\form\MeterDataBatch.js" />
    <include name="source\widgets\form\MeterDataOffsetByDateRange.js" />
    <include name="source\widgets\form\MeterDataDeleteByDateRange.js" />
    <include name="source\widgets\form\MeterOperation.js" />
    <include name="source\widgets\form\Palette.js" />
    <include name="source\widgets\form\PaletteColor.js" />
    <include name="source\widgets\form\Playlist.js" />
    <include name="source\widgets\form\PlaylistPage.js" />
    <include name="source\widgets\form\EnergyCertificateCategory.js" />
    <include name="source\widgets\form\EnergyCertificate.js" />
    <include name="source\widgets\form\AlarmDefinition.js" />
    <include name="source\widgets\form\AlarmDefinitionWizard.js" />
    <include name="source\widgets\form\AlarmInstance.js" />
    <include name="source\widgets\form\AlarmTable.js" />
    <include name="source\widgets\form\MeterValidationRule.js" />
    <include name="source\widgets\form\ChartFormPanel.js" />
    <include name="source\widgets\form\Chart.js" />
    <include name="source\widgets\form\ChartWizard.js" />
    <include name="source\widgets\form\ChartScheduler.js" />
    <include name="source\widgets\form\DynamicDisplayColorTemplate.js" />
    <include name="source\widgets\form\DynamicDisplay.js" />
    <include name="source\widgets\form\DynamicDisplayImage.js" />
    <include name="source\widgets\form\DataSerie.js" />
    <include name="source\widgets\form\DataSeriePsetRatio.js" />
    <include name="source\widgets\form\DataSerieColorElement.js" />
    <include name="source\widgets\form\PortalTemplate.js" />
    <include name="source\widgets\form\CalculatedSerie.js" />
    <include name="source\widgets\form\StatisticalSerie.js" />
    <include name="source\widgets\form\ChartPsetAttributeHistorical.js" />
    <include name="source\widgets\form\ChartAxis.js" />
    <include name="source\widgets\form\FilterSpatialPset.js" />
    <include name="source\widgets\form\ChartHistoricalAnalysis.js" />
    <include name="source\widgets\form\ChartMarker.js" />
    <include name="source\widgets\form\Mail.js" />
    <include name="source\widgets\form\Link.js" />
    <include name="source\widgets\form\PortalWindow.js" />
    <include name="source\widgets\form\PortalTab.js" />
    <include name="source\widgets\form\PortalColumn.js" />
    <include name="source\widgets\form\Portlet.js" />
    <include name="source\widgets\form\EventLog.js" />
    <include name="source\widgets\form\VirtualFile.js" />
    <include name="source\widgets\form\Task.js" />
    <include name="source\widgets\form\WeatherLocation.js" />
    <include name="source\widgets\form\WebFrame.js" />
    <include name="source\widgets\form\Map.js" />
    <include name="source\widgets\form\DrawingCanvas.js" />
    <include name="source\widgets\form\DrawingCanvasChart.js" />
    <include name="source\widgets\form\Schedule.js" />
    <include name="source\widgets\form\Step.js" />
    <include name="source\widgets\form\Job.js" />
    <include name="source\widgets\form\MachineInstance.js" />
    <include name="source\widgets\form\DrawingCanvasImage.js" />
    <include name="source\widgets\form\StruxureWareDataAcquisitionContainer.js" />
    <include name="source\widgets\form\EWSDataAcquisitionContainer.js" />
    <include name="source\widgets\form\RestDataAcquisitionContainer.js" />
    <include name="source\widgets\form\MappingTask.js" />
    <include name="source\widgets\form\RestDataAcquisitionEndpoint.js" />
    <include name="source\widgets\form\ActionRequestWizard.js" />
    <include name="source\widgets\form\Tenant.js" />
    <include name="source\widgets\form\Algorithm.js" />
    <include name="source\widgets\form\Logo.js" />
    <include name="source\widgets\form\ReportSubscriptionSchedule.js" />
    <include name="source\widgets\form\ReportSubscription.js" />
    <include name="source\widgets\form\ChartAlgorithm.js" />
    <include name="source\widgets\form\FlipCard.js" />
    <include name="source\widgets\form\FlipCardEntry.js" />
    <include name="source\widgets\form\MeterDataExportTask.js" />
    <include name="source\widgets\form\AuditEntity.js" />
    <include name="source\widgets\form\AlgorithmInputDefinition.js" />
    <include name="source\widgets\form\AlgorithmInputValue.js" />
    <include name="source\widgets\form\MonthYear.js" />
    <include name="source\widgets\viewer\Image.js" />
    <include name="source\widgets\chart\ChartImage.js" />
    <include name="source\widgets\chart\DynamicDisplayImage.js" />
    <include name="source\widgets\chart\DynamicDisplayImageImage.js" />
    <include name="source\widgets\chart\PortalTabImage.js" />
    <include name="source\widgets\chart\HighCharts.js" />
    <include name="source\widgets\chart\Sencha.js" />
    <include name="source\widgets\chart\Viewer.js" />
    <include name="source\widgets\chart\CustomGrid.js" />
    <include name="source\widgets\chart\Html.js" />
    <include name="source\widgets\panel\Occupant.js" />
    <include name="source\widgets\workflow\Viewer.js" />
    <include name="source\widgets\panel\Localization.js" />
    <include name="source\widgets\panel\User.js" />
    <include name="source\widgets\panel\ApplicationGroup.js" />
    <include name="source\widgets\panel\ManageBuildingStorey.js" />
    <include name="source\widgets\panel\ClassificationDefinition.js" />
    <include name="source\widgets\panel\ActionRequest.js" />
    <include name="source\widgets\panel\ActionRequestSpecific.js" />
    <include name="source\widgets\panel\CalendarEventCategory.js" />
    <include name="source\widgets\panel\CalendarDefinition.js" />
    <include name="source\widgets\panel\CalendarLocation.js" />
    <include name="source\widgets\panel\CalendarTask.js" />
    <include name="source\widgets\panel\Priority.js" />
    <include name="source\widgets\panel\Meter.js" />
    <include name="source\widgets\panel\MeterData.js" />
    <include name="source\widgets\panel\Palette.js" />
    <include name="source\widgets\panel\Playlist.js" />
    <include name="source\widgets\panel\EnergyCertificate.js" />
    <include name="source\widgets\panel\AlarmDefinition.js" />
    <include name="source\widgets\panel\MeterValidationRule.js" />
    <include name="source\widgets\panel\Chart.js" />
    <include name="source\widgets\panel\ChartScheduler.js" />
    <include name="source\widgets\panel\DynamicDisplay.js" />
    <include name="source\widgets\panel\Mail.js" />
    <include name="source\widgets\panel\Link.js" />
    <include name="source\widgets\panel\LoginHistory.js" />
    <include name="source\widgets\panel\LoginHistoryActive.js" />
    <include name="source\widgets\panel\PortalWindow.js" />
    <include name="source\widgets\panel\PortalTemplate.js" />
    <include name="source\widgets\panel\AzManOperation.js" />
    <include name="source\widgets\panel\AzManTask.js" />
    <include name="source\widgets\panel\EventLog.js" />
    <include name="source\widgets\panel\VirtualFile.js" />
    <include name="source\widgets\panel\ReportViewer.js" />
    <include name="source\widgets\panel\WeatherLocation.js" />
    <include name="source\widgets\panel\WebFrame.js" />
    <include name="source\widgets\panel\Map.js" />
    <include name="source\widgets\panel\DrawingCanvas.js" />
    <include name="source\widgets\panel\MachineInstance.js" />
    <include name="source\widgets\panel\Job.js" />
    <include name="source\widgets\panel\RestDataAcquisitionContainer.js" />
    <include name="source\widgets\panel\StruxurewareDataAcquisitionContainer.js" />
    <include name="source\widgets\panel\EWSDataAcquisitionContainer.js" />
    <include name="source\widgets\panel\RESTDataAcquisitionContainer.js" />
    <include name="source\widgets\panel\MeterDataBatch.js" />
    <include name="source\widgets\panel\AlarmTable.js" />
    <include name="source\widgets\panel\DynamicDisplayImage.js" />
    <include name="source\widgets\panel\DynamicDisplayColorTemplate.js" />
    <include name="source\widgets\panel\AzManRole.js" />
    <include name="source\widgets\panel\TraceEntry.js" />
    <include name="source\widgets\panel\TraceSummary.js" />
    <include name="source\widgets\panel\Tenant.js" />
    <include name="source\widgets\panel\Algorithm.js" />
    <include name="source\widgets\panel\FlipCard.js" />
    <include name="source\widgets\panel\AuditEntity.js" />
    <include name="source\widgets\panel\AzManFilter.js" />
    <include name="source\widgets\form\UserPortalTemplate.js" />
    <include name="source\widgets\desktop\Desktop.js" />
    <include name="source\widgets\desktop\TaskBar.js" />
    <include name="source\widgets\desktop\StartMenu.js" />
    <include name="source\widgets\desktop\IconManager.js" />
    <include name="source\widgets\viewport\MainViewPort.js" />
    <include name="source\rtl\rtl.js" />
    <include name="source\widgets\App.js" />
  </target>
  <file name="source\core\DomQuery.js" path="core" />
  <file name="source\core\Viz.js" path="core" />
  <file name="source\data\AdoNetStore.js" path="data" />
  <file name="source\data\AdoNetXmlReader.js" path="data" />
  <file name="source\data\AdoStore.js" path="data" />
  <file name="source\data\AdoXmlReader.js" path="data" />
  <file name="source\data\XmlReader.js" path="data" />
  <file name="source\util\MessageBus.js" path="util" />
  <file name="source\core\Localize.js" path="core" />
  <file name="source\widgets\LoginDialog.js" path="widgets" />
  <file name="source\services\Services.js" path="services" />
  <file name="source\core\AjaxNet.js" path="core" />
  <file name="source\widgets\WCFTreeLoader.js" path="widgets" />
  <file name="source\data\WCFHttpProxy.js" path="data" />
  <file name="source\data\WCFSimpleStore.js" path="data" />
  <file name="source\core\Overrides.js" path="core" />
  <file name="source\plugins\GridExport.js" path="plugins" />
  <file name="source\data\JsonWriter.js" path="data" />
  <file name="source\config\MainViewPort.js" path="config" />
  <file name="source\core\Fx.js" path="core" />
  <file name="source\services\BusinessEntity.js" path="services" />
  <file name="source\util\SyncThread.js" path="util" />
  <file name="source\util\SelectionManipulation.js" path="util" />
  <file name="source\widgets\container\SearchOccupant.js" path="widgets\container" />
  <file name="source\plugins\GridValidator.js" path="plugins" />
  <file name="source\widgets\form\Role.js" path="widgets\form" />
  <file name="source\widgets\misc\ColorPalette.js" path="widgets\misc" />
  <file name="source\core\RgbColor.js" path="core" />
  <file name="source\core\Local.js" path="core" />
  <file name="source\plugins\ViewerSlider.js" path="plugins" />
  <file name="source\plugins\DragDropTabs.js" path="plugins" />
  <file name="source\widgets\grid\Portal.js" path="widgets\grid" />
  <file name="source\plugins\RemoteValidator.js" path="plugins" />
  <file name="source\widgets\store\FilterSpatial.js" path="widgets\store" />
  <file name="source\widgets\form\TestDateTime.js" path="widgets\form" />
  <file name="source\widgets\grid\TestDateTime.js" path="widgets\grid" />
  <file name="source\widgets\store\TestDateTime.js" path="widgets\store" />
  <file name="source\config\Columns.js" path="config" />
  <file name="source\core\Const.js" path="core" />
  <file name="source\rtl\rtl.js" path="rtl" />
  <file name="source\widgets\store\MailWorkflowEvents.js" path="widgets\store" />
  <file name="source\widgets\panel\RestDataAcquisitionContainer.js" path="widgets\panel" />
  <file name="source\plugins\ValidationStatus.js" path="plugins" />
  <file name="source\util\Workflow.js" path="util" />
  <file name="source\state\TabPanel.js" path="ux" />
  <file name="source\state\Tree.js" path="ux" />
  <file name="source\util\Location.js" path="util" />
  <file name="source\plugins\MonthPicker.js" path="plugins" />
  <file name="source\state\RESTfulProvider.js" path="state" />
  <file name="source\util\Portal.js" path="util" />
  <file name="source\util\Chart.js" path="util" />
  <file name="source\services\AssemblyVersion.js" path="services" />
  <file name="source\plugins\IntelliSense.js" path="plugins" />
  <file name="source\plugins\RapidSearch.js" path="plugins" />
  <file name="source\util\IntelliSense.js" path="util" />
  <file name="source\data\PagingStore.js" path="data" />
  <file name="source\widgets\store\LocationFiltered.js" path="widgets\store" />
  <file name="source\widgets\grid\LocationFiltered.js" path="widgets\grid" />
  <file name="source\widgets\combo\LocationFiltered.js" path="widgets\combo" />
  <file name="source\ux\Calendar.js" path="ux" />
  <file name="source\ux\CalendarColorPaletteField.js" path="ux" />
  <file name="source\ux\ColorPicker.js" path="ux" />
  <file name="source\ux\ColorPickerField.js" path="ux" />
  <file name="source\ux\ComboEnumFilter.js" path="ux" />
  <file name="source\ux\CompositeStringFilter.js" path="ux" />
  <file name="source\ux\DateTime.js" path="ux" />
  <file name="source\ux\GenericFilter.js" path="ux" />
  <file name="source\ux\GridSummary.js" path="ux" />
  <file name="source\ux\GUID.js" path="ux" />
  <file name="source\ux\HtmlEditor.js" path="ux" />
  <file name="source\ux\JSONP.js" path="ux" />
  <file name="source\ux\MenuCheckItemIcon.js" path="ux" />
  <file name="source\ux\MenuStore.js" path="ux" />
  <file name="source\ux\Notification.js" path="ux" />
  <file name="source\ux\Ribbon.js" path="ux" />
  <file name="source\ux\RowExpander.js" path="ux" />
  <file name="source\ux\RowLayout.js" path="ux" />
  <file name="source\ux\ScriptLoader.js" path="ux" />
  <file name="source\ux\StatusBar.js" path="ux" />
  <file name="source\ux\SuperboxSelect.js" path="ux" />
  <file name="source\ux\TreecombomultiselectFilter.js" path="ux" />
  <file name="source\ux\VerticalTabPanel.js" path="ux" />
  <file name="source\ux\WriteStore.js" path="ux" />
  <file name="source\ux\CodeMirror.js" path="ux" />
  <file name="source\widgets\button\ButtonGroup.js" path="widgets\button" />
  <file name="source\widgets\chart\ChartImage.js" path="widgets\chart" />
  <file name="source\widgets\chart\CustomGrid.js" path="widgets\chart" />
  <file name="source\widgets\chart\DynamicDisplayImage.js" path="widgets\chart" />
  <file name="source\widgets\chart\DynamicDisplayImageImage.js" path="widgets\chart" />
  <file name="source\widgets\chart\HighCharts.js" path="widgets\chart" />
  <file name="source\widgets\chart\PortalTabImage.js" path="widgets\chart" />
  <file name="source\widgets\chart\Sencha.js" path="widgets\chart" />
  <file name="source\widgets\chart\Viewer.js" path="widgets\chart" />
  <file name="source\widgets\chart\Zing.js" path="widgets\chart" />
  <file name="source\widgets\combo\AuthenticationProvider.js" path="widgets\combo" />
  <file name="source\widgets\combo\CalendarDayName.js" path="widgets\combo" />
  <file name="source\widgets\combo\CalendarEventCategory.js" path="widgets\combo" />
  <file name="source\widgets\combo\CalendarFrequencyOccurrence.js" path="widgets\combo" />
  <file name="source\widgets\combo\CalendarMonthName.js" path="widgets\combo" />
  <file name="source\widgets\combo\AzManTask.js" path="widgets\combo" />
  <file name="source\widgets\combo\Link.js" path="widgets\combo" />
  <file name="source\widgets\combo\Chart.js" path="widgets\combo" />
  <file name="source\widgets\combo\ChartAvailableDataSerieLocalId.js" path="widgets\combo" />
  <file name="source\widgets\combo\ChartAxis.js" path="widgets\combo" />
  <file name="source\widgets\combo\ChartClassificationLevel.js" path="widgets\combo" />
  <file name="source\widgets\combo\ClearableComboBox.js" path="widgets\combo" />
  <file name="source\widgets\combo\Color.js" path="widgets\combo" />
  <file name="source\widgets\combo\ComboBox.js" path="widgets\combo" />
  <file name="source\widgets\combo\Css.js" path="widgets\combo" />
  <file name="source\widgets\combo\DataAcquisitionEndpoint.js" path="widgets\combo" />
  <file name="source\widgets\combo\DesktopBackground.js" path="widgets\combo" />
  <file name="source\widgets\combo\DynamicDisplay.js" path="widgets\combo" />
  <file name="source\widgets\combo\DynamicDisplayColorTemplate.js" path="widgets\combo" />
  <file name="source\widgets\combo\DynamicDisplayImage.js" path="widgets\combo" />
  <file name="source\widgets\combo\EnergyCertificate.js" path="widgets\combo" />
  <file name="source\widgets\combo\Enum.js" path="widgets\combo" />
  <file name="source\widgets\combo\FontFamily.js" path="widgets\combo" />
  <file name="source\widgets\combo\ListElement.js" path="widgets\combo" />
  <file name="source\widgets\combo\Localization.js" path="widgets\combo" />
  <file name="source\widgets\combo\LocalizationCulture.js" path="widgets\combo" />
  <file name="source\widgets\combo\Meter.js" path="widgets\combo" />
  <file name="source\widgets\combo\MeterOperation.js" path="widgets\combo" />
  <file name="source\widgets\combo\MethodDefinition.js" path="widgets\combo" />
  <file name="source\widgets\combo\Occupant.js" path="widgets\combo" />
  <file name="source\widgets\combo\Palette.js" path="widgets\combo" />
  <file name="source\widgets\combo\Playlist.js" path="widgets\combo" />
  <file name="source\widgets\combo\PortalColumn.js" path="widgets\combo" />
  <file name="source\widgets\combo\PortalTab.js" path="widgets\combo" />
  <file name="source\widgets\combo\Priority.js" path="widgets\combo" />
  <file name="source\widgets\combo\PsetUsageName.js" path="widgets\combo" />
  <file name="source\widgets\combo\RapidSearch.js" path="widgets\combo" />
  <file name="source\widgets\combo\ReportParameterValue.js" path="widgets\combo" />
  <file name="source\widgets\combo\RestDataAcquisitionProvider.js" path="widgets\combo" />
  <file name="source\widgets\combo\Algorithm.js" path="widgets\combo" />
  <file name="source\widgets\combo\TimeZone.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeCombo.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboClassificationItemAlarmDefinition.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboClassificationItemEquipment.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboClassificationItemEventLog.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboClassificationItemKPI.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboClassificationItemMail.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboClassificationItemMeter.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboClassificationItemOrganization.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboClassificationItemWork.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboClassificationItemWorktype.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboMultiSelect.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboMultiSelectClassificationItemAlarmDefinition.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboMultiSelectClassificationItemEventLog.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboMultiSelectClassificationItemMeter.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboMultiSelectClassificationItemOrganization.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboMultiSelectClassificationItemWork.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboMultiSelectClassificationItemWorktype.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboMultiSelectOrganization.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboMultiSelectSpatial.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboOrganization.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboPortlet.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboPset.js" path="widgets\combo" />
  <file name="source\widgets\combo\TreeComboSpatial.js" path="widgets\combo" />
  <file name="source\widgets\combo\UserFiltered.js" path="widgets\combo" />
  <file name="source\widgets\combo\WeatherLocationSearch.js" path="widgets\combo" />
  <file name="source\widgets\combo\WorkflowActionElement.js" path="widgets\combo" />
  <file name="source\widgets\combo\WorkflowAssemblyDefinition.js" path="widgets\combo" />
  <file name="source\widgets\dataview\Chart.js" path="widgets\dataview" />
  <file name="source\widgets\dataview\DataView.js" path="widgets\dataview" />
  <file name="source\widgets\dataview\DynamicDisplay.js" path="widgets\dataview" />
  <file name="source\widgets\dataview\DynamicDisplayImage.js" path="widgets\dataview" />
  <file name="source\widgets\dataview\Palette.js" path="widgets\dataview" />
  <file name="source\widgets\desktop\Desktop.js" path="widgets\desktop" />
  <file name="source\widgets\desktop\IconManager.js" path="widgets\desktop" />
  <file name="source\widgets\desktop\StartMenu.js" path="widgets\desktop" />
  <file name="source\widgets\desktop\TaskBar.js" path="widgets\desktop" />
  <file name="source\widgets\form\Action.js" path="widgets\form" />
  <file name="source\widgets\form\ActionRequest.js" path="widgets\form" />
  <file name="source\widgets\form\ActionRequestWizard.js" path="widgets\form" />
  <file name="source\widgets\form\AlarmDefinition.js" path="widgets\form" />
  <file name="source\widgets\form\AlarmDefinitionWizard.js" path="widgets\form" />
  <file name="source\widgets\form\AlarmInstance.js" path="widgets\form" />
  <file name="source\widgets\form\AlarmTable.js" path="widgets\form" />
  <file name="source\widgets\form\ApplicationGroup.js" path="widgets\form" />
  <file name="source\widgets\form\AzManFilter.js" path="widgets\form" />
  <file name="source\widgets\form\AzManOperation.js" path="widgets\form" />
  <file name="source\widgets\form\AzManRole.js" path="widgets\form" />
  <file name="source\widgets\form\AzManTask.js" path="widgets\form" />
  <file name="source\widgets\form\Building.js" path="widgets\form" />
  <file name="source\widgets\form\BuildingStorey.js" path="widgets\form" />
  <file name="source\widgets\form\CalculatedSerie.js" path="widgets\form" />
  <file name="source\widgets\form\CalendarDefinition.js" path="widgets\form" />
  <file name="source\widgets\form\CalendarEventCategory.js" path="widgets\form" />
  <file name="source\widgets\form\ChartFormPanel.js" path="widgets\form" />
  <file name="source\widgets\form\Chart.js" path="widgets\form" />
  <file name="source\widgets\form\ChartAxis.js" path="widgets\form" />
  <file name="source\widgets\form\ChartHistoricalAnalysis.js" path="widgets\form" />
  <file name="source\widgets\form\ChartMarker.js" path="widgets\form" />
  <file name="source\widgets\form\ChartPsetAttributeHistorical.js" path="widgets\form" />
  <file name="source\widgets\form\ChartScheduler.js" path="widgets\form" />
  <file name="source\widgets\form\ChartWizard.js" path="widgets\form" />
  <file name="source\widgets\form\Checkbox.js" path="widgets\form" />
  <file name="source\widgets\form\ClassificationDefinition.js" path="widgets\form" />
  <file name="source\widgets\form\ClassificationItem.js" path="widgets\form" />
  <file name="source\widgets\form\ColorPalette.js" path="widgets\form" />
  <file name="source\widgets\form\CompositeField.js" path="widgets\form" />
  <file name="source\widgets\form\DataSerie.js" path="widgets\form" />
  <file name="source\widgets\form\DataSerieColorElement.js" path="widgets\form" />
  <file name="source\widgets\form\DataSeriePsetRatio.js" path="widgets\form" />
  <file name="source\widgets\form\DateTime.js" path="widgets\form" />
  <file name="source\widgets\form\DisplayFormatField.js" path="widgets\form" />
  <file name="source\widgets\form\DrawingCanvas.js" path="widgets\form" />
  <file name="source\widgets\form\DrawingCanvasChart.js" path="widgets\form" />
  <file name="source\widgets\form\DrawingCanvasImage.js" path="widgets\form" />
  <file name="source\widgets\form\DynamicDisplay.js" path="widgets\form" />
  <file name="source\widgets\form\DynamicDisplayColorTemplate.js" path="widgets\form" />
  <file name="source\widgets\form\DynamicDisplayImage.js" path="widgets\form" />
  <file name="source\widgets\form\EnergyCertificate.js" path="widgets\form" />
  <file name="source\widgets\form\EnergyCertificateCategory.js" path="widgets\form" />
  <file name="source\widgets\form\EventLog.js" path="widgets\form" />
  <file name="source\widgets\form\EWSDataAcquisitionContainer.js" path="widgets\form" />
  <file name="source\widgets\form\FileUploadField.js" path="widgets\form" />
  <file name="source\widgets\form\FilterSpatialPset.js" path="widgets\form" />
  <file name="source\widgets\form\FormPanel.js" path="widgets\form" />
  <file name="source\widgets\form\Furniture.js" path="widgets\form" />
  <file name="source\widgets\form\ImageUploadField.js" path="widgets\form" />
  <file name="source\widgets\form\Job.js" path="widgets\form" />
  <file name="source\widgets\form\Localization.js" path="widgets\form" />
  <file name="source\widgets\form\MachineInstance.js" path="widgets\form" />
  <file name="source\widgets\form\Mail.js" path="widgets\form" />
  <file name="source\widgets\form\Link.js" path="widgets\form" />
  <file name="source\widgets\form\Map.js" path="widgets\form" />
  <file name="source\widgets\form\MappingTask.js" path="widgets\form" />
  <file name="source\widgets\form\Meter.js" path="widgets\form" />
  <file name="source\widgets\form\MeterData.js" path="widgets\form" />
  <file name="source\widgets\form\MeterDataBatch.js" path="widgets\form" />
  <file name="source\widgets\form\MeterDataDeleteByDateRange.js" path="widgets\form" />
  <file name="source\widgets\form\MeterDataGenerate.js" path="widgets\form" />
  <file name="source\widgets\form\MeterDataOffsetByDateRange.js" path="widgets\form" />
  <file name="source\widgets\form\MeterOperation.js" path="widgets\form" />
  <file name="source\widgets\form\MeterValidationRule.js" path="widgets\form" />
  <file name="source\widgets\form\NumberField.js" path="widgets\form" />
  <file name="source\widgets\form\Occupant.js" path="widgets\form" />
  <file name="source\widgets\form\Organization.js" path="widgets\form" />
  <file name="source\widgets\form\Palette.js" path="widgets\form" />
  <file name="source\widgets\form\PaletteColor.js" path="widgets\form" />
  <file name="source\widgets\form\Playlist.js" path="widgets\form" />
  <file name="source\widgets\form\PlaylistPage.js" path="widgets\form" />
  <file name="source\widgets\form\PortalColumn.js" path="widgets\form" />
  <file name="source\widgets\form\PortalTab.js" path="widgets\form" />
  <file name="source\widgets\form\PortalTemplate.js" path="widgets\form" />
  <file name="source\widgets\form\PortalWindow.js" path="widgets\form" />
  <file name="source\widgets\form\Portlet.js" path="widgets\form" />
  <file name="source\widgets\form\PrefixField.js" path="widgets\form" />
  <file name="source\widgets\form\Priority.js" path="widgets\form" />
  <file name="source\widgets\form\PsetAttributeDefinition.js" path="widgets\form" />
  <file name="source\widgets\form\PsetAttributeHistorical.js" path="widgets\form" />
  <file name="source\widgets\form\PsetAttributeHistoricalEndpoint.js" path="widgets\form" />
  <file name="source\widgets\form\PsetDefinition.js" path="widgets\form" />
  <file name="source\widgets\form\PsetList.js" path="widgets\form" />
  <file name="source\widgets\form\RestDataAcquisitionContainer.js" path="widgets\form" />
  <file name="source\widgets\form\RestDataAcquisitionEndpoint.js" path="widgets\form" />
  <file name="source\widgets\form\Schedule.js" path="widgets\form" />
  <file name="source\widgets\form\Algorithm.js" path="widgets\form" />
  <file name="source\widgets\form\Search.js" path="widgets\form" />
  <file name="source\widgets\form\Site.js" path="widgets\form" />
  <file name="source\widgets\form\Space.js" path="widgets\form" />
  <file name="source\widgets\form\SpinnerField.js" path="widgets\form" />
  <file name="source\widgets\form\StatisticalSerie.js" path="widgets\form" />
  <file name="source\widgets\form\Step.js" path="widgets\form" />
  <file name="source\widgets\form\StruxureWareDataAcquisitionContainer.js" path="widgets\form" />
  <file name="source\widgets\form\Task.js" path="widgets\form" />
  <file name="source\widgets\form\Tenant.js" path="widgets\form" />
  <file name="source\widgets\form\User.js" path="widgets\form" />
  <file name="source\widgets\form\UserInfo.js" path="widgets\form" />
  <file name="source\widgets\form\VirtualFile.js" path="widgets\form" />
  <file name="source\widgets\form\VTypes.js" path="widgets\form" />
  <file name="source\widgets\form\WeatherLocation.js" path="widgets\form" />
  <file name="source\widgets\form\WebFrame.js" path="widgets\form" />
  <file name="source\widgets\grid\ActionRequest.js" path="widgets\grid" />
  <file name="source\widgets\grid\AlarmDefinition.js" path="widgets\grid" />
  <file name="source\widgets\grid\AlarmInstance.js" path="widgets\grid" />
  <file name="source\widgets\grid\AlarmTable.js" path="widgets\grid" />
  <file name="source\widgets\grid\ApplicationGroup.js" path="widgets\grid" />
  <file name="source\widgets\grid\Approval.js" path="widgets\grid" />
  <file name="source\widgets\grid\AzManOperation.js" path="widgets\grid" />
  <file name="source\widgets\grid\AzManTask.js" path="widgets\grid" />
  <file name="source\widgets\grid\CalculatedSerie.js" path="widgets\grid" />
  <file name="source\widgets\grid\CalendarDefinition.js" path="widgets\grid" />
  <file name="source\widgets\grid\CalendarEventCategory.js" path="widgets\grid" />
  <file name="source\widgets\grid\Chart.js" path="widgets\grid" />
  <file name="source\widgets\grid\ChartAxis.js" path="widgets\grid" />
  <file name="source\widgets\grid\ChartHistoricalAnalysis.js" path="widgets\grid" />
  <file name="source\widgets\grid\ChartMarker.js" path="widgets\grid" />
  <file name="source\widgets\grid\ChartPsetAttributeHistorical.js" path="widgets\grid" />
  <file name="source\widgets\grid\ChartScheduler.js" path="widgets\grid" />
  <file name="source\widgets\grid\ClassificationDefinition.js" path="widgets\grid" />
  <file name="source\widgets\grid\DataPoint.js" path="widgets\grid" />
  <file name="source\widgets\grid\DataSerie.js" path="widgets\grid" />
  <file name="source\widgets\grid\DataSerieColorElement.js" path="widgets\grid" />
  <file name="source\widgets\grid\DataSeriePsetRatio.js" path="widgets\grid" />
  <file name="source\widgets\grid\DrawingCanvas.js" path="widgets\grid" />
  <file name="source\widgets\grid\DrawingCanvasChart.js" path="widgets\grid" />
  <file name="source\widgets\grid\DrawingCanvasImage.js" path="widgets\grid" />
  <file name="source\widgets\grid\DynamicDisplay.js" path="widgets\grid" />
  <file name="source\widgets\grid\DynamicDisplayColorTemplate.js" path="widgets\grid" />
  <file name="source\widgets\grid\DynamicDisplayImage.js" path="widgets\grid" />
  <file name="source\widgets\grid\EnergyCertificate.js" path="widgets\grid" />
  <file name="source\widgets\grid\EnergyCertificateCategory.js" path="widgets\grid" />
  <file name="source\widgets\grid\EventLog.js" path="widgets\grid" />
  <file name="source\widgets\grid\EWSDataAcquisitionContainer.js" path="widgets\grid" />
  <file name="source\widgets\grid\EWSDataAcquisitionEndPoint.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterAlarmDefinition.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterAlarmDefinitionClassification.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterAzmanRole.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterChart.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterChartScheduler.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterClassificationItem.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterDynamicDisplay.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterEventLog.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterMeterClassification.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterPlaylist.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterLink.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterPortalTemplate.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterPortalWindow.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterReport.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterSpatial.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterSpatialPset.js" path="widgets\grid" />
  <file name="source\widgets\grid\FilterSpatialPsetLocation.js" path="widgets\grid" />
  <file name="source\widgets\grid\GridPanel.js" path="widgets\grid" />
  <file name="source\widgets\grid\Job.js" path="widgets\grid" />
  <file name="source\widgets\grid\Localization.js" path="widgets\grid" />
  <file name="source\widgets\grid\LoginHistory.js" path="widgets\grid" />
  <file name="source\widgets\grid\MachineInstance.js" path="widgets\grid" />
  <file name="source\widgets\grid\Mail.js" path="widgets\grid" />
  <file name="source\widgets\grid\Link.js" path="widgets\grid" />
  <file name="source\widgets\grid\Map.js" path="widgets\grid" />
  <file name="source\widgets\grid\MappingTask.js" path="widgets\grid" />
  <file name="source\widgets\grid\Meter.js" path="widgets\grid" />
  <file name="source\widgets\grid\MeterData.js" path="widgets\grid" />
  <file name="source\widgets\grid\MeterOperation.js" path="widgets\grid" />
  <file name="source\widgets\grid\MeterValidationRule.js" path="widgets\grid" />
  <file name="source\widgets\grid\Occupant.js" path="widgets\grid" />
  <file name="source\widgets\grid\PagingRowNumberer.js" path="widgets\grid" />
  <file name="source\widgets\grid\Palette.js" path="widgets\grid" />
  <file name="source\widgets\grid\PaletteColor.js" path="widgets\grid" />
  <file name="source\widgets\grid\Playlist.js" path="widgets\grid" />
  <file name="source\widgets\grid\PlaylistPage.js" path="widgets\grid" />
  <file name="source\widgets\grid\PortalColumn.js" path="widgets\grid" />
  <file name="source\widgets\grid\PortalTab.js" path="widgets\grid" />
  <file name="source\widgets\grid\PortalTemplate.js" path="widgets\grid" />
  <file name="source\widgets\grid\PortalWindow.js" path="widgets\grid" />
  <file name="source\widgets\grid\Portlet.js" path="widgets\grid" />
  <file name="source\widgets\grid\Priority.js" path="widgets\grid" />
  <file name="source\widgets\grid\PsetAttributeHistorical.js" path="widgets\grid" />
  <file name="source\widgets\grid\PsetList.js" path="widgets\grid" />
  <file name="source\widgets\grid\RestDataAcquisitionContainer.js" path="widgets\grid" />
  <file name="source\widgets\grid\RestDataAcquisitionEndpoint.js" path="widgets\grid" />
  <file name="source\widgets\grid\Schedule.js" path="widgets\grid" />
  <file name="source\widgets\grid\Algorithm.js" path="widgets\grid" />
  <file name="source\widgets\grid\StatisticalSerie.js" path="widgets\grid" />
  <file name="source\widgets\grid\Step.js" path="widgets\grid" />
  <file name="source\widgets\grid\StruxureWareDataAcquisitionContainer.js" path="widgets\grid" />
  <file name="source\widgets\grid\StruxureWareDataAcquisitionEndpoint.js" path="widgets\grid" />
  <file name="source\widgets\grid\Task.js" path="widgets\grid" />
  <file name="source\widgets\grid\Tenant.js" path="widgets\grid" />
  <file name="source\widgets\grid\TraceEntry.js" path="widgets\grid" />
  <file name="source\widgets\grid\User.js" path="widgets\grid" />
  <file name="source\widgets\grid\UserFiltered.js" path="widgets\grid" />
  <file name="source\widgets\grid\VirtualFile.js" path="widgets\grid" />
  <file name="source\widgets\grid\WeatherLocation.js" path="widgets\grid" />
  <file name="source\widgets\grid\WebFrame.js" path="widgets\grid" />
  <file name="source\widgets\grid\WorkflowActionElement.js" path="widgets\grid" />
  <file name="source\widgets\grid\ZoningLegend.js" path="widgets\grid" />
  <file name="source\widgets\panel\ActionRequest.js" path="widgets\panel" />
  <file name="source\widgets\panel\ActionRequestSpecific.js" path="widgets\panel" />
  <file name="source\widgets\panel\AlarmDefinition.js" path="widgets\panel" />
  <file name="source\widgets\panel\AlarmTable.js" path="widgets\panel" />
  <file name="source\widgets\panel\ApplicationGroup.js" path="widgets\panel" />
  <file name="source\widgets\panel\AzManOperation.js" path="widgets\panel" />
  <file name="source\widgets\panel\AzManRole.js" path="widgets\panel" />
  <file name="source\widgets\panel\AzManTask.js" path="widgets\panel" />
  <file name="source\widgets\panel\CalendarDefinition.js" path="widgets\panel" />
  <file name="source\widgets\panel\CalendarEventCategory.js" path="widgets\panel" />
  <file name="source\widgets\panel\CalendarLocation.js" path="widgets\panel" />
  <file name="source\widgets\panel\CalendarTask.js" path="widgets\panel" />
  <file name="source\widgets\panel\Chart.js" path="widgets\panel" />
  <file name="source\widgets\panel\ChartScheduler.js" path="widgets\panel" />
  <file name="source\widgets\panel\ClassificationDefinition.js" path="widgets\panel" />
  <file name="source\widgets\panel\DrawingCanvas.js" path="widgets\panel" />
  <file name="source\widgets\panel\DynamicDisplay.js" path="widgets\panel" />
  <file name="source\widgets\panel\DynamicDisplayColorTemplate.js" path="widgets\panel" />
  <file name="source\widgets\panel\DynamicDisplayImage.js" path="widgets\panel" />
  <file name="source\widgets\panel\EnergyCertificate.js" path="widgets\panel" />
  <file name="source\widgets\panel\EventLog.js" path="widgets\panel" />
  <file name="source\widgets\panel\EWSDataAcquisitionContainer.js" path="widgets\panel" />
  <file name="source\widgets\panel\Job.js" path="widgets\panel" />
  <file name="source\widgets\panel\Localization.js" path="widgets\panel" />
  <file name="source\widgets\panel\LoginHistory.js" path="widgets\panel" />
  <file name="source\widgets\panel\LoginHistoryActive.js" path="widgets\panel" />
  <file name="source\widgets\panel\MachineInstance.js" path="widgets\panel" />
  <file name="source\widgets\panel\Mail.js" path="widgets\panel" />
  <file name="source\widgets\panel\Link.js" path="widgets\panel" />
  <file name="source\widgets\panel\ManageBuildingStorey.js" path="widgets\panel" />
  <file name="source\widgets\panel\Map.js" path="widgets\panel" />
  <file name="source\widgets\panel\Meter.js" path="widgets\panel" />
  <file name="source\widgets\panel\MeterData.js" path="widgets\panel" />
  <file name="source\widgets\panel\MeterDataBatch.js" path="widgets\panel" />
  <file name="source\widgets\panel\MeterValidationRule.js" path="widgets\panel" />
  <file name="source\widgets\panel\Occupant.js" path="widgets\panel" />
  <file name="source\widgets\panel\Palette.js" path="widgets\panel" />
  <file name="source\widgets\panel\Panel.js" path="widgets\panel" />
  <file name="source\widgets\panel\Playlist.js" path="widgets\panel" />
  <file name="source\widgets\panel\PortalTemplate.js" path="widgets\panel" />
  <file name="source\widgets\panel\PortalWindow.js" path="widgets\panel" />
  <file name="source\widgets\panel\Priority.js" path="widgets\panel" />
  <file name="source\widgets\panel\ReportViewer.js" path="widgets\panel" />
  <file name="source\widgets\panel\RESTDataAcquisitionContainer.js" path="widgets\panel" />
  <file name="source\widgets\panel\Algorithm.js" path="widgets\panel" />
  <file name="source\widgets\panel\StruxurewareDataAcquisitionContainer.js" path="widgets\panel" />
  <file name="source\widgets\panel\Tenant.js" path="widgets\panel" />
  <file name="source\widgets\panel\TraceEntry.js" path="widgets\panel" />
  <file name="source\widgets\panel\User.js" path="widgets\panel" />
  <file name="source\widgets\panel\VirtualFile.js" path="widgets\panel" />
  <file name="source\widgets\panel\WeatherLocation.js" path="widgets\panel" />
  <file name="source\widgets\panel\WebFrame.js" path="widgets\panel" />
  <file name="source\widgets\portal\PortalColumn.js" path="widgets\portal" />
  <file name="source\widgets\portal\PortalPanel.js" path="widgets\portal" />
  <file name="source\widgets\portal\PortalTab.js" path="widgets\portal" />
  <file name="source\widgets\portal\PortalWindow.js" path="widgets\portal" />
  <file name="source\widgets\portal\Portlet.js" path="widgets\portal" />
  <file name="source\widgets\portal\PortletAlarmTable.js" path="widgets\portal" />
  <file name="source\widgets\portal\PortletDrawingCanvas.js" path="widgets\portal" />
  <file name="source\widgets\portal\PortletMap.js" path="widgets\portal" />
  <file name="source\widgets\portal\PortletWeather.js" path="widgets\portal" />
  <file name="source\widgets\portal\PortletWebFrame.js" path="widgets\portal" />
  <file name="source\widgets\portal\PortletWindow.js" path="widgets\portal" />
  <file name="source\widgets\silverlight\ChartViewer.js" path="widgets\silverlight" />
  <file name="source\widgets\silverlight\Silverlight.js" path="widgets\silverlight" />
  <file name="source\widgets\silverlight\SilverlightComponent.js" path="widgets\silverlight" />
  <file name="source\widgets\silverlight\Viewer.js" path="widgets\silverlight" />
  <file name="source\widgets\store\ActionRequest.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmDefinition.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmDefinitionChart.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmDefinitionFilterMeterClassification.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmDefinitionFilterSpatial.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmDefinitionFilterSpatialPset.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmDefinitionFilterSpatialPsetLocation.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmDefinitionMeter.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmDefinitionMeterAll.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmInstance.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmTable.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmTableAlarmDefinition.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmTableAlarmDefinitionAll.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmTableAlarmInstance.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmTableFilterAlarmDefinitionClassification.js" path="widgets\store" />
  <file name="source\widgets\store\AlarmTableFilterSpatial.js" path="widgets\store" />
  <file name="source\widgets\store\ApplicationGroup.js" path="widgets\store" />
  <file name="source\widgets\store\ApplicationGroupAuthorization.js" path="widgets\store" />
  <file name="source\widgets\store\ApplicationGroupAzManRole.js" path="widgets\store" />
  <file name="source\widgets\store\Approval.js" path="widgets\store" />
  <file name="source\widgets\store\Audit.js" path="widgets\store" />
  <file name="source\widgets\store\AuditEntity.js" path="widgets\store" />
  <file name="source\widgets\store\AuthenticationProvider.js" path="widgets\store" />
  <file name="source\widgets\store\Authorization.js" path="widgets\store" />
  <file name="source\widgets\store\AzManFilterAlarmDefinition.js" path="widgets\store" />
  <file name="source\widgets\store\AzManFilterAuthorization.js" path="widgets\store" />
  <file name="source\widgets\store\AzManFilterChart.js" path="widgets\store" />
  <file name="source\widgets\store\AzmanFilterChartScheduler.js" path="widgets\store" />
  <file name="source\widgets\store\AzManFilterClassificationItem.js" path="widgets\store" />
  <file name="source\widgets\store\AzManFilterDynamicDisplay.js" path="widgets\store" />
  <file name="source\widgets\store\AzmanFilterPlaylist.js" path="widgets\store" />
  <file name="source\widgets\store\AzmanFilterLink.js" path="widgets\store" />
  <file name="source\widgets\store\AzmanFilterPortalTemplate.js" path="widgets\store" />
  <file name="source\widgets\store\AzManFilterPortalWindow.js" path="widgets\store" />
  <file name="source\widgets\store\AzManFilterReport.js" path="widgets\store" />
  <file name="source\widgets\store\AzManFilterSpatial.js" path="widgets\store" />
  <file name="source\widgets\store\AzManFilterUserAuthorization.js" path="widgets\store" />
  <file name="source\widgets\store\AzManOperation.js" path="widgets\store" />
  <file name="source\widgets\store\AzManTask.js" path="widgets\store" />
  <file name="source\widgets\store\AzManTaskOperation.js" path="widgets\store" />
  <file name="source\widgets\store\CalculatedSerie.js" path="widgets\store" />
  <file name="source\widgets\store\CalendarDayName.js" path="widgets\store" />
  <file name="source\widgets\store\CalendarDefinition.js" path="widgets\store" />
  <file name="source\widgets\store\CalendarEventCategory.js" path="widgets\store" />
  <file name="source\widgets\store\CalendarFrequencyOccurrence.js" path="widgets\store" />
  <file name="source\widgets\store\CalendarMonthName.js" path="widgets\store" />
  <file name="source\widgets\store\Chart.js" path="widgets\store" />
  <file name="source\widgets\store\ChartAvailableDataSerieLocalId.js" path="widgets\store" />
  <file name="source\widgets\store\ChartAxis.js" path="widgets\store" />
  <file name="source\widgets\store\ChartClassificationLevel.js" path="widgets\store" />
  <file name="source\widgets\store\ChartFilterAlarmDefinitionClassification.js" path="widgets\store" />
  <file name="source\widgets\store\ChartFilterEventLogClassification.js" path="widgets\store" />
  <file name="source\widgets\store\ChartFilterMeterClassification.js" path="widgets\store" />
  <file name="source\widgets\store\ChartFilterSpatial.js" path="widgets\store" />
  <file name="source\widgets\store\ChartFilterSpatialPset.js" path="widgets\store" />
  <file name="source\widgets\store\ChartFilterSpatialPsetLocation.js" path="widgets\store" />
  <file name="source\widgets\store\ChartHistoricalAnalysis.js" path="widgets\store" />
  <file name="source\widgets\store\ChartKPI.js" path="widgets\store" />
  <file name="source\widgets\store\ChartKPIAzManRole.js" path="widgets\store" />
  <file name="source\widgets\store\ChartLocalisation.js" path="widgets\store" />
  <file name="source\widgets\store\ChartMarker.js" path="widgets\store" />
  <file name="source\widgets\store\ChartMeter.js" path="widgets\store" />
  <file name="source\widgets\store\ChartMeterAll.js" path="widgets\store" />
  <file name="source\widgets\store\ChartPsetAttributeHistorical.js" path="widgets\store" />
  <file name="source\widgets\store\ChartScheduler.js" path="widgets\store" />
  <file name="source\widgets\store\ChartSchedulerChart.js" path="widgets\store" />
  <file name="source\widgets\store\ChartSchedulerPortalWindow.js" path="widgets\store" />
  <file name="source\widgets\store\ClassificationDefinition.js" path="widgets\store" />
  <file name="source\widgets\store\ClassificationItemDefinition.js" path="widgets\store" />
  <file name="source\widgets\store\Color.js" path="widgets\store" />
  <file name="source\widgets\store\DataAcquisitionEndpoint.js" path="widgets\store" />
  <file name="source\widgets\store\DataPoint.js" path="widgets\store" />
  <file name="source\widgets\store\DataSerie.js" path="widgets\store" />
  <file name="source\widgets\store\DataSerieColorElement.js" path="widgets\store" />
  <file name="source\widgets\store\DataSeriePsetRatio.js" path="widgets\store" />
  <file name="source\widgets\store\DesktopBackground.js" path="widgets\store" />
  <file name="source\widgets\store\DetailedAudit.js" path="widgets\store" />
  <file name="source\widgets\store\DrawingCanvas.js" path="widgets\store" />
  <file name="source\widgets\store\DrawingCanvasChart.js" path="widgets\store" />
  <file name="source\widgets\store\DrawingCanvasImage.js" path="widgets\store" />
  <file name="source\widgets\store\DynamicDisplay.js" path="widgets\store" />
  <file name="source\widgets\store\DynamicDisplayColorTemplate.js" path="widgets\store" />
  <file name="source\widgets\store\DynamicDisplayImage.js" path="widgets\store" />
  <file name="source\widgets\store\EnergyCertificate.js" path="widgets\store" />
  <file name="source\widgets\store\EnergyCertificateCategory.js" path="widgets\store" />
  <file name="source\widgets\store\EntityPortlet.js" path="widgets\store" />
  <file name="source\widgets\store\Enum.js" path="widgets\store" />
  <file name="source\widgets\store\EventLog.js" path="widgets\store" />
  <file name="source\widgets\store\EWSDataAcquisitionContainer.js" path="widgets\store" />
  <file name="source\widgets\store\EWSDataAcquisitionEndpoint.js" path="widgets\store" />
  <file name="source\widgets\store\FontFamily.js" path="widgets\store" />
  <file name="source\widgets\store\Job.js" path="widgets\store" />
  <file name="source\widgets\store\JobSchedule.js" path="widgets\store" />
  <file name="source\widgets\store\ListElement.js" path="widgets\store" />
  <file name="source\widgets\store\Localization.js" path="widgets\store" />
  <file name="source\widgets\store\LocalizationCulture.js" path="widgets\store" />
  <file name="source\widgets\store\LoginHistory.js" path="widgets\store" />
  <file name="source\widgets\store\LoginHistoryActive.js" path="widgets\store" />
  <file name="source\widgets\store\MachineInstance.js" path="widgets\store" />
  <file name="source\widgets\store\Mail.js" path="widgets\store" />
  <file name="source\widgets\store\Link.js" path="widgets\store" />
  <file name="source\widgets\store\MailClassificationItem.js" path="widgets\store" />
  <file name="source\widgets\store\MailLocation.js" path="widgets\store" />
  <file name="source\widgets\store\MailPriority.js" path="widgets\store" />
  <file name="source\widgets\store\MailWorkflowActionElement.js" path="widgets\store" />
  <file name="source\widgets\store\Map.js" path="widgets\store" />
  <file name="source\widgets\store\MappingTask.js" path="widgets\store" />
  <file name="source\widgets\store\Meter.js" path="widgets\store" />
  <file name="source\widgets\store\MeterAlarmInstance.js" path="widgets\store" />
  <file name="source\widgets\store\MeterData.js" path="widgets\store" />
  <file name="source\widgets\store\MeterDataAlarmInstance.js" path="widgets\store" />
  <file name="source\widgets\store\MeterOperation.js" path="widgets\store" />
  <file name="source\widgets\store\MeterValidationRule.js" path="widgets\store" />
  <file name="source\widgets\store\MeterValidationRuleAlarmInstance.js" path="widgets\store" />
  <file name="source\widgets\store\MeterValidationRuleFilterMeterClassification.js" path="widgets\store" />
  <file name="source\widgets\store\MeterValidationRuleFilterSpatial.js" path="widgets\store" />
  <file name="source\widgets\store\MeterValidationRuleMeter.js" path="widgets\store" />
  <file name="source\widgets\store\MeterValidationRuleMeterAll.js" path="widgets\store" />
  <file name="source\widgets\store\MethodDefinition.js" path="widgets\store" />
  <file name="source\widgets\store\ObjectClassificationItem.js" path="widgets\store" />
  <file name="source\widgets\store\Occupant.js" path="widgets\store" />
  <file name="source\widgets\store\Organization.js" path="widgets\store" />
  <file name="source\widgets\store\Palette.js" path="widgets\store" />
  <file name="source\widgets\store\PaletteColor.js" path="widgets\store" />
  <file name="source\widgets\store\Playlist.js" path="widgets\store" />
  <file name="source\widgets\store\PlaylistPage.js" path="widgets\store" />
  <file name="source\widgets\store\PortalColumn.js" path="widgets\store" />
  <file name="source\widgets\store\PortalTab.js" path="widgets\store" />
  <file name="source\widgets\store\PortalTemplate.js" path="widgets\store" />
  <file name="source\widgets\store\PortalTemplateFOLMembershipUser.js" path="widgets\store" />
  <file name="source\widgets\store\PortalTemplatePortalWindow.js" path="widgets\store" />
  <file name="source\widgets\store\PortalWindow.js" path="widgets\store" />
  <file name="source\widgets\store\Portlet.js" path="widgets\store" />
  <file name="source\widgets\store\Priority.js" path="widgets\store" />
  <file name="source\widgets\store\PsetAttributeHistorical.js" path="widgets\store" />
  <file name="source\widgets\store\PsetDefinition.js" path="widgets\store" />
  <file name="source\widgets\store\PsetList.js" path="widgets\store" />
  <file name="source\widgets\store\PsetUsageName.js" path="widgets\store" />
  <file name="source\widgets\store\RapidSearch.js" path="widgets\store" />
  <file name="source\widgets\store\ReportParameterValue.js" path="widgets\store" />
  <file name="source\widgets\store\RestDataAcquisitionContainer.js" path="widgets\store" />
  <file name="source\widgets\store\RestDataAcquisitionEndpoint.js" path="widgets\store" />
  <file name="source\widgets\store\RestDataAcquisitionProvider.js" path="widgets\store" />
  <file name="source\widgets\store\Algorithm.js" path="widgets\store" />
  <file name="source\widgets\store\Site.js" path="widgets\store" />
  <file name="source\widgets\store\StatisticalSerie.js" path="widgets\store" />
  <file name="source\widgets\store\StepAlarmDefinition.js" path="widgets\store" />
  <file name="source\widgets\store\StepPlaylist.js" path="widgets\store" />
  <file name="source\widgets\store\StruxureWareDataAcquisitionContainer.js" path="widgets\store" />
  <file name="source\widgets\store\StruxureWareDataAcquisitionEndpoint.js" path="widgets\store" />
  <file name="source\widgets\store\Task.js" path="widgets\store" />
  <file name="source\widgets\store\Tenant.js" path="widgets\store" />
  <file name="source\widgets\store\TimeZone.js" path="widgets\store" />
  <file name="source\widgets\store\TraceEntry.js" path="widgets\store" />
  <file name="source\widgets\store\User.js" path="widgets\store" />
  <file name="source\widgets\store\UserApplicationGroup.js" path="widgets\store" />
  <file name="source\widgets\store\UserFiltered.js" path="widgets\store" />
  <file name="source\widgets\store\VirtualFile.js" path="widgets\store" />
  <file name="source\widgets\store\WeatherLocation.js" path="widgets\store" />
  <file name="source\widgets\store\WeatherLocationSearch.js" path="widgets\store" />
  <file name="source\widgets\store\WebFrame.js" path="widgets\store" />
  <file name="source\widgets\store\WorkflowActionElement.js" path="widgets\store" />
  <file name="source\widgets\store\WorkflowAssemblyDefinition.js" path="widgets\store" />
  <file name="source\widgets\tabpanel\TabPanel.js" path="widgets\tabpanel" />
  <file name="source\widgets\toolbar\TabToolbar.js" path="widgets\toolbar" />
  <file name="source\widgets\tree\AlarmDefinitionClassification.js" path="widgets\tree" />
  <file name="source\widgets\tree\ApplicationGroup.js" path="widgets\tree" />
  <file name="source\widgets\tree\AzManFilter.js" path="widgets\tree" />
  <file name="source\widgets\tree\AzManRole.js" path="widgets\tree" />
  <file name="source\widgets\tree\AzManOperation.js" path="widgets\tree" />
  <file name="source\widgets\tree\ClassificationItem.js" path="widgets\tree" />
  <file name="source\widgets\tree\DataAcquisition.js" path="widgets\tree" />
  <file name="source\widgets\tree\MeterClassification.js" path="widgets\tree" />
  <file name="source\widgets\tree\Organization.js" path="widgets\tree" />
  <file name="source\widgets\tree\Portlet.js" path="widgets\tree" />
  <file name="source\widgets\tree\Pset.js" path="widgets\tree" />
  <file name="source\widgets\tree\Report.js" path="widgets\tree" />
  <file name="source\widgets\tree\Spatial.js" path="widgets\tree" />
  <file name="source\widgets\tree\TreeLoaderWCF.js" path="widgets\tree" />
  <file name="source\widgets\tree\TreePanel.js" path="widgets\tree" />
  <file name="source\widgets\tree\TreeSorter.js" path="widgets\tree" />
  <file name="source\widgets\tree\User.js" path="widgets\tree" />
  <file name="source\widgets\viewer\Image.js" path="widgets\viewer" />
  <file name="source\widgets\viewport\MainViewPort.js" path="widgets\viewport" />
  <file name="source\widgets\window\AboutBox.js" path="widgets\window" />
  <file name="source\widgets\window\AzmanImportXml.js" path="widgets\window" />
  <file name="source\widgets\window\ChangePassword.js" path="widgets\window" />
  <file name="source\widgets\window\ImportXml.js" path="widgets\window" />
  <file name="source\widgets\window\LoginDialog.js" path="widgets\window" />
  <file name="source\widgets\window\Occupant.js" path="widgets\window" />
  <file name="source\widgets\window\Translator.js" path="widgets\window" />
  <file name="source\widgets\workflow\Viewer.js" path="widgets\workflow" />
  <file name="source\widgets\App.js" path="widgets" />
  <file name="source\widgets\ComboLangue.js" path="widgets" />
  <file name="source\widgets\ComboTheme.js" path="widgets" />
  <file name="source\widgets\Desktop.js" path="widgets" />
  <file name="source\widgets\LoadingIndicator.js" path="widgets" />
  <file name="source\widgets\form\Logo.js" path="widgets\form" />
  <file name="source\widgets\desktop\LogoImage.js" path="widgets\desktop" />
  <file name="source\widgets\form\ReportSubscription.js" path="widgets\form" />
  <file name="source\widgets\grid\ReportSubscription.js" path="widgets\grid" />
  <file name="source\widgets\store\ReportSubscription.js" path="widgets\store" />
  <file name="source\widgets\form\ReportSubscriptionSchedule.js" path="widgets\form" />
  <file name="source\widgets\combo\CalendarEventCategoryTitle.js" path="widgets\combo" />
  <file name="source\widgets\combo\VirtualFileType.js" path="widgets\combo" />
  <file name="source\widgets\form\ChartAlgorithm.js" path="widgets\form" />
  <file name="source\widgets\grid\ChartAlgorithm.js" path="widgets\grid" />
  <file name="source\widgets\store\CalendarEventCategoryTitle.js" path="widgets\store" />
  <file name="source\widgets\store\ChartAlgorithm.js" path="widgets\store" />
  <file name="source\widgets\form\FlipCard.js" path="widgets\form" />
  <file name="source\widgets\form\FlipCardEntry.js" path="widgets\form" />
  <file name="source\widgets\grid\FlipCard.js" path="widgets\grid" />
  <file name="source\widgets\grid\FlipCardEntry.js" path="widgets\grid" />
  <file name="source\widgets\panel\FlipCard.js" path="widgets\panel" />
  <file name="source\widgets\portal\PortletFlipCard.js" path="widgets\portal" />
  <file name="source\widgets\store\FlipCard.js" path="widgets\store" />
  <file name="source\widgets\store\FlipCardEntry.js" path="widgets\store" />
  <file name="source\util\Fullscreen.js" path="util" />
  <file name="source\widgets\form\MeterDataExportTask.js" path="widgets\form" />
  <file name="source\widgets\grid\MeterDataExportTask.js" path="widgets\grid" />
  <file name="source\widgets\store\MeterDataExportTask.js" path="widgets\store" />
  <file name="source\widgets\combo\MappingTask.js" path="widgets\combo" />
  <file name="source\widgets\store\MeterDataExportTaskMeter.js" path="widgets\store" />
  <file name="source\widgets\form\AuditEntity.js" path="widgets\form" />
  <file name="source\widgets\grid\AuditEntity.js" path="widgets\grid" />
  <file name="source\widgets\panel\AuditEntity.js" path="widgets\panel" />
  <file name="source\widgets\combo\AuditableEntity.js" path="widgets\combo" />
  <file name="source\widgets\store\AuditableEntity.js" path="widgets\store" />
  <file name="source\widgets\store\StepPortalTemplate.js" path="widgets\store" />
  <file name="source\widgets\store\PortalTemplateFilterSpatial.js" path="widgets\store" />
  <file name="source\widgets\store\EntityCount.js" path="widgets\store" />
  <file name="source\widgets\grid\EntityCount.js" path="widgets\grid" />
  <file name="source\widgets\grid\TraceSummary.js" path="widgets\grid" />
  <file name="source\widgets\panel\TraceSummary.js" path="widgets\panel" />
  <file name="source\widgets\store\TraceSummary.js" path="widgets\store" />
  <file name="source\widgets\grid\JobInstance.js" path="widgets\grid" />
  <file name="source\widgets\store\JobInstance.js" path="widgets\store" />
  <file name="source\widgets\form\AlgorithmInputDefinition.js" path="widgets\form" />
  <file name="source\widgets\form\AlgorithmInputValue.js" path="widgets\form" />
  <file name="source\widgets\grid\AlgorithmInputDefinition.js" path="widgets\grid" />
  <file name="source\widgets\grid\AlgorithmInputValue.js" path="widgets\grid" />
  <file name="source\widgets\store\AlgorithmInputDefinition.js" path="widgets\store" />
  <file name="source\widgets\store\AlgorithmInputValue.js" path="widgets\store" />
  <file name="source\widgets\combo\AlgorithmInputDefinition.js" path="widgets\combo" />
  <file name="source\ux\LinkButton.js" path="ux" />
  <file name="source\widgets\window\ForgotPasswordDialog.js" path="widgets\window" />
  <file name="source\widgets\window\LoginAsUser.js" path="widgets\window" />
  <file name="source\widgets\chart\Html.js" path="widgets\chart" />
  <file name="source\widgets\form\MonthYear.js" path="widgets\form" />
  <file name="source\plugins\dotnetcharting_highcharts.js" path="plugins" />
  <file name="source\widgets\grid\AzManFilter.js" path="widgets\grid" />
  <file name="source\widgets\panel\AzManFilter.js" path="widgets\panel" />
  <file name="source\widgets\store\AzManFilter.js" path="widgets\store" />
  <file name="source\widgets\combo\TreeComboAzmanFilter.js" path="widgets\combo" />
  <file name="source\widgets\store\AzManFilterByLocation.js" path="widgets\store" />
  <file name="source\plugins\Flip.js" path="plugins" />
  <file name="source\widgets\grid\CheckboxSelectionModel.js" path="widgets\grid" />
  <file name="source\widgets\form\UserPortalTemplate.js" path="widgets\form" />
</project>
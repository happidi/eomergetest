﻿function displayPortalTab(KeyPortalTab) {
    Viz.Services.EnergyWCF.PortalTab_GetItem({
                Key     : KeyPortalTab,
                fields  : ["Columns/Portlets/Entity"],
                success : function (entity) {
                    var portaltabConfig = Viz.util.Portal.getPortalTabConfig(entity);
                    portaltabConfig.header = false;
                    portaltabConfig.region = 'center';
                    var portaltab = new Viz.portal.PortalTab(portaltabConfig);
                    var thisView = new Ext.Viewport({
                                title     : 'viewport',
                                layout    : 'border',
                                items     : [portaltab],
                                listeners : {
                                    render : function() {
                                        portaltab.onActivate();
                                    },
                                    scope  : this
                                }
                            });
                },
                scope   : this
            });
}

Ext.onReady(function() {
            initPrintScreen();
        })
﻿using System.ServiceModel;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.WCFService {
	/// <summary>
	/// Implementation for SpaceManagement Services.
	/// </summary>
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
	public class SpaceManagementWCF : BaseModuleWCF, ISpaceManagementWCF {

		private ISpaceManagementBusinessLayer m_SpaceManagementBusinessLayer;

		/// <summary>
		/// Public ctor.
		/// </summary>
		public SpaceManagementWCF() {
			// Policy injection on the business layer instance.
			m_SpaceManagementBusinessLayer = Helper.CreateInstance<SpaceManagementBusinessLayer, ISpaceManagementBusinessLayer>();
		}

		/// <summary>
		/// Gets the zoning 
		/// </summary>
		/// <param name="KeyBuildingStorey"></param>
		/// <param name="KeyClassificationItem">The filter for Classifications (optional)</param>
		/// <returns></returns>
		public JsonStore<Space> BuildingStorey_GetZoning(string KeyBuildingStorey, string KeyClassificationItem) {
			return m_SpaceManagementBusinessLayer.BuildingStorey_GetZoning(KeyBuildingStorey, KeyClassificationItem);
		}
	}
}

﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.DataLayer {

	/// <summary>
	/// Contains Factory methods for dialoguing with the database.
	/// The class encapsulates a Data Access Enterprise Library Block and is agnostic from database type.
	/// It should be accessed exclusively via it's interface Vizelia.FOL.DataLayer.Interfaces.IDataAccess.
	/// It is necessary to define the relevant configuration block in app.config or web.config.
	/// </summary>
	public class DataAccess : IDataAccess {

		private readonly string m_DatabaseName;
		private Database m_DB;

		/// <summary>
		/// Initializes a new instance of the <see cref="DataAccess"/> class.
		/// </summary>
		/// <param name="dbServer">The db server.</param>
		/// <param name="dbName">Name of the db.</param>
		/// <param name="dbUser">The db user.</param>
		/// <param name="dbPwd">The db PWD.</param>
		public DataAccess(string dbServer, string dbName, string dbUser, string dbPwd) {
			m_DB = new SqlDatabase(String.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", dbServer, dbName, dbUser, dbPwd));
		}

		/// <summary>
		/// Ctor.
		/// </summary>
		/// <param name="databasename">The name of the database.</param>
		public DataAccess(string databasename) {
			m_DatabaseName = databasename;
			m_DB = InternalCreateDatabase();
		}

		/// <summary>
		/// Ctor.
		/// </summary>
		public DataAccess() {
			m_DB = InternalCreateDatabase();
		}

		/// <summary>
		/// Internals the create database.
		/// </summary>
		/// <returns></returns>
		private Database InternalCreateDatabase() {
			if (string.IsNullOrEmpty(m_DatabaseName))
				return EnterpriseLibraryContainer.Current.GetInstance<Database>();
			return EnterpriseLibraryContainer.Current.GetInstance<Database>(m_DatabaseName);
		}

		/// <summary>
		/// @private
		/// Adds a parameter to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="dbType">The type of the param.</param>
		/// <param name="value">The value of the param.</param>
		private void AddInParameter(DbCommand command, string name, DbType dbType, object value) {
			m_DB.AddInParameter(command, name, dbType, value);
		}

		/// <summary>
		/// Adds a bool or bit param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		public void AddInParameterBool(DbCommand command, string name, bool? value) {
			m_DB.AddInParameter(command, name, DbType.Boolean, value);
		}

		/// <summary>
		/// Adds a byte array param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		public void AddInParameterByteArray(DbCommand command, string name, byte[] value) {
			m_DB.AddInParameter(command, name, DbType.Binary, value);
		}

		/// <summary>
		/// Adds a datetime param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		public void AddInParameterDateTime(DbCommand command, string name, DateTime? value) {
			//db.AddInParameter(command, name, DbType.DateTimeOffset, value);

			if (value.HasValue)
				m_DB.AddInParameter(command, name, DbType.DateTime, value.Value.ToUniversalTime());
			else
				m_DB.AddInParameter(command, name, DbType.DateTime, null);

		}

		/// <summary>
		/// Adds a double param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		public void AddInParameterDouble(DbCommand command, string name, double? value) {
			m_DB.AddInParameter(command, name, DbType.Double, value);
		}

		/// <summary>
		/// Adds a decimal param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		public void AddInParameterDecimal(DbCommand command, string name, decimal? value) {
			m_DB.AddInParameter(command, name, DbType.Currency, value);
		}

		/// <summary>
		/// Adds a guid param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		public void AddInParameterGuid(DbCommand command, string name, Guid value) {
			m_DB.AddInParameter(command, name, DbType.Guid, value);
		}

		/// <summary>
		/// Adds a int param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		public void AddInParameterInt(DbCommand command, string name, int? value) {
			m_DB.AddInParameter(command, name, DbType.Int32, value);
		}

		/// <summary>
		/// Adds a string param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param</param>
		/// <param name="value">The value of the param.</param>
		public void AddInParameterString(DbCommand command, string name, string value) {
			m_DB.AddInParameter(command, name, DbType.String, value);
		}

		/// <summary>
		/// Adds a Table param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		/// <param name="typeName">Name of the type.</param>
		public void AddInParameterTable(DbCommand command, string name, DataTable value, string typeName) {
			((SqlDatabase)m_DB).AddInParameter(command, name, SqlDbType.Structured, value);
			var param = (SqlParameter)command.Parameters["@" + name];
			param.TypeName = typeName;
		}

		/// <summary>
		/// Adds a xml param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		public void AddInParameterXml(DbCommand command, string name, string value) {
			m_DB.AddInParameter(command, name, DbType.Xml, value);
		}

		/// <summary>
		/// @private
		/// Adds an out param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="dbType">The type of the param.</param>
		/// <param name="size">The size of the param.</param>
		private void AddOutParameter(DbCommand command, string name, DbType dbType, int size) {
			m_DB.AddOutParameter(command, name, dbType, size);
		}

		/// <summary>
		/// Adds an out int param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the out param.</param>
		public void AddOutParameterInt(DbCommand command, string name) {
			m_DB.AddOutParameter(command, name, DbType.Int32, sizeof(Int32));
		}

		/// <summary>
		/// Adds an out string param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the out param.</param>
		public void AddOutParameterString(DbCommand command, string name) {
			m_DB.AddOutParameter(command, name, DbType.String, 255);
		}

		/// <summary>
		/// Adds a returnValue param to a DbCommand.
		/// This seems to be a SQL Server specific feature for stored procedures.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		public void AddReturnParameter(DbCommand command) {
			m_DB.AddParameter(command, "@ReturnValue", DbType.Int32, ParameterDirection.ReturnValue, string.Empty, DataRowVersion.Default, null);
		}

		/// <summary>
		/// Executes the command and
		/// returns the results as a System.Data.DataSet.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <returns>System.Data.DataSet with results.</returns>
		public DataSet ExecuteDataSet(DbCommand command) {
			return Retry(() => {
				var retVal = m_DB.ExecuteDataSet(command);
				return retVal;
			});
		}

		/// <summary>
		/// Executes the commandText interpreted as specified by the commandType and
		/// returns the results in a new System.Data.DataSet.
		/// </summary>
		/// <param name="storedProcedureName">The stored procedure to execute.</param>
		/// <param name="parameterValues">
		/// An array of paramters to pass to the stored procedure. The parameter values
		/// must be in call order as they appear in the stored procedure.
		/// </param>
		/// <returns>A System.Data.DataSet with the results of the storedProcedureName.</returns>
		public DataSet ExecuteDataSet(string storedProcedureName, params object[] parameterValues) {
			return Retry(() => {
				var retVal = m_DB.ExecuteDataSet(storedProcedureName, parameterValues);
				return retVal;
			});
		}

		/// <summary>
		/// Executes the commandText interpreted as specified by the commandType and
		/// returns the results as a System.Data.DataSet.
		/// </summary>
		/// <param name="commandType">One of the System.Data.CommandType values.</param>
		/// <param name="commandText">The command text to execute.</param>
		/// <returns>A System.Data.DataSet with the results of the commandText.</returns>
		public DataSet ExecuteDataSet(CommandType commandType, string commandText) {
			return Retry(() => {
				var retVal = m_DB.ExecuteDataSet(commandType, commandText);
				return retVal;
			});
		}

		/// <summary>
		/// Executes the command and returns an int.
		/// </summary>
		/// <param name="command">The command that contains the query to execute.</param>
		/// <returns>The result int of the query.</returns>
		public int ExecuteNonQuery(DbCommand command) {
			return Retry(() => {
				var retVal = m_DB.ExecuteNonQuery(command);
				return retVal;
			});
		}


		/// <summary>
		/// Executes the storedProcedureName using the given parameterValues and returns
		/// the number of rows affected.
		/// </summary>
		/// <param name="storedProcedureName">The name of the stored procedure to execute.</param>
		/// <param name="parameterValues">
		/// An array of paramters to pass to the stored procedure. The parameter values
		/// must be in call order as they appear in the stored procedure.
		/// </param>
		/// <returns>The number of rows affected.</returns>
		public int ExecuteNonQuery(string storedProcedureName, params object[] parameterValues) {
			return Retry(() => {
				var retVal = m_DB.ExecuteNonQuery(storedProcedureName, parameterValues);
				return retVal;
			});
		}

		/// <summary>
		/// Executes the command and
		/// returns the results as a IDataReader.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <returns>IDataReader instance.</returns>
		public IDataReader ExecuteReader(DbCommand command) {
			return Retry(() => {
				var retVal = m_DB.ExecuteReader(command);
				return retVal;
			});
		}

		/// <summary>
		/// Executes the storedProcedureName with the given parameterValues and returns
		/// an System.Data.IDataReader through which the result can be read.  It is the
		/// responsibility of the caller to close the connection and reader when finished.
		/// </summary>
		/// <param name="storedProcedureName">The command that contains the query to execute.</param>
		/// <param name="parameterValues">
		/// An array of paramters to pass to the stored procedure. The parameter values
		/// must be in call order as they appear in the stored procedure.
		/// </param>
		/// <returns>An System.Data.IDataReader object.</returns>
		public IDataReader ExecuteReader(string storedProcedureName, params object[] parameterValues) {
			return Retry(() => {
				var retVal = m_DB.ExecuteReader(storedProcedureName, parameterValues);
				return retVal;
			});
		}

		/// <summary>
		/// Executes the command and
		/// returns the results as a IDataReader.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="commandText">The command text.</param>
		public IDataReader ExecuteReader(CommandType type, string commandText) {
			return Retry(() => {
				var retVal = m_DB.ExecuteReader(type, commandText);
				return retVal;
			});
		}

		/// <summary>
		/// Gets a parameter value.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the parameter.</param>
		/// <returns>The value of the parameter.</returns>
		public object GetParameterValue(DbCommand command, string name) {
			var retVal = m_DB.GetParameterValue(command, name);
			return retVal;
		}

		/// <summary>
		/// Gets the return value of a stored procedure.
		/// This seems to be a specific feature of SQL Server stored procedures.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <returns>The return value.</returns>
		public int GetReturnParameter(DbCommand command) {
			return (int)m_DB.GetParameterValue(command, "@ReturnValue");
		}

		/// <summary>
		/// Returns a command from a stored procedure name.
		/// </summary>
		/// <param name="storedProcedureName">The name of the stored procedure.</param>
		/// <returns>The DbCommand.</returns>
		public DbCommand GetStoredProcCommand(string storedProcedureName) {
			return m_DB.GetStoredProcCommand(storedProcedureName);
		}

		/// <summary>
		/// Gets the SQL string command.
		/// </summary>
		/// <param name="sqlString">The SQL string.</param>
		/// <returns></returns>
		public DbCommand GetSqlStringCommand(string sqlString) {
			return m_DB.GetSqlStringCommand(sqlString);
		}

		/// <summary>
		/// Retries the specified func.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="func">The func.</param>
		/// <returns></returns>
		private T Retry<T>(Func<T> func) {
			int count = 3;
			TimeSpan delay = TimeSpan.FromSeconds(1);
			while (true) {
				try {
					return func();
				}
				catch (SqlException e) {
					TracingService.Write(e);
					--count;
					bool retry =
						// Deadlock
						e.Number == 1205 ||
						// Timeout
						e.Number == -2;

					if (count <= 0 || !retry) throw;

					m_DB = InternalCreateDatabase();
					Thread.Sleep(delay);
				}
			}
		}
	}
}

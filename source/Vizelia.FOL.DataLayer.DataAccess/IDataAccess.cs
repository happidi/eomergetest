﻿using System;
using System.Data;
using System.Data.Common;

namespace Vizelia.FOL.DataLayer.Interfaces {

	/// <summary>
	/// Public interface for DataAccess.
	/// </summary>
	public interface IDataAccess {

		/// <summary>
		/// Adds a bool or bit param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		void AddInParameterBool(DbCommand command, string name, bool? value);

		/// <summary>
		/// Adds a byte array param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		void AddInParameterByteArray(DbCommand command, string name, byte[] value);


		/// <summary>
		/// Adds a datetime param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		void AddInParameterDateTime(DbCommand command, string name, DateTime? value);

		/// <summary>
		/// Adds a double param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		void AddInParameterDouble(DbCommand command, string name, double? value);

		/// <summary>
		/// Adds a decimal param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		void AddInParameterDecimal(DbCommand command, string name, decimal? value);

		/// <summary>
		/// Adds a guid param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		void AddInParameterGuid(DbCommand command, string name, Guid value);

		/// <summary>
		/// Adds a int param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		void AddInParameterInt(DbCommand command, string name, int? value);

		/// <summary>
		/// Adds a string param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param</param>
		/// <param name="value">The value of the param.</param>
		void AddInParameterString(DbCommand command, string name, string value);

		/// <summary>
		/// Adds a Table param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		/// <param name="typeName">Name of the type.</param>
		void AddInParameterTable(DbCommand command, string name, DataTable value, string typeName);

		/// <summary>
		/// Adds a xml param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the param.</param>
		/// <param name="value">The value of the param.</param>
		void AddInParameterXml(DbCommand command, string name, string value);

		/// <summary>
		/// Adds an out int param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the out param.</param>
		void AddOutParameterInt(DbCommand command, string name);

		/// <summary>
		/// Adds an out string param to a DbCommand.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the out param.</param>
		void AddOutParameterString(DbCommand command, string name);

		/// <summary>
		/// Adds a returnValue param to a DbCommand.
		/// This seems to be a SQL Server specific feature for stored procedures.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		void AddReturnParameter(DbCommand command);

		/// <summary>
		/// Executes the command and
		/// returns the results as a System.Data.DataSet.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <returns>System.Data.DataSet with results.</returns>
		DataSet ExecuteDataSet(DbCommand command);

		/// <summary>
		/// Executes the commandText interpreted as specified by the commandType and
		/// returns the results in a new System.Data.DataSet.
		/// </summary>
		/// <param name="storedProcedureName">The stored procedure to execute.</param>
		/// <param name="parameterValues">
		/// An array of paramters to pass to the stored procedure. The parameter values
		/// must be in call order as they appear in the stored procedure.
		/// </param>
		/// <returns>A System.Data.DataSet with the results of the storedProcedureName.</returns>
		DataSet ExecuteDataSet(string storedProcedureName, params object[] parameterValues);

		/// <summary>
		/// Executes the commandText interpreted as specified by the commandType and
		/// returns the results as a System.Data.DataSet.
		/// </summary>
		/// <param name="commandType">One of the System.Data.CommandType values.</param>
		/// <param name="commandText">The command text to execute.</param>
		/// <returns>A System.Data.DataSet with the results of the commandText.</returns>
		DataSet ExecuteDataSet(CommandType commandType, string commandText);

		/// <summary>
		/// Executes the command and returns an int.
		/// </summary>
		/// <param name="command">The command that contains the query to execute.</param>
		/// <returns>The result int of the query.</returns>
		int ExecuteNonQuery(DbCommand command);

		/// <summary>
		/// Executes the commandText interpreted as specified by the commandType and
		/// returns the results in a new System.Data.DataSet.
		/// </summary>
		/// <param name="storedProcedureName">The stored procedure to execute.</param>
		/// <param name="parameterValues">
		/// An array of paramters to pass to the stored procedure. The parameter values
		/// must be in call order as they appear in the stored procedure.
		/// </param>
		/// <returns>A System.Data.DataSet with the results of the storedProcedureName.</returns>
		int ExecuteNonQuery(string storedProcedureName, params object[] parameterValues);

		/// <summary>
		/// Executes the command and
		/// returns the results as a IDataReader.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <returns>IDataReader instance.</returns>
		IDataReader ExecuteReader(DbCommand command);

		/// <summary>
		/// Executes the storedProcedureName with the given parameterValues and returns
		/// an System.Data.IDataReader through which the result can be read.  It is the
		/// responsibility of the caller to close the connection and reader when finished.
		/// </summary>
		/// <param name="storedProcedureName">The command that contains the query to execute.</param>
		/// <param name="parameterValues">
		/// An array of paramters to pass to the stored procedure. The parameter values
		/// must be in call order as they appear in the stored procedure.
		/// </param>
		/// <returns>An System.Data.IDataReader object.</returns>
		IDataReader ExecuteReader(string storedProcedureName, params object[] parameterValues);

		/// <summary>
		/// Executes the command and
		/// returns the results as a IDataReader.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="commandText">The command text.</param>
		IDataReader ExecuteReader(CommandType type, string commandText);

		/// <summary>
		/// Gets a parameter value.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <param name="name">The name of the parameter.</param>
		/// <returns>The value of the parameter.</returns>
		object GetParameterValue(DbCommand command, string name);

		/// <summary>
		/// Gets the return value of a stored procedure.
		/// This seems to be a specific feature of SQL Server stored procedures.
		/// </summary>
		/// <param name="command">The DbCommand.</param>
		/// <returns>The return value.</returns>
		int GetReturnParameter(DbCommand command);

		/// <summary>
		/// Returns a command from a stored procedure name.
		/// </summary>
		/// <param name="storedProcedureName">The name of the stored procedure.</param>
		/// <returns>The DbCommand.</returns>
		DbCommand GetStoredProcCommand(string storedProcedureName);

		/// <summary>
		/// Gets the SQL string command.
		/// </summary>
		/// <param name="sqlString">The SQL string.</param>
		/// <returns></returns>
		DbCommand GetSqlStringCommand(string sqlString);
	}
}

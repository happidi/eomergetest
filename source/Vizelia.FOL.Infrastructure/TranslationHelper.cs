﻿using System;
using System.Web;

namespace Vizelia.FOL.Infrastructure {
	/// <summary>
	/// A helper for performing translations. Used by both the application and the builders.
	/// </summary>
	public static class TranslationHelper {
		private const string google_accolade_left = "146tbOFvr2lQR9yOdUWgr4";
		private const string google_accolade_right = "3SvprnOffA0wbg5dBvdFbe";
		private const string google_api_key = "AIzaSyCUq0EGiRBAN44Dwbtqxxz_gsWsegb1URk";
		/// <summary>
		/// Encodes a string for google translation service before it is sent.
		/// </summary>
		/// <param name="value">The string to encode.</param>
		/// <returns>The string encoded.</returns>
		private static string GoogleEncodeString(string value) {
			value = value.Replace("{", google_accolade_left);
			value = value.Replace("}", google_accolade_right);
			return value;
		}

		/// <summary>
		/// Decodes a string from google translation service after it has been received.
		/// </summary>
		/// <param name="value">The string to decode.</param>
		/// <returns>The string decoded.</returns>
		private static string GoogleDecodeString(string value) {
			value = value.Replace(google_accolade_left, "{");
			value = value.Replace(google_accolade_right, "}");
			value = HttpUtility.HtmlDecode(value);
			return value;
		}

		/// <summary>
		/// Translate Text using Google Translate V1.
		/// </summary>
		/// <param name="input">The string to translate.</param>
		/// <param name="languageFrom">2 letters Language source. e.g. "en , da" language pair means to translate from English to Danish</param>
		/// <param name="languageTo">2 letters language destination. e.g. "en , da" language pair means to translate from English to Danish</param>
		/// <returns>Translated to String</returns>
		[Obsolete]
		private static string TranslateTextV1(string input, string languageFrom, string languageTo) {
			var languagePair = languageFrom + '|' + languageTo;

			// Encoding string before calling translation service.
			input = GoogleEncodeString(input);
			string result;

			WebPost webPost = new WebPost("http://ajax.googleapis.com/ajax/services/language/translate");
			webPost.TimeOut = 10000;
			webPost.Type = WebPost.PostTypeEnum.Post;
			webPost.PostItems.Add("v", "1.0");
			webPost.PostItems.Add("q", input);
			webPost.PostItems.Add("langpair", languagePair);
			result = webPost.Post();

			// Deserializing the json result.
			GoogleTranslationV1 translation = SerializationHelper.DeserializeJson<GoogleTranslationV1>(result);
			if (translation.responseStatus != "200")
				throw new ArgumentException(translation.responseDetails + " " + languagePair);
			result = translation.responseData.translatedText;
			// Decoding string after calling translation service.
			result = GoogleDecodeString(result);
			return result;
		}

		/// <summary>
		/// Translate Text using Google Translate V2.
		/// </summary>
		/// <param name="input">The string to translate.</param>
		/// <param name="languageFrom">2 letters Language source. e.g. "en , da" language pair means to translate from English to Danish</param>
		/// <param name="languageTo">2 letters language destination. e.g. "en , da" language pair means to translate from English to Danish</param>
		/// <returns>Translated to String</returns>
		public static string TranslateText(string input, string languageFrom, string languageTo) {
			// Fix for Chinese (Simplified)
			languageTo = TransformForGoogle(languageTo);
			languageFrom = TransformForGoogle(languageFrom);

			if (languageTo == languageFrom)
				return input;

			var languagePair = languageFrom + '|' + languageTo;

			// Encoding string before calling translation service.
			input = GoogleEncodeString(input);
			string result;

			WebPost webPost = new WebPost("https://www.googleapis.com/language/translate/v2");
			webPost.TimeOut = 10000;
			webPost.Type = WebPost.PostTypeEnum.Get;
			webPost.PostItems.Add("key", google_api_key);
			webPost.PostItems.Add("prettyprint", "true");
			webPost.PostItems.Add("q", input);
			webPost.PostItems.Add("source", languageFrom);
			webPost.PostItems.Add("target", languageTo);

			result = webPost.Post();

			// Deserializing the json result.
			GoogleTranslationV2 translation = SerializationHelper.DeserializeJson<GoogleTranslationV2>(result);
			if (translation == null ) {
				throw new ArgumentException(result + " " + languagePair);
			}
			if(translation.data.translations == null || translation.data.translations.Count <= 0) {
			throw new ArgumentException(result + " " + languagePair);
			}
			result = translation.data.translations[0].translatedText;
			// Decoding string after calling translation service.
			result = GoogleDecodeString(result);
			return result;
		}

		/// <summary>
		/// Transforms for google.
		/// </summary>
		/// <param name="languageTo">The language to.</param>
		/// <returns></returns>
		private static string TransformForGoogle(string languageTo) {
			if (languageTo.ToLower().Equals("zh")) {
				languageTo = "zh-CN";
			}
			else {
				languageTo = languageTo.Substring(0, 2);
			}
			return languageTo;
		}
	}
}

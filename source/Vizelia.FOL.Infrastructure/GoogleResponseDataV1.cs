using System.Runtime.Serialization;

namespace Vizelia.FOL.Infrastructure
{
	/// <summary>
	/// Business entity for response data coming from google translation service V1.
	/// </summary>
	[DataContract]
	internal class GoogleResponseDataV1 {
		/// <summary>
		/// The translated text.
		/// </summary>
		[DataMember]
		public string translatedText { get; set; }
	}
}
﻿using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Vizelia.FOL.Infrastructure {

	/// <summary>
	/// Business entity for language messages coming from google translation service V1.
	/// </summary>
	[DataContract]
	internal class GoogleTranslationV1 {

		/// <summary>
		/// The response data.
		/// </summary>
		[DataMember]
		public GoogleResponseDataV1 responseData { get; set; }

		/// <summary>
		/// The response details.
		/// </summary>
		[DataMember]
		public string responseDetails { get; set; }

		/// <summary>
		/// The response status.
		/// </summary>
		[DataMember]
		public string responseStatus { get; set; }

	}

	

}

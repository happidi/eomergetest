﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace Vizelia.FOL.Infrastructure {
	/// <summary>
	/// A helper for executing dos commands.
	/// </summary>
	public static class CmdHelper {

		/// <summary>
		/// Executes a DOS command.
		/// You can execute separate commands using the following syntax : ExecuteCmd("cd d &amp; dir");
		/// </summary>
		/// <param name="command">The command to execute.</param>
		/// <param name="shouldFlushOutputToConsole">if set to <c>true</c> [should flush output to console].</param>
		/// <returns></returns>
		public static ExecutionResult ExecuteCmd(string command, bool shouldFlushOutputToConsole = false) {
			var procStartInfo = new ProcessStartInfo("cmd", "/c " + command);
			return ExecuteProcess(procStartInfo, shouldFlushOutputToConsole);
		}

		/// <summary>
		/// Executes the batch.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <returns></returns>
		public static ExecutionResult ExecuteBatch(string fileName) {
			var procStartInfo = new ProcessStartInfo(fileName);
			return ExecuteProcess(procStartInfo);
		}

		/// <summary>
		/// Executes the process. (Internal).
		/// </summary>
		/// <param name="procStartInfo">The proc start info.</param>
		/// <param name="shouldFlushOutputToConsole">if set to <c>true</c> [should flush output to console].</param>
		/// <returns></returns>
		private static ExecutionResult ExecuteProcess(ProcessStartInfo procStartInfo, bool shouldFlushOutputToConsole = false) {
			// The following commands are needed to redirect the standard output.
			// This means that it will be redirected to the Process.StandardOutput StreamReader.
			procStartInfo.RedirectStandardOutput = true;
			procStartInfo.RedirectStandardError = true;
			procStartInfo.UseShellExecute = false;
			// Do not create the black window.
			procStartInfo.CreateNoWindow = true;
			// Now we create a process, assign its ProcessStartInfo and start it
			var proc = new Process {StartInfo = procStartInfo};

			var outputs = new StringBuilder();
			var errors = new StringBuilder();

			if (shouldFlushOutputToConsole) {
				proc.ErrorDataReceived += (sender, args) => {
					Console.WriteLine(args.Data);
					errors.AppendLine(args.Data);
				};
				proc.OutputDataReceived += (sender, args) => {
					Console.WriteLine(args.Data);
					outputs.AppendLine(args.Data);
				};
			}

			proc.Start();
			
			if (shouldFlushOutputToConsole) {
				proc.BeginErrorReadLine();
				proc.BeginOutputReadLine();
				proc.WaitForExit();
			}
			else {
				outputs.AppendLine(proc.StandardOutput.ReadToEnd());
				errors.AppendLine(proc.StandardError.ReadToEnd());
			}
			
			// Display the command output.
			return new ExecutionResult { Output = outputs.ToString(), Error = errors.ToString() };
		}
		
		/// <summary>
		/// Writes to console.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="type">The type.</param>
		/// <param name="newLine">if set to <c>true</c> [new line].</param>
		/// <param name="silentMode">if set to <c>true</c> [silent mode].</param>
		public static void WriteToConsole(string input, MessageType type = MessageType.Message, bool newLine = true, bool silentMode = false) {
			if (silentMode && (type != MessageType.Error)) return;

			var originalColor = Console.ForegroundColor;
			Console.ForegroundColor = GetColorByMessageType(type);
			Console.Write(input + (newLine ? Environment.NewLine : string.Empty));
			Console.ForegroundColor = originalColor;
		}

		/// <summary>
		/// Writes to console.
		/// </summary>
		/// <param name="msg">The MSG.</param>
		/// <param name="silentMode">if set to <c>true</c> [silent mode].</param>
		public static void WriteToConsole(OutputMessage msg, bool silentMode = false) {
			if (silentMode && (msg.MessageType != MessageType.Error)) return;

			var originalColor = Console.ForegroundColor;
			Console.ForegroundColor = GetColorByMessageType(msg.MessageType);
			Console.Write(msg.Message + (msg.NewLine ? Environment.NewLine : string.Empty));
			Console.ForegroundColor = originalColor;
		}

		/// <summary>
		/// Gets the type of the color by message.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		private static ConsoleColor GetColorByMessageType(MessageType type) {
			switch (type) {
				case MessageType.Message: return ConsoleColor.Gray;
				case MessageType.Error: return ConsoleColor.Red;
				case MessageType.Success: return ConsoleColor.Green;
				case MessageType.Warning: return ConsoleColor.Yellow;
				default: return ConsoleColor.Gray;
			}
		}
	}

	/// <summary>
	/// Message type for cmd helper console write
	/// </summary>
	public enum MessageType {
		/// <summary>
		/// Simple message
		/// </summary>
		Message,
		/// <summary>
		/// Error message
		/// </summary>
		Error,
		/// <summary>
		/// Success message
		/// </summary>
		Success,
		/// <summary>
		/// Warning message
		/// </summary>
		Warning
	}

	/// <summary>
	/// Message with Message type combination
	/// </summary>
	public class OutputMessage {
		/// <summary>
		/// Gets or sets a value indicating whether [new line].
		/// </summary>
		/// <value>
		///   <c>true</c> if [new line]; otherwise, <c>false</c>.
		/// </value>
		public bool NewLine { get; set; }
		/// <summary>
		/// Gets or sets the type of the message.
		/// </summary>
		/// <value>
		/// The type of the message.
		/// </value>
		public MessageType MessageType { get; set; }
		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		/// <value>
		/// The message.
		/// </value>
		public string Message { get; set; }
	}

	/// <summary>
	/// Execution result outputs
	/// </summary>
	public class ExecutionResult {
		/// <summary>
		/// Gets or sets the output.
		/// </summary>
		/// <value>
		/// The output.
		/// </value>
		public string Output { get; set; }
		/// <summary>
		/// Gets or sets the error.
		/// </summary>
		/// <value>
		/// The error.
		/// </value>
		public string Error { get; set; }
	}
}

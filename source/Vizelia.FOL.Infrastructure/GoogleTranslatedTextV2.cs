﻿using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Vizelia.FOL.Infrastructure {


	/// <summary>
	/// Business entity for tranlatedText section coming from google translation service V2.
	/// </summary>
	[DataContract]
	internal class GoogleTranslatedTextV2 {
		/// <summary>
		/// Gets or sets the translated text.
		/// </summary>
		/// <value>The translated text.</value>
		[DataMember]
		public string translatedText { get; set; }
	}
}

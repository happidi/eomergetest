﻿using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Vizelia.FOL.Infrastructure {

	/// <summary>
	/// Business entity for language messages coming from google translation service V2.
	/// </summary>
	[DataContract]
	internal class GoogleTranslationV2 {

		/// <summary>
		/// Gets or sets the data.
		/// </summary>
		/// <value>The data.</value>
		[DataMember]
		public GoogleTranslationDataV2 data { get; set; }


	}
}

﻿using System.Configuration;
using System.Web.Configuration;

namespace Vizelia.FOL.Infrastructure {
	/// <summary>
	/// Configuration helper class
	/// </summary>
	public static class ConfigurationHelper {
		/// <summary>
		/// Gets the web configuration.
		/// </summary>
		/// <param name="site">The site.</param>
		/// <returns></returns>
		public static Configuration GetWebConfiguration(string site) {
			var vdm = new VirtualDirectoryMapping(site, true);
			var wcfm = new WebConfigurationFileMap();
			wcfm.VirtualDirectories.Add("/", vdm);
			var configuration = WebConfigurationManager.OpenMappedWebConfiguration(wcfm, "/");
			return configuration;
		}
	}
}

﻿using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Vizelia.FOL.Infrastructure {

	/// <summary>
	/// Business entity for data section coming from google translation service V2.
	/// </summary>
	[DataContract]
	internal class GoogleTranslationDataV2 {
		/// <summary>
		/// Gets or sets the translations.
		/// </summary>
		/// <value>The translations.</value>
		[DataMember]
		public List<GoogleTranslatedTextV2> translations { get; set; }

	}
}

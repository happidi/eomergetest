﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Script.Serialization;

namespace Vizelia.FOL.Infrastructure {
	/// <summary>
	/// An infrastructural helper for common features, used both in the application and in builders that run in compile-time.
	/// </summary>
	public static class SerializationHelper {
		/// <summary>
		/// A helper method for serializing object to json strings.
		/// </summary>
		/// <param name="obj">The object to serialize.</param>
		/// <param name="objectType">The type of the object to dehydrate.</param>
		/// <returns>The json string value of the object.</returns>
		public static string SerializeJson(object obj, Type objectType) {
			/*
			var serializer = new DataContractJsonSerializer(objectType);
			string json;
			using (var stream = new MemoryStream()) {
				serializer.WriteObject(stream, obj);
				json = Encoding.UTF8.GetString(stream.ToArray());
			}
			return json;
		    */
			JavaScriptSerializer serializer = new JavaScriptSerializer();
			string json = serializer.Serialize(obj);
			return json;
		}

		/// <summary>
		/// A helper method for serializing object to json strings.
		/// </summary>
		/// <param name="obj">The object to serialize.</param>
		/// <returns>The json string value of the object.</returns>
		public static string SerializeJson(object obj) {
			return SerializeJson(obj, obj.GetType());
		}

		/// <summary>
		/// A helper method for deserializing json string .
		/// </summary>
		/// <param name="json">The json string value.</param>
		/// <param name="objectType">The type of the object to rehydrate.</param>
		/// <returns>The object rehydrated from the json string</returns>
		public static object DeserializeJson(string json, Type objectType) {
			//try {
			//    // special case for datetime
			//    if (objectType == typeof(DateTime))
			//        json = "\"" + json.Replace("/", "\\/") + "\"";

			//    var serializer = new DataContractJsonSerializer(objectType);
			//    using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(json))) {
			//        return serializer.ReadObject(stream);
			//    }
			//}
			//// in case the json string passed is a simple type not encoded we just return it as is.
			//catch {
			//    return json;
			//}
			if (objectType == typeof(string))
				return json;
			JavaScriptSerializer serializer = new JavaScriptSerializer();
			var retVal = serializer.Deserialize(json, objectType);
			return retVal;
		}


		/// <summary>
		/// A helper method for deserializing json strings. 
		/// </summary>
		/// <typeparam name="T">Genric type.</typeparam>
		/// <param name="json">The json string value.</param>
		/// <returns>The object rehydrated from the json string</returns>
		public static T DeserializeJson<T>(string json) {
			//return (T)DeserializeJson(json, typeof(T));
			JavaScriptSerializer serializer = new JavaScriptSerializer();
			var retVal = serializer.Deserialize<T>(json);
			return retVal;
		}

	}
}
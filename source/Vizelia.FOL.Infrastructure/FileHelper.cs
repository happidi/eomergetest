﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Vizelia.FOL.Infrastructure {
	/// <summary>
	/// A helper for files and directories.
	/// </summary>
	public static class FileHelper {

		/// <summary>
		/// Deletes the directory.
		/// </summary>
		/// <param name="target_dir">The target_dir.</param>
		public static void DirectoryDelete2(string target_dir) {
			DeleteDirectoryFiles(target_dir);
			while (Directory.Exists(target_dir)) {
				DeleteDirectoryDirs(target_dir);
			}
		}

		private static void DeleteDirectoryDirs(string target_dir) {
			Thread.Sleep(100);

			if (Directory.Exists(target_dir)) {

				string[] dirs = Directory.GetDirectories(target_dir);

				if (dirs.Length == 0)
					Directory.Delete(target_dir, false);
				else
					foreach (string dir in dirs)
						DeleteDirectoryDirs(dir);
			}
		}

		private static void DeleteDirectoryFiles(string target_dir) {
			string[] files = Directory.GetFiles(target_dir);
			string[] dirs = Directory.GetDirectories(target_dir);

			foreach (string file in files) {
				File.SetAttributes(file, FileAttributes.Normal);
				File.Delete(file);
			}

			foreach (string dir in dirs) {
				DeleteDirectoryFiles(dir);
			}
		}


		/// <summary>
		/// Directories the delete.
		/// </summary>
		/// <param name="sourceDirName">Name of the source dir.</param>
		public static bool DirectoryDelete(string sourceDirName) {
			if (Directory.Exists(sourceDirName)) {

				try {
					Directory.Delete(sourceDirName, false);
				}
				catch (IOException) {
					Thread.Sleep(200);
					Directory.Delete(sourceDirName, false);
				}

				//string[] files = Directory.GetFiles(sourceDirName);
				//string[] dirs = Directory.GetDirectories(sourceDirName);
				//foreach (string file in files) {
				//    File.SetAttributes(file, FileAttributes.Normal);
				//    File.Delete(file);
				//}
				//foreach (string dir in dirs) {
				//    DirectoryDelete(dir);
				//}
				//Directory.Delete(sourceDirName, false);

				return true;
			}
			return false;
		}

		/// <summary>
		/// Copies the directory.
		/// </summary>
		/// <param name="sourceDirName">Name of the source dir.</param>
		/// <param name="destDirName">Name of the dest dir.</param>
		/// <param name="copySubDirs">if set to <c>true</c> [copy sub dirs].</param>
		public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs = true) {
			DirectoryInfo dir = new DirectoryInfo(sourceDirName);
			DirectoryInfo[] dirs = dir.GetDirectories();
			// If the source directory does not exist, throw an exception.
			if (!dir.Exists) {
				throw new DirectoryNotFoundException("Source directory does not exist or could not be found: " + sourceDirName);
			}
			// If the destination directory does not exist, create it.
			if (!Directory.Exists(destDirName)) {
				Directory.CreateDirectory(destDirName);
			}
			// Get the file contents of the directory to copy.
			FileInfo[] files = dir.GetFiles();
			foreach (FileInfo file in files) {
				// Create the path to the new copy of the file.
				string temppath = Path.Combine(destDirName, file.Name);
				// Copy the file.
				file.CopyTo(temppath, false);
			}
			// If copySubDirs is true, copy the subdirectories.
			if (!copySubDirs) return;
			foreach (DirectoryInfo subdir in dirs) {
				// Create the subdirectory.
				string temppath = Path.Combine(destDirName, subdir.Name);
				// Copy the subdirectories.
				DirectoryCopy(subdir.FullName, temppath);
			}
		}

	}
}

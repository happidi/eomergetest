﻿using System;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.RegularExpressions;
using System.Reflection;

namespace Vizelia.FOL.Infrastructure {

	/// <summary>
	/// A helper for several WCF methods that allow us no to use reflection.
	/// </summary>
	public static class WCFHelper {

		private static bool IsValidStart(char c) {
			return (Char.GetUnicodeCategory(c) != UnicodeCategory.DecimalDigitNumber);
		}


		private static void AddToNamespace(StringBuilder builder, string fragment) {
			if (fragment != null) {
				bool flag = true;
				for (int i = 0; (i < fragment.Length) && (builder.Length < 0x1ff); i++) {
					char c = fragment[i];
					if (IsValid(c)) {
						if (flag && !IsValidStart(c)) {
							builder.Append("_");
						}
						builder.Append(c);
						flag = false;
					}
					else if ((((c == '.') || (c == '/')) || (c == ':')) && ((builder.Length == 1) || ((builder.Length > 1) && (builder[builder.Length - 1] != '.')))) {
						builder.Append('.');
						flag = true;
					}
				}
			}
		}

		private static bool IsValid(char c) {
			switch (Char.GetUnicodeCategory(c)) {
				case UnicodeCategory.UppercaseLetter:
				case UnicodeCategory.LowercaseLetter:
				case UnicodeCategory.TitlecaseLetter:
				case UnicodeCategory.ModifierLetter:
				case UnicodeCategory.OtherLetter:
				case UnicodeCategory.NonSpacingMark:
				case UnicodeCategory.SpacingCombiningMark:
				case UnicodeCategory.DecimalDigitNumber:
				case UnicodeCategory.ConnectorPunctuation:
					return true;
			}
			return false;
		}

		/// <summary>
		/// A helper function to convert a wcf namespace into it's javascript notation
		/// </summary>
		/// <param name="ns">The namespace</param>
		/// <returns>The converted namespace, if namespace is null returns tempuri.org</returns>
		public static string GetClientTypeNamespace(string ns) {
			if (ns == null) {
				ns = "http://tempuri.org/";
			}
			if (String.IsNullOrEmpty(ns)) {
				return String.Empty;
			}

			Uri result = null;
			StringBuilder builder = new StringBuilder();
			if (Uri.TryCreate(ns, UriKind.RelativeOrAbsolute, out result)) {
				if (!result.IsAbsoluteUri) {
					AddToNamespace(builder, result.OriginalString);
				}
				else {
					string absoluteUri = result.AbsoluteUri;
					if (absoluteUri.StartsWith("http://schemas.datacontract.org/2004/07/", StringComparison.Ordinal)) {
						AddToNamespace(builder, absoluteUri.Substring("http://schemas.datacontract.org/2004/07/".Length));
					}
					else {
						string host = result.Host;
						if (host != null) {
							AddToNamespace(builder, host);
						}
						string pathAndQuery = result.PathAndQuery;
						if (pathAndQuery != null) {
							AddToNamespace(builder, pathAndQuery);
						}
					}
				}
			}
			if (builder.Length == 0) {
				return String.Empty;
			}
			int length = builder.Length;
			if (builder[builder.Length - 1] == '.') {
				length--;
			}
			length = Math.Min(0x1ff, length);
			return builder.ToString(0, length);
		}


		private static string ProcessAttributes(Match match, IDictionary attribs) {
			string str = String.Empty;
			CaptureCollection captures = match.Groups["attrname"].Captures;
			CaptureCollection captures2 = match.Groups["attrval"].Captures;
			CaptureCollection captures3 = match.Groups["equal"].Captures;
			for (int i = 0; i < captures.Count; i++) {
				string key = captures[i].ToString();
				string str3 = captures2[i].ToString();
				bool flag = captures3[i].ToString().Length > 0;
				if (key != null) {
					if (!flag && (i == 0)) {
						str = key;
					}
					else {
						try {
							if (attribs != null) {
								attribs.Add(key, str3);
							}
						}
						catch (ArgumentException) {
							throw;
						}
					}
				}
			}
			return str;
		}

		/// <summary>
		/// A helper function to retrieve the attribute type Service from an .svc file
		/// </summary>
		/// <param name="serviceText">The content of the .svc file</param>
		/// <returns></returns>
		public static string GetSvcTypeFromContent(string serviceText) {
			string result = null;
			if (serviceText.IndexOf('>') == -1) {
				throw new ArgumentException("Hosting_BuildProviderDirectiveEndBracketMissing", "serviceText");
			}
			SimpleDirectiveRegex directiveRegex = new SimpleDirectiveRegex();
			Match match;
			match = directiveRegex.Match(serviceText, 0);
			if (match.Success) {
				IDictionary attribs = CollectionsUtil.CreateCaseInsensitiveSortedList();
				string directiveName = ProcessAttributes(match, attribs);
				result = (string)attribs["Service"];
			}
			return result;
		}

		/// <summary>
		/// A helper function to convert a DotNet type to a Javascript type.
		/// </summary>
		/// <param name="t">The DotNet type.</param>
		/// <returns>The Javascript type.</returns>
		public static string GetJavascriptType(Type t) {
			if (t.IsGenericType) {
				Type genericType = t.GetGenericTypeDefinition();
				if (genericType.FullName.Contains("System.Nullable"))
					return GetJavascriptType(t.GetGenericArguments().First());
				else if (genericType.FullName.Contains("System.Collections.Generic.List"))
					return GetJavascriptType(t.GetGenericArguments().First()) + "[]";
				else
					return GetObjectTypeName(t.FullName);
			}
			else {
				TypeCode typeCode = Type.GetTypeCode(t);
				switch (typeCode) {
					case TypeCode.Boolean:
						return "Boolean";

					case TypeCode.Byte:
					case TypeCode.Decimal:
					case TypeCode.Double:
					case TypeCode.Int16:
					case TypeCode.Int32:
					case TypeCode.Int64:
					case TypeCode.SByte:
					case TypeCode.Single:
					case TypeCode.UInt16:
					case TypeCode.UInt32:
					case TypeCode.UInt64:
						return "Number";

					case TypeCode.String:
					case TypeCode.Char:
						return "String";

					case TypeCode.DateTime:
						return "Date";

					case TypeCode.Object:
						return GetObjectTypeName(t.FullName);
					default:
						return GetObjectTypeName(t.FullName);
				}
			}
		}

		/// <summary>
		/// Gets the object type name
		/// </summary>
		/// <param name="fullName">The fullName.</param>
		/// <returns></returns>
		private static string GetObjectTypeName(string fullName) {
			if (!String.IsNullOrEmpty(fullName))
				return fullName.Replace("Vizelia.FOL.BusinessEntities", "Viz.BusinessEntity");
			else
				return "Object";
		}

		/// <summary>
		/// A helper function that retreives the property id of a business entity.
		/// </summary>
		/// <param name="t">The type of the business entity.</param>
		/// <returns>The property id name or null if not defined.</returns>
		public static string GetKeyAttribute(Type t) {
			var att = (from attribute in Attribute.GetCustomAttributes(t)
					   where attribute.ToString() == "Vizelia.FOL.Common.KeyAttribute"
					   select attribute).FirstOrDefault();

			if (att != null) {
				var val = att.GetType().InvokeMember("PropertyNameId", System.Reflection.BindingFlags.GetProperty, null, att, null);
				if (val != null)
					return val.ToString();
			}
			return "";
		}

		/// <summary>
		/// Gets the assembly GUID.
		/// </summary>
		/// <param name="ass">The ass.</param>
		/// <returns></returns>
		public static Guid GetAssemblyGuid(Assembly ass) {
			return ass.ManifestModule.ModuleVersionId;
		}

		/// <summary>
		/// Gets the valid urls for starting a new session.
		/// </summary>
		/// <value>The valid urls.</value>
		public static string[] ValidUrls {
			get {
				return new[] { ".aspx", "Public.svc" };
			}
		}



	}
}

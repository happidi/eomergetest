﻿using System;
using System.Collections.Generic;
using System.Linq;
using DDay.iCal;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests
{
    public class EnergyAggregatorBusinessLayerCalendarTests : BaseEnergyAggregatorBusinessLayerTests
    {
		private const string const_meter_data_file_name =
		"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.Common.MonthlyMeterData.txt";
		const string const_utc_time_zone_id = "UTC";

	    private CalendarEventCategory m_TestCalendarCategory;
        const string const_virtual_meter_key = "#66";

		[SetUp]
	    public override void SetUp()
	    {
			base.SetUp();

		    m_TestCalendarCategory = new CalendarEventCategory()
		    {
			    KeyCalendarEventCategory = "1",
				LocalId =  "Calendar Test Category"
		    };
		
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Seconds;// Set the Time aggregation to the lowest so data will not be affected by aggregation
	    }

	    protected override string TestMeterDataFilePath
	    {
		    get { return const_meter_data_file_name; }
	    }

        [Test]
		public void ChartProcess_IncludeCalendarEventForLocation_SerieWithPointsInsideCalendarEventRange() {
            //Arrange
            SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

            //Create Calendar event that falls inside the range of the meter's data
			int middleIndex = testMeterData.Keys.Count / 2;
			CalendarEvent calendarEvent = CreateTestCalendarEvent(DefaultKeyLocation,testMeterData.Keys[1], testMeterData.Keys[middleIndex]);
			AddCalendarEventToCalendars(calendarEvent);
			
			TestDataContainer.Chart.CalendarEventCategory = m_TestCalendarCategory;
            TestDataContainer.Chart.CalendarMode = ChartCalendarMode.Include;
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
			//Validate that all of the result series points fall inside the range of the defined calendar
	        var resultPoints = resultSeries[0].Points;
			Assert.IsTrue(resultPoints.Count  > 0);
			foreach (DataPoint dataPoint in resultPoints)
            {
                Assert.IsTrue(dataPoint.XDateTime >= calendarEvent.StartDate &&
                              dataPoint.XDateTime <= calendarEvent.EndDate);
            }
        }

        [Test]
		public void ChartProcess_ExcludeCalendarEventForLocation_SerieWithPointsOutsideCalendarEventRange()
        {
            //Arrange
            SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

            //Create Calendar event that falls inside the range of the meter's data
			int middleIndex = testMeterData.Keys.Count / 2;
			CalendarEvent calendarEvent = CreateTestCalendarEvent(DefaultKeyLocation, testMeterData.Keys[1], testMeterData.Keys[middleIndex]);
			AddCalendarEventToCalendars(calendarEvent);

			TestDataContainer.Chart.CalendarEventCategory = m_TestCalendarCategory;
            TestDataContainer.Chart.CalendarMode = ChartCalendarMode.Exclude;
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
			var resultPoints = resultSeries[0].Points;
			Assert.IsTrue(resultPoints.Count > 0);
			foreach (DataPoint dataPoint in resultPoints)
            {
                Assert.IsTrue(dataPoint.XDateTime <= calendarEvent.StartDate ||
                              dataPoint.XDateTime >= calendarEvent.EndDate);
            }
        }

        [Test]
		public void ChartProcess_SplitByEventCalendarEventForLocation_SeriesWithDatesForeachCalendarEvent() {
            //Arrange
            SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

            //Create Calendar events that fall inside the range of the meter's data
	        int middleIndex = testMeterData.Keys.Count / 2;
			CalendarEvent firstCalendarEvent = CreateTestCalendarEvent(DefaultKeyLocation, testMeterData.Keys[1], testMeterData.Keys[middleIndex - 1]);
			CalendarEvent secondCalendarEvent = CreateTestCalendarEvent(DefaultKeyLocation, testMeterData.Keys[middleIndex + 1], testMeterData.Keys[testMeterData.Keys.Count - 1]);
			AddCalendarEventToCalendars(firstCalendarEvent);
			AddCalendarEventToCalendars(secondCalendarEvent);

			TestDataContainer.Chart.CalendarEventCategory = m_TestCalendarCategory;
			TestDataContainer.Chart.CalendarMode = ChartCalendarMode.SplitByEvent;
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var firstCalendarSerie = resultSeries.First(x => x.Name.Contains(firstCalendarEvent.Title));
            var secondCalendarSerie = resultSeries.First(x => x.Name.Contains(secondCalendarEvent.Title));
            
			//Validate that there are 2 generated series, one for each calendar event with dates in each calendar event
			Assert.AreEqual(resultSeries.Count, 2);
			Assert.IsTrue(firstCalendarSerie.Points.Count > 0);
			Assert.IsTrue(secondCalendarSerie.Points.Count > 0);
			foreach (DataPoint dataPoint in firstCalendarSerie.Points)
            {
                Assert.IsTrue(dataPoint.XDateTime >= firstCalendarEvent.StartDate &&
                              dataPoint.XDateTime <= firstCalendarEvent.EndDate);
            }

			foreach (DataPoint dataPoint in secondCalendarSerie.Points)
            {
                Assert.IsTrue(dataPoint.XDateTime >= secondCalendarEvent.StartDate &&
                              dataPoint.XDateTime <= secondCalendarEvent.EndDate);
            }

        }

        [Test]
		public void ChartProcess_IncludeCalendarEventForLocationWith2MetersOnTheSameLocation_SeriesForAllMetersWithPointsInsideCalendarEventRange() {
            
			//Arrange
			//Add another meter to the same location
	        var secondMeter = new Meter
	        {
		        KeyMeter = "#106",
		        KeyLocation = DefaultKeyLocation,
		        Factor = 1,
		        KeyClassificationItem = DefaultTestClassificationItemKey,
		        Name = "Test Site Second Meter"
	        };
            TestDataContainer.Meters.Add(secondMeter);

			SortedList<DateTime, MD> testDefaultMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Generate Meter Data for the second meter
			int keySecondMeter = MeterHelper.ConvertMeterKeyToInt(secondMeter.KeyMeter);
			var secondMeterData = CloneMeterData(testDefaultMeterData, keySecondMeter);
			TestDataContainer.MeterData[keySecondMeter] = secondMeterData;

			//Create Calendar event that falls inside the range of the meter's data
            SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];
			int middleIndex = testMeterData.Keys.Count / 2;
			CalendarEvent calendarEvent = CreateTestCalendarEvent(DefaultKeyLocation, testMeterData.Keys[1], testMeterData.Keys[middleIndex]);
            AddCalendarEventToCalendars(calendarEvent);

			TestDataContainer.Chart.CalendarEventCategory = m_TestCalendarCategory;
            TestDataContainer.Chart.CalendarMode = ChartCalendarMode.Include;
        
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
	        IEnumerable<DataPoint> allPoints = resultSeries.SelectMany(dataSerie => dataSerie.Points);
			Assert.IsTrue(allPoints.Any());
		
			//All series points must fall inside the calendar range
			foreach (DataPoint dataPoint in allPoints)
            {
                Assert.IsTrue(dataPoint.XDateTime >= calendarEvent.StartDate &&
                              dataPoint.XDateTime <= calendarEvent.EndDate);
            }

        }

        [Test]
		public void ChartProcess_ExcludeCalendarEventForLocationWith2MetersOnTheSameLocation_SeriesForAllMetersWithPointsOutsideCalendarEventRange() {
            //Arrange
            var secondMeter = new Meter
            {
                KeyMeter = "#106",
                KeyLocation = DefaultKeyLocation,
                Factor = 1.0,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Name = "Test Site Second Meter"
            };
            TestDataContainer.Meters.Add(secondMeter);

            SortedList<DateTime, MD> testDefaultMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Generate Meter Data for the second meter
			int keySecondMeter = MeterHelper.ConvertMeterKeyToInt(secondMeter.KeyMeter);
			var secondMeterData = CloneMeterData(testDefaultMeterData, keySecondMeter);
			TestDataContainer.MeterData[keySecondMeter] = secondMeterData;

            //Create Calendar event that falls inside the range of the meter's data
			int middleIndex = testDefaultMeterData.Keys.Count / 2;
			CalendarEvent calendarEvent = CreateTestCalendarEvent(DefaultKeyLocation, testDefaultMeterData.Keys[1], testDefaultMeterData.Keys[middleIndex]);
            AddCalendarEventToCalendars(calendarEvent);

			TestDataContainer.Chart.CalendarEventCategory = m_TestCalendarCategory;
            TestDataContainer.Chart.CalendarMode = ChartCalendarMode.Exclude;

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
			//All series points must fall outside the calendar range
			IEnumerable<DataPoint> allPoints = resultSeries.SelectMany(dataSerie => dataSerie.Points);
			Assert.IsTrue(allPoints.Any());

			foreach (DataPoint dataPoint in allPoints)
            {
                Assert.IsTrue(dataPoint.XDateTime <= calendarEvent.StartDate ||
                              dataPoint.XDateTime >= calendarEvent.EndDate);
            }
        }

        [Test]
		public void ChartProcess_SplitByEventCalendarEventForLocationWith2MetersOnTheSameLocation_SeriesForAllMetersWithDatesForeachCalendarEvent() {
            
			//Arrange
			//Add a second meter to the same location
			var secondMeter = new Meter
            {
                KeyMeter = "#106",
                KeyLocation = DefaultKeyLocation,
                Factor = 1.0,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Name = "Test Site Second Meter"
            };
			TestDataContainer.Meters.Add(secondMeter);

			SortedList<DateTime, MD> testDefaultMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Generate Meter Data for the second meter
			int keySecondMeter = MeterHelper.ConvertMeterKeyToInt(secondMeter.KeyMeter);
			var secondMeterData = CloneMeterData(testDefaultMeterData, keySecondMeter);
	        TestDataContainer.MeterData[keySecondMeter] = secondMeterData;

			//Create Calendar events that fall inside the range of the meter's data
			int middleIndex = testDefaultMeterData.Keys.Count / 2;
			CalendarEvent firstCalendarEvent = CreateTestCalendarEvent(DefaultKeyLocation, testDefaultMeterData.Keys[1], testDefaultMeterData.Keys[middleIndex - 1]);
			CalendarEvent secondCalendarEvent = CreateTestCalendarEvent(DefaultKeyLocation, testDefaultMeterData.Keys[middleIndex + 1], testDefaultMeterData.Keys[testDefaultMeterData.Keys.Count - 1]);
            AddCalendarEventToCalendars(firstCalendarEvent);
            AddCalendarEventToCalendars(secondCalendarEvent);

			TestDataContainer.Chart.CalendarEventCategory = m_TestCalendarCategory;
            TestDataContainer.Chart.CalendarMode = ChartCalendarMode.SplitByEvent;

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var firstCalendarSeries = resultSeries.Where(x => x.Name.Contains(firstCalendarEvent.Title));
            var secondCalendarSeries = resultSeries.Where(x => x.Name.Contains(secondCalendarEvent.Title));

			IEnumerable<DataPoint> allPointsFromFirstSeries = firstCalendarSeries.SelectMany(dataSerie => dataSerie.Points);
			Assert.IsTrue(allPointsFromFirstSeries.Any());
            foreach (DataPoint dataPoint in allPointsFromFirstSeries) {
                Assert.IsTrue(dataPoint.XDateTime >= firstCalendarEvent.StartDate &&
                              dataPoint.XDateTime <= firstCalendarEvent.EndDate);
            }

			IEnumerable<DataPoint> allPointsFromSecondSeries = secondCalendarSeries.SelectMany(dataSerie => dataSerie.Points);
			Assert.IsTrue(allPointsFromSecondSeries.Any());
			foreach (DataPoint dataPoint in allPointsFromSecondSeries) {
                Assert.IsTrue(dataPoint.XDateTime >= secondCalendarEvent.StartDate &&
                              dataPoint.XDateTime <= secondCalendarEvent.EndDate);
            }
        }

		[Test]
		public void ChartProcess_IncludeCalendarEventWithDailyRecurrenceForMeter_SeriesWithAllMeterValues() {

			//Arrange
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Create a calendar event with a daily recurrence pattern 
			var calendarEvent = CreateTestCalendarEvent(DefaultKeyMeterStr, testMeterData.Keys[0], testMeterData.Keys[0]);
			calendarEvent.IsAllDay = true;
			calendarEvent.RecurrencePattern = new CalendarRecurrencePattern() {
				Frequency = CalendarFrequencyType.Daily
			};
			AddCalendarEventToCalendars(calendarEvent);

			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			TestDataContainer.Chart.StartDate = calendarEvent.StartDate.GetValueOrDefault();
			TestDataContainer.Chart.CalendarEventCategory = m_TestCalendarCategory;
			TestDataContainer.Chart.CalendarMode = ChartCalendarMode.Include;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			//All the points should be returned because of the recurrence pattern

			Assert.IsTrue(resultSeries[0].Points.Count > 0);
			Assert.AreEqual(testMeterData.Values.Count, resultSeries[0].Points.Count);
		}

		[Test]
		public void ChartProcess_ExcludeCalendarEventWithDailyRecurrenceForMeter_SeriesWithNoMeterValues() {

			//Arrange
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Create an event with a daily recurrence pattern 
			var calendarEvent = CreateTestCalendarEvent(DefaultKeyMeterStr, testMeterData.Keys[0], testMeterData.Keys[0]);
			calendarEvent.IsAllDay = true;
			calendarEvent.RecurrencePattern = new CalendarRecurrencePattern() {
				Frequency = CalendarFrequencyType.Daily
			};

			AddCalendarEventToCalendars(calendarEvent);

			TestDataContainer.Chart.CalendarEventCategory = m_TestCalendarCategory;
			TestDataContainer.Chart.CalendarMode = ChartCalendarMode.Exclude;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			Assert.AreEqual(resultSeries[0].PointsWithValue.Count, 0);
		}

        [Test]
		public void ChartProcess_IncludeCalendarEventOnVirtualMeter_VirtualMeterSeriesWithPointsInsideCalndarRange() {
            
			//Arrange
            MeterOperation meterOperation = new MeterOperation
            {
                KeyMeter = const_virtual_meter_key,
                KeyMeter1 = DefaultKeyMeterStr,
                IfcType = "IfcMeterOperation",
                LocalId = "Operation_Duplicate",
                Factor1 = 2,
                KeyMeterOperation = "1",
                Factor2 = 1
            };

            Meter virtualMeter = new Meter
            {
                MeterOperations = new List<MeterOperation> { meterOperation },
                KeyMeter = const_virtual_meter_key,
                Factor = 1.0,
                KeyLocation = DefaultKeyLocation,
                KeyMeterOperationResult = "1",
                Name = "Test Site Meter Virtual",
				CalendarEventCategory = m_TestCalendarCategory,
				CalendarMode = ChartCalendarMode.Include
			};

            TestDataContainer.Meters.Add(virtualMeter);
            SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Create Calendar event that falls inside the range of the meter's data
			int middleIndex = testMeterData.Keys.Count / 2;
			var calendarEvent = CreateTestCalendarEvent(virtualMeter.KeyLocation, testMeterData.Keys[1], testMeterData.Keys[middleIndex]);

            AddCalendarEventToCalendars(calendarEvent);
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
			DataSerie virtualMeterSerie = resultSeries.First(dataSerie => dataSerie.KeyMeter == virtualMeter.KeyMeter);
			IEnumerable<DataPoint> virtualMeterPoints = virtualMeterSerie.Points;
			
			Assert.IsTrue(virtualMeterPoints.Any());

			//All virtual meter points must fall outside calendar range
			foreach (DataPoint dataPoint in virtualMeterPoints) {
				Assert.IsTrue(dataPoint.XDateTime >= calendarEvent.StartDate &&
							  dataPoint.XDateTime <= calendarEvent.EndDate);
			}
        }

        [Test]
		public void ChartProcess_ExcludeCalendarEventOnVirtualMeter_VirtualMeterSeriesWithPointsOutsideCalndarRange()
        {
            //Arrange
            MeterOperation meterOperation = new MeterOperation
            {
                KeyMeter = const_virtual_meter_key,
                KeyMeter1 = DefaultKeyMeterStr,
                IfcType = "IfcMeterOperation",
                LocalId = "Operation_Duplicate",
                Factor1 = 2,
                KeyMeterOperation = "1",
                Factor2 = 1,
            };

            Meter virtualMeter = new Meter
            {
                MeterOperations = new List<MeterOperation> { meterOperation },
                KeyMeter = const_virtual_meter_key,
                Factor = 1.0,
                KeyLocation = DefaultKeyLocation,
                KeyMeterOperationResult = "1",
                Name = "Test Site Meter Virtual",
				CalendarEventCategory = m_TestCalendarCategory,
				CalendarMode = ChartCalendarMode.Exclude
            };

            TestDataContainer.Meters.Add(virtualMeter);
            SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Create Calendar event on the virtual meter that falls inside the range of the meter's data
			int middleIndex = testMeterData.Keys.Count / 2;
			var calendarEvent = CreateTestCalendarEvent(virtualMeter.KeyMeter,testMeterData.Keys[1], testMeterData.Keys[middleIndex]);
            AddCalendarEventToCalendars(calendarEvent);
          
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert

			DataSerie virtualMeterSerie = resultSeries.First(dataSerie => dataSerie.KeyMeter == virtualMeter.KeyMeter);
			IEnumerable<DataPoint> virtualMeterPoints = virtualMeterSerie.Points;

			Assert.IsTrue(virtualMeterPoints.Any());
			
			//All virtual meter points must fall outside calendar range
			foreach (DataPoint dataPoint in virtualMeterPoints)
            {
                Assert.IsTrue(dataPoint.XDateTime <= calendarEvent.StartDate ||
                              dataPoint.XDateTime >= calendarEvent.EndDate);
            }

			
        }

        [Test]
		public void ChartProcess_SplitByEventCalendarEventOnVirtualMeter_VirtualMeterSerieWithPointsOfTheSelectedCalendarEvent()
        {
            //Arrange
            MeterOperation meterOperation = new MeterOperation
            {
                KeyMeter = const_virtual_meter_key,
                KeyMeter1 = DefaultKeyMeterStr,
                IfcType = "IfcMeterOperation",
                LocalId = "Operation_Duplicate",
                Factor1 = 2,
                KeyMeterOperation = "1",
                Factor2 = 1
            };

			Meter virtualMeter = new Meter {
				MeterOperations = new List<MeterOperation> { meterOperation },
				KeyMeter = const_virtual_meter_key,
				Factor = 1.0,
				KeyLocation = DefaultKeyLocation,
				KeyMeterOperationResult = "1",
				Name = "Test Site Meter Virtual",
				CalendarEventCategory = m_TestCalendarCategory,
				CalendarMode = ChartCalendarMode.SplitByEvent,
			};

			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Create Calendar events for the virtual meter that fall inside the range of the meter's data
			int middleIndex = testMeterData.Keys.Count / 2;
			CalendarEvent firstCalendarEvent = CreateTestCalendarEvent(virtualMeter.KeyMeter, testMeterData.Keys[1], testMeterData.Keys[middleIndex - 1]);
			CalendarEvent secondCalendarEvent = CreateTestCalendarEvent(virtualMeter.KeyMeter, testMeterData.Keys[middleIndex + 1], testMeterData.Keys[testMeterData.Keys.Count - 1]);
			AddCalendarEventToCalendars(firstCalendarEvent);
			AddCalendarEventToCalendars(secondCalendarEvent);

			//Choose the first calendar event for the virtual meter
	        virtualMeter.CalendarEventCategoryTitle = firstCalendarEvent.Title;

            TestDataContainer.Meters.Add(virtualMeter);

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();
	
			DataSerie virtualMeterSerie = resultSeries.First(dataSerie => dataSerie.KeyMeter == virtualMeter.KeyMeter);
			IEnumerable<DataPoint> virtualMeterPoints = virtualMeterSerie.Points;

			//Assert
			Assert.IsTrue(virtualMeterPoints.Any());

			//Since the first calendar event has been chosen for the meter, make sure that all of meter points are within the calendar dates range
            foreach (DataPoint dataPoint in virtualMeterPoints)
            {
                Assert.IsTrue(dataPoint.XDateTime >= firstCalendarEvent.StartDate &&
                              dataPoint.XDateTime <= firstCalendarEvent.EndDate);
            }
            
        }

	    private CalendarEvent CreateTestCalendarEvent(string location, DateTime startDate, DateTime endDate)
	    {
		    string id = Guid.NewGuid().ToString();
			var calendarEvent = new CalendarEvent() {
				LocalId = id,
				Title = id,
				StartDate = startDate,
				EndDate = endDate,
				KeyLocation = location,
				Key = id,
				Category = m_TestCalendarCategory.LocalId,
				TimeZoneId = const_utc_time_zone_id
			};

		    return calendarEvent;
	    }

		
		private void AddCalendarEventToCalendars(CalendarEvent item)
		{
			Tuple<string, string> calendarData;

			TestDataContainer.Calendars.TryGetValue(item.KeyLocation, out calendarData);

			iCalendar iCalendar = calendarData == null ? new iCalendar() : CalendarHelper.Deserialize(calendarData.Item1);
			item.ToiCal(iCalendar);
			CalendarHelper.AddTimeZone(iCalendar, item.TimeZone);

			string iCalData = CalendarHelper.Serialize(iCalendar);

			if (calendarData == null)
			{
				//Create new event for a new calendar
				TestDataContainer.Calendars.Add(item.KeyLocation, new Tuple<string, string>(iCalData,item.TimeZoneId));
			}
			else
			{
				//Add new event to existing calendar
				TestDataContainer.Calendars[item.KeyLocation] = new Tuple<string, string>(iCalData, item.TimeZoneId);
			}
	    }
    }
}

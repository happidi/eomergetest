﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {

	public class EnergyAggregatorTimeRangeTests : BaseEnergyAggregatorBusinessLayerTests
	{
		private const string const_meter_data_file_name =
			"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.Common.MonthlyMeterData.txt";

		protected override string TestMeterDataFilePath
		{
			get { return const_meter_data_file_name; }
		}

		#region Static Dates

		[Test]
		public void ChartProcess_StaticTimeRangeIdenticalToMeterDataRange_ChartWithIdenticalTimeRangeToMeterData()
		{
			//Arrange
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			TestDataContainer.Chart.StartDate = testMeterData.Keys[0]; //Start the filter from the first Meter data date
			TestDataContainer.Chart.EndDate = testMeterData.Keys[testMeterData.Keys.Count - 1];//End the filter at the last Meter data date

			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			List<DataSerie> resultSeries = result.Series.ToList();

			//Assert
			int expectedResultsCount = testMeterData.Count - 1;

			var resultSeriePoints = resultSeries[0].PointsWithValue;
			Assert.AreEqual(expectedResultsCount, resultSeriePoints.Count);

			for (int i = 0; i < testMeterData.Values.Count - 1; i++)
			{
				Assert.AreEqual(testMeterData.Values[i].Value, resultSeriePoints[i].YValue.Value);
			}
		}

		[Test]
		public void ChartProcess_StaticTimeRangeContainsMeterDataRange_ChartThatContainsAllTheMeterData()
		{
			//Arrange
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Start the filter before the first Meter data date
			TestDataContainer.Chart.StartDate = testMeterData.Keys[0].AddDays(-1);

			//End the filter after the last Meter data date
			TestDataContainer.Chart.EndDate = testMeterData.Keys[testMeterData.Keys.Count - 1].AddDays(1);

			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			List<DataSerie> resultSeries = result.Series.ToList();

			//Assert
			Assert.AreEqual(testMeterData.Count, resultSeries[0].PointsWithValue.Count);

			for (int i = 0; i < testMeterData.Values.Count; i++)
			{
				Assert.AreEqual(testMeterData.Values[i].Value, resultSeries[0].PointsWithValue[i].YValue.Value);
			}
		}

		[TestCase(1, -1)]
		[TestCase(-1, -1)]
		[TestCase(1, 1)]
		public void ChartProcess_StaticTimeRangeInsideMeterDataRange__ChartWithDataInsideMeterDataRange(
			int startDateShiftFactor, int endDateShiftFactor)
		{

			//Arrange
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Start the filter after the first Meter data date
			TestDataContainer.Chart.StartDate = testMeterData.Keys[0].AddDays(startDateShiftFactor);

			//End the filter before the last Meter data date
			TestDataContainer.Chart.EndDate = testMeterData.Keys[testMeterData.Keys.Count - 1].AddDays(endDateShiftFactor);

			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(),
				Thread.CurrentThread.CurrentCulture.Name);
			DataSerie resultSerie = result.Series.ToList()[0];

			//Assert
			Assert.Less(resultSerie.PointsWithValue.Count, testMeterData.Count);

			//Validate that the result serie is a sub set of the test meter data within the specified range
			foreach (var resultPoint in resultSerie.PointsWithValue)
			{
				var testValue = testMeterData.Values.FirstOrDefault(md => md.Value == resultPoint.YValue.Value);
				Assert.IsNotNull(testValue);
			}
		}

		[Test]
		public void ChartProcess_StaticTimeRangeSameDateDifferentHoursWhenMeterDataExists__ChartWithMeterDataForThatDate()
		{
			//Arrange
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;

			var meterDataForDate = new MD() {AcquisitionDateTime = DateTime.UtcNow, KeyMeter = DefaultMeterKey, Value = 111};
			SortedList<DateTime, MD> testMeterData =
				TestDataContainer.MeterData[DefaultMeterKey] = new SortedList<DateTime, MD>
				{
					{
						meterDataForDate.AcquisitionDateTime, meterDataForDate
					}
				};


			TestDataContainer.Chart.StartDate = meterDataForDate.AcquisitionDateTime.AddHours(-1);
			TestDataContainer.Chart.EndDate = meterDataForDate.AcquisitionDateTime.AddHours(1);

			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			List<DataSerie> resultSeries = result.Series.ToList();

			//Assert
			Assert.IsTrue(resultSeries[0].PointsWithValue.Count > 0);
			Assert.AreEqual(testMeterData.Values[0].Value, resultSeries[0].PointsWithValue[0].YValue.Value);

		}


		#endregion

		#region Dynamic Dates

		[Test, Combinatorial]
		public void ChartProcess_DynamicTimeRangeWithFinishedInterval_AggregateLastPeriod(
			[Values(AxisTimeInterval.Months, AxisTimeInterval.Weeks, AxisTimeInterval.Days, AxisTimeInterval.Hours,
				AxisTimeInterval.Minutes, AxisTimeInterval.Quarters, AxisTimeInterval.Semesters, AxisTimeInterval.Years)] AxisTimeInterval axisTimeInterval,
			[Values(1)] int frequency,
			[Values(true, false)] bool completeInterval)
		{
			//Arrange
			TestDataContainer.Chart.DynamicTimeScaleEnabled = true;
			TestDataContainer.Chart.DynamicTimeScaleInterval = axisTimeInterval;
			TestDataContainer.Chart.DynamicTimeScaleFrequency = frequency;
			TestDataContainer.Chart.TimeInterval = axisTimeInterval;
			TestDataContainer.Chart.DynamicTimeScaleCompleteInterval = completeInterval;
			DateTime startDate = axisTimeInterval == AxisTimeInterval.Weeks
				? DateTime.UtcNow.AddYears(-1).AddDays(1)
				: DateTime.UtcNow.AddYears(-1);
			var meterData = GenerateRandomMeterData(DefaultKeyMeterStr, startDate, DateTime.UtcNow, 1,
				axisTimeInterval);
			TestDataContainer.MeterData = meterData;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			var meterDataList = meterData[Convert.ToInt32(DefaultKeyMeterStr.Substring(1))].Values.ToList();
			int updatedFrequency = completeInterval ? frequency + 1 : frequency;
				//if completeInterval we want to take one interval backwards
			var lastMeterData = meterDataList.GetRange(meterDataList.Count - updatedFrequency, frequency);
				//get the last "frequency"
			var sum = lastMeterData.Sum(x => x.Value); //because we have one data point per interval we can compare the sum
			Assert.AreEqual(frequency, dataPoint.Count);
			Assert.AreEqual(sum, dataPoint.Sum(x => x.YValue));
		}

		[Test, Combinatorial]
		public void ChartProcess_DynamicTimeRangeWithEndToNow_AggregateLastMonthPeriod(
			[Values(AxisTimeInterval.Months)] AxisTimeInterval axisTimeInterval,
			[Values(1)] int frequency,
			[Values(true, false)] bool endToNow)
		{
			//Arrange
			TestDataContainer.Chart.DynamicTimeScaleEnabled = true;
			TestDataContainer.Chart.DynamicTimeScaleInterval = axisTimeInterval;
			TestDataContainer.Chart.DynamicTimeScaleFrequency = frequency;
			TestDataContainer.Chart.TimeInterval = axisTimeInterval;
			TestDataContainer.Chart.DynamicTimeScaleCalendarEndToNow = endToNow;
			TestDataContainer.Chart.DynamicTimeScaleCalendar = true;
			var currentYear = DateTime.UtcNow.Year;
			var currentMonth = DateTime.UtcNow.Month;
			var startDate = new DateTime(currentYear, currentMonth, 1); //the first day of the current month
			var endDate = new DateTime(currentYear, currentMonth, DateTime.DaysInMonth(currentYear, currentMonth));
				//the last day of the current month
			var meterData = GenerateRandomMeterData(DefaultKeyMeterStr, startDate, endDate, 1,
				AxisTimeInterval.Weeks); // generate weekly data for the current month
			TestDataContainer.MeterData = meterData;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			var meterDataList = meterData[Convert.ToInt32(DefaultKeyMeterStr.Substring(1))].Values.ToList();
			var lastMeterData = endToNow
				? meterDataList.Where(x => x.AcquisitionDateTime >= startDate && x.AcquisitionDateTime <= DateTime.UtcNow)
				: meterDataList.Where(x => x.AcquisitionDateTime >= startDate);
				//if end to now we want to compare until the current day, else we want to compare until the end of the month
			var sum = lastMeterData.Sum(x => x.Value); //aggregation of the meter data.
			Assert.AreEqual(frequency, dataPoint.Count);
			Assert.AreEqual(sum, dataPoint.Sum(x => x.YValue));
		}


		[Test]
		public void ChartProcess_StaticAndDynamicRangeForSamePeriod_IdenticalResultsForBothTypes() {
		
			//Arrange

			//Generate some test data for the past year
            //subtracting 5 days so the start date won't be exactly like the chart start date which can cause irregular results.
			DateTime meterDataStartDate = DateTime.UtcNow.AddDays(-5).AddYears(-1);  
			DateTime meterDataEndDate = DateTime.UtcNow.AddDays(-1);

			const AxisTimeInterval dynamicTimeScaleInterval = AxisTimeInterval.Months;
			ConcurrentDictionary<int, SortedList<DateTime, MD>> meterData = GenerateRandomMeterData(DefaultKeyMeterStr,
				meterDataStartDate, meterDataEndDate, 1,
				dynamicTimeScaleInterval);

			TestDataContainer.MeterData = meterData;

			// Initialize the Static range parameters
			const int scaleFrequencyFactor = 3; 
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			TestDataContainer.Chart.StartDate = DateTime.UtcNow.AddMonths(-scaleFrequencyFactor);
			TestDataContainer.Chart.EndDate = DateTime.UtcNow;
		
			InitializeTest();

			// Generate series by using the static time range
			var staticRangeResult = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			List<DataSerie> staticRangeResultSeries = staticRangeResult.Series.ToList();

			// Reset all data and Initialize the Dynamic range parameters
			EnergyAggregator.SetIsInitialized(false);
			TestDataContainer = InitializeTestDataContainer();

			// Initialize the Dynamic range parameters
			TestDataContainer.Chart.DynamicTimeScaleEnabled = true;
			TestDataContainer.Chart.DynamicTimeScaleFrequency = scaleFrequencyFactor;
			TestDataContainer.Chart.DynamicTimeScaleInterval = dynamicTimeScaleInterval;
			TestDataContainer.Chart.DynamicTimeScaleCalendar = false; // do not use the calendar period to expect the same behavior as the static time range
			TestDataContainer.MeterData = meterData;

			InitializeTest();

			// Generate series by using the dynamic time range
			var dynamicRangeResult = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			List<DataSerie> dynamicRangeResultSeries = dynamicRangeResult.Series.ToList();
			
			//Assert
			Assert.AreEqual(staticRangeResultSeries[0].PointsWithValue.Count, dynamicRangeResultSeries[0].PointsWithValue.Count);

			foreach (DataPoint dynamicResult in dynamicRangeResultSeries[0].PointsWithValue) {
				var staticResultPoint =
					staticRangeResultSeries[0].PointsWithValue.FirstOrDefault(p => p.YValue == dynamicResult.YValue &&
																				   p.XDateTime == dynamicResult.XDateTime);
				Assert.IsNotNull(staticResultPoint);

			}
		}

		#endregion
	}
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDay.Collections;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests
{
    public class EnergyAggregatorBusinessLayerDataSeriesCustomization : BaseEnergyAggregatorBusinessLayerTests {
        private const string const_key_data_serie = "1";
        private readonly string m_DataSerieName = string.Format("{0} - {1}", DefaultMeterName, DefaultClassificationItemName);
		private readonly string m_DataSerieLocalId = string.Format("{0} - {1}", DefaultLocationName + " / " + DefaultMeterName, DefaultClassificationItemName);
        public List<DataSerie> InitializeDataSeriesCustomizationTest(ConcurrentDictionary<int, SortedList<DateTime, MD>> meterData = null) 
        {
            if (meterData != null) {
                TestDataContainer.MeterData = meterData;    
            }
            InitializeTest();
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            EnergyAggregator.SetIsInitialized(false);
            return result.Series.ToList();
        }

        [TestCase(3.0)]
        public void ChartProcess_DataSerieCustomization_DataSerieRatio_DataSerieDividedByRatio(double? ratio) {
            //Arrange
            var dataSerieWithoutCustomization = InitializeDataSeriesCustomizationTest(); //dry run - to get the data without customization

            var dataSerie = new DataSerie {
                KeyChart = TestChartkey,
                KeyDataSerie = const_key_data_serie,
				LocalId = TestDataContainer.Chart.Series.ToList()[0].LocalId,
                Name = m_DataSerieName,
                Ratio = ratio
            };

            TestDataContainer.Chart.Series = new DataSerieCollection ( new List<DataSerie> { dataSerie } );

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            Assert.AreEqual(dataSerieWithoutCustomization[0].PointsWithValue[0].YValue / ratio, resultSeries[0].PointsWithValue[0].YValue);
        }

        [TestCase(200.0, 100.0)]
        public void ChartProcess_DataSerieCustomization_LimitElementsWithoutRawData_DataSerieIsLimitedByMinMaxValues(double upperLimit, double lowerLimit)
        {
            //Arrange
            var dataSerieWithoutCustomization = InitializeDataSeriesCustomizationTest(); //dry run - to get the data without customization

            var dataSerie = new DataSerie
            {
                KeyChart = TestChartkey,
                KeyDataSerie = const_key_data_serie,
				LocalId = TestDataContainer.Chart.Series.ToList()[0].LocalId,
                Name = m_DataSerieName,
                Ratio = 1.0,
                LimitElementMaxValue = upperLimit,
                LimitElementMinValue = lowerLimit,
                LimitElementRawData = false
            };

            TestDataContainer.Chart.Series = new DataSerieCollection(new List<DataSerie> { dataSerie });

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var limitedDataSerie = dataSerieWithoutCustomization.First().PointsWithValue.Where(points =>
                        points.YValue >= dataSerie.LimitElementMinValue &&
                        points.YValue <= dataSerie.LimitElementMaxValue).ToList();  //Apply the limits on the data without the customization to compare with the EA customized results

            Assert.AreEqual(limitedDataSerie.Count(), resultSeries[0].PointsWithValue.Count);

            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++) {
                Assert.AreEqual(limitedDataSerie[i].YValue, resultSeries[0].PointsWithValue[i].YValue);
            }
        }



        [TestCase(200.0)]
        public void ChartProcess_DataSerieCustomization_LimitElementsWithoutRawData_DataSerieIsLimitedByMaxValue(double upperLimit)
        {
            //Arrange
            var dataSerieWithoutCustomization = InitializeDataSeriesCustomizationTest(); //dry run - to get the data without customization

            var dataSerie = new DataSerie
            {
                KeyChart = TestChartkey,
                KeyDataSerie = const_key_data_serie,
				LocalId = TestDataContainer.Chart.Series.ToList()[0].LocalId,
                Name = m_DataSerieName,
                Ratio = 1.0,
                LimitElementMaxValue = upperLimit,
                LimitElementRawData = false
            };

            TestDataContainer.Chart.Series = new DataSerieCollection(new List<DataSerie> { dataSerie });

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var limitedDataSerie = dataSerieWithoutCustomization.First().PointsWithValue.Where(points =>
                        points.YValue <= dataSerie.LimitElementMaxValue).ToList();  //Apply the limits on the data without the customization to compare with the EA customized results

            Assert.AreEqual(limitedDataSerie.Count(), resultSeries[0].PointsWithValue.Count);

            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++)
            {
                Assert.AreEqual(limitedDataSerie[i].YValue, resultSeries[0].PointsWithValue[i].YValue);
            }
        }



        [TestCase(100.0)]
        public void ChartProcess_DataSerieCustomization_LimitElementsWithoutRawData_DataSerieIsLimitedByMinValue(double lowerLimit)
        {
            //Arrange
            var dataSerieWithoutCustomization = InitializeDataSeriesCustomizationTest(); //dry run - to get the data without customization

            var dataSerie = new DataSerie
            {
                KeyChart = TestChartkey,
                KeyDataSerie = const_key_data_serie,
				LocalId = TestDataContainer.Chart.Series.ToList()[0].LocalId,
                Name = m_DataSerieName,
                Ratio = 1.0,
                LimitElementMinValue = lowerLimit,
                LimitElementRawData = false
            };

            TestDataContainer.Chart.Series = new DataSerieCollection(new List<DataSerie> { dataSerie });

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var limitedDataSerie = dataSerieWithoutCustomization.First().PointsWithValue.Where(points =>
                        points.YValue >= dataSerie.LimitElementMinValue).ToList();  //Apply the limits on the data without the customization to compare with the EA customized results

            Assert.AreEqual(limitedDataSerie.Count(), resultSeries[0].PointsWithValue.Count);

            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++)
            {
                Assert.AreEqual(limitedDataSerie[i].YValue, resultSeries[0].PointsWithValue[i].YValue);
            }
        }



        [TestCase(30.0, 20.0)]
        public void ChartProcess_DataSerieCustomization_LimitElementsWithRawData_DataSerieAggregatedOnlyWithLimitedRawData(double upperLimit, double lowerLimit)
        {
            //Arrange
            SortedList<DateTime, MD> meterDataSortedList;
            TestDataContainer.MeterData.TryGetValue(DefaultMeterKey, out meterDataSortedList);
            var limitedMeterDataList = new SortedList<DateTime, MD>();
            foreach (var kvp in meterDataSortedList.Where(kvp => kvp.Value.Value >= lowerLimit && kvp.Value.Value <= upperLimit)) // apply the limits on raw meter data
            {
                limitedMeterDataList.Add(kvp.Key, kvp.Value);
            }

            var limitedmeterData = new ConcurrentDictionary<int, SortedList<DateTime, MD>>();
            limitedmeterData.TryAdd(DefaultMeterKey, limitedMeterDataList);
            var dataSerieWithoutCustomization = InitializeDataSeriesCustomizationTest(limitedmeterData);  //dry run - to get the data without customization with modified meter data

            var dataSerie = new DataSerie
            {
                KeyChart = TestChartkey,
                KeyDataSerie = const_key_data_serie,
				LocalId = TestDataContainer.Chart.Series.ToList()[0].LocalId,
                Name = m_DataSerieName,
                Ratio = 1.0,
                LimitElementMaxValue = upperLimit,
                LimitElementMinValue = lowerLimit,
                LimitElementRawData = true
            };

            TestDataContainer.Chart.Series = new DataSerieCollection(new List<DataSerie> { dataSerie });

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            Assert.AreEqual(dataSerieWithoutCustomization[0].PointsWithValue.Count, resultSeries[0].PointsWithValue.Count);

            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++)
            {
                Assert.AreEqual(dataSerieWithoutCustomization[0].PointsWithValue[i].YValue, resultSeries[0].PointsWithValue[i].YValue);
            }
        }

        [TestCase(20.0)]
        public void ChartProcess_DataSerieCustomization_LimitElementsWithRawData_MinValueOnly_DataSerieAggregatedOnlyWithLimitedRawData(double lowerLimit)
        {
            //Arrange
            SortedList<DateTime, MD> meterDataSortedList;
            TestDataContainer.MeterData.TryGetValue(DefaultMeterKey, out meterDataSortedList);
            var limitedMeterDataList = new SortedList<DateTime, MD>();
            foreach (var kvp in meterDataSortedList.Where(kvp => kvp.Value.Value >= lowerLimit)) // apply the limits on raw meter data
            {
                limitedMeterDataList.Add(kvp.Key, kvp.Value);
            }

            var limitedmeterData = new ConcurrentDictionary<int, SortedList<DateTime, MD>>();
            limitedmeterData.TryAdd(DefaultMeterKey, limitedMeterDataList);
            var dataSerieWithoutCustomization = InitializeDataSeriesCustomizationTest(limitedmeterData);  //dry run - to get the data without customization with modified meter data

            var dataSerie = new DataSerie
            {
                KeyChart = TestChartkey,
                KeyDataSerie = const_key_data_serie,
				LocalId = TestDataContainer.Chart.Series.ToList()[0].LocalId,
                Name = m_DataSerieName,
                Ratio = 1.0,
                LimitElementMinValue = lowerLimit,
                LimitElementRawData = true
            };

            TestDataContainer.Chart.Series = new DataSerieCollection(new List<DataSerie> { dataSerie });

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            Assert.AreEqual(dataSerieWithoutCustomization[0].PointsWithValue.Count, resultSeries[0].PointsWithValue.Count);

            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++)
            {
                Assert.AreEqual(dataSerieWithoutCustomization[0].PointsWithValue[i].YValue, resultSeries[0].PointsWithValue[i].YValue);
            }
        }

        [TestCase(30.0)]
        public void ChartProcess_DataSerieCustomization_LimitElementsWithRawData_MaxLimitOnly_DataSerieAggregatedOnlyWithLimitedRawData(double upperLimit)
        {
            //Arrange
            SortedList<DateTime, MD> meterDataSortedList;
            TestDataContainer.MeterData.TryGetValue(DefaultMeterKey, out meterDataSortedList);
            var limitedMeterDataList = new SortedList<DateTime, MD>();
            foreach (var kvp in meterDataSortedList.Where(kvp => kvp.Value.Value <= upperLimit)) // apply the limits on raw meter data
            {
                limitedMeterDataList.Add(kvp.Key, kvp.Value);
            }

            var limitedmeterData = new ConcurrentDictionary<int, SortedList<DateTime, MD>>();
            limitedmeterData.TryAdd(DefaultMeterKey, limitedMeterDataList);
            var dataSerieWithoutCustomization = InitializeDataSeriesCustomizationTest(limitedmeterData);  //dry run - to get the data without customization with modified meter data

            var dataSerie = new DataSerie
            {
                KeyChart = TestChartkey,
                KeyDataSerie = const_key_data_serie,
				LocalId = TestDataContainer.Chart.Series.ToList()[0].LocalId,
                Name = m_DataSerieName,
                Ratio = 1.0,
                LimitElementMaxValue = upperLimit,
                LimitElementRawData = true
            };

            TestDataContainer.Chart.Series = new DataSerieCollection(new List<DataSerie> { dataSerie });

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            Assert.AreEqual(dataSerieWithoutCustomization[0].PointsWithValue.Count, resultSeries[0].PointsWithValue.Count);

            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++)
            {
                Assert.AreEqual(dataSerieWithoutCustomization[0].PointsWithValue[i].YValue, resultSeries[0].PointsWithValue[i].YValue);
            }
        }


        [TestCase(30.0, 5.0)]
        public void ChartProcess_DataSerieCustomization_LimitElementCountWithRawData_CountNumberOfMeterDataInRange(double upperLimit, double lowerLimit)
        {
            //Arrange
            SortedList<DateTime, MD> meterDataSortedList;
            TestDataContainer.MeterData.TryGetValue(DefaultMeterKey, out meterDataSortedList);

            var numOfMeterDataInRange = meterDataSortedList.Values.Count(md => md.Value >= lowerLimit && md.Value <= upperLimit); //get the number of meter data inside the limit

            var dataSerie = new DataSerie
            {
                KeyChart = TestChartkey,
                KeyDataSerie = const_key_data_serie,
				LocalId = m_DataSerieLocalId,
                Name = m_DataSerieName,
                Ratio = 1.0,
                LimitElementMaxValue = upperLimit,
                LimitElementMinValue = lowerLimit,
                LimitElementRawData = true,
                LimitElementCount = true
            };

            TestDataContainer.Chart.Series = new DataSerieCollection(new List<DataSerie> { dataSerie });

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var dataPoints = resultSeries[0].PointsWithValue.Select(x => x.YValue);
            Assert.AreEqual(numOfMeterDataInRange, dataPoints.Sum());
        }

        [TestCase(150.0, 100.0)]
        public void ChartProcess_DataSerieCustomization_LimitElementCountWithoutRawData_CountNumberOfDataPointsInRange(double upperLimit, double lowerLimit)
        {
            //Arrange
            var dataSerieWithoutCustomization = InitializeDataSeriesCustomizationTest();

            var numOfMeterDataInRange = dataSerieWithoutCustomization[0].PointsWithValue.Count(dp => dp.YValue >= lowerLimit && dp.YValue <= upperLimit); //get the number of data points inside the limit

            var dataSerie = new DataSerie
            {
                KeyChart = TestChartkey,
                KeyDataSerie = const_key_data_serie,
				LocalId = TestDataContainer.Chart.Series.ToList()[0].LocalId,
                Name = m_DataSerieName,
                Ratio = 1.0,
                LimitElementMaxValue = upperLimit,
                LimitElementMinValue = lowerLimit,
                LimitElementRawData = false,
                LimitElementCount = true
            };

            TestDataContainer.Chart.Series = new DataSerieCollection(new List<DataSerie> { dataSerie });

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var dataPointsValues = resultSeries[0].PointsWithValue.Select(x => x.YValue);
            Assert.AreEqual(numOfMeterDataInRange, dataPointsValues.Sum());
        }

        [TestCase(1)]
        [TestCase(2)]
        public void ChartProcess_MaxNumberOfProcessedMeter_NumberOfResultingSerieIsLimitedToMaxMeter(int maxMeter) {
            //Arrange
            TestDataContainer.Chart.MaxMeter = maxMeter;
            var secondMeter = new Meter
            {
                KeyMeter = "#111",
                KeyLocation = DefaultKeyLocation,
                Factor = 1.0,
                IsAcquisitionDateTimeEndInterval = false,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Name = "Test Site Second Meter"
            };

            TestDataContainer.Meters.Add(secondMeter);

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            Assert.AreEqual(maxMeter, resultSeries.Count);
            
        }

        [TestCase(DataPointHighlight.Border, "725151",200d,120d)]
        [TestCase(DataPointHighlight.Hatch, "725153", 200d, 120d)]
        [TestCase(DataPointHighlight.Fill, "725168", 200d, 120d)]
        [TestCase(DataPointHighlight.Border, "725151", 200d, null)]
        [TestCase(DataPointHighlight.Hatch, "725153", 200d, null)]
        [TestCase(DataPointHighlight.Fill, "725168", 200d, null)]
        [TestCase(DataPointHighlight.Border, "725151", null, 120d)]
        [TestCase(DataPointHighlight.Hatch, "725153", null, 120d)]
        [TestCase(DataPointHighlight.Fill, "725168", null, 120d)]
        public void ChartProcess_DataSerieCustomization_ColorRules_DataPointInRangeColored(DataPointHighlight dataPointHighlight, string color,double? maxValue,double? minValue) {
            //Arrange
            var dataSerieColorElement = new DataSerieColorElement 
            {
                Color = color,
                Highlight = dataPointHighlight,
                KeyDataSerie = const_key_data_serie,
                KeyDataSerieColorElement = "1",
                LocalId = "1",
                //MaxValue = 200.0,
                //MinValue = 120.0
                MaxValue = maxValue,
                MinValue = minValue
            };

            var dataSerie = new DataSerie
            {
                KeyChart = TestChartkey,
                KeyDataSerie = const_key_data_serie,
				LocalId = m_DataSerieLocalId,
                Name = m_DataSerieName,
                Ratio = 1.0,
                ColorElements = new List<DataSerieColorElement> { dataSerieColorElement }
            };

            TestDataContainer.Chart.Series = new DataSerieCollection(new List<DataSerie> { dataSerie });

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            foreach (var dataPoint in resultSeries[0].PointsWithValue) {
                if (dataSerieColorElement.MinValue.HasValue && dataSerieColorElement.MaxValue.HasValue)
                {
                       if (dataPoint.YValue >= dataSerieColorElement.MinValue &&
                    dataPoint.YValue <= dataSerieColorElement.MaxValue) { //if data point in range check the colors
                        Assert.AreEqual(dataSerieColorElement.Color, dataPoint.HighlightColor);
                        Assert.AreEqual(dataSerieColorElement.Highlight, dataPoint.Highlight);
                }
                else
                {  //the default values
                    Assert.IsNull(dataPoint.HighlightColor); 
                    Assert.AreEqual(DataPointHighlight.Fill, dataPoint.Highlight);
                }

                }
                else if (
                    (dataSerieColorElement.MinValue.HasValue && dataPoint.YValue >= dataSerieColorElement.MinValue) ||
                    (dataSerieColorElement.MaxValue.HasValue && dataPoint.YValue <= dataSerieColorElement.MaxValue)
                    )
                { //if data point in range check the colors
                        Assert.AreEqual(dataSerieColorElement.Color, dataPoint.HighlightColor);
                        Assert.AreEqual(dataSerieColorElement.Highlight, dataPoint.Highlight);
                }
                else
                {  //the default values
                    Assert.IsNull(dataPoint.HighlightColor); 
                    Assert.AreEqual(DataPointHighlight.Fill, dataPoint.Highlight);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Xml.Linq;
using FakeItEasy;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A concrete provider for tracing to be used in testing. Does nothing.
	/// </summary>
	public class TestingTracingProvider : TracingProvider {
		private ITraceScope m_TracingScope;

		/// <summary>
		/// Gets the trace scope for the given tracing category and related entity key.
		/// </summary>
		/// <param name="traceCategory">The trace category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		public override ITraceScope StartTracing(string traceCategory, string relatedKeyEntity) {
			m_TracingScope = A.Fake<ITraceScope>();
			return m_TracingScope;
		}

		/// <summary>
		/// Writes the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public override void Write(string message) {
			
		}

		/// <summary>
		/// Writes the specified message with the given severity.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="message">The message.</param>
		public override void Write(TraceEntrySeverity severity, string message) {
			
		}

		/// <summary>
		/// Flushes the log.
		/// </summary>
		public override void Flush() {
			
		}

		/// <summary>
		/// Writes the specified message with the given severity.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="message">The message.</param>
		/// <param name="category">The category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		public override void Write(TraceEntrySeverity severity, string message, string category, string relatedKeyEntity) {
		}

		/// <summary>
		/// Traces the specified action with the given parameters.
		/// Documents begin, end and exception if occures.
		/// </summary>
		/// <typeparam name="T">The function result type</typeparam>
		/// <param name="traceCategory">The trace category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		/// <param name="message">The message.</param>
		/// <param name="action">The action.</param>
		public override T Trace<T>(string traceCategory, string relatedKeyEntity, string message, Func<T> action) {
			return default(T);
		}

		/// <summary>
		/// Traces the specified action within an existing tracing scope.
		/// Documents begin, end and exception if occures.
		/// </summary>
		/// <typeparam name="T">The function result type</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="action">The action.</param>
		/// <returns></returns>
		public override T Trace<T>(string message, Func<T> action) {
			return default(T);
		}

		/// <summary>
		/// Writes the specified exception to the trace log.
		/// </summary>
		/// <param name="exception">The exception.</param>
		public override void Write(Exception exception) {
		}

		/// <summary>
		/// Continues tracing from other thread. Only copies the most recent tracing scope. It needs to be disposed of in the original thread.
		/// </summary>
		/// <param name="originalThreadTraceScope">The scope from other thread.</param>
		/// <returns></returns>
		public override ITraceScope ContinueTracing(ITraceScope originalThreadTraceScope) {
			return originalThreadTraceScope;
		}

		/// <summary>
		/// Gets the current trace scope. Returns null if no current scope exists.
		/// </summary>
		/// <returns></returns>
		public override ITraceScope GetCurrentTraceScope() {
			return m_TracingScope;
		}
	}
}

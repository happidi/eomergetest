﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json.Linq;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {
	public static class TestHelper {
	
		public static ConcurrentDictionary<int, SortedList<DateTime, MD>> ParseMeterDataFromFile(string path) {

			if (string.IsNullOrEmpty(path))
			{
				throw new ArgumentNullException(path, "File path cannot be empty.");
			}

			Stream meterDataStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(path);
			StreamReader reader = new StreamReader(meterDataStream);
			string text = reader.ReadToEnd();
			JObject jo = JObject.Parse(text);
			var obj = jo.First.First.AsEnumerable().ToList()[4].ToList();//TODO - Change it to something more efficient and normal
			MD meterData;
			var unsortedDictionary = new Dictionary<int, Dictionary<DateTime, MD>>();
			foreach (dynamic data in obj[0]) {
				string key = (string)data.KeyMeter;
				int keyMeter = Convert.ToInt32(key.Substring(1));
				if (!unsortedDictionary.ContainsKey(keyMeter)) {
					unsortedDictionary.Add(keyMeter, new Dictionary<DateTime, MD>());
				}
				meterData = new MD
				{
					AcquisitionDateTime = (DateTime) data.AcquisitionDateTime,
					KeyMeter = keyMeter,
					KeyMeterData = (long) data.KeyMeterData,
					Validity = (byte) data.Validity,
					Value = (double) data.Value
				};


				if (!unsortedDictionary[keyMeter].ContainsKey(meterData.AcquisitionDateTime)) {
					unsortedDictionary[keyMeter].Add(meterData.AcquisitionDateTime, meterData);
				}

			}

			var retVal = new ConcurrentDictionary<int, SortedList<DateTime, MD>>(unsortedDictionary.AsParallel()
										   .Select(item => new KeyValuePair<int, SortedList<DateTime, MD>>(item.Key, new SortedList<DateTime, MD>(item.Value))));

			return retVal;
		}

		public static Tuple<ConcurrentDictionary<int, SortedList<DateTime, MD>>, IEnumerable> GetTestDataAndResult(string dataPath, string resultPath, ITextFileParser resultParser) {
			var results = resultParser.Parse(resultPath);
			var data = ParseMeterDataFromFile(dataPath);

			return new Tuple<ConcurrentDictionary<int, SortedList<DateTime, MD>>, IEnumerable>(data, results);

		}
			
	}
}

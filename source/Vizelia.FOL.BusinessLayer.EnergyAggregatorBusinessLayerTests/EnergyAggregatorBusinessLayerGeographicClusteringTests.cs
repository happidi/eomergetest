﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {

	public class EnergyAggregatorBusinessLayerGeographicClusteringTests : BaseEnergyAggregatorBusinessLayerTests {

		private const string const_meter_data_file_name =
			"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.Common.MonthlyMeterData.txt";
		private const string const_khw_classification_title = "kwh";
		private const string const_sub_khw_classification_title = "sub_kwh";
		private const string const_kwh_classification_key = "#1";
		private const string const_sub_kwh_classification_key = "#2";
		private const string const_temperature_classification_key = "#3";
		private const string const_temperature_classification_title = "temperature";

		private const string const_root_site_key = "#4";
		private const string const_site_key = "#100";
		private const string const_building_key = "#108";
		private const string const_floor_key = "#109";
		private const string const_space_key = "#110";
		private const string const_equipment_key = "#111";
	
		/// <summary>
		/// The points that are expected as a result of a test
		/// </summary>
		private IList<double> m_ExpectedResultPoints;
		
		
		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			//The expected points equal to the first meter values since all of the meters data is copied from it for all the tests
			m_ExpectedResultPoints = TestDataContainer.MeterData[DefaultMeterKey].Values.Select(md => md.Value).ToList();
		}

		[TestCase(ChartLocalisation.BySite)]
		[TestCase(ChartLocalisation.ByBuilding)]
		[TestCase(ChartLocalisation.ByFloor)]
		[TestCase(ChartLocalisation.BySpace)]
		[TestCase(ChartLocalisation.ByFurniture)]
		public void Chart_GeographicClustering_OneMeterPerLocation_AggregatedByLocation(ChartLocalisation chartLocalisation) {
			//Arrange
			
			TestDataContainer.Chart.Localisation = chartLocalisation;
			InitializeTest();

			//Act
			Chart result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();
			var locationTypeName = ConvertChartLocalisationToHierarchySpatial(chartLocalisation);
			var locations = TestDataContainer.Locations.Values.Where(x => x.TypeName == locationTypeName).Select(x => x.KeyLocation);
			var resultSerie = resultSeries.First(x => locations.Contains(x.KeyLocation));

			//Assert 
			for (int i = 0; i < m_ExpectedResultPoints.Count(); i++) {
				Assert.AreEqual(m_ExpectedResultPoints[i] * (int)locationTypeName, resultSerie.PointsWithValue[i].YValue.Value);
			}
		}

		[Test]
		public void Chart_GeographicClustering_OneMeterPerLocationWithMultipleSiteHierarchy_AggregatedByLocation() {
			//Arrange
			const int const_localisation_site_level = 1;
			TestDataContainer.Chart.Localisation = ChartLocalisation.BySite;
			TestDataContainer.Chart.LocalisationSiteLevel = const_localisation_site_level;
			var rootSiteMeter = new Meter {
				KeyMeter = "#114",
				KeyLocation = const_root_site_key,
				Name = "Test",
				Factor = 1.0,
				KeyClassificationItem = const_kwh_classification_key
			};
			TestDataContainer.Meters.Add(rootSiteMeter);

			var meter1Data = TestDataContainer.MeterData[DefaultMeterKey];
			int rootSiteMeterKey = MeterHelper.ConvertMeterKeyToInt(rootSiteMeter.KeyMeter);
			TestDataContainer.MeterData[rootSiteMeterKey] = CloneMeterData(meter1Data, rootSiteMeterKey); 

			InitializeTest();

			//Act
			Chart result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();
			var locations = TestDataContainer.Locations.Values.Where(x => x.TypeName == HierarchySpatialTypeName.IfcSite).Select(x => x.KeyLocation);
			var resultSerie = resultSeries.First(x => locations.Contains(x.KeyLocation));

			//Assert
			for (int i = 0; i < m_ExpectedResultPoints.Count; i++) {
				Assert.AreEqual(m_ExpectedResultPoints[i] * ((int)HierarchySpatialTypeName.IfcSite + const_localisation_site_level), resultSerie.PointsWithValue[i].YValue.Value);
			}
		}

		[TestCase(ChartLocalisation.BySite)]
		[TestCase(ChartLocalisation.ByBuilding)]
		[TestCase(ChartLocalisation.ByFloor)]
		[TestCase(ChartLocalisation.BySpace)]
		[TestCase(ChartLocalisation.ByFurniture)]
		public void Chart_GeographicAndClassificationAggregation_LowestClassificationAggregation_AggregationIsPerformedPerClassificationLevel(ChartLocalisation chartLocalisation) {
			//Arrange
			TestDataContainer.Chart.Localisation = chartLocalisation;
			TestDataContainer.Chart.ClassificationLevel = 2; // the lowest level in our classification hierarchy

			//Add another meter to every level with a sub classification
			int keyMeter1 = 114;
			int keyMeter2 = 115;
			int keyMeter3 = 116;
			int keyMeter4 = 117;
			int keyMeter5 = 118;
			TestDataContainer.Meters.AddRange(new List<Meter>()
              {
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter1), KeyLocation = const_site_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_sub_kwh_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter2), KeyLocation = const_building_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_sub_kwh_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter3), KeyLocation = const_floor_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_sub_kwh_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter4), KeyLocation = const_space_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_sub_kwh_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter5), KeyLocation = const_equipment_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_sub_kwh_classification_key},
              });

			//Generate data for the additional meters using the first meter
			var defaultMeterData = TestDataContainer.MeterData[DefaultMeterKey];
			TestDataContainer.MeterData[keyMeter1] = CloneMeterData(defaultMeterData, keyMeter1);
			TestDataContainer.MeterData[keyMeter2] = CloneMeterData(defaultMeterData, keyMeter2);
			TestDataContainer.MeterData[keyMeter3] = CloneMeterData(defaultMeterData, keyMeter3);
			TestDataContainer.MeterData[keyMeter4] = CloneMeterData(defaultMeterData, keyMeter4);
			TestDataContainer.MeterData[keyMeter5] = CloneMeterData(defaultMeterData, keyMeter5);

			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			var locationTypeName = ConvertChartLocalisationToHierarchySpatial(chartLocalisation);
			var locations = TestDataContainer.Locations.Values.Where(x => x.TypeName == locationTypeName).Select(x => x.KeyLocation);
			var resultSerie = resultSeries.Where(x => locations.Contains(x.KeyLocation));

			//Assert 
			foreach (DataSerie serie in resultSerie) {  //we are validating that each classification type is aggragated seperatly
				for (int i = 0; i < m_ExpectedResultPoints.Count; i++) {
					Assert.AreEqual(m_ExpectedResultPoints[i] * (int)locationTypeName, serie.PointsWithValue[i].YValue.Value);
				}	
			}
		}

		[TestCase(ChartLocalisation.BySite)]
		[TestCase(ChartLocalisation.ByBuilding)]
		[TestCase(ChartLocalisation.ByFloor)]
		[TestCase(ChartLocalisation.BySpace)]
		[TestCase(ChartLocalisation.ByFurniture)]
		public void Chart_GeographicAndClassificationAggregation_ParentClassificationAggregation_AggregationIsPerformedWithParentClassificationLevel(ChartLocalisation chartLocalisation) {
			
			//Arrange
			TestDataContainer.Chart.Localisation = chartLocalisation;
			TestDataContainer.Chart.ClassificationLevel = 1; // the highest level in our classification hierarchy

			//Add another meter to every level with a sub classification
			int keyMeter1 = 114;
			int keyMeter2 = 115;
			int keyMeter3 = 116;
			int keyMeter4 = 117;
			int keyMeter5 = 118;
			TestDataContainer.Meters.AddRange(new List<Meter>()
              {
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter1), KeyLocation = const_site_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_sub_kwh_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter2), KeyLocation = const_building_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_sub_kwh_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter3), KeyLocation = const_floor_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_sub_kwh_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter4), KeyLocation = const_space_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_sub_kwh_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter5), KeyLocation = const_equipment_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_sub_kwh_classification_key},
              });

			//Generate data for the additional meters using the first meter
			var defaultMeterData = TestDataContainer.MeterData[DefaultMeterKey];
			TestDataContainer.MeterData[keyMeter1] = CloneMeterData(defaultMeterData, keyMeter1);
			TestDataContainer.MeterData[keyMeter2] = CloneMeterData(defaultMeterData, keyMeter2);
			TestDataContainer.MeterData[keyMeter3] = CloneMeterData(defaultMeterData, keyMeter3);
			TestDataContainer.MeterData[keyMeter4] = CloneMeterData(defaultMeterData, keyMeter4);
			TestDataContainer.MeterData[keyMeter5] = CloneMeterData(defaultMeterData, keyMeter5);
			InitializeTest();


			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			HierarchySpatialTypeName locationTypeName = ConvertChartLocalisationToHierarchySpatial(chartLocalisation);
			IEnumerable<string> locationKeys = TestDataContainer.Locations.Values.Where(x => x.TypeName == locationTypeName).Select(x => x.KeyLocation);
			DataSerie resultSerie = resultSeries.First(x => locationKeys.Contains(x.KeyLocation));
			List<Meter> metersInLocation = GetMetersInLocation(locationKeys);

			for (int i = 0; i < m_ExpectedResultPoints.Count; i++) {
				Assert.AreEqual(m_ExpectedResultPoints[i] * (int)locationTypeName * metersInLocation.Count, resultSerie.PointsWithValue[i].YValue.Value);
			}
		}

		[TestCase(ChartLocalisation.ByBuilding)]
		[TestCase(ChartLocalisation.ByFloor)]
		[TestCase(ChartLocalisation.BySpace)]
		[TestCase(ChartLocalisation.ByFurniture)]
		public void Chart_GeographicClustering_MetersInHigherAggregationLocation_AggregatedByLocationInAllHigherLocations(ChartLocalisation chartLocalisation) {

			//Arrange
			TestDataContainer.Chart.Localisation = chartLocalisation;

			//Add another meter to every level  with a classification
			int keyMeter1 = 114;
			int keyMeter2 = 115;
			int keyMeter3 = 116;
			int keyMeter4 = 117;
			int keyMeter5 = 118;
			TestDataContainer.Meters.AddRange(new List<Meter>()
              {
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter1), KeyLocation = const_site_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_kwh_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter2), KeyLocation = const_building_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_kwh_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter3), KeyLocation = const_floor_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_kwh_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter4), KeyLocation = const_space_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_kwh_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter5), KeyLocation = const_equipment_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_kwh_classification_key},
              });

			//Generate data for the additional meters using the first meter
			var defaultMeterData = TestDataContainer.MeterData[DefaultMeterKey];
			TestDataContainer.MeterData[keyMeter1] = CloneMeterData(defaultMeterData, keyMeter1);
			TestDataContainer.MeterData[keyMeter2] = CloneMeterData(defaultMeterData, keyMeter2);
			TestDataContainer.MeterData[keyMeter3] = CloneMeterData(defaultMeterData, keyMeter3);
			TestDataContainer.MeterData[keyMeter4] = CloneMeterData(defaultMeterData, keyMeter4);
			TestDataContainer.MeterData[keyMeter5] = CloneMeterData(defaultMeterData, keyMeter5); 

			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert 
			IEnumerable<ChartLocalisation> higherGroupingLocationsTypeNames =
				 Enum.GetValues(typeof(ChartLocalisation))
					  .Cast<ChartLocalisation>()
					  .Where(enumValue => (int)enumValue > (int)chartLocalisation);

			//go over all of the locations that are higher in heirarchy then the tested location and validate their results
			foreach (ChartLocalisation higherGroupingLocationTypeName in higherGroupingLocationsTypeNames) {
				HierarchySpatialTypeName higherLocationTypeName = ConvertChartLocalisationToHierarchySpatial(higherGroupingLocationTypeName);

				IEnumerable<string> locationKeys = TestDataContainer.Locations.Values.Where(x => x.TypeName == higherLocationTypeName).Select(x => x.KeyLocation);

				List<Meter> metersInLocation = GetMetersInLocation(locationKeys);

				DataSerie resultSerie = resultSeries.First(x => locationKeys.Contains(x.KeyLocation));

				for (int i = 0; i < m_ExpectedResultPoints.Count; i++) {
					Assert.AreEqual(m_ExpectedResultPoints[i] * metersInLocation.Count, resultSerie.PointsWithValue[i].YValue.Value);
				}
			}
		}

		[TestCase(ChartLocalisation.BySite)]
		[TestCase(ChartLocalisation.ByBuilding)]
		[TestCase(ChartLocalisation.ByFloor)]
		[TestCase(ChartLocalisation.BySpace)]
		[TestCase(ChartLocalisation.ByFurniture)]
		public void Chart_GeographicAndClassificationAggregation_DifferentClassificationAggregation_AggregationIsPerformedPerClassificationType(ChartLocalisation chartLocalisation) {
			
			//Arrange
			TestDataContainer.Chart.Localisation = chartLocalisation;

			//Add another meter to every level  with a different classification
			int keyMeter1 = 114;
			int keyMeter2 = 115;
			int keyMeter3 = 116;
			int keyMeter4 = 117;
			int keyMeter5 = 118;
			TestDataContainer.Meters.AddRange(new List<Meter>()
              {
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter1), KeyLocation = const_site_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_temperature_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter2), KeyLocation = const_building_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_temperature_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter3), KeyLocation = const_floor_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_temperature_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter4), KeyLocation = const_space_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_temperature_classification_key},
                        new Meter {KeyMeter = MeterHelper.ConvertMeterKeyToString(keyMeter5), KeyLocation = const_equipment_key, Name = "Test", Factor = 1.0, KeyClassificationItem = const_temperature_classification_key},
              });

			//Generate data for the additional meters using the first meter
			var defaultMeterData = TestDataContainer.MeterData[DefaultMeterKey];
			TestDataContainer.MeterData[keyMeter1] = CloneMeterData(defaultMeterData, keyMeter1);
			TestDataContainer.MeterData[keyMeter2] = CloneMeterData(defaultMeterData, keyMeter2);
			TestDataContainer.MeterData[keyMeter3] = CloneMeterData(defaultMeterData, keyMeter3);
			TestDataContainer.MeterData[keyMeter4] = CloneMeterData(defaultMeterData, keyMeter4);
			TestDataContainer.MeterData[keyMeter5] = CloneMeterData(defaultMeterData, keyMeter5);
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			var locationTypeName = ConvertChartLocalisationToHierarchySpatial(chartLocalisation);
			var locations = TestDataContainer.Locations.Values.Where(x => x.TypeName == locationTypeName).Select(x => x.KeyLocation);
			var resultSerie = resultSeries.Where(x => locations.Contains(x.KeyLocation));

			//Assert 
			foreach (DataSerie serie in resultSerie) {  //we are validating that each classification type is aggragated seperatly
				for (int i = 0; i < m_ExpectedResultPoints.Count; i++) {
					Assert.AreEqual(m_ExpectedResultPoints[i] * (int)locationTypeName, serie.PointsWithValue[i].YValue.Value);
				}
			}
		}

		private List<Meter> GetMetersInLocation(IEnumerable<string> locationKeys) {
			var metersInLocation = new List<Meter>();
			foreach (var locationKey in locationKeys) {
				metersInLocation.AddRange(TestDataContainer.Meters.Where(m => m.KeyLocation == locationKey));
			}

			return metersInLocation;
		}

		private HierarchySpatialTypeName ConvertChartLocalisationToHierarchySpatial(ChartLocalisation chartLocalisation) {
			switch (chartLocalisation) {
				case ChartLocalisation.ByMeter:
					return HierarchySpatialTypeName.IfcMeter;
				case ChartLocalisation.ByFurniture:
					return HierarchySpatialTypeName.IfcFurniture;
				case ChartLocalisation.BySpace:
					return HierarchySpatialTypeName.IfcSpace;
				case ChartLocalisation.ByFloor:
					return HierarchySpatialTypeName.IfcBuildingStorey;
				case ChartLocalisation.ByBuilding:
					return HierarchySpatialTypeName.IfcBuilding;
				case ChartLocalisation.BySite:
					return HierarchySpatialTypeName.IfcSite;
				default:
					throw new ArgumentOutOfRangeException("chartLocalisation");
			}
		}

		protected override string TestMeterDataFilePath {
			get { return const_meter_data_file_name; }
		}

		protected override TestDataContainer InitializeTestDataContainer() {

			var testDataContainer = base.InitializeTestDataContainer();
		
				testDataContainer.ClassificationItems = new Dictionary<string, ClassificationItem>
				{
					{
						const_kwh_classification_key, new ClassificationItem {KeyClassificationItem = const_kwh_classification_key,Level = 1,LocalId = "METERCLASS_kwh", LongPath = "kwh", Title = const_khw_classification_title}
					},
					{
						const_sub_kwh_classification_key, new ClassificationItem {KeyClassificationItem = const_sub_kwh_classification_key,Level = 2, KeyParent = const_kwh_classification_key, LocalId = "METERCLASS_sub_kwh", LongPath = "kwh/sub_kwh", Title = const_sub_khw_classification_title}
					}, 
					{
						const_temperature_classification_key, new ClassificationItem{KeyClassificationItem = const_temperature_classification_key,Level = 1,LocalId = "METERCLASS_temperature", LongPath = "temperature", Title = const_temperature_classification_title}
					},
					
				};

			#region Locations Init
				Location root = new Location {
					KeyLocation = const_root_site_key,
					KeyLocationPath = "/" + const_root_site_key,
					TypeName = HierarchySpatialTypeName.IfcSite,
					Name = "test root site"
				};
				Location site = new Location {
					KeyLocation = const_site_key,
					KeyLocationPath = root.KeyLocationPath + "/" + const_site_key,
					KeyParent = root.KeyLocation,
					Level = 1,
					TypeName = HierarchySpatialTypeName.IfcSite,
					Name = "test site"
				};
				Location building = new Location {
					KeyLocation = const_building_key,
					KeyLocationPath = site.KeyLocationPath + "/" + const_building_key,
					KeyParent = site.KeyLocation,
					Level = 2,
					TypeName = HierarchySpatialTypeName.IfcBuilding,
					Name = "test building"
				};

				Location floor = new Location {
					KeyLocation = const_floor_key,
					KeyLocationPath = building.KeyLocationPath + "/" + const_floor_key,
					KeyParent = building.KeyLocation,
					Level = 3,
					TypeName = HierarchySpatialTypeName.IfcBuildingStorey,
					Name = "test floor",
					IfcType = "IfcBuildingStorey"
				};

				Location space = new Location {
					KeyLocation = const_space_key,
					KeyLocationPath = floor.KeyLocationPath + "/" + const_space_key,
					KeyParent = floor.KeyLocation,
					Level = 4,
					TypeName = HierarchySpatialTypeName.IfcSpace,
					Name = "test space",
					IfcType = "IfcSpace"
				};

				Location equipment = new Location {
					KeyLocation = const_equipment_key,
					KeyLocationPath = space.KeyLocationPath + "/" + const_equipment_key,
					KeyParent = space.KeyLocation,
					Level = 5,
					TypeName = HierarchySpatialTypeName.IfcFurniture,
					Name = "test equipment"
				};

				#endregion
			
			testDataContainer.Locations = new Dictionary<string, Location>
			{
				{const_root_site_key, root},
				{const_site_key, site},
				{const_building_key, building},
				{const_floor_key, floor},
				{const_space_key, space},
				{const_equipment_key, equipment}
			};

			foreach (Location location in testDataContainer.Locations.Values){
				location.LongPath = location.KeyLocationPath;
			}

			//Meters
			var meter1 = new Meter {KeyMeter = m_DefaultKeyMeterStr, KeyLocation = const_site_key, Name = "Test Site Meter", Factor = 1.0, KeyClassificationItem = const_kwh_classification_key};
			var meter2 = new Meter {KeyMeter = "#106", KeyLocation = const_building_key, Name = "Test Building Meter", Factor = 1.0, KeyClassificationItem = const_kwh_classification_key };
			var meter3 = new Meter {KeyMeter = "#107", KeyLocation = const_floor_key, Name = "Test Floor Meter", Factor = 1.0, KeyClassificationItem = const_kwh_classification_key};
			var meter4 = new Meter {KeyMeter = "#108", KeyLocation = const_space_key, Name = "Test Space Meter", Factor = 1.0, KeyClassificationItem = const_kwh_classification_key};
			var meter5 = new Meter {KeyMeter = "#109", KeyLocation = const_equipment_key, Name = "Test Equipment Meter", Factor = 1.0, KeyClassificationItem = const_kwh_classification_key};

			var meter1Data = testDataContainer.MeterData[DefaultMeterKey];
			testDataContainer.Meters = new List<Meter>
			{
				meter1,
				meter2,
				meter3,
				meter4,
				meter5
			};
			testDataContainer.MeterData = new ConcurrentDictionary<int, SortedList<DateTime, MD>>();

			testDataContainer.MeterData[DefaultMeterKey] = meter1Data;
			int meter2Key = MeterHelper.ConvertMeterKeyToInt(meter2.KeyMeter);
			testDataContainer.MeterData[meter2Key] = CloneMeterData(meter1Data, meter2Key);
			int meter3Key = MeterHelper.ConvertMeterKeyToInt(meter3.KeyMeter);
			testDataContainer.MeterData[meter3Key] = CloneMeterData(meter1Data, meter3Key);
			int meter4Key = MeterHelper.ConvertMeterKeyToInt(meter4.KeyMeter);
			testDataContainer.MeterData[meter4Key] = CloneMeterData(meter1Data, meter4Key);
			int meter5Key = MeterHelper.ConvertMeterKeyToInt(meter5.KeyMeter);
			testDataContainer.MeterData[meter5Key] = CloneMeterData(meter1Data, meter5Key);
			
			return testDataContainer;
		}

	}
}

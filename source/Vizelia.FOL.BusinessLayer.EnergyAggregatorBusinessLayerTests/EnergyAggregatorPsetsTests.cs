using System;
using System.Collections.Generic;
using System.Linq;
using NCalc;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests
{
	public class EnergyAggregatorPsetsTests : BaseEnergyAggregatorBusinessLayerTests
	{
		private readonly string m_DataSerieName = string.Format("{0} - {1}", DefaultMeterName, DefaultClassificationItemName);
		private readonly string m_DataSerieLocalId = string.Format("{0} - {1}", DefaultLocationName + " / " + DefaultMeterName, DefaultClassificationItemName);
		private const string const_meter_data_file_name =
			"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.Common.MonthlyMeterData.txt";

		/// <summary>
		/// Holds the raw series of a chart before any pset ratio calculations
		/// </summary>
		private List<DataSerie> m_SeriesWithoutPsetRatio;

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();
			
			//Generate data series on the chart without pset ratio as a baseline for all tests results
			InitializeTest();
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			m_SeriesWithoutPsetRatio = result.Series.ToList();
			EnergyAggregator.SetIsInitialized(false);
		}

		[TearDown]
		public override void TearDown()
		{
			base.TearDown();
			m_SeriesWithoutPsetRatio = null;
		}

		protected override string TestMeterDataFilePath
		{
			get { return const_meter_data_file_name; }
		}

		/// <summary>
		/// Tests the functionality of pset ratio on an existing series in a chart.
		/// </summary>
		/// <param name="psetRatioOperator">The psetRatioOperator performed between the historical pset data and the series data for the pset ratio calculation</param>
		[TestCase(ArithmeticOperator.Multiply)]
		[TestCase(ArithmeticOperator.Divide)]
		public void Chart_PsetRatioOnMeterSeries_SeriesDataCalculatedAccordingly(ArithmeticOperator psetRatioOperator)
		{
			//Arrange 
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Define historical pset for each date of the meter data
			const int psetAttibuteValue = 10;
			foreach (DateTime date in testMeterData.Keys)
			{
				var psetAttributeHistorical = GenerateHistoricalPsetAttribute(date, psetAttibuteValue, DefaultKeyMeterStr);
				TestDataContainer.HistoricalData.Add(psetAttributeHistorical.KeyPsetAttributeHistorical, psetAttributeHistorical);
			}

			InitializeTest();

			AddTestSerieToTheChart();

			//Add pset ratio to the data series
			TestDataContainer.Chart.DataSeries[0].PsetRatios = new List<DataSeriePsetRatio>()
			{
				new DataSeriePsetRatio()
				{
					KeyChart = TestChartkey,
					KeyPropertySingleValue = "1",
					UsageName = "IfcMeter",
					KeyDataSerie = TestDataContainer.Chart.DataSeries[0].KeyDataSerie,
					Operator = psetRatioOperator,
					Order = 1
				}
			};
			
			//Act
			var resultWithPsetRatio = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			DataSerie resultSerieWithPsetRatio = resultWithPsetRatio.Series.ToList()[0];

			var pointsBeforePsetRatio = m_SeriesWithoutPsetRatio[0].Points;
			
			//Validate that all of the meter data have been calculated according to the historical pest values
			for (int i = 0; i < resultSerieWithPsetRatio.Points.Count; i++)
			{
				var valueBeforePsetRatio = pointsBeforePsetRatio[i].YValue.Value;
				var valueAfterPsetRatio = resultSerieWithPsetRatio.Points[i].YValue.Value;

				double expected = CalculationHelper.ExecuteOperationByOperator(psetRatioOperator, valueBeforePsetRatio, psetAttibuteValue);
				Assert.AreEqual(expected, valueAfterPsetRatio);
			}
		}

		/// <summary>
		/// Tests the functionality of pset ratio on an existing series in a chart with Time Aggregation on the data.
		/// </summary>
		/// <param name="psetRatioOperator">The operation performed between the historical pset data and the series data for the pset ratio calculation</param>
		/// <param name="groupOperator">The grouping psetRatioOperator that is performed on the historical pset data during the time aggregation</param>
		[Test, Combinatorial]
		public void Chart_PsetRatioOnMeterSeriesWithTimeAggregation_SeriesDataCalculatedAccordingly(
			[Values(ArithmeticOperator.Multiply, ArithmeticOperator.Divide)] ArithmeticOperator psetRatioOperator,
			[Values(MathematicOperator.SUM, MathematicOperator.MIN, MathematicOperator.MAX, MathematicOperator.AVG)] MathematicOperator groupOperator)
		{
			//Arrange 
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Generate historical pset with different values for each date of the meter data
			int i = 1;
			foreach (DateTime date in testMeterData.Keys)
			{
				PsetAttributeHistorical psetAttributeHistorical = GenerateHistoricalPsetAttribute(date, i, DefaultKeyMeterStr);
				TestDataContainer.HistoricalData.Add(psetAttributeHistorical.KeyPsetAttributeHistorical, psetAttributeHistorical);
				i++;
			}

			InitializeTest();

			AddTestSerieToTheChart();

			//Add pset ratio to the data series
			TestDataContainer.Chart.DataSeries[0].PsetRatios = new List<DataSeriePsetRatio>()
			{
				new DataSeriePsetRatio()
				{
					KeyChart = TestChartkey,
					KeyPropertySingleValue = "1",
					UsageName = "IfcMeter",
					KeyDataSerie = TestDataContainer.Chart.DataSeries[0].KeyDataSerie,
					Operator = psetRatioOperator,
					Order = 1,
					GroupOperation = groupOperator
				}
			};

			//Define TimeInterval Aggregation to expect that all of the values(meter and historical) will be aggregated
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Global;

			//Act
			var resultWithPsetRatio = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			DataSerie resultSerieWithPsetRatio = resultWithPsetRatio.Series.ToList()[0];

			//Calculate the expected Result
			//First perform the group psetRatioOperator on the historical data
			string formula = CalculationHelper.ConvertMathematicOperatorToNCalcFormula(groupOperator);
			Expression ncalcExpression = new Expression(formula, EvaluateOptions.IgnoreCase);
			ncalcExpression.EvaluateFunction += CalculationHelper.CalculatedSerieCalculations;
			ncalcExpression.Parameters["x"] = TestDataContainer.HistoricalData.Values.Select(v => v.AttributeValue);
			var historicalPsetGrouppedResult = (double) ncalcExpression.Evaluate();

			//Calculate the expected result of the meter data aggregation.
			double meterDataAggregationResult = m_SeriesWithoutPsetRatio[0].Points.Select(p => p.YValue).Sum().GetValueOrDefault();

			//Perform Arithmetic psetRatioOperator to calculate the final expected result
			double expectedResult = CalculationHelper.ExecuteOperationByOperator(psetRatioOperator, meterDataAggregationResult, historicalPsetGrouppedResult);

			//Assert
			Assert.AreEqual(resultSerieWithPsetRatio.Points.Count, 1);//Only one point should be returned since all values should be aggregated
			Assert.AreEqual(expectedResult, resultSerieWithPsetRatio.Points[0].YValue.Value);
		}

	
		/// <summary>
		/// Tests the functionality of pset ratio with "FillMissingValues" turned on on an existing series in a chart and a historical pset with some missing values vs the series dates
		/// </summary>
		/// <param name="psetRatioOperator">The operation performed between the historical pset data and the series data for the pset ratio calculation</param>
		[TestCase(ArithmeticOperator.Multiply)]
		[TestCase(ArithmeticOperator.Divide)]
		public void Chart_PsetRatioOnMeterSeriesMissingHistoricalValueWithFillMissingValues_MissingValuesAreFilledAndRatioCalculatedOnMissingValues(ArithmeticOperator psetRatioOperator)
		{
			//Arrange 
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Define a historical pset with a single value 
			const int psetAttibuteValue = 10;
			var psetAttributeHistorical = GenerateHistoricalPsetAttribute(testMeterData.Keys[0], psetAttibuteValue, DefaultKeyMeterStr);
			TestDataContainer.HistoricalData.Add(psetAttributeHistorical.KeyPsetAttributeHistorical, psetAttributeHistorical);

			InitializeTest();

			AddTestSerieToTheChart();
			//Add pset ratio to the data series
			TestDataContainer.Chart.DataSeries[0].PsetRatios = new List<DataSeriePsetRatio>()
			{
				new DataSeriePsetRatio()
				{
					KeyChart = TestChartkey,
					KeyPropertySingleValue = "1",
					UsageName = "IfcMeter",
					KeyDataSerie = TestDataContainer.Chart.DataSeries[0].KeyDataSerie,
					Operator = psetRatioOperator,
					Order = 1,
					FillMissingValues = true // Turn on the fill missing values functionality
				}
			};

			//Act
			var resultWithPsetRatio = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			DataSerie resultSerieWithPsetRatio = resultWithPsetRatio.Series.ToList()[0];

			//Validate that all of the values in the series have been affected by the pset ratio despite the missing values in the historical data  
			for (int i = 0; i < resultSerieWithPsetRatio.Points.Count; i++)
			{
				var valueBeforePsetRatio = m_SeriesWithoutPsetRatio[0].Points[i].YValue.Value;
				var valueAfterPsetRatio = resultSerieWithPsetRatio.Points[i].YValue.Value;

				double expected = CalculationHelper.ExecuteOperationByOperator(psetRatioOperator, valueBeforePsetRatio, psetAttibuteValue);
				Assert.AreEqual(expected, valueAfterPsetRatio);
			}
		}

		/// <summary>
		/// Tests the functionality of pset ratio with "FillMissingValues" turned off on an existing series in a chart and a historical pset with some missing values vs the series dates
		/// </summary>
		/// <param name="psetRatioOperator">The operation performed between the historical pset data and the series data for the pset ratio calculation</param>
		[TestCase(ArithmeticOperator.Multiply)]
		[TestCase(ArithmeticOperator.Divide)]
		public void Chart_PsetRatioOnMeterSeriesWithMissingHistoricalValueWithoutFillMissingValues_MissingValuesAreNotFilledAndSeriesIsNotCalculatedOnMissingValues(ArithmeticOperator psetRatioOperator) {

			//Arrange 
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Define a historical pset with a single value 
			const int psetAttibuteValue = 10;
			var psetAttributeHistorical = GenerateHistoricalPsetAttribute(testMeterData.Keys[0], psetAttibuteValue, DefaultKeyMeterStr);
			TestDataContainer.HistoricalData.Add(psetAttributeHistorical.KeyPsetAttributeHistorical, psetAttributeHistorical);
			InitializeTest();
			
			AddTestSerieToTheChart();
			//Add pset ratio to the data series
			TestDataContainer.Chart.DataSeries[0].PsetRatios = new List<DataSeriePsetRatio>()
			{
				new DataSeriePsetRatio()
				{
					KeyChart = TestChartkey,
					KeyPropertySingleValue = "1",
					UsageName = "IfcMeter",
					KeyDataSerie = TestDataContainer.Chart.DataSeries[0].KeyDataSerie,
					Operator = psetRatioOperator,
					Order = 1,
					FillMissingValues = false // Turn off the fill missing values functionality
				}
			};

			//Remove the calculated points from the data series so they will be recalculated 
			TestDataContainer.Chart.DataSeries[0].Points = null;

			//Act
			var resultWithPsetRatio = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			DataSerie resultSerieWithPsetRatio = resultWithPsetRatio.Series.ToList()[0];
			
			for (int i = 1; i < resultSerieWithPsetRatio.Points.Count; i++) {

				var valueBeforePsetRatio = m_SeriesWithoutPsetRatio[0].Points[i].YValue.Value;
				var valueAfterPsetRatio = resultSerieWithPsetRatio.Points[i].YValue.Value;

				double resultWithCalculation = CalculationHelper.ExecuteOperationByOperator(psetRatioOperator, valueBeforePsetRatio, psetAttibuteValue);
				
				//Validate that only the first value of the serie has been affected by the pset ratio
				//Since the historical pset has been defined only for that date and the fill missing values functionality is turned off
				if (i == 0)
				{
					Assert.AreEqual(resultWithCalculation, valueAfterPsetRatio);
				}
				else
				{
					Assert.AreNotEqual(resultWithCalculation, valueAfterPsetRatio);
				}
			}
		}

		/// <summary>
		/// Tests the functionality of pset ratio with "StopAtFirstLevel" turned off on an existing series in a chart
		///  Historical pset values are defined on multiple sites
		/// </summary>
		/// <param name="psetRatioOperator">The operation performed between the historical pset data and the series data for the pset ratio calculation</param>

		[TestCase(ArithmeticOperator.Multiply)]
		[TestCase(ArithmeticOperator.Divide)]
		public void Chart_PsetRatioOnDifferentLocationsInHeirarchyStopAtFirstLevelIsFalse_SeriesDataCalculatedAccordingToFirstLevelData(ArithmeticOperator psetRatioOperator) {
			//Arrange 
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Create Spacial Hierarchy
			Location site1 = new Location
			{
				KeyLocation = "#1",
				KeyLocationPath = "#1",
				Level = 0,
				TypeName = HierarchySpatialTypeName.IfcSite,
				Name = "test site1"
			};

			Location site2 = new Location
			{
				KeyLocation = "#2",
				KeyLocationPath = site1.KeyLocationPath + "/" + "#2",
				KeyParent = site1.KeyLocation,
				Level = 1,
				TypeName = HierarchySpatialTypeName.IfcSite,
				Name = "test site2"
			};

			Location site3 = new Location
			{
				KeyLocation = DefaultKeyLocation,
				KeyLocationPath = site2.KeyLocationPath + "/" + DefaultKeyLocation,
				KeyParent = site2.KeyLocation,
				Level = 3,
				TypeName = HierarchySpatialTypeName.IfcSite,
				Name = "test site3"
			};

			TestDataContainer.Locations = new Dictionary<string, Location>
			{
				{site1.KeyLocation, site1},
				{site2.KeyLocation, site2},
				{DefaultKeyLocation, site3},
			};

			foreach (Location location in TestDataContainer.Locations.Values) {
				location.LongPath = location.KeyLocationPath;
			}

			const int psetAttibuteValue = 10;

			//Generate historical pset data on all of the sites in the heirarchy
			foreach (DateTime date in testMeterData.Keys)
			{
				PsetAttributeHistorical site1Pset = GenerateHistoricalPsetAttribute(date, psetAttibuteValue, site1.KeyLocation);
				TestDataContainer.HistoricalData.Add(site1Pset.KeyPsetAttributeHistorical, site1Pset);

				PsetAttributeHistorical site2Pset = GenerateHistoricalPsetAttribute(date, psetAttibuteValue, site2.KeyLocation);
				TestDataContainer.HistoricalData.Add(site2Pset.KeyPsetAttributeHistorical, site2Pset);

				PsetAttributeHistorical site3Pset = GenerateHistoricalPsetAttribute(date, psetAttibuteValue, site3.KeyLocation);
				TestDataContainer.HistoricalData.Add(site3Pset.KeyPsetAttributeHistorical, site3Pset);
			}
		
			InitializeTest();
			string serieLocalId = string.Format("{0} - {1}", site1.KeyLocation + "/" + site2.KeyLocation + "/" + site3.KeyLocation + " / " + DefaultMeterName, DefaultClassificationItemName);

			AddTestSerieToTheChart(serieLocalId);

			//Add pset ratio to the data series
			TestDataContainer.Chart.DataSeries[0].PsetRatios = new List<DataSeriePsetRatio>()
			{
				new DataSeriePsetRatio()
				{
					KeyChart = TestChartkey,
					KeyPropertySingleValue = "1",
					UsageName = "IfcSite",
					KeyDataSerie = TestDataContainer.Chart.DataSeries[0].KeyDataSerie,
					Operator = psetRatioOperator,
					Order = 1,
					StopAtFirstLevelEncountered = false
				}
			};

			//Act
			var resultWithPsetRatio = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			DataSerie resultSerieWithPsetRatio = resultWithPsetRatio.Series.ToList()[0];

			var pointsBeforePsetRatio = m_SeriesWithoutPsetRatio[0].Points;

			int numberOfLocationsInHeirarchy = TestDataContainer.Locations.Count;
			
			//Validate that all of the meter data have been calculated according to the historical pest values
			for (int i = 0; i < resultSerieWithPsetRatio.Points.Count; i++) {
				var valueBeforePsetRatio = pointsBeforePsetRatio[i].YValue.Value;
				var valueAfterPsetRatio = resultSerieWithPsetRatio.Points[i].YValue.Value;

				//Since StopAtFirstLevelEncountered is turned off, we expect that the pset ratio is calculated by summing the results from all values 
				//in all the hierarchal locations of the pset type
				double expected = CalculationHelper.ExecuteOperationByOperator(psetRatioOperator, valueBeforePsetRatio, numberOfLocationsInHeirarchy * psetAttibuteValue);
				Assert.AreEqual(expected, valueAfterPsetRatio);
			}
		}

		/// <summary>
		/// Tests the functionality of pset ratio with "StopAtFirstLevel" turned on on an existing series in a chart
		/// Historical pset values are defined on multiple sites
		/// </summary>
		[TestCase(ArithmeticOperator.Multiply)]
		[TestCase(ArithmeticOperator.Divide)]
		public void Chart_PsetRatioOnDifferentLocationsInHeirarchyStopAtFirstLevelIsTrue_SeriesDataCalculatedAccordingToFirstLevelData(ArithmeticOperator psetRatioOperator) {
			//Arrange 
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Create SPacial Hierarchy
			Location site1 = new Location {
				KeyLocation = "#1",
				KeyLocationPath =  "#1",
				Level = 0,
				TypeName = HierarchySpatialTypeName.IfcSite,
				Name = "test site1"
			};
			Location site2 = new Location {
				KeyLocation = "#2",
				KeyLocationPath = site1.KeyLocationPath + "/" + "#2",
				KeyParent = site1.KeyLocation,
				Level = 1,
				TypeName = HierarchySpatialTypeName.IfcSite,
				Name = "test site2"
			};
			Location site3 = new Location {
				KeyLocation = DefaultKeyLocation,
				KeyLocationPath = site2.KeyLocationPath + "/" + DefaultKeyLocation,
				KeyParent = site2.KeyLocation,
				Level = 3,
				TypeName = HierarchySpatialTypeName.IfcSite,
				Name = "test site3"
			};

			TestDataContainer.Locations = new Dictionary<string, Location>
			{
				{site1.KeyLocation, site1},
				{site2.KeyLocation, site2},
				{DefaultKeyLocation, site3},
			};
			foreach (Location location in TestDataContainer.Locations.Values) {
				location.LongPath = location.KeyLocationPath;
			}

			const int psetAttibuteValue = 10;

			//Generate historical pset data on all of the sites in the heirarchy
			foreach (DateTime date in testMeterData.Keys) {
				PsetAttributeHistorical site1Pset = GenerateHistoricalPsetAttribute(date, psetAttibuteValue, site1.KeyLocation);
				TestDataContainer.HistoricalData.Add(site1Pset.KeyPsetAttributeHistorical, site1Pset);

				PsetAttributeHistorical site2Pset = GenerateHistoricalPsetAttribute(date, psetAttibuteValue, site2.KeyLocation);
				TestDataContainer.HistoricalData.Add(site2Pset.KeyPsetAttributeHistorical, site2Pset);

				PsetAttributeHistorical site3Pset = GenerateHistoricalPsetAttribute(date, psetAttibuteValue, site3.KeyLocation);
				TestDataContainer.HistoricalData.Add(site3Pset.KeyPsetAttributeHistorical, site3Pset);
			}

			InitializeTest();

			string serieLocalId = string.Format("{0} - {1}", site1.KeyLocation + "/" + site2.KeyLocation + "/" + site3.KeyLocation + " / " + DefaultMeterName, DefaultClassificationItemName);
			AddTestSerieToTheChart(serieLocalId);

			//Add pset ratio to the data series
			TestDataContainer.Chart.DataSeries[0].PsetRatios = new List<DataSeriePsetRatio>()
			{
				new DataSeriePsetRatio()
				{
					KeyChart = TestChartkey,
					KeyPropertySingleValue = "1",
					UsageName = "IfcSite",
					KeyDataSerie = TestDataContainer.Chart.DataSeries[0].KeyDataSerie,
					Operator = psetRatioOperator,
					Order = 1,
					StopAtFirstLevelEncountered = true
				}
			};

			//Act
			var resultWithPsetRatio = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			DataSerie resultSerieWithPsetRatio = resultWithPsetRatio.Series.ToList()[0];

			var pointsBeforePsetRatio = m_SeriesWithoutPsetRatio[0].Points;
			//Validate that all of the meter data have been calculated according to the historical pest values
			for (int i = 0; i < resultSerieWithPsetRatio.Points.Count; i++) {
				var valueBeforePsetRatio = pointsBeforePsetRatio[i].YValue.Value;
				var valueAfterPsetRatio = resultSerieWithPsetRatio.Points[i].YValue.Value;

				//Since StopAtFirstLevelEncountered is turned on, we expect that the pset ratio is calculated only by one location
				double expected = CalculationHelper.ExecuteOperationByOperator(psetRatioOperator, valueBeforePsetRatio, psetAttibuteValue);
				Assert.AreEqual(expected, valueAfterPsetRatio);
			}
		}

		private PsetAttributeHistorical GenerateHistoricalPsetAttribute(DateTime date, double value, string locationKey)
		{
			string psetKey = Guid.NewGuid().ToString();
			var psetAttributeHistorical = new PsetAttributeHistorical()
			{
				AttributeAcquisitionDateTime = date,
				AttributeValue = value,
				KeyObject = locationKey,
				KeyPropertySingleValue = "1",
				KeyPsetAttributeHistorical = psetKey
			};

			return psetAttributeHistorical;
		}

		private void AddTestSerieToTheChart(string serieLocalId = null)
		{
			var dataSerie = new DataSerie
			{
				KeyChart = TestChartkey,
				KeyDataSerie = "1",
				LocalId = string.IsNullOrEmpty(serieLocalId) ? m_DataSerieLocalId : serieLocalId,
				Name = m_DataSerieName,
				KeyMeter = DefaultKeyMeterStr
			};

			TestDataContainer.Chart.DataSeries = new List<DataSerie>() {dataSerie};
			TestDataContainer.Chart.Series = new DataSerieCollection(new List<DataSerie> {dataSerie});
		}

        private IEnumerable<Tuple<AxisTimeInterval, Func<DateTime, int, DateTime>>> FillMissingValuesSource
        {
            get
            {
                yield return new Tuple<AxisTimeInterval, Func<DateTime, int, DateTime>>(AxisTimeInterval.Days, (x, y) => x.AddDays(y));
                yield return new Tuple<AxisTimeInterval, Func<DateTime, int, DateTime>>(AxisTimeInterval.Months, (x, y) => x.AddMonths(y));                
                yield return new Tuple<AxisTimeInterval, Func<DateTime, int, DateTime>>(AxisTimeInterval.Hours, (x, y) => x.AddHours(y));
                
            }
	    }

       [TestCaseSource("FillMissingValuesSource")]
        public void Chart_HistoricalPset_FillMissingValues_ValuesAreFilledAccordingToTimeInterval(Tuple<AxisTimeInterval, Func<DateTime, int, DateTime>> timeInterval)
        {
            //Arrange 
            const int psetAttibuteValue = 10;
            SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];
            //Chart Setup
            TestDataContainer.Chart.TimeInterval = timeInterval.Item1;
            TestDataContainer.Chart.StartDate = new DateTime(2012, 1, 1);
            //Create Spacial Hierarchy
            #region Setup Location
            Location site1 = new Location
                {
                    KeyLocation = DefaultKeyLocation,
                    KeyLocationPath = DefaultKeyLocation,
                    Level = 0,
                    TypeName = HierarchySpatialTypeName.IfcSite,
                    Name = "test site1"
                };

            TestDataContainer.Locations = new Dictionary<string, Location>
			{
				{site1.KeyLocation, site1},
			};

            foreach (Location location in TestDataContainer.Locations.Values)
            {
                location.LongPath = location.KeyLocationPath;
            }

            //Generate historical pset data on all of the sites in the heirarchy
            foreach (DateTime date in testMeterData.Keys)
            {
                PsetAttributeHistorical site1Pset = GenerateHistoricalPsetAttribute(date, psetAttibuteValue, site1.KeyLocation);
                TestDataContainer.HistoricalData.Add(site1Pset.KeyPsetAttributeHistorical, site1Pset);
            } 
            #endregion

            InitializeTest();

            string serieLocalId = string.Format("{0} - {1}", site1.KeyLocation + "/" + DefaultMeterName, DefaultClassificationItemName);

            AddTestSerieToTheChart(serieLocalId);

           #region Create Historical Pset

           ChartPsetAttributeHistorical historicalPset = new ChartPsetAttributeHistorical {
               PsetName = "testPset",
               KeyPropertySingleValue = "1",
               FillMissingValues = true
           };
           TestDataContainer.Chart.HistoricalPsets = new List<ChartPsetAttributeHistorical> {historicalPset};

           #endregion

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var dataPoints = result.Series.ToList().First().PointsWithValue;
            var dataPointsFilledMissingValues = result.Series.ToList().Last().PointsWithValue;            
            //Assert
            for (int i = 0; i < dataPointsFilledMissingValues.Count; i++)
            {
                Assert.AreEqual(timeInterval.Item2(TestDataContainer.Chart.StartDate, i), dataPointsFilledMissingValues[i].XDateTime);
                Assert.AreEqual(psetAttibuteValue, dataPointsFilledMissingValues[i].YValue);
            }
         
        }

        [TestCase(AxisTimeInterval.Global)]
        [TestCase(AxisTimeInterval.None)]
       public void Chart_HistoricalPset_FillMissingValuesGlobalAndNone_NoMeterDataInTheChartDates_ValuesAreFilledAccordingToTimeInterval(AxisTimeInterval timeInterval)
       {
           //Arrange 
           SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];
           const int psetAttibuteValue = 10;
           //Chart Setup
           TestDataContainer.Chart.TimeInterval = timeInterval;
           TestDataContainer.Chart.StartDate = new DateTime(2014, 1, 1); //start date is after the meter data, thats why we fill the missing value in global or none
           //Create Spacial Hierarchy
           #region Setup Location
           Location site1 = new Location
           {
               KeyLocation = DefaultKeyLocation,
               KeyLocationPath = DefaultKeyLocation,
               Level = 0,
               TypeName = HierarchySpatialTypeName.IfcSite,
               Name = "test site1"
           };

           TestDataContainer.Locations = new Dictionary<string, Location>
			{
				{site1.KeyLocation, site1},
			};

           foreach (Location location in TestDataContainer.Locations.Values)
           {
               location.LongPath = location.KeyLocationPath;
           }

           //Generate historical pset data on all of the sites in the heirarchy
           foreach (DateTime date in testMeterData.Keys)
           {
               PsetAttributeHistorical site1Pset = GenerateHistoricalPsetAttribute(date, psetAttibuteValue, site1.KeyLocation);
               TestDataContainer.HistoricalData.Add(site1Pset.KeyPsetAttributeHistorical, site1Pset);
           }
           #endregion

           InitializeTest();

           string serieLocalId = string.Format("{0} - {1}", site1.KeyLocation + "/" + DefaultMeterName, DefaultClassificationItemName);

           AddTestSerieToTheChart(serieLocalId);

           #region Create Historical Pset

           ChartPsetAttributeHistorical historicalPset = new ChartPsetAttributeHistorical
           {
               PsetName = "testPset",
               KeyPropertySingleValue = "1",
               FillMissingValues = true
           };
           TestDataContainer.Chart.HistoricalPsets = new List<ChartPsetAttributeHistorical> { historicalPset };

           #endregion

           //Act
           var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
           var dataPointsFilledMissingValues = result.Series.ToList().Last().PointsWithValue;
           
           //Assert
           Assert.AreEqual(1, dataPointsFilledMissingValues.Count);
           Assert.AreEqual(psetAttibuteValue, dataPointsFilledMissingValues.First().YValue);

       }
	}
}
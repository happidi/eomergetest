﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using NCalc;
using NCalc.Domain;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {
	/// <summary>
	/// Holds calculated serie mathematical calculation
	/// </summary>
	public static class CalculationHelper {

		private const string PercentageFormula = "(b - a) / a";
		private const string DeltaFormula = "(b - a)";
		private const string HigherFormula = ">";
		private const string LowerFormula = "<";

		private static void Inverse(FunctionArgs args) {
			var value = (double)args.Parameters[0].Evaluate();
			args.Result = value != 0 ? Math.Pow(value, -1) : double.NaN;
		}

		private static void Percentage(FunctionArgs args) {
			CalculateIterateParameters(args, PercentageFormula);
		}

		private static void Delta(FunctionArgs args) {
			CalculateIterateParameters(args, DeltaFormula);
		}

		private static void CalculateIterateParameters(FunctionArgs args, string inExpression) {
			var param = args.Parameters[0].Parameters.First().Value;
			var firstList = ((IEnumerable<double>)param).Skip(1).ToList();
			var secondList = ((IEnumerable<double>)param).ToList();
			secondList.Insert(0, 1.0);//insert 1 at the begining of the list to get 0 in the first value.
			firstList.Insert(0, 1.0);
			secondList = secondList.Take(firstList.Count).ToList();
			Expression expression = new Expression(inExpression, EvaluateOptions.IterateParameters);
			expression.Parameters["a"] = secondList;
			expression.Parameters["b"] = firstList;
			args.Result = expression.Evaluate();

		}

		private static void YearToDate(FunctionArgs args) {
			var value = args.Parameters[0].Parameters.First().Value;
			var tmpNum = 0.0;
			args.Result = (from num in (IEnumerable<double>)value select tmpNum += num).ToList();
			
		}

		private static void Log(FunctionArgs args) {
			args.Result = Math.Log((double)args.Parameters[0].Evaluate());
		}

		private static void Abs(FunctionArgs args) {
			args.Result = (double)args.Parameters[0].Evaluate();
		}

		private static void Higher(FunctionArgs args) {
			ApplyBoolOperatorOnList(args, HigherFormula);
		}

		private static void Lower(FunctionArgs args) {
			ApplyBoolOperatorOnList(args, LowerFormula);
		}

		private static void ApplyBoolOperatorOnList(FunctionArgs args, string op) {
			var value = args.Parameters[0].Parameters.First().Value;
			var param = Convert.ToDouble(args.Parameters[1].Evaluate());
			Expression expression = new Expression("[x]" + op + param, EvaluateOptions.IterateParameters);
			expression.Parameters["x"] = value;
			var boolRes = (IList)expression.Evaluate();
			List<double> result = new List<double>();
			for (int i = 0; i < boolRes.Count; i++) {
				if ((bool)boolRes[i]) {
					result.Add(((IEnumerable<double>)value).ToList()[i]);
				}
			}

			args.Result = result;
		}

		private static void Average(FunctionArgs args) {
			var value = args.Parameters[0].Parameters.First().Value;
			args.Result = ((IEnumerable<double>)value).ToList().Average();
		}

		private static void First(FunctionArgs args) {
			var value = args.Parameters[0].Parameters.First().Value;
			args.Result = ((IEnumerable<double>)value).ToList().First();
		}

		private static void Last(FunctionArgs args) {
			var value = args.Parameters[0].Parameters.First().Value;
			args.Result = ((IEnumerable<double>)value).ToList().Last();
		}

		private static void Maximum(FunctionArgs args) {
			var value = args.Parameters[0].Parameters.First().Value;
			args.Result = ((IEnumerable<double>)value).ToList().Max();
		}

		private static void Minimum(FunctionArgs args) {
			var value = args.Parameters[0].Parameters.First().Value;
			args.Result = ((IEnumerable<double>)value).ToList().Min();
		}

		private static void Sum(FunctionArgs args) {
			var value = args.Parameters[0].Parameters.First().Value;
			args.Result = ((IEnumerable<double>)value).ToList().Sum();
		}

		private static void Penultimate(FunctionArgs args) {
			var value = args.Parameters[0].Parameters.First().Value;
			var argsList = ((IEnumerable<double>) value).ToList();
			var lastIndex = argsList.Count > 1 ? argsList.Count - 2 : 0;
			args.Result = argsList[lastIndex];
		}


		private static void Count(FunctionArgs args)
		{
			args.Result = 1.0;
		}


		/// <summary>
		/// Entry point for the Calculated serie calculations.
		/// </summary>
		/// <param name="operationName">The operation Name.</param>
		/// <param name="args">The args.</param>
		public static void CalculatedSerieCalculations(string operationName, FunctionArgs args) {
			switch (operationName) {
				case "inverse":
					Inverse(args);
					break;
				case "percentage":
					Percentage(args);
					break;
				case "delta":
					Delta(args);
					break;
				case "ytd":
					YearToDate(args);
					break;
				case "log":
					Log(args);
					break;
				case "abs":
					Abs(args);
					break;
				case "higher":
					Higher(args);
					break;
				case "lower":
					Lower(args);
					break;
				case "average":
					Average(args);
					break;
				case "first":
					First(args);
					break;
				case "last":
					Last(args);
					break;
				case "maximum":
					Maximum(args);
					break;
				case "minimum":
					Minimum(args);
					break;
				case "sum":
					Sum(args);
					break;
				case "penultimate":
					Penultimate(args);
					break;
				case "count":
					Count(args);
					break;

			}
		}


		public static string ConvertAlgebricFunctionToNCalcFormula(AlgebricFunction algebricFunction) {

			switch (algebricFunction)
			{
				case AlgebricFunction.Cos:
					return "cos(x)";
				case AlgebricFunction.Sin:
					return "sin(x)";
				case AlgebricFunction.Tan:
					return "tan(x)";
				case AlgebricFunction.Sqrt:
					return "sqrt([x])";
				case AlgebricFunction.Abs:
					return "abs([x])";
				case AlgebricFunction.Inverse:
					return "inverse([x])";
				case AlgebricFunction.Log:
					return "log([x])";
				case AlgebricFunction.Percentage:
					return "percentage([x])";
				case AlgebricFunction.Count:
					return "count([x])";
				case AlgebricFunction.Delta:
					return "delta([x])";
				case AlgebricFunction.YTD:
					return "ytd([x])";
				default:
					throw new ArgumentOutOfRangeException("algebricFunction");
			}
		}

		/// <summary>
		/// Converts the mathematic operator to NCalcFormula string representation.
		/// </summary>
		public static string ConvertMathematicOperatorToNCalcFormula(MathematicOperator mathematicOperator) {

			switch (mathematicOperator)
			{
				case MathematicOperator.SUM:
					return "sum([x])";
				case MathematicOperator.MAX:
					return "maximum([x])";
				case MathematicOperator.MIN:
					return "minimum([x])";
				case MathematicOperator.AVG:
					return "average([x])";
				default:
					throw new ArgumentOutOfRangeException("mathematicOperator");
			}
		}


		/// <summary>
		/// Executes an arithmetic operation between two given values according the operation defined in the ArithmeticOperator
		/// </summary>
		/// <param name="arithmeticOperator">The arithmetic operator.</param>
		/// <param name="value1">The value1.</param>
		/// <param name="value2">The value2.</param>
		/// <returns>the result of the operation</returns>
		/// <exception cref="System.ArgumentOutOfRangeException">arithmeticOperator</exception>
		public static double ExecuteOperationByOperator(ArithmeticOperator arithmeticOperator, double value1, double value2)
		{
			switch (arithmeticOperator)
			{
				case ArithmeticOperator.Add:
					return value1 + value2;
				case ArithmeticOperator.Subtract:
					return value1 - value2;
				case ArithmeticOperator.Multiply:
					return value1*value2;
				case ArithmeticOperator.Divide:
					return value1/value2;
				case ArithmeticOperator.Power:
					return Math.Pow(value1, value2);
				default:
					throw new ArgumentOutOfRangeException("arithmeticOperator");
			}
		}


		public static Func<double, double, double> GetMathematicalFunctionFromMathematicOperator(MathematicOperator mathematicOperator) {
			switch (mathematicOperator) {
				case MathematicOperator.MAX:
					return Math.Max;
				case MathematicOperator.MIN:
					return Math.Min;
				case MathematicOperator.SUM:
					return (x, y) => x + y;
				case MathematicOperator.AVG:
					return (x, y) => (x + y) / 2;
			}
			return null;
		}

        public static double ComputeCorrelationCoefficient(double[] values1, double[] values2)
        {
            if (values1.Length != values2.Length)
                throw new ArgumentException("values must be the same length");

            var avg1 = values1.Average();
            var avg2 = values2.Average();

            var sum1 = values1.Zip(values2, (x1, y1) => (x1 - avg1) * (y1 - avg2)).Sum();

            var sumSqr1 = values1.Sum(x => Math.Pow((x - avg1), 2.0));
            var sumSqr2 = values2.Sum(y => Math.Pow((y - avg2), 2.0));

            var result = sum1 / Math.Sqrt(sumSqr1 * sumSqr2);

            return result;
        }
	}
}

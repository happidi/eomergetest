﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using FakeItEasy.Configuration;
using NUnit.Framework;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {
	public class EmbeddedResourceTextFileParser : ITextFileParser {

		public IEnumerable Parse(string path) {
			List<string> str = new List<string>();
			using (Stream meterDataStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(path))
			using(var reader = new StreamReader(meterDataStream))
			{
				while (!reader.EndOfStream) {
					str.Add(reader.ReadLine());
				}
			}

			return str;
		}
	}
}

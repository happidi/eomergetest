﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests
{
    public class EnergyAggregatorMeterDataValidationTests : BaseEnergyAggregatorBusinessLayerTests
    {
		private const string const_red_color = "ff0000";
        private const string const_blue_color = "0000ff";
       

        [TestCase(MeterDataValidity.Invalid, const_red_color)]
        [TestCase(MeterDataValidity.Estimated, const_blue_color)]
        public void ChartProcess_MeterDataInvalid_HighlightedDataPoint(MeterDataValidity meterDataValidity, string expectedColor) {
            //Arrange
            TestDataContainer.Chart.HideMeterDataValidity = false;
            TestDataContainer.MeterData[DefaultMeterKey].Values[0].Validity = (byte)meterDataValidity;

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            List<DataSerie> resultSeries = result.Series.ToList();

            //Assert
            Assert.AreEqual(expectedColor, resultSeries[0].Points[0].HighlightColor);
            Assert.AreEqual(DataPointHighlight.Border, resultSeries[0].Points[0].Highlight);
            for (int i = 1; i < resultSeries[0].Points.Count; i++) {
                Assert.IsNull(resultSeries[0].Points[i].HighlightColor);
                Assert.AreEqual(DataPointHighlight.Fill, resultSeries[0].Points[i].Highlight);
            }

        }

        [TestCase(MeterDataValidity.Invalid)]
        [TestCase(MeterDataValidity.Estimated)]
        public void ChartProcess_MeterDataInvalidHidingMeterDataValidity_NoHighlightedData(MeterDataValidity meterDataValidity)
        {
            //Arrange
            TestDataContainer.Chart.HideMeterDataValidity = true;
            TestDataContainer.MeterData[DefaultMeterKey].Values[0].Validity = (byte)meterDataValidity;

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            List<DataSerie> resultSeries = result.Series.ToList();

            //Assert
            for (int i = 0; i < resultSeries[0].Points.Count; i++)
            {
                Assert.IsNull(resultSeries[0].Points[i].HighlightColor);
                Assert.AreEqual(DataPointHighlight.Fill, resultSeries[0].Points[i].Highlight);
            }

        }
    }
}

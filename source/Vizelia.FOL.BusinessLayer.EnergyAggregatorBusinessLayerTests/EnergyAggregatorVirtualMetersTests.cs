﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using NCalc;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {
	public class EnergyAggregatorVirtualMetersTests : BaseEnergyAggregatorBusinessLayerTests {
		private const string const_meter_data_file_name =
			"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.Common.MonthlyMeterData.txt";
		private const string const_virtual_meter_key = "#66";
        private const string const_second_virtual_meter_key = "#67";
        private const string const_third_virtual_meter_key = "#68";
	    private const string const_second_meter_key = "#111";


		protected override string TestMeterDataFilePath
		{
			get { return const_meter_data_file_name; }
		}

		[Test]
		public void ChartProcess_VirtualMeterIdenticalToRealMeter_RealAndVirtualHaveIdenticalSeries() {
			
			//Arrange
			MeterOperation meterOperation = new MeterOperation {
				KeyMeter = const_virtual_meter_key,
				KeyMeter1 = DefaultKeyMeterStr,
				IfcType = "IfcMeterOperation",
				LocalId = "Operation_Duplicate",
				Factor1 = 1,
				KeyMeterOperation = "1",
				Factor2 = 1
			};

			Meter virtualMeter = new Meter {
				MeterOperations = new List<MeterOperation> {meterOperation},
                KeyMeter = const_virtual_meter_key,
				Factor = 1.0,
				KeyLocation = DefaultKeyLocation,
				KeyMeterOperationResult = "1",
				Name = "Test Site Meter Virtual"
			};

			TestDataContainer.Meters.Add(virtualMeter);
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			List<DataSerie> resultSeries = result.Series.ToList();

			//Assert
			Assert.AreEqual(TestDataContainer.Meters.Count, resultSeries.Count);
			Assert.AreEqual(resultSeries[0].PointsWithValue.Count, resultSeries[1].PointsWithValue.Count);
			for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++) {
				Assert.AreEqual(resultSeries[0].PointsWithValue[i].YValue, resultSeries[1].PointsWithValue[i].YValue);
			}
		}

		/// <summary>
		/// This tests a special case where the second factor in the formula equals to 0 creating the following formula : v = m1 / 0 * m2
		/// The expected result is that the values of the virtual meter all equal to 0,
		/// </summary>
		[Test]
		public void ChartProcess_VirtualMeterOperationIsDivisonOf2MetersWithZeroFactorForSecondMeter_VirtualMeterValuesAreZeros() {
			
			//Arrange
			//Add another meter
			Meter secondMeter = new Meter {
				KeyMeter = "#1",
				KeyLocation = DefaultKeyLocation,
				Factor = 1.0,
				KeyClassificationItem = DefaultTestClassificationItemKey,
				Name = "Test Site Second Meter"
			};

			//Generate Data for the meter
			SortedList<DateTime, MD> defaultMeterData = TestDataContainer.MeterData[DefaultMeterKey];
			int meter2Key = MeterHelper.ConvertMeterKeyToInt(secondMeter.KeyMeter);
			SortedList<DateTime, MD> meter2Data = CloneMeterData(defaultMeterData, meter2Key);
			TestDataContainer.MeterData[meter2Key] = meter2Data;


			MeterOperation meterOperation = new MeterOperation {
				KeyMeter = const_virtual_meter_key,
				KeyMeter1 = DefaultKeyMeterStr,
				IfcType = "IfcMeterOperation",
				LocalId = "Operation_Divide",
				Factor1 = 1,
				KeyMeterOperation = "1",
				Operator = ArithmeticOperator.Divide,
				KeyMeter2 = secondMeter.KeyMeter,
				Factor2 = 0
			};

			Meter virtualMeter = new Meter {
				MeterOperations = new List<MeterOperation> {meterOperation},
				KeyMeter = const_virtual_meter_key,
				Factor = 1.0,
				KeyLocation = DefaultKeyLocation,
				KeyMeterOperationResult = "1",
				KeyClassificationItem = DefaultTestClassificationItemKey,
				Name = "Test Site Meter Virtual"
			};


			TestDataContainer.Meters.Add(secondMeter);
			TestDataContainer.Meters.Add(virtualMeter);
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(),
				DefaultCulture);
			List<DataSerie> resultSeries = result.Series.ToList();
			var virtualMeterPoints = resultSeries.First(x => x.KeyMeter == const_virtual_meter_key).Points;
			
			//Assert
			for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++) {
				Assert.AreEqual(0, virtualMeterPoints[i].YValue);
			}
		}


        [TestCase(ArithmeticOperator.Add)]
        [TestCase(ArithmeticOperator.Divide)]
        [TestCase(ArithmeticOperator.Multiply)]
        [TestCase(ArithmeticOperator.Power)]
        [TestCase(ArithmeticOperator.Subtract)]
        public void ChartProcess_MultipleVirtualMeters_VirtualMeterOperateOnAnotherVirtualMeter(ArithmeticOperator meterOperator)
        {
            //Arrange
            #region First Virtual Meter and Operation
            MeterOperation meterOperation = new MeterOperation
                {
                    KeyMeter = const_virtual_meter_key,
                    KeyMeter1 = DefaultKeyMeterStr,
                    IfcType = "IfcMeterOperation",
                    LocalId = "Operation_Duplicate",
                    Factor1 = 1,
                    KeyMeterOperation = "1",
                    Operator = meterOperator,
                    KeyMeter2 = const_second_meter_key,
                    Factor2 = 1
                };

            Meter virtualMeter = new Meter
            {
                MeterOperations = new List<MeterOperation> { meterOperation },
                KeyMeter = const_virtual_meter_key,
                Factor = 1.0,
                KeyLocation = DefaultKeyLocation,
                KeyMeterOperationResult = "1",
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Name = "Test Site Meter Virtual"
            }; 
            #endregion

            Meter secondMeter = new Meter
            {
                KeyMeter = const_second_meter_key,
                KeyLocation = DefaultKeyLocation,
                Factor = 1.0,
                IsAcquisitionDateTimeEndInterval = false,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Name = "Test Site Second Meter"
            };

			//Generate Data for the meter
			SortedList<DateTime, MD> defaultMeterData = TestDataContainer.MeterData[DefaultMeterKey];
			int meter2Key = MeterHelper.ConvertMeterKeyToInt(secondMeter.KeyMeter);
			SortedList<DateTime, MD> meter2Data = CloneMeterData(defaultMeterData, meter2Key);
			TestDataContainer.MeterData[meter2Key] = meter2Data;


            #region Second Virtual Meter and Operation
            MeterOperation secondMeterOperation = new MeterOperation
          {
              KeyMeter = const_second_virtual_meter_key,
              KeyMeter1 = const_virtual_meter_key, //key of virtual meter
              IfcType = "IfcMeterOperation",
              LocalId = "Second_Operation_Duplicate",
              Factor1 = 1,
              KeyMeterOperation = "2",
              Operator = meterOperator,
              KeyMeter2 = const_second_meter_key,
              Factor2 = 1
          };

            Meter secondVirtualMeter = new Meter
            {
                MeterOperations = new List<MeterOperation> { secondMeterOperation },
                KeyMeter = const_second_virtual_meter_key,
                Factor = 1.0,
                KeyLocation = DefaultKeyLocation,
                KeyMeterOperationResult = "2",
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Name = "Test Site Second Meter Virtual"
            }; 
            #endregion

            TestDataContainer.Meters.Add(secondMeter);
            TestDataContainer.Meters.Add(virtualMeter);
            TestDataContainer.Meters.Add(secondVirtualMeter);
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(),
                DefaultCulture);
            List<DataSerie> resultSeries = result.Series.ToList();

            //Assert
            var firstVirtualMeterResult = resultSeries.First(x => x.KeyMeter == const_virtual_meter_key).PointsWithValue; 
            var secondMeterResult = resultSeries.First(x => x.KeyMeter == const_second_meter_key).PointsWithValue;
            var secondVirtualMeterResult = resultSeries.First(x => x.KeyMeter == const_second_virtual_meter_key).PointsWithValue;
             for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++)
            {
                var calculatedResult = CalculationHelper.ExecuteOperationByOperator(meterOperator, firstVirtualMeterResult[i].YValue.Value, secondMeterResult[i].YValue.Value);
                Assert.AreEqual(calculatedResult, secondVirtualMeterResult[i].YValue);
            }
		}


		[Test, Combinatorial]
        [Category("LongRunning")]
		public void ChartProcess_VirtualMeterThatIsAResultOfAnOperationBetween2RealMeters_VirtualMeterValuesCalculatedAccordingly(
														 [Values(ArithmeticOperator.Add, ArithmeticOperator.Multiply, ArithmeticOperator.Divide, ArithmeticOperator.Subtract, ArithmeticOperator.Power)] 
														 ArithmeticOperator arithmeticOperator,
														 [Values(AlgebricFunction.Cos, AlgebricFunction.Sin, AlgebricFunction.Tan, AlgebricFunction.Sqrt, AlgebricFunction.Abs, AlgebricFunction.Inverse, AlgebricFunction.Log,AlgebricFunction.Count)] 
														 AlgebricFunction function1,
														 [Values(AlgebricFunction.Cos, AlgebricFunction.Sin, AlgebricFunction.Tan, AlgebricFunction.Sqrt, AlgebricFunction.Abs, AlgebricFunction.Inverse, AlgebricFunction.Log, AlgebricFunction.Count)] 
													     AlgebricFunction function2,
														 [Values(1, 2)] int factor1,
														 [Values(1, 2)] int factor2,
														 [Values(0, 1)] int offset1,
													     [Values(0, 1)] int offset2)
		{

			//Add another meter 
			var meter2 = new Meter {
				KeyMeter = "#999",
				KeyLocation = DefaultKeyLocation,
				Name = "Test",
				Factor = 1.0,
				KeyClassificationItem = DefaultTestClassificationItemKey
			};
			TestDataContainer.Meters.Add(meter2);

			//Generate Data for the meter
			SortedList<DateTime, MD> defaultMeterData = TestDataContainer.MeterData[DefaultMeterKey];
			int meter2Key = MeterHelper.ConvertMeterKeyToInt(meter2.KeyMeter);
			SortedList<DateTime, MD> meter2Data = CloneMeterData(defaultMeterData, meter2Key);
			TestDataContainer.MeterData[meter2Key] = meter2Data;

			const string virtualMeterKey = "#666";
			
			var meterOperation = new MeterOperation {
				KeyMeter = virtualMeterKey,
				KeyMeter1 = DefaultKeyMeterStr,
				KeyMeter2 = meter2.KeyMeter,
				Operator = arithmeticOperator,
				IfcType = "IfcMeterOperation",
				LocalId = "All_Operations",
				Factor1 = factor1,
				Factor2 = factor2,
				Offset1 = offset1,
				Offset2 = offset2,
				KeyMeterOperation = "1",
				Function1 = function1,
				Function2 = function2
			};

			var virtualMeter = new Meter {
				MeterOperations = new List<MeterOperation> { meterOperation },
				KeyMeter = virtualMeterKey,
				Factor = 1.0,
				KeyLocation = DefaultKeyLocation,
				KeyMeterOperationResult = "1",
				KeyClassificationItem = DefaultTestClassificationItemKey,
				Name = "Test Site Meter Virtual"
			};
			TestDataContainer.Meters.Add(virtualMeter);

			InitializeTest();

			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			List<DataSerie> resultSeries = result.Series.ToList();

			DataSerie virtualMeterResultSerie = resultSeries.FirstOrDefault(s => s.KeyMeter == virtualMeter.KeyMeter);
			var meter1ResultSerie = resultSeries.FirstOrDefault(s => s.KeyMeter == DefaultKeyMeterStr);
			var meter2ResultSerie = resultSeries.FirstOrDefault(s => s.KeyMeter == meter2.KeyMeter);

			Assert.IsNotNull(virtualMeterResultSerie);
			Assert.IsNotNull(meter1ResultSerie);
			Assert.IsNotNull(meter2ResultSerie);
			Assert.AreEqual(TestDataContainer.Meters.Count, resultSeries.Count);

			
			for (int i = 0; i < virtualMeterResultSerie.PointsWithValue.Count; i++)
			{
				//Calculate meter1 expected result
				string formula1 = CalculationHelper.ConvertAlgebricFunctionToNCalcFormula(function1);
				var expression1 = new Expression(formula1, EvaluateOptions.IgnoreCase);
			    expression1.EvaluateFunction += CalculationHelper.CalculatedSerieCalculations;
				expression1.Parameters["x"] = meter1ResultSerie.PointsWithValue[i].YValue;
				double? meter1CalculatedValue = (double)expression1.Evaluate() * factor1 + offset1;

				//Calculate meter2 expected result
				string formula2 = CalculationHelper.ConvertAlgebricFunctionToNCalcFormula(function2);
				var expression2 = new Expression(formula2, EvaluateOptions.IgnoreCase);
				expression2.EvaluateFunction += CalculationHelper.CalculatedSerieCalculations;
				expression2.Parameters["x"] = meter2ResultSerie.PointsWithValue[i].YValue;
				double? meter2ExpectedValue = (double)expression2.Evaluate() * factor2 + offset2;

				//Calculate the final expected result
				double expectedResult = CalculationHelper.ExecuteOperationByOperator(arithmeticOperator,
																					meter1CalculatedValue.GetValueOrDefault(),
																					meter2ExpectedValue.GetValueOrDefault()
																					);

				Assert.AreEqual(expectedResult, virtualMeterResultSerie.PointsWithValue[i].YValue);

			}
		}
	

	
		[TestCase(AxisTimeInterval.Global)]
		[TestCase(AxisTimeInterval.Years)]
		[TestCase(AxisTimeInterval.Semesters)]
		[TestCase(AxisTimeInterval.Quarters)]
		[TestCase(AxisTimeInterval.Months)]
		[TestCase(AxisTimeInterval.Weeks)]
		[TestCase(AxisTimeInterval.Days)]
		[TestCase(AxisTimeInterval.Hours)]
		[TestCase(AxisTimeInterval.Minutes)]
		[TestCase(AxisTimeInterval.Seconds)]
		public void ChartProcess_VirtualMeterWithTimeAggregateOperationOnARealMeter_VirtualMeterDataIsAggregated(AxisTimeInterval operationTimeInterval)
																											     {
			//Generate some test data 
			DateTime meterDataStartDate = CalcMeterDataGenerationStartDateBasedOnTimeScalingInterval(operationTimeInterval);
			DateTime meterDataEndDate = DateTime.UtcNow;

			AxisTimeInterval meterDataGenerationFrequency =
				CalcMeterDataGenerationFrequencyBasedOnTimeScalingInterval(operationTimeInterval);
			ConcurrentDictionary<int, SortedList<DateTime, MD>> realMeterTestData = GenerateRandomMeterData(DefaultKeyMeterStr, meterDataStartDate, meterDataEndDate, 1,
				meterDataGenerationFrequency);

			TestDataContainer.MeterData = realMeterTestData;
			
			//Define a virtual meter 
			const string virtualMeterKey = "#666";
			var meterOperation = new MeterOperation {
				KeyMeter = virtualMeterKey,
				KeyMeter1 = DefaultKeyMeterStr,
				IfcType = "IfcMeterOperation",
				LocalId = "Operation_Aggregate",
				Factor1 = 1,
				KeyMeterOperation = "1",
			};

			var virtualMeter = new Meter {
				MeterOperations = new List<MeterOperation> { meterOperation },
				KeyMeter = virtualMeterKey,
				Factor = 1.0,
				KeyLocation = DefaultKeyLocation,
				KeyMeterOperationResult = "1",
				Name = "Test Site Meter Virtual",
				MeterOperationTimeScaleInterval = operationTimeInterval
			};

			TestDataContainer.Meters.Add(virtualMeter);
			
			// Define time scaling operation on the chart so that the real meter's data is aggregated
			TestDataContainer.Chart.TimeInterval = operationTimeInterval;
			InitializeTest();

			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			List<DataSerie> resultSeries = result.Series.ToList();

			var firstMeterResultPoints = resultSeries[0].PointsWithValue;
			var secondMeterResultPoints = resultSeries[1].PointsWithValue;

			Assert.AreEqual(TestDataContainer.Meters.Count, resultSeries.Count);
			Assert.AreEqual(firstMeterResultPoints.Count, secondMeterResultPoints.Count);

			for (int i = 0; i < firstMeterResultPoints.Count; i++) {
				Assert.AreEqual(firstMeterResultPoints[i].YValue, secondMeterResultPoints[i].YValue);
			}
		}

	    [Test]
        [Ignore]
        public void ChartProcess_MultipleVirtualMeters_NestingOfMoreThen2VirtualMeters_ThirdVirtualMeterShouldBeEmpty()
        {
            //Arrange

            #region First Virtual Meter and Operation

            MeterOperation meterOperation = new MeterOperation {
                KeyMeter = const_virtual_meter_key,
                KeyMeter1 = DefaultKeyMeterStr,
                IfcType = "IfcMeterOperation",
                LocalId = "Operation_Duplicate",
                Factor1 = 1,
                KeyMeterOperation = "1",
                Operator = ArithmeticOperator.None,
            };

            Meter virtualMeter = new Meter {
                MeterOperations = new List<MeterOperation> {meterOperation},
                KeyMeter = const_virtual_meter_key,
                Factor = 1.0,
                KeyLocation = DefaultKeyLocation,
                KeyMeterOperationResult = "1",
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Name = "Test Site Meter Virtual"
            };

            #endregion


            #region Second Virtual Meter and Operation

            MeterOperation secondMeterOperation = new MeterOperation {
                KeyMeter = const_second_virtual_meter_key,
                KeyMeter1 = const_virtual_meter_key, //key of virtual meter
                IfcType = "IfcMeterOperation",
                LocalId = "Second_Operation_Duplicate",
                Factor1 = 1,
                KeyMeterOperation = "2",
                Operator = ArithmeticOperator.None,
            };

            Meter secondVirtualMeter = new Meter {
                MeterOperations = new List<MeterOperation> {secondMeterOperation},
                KeyMeter = const_second_virtual_meter_key,
                Factor = 1.0,
                KeyLocation = DefaultKeyLocation,
                KeyMeterOperationResult = "2",
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Name = "Test Site Second Meter Virtual"
            };

            #endregion


            #region Third Virtual Meter and Operation

            MeterOperation thirdMeterOperation = new MeterOperation {
                KeyMeter = const_third_virtual_meter_key,
                KeyMeter1 = const_second_virtual_meter_key, //key of virtual meter
                IfcType = "IfcMeterOperation",
                LocalId = "Third_Operation_Duplicate",
                Factor1 = 1,
                KeyMeterOperation = "3",
                Operator = ArithmeticOperator.None,
            };

            Meter thirdVirtualMeter = new Meter {
                MeterOperations = new List<MeterOperation> {thirdMeterOperation},
                KeyMeter = const_third_virtual_meter_key,
                Factor = 1.0,
                KeyLocation = DefaultKeyLocation,
                KeyMeterOperationResult = "3",
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Name = "Test Site Third Meter Virtual"
            };

            #endregion


            TestDataContainer.Meters.Add(virtualMeter);
            TestDataContainer.Meters.Add(secondVirtualMeter);
            TestDataContainer.Meters.Add(thirdVirtualMeter);
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(),
                DefaultCulture);
            List<DataSerie> resultSeries = result.Series.ToList();

            //Assert
            Assert.AreEqual(0,
                resultSeries.First(x => x.KeyMeter == const_third_virtual_meter_key).PointsWithValue.Count);
        }

		/// <summary>
		/// Set the start date of the meter data generation in a way that there will be generated enough values in the entire range,
		/// so that the data eventually could be scaled by time
		/// </summary>
		/// <param name="testTimeScaleInterval"></param>
		/// <returns></returns>
		private DateTime CalcMeterDataGenerationStartDateBasedOnTimeScalingInterval(AxisTimeInterval testTimeScaleInterval) {
			DateTime startDate = DateTime.UtcNow;
			switch (testTimeScaleInterval) {
				case AxisTimeInterval.Global:
					return startDate.AddYears(-2);
				case AxisTimeInterval.Years:
				case AxisTimeInterval.Semesters:
				case AxisTimeInterval.Quarters:
				case AxisTimeInterval.Months:
				case AxisTimeInterval.Weeks:
				case AxisTimeInterval.Days:
					return startDate.AddYears(-1);
				case AxisTimeInterval.Hours:
				case AxisTimeInterval.Minutes:
				case AxisTimeInterval.Seconds:
					return startDate.AddDays(-1);
				default:
					throw new ArgumentOutOfRangeException("testTimeScaleInterval");
			}
		}

		/// <summary>
		/// Calculate the frequency of test meter data generation according to the time scaling Type
		/// For instance : if the time scaling of data is Months, then the frequency of the data generation should be lower then that
		/// so that it will contain values for scaling, so the result is Weeks.
		/// </summary> 
		private AxisTimeInterval CalcMeterDataGenerationFrequencyBasedOnTimeScalingInterval(AxisTimeInterval testTimeScaleInterval) {
			switch (testTimeScaleInterval) {
				case AxisTimeInterval.Global:
					return AxisTimeInterval.Years;
				case AxisTimeInterval.Years:
					return AxisTimeInterval.Semesters;
				case AxisTimeInterval.Semesters:
					return AxisTimeInterval.Quarters;
				case AxisTimeInterval.Quarters:
					return AxisTimeInterval.Months;
				case AxisTimeInterval.Months:
					return AxisTimeInterval.Weeks;
				case AxisTimeInterval.Weeks:
					return AxisTimeInterval.Days;
				case AxisTimeInterval.Days:
					return AxisTimeInterval.Hours;
				case AxisTimeInterval.Hours:
					return AxisTimeInterval.Minutes;
				case AxisTimeInterval.Minutes:
				case AxisTimeInterval.Seconds:
					return AxisTimeInterval.Seconds;
				default:
					throw new ArgumentOutOfRangeException("testTimeScaleInterval");
			}
		}
	
	}

}

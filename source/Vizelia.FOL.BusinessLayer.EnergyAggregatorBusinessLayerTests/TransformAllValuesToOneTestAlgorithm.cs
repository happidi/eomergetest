﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vizelia.FOL.BusinessEntities;

//This has to be the namespace of the algorithm for it to be identified as a builtIn algorithm
namespace Vizelia.FOL.ConcreteProviders {
	
	/// <summary>
	/// A test algorithm that transforms all the values in a given DataSerie to 1's
	/// </summary>
	public class TransformAllValuesToOneTestAlgorithm : IAnalyticsExtensibility
	{
		private const string const_serie_id = "TestAlgorithmId";

		public Tuple<Chart, List<DataSerie>, Dictionary<string, string>> Execute(EnergyAggregatorContext context, Chart chart, List<DataSerie> series, Dictionary<string, string> inputs)
		{
			if (series.Any())
			{
				foreach (DataPoint dataPoint in series[0].Points)
				{
					dataPoint.YValue = 1;
				}
			}

			return new Tuple<Chart, List<DataSerie>, Dictionary<string, string>>(chart, series, new Dictionary<string, string>());
		}

		public Dictionary<string, string> GetPublicInputs()
		{
			return new Dictionary<string, string>();
		}

		/// <summary>
		/// Gets the data series local id.
		/// </summary>
		/// <returns/>
		public List<ListElement> GetDataSeriesLocalId()
		{
			return new List<ListElement>
			{
				new ListElement
				{
					Id = const_serie_id,
					Label = const_serie_id,
					LocalId = const_serie_id,
					Order = 1,
					Value = const_serie_id,
					MsgCode = const_serie_id
				}
			};
		}
	}
}

﻿{
	"d" : {
		"__type" : "JsonStoreOfMeterData:#Vizelia.FOL.BusinessEntities",
		"message" : null,
		"metaData" : {
			"__type" : "JsonMetaDataOfMeterData:#Vizelia.FOL.BusinessEntities",
			"fields" : [{
					"__type" : "JsonField:#Vizelia.FOL.BusinessEntities",
					"header" : null,
					"mapping" : "KeyMeterData",
					"name" : "KeyMeterData",
					"type" : "string"
				}, {
					"__type" : "JsonField:#Vizelia.FOL.BusinessEntities",
					"header" : null,
					"mapping" : "KeyMeter",
					"name" : "KeyMeter",
					"type" : "string"
				}, {
					"__type" : "JsonField:#Vizelia.FOL.BusinessEntities",
					"header" : null,
					"mapping" : "MeterLocalId",
					"name" : "MeterLocalId",
					"type" : "string"
				}, {
					"__type" : "JsonField:#Vizelia.FOL.BusinessEntities",
					"header" : null,
					"mapping" : "AcquisitionDateTime",
					"name" : "AcquisitionDateTime",
					"type" : "date"
				}, {
					"__type" : "JsonField:#Vizelia.FOL.BusinessEntities",
					"header" : null,
					"mapping" : "AcquisitionDateTimeLocal",
					"name" : "AcquisitionDateTimeLocal",
					"type" : "string"
				}, {
					"__type" : "JsonField:#Vizelia.FOL.BusinessEntities",
					"header" : null,
					"mapping" : "TimeZoneLocal",
					"name" : "TimeZoneLocal",
					"type" : "string"
				}, {
					"__type" : "JsonField:#Vizelia.FOL.BusinessEntities",
					"header" : null,
					"mapping" : "Value",
					"name" : "Value",
					"type" : "float"
				}, {
					"__type" : "JsonField:#Vizelia.FOL.BusinessEntities",
					"header" : null,
					"mapping" : "Validity",
					"name" : "Validity",
					"type" : "int"
				}, {
					"__type" : "JsonField:#Vizelia.FOL.BusinessEntities",
					"header" : null,
					"mapping" : "IfcType",
					"name" : "IfcType",
					"type" : "string"
				}
			],
			"idProperty" : "KeyMeterData",
			"messageProperty" : "message",
			"root" : "records",
			"sortInfo" : null,
			"successProperty" : "success",
			"totalProperty" : "recordCount"
		},
		"recordCount" : 53,
		"records" : [{
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1388440800000)\/",
				"AcquisitionDateTimeLocal" : "12\/31\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "53",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 97
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1387836000000)\/",
				"AcquisitionDateTimeLocal" : "12\/24\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "52",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 79
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1387231200000)\/",
				"AcquisitionDateTimeLocal" : "12\/17\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "51",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 89
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1386626400000)\/",
				"AcquisitionDateTimeLocal" : "12\/10\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "50",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 73
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1386021600000)\/",
				"AcquisitionDateTimeLocal" : "12\/03\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "49",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 28
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1385416800000)\/",
				"AcquisitionDateTimeLocal" : "11\/26\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "48",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 34
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1384812000000)\/",
				"AcquisitionDateTimeLocal" : "11\/19\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "47",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 19
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1384207200000)\/",
				"AcquisitionDateTimeLocal" : "11\/12\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "46",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 18
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1383602400000)\/",
				"AcquisitionDateTimeLocal" : "11\/05\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "45",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 35
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1382997600000)\/",
				"AcquisitionDateTimeLocal" : "10\/29\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "44",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 18
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1382389200000)\/",
				"AcquisitionDateTimeLocal" : "10\/22\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "43",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 5
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1381784400000)\/",
				"AcquisitionDateTimeLocal" : "10\/15\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "42",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 18
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1381179600000)\/",
				"AcquisitionDateTimeLocal" : "10\/08\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "41",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 24
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1380574800000)\/",
				"AcquisitionDateTimeLocal" : "10\/01\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "40",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 71
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1379970000000)\/",
				"AcquisitionDateTimeLocal" : "09\/24\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "39",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 89
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1379365200000)\/",
				"AcquisitionDateTimeLocal" : "09\/17\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "38",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 10
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1378760400000)\/",
				"AcquisitionDateTimeLocal" : "09\/10\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "37",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 0
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1378155600000)\/",
				"AcquisitionDateTimeLocal" : "09\/03\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "36",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 58
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1377550800000)\/",
				"AcquisitionDateTimeLocal" : "08\/27\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "35",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 40
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1376946000000)\/",
				"AcquisitionDateTimeLocal" : "08\/20\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "34",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 31
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1376341200000)\/",
				"AcquisitionDateTimeLocal" : "08\/13\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "33",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 32
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1375736400000)\/",
				"AcquisitionDateTimeLocal" : "08\/06\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "32",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 42
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1375131600000)\/",
				"AcquisitionDateTimeLocal" : "07\/30\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "31",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 95
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1374526800000)\/",
				"AcquisitionDateTimeLocal" : "07\/23\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "30",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 88
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1373922000000)\/",
				"AcquisitionDateTimeLocal" : "07\/16\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "29",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 44
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1373317200000)\/",
				"AcquisitionDateTimeLocal" : "07\/09\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "28",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 70
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1372712400000)\/",
				"AcquisitionDateTimeLocal" : "07\/02\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "27",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 42
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1372107600000)\/",
				"AcquisitionDateTimeLocal" : "06\/25\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "26",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 6
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1371502800000)\/",
				"AcquisitionDateTimeLocal" : "06\/18\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "25",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 54
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1370898000000)\/",
				"AcquisitionDateTimeLocal" : "06\/11\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "24",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 17
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1370293200000)\/",
				"AcquisitionDateTimeLocal" : "06\/04\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "23",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 28
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1369688400000)\/",
				"AcquisitionDateTimeLocal" : "05\/28\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "22",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 12
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1369083600000)\/",
				"AcquisitionDateTimeLocal" : "05\/21\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "21",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 51
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1368478800000)\/",
				"AcquisitionDateTimeLocal" : "05\/14\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "20",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 1
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1367874000000)\/",
				"AcquisitionDateTimeLocal" : "05\/07\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "19",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 84
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1367269200000)\/",
				"AcquisitionDateTimeLocal" : "04\/30\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "18",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 68
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1366664400000)\/",
				"AcquisitionDateTimeLocal" : "04\/23\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "17",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 95
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1366059600000)\/",
				"AcquisitionDateTimeLocal" : "04\/16\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "16",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 52
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1365454800000)\/",
				"AcquisitionDateTimeLocal" : "04\/09\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "15",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 89
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1364850000000)\/",
				"AcquisitionDateTimeLocal" : "04\/02\/2013 00:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "14",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 58
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1364248800000)\/",
				"AcquisitionDateTimeLocal" : "03\/26\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "13",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 16
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1363644000000)\/",
				"AcquisitionDateTimeLocal" : "03\/19\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "12",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 63
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1363039200000)\/",
				"AcquisitionDateTimeLocal" : "03\/12\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "11",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 90
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1362434400000)\/",
				"AcquisitionDateTimeLocal" : "03\/05\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "10",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 83
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1361829600000)\/",
				"AcquisitionDateTimeLocal" : "02\/26\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "9",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 9
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1361224800000)\/",
				"AcquisitionDateTimeLocal" : "02\/19\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "8",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 96
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1360620000000)\/",
				"AcquisitionDateTimeLocal" : "02\/12\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "7",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 31
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1360015200000)\/",
				"AcquisitionDateTimeLocal" : "02\/05\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "6",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 59
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1359410400000)\/",
				"AcquisitionDateTimeLocal" : "01\/29\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "5",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 38
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1358805600000)\/",
				"AcquisitionDateTimeLocal" : "01\/22\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "4",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 7
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1358200800000)\/",
				"AcquisitionDateTimeLocal" : "01\/15\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "3",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 44
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1357596000000)\/",
				"AcquisitionDateTimeLocal" : "01\/08\/2013 01:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "2",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 15
			}, {
				"__type" : "MeterData:#Vizelia.FOL.BusinessEntities",
				"IfcType" : "IfcMeterData",
				"AcquisitionDateTime" : "\/Date(1357012800000)\/",
				"AcquisitionDateTimeLocal" : "01\/01\/2013 06:00:00",
				"KeyMeter" : "#105",
				"KeyMeterData" : "1",
				"MeterLocalId" : null,
				"TimeZoneLocal" : "(UTC+03:00) Amman",
				"Validity" : 0,
				"Value" : 41
			}
		],
		"success" : true
	}
}

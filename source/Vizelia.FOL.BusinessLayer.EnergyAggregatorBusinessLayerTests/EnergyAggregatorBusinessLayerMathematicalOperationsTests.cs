﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {
	/// <summary>
	/// Test Class for Energy Aggregator Mathematical operations
	/// </summary>
	[TestFixture]
	public class EnergyAggregatorBusinessLayerMathematicalOperationsTests : BaseEnergyAggregatorBusinessLayerTests {


		[TestCase(1.0, Result = 252.0)]
		[TestCase(2.5, Result = 252.0 * 2.5)]
		[TestCase(0, Result = 252.0 * 0)]
		[TestCase(100, Result = 252.0 * 100)]
		public double? ChartProcess_MeterDataFactor_MeterDataChangedByFactorValue(double factor) {
			//Arrange
			TestDataContainer.Meters[0].Factor = factor;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);

			//Assert
			return result.Series.ToList().First().PointsWithValue[2].YValue;
		}


		[TestCase(0, Result = 252.0)]
		[TestCase(-50, Result = 252.0 - (  50 * 4))] // 4 - the number of the points aggragated in this month
		[TestCase(50, Result = 252.0 + (50 * 4))] // 4 - the number of the points aggragated in this month
		public double? ChartProcess_MeterDataOffset_MeterDataChangedByOffsetValue(double offset) {
			//Arrange
			TestDataContainer.Meters[0].Offset = offset;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);

			//Assert
			return result.Series.ToList().First().PointsWithValue[2].YValue;
		}

	}
}

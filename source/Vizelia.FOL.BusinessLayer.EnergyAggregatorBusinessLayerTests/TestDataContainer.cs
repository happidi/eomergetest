﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {
	/// <summary>
	/// Contains relevant test data
	/// </summary>
	public class TestDataContainer {
		/// <summary>
		/// Gets or sets the tenant date range list.
		/// </summary>
		/// <value>
		/// The tenant date range list.
		/// </value>
		public List<EnergyAggregatorContext> TenantDateRangeList { get; set; }
		/// <summary>
		/// Gets or sets the locations.
		/// </summary>
		/// <value>
		/// The locations.
		/// </value>
		public Dictionary<string, Location> Locations { get; set; }
		/// <summary>
		/// Gets or sets the classification items.
		/// </summary>
		/// <value>
		/// The classification items.
		/// </value>
		public Dictionary<string, ClassificationItem> ClassificationItems { get; set; }
		/// <summary>
		/// Gets or sets the meter data.
		/// </summary>
		/// <value>
		/// The meter data.
		/// </value>
		public ConcurrentDictionary<int, SortedList<DateTime, MD>> MeterData { get; set; }
		/// <summary>
		/// Gets or sets the chart.
		/// </summary>
		/// <value>
		/// The chart.
		/// </value>
		public Chart Chart { get; set; }
		/// <summary>
		/// Gets or sets the meters.
		/// </summary>
		/// <value>
		/// The meters.
		/// </value>
		public List<Meter> Meters { get; set; }
        /// <summary>
        /// Gets or sets the Calendars.
        /// </summary>
        /// <value>
        /// The Calendars.
        /// </value>
		public Dictionary<string, Tuple<string, string>> Calendars { get; set; }
        /// <summary>
        /// Gets or sets the Localization Dictionary.
        /// </summary>
        /// <value>
        /// The Localization Dictionary.
        /// </value>
        public Dictionary<string, Dictionary<string, string>> LocalizationDictionary { get; set; }

        public Dictionary<string, PsetAttributeHistorical> HistoricalData { get; set; }

	}
}
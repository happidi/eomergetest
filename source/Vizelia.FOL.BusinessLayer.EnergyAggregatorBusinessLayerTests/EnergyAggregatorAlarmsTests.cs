﻿using System;
using System.Collections.Generic;
using System.Linq;
using DDay.iCal;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests
{
	public class EnergyAggregatorAlarmsTests : BaseEnergyAggregatorBusinessLayerTests
	{
		private const string const_meter_data_file_name =
		"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.AlarmsTests.MonthlyDataWithOrderedValues.txt";

		protected override string TestMeterDataFilePath
		{
			get { return const_meter_data_file_name; }
		}

		[TestCase(FilterCondition.Equals)]
		[TestCase(FilterCondition.NotEquals)]
		[TestCase(FilterCondition.Higher)]
		[TestCase(FilterCondition.Lower)]
		[TestCase(FilterCondition.Between)]
		[TestCase(FilterCondition.Outside)]
		public void AlarmDefinition_Process_AlarmDefinitionOnMeter_AlarmInstancesCreated(FilterCondition condition)
		{
			//Arrange
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			IList<MD> meterValues = testMeterData.Values;
			int middleIndex = meterValues.Count()/2;
			double threshold1 = meterValues[middleIndex - 1].Value; //Use the middle value as the first threshold for the alarm condition
			double threshold2 = meterValues[middleIndex + 1].Value; 

			var testAlarmDefinition = new AlarmDefinition()
			{
				LocalId = "Meter_Data_Alarm",
				Title = "Meter Data Alarm",
				KeyLocation = DefaultKeyLocation,
				StartDate = testMeterData.Keys[0].AddYears(-1),
				EndDate = testMeterData.Keys[testMeterData.Count - 1].AddYears(1),
				Meters = TestDataContainer.Meters,
				Threshold = threshold1,
				Threshold2 = threshold2,
				Condition = condition
			};

			InitializeTest();

			//Act
			Dictionary<AlarmDefinition, List<AlarmInstance>> processdAlarmDefinitions= EnergyAggregator.AlarmDefinition_Process(EnergyAggregatorContext, new List<AlarmDefinition> {testAlarmDefinition});
			
			//Assert
			List<AlarmInstance> processedAlarmInstances = processdAlarmDefinitions[testAlarmDefinition];
			IEnumerable<MD> meterValuesByCondition = FilterTestMeterDataByAlarmCondition(condition, meterValues, threshold1, threshold2);
			
				
			Assert.AreEqual(meterValuesByCondition.Count(), processedAlarmInstances.Count);
			
			//Validate that all the values that comply the conditions are the values for which the alarm instances were created
			foreach (AlarmInstance alarmInstance in processedAlarmInstances) {
				var valueToAlarm = meterValuesByCondition.FirstOrDefault(md => md.Value == alarmInstance.Value);
				Assert.IsNotNull(valueToAlarm);
			}
		}

		[TestCase(FilterCondition.Equals)]
		[TestCase(FilterCondition.NotEquals)]
		[TestCase(FilterCondition.Higher)]
		[TestCase(FilterCondition.Lower)]
		[TestCase(FilterCondition.Between)]
		[TestCase(FilterCondition.Outside)]
		public void AlarmDefinition_Process_AlarmDefinitionOnChart_AlarmInstancesCreated(FilterCondition condition) {

			//Arrange
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			IList<MD> meterValues = testMeterData.Values;
			int middleIndex = meterValues.Count() / 2;
			double threshold1 = meterValues[middleIndex - 1].Value; //Use the middle value as the first threshold for the alarm condition
			double threshold2 = meterValues[middleIndex + 1].Value;

			var testAlarmDefinition = new AlarmDefinition()
			{
				LocalId = "Meter_Data_Alarm",
				Title = "Meter Data Alarm",
				KeyLocation = DefaultKeyLocation,
				StartDate = testMeterData.Keys[0].AddYears(-1),
				EndDate = testMeterData.Keys[testMeterData.Count - 1].AddYears(1),
				Charts = new List<Tuple<Chart, List<Meter>, List<Location>>>()
				{
					new Tuple<Chart, List<Meter>, List<Location>>(TestDataContainer.Chart,
																  TestDataContainer.Meters,
						                                          TestDataContainer.Locations.Values.ToList())
				},
				Threshold = threshold1,
				Threshold2 = threshold2,
				Condition = condition
			};

			InitializeTest();

			//Act
			Dictionary<AlarmDefinition, List<AlarmInstance>> processdAlarmDefinitions = EnergyAggregator.AlarmDefinition_Process(EnergyAggregatorContext, new List<AlarmDefinition> { testAlarmDefinition });

			//Assert
			List<AlarmInstance> processedAlarmInstances = processdAlarmDefinitions[testAlarmDefinition];
			IEnumerable<MD> meterValuesByCondition = FilterTestMeterDataByAlarmCondition(condition, meterValues, threshold1, threshold2);

			Assert.AreEqual(meterValuesByCondition.Count(), processedAlarmInstances.Count);

			//Validate that all the values that comply the conditions are the values for which the alarm instances were created
			foreach (AlarmInstance alarmInstance in processedAlarmInstances) {
				var valueToAlarm = meterValuesByCondition.FirstOrDefault(md => md.Value == alarmInstance.Value);
				Assert.IsNotNull(valueToAlarm);
			}
		}

		[Test]
		public void AlarmDefinition_Process_AlarmDefinitionOnMeterWithYearlyTimeIntervalAggregation_AlarmInstancesCreatedForAggregatedValues() {

			//Arrange
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Set threshold to be the result of aggregation of all of the values
			double expectedAggregationResult = testMeterData.Values.Select(md => md.Value).Sum();

			var testAlarmDefinition = new AlarmDefinition() {
				LocalId = "Meter_Data_Alarm",
				Title = "Meter Data Alarm",
				KeyLocation = DefaultKeyLocation,
				StartDate = testMeterData.Keys[0].AddYears(-1),
				EndDate = testMeterData.Keys[testMeterData.Count - 1].AddYears(1),
				Meters = TestDataContainer.Meters,
				Threshold = expectedAggregationResult, // set the threshold to be equal to the aggregation result
				Condition = FilterCondition.Equals,
				TimeInterval = AxisTimeInterval.Global
			};

			InitializeTest();

			//Act
			Dictionary<AlarmDefinition, List<AlarmInstance>> processdAlarmDefinitions = EnergyAggregator.AlarmDefinition_Process(EnergyAggregatorContext, new List<AlarmDefinition> { testAlarmDefinition });
			List<AlarmInstance> processedAlarmInstances = processdAlarmDefinitions[testAlarmDefinition];

			//Assert
			Assert.AreEqual(1, processedAlarmInstances.Count); // only one value should be returned after aggregation
			Assert.AreEqual(expectedAggregationResult, processedAlarmInstances[0].Value);
		}

		[Test]
		public void AlarmDefinition_Process_AlarmDefinitionOnMeterWithIncludeCalenderAggregation_AlarmInstancesCreatedByCalendarDates()
		{
			const string tomeZoneId = "UTC";

			//Arrange
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			const string calendarCategoryName = "Calendar Test Category"; 
			
			//Create Calendar event that falls inside the range of the meter's data
			int middleIndex = testMeterData.Keys.Count / 2;
			var calendarEvent = new CalendarEvent() {
				LocalId = "Calendar event for alarms",
				Title = "Calendar event for alarms",
				StartDate = testMeterData.Keys[1],
				EndDate = testMeterData.Keys[middleIndex],
				KeyLocation = DefaultKeyMeterStr,
				Key = "1",
				Category = calendarCategoryName
			};
			
			var iCalendar = new iCalendar();
			calendarEvent.ToiCal(iCalendar);
			string iCalData = CalendarHelper.Serialize(iCalendar);
			
			//Create calendar for the meter
			TestDataContainer.Calendars = new Dictionary<string, Tuple<string, string>>
			{
				{
					DefaultKeyMeterStr, new Tuple<string, string>(iCalData, tomeZoneId)
				}
			};

			//Create an alarm with a calendar
			// define the Threshold to be higher then the first value in the meter data
			var testAlarmDefinition = new AlarmDefinition()
			{
				LocalId = "Meter_Data_Alarm",
				Title = "Meter Data Alarm",
				KeyLocation = DefaultKeyLocation,
				StartDate = testMeterData.Keys[0].AddYears(-1),
				EndDate = testMeterData.Keys[testMeterData.Count - 1].AddYears(1),
				Meters = TestDataContainer.Meters,
				Threshold = testMeterData.Values[0].Value,
				Condition = FilterCondition.Higher, 
				CalendarMode = ChartCalendarMode.Include,
				CalendarEventCategory = new CalendarEventCategory()
				{
					KeyCalendarEventCategory = "1",
					LocalId = calendarCategoryName					
				}
			};

			InitializeTest();

			//Act
			Dictionary<AlarmDefinition, List<AlarmInstance>> processdAlarmDefinitions =
				EnergyAggregator.AlarmDefinition_Process(EnergyAggregatorContext, new List<AlarmDefinition> {testAlarmDefinition});
			List<AlarmInstance> processedAlarmInstances = processdAlarmDefinitions[testAlarmDefinition];


			IEnumerable<MD> testMeterValuesByAlarmCondition = FilterTestMeterDataByAlarmCondition(testAlarmDefinition.Condition,
																							  testMeterData.Values,
																							  testAlarmDefinition.Threshold.GetValueOrDefault());

			//The amount of the values that apply the alarm condition should be different from the alarm instances created due to the calendar inclusion 
			Assert.AreNotEqual(testMeterValuesByAlarmCondition.Count(), processedAlarmInstances.Count);

			//Validate that all of the alarm instances are in the range of the calender's event dates 
			foreach (AlarmInstance alarmInstance in processedAlarmInstances)
			{
				Assert.IsTrue(alarmInstance.InstanceDateTime >= calendarEvent.StartDate && 
							  alarmInstance.InstanceDateTime <= calendarEvent.EndDate);
			}
		}

		[Test]
		public void AlarmDefinition_Process_AlarmDefinitionOnMeterWithExcludeCalenderAggregation_AlarmInstancesCreatedOnlyOutsideOfCalendarRange() {
			const string tomeZoneId = "UTC";

			//Arrange
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			const string calendarCategoryName = "Calendar Test Category";

			//Create Calendar event that falls inside the range of the meter's data
			int middleIndex = testMeterData.Keys.Count / 2;
			var calendarEvent = new CalendarEvent() {
				LocalId = "Calendar event for alarms",
				Title = "Calendar event for alarms",
				StartDate = testMeterData.Keys[1],
				EndDate = testMeterData.Keys[middleIndex],
				KeyLocation = DefaultKeyMeterStr,
				Key = "1",
				Category = calendarCategoryName,
				TimeZoneId = tomeZoneId
			};

			var iCalendar = new iCalendar();
			calendarEvent.ToiCal(iCalendar);
			string iCalData = CalendarHelper.Serialize(iCalendar);

			//Create calendar for the meter
			TestDataContainer.Calendars = new Dictionary<string, Tuple<string, string>>
			{
				{
					DefaultKeyMeterStr, new Tuple<string, string>(iCalData, tomeZoneId)
				}
			};

			//Create an alarm with a calendar
			// define the Threshold to be higher then the first value in the meter data
			var testAlarmDefinition = new AlarmDefinition() {
				LocalId = "Meter_Data_Alarm",
				Title = "Meter Data Alarm",
				KeyLocation = DefaultKeyLocation,
				StartDate = testMeterData.Keys[0].AddYears(-1),
				EndDate = testMeterData.Keys[testMeterData.Count - 1].AddYears(1),
				Meters = TestDataContainer.Meters,
				Threshold = testMeterData.Values[0].Value,
				Condition = FilterCondition.Higher,
				CalendarMode = ChartCalendarMode.Exclude,
				CalendarEventCategory = new CalendarEventCategory() {
					KeyCalendarEventCategory = "1",
					LocalId = calendarCategoryName
				}
			};

			InitializeTest();

			//Act
			Dictionary<AlarmDefinition, List<AlarmInstance>> processdAlarmDefinitions =
				EnergyAggregator.AlarmDefinition_Process(EnergyAggregatorContext, new List<AlarmDefinition> { testAlarmDefinition });
			List<AlarmInstance> processedAlarmInstances = processdAlarmDefinitions[testAlarmDefinition];


			IEnumerable<MD> testMeterValuesByAlarmCondition = FilterTestMeterDataByAlarmCondition(testAlarmDefinition.Condition,
																							  testMeterData.Values,
																							  testAlarmDefinition.Threshold.GetValueOrDefault());

			//The amount of the values that apply the alarm condition should be different from the alarm instances created due to the calendar inclusion 
			Assert.AreNotEqual(testMeterValuesByAlarmCondition.Count(), processedAlarmInstances.Count);

			//Validate that all of the alarm instances are outside of the calender's range
			foreach (AlarmInstance alarmInstance in processedAlarmInstances) {
				Assert.IsTrue(alarmInstance.InstanceDateTime <= calendarEvent.StartDate ||
							  alarmInstance.InstanceDateTime >= calendarEvent.EndDate);
			}
		}

		[Test]
		public void AlarmDefinition_Process_AlarmDefinitionOnMeterUsePercentageDelta_AlarmInstancesCreatedForValuesDefinedAsSpikes() {
			//Arrange
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			//Update all of the meter data values in a way that the next value will be bigger than the previous one  by the threshold %
			IList<MD> meterData = testMeterData.Values;
			double threshold = 10;
			for (int i = 0; i < meterData.Count; i++)
			{
				if (i > 0)
				{
					var previousValue = meterData[i - 1].Value;
					meterData[i].Value = previousValue * (100 + threshold) / 100 + previousValue;
				}
				else
				{
					meterData[i].Value = 1;
				}
			}

			var testAlarmDefinition = new AlarmDefinition() {
				LocalId = "Meter_Data_Alarm",
				Title = "Meter Data Alarm",
				KeyLocation = DefaultKeyLocation,
				StartDate = testMeterData.Keys[0].AddYears(-1),
				EndDate = testMeterData.Keys[testMeterData.Count - 1].AddYears(1),
				Meters = TestDataContainer.Meters,
				Threshold = threshold, 
				Condition = FilterCondition.Higher,
				UsePercentageDelta = true
			};

			InitializeTest();

			//Act
			Dictionary<AlarmDefinition, List<AlarmInstance>> processdAlarmDefinitions = EnergyAggregator.AlarmDefinition_Process(EnergyAggregatorContext, new List<AlarmDefinition> { testAlarmDefinition });

			//Assert
			List<AlarmInstance> processedAlarmInstances = processdAlarmDefinitions[testAlarmDefinition];

			//Expect that all alarm instances will be created for all of the meter values except for the first one
			Assert.AreEqual( meterData.Count() - 1, processedAlarmInstances.Count);

			//Validate that all the values that comply the conditions are the values for which the alarm instances were created
			for (int i = 0; i < meterData.Count; i++)
			{
				var valueToAlarm = processedAlarmInstances.FirstOrDefault(md => md.Value == meterData[i].Value);
				if (i == 0)
				{
					Assert.IsNull(valueToAlarm); 
				}
				else
				{
					Assert.IsNotNull(valueToAlarm);
				}
			}
		}

		[TestCase(FilterCondition.Equals)]
		[TestCase(FilterCondition.NotEquals)]
		[TestCase(FilterCondition.Higher)]
		[TestCase(FilterCondition.Lower)]
		[TestCase(FilterCondition.Between)]
		[TestCase(FilterCondition.Outside)]
		public void AlarmDefinition_Process_AlarmDefinitionOnMeterAndLastProcessDateTimeIdDefined_AlarmInstancesCreatedOnlyAfterTheLastProcessDateTime(FilterCondition condition) {
			//Arrange
			SortedList<DateTime, MD> testMeterData = TestDataContainer.MeterData[DefaultMeterKey];

			IList<MD> meterValues = testMeterData.Values;
			int middleIndex = meterValues.Count() / 2;
			double threshold1 = meterValues[middleIndex - 1].Value; //Use the middle value as the first threshold for the alarm condition
			double threshold2 = meterValues[middleIndex + 1].Value;
			DateTime lastProcessDateTime = meterValues[middleIndex - 2].AcquisitionDateTime;

			var testAlarmDefinition = new AlarmDefinition() {
				LocalId = "Meter_Data_Alarm",
				Title = "Meter Data Alarm",
				KeyLocation = DefaultKeyLocation,
				StartDate = testMeterData.Keys[0].AddYears(-1),
				EndDate = testMeterData.Keys[testMeterData.Count - 1].AddYears(1),
				Meters = TestDataContainer.Meters,
				Threshold = threshold1,
				Threshold2 = threshold2,
				Condition = condition,
				LastProcessDateTime = lastProcessDateTime
			};

			InitializeTest();

			//Act
			Dictionary<AlarmDefinition, List<AlarmInstance>> processdAlarmDefinitions = EnergyAggregator.AlarmDefinition_Process(EnergyAggregatorContext, new List<AlarmDefinition> { testAlarmDefinition });

			//Assert
			List<AlarmInstance> processedAlarmInstances = processdAlarmDefinitions[testAlarmDefinition];
			IEnumerable<MD> meterValuesByCondition = FilterTestMeterDataByAlarmCondition(condition, meterValues, threshold1, threshold2);

			//THe expected return values should all be after the defined LastProcessDateTime
			IEnumerable<MD> meterValuesAfterLastProcessedDate =
				meterValuesByCondition.Where(md => md.AcquisitionDateTime >= testAlarmDefinition.LastProcessDateTime);

			Assert.AreEqual(meterValuesAfterLastProcessedDate.Count(),processedAlarmInstances.Count);

			//Validate that all the values that comply the conditions are the values for which the alarm instances were created
			foreach (AlarmInstance alarmInstance in processedAlarmInstances) {
				var valueToAlarm = meterValuesAfterLastProcessedDate.FirstOrDefault(md => md.Value == alarmInstance.Value);
				Assert.IsNotNull(valueToAlarm);
			}
		}



		private IEnumerable<MD> FilterTestMeterDataByAlarmCondition(FilterCondition condition,IEnumerable<MD> meterValues, double threshold1, double? threshold2 = null) {
			Func<MD, bool> filterCondition = null;
			
			switch (condition) {
				case FilterCondition.Equals:
					filterCondition = md => md.Value == threshold1;
					break;
				case FilterCondition.NotEquals:
					filterCondition = md => md.Value != threshold1;
					break;
				case FilterCondition.Higher:
					filterCondition = md => md.Value >= threshold1;
					break;
				case FilterCondition.Lower:
					filterCondition = md => md.Value <= threshold1;
					break;
				case FilterCondition.Between:

					filterCondition = md => md.Value >= threshold1 && md.Value <= threshold2.GetValueOrDefault();
					break;
				case FilterCondition.Outside:
					filterCondition = md => md.Value <= threshold1 || md.Value >= threshold2.GetValueOrDefault();
					break;
				default:
					throw new ArgumentOutOfRangeException("condition");
			}

			IEnumerable<MD> filteredMeterData = meterValues.Where(filterCondition);

			return filteredMeterData.ToList();
		}

	}
}

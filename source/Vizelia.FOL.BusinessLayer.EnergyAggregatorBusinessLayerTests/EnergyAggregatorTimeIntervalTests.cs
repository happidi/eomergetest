﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {
	/// <summary>
	/// Test class for Time Interval in the Energy Aggregator
	/// </summary>
	[TestFixture]
	public class EnergyAggregatorTimeIntervalTests : BaseEnergyAggregatorBusinessLayerTests {

		private const string const_meter_data_values_file_name =
			"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.EnergyAggregatorTimeIntervalTests.Semesters_1YearData_DataSerieValues_TestData.Data.txt";

		private const string const_meter_result_values_file_name =
			"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.EnergyAggregatorTimeIntervalTests.Semesters_1YearData_DataSerieValues_TestData.Results.txt";

		private const string const_meter_data_dates_file_name =
			"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.EnergyAggregatorTimeIntervalTests.Semesters_1YearData_DataSerieDates_TestData.Data.txt";

		private const string const_meter_result_dates_file_name =
			"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.EnergyAggregatorTimeIntervalTests.Semesters_1YearData_DataSerieDates_TestData.Results.txt";

		private const string const_meter_year_data_file_name =
			"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.EnergyAggregatorTimeIntervalTests.YearMeterDataHoursInterval.txt";

	
		/// <summary>
		/// Chart_s the time interval_ data aggregation by time interval_ second point value.
		/// </summary>
		/// <param name="timeInterval">The time interval.</param>
		/// <returns></returns>
		[TestCase(AxisTimeInterval.Months, Result = 252.0)]
		[TestCase(AxisTimeInterval.Days, Result = 44.0)]
		[TestCase(AxisTimeInterval.Quarters, Result = 712)]
		[TestCase(AxisTimeInterval.Weeks, Result = 44.0)]
		public double? Chart_TimeInterval_DataAggregationByTimeInterval_SecondPointValue(AxisTimeInterval timeInterval) {
			//Arrange
			TestDataContainer.Chart.TimeInterval = timeInterval;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			return result.Series.ToList().First().PointsWithValue[2].YValue;
		}

		private IEnumerable Chart_TimeInterval_DataAggregationByTimeInterval_ThirdPointValue_TestData() {
			var meterData = new ConcurrentDictionary<int, SortedList<DateTime, MD>>(TestHelper.ParseMeterDataFromFile(TestMeterDataFilePath));
			List<TestCaseData> testCases = new List<TestCaseData>();
			testCases.Add(new TestCaseData(meterData, AxisTimeInterval.Months).Returns(252.0));
			testCases.Add(new TestCaseData(meterData, AxisTimeInterval.Days).Returns(44.0));
			testCases.Add(new TestCaseData(meterData, AxisTimeInterval.Quarters).Returns(712.0));
			testCases.Add(new TestCaseData(meterData, AxisTimeInterval.Weeks).Returns(44.0));
			return testCases;
			//other data source

		}

		[Test, TestCaseSource("Chart_TimeInterval_DataAggregationByTimeInterval_ThirdPointValue_TestData")]
		public double? Chart_TimeInterval_DataAggregationByTimeInterval_ThirdPointValue(ConcurrentDictionary<int, SortedList<DateTime, MD>> meterData, AxisTimeInterval timeInterval) {
			//Arrange
			TestDataContainer.Chart.TimeInterval = timeInterval;
			TestDataContainer.MeterData = meterData;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			return result.Series.ToList().First().PointsWithValue[2].YValue;
		}

		#region Semesters
		private static IEnumerable<Tuple<ConcurrentDictionary<int, SortedList<DateTime, MD>>, IEnumerable>> Chart_TimeInterval_Semesters_1YearData_DataSerieValues_TestData() {
			//yield return new Tuple<Concurrent Dictionary<int, SortedList<DateTime, MD>>, int[]>(TestHelper.ParseMeterDataFromFile(), new[] { 1107, 1249 });
			yield return TestHelper.GetTestDataAndResult(const_meter_data_values_file_name, const_meter_result_values_file_name, new EmbeddedResourceTextFileParser());
		}

		[Test, TestCaseSource("Chart_TimeInterval_Semesters_1YearData_DataSerieValues_TestData")]
		public void Chart_TimeInterval_Semesters_1YearData_DataSerieValues(Tuple<ConcurrentDictionary<int, SortedList<DateTime, MD>>, IEnumerable> meterDataResultTuple) {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Semesters;
			TestDataContainer.MeterData = meterDataResultTuple.Item1;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			int i = 0;
			foreach (var point in meterDataResultTuple.Item2) {
				Assert.AreEqual(Convert.ToDouble(point), dataPoint[i].YValue);
				i++;
			}
		}

		private static IEnumerable<Tuple<ConcurrentDictionary<int, SortedList<DateTime, MD>>, IEnumerable>> Chart_TimeInterval_Semesters_1YearData_DataSerieDates_TestData() {
			yield return TestHelper.GetTestDataAndResult(const_meter_data_dates_file_name, const_meter_result_dates_file_name, new EmbeddedResourceTextFileParser());
		}

		[Test, TestCaseSource("Chart_TimeInterval_Semesters_1YearData_DataSerieDates_TestData")]
		public void Chart_TimeInterval_Semesters_1YearData_DataSerieDates(Tuple<ConcurrentDictionary<int, SortedList<DateTime, MD>>, IEnumerable> meterDataResultTuple) {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Semesters;
			TestDataContainer.MeterData = meterDataResultTuple.Item1;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			int i = 0;
			foreach (var point in meterDataResultTuple.Item2) {
                var dt = Convert.ToDateTime(point, DateTimeFormatInfo.InvariantInfo);
                Assert.AreEqual(dt, dataPoint[i].XDateTime.Value);
				i++;
			}
		}

		[Test]
		public void Chart_TimeInterval_Semesters_1YearData_2DataSerieValueAndDate() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Semesters;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(2, dataPoint.Count);
			Assert.AreEqual(1207, dataPoint[0].YValue);
			Assert.AreEqual(1249, dataPoint[1].YValue);
			Assert.AreEqual(dt, dataPoint[0].XDateTime);
			Assert.AreEqual(dt.AddMonths(6), dataPoint[1].XDateTime);
		}

		[Test]
		public void Chart_TimeInterval_Semesters_1YearDataWithStartEndDate_FirstDataSerieValueAndDate() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Semesters;
			TestDataContainer.Chart.StartDate = new DateTime(2013, 1, 1);
			TestDataContainer.Chart.EndDate = new DateTime(2013, 6, 1);
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(1, dataPoint.Count);
			Assert.AreEqual(1102, dataPoint[0].YValue);
			Assert.AreEqual(dt, dataPoint[0].XDateTime);
		}

		#endregion

		#region Hours

		[Test]
		public void Chart_TimeInterval_Hours_1YearData_DataSeriesValuesAndDates() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Hours;
			TestDataContainer.MeterData =
				TestHelper.ParseMeterDataFromFile(
					const_meter_year_data_file_name);
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(8760, dataPoint.Count);
			Assert.AreEqual(49, dataPoint[0].YValue);
			Assert.AreEqual(dt, dataPoint[0].XDateTime);
		
			for (int i = 1; i < dataPoint.Count; i++) {
				Assert.AreEqual(dt.AddHours(i), dataPoint[i].XDateTime);
			}
		}

		[Test]
		public void Chart_TimeInterval_Hours_1YearDataWithStartEndDate_FirstDataSerieValueAndDate() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Hours;
			TestDataContainer.Chart.StartDate = new DateTime(2013, 1, 1, 0 ,0, 0, DateTimeKind.Utc);
			TestDataContainer.Chart.EndDate = new DateTime(2013, 1, 2, 0, 0, 0, DateTimeKind.Utc);
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			TestDataContainer.MeterData =
				TestHelper.ParseMeterDataFromFile(const_meter_year_data_file_name);
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(24, dataPoint.Count);
			Assert.AreEqual(49, dataPoint[0].YValue);
			Assert.AreEqual(dt, dataPoint[0].XDateTime);
		}


		#endregion

		#region Global

		[Test]
		public void Chart_TimeInterval_Global_1YearData_DataSeriesValuesAndDates() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Global;
			
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(1, dataPoint.Count);
			Assert.AreEqual(2456, dataPoint[0].YValue);
		}

	    [Test]
	    public void Chart_TimeInterval_None_1YearData_DataSeriesValuesAndDates() {
	        //Arrange
	        TestDataContainer.Chart.TimeInterval = AxisTimeInterval.None;

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var dataPoint = result.Series.ToList().First().PointsWithValue;

            //Assert
            Assert.AreEqual(1, dataPoint.Count);
            Assert.AreEqual(2456, dataPoint[0].YValue);

	    }

		[Test]
		public void Chart_TimeInterval_Global_1YearDataWithStartEndDate_FirstDataSerieValueAndDate() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Global;
			TestDataContainer.Chart.StartDate = new DateTime(2013, 1, 1);
			TestDataContainer.Chart.EndDate = new DateTime(2013, 7, 1);
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(1, dataPoint.Count);
			Assert.AreEqual(1207, dataPoint[0].YValue);
		}

		#endregion

		#region Minutes/Seconds

		[TestCase(AxisTimeInterval.Seconds)]
		[TestCase(AxisTimeInterval.Minutes)]
		public void Chart_TimeInterval_Minutes_Seconds_1YearData_DataSeriesValuesAndDates(AxisTimeInterval timeInterval) {
			//Arrange
			TestDataContainer.Chart.TimeInterval = timeInterval;

			TestDataContainer.MeterData =
				TestHelper.ParseMeterDataFromFile(
					const_meter_year_data_file_name);
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(8760, dataPoint.Count);
			Assert.AreEqual(49, dataPoint[0].YValue);
			Assert.AreEqual(dt, dataPoint[0].XDateTime);
			
			for (int i = 1; i < dataPoint.Count; i++) {
				Assert.AreEqual(dt.AddHours(i), dataPoint[i].XDateTime);
			}
		}

		[Test]
		public void Chart_TimeInterval_Minutes_1YearDataWithStartEndDate_FirstDataSerieValueAndDate() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Minutes;
			TestDataContainer.Chart.StartDate = new DateTime(2013, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TestDataContainer.Chart.EndDate = new DateTime(2013, 1, 2, 0, 0, 0, DateTimeKind.Utc);
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			TestDataContainer.MeterData =
				TestHelper.ParseMeterDataFromFile(const_meter_year_data_file_name);
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(24, dataPoint.Count);  //the data is hourly that's why there is 24 points
			Assert.AreEqual(49, dataPoint[0].YValue);
			Assert.AreEqual(dt, dataPoint[0].XDateTime);
		}

		#endregion

		#region Quarters

		/// <summary>
		/// Test for Quarters time interval Data Aggregation
		/// </summary>
		[Test]
		public void Chart_TimeInterval_Quarters_1YearData_DataSeriesValuesAndDates() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Quarters;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(dataPoint.Count, 4);
			Assert.AreEqual(592.0, dataPoint[0].YValue);
			Assert.AreEqual(615.0, dataPoint[1].YValue);
			Assert.AreEqual(712.0, dataPoint[2].YValue);
			Assert.AreEqual(537.0, dataPoint[3].YValue);
			for (int i = 0; i < dataPoint.Count; i++) {
				Assert.AreEqual(dt.AddMonths(i*3), dataPoint[i].XDateTime);	
			}

		}

		[Test]
		public void Chart_TimeInterval_Quarters_1YearDataWithStartEndDate_FirstDataSerieValueAndDate() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Quarters;
			TestDataContainer.Chart.StartDate = new DateTime(2013, 1, 1);
			TestDataContainer.Chart.EndDate = new DateTime(2013, 7, 1);
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(2, dataPoint.Count);
		}


		#endregion 

		#region Days

		[Test]
		public void Chart_TimeInterval_Days_1YearData_DataSeriesValuesAndDates() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Days;
			TestDataContainer.MeterData =
				TestHelper.ParseMeterDataFromFile(const_meter_year_data_file_name);
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(365, dataPoint.Count);
			Assert.AreEqual(1190, dataPoint[0].YValue);
			Assert.AreEqual(dt, dataPoint[0].XDateTime);
		
			for (int i = 1; i < dataPoint.Count; i++) {
				Assert.AreEqual(dt.AddDays(i), dataPoint[i].XDateTime);
			}
		}

		[Test]
		public void Chart_TimeInterval_Days_1YearDataWithStartEndDate_FirstDataSerieValueAndDate() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Days;
			TestDataContainer.Chart.StartDate = new DateTime(2013, 1, 1);
			TestDataContainer.Chart.EndDate = new DateTime(2013, 2, 1);
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			TestDataContainer.MeterData =
				TestHelper.ParseMeterDataFromFile(
					const_meter_year_data_file_name);
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(31, dataPoint.Count);
			Assert.AreEqual(1190, dataPoint[0].YValue);
			Assert.AreEqual(dt, dataPoint[0].XDateTime);
		}

		#endregion

		#region Weeks

		[Test]
		public void Chart_TimeInterval_Weeks_1YearData_DataSeriesValuesAndDates() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Weeks;
			TestDataContainer.MeterData =
				TestHelper.ParseMeterDataFromFile(const_meter_year_data_file_name);
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2012, 12, 31);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(53, dataPoint.Count);//53 due to time zone problem
			Assert.AreEqual(7486, dataPoint[0].YValue);
			Assert.AreEqual(dt, dataPoint[0].XDateTime);
			//Assert.AreEqual(dt.AddHours(0), dataPoint[0].XDateTime);
			for (int i = 1; i < dataPoint.Count; i++) {
				Assert.AreEqual(dt.AddDays(i*7), dataPoint[i].XDateTime);
			}
		}

		[Test]
		public void Chart_TimeInterval_Weeks_1YearDataWithStartEndDate_FirstDataSerieValueAndDate() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Weeks;
			TestDataContainer.Chart.StartDate = new DateTime(2013, 1, 1);
			TestDataContainer.Chart.EndDate = new DateTime(2013, 1, 31);
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			TestDataContainer.MeterData =
				TestHelper.ParseMeterDataFromFile(const_meter_year_data_file_name);
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(5, dataPoint.Count);
			Assert.AreEqual(7486, dataPoint[0].YValue);
		}


		#endregion

		#region Years

		[Test]
		public void Chart_TimeInterval_Years_1YearData_DataSeriesValuesAndDates() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Years;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(1, dataPoint.Count);
			Assert.AreEqual(2456, dataPoint[0].YValue);
			Assert.AreEqual(dt, dataPoint[0].XDateTime);
		}

		[Test]
		public void Chart_TimeInterval_Years_1YearDataWithStartEndDate_FirstDataSerieValueAndDate() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Years;
			TestDataContainer.Chart.StartDate = new DateTime(2013, 1, 1);
			TestDataContainer.Chart.EndDate = new DateTime(2013, 6, 1);
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(1, dataPoint.Count);
			Assert.AreEqual(1102, dataPoint[0].YValue);
			Assert.AreEqual(dt, dataPoint[0].XDateTime);
		}


		#endregion

		#region Monthes

		[Test]
		public void Chart_TimeInterval_Monthes_1YearData_DataSeriesValuesAndDates() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Months;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(12, dataPoint.Count);
			Assert.AreEqual(252.0, dataPoint[2].YValue);
			for (int i = 1; i < dataPoint.Count; i++) {
				Assert.AreEqual(dt.AddMonths(i), dataPoint[i].XDateTime);
			}
		}

		[Test]
		public void Chart_TimeInterval_Monthes_1YearDataWithStartEndDate_FirstDataSerieValueAndDate() {
			//Arrange
			TestDataContainer.Chart.TimeInterval = AxisTimeInterval.Months;
			TestDataContainer.Chart.StartDate = new DateTime(2013, 1, 1);
			TestDataContainer.Chart.EndDate = new DateTime(2013, 7, 1);
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var dt = new DateTime(2013, 1, 1);
			var dataPoint = result.Series.ToList().First().PointsWithValue;

			//Assert
			Assert.AreEqual(6, dataPoint.Count);
		}

		#endregion 

		#region General

		[TestCase(AxisTimeInterval.Months, 6)]
		[TestCase(AxisTimeInterval.Days, 26)]
		[TestCase(AxisTimeInterval.Quarters, 2)]
		[TestCase(AxisTimeInterval.Weeks, 26)]
		[TestCase(AxisTimeInterval.Global, 1)]
		[TestCase(AxisTimeInterval.Hours, 26)]
		[TestCase(AxisTimeInterval.Minutes, 26)]
		[TestCase(AxisTimeInterval.Seconds, 26)]
		[TestCase(AxisTimeInterval.Semesters, 1)]
		[TestCase(AxisTimeInterval.Years, 1)]
        [TestCase(AxisTimeInterval.None, 1)]
		public void Chart_TimeInterval_Days_6MonthsDataGetSecondDataPoint_Throws(AxisTimeInterval timeInterval, int pointsInFirstTimeInterval) {
			//Arrange
			TestDataContainer.Chart.TimeInterval = timeInterval;
			TestDataContainer.Chart.StartDate = new DateTime(2013, 1, 1);
			TestDataContainer.Chart.EndDate = new DateTime(2013, 7, 1);
			TestDataContainer.Chart.DynamicTimeScaleEnabled = false;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);


			//Assert
			Assert.Throws<ArgumentOutOfRangeException>(
				() => { var dataPoint = result.Series.ToList().First().PointsWithValue[pointsInFirstTimeInterval]; });
		}

		#endregion
	}



}

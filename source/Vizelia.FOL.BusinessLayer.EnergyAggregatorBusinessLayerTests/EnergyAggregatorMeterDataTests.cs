﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {
	public class EnergyAggregatorMeterDataTests : BaseEnergyAggregatorBusinessLayerTests{
		
		[Test]
		public void MeterData_GetStoreFromMeter_MeterWithData_RerurnsMetrerData()
		{
			//Arrange
			Meter meter = TestDataContainer.Meters.First(m => m.KeyMeter == DefaultKeyMeterStr);
			SortedList<DateTime, MD> meterData = TestDataContainer.MeterData[DefaultMeterKey];
			
			InitializeTest();

			//Act
			var pagingParameter = new PagingParameter();
			JsonStore<MeterData> resultMeterData = EnergyAggregator.MeterData_GetStoreFromMeter(EnergyAggregatorContext, meter, pagingParameter, null, null);


			//Assert
			Assert.AreEqual(resultMeterData.recordCount, meterData.Count);

			foreach (MeterData resultData in resultMeterData.records)
			{
				MD record = meterData.Values.FirstOrDefault(md => md.KeyMeter == MeterHelper.ConvertMeterKeyToInt(resultData.KeyMeter) &&
																  md.Value == resultData.Value);
				Assert.IsNotNull(record);
			}
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void MeterData_GetStoreFromMeter_NoPagingParameter_ThrowsException() {
			//Arrange
			Meter meter = TestDataContainer.Meters.First(m => m.KeyMeter == DefaultKeyMeterStr);

			InitializeTest();

			//Act
			PagingParameter pagingParameter = null;
			EnergyAggregator.MeterData_GetStoreFromMeter(EnergyAggregatorContext, meter, pagingParameter, null, null);

		}

		[Test]
		public void MeterData_GetStoreFromMeter_NoMeter_NoDataIsReturned (){
			//Arrange
			Meter meter = null;

			InitializeTest();

			//Act
			var pagingParameter = new PagingParameter();
			JsonStore<MeterData> resultMeterData = EnergyAggregator.MeterData_GetStoreFromMeter(EnergyAggregatorContext, meter, pagingParameter, null, null);

			Assert.IsTrue(resultMeterData.recordCount == 0);

		}


		[Test]
		public void MeterData_GetLastFromMeter_MeterWithData_RerurnsMetrerLastItemData() {
			
			//Arrange
			Meter meter = TestDataContainer.Meters.First(m => m.KeyMeter == DefaultKeyMeterStr);
			SortedList<DateTime, MD> meterData = TestDataContainer.MeterData[DefaultMeterKey];

			InitializeTest();

			//Act
			MeterData resultMeterData = EnergyAggregator.MeterData_GetLastFromMeter(EnergyAggregatorContext, meter);

			//Assert
			KeyValuePair<DateTime, MD> lastMeterValue = meterData.Last();

			Assert.AreEqual(lastMeterValue.Value.Value , resultMeterData.Value);
		}


		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void MeterData_GetLastFromMeter_NullMeter_ThrowsException() {

			//Arrange
			Meter meter = null;

			InitializeTest();

			//Act
			EnergyAggregator.MeterData_GetLastFromMeter(EnergyAggregatorContext, meter);
		}
	}
}

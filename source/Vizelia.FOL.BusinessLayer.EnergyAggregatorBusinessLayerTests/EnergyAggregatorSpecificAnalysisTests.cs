﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDay.iCal.Serialization.iCalendar;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;
using System.Drawing;
using Vizelia.FOL.Common.Localization;
using Filter = Vizelia.FOL.BusinessEntities.Filter;


namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests
{
    public class EnergyAggregatorSpecificAnalysisTests : BaseEnergyAggregatorBusinessLayerTests {
        private const string const_meter_data_file_name =
        "Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.SpecificAnalysisTests.MeterDataJson3Meters.txt";

	
        [Test]
        public void ChartProcess_SpecificAnalysis_HeatMap_DataPointsColoredAscending() {
            //Arrange
            TestDataContainer.Chart.HeatMapMaxColor = "000000"; //black
            TestDataContainer.Chart.HeatMapMinColor = "FFFFFF"; //white
            TestDataContainer.Chart.SpecificAnalysis = ChartSpecificAnalysis.HeatMap;

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(),
                DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var tempColor = TestDataContainer.Chart.HeatMapMinColor;
            var colorList = resultSeries[0].PointsWithValue.OrderBy(dataPoint => dataPoint.YValue).Select(x => x.Color);
                //ordering the points by their value and getting the color
            Assert.AreEqual(TestDataContainer.Chart.HeatMapMinColor.ToLower(), colorList.First().Value.Name.Substring(2));
            Assert.AreEqual(TestDataContainer.Chart.HeatMapMaxColor.ToLower(), colorList.Last().Value.Name.Substring(2));
            foreach (var color in colorList) {
                var colorValue = color.Value.Name.Substring(2); //removing the first 2 chars to get the actual value
                Assert.LessOrEqual(Convert.ToInt32(colorValue, 16), Convert.ToInt32(tempColor, 16));
                    // checking that each color is lower or equal to the previous one
                tempColor = colorValue;
            }
        }

        [Test]
        public void ChartProcess_SpecificAnalysis_LoadDurationCurve_PointsYValueDescendingXValueAscending() {
            //Arrange
            TestDataContainer.Chart.SpecificAnalysis = ChartSpecificAnalysis.LoadDurationCurve;

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(),
                DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            DataPoint tempDataPoint = resultSeries[0].PointsWithValue.First();
            Assert.AreEqual(0, tempDataPoint.XValue);
            Assert.AreEqual(100, resultSeries[0].PointsWithValue.Last().XValue);
            int dataPointIndex = 1;
            foreach (var dataPoint in resultSeries[0].PointsWithValue) {
                Assert.LessOrEqual(dataPoint.YValue, tempDataPoint.YValue); // YValue Descending
                Assert.GreaterOrEqual(dataPoint.XValue, tempDataPoint.XValue); // XValue Ascending
                Assert.GreaterOrEqual(dataPoint.XValue, 0);
                Assert.LessOrEqual(dataPoint.XValue, 100);
                Assert.AreEqual(dataPointIndex.ToString(), dataPoint.Name); // Data point name is the index
                tempDataPoint = dataPoint;
                dataPointIndex++;
            }
        }

        [Test]
        public void ChartProcess_SpecificAnalysis_Correlation_CorrelationAndCloudGraphsCreated() {
            //Arrange
            const string secondMeterKey = "#106";
            var secondMeter = new Meter {
                KeyMeter = secondMeterKey,
                KeyLocation = DefaultKeyLocation,
                Factor = 1.0,
                IsAcquisitionDateTimeEndInterval = false,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Name = "Test Site Second Meter"
            };

            TestDataContainer.Meters.Add(secondMeter);
            TestDataContainer.Chart.SpecificAnalysis = ChartSpecificAnalysis.Correlation;
            TestDataContainer.Chart.CorrelationXSerieLocalId = string.Format("{0} - {1}", DefaultLocationName + " / " + DefaultMeterName, DefaultClassificationItemName);
            TestDataContainer.Chart.CorrelationYSerieLocalId = string.Format("{0} - {1}", DefaultLocationName + " / " + secondMeter.Name, DefaultClassificationItemName);

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(),
                DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var cloudPoints = resultSeries.First(x => x.LocalId.Contains(Langue.msg_chart_correlation_cloud)).PointsWithValue;
            
            var firstMeterPoints =
                resultSeries.First(x => x.LocalId.Contains(TestDataContainer.Chart.CorrelationXSerieLocalId))
                    .PointsWithValue;
            var secondMeterPoints =
                resultSeries.First(x => x.LocalId.Contains(TestDataContainer.Chart.CorrelationYSerieLocalId))
                    .PointsWithValue;
            for (int i = 0; i < cloudPoints.Count; i++) {
                Assert.AreEqual(firstMeterPoints[i].YValue, cloudPoints[i].XValue); // X axis of cloud serie is the Y value of the first data serie 
                Assert.AreEqual(secondMeterPoints[i].YValue, cloudPoints[i].YValue); // Y axis of cloud serie is the Y value of the second data serie
            }

            var lineDataSerie = resultSeries.First(x => x.LocalId.Contains(Langue.msg_chart_correlation_line)); 
            var square = Convert.ToDouble(lineDataSerie.Name.Substring(lineDataSerie.Name.IndexOf(':') + 1)); // the correlation value is kept in the serie name...
            //var correlation = CalculateCorrelation(firstMeterPoints.Select(x => x.YValue.Value).ToList(), secondMeterPoints.Select(x => x.YValue.Value).ToList());
            var correlation = CalculationHelper.ComputeCorrelationCoefficient(firstMeterPoints.Select(x => x.YValue.Value).ToArray(), secondMeterPoints.Select(x => x.YValue.Value).ToArray());
            Assert.AreEqual(Math.Round(correlation, 2), square);
            
        }

        

        [TestCase(20)]
        [TestCase(0)]
        [TestCase(100)]
        public void ChartProcess_SpecificAnalysis_DifferenceHighlight_DataSerieColoredByThreshold(int threshold)
        {
            //Arrange
            const string secondMeterKey = "#106";
            var secondMeter = new Meter
            {
                KeyMeter = secondMeterKey,
                KeyLocation = DefaultKeyLocation,
                Factor = 1.0,
                IsAcquisitionDateTimeEndInterval = false,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Name = "Test Site Second Meter"
            };

            TestDataContainer.Meters.Add(secondMeter);
            TestDataContainer.Chart.SpecificAnalysis = ChartSpecificAnalysis.DifferenceHighlight;
			TestDataContainer.Chart.DifferenceHighlightSerie1LocalId = string.Format("{0} - {1}", DefaultLocationName + " / " + DefaultMeterName, DefaultClassificationItemName);
			TestDataContainer.Chart.DifferenceHighlightSerie2LocalId = string.Format("{0} - {1}", DefaultLocationName + " / " + secondMeter.Name, DefaultClassificationItemName);
            TestDataContainer.Chart.DifferenceHighlightSerieThreshold = threshold;

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(),
                DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var differencePoints = resultSeries.First(x => x.Name == Langue.msg_chart_differencehighlight_line).PointsWithValue;
            var firstMeterPoints = resultSeries.First(x => x.LocalId == TestDataContainer.Chart.DifferenceHighlightSerie1LocalId).PointsWithValue;
			var secondMeterPoints = resultSeries.First(x => x.LocalId == TestDataContainer.Chart.DifferenceHighlightSerie2LocalId).PointsWithValue;

            for (var i = 0; i < differencePoints.Count; i++) {
                var diff = ((firstMeterPoints[i].YValue - secondMeterPoints[i].YValue)*100d)/firstMeterPoints[i].YValue;
                Assert.AreEqual(Math.Round(diff.Value, 2), Convert.ToDouble(differencePoints[i].Tooltip.Substring(0, differencePoints[i].Tooltip.Length - 2))); //the diff is kept in the tooltip
                if (Math.Abs(diff.Value) < threshold) { //if the diff is smaller then threshold the point is colored.
                    Assert.AreEqual(Color.Transparent, differencePoints[i].Color);
                }
                else {
                    Assert.IsNull(differencePoints[i].Color);
                }
            }
        }

        protected override string TestMeterDataFilePath {
            get { return const_meter_data_file_name; }
        }
    }
}

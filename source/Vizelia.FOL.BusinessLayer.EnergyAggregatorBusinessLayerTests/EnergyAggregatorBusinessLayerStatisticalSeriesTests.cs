﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDay.Collections;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests
{
    public class EnergyAggregatorBusinessLayerStatisticalSeriesTests : BaseEnergyAggregatorBusinessLayerTests
    {
        private const string const_meter_data_file_name =
        "Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.Common.MonthlyMeterData.txt";

		private readonly string m_DataSerieLocalId = string.Format("{0} - {1}", DefaultLocationName + " / " + DefaultMeterName, DefaultClassificationItemName);

        private const string const_calculated_serie_key = "1";
        private const string const_calculated_serie_name = "CalculatedSerie1";
        private const string const_calculated_serie_localid = "CalculatedSerie1";

        protected override string TestMeterDataFilePath {
            get { return const_meter_data_file_name; }
        }

        [TestCase(MathematicOperator.SUM)]
        [TestCase(MathematicOperator.AVG)]
        [TestCase(MathematicOperator.MAX)]
        [TestCase(MathematicOperator.MIN)]
        public void ChartProcess_StatisticalSerieOperator_ApplyOperatorOnDataSeries(MathematicOperator mathematicOperator) {
            //Arrange
            const string secondMeterKeyStr = "#106";
            var statisticalSerie = new StatisticalSerie {
                Operator = mathematicOperator,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                KeyChart = TestChartkey
            };

            var meter = new Meter
            {
                KeyMeter = secondMeterKeyStr,
                KeyLocation = DefaultKeyLocation,
                Factor = 1.0,
                IsAcquisitionDateTimeEndInterval = false,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Name = "Test Site Second Meter"
            };

			var meter1Data = TestDataContainer.MeterData[DefaultMeterKey];
			int secondMeterKey = MeterHelper.ConvertMeterKeyToInt(meter.KeyMeter);
			TestDataContainer.MeterData[secondMeterKey] = CloneMeterData(meter1Data, secondMeterKey); 

            TestDataContainer.Meters.Add(meter);

            TestDataContainer.Chart.StatisticalSeries = new List<StatisticalSerie> {statisticalSerie};

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var statisticalDataSerie =
                resultSeries.First(
                    x => x.LocalId.Contains(mathematicOperator.ToString(), StringComparison.OrdinalIgnoreCase));
            var firstMeterDataSerie = resultSeries.First(x => x.KeyMeter == DefaultKeyMeterStr);
            var secondMeterDataSerie = resultSeries.First(x => x.KeyMeter == secondMeterKeyStr);

            var mathematicalFunction = CalculationHelper.GetMathematicalFunctionFromMathematicOperator(mathematicOperator);
            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++) {
                Assert.AreEqual(mathematicalFunction(firstMeterDataSerie.PointsWithValue[i].YValue.Value, secondMeterDataSerie.PointsWithValue[i].YValue.Value), statisticalDataSerie.PointsWithValue[i].YValue);    
            }
        }

        [Test]
        public void ChartProcess_StatisticalSeries_DontIncludeHiddenSeriesRunAfterCalculatedSerie_StatisticalSeriesEqualCalculatedSerie()
        {
            //Arrange
            var statisticalSerie = new StatisticalSerie
            {
                Operator = MathematicOperator.SUM,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                KeyChart = TestChartkey,
                IncludeHiddenSeries = false,
                RunAfterCalculatedSeries = true
            };

            var calculatedSerie = new CalculatedSerie {
				KeyCalculatedSerie = const_calculated_serie_key,
				KeyChart = TestChartkey,
				KeyLocalizationCulture = DefaultCulture,
				LocalId = const_calculated_serie_localid,
				Name = const_calculated_serie_name,
				Order = 1,
                HideUsedSeries = true,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Formula = string.Format("({0} - {1})", DefaultLocationName + " / " + DefaultMeterName, DefaultClassificationItemName)
			};

            TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> {calculatedSerie};
            TestDataContainer.Chart.StatisticalSeries = new List<StatisticalSerie> {statisticalSerie};
            
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var calculatedResultSerie = resultSeries.First(dataSerie => dataSerie.Name == calculatedSerie.Name);
            var statisticalDataSerie =
               resultSeries.First(
                   x => x.LocalId.Contains(statisticalSerie.Operator.ToString(), StringComparison.OrdinalIgnoreCase));
            
            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++) {
                Assert.AreEqual(calculatedResultSerie.PointsWithValue[i].YValue.Value, statisticalDataSerie.PointsWithValue[i].YValue.Value);
            }
        }


        [Test]
        public void ChartProcess_StatisticalSeries_IncludeHiddenSeriesRunAfterCalculatedSerie_StatisticalSeriesIncludesCalculatedAndMeterSeries()
        {
            //Arrange
            var statisticalSerie = new StatisticalSerie
            {
                Operator = MathematicOperator.SUM,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                KeyChart = TestChartkey,
                IncludeHiddenSeries = true,
                RunAfterCalculatedSeries = true
            };

            var calculatedSerie = new CalculatedSerie
            {
                KeyCalculatedSerie = const_calculated_serie_key,
                KeyChart = TestChartkey,
                KeyLocalizationCulture = DefaultCulture,
                LocalId = const_calculated_serie_localid,
                Name = const_calculated_serie_name,
                Order = 1,
                HideUsedSeries = true,
                KeyClassificationItem = DefaultTestClassificationItemKey,
				Formula = string.Format("({0} - {1})", DefaultLocationName + " / " + DefaultMeterName, DefaultClassificationItemName)
            };

            TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { calculatedSerie };
            TestDataContainer.Chart.StatisticalSeries = new List<StatisticalSerie> { statisticalSerie };

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var calculatedResultSerie = resultSeries.First(dataSerie => dataSerie.Name == calculatedSerie.Name);
            var statisticalDataSerie =
               resultSeries.First(
                   x => x.LocalId.Contains(statisticalSerie.Operator.ToString(), StringComparison.OrdinalIgnoreCase));
            var meterResultSerie = resultSeries.First(dataSerie => dataSerie.KeyMeter == DefaultKeyMeterStr);

            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++)
            {
                Assert.AreEqual(calculatedResultSerie.PointsWithValue[i].YValue.Value + meterResultSerie.PointsWithValue[i].YValue.Value, statisticalDataSerie.PointsWithValue[i].YValue.Value);
            }
        }


        [Test]
        public void ChartProcess_StatisticalSeries_IncludeHiddenSeriesDontRunAfterCalculatedSerie_StatisticalSeriesEqualMeterSerie()
        {
            //Arrange
            var statisticalSerie = new StatisticalSerie
            {
                Operator = MathematicOperator.SUM,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                KeyChart = TestChartkey,
                IncludeHiddenSeries = true,
                RunAfterCalculatedSeries = false
            };

            var calculatedSerie = new CalculatedSerie
            {
                KeyCalculatedSerie = const_calculated_serie_key,
                KeyChart = TestChartkey,
                KeyLocalizationCulture = DefaultCulture,
                LocalId = const_calculated_serie_localid,
                Name = const_calculated_serie_name,
                Order = 1,
                HideUsedSeries = true,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Formula = string.Format("({0} - {1})", DefaultMeterName, DefaultClassificationItemName)
            };

            TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { calculatedSerie };
            TestDataContainer.Chart.StatisticalSeries = new List<StatisticalSerie> { statisticalSerie };

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var meterResultSerie = resultSeries.First(dataSerie => dataSerie.KeyMeter == DefaultKeyMeterStr);
            var statisticalDataSerie =
               resultSeries.First(
                   x => x.LocalId.Contains(statisticalSerie.Operator.ToString(), StringComparison.OrdinalIgnoreCase));

            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++)
            {
                Assert.AreEqual(meterResultSerie.PointsWithValue[i].YValue.Value, statisticalDataSerie.PointsWithValue[i].YValue.Value);
            }
        }


        [Test]
        public void ChartProcess_StatisticalSeries_DontIncludeHiddenSeriesDontRunAfterCalculatedSerie_StatisticalSeriesEqualMeterSerie()
        {
            //Arrange
            var statisticalSerie = new StatisticalSerie
            {
                Operator = MathematicOperator.SUM,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                KeyChart = TestChartkey,
                IncludeHiddenSeries = false,
                RunAfterCalculatedSeries = false
            };

            var calculatedSerie = new CalculatedSerie
            {
                KeyCalculatedSerie = const_calculated_serie_key,
                KeyChart = TestChartkey,
                KeyLocalizationCulture = DefaultCulture,
                LocalId = const_calculated_serie_localid,
                Name = const_calculated_serie_name,
                Order = 1,
                HideUsedSeries = true,
                KeyClassificationItem = DefaultTestClassificationItemKey,
                Formula = string.Format("({0} - {1})", DefaultMeterName, DefaultClassificationItemName)
            };

            TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { calculatedSerie };
            TestDataContainer.Chart.StatisticalSeries = new List<StatisticalSerie> { statisticalSerie };

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            var meterResultSerie = resultSeries.First(dataSerie => dataSerie.KeyMeter == DefaultKeyMeterStr);
            var statisticalDataSerie =
               resultSeries.First(
                   x => x.LocalId.Contains(statisticalSerie.Operator.ToString(), StringComparison.OrdinalIgnoreCase));

            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++)
            {
                Assert.AreEqual(meterResultSerie.PointsWithValue[i].YValue.Value, statisticalDataSerie.PointsWithValue[i].YValue.Value);
            }
        }

    }
}

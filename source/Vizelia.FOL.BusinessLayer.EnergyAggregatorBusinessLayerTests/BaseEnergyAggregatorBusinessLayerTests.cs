﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DDay.iCal;
using FakeItEasy;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer.BrokerDB;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {
	
	/// <summary>
	/// Base class for Energy Aggregator Tests
	/// </summary>
	[TestFixture]
	public abstract class BaseEnergyAggregatorBusinessLayerTests
	{
		protected EnergyAggregatorBusinessLayer EnergyAggregator;
		protected EnergyAggregatorContext EnergyAggregatorContext;

		/// <summary>
		/// The default meter data file for the tests(unless overridden) is a file containing generated data with a weekly time interval
		/// </summary>
		public const string const_meter_data_path = @"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.Common.WeeklyMeterData.txt";

		/// <summary>
		/// Culture List - For mocking the Data Base 
		/// </summary>
		public static readonly List<string> CultureList = new List<string> {
			"af","ar", "be", "bg", "ca", "cs", "da", "de", "el", "en", "en-gb", "es",
			"et","fa", "fi", "fr", "gl", "he", "hi", "hr", "hu", "id", "is", "it", "ja",
			"ko", "lt", "lv", "mk", "ms", "nl", "no", "pl", "pt", "ro", "ru", "sk", "sl", "sq",
			"sr", "sv", "sw", "th", "tr", "uk", "vi", "zh"
		};

		protected const string DefaultCulture = "en-US";
		protected const string DefaultKeyLocation = "#99";
		protected const string DefaultLocationName = "Root Site";
		protected const string DefaultTestClassificationItemKey = "#101";
		protected const string TestChartkey = "1";
	    protected const string DefaultClassificationItemName = "kwh";
	    protected const string DefaultMeterName = "Test Site Meter";


		protected string m_DefaultKeyMeterStr;

		protected string DefaultKeyMeterStr {
			get { return m_DefaultKeyMeterStr; } 
		}
		

		protected int DefaultMeterKey {
			get { return MeterHelper.ConvertMeterKeyToInt(DefaultKeyMeterStr); }
		}

		/// <summary>
		/// Holds the test data container.
		/// </summary>
		protected TestDataContainer TestDataContainer { get; set; }

		/// <summary>
		/// Gets the path of the file that contains the meter data for the tests
		/// </summary>
		/// <value>
		/// The test meter data file path.
		/// </value>
		protected virtual string TestMeterDataFilePath {
			get { return const_meter_data_path; } 
		}

		/// <summary>
		/// Initializes the test data container.
		/// </summary>
		/// <returns></returns>
		protected virtual TestDataContainer InitializeTestDataContainer()
		{
			ConcurrentDictionary<int, SortedList<DateTime, MD>> meterData = TestHelper.ParseMeterDataFromFile(TestMeterDataFilePath);
			
			//The default meter key is initialized to the first meter key in the file
			m_DefaultKeyMeterStr = MeterHelper.ConvertMeterKeyToString(meterData.Keys.First());

			var testDataContainer = new TestDataContainer {

				ClassificationItems = new Dictionary<string, ClassificationItem>
				{
					{DefaultTestClassificationItemKey, new ClassificationItem {KeyClassificationItem = DefaultTestClassificationItemKey, LocalId = "METERCLASS_kwh", LongPath = "kwh", Title = DefaultClassificationItemName}}
				},
				Locations = new Dictionary<string, Location>
				{
					{DefaultKeyLocation, new Location {KeyLocation = DefaultKeyLocation,LongPath = DefaultLocationName}}
				},
				Meters = new List<Meter>
				{
					new Meter
					{
						KeyMeter = m_DefaultKeyMeterStr,
						KeyLocation = DefaultKeyLocation,
						Factor = 1.0,
						IsAcquisitionDateTimeEndInterval = false,
						KeyClassificationItem = DefaultTestClassificationItemKey,
						Name = DefaultMeterName
					}
				},
				MeterData  = meterData,
				TenantDateRangeList = new List<EnergyAggregatorContext>(),
				Chart = new Chart {
					KeyChart = TestChartkey,
					HistoricalAnalysisDefinitions = new List<ChartHistoricalAnalysis>()
				},
				Calendars = new Dictionary<string, Tuple<string, string>>(),
                HistoricalData = new Dictionary<string, PsetAttributeHistorical>(),
                LocalizationDictionary = new Dictionary<string, Dictionary<string, string>> { { "en", new Dictionary<string, string>() } }
			};

			// Disable the Dynamic Time scaling of the chart that is enabled by default so that the Start and End dates of the chart could be set
			// according to the test data and not by the current DateTime
			testDataContainer.Chart.DynamicTimeScaleEnabled = false;
			testDataContainer.Chart.StartDate = testDataContainer.MeterData[DefaultMeterKey].Keys[0].Date;
			testDataContainer.Chart.EndDate = testDataContainer.MeterData[DefaultMeterKey].Keys.Last().AddYears(1).Date;

			return testDataContainer;
		}


		[SetUp]
		public virtual void SetUp() {
			TestDataContainer = InitializeTestDataContainer();
		}

		/// <summary>
		/// Initializes the test.
		/// </summary>
		protected void InitializeTest() {

			EnergyAggregatorContext = new EnergyAggregatorContext {
				ApplicationName = "FacilityOnLine",
				Culture = DefaultCulture,
				EndDate = new DateTime(2017, 1, 1).AddSeconds(-1),
				StartDate = new DateTime(1990, 1, 1),
				TenantUrl = @"http://facilityonline/Energy.svc",
				Username = "Admin",
				StartInSeperateThread = false
			};
			TestDataContainer.TenantDateRangeList.Add(EnergyAggregatorContext);
		
			EnergyAggregator = new EnergyAggregatorBusinessLayer();
			
			var coreBusinessLayer = A.Fake<ICoreBusinessLayer>();
			A.CallTo(() => coreBusinessLayer.Location_GetDictionary()).Returns(TestDataContainer.Locations);
			A.CallTo(() => coreBusinessLayer.ClassificationItem_GetDictionary("")).Returns(TestDataContainer.ClassificationItems);
			A.CallTo(() => coreBusinessLayer.CalendarEvent_GetAll()).Returns(TestDataContainer.Calendars);
			A.CallTo(() => coreBusinessLayer.PsetAttributeHistorical_GetDictionary()).Returns(TestDataContainer.HistoricalData);


			var energyBusinessLayer = A.Fake<IEnergyBusinessLayer>();

			A.CallTo(() => energyBusinessLayer.MeterData_GetAllByDateRange(EnergyAggregatorContext.StartDate, EnergyAggregatorContext.EndDate, null)).Returns(TestDataContainer.MeterData);

			A.CallTo(() => energyBusinessLayer.Chart_GetItem(TestChartkey, "*"))
				.Returns(TestDataContainer.Chart);
			
			Location testLocation = new Location();
			A.CallTo(() => energyBusinessLayer.Location_GetItem(DefaultKeyLocation)).Returns(TestDataContainer.Locations.TryGetValue(DefaultKeyLocation, out testLocation) ? testLocation : new Location { KeyLocation = DefaultKeyLocation });

			A.CallTo(() => energyBusinessLayer.Chart_GetMeters(TestChartkey, true, null, null, null)).Returns(TestDataContainer.Meters);
			List<string> locationKeys = new List<string> { TestDataContainer.Locations.Select(location => location.Key).ToString() };
			A.CallTo(() => energyBusinessLayer.Chart_GetLocationKeys(TestChartkey, TestDataContainer.Meters)).Returns(locationKeys);
			A.CallTo(() => energyBusinessLayer.Meter_GetDictionary()).Returns(TestDataContainer.Meters.ToDictionary(k => MeterHelper.ConvertMeterKeyToInt(k.KeyMeter)));
		    A.CallTo(() => energyBusinessLayer.Chart_GetLocations(TestChartkey, TestDataContainer.Meters))
		        .Returns(TestDataContainer.Locations.Values.ToList());
            

			TestResolver testResolver = new TestResolver();
			Helper.Resolver = testResolver;

			var coreDataAccess = A.Fake<ICoreDataAccess>();
			testResolver.AddFake(typeof(ICoreDataAccess), coreDataAccess);
			IServiceDeskBusinessLayer sdbl = A.Fake<IServiceDeskBusinessLayer>();
			testResolver.AddFake(typeof(IServiceDeskBusinessLayer), sdbl);

			
			var calendarBroker = A.Fake<CalendarOccurrencesBroker>();
			A.CallTo(() => calendarBroker.GetAll(true)).Returns(new List<CalendarOccurrences>());
			A.CallTo(() => calendarBroker.Save(null)).WithAnyArguments().Returns(null);

			testResolver.AddFake(typeof(ICoreBusinessLayer), coreBusinessLayer);
			testResolver.AddFake(typeof(IEnergyBusinessLayer), energyBusinessLayer);
			testResolver.AddFake(typeof(CalendarOccurrencesBroker), calendarBroker);




			CacheService.Add("const_cache_licence_validity", true);
			CacheService.Add("supportedCultures", CultureList);
            CacheService.Add(CacheKey.const_cache_localization, TestDataContainer.LocalizationDictionary);
			
			EnergyAggregator.Initialize(TestDataContainer.TenantDateRangeList);

		}

		[TearDown]
        public virtual void TearDown()
        {
			EnergyAggregator.SetIsInitialized(false);
		}


		protected ConcurrentDictionary<int, SortedList<DateTime, MD>> GenerateRandomMeterData(string localId, DateTime startDate, DateTime endDate, int frequencyNumber, AxisTimeInterval frequency) {

			var rand = new Random();
			long count = 0;
			int totalCount = GetAmountOfMeterDataItemsInRange(startDate, endDate, frequencyNumber, frequency);
			bool shouldContinue = totalCount > 0;
			int keyMeter = MeterHelper.ConvertMeterKeyToInt(localId);
			var unsortedDictionary = new Dictionary<int, Dictionary<DateTime, MD>> { { keyMeter, new Dictionary<DateTime, MD>() } };

			while (shouldContinue) {
				var meterData = new MD {
					KeyMeter = keyMeter, // we set this since we use the mapping for bulk insert.
					AcquisitionDateTime = startDate,
					Value = rand.Next(0, 100),
					Validity = 0,
					KeyMeterData = count
				};

				// Progress the start date for the next iteration.
				startDate = AddFrequency(startDate, frequencyNumber, frequency);
				shouldContinue = startDate <= endDate;
				count++;

				if (!unsortedDictionary[keyMeter].ContainsKey(meterData.AcquisitionDateTime)) {
					unsortedDictionary[keyMeter].Add(meterData.AcquisitionDateTime, meterData);
				}
			}

			return
				new ConcurrentDictionary<int, SortedList<DateTime, MD>>(
					unsortedDictionary.Select(
						item => new KeyValuePair<int, SortedList<DateTime, MD>>(item.Key, new SortedList<DateTime, MD>(item.Value))));
		}

		private static int GetAmountOfMeterDataItemsInRange(DateTime start, DateTime end, int frequencyNumber,
			AxisTimeInterval frequency) {
			int totalSeconds = Convert.ToInt32((end - start).TotalSeconds);
			int totalCount;

			switch (frequency) {
				case AxisTimeInterval.Seconds:
					totalCount = totalSeconds / frequencyNumber;
					break;
				case AxisTimeInterval.Minutes:
					totalCount = totalSeconds / (60 * frequencyNumber);
					break;

				case AxisTimeInterval.Hours:
					totalCount = totalSeconds / (60 * 60 * frequencyNumber);
					break;

				case AxisTimeInterval.Days:
					totalCount = totalSeconds / (24 * 60 * 60 * frequencyNumber);
					break;

				case AxisTimeInterval.Weeks:
					totalCount = totalSeconds / (7 * 24 * 60 * 60 * frequencyNumber);
					break;
				case AxisTimeInterval.Months:
					totalCount = totalSeconds / (30 * 24 * 60 * 60 * frequencyNumber);
					break;

				case AxisTimeInterval.Quarters:
					totalCount = totalSeconds / (4 * 30 * 24 * 60 * 60 * frequencyNumber);
					break;
				case AxisTimeInterval.Semesters:
					totalCount = totalSeconds / (2 * 30 * 24 * 60 * 60 * frequencyNumber);
					break;

				case AxisTimeInterval.Years:
					totalCount = totalSeconds / (365 * 24 * 60 * 60 * frequencyNumber);
					break;

				default:
					throw new ArgumentOutOfRangeException("frequency");
			}

			return totalCount;
		}

		protected DateTime AddFrequency(DateTime date, int frequencyNumber, AxisTimeInterval frequency) {
			DateTime newDate;

			switch (frequency) {
				case AxisTimeInterval.Years:
					newDate = date.AddYears(1 * frequencyNumber);
					break;
				case AxisTimeInterval.Quarters:
					newDate = date.AddMonths(3 * frequencyNumber);
					break;
				case AxisTimeInterval.Semesters:
					newDate = date.AddMonths(6 * frequencyNumber);
					break;
				case AxisTimeInterval.Months:
					newDate = date.AddMonths(1 * frequencyNumber);
					break;
				case AxisTimeInterval.Weeks:
					newDate = date.AddDays(7 * frequencyNumber);
					break;
				case AxisTimeInterval.Days:
					newDate = date.AddDays(1 * frequencyNumber);
					break;
				case AxisTimeInterval.Hours:
					newDate = date.AddHours(1 * frequencyNumber);
					break;
				case AxisTimeInterval.Minutes:
					newDate = date.AddMinutes(1 * frequencyNumber);
					break;
				case AxisTimeInterval.Seconds:
					newDate = date.AddSeconds(1 * frequencyNumber);
					break;
				default:
					throw new ArgumentOutOfRangeException("frequency");
			}

			return newDate;
		}


		protected SortedList<DateTime, MD> CloneMeterData(SortedList<DateTime, MD> sourceMeterData, int targetMeterKey) {

			var clonedMeterDataList = new SortedList<DateTime, MD>();

			foreach (var key in sourceMeterData.Keys) {
				MD sourceData = sourceMeterData[key];
				MD clonedMeterData = new MD() {
					KeyMeter = targetMeterKey,
					AcquisitionDateTime = sourceData.AcquisitionDateTime,
					Value = sourceData.Value
				};

				clonedMeterDataList.Add(key, clonedMeterData);
			}

			return clonedMeterDataList;
		}

	}
}

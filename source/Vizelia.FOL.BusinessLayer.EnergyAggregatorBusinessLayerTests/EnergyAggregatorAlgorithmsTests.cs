﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {

	/// <summary>
	///  The algorithms that can currently be tested on the chart can only be <see cref="AlgorithmSource.BuiltIn"/>
	///  In order to test <see cref="AlgorithmSource.ExternalLibrary"/>, The class <see cref="AlgorithmHelper"/> must become testable
	///  This class contains all the logic of resolving and executing the the Algorithms and is currently a static class with static methods
	///  It contains the usage of file system in the case of external algorithms and this logic cannot currently be mocked
	/// </summary>
	public class EnergyAggregatorAlgorithmsTests : BaseEnergyAggregatorBusinessLayerTests {

		[Test]
		public void ChartProcess_ChartWithBuiltInAlgorithm_SeriesValuesHaveBeenTransformed()
		{
			//Arrange
			//Use a test Algorithm that transforms all values of the series to 1
			var chartAlgorithm = new ChartAlgorithm()
			{
				KeyAlgorithm = "1",
				Algorithm = new Algorithm()
				{
					Name =
						"Vizelia.FOL.ConcreteProviders.TransformAllValuesToOneTestAlgorithm,Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests",
					UniqueId = Guid.NewGuid().ToString(),
					KeyAlgorithm = "1",
					Source = AlgorithmSource.BuiltIn 
				}
			};

			TestDataContainer.Chart.Algorithms = new List<ChartAlgorithm> {chartAlgorithm};
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			
			//Assert
			List<DataSerie> resultSeries = result.Series.ToList();
			List<DataPoint> dataPoints = resultSeries[0].Points;
			foreach (DataPoint dataPoint in dataPoints)
			{
				Assert.AreEqual(1, dataPoint.YValue);
			}
		}
	}
}

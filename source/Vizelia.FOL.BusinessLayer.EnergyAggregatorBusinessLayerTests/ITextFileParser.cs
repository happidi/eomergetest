﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {
	public interface ITextFileParser {
		/// <summary>
		/// Parses a text file in the specified path.
		/// </summary>
		/// <param name="path">The path of the text file</param>
		/// <returns>Collection of the parsed data</returns>
		IEnumerable Parse(string path);
	}
}

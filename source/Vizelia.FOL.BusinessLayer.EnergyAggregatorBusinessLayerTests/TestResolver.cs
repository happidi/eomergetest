﻿using System;
using System.Collections.Concurrent;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {
	public class TestResolver :IResolver  {


		 ConcurrentDictionary<Type, object> fakesDictionary = new ConcurrentDictionary<Type, object>();
		
		/// <summary>
		 /// Adds the fake object.
		 /// </summary>
		 /// <param name="type">The type.</param>
		 /// <param name="fakedClass">The faked class.</param>
		public void AddFake(Type type, object fakedClass) {
			fakesDictionary.TryAdd(type, fakedClass);
		}

		private IResolver m_RealResolver = new Resolver();

		/// <summary>
		/// Resolves the given type T to the first concrete implementation of it found using reflection.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public T Resolve<T>() {
			
			object value;
			fakesDictionary.TryGetValue(typeof(T), out value);

			if (value is T)
				return (T)value;

			return m_RealResolver.Resolve<T>();
		}

		/// <summary>
		/// Resolves the requested type to its concrete type which is a class.
		/// </summary>
		/// <param name="abstractType">Type of the abstract.</param>
		/// <returns></returns>
		public Type ResolveToConcreteClassType(Type abstractType) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Resolves the type by the given logical test.
		/// </summary>
		/// <param name="logicalTest">The logical test.</param>
		/// <returns></returns>
		public Type ResolveType(Func<Type, bool> logicalTest) {
			throw new NotImplementedException();
		}
	}
}
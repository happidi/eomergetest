﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using NCalc;
using NUnit.Framework;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests {
	/// <summary>
	/// Test class for Calculated Serie in the Energy Aggregator
	/// </summary>
	public class EnergyAggregatorBusinessLayerCalculatedSerieTests : BaseEnergyAggregatorBusinessLayerTests{
		private const string const_calculated_serie_name = "CalculatedSerie1";
		private const string const_calculated_serie_localid = "CalculatedSerie1";
		private const string const_calculated_serie_key = "1";
		private const string const_meter_name = "Meter Tel Aviv";
		private const string const_classification_title = "kwh";
        private const string const_khw_classification_title = "kwh";
		private const string const_meter_classification_formula = "(" + DefaultLocationName +" / "+ const_meter_name + " - " + const_classification_title + ")";
		private CalculatedSerie m_DefaultCalculatedSerie;
        private const string const_sub_khw_classification_title = "sub_kwh";
        private const string const_kwh_classification_key = "#1";
        private const string const_sub_kwh_classification_key = "#2";
		private const string const_meter_data_file_name =
	"Vizelia.FOL.BusinessLayer.EnergyAggregatorBusinessLayerTests.TestResources.SpecificAnalysisTests.MeterDataJson3Meters.txt";
    
		/// <summary>
		/// Initializes the calculated serie tests.
		/// </summary>
		[SetUp]
		public void InitializeCalculatedSerieTests() {
			m_DefaultCalculatedSerie = new CalculatedSerie {
				KeyCalculatedSerie = const_calculated_serie_key,
				KeyChart = TestChartkey,
				KeyLocalizationCulture = DefaultCulture,
				LocalId = const_calculated_serie_localid,
				Name = const_calculated_serie_name,
				Order = 1
			};
		}


		[TestCase("cos([x])")]
		[TestCase("sin([x]) * 3 + 2 + 34")]
		[TestCase("tan([x]) * 3 + 2")]
		[TestCase("sqrt([x]) * 3 + 2")]
		[TestCase("abs([x]) * 3 + 2")]
		[TestCase("log([x]) * 3 + 2")]
		[TestCase("inverse([x]) * 4 + 2")]
		[TestCase("cos([x]) + sin([x]) + tan([x]) + sqrt([x]) + log([x]) + inverse([x]) + abs([x])")]
		[TestCase("([x])/2")]
		[TestCase("2 * ([x])")]
		public void Chart_CalculatedSerie_Function_SimpleFormulaCalculation(string formula) {
			//Arrange
			m_DefaultCalculatedSerie.Formula = formula.Replace("[x]", const_meter_classification_formula);
			TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { m_DefaultCalculatedSerie };
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			Assert.AreEqual(resultSeries[0].PointsWithValue.Count, resultSeries[1].PointsWithValue.Count);
			Expression expression = new Expression(formula, EvaluateOptions.IgnoreCase);
			expression.EvaluateFunction += CalculationHelper.CalculatedSerieCalculations;
			
			for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++) {
				expression.Parameters["x"] = resultSeries[0].PointsWithValue[i].YValue.Value;
				Assert.AreEqual(expression.Evaluate(), resultSeries[1].PointsWithValue[i].YValue);
			}
		}



		[TestCase("cos([x])")]
		[TestCase("sin([x]) * 3 + 2 + 34")]
		[TestCase("tan([x]) * 3 + 2")]
		[TestCase("sqrt([x]) * 3 + 2")]
		[TestCase("abs([x]) * 3 + 2")]
		[TestCase("log([x]) * 3 + 2")]
		[TestCase("inverse([x]) * 4 + 2")]
		[TestCase("cos([x]) + sin([x]) + tan([x]) + sqrt([x]) + log([x]) + inverse([x]) + abs([x])")]
		[TestCase("([x])/2")]
		[TestCase("2 * ([x])")]
		public void GenerateCalculatedSeries_Function_SimpleFormulaCalculation(string formula) {
			
			//Arrange
			InitializeTest();
			//Create series
			Chart chart = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = chart.Series.ToList();

			//Create calculated series on the 
			m_DefaultCalculatedSerie.Formula = formula.Replace("[x]", const_meter_classification_formula);
			var calculatedSeries = new List<CalculatedSerie> { m_DefaultCalculatedSerie };

			//Act 
			EnergyAggregator.GenerateCalculatedSeries(chart.KeyChart, chart.UseSpatialPath, chart.UseClassificationPath,
				calculatedSeries, resultSeries, chart.Series, null);


			//Assert
			DataSerie calculatedSerie = resultSeries.FirstOrDefault(s => s.Name == const_calculated_serie_name);

			Assert.AreEqual(resultSeries[0].PointsWithValue.Count, calculatedSerie.PointsWithValue.Count);
			Expression expression = new Expression(formula, EvaluateOptions.IgnoreCase);
			expression.EvaluateFunction += CalculationHelper.CalculatedSerieCalculations;

			for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++) {
				expression.Parameters["x"] = resultSeries[0].PointsWithValue[i].YValue.Value;
				Assert.AreEqual(expression.Evaluate(), calculatedSerie.PointsWithValue[i].YValue);
			}
		}


		[TestCase("([x])^2")]
		[TestCase("([x])^0")]
		[TestCase("([x])^1")]
		[TestCase("([x])^-1")]
		public void Chart_CalculateSerie_PowerArithmeticOperator_SerieMultipliedByPower(string formula) {
			//Arrange
			m_DefaultCalculatedSerie.Formula = formula.Replace("[x]", const_meter_classification_formula);
			TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { m_DefaultCalculatedSerie };
			InitializeTest();
			var args = formula.Split('^');
			string pow = string.Format("Pow({0},{1})", args[0], args[1]); //converting ^ to Pow - so we can use NCalc power operator.

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			Assert.AreEqual(resultSeries[0].PointsWithValue.Count, resultSeries[1].PointsWithValue.Count);
			Expression expression = new Expression(pow, EvaluateOptions.IgnoreCase);
			expression.EvaluateFunction += CalculationHelper.CalculatedSerieCalculations;
		
			for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++) {
				expression.Parameters["x"] = resultSeries[0].PointsWithValue[i].YValue.Value;
				Assert.AreEqual(expression.Evaluate(), resultSeries[1].PointsWithValue[i].YValue);
			}
		}

		[TestCase("percentage([x])")]
		[TestCase("delta([x])")]
		[TestCase("ytd([x])")]
		public void Chart_CalculatedSerie_Function_ComplexFormulaCalculation(string formula) {
			//Arrange
			m_DefaultCalculatedSerie.Formula = formula.Replace("[x]", const_meter_classification_formula);
			TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { m_DefaultCalculatedSerie };
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			Assert.AreEqual(resultSeries[0].PointsWithValue.Count, resultSeries[1].PointsWithValue.Count);
			Expression expression = new Expression(formula, EvaluateOptions.IgnoreCase);
			expression.EvaluateFunction += CalculationHelper.CalculatedSerieCalculations;
			expression.Parameters["x"] = resultSeries[0].PointsWithValue.Select(x => x.YValue.Value);
			var evalResult = (IList) expression.Evaluate();
			for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++) {
				//if we encounter precision point problems we can use Math.Round when asserting
				Assert.AreEqual(evalResult[i], resultSeries[1].PointsWithValue[i].YValue);
			}

		}

		[TestCase("higher(([x]) , 146)")]
		[TestCase("lower(([x]) , 146)")]
		public void Chart_CalculatedSerie_OneParameter_ComplexFormulaCalculation(string formula) {
			//Arrange 
			m_DefaultCalculatedSerie.Formula = formula.Replace("[x]", const_meter_classification_formula);
			TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { m_DefaultCalculatedSerie };
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			Expression expression = new Expression(formula, EvaluateOptions.IgnoreCase);
			expression.EvaluateFunction += CalculationHelper.CalculatedSerieCalculations;
			expression.Parameters["x"] = resultSeries[0].PointsWithValue.Select(x => x.YValue.Value);
			var evalResult = (IList)expression.Evaluate();
			Assert.AreEqual(evalResult.Count, resultSeries[1].PointsWithValue.Count);
			for (int i = 0; i < resultSeries[1].PointsWithValue.Count; i++) {
				//if we encounter precision point problems we can use Math.Round when asserting
				Assert.AreEqual(evalResult[i], resultSeries[1].PointsWithValue[i].YValue);
			}
		}

		[TestCase("average([x]) * 3")]
		[TestCase("first([x]) * 3")]
		[TestCase("last([x]) * 3")]
		[TestCase("maximum([x]) * 3")]
		[TestCase("minimum([x]) * 3")]
		[TestCase("sum([x]) * 3")]
		[TestCase("penultimate([x]) * 3")]
		public void Chart_CalculatedSerie_Projection_ComplexFormulaCalculation(string formula) {
			//Arrange
			m_DefaultCalculatedSerie.Formula = formula.Replace("[x]", const_meter_classification_formula);
			TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { m_DefaultCalculatedSerie };
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			Expression expression = new Expression(formula, EvaluateOptions.IgnoreCase);
			expression.EvaluateFunction += CalculationHelper.CalculatedSerieCalculations;
			expression.Parameters["x"] = resultSeries[0].PointsWithValue.Select(x => x.YValue.Value);
			var evalResult = expression.Evaluate();
			Assert.AreEqual(1, resultSeries[1].PointsWithValue.Count);
			Assert.AreEqual(evalResult, resultSeries[1].PointsWithValue[0].YValue);
		}


		[TestCase("([x]) * 3")]
        [Ignore]
		public void Chart_CalculatedSerie_MeterClassification_InvalidFormula(string formula) {
			//Arrange
			m_DefaultCalculatedSerie.Formula = formula.Replace("[x]", const_meter_classification_formula);
			TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { m_DefaultCalculatedSerie };
			TestDataContainer.Chart.Localisation = ChartLocalisation.ByGroupName;
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);

			//Assert
			Assert.Contains(Langue.msg_error_calculatedserie_invalidformula, result.Errors);
		}


		[TestCase("([x])", "([x])", "(cs_first) + (cs_second)")]
		public void Chart_CalculatedSerie_CalculatedSerieOrder_CombinedResult(string firstFormula, string secondFormula, string combinedFormula) {
			//Arrange
			var firstCalculatedSerie = new CalculatedSerie { Name = "cs_first", LocalId = "cs_first", KeyCalculatedSerie = "1", KeyLocalizationCulture = DefaultCulture, KeyChart = TestChartkey, Order = 1, Formula = firstFormula.Replace("[x]", const_meter_classification_formula) };
			var secondCalculatedSerie = new CalculatedSerie { Name = "cs_second", LocalId = "cs_second", KeyCalculatedSerie = "2", KeyLocalizationCulture = DefaultCulture, KeyChart = TestChartkey, Order = 2, Formula = secondFormula.Replace("[x]", const_meter_classification_formula) };
			var combinedCalculatedSerie = new CalculatedSerie { Name = "cs_combined", LocalId = "cs_combined", KeyCalculatedSerie = "3", KeyLocalizationCulture = DefaultCulture, KeyChart = TestChartkey, Order = 3, Formula = combinedFormula};

			TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { firstCalculatedSerie, secondCalculatedSerie, combinedCalculatedSerie };
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			var firstDataSerie = resultSeries.Where(x => x.Name == firstCalculatedSerie.Name).Select(x => x.PointsWithValue);
			var secondDataSerie = resultSeries.Where(x => x.Name == secondCalculatedSerie.Name).Select(x => x.PointsWithValue);
			var combinedDataSerie = resultSeries.Where(x => x.Name == combinedCalculatedSerie.Name).Select(x => x.PointsWithValue);

			for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++) {
				Assert.AreEqual(firstDataSerie.Select(x => x[i].YValue).Single() + secondDataSerie.Select(x => x[i].YValue).Single(), combinedDataSerie.Select(x => x[i].YValue).Single());
			}
		}

		[TestCase("([x])", "(cs_first) + (not_exist)")]
		public void Chart_CalculatedSerie_MultipleCalculatedSerie_OneSerieMissing_IgnoreMissingSerie(string firstFormula, string combinedFormula) {
			//Arrange
			var firstCalculatedSerie = new CalculatedSerie { Name = "cs_first", LocalId = "cs_first", KeyCalculatedSerie = "1", KeyLocalizationCulture = DefaultCulture, KeyChart = TestChartkey, Order = 1, Formula = firstFormula.Replace("[x]", const_meter_classification_formula) };
			var combinedCalculatedSerie = new CalculatedSerie { Name = "cs_combined", LocalId = "cs_combined", KeyCalculatedSerie = "3", KeyLocalizationCulture = DefaultCulture, KeyChart = TestChartkey, Order = 3, Formula = combinedFormula };

			TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { firstCalculatedSerie, combinedCalculatedSerie };
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			var firstDataSerie = resultSeries.Where(x => x.Name == firstCalculatedSerie.Name).Select(x => x.PointsWithValue);
			var combinedDataSerie = resultSeries.Where(x => x.Name == combinedCalculatedSerie.Name).Select(x => x.PointsWithValue);

			for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++) {
				Assert.AreEqual(firstDataSerie.Select(x => x[i].YValue).Single(), combinedDataSerie.Select(x => x[i].YValue).Single());
			}
		}

		[TestCase("([x])", "([x])", "(cs_first) + (cs_second)")]
		public void Chart_CalculatedSerie_CalculatedSerieOrder_InvalidOrder_ChartError(string firstFormula, string secondFormula, string combinedFormula) {
			//Arrange
			var firstCalculatedSerie = new CalculatedSerie { Name = "cs_first", LocalId = "cs_first", KeyCalculatedSerie = "1", KeyLocalizationCulture = DefaultCulture, KeyChart = TestChartkey, Order = 3, Formula = firstFormula.Replace("[x]", const_meter_classification_formula) };
			var secondCalculatedSerie = new CalculatedSerie { Name = "cs_second", LocalId = "cs_second", KeyCalculatedSerie = "2", KeyLocalizationCulture = DefaultCulture, KeyChart = TestChartkey, Order = 2, Formula = secondFormula.Replace("[x]", const_meter_classification_formula) };
			var combinedCalculatedSerie = new CalculatedSerie { Name = "cs_combined", LocalId = "cs_combined", KeyCalculatedSerie = "3", KeyLocalizationCulture = DefaultCulture, KeyChart = TestChartkey, Order = 1, Formula = combinedFormula };

			TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { firstCalculatedSerie, secondCalculatedSerie, combinedCalculatedSerie };
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			Assert.AreEqual(result.Warnings.Count, 1);
			Assert.AreEqual(string.Format(Langue.msg_error_calculatedserie_invalidformula_calcseries, "cs_combined"), result.Warnings[0]);
			Assert.Throws<ArgumentOutOfRangeException>(
				() => { var dataPoint = resultSeries[3]; }); //there is no third serie because of the wrong serie order.

		}

		[TestCase("CsLocalId")]
		public void Chart_CalculatedSerie_CalculatedSerieOrder_CalculateItself_ChartError(string localId) {
			//Arrange
			var firstCalculatedSerie = new CalculatedSerie {
				Name = "cs_first",
				LocalId = localId,
				KeyCalculatedSerie = "1",
				KeyLocalizationCulture = DefaultCulture,
				KeyChart = TestChartkey,
				Order = 1,
				Formula = "("+localId+")"
			};
			TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { firstCalculatedSerie};
			InitializeTest();

			//Act
			var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
			var resultSeries = result.Series.ToList();

			//Assert
			Assert.AreEqual(result.Warnings.Count, 1);
			Assert.AreEqual(string.Format(Langue.msg_error_calculatedserie_invalidformula_calcseries, "cs_first"), result.Warnings[0]);

//pre2.11			Assert.AreEqual(string.Format(Langue.msg_error_calculatedserie_invalidformula, "(" + localId + ")"), result.Errors[0]);
			Assert.Throws<ArgumentOutOfRangeException>(
				() => { var dataPoint = resultSeries[1]; }); //there is no calculated serie
		}

        [Test]
	    public void Chart_CalculatedSerie_ClassificationCustomization_MultiplyAllSeriesInClassification() {
            //Arrange
            const string classification = "Root / METER / METERCLASS_kwh";
            var firstCalculatedSerie = new CalculatedSerie
            {
                Name = "cs_first",
                LocalId = "CsLocalId",
                KeyCalculatedSerie = "1",
                KeyLocalizationCulture = DefaultCulture,
                KeyChart = TestChartkey,
                Order = 1,
                Formula = string.Format("#{0}# * 2", classification),
                ClassificationItemLocalIdPath = "test"
            };
            TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { firstCalculatedSerie };
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            Assert.AreEqual(resultSeries[0].PointsWithValue.Count, resultSeries[1].PointsWithValue.Count);
            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++) {
                Assert.AreEqual(resultSeries[0].PointsWithValue[i].YValue * 2, resultSeries[1].PointsWithValue[i].YValue);    
            }
            
	    }

        [TestCase("%testkey% * ([x])", "3")]
        public void Chart_CalculatedSerie_AddStringFromLanguageManagement_StringIsCalculatedInFormula(string formula, string value) {
            //Arrange
            var englishDictionary = new Dictionary<string, string> {
                {"testkey", value}
            };
            var localizationDictionary = new Dictionary<string, Dictionary<string, string>> { { "en", englishDictionary } };
            TestDataContainer.LocalizationDictionary = localizationDictionary;
            m_DefaultCalculatedSerie.Formula = formula.Replace("[x]", const_meter_classification_formula);
            TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { m_DefaultCalculatedSerie };
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            Assert.AreEqual(resultSeries[0].PointsWithValue.Count, resultSeries[1].PointsWithValue.Count);
            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++)
            {
                Assert.AreEqual(Convert.ToDouble(value) * resultSeries[0].PointsWithValue[i].YValue, resultSeries[1].PointsWithValue[i].YValue);
            }

	    }

        [TestCase("3,4 * ([x])", "fr")]
        public void Chart_CalculatedSerie_Culture_NumberIsChangedAccordingToCulture(string formula, string culture) {
            //Arrange
            var frenchDictionary = new Dictionary<string, string> {};
            m_DefaultCalculatedSerie.KeyLocalizationCulture = culture;
            var localizationDictionary = new Dictionary<string, Dictionary<string, string>> { { culture, frenchDictionary } };
            TestDataContainer.LocalizationDictionary = localizationDictionary;
            m_DefaultCalculatedSerie.Formula = formula.Replace("[x]", const_meter_classification_formula);
            TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { m_DefaultCalculatedSerie };
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            Assert.AreEqual(resultSeries[0].PointsWithValue.Count, resultSeries[1].PointsWithValue.Count);
            for (int i = 0; i < resultSeries[0].PointsWithValue.Count; i++)
            {
                Assert.AreEqual(3.4 * resultSeries[0].PointsWithValue[i].YValue, resultSeries[1].PointsWithValue[i].YValue);
            }
        }

        [TestCase("3 * ([x])")]
        public void Chart_CalculatedSerie_CalculatedSerieWithClassification_CalaculatedSerieIncludedInClassificationCalculation(string formula) {
            //Arrange
            const string classification = "Root / METER / METERCLASS_kwh";
            var secondCalculatedSerie = new CalculatedSerie
            {
                Name = "cs_first",
                LocalId = "CsLocalId",
                KeyCalculatedSerie = "2",
                KeyLocalizationCulture = DefaultCulture,
                KeyChart = TestChartkey,
                Order = 2,
                Formula = string.Format("#{0}# * 2", classification),
                ClassificationItemLocalIdPath = "testSha"
            };

            m_DefaultCalculatedSerie.KeyClassificationItem = const_kwh_classification_key; //default 
            m_DefaultCalculatedSerie.Formula = formula.Replace("[x]", const_meter_classification_formula);
            TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { secondCalculatedSerie, m_DefaultCalculatedSerie };
            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);
            var resultSeries = result.Series.ToList();

            //Assert
            Assert.AreEqual(3, resultSeries.Count); // we expect 3 series - 1 real meter, 1 default calculated serie with classification, second calculated serie with formula on the classification 
	    }

        [Test]
	    public void Chart_CalculatedSerie_ClassificationCalculatedSerieOn2MetersWithSameLocationAndClassification_Error() { // Gilles Rule
            //Arrange
            const string classification = "Root / METER / METERCLASS_kwh";
            var firstCalculatedSerie = new CalculatedSerie
            {
                Name = "cs_first",
                LocalId = "CsLocalId",
                KeyCalculatedSerie = "1",
                KeyLocalizationCulture = DefaultCulture,
                KeyChart = TestChartkey,
                Order = 1,
                Formula = string.Format("#{0}# * 2", classification),
                ClassificationItemLocalIdPath = "test"
            };

            const string secondMeterKey = "#106";
            var secondMeter = new Meter
            {
                KeyMeter = secondMeterKey,
                KeyLocation = DefaultKeyLocation,
                Factor = 1.0,
                IsAcquisitionDateTimeEndInterval = false,
                KeyClassificationItem = const_kwh_classification_key,
                Name = "Test Site Second Meter"
            };

            const string thirdMeterKey = "#111";
            var thirdMeter = new Meter
            {
                KeyMeter = thirdMeterKey,
                KeyLocation = DefaultKeyLocation,
                Factor = 1.0,
                IsAcquisitionDateTimeEndInterval = false,
                KeyClassificationItem = const_kwh_classification_key,
                Name = "Test Site Third Meter"
            };

            TestDataContainer.Meters = new List<Meter>{secondMeter, thirdMeter};

            TestDataContainer.Chart.CalculatedSeries = new List<CalculatedSerie> { firstCalculatedSerie };

            InitializeTest();

            //Act
            var result = EnergyAggregator.Chart_Process(EnergyAggregatorContext, TestChartkey, new List<Filter>(), DefaultCulture);

            //Assert
            var errorString = string.Format(
                Langue.msg_error_calculatedserie_multiple_series_with_same_keylocation_and_same_classification, classification, DefaultLocationName);
            Assert.Contains(errorString, result.Warnings);
	    }
		protected override TestDataContainer InitializeTestDataContainer()
		{
			var testDataContainer =  base.InitializeTestDataContainer();

			testDataContainer.ClassificationItems = new Dictionary<string, ClassificationItem>
				{
					{
						const_kwh_classification_key, new ClassificationItem {KeyClassificationItem = const_kwh_classification_key,Level = 1,LocalId = "METERCLASS_kwh", LongPath = "kwh", Title = const_khw_classification_title, LocalIdPath = "Root / METER / METERCLASS_kwh"}
					},
					{
						const_sub_kwh_classification_key, new ClassificationItem {KeyClassificationItem = const_sub_kwh_classification_key,Level = 2, KeyParent = const_kwh_classification_key, LocalId = "METERCLASS_sub_kwh", LongPath = "kwh/sub_kwh", Title = const_sub_khw_classification_title, LocalIdPath = "Root / METER / METERCLASS_kwh / classification_subKwh"}
					} 
					
				};

			testDataContainer.Meters = new List<Meter>
			{
				new Meter
				{
					KeyMeter = DefaultKeyMeterStr,
					KeyLocation = DefaultKeyLocation,
					Factor = 1.0,
					IsAcquisitionDateTimeEndInterval = false,
					Name = const_meter_name,
					ClassificationItemTitle = const_khw_classification_title,
					KeyClassificationItem = const_kwh_classification_key
				}
			};
				
			return testDataContainer;
		}
         protected override string TestMeterDataFilePath
        {
            get { return const_meter_data_file_name; }
        }
	}
}

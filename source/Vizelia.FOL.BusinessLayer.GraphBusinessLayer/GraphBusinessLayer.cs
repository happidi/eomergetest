﻿using System.Data;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.DataLayer;
using Vizelia.FOL.DataLayer.Interfaces;

namespace Vizelia.FOL.BusinessLayer {

	/// <summary>
	/// Business Layer implementation for Core.
	/// </summary>
	public class GraphBusinessLayer : IGraphBusinessLayer {
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <param name="viewName">Name of the view.</param>
		/// <returns></returns>
		public DataSet GetView(string viewName) {
			IDataAccess db = new DataAccess();
			return db.ExecuteDataSet(CommandType.TableDirect, viewName);
		}
	}

}

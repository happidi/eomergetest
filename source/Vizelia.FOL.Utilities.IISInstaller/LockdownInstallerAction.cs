﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Utilities.CommandLine;

namespace Vizelia.FOL.Utilities.IISInstaller {
	/// <summary>
	/// Command-line tool for encrypting and decrypting frontend website configuration files
	/// </summary>
	internal class LockdownInstallerAction : InstallerAction {
		private bool Protect { get; set; }
		private string Site { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="LockdownInstallerAction"/> class.
		/// </summary>
		public LockdownInstallerAction() {
			var argProtect = new Argument() {
			                                	Syntax = "[true|false]",
			                                	Help = "Encrypts or Decrypts the config.",
			                                	Type = ArgumentType.Mandatory,
			                                	Name = "Protect",
												Map = s => Protect = bool.Parse(s)
			                                };

			var argSite = new Argument {
			                           	Syntax = "<string>",
			                           	Help = "The absolute path to directory that contains the configuration file.",
			                           	Type = ArgumentType.Mandatory,
			                           	Name = "Site",
			                           	Map = s => Site = s,
			                           	Validate = s => {
			                           	           	var isValid = true;
			                           	           	if (!Directory.Exists(s)) {
			                           	           		OutputHelper.WriteToOutput(string.Format("directory doesn't exist: {0}", s),
			                           	           		              MessageType.Error);
			                           	           		isValid = false;
			                           	           	}
			                           	           	else if (!File.Exists(s.TrimEnd('\\') + "\\web.config")) {
			                           	           		OutputHelper.WriteToOutput(string.Format("web.config wasn't found in folder: {0}", s),
			                           	           		              MessageType.Error);
			                           	           		isValid = false;
			                           	           	}

			                           	           	return isValid;
			                           	           }
			                           };

			Arguments.Add(argProtect.Name, argProtect);
			Arguments.Add(argSite.Name, argSite);
		}
		
		/// <summary>
		/// Starts this instance.
		/// </summary>
		protected override void DoAction() {
			var lockdown = new Lockdown();
			lockdown.ProtectConfig(Site, Protect, SilentMode);
		}

		/// <summary>
		/// Gets the action description.
		/// </summary>
		/// <returns></returns>
		protected override string GetActionDescription() {
			return "Lockdown Utility - Performs cryptographic operations on configuration files.";
		}
	}
}
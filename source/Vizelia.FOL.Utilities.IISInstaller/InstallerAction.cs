﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Utilities.CommandLine;

namespace Vizelia.FOL.Utilities.IISInstaller {

	/// <summary>
	/// Install action interface
	/// </summary>
	internal abstract class InstallerAction {
		protected bool SilentMode { get; set; }
		protected Dictionary<string, Argument> Arguments { get; set; } 

		protected InstallerAction() {
			Arguments = new Dictionary<string, Argument>();

			var argSilent = new Argument() {
			                               	Syntax = "[true|false]",
			                               	Help = "Silent mode will only print errors.",
			                               	Type = ArgumentType.Optional,
			                               	Name = "Silent",
			                               	DefaultValue = "false",
			                               	Map = s => {
			                               	      	SilentMode = bool.Parse(s);
			                               	      	OutputHelper.SilentMode = SilentMode;
			                               	      }
			                               };
			
			Arguments.Add(argSilent.Name, argSilent);
		}

		/// <summary>
		/// Shows the usage.
		/// </summary>
		public void ShowUsage() {
			OutputHelper.WriteToOutput(GetHeader());
			OutputHelper.WriteToOutput(GetActionDescription());
			OutputHelper.WriteToOutput(string.Empty);
			
			var orderedUsage = new List<ArgumentHelpStrings>();
			Arguments.Values.ForEach(x => {
			                         	if (x.Type == ArgumentType.Mandatory)
			                         		orderedUsage.Add(new ArgumentHelpStrings() {
			                         		                                           	Help = x.Help.Substring(x.Help.IndexOf(")") + 2),
			                         		                                           	Syntax = x.Syntax,
			                         		                                           	Type = x.Type
			                         		                                           });
			                         });
			Arguments.Values.ForEach(x => {
				if (x.Type == ArgumentType.Optional)
					orderedUsage.Add(new ArgumentHelpStrings() {
					                                           	Help = x.Help.Substring(x.Help.IndexOf(")") + 2),
					                                           	Syntax = x.Syntax,
					                                           	Type = x.Type
					                                           });
			                         });
			
			OutputHelper.WriteToOutput(UsageHelper.GetUsageString(orderedUsage));
		}

		/// <summary>
		/// Setups the arguments.
		/// </summary>
		/// <param name="args">The args.</param>
		protected virtual void SetupArguments(string[] args) {
			var commandLine = new Arguments(args);

			if (!CheckMandatoryArguments(commandLine)) {
				UsageHelper.Abort("Mandatory arguments are missing.");
			}

			// TODO: rename to map properties 
			if (!ConfigureProperties(commandLine)) {
				UsageHelper.Abort("Operation could not configure arguments.");
			}

			if (!ValidateArguments(commandLine)) {
				UsageHelper.Abort("Operation could not validate arguments.");
			}

			if (!MapArguments(commandLine)) {
				UsageHelper.Abort("Operation could not map arguments.");
			}


			try {
				CalculateProperties();
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput(ex.Message, MessageType.Error);
				UsageHelper.Abort("Operation could not calculate parameters.");
			}

			try {
				ValidateArgumentsGlobally();
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput(ex.Message, MessageType.Error);
				UsageHelper.Abort("Operation failed at validation.");
			}
		}

		/// <summary>
		/// Maps the arguments.
		/// </summary>
		/// <param name="commandLine">The command line.</param>
		/// <returns></returns>
		private bool MapArguments(Arguments commandLine) {
			var isArgumentsConfigured = true;
			Arguments.Values.ForEach(argument => {
				try {
					if (argument.Map != null) {
						argument.Map(argument.Value);
					}
					else {
						OutputHelper.WriteToOutput("mapping is missing for " + argument.Name + " argument", MessageType.Warning);
					}
				}
				catch (Exception ex) {
					isArgumentsConfigured = false;
					OutputHelper.WriteToOutput(argument.InvalidValueString, MessageType.Error);
					OutputHelper.WriteToOutput(ex.Message, MessageType.Error);
				}
			});

			return isArgumentsConfigured;
		}

		/// <summary>
		/// Validates the arguments globally.
		/// </summary>
		protected virtual void ValidateArgumentsGlobally() {}

		/// <summary>
		/// Checks the mandatory arguments.
		/// </summary>
		/// <param name="commandLine">The command line.</param>
		protected virtual bool CheckMandatoryArguments(Arguments commandLine) {
			bool isArgumentValid = true;

			Arguments.Values.ForEach(argument => {
			                  	if (argument.Type == ArgumentType.Mandatory && string.IsNullOrEmpty(commandLine[argument.Name])) {
			                  		OutputHelper.WriteToOutput(argument.MissingValue, MessageType.Error);
			                  		isArgumentValid = false;
			                  	}
			                  });
			
			return isArgumentValid;
		}

		/// <summary>
		/// Configures the properties.
		/// </summary>
		/// <param name="commandLine">The command line.</param>
		protected virtual bool ConfigureProperties(Arguments commandLine) {
			var isArgumentsConfigured = true;
			Arguments.Values.ForEach(argument => {
			                         	try {
			                         		if (argument.Configure != null) {
												argument.Configure(commandLine[argument.Name]);	
			                         		}
			                         		else {
			                         			argument.Value = commandLine[argument.Name];
			                         		}
			                         	}
			                         	catch (Exception ex) {
			                         		isArgumentsConfigured = false;
											OutputHelper.WriteToOutput(argument.InvalidValueString, MessageType.Error);
			                         		OutputHelper.WriteToOutput(ex.Message, MessageType.Error);
			                         	}
			                         });

			return isArgumentsConfigured;
		}

		/// <summary>
		/// Configures the properties.
		/// </summary>
		/// <param name="commandLine">The command line.</param>
		protected virtual bool ValidateArguments(Arguments commandLine) {
			var isArgumentValid = true;
			Arguments.Values.ForEach(argument => {
			                         	try {
			                         		if (argument.Validate != null && !argument.Validate(commandLine[argument.Name])) {
												isArgumentValid = false;
												OutputHelper.WriteToOutput("Error in " + argument.Name + " argument validation", MessageType.Error);
			                         		}
			                         	}
			                         	catch (Exception ex) {
			                         		isArgumentValid = false;
			                         		OutputHelper.WriteToOutput(ex.Message, MessageType.Error);
			                         	}
			});

			return isArgumentValid;
		}
		
		/// <summary>
		/// Calculates the properties.
		/// </summary>
		protected virtual void CalculateProperties() {}

		/// <summary>
		/// Gets the action description.
		/// </summary>
		/// <returns></returns>
		protected abstract string GetActionDescription();

		/// <summary>
		/// Gets the header usage string.
		/// </summary>
		/// <returns></returns>
		protected string GetHeader() {
			return string.Format("Command-line tool for deploying Energy Operation {0} to IIS.\n" +
				"Copyright (c) Vizelia - Schneider Electric. All rights reserved.\n",
			                     Assembly.GetExecutingAssembly().GetName().Version);
		}
		
		/// <summary>
		/// Does the action.
		/// </summary>
		protected abstract void DoAction();
		
		/// <summary>
		/// Does the action.
		/// </summary>
		/// <param name="args">The args.</param>
		public void DoAction(string[] args) {
			if (!ParseArguments(args)) return;

			try {
				DoAction();
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput(ex.Message, MessageType.Error);
			}
		}

		/// <summary>
		/// Parses the arguments.
		/// </summary>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		protected bool ParseArguments(string[] args) {
			if ((args.Length == 0) || (args[0].Contains("?"))) {
				ShowUsage();
				return false;
			}
			
			SetupArguments(args);
			return true;
		}
	}
}

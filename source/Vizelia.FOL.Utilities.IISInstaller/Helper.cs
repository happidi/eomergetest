﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Vizelia.FOL.Common;
using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Utilities.CommandLine;

namespace Vizelia.FOL.Utilities.IISInstaller {
	/// <summary>
	/// Help class for the installer
	/// </summary>
	public static class Helper {
		/// <summary>
		/// Validates the password strength.
		/// </summary>
		/// <param name="password">The password.</param>
		/// <returns></returns>
		public static bool ValidatePasswordStrength(string password) {
			if (!((password.Length >= 6) && (password.Count(ch => !Char.IsLetterOrDigit(ch)) >= 2))) {
				OutputHelper.WriteToOutput("Password should contain at least 6 chars, 2 of them non-alphanumeric.", MessageType.Error);
				return false;
			}

			return true;
		}

		/// <summary>
		/// Executes the command.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="silent">if set to <c>true</c> [silent].</param>
		/// <param name="shouldFlushOutputToConsole">if set to <c>true</c> [should flush output to console].</param>
		/// <returns></returns>
		public static ExecutionResult ExecuteCommand(string command, bool silent = false, bool shouldFlushOutputToConsole = false) {
			var result = CmdHelper.ExecuteCmd(command, shouldFlushOutputToConsole);
			if (!string.IsNullOrEmpty(result.Error) && !silent) OutputHelper.WriteToOutput(result.Error, MessageType.Error, false);

			return result;
		}

		public static void DoAction(Action action, string errorMsg, bool throwException = true, bool exitApplication = false) {
			try {
				action();
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput(errorMsg, MessageType.Error);
				OutputHelper.WriteToOutput(ex.Message, MessageType.Error);

				if (exitApplication) {
					UsageHelper.Abort("Aborting action.");
				}

				if (throwException) {
					throw new VizeliaException(errorMsg);
				}
			}
		}

		/// <summary>
		/// Configures the name of the host.
		/// </summary>
		/// <param name="address">The address.</param>
		/// <param name="hostname">The hostname.</param>
		public static void ConfigureHostName(string address, string hostname) {
			string windowsPath = Environment.GetEnvironmentVariable("windir");
			string path = windowsPath + @"\system32\drivers\etc\hosts";
			StreamReader sr = new StreamReader(path);
			string file_string = sr.ReadToEnd();
			sr.Close();
			var oldLines = file_string.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			var newLines = oldLines.Where(line => !(line.IndexOf(hostname, StringComparison.OrdinalIgnoreCase) >= 0 && !line.StartsWith("#"))).ToList();
			newLines.Add(address + "\t" + hostname);
			StreamWriter sw = new StreamWriter(path);
			foreach (var newLine in newLines) {
				sw.WriteLine(newLine);
			}
			sw.Close();
			OutputHelper.WriteToOutput(string.Format("...create host name : {0} {1}", address, hostname));
		}

		/// <summary>
		/// Checks the SSL certificate.
		/// </summary>
		public static void CheckSSLCertificate(string SSLCertificate, string SSLCertificatePassword) {
			try {
				var certificate = new X509Certificate2(SSLCertificate, SSLCertificatePassword);
			}
			catch (Exception) {
				OutputHelper.WriteToOutput(string.Format("Could not find the SSL certificate or SSL password is incorrect : {0}", SSLCertificate), MessageType.Error);
				UsageHelper.Abort("Installation aborted.");
			}
		}

		public static void TrustHttps() {
			System.Net.ServicePointManager.ServerCertificateValidationCallback =
				((sender, certificate, chain, sslPolicyErrors) => true);
		}
	}

	/// <summary>
	/// Class that handles buffered output messages
	/// </summary>
	public static class OutputHelper {
		/// <summary>
		/// Gets or sets the output messages.
		/// </summary>
		/// <value>
		/// The output messages.
		/// </value>
		public static List<OutputMessage> OutputMessages { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether should write in silent mode.
		/// </summary>
		/// <value>
		///   <c>true</c> if [silent mode]; otherwise, <c>false</c>.
		/// </value>
		public static bool SilentMode { get; set; }

		/// <summary>
		/// Initializes the <see cref="OutputHelper"/> class.
		/// </summary>
		static OutputHelper() {
			OutputMessages = new List<OutputMessage>();
		}

		/// <summary>
		/// Writes to console.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="type">The type.</param>
		/// <param name="newLine">if set to <c>true</c> [new line].</param>
		public static void WriteToOutput(string input, MessageType type = MessageType.Message,
			bool newLine = true, bool? silentMode = null) {
			var message = new OutputMessage { Message = input, MessageType = type, NewLine = newLine };
			OutputMessages.Add(message);
			CmdHelper.WriteToConsole(message, silentMode ?? SilentMode);
		}
	}
}

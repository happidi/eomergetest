﻿using System;
using System.Diagnostics;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Threading;
using Microsoft.Web.Administration;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Utilities.CommandLine;
using Vizelia.FOL.Utilities.WCFClient.CookieManager;
using Vizelia.FOL.WCFService.Contracts;
using System.Linq;

namespace Vizelia.FOL.Utilities.IISInstaller {
	internal class NewTenantInstallerAction : InstallerAction {

		protected string SSLCertificate { get; set; }
		protected string SSLCertificatePassword { get; set; }
		private string Url { get; set; }
		private string MainTenantUrl { get; set; }
		private string MainTenantName { get; set; }
		private string MainTenantAdminPassword { get; set; }
		private string AdminPassword { get; set; }
		private string TenantName { get; set; }
		protected string AdminTenantEmail { get; set; }
		protected bool SSLOnly { get; set; }

		public NewTenantInstallerAction() {
			var argMainTenantName = new Argument {
			                                     	Syntax = "<Main tenant name>",
			                                     	Help = "Main tenant name.",
			                                     	Type = ArgumentType.Mandatory,
													Name = "MainTenantName",
													Map = s => MainTenantName = s
			                                     };

			var argMainTenantAdminPassword = new Argument {
			                                              	Syntax = "<string>",
			                                              	Help = "Password for main tenant admin account.",
			                                              	Type = ArgumentType.Mandatory,
			                                              	Name = "MainTenantAdminPassword",
			                                              	Map = s => MainTenantAdminPassword = s
			                                              };
			var argTenantName = new Argument {
			                                 	Syntax = "<string>",
			                                 	Help = "Name of the tenant.",
			                                 	Type = ArgumentType.Mandatory,
			                                 	Name = "TenantName",
			                                 	Map = s => TenantName = s
			                                 };
			var argAdminPassword = new Argument {
			                                    	Syntax = "<password>",
			                                    	Help =
			                                    		"New tenant admin password. Use at least 6 chars, 2 of them non-alphanumeric.",
			                                    	Type = ArgumentType.Mandatory,
			                                    	Name = "AdminPassword",
			                                    	Map = s => AdminPassword = s,
			                                    	Validate = Helper.ValidatePasswordStrength
			                                    };
			var argUrl = new Argument {
			                          	Syntax = "<Url>",
			                          	Help = "Url of the new tenant. Without http prefix.",
			                          	Type = ArgumentType.Mandatory,
			                          	Name = "Url",
			                          	Map = s => Url = s
			                          };
			var argAdminTenantEmail = new Argument {
			                                       	Syntax = "<email>",
			                                       	Help = "New tenant admin email.",
			                                       	Type = ArgumentType.Optional,
			                                       	Name = "AdminTenantEmail",
			                                       	Map = s => AdminTenantEmail = s,
													DefaultValue = "support.teamfoundation@vizelia.com"
			                                       };
			var argMainTenantUrl = new Argument {
				Syntax = "<Url>",
				Help = "Url of the main tenant.",
				Type = ArgumentType.Mandatory,
				Name = "MainTenantUrl",
				Map = s => MainTenantUrl = s,
				DefaultValue = "FacilityOnLine"
			};
			var argSSLCertificate = new Argument {
				Syntax = "<string>",
				Help = "The path to the ssl certificate (.pfx).",
				Type = ArgumentType.Optional,
				Name = "SSLCertificate",
				Map = s => SSLCertificate = s,
				DefaultValue = @".\Support Files\VizeliaLocalDevelopmentCertificate.pfx"
			};
			var argSSLCertificatePassword = new Argument {
				Syntax = "<string>",
				Help = "The password of the ssl certificate.",
				Type = ArgumentType.Optional,
				Name = "SSLCertificatePassword",
				Map = s => SSLCertificatePassword = s,
				DefaultValue = @"P@ssw0rd",
				HideDefaultValue = true
			};
			var argSSLOnly = new Argument {
				Syntax = "[true|false]",
                Help = "Ensures all requests go through https. Requests to http are automatically redirected to https.",
                Type = ArgumentType.Optional,
				Name = "SSLOnly",
				Map = s => SSLOnly = bool.Parse(s),
				DefaultValue = "false"
			};

			Arguments.Add(argSSLOnly.Name, argSSLOnly);
			Arguments.Add(argSSLCertificate.Name, argSSLCertificate);
			Arguments.Add(argSSLCertificatePassword.Name, argSSLCertificatePassword);
			Arguments.Add(argMainTenantUrl.Name, argMainTenantUrl);
			Arguments.Add(argAdminPassword.Name, argAdminPassword);
			Arguments.Add(argAdminTenantEmail.Name, argAdminTenantEmail);
			Arguments.Add(argMainTenantAdminPassword.Name, argMainTenantAdminPassword);
			Arguments.Add(argMainTenantName.Name, argMainTenantName);
			Arguments.Add(argTenantName.Name, argTenantName);
			Arguments.Add(argUrl.Name, argUrl);
		}

		/// <summary>
		/// Validates the arguments globally.
		/// </summary>
		protected override void ValidateArgumentsGlobally() {
			base.ValidateArgumentsGlobally();

			Helper.CheckSSLCertificate(SSLCertificate, SSLCertificatePassword);
		}

		/// <summary>
		/// Configures the tenant data.
		/// </summary>
		protected override void DoAction() {
			// Trust all certificates
			Helper.TrustHttps();

			var cookieManager = CookieManagerEndpointBehavior.CreateNew();
			var publicFactory = new ChannelFactory<IPublicWCF>(new BasicHttpBinding(), new EndpointAddress(new Uri(string.Format("http://{0}/public.svc/soap", MainTenantUrl.TrimEnd('/')))));
			var tenancyFactory = new ChannelFactory<ITenancyWCF>(new BasicHttpBinding(), new EndpointAddress(new Uri(string.Format("http://{0}/tenancy.svc/soap", MainTenantUrl.TrimEnd('/')))));
			var coreFactory = new ChannelFactory<ICoreWCF>(new BasicHttpBinding(), new EndpointAddress(new Uri(string.Format("http://{0}/core.svc/soap", MainTenantUrl.TrimEnd('/')))));
			var publicFactoryHttps = new ChannelFactory<IPublicWCF>(new BasicHttpBinding(BasicHttpSecurityMode.Transport), new EndpointAddress(new Uri(string.Format("https://{0}/public.svc/soap", MainTenantUrl.TrimEnd('/')))));
			var tenancyFactoryHttps = new ChannelFactory<ITenancyWCF>(new BasicHttpBinding(BasicHttpSecurityMode.Transport), new EndpointAddress(new Uri(string.Format("https://{0}/tenancy.svc/soap", MainTenantUrl.TrimEnd('/')))));
			var coreFactoryHttps = new ChannelFactory<ICoreWCF>(new BasicHttpBinding(BasicHttpSecurityMode.Transport), new EndpointAddress(new Uri(string.Format("https://{0}/core.svc/soap", MainTenantUrl.TrimEnd('/')))));
			publicFactory.Endpoint.Behaviors.Add(cookieManager);
			tenancyFactory.Endpoint.Behaviors.Add(cookieManager);
			coreFactory.Endpoint.Behaviors.Add(cookieManager);
			publicFactoryHttps.Endpoint.Behaviors.Add(cookieManager);
			tenancyFactoryHttps.Endpoint.Behaviors.Add(cookieManager);
			coreFactoryHttps.Endpoint.Behaviors.Add(cookieManager);
			
			var clientPublic = publicFactory.CreateChannel();
			var clientTenancy = tenancyFactory.CreateChannel();
			var clientCore = coreFactory.CreateChannel();
			var clientPublicHttps = publicFactoryHttps.CreateChannel();
			var clientTenancyHttps = tenancyFactoryHttps.CreateChannel();
			var clientCoreHttps = coreFactoryHttps.CreateChannel();
			
			using (publicFactory)
			using (tenancyFactory)
			using (coreFactory)
			using (publicFactoryHttps)
			using (tenancyFactoryHttps)
			using (coreFactoryHttps) {
				if (Url != MainTenantUrl) {
					ConfigureSharedTenantInIIS();
				}

				Login(clientPublic, clientPublicHttps);

				var longOperationId = CreateNewTenant(clientTenancyHttps, clientTenancy);

				// Sleep for 15 seconds for status to change to InProgress
				Thread.Sleep(15000);

				CheckNewTenantCreationStatus(clientCoreHttps, clientCore, longOperationId);
			}
		}

		/// <summary>
		/// Configures the shared tenant in IIS.
		/// </summary>
		private void ConfigureSharedTenantInIIS() {
			//Debugger.Launch();
			using (var serverManager = new ServerManager()) {
				bool isServerDirty = false;
				Microsoft.Web.Administration.Site site = serverManager.Sites[MainTenantUrl] ??
				                                         serverManager.Sites.First(x => x.Bindings.Any(y => y.Host == MainTenantUrl));
				var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
				store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadWrite);
				var certificate = new X509Certificate2(SSLCertificate, SSLCertificatePassword, X509KeyStorageFlags.PersistKeySet);
				if (!store.Certificates.Contains(certificate))
					store.Add(certificate);
				try {
					Binding binding = site.Bindings.Add("*:443:" + Url, certificate.GetCertHash(), store.Name);
					binding.Protocol = "https";
					isServerDirty = true;
				}
				catch (Exception ex) {
					if (ex.Message.Contains("Cannot add duplicate collection entry of type 'binding'")) {
						OutputHelper.WriteToOutput("...https binding already exists for the given Url");
					}
					else {
						throw;
					}
				}
				finally {
					store.Close();
				}
					
//!user story 3591				if (!SSLOnly) {
					try {
						site.Bindings.Add("*:80:" + Url, "http");
						isServerDirty = true;
					}
					catch (Exception ex) {
						if (ex.Message.Contains("Cannot add duplicate collection entry of type 'binding'")) {
							OutputHelper.WriteToOutput("...http binding already exists for the given Url");
						}
						else {
							throw;
						}
					}
//				}

				if (isServerDirty) {
					serverManager.CommitChanges();
				}
			}

			OutputHelper.WriteToOutput("...new tenant site binding in IIS passed");

			Helper.ConfigureHostName("127.0.0.1", Url);
		}

		/// <summary>
		/// Checks the new tenant creation status.
		/// </summary>
		/// <param name="clientCoreHttps">The client core HTTPS.</param>
		/// <param name="clientCore">The client core.</param>
		/// <param name="longOperationId">The long operation id.</param>
		private void CheckNewTenantCreationStatus(ICoreWCF clientCoreHttps, ICoreWCF clientCore, Guid longOperationId) {
			var useHttps = false;
			LongRunningOperationState state;
			try {
				state = clientCore.GetLongRunningOperationProgress(longOperationId);
			}
			catch {
				state = clientCoreHttps.GetLongRunningOperationProgress(longOperationId);
				useHttps = true;
			}

			while (state.Status != LongRunningOperationStatus.ResultAvailable &&
			       state.Status != LongRunningOperationStatus.Exception &&
				   state.Status != LongRunningOperationStatus.Initialized) {
				OutputHelper.WriteToOutput(".", newLine: false);
				state = useHttps
				        	? clientCoreHttps.GetLongRunningOperationProgress(longOperationId)
				        	: clientCore.GetLongRunningOperationProgress(longOperationId);
				Thread.Sleep(1000);
			}

			OutputHelper.WriteToOutput(string.Empty);

			if (state.Status == LongRunningOperationStatus.ResultAvailable) {
				var entity = (FormResponse) (useHttps
				                             	? clientCoreHttps.GetLongRunningOperationResultEntity(longOperationId)
				                             	: clientCore.GetLongRunningOperationResultEntity(longOperationId));
				if (entity.success) {
					OutputHelper.WriteToOutput("New tenant was successfuly initialized.", MessageType.Success);
				}
				else {
					OutputHelper.WriteToOutput(string.Format("Error in new tenant initialization: {0}", entity.msg), MessageType.Error);
				}
			}
			else if (state.Status == LongRunningOperationStatus.Initialized) {
				OutputHelper.WriteToOutput("New tenant creation was not started, possibly due to the lack of permissions.", MessageType.Error);
			}
			else {
				OutputHelper.WriteToOutput(string.Format("Error in new tenant initialization last status was: {0}", state.Status),
				              MessageType.Error);
			}
		}

		/// <summary>
		/// Creates the new tenant.
		/// </summary>
		/// <param name="clientTenancyHttps">The client tenancy HTTPS.</param>
		/// <param name="clientTenancy">The client tenancy.</param>
		/// <returns></returns>
		private Guid CreateNewTenant(ITenancyWCF clientTenancyHttps, ITenancyWCF clientTenancy) {
			var longOperationId = Guid.NewGuid();
			var ten = new Tenant {
			                       	AdminEmail = AdminTenantEmail,
			                       	AdminPassword = AdminPassword,
			                       	Url = Url,
			                       	IsIsolated = false,
			                       	Name = TenantName
			                       };

			try {
				clientTenancy.Tenant_FormCreate(ten, longOperationId);
			}
			catch {
				clientTenancyHttps.Tenant_FormCreate(ten, longOperationId);
			}

			OutputHelper.WriteToOutput("Operation started...", newLine: false);
			return longOperationId;
		}

		/// <summary>
		/// Logins the specified client public.
		/// </summary>
		/// <param name="clientPublic">The client public.</param>
		/// <param name="clientPublicHttps">The client public HTTPS.</param>
		private void Login(IPublicWCF clientPublic, IPublicWCF clientPublicHttps) {
			FormResponse response;

			try {
				response = clientPublic.Login_Response(string.Format("{0}\\Admin", MainTenantName), MainTenantAdminPassword, true,
				                                       null);
			}
			catch {
				response = clientPublicHttps.Login_Response(string.Format("{0}\\Admin", MainTenantName),
				                                            MainTenantAdminPassword, true, null);
			}

			if (!response.success) {
				OutputHelper.WriteToOutput(response.msg, MessageType.Error);
				if (response.errors != null) {
					foreach (var formError in response.errors) {
						OutputHelper.WriteToOutput(formError.msg, MessageType.Error);
					}
				}
				UsageHelper.Abort("Login error.");
			}
			else {
				OutputHelper.WriteToOutput("Login successful...");
			}
		}

		/// <summary>
		/// Gets the action description.
		/// </summary>
		/// <returns></returns>
		protected override string GetActionDescription() {
			return "Create New Tenant Utility - Creates a new shared tenant in the application.";
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Vizelia.FOL.Infrastructure;

namespace Vizelia.FOL.Utilities.IISInstaller {
	internal class Lockdown {
		private bool SilentMode { get; set; }
		private bool Protect { get; set; }
		private int SectionsEncrypted { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Lockdown"/> class.
		/// </summary>
		public Lockdown() {
			SectionsEncrypted = 0;
		}

		/// <summary>
		/// Protects the config.
		/// </summary>
		/// <param name="targetDirectory">The target directory.</param>
		/// <param name="shouldProtect">if set to <c>true</c> [should protect].</param>
		/// <param name="silentMode">if set to <c>true</c> [silent mode].</param>
		/// <returns></returns>
		public void ProtectConfig(string targetDirectory, bool shouldProtect, bool silentMode) {
			Protect = shouldProtect;
			SilentMode = silentMode;

			OutputHelper.WriteToOutput(shouldProtect ? "Protect mode" : "Unprotect mode");

			var configuration = ConfigurationHelper.GetWebConfiguration(targetDirectory);

			EncryptDecryptConfig(configuration);

			OutputHelper.WriteToOutput(string.Format("{0}Performed cryptographic operation on {1} sections", Environment.NewLine,
										SectionsEncrypted));
		}

		/// <summary>
		/// Encrypts the decrypt config.
		/// </summary>
		/// <param name="configuration">The configuration.</param>
		private void EncryptDecryptConfig(Configuration configuration) {
			SectionsEncrypted = 0;

			EncryptDecryptSections(configuration.Sections);
			EncryptDecryptGroups(configuration.SectionGroups);

			configuration.Save();
		}

		private void EncryptDecryptSections(ConfigurationSectionCollection sectionCollection) {
			foreach (var section in sectionCollection) {
				EncryptDecrypt((ConfigurationSection)section);
			}
		}

		private void EncryptDecryptGroups(ConfigurationSectionGroupCollection groupCollection) {
			foreach (var item in groupCollection) {
				var groupItem = (ConfigurationSectionGroup)item;
				EncryptDecryptSections(groupItem.Sections);
				EncryptDecryptGroups(groupItem.SectionGroups);
			}
		}

		/// <summary>
		/// Encrypts the decrypt.
		/// </summary>
		/// <param name="configSection">The config section.</param>
		private void EncryptDecrypt(ConfigurationSection configSection) {
			try {
				// Don't use inherited configuration sections
				if (configSection.ElementInformation.IsPresent) {
					OutputHelper.WriteToOutput(configSection.SectionInformation.SectionName + ": ", newLine: false);

					if (Protect) {
						if (!configSection.SectionInformation.IsProtected) {
							configSection.SectionInformation.ProtectSection("DataProtectionConfigurationProvider");
							OutputHelper.WriteToOutput("protected", MessageType.Success);
							SectionsEncrypted++;
						}
						else {
							OutputHelper.WriteToOutput("already protected", MessageType.Warning);
						}
					}
					else {
						if (configSection.SectionInformation.IsProtected) {
							configSection.SectionInformation.UnprotectSection();
							OutputHelper.WriteToOutput("unprotected", MessageType.Success);
							SectionsEncrypted++;
						}
						else {
							OutputHelper.WriteToOutput("already unprotected", MessageType.Warning);
						}
					}
				}
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput(ex.Message, MessageType.Error);
			}
		}
	}
}

﻿using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Utilities.CommandLine;

namespace Vizelia.FOL.Utilities.IISInstaller {
	/// <summary>
	/// Main router to install actions
	/// </summary>
	internal class MainInstallerAction : InstallerAction {
		private string[] Args { get; set; }
		private InstallerAction Action { get; set; }
		private bool ShouldShowUsage { get; set; }
		private const string ActionSyntax = "[Install|Update|Lockdown|NewTenant]";

		public MainInstallerAction() {
			var argAction = new Argument {
			                             	Syntax = ActionSyntax,
			                             	Help = "Action to perform, use /Help:<action> for action usage.",
			                             	Type = ArgumentType.Mandatory,
			                             	Name = "Action",
											HideDefaultValue = true
			                             };
			var argHelp = new Argument {
			                       	Syntax = ActionSyntax,
			                       	Help = "Help on different installer actions.",
			                       	Type = ArgumentType.Optional,
									Name = "Help",
									DefaultValue = string.Empty,
									HideDefaultValue = true
			                       };

			Arguments.Clear();
			Arguments.Add(argAction.Name, argAction);
			Arguments.Add(argHelp.Name, argHelp);
		}

		/// <summary>
		/// Does the action.
		/// </summary>
		protected override void DoAction() {
			if (Action == null) {
				ShowUsage();
			}
			else {
				if (ShouldShowUsage) {
					Action.ShowUsage();
				}
				else {
					Action.DoAction(Args);
				}
			}
		}

		/// <summary>
		/// Setups the arguments.
		/// </summary>
		/// <param name="args">The args.</param>
		protected override void SetupArguments(string[] args) {
			Args = args;
			var commandLine = new Arguments(args);
			
			ConfigureProperties(commandLine);
		}
		
		/// <summary>
		/// Configures the properties.
		/// </summary>
		/// <param name="commandLine">The command line.</param>
		protected override bool ConfigureProperties(Arguments commandLine) {
			// If help is found in parameters present a show usage
			if (commandLine["Help"] != null) {
				SetAction(commandLine["Help"]);
				ShouldShowUsage = true;
				return true;
			}

			// Set action or show usage if wrong action is given
			if (commandLine["Action"] != null) {
				SetAction(commandLine["Action"]);
			}
			else {
				OutputHelper.WriteToOutput("Argument Action not supplied", MessageType.Error);
				ShouldShowUsage = true;
			}

			return true;
		}

		private void SetAction(string actionName) {
			actionName = actionName.ToLower();
			switch (actionName) {
				case "install": Action = new InstallApplicationInstallerAction(); break;
				case "update": Action = new UpdateApplicationInstallerAction(); break;
				case "lockdown": Action = new LockdownInstallerAction(); break;
				case "newtenant": Action = new NewTenantInstallerAction(); break;
			}
		}

		protected override string GetActionDescription() {
			return "Installer Utility - Performs different utility operations.";
		}
	}
}

﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using Ionic.Zlib;
using Microsoft.Web.Administration;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Utilities.CommandLine;

namespace Vizelia.FOL.Utilities.IISInstaller {
	/// <summary>
	/// Action copies files for the new version and configures the web config
	/// </summary>
	internal class UpdateApplicationInstallerAction : ApplicationInstallerAction {
		protected string FrontEndPath { get; set; }
		protected bool UpdateAzman { get; set; }
		protected bool Backup { get; set; }

		public UpdateApplicationInstallerAction() {
			var argSiteDirectory = new Argument {
				Syntax = "<string>",
				Help = "Site directory.",
				Type = ArgumentType.Mandatory,
				Name = "SiteDirectory",
				Map = s => FrontEndPath = s
			};
			argSiteDirectory.Validate = s => CheckDirectory(argSiteDirectory.Value);

			var argUpdateAzman = new Argument {
				Syntax = "[true|false]",
				Help = "Should update azman.",
				Type = ArgumentType.Optional,
				Name = "UpdateAzman",
				Map = s => UpdateAzman = bool.Parse(s),
				DefaultValue = "false"
			};

			var argBackup = new Argument {
				Syntax = "[true|false]",
				Help = "Should backup files.",
				Type = ArgumentType.Optional,
				Name = "Backup",
				Map = s => Backup = bool.Parse(s),
				DefaultValue = "true"
			};

			var argVizeliaLicenceFile = new Argument {
				Syntax = "<string>",
				Help = "The path to vizelia licence file.",
				Type = ArgumentType.Optional,
				Name = "VizeliaLicenceFile",
				Map = s => VizeliaLicenceFile = s,
				HideDefaultValue = true
			};
			argVizeliaLicenceFile.Validate = s => {
				if (argVizeliaLicenceFile.Value != null) {
					return CheckFile(argVizeliaLicenceFile.Value);
				}
				return true;
			};

			Arguments.Add(argSiteDirectory.Name, argSiteDirectory);
			Arguments.Add(argUpdateAzman.Name, argUpdateAzman);
			Arguments.Add(argBackup.Name, argBackup);
			Arguments.Add(argVizeliaLicenceFile.Name, argVizeliaLicenceFile);
		}

		/// <summary>
		/// Calculates the properties.
		/// </summary>
		protected override void CalculateProperties() {
			SetUrlFromIIS(FrontEndPath);
			FrontEndUrl = Url;
			TargetDirectory = Directory.GetParent(FrontEndPath).FullName;

			// For old versions that have the IIS7Isolated 
			IsolationProvider = "SharedTenantProvisioningProvider";
			//IsolationProvider = ((GenericProviderSection)ConfigurationHelper.GetWebConfiguration(FrontEndPath).Sections["VizeliaTenantProvisioningSection"]).DefaultProvider;


			string SchedulerPath = Path.Combine(new DirectoryInfo(FrontEndPath).Parent.FullName, "Scheduler");
			string configFile = Path.Combine(FrontEndPath, "web.config");
			string configFileScheduler = Path.Combine(SchedulerPath, "web.config");
			var doc = new XmlDocument();
			doc.Load(configFile);
			var docScheduler = new XmlDocument();
			docScheduler.Load(configFileScheduler);

			string connectionString = null;

			string SchedulerUsernameScheduler = null;
			string SchedulerPasswordScheduler = null;
			// try opening using WebConfiguration 
			try {
				Helper.DoAction(() => {
					Debug = bool.Parse(ConfigurationHelper.GetWebConfiguration(FrontEndPath).SectionGroups["system.web"].Sections["compilation"].
						ElementInformation.Properties["debug"].Value.ToString());
				}, "Error at extracting Debug parameter.");

				VizeliaConfiguration configuration = null;
				VizeliaConfiguration configurationScheduler = null;

				Helper.DoAction(() => {
					configuration =
						(VizeliaConfiguration)(ConfigurationHelper.GetWebConfiguration(FrontEndPath).Sections["VizeliaConfiguration"]);
				}, "Error at getting the vizelia configuration section from frontend config file");

				Helper.DoAction(() => {
					configurationScheduler =
						(VizeliaConfiguration)(ConfigurationHelper.GetWebConfiguration(SchedulerPath).Sections["VizeliaConfiguration"]);
				}, "Error at getting the vizelia configuration section from scheduler config file");

				Helper.DoAction(() => {
					Protect = configuration.SectionInformation.IsProtected;
				}, "Error at extracting the Protect parameter");

				Helper.DoAction(() => {
					TenantName = configuration.Deployment.ApplicationName;
				}, "Error at extracting the TenantName parameter");

				Helper.DoAction(() => {
					AdminTenantEmail = configuration.Deployment.AdminTenantEmail;
				}, "Error at extracting the AdminTenantEmail parameter");

				Helper.DoAction(() => {
					HelpUrl = configuration.Deployment.HelpURL;
				}, "Error at extracting the HelpUrl parameter");

				Helper.DoAction(() => {
					VizeliaLicenceFile = VizeliaLicenceFile ?? configuration.Deployment.VizeliaLicence;
				}, "Error at extracting the VizeliaLicenceFile parameter");

				if (string.IsNullOrEmpty(VizeliaLicenceFile)) {
					OutputHelper.WriteToOutput("VizeliaLicenceFile parameter is null or empty", MessageType.Error);
					throw new Exception("VizeliaLicenceFile parameter is null or empty");
				}

				Helper.DoAction(() => {
					SchedulerUsername = configuration.Authentication.SchedulerUsername;
					SchedulerUsernameScheduler = configurationScheduler.Authentication.SchedulerUsername;
				}, "Error at extracting the SchedulerUsername parameter");

				Helper.DoAction(() => {
					SchedulerPassword = configuration.Authentication.SchedulerPassword;
					SchedulerPasswordScheduler = configurationScheduler.Authentication.SchedulerPassword;
				}, "Error at extracting the SchedulerPassword parameter");

				Helper.DoAction(() => {
					ForgotPassword = configuration.Authentication.AllowForgotPassword;
				}, "Error at extracting the AllowForgotPassword parameter");

				Helper.DoAction(() => {
					foreach (var conString in ConfigurationHelper.GetWebConfiguration(FrontEndPath).ConnectionStrings.ConnectionStrings
						) {
						var settings = (ConnectionStringSettings)conString;
						if (settings.ConnectionString.Contains("Data Source")) {
							connectionString = settings.ConnectionString;
							break;
						}
					}
				}, "Error at extracting a connectionString from web.config");
			}
			// fallback if old config and webConfiguration fails to identify elements/attributes then use xmldoc (v41->v46)
			catch {
				OutputHelper.WriteToOutput("Fallback to xml method extraction of the configuration file.", MessageType.Warning);
				XmlElement root = doc.DocumentElement;
				XmlElement rootScheduler = docScheduler.DocumentElement;

				Helper.DoAction(() => {
					Debug = bool.Parse(root.SelectSingleNode("//system.web/compilation").Attributes.GetNamedItem("debug").Value);
				}, "Error at extracting the Debug parameter");
				Protect = false;
				Helper.DoAction(() => {
					TenantName = root["VizeliaConfiguration"]["deployment"].Attributes.GetNamedItem("applicationName").Value;
				}, "Error at extracting the TenantName parameter");
				Helper.DoAction(() => {
					AdminTenantEmail = root["VizeliaConfiguration"]["deployment"].Attributes.GetNamedItem("adminTenantEmail").Value;
				}, "Error at extracting the AdminTenantEmail parameter");

				Helper.DoAction(() => {
					HelpUrl = root["VizeliaConfiguration"]["deployment"].Attributes.GetNamedItem("helpUrl").Value;
				}, "Error at extracting the helpUrl parameter");

				Helper.DoAction(() => {
					VizeliaLicenceFile = VizeliaLicenceFile ??
										 root["VizeliaConfiguration"]["deployment"].Attributes.GetNamedItem("VizeliaLicence").Value;
				}, "Error at extracting the VizeliaLicenceFile parameter");
				if (string.IsNullOrEmpty(VizeliaLicenceFile)) throw new Exception("VizeliaLicenceFile parameter is null or empty");
				Helper.DoAction(() => {
					SchedulerUsername = root["VizeliaConfiguration"]["authentication"].Attributes.GetNamedItem("schedulerUsername").Value;
					SchedulerUsernameScheduler = rootScheduler["VizeliaConfiguration"]["authentication"].Attributes.GetNamedItem("schedulerUsername").Value;
				}, "Error at extracting the SchedulerUsername parameter");
				Helper.DoAction(() => {
					SchedulerPassword = root["VizeliaConfiguration"]["authentication"].Attributes.GetNamedItem("schedulerPassword").Value;
					SchedulerPasswordScheduler = rootScheduler["VizeliaConfiguration"]["authentication"].Attributes.GetNamedItem("schedulerPassword").Value;
				}, "Error at extracting the SchedulerPassword parameter");
				Helper.DoAction(() => {
					ForgotPassword = bool.Parse(root["VizeliaConfiguration"]["authentication"].Attributes.GetNamedItem("allowForgotPassword").Value);
				}, "Error at extracting the AllowForgotPassword parameter");
				Helper.DoAction(() => {
					var connectionsStringNode =
						root.SelectSingleNode("connectionStrings/add[starts-with(@connectionString,'Data Source')]");
					connectionString = connectionsStringNode.Attributes.GetNamedItem("connectionString").Value;
				}, "Error at extracting a connectionString");

			}
			FrontEndName = TenantName;

			Helper.DoAction(() => {
				SMTPFrom =
					doc.DocumentElement.SelectSingleNode("//system.net/mailSettings/smtp").Attributes.GetNamedItem("from").Value;
				if (SMTPFrom.ToLower() == "support.teamfoundation@vizelia.com") SMTPFrom = null;
			}, "Error at extracting smtp from mail");


			if (SchedulerUsername != SchedulerUsernameScheduler) {
				OutputHelper.WriteToOutput("Scheduler username in frontend and scheduler configuration file are not the same.", MessageType.Error);
				Environment.Exit(0);
			}

			if (SchedulerPassword != SchedulerPasswordScheduler) {
				OutputHelper.WriteToOutput("Scheduler password in frontend and scheduler configuration file are not the same.", MessageType.Error);
				Environment.Exit(0);
			}

			if (!Helper.ValidatePasswordStrength(SchedulerPassword)) {
				OutputHelper.WriteToOutput("Scheduler password is not valid.", MessageType.Error);
				Environment.Exit(0);
			}

			Helper.DoAction(() => {
				var builder = new SqlConnectionStringBuilder(connectionString);
				DBServer = builder.DataSource;
				DBUser = builder.UserID;
				DBPwd = builder.Password;
				DBPrefix = builder.InitialCatalog.Substring(0, builder.InitialCatalog.IndexOf('_'));
			}, "Error at extracting the DB parameters from connectionString");

			ConfigureSSLOnlyParameter();

			base.CalculateProperties();
		}

		/// <summary>
		/// Configures the SSL only parameter.
		/// </summary>
		protected void ConfigureSSLOnlyParameter() {
			try {
#if true
                // The front end web.config file's <rewrite> rule for "Redirect HTTP to HTTPS"
                // supplies the value we need in its enabled attribute.
                SSLOnly = false;    // default
                var doc = new XmlDocument();
                string configFile = Path.Combine(TargetDirectory, FrontEndName, "web.config");
                doc.Load(configFile);
                XmlElement Root = doc.DocumentElement;
                if (Root == null)
                    return;
                var redirectHttpRule = Root.SelectSingleNode("//system.webServer/rewrite/rules/rule[@name=\"Redirect HTTP to HTTPS\"]");
                if (redirectHttpRule != null)
                {
                    // if no enabled property exists, it already means its in use.
                    // Only modify it to true if present
                    XmlAttribute enabledAtt = redirectHttpRule.Attributes["enabled"];
                    if (enabledAtt != null)
                    {
                        SSLOnly = bool.Parse(enabledAtt.Value);
#if DEBUG
                    // this message is mostly here for testing
                        OutputHelper.WriteToOutput(String.Format("Detected SSLOnly rule as {0}", enabledAtt.Value));
#endif
                    }
                }

#else   // this depends on missing the port 80 endpoint. User Story 3591 always installs port 80. So this test no longer works.
				using (var manager = new ServerManager()) {
					var site = manager.Sites[FrontEndName];
					SSLOnly = !site.Bindings.Any(binding => binding.BindingInformation.Contains(":80:"));
				}
#endif
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput("Error in discovering SSLOnly configuration from IIS.");
				UsageHelper.Abort(ex.Message);
			}
		}

		/// <summary>
		/// Sets the URL from IIS.
		/// </summary>
		/// <param name="frontEndPath">The front end path.</param>
		private void SetUrlFromIIS(string frontEndPath) {
			string error = null;
			try {
				using (var manager = new ServerManager()) {
					foreach (var site in manager.Sites) {
						if (site.Applications.Any(application => application.VirtualDirectories.Any(virtualDirectory => virtualDirectory.PhysicalPath.ToLower().Equals(frontEndPath.ToLower())))) {
							int lastIndex = site.Bindings[0].BindingInformation.LastIndexOf(":");
							Url = site.Bindings[0].BindingInformation.Substring(lastIndex + 1);
							break;
						}
					}
				}
			}
			catch (Exception ex) {
				error = ex.Message;
			}

			if (string.IsNullOrEmpty(Url)) {
				OutputHelper.WriteToOutput("Error in extraction the url of the application from IIS.", MessageType.Error);
				if (error != null) UsageHelper.Abort(error);
			}
		}

		/// <summary>
		/// Gets the action description.
		/// </summary>
		/// <returns></returns>
		protected override string GetActionDescription() {
			return "Update Utility - Updates an old version of the application.";
		}

		/// <summary>
		/// Copies the source and configures web.config.
		/// </summary>
		protected override void DoAction() {
			OutputHelper.WriteToOutput("...update is starting...");

			// deploy.bat
			ConfigureSQL();

			TurnApplicationPools(false);
			Thread.Sleep(5000);

			//Debugger.Launch();

			DoBackup();

			CopyFiles(Path.Combine(SourceLocation, SourceLocationFolderFrontEnd), Path.Combine(TargetDirectory, FrontEndName));
			CopyFiles(Path.Combine(SourceLocation, SourceLocationFolderScheduler), Path.Combine(TargetDirectory, SchedulerName));
			CopyFiles(Path.Combine(SourceLocation, SourceLocationFolderEnergyAggregator), Path.Combine(TargetDirectory, EnergyAggregatorName));

			DisableOldSSLProtocolsFunc();
			ConfigureWebConfig(Path.Combine(TargetDirectory, FrontEndName), false, ConfigType.FrontEnd);
			ConfigureWebConfig(Path.Combine(TargetDirectory, SchedulerName), true, ConfigType.Scheduler);
			ConfigureWebConfig(Path.Combine(TargetDirectory, EnergyAggregatorName), true, ConfigType.EnergyAggregator);
			TurnApplicationPools(true);

			// Vizelia.FOL.Utilities.AzmanUpdateTool.exe
			ConfigureAzman();

			OutputHelper.WriteToOutput(string.Format("{0}Update completed.", Environment.NewLine), MessageType.Success);
		}

		private void DoBackup() {
			if (!Backup) return;

			OutputHelper.WriteToOutput("...performing backup on files...");

			var archiveName = string.Format("backup.{0}.{1}.zip", String.Format("{0:yyyy.MM.dd.HH.mm.ss}", DateTime.Now),
											FrontEndName);
			CompressionHelper.Compress(new[] {
			                                 	Path.Combine(TargetDirectory, FrontEndName),
			                                 	Path.Combine(TargetDirectory, SchedulerName),
			                                 	Path.Combine(TargetDirectory, EnergyAggregatorName)
			                                 },
									   archiveName, CompressionLevel.None);

			OutputHelper.WriteToOutput("...performing backup on files -> " +
									   Path.Combine(Environment.CurrentDirectory, archiveName));
		}

		/// <summary>
		/// Configures the azman.
		/// </summary>
		private void ConfigureAzman() {
			if (UpdateAzman) {
				OutputHelper.WriteToOutput("...starting to update azman permissions - long operation");
				var azmanFilePath = Path.Combine(FrontEndPath, @"azman\azman.xml");
				var result = Helper.ExecuteCommand(string.Format("Vizelia.FOL.Utilities.AzmanUpdateTool.exe /AzmanFile:{0} /SchedulerUsername:{1}", azmanFilePath, SchedulerUsername));
				OutputHelper.WriteToOutput("...finished updating azman permissions");
			}
		}

		/// <summary>
		/// Turns the application pools.
		/// </summary>
		/// <param name="turnOn">if set to <c>true</c> [turn on] else turns off.</param>
		protected void TurnApplicationPools(bool turnOn) {
			try {
				using (var manager = new ServerManager()) {
					var poolFrontEnd = manager.ApplicationPools[FrontEndName];
					var poolEA = manager.ApplicationPools[EnergyAggregatorName];
					var poolScheduler = manager.ApplicationPools[SchedulerName];
					if (turnOn) {
						if (!poolFrontEnd.State.ToString().ToLower().Equals("started")) poolFrontEnd.Start();
						if (!poolEA.State.ToString().ToLower().Equals("started")) poolEA.Start();
						if (!poolScheduler.State.ToString().ToLower().Equals("started")) poolScheduler.Start();
						OutputHelper.WriteToOutput("...application pools started.");
					}
					else {
						if (!poolFrontEnd.State.ToString().ToLower().Equals("stopped")) poolFrontEnd.Stop();
						if (!poolEA.State.ToString().ToLower().Equals("stopped")) poolEA.Stop();
						if (!poolScheduler.State.ToString().ToLower().Equals("stopped")) poolScheduler.Stop();
						OutputHelper.WriteToOutput("...application pools stopped.");
					}
				}
			}
			catch (Exception ex) {
				UsageHelper.Abort(string.Format("Could not turn {0} application pools. {1}", (turnOn ? "on" : "off"), ex.Message));
			}
		}
	}
}

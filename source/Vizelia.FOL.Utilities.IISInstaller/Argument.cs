﻿using System;
using Vizelia.FOL.Utilities.CommandLine;

namespace Vizelia.FOL.Utilities.IISInstaller {
	/// <summary>
	/// Argument class 
	/// </summary>
	internal class Argument {
		private string help;
		private string syntax;
		private string mValue;

		public string Help {
			get {
				return string.Format("({0}) {1}{2}", Type.ToString().ToLower(), help,
				                     (Type == ArgumentType.Optional && !HideDefaultValue
				                      	? string.Format(" Default value {0}.", DefaultValue)
				                      	: string.Empty));
			}
			set { help = value; }
		}

		public string InvalidValueString {
			get { return string.Format("Invalid {0} parameter value. Usage: {1}", Name, Syntax); }
		}

		public string MissingValue {
			get { return string.Format("{0} parameter is missing.", Name); }
		}

		public string Syntax {
			get { return "/" + Name + ":" + syntax; }
			set { syntax = value; }
		}

		public ArgumentType Type { get; set; }
		public string Name { get; set; }
		public string DefaultValue { get; set; }
		public bool HideDefaultValue { get; set; }

		public string Value {
			get { return (mValue == null && Type == ArgumentType.Optional) ? DefaultValue : mValue; }
			set { mValue = value; }
		}

		/// <summary>
		/// Configure and set value
		/// </summary>
		public Action<string> Configure;

		/// <summary>
		/// Validate value
		/// </summary>
		public Func<string, bool> Validate;

		/// <summary>
		/// Map to local property
		/// </summary>
		public Action<string> Map;

		/// <summary>
		/// Initializes a new instance of the <see cref="Argument"/> class.
		/// </summary>
		public Argument() {
			Configure = s => Value = s;
			HideDefaultValue = false;
		}
	}
}

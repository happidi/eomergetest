﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Threading;
using System.Xml;
using Microsoft.Web.Administration;
using Microsoft.Win32;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Utilities.CommandLine;
using Vizelia.FOL.WCFService.Contracts;
using Configuration = Microsoft.Web.Administration.Configuration;
using ConfigurationElement = Microsoft.Web.Administration.ConfigurationElement;
using ConfigurationElementCollection = Microsoft.Web.Administration.ConfigurationElementCollection;
using ConfigurationSection = Microsoft.Web.Administration.ConfigurationSection;

namespace Vizelia.FOL.Utilities.IISInstaller {
	/// <summary>
	/// Setup a developer environment with the relvant websites, application pools and autostartproviders
	/// </summary>
	internal class InstallApplicationInstallerAction : ApplicationInstallerAction {

		// Given
		protected string AdminPassword { get; set; }
		protected string SSLCertificate { get; set; }
		protected string SSLCertificatePassword { get; set; }

        /// <summary>
        /// Associated with /InitializeTenant parameter.
		/// /InitializeTenant parameter with no value: same as /InitializeTenant:True.
		/// /InitializeTenant not used sets this to null.
        /// </summary>
		/// <value>
		/// true -> Initialize the tenant. Do not prompt.
		/// false -> Do not initialize the tenant. Do not prompt.
		/// null -> prompt the user.
		/// </value>
        protected bool? AutoInitializeTenant { get; set; }
		
		protected int AutoInitializeTenantTimeout { get; set; }

		// Calculated
		protected string FrontEndAutoStartProviderName { get; set; }
		protected string SchedulerAutoStartProviderName { get; set; }

		protected override void DoAction() {
			OutputHelper.WriteToOutput("...installation is starting...");

			ConfigureSQL();
			ConfigureCompression();
			ConfigureSQLReportingServices();
			ConfigureSiteShoterRegistry();
			DisableOldSSLProtocolsFunc();
			ConfigureUTC();
			ConfigureAutoStartProviders();
			ConfigureSites();
			ConfigureTrustedLogins();
			Helper.TrustHttps();
			VerifyAppIsRunning();
			ConfigureTenant();

			OutputHelper.WriteToOutput(string.Format("{0}Installation completed.", Environment.NewLine), MessageType.Success);
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="InstallApplicationInstallerAction"/> class.
		/// </summary>
		public InstallApplicationInstallerAction() {
			var argDBPrefix = new Argument() {
				Syntax = "<string>",
				Help = "The prefix used for databases.",
				Type = ArgumentType.Optional,
				Name = "DBPrefix",
				DefaultValue = "vizelia",
				Map = s => DBPrefix = s
			};
			var argDBPwd = new Argument() {
				Syntax = "<string>",
				Help = "A valid password for the database user. Ensure it is unique and strong.",
				Type = ArgumentType.Optional,
				Name = "DBPwd",
				Map = s => DBPwd = s,
				HideDefaultValue = true
//NOTE: There should never be a default value. We want users to create unique PWs
			};
            var argModConnection = new Argument()
            {
                Syntax = "[Login|Trust|No]",
                Help = "If and how to update connection strings. 'Login' requires /DbPwd. 'Trust' uses Trust_Connection. 'No' will not change your connection strings.",
                Type = ArgumentType.Optional,
                Name = "ModConnection",
                DefaultValue = "Login",
                Map = s => { 
                    ModConnectionType temp;
                    if (Enum.TryParse<ModConnectionType>(s, out temp))
                        ModConnection = temp;
                    else
                        throw new ArgumentException(String.Format("/ModConnection requires one of these: 'Login', 'Trust', 'No'. You used {0}.", s));
                }
            };
            var argDBServer = new Argument()
            {
				Syntax = "<string>",
				Help = "The database server.",
				Type = ArgumentType.Optional,
				Name = "DBServer",
				DefaultValue = "(local)",
				Map = s => DBServer = s
			};
			var argDBUser = new Argument() {
				Syntax = "<string>",
				Help = "A valid user for the database.",
				Type = ArgumentType.Optional,
				Name = "DBUser",
				DefaultValue = "sa",
				Map = s => DBUser = s
			};
			var argDebug = new Argument() {
				Syntax = "[true|false]",
				Help = "The debug mode, true or false.",
				Type = ArgumentType.Optional,
				Name = "Debug",
				DefaultValue = "false",
				Map = s => Debug = bool.Parse(s)
			};
			var argAdminTenantEMail = new Argument() {
				Syntax = "<string>",
				Help = "The email of the admin tenant.",
				Type = ArgumentType.Optional,
				Name = "AdminTenantEmail",
				DefaultValue = "support.teamfoundation@vizelia.com",
				Map = s => AdminTenantEmail = s
			};

			var argHelpUrl = new Argument() {
				Syntax = "<string>",
				Help = "The url to the help page.",
				Type = ArgumentType.Optional,
				Name = "HelpUrl",
				DefaultValue = "",
				Map = s => HelpUrl = s
			};

			var argTenantName = new Argument() {
				Syntax = "<string>",
				Help = "The tenant name.",
				Type = ArgumentType.Mandatory,
				Name = "TenantName",
				Map = s => TenantName = s
			};
			var argUrl = new Argument() {
				Syntax = "<string>",
				Help = "The url used to access this tenant. Should be in the form tenant.domain.com.",
				Type = ArgumentType.Mandatory,
				Name = "Url",
				Map = s => Url = s
			};
			var argTargetDirectory = new Argument() {
				Syntax = "<string>",
				Help = "The directory where the files will be installed.",
				Type = ArgumentType.Mandatory,
				Name = "TargetDirectory",
				Map = s => TargetDirectory = s
			};

			//var argIsolationMode = new Argument() {
			//                                        Syntax = "[Isolated|Shared]",
			//                                        Help = "The isolation mode, isolated or shared.",
			//                                        Type = ArgumentType.Mandatory,
			//                                        Name = "IsolationMode",
			//                                        Map = s => IsolationProvider = s
			//                                      };
			//argIsolationMode.Configure = s => {
			//                                if (s.ToLower().Equals("isolated")) argIsolationMode.Value = IsolatedProviderName;
			//                                else if (s.ToLower().Equals("shared")) argIsolationMode.Value = SharedProviderName;
			//                                else throw new Exception(string.Format("value was incorrect: {0}", s));
			//                             };
			var argAdminPassword = new Argument {
				Syntax = "<password>",
				Help = "The administrator's password with at least 6 chars, 2 of them non-alphanumeric.",
				Type = ArgumentType.Mandatory,
				Name = "AdminPassword",
				Map = s => AdminPassword = s,
				Validate = s => !s.All(Char.IsLetterOrDigit)
			};

			argAdminPassword.Validate = Helper.ValidatePasswordStrength;

			var argSchedulerPassword = new Argument() {
				Syntax = "<password>",
				Help = "Scheduler password with at least 6 chars, 2 of them non-alphanumeric.",
				Type = ArgumentType.Mandatory,
				Name = "SchedulerPassword",
				Map = s => SchedulerPassword = s
			};
			argSchedulerPassword.Validate = Helper.ValidatePasswordStrength;
			var argSchedulerUsername = new Argument() {
				Syntax = "<string>",
				Help = "Scheduler password.",
				Type = ArgumentType.Optional,
				Name = "SchedulerUsername",
				Map = s => SchedulerUsername = s,
				DefaultValue = "Scheduler"
			};
			var argSSLCertificate = new Argument() {
				Syntax = "<string>",
				Help = "The path to the ssl certificate (.pfx).",
				Type = ArgumentType.Optional,
				Name = "SSLCertificate",
				Map = s => SSLCertificate = s,
				DefaultValue = @".\Support Files\VizeliaLocalDevelopmentCertificate.pfx"
			};
			var argSSLCertificatePassword = new Argument() {
				Syntax = "<string>",
				Help = "The password of the ssl certificate.",
				Type = ArgumentType.Optional,
				Name = "SSLCertificatePassword",
				Map = s => SSLCertificatePassword = s,
				DefaultValue = @"P@ssw0rd",
				HideDefaultValue = true
			};
			var argSSLOnly = new Argument() {
				Syntax = "[true|false]",
				Help = "Ensures all requests go through https. Requests to http are automatically redirected to https.",
				Type = ArgumentType.Optional,
				Name = "SSLOnly",
				Map = s => SSLOnly = bool.Parse(s),
				DefaultValue = "false"
			};
			argSSLOnly.Configure = s => {
				bool result;
				if (s != null && bool.TryParse(s, out result) && result)
					ServicePointManager.ServerCertificateValidationCallback =
						((sender, certificate, chain, sslPolicyErrors) => true);
				argSSLOnly.Value = s;
			};
			var argVizeliaLicenceFile = new Argument {
				Syntax = "<string>",
				Help = "The path to vizelia licence file.",
				Type = ArgumentType.Mandatory,
				Name = "VizeliaLicenceFile",
				Map = s => VizeliaLicenceFile = s
			};
			argVizeliaLicenceFile.Validate = s => CheckFile(argVizeliaLicenceFile.Value);
			var argDotNetChartingLicense = new Argument {
				Syntax = "<string>",
				Help = "The path to DotNetCharting licence file. This parameter should not be used on non-production servers without permission from the Ops manager who approves DotNetCharting licence distribution.",
				Type = ArgumentType.Optional,
				Name = "DotNetChartingLicence",	//NOTE: Used Licence instead of License to conform with existing VizeliaLicenceFile
				Map = s => DotNetChartingLicence = s
			};
			argDotNetChartingLicense.Validate = s => 
			{
				if (s == null)	// no parameter defined
					return true;
				if (s == "")	// parameter blank means delete
					return true;
/*
				if (s.StartsWith("{") && s.EndsWith("}"))
					return true;
 */ 
				return CheckFile(s);
			};
			var argProtect = new Argument() {
				Syntax = "[true|false]",
				Help = "Encrypt the config, yes or no.",
				Type = ArgumentType.Optional,
				Name = "Protect",
				DefaultValue = "false",
				Map = s => Protect = bool.Parse(s)
			};
			var argForgotPassword = new Argument {
				Syntax = "[true|false]",
				Help = "Show forgot password button, true or false.",
				Type = ArgumentType.Optional,
				Name = "ForgotPassword",
				DefaultValue = "false",
				Map = s => ForgotPassword = bool.Parse(s)
			};
			var argInitializeTenant = new Argument() {
				Syntax = "[true|false]",
				Help = "'true' initializes the tenant without a prompt. 'false' does not initialize the tenant and does not prompt. Not specified prompts to initialize the tenant.",
				Type = ArgumentType.Optional,
				Name = "InitializeTenant",
				Map = s => 
				{
					if (s == null)
						AutoInitializeTenant = null;
					else if (String.IsNullOrWhiteSpace(s))
						AutoInitializeTenant = true;
					else 
						AutoInitializeTenant = bool.Parse(s);
				},
				DefaultValue = null
			};

			var argInitializeTenantTimeout = new Argument() {
				Syntax = "digits only",
				Help = String.Format("Specifies timeout in minutes for tenant initialization checks. Works when {0} switch is enabled. Disabled when 0.", argInitializeTenant.Name),
				Type = ArgumentType.Optional,
				Name = "InitializeTenantTimeout",
				Map = s => AutoInitializeTenantTimeout = int.Parse(s),
				DefaultValue = "30"
			};

		/* Design guidelines for blank from User Story 5147:
			If /ProductionMode is blank , set the default to false. 
			Developers and testers are inherently lazy. 
			They will enter the minimum number of parameters to make something work. 
			So, if the ProductionMode switch is optional, then we'll want the default value 
			configured in a safe way.
		*/
			var argProductionMode = new Argument() {
				Syntax = "[true|false]",
				Help = "When true, the server is for production use and will get the production mode licenses. Use false on all other servers.",
				Type = ArgumentType.Optional,
				Name = "ProductionMode",
				Map = s => ProductionMode = !String.IsNullOrWhiteSpace(s) && 
					s.Equals("true", StringComparison.InvariantCultureIgnoreCase),
				DefaultValue = "false"
			};


			Arguments.Add(argTenantName.Name, argTenantName);
			Arguments.Add(argUrl.Name, argUrl);
			Arguments.Add(argTargetDirectory.Name, argTargetDirectory);
			//Arguments.Add(argIsolationMode.Name, argIsolationMode);
			Arguments.Add(argAdminPassword.Name, argAdminPassword);
			Arguments.Add(argSchedulerPassword.Name, argSchedulerPassword);
			Arguments.Add(argSchedulerUsername.Name, argSchedulerUsername);
			Arguments.Add(argSSLCertificate.Name, argSSLCertificate);
			Arguments.Add(argSSLCertificatePassword.Name, argSSLCertificatePassword);
			Arguments.Add(argSSLOnly.Name, argSSLOnly);
			Arguments.Add(argVizeliaLicenceFile.Name, argVizeliaLicenceFile);
			Arguments.Add(argDotNetChartingLicense.Name, argDotNetChartingLicense);
			Arguments.Add(argDBPrefix.Name, argDBPrefix);
			Arguments.Add(argDBPwd.Name, argDBPwd);
			Arguments.Add(argDBServer.Name, argDBServer);
			Arguments.Add(argDBUser.Name, argDBUser);
            Arguments.Add(argModConnection.Name, argModConnection);
			Arguments.Add(argAdminTenantEMail.Name, argAdminTenantEMail);
			Arguments.Add(argHelpUrl.Name, argHelpUrl);
			Arguments.Add(argDebug.Name, argDebug);
			Arguments.Add(argProtect.Name, argProtect);
			Arguments.Add(argForgotPassword.Name, argForgotPassword);
            Arguments.Add(argInitializeTenant.Name, argInitializeTenant);
			Arguments.Add(argInitializeTenantTimeout.Name, argInitializeTenantTimeout);
			Arguments.Add(argProductionMode.Name, argProductionMode);
		}

		/// <summary>
		/// Calculates the properties.
		/// </summary>
		protected override void CalculateProperties() {
			base.CalculateProperties();

			FrontEndAutoStartProviderName = "Vizelia Frontend AutoStartProvider";
			SchedulerAutoStartProviderName = "Vizelia Scheduler AutoStartProvider";
		}

		/// <summary>
		/// Configures the SQL reporting services.
		/// </summary>
		protected void ConfigureSQLReportingServices() {
			ConfigureSQLReportingServicesConfigFile();
			ConfigureSQLReportingServicesRegistry();
		}

		/// <summary>
		/// Configures the SQL reporting services config file.
		/// </summary>
		protected void ConfigureSQLReportingServicesConfigFile() {
			try {
				var doc = new XmlDocument();
				const string configFile = @"C:\Program Files\Microsoft SQL Server\MSRS10_50.MSSQLSERVER\Reporting Services\ReportServer\rsreportserver.config";
				doc.Load(configFile);

				var rsElem = doc.SelectSingleNode("//AuthenticationTypes/RSWindowsBasic");
				if (rsElem == null) {
					var ateAuthenticationTypes = doc.SelectSingleNode("//AuthenticationTypes");
					var ateRSWindowsBasic = doc.CreateElement("RSWindowsBasic");
					ateAuthenticationTypes.AppendChild(ateRSWindowsBasic);
				}
				else {
					rsElem.RemoveAll();
				}

				var serElem = doc.SelectSingleNode("//Service/IsReportBuilderAnonymousAccessEnabled");
				if (serElem == null) {
					var seService = doc.SelectSingleNode("//Service");
					var seIsReportBuilderAnonumousAccessEnabled = doc.CreateElement("IsReportBuilderAnonymousAccessEnabled");
					seIsReportBuilderAnonumousAccessEnabled.InnerText = "true";
					seService.AppendChild(seIsReportBuilderAnonumousAccessEnabled);
				}
				else {
					serElem.RemoveAll();
					serElem.InnerText = "true";
				}

				doc.Save(configFile);

				OutputHelper.WriteToOutput("...configure SQL reporting services config file");
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput(string.Format("configure sql reporting services config file error: {0}", ex.Message), MessageType.Error);
			}
		}

		/// <summary>
		/// Gets the old scheduler user credentials.
		/// </summary>
		/// <param name="schedulerUsername">The scheduler username.</param>
		/// <param name="schedulerPassword">The scheduler password.</param>
		protected void GetOldSchedulerUserCredentials(out string schedulerUsername, out string schedulerPassword) {
			schedulerUsername = null;
			schedulerPassword = null;

			try {
				var target = Path.Combine(TargetDirectory, SchedulerName);
				var configuration = ConfigurationHelper.GetWebConfiguration(target);
				var vizeliaConfiguration = configuration.Sections["VizeliaConfiguration"];
				if (vizeliaConfiguration != null) {
					var vizeliaConfigurationObject = (VizeliaConfiguration)vizeliaConfiguration;
					schedulerUsername = vizeliaConfigurationObject.Authentication.SchedulerUsername;
					schedulerPassword = vizeliaConfigurationObject.Authentication.SchedulerPassword;
				}
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput(string.Format("Error at getting existing scheduler credentials: {0}", ex.Message), MessageType.Error);
			}
		}

		/// <summary>
		/// Configures the site shoter registry.
		/// </summary>
		protected void ConfigureSiteShoterRegistry() {
			try {
				var keyBrowserEmulator =
					Registry.LocalMachine.OpenSubKey(
						@"SOFTWARE\Wow6432Node\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BROWSER_EMULATION", true);
				keyBrowserEmulator.SetValue("SiteShoter.exe", 9999, RegistryValueKind.DWord);
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput(string.Format("configure siteshoter registry error: {0}", ex.Message), MessageType.Error);
			}
		}

		/// <summary>
		/// Configures the SQL reporting services registry.
		/// </summary>
		protected void ConfigureSQLReportingServicesRegistry() {
			try {
				var keyLanmanParameters =
					Registry.LocalMachine.OpenSubKey(@"System\CurrentControlSet\Services\LanmanServer\Parameters", true);
				keyLanmanParameters.SetValue("DisableStrictNameChecking", 1, RegistryValueKind.DWord);

				var keyLsa = Registry.LocalMachine.OpenSubKey(@"System\CurrentControlSet\Control\Lsa", true);
				keyLsa.SetValue("DisableLoopbackCheck", 1, RegistryValueKind.DWord);

				OutputHelper.WriteToOutput("...configure sql reporting services registry");
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput(string.Format("configure sql reporting services registry error: {0}", ex.Message), MessageType.Error);
			}
		}

		/// <summary>
		/// Configures the compression.
		/// </summary>
		protected void ConfigureCompression() {
			try {
				var doc = new XmlDocument();
				var configFile = Environment.SystemDirectory + @"\inetsrv\config\ApplicationHost.config";
				doc.Load(configFile);

				var hceHttpCompression = doc.DocumentElement.SelectSingleNode("//httpCompression");
				if (hceHttpCompression != null) {
					hceHttpCompression.RemoveAll();
				}
				else {
					hceHttpCompression = doc.CreateElement("httpCompression");
					doc.DocumentElement.AppendChild(hceHttpCompression);
				}

				var hceScheme = CompressionCreateSchemeElement(doc);
				var hceDynamicTypes = CompressionCreateDynamicTypesElement(doc);
				var hceStaticTypes = CompressionCreateStaticTypesElement(doc);
				CompressionCreateHttpCompressionElement(hceStaticTypes, hceScheme, hceDynamicTypes, hceHttpCompression, doc);

				doc.Save(configFile);

				OutputHelper.WriteToOutput("...configure compression.");
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput(string.Format("configure compression error: {0}", ex.Message), MessageType.Error);
			}
		}

		/// <summary>
		/// Compressions the create HTTP compression element.
		/// </summary>
		/// <param name="hceStaticTypes">The hce static types.</param>
		/// <param name="hceScheme">The hce scheme.</param>
		/// <param name="hceDynamicTypes">The hce dynamic types.</param>
		/// <param name="hceHttpCompression">The hce HTTP compression.</param>
		/// <param name="doc">The doc.</param>
		protected void CompressionCreateHttpCompressionElement(XmlElement hceStaticTypes, XmlElement hceScheme,
																	XmlElement hceDynamicTypes, XmlNode hceHttpCompression,
																	XmlDocument doc) {
			var hcaHttpCompressionDirectory = doc.CreateAttribute("directory");
			var hcaHttpCompressionMinFileSizeForComp = doc.CreateAttribute("minFileSizeForComp");
			hcaHttpCompressionDirectory.Value = @"%SystemDrive%\inetpub\temp\IIS Temporary Compressed Files";
			hcaHttpCompressionMinFileSizeForComp.Value = "256";
			hceHttpCompression.Attributes.Append(hcaHttpCompressionDirectory);
			hceHttpCompression.Attributes.Append(hcaHttpCompressionMinFileSizeForComp);
			hceHttpCompression.AppendChild(hceScheme);
			hceHttpCompression.AppendChild(hceDynamicTypes);
			hceHttpCompression.AppendChild(hceStaticTypes);
		}

		/// <summary>
		/// Compressions the create scheme element.
		/// </summary>
		/// <param name="doc">The doc.</param>
		/// <returns></returns>
		protected XmlElement CompressionCreateSchemeElement(XmlDocument doc) {
			var hceScheme = doc.CreateElement("scheme");
			var hcaSchemeName = doc.CreateAttribute("name");
			var hcaSchemeDll = doc.CreateAttribute("dll");
			hcaSchemeName.Value = "gzip";
			hcaSchemeDll.Value = @"%Windir%\system32\inetsrv\gzip.dll";
			hceScheme.Attributes.Append(hcaSchemeName);
			hceScheme.Attributes.Append(hcaSchemeDll);
			return hceScheme;
		}

		/// <summary>
		/// Compressions the create static types element.
		/// </summary>
		/// <param name="doc">The doc.</param>
		/// <returns></returns>
		protected XmlElement CompressionCreateStaticTypesElement(XmlDocument doc) {
			var hceStaticTypes = doc.CreateElement("staticTypes");
			var hceStaticTypesAdd1 = doc.CreateElement("add");
			var hceStaticTypesAdd2 = doc.CreateElement("add");
			var hceStaticTypesAdd3 = doc.CreateElement("add");
			var hceStaticTypesAdd4 = doc.CreateElement("add");
			var hcaStaticTypesMimeType1 = doc.CreateAttribute("mimeType");
			var hcaStaticTypesMimeType2 = doc.CreateAttribute("mimeType");
			var hcaStaticTypesMimeType3 = doc.CreateAttribute("mimeType");
			var hcaStaticTypesMimeType4 = doc.CreateAttribute("mimeType");
			var hcaStaticTypesEnabled1 = doc.CreateAttribute("enabled");
			var hcaStaticTypesEnabled2 = doc.CreateAttribute("enabled");
			var hcaStaticTypesEnabled3 = doc.CreateAttribute("enabled");
			var hcaStaticTypesEnabled4 = doc.CreateAttribute("enabled");
			hcaStaticTypesMimeType1.Value = "text/*";
			hcaStaticTypesMimeType2.Value = "message/*";
			hcaStaticTypesMimeType3.Value = "application/*";
			hcaStaticTypesMimeType4.Value = "*/*";
			hcaStaticTypesEnabled1.Value = "true";
			hcaStaticTypesEnabled2.Value = "true";
			hcaStaticTypesEnabled3.Value = "true";
			hcaStaticTypesEnabled4.Value = "false";
			hceStaticTypesAdd1.Attributes.Append(hcaStaticTypesMimeType1);
			hceStaticTypesAdd1.Attributes.Append(hcaStaticTypesEnabled1);
			hceStaticTypesAdd2.Attributes.Append(hcaStaticTypesMimeType2);
			hceStaticTypesAdd2.Attributes.Append(hcaStaticTypesEnabled2);
			hceStaticTypesAdd3.Attributes.Append(hcaStaticTypesMimeType3);
			hceStaticTypesAdd3.Attributes.Append(hcaStaticTypesEnabled3);
			hceStaticTypesAdd4.Attributes.Append(hcaStaticTypesMimeType4);
			hceStaticTypesAdd4.Attributes.Append(hcaStaticTypesEnabled4);
			hceStaticTypes.AppendChild(hceStaticTypesAdd1);
			hceStaticTypes.AppendChild(hceStaticTypesAdd2);
			hceStaticTypes.AppendChild(hceStaticTypesAdd3);
			hceStaticTypes.AppendChild(hceStaticTypesAdd4);
			return hceStaticTypes;
		}

		/// <summary>
		/// Compressions the create dynamic types element.
		/// </summary>
		/// <param name="doc">The doc.</param>
		/// <returns></returns>
		protected XmlElement CompressionCreateDynamicTypesElement(XmlDocument doc) {
			var hceDynamicTypes = doc.CreateElement("dynamicTypes");
			var hceDynamicTypesAdd1 = doc.CreateElement("add");
			var hceDynamicTypesAdd2 = doc.CreateElement("add");
			var hceDynamicTypesAdd3 = doc.CreateElement("add");
			var hceDynamicTypesAdd4 = doc.CreateElement("add");
			var hcaDynamicTypesMimeType1 = doc.CreateAttribute("mimeType");
			var hcaDynamicTypesMimeType2 = doc.CreateAttribute("mimeType");
			var hcaDynamicTypesMimeType3 = doc.CreateAttribute("mimeType");
			var hcaDynamicTypesMimeType4 = doc.CreateAttribute("mimeType");
			var hcaDynamicTypesEnabled1 = doc.CreateAttribute("enabled");
			var hcaDynamicTypesEnabled2 = doc.CreateAttribute("enabled");
			var hcaDynamicTypesEnabled3 = doc.CreateAttribute("enabled");
			var hcaDynamicTypesEnabled4 = doc.CreateAttribute("enabled");
			hcaDynamicTypesMimeType1.Value = "text/*";
			hcaDynamicTypesMimeType2.Value = "message/*";
			hcaDynamicTypesMimeType3.Value = "application/*";
			hcaDynamicTypesMimeType4.Value = "*/*";
			hcaDynamicTypesEnabled1.Value = "true";
			hcaDynamicTypesEnabled2.Value = "true";
			hcaDynamicTypesEnabled3.Value = "true";
			hcaDynamicTypesEnabled4.Value = "false";
			hceDynamicTypesAdd1.Attributes.Append(hcaDynamicTypesMimeType1);
			hceDynamicTypesAdd1.Attributes.Append(hcaDynamicTypesEnabled1);
			hceDynamicTypesAdd2.Attributes.Append(hcaDynamicTypesMimeType2);
			hceDynamicTypesAdd2.Attributes.Append(hcaDynamicTypesEnabled2);
			hceDynamicTypesAdd3.Attributes.Append(hcaDynamicTypesMimeType3);
			hceDynamicTypesAdd3.Attributes.Append(hcaDynamicTypesEnabled3);
			hceDynamicTypesAdd4.Attributes.Append(hcaDynamicTypesMimeType4);
			hceDynamicTypesAdd4.Attributes.Append(hcaDynamicTypesEnabled4);
			hceDynamicTypes.AppendChild(hceDynamicTypesAdd1);
			hceDynamicTypes.AppendChild(hceDynamicTypesAdd2);
			hceDynamicTypes.AppendChild(hceDynamicTypesAdd3);
			hceDynamicTypes.AppendChild(hceDynamicTypesAdd4);
			return hceDynamicTypes;
		}

		/// <summary>
		/// Verifies the app is running.
		/// </summary>
		protected void VerifyAppIsRunning() {
			var numberOfTriesLeft = 3;
			bool successFirstRun = false;
			HttpWebResponse response = null;
			var urlPublicSvc = string.Format("{0}{1}/public.svc", ServicesProtocolPrefix, FrontEndUrl);
			OutputHelper.WriteToOutput(string.Format("{0}...verifying web application availability", Environment.NewLine));
			while (!successFirstRun && numberOfTriesLeft > 0) {
				try {
					var request = (HttpWebRequest)WebRequest.Create(urlPublicSvc);
					response = (HttpWebResponse)request.GetResponse();
				}
				catch (Exception ex) {
					OutputHelper.WriteToOutput(string.Format("verify application is running error: {0}. {1} tries left.", ex.Message, numberOfTriesLeft - 1));
					response = null;
				}
				finally {
					numberOfTriesLeft--;
					if (response != null) {
						if (response.StatusCode == HttpStatusCode.OK) {
							successFirstRun = true;
						}
						else {
							OutputHelper.WriteToOutput(string.Format("verify application is running web status code: {0} instead of 200. {1} tries left.",
								(int)response.StatusCode, numberOfTriesLeft));
						}
					}

					if (!successFirstRun) {
						Thread.Sleep(5000);
					}
				}
			}

			if (successFirstRun) {
				OutputHelper.WriteToOutput("...frontend web request was successful.");
			}
			else {
				var output = "frontend first run was unsuccessful.{0}manually run this link in the browser" +
								" and check that it loads normally and doesn't return errors.{0}do this before" +
								" initializing new tenant in the next step.{0}link: {1}";
				OutputHelper.WriteToOutput(string.Format(output, Environment.NewLine, urlPublicSvc), MessageType.Error);
			}
		}

		/// <summary>
		/// Checks the properties.
		/// </summary>
		protected override void ValidateArgumentsGlobally() {
			Helper.CheckSSLCertificate(SSLCertificate, SSLCertificatePassword);
			CheckDirectory(SourceLocation);
#if true  
            base.ValidateArgumentsGlobally();
#else  // let the ancestor handle these actions and more
			CheckDirectory(Path.Combine(SourceLocation, SourceLocationFolderFrontEnd));
			CheckDirectory(Path.Combine(SourceLocation, SourceLocationFolderScheduler));
			CheckDirectory(Path.Combine(SourceLocation, SourceLocationFolderEnergyAggregator));
#endif
			ValidateSchedulerCredentials();

		}

		/// <summary>
		/// Validates the scheduler credentials.
		/// </summary>
		protected void ValidateSchedulerCredentials() {
			var valid = true;
			string schedulerUsername;
			string schedulerPassword;
			GetOldSchedulerUserCredentials(out schedulerUsername, out schedulerPassword);
			if (schedulerUsername != null && schedulerUsername != SchedulerUsername) {
				valid = false;
				OutputHelper.WriteToOutput("SchedulerUsername must be set to " + schedulerUsername + ", it was already defined on this server.",
							  MessageType.Error);
			}
			if (schedulerPassword != null && schedulerPassword != SchedulerPassword) {
				valid = false;
				OutputHelper.WriteToOutput("SchedulerPassword must be set to original password as it was defined on previous installation.",
							  MessageType.Error);
			}
			if (!valid) {
				UsageHelper.Abort("Existing installation was found. Scheduler credentials must match.");
			}
		}

		/// <summary>
		/// Configures the application.
		/// </summary>
		/// <param name="siteName">Name of the site.</param>
		/// <param name="applicationVirtualPath">The application virtual path.</param>
		/// <param name="nvc">The NVC.</param>
		protected void ConfigureApplication(string siteName, string applicationVirtualPath, NameValueCollection nvc) {
			using (ServerManager serverManager = new ServerManager()) {
				var application = serverManager.Sites[siteName].Applications[applicationVirtualPath];
				foreach (var key in nvc.AllKeys) {
					application[key] = nvc[key];
				}
				serverManager.CommitChanges();
			}
		}

		/// <summary>
		/// Configures the auto start provider.
		/// </summary>
		/// <param name="autoStartProviderName">Name of the auto start provider.</param>
		/// <param name="type">The type.</param>
		protected void ConfigureAutoStartProvider(string autoStartProviderName, string type) {
			NameValueCollection nvc = new NameValueCollection();
			nvc["name"] = autoStartProviderName;
			nvc["type"] = type;
			CreateIISObject("system.applicationHost/serviceAutoStartProviders", nvc);
			OutputHelper.WriteToOutput(string.Format("...configure autostart provider : {0}", autoStartProviderName));
		}

		/// <summary>
		/// Configures the auto start providers.
		/// </summary>
		protected void ConfigureAutoStartProviders() {
			// create the 2 autostart providers
			ConfigureAutoStartProvider(SchedulerAutoStartProviderName, "Vizelia.FOL.ConcreteProviders.QuartzSchedulerAutoStartProvider, Vizelia.FOL.ConcreteProviders");
			ConfigureAutoStartProvider(FrontEndAutoStartProviderName, "Vizelia.FOL.ConcreteProviders.FrontendAutoStartProvider, Vizelia.FOL.ConcreteProviders");
		}

		/// <summary>
		/// Configures a site for HTTPS.
		/// </summary>
		/// <param name="siteName">Name of the site.</param>
		protected void ConfigureHttps(string siteName) {
			using (ServerManager serverManager = new ServerManager()) {
				var site = serverManager.Sites[siteName];
				// skip is already https binding configured
				foreach (var b in site.Bindings) {
					if (b.Protocol == "https") {
						return;
					}
				}
				// install the certificate in the store
				X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
				store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadWrite);
				X509Certificate2 certificate = new X509Certificate2(SSLCertificate, SSLCertificatePassword);
				store.Add(certificate);
				// set the IIS website to use the certificate for SSL
				var binding = site.Bindings.Add("*:443:", certificate.GetCertHash(), store.Name);
				binding.Protocol = "https";
				store.Close();

				serverManager.CommitChanges();
			}
		}


		/// <summary>
		/// Configures the site.
		/// </summary>
		/// <param name="siteName">Name of the site.</param>
		/// <param name="nvc">The NVC.</param>
		protected void ConfigureSite(string siteName, NameValueCollection nvc) {
			using (ServerManager serverManager = new ServerManager()) {
				var site = serverManager.Sites[siteName].Applications[0];
				foreach (var key in nvc.AllKeys) {
					site[key] = nvc[key];
				}
				serverManager.CommitChanges();
			}
			OutputHelper.WriteToOutput(string.Format("...configure site : {0}", siteName));
		}

		/// <summary>
		/// Creates and configure the IIS web sites required for the application.
		/// </summary>
		protected void ConfigureSites() {
			NameValueCollection nvc;
			string target = "";
			string source = "";

			OutputHelper.WriteToOutput(string.Empty);
			nvc = new NameValueCollection();
			nvc["applicationPool"] = FrontEndName;
			nvc["serviceAutoStartEnabled"] = "true";
			nvc["serviceAutoStartProvider"] = FrontEndAutoStartProviderName;
			DeleteSite(FrontEndName);
			DeletePool(nvc["applicationPool"]);
			target = Path.Combine(TargetDirectory, FrontEndName);
			source = Path.Combine(SourceLocation, SourceLocationFolderFrontEnd);
			CopyFiles(source, target);
			CreateApplicationPool(nvc["applicationPool"]);
			ConfigureAppPoolRecyling(nvc["applicationPool"], TimeSpan.FromHours(12), TimeSpan.FromHours(24), 0);
			CreateSite(FrontEndName, FrontEndUrl, target);
			ConfigureSSLOnlyInIIS();
			ConfigureSite(FrontEndName, nvc);
			ConfigureWebConfig(target, false, ConfigType.FrontEnd);

			OutputHelper.WriteToOutput(string.Empty);
			nvc = new NameValueCollection();
			nvc["applicationPool"] = SchedulerName;
			nvc["serviceAutoStartEnabled"] = "true";
			nvc["serviceAutoStartProvider"] = SchedulerAutoStartProviderName;
			DeleteSite(SchedulerName);
			DeletePool(nvc["applicationPool"]);
			target = Path.Combine(TargetDirectory, SchedulerName);
			source = Path.Combine(SourceLocation, SourceLocationFolderScheduler);
			CopyFiles(source, target);
			CreateApplicationPool(nvc["applicationPool"]);
			ConfigureAppPoolRecyling(nvc["applicationPool"], TimeSpan.FromMinutes(20), TimeSpan.FromHours(24), 0);
			CreateSite(SchedulerName, SchedulerUrl, target);
			ConfigureSite(SchedulerName, nvc);
			ConfigureWebConfig(target, true, ConfigType.Scheduler);

			OutputHelper.WriteToOutput(string.Empty);
			nvc = new NameValueCollection();
			nvc["applicationPool"] = EnergyAggregatorName;
			//nvc["EnabledProtocols"] = "http,net.tcp";
			DeleteSite(EnergyAggregatorName);
			DeletePool(nvc["applicationPool"]);
			target = Path.Combine(TargetDirectory, EnergyAggregatorName);
			source = Path.Combine(SourceLocation, SourceLocationFolderEnergyAggregator);
			CopyFiles(source, target);
			CreateApplicationPool(nvc["applicationPool"]);
			ConfigureAppPoolRecyling(nvc["applicationPool"], new TimeSpan(0), new TimeSpan(0), 0);
			CreateSite(EnergyAggregatorName, EnergyAggregatorUrl, target);
			ConfigureSite(EnergyAggregatorName, nvc);
			ConfigureWebConfig(target, true, ConfigType.EnergyAggregator);
		}

		/// <summary>
		/// Configures the tenant data.
		/// </summary>
		protected void ConfigureTenant() {
            string consoleResponse = "";
			if (!AutoInitializeTenant.HasValue) {
				OutputHelper.WriteToOutput(string.Format("{1}Do you want to initialize data for tenant {0}? Y/N", TenantName, Environment.NewLine), silentMode: false);
				consoleResponse = Console.ReadLine();
			}
			else if (!AutoInitializeTenant.Value) {
				var output = "You chose not to initialize the Tenant. If you want to manually initialize, generate a guid, and browse to here{0}http://{1}/public.svc/InitializeTenant?operationId=<Generated guid>";
				OutputHelper.WriteToOutput(string.Format(output, Environment.NewLine, FrontEndUrl), MessageType.Message);
				return;
			}
			else
				consoleResponse = "Y";
			if (consoleResponse != null && consoleResponse.ToUpper() == "Y") {
				bool success = false;
				int numberOfTriesLeft = 3;
				HttpWebResponse response = null;

				while (!success && numberOfTriesLeft > 0) {
					try {
						var request =
								(HttpWebRequest)WebRequest.Create(
								string.Format("{0}{1}/public.svc/InitializeTenant?operationId={2}&adminPassword={3}&tenantName={4}",
								ServicesProtocolPrefix, FrontEndUrl, Guid.NewGuid().ToString(), AdminPassword, TenantName));
						response =
							(HttpWebResponse)request.GetResponse();
					}
					catch (Exception ex) {
						OutputHelper.WriteToOutput(string.Format("configure tenant error: {0}. {1} tries left.", ex.Message, numberOfTriesLeft - 1));
						response = null;
					}
					finally {
						numberOfTriesLeft--;
						if (response != null) {
							if (response.StatusCode == HttpStatusCode.Accepted) {
								success = true;
							}
							else {
								OutputHelper.WriteToOutput(string.Format("configure tenant web status code: {0} instead of 202. {1} tries left.", (int)response.StatusCode, numberOfTriesLeft));
							}
						}

						if (!success) {
							Thread.Sleep(5000);
						}
					}
				}

				if (success) {
					if (AutoInitializeTenantTimeout > 0) {
						OutputHelper.WriteToOutput("...tenant is being initialized, please wait.");
						using (var publicFactory = new ChannelFactory<IPublicWCF>(new BasicHttpBinding(), new EndpointAddress(new Uri(string.Format("http://{0}/public.svc/soap", Url.TrimEnd('/'))))))
						using (
							var publicFactoryHttps = new ChannelFactory<IPublicWCF>(new BasicHttpBinding(BasicHttpSecurityMode.Transport),
								new EndpointAddress(new Uri(string.Format("https://{0}/public.svc/soap", Url.TrimEnd('/')))))) {
							var clientPublic = publicFactory.CreateChannel();
							var clientPublicHttps = publicFactoryHttps.CreateChannel();

							CheckTenantInitializationStatus(clientPublicHttps, clientPublic);
						}
					}
					else {
						OutputHelper.WriteToOutput("...tenant initialization started, please check email or server trace for completion message");
					}
				}
				else {
					var output = "Failed to initialize Tenant. Generate a guid, and manually browse to here{0}http://{1}/public.svc/InitializeTenant?operationId=<Generated guid>";
					OutputHelper.WriteToOutput(string.Format(output, Environment.NewLine, FrontEndUrl), MessageType.Error);
				}
			}
		}

		private void CheckTenantInitializationStatus(IPublicWCF clientPublicHttps, IPublicWCF clientPublic) {
			var useHttps = false;
			bool isInitialized;
			try {
				isInitialized = clientPublic.IsTenantInitialized();
			}
			catch {
				isInitialized = clientPublicHttps.IsTenantInitialized();
				useHttps = true;
			}

			var startTime = DateTime.Now;
			var maxTimespan = TimeSpan.FromMinutes(AutoInitializeTenantTimeout);
			var timeoutOccurred = false;
			while (!isInitialized) {
				try {
					OutputHelper.WriteToOutput(".", newLine: false);
					isInitialized = useHttps ? clientPublicHttps.IsTenantInitialized() : clientPublic.IsTenantInitialized();
					if (DateTime.Now - startTime > maxTimespan) {
						timeoutOccurred = true;
						break;
					}
					Thread.Sleep(1000);
				}
				catch (Exception) {}
			}

			OutputHelper.WriteToOutput(string.Empty);
			if (timeoutOccurred) {
				throw new VizeliaException("Timeout occurred while checking tenant initialization status.");
			}
			
			OutputHelper.WriteToOutput("Tenant was initialized successfully.", MessageType.Success);
		}

		/// <summary>
		/// Configures the server to be UTC.
		/// </summary>
		protected void ConfigureUTC() {
			Helper.ExecuteCommand("tzutil /s \"UTC\"");
			OutputHelper.WriteToOutput("...configure UTC");
		}

		/// <summary>
		/// Creates an IIS application.
		/// </summary>
		/// <param name="siteName">Name of the site.</param>
		/// <param name="applicationVirtualPath">The application virtual path.</param>
		/// <param name="applicationPhysicalPath">The application physical path.</param>
		protected void CreateApplication(string siteName, string applicationVirtualPath, string applicationPhysicalPath) {
			using (var serverManager = new ServerManager()) {
				var site = serverManager.Sites[siteName];
				var virtualDirectory = site.Applications[applicationVirtualPath] ?? site.Applications.Add(applicationVirtualPath, applicationPhysicalPath);
				serverManager.CommitChanges();
			}
		}

		/// <summary>
		/// Creates an application pool.
		/// </summary>
		/// <param name="poolName">Name of the pool.</param>
		protected void CreateApplicationPool(string poolName) {
			var nvc = new NameValueCollection();
			nvc["name"] = poolName;
			nvc["managedRuntimeVersion"] = "v4.0";
			nvc["startMode"] = "AlwaysRunning";
			CreateIISObject("system.applicationHost/applicationPools", nvc);
			OutputHelper.WriteToOutput(string.Format("...create appPool : {0}", poolName));
		}

		/// <summary>
		/// Configures the app pool disable recycling.
		/// </summary>
		/// <param name="poolName">Name of the pool.</param>
		/// <param name="idleTimeout">The idle timeout.</param>
		/// <param name="periodRestart">The period restart.</param>
		protected void ConfigureAppPoolRecyling(string poolName, TimeSpan idleTimeout, TimeSpan periodRestart, long virtualMemoryLimit) {
			using (var serverManager = new ServerManager()) {
				ApplicationPool apppool = serverManager.ApplicationPools[poolName];
				apppool.ProcessModel.IdleTimeout = idleTimeout;
				//apppool.ProcessModel.LoadUserProfile = true;
				apppool.Recycling.PeriodicRestart.Time = periodRestart;
				apppool.Recycling.PeriodicRestart.Schedule.Delete();
				apppool.Recycling.PeriodicRestart.Memory = virtualMemoryLimit;
				apppool.Recycling.PeriodicRestart.PrivateMemory = 0;
				apppool.Recycling.PeriodicRestart.Requests = 0;
				serverManager.CommitChanges();
			}
		}

		/// <summary>
		/// Creates a generic object in IIS
		/// </summary>
		/// <param name="sectionName">Name of the section.</param>
		/// <param name="nvc">The NVC.</param>
		protected void CreateIISObject(string sectionName, NameValueCollection nvc) {
			using (var serverManager = new ServerManager()) {
				Configuration config = serverManager.GetApplicationHostConfiguration();
				ConfigurationSection section = config.GetSection(sectionName);
				ConfigurationElementCollection sectionCollection = section.GetCollection();
				foreach (var item in sectionCollection) {
					if (string.Compare(item.Attributes["name"].Value.ToString(), nvc["name"], true) == 0)
						return;
				}

				ConfigurationElement addElement = sectionCollection.CreateElement("add");
				foreach (var key in nvc.AllKeys) {
					addElement[key] = nvc[key];
				}
				sectionCollection.Add(addElement);
				serverManager.CommitChanges();
			}
		}

		/// <summary>
		/// Creates an IIS website
		/// </summary>
		/// <param name="siteName">Name of the site.</param>
		/// <param name="url">The URL.</param>
		/// <param name="path">The path.</param>
		/// <param name="port">The port.</param>
		/// <param name="defaultTcpBinding">if set to <c>true</c> [default TCP binding].</param>
		protected void CreateSite(string siteName, string url, string path, int port = 80, bool defaultTcpBinding = false) {
			using (var serverManager = new ServerManager()) {

				// if site does not exist then create it
				Site site = serverManager.Sites[siteName];
				if (site != null)
					site.Delete();

				site = serverManager.Sites.Add(siteName, "http", string.Format("*:{0}:{1}", port.ToString(), url), path);

				// create the https endpoint with site
				var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
				store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadWrite);
				var certificate = new X509Certificate2(SSLCertificate, SSLCertificatePassword, X509KeyStorageFlags.PersistKeySet);
				if (!store.Certificates.Contains(certificate))
					store.Add(certificate);


				var binding = site.Bindings.Add("*:443:" + url, certificate.GetCertHash(), store.Name);
				binding.Protocol = "https";

				store.Close();

				// create the tcp endpoint
				if (defaultTcpBinding) {
					site.Bindings.Add("808:*", "net.tcp");
				}

				try {
					serverManager.CommitChanges();
				}
				catch (Exception ex) {
					if (ex.Message.Contains("0x80070520")) {
						OutputHelper.WriteToOutput("Certificate Error. Consult system administrator/support about manually importing the certificate from the IIS console.", MessageType.Error);
					}
					throw;
				}

				Helper.ConfigureHostName("127.0.0.1", url);
			}
			OutputHelper.WriteToOutput(string.Format("...create site : {0}", siteName));
		}

		/// <summary>
		/// Deletes the pool.
		/// </summary>
		/// <param name="appPoolName">Name of the app pool.</param>
		protected void DeletePool(string appPoolName) {
			using (var serverManager = new ServerManager()) {
				var appPool = serverManager.ApplicationPools[appPoolName];
				if (appPool != null) {
					serverManager.ApplicationPools.Remove(appPool);
					serverManager.CommitChanges();
					OutputHelper.WriteToOutput(string.Format("...delete pool : {0}", appPoolName));
				}
			}

		}

		/// <summary>
		/// Creates an IIS website
		/// </summary>
		/// <param name="siteName">Name of the site.</param>
		protected void DeleteSite(string siteName) {
			using (var serverManager = new ServerManager()) {

				// if site does not exist then create it
				Site site = serverManager.Sites[siteName];
				if (site != null) {
					site.Delete();
					serverManager.CommitChanges();
					OutputHelper.WriteToOutput(string.Format("...delete site : {0}", siteName));
				}

			}

		}

		/// <summary>
		/// Gets the action description.
		/// </summary>
		/// <returns></returns>
		protected override string GetActionDescription() {
			return "Install Utility - Installs a clean copy of the application.";
		}
	}
}
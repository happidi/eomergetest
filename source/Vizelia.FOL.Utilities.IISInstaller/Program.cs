﻿namespace Vizelia.FOL.Utilities.IISInstaller {
	
	/// <summary>
	/// Setup a developer environment with the relvant websites, application pools and autostartproviders
	/// </summary>
	internal class Program {

		/// <summary>
		/// Routes action requests to different models
		/// </summary>
		/// <param name="args">The args.</param>
		private static void Main(string[] args) {
			InstallerAction action = new MainInstallerAction();
			action.DoAction(args);
		}
	}
}
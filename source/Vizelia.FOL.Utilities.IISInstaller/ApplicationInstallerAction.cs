﻿using System;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using Microsoft.Web.Administration;
using Microsoft.Win32;
using Vizelia.FOL.Common;
using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Utilities.CommandLine;

namespace Vizelia.FOL.Utilities.IISInstaller {
	/// <summary>
	/// Setup a developer environment with the relvant websites, application pools and autostartproviders
	/// </summary>
	internal abstract class ApplicationInstallerAction : InstallerAction {

		// Const
		//protected const string IsolatedProviderName = "IIS7IsolatedTenantProvisioningProvider";
		protected const string SharedProviderName = "SharedTenantProvisioningProvider";
		protected const string KeySSL2 = "SSL 2.0";
		protected const string KeySSL3 = "SSL 3.0";
		protected const string KeyServer = "Server";

		protected string IsolationProvider = SharedProviderName;
		protected string TenantName { get; set; }
		protected string Url { get; set; }
		protected string AdminTenantEmail { get; set; }
		protected string SMTPFrom { get; set; }
		protected string HelpUrl { get; set; }
		protected string DBPrefix { get; set; }
		protected string DBPwd { get; set; }
		protected string DBServer { get; set; }
		protected string DBUser { get; set; }
		protected string TargetDirectory { get; set; }
		protected string SchedulerUsername { get; set; }
		protected string SchedulerPassword { get; set; }
		protected string SourceLocation { get; set; }
		protected string SQLSourceDirectory { get; set; }
		protected string VizeliaLicenceFile { get; set; }
		
	/// <summary>
	/// DotNetCharting licenses are a protected resource and must only be assigned
	/// to servers for which the license was intended.
	/// </summary>
	/// <value>
	/// <para>When null, do not change the current setup. Null is used when there is no /DotNetChartingLicense parameter.</para>
	/// <para>When "", remove the license file and entry in web.config.</para>
	/// <para>When a file path, use that to establish the license both in the destination charting folder
	/// and in web.config.</para>
	/// <para>When a token like '{CODE}', establish the license from a stored resource associated with the token.</para>
	/// </value>
		protected string DotNetChartingLicence { get; set; }	//NOTE: Used Licence instead of License to conform with existing VizeliaLicenceFile
		protected bool Protect { get; set; }
		protected bool Debug { get; set; }
		protected bool SSLOnly { get; set; }
		protected bool DisableOldSSLProtocols { get; set; }
		protected bool ForgotPassword { get; set; }

/// <summary>
/// Determines if and how the ConnectionStrings section of web.config is modified.
/// </summary>
/// <value>
/// <para>* Login (the default) - Installs full login in connection strings.</para>
/// <para>* Trust - Install Trust_Connection in connection string.
/// Requires the App Pool Users to be registered as logins and users.</para>
/// <para>* No - Take no action on the connection strings in the file.</para>
/// </value>
        protected ModConnectionType ModConnection { get; set; }

/// <summary>
/// Used by the ModConnection property.
/// </summary>
        public enum ModConnectionType
        {
/// <summary>
/// Install full login in connection string
/// </summary>
            Login,
/// <summary>
/// Install Trust_Connection in connection string.
/// Requires the App Pool Users to be registered as logins and users.
/// </summary>
            Trust,
/// <summary>
/// Take no action on the connection strings in the file.
/// </summary>
            No
        }
		/// <summary>
		/// When true, do setup only suitable for production mode like
		/// installing production mode Bing maps license.
		/// </summary>
		protected bool ProductionMode { get; set; }

		
		// calculated
		protected string FrontEndName { get; set; }
		protected string FrontEndUrl { get; set; }
		protected string EnergyAggregatorName { get; set; }
		protected string EnergyAggregatorUrl { get; set; }
		protected string SchedulerName { get; set; }
		protected string SchedulerUrl { get; set; }
		protected string SourceLocationFolderFrontEnd { get; set; }
		protected string SourceLocationFolderScheduler { get; set; }
		protected string SourceLocationFolderEnergyAggregator { get; set; }
		protected string ServicesProtocolPrefix { get { return SSLOnly ? "https://" : "http://"; } }

		/// <summary>
		/// Initializes a new instance of the <see cref="ApplicationInstallerAction"/> class.
		/// </summary>
		protected ApplicationInstallerAction() {
			var argSourceLocation = new Argument() {
				Syntax = "<string>",
				Help =
					"Directory that contains the sources files. Should contain Vizelia.FOL.Web, Vizelia.FOL.Web.Scheduler, Vizelia.FOL.EnergyAggregator.",
				Type = ArgumentType.Mandatory,
				Name = "SourceDirectory",
				Map = s => SourceLocation = s
			};
			argSourceLocation.Validate = s => CheckDirectory(argSourceLocation.Value);
			var argSQLSourceDirectory = new Argument() {
				Syntax = "<string>",
				Help =
					"Directory that contains the sources files for database. Should contain scripts for schema compare. If parameter is not set installer will not attempt to install DB.",
				Type = ArgumentType.Optional,
				Name = "SQLSourceDirectory",
				Map = s => SQLSourceDirectory = s,
				HideDefaultValue = true
			};
			var argDisableOldSSLProtocols = new Argument() {
				Syntax = "[true|false]",
				Help =
					"Disables SSL 2.0 and SSL 3.0 protocols in the server. Requires manual restart.",
				Type = ArgumentType.Optional,
				Name = "DisableOldSSLProtocols",
				Map = s => DisableOldSSLProtocols = bool.Parse(s),
				DefaultValue = "true",
				HideDefaultValue = false
			};

			Arguments.Add(argSourceLocation.Name, argSourceLocation);
			Arguments.Add(argSQLSourceDirectory.Name, argSQLSourceDirectory);
			Arguments.Add(argDisableOldSSLProtocols.Name, argDisableOldSSLProtocols);
		}

		/// <summary>
		/// Calculates the properties.
		/// </summary>
		protected override void CalculateProperties() {
			base.CalculateProperties();

			FrontEndName = TenantName;
			FrontEndUrl = Url;
			EnergyAggregatorName = TenantName + "_ea";
			if (Url.IndexOf(".") > 0)
				EnergyAggregatorUrl = Url.Insert(Url.IndexOf("."), "_ea");
			else
				EnergyAggregatorUrl = Url + "_ea";
			SchedulerName = "Scheduler";
			SchedulerUrl = "Scheduler";

			// will not change unless we change the project name.
			SourceLocationFolderFrontEnd = "Vizelia.FOL.Web";
			SourceLocationFolderScheduler = "Vizelia.FOL.Web.Scheduler";
			SourceLocationFolderEnergyAggregator = "Vizelia.FOL.EnergyAggregator";
		}

		/// <summary>
		/// Validates the arguments globally.
		/// </summary>
		protected override void ValidateArgumentsGlobally() {
            if (ModConnection == ModConnectionType.Login && String.IsNullOrWhiteSpace(DBPwd))
                throw new ArgumentException("Must supply a password in /DbPwd. Ensure it is unique and strong.");
            if (ModConnection == ModConnectionType.Login && String.IsNullOrWhiteSpace(DBUser))
                throw new ArgumentException("Must supply a user name for a login into a database in /DbUser.");

			base.ValidateArgumentsGlobally();

			if (!CheckDirectory(Path.Combine(SourceLocation, SourceLocationFolderFrontEnd))) {
				throw new VizeliaException("Frontend source directory was not found");
			}
			if (!CheckDirectory(Path.Combine(SourceLocation, SourceLocationFolderScheduler))) {
				throw new VizeliaException("Scheduler source directory was not found");
			}
			if (!CheckDirectory(Path.Combine(SourceLocation, SourceLocationFolderEnergyAggregator))) {
				throw new VizeliaException("Energy Aggregator source directory was not found");
			}
		}
		
		/// <summary>
		/// Checks the directory for existence.
		/// </summary>
		/// <param name="path">The path.</param>
		protected bool CheckDirectory(string path) {
			if (!Directory.Exists(path)) {
				OutputHelper.WriteToOutput(string.Format("Could not find directory : {0}", path), MessageType.Error);
				return false;
			}
			return true;
		}

		/// <summary>
		/// Checks the directory for existence.
		/// </summary>
		/// <param name="path">The path.</param>
		protected bool CheckFile(string path) {
			if (!File.Exists(path)) {
				OutputHelper.WriteToOutput(string.Format("Could not find file : {0}", path), MessageType.Error);
				return false;
			}
			return true;
		}
		
		/// <summary>
		/// Disable deprecated ssl protocols for the server: ssl2, ssl3
		/// </summary>
		protected void DisableOldSSLProtocolsFunc() {
			if (DisableOldSSLProtocols) {
				try {
					var protocolsKey =
						Registry.LocalMachine.OpenSubKey(@"System\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols", true);

					DisableSSLProtocolInRegistry(protocolsKey, KeySSL2);
					DisableSSLProtocolInRegistry(protocolsKey, KeySSL3);
				}
				catch (Exception ex) {
					OutputHelper.WriteToOutput(string.Format("configure ssl protocols in registry error: {0}", ex.Message),
						MessageType.Error);
				}
			}
		}

		/// <summary>
		/// Helper for disabling deprecated ssl protocols for the server
		/// </summary>
		private void DisableSSLProtocolInRegistry(RegistryKey protocolsKey, string protocol) {
			var protocolSpecificKey = protocolsKey.OpenSubKey(protocol, true) ?? protocolsKey.CreateSubKey(protocol);
			var serverKey = protocolSpecificKey.OpenSubKey(KeyServer, true) ?? protocolSpecificKey.CreateSubKey(KeyServer);
			serverKey.SetValue("Enabled", 0, RegistryValueKind.DWord);
		}

		/// <summary>
		/// Configures the web config.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="onlyDns">if set to <c>true</c> [only DNS].</param>
		/// <param name="configType">Type of the config.</param>
		protected void ConfigureWebConfig(string path, bool onlyDns, ConfigType configType) {
			string logFolder = Path.Combine(TargetDirectory, "logs");
			string tempFolder = Path.Combine(TargetDirectory, "temp");
			string mailFolder = Path.Combine(TargetDirectory, "maillogs");


			string applicationName = TenantName;

			string adminTenantEmail = AdminTenantEmail;
			string fromMail = SMTPFrom;


			const string applicationLabel = "Energy Operation";

			var doc = new XmlDocument();
			string configFile = Path.Combine(path, "web.config");
			doc.Load(configFile);


			XmlElement Root = doc.DocumentElement;
			if (Root == null)
				return;

			if (!onlyDns) {

				Directory.CreateDirectory(logFolder);
				var rollingFlatFile = Root.SelectSingleNode("//loggingConfiguration[@name=\"Logging Application Block\"]/listeners/add[@name=\"Rolling Flat File Error Trace Listener\"]");
				if (rollingFlatFile != null)
					rollingFlatFile.Attributes.GetNamedItem("fileName").Value = Path.Combine(logFolder, "EnergyOperationErrors.log");

				Directory.CreateDirectory(tempFolder);

				string dotNetChartingLicence = PrepareDotNetChartingLicense();

				var vizeliaConfiguration = Root["VizeliaConfiguration"];
				if (vizeliaConfiguration != null) {
					var deployment = vizeliaConfiguration["deployment"];
					if (deployment != null) {
						deployment.Attributes.GetNamedItem("tempDirectory").Value = tempFolder;
						if (dotNetChartingLicence != null)	// null is a flag to skip this.
							if (dotNetChartingLicence != String.Empty)
								deployment.Attributes.GetNamedItem("dotNetChartingLicence").Value = dotNetChartingLicence;
							else
								deployment.Attributes.RemoveNamedItem("dotNetChartingLicence"); 
						deployment.Attributes.GetNamedItem("applicationName").Value = applicationName;
						deployment.Attributes.GetNamedItem("adminTenantEmail").Value = adminTenantEmail;
						deployment.Attributes.GetNamedItem("helpUrl").Value = HelpUrl;
					}
					var mail = vizeliaConfiguration["mail"];
					if (mail != null)
						mail.Attributes.GetNamedItem("logPath").Value = mailFolder;
					var authentication = vizeliaConfiguration["authentication"];
					if (authentication != null) {
						authentication.Attributes.GetNamedItem("allowForgotPassword").Value = ForgotPassword.ToString(CultureInfo.InvariantCulture).ToLower();
					}
				}
				string fromAddress = null;
				if (!string.IsNullOrEmpty(fromMail)) {
					fromAddress = applicationLabel + " ({property(MachineName)}) <" + fromMail + ">";
				}

				var smtp = Root.SelectSingleNode("//system.net/mailSettings/smtp");
				if (smtp != null && !string.IsNullOrEmpty(fromMail)) smtp.Attributes.GetNamedItem("from").Value = fromMail;

				var mappingReportEmail = Root.SelectSingleNode("//loggingConfiguration[@name=\"Logging Application Block\"]/listeners/add[@name=\"MappingReport Email TraceListener\"]");
				if (mappingReportEmail != null) {
					mappingReportEmail.Attributes.GetNamedItem("toAddress").Value = adminTenantEmail;
					if (!string.IsNullOrEmpty(fromAddress)) {
						mappingReportEmail.Attributes.GetNamedItem("fromAddress").Value = fromAddress;
					}
				}

				var traceEmail = Root.SelectSingleNode("//loggingConfiguration[@name=\"Logging Application Block\"]/listeners/add[@name=\"Vizelia Email TraceListener\"]");
				if (traceEmail != null) {
					traceEmail.Attributes.GetNamedItem("toAddress").Value = adminTenantEmail;
					if (!string.IsNullOrEmpty(fromAddress)) {
						traceEmail.Attributes.GetNamedItem("fromAddress").Value = fromAddress;
					}
				}
				var compilation = Root.SelectSingleNode("//system.web/compilation");
				if (compilation != null) compilation.Attributes.GetNamedItem("debug").Value = Debug ? "true" : "false";

				var eaEndPoint = Root.SelectSingleNode("//system.serviceModel/client/endpoint[@name=\"SeparatedEnergyAggregator\"]");
				if (eaEndPoint != null)
					eaEndPoint.Attributes.GetNamedItem("address").Value = "http://" + EnergyAggregatorUrl + "/EnergyAggregator.svc";

				var publicEndPoint = Root.SelectSingleNode("//system.serviceModel/client/endpoint[@name=\"PublicProxy\"]");
				if (publicEndPoint != null)
					publicEndPoint.Attributes.GetNamedItem("address").Value = ""; // "http://" + FrontEndUrl + "/Public.svc/soap";

                if (SSLOnly)    // flips enabled=true. Never flips it back to false
                {
                // SSLOnly uses the URL rewrite feature to do the work.
                // It assumes <rule name="Redirect HTTP to HTTPS"> exists in the web.config file because we put it there on new installs.
                // Sites prior to EO 2.5 don't have this unless Prof Services already inserted it on sites that needed it.
                // So we don't bother creating that rule if its missing.
                    var redirectHttpRule = Root.SelectSingleNode("//system.webServer/rewrite/rules/rule[@name=\"Redirect HTTP to HTTPS\"]");
                    if (redirectHttpRule != null)
                    {
                        // if no enabled property exists, it already means its enabled.
                        // Only modify it to true if present
                        XmlAttribute enabledAtt = redirectHttpRule.Attributes["enabled"];
                        if (enabledAtt != null)
                            enabledAtt.Value = "true";
                    }
                }
			}

            ConfigureConnectionStrings(Root);

			if (configType == ConfigType.FrontEnd || configType == ConfigType.EnergyAggregator) {
				try {
					Root["VizeliaConfiguration"]["deployment"].Attributes.GetNamedItem("VizeliaLicence").Value = VizeliaLicenceFile;
				}
				catch (Exception ex) {
					OutputHelper.WriteToOutput("Error setting vizelia licence file in web configuration.", MessageType.Error);
					OutputHelper.WriteToOutput(ex.Message, MessageType.Error);
				}
			}

			if (configType == ConfigType.FrontEnd || configType == ConfigType.Scheduler) {
				SetSchedulerCredentials(doc);
			}

			if (configType == ConfigType.FrontEnd) {
				Root.GetElementsByTagName("VizeliaTenantProvisioningSection")[0].Attributes["defaultProvider"].Value = IsolationProvider;

				if (SSLOnly) {
					ConfigureSSLOnlyFrontEnd(Root);
				}
			}
			UpdateBingProviderKey(Root, doc);

			doc.Save(Path.Combine(path, "web.config"));
			OutputHelper.WriteToOutput(string.Format("...configure web.config in : {0}", path));

			// If protect is true then should encrypt, if it is false then should do nothing - not decrypt
			// lockdown.ProtectConfig(path, false) decryption - this option is reserved only to lockdown action and not install/update actions. 
			if (Protect) {
				var lockdown = new Lockdown();
				lockdown.ProtectConfig(path, true, true);
			}
		}

		/// <summary>
		/// Configures the SSL only front end.
		/// </summary>
		/// <param name="Root">The root.</param>
		private void ConfigureSSLOnlyFrontEnd(XmlElement Root) {

			ConfigureAuthCookieSsl(Root);

			var serviceMetadataNode =
				Root.SelectSingleNode("system.serviceModel/behaviors/serviceBehaviors/behavior/serviceMetadata");
			serviceMetadataNode.Attributes["httpGetEnabled"].Value = "false";
#if false // User Story 3591. This is no longer used because Url Rewrite in the <rewrite> section of the web.config file maps it
			var services = Root.SelectNodes("system.serviceModel/services/service");
			for (int i = 0; i < services.Count; i++) {
				var serviceNode = services[i];
				int indexOfEndpointNamedJson = -1;
				int indexOfEndpointNamedSoapHttp = -1;
				for (int j = 0; j < serviceNode.ChildNodes.Count; j++) {
					switch (serviceNode.ChildNodes[j].Attributes["name"].Value) {
						case "json":
							indexOfEndpointNamedJson = j;
							break;
						case "xml":
							serviceNode.ChildNodes[j].Attributes["bindingConfiguration"].Value = "poxHttpsBinding";
							break;
						case "soap":
							indexOfEndpointNamedSoapHttp = j;
							break;
						case "mex":
							serviceNode.ChildNodes[j].Attributes["binding"].Value = "mexHttpsBinding";
							break;
						default:
							break;
					}
				}

				XmlNode soapHttpChild = serviceNode.ChildNodes[indexOfEndpointNamedSoapHttp];
				XmlNode jsonChild = serviceNode.ChildNodes[indexOfEndpointNamedJson];

				serviceNode.RemoveChild(soapHttpChild);
				serviceNode.RemoveChild(jsonChild);
			}
#endif
        }

		/// <summary>
		/// Configures the authentication cookie to use SSL.
		/// </summary>
		/// <param name="root">The root.</param>
		private void ConfigureAuthCookieSsl(XmlElement root) {
			try {
				var formsAuthenticationNode = root.SelectSingleNode("system.web/authentication/forms");
				formsAuthenticationNode.Attributes["requireSSL"].Value = "true";
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput(string.Format("Error at configuring auth cookie SSL. {0}", ex.Message), MessageType.Warning);
			}
		}

/// <summary>
/// Modifies the connectionStrings to replace the connectionString property with 
/// the user name from DbUser and password from DbPwd. This method also tests the connection strings.
/// </summary>
/// <remarks>
/// <para>The default web.config file has connectionString properties that use a trusted connection.
/// This will be overwritten.</para>
/// <para>If the KeepConnection property is true, no changes are made here.</para>
/// <para>If DbPwd is null or "", it is an error. The error handling should be checked 
/// before any work is done, but its also done here.</para>
/// </remarks>
/// <param name="root"></param>
        private void ConfigureConnectionStrings(XmlElement root)
        {
            if (ModConnection == ModConnectionType.No)
                return;
            if ((ModConnection == ModConnectionType.Login) && (String.IsNullOrWhiteSpace(DBPwd)))
                throw new InvalidOperationException("Must specify a password in /DbPwd.");

            var connectionString = root.SelectSingleNode("//connectionStrings");
            if (connectionString != null)
                foreach (XmlNode dns in connectionString.ChildNodes)
                {
                    if (dns.Attributes.GetNamedItem("providerName").Value == "System.Data.SqlClient" && !dns.Attributes.GetNamedItem("connectionString").Value.StartsWith("LDAP"))
                    {
                        var nameAttribute = dns.Attributes.GetNamedItem("name").Value;
						string name = nameAttribute;
                        name = name.Replace("dns_", "");
                        if (name == "vizelia_meter_data")
                            name = DBPrefix + "_" + "code";
						else if (nameAttribute == "EnergyModel")
							name = DBPrefix + "_" + "code";
                        else
                        {
                            name = name.Replace("vizelia", DBPrefix);
                        }
						
                        var connString = (ModConnection == ModConnectionType.Login) ?
                                String.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3}", DBServer, name, DBUser, DBPwd) :
                                String.Format("Data Source={0};Initial Catalog={1};Trusted_Connection=Yes", DBServer, name);
						if (nameAttribute == "EnergyModel") {
							connString += ";multipleactiveresultsets=True;App=EntityFramework";
						}
	                    dns.Attributes.GetNamedItem("connectionString").Value = connString;

						try {
							using (var conn = new SqlConnection(connString)) {
								conn.Open();
							}
						}
						catch (Exception) {
							OutputHelper.WriteToOutput(string.Format("Cannot connect to DB: {0}", connString), MessageType.Warning);
						}
                    }
                }


        }

		/// <summary>
		/// Sets the scheduler credentials.
		/// </summary>
		/// <param name="doc">The doc.</param>
		protected void SetSchedulerCredentials(XmlDocument doc) {
			try {
				var authentication = doc.DocumentElement["VizeliaConfiguration"]["authentication"];
				authentication.Attributes["schedulerUsername"].Value = SchedulerUsername;
				authentication.Attributes["schedulerPassword"].Value = SchedulerPassword;
			}
			catch (Exception ex) {
				OutputHelper.WriteToOutput("Error at setting scheduler credentials: ", MessageType.Error);
				OutputHelper.WriteToOutput(ex.Message, MessageType.Error);
			}
		}

		/// <summary>
		/// Copies the files using xcopy.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="target">The target.</param>
		/// <param name="removeTargetBeforeCopy">if set to <c>true</c> removes target first.</param>
		protected void CopyFiles(string source, string target, bool removeTargetBinDirBeforeCopy = true) {
			ExecutionResult result;
			var binDir = Path.Combine(target, "Bin");
            if (removeTargetBinDirBeforeCopy && Directory.Exists(binDir)) {
                Helper.ExecuteCommand(String.Format("rd \"{0}\" /Q /S", binDir));
			}
		    
			result =
				Helper.ExecuteCommand(String.Format("xcopy \"{0}\" \"{1}\" /E /I /Q /H /R /Y", source, target));
			OutputHelper.WriteToOutput(string.Format("...copy {0} -> {1}", source, target));
		}

		/// <summary>
		/// Delete the directory at the given path.
		/// </summary>
		/// <param name="target">The target file path. Expects a valid path.</param>
		/// <param name="silent">When false, output info about the deletion. When
		/// true, do not report anything to the console. If not supplied, it is the same as false.</param>
		protected void RemoveDirectory(string target, bool silent = false) {
			Helper.ExecuteCommand(String.Format("rd \"{0}\" /Q /S", target));

			if (!silent)
				OutputHelper.WriteToOutput(string.Format("...remove directory {0}", target));
		}

		/// <summary>
		/// Configures the SSL only in IIS.
		/// </summary>
		protected void ConfigureSSLOnlyInIIS() {
#if false   // User Story 3591. Need both ports to allow <rewrite> to redirect to https
			try {
				using (var manager = new ServerManager()) {
					var site = manager.Sites[FrontEndName];
					if (SSLOnly) {
						var bindingHttp = site.Bindings.FirstOrDefault(binding => binding.BindingInformation.Contains(":80:"));
						site.Bindings.Remove(bindingHttp);
						manager.CommitChanges();
						OutputHelper.WriteToOutput("...applied SSLOnly confuguration in IIS");
					}
				}
			}
			catch (Exception ex) {
				UsageHelper.Abort("Could not apply SSLOnly configuration to IIS. " + ex.Message);
			}
#endif
		}

		/// <summary>
		/// Configures the SQL.
		/// </summary>
		protected void ConfigureSQL() {
			if (SQLSourceDirectory == null) return;

			// remove tempdeploy if exists
			var tempDeployDirPath = Path.Combine(Path.GetPathRoot(Environment.CurrentDirectory), "tempdeploy");
			if (Directory.Exists(tempDeployDirPath)) {
				RemoveDirectory(tempDeployDirPath);
			}
			OutputHelper.WriteToOutput("...configuring db schema.");
//			var result = Helper.ExecuteCommand(string.Format("deploy.bat {0}", SQLSourceDirectory), shouldFlushOutputToConsole: true);
			string cmdline = string.Format("deploydb.bat /path:\"{0}\" /dbuser:{1} /dbpwd:{2} /dbprefix:{3} /dbserver:{4}", SQLSourceDirectory, DBUser, DBPwd, DBPrefix, DBServer);
			var result = Helper.ExecuteCommand(cmdline, shouldFlushOutputToConsole: true);


		}

		/// <summary>
		/// Adds or removes the DotNetCharting license file in the destination Charting folder.
		/// Returns a path that is used by the caller to modify the dotNetChartingLicense= attribute
		/// in the VizeliaConfiguration section of web.config.
		/// </summary>
		/// <remarks>
		/// <para>The DotNetChartingLicense property determines what happens.</para>
		/// <para>null - do nothing.</para>
		/// <para>"" - remove the file from the destination charting folder and entry in web.config.
		/// <para>file path - copy the file into the destination charting folder and update the entry in web.config.</para>
		/// Always deletes any previous license file before adding so only one will be in the charting folder.</para>
		/// </remarks>
		/// <returns>The license file path in the destination; empty string indicates delete the entry in web.config;
		/// null indicates do not change the existing (for /Action:Upgrade).</returns>
		protected virtual string PrepareDotNetChartingLicense() {
			if (DotNetChartingLicence == null)	// indicates no change to make
				return null;
			string destinationLicenseFileFolder = Path.Combine(TargetDirectory, "Charting");
//				Helper.ExecuteCommand(String.Format("rd \"{0}\" /S /Q", Path.Combine(TargetDirectory, "charting")), true);
			RemoveDirectory(destinationLicenseFileFolder, true);
			switch (DotNetChartingLicence) {
				case "":
					return String.Empty;	// remove the license info in web.config
/* if we store default licenses and use {CODE} tokens, this is an example. Be sure to add the resource...
				case "{835820}":
					string dotNetChartingLicence = Path.Combine(destinationLicenseFileFolder, "835820.lic");
					Directory.CreateDirectory(destinationLicenseFileFolder);
					var bw = new BinaryWriter(File.OpenWrite(dotNetChartingLicence));
					bw.Write(Resource._835820);
					bw.Flush();
					bw.Close();
					return dotNetChartingLicence;
 */ 
				default:
					try {
						string filename = Path.GetFileName(DotNetChartingLicence);
						string destinationPath = Path.Combine(destinationLicenseFileFolder, filename);
						Directory.CreateDirectory(destinationLicenseFileFolder);
						CopyFiles(DotNetChartingLicence, destinationLicenseFileFolder);
						return destinationPath;

					}
					catch (Exception) {
						OutputHelper.WriteToOutput(
							String.Format("Could not use the value of the /DotNetChartingLicense parameter. No DotNetChartingLicense has been installed.", DotNetChartingLicence), MessageType.Error);
						return null;	// do not change the current license setup
					}
			}

        }

		/// <summary>
		/// Modifies the key parameter of the BingProvider to reflect the correct license.
		/// </summary>
		/// <param name="root"></param>
		/// <param name="doc"></param>
		protected virtual void UpdateBingProviderKey(XmlNode root, XmlDocument doc){
			if (this is InstallApplicationInstallerAction) {

				const string const_ProductionLicenseKey = "ApBB1uo2PNon1PRxwYCzhwkY7WwNzvKmPsAnylN7q--c-7X7zildCN4O23OPfLQx";
				const string const_NonProductionLicenseKey = "Ajo1YyChsDF1gwyAoBnq8I09EwxotBV8BZkmcQehgRuaJCr5TpxoAODM5QjlYahA";
				var nsm = new XmlNamespaceManager(doc.NameTable);
				nsm.AddNamespace("mapSection", "http://Vizelia.FOL.Providers.MapSection");

				var bingProvider = root.SelectSingleNode("mapSection:VizeliaMapSection/mapSection:providers/mapSection:add[@name=\"Bing\"]", nsm);
				if (bingProvider != null) {
					string key = ProductionMode ?
						const_ProductionLicenseKey :
						const_NonProductionLicenseKey;

					XmlNode bingKeyNode = bingProvider.Attributes.GetNamedItem("key");
					if (bingKeyNode == null)
					{
						bingKeyNode= doc.CreateAttribute("key");
						bingProvider.Attributes.Append((XmlAttribute) bingKeyNode);
					}
					bingKeyNode.Value = key;
					
				}

			}

		}

		/// <summary>
		/// When ModConnection = Trust, run a SQL script that attempts
		/// to setup Logins for each app and Users on each database.
		/// These support the Trusted_Connection=true attribute on a 
		/// Connection String.
		/// </summary>
		protected void ConfigureTrustedLogins() {
			if (ModConnection == ModConnectionType.Trust) {
				OutputHelper.WriteToOutput("...adding logins and users to database to support Trusted_Connection in connection strings.");
				string cmdline = string.Format("sqlcmd -U {0} -P {1} -m -1 -v TenantName={2} DbPrefix={3} -i \"{4}\"", 
					DBUser, DBPwd, TenantName, DBPrefix, @".\Support Files\InstallTrustedLogins.sql");

				var result = Helper.ExecuteCommand(cmdline, shouldFlushOutputToConsole: true);

			}
		}
	}

	internal enum ConfigType {
		FrontEnd,
		Scheduler,
		EnergyAggregator
	}
}
﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Error handler class for exception shielding.
	/// </summary>
	public class ErrorHandler : IErrorHandler, IServiceBehavior {
		/// <summary>
		/// Checks if the WCF Operation is one way.
		/// </summary>
		private bool IsOneWayOperation() {
			bool isOneWay = false;

			try {
				var operationCtx = OperationContext.Current;
				string action = OperationContext.Current.IncomingMessageHeaders.Action;

				if (operationCtx.IncomingMessageProperties.ContainsKey("HttpOperationName")) {
					string operationName = (string)operationCtx.IncomingMessageProperties["HttpOperationName"];
					isOneWay = operationCtx.EndpointDispatcher.DispatchRuntime.Operations[operationName].IsOneWay;
				} 
				else {
					isOneWay = operationCtx.EndpointDispatcher.DispatchRuntime.Operations.Any(o => o.IsOneWay && o.Action.Equals(action));
				}
			}
			catch { }

			return isOneWay;
		}

		/// <summary>
		/// Enables the creation of a custom <see cref="T:System.ServiceModel.FaultException`1"/> that is returned from an exception in the course of a service method.
		/// </summary>
		/// <param name="error">The <see cref="T:System.Exception"/> object thrown in the course of the service operation.</param><param name="version">The SOAP version of the message.</param><param name="fault">The <see cref="T:System.ServiceModel.Channels.Message"/> object that is returned to the client, or service, in the duplex case.</param>
		public void ProvideFault(Exception error, MessageVersion version, ref Message fault) {

			var exceptionType = error.GetType().ToString();
			if (!IsOneWayOperation()) {
				HttpContext.Current.Response.AddHeader("ExceptionType", exceptionType);
			}

			FaultException faultException;
			
			if (error is VizeliaDatabaseException ||  
				error is VizeliaValidationException){
				faultException = new FaultException(error.Message);
			}
			else {
				faultException = new FaultException("An unexpected error has occurred: Error on request " + ContextHelper.RequestId);
			}

			MessageFault messageFault = faultException.CreateMessageFault();
			fault = Message.CreateMessage(version, messageFault, faultException.Action);
		}

		/// <summary>
		/// Enables error-related processing and returns a value that indicates whether the dispatcher aborts the session and the instance context in certain cases. 
		/// </summary>
		/// <returns>
		/// true if  should not abort the session (if there is one) and instance context if the instance context is not <see cref="F:System.ServiceModel.InstanceContextMode.Single"/>; otherwise, false. The default is false.
		/// </returns>
		/// <param name="error">The exception thrown during processing.</param>
		public bool HandleError(Exception error) {
			try {
				if (error is VizeliaSecurityException) return true;
				CookieData cookieData;
				SessionService.SetApplicationName(out cookieData);

				TracingService.Write(TraceEntrySeverity.Error,error.ToString(),"ErrorHandler",string.Empty);
				return true;
			}

			catch (Exception) {

				throw new Exception("error in the handler");

			}

		}

		/// <summary>
		/// Provides the ability to inspect the service host and the service description to confirm that the service can run successfully.
		/// </summary>
		/// <param name="serviceDescription">The service description.</param><param name="serviceHostBase">The service host that is currently being constructed.</param>
		public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase) {
		}

		/// <summary>
		/// Provides the ability to pass custom data to binding elements to support the contract implementation.
		/// </summary>
		/// <param name="serviceDescription">The service description of the service.</param><param name="serviceHostBase">The host of the service.</param><param name="endpoints">The service endpoints.</param><param name="bindingParameters">Custom objects to which binding elements have access.</param>
		public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters) {
		}

		/// <summary>
		/// Provides the ability to change run-time property values or insert custom extension objects such as error handlers, message or parameter interceptors, security extensions, and other custom extension objects.
		/// </summary>
		/// <param name="serviceDescription">The service description.</param><param name="serviceHostBase">The host that is currently being built.</param>
		public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase) {
			IErrorHandler errorHandler = new ErrorHandler();

			foreach (ChannelDispatcherBase channelDispatcherBase in serviceHostBase.ChannelDispatchers) {
				var channelDispatcher = (ChannelDispatcher)channelDispatcherBase;
				channelDispatcher.ErrorHandlers.Add(errorHandler);


			}                   

		}
	}
}
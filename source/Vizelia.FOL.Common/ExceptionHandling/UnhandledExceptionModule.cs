﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;
using EventLog = System.Diagnostics.EventLog;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A module that writes unhandled exceptions to the trace. Doesnt stop the exception being thrown.
	/// </summary>
	public class UnhandledExceptionModule : IHttpModule {

		/// <summary>
		/// To keep from writing the exception multiple times
		/// </summary>
		static int _unhandledExceptionCount = 0;


		/// <summary>
		/// Inits the specified app.
		/// </summary>
		/// <param name="app">The app.</param>
		public void Init(HttpApplication app) {
			
			AppDomain.CurrentDomain.UnhandledException +=
				new UnhandledExceptionEventHandler(OnUnhandledException);


		}

		/// <summary>
		/// Disposes of the resources (other than memory) used by the module that implements <see cref="T:System.Web.IHttpModule"/>.
		/// </summary>
		public void Dispose() {
		}

		/// <summary>
		/// Called when [unhandled exception].
		/// </summary>
		/// <param name="o">The o.</param>
		/// <param name="e">The <see cref="System.UnhandledExceptionEventArgs"/> instance containing the event data.</param>
		private void OnUnhandledException(object o, UnhandledExceptionEventArgs e) {
			if (Interlocked.Exchange(ref _unhandledExceptionCount, 1) != 0) return; 
			// Build a message containing the exception details
			string message = "UnhandledException logged by UnhandledExceptionModule.dll: ";
			TracingService.Write(TraceEntrySeverity.Error, message + e.ExceptionObject);
		}
	}
}

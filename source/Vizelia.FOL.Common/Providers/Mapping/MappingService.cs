﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;


namespace Vizelia.FOL.Providers {
	/// <summary>
	/// A service that calls the Mapping provider.
	/// </summary>
	public class MappingService {
		private static IMappingProvider defaultProvider = null;
		private static readonly GenericProviderCollection<MappingProvider, IMappingProvider> providers = LoadMappingProviders();


		private static GenericProviderCollection<MappingProvider, IMappingProvider> LoadMappingProviders() {
			GenericProviderCollection<MappingProvider, IMappingProvider> providerCollection = GenericProviderHelper.LoadProviders<MappingProvider, GenericProviderSection, IMappingProvider>("VizeliaMappingSection", out defaultProvider);

			// Check if ExternalProviders is here.
			// If so, replace existing EWS RSP provider with new implementation.
			try {
				string externalProviderAssemblyFilename = "SchneiderElectric.EO.MappingProviders.EWSRSP.dll";
				string classFullyQualifiedName = "SchneiderElectric.EO.MappingProviders.EWSRSP.EWSRSPMappingProvider";

				string path = Path.GetDirectoryName(new Uri(typeof(MappingService).Assembly.CodeBase).LocalPath);
				string externalProvidersAssemblyPath = Path.Combine(path, externalProviderAssemblyFilename);
				
				if (File.Exists(externalProvidersAssemblyPath)) {
					Assembly externalProviders = Assembly.LoadFrom(externalProvidersAssemblyPath);
					var instance = (MappingProvider)externalProviders.CreateInstance(classFullyQualifiedName);

					if (instance != null) {
						instance.Initialize("NewEWSRSP", new NameValueCollection { { "filename", "*.xml" } });
						providerCollection.Add(instance); // Insert external implementation.
						providerCollection.Remove("EWSRSP"); // Remove platform implementation.
					}
				}
			}
			catch (Exception e) {
				try {
					TracingService.Write(e);
				}
				catch {
					// Do nothing.
				}
			}

			return providerCollection;
		}

		/// <summary>
		/// Gets a collection of the long-running operation providers.
		/// </summary>
		public static GenericProviderCollection<MappingProvider, IMappingProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Starts the mapping process.
		/// </summary>
		/// <returns></returns>
		public static MappingContext StartMappingProcess() {
			ExternalCrudNotificationService.IsAsynchronousMode = true;

			MappingContext mappingContext = defaultProvider.CreateMappingContext();

			// Initialize for the whole process.
			mappingContext.MappingSummary.Start();

			return mappingContext;
		}

		/// <summary>
		/// Finishes the mapping process.
		/// </summary>
		/// <param name="summary">The summary.</param>
		/// <returns></returns>
		public static LongRunningOperationResult FinishMappingProcess(IMappingSummary summary) {
			ExternalCrudNotificationService.FlushWaitingNotifications();

			summary.Stop();
			LongRunningOperationResult result = CreateResult(summary);


			return result;
		}

		/// <summary>
		/// Finishes the mapping process with an error.
		/// </summary>
		/// <param name="summary">The summary.</param>
		/// <param name="exception">The exception.</param>
		public static void FinishMappingProcess(IMappingSummary summary, Exception exception) {
			summary.LogError(exception, Langue.msg_mapping_error);
			FinishMappingProcess(summary);
		}

		/// <summary>
		/// Maps the specified mapping file as a part of a mapping process that should be started and finished externally to this call.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="fileContent">Content of the file. Note: this stream needs to support Seek().</param>
		/// <param name="mappingContext">The mapping context.</param>
		/// <exception cref="MappingException">In case of a mapping error.</exception>
		public static void Map(string filename, Stream fileContent, MappingContext mappingContext) {
			try {
				IMappingProvider provider = GetProvider(filename, fileContent, mappingContext.MappingSummary);
				provider.Map(filename, fileContent, mappingContext);
			}
			catch (MappingException) {
				// In this case we don't need to wrap it, just throw it on.
				throw;
			}
			catch (Exception exception) {
				// We want to wrap any exception coming out of this so the consumer will know what to catch.
				throw new MappingException(Langue.msg_mapping_error, exception);
			}
		}


		/// <summary>
		/// Maps the specified mapping file as a mapping process with a single file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="fileContent">Content of the file.</param>
		/// <returns>
		/// The long running operation result
		/// </returns>
		/// <exception cref="MappingException">In case of a mapping error.</exception>
		public static LongRunningOperationResult Map(string filename, Stream fileContent) {
			MappingContext mappingContext = StartMappingProcess();
			IMappingSummary summary = mappingContext.MappingSummary;
			
			bool canStart = true;

			try {
				ExternalCrudNotificationService.ThrowOnNotReady();
			}
			catch (Exception exception) {
				canStart = false;
				summary.LogError(exception, "Cannot perform manual mapping.", false);
			}

			if (canStart) {
				try {
					Map(filename, fileContent, mappingContext);
				}
				catch {
					// Although there has been an exception, it should have been logged by the provider.
					// We do not want to throw it out. Instead, we want to return the log that contains it.
					// This Map() call is made through the UI, so we want them to have the log.
					// In case of Scheduled Mapping, another overload of Map() is called which doesn't catch exceptions.
				}
			}

			LongRunningOperationResult result = FinishMappingProcess(summary);

			return result;
		}


		/// <summary>
		/// Creates the result.
		/// </summary>
		/// <param name="summary">The summary.</param>
		/// <returns></returns>
		private static LongRunningOperationResult CreateResult(IMappingSummary summary) {
			string report = summary.MappingLog.ToString();
			MemoryStream reportStream = report.GetStream();
			string outputFilename = String.Format(Langue.msg_mapping_attachment, DateTime.Now.ToString("HH_mm_ss"));
			var file = new StreamResult(reportStream, outputFilename, "txt", MimeType.Text);

			var result = new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, file);
			return result;
		}


	    /// <summary>
	    /// Gets the provider.
	    /// </summary>
	    /// <param name="fileName">Name of the file.</param>
	    /// <param name="mappingFile">The mapping file.</param>
	    /// <param name="summary"> </param>
	    /// <returns></returns>
	    private static IMappingProvider GetProvider(string fileName, Stream mappingFile, IMappingSummary summary) {
			IMappingProvider relevantProvider = Providers.OfType<IMappingProvider>().FirstOrDefault(p => p.CanMapFile(fileName, mappingFile));

			if (relevantProvider == null) {
				string message = string.Format("Unmappable file '{0}'.", fileName);
				summary.LogError(string.Empty, message, true);
				throw new UnmappableFileException(message);
			}

			return relevantProvider;
		}

		/// <summary>
		/// Maps the entities.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <param name="shouldWaitForEnergyAggregator">if set to <c>true</c> [should wait for energy aggregator].</param>
		/// <returns></returns>
		public static IMappingSummary MapEntities(List<IMappingRecordConverter> entities, bool shouldWaitForEnergyAggregator = true) {
			MappingContext mappingContext = StartMappingProcess();
			IMappingSummary summary = mappingContext.MappingSummary;
			
			summary.ShouldSendReport = false;

			if (shouldWaitForEnergyAggregator) {
				Helper.RetryIfFalse(30, 1000, ExternalCrudNotificationService.IsReadyForNotifications, "ExternalCrudNotificationService.IsReadyForNotifications");
			}

			try {
				defaultProvider.MapEntities(entities, mappingContext);
			}
			finally {
				FinishMappingProcess(summary);
			}

			return summary;
		}

		/// <summary>
		/// Gets all mapping objects.
		/// </summary>
		public static string GetAllMappingObjects() {
			var stringBuilder = new StringBuilder();
			foreach (var currentProvider in providers) {
				string allMappingObjects = ((IMappingProvider)currentProvider).GetAllMappingObjects();
				stringBuilder.Append(allMappingObjects);
			}

			return stringBuilder.ToString();
		}

		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		/// <returns></returns>
		public static IEnumerable<string> GetAllowedMappingFileExtensions() {
			var extensions = new List<string>();

			foreach (var provider in providers) {
				extensions.AddRange(((IMappingProvider)provider).GetAllowedMappingFileExtensions());
			}

			var fileExtensions = extensions.Select(e => e.ToLower()).Distinct().ToList();

			return fileExtensions;
		}

		/// <summary>
		/// Exports the specified business entities.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <returns></returns>
		public static string Export(List<object> entities) {
			string exportedText = defaultProvider.Export(entities);
			return exportedText;
		}

		/// <summary>
		/// Determines whether the mapping service can map the specified filename.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <returns>
		///   <c>true</c> if the mapping service can map the file the specified filename; otherwise, <c>false</c>.
		/// </returns>
		public static bool CanMapFile(string filename) {
			bool canMapFile = Providers.OfType<IMappingProvider>().Any(p => p.CanMapFile(filename));
			return canMapFile;
		}

		/// <summary>
		/// Wraps the object with thier co-responding mappings.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <returns></returns>
		public static IEnumerable<IMappingRecordConverter> GetMappings(List<object> entities)
		{
			return defaultProvider.GetMappings(entities);
		}
	}
}
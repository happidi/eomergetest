using System;
using System.Diagnostics;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// An abstraction of the mapping summary.
	/// </summary>
	public interface IMappingSummary {

		/// <summary>
		/// Gets the mapping log.
		/// </summary>
		StringBuilder MappingLog { get; }

		/// <summary>
		/// Gets or sets the emails that the summary should be sent to.
		/// </summary>
		[ViewableByDataModel]
		string Emails { get; set; }

		/// <summary>
		/// Gets the total create count.
		/// </summary>
		[ViewableByDataModel]
		int TotalCreateCount { get; }

		/// <summary>
		/// Gets the total destory count.
		/// </summary>
		[ViewableByDataModel]
		int TotalDestoryCount { get; }

		/// <summary>
		/// Gets the total error count.
		/// </summary>
		[ViewableByDataModel]
		int TotalErrorCount { get; }

		/// <summary>
		/// Gets the total record count.
		/// </summary>
		[ViewableByDataModel]
		int TotalRecordCount { get; }

		/// <summary>
		/// Gets the total entity count.
		/// </summary>
		[ViewableByDataModel]
		int TotalEntityCount { get; }

		/// <summary>
		/// Gets the total success count.
		/// </summary>
		[ViewableByDataModel]
		int TotalSuccessCount { get; }

		/// <summary>
		/// Gets the total update count.
		/// </summary>
		[ViewableByDataModel]
		int TotalUpdateCount { get; }

		/// <summary>
		/// The file count.
		/// </summary>
		[ViewableByDataModel]
		int FileCount { get; }

		/// <summary>
		/// Gets a value indicating whether this instance is cancelled.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is cancelled; otherwise, <c>false</c>.
		/// </value>
		[ViewableByDataModel]
		bool IsCancelled { get; }

		/// <summary>
		/// Gets the elapsed time.
		/// </summary>
		[ViewableByDataModel]
		TimeSpan TotalElapsedTime { get; }

		/// <summary>
		/// Gets or sets a value indicating whether the report should be sent by email or not.
		/// </summary>
		/// <value>
		///   <c>true</c> if the report should be sent by email; otherwise, <c>false</c>.
		/// </value>
		bool ShouldSendReport { get; set; }

		/// <summary>
		/// Gets a value indicating whether this instance is mapping in progress.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is mapping in progress; otherwise, <c>false</c>.
		/// </value>
		bool IsMappingInProgress { get; }

		/// <summary>
		/// Counts the entity by its action.
		/// </summary>
		/// <param name="action">The action.</param>
		void CountEntity(string action);

		/// <summary>
		/// Counts the record.
		/// </summary>
		void CountRecord();

		/// <summary>
		/// Starts the summary for the whole mapping process.
		/// </summary>
		void Start();

		/// <summary>
		/// Starts the summary for a file.
		/// </summary>
		void StartFile(string filename);

		/// <summary>
		/// Stops the summary for the mapping file.
		/// </summary>
		void StopFile();

		/// <summary>
		/// Stops the summary for the whole mapping process.
		/// </summary>
		void Stop();

		/// <summary>
		/// Logs the action start.
		/// </summary>
		/// <param name="action">The action.</param>
		void LogActionStart(string action);

		/// <summary>
		/// Logs the action end.
		/// </summary>
		/// <param name="action">The action.</param>
		void LogActionEnd(string action);

		/// <summary>
		/// Logs the success.
		/// </summary>
		/// <param name="localid">The localid.</param>
		/// <param name="message">The message.</param>
		void LogSuccess(string localid, string message);

		/// <summary>
		/// Logs the error.
		/// </summary>
		/// <param name="exception">The exception.</param>
		/// <param name="logMessage">The log message.</param>
		void LogError(Exception exception, string logMessage);

		/// <summary>
		/// Logs the error.
		/// </summary>
		/// <param name="localid">The localid.</param>
		/// <param name="logMessage">The log message.</param>
		/// <param name="shouldWriteToTrace">if set to <c>true</c> [should write to trace].</param>
		void LogError(string localid, string logMessage, bool shouldWriteToTrace = true);

		/// <summary>
		/// Logs the error.
		/// </summary>
		/// <param name="exception">The exception.</param>
		/// <param name="logMessage">The log message.</param>
		/// <param name="logDetailedException">if set to <c>true</c> [log detailed exception].</param>
		void LogError(Exception exception, string logMessage, bool logDetailedException);

		/// <summary>
		/// Logs the cancelation.
		/// </summary>
		/// <param name="logMessage">The log message.</param>
		void LogCancelation(string logMessage);

		/// <summary>
		/// Logs the Skipped records. Determines the record offset from
		/// the total entity count.
		/// </summary>
		void LogSkipped();

	}
}

using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers
{
	/// <summary>
	/// Holds Mapping Configuration Data
	/// </summary>
	public class MappingConfiguration
	{

		/// <summary>
		/// Gets or sets the options of auto creation of meters during mapping.
		/// </summary>
		public AutoCreateMeterOptions AutoCreateMeterOptions { get; set; }


		/// <summary>
		/// Holds the location key of the auto generated meters.
		/// </summary>
		public string AutoCreatedMetersKeyLocation { get; set; }

	}
}
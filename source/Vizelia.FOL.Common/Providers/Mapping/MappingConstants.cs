﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Providers {
	
	/// <summary>
	/// Mapping Constants
	/// </summary>
	public static class MappingConstants
	{
		/// <summary>
		/// The LocalId of the default site under which the unresolved meters are created during mapping
		/// </summary>
		public const string const_unresolved_meters_default_spatial_id = "SITE_NEW_METERS";

		/// <summary>
		/// The Name of the default site under which the unresolved meters are created during mapping
		/// </summary>
		public const string const_unresolved_meters_default_spatial_name = "SITE_NEW_METERS";

		/// <summary>
		/// The LocalId of the default classification under which the unresolved meters are created during mapping
		/// </summary>
		public const string const_unresolved_meters_default_classification_id = "METER_CLASS_UNKNOWN";

		/// <summary>
		/// The Name of the default classification under which the unresolved meters are created during mapping
		/// </summary>
		public const string const_unresolved_meters_default_classification_name = "Meter Class Unknown";
	}
}

using System.Collections.Generic;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers
{
	/// <summary>
	/// Defines the contract to provide Mapping services using custom Mapping providers.
	/// </summary>
	public abstract class MappingProvider : GenericProviderBase, IMappingProvider
	{
		/// <summary>
		/// Maps the specified mapping file.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="fileContent">Content of the file.</param>
		/// <param name="mappingContext">The mapping context.</param>
		public abstract void Map(string fileName, Stream fileContent, MappingContext mappingContext);

		/// <summary>
		/// Maps the entities.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <param name="mappingContext">The mapping context.</param>
		public abstract void MapEntities(List<IMappingRecordConverter> entities, MappingContext mappingContext);

		/// <summary>
		/// Gets all mapping objects.
		/// </summary>
		public abstract string GetAllMappingObjects();

		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		/// <returns></returns>
		public abstract IEnumerable<string> GetAllowedMappingFileExtensions();

		/// <summary>
		/// Determines whether the provider can map the specified file or not, only by its filename.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <returns>
		///   <c>true</c> if the provider can map the specified file; otherwise, <c>false</c>.
		/// </returns>
		public abstract bool CanMapFile(string filename);

		/// <summary>
		/// Determines whether the provider can map the specified file or not.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="mappingFile"></param>
		/// <returns>
		///   <c>true</c> if the provider can map the specified file; otherwise, <c>false</c>.
		/// </returns>
		public abstract bool CanMapFile(string filename, Stream mappingFile);


		/// <summary>
		/// Creates the mapping summary.
		/// </summary>
		/// <returns></returns>
		public abstract MappingContext CreateMappingContext();

		/// <summary>
		/// Exports the specified business entities.
		/// </summary>
		/// <param name="businessEntities">The business entities.</param>
		/// <returns></returns>
		public abstract string Export(List<object> businessEntities);

		/// <summary>
		/// Wraps the object with thier co-responding mappings.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <returns></returns>
		public abstract IEnumerable<IMappingRecordConverter> GetMappings(List<object> entities);
	}
}
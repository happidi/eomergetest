﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Holdes the mapping context data
	/// </summary>
	public class MappingContext {
		
		/// <summary>
		/// Gets or sets the mapping summary.
		/// </summary>
		public IMappingSummary MappingSummary { get; set; }

		/// <summary>
		/// Gets or sets the mapping configuration.
		/// </summary>
		public MappingConfiguration MappingConfiguration { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="MappingContext"/> class.
		/// </summary>
		public MappingContext(){
			MappingConfiguration = new MappingConfiguration();
		}
	}
}

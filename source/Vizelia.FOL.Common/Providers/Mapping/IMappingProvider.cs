using System.Collections.Generic;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers
{
	/// <summary>
	/// An interface for a mapping provider.
	/// </summary>
	public interface IMappingProvider : IGenericProviderBase
	{
		/// <summary>
		/// Maps the specified mapping file.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="fileContent">Content of the file.</param>
		/// <param name="mappingContext">The mapping context.</param>
		void Map(string fileName, Stream fileContent, MappingContext mappingContext);

		/// <summary>
		/// Maps the entities.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <param name="mappingContext">The mapping context.</param>
		void MapEntities(List<IMappingRecordConverter> entities, MappingContext mappingContext);

		/// <summary>
		/// Gets all mapping objects.
		/// </summary>
		string GetAllMappingObjects();

		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		/// <returns></returns>
		IEnumerable<string> GetAllowedMappingFileExtensions();

		/// <summary>
		/// Determines whether the provider can map the specified file or not, only by its filename.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <returns>
		///   <c>true</c> if the provider can map the specified file; otherwise, <c>false</c>.
		/// </returns>
		bool CanMapFile(string filename);

		/// <summary>
		/// Determines whether the provider can map the specified file or not.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="mappingFile">The mapping file.</param>
		/// <returns>
		///   <c>true</c> if the provider can map the specified file; otherwise, <c>false</c>.
		/// </returns>
		bool CanMapFile(string filename, Stream mappingFile);

		/// <summary>
		/// Creates the mapping summary.
		/// </summary>
		/// <returns></returns>
		MappingContext CreateMappingContext();

		/// <summary>
		/// Exports the specified business entities.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <returns></returns>
		string Export(List<object> entities);

		/// <summary>
		/// Wraps the object with thier co-responding mappings.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <returns></returns>
		IEnumerable<IMappingRecordConverter> GetMappings(List<object> entities);
	}
}
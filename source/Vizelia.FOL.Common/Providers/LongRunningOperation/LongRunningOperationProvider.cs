﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide long-running operation services using long-running operation providers.
	/// </summary>
	public abstract class LongRunningOperationProvider : GenericProviderBase, ILongRunningOperationProvider {
		/// <summary>
		/// Gets the operationId from the incoming HTTP request.
		/// </summary>
		/// <returns>The id of the operation.</returns>
		public abstract Guid GetOperationIdFromRequest();

		/// <summary>
		/// Invokes the specified long running operation with the given operation ID.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="state">The state.</param>
		/// <param name="workFunction">The work function.</param>
		public abstract void Invoke(Guid operationId, object state, Func<LongRunningOperationResult> workFunction);

		/// <summary>
		/// Invokes the specified long running operation with the given operation ID.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="workFunction">The work function.</param>
		public abstract void Invoke(Guid operationId, Func<LongRunningOperationResult> workFunction);

		/// <summary>
		/// Gets the operation state.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <returns></returns>
		public abstract LongRunningOperationState GetOperationState(Guid operationId);

		/// <summary>
		/// Updates the operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="clientRequest">The client request.</param>
		public abstract void UpdateOperation(Guid operationId, LongRunningOperationClientRequest clientRequest);

		/// <summary>
		/// Completes the specified operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		public abstract void Complete(Guid operationId);

		/// <summary>
		/// Clears the next child operation parameters.
		/// </summary>
		public abstract void ClearChildOperationParameters();

		/// <summary>
		/// Prepares for a child long-running operation that is about to be started.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="percentMin">The percent min.</param>
		/// <param name="percentMax">The percent max.</param>
		public abstract void PrepareForChildOperation(string name, int percentMin, int percentMax);

		/// <summary>
		/// Reports that the operation is in progress.
		/// </summary>
		public abstract void ReportProgress();

		/// <summary>
		/// Reports the progress.
		/// </summary>
		/// <param name="percentDone">The percent done.</param>
		public abstract void ReportProgress(int percentDone);

		/// <summary>
		/// Reports the progress.
		/// </summary>
		/// <param name="percentDone">The percent done.</param>
		/// <param name="message">The message.</param>
		public abstract void ReportProgress(int percentDone, string message);

		/// <summary>
		/// Determines whether cancelation is pending for this operation or not.
		/// </summary>
		/// <returns>
		///   <c>true</c> if cancelation is pending for this operation; otherwise, <c>false</c>.
		/// </returns>
		public abstract bool IsCancelationPending();

		/// <summary>
		/// Gets the current operation id.
		/// </summary>
		/// <returns></returns>
		public abstract Guid? GetCurrentOperationId();
	}

	/// <summary>
	/// The interface for a long-running operation provider.
	/// </summary>
	public interface ILongRunningOperationProvider : IGenericProviderBase {
		/// <summary>
		/// Gets the operationId from the incoming HTTP request.
		/// </summary>
		/// <returns>The id of the operation.</returns>
		Guid GetOperationIdFromRequest();

		/// <summary>
		/// Invokes the specified long running operation with the given operation ID.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="state">The state.</param>
		/// <param name="workFunction">The work function.</param>
		void Invoke(Guid operationId, object state, Func<LongRunningOperationResult> workFunction);

		/// <summary>
		/// Invokes the specified long running operation with the given operation ID.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="workFunction">The work function.</param>
		void Invoke(Guid operationId, Func<LongRunningOperationResult> workFunction);

		/// <summary>
		/// Gets the operation state.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <returns></returns>
		LongRunningOperationState GetOperationState(Guid operationId);

		/// <summary>
		/// Updates the operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="clientRequest">The client request.</param>
		void UpdateOperation(Guid operationId, LongRunningOperationClientRequest clientRequest);

		/// <summary>
		/// Completes the specified operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		void Complete(Guid operationId);

		/// <summary>
		/// Clears the next child operation parameters.
		/// </summary>
		void ClearChildOperationParameters();

		/// <summary>
		/// Prepares for a child long-running operation that is about to be started.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="percentMin">The percent min.</param>
		/// <param name="percentMax">The percent max.</param>
		void PrepareForChildOperation(string name, int percentMin, int percentMax);

		/// <summary>
		/// Reports that the operation is in progress.
		/// </summary>
		void ReportProgress();

		/// <summary>
		/// Reports the progress.
		/// </summary>
		/// <param name="percentDone">The percent done.</param>
		void ReportProgress(int percentDone);

		/// <summary>
		/// Reports the progress.
		/// </summary>
		/// <param name="percentDone">The percent done.</param>
		/// <param name="message">The message.</param>
		void ReportProgress(int percentDone, string message);

		/// <summary>
		/// Determines whether cancelation is pending for this operation or not.
		/// </summary>
		/// <returns>
		///   <c>true</c> if cancelation is pending for this operation; otherwise, <c>false</c>.
		/// </returns>
		bool IsCancelationPending();

		/// <summary>
		/// Gets the current operation id.
		/// </summary>
		/// <returns></returns>
		Guid? GetCurrentOperationId();
	}
}

﻿using System;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// A result of a long-running operation.
	/// </summary>
	public class LongRunningOperationResult {
		/// <summary>
		/// Gets or sets the status.
		/// </summary>
		/// <value>
		/// The status.
		/// </value>
		public LongRunningOperationStatus Status { get; private set; }

		/// <summary>
		/// Gets or sets the output.
		/// </summary>
		/// <value>
		/// The output.
		/// </value>
		public object Output { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="LongRunningOperationResult"/> class.
		/// </summary>
		/// <param name="status">The status.</param>
		public LongRunningOperationResult(LongRunningOperationStatus status) {
			Status = status;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="LongRunningOperationResult"/> class.
		/// </summary>
		/// <param name="status">The status.</param>
		/// <param name="output">The output.</param>
		public LongRunningOperationResult(LongRunningOperationStatus status, object output) {
			// Validate input
			if (status == LongRunningOperationStatus.Completed && output != null) {
				throw new ArgumentException("Cannot use Completed status when output is given, use ResultAvailable instead.");
			}
			if (status == LongRunningOperationStatus.Initialized || status == LongRunningOperationStatus.InProgress) {
				throw new ArgumentException("Given status is invalid for completing a long-running operation.");
			}

			Status = status;
			Output = output;
		}
	}
}

﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// A service that calls the long-running operation provider
	/// </summary>
	public class LongRunningOperationService {
		private static ILongRunningOperationProvider provider = null;
		private static GenericProviderCollection<LongRunningOperationProvider, ILongRunningOperationProvider> providers = GenericProviderHelper.LoadProviders<LongRunningOperationProvider, GenericProviderSection, ILongRunningOperationProvider>("VizeliaLongRunningOperationSection", out provider);

		/// <summary>
		/// Gets a collection of the long-running operation providers.
		/// </summary>
		public static GenericProviderCollection<LongRunningOperationProvider, ILongRunningOperationProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Gets the operationId from the incoming HTTP request.
		/// </summary>
		/// <returns>The id of the operation.</returns>
		public static Guid GetOperationIdFromRequest() {
			return provider.GetOperationIdFromRequest();
		}

		/// <summary>
		/// Invokes the specified long running operation with the given operation ID.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="state">The state.</param>
		/// <param name="workFunction">The work function.</param>
		public static void Invoke(Guid operationId, object state, Func<LongRunningOperationResult> workFunction) {
			provider.Invoke(operationId, state, workFunction);
		}

		/// <summary>
		/// Invokes the specified long running operation with the given operation ID.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="workFunction">The work function.</param>
		public static void Invoke(Guid operationId, Func<LongRunningOperationResult> workFunction) {
			provider.Invoke(operationId, workFunction);
		}

		/// <summary>
		/// Gets the operation state.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		public static LongRunningOperationState GetOperationState(Guid operationId) {
			return provider.GetOperationState(operationId);
		}


		/// <summary>
		/// Updates the operation with a client request.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="clientRequest">The client request.</param>
		public static void UpdateOperation(Guid operationId, LongRunningOperationClientRequest clientRequest) {
			provider.UpdateOperation(operationId, clientRequest);
		}

		/// <summary>
		/// Completes the specified operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		public static void Complete(Guid operationId) {
			provider.Complete(operationId);
		}


		/// <summary>
		/// Clears the next child operation parameters.
		/// </summary>
		public static void ClearChildOperationParameters() {
			provider.ClearChildOperationParameters();
		}

		/// <summary>
		/// Prepares for a child long-running operation that is about to be started.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="percentMin">The percent min.</param>
		/// <param name="percentMax">The percent max.</param>
		public static void PrepareForChildOperation(string name, int percentMin, int percentMax) {
			provider.PrepareForChildOperation(name, percentMin, percentMax);
		}

		/// <summary>
		/// Gets the current operation id.
		/// </summary>
		/// <returns></returns>
		public static Guid? GetCurrentOperationId() {
			return provider.GetCurrentOperationId();
		}

		/// <summary>
		/// Reports that the operation is in progress.
		/// </summary>
		public static void ReportProgress() {
			provider.ReportProgress();
		}

		/// <summary>
		/// Reports the progress.
		/// </summary>
		/// <param name="percentDone">The percent done.</param>
		public static void ReportProgress(int percentDone) {
			provider.ReportProgress(percentDone);
		}

		/// <summary>
		/// Reports the progress.
		/// </summary>
		/// <param name="percentDone">The percent done.</param>
		/// <param name="message">The message.</param>
		public static void ReportProgress(int percentDone, string message) {
			provider.ReportProgress(percentDone, message);
		}

		/// <summary>
		/// Determines whether cancelation is pending for this operation or not.
		/// </summary>
		/// <returns>
		///   <c>true</c> if cancelation is pending for this operation; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsCancelationPending() {
			bool isCancelationPending = provider.IsCancelationPending();
			return isCancelationPending;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// A service that calls the MappingConversion provider.
	/// </summary>
	public static class MappingConversionService {
		private static IMappingConversionProvider provider = null;
		private static GenericProviderCollection<MappingConversionProvider, IMappingConversionProvider> providers = GenericProviderHelper.LoadProviders<MappingConversionProvider, GenericProviderSection, IMappingConversionProvider>("VizeliaMappingConversionSection", out provider);
		/// <summary>
		/// Gets a collection of the Map providers.
		/// </summary>
		public static GenericProviderCollection<MappingConversionProvider, IMappingConversionProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Gets the entities from file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <returns></returns>
		public static List<object> GetEntities(string filename) {
			return provider.GetEntities(filename);
		}

		/// <summary>
		/// Gets the entities from stream.
		/// </summary>
		/// <param name="streamFile">The stream file.</param>
		/// <returns></returns>
		public static List<object> GetEntities(Stream streamFile) {
			return provider.GetEntities(streamFile);
		}
	}
}

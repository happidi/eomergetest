﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide MappingConversion services using custom MappingConversion providers.
	/// </summary>
	public abstract class MappingConversionProvider : GenericProviderBase, IMappingConversionProvider
	{
		/// <summary>
		/// Gets the entities from file or stream.
		/// </summary>
		/// <param name="filenameOrStream">The filename or stream.</param>
		/// <returns></returns>
		public abstract List<object> GetEntities(object filenameOrStream);
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IMappingConversionProvider : IGenericProviderBase {
		/// <summary>
		/// Gets the entities from file or stream.
		/// </summary>
		/// <param name="filenameOrStream">The filename or stream.</param>
		/// <returns></returns>
		List<object> GetEntities(object filenameOrStream);
	}
}

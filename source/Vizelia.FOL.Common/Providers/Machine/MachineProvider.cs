﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;
using System.Drawing;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide charting services using custom charting providers.
	/// </summary>
	public abstract class MachineProvider : GenericProviderBase, IMachineProvider {

		/// <summary>
		/// Returns the list of current MachineInstance.
		/// </summary>
		/// <returns></returns>
		public abstract List<MachineInstance> GetAllInstances();


		/// <summary>
		/// Restarts the specified machine.
		/// </summary>
		/// <param name="machine">The machine.</param>
		public abstract void Restart(MachineInstance machine);

		/// <summary>
		/// Gets the list of machine for a specific purpose: i.e : Vizelia.FOL.EnergyAggregatorService.
		/// </summary>
		/// <param name="purpose">The purpose.</param>
		/// <returns></returns>
		public abstract List<MachineInstance> GetList(string purpose);

		/// <summary>
		/// Creates a new machine instance.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public abstract MachineInstance Create(MachineInstance item);

		/// <summary>
		/// Deletes the specified machineinstance.
		/// </summary>
		/// <param name="item">The item.</param>
		public abstract bool Delete(MachineInstance item);

		/// <summary>
		/// Determines whether this MachineInstance service is enabled.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the service is enabled; otherwise, <c>false</c>.
		/// </returns>
		public abstract bool IsEnabled();
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IMachineProvider : IGenericProviderBase {

		/// <summary>
		/// Returns the list of current MachineInstance.
		/// </summary>
		/// <returns></returns>
		List<MachineInstance> GetAllInstances();


		/// <summary>
		/// Restarts the specified machine.
		/// </summary>
		/// <param name="machine">The machine.</param>
		void Restart(MachineInstance machine);


		/// <summary>
		/// Gets the list of machine for a specific purpose: i.e : Vizelia.FOL.EnergyAggregatorService.
		/// </summary>
		/// <param name="purpose">The purpose.</param>
		/// <returns></returns>
		List<MachineInstance> GetList(string purpose);

		/// <summary>
		/// Creates a new machine instance.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		MachineInstance Create(MachineInstance item);


		/// <summary>
		/// Deletes the specified machineinstance.
		/// </summary>
		/// <param name="item">The item.</param>
		bool Delete(MachineInstance item);

		/// <summary>
		/// Determines whether this MachineInstance service is enabled.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the service is enabled; otherwise, <c>false</c>.
		/// </returns>
		bool IsEnabled();
	}
}

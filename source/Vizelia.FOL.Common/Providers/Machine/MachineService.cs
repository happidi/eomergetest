﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;
using System.Collections;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;
using System.Drawing;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Machine Service static class.
	/// </summary>
	public static class MachineService {
		private static IMachineProvider provider = null;
		private static GenericProviderCollection<MachineProvider, IMachineProvider> providers = GenericProviderHelper.LoadProviders<MachineProvider, GenericProviderSection, IMachineProvider>("VizeliaMachineSection", out provider);

		/// <summary>
		/// Gets a collection of the Machine providers.
		/// </summary>
		public static GenericProviderCollection<MachineProvider, IMachineProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Returns the list of current MachineInstance.
		/// </summary>
		/// <returns></returns>
		public static List<MachineInstance> GetList() {
			return provider.GetAllInstances();
		}


		/// <summary>
		/// Restarts the specified machine.
		/// </summary>
		/// <param name="machine">The machine.</param>
		public static void Restart(MachineInstance machine) {
			provider.Restart(machine);
		}

		/// <summary>
		/// Gets the list of machine for a specific purpose: i.e : Vizelia.FOL.EnergyAggregatorService.
		/// </summary>
		/// <param name="purpose">The purpose.</param>
		/// <returns></returns>
		public static List<MachineInstance> GetList(string purpose) {
			return provider.GetList(purpose);
		}

		/// <summary>
		/// Creates a new machine instance.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public static MachineInstance Create(MachineInstance item) {
			return provider.Create(item);
		}
		/// <summary>
		/// Deletes the specified machineinstance.
		/// </summary>
		/// <param name="item">The item.</param>
		public static bool Delete(MachineInstance item) {
			return provider.Delete(item);
		}

		/// <summary>
		/// Determines whether this MachineInstance service is enabled.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the service is enabled; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsEnabled() {
			return provider.IsEnabled();
		}
	}

}

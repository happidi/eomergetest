using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using NetSqlAzMan.Interfaces;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// The FOL Role Provider interface.
	/// </summary>
	public interface IFOLRoleProvider {
		/// <summary>
		/// Gets a brief, friendly description suitable for display in administrative tools or other user interfaces (UIs).
		/// </summary>
		string Description { get; }

		/// <summary>
		/// Gets the friendly name used to refer to the provider during configuration.
		/// </summary>
		string Name { get; }

		/// <summary>
		/// Gets or sets the name of the application to store and retrieve role information for.
		/// </summary>
		/// <returns>The name of the application to store and retrieve role information for.</returns>
		string ApplicationName { get; set; }

		/// <summary>
		/// Gets the list of authorizations for a specific application group.
		/// </summary>
		/// <param name="applicationGroupId">The application group id to retreive authorizations.</param>
		/// <returns></returns>
		List<FOLMembershipUser> Authorization_GetListByApplicationGroupId(string applicationGroupId);

		/// <summary>
		/// Gets the list of authorizations for a specific application group.
		/// </summary>
		/// <param name="applicationGroupId">The application group id to retreive authorizations.</param>
		/// <returns></returns>
		List<AuthorizationItem> AzManRole_GetListByApplicationGroupId(string applicationGroupId);

		/// <summary>
		/// Creates a new ApplicationGroup.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		ApplicationGroup ApplicationGroup_Create(ApplicationGroup item);

		/// <summary>
		/// Deletes an ApplicationGroup entity.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool ApplicationGroup_Delete(ApplicationGroup item);

		/// <summary>
		/// Gets all the application groups.
		/// </summary>
		/// <returns></returns>
		List<IAzManApplicationGroup> ApplicationGroup_GetAll();

		/// <summary>
		/// Gets an ApplicationGroup by its key.
		/// </summary>
		/// <param name="Key">The Key of the ApplicationGroup.</param>
		/// <returns></returns>
		ApplicationGroup ApplicationGroup_GetItem(string Key);

		/// <summary>
		/// Gets an ApplicationGroup by its name.
		/// </summary>
		/// <param name="name">The name of the ApplicationGroup.</param>
		/// <returns></returns>
		ApplicationGroup ApplicationGroup_GetItemByName(string name);

		/// <summary>
		/// Generates the json structure of the ApplicationGroup treeview.
		/// </summary>
		/// <returns></returns>
		List<TreeNode> ApplicationGroup_GetTree();

		/// <summary>
		/// Updates an ApplicationGroup.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		ApplicationGroup ApplicationGroup_Update(ApplicationGroup item);

		/// <summary>
		/// Gets all authorizations items of a specified user.
		/// </summary>
		/// <param name="username">The user name. If null string is passed (or an empty string) then the current user is retreived.</param>
		/// <param name="filterTypeOnly">if set to <c>true</c> [filter type only].</param>
		/// <returns></returns>
		List<AuthorizationItem> AuthorizationItem_GetAll(string username, bool filterTypeOnly = false);

		/// <summary>
		/// Checks the access.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="itemName">Name of the item.</param>
		/// <returns></returns>
		bool CheckAccess(string username, string itemName);

		/// <summary>
		/// Gets an AuthorizationItem by its name.
		/// </summary>
		/// <param name="name">The name of the AuthorizationItem.</param>
		/// <returns></returns>
		AuthorizationItem AuthorizationItem_GetByName(string name);

		/// <summary>
		/// Gets a store of auhorization for a specific application groups.
		/// </summary>
		/// <param name="id">The AzMan filter id for which to retreive the authorizations.</param>
		/// <returns></returns>
		List<ApplicationGroup> AzManFilter_Authorization_GetList(string id);

		/// <summary>
		/// Gets a store of user authorization for a specific application groups.
		/// </summary>
		/// <param name="filterId">The AzMan filter id for which to retreive the authorizations.</param>
		/// <returns></returns>
		List<FOLMembershipUser> AzManFilter_UserAuthorization_GetList(string filterId);

		/// <summary>
		/// Gets all azman filters with paging and search keyword
		/// </summary>
		/// <param name="paging"></param>
		/// <param name="totalRecordCount"></param>
		/// <returns></returns>
		List<AuthorizationItem> AzManFilter_GetAll(PagingParameter paging, out int totalRecordCount);

		/// <summary>
		/// Gets azman filters by key location
		/// </summary>
		/// <param name="keyLocations"></param>
		List<AuthorizationItem> AzManFilter_GetByLocation(string[] keyLocations);

		/// <summary>
		/// Creates a new filter item.
		/// </summary>
		/// <param name="parent">The id of the parent of the item.</param>
		/// <param name="name">The name of the item.</param>
		/// <param name="description">The description of the item.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="filterLink"> </param>
		/// <returns>
		/// The authorization item created. Null if the creation failed.
		/// </returns>
		AuthorizationItem AzManFilter_Create(string parent, string name, string description, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, List<SecurableEntity> filterLink);

		/// <summary>
		/// Generates the json structure of the AzMan Filter treeview.
		/// </summary>
		/// <param name="itemId">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		List<TreeNode> AzManFilter_GetTree(string itemId);

		/// <summary>
		/// Gets the filter for an item.
		/// </summary>
		/// <param name="id">The id of the item.</param>
		/// <param name="searchByName">True to search by name instead of by the id.</param>
		/// <param name="typeName">Name of the type.</param>
		/// <returns>
		/// The list of filter elements.
		/// </returns>
		List<SecurableEntity> AzManFilter_Filter_GetList(string id, bool searchByName, string typeName);

		/// <summary>
		/// Updates a filter item.
		/// </summary>
		/// <param name="id">The id of the item to update.</param>
		/// <param name="name">The name of the item to update.</param>
		/// <param name="description">The description of the item to update.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="filterLink">The filter link.</param>
		/// <returns>
		/// True if operation was successfull, false otherwise.
		/// </returns>
		IAzManItem AzManFilter_Update(string id, string name, string description, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, List<SecurableEntity> filterLink);

		/// <summary>
		/// Updates a azman task.
		/// </summary>
		/// <param name="id">The id of the item to update.</param>
		/// <param name="name">The name of the item to update.</param>
		/// <param name="description">The description of the item to update.</param>
		/// <returns>
		/// True if operation was successfull, false otherwise.
		/// </returns>
		IAzManItem AzManTask_Update(string id, string name, string description);

		/// <summary>
		/// Deletes an azman item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		bool AzManItem_Delete(AuthorizationItem item);

		/// <summary>
		/// Gtes an AzMan item.
		/// </summary>
		/// <param name="Key">The Key.</param>
		/// <returns></returns>
		AuthorizationItem AzManItem_GetItem(string Key);

		/// <summary>
		/// Gets the children of an AzMan item.
		/// </summary>
		/// <param name="itemId">The Key of the parent node.</param>
		/// <returns></returns>
		List<AuthorizationItem> AzManItem_GetChildren(string itemId);

		/// <summary>
		/// Creates a new Operation item.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="description">The description.</param>
		/// <returns></returns>
		AuthorizationItem AzManOperation_Create(string name, string description);

		/// <summary>
		/// Gets all the AzMan operations.
		/// </summary>
		/// <returns></returns>
		List<AuthorizationItem> AzManOperation_GetAll();

		/// <summary>
		/// Creates a new Role item.
		/// </summary>
		/// <param name="parent">The id of the parent of the item.</param>
		/// <param name="name">The name of the item.</param>
		/// <param name="description">The description of the item.</param>
		/// <param name="filterSpatial">The list of spatial filter elements.</param>
		/// <param name="filterReport">The filter report.</param>
		/// <param name="filterChart">The filter chart.</param>
		/// <param name="filterAlarmDefinition">The filter alarm definition.</param>
		/// <param name="filterDynamicDisplay">The filter dynamic display.</param>
		/// <param name="filterPlaylist">The filter playlist.</param>
		/// <param name="filterChartScheduler">The filter chart scheduler.</param>
		/// <param name="filterPortalTemplate">The filter portal template.</param>
		/// <param name="filterPortalWindow">The filter portal window.</param>
		/// <param name="filterClassificationItem">The filter classification item.</param>
		/// <param name="filterLink"> </param>
		/// <returns>
		/// True if operation was successfull, false otherwise.
		/// </returns>
		AuthorizationItem AzManRole_Create(string parent, string name, string description, List<SecurableEntity> filterSpatial, List<SecurableEntity> filterReport, List<SecurableEntity> filterChart, List<SecurableEntity> filterAlarmDefinition, List<SecurableEntity> filterDynamicDisplay, List<SecurableEntity> filterPlaylist, List<SecurableEntity> filterChartScheduler, List<SecurableEntity> filterPortalTemplate, List<SecurableEntity> filterPortalWindow, List<SecurableEntity> filterClassificationItem, List<SecurableEntity> filterLink);

		/// <summary>
		/// Copies an azman role.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns>True if the copy worked, false otherwise.</returns>
		bool AzManRole_Copy(AuthorizationItem item);

		/// <summary>
		/// Generates the json structure of the AzMan Role treeview.
		/// </summary>
		/// <param name="itemId">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		List<TreeNode> AzManRole_GetTree(string itemId);

		/// <summary>
		/// Get the store of saved AzmanRoles for a specific Chart KPI.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		JsonStore<AuthorizationItem> AzManRole_GetStoreByKeyChartKPI(PagingParameter paging, string KeyChart);

		/// <summary>
		/// Creates a new Task item.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="description">The description.</param>
		/// <returns></returns>
		AuthorizationItem AzManTask_Create(string name, string description);

		/// <summary>
		/// Adds and removes operations.
		/// </summary>
		/// <param name="authorizationItems">The operations.</param>
		/// <param name="authorizationItem">The ret val.</param>
		bool AddRemoveAuthorizationItems(AuthorizationItem authorizationItem, CrudStore<AuthorizationItem> authorizationItems);

		/// <summary>
		/// Gets all the AzMan tasks.
		/// </summary>
		/// <returns></returns>
		List<AuthorizationItem> AzManTask_GetAll();

		/// <summary>
		/// Creates (or recreates) the filter spatial attribute.
		/// </summary>
		/// <param name="item">The item to create the attribute on.</param>
		/// <param name="filters">The filters.</param>
		/// <param name="typeName">Name of the type.</param>
		/// <returns>
		/// The created attribute.
		/// </returns>
		IAzManAttribute<IAzManItem> CreateAttributeFilter(IAzManItem item, List<SecurableEntity> filters, string typeName);

		/// <summary>
		/// Creates the list of authorizations for the specified item.
		/// </summary>
		/// <param name="authorizationItem">The authorization item.</param>
		/// <param name="applicationGroups">The list of application groups authorized.</param>
		void CreateAzManItemApplicationGroups(AuthorizationItem authorizationItem, CrudStore<ApplicationGroup> applicationGroups);

		/// <summary>
		/// Creates the list of authorizations for the specified item.
		/// </summary>
		/// <param name="authorizationItem">The authorization item.</param>
		/// <param name="users">The users.</param>
		void CreateAzManItemMembers(AuthorizationItem authorizationItem, CrudStore<FOLMembershipUser> users);

		/// <summary>
		/// Create members for an application group.
		/// </summary>
		/// <param name="applicationGroup">The application group.</param>
		/// <param name="members">The list of users to add as members of the application group.</param>
		void CreateGroupMembers(ApplicationGroup applicationGroup, CrudStore<FOLMembershipUser> members);

		/// <summary>
		/// Modify the list of authorizations for an application group.
		/// </summary>
		/// <param name="applicationGroup">The application group.</param>
		/// <param name="roles">The roles.</param>
		/// <param name="userName">Name of the user.</param>
		void CreateGroupAuthorizations(ApplicationGroup applicationGroup, CrudStore<AuthorizationItem> roles, string userName = null);

		/// <summary>
		/// Exports the AzMan configuration as a xml stream file.
		/// </summary>
		/// <returns>The xml stream file.</returns>
		MemoryStream Export();

		/// <summary>
		/// Imports the specified XML node.
		/// </summary>
		/// <param name="xmlNode">The XML node.</param>
		/// <param name="importOptions">The import options.</param>
		/// <param name="userName">Name of the user.</param>
		void Import(XmlNode xmlNode, AzmanImportOptions importOptions, string userName = null);

		/// <summary>
		/// Imports the specified XML .
		/// </summary>
		/// <param name="xmlStream">The XML stream.</param>
		/// <param name="importOptions">The import options.</param>
		void Import(Stream xmlStream, AzmanImportOptions importOptions);

		/// <summary>
		/// Gets the authorizations for an item.
		/// </summary>
		/// <param name="id">The id of the item.</param>
		/// <returns>The list of spatial filter elements.</returns>
		List<FOLMembershipUser> GetAuthorization(string id);

		/// <summary>
		/// Gets the complete list of spatial elements associated to a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="filterType">Type of the filter.</param>
		/// <returns>
		/// The list of spatial elements associated to the user.
		/// </returns>
		List<SecurableEntity> GetFilterForUser(string username, Type filterType);

		/// <summary>
		/// Gets the complete list of spatial elements associated to a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="filterTypes">The filter types.</param>
		/// <returns>
		/// The list of spatial elements associated to the user.
		/// </returns>
		Dictionary<string, List<SecurableEntity>> GetFiltersDictionaryForUser(string username, List<Type> filterTypes);

		/// <summary>
		///  Gets a list of the roles that a specified user is in for the configured applicationName.
		/// </summary>
		/// <param name="username">The user to return a list of roles for.</param>
		/// <returns>A string array containing the names of all the roles that the specified user is in for the configured applicationName.</returns>
		string[] GetRolesForUser(string username);

		/// <summary>
		///  We need to override this to allow a custom name for the provider in web.config.
		///  The parent class NetSqlAzManRoleProvider does not respect the name attribute of the configuration section and search for the specific entry "NetSqlAzManRoleProvider"
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="config">The config.</param>
		void Initialize(string name, System.Collections.Specialized.NameValueCollection config);

		/// <summary>
		/// Gets a value indicating whether the specified user is in the specified role for the configured applicationName.
		/// </summary>
		/// <param name="username">The user name to search for.</param>
		/// <param name="roleName">The role to search in.</param>
		/// <returns>true if the specified user is in the specified role for the configured applicationName, false otherwise.</returns>
		bool IsUserInRole(string username, string roleName);

		/// <summary>
		/// Gets the list of application group for a specific user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns></returns>
		List<ApplicationGroup> User_ApplicationGroup_GetList(string username);

		/// <summary>
		/// Removes reference to a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		void User_Remove(string username);

		/// <summary>
		/// Invalidates the storage cache.
		/// Also, creates a new guid for azman cache state - this is done so that if we run this on multiple servers we can know cross server if we need to refresh the cache
		/// </summary>
		void InvalidateStorageCache();

		/// <summary>
		/// Gets the azman tree starting from the given item.
		/// </summary>
		/// <param name="itemId">The item id.</param>
		/// <returns>
		/// A list of tree nodes
		/// </returns>
		List<TreeNode> AzManItem_GetAncestorTree(string itemId);

		/// <summary>
		/// Removes the authorizations for a user.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		void RemoveAuthorizationForUser(string userName);

		/// <summary>
		/// Clears the cache username_UserSSID cache value.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <returns></returns>
		void ClearCache(string username);

        /// <summary>
        /// Retrieves a list of application groups.
        /// </summary>
        /// <returns></returns>
        Dictionary<string, List<string>> Users_ApplicationGroups_GetAll();
	}
}
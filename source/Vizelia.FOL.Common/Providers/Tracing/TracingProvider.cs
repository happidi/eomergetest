﻿using System;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide Tracing services using providers.
	/// </summary>
	public abstract class TracingProvider : GenericProviderBase, ITracingProvider {

		/// <summary>
		/// Gets the trace scope for the given tracing category and related entity key.
		/// </summary>
		/// <param name="traceCategory">The trace category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		public abstract ITraceScope StartTracing(string traceCategory, string relatedKeyEntity);

		/// <summary>
		/// Writes the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public abstract void Write(string message);

		/// <summary>
		/// Writes the specified message with the given severity.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="message">The message.</param>
		public abstract void Write(TraceEntrySeverity severity, string message);

		/// <summary>
		/// Flushes the log.
		/// </summary>
		public abstract void Flush();

		/// <summary>
		/// Writes the specified message with the given severity.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="message">The message.</param>
		/// <param name="category">The category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		public abstract void Write(TraceEntrySeverity severity, string message, string category, string relatedKeyEntity);

		/// <summary>
		/// Traces the specified action with the given parameters.
		/// Documents begin, end and exception if occures.
		/// </summary>
		/// <typeparam name="T">The function result type</typeparam>
		/// <param name="traceCategory">The trace category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		/// <param name="message">The message.</param>
		/// <param name="action">The action.</param>
		public abstract T Trace<T>(string traceCategory, string relatedKeyEntity, string message, Func<T> action);

		/// <summary>
		/// Traces the specified action within an existing tracing scope.
		/// Documents begin, end and exception if occures.
		/// </summary>
		/// <typeparam name="T">The function result type</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="action">The action.</param>
		/// <returns></returns>
		public abstract T Trace<T>(string message, Func<T> action);

		/// <summary>
		/// Writes the specified exception to the trace log.
		/// </summary>
		/// <param name="exception">The exception.</param>
		public abstract void Write(Exception exception);

		/// <summary>
		/// Continues tracing from other thread. Only copies the most recent tracing scope. It needs to be disposed of in the original thread.
		/// </summary>
		/// <param name="originalThreadTraceScope">The scope from other thread.</param>
		/// <returns></returns>
		public abstract ITraceScope ContinueTracing(ITraceScope originalThreadTraceScope);

		/// <summary>
		/// Gets the current trace scope. Returns null if no current scope exists.
		/// </summary>
		/// <returns></returns>
		public abstract ITraceScope GetCurrentTraceScope();
	}
}

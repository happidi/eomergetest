using System;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Represents a trace scope.
	/// </summary>
	public interface ITraceScope : IDisposable {
	}
}
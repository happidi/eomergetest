﻿using System;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Tracing Service static class.
	/// </summary>
	public static class TracingService {
		private static ITracingProvider provider;
		private static readonly GenericProviderCollection<TracingProvider, ITracingProvider> providers = GenericProviderHelper.LoadProviders<TracingProvider, GenericProviderSection, ITracingProvider>("VizeliaTracingSection", out provider);

		/// <summary>
		/// Gets a collection of the Tracing providers.
		/// </summary>
		public static GenericProviderCollection<TracingProvider, ITracingProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Gets the trace writer for the given tracing category and related entity key.
		/// </summary>
		/// <param name="traceCategory">The trace category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		public static ITraceScope StartTracing(string traceCategory, string relatedKeyEntity) {
			ITraceScope traceWriter = provider.StartTracing(traceCategory, relatedKeyEntity);

			return traceWriter;
		}

		/// <summary>
		/// Traces the specified action with the given parameters.
		/// Documents begin, end and exception if occures.
		/// </summary>
		/// <param name="traceCategory">The trace category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		/// <param name="message">The message.</param>
		/// <param name="action">The action.</param>
		public static void Trace(string traceCategory, string relatedKeyEntity, string message, Action action) {
			Trace<object>(traceCategory, relatedKeyEntity, message, () => {
				action();
				return null;
			});
		}

		/// <summary>
		/// Traces the specified action with the given parameters.
		/// Documents begin, end and exception if occures.
		/// </summary>
		/// <param name="traceCategory">The trace category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		/// <param name="message">The message.</param>
		/// <param name="action">The action.</param>
		public static T Trace<T>(string traceCategory, string relatedKeyEntity, string message, Func<T> action) {
			T result = provider.Trace(traceCategory, relatedKeyEntity, message, action);
			return result;
		}

		/// <summary>
		/// Traces the specified action within an existing tracing scope.
		/// Documents begin, end and exception if occures.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="action">The action.</param>
		public static void Trace(string message, Action action) {
			Trace<object>(message, () => {
				action();
				return null;
			});
		}

		/// <summary>
		/// Traces the specified action within an existing tracing scope.
		/// Documents begin, end and exception if occures.
		/// </summary>
		/// <typeparam name="T">The function result type</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="action">The action.</param>
		/// <returns></returns>
		public static T Trace<T>(string message, Func<T> action) {
			T result = provider.Trace(message, action);
			return result;
		}

		/// <summary>
		/// Writes the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public static void Write(string message) {
			provider.Write(message);
		}

		/// <summary>
		/// Writes the specified message.
		/// </summary>
		/// <param name="messageFormat">The message format.</param>
		/// <param name="args">The args.</param>
		public static void Write(string messageFormat, params object[] args) {
			provider.Write(string.Format(messageFormat, args));
		}

		/// <summary>
		/// Writes the specified message with the given severity.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="message">The message.</param>
		public static void Write(TraceEntrySeverity severity, string message) {
			provider.Write(severity, message);
		}

		/// <summary>
		/// Writes the specified message with the given severity.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="messageFormat">The message format.</param>
		/// <param name="args">The args.</param>
		public static void Write(TraceEntrySeverity severity, string messageFormat, params object[] args) {
			provider.Write(severity, string.Format(messageFormat, args));
		}

		/// <summary>
		/// Writes the specified exception to the trace log.
		/// </summary>
		/// <param name="exception">The exception.</param>
		public static void Write(Exception exception) {
			provider.Write(exception);
		}

		/// <summary>
		/// Writes the specified message with the given severity.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="message">The message.</param>
		/// <param name="category">The category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		public static void Write(TraceEntrySeverity severity, string message, string category, string relatedKeyEntity) {
			provider.Write(severity, message, category, relatedKeyEntity);
		}

		/// <summary>
		/// Continues tracing from other thread. Only copies the most recent tracing scope. It needs to be disposed of in the original thread.
		/// </summary>
		/// <param name="originalThreadTraceScope">The scope from other thread.</param>
		/// <returns></returns>
		public static ITraceScope ContinueTracing(ITraceScope originalThreadTraceScope) {
			ITraceScope scope = provider.ContinueTracing(originalThreadTraceScope);
			return scope;
		}

		/// <summary>
		/// Gets the current trace scope. Returns null if no current scope exists.
		/// </summary>
		/// <returns></returns>
		public static ITraceScope GetCurrentTraceScope() {
			ITraceScope scope = provider.GetCurrentTraceScope();
			return scope;
		}
	}

}

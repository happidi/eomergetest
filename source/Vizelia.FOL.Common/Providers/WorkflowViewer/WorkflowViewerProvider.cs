﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide workflow services using custom workflow providers.
	/// </summary>
	public abstract class WorkflowViewerProvider : GenericProviderBase, IWorkflowViewerProvider {

		/// <summary>
		/// Gets the xaml folder.
		/// </summary>
		/// <returns></returns>
		public abstract string GetXamlFolder();

		/// <summary>
		/// Renders the workflow as an image stream.
		/// </summary>
		/// <param name="xamlFileName">The xaml file name (no extension, no path).</param>
		/// <param name="xamlDesign">The xaml design.</param>
		/// <param name="currentState">The current state if the workflow.</param>
		/// <param name="assembliesToLoad">The list of the assemblies to load.</param>
		/// <param name="width">The width of the image.</param>
		/// <param name="height">The height of the image.</param>
		/// <returns></returns>
		public abstract StreamResult RenderWorkflowStream(string xamlFileName, string xamlDesign, string currentState, string[] assembliesToLoad, int width, int height);

		
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IWorkflowViewerProvider : IGenericProviderBase {

		/// <summary>
		/// Gets the xaml folder.
		/// </summary>
		/// <returns></returns>
		string GetXamlFolder();

		/// <summary>
		/// Renders the workflow as an image stream.
		/// </summary>
		/// <param name="xamlFileName">The xaml file name (no extension, no path).</param>
		/// <param name="xamlDesign">The xaml design.</param>
		/// <param name="currentState">The current state if the workflow.</param>
		/// <param name="assembliesToLoad">The list of the assemblies to load.</param>
		/// <param name="width">The width of the image.</param>
		/// <param name="height">The height of the image.</param>
		/// <returns></returns>
		StreamResult RenderWorkflowStream(string xamlFileName, string xamlDesign, string currentState, string[] assembliesToLoad, int width, int height);

		
	}
}

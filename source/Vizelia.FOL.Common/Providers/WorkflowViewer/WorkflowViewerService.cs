﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Workflow Service static class.
	/// </summary>
	public static class WorkflowViewerService {
		private static IWorkflowViewerProvider provider;
		private static GenericProviderCollection<WorkflowViewerProvider, IWorkflowViewerProvider> providers = GenericProviderHelper.LoadProviders<WorkflowViewerProvider, GenericProviderSection, IWorkflowViewerProvider>("VizeliaWorkflowViewerSection", out provider);

		/// <summary>
		/// Gets a collection of the workflow providers.
		/// </summary>
		public static GenericProviderCollection<WorkflowViewerProvider, IWorkflowViewerProvider> Providers {
			get {
				return providers;
			}
		}


		/// <summary>
		/// Renders the workflow as an image stream.
		/// </summary>
		/// <param name="xamlFileName">The xaml file name (no extension, no path).</param>
		/// <param name="xamlDesign">The xaml design.</param>
		/// <param name="currentState">The current state if the workflow.</param>
		/// <param name="assembliesToLoad">The list of the assemblies to load.</param>
		/// <param name="width">The width of the image.</param>
		/// <param name="height">The height of the image.</param>
		/// <returns></returns>
		public static StreamResult RenderWorkflowStream(string xamlFileName, string xamlDesign, string currentState, string[] assembliesToLoad, int width, int height) {
			return provider.RenderWorkflowStream(xamlFileName, xamlDesign, currentState, assembliesToLoad, width, height);
		}

		/// <summary>
		/// Gets the xaml folder.
		/// </summary>
		/// <returns></returns>
		public static string GetXamlFolder() {
			return provider.GetXamlFolder();
		}
	}

}

﻿using System;
using System.Collections.Generic;
using System.Web;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide Cache services using providers.
	/// </summary>
	public abstract class CacheProvider : GenericProviderBase, ICacheProvider {


		/// <summary>
		/// Adds new CacheItem to cache. 
		/// If another item already exists with the same key, that item is removed before the new item is added. 
		/// If any failure occurs during this process, the cache will not contain the item being added. 
		/// Items added with this method will be not expire, and will have a Normal Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority priority.
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		public abstract void Add(string key, object value);

		/// <summary>
		/// Adds new CacheItem to cache. 
		/// If another item already exists with the same key, that item is removed before the new item is added. 
		/// If any failure occurs during this process, the cache will not contain the item being added. 
		/// Items added with this method will be not expire, and will have a Normal Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority priority.
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		/// <param name="expirationInMinutes">Expires after the given number of minutes. O indicates that the cache should never expired.</param>
		public abstract void Add(string key, object value, double expirationInMinutes);


		/// <summary>
		/// Returns the value associated with the given key.
		/// </summary>
		/// <param name="key">Key of item to return from cache.</param>
		/// <returns></returns>
		public abstract object GetData(string key);


		/// <summary>
		/// Adds a new item to the memory cache.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="expirationInMinutes">The expiration in minutes.</param>
		public virtual void MemoryCacheAdd(string key, object value, double expirationInMinutes = 1000) {
			string cacheKey = EncodeCacheKey(key);
			HttpRuntime.Cache.Add(cacheKey, value, null, DateTime.Now.AddMinutes(expirationInMinutes), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.High, null);
		}


		/// <summary>
		/// Returns the value associated with the given key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public virtual object MemoryCacheGetData(string key) {
			string cacheKey = EncodeCacheKey(key);
			object data = HttpRuntime.Cache.Get(cacheKey);
			return data;
		}

		/// <summary>
		/// Removes the given item from the memory cache. 
		/// </summary>
		/// <param name="key">The key.</param>
		public virtual void MemoryCacheRemove(string key) {
			string cacheKey = EncodeCacheKey(key);
			HttpRuntime.Cache.Remove(cacheKey);
		}

		/// <summary>
		/// Encodes the cache key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		protected virtual string EncodeCacheKey(string key) {
			string encodeCacheKey = string.Format("{0}::{1}", ContextHelper.ApplicationName, key);
			return encodeCacheKey;
		}

		/// <summary>
		/// Decoded the cache key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		protected virtual string DecodeCacheKey(string key) {
			string decodedCacheKey = key.Substring(key.IndexOf("::", StringComparison.Ordinal) + 2);
			return decodedCacheKey;
		}
		/// <summary>
		/// Removes the given item from the cache. 
		/// If no item exists with that key, this method does nothing.
		/// </summary>
		/// <param name="key">Key of item to remove from cache.</param>
		public abstract void Remove(string key);

		/// <summary>
		/// Locks the specified action.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="lockName">Name of the lock.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval in milliseconds.</param>
		/// <param name="async">if set to <c>true</c> [async].</param>
		public abstract void Lock(Action action, string lockName = null, int maxRetries = 3, int retryInterval = 100, bool async = false);

		/// <summary>
		/// Gets the existing items.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public abstract IDictionary<string, object> GetData(List<string> keys);
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface ICacheProvider : IGenericProviderBase {

		/// <summary>
		/// Adds new CacheItem to cache. 
		/// If another item already exists with the same key, that item is removed before the new item is added. 
		/// If any failure occurs during this process, the cache will not contain the item being added. 
		/// Items added with this method will be not expire, and will have a Normal Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority priority.
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		void Add(string key, object value);

		/// <summary>
		/// Adds new CacheItem to cache. 
		/// If another item already exists with the same key, that item is removed before the new item is added. 
		/// If any failure occurs during this process, the cache will not contain the item being added. 
		/// Items added with this method will be not expire, and will have a Normal Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority priority.
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		/// <param name="expirationInMinutes">Expires after the given number of minutes. O indicates that the cache should never expired.</param>
		void Add(string key, object value, double expirationInMinutes);


		/// <summary>
		/// Returns the value associated with the given key.
		/// </summary>
		/// <param name="key">Key of item to return from cache.</param>
		/// <returns></returns>
		object GetData(string key);

		/// <summary>
		/// Adds a new item to the memory cache.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="expirationInMinutes">The expiration in minutes.</param>
		void MemoryCacheAdd(string key, object value, double expirationInMinutes = 1000);


		/// <summary>
		/// Returns the value associated with the given key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		object MemoryCacheGetData(string key);

		/// <summary>
		/// Removes the given item from the memory cache. 
		/// </summary>
		/// <param name="key">The key.</param>
		void MemoryCacheRemove(string key);

		/// <summary>
		/// Removes the given item from the cache. 
		/// If no item exists with that key, this method does nothing.
		/// </summary>
		/// <param name="key">Key of item to remove from cache.</param>
		void Remove(string key);


		/// <summary>
		/// Locks the specified action.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="lockName">Name of the lock.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval in milliseconds.</param>
		/// <param name="async">if set to <c>true</c> [async].</param>
		void Lock(Action action, string lockName = null, int maxRetries = 3, int retryInterval = 100, bool async = false);


		/// <summary>
		/// Gets the existing items.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		IDictionary<string, object> GetData(List<string> keys);
	}
}

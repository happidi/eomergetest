﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Cache Service static class.
	/// </summary>
	public static class CacheService {

		private static ICacheProvider provider;
		private static GenericProviderCollection<CacheProvider, ICacheProvider> providers;

		static CacheService() {
			//Debugger.Launch();
			providers = GenericProviderHelper.LoadProviders<CacheProvider, GenericProviderSection, ICacheProvider>("VizeliaCacheSection", out provider);
		}

		/// <summary>
		/// Gets a collection of the Cache providers.
		/// </summary>
		public static GenericProviderCollection<CacheProvider, ICacheProvider> Providers {
			get { return providers; }
		}

		/// <summary>
		/// Adds new CacheItem to cache. 
		/// If another item already exists with the same key, that item is removed before the new item is added. 
		/// If any failure occurs during this process, the cache will not contain the item being added. 
		/// Items added with this method will be not expire, and will have a Normal Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority priority.
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		public static void Add(string key, object value) {
			provider.Add(key, value);
		}

		/// <summary>
		/// Adds new CacheItem to cache. 
		/// If another item already exists with the same key, that item is removed before the new item is added. 
		/// If any failure occurs during this process, the cache will not contain the item being added. 
		/// Items added with this method will be not expire, and will have a Normal Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority priority.
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		/// <param name="expirationInMinutes">Expires after the given number of minutes. O indicates that the cache should never expired.</param>
		public static void Add(string key, object value, double expirationInMinutes) {
			provider.Add(key, value, expirationInMinutes);
		}

		/// <summary>
		/// Gets the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="createNewIfNull">if set to <c>true</c> [create new if null].</param>
		/// <returns></returns>
		public static T Get<T>(string key, bool createNewIfNull = false) where T : new() {

			var retVal = GetData(key);
			if (retVal is T) {
				return (T)retVal;
			}

			T castRetVal = createNewIfNull ? Helper.CreateFromType<T>() : default(T);
			return castRetVal;
		}

		/// <summary>
		/// Gets the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="cacheMissFunction">The logic to put data into the cache in case of a cache miss.</param>
		/// <param name="expirationInMinutes">The expiration in minutes. Default to 120. Use a value of 0 if you want the item to always stay in the cache</param>
		/// <param name="location">The location.</param>
		/// <param name="validationFunction">The validation function.</param>
		/// <returns></returns>
		public static T Get<T>(string key, Func<T> cacheMissFunction, Func<T, bool> validationFunction, double expirationInMinutes = 120, CacheLocation location = CacheLocation.Distributed) {
			T retval;
			if (location == CacheLocation.Distributed) {
				object data = GetData(key);
				if (data != null) {
					retval = (T)data;
				}
				else {
					retval = cacheMissFunction();
					if (validationFunction(retval)) {
						if (expirationInMinutes <= 0)
							Add(key, retval);
						else
							Add(key, retval, expirationInMinutes);
					}
				}
			}
			else {
				object data = MemoryCacheGetData(key);
				if (data != null) {
					retval = (T)data;
				}
				else {
					retval = cacheMissFunction();
					if (validationFunction(retval)) {
						if (expirationInMinutes <= 0)
							MemoryCacheAdd(key, retval);
						else
							MemoryCacheAdd(key, retval, expirationInMinutes);
					}
				}
			}
			return retval;
		}

		/// <summary>
		/// Gets the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <param name="cacheMissFunction">The cache miss function.</param>
		/// <param name="expirationInMinutes">The expiration in minutes.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		public static T Get<T>(string key, Func<T> cacheMissFunction, double expirationInMinutes = 120, CacheLocation location = CacheLocation.Distributed) {
			return Get(key, cacheMissFunction, x => true, expirationInMinutes, location);
		}

		/// <summary>
		/// Returns the value associated with the given key.
		/// </summary>
		/// <param name="key">Key of item to return from cache.</param>
		/// <returns></returns>
		public static object GetData(string key) {
			return provider.GetData(key);
		}

		/// <summary>
		/// Adds a new item to the memory cache.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="expirationInMinutes">The expiration in minutes.</param>
		public static void MemoryCacheAdd(string key, object value, double expirationInMinutes = 1000) {
			provider.MemoryCacheAdd(key, value, expirationInMinutes);
		}


		/// <summary>
		/// Returns the value associated with the given key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public static object MemoryCacheGetData(string key) {
			return provider.MemoryCacheGetData(key);
		}

		/// <summary>
		/// Removes the given item from the memory cache. 
		/// </summary>
		/// <param name="key">The key.</param>
		public static void MemoryCacheRemove(string key) {
			provider.MemoryCacheRemove(key);
		}

		/// <summary>
		/// Removes the given item from the cache. 
		/// If no item exists with that key, this method does nothing.
		/// </summary>
		/// <param name="key">Key of item to remove from cache.</param>
		public static void Remove(string key) {
			provider.Remove(key);
		}


		/// <summary>
		/// Locks the specified action.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="lockName">Name of the lock.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval in millisecondes.</param>
		/// <param name="async">if set to <c>true</c> [async].</param>
		public static void Lock(Action action, string lockName = null, int maxRetries = 3, int retryInterval = 100, bool async = false) {
			provider.Lock(action, lockName, maxRetries, retryInterval, async);
		}

		/// <summary>
		/// Gets the existing items.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public static IDictionary<string, object> GetData(List<string> keys) {
			return provider.GetData(keys);
		}
	}
}
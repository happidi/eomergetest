﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// External Crud Notification Service static class.
	/// </summary>
	public static class FormulaEvaluatorService {
		private const int const_max_execution_seconds = 10;
		private static IFormulaEvaluatorProvider provider = null;

		private static GenericProviderCollection<FormulaEvaluatorProvider, IFormulaEvaluatorProvider> providers = GenericProviderHelper.LoadProviders<FormulaEvaluatorProvider, GenericProviderSection, IFormulaEvaluatorProvider>("VizeliaFormulaEvaluatorSection", out provider);


		/// <summary>
		/// Gets a collection of the FormulaEvaluator providers.
		/// </summary>
		public static GenericProviderCollection<FormulaEvaluatorProvider, IFormulaEvaluatorProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Evaluates the specified calculated serie.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="calcul">The calculated serie.</param>
		/// <param name="context">When supplied and there is a formula error, an entry is written
		/// to the Chart's Warnings.</param>
		/// <returns></returns>
		public static DataSerie Evaluate(List<DataSerie> series, CalculatedSerie calcul, IFormulaEvaluatorContext context = null) {
			DataSerie retVal = null;
			Thread thread = null;
			var task = Helper.StartNewThread(() => {
				try {
					thread = Thread.CurrentThread;
					retVal = provider.Evaluate(series, calcul, context);
				}
				catch (Exception ex) {
					TracingService.Write(ex);
				}
			});

			if (!task.Wait(const_max_execution_seconds * 1000)) {
				if (thread != null) {
					thread.Abort();
				}
				TracingService.Write(string.Format("FormulaEvaluatorService.Evaluate - Evaluation of data series with '{0}' took more than {1} seconds", calcul.Formula, const_max_execution_seconds));
			}

			return retVal;
		}

		/// <summary>
		/// Evaluates the specified calculated series.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="calculs">The calculs.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		/// <returns></returns>
		public static List<Tuple<DataSerie, CalculatedSerie, Exception>> Evaluate(
			List<DataSerie> series, List<CalculatedSerie> calculs, IFormulaEvaluatorContext context = null) {
			List<Tuple<DataSerie, CalculatedSerie, Exception>> retVal = null;
			Thread thread = null;
			var task = Helper.StartNewThread(() => {
				try {
					thread = Thread.CurrentThread;
					retVal = provider.Evaluate(series, calculs, context);
				}
				catch (Exception ex) {
					TracingService.Write(ex);
				}
			});

			if (!task.Wait(const_max_execution_seconds * 1000)) {
				if (thread != null) {
					thread.Abort();
				}
				TracingService.Write(string.Format("FormulaEvaluatorService.Evaluate - Evaluation of data series took more than {0} seconds", const_max_execution_seconds));
			}

			return retVal;
		}

		/// <summary>
		/// Evaluates the specified formula.
		/// </summary>
		/// <param name="formula">The formula.</param>
		/// <param name="keyLocalizationCulture">The key localization culture.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		/// <returns></returns>
		public static double Evaluate(string formula, string keyLocalizationCulture = null, IFormulaEvaluatorContext context = null) {
			double retVal = double.NaN;
			Thread thread = null;
			var task = Helper.StartNewThread(() => {
				try {
					thread = Thread.CurrentThread;
					retVal = provider.Evaluate(formula, keyLocalizationCulture, context);
				}
				catch (Exception ex) {
					TracingService.Write(ex);
				}
			});

			if (!task.Wait(const_max_execution_seconds * 1000)) {
				if (thread != null) {
					thread.Abort();
				}
				TracingService.Write(string.Format("FormulaEvaluatorService.Evaluate - Evaluation of '{0}' took more than {1} seconds", formula, const_max_execution_seconds));
			}

			return retVal;
		}

		/// <summary>
		/// Validates the specified calculated serie.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="calcul">The calculated serie.</param>
		/// <returns></returns>
		public static bool Validate(List<DataSerie> series, CalculatedSerie calcul) {
			return provider.Validate(series, calcul);
		}

		/// <summary>
		/// Validates the specified formula.
		/// </summary>
		/// <param name="formula">The formula.</param>
		/// <returns></returns>
		public static bool Validate(string formula) {
			return provider.Validate(formula);
		}

		/// <summary>
		/// Evaluates the text.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="delimiter">The delimiter.</param>
		/// <param name="returnTextFormat">The return text format.</param>
		/// <param name="keyLocalizationCulture">The key localization culture.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		/// <returns></returns>
		public static string EvaluateText(string text, char delimiter, string returnTextFormat, 
			string keyLocalizationCulture = null, IFormulaEvaluatorContext context = null) {
			//we remove formatting that razor could have introduced.
			text = text.Replace("&#160;", "");
			var delimiterRegex = new Regex(@"" + delimiter + "([^" + delimiter + "]*)" + delimiter, RegexOptions.Compiled);
			var match = delimiterRegex.Match(text);
			while (match.Success) {
				//we remove html tags from inside the formula
				var formula = match.Groups[1].Value.StripTagsCharArray();

				var firstBlank = formula.IndexOf(' ');
				if (firstBlank > 0) {
					var cult = formula.Substring(0, firstBlank);
					if (Helper.IsValidCulture(cult)) {
						keyLocalizationCulture = cult;
						formula = formula.Replace(cult, "");
					}
				}


				double result = Evaluate(formula, keyLocalizationCulture, context);
				var oldValue = match.Groups[0].Value;
				text = text.Replace(oldValue, !double.IsNaN(result) ? result.ToString(returnTextFormat) : "");
				match = delimiterRegex.Match(text);

			}

			return text;
		}
	}

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide FormulaEvaluator services using providers.
	/// </summary>
	public abstract class FormulaEvaluatorProvider : GenericProviderBase, IFormulaEvaluatorProvider {

		/// <summary>
		/// Evaluates the specified calculated serie.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="calcul">The calculated serie.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		/// <returns></returns>
		public abstract DataSerie Evaluate(List<DataSerie> series, CalculatedSerie calcul, IFormulaEvaluatorContext context = null);


		/// <summary>
		/// Evaluates the specified calculated series.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="calculatedSeries">The calculs.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		/// <returns></returns>
		public abstract List<Tuple<DataSerie, CalculatedSerie, Exception>> Evaluate(
			List<DataSerie> series, List<CalculatedSerie> calculatedSeries, IFormulaEvaluatorContext context = null);

		/// <summary>
		/// Evaluates the specified formula.
		/// </summary>
		/// <param name="formula">The formula.</param>
		/// <param name="KeyLocalizationCulture">The key localization culture.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		/// <returns></returns>
		public abstract double Evaluate(string formula, string KeyLocalizationCulture = null, IFormulaEvaluatorContext context = null);

		/// <summary>
		/// Validates the specified calculated serie.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="calcul">The calculated serie.</param>
		/// <returns></returns>
		public abstract bool Validate(List<DataSerie> series, CalculatedSerie calcul);

		/// <summary>
		/// Validates the specified formula.
		/// </summary>
		/// <param name="formula">The formula.</param>
		/// <returns></returns>
		public abstract bool Validate(string formula);

	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IFormulaEvaluatorProvider : IGenericProviderBase {
		/// <summary>
		/// Evaluates the specified calculated serie.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="calcul">The calculated serie.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		/// <returns></returns>
		DataSerie Evaluate(List<DataSerie> series, CalculatedSerie calcul, IFormulaEvaluatorContext context = null);


		/// <summary>
		/// Evaluates the specified calculated series.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="calculatedSeries">The calculs.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		/// <returns></returns>
		List<Tuple<DataSerie, CalculatedSerie, Exception>> Evaluate(
			List<DataSerie> series, List<CalculatedSerie> calculatedSeries, IFormulaEvaluatorContext context = null);

		/// <summary>
		/// Evaluates the specified formula.
		/// </summary>
		/// <param name="formula">The formula.</param>
		/// <param name="KeyLocalizationCulture">The key localization culture.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		/// <returns></returns>
		double Evaluate(string formula, string KeyLocalizationCulture = null, IFormulaEvaluatorContext context = null);

		/// <summary>
		/// Validates the specified calculated serie.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="calcul">The calculated serie.</param>
		/// <returns></returns>
		bool Validate(List<DataSerie> series, CalculatedSerie calcul);

		/// <summary>
		/// Validates the specified formula.
		/// </summary>
		/// <param name="formula">The formula.</param>
		/// <returns></returns>
		bool Validate(string formula);


	}
}
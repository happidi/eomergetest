﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Routes messages into a string list.
	/// </summary>
	public class StringListFormulaEvaluatorContext : IFormulaEvaluatorContext {
		List<string> m_Errors;
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="errors"></param>
		public StringListFormulaEvaluatorContext(List<string> errors) {
			m_Errors = errors;
		}
		/// <summary>
		/// Formula evaluator uses this to report an error in the calculation
		/// back to the caller.
		/// </summary>
		public virtual void ReportError(string message) {
			m_Errors.Add(message);
		}
	}

}

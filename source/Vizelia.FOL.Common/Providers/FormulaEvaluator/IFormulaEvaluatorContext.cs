﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Used by the Formula evaluator system to let the caller of FormulaEvaluatorService
	/// receive any errors generated in the formula evaluation process.
	/// </summary>
	public interface IFormulaEvaluatorContext {
		/// <summary>
		/// Formula evaluator uses this to report an error in the calculation
		/// back to the caller.
		/// </summary>
		/// <param name="message">The problem found in the formula evaluator.</param>
		void ReportError(string message);

	}



}

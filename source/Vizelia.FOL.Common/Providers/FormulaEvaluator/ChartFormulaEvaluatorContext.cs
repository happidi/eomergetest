﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Used by the Formula evaluator system to let the caller of FormulaEvaluatorService
	/// receive any errors generated in the formula evaluation process.
	/// Context for when a Chart uses the Formula Evaluator.
	/// </summary>
	public class ChartFormulaEvaluatorContext : IFormulaEvaluatorContext {
		private Chart m_Chart;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="chart">Chart consuming the formula evaluator</param>
		public ChartFormulaEvaluatorContext(Chart chart) {
			if (chart == null)
				throw new ArgumentNullException();
			m_Chart = chart;
		}

		/// <summary>
		/// Formula evaluator uses this to report an error in the calculation
		/// back to the caller.
		/// </summary>
		/// <param name="message">The problem found in the formula evaluator.</param>
		public virtual void ReportError(string message) {
			m_Chart.Warnings.Add(message);

			message = message + String.Format(" Chart name: {0}.", m_Chart.Title);
			Vizelia.FOL.Providers.TracingService.Write(TraceEntrySeverity.Warning, message, "Chart", m_Chart.GetKey());
		}
	}
}

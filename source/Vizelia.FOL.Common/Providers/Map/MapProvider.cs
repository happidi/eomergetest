﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;
using System.Drawing;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide charting services using custom charting providers.
	/// </summary>
	public abstract class MapProvider : GenericProviderBase, IMapProvider {

		/// <summary>
		/// Returns a list of WeatherLocation based on a search string.
		/// </summary>
		/// <param name="address">the address to search for.</param>
		/// <returns></returns>
		public abstract MapLocation GetLocationFromAddress(string address);


		/// <summary>
		/// Gets the Key to use with the current provider.
		/// </summary>
		/// <returns></returns>
		public abstract string  GetKey();


	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IMapProvider : IGenericProviderBase {

		/// <summary>
		/// Returns a list of WeatherLocation based on a search string.
		/// </summary>
		/// <param name="address">the address to search for.</param>
		/// <returns></returns>
		MapLocation GetLocationFromAddress(string address);

		/// <summary>
		/// Gets the Key to use with the current provider.
		/// </summary>
		/// <returns></returns>
		string GetKey();


	}
}

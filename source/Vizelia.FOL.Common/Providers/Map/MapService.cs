﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;
using System.Collections;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;
using System.Drawing;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Map Service static class.
	/// </summary>
	public static class MapService {
		private static IMapProvider provider = null;
		private static GenericProviderCollection<MapProvider, IMapProvider> providers = GenericProviderHelper.LoadProviders<MapProvider, GenericProviderSection, IMapProvider>("VizeliaMapSection", out provider);

		/// <summary>
		/// Gets a collection of the Map providers.
		/// </summary>
		public static GenericProviderCollection<MapProvider, IMapProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Returns a list of WeatherLocation based on a search string.
		/// </summary>
		/// <param name="address">the address to search for.</param>
		/// <returns></returns>
		public static MapLocation GetLocationFromAddress(string address) {
			return provider.GetLocationFromAddress(address);
		}

		/// <summary>
		/// Gets the Key to use with the current provider.
		/// </summary>
		/// <returns></returns>
		public static string GetKey() {
			return provider.GetKey();
		}

	}

}

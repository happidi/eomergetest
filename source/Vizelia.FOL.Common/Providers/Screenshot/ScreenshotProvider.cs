﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Implement to create a custom Screenshot provider
	/// 
	/// </summary>
	public abstract class ScreenshotProvider : GenericProviderBase, IScreenshotProvider {
		/// <summary>
		/// Get a stream result which contains a screenshot of the given url
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <param name="localUrl">if set to <c>true</c> the url is local and the servicescreenshoturl will be appended.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="outputType">Type of the output.</param>
		/// <param name="mimeType">Type of the MIME.</param>
		/// <param name="keyLocalisationCulture">Culture.</param>
		/// <param name="retryNumber"></param>
		/// <returns></returns>
		public abstract StreamResult GetScreenshot(string url, bool localUrl, int width, int height, ScreenshotType outputType, string mimeType, string keyLocalisationCulture, int retryNumber = 0);
	}
	/// <summary>
	/// Interface to implement to create a custom Screenshot provider
	/// </summary>
	public interface IScreenshotProvider : IGenericProviderBase {
		/// <summary>
		/// Get a stream result which contains a screenshot of the given url
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <param name="localUrl">if set to <c>true</c> the url is local and the servicescreenshoturl will be appended.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="outputType">Type of the output.</param>
		/// <param name="mimeType">Type of the MIME.</param>
		/// <param name="keyLocalisationCulture">Culture.</param>
		/// <param name="retryNumber">The retry number.</param>
		/// <returns></returns>
		StreamResult GetScreenshot(string url, bool localUrl, int width, int height, ScreenshotType outputType, string mimeType, string keyLocalisationCulture, int retryNumber = 0);

	}


}
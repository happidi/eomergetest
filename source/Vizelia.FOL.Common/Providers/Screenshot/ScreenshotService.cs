﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using Vizelia.FOL.Common;
using System.Collections;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Provide screenshot services
	/// </summary>
	public static class ScreenshotService
	{
		private static IScreenshotProvider provider = null;
		private static GenericProviderCollection<ScreenshotProvider, IScreenshotProvider> providers;
		

		static ScreenshotService() {
			providers = GenericProviderHelper.LoadProviders<ScreenshotProvider, GenericProviderSection, IScreenshotProvider>("VizeliaScreenshotSection", out provider);	
		}
		 
		/// <summary>
		/// Gets a collection of the Screenshot providers.
		/// </summary>
		public static GenericProviderCollection<ScreenshotProvider, IScreenshotProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Gets the screen shot.
		/// </summary>
		/// <param name="Url">The URL.</param>
		/// <param name="localUrl">if set to <c>true</c> the url is local and the servicescreenshoturl will be appended.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="outputType">Type of the output.</param>
		/// <param name="mimeType">Type of the MIME.</param>
		/// <param name="keyLocalisationCulture">Culture.</param>
		/// <returns></returns>
		public static StreamResult GetScreenshot(string Url, bool localUrl, int width, int height, ScreenshotType outputType, string mimeType, string keyLocalisationCulture) {
			var retVal = provider.GetScreenshot(Url, localUrl, width, height, outputType, mimeType, keyLocalisationCulture);
			return retVal;
		}


	}

}
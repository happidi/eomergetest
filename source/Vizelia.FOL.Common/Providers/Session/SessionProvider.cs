﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Web;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Infrastructure;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide Session services using providers.
	/// </summary>
	public abstract class SessionProvider : GenericProviderBase, ISessionProvider {
		//private const List<string> InternalOperations = new List<string>(){};

		/// <summary>
		/// Abandons the session. (deletes cookie session, and signout the user).
		/// </summary>
		public abstract void Abandon();

		/// <summary>
		/// Adds the specified session key and value to the session.
		/// </summary>
		/// <param name="name">The name of the key.</param>
		/// <param name="value">The value.</param>
		public abstract void Add(string name, object value);

		/// <summary>
		/// Adds the specified session key and value to the session.
		/// </summary>
		/// <param name="dictionary">The dictionary.</param>
		public abstract void Add(IDictionary<string, object> dictionary);

		/// <summary>
		/// Gets the session item with the specified name.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		protected abstract object Get(string sessionID, string name);

		/// <summary>
		/// Creates or slides the session when it already exists.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="userName">The user name.</param>
		/// <param name="force">if set to <c>true</c> force create or slide. Otherwise, depending on context.</param>
		public abstract void CreateOrSlideSession(string sessionID, string userName, bool force = false);

		/// <summary>
		/// Ends the session.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="expirationDate">The expiration date.</param>
		public abstract void EndSession(string sessionID, string userName, DateTime expirationDate);

		/// <summary>
		/// Gets the session item with the specified name.
		/// </summary>
		/// <value></value>
		public abstract object Get(string name);

		/// <summary>
		/// Gets the session ID.
		/// </summary>
		/// <returns></returns>
		public virtual string GetSessionID() {
			var sessionID = (string)ContextHelper.Get("SessionID");
			if (!String.IsNullOrEmpty(sessionID))
				return sessionID;

			try {
				HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
				sessionID = null;

				if (authCookie != null) {
					string encTicket = authCookie.Value;
					if (String.IsNullOrEmpty(encTicket))
						return null;

					// decrypt the ticket if possible. 
					FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(encTicket);

					if (ticket.Expired)
						return null;

					sessionID = new CookieData(ticket.UserData).SessionId;
					ContextHelper.Add("SessionID", sessionID);
				}

				return sessionID;
			}
			catch {
				return null;
			}
		}

		/// <summary>
		/// Removes the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		public abstract void Remove(string name);

		/// <summary>
		/// Removes the specified keys.
		/// </summary>
		/// <param name="keys">The keys.</param>
		public abstract void Remove(List<string> keys);

		/// <summary>
		/// Sessions the exist.
		/// </summary>
		/// <returns></returns>
		public abstract bool SessionExist();

		/// <summary>
		/// Gets the active sessions.
		/// </summary>
		/// <returns></returns>
		public abstract string[] GetActiveSessions();

		/// <summary>
		/// Gets the data.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public abstract IDictionary<string, object> GetData(List<string> keys);


		/// <summary>
		/// Returns true if the session items in cache must be renewed.
		/// </summary>
		/// <param name="sessionId">The session ID.</param>
		/// <param name="force">if set to <c>true</c> force renew.</param>
		/// <returns></returns>
		protected virtual bool MustRenewSession(string sessionId, bool force) {
			var renew = ContextHelper.Get(ContextKey.const_context_renewsession);
			if (!force && (renew == null || (bool)renew == false)) {
				return false;
			}

			// in this case renew is true, so we change its value to avoid multiple renews.
			ContextHelper.Add(ContextKey.const_context_renewsession, false);

			var sessionContext = Get(sessionId, SessionKey.const_session_context) as SessionContext;
			if (sessionContext == null)
				return true;
			var t1 = DateTime.UtcNow;
			var t2 = sessionContext.LastRefreshDate;
			return t1 - t2 >= new TimeSpan(0, 1, 0);
		}

		/// <summary>
		/// Sets the application name for the current request by using ContextHelper;
		/// for easy access this method also returns the application name.
		/// </summary>
		/// <param name="cookieData">The cookie data.</param>
		/// <returns>
		/// the application name
		/// </returns>
		public virtual string SetApplicationName(out CookieData cookieData) {
			cookieData = default(CookieData);
			string applicationName = null;

			HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
			if (authCookie != null) {
				string encTicket = authCookie.Value;
				if (!String.IsNullOrEmpty(encTicket)) {
					// decrypt the ticket if possible. 
					FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(encTicket);

					if (!ticket.Expired) {
						cookieData = new CookieData(ticket.UserData);
						applicationName = cookieData.ApplicationName;

					}
				}
			}

			if (TenantProvisioningService.Mode == MultitenantHostingMode.Isolated) {
				applicationName = VizeliaConfiguration.Instance.Deployment.ApplicationName;
			}

			ContextHelper.ApplicationName = applicationName;
			return applicationName;
		}


		/// <summary>
		/// Sets the authorization cookie.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="createPersistentCookie">if set to <c>true</c> [create persistent cookie].</param>
		/// <param name="pageId">The page ID.</param>
		/// <returns></returns>
		public virtual string SetAuthCookie(string userName, bool createPersistentCookie, out string pageId) {
			ContextHelper.Add(ContextKey.const_context_renewsession, true);
			// Store the Session ID for use in one-way calls.
			string sessionID = StartSession(userName);  // HttpContext.Current.Session.SessionID;

			// Let FormsAuthentication do its cookie-issuing logic (we need it).
			var cookie = FormsAuthentication.GetAuthCookie(userName, createPersistentCookie);

			pageId = GenerateSessionID();
			var cookieData = new CookieData() {
				SessionId = sessionID,
				ApplicationName = ContextHelper.ApplicationName,
				PageId = pageId
			};

			// Decrypt the cookie, add our data, and encrypt it back.
			var ticket = FormsAuthentication.Decrypt(cookie.Value);
			var newTicket = new FormsAuthenticationTicket(
				ticket.Version,
				ticket.Name,
				ticket.IssueDate,
				ticket.Expiration,
				createPersistentCookie,
				cookieData.ToString(),
				ticket.CookiePath);
			cookie.Value = FormsAuthentication.Encrypt(newTicket);
			if (createPersistentCookie)
				cookie.Expires = newTicket.Expiration;
			HttpContext.Current.Request.Cookies.Clear();
			HttpContext.Current.Response.Cookies.Clear();
			HttpContext.Current.Response.Cookies.Set(cookie);

			return sessionID;
		}

		/// <summary>
		/// Starts the session.
		/// </summary>
		/// <param name="userName">The user name starting the session.</param>
		/// <returns></returns>
		public virtual string StartSession(string userName) {
			var sessionID = GenerateSessionID();

			if (!String.IsNullOrEmpty(userName)) {
				CreateOrSlideSession(sessionID, userName);
			}

			ContextHelper.Add("SessionID", sessionID);

			return sessionID;
		}

		/// <summary>
		/// Raises the timeout exception.
		/// </summary>
		public virtual void RaiseTimeoutException() {
			Abandon();
			throw new VizeliaSessionTimeoutException();
		}

		/// <summary>
		/// Generates a new SessionID.
		/// </summary>
		/// <returns></returns>
		public virtual string GenerateSessionID() {
			// the RNG.Create() should only be invoked once
			// should be in a static variable when incorporated into the session/authentication solution

			// ReSharper disable AccessToStaticMemberViaDerivedType
			using (RandomNumberGenerator cryptoProvider = RNGCryptoServiceProvider.Create()) {
				// ReSharper restore AccessToStaticMemberViaDerivedType

				Type sessionIdType = Type.GetType("System.Web.SessionState.SessionId,System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");

				if (sessionIdType != null) {
					MethodInfo createMethod = sessionIdType.GetMethod("Create", BindingFlags.NonPublic | BindingFlags.Static);
					string result = (string)createMethod.Invoke(null, new object[] { cryptoProvider });
					return result;
				}
				return Guid.NewGuid().ToString();
			}
		}


		/// <summary>
		/// Determines whether the requested url is publicly accessible.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the requested url is publicly accessible; otherwise, <c>false</c>.
		/// </returns>
		public virtual bool IsPubliclyAccessible() {
			var operationCtx = OperationContext.Current;
			var serviceName = operationCtx.Host.Description.ServiceType.Name;

			//string url = HttpContext.Current.Request.Url.ToString();
			// session is valid only if it comes from a aspx page or a call to public services.
			return serviceName.Contains("PublicWCF");
			//Uri url = HttpContext.Current.Request.Url;
			//return url.Segments[1].Contains(".aspx");
		}
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface ISessionProvider : IGenericProviderBase {
		/// <summary>
		/// Abandons the session. (deletes cookie session, and signout the user).
		/// </summary>
		void Abandon();

		/// <summary>
		/// Adds the specified session key and value to the session.
		/// </summary>
		/// <param name="name">The name of the key.</param>
		/// <param name="value">The value.</param>
		void Add(string name, object value);


		/// <summary>
		/// Adds the specified session key and value to the session.
		/// </summary>
		/// <param name="dictionary">The dictionary.</param>
		void Add(IDictionary<string, object> dictionary);

		/// <summary>
		/// Creates or slides the session when it already exists.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="userName">The user name.</param>
		/// <param name="force">if set to <c>true</c> force create or slide. Otherwise, depending on context.</param>
		void CreateOrSlideSession(string sessionID, string userName, bool force = false);

		/// <summary>
		/// Ends the session.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="expirationDate">The expiration date.</param>
		void EndSession(string sessionID, string userName, DateTime expirationDate);

		/// <summary>
		/// Gets the session item with the specified name.
		/// </summary>
		/// <value></value>
		object Get(string name);

		/// <summary>
		/// Gets the session ID.
		/// </summary>
		/// <returns></returns>
		string GetSessionID();

		/// <summary>
		/// Removes the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		void Remove(string name);

		/// <summary>
		/// Removes the specified keys.
		/// </summary>
		/// <param name="keys">The keys.</param>
		void Remove(List<string> keys);

		/// <summary>
		/// Sessions the exist.
		/// </summary>
		/// <returns></returns>
		bool SessionExist();

		/// <summary>
		/// Sets the application name for the current request by using ContextHelper; 
		/// for easy access this method also returns the application name.
		/// </summary>
		/// <param name="cookieData"> </param>
		/// <returns>the application name</returns>
		string SetApplicationName(out CookieData cookieData);


		/// <summary>
		/// Sets the authorization cookie.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="createPersistentCookie">if set to <c>true</c> [create persistent cookie].</param>
		/// <param name="pageId">The CSRF GUID.</param>
		/// <returns></returns>
		string SetAuthCookie(string userName, bool createPersistentCookie, out string pageId);

		/// <summary>
		/// Starts the session.
		/// </summary>
		/// <param name="userName">The user name starting the session.</param>
		/// <returns></returns>
		string StartSession(string userName);

		/// <summary>
		/// Raises the timeout exception.
		/// </summary>
		void RaiseTimeoutException();

		/// <summary>
		/// Generates a new SessionID.
		/// </summary>
		/// <returns></returns>
		string GenerateSessionID();

		/// <summary>
		/// Determines whether the requested url is publicly accessible.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the requested url is publicly accessible; otherwise, <c>false</c>.
		/// </returns>
		bool IsPubliclyAccessible();

		/// <summary>
		/// Gets the active sessions.
		/// </summary>
		/// <returns></returns>
		string[] GetActiveSessions();

		/// <summary>
		/// Gets the data.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		IDictionary<string, object> GetData(List<string> keys);
	}
}

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Session Service static class.
	/// </summary>
	public static class SessionService {

		private static ISessionProvider provider;
		private static GenericProviderCollection<SessionProvider, ISessionProvider> providers;

		private static TimeSpan timeout;

		/// <summary>
		/// Request header name to mark internal calls and bypass security.
		/// </summary>
		public const string const_internal_request_header_name = "VIZORIGIN";

		/// <summary>
		/// Request header name to mark GUID for blocking CSRF
		/// </summary>
		public const string const_page_id = "PAGEID"; 

		static SessionService() {
			providers = GenericProviderHelper.LoadProviders<SessionProvider, GenericProviderSection, ISessionProvider>("VizeliaSessionSection", out provider);
			string applicationName = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
			System.Configuration.Configuration cfg = WebConfigurationManager.OpenWebConfiguration(applicationName);
			AuthenticationSection pConfig = (AuthenticationSection)cfg.GetSection("system.web/authentication");
			var frmSection = pConfig.Forms;
			//cookie = frmSection.Name;
			timeout = frmSection.Timeout;
		}

		/// <summary>
		/// Gets the timeout.
		/// </summary>
		/// <value>The timeout.</value>
		public static TimeSpan Timeout {
			get {
				return timeout;
			}
		}

		/// <summary>
		/// Gets a collection of the Session providers.
		/// </summary>
		public static GenericProviderCollection<SessionProvider, ISessionProvider> Providers {
			get { return providers; }
		}

		/// <summary>
		/// Abandons the session. (deletes cookie session, and signout the user).
		/// </summary>
		public static void Abandon() {
			provider.Abandon();
		}
		/// <summary>
		/// Adds the specified session key and value to the session.
		/// </summary>
		/// <param name="name">The name of the key.</param>
		/// <param name="value">The value.</param>
		public static void Add(string name, object value) {
			provider.Add(name, value);
		}

        /// <summary>
        /// Adds the specified session key and value to the session.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        public static void Add(IDictionary<string, object> dictionary ) {
            provider.Add(dictionary);
        }

		/// <summary>
		/// Creates or slides the session when it already exists.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="userName">The user name.</param>
		/// <param name="force">if set to <c>true</c> force create or slide. Otherwise, depending on context.</param>
		public static void CreateOrSlideSession(string sessionID, string userName, bool force = false) {
			provider.CreateOrSlideSession(sessionID, userName, force);
		}

		/// <summary>
		/// Ends the session.
		/// </summary>
		/// <param name="sessionID">The session ID.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="expirationDate">The expiration date.</param>
		public static void EndSession(string sessionID, string userName, DateTime expirationDate) {
			provider.EndSession(sessionID, userName, expirationDate);
		}

		/// <summary>
		/// Gets the session item with the specified name.
		/// </summary>
		/// <value></value>
		public static object Get(string name) {
			return provider.Get(name);
		}

		/// <summary>
		/// Gets the session item with the specified name.
		/// </summary>
		/// <value></value>
		public static IDictionary<string, object> GetData(List<string> keys) {
			return provider.GetData(keys);
		}

		/// <summary>
		/// Gets the session ID.
		/// </summary>
		/// <returns></returns>
		public static string GetSessionID() {
			return provider.GetSessionID();
		}

		/// <summary>
		/// Removes the specified name.
		/// </summary>
		/// <param name="name">The name.</param>
		public static void Remove(string name) {
			provider.Remove(name);
		}

        /// <summary>
        /// Removes the specified keys.
        /// </summary>
        /// <param name="keys">The keys.</param>
        public static void Remove(List<string> keys) {
            provider.Remove(keys);
        }

		/// <summary>
		/// Returns true if a session exists.
		/// </summary>
		/// <returns></returns>
		public static bool SessionExist() {
			return provider.SessionExist();
		}


		/// <summary>
		/// Sets the application name for the current request by using ContextHelper; 
		/// for easy access this method also returns the application name.
		/// </summary>
		/// <param name="cookieData">The cookie data.</param>
		/// <returns>the application name</returns>
		public static string SetApplicationName(out CookieData cookieData) {
			return provider.SetApplicationName(out cookieData);
		}


		/// <summary>
		/// Prepares the must renew session.
		/// </summary>
		public static void PrepareMustRenewSession() {
			HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
			if (authCookie == null || string.IsNullOrEmpty(authCookie.Value))
				return;
			string encTicket = authCookie.Value;
			if (String.IsNullOrEmpty(encTicket))
				return;

			FormsAuthenticationTicket tOld = FormsAuthentication.Decrypt(encTicket);
			
			if (tOld == null) {
				return;
			}
			
			//var cookieData = new CookieData(tOld.UserData);
			//var sessionID = cookieData.SessionId;

			if (tOld.Expired) {
				ContextHelper.Add(ContextKey.const_context_renewsession, true);
				//TracingService.Write(TraceEntrySeverity.Information, "Must renew session " + sessionID);
				return;
			}
			DateTime utcNow = DateTime.UtcNow;
			TimeSpan span = utcNow - tOld.IssueDate.ToUniversalTime();
			TimeSpan span2 = tOld.Expiration.ToUniversalTime() - utcNow;
			if (span2 <= span) {
				ContextHelper.Add(ContextKey.const_context_renewsession, true);
				//TracingService.Write(TraceEntrySeverity.Information, "Must renew session " + sessionID);
			}
		}

		/// <summary>
		/// Raises the timeout exception.
		/// </summary>
		public static void RaiseTimeoutException() {
			Abandon();
			//HttpContext.Current.Request.Cookies.Clear();
			throw new VizeliaSessionTimeoutException();
		}

		/// <summary>
		/// Starts the session.
		/// </summary>
		/// <param name="userName">The user name starting the session.</param>
		/// <returns></returns>
		public static string StartSession(string userName) {
			return provider.StartSession(userName);
		}

		/// <summary>
		/// Generates a new SessionID.
		/// </summary>
		/// <returns></returns>
		public static string GenerateSessionID() {
			return provider.GenerateSessionID();
		}

		/// <summary>
		/// Sets the authorization cookie.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="createPersistentCookie">if set to <c>true</c> [create persistent cookie].</param>
		/// <param name="pageId">The page ID.</param>
		/// <returns></returns>
		public static string SetAuthCookie(string userName, bool createPersistentCookie,out string pageId) {
			return provider.SetAuthCookie(userName, createPersistentCookie,out pageId);
		}


		/// <summary>
		/// Determines whether the requested url is publicly accessible.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the requested url is publicly accessible; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsPubliclyAccessible() {
			return provider.IsPubliclyAccessible();
		}

		/// <summary>
		/// Gets the active sessions.
		/// </summary>
		/// <returns></returns>
		public static string[] GetActiveSessions() {
			return provider.GetActiveSessions();
		}
	}
}
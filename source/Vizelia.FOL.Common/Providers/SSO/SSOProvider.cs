﻿using System;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Diagnostics;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide SSO services using custom SSO providers.
	/// </summary>
	[ApplyNoPolicies]
	public abstract class SSOProvider : GenericProviderBase, ISSOProvider {
		private const string const_attribute_error_redirect_to = "errorRedirectTo";
		private const string const_attribute_open_id_url = "openIdUrl";
		private const string const_attribute_icon_cls = "iconCls";
		private const string const_attribute_is_runnable_from_login = "isRunnableFromLogin";
		private const string const_attribute_msg_code = "msgCode";
		private const string const_attribute_create_if_not_found = "createIfNotFound";
		private const string const_attribute_id_property_path = "idPropertyPath";
		private const string const_attribute_user_suffix = "userSuffix";


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		/// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
		///   
		/// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		///   
		/// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.</exception>
		public override void Initialize(string name, NameValueCollection config) {
			base.Initialize(name, config);

			if ( config[const_attribute_error_redirect_to] == null) {
				throw new ProviderException(const_attribute_error_redirect_to + " is missing");
			}
			try {
				ErrorRedirectTo = config[const_attribute_error_redirect_to];
				config.Remove(const_attribute_error_redirect_to);
			}
			catch (UriFormatException) {
				throw new ProviderException("ErrorRedirectTo is not a valid URI " + config[const_attribute_error_redirect_to]);
			}
			

			if (!string.IsNullOrWhiteSpace(config[const_attribute_user_suffix])) {
				UserSuffix = config[const_attribute_user_suffix];
				config.Remove(const_attribute_user_suffix);
			}
			else {
				UserSuffix = string.Empty;
			}

			if (String.IsNullOrEmpty(config[const_attribute_is_runnable_from_login])) {
				throw new ProviderException(const_attribute_is_runnable_from_login + " is missing");
			}
			bool isRunnableFromLogin = false; 
			bool.TryParse(config[const_attribute_is_runnable_from_login],out isRunnableFromLogin);
			IsRunnableFromLogin = isRunnableFromLogin;

			config.Remove(const_attribute_is_runnable_from_login);


			if (String.IsNullOrEmpty(config[const_attribute_create_if_not_found])) {
				throw new ProviderException(const_attribute_create_if_not_found + " is missing");
			}
			bool createIfNotFound = false;
			bool.TryParse(config[const_attribute_create_if_not_found], out createIfNotFound);
			CreateIfNotFound = createIfNotFound;

			config.Remove(const_attribute_create_if_not_found);
			
			if (!String.IsNullOrEmpty(config[const_attribute_icon_cls])) {
				IconCls = config[const_attribute_icon_cls];
				config.Remove(const_attribute_icon_cls);
			}
			

			 

			if (!String.IsNullOrEmpty(config[const_attribute_id_property_path])) {
				IdPropertyPath = config[const_attribute_id_property_path];
				config.Remove(const_attribute_id_property_path);	
			}
			
			


			
			ApplicationName = ContextHelper.ApplicationName;

			if (String.IsNullOrEmpty(config[const_attribute_msg_code])) {
				MsgCode = Name;
			}
			else {
				MsgCode = config[const_attribute_msg_code];
			}

			try {
				if (!String.IsNullOrWhiteSpace(config[const_attribute_open_id_url])) {
					OpenIdUrl = config[const_attribute_open_id_url];
				}
				
			}
			catch (Exception) {
				
			}
			
		}

		/// <summary>
		/// Gets or sets the icon CLS.
		/// </summary>
		/// <value>
		/// The icon CLS.
		/// </value>
		public string IconCls { get; set; }


		/// <summary>
		/// Gets or sets the id property path.
		/// </summary>
		/// <value>
		/// The id property path.
		/// </value>
		public string IdPropertyPath { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this provider is runnable from login.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this provider is runnable from login; otherwise, <c>false</c>.
		/// </value>
		public bool IsRunnableFromLogin { get; set; }

		/// <summary>
		/// Gets the url to redirect if an error occurs.
		/// </summary>
		public string ErrorRedirectTo { get; set; }


		/// <summary>
		/// Gets or sets the user suffix.
		/// </summary>
		/// <value>
		/// The user suffix.
		/// </value>
		public string UserSuffix { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to create if not found.
		/// </summary>
		/// <value>
		///   <c>true</c> if we want to create if not found; otherwise, <c>false</c>.
		/// </value>
		public bool CreateIfNotFound { get; set; }
		/// <summary>
		/// Gets or sets the name of the application.
		/// </summary>
		/// <value>
		/// The name of the application.
		/// </value>
		public string ApplicationName { get; set; }

		/// <summary>
		/// Gets or sets the open id URL.
		/// </summary>
		/// <value>
		/// The open id URL.
		/// </value>
		public string OpenIdUrl { get; set; }


		/// <summary>
		/// Gets or sets the message code for the display name for the provider.
		/// </summary>
		/// <value>
		/// The message code.
		/// </value>
		public string MsgCode { get; set; }



		/// <summary>
		/// Validates a username out of the secret secret.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <returns>
		/// True if validation succeeded, false otherwise
		/// </returns>
		public abstract UserDetails Validate(out string applicationName);

		/// <summary>
		/// Gets the user.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="userSuffix">The suffix attached to the username.</param>
		/// <returns></returns>
		public abstract FOLMembershipUser GetUser(string username, string userSuffix);


		/// <summary>
		/// Creates the user.
		/// </summary>
		/// <param name="userDetails">The user details.</param>
		/// <param name="userSuffix">The suffix attached to the username.</param>
		/// <returns></returns>
		public abstract string CreateUser(UserDetails userDetails, string userSuffix);

	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface ISSOProvider : IGenericProviderBase {
		/// <summary>
		/// Validates a username out of the secret secret.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <returns>
		/// True if validation succeeded, false otherwise
		/// </returns>
		[ApplyNoPolicies]
		UserDetails Validate(out string applicationName);

		/// <summary>
		/// Gets the user.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="userSuffix">The suffix attached to the username.</param>
		/// <returns></returns>
		FOLMembershipUser GetUser(string username, string userSuffix);

		/// <summary>
		/// Creates the user.
		/// </summary>
		/// <param name="userDetails">The user details.</param>
		/// <param name="userSuffix">The suffix attached to the username.</param>
		/// <returns></returns>
		string CreateUser(UserDetails userDetails, string userSuffix);

		/// <summary>
		/// Gets the url to redirect if an error occurs.
		/// </summary>
		string ErrorRedirectTo { get; }

		/// <summary>
		/// Gets or sets the user suffix.
		/// </summary>
		/// <value>
		/// The user suffix.
		/// </value>
		string UserSuffix { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to create if not found.
		/// </summary>
		/// <value>
		///   <c>true</c> if we want to create if not found; otherwise, <c>false</c>.
		/// </value>
		bool CreateIfNotFound { get; set; }

		/// <summary>
		/// Gets or sets the name of the application.
		/// </summary>
		/// <value>
		/// The name of the application.
		/// </value>
		string ApplicationName { get; set; }

		/// <summary>
		/// Gets or sets the open id URL.
		/// </summary>
		/// <value>
		/// The open id URL.
		/// </value>
		string OpenIdUrl { get; set; }

		/// <summary>
		/// Gets or sets the message code for the display name for the provider.
		/// </summary>
		/// <value>
		/// The message code.
		/// </value>
		string MsgCode { get; set; }


		/// <summary>
		/// Gets or sets the id property path.
		/// </summary>
		/// <value>
		/// The id property path.
		/// </value>
		string IdPropertyPath { get; set; }

	}
}

﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// SSO Service static class.
	/// </summary>
	public static class SSOService {
		private static ISSOProvider provider = null;
		private static GenericProviderCollection<SSOProvider, ISSOProvider> providers = GenericProviderHelper.LoadProviders<SSOProvider, GenericProviderSection, ISSOProvider>("VizeliaSSOSection", out provider);

		/// <summary>
		/// Gets a collection of the SSO providers.
		/// </summary>
		public static GenericProviderCollection<SSOProvider, ISSOProvider> Providers {
			get {
				return providers;
			}
		}


		/// <summary>
		/// Validates a username out of the secret secret.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="errorRedirectTo">The url to redirect to in case of an error </param>
		/// <param name="providerName">Name of the provider.</param>
		/// <returns>
		/// True if validation succeeded, false otherwise
		/// </returns>
		public static bool Validate(out string username, out string applicationName, out string errorRedirectTo ,string providerName = "") {
			ISSOProvider currentProvider = provider;
			if (providerName != "") {
				currentProvider = providers[providerName];
			}
			var userDetails = currentProvider.Validate(out applicationName);
			errorRedirectTo = !string.IsNullOrWhiteSpace(userDetails.ErrorRedirect) ? userDetails.ErrorRedirect : currentProvider.ErrorRedirectTo;
			FOLMembershipUser user = null;
			if (userDetails.IsValidated) {
				TenantHelper.RunInDifferentTenantContext(applicationName, () => user = currentProvider.GetUser(userDetails.Username, currentProvider.UserSuffix));
				if (user != null) {
					username = user.UserName;
					return true;
				}
				else {
					if (currentProvider.CreateIfNotFound) {
						string userName = string.Empty;
						TenantHelper.RunInDifferentTenantContext(applicationName, () => userName = currentProvider.CreateUser(userDetails, currentProvider.UserSuffix), "Admin");
						username = userName;
						return true;
					}
					else {
						username = string.Empty;
						return false;
					}
				}
			}
			username = string.Empty;
			return false;
		}


		/// <summary>
		/// Gets or sets the open id URL.
		/// </summary>
		/// <value>
		/// The open id URL.
		/// </value>
		public static string GetOpenIdUrl(string providerName = "") {
			if (providerName != "") {
				return providers[providerName].OpenIdUrl;
			}
			return provider.OpenIdUrl;
		}

		/// <summary>
		/// Gets the name of the application.
		/// </summary>
		/// <value>
		/// The name of the application.
		/// </value>
		public static string GetApplicationName(string providerName = "") {
			if (providerName != "") {
				return providers[providerName].ApplicationName;
			}
			return provider.ApplicationName;
		}
	}
}

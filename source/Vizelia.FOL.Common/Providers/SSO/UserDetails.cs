﻿namespace Vizelia.FOL.Providers {
	/// <summary>
	/// A class to return user details from SSO providers, has username and isValidated (Mandatory) and Name and Email (Optional) 
	/// </summary>
	public class UserDetails {

		/// <summary>
		/// Initializes a new instance of the <see cref="UserDetails"/> class.
		/// </summary>
		/// <param name="isValidated">if set to <c>true</c> [is validated].</param>
		/// <param name="username">The username.</param>
		public UserDetails(bool isValidated, string username) {
			IsValidated = isValidated;
			Username = username;
		}
		/// <summary>
		/// Gets or sets a value indicating whether this instance is validated.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is validated; otherwise, <c>false</c>.
		/// </value>
		public bool IsValidated { get; set; }
		/// <summary>
		/// Gets or sets the username.
		/// </summary>
		/// <value>
		/// The username.
		/// </value>
		public string Username { get; set; }
		/// <summary>
		/// Gets or sets the first name.
		/// </summary>
		/// <value>
		/// The first name.
		/// </value>
		public string FirstName { get; set; }
		/// <summary>
		/// Gets or sets the last name.
		/// </summary>
		/// <value>
		/// The last name.
		/// </value>
		public string LastName { get; set; }
		/// <summary>
		/// Gets or sets the email.
		/// </summary>
		/// <value>
		/// The email.
		/// </value>
		public string Email { get; set; }

		/// <summary>
		/// Gets or sets the error redirect. Only set if the sso provider has a dynamic error page per login.
		/// </summary>
		/// <value>
		/// The error redirect.
		/// </value>
		public string ErrorRedirect { get; set; }
	}
}
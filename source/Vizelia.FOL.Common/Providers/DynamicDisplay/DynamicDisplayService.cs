﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;
using System.Collections;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// DynamicDisplay Service static class.
	/// </summary>
	public static class DynamicDisplayService {
		private static IDynamicDisplayProvider provider = null;
		private static GenericProviderCollection<DynamicDisplayProvider, IDynamicDisplayProvider> providers = GenericProviderHelper.LoadProviders<DynamicDisplayProvider, GenericProviderSection, IDynamicDisplayProvider>("VizeliaDynamicDisplaySection", out provider);

		/// <summary>
		/// Gets a collection of the dynamicdisplay providers.
		/// </summary>
		public static GenericProviderCollection<DynamicDisplayProvider, IDynamicDisplayProvider> Providers {
			get {
				return providers;
			}
		}


		/// <summary>
		/// Renders the dynamic display as a stream.
		/// </summary>
		/// <param name="display">The dynamicdisplay.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
			/// <returns></returns>
		public static Tuple<StreamResult, Padding> RenderDynamicDisplayStream(DynamicDisplay display, int width, int height) {
			return provider.RenderDynamicDisplayStream(display, width, height);
		}
	}

}

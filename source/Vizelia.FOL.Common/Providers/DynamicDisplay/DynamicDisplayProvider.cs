﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide dynamicdisplay services using custom dynamicdisplay providers.
	/// </summary>
	public abstract class DynamicDisplayProvider : GenericProviderBase, IDynamicDisplayProvider {

		/// <summary>
		/// Renders the dynamic display as a stream.
		/// </summary>
		/// <param name="display">The dynamicdisplay.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public abstract Tuple<StreamResult,Padding> RenderDynamicDisplayStream(DynamicDisplay display, int width, int height);


	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IDynamicDisplayProvider : IGenericProviderBase {

		/// <summary>
		/// Renders the dynamic display as a stream.
		/// </summary>
		/// <param name="display">The dynamicdisplay.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		Tuple<StreamResult, Padding> RenderDynamicDisplayStream(DynamicDisplay display, int width, int height);

	}
}

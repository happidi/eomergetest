﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide charting services using custom charting providers.
	/// </summary>
	public abstract class ChartPdfProvider : GenericProviderBase, IChartPdfProvider {
		/// <summary>
		/// The alternate color for the grid
		/// </summary>
		protected Color AlternateGridColor = Color.WhiteSmoke;
		/// <summary>
		/// The default color for the grid
		/// </summary>
		protected Color DefaultGridColor = Color.White;
		/// <summary>
		/// The color for the header of the grid
		/// </summary>
		protected Color GridHeaderColor = Color.ForestGreen;
		/// <summary>
		/// The color for the text font for the grid
		/// </summary>
		protected Color GridHeaderFontColor = Color.White;
		/// <summary>
		/// The font for the text of the header
		/// </summary>
		protected Color PdfHeaderFontColor = Color.Gray;
		/// <summary>
		/// The top color of the header gradient
		/// </summary>
		protected Color PdfHeaderGradientTop = Color.White;
		/// <summary>
		/// The bottom color of the header gradient
		/// </summary>
		protected Color PdfHeaderGradientBottom = Color.YellowGreen;

		/// <summary>
		/// A list of cultures that are RTL
		/// </summary>
		protected List<string> RtlCultures = new List<string> { "he", "ar", "fa" }; //Hebrew, Arabic, Persian

        /// <summary>
		/// Gets the vizelia logo.
		/// </summary>
		/// <returns></returns>
		protected Stream GetVizeliaLogo() {
		    return Helper.GetResourceStream("Vizelia.FOL.Web.resources.images.default.embedded.logo_vizelia_600.png");
		}

		/// <summary>
		/// Renders the chart as an Pdf spreadsheet stream.
		/// </summary>
		/// <param name="charts">The charts data.</param>
		/// <param name="title"></param>
		/// <returns></returns>
		public abstract StreamResult RenderPdfStream(IEnumerable<Tuple<Chart, StreamResult>> charts, string title);

		/// <summary>
		/// Return the PDF Page default size in pixel.
		/// </summary>
		/// <returns></returns>
		public abstract Size GetPageDefaultSize();

        /// <summary>
		/// Renders the PDF images stream.
		/// </summary>
		/// <param name="images">The images.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		public abstract StreamResult RenderPdfImagesStream(List<StreamResult> images, int width, int height, string title);
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IChartPdfProvider : IGenericProviderBase {

		/// <summary>
		/// Renders the chart as an Pdf spreadsheet stream.
		/// </summary>
		/// <param name="charts">The charts data.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		StreamResult RenderPdfStream(IEnumerable<Tuple<Chart, StreamResult>> charts, string title);


		/// <summary>
		/// Return the PDF Page default size in pixel.
		/// </summary>
		/// <returns></returns>
		Size GetPageDefaultSize();

		/// <summary>
		/// Renders the PDF images stream.
		/// </summary>
		/// <param name="images">The images.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		StreamResult RenderPdfImagesStream(List<StreamResult> images, int width, int height, string title);
	}
}

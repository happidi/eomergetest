﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Pdf Chart Service static class.
	/// </summary>
	public static class ChartPdfService {
		private static IChartPdfProvider provider = null;
		private static GenericProviderCollection<ChartPdfProvider, IChartPdfProvider> providers = GenericProviderHelper.LoadProviders<ChartPdfProvider, GenericProviderSection, IChartPdfProvider>("VizeliaChartPdfSection", out provider);

		/// <summary>
		/// Gets a collection of the Pdf chart providers.
		/// </summary>
		public static GenericProviderCollection<ChartPdfProvider, IChartPdfProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Renders the chart as an Pdf spreadsheet stream.
		/// </summary>
		/// <param name="charts">The charts data.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		public static StreamResult RenderPdfStream(IEnumerable<Tuple<Chart, StreamResult>> charts, string title) {
			return provider.RenderPdfStream(charts, title);
		}

		/// <summary>
		/// Return the PDF Page default size in pixel.
		/// </summary>
		/// <returns></returns>
		public static Size GetPageDefaultSize() {
			return provider.GetPageDefaultSize();
		}

		/// <summary>
		/// Renders the PDF images stream.
		/// </summary>
		/// <param name="images">The images.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		public static StreamResult RenderPdfImagesStream(List<StreamResult> images, int width, int height, string title) {
			return provider.RenderPdfImagesStream(images, width, height, title);
		}
	}
}

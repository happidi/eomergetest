﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide charting services using custom charting providers.
	/// </summary>
	public abstract class ChartExcelProvider : GenericProviderBase, IChartExcelProvider {
		/// <summary>
		/// Renders the chart as an excel spreadsheet stream.
		/// </summary>
		/// <param name="charts">The charts data.</param>
		/// <param name="title"></param>
		/// <returns></returns>
		public abstract StreamResult RenderExcelStream(List<Tuple<Chart, StreamResult>> charts, string title);
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IChartExcelProvider : IGenericProviderBase {
		/// <summary>
		/// Renders the chart as an excel spreadsheet stream.
		/// </summary>
		/// <param name="chartsWithImages">The charts with images.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		StreamResult RenderExcelStream(List<Tuple<Chart, StreamResult>> chartsWithImages, string title);

	}
}

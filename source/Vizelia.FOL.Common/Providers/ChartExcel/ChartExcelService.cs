﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;
using System.Collections;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Excel Chart Service static class.
	/// </summary>
	public static class ChartExcelService {
		private static IChartExcelProvider provider = null;
		private static GenericProviderCollection<ChartExcelProvider, IChartExcelProvider> providers = GenericProviderHelper.LoadProviders<ChartExcelProvider, GenericProviderSection, IChartExcelProvider>("VizeliaChartExcelSection", out provider);

		/// <summary>
		/// Gets a collection of the excel chart providers.
		/// </summary>
		public static GenericProviderCollection<ChartExcelProvider, IChartExcelProvider> Providers {
			get {
				return providers;
			}
		}


		/// <summary>
		/// Renders the chart as an excel spreadsheet stream.
		/// </summary>
		/// <param name="chartsWithImages">The charts with images.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		public static StreamResult RenderExcelStream(List<Tuple<Chart, StreamResult>> chartsWithImages,string title) {
			return provider.RenderExcelStream(chartsWithImages,title);
		}

	}

}

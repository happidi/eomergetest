﻿using System.Collections.Generic;
using Vizelia.FOL.Common;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Defines the contract to provide AntiXss validation services using providers.
	/// </summary>
	[ApplyNoPolicies]
	public abstract class AntiXssValidationProvider : GenericProviderBase, IAntiXssValidationProvider {
		/// <summary>
		/// Gets or sets a value indicating whether the provider should perform auto clean.
		/// </summary>
		/// <value>
		///   <c>true</c> if [auto clean]; otherwise, <c>false</c>.
		/// </value>
		public abstract bool AutoClean { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the provider is disabled or not
		/// </summary>
		/// <value>
		///   <c>true</c> if [disable]; otherwise, <c>false</c>.
		/// </value>
		public abstract bool Disable { get; set; }

		/// <summary>
		/// Determines whether the specified body HTML is valid.
		/// </summary>
		/// <param name="bodyHtml">The body HTML.</param>
		/// <returns>
		///   <c>true</c> if the specified body HTML is valid; otherwise, <c>false</c>.
		/// </returns>
		[ApplyNoPolicies]
		public abstract bool IsValid(string bodyHtml);

		/// <summary>
		/// Cleans the specified body HTML.
		/// </summary>
		/// <param name="bodyHtml">The body HTML.</param>
		/// <returns></returns>
		[ApplyNoPolicies]
		public abstract string Clean(string bodyHtml);

		/// <summary>
		/// Gets the additional white list tags.
		/// </summary>
		/// <returns></returns>
		public abstract List<string> GetAdditionalWhiteListTags();
	}

	/// <summary>
	/// AntiXssValidation provider inteface
	/// </summary>
	public interface IAntiXssValidationProvider : IGenericProviderBase {
		/// <summary>
		/// Gets or sets a value indicating whether the provider should perform auto clean.
		/// </summary>
		/// <value>
		///   <c>true</c> if [auto clean]; otherwise, <c>false</c>.
		/// </value>
		[ApplyNoPolicies]
		bool AutoClean { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the provider is disabled or not
		/// </summary>
		/// <value>
		///   <c>true</c> if [disable]; otherwise, <c>false</c>.
		/// </value>
		[ApplyNoPolicies]
		bool Disable { get; set; }

		/// <summary>
		/// Determines whether the specified body HTML is valid.
		/// </summary>
		/// <param name="bodyHtml">The body HTML.</param>
		/// <returns>
		///   <c>true</c> if the specified body HTML is valid; otherwise, <c>false</c>.
		/// </returns>
		[ApplyNoPolicies]
		bool IsValid(string bodyHtml);

		/// <summary>
		/// Cleans the specified body HTML.
		/// </summary>
		/// <param name="bodyHtml">The body HTML.</param>
		/// <returns></returns>
		[ApplyNoPolicies]
		string Clean(string bodyHtml);

		/// <summary>
		/// Gets the additional white list tags.
		/// </summary>
		/// <returns></returns>
		[ApplyNoPolicies]
		List<string> GetAdditionalWhiteListTags();
	}
}

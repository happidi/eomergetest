﻿using System.Collections.Generic;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// AntiXssValidation Provider Class
	/// </summary>
	public static class AntiXssValidationService {
		/// <summary>
		/// the default provider
		/// </summary>
		private static IAntiXssValidationProvider provider = null;
		/// <summary>
		/// list of all providers from config
		/// </summary>
		private static GenericProviderCollection<AntiXssValidationProvider, IAntiXssValidationProvider> providers = GenericProviderHelper.LoadProviders<AntiXssValidationProvider, GenericProviderSection, IAntiXssValidationProvider>("VizeliaAntiXssValidationSection", out provider);

		/// <summary>
		/// Gets the providers.
		/// </summary>
		public static GenericProviderCollection<AntiXssValidationProvider, IAntiXssValidationProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the provider should perform auto clean.
		/// </summary>
		/// <value>
		///   <c>true</c> if [auto clean]; otherwise, <c>false</c>.
		/// </value>
		public static bool AutoClean {
			get { return provider.AutoClean; }
		}

		/// <summary>
		/// Gets a value indicating whether this <see cref="AntiXssValidationService"/> is disable.
		/// </summary>
		/// <value>
		///   <c>true</c> if disable; otherwise, <c>false</c>.
		/// </value>
		public static bool Disable {
			get { return provider.Disable; }
		}


		/// <summary>
		/// Determines whether the specified body HTML is valid.
		/// </summary>
		/// <param name="bodyHtml">The body HTML.</param>
		/// <param name="isSanitized">if set to <c>true</c> [is sanitized].</param>
		/// <param name="invalidTags">The invalid tags.</param>
		/// <param name="sanitizedHtml">The sanitized HTML.</param>
		/// <returns>
		///   <c>true</c> if the specified body HTML is valid; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsValid(string bodyHtml, out bool isSanitized, out List<string> invalidTags, out string sanitizedHtml) {

			// isSanitized refers to the html before the sanitize proccess
			sanitizedHtml = HtmlSanitizerHelper.Sanitize(bodyHtml, out isSanitized, out invalidTags);
			return (isSanitized && provider.IsValid(bodyHtml));
		}

		/// <summary>
		/// Cleans the specified body HTML.
		/// </summary>
		/// <param name="bodyHtml">The body HTML.</param>
		/// <returns></returns>
		public static string Clean(string bodyHtml) {
			return provider.Clean(bodyHtml);
		}

		/// <summary>
		/// Gets the additional white list tags.
		/// </summary>
		/// <returns></returns>
		public static List<string> GetAdditionalWhiteListTags() {
			return provider.GetAdditionalWhiteListTags();
		}
	}
}
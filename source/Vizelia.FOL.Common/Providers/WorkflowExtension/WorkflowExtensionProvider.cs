﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide WorkflowExtension services using providers.
	/// </summary>
	public abstract class WorkflowExtensionProvider : GenericProviderBase, IWorkflowExtensionProvider {


		/// <summary>
		/// Gets the extension.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public abstract T GetExtension<T>();


		/// <summary>
		/// Gets the extension.
		/// </summary>
		/// <param name="extensionType">Type of the extension.</param>
		/// <returns></returns>
		public abstract object GetExtension(Type extensionType);

		/// <summary>
		/// Runs workflow when raising an event on a workflow entity.
		/// </summary>
		/// <param name="entity">The workflow entity.</param>
		/// <param name="eventName">Name of the event.</param>
		/// <returns></returns>
		public abstract FormResponse RaiseEvent(IWorkflowEntity entity, string eventName);

		/// <summary>
		/// Updates the definition of the workflow.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public abstract FormResponse UpdateDefinition(IWorkflowEntity entity);
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	[EnterpriseLibraryNotWrappable]
	public interface IWorkflowExtensionProvider : IGenericProviderBase {

		/// <summary>
		/// Gets the extension.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		T GetExtension<T>();


		/// <summary>
		/// Gets the extension.
		/// </summary>
		/// <param name="extensionType">Type of the extension.</param>
		/// <returns></returns>
		object GetExtension(Type extensionType);

		/// <summary>
		/// Runs workflow when raising an event on a workflow entity.
		/// </summary>
		/// <param name="entity">The workflow entity.</param>
		/// <param name="eventName">Name of the event.</param>
		/// <returns></returns>
		FormResponse RaiseEvent(IWorkflowEntity entity, string eventName);

		/// <summary>
		/// Updates the definition of the workflow.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		FormResponse UpdateDefinition(IWorkflowEntity entity);
	}
}
﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// WorkflowExtension Service static class.
	/// </summary>
	public static class WorkflowExtensionService {
		private static IWorkflowExtensionProvider provider;
		private static GenericProviderCollection<WorkflowExtensionProvider, IWorkflowExtensionProvider> providers = GenericProviderHelper.LoadProviders<WorkflowExtensionProvider, GenericProviderSection, IWorkflowExtensionProvider>("VizeliaWorkflowExtensionSection", out provider);

		/// <summary>
		/// Gets a collection of the WorkflowExtension providers.
		/// </summary>
		public static GenericProviderCollection<WorkflowExtensionProvider, IWorkflowExtensionProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Gets the extension.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static T GetExtension<T>() {
			return provider.GetExtension<T>();
		}

		/// <summary>
		/// Gets the extension.
		/// </summary>
		/// <param name="extensionType">Type of the extension.</param>
		/// <returns></returns>
		public static object GetExtension(Type extensionType) {
			return provider.GetExtension(extensionType);
		}

		/// <summary>
		/// Runs workflow when raising an event on a workflow entity.
		/// </summary>
		/// <param name="entity">The workflow entity.</param>
		/// <param name="eventName">Name of the event.</param>
		/// <returns></returns>
		public static FormResponse RaiseEvent(IWorkflowEntity entity, string eventName) {
			return provider.RaiseEvent(entity, eventName);
		}

		/// <summary>
		/// Updates the definition of the workflow.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public static FormResponse UpdateDefinition(IWorkflowEntity entity) {
			return provider.UpdateDefinition(entity);
		}
	}

}

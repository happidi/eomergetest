﻿using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide charting services using custom charting providers.
	/// </summary>
	public abstract class MeterDataProvider : GenericProviderBase, IMeterDataProvider {
		/// <summary>
		/// Gets the storage.
		/// </summary>
		/// <returns></returns>
		public abstract IMeterDataStorage GetStorage();
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IMeterDataProvider : IGenericProviderBase {
		/// <summary>
		/// Gets the storage.
		/// </summary>
		/// <returns></returns>
		IMeterDataStorage GetStorage();
	}
}

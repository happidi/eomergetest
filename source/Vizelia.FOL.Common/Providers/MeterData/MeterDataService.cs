﻿using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// MeterData Service static class.
	/// </summary>
	public static class MeterDataService {
		private static IMeterDataProvider provider = null;
		private static GenericProviderCollection<MeterDataProvider, IMeterDataProvider> providers = GenericProviderHelper.LoadProviders<MeterDataProvider, GenericProviderSection, IMeterDataProvider>("VizeliaMeterDataSection", out provider);

		/// <summary>
		/// Gets a collection of the MeterData providers.
		/// </summary>
		public static GenericProviderCollection<MeterDataProvider, IMeterDataProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Gets the storage.
		/// </summary>
		/// <returns></returns>
		public static IMeterDataStorage GetStorage() {
			IMeterDataStorage storage = provider.GetStorage();
			return storage;
		}

	}

}

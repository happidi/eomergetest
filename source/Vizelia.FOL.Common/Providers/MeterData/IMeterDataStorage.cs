using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// The meter data storage instance
	/// </summary>
	public interface IMeterDataStorage {
		/// <summary>
		/// Adds the specified key meter.
		/// </summary>
		/// <param name="keyMeter">The key meter.</param>
		/// <param name="values">The values.</param>
		/// <returns></returns>
		bool Add(int keyMeter, IDictionary<DateTime, MD> values);

		/// <summary>
		/// Adds the specified data.
		/// </summary>
		/// <param name="values">The values.</param>
		/// <returns></returns>
		bool AddRange(ConcurrentDictionary<int, SortedList<DateTime, MD>> values);

		/// <summary>
		/// Gets the or add.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="valueFactory">The value factory.</param>
		/// <returns></returns>
		IDictionary<DateTime, MD> GetOrAdd(int key, Func<int, IDictionary<DateTime, MD>> valueFactory);

		/// <summary>
		/// Gets the meter data count.
		/// </summary>
		/// <returns></returns>
		int GetMeterDataCount();

		/// <summary>
		/// Inits this instance.
		/// </summary>
		void Init();

		/// <summary>
		/// Gets or sets the Dictionary DateTime,MD with the specified key.
		/// </summary>
		IDictionary<DateTime, MD> this[int key] {
			get;
			set;
		}

		/// <summary>
		/// Tries the get value.
		/// </summary>
		/// <param name="keyMeter">The key meter.</param>
		/// <param name="value">The value.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="includeStartDate">if set to <c>true</c> [include start date].</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="includeEndDate">if set to <c>true</c> [include end date].</param>
		/// <returns></returns>
		bool TryGetValue(int keyMeter, out IDictionary<DateTime, MD> value, DateTime? startDate = null, bool includeStartDate = true, DateTime? endDate = null, bool includeEndDate = false);


		/// <summary>
		/// Tries the get value.
		/// </summary>
		/// <param name="keyMeter">The key meter.</param>
		/// <param name="value">The value.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="includeStartDate">if set to <c>true</c> [include start date].</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="includeEndDate">if set to <c>true</c> [include end date].</param>
		/// <returns></returns>
		bool TryGetValueAsList(int keyMeter, out List<MD> value, DateTime? startDate, bool includeStartDate, DateTime? endDate, bool includeEndDate);
		
		//NOTE: This is if we want to make the change that accumulated meters will take an additional data point outside the given dates.
		//bool TryGetValueAsList(int keyMeter, out List<MD> value, DateTime? startDate , bool includeStartDate , DateTime? endDate , bool includeEndDate , Meter meter);

	}
}
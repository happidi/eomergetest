﻿using System;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Diagnostics;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide SSO services using custom SSO providers.
	/// </summary>
	[ApplyNoPolicies]
	public abstract class FileProtectionProvider : GenericProviderBase, IFileProtectionProvider {
		private const string const_attribute_extensions = "extensions";
		private const string const_attribute_is_active = "isActive";
		
		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		/// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
		///   
		/// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		///   
		/// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.</exception>
		public override void Initialize(string name, NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_is_active])) {
				throw new ProviderException(const_attribute_is_active + " is missing");
			}
			bool isActive;
			bool.TryParse(config[const_attribute_is_active],out isActive);
			IsActive = isActive;
			config.Remove(const_attribute_is_active);

			if (String.IsNullOrEmpty(config[const_attribute_extensions])) {
				throw new ProviderException(const_attribute_extensions + " is missing");
			}
			Extensions = config[const_attribute_extensions];
			config.Remove(const_attribute_extensions);
		}

		/// <summary>
		/// Gets the extensions.
		/// </summary>
		public string Extensions { get; private set; }


		/// <summary>
		/// True if the provider is activated.
		/// </summary>
		public bool IsActive { get; private set; }



		/// <summary>
		/// Validates a document is safe to process in the system.
		/// </summary>
		/// <param name="document">The document.</param>
		/// <returns>
		/// True if validation succeeded, false otherwise
		/// </returns>
		public abstract  bool Validate(Document document);
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IFileProtectionProvider : IGenericProviderBase {
		/// <summary>
		/// Validates a document is safe to process in the system.
		/// </summary>
		/// <param name="document">The document.</param>
		/// <returns>
		/// True if validation succeeded, false otherwise
		/// </returns>
		[ApplyNoPolicies]
		bool Validate(Document document);

		/// <summary>
		/// Gets the extensions.
		/// </summary>
		string Extensions { get; }


		/// <summary>
		/// True if the provider is activated.
		/// </summary>
		bool IsActive { get; }

	}
}

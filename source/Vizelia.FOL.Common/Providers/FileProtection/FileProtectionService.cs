﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// SSO Service static class.
	/// </summary>
	public static class FileProtectionService {
		private static IFileProtectionProvider provider = null;
		private static GenericProviderCollection<FileProtectionProvider, IFileProtectionProvider> providers = GenericProviderHelper.LoadProviders<FileProtectionProvider, GenericProviderSection, IFileProtectionProvider>("VizeliaFileProtectionSection", out provider);

		/// <summary>
		/// Gets a collection of the SSO providers.
		/// </summary>
		public static GenericProviderCollection<FileProtectionProvider, IFileProtectionProvider> Providers {
			get {
				return providers;
			}
		}


		/// <summary>
		/// Validates a username out of the secret secret.
		/// </summary>
		/// <param name="document">The document.</param>
		/// <returns>
		/// True if validation succeeded, false otherwise
		/// </returns>
		public static bool Validate(Document document) {
			bool isValidated = true;
			foreach (FileProtectionProvider provider in providers) {
				if (provider.IsActive) {
					isValidated = isValidated && provider.Validate(document);
				}
			}
			return isValidated;
		}

	}
}

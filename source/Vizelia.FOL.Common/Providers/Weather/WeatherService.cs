﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Weather Service static class.
	/// </summary>
	public static class WeatherService {
		private static IWeatherProvider provider;
		private static GenericProviderCollection<WeatherProvider, IWeatherProvider> providers = GenericProviderHelper.LoadProviders<WeatherProvider, GenericProviderSection, IWeatherProvider>("VizeliaWeatherSection", out provider);

		/// <summary>
		/// Gets a collection of the Weather providers.
		/// </summary>
		public static GenericProviderCollection<WeatherProvider, IWeatherProvider> Providers {
			get {
				return providers;
			}
		}


		/// <summary>
		/// Returns a list of WeatherLocation based on a search string.
		/// </summary>
		/// <param name="search">The search.</param>
		/// <returns></returns>
		public static List<WeatherLocation> GetWeatherLocation(string search) {
			return provider.GetWeatherLocation(search);
		}

		/// <summary>
		/// Gets the weather five day forecast.
		/// </summary>
		/// <param name="location">The WeatherLocation.</param>
		/// <returns></returns>
		public static List<WeatherForecast> GetWeatherFiveDayForecast(WeatherLocation location) {
			return provider.GetWeatherFiveDayForecast(location);
		}
	}

}

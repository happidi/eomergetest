﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide charting services using custom charting providers.
	/// </summary>
	public abstract class WeatherProvider : GenericProviderBase, IWeatherProvider {

		/// <summary>
		/// Returns a list of WeatherLocation based on a search string.
		/// </summary>
		/// <param name="search">The search.</param>
		/// <returns></returns>
		public abstract List<WeatherLocation> GetWeatherLocation(string search);


		/// <summary>
		/// Gets the weather five day forecast.
		/// </summary>
		/// <param name="location">The WeatherLocation.</param>
		/// <returns></returns>
		public abstract List<WeatherForecast> GetWeatherFiveDayForecast(WeatherLocation location);

	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IWeatherProvider : IGenericProviderBase {

		/// <summary>
		/// Returns a list of WeatherLocation based on a search string.
		/// </summary>
		/// <param name="search">The search.</param>
		/// <returns></returns>
		List<WeatherLocation> GetWeatherLocation(string search);


		/// <summary>
		/// Gets the weather five day forecast.
		/// </summary>
		/// <param name="location">The WeatherLocation.</param>
		/// <returns></returns>
		 List<WeatherForecast> GetWeatherFiveDayForecast(WeatherLocation location);

	}
}

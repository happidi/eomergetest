﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide charting services using custom charting providers.
	/// </summary>
	public abstract class ChartingProvider : GenericProviderBase, IChartingProvider {

		/// <summary>
		/// Renders the chart as a stream.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		public abstract StreamResult RenderChartStream(Chart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding);


		/// <summary>
		/// Renders the chart as an image map.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		public abstract ImageMap RenderChartImageMap(Chart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding);


		/// <summary>
		/// Renders the javascript chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		public abstract StreamResult RenderChartJavascript(Chart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding);

		/// <summary>
		/// Gets the Chart as an html string.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public abstract string GetChartHtml(Chart chart);

		/// <summary>
		/// Gets the list of colors implementented by each Palette available in the provider.
		/// </summary>
		/// <returns></returns>
		public abstract List<Tuple<string, List<Color>>> GetPalettes();


		/// <summary>
		/// Renders a micro chart.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="value">The value.</param>
		/// <param name="targetValue">The target value.</param>
		/// <param name="minValue">The min value.</param>
		/// <param name="color">The color.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public abstract StreamResult RenderMicroChart(string text, double value, double targetValue, double? minValue, Color color, int width, int height);


	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IChartingProvider : IGenericProviderBase {

		/// <summary>
		/// Renders the chart as a stream.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		StreamResult RenderChartStream(Chart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding);

		/// <summary>
		/// Renders the chart as an image map.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		ImageMap RenderChartImageMap(Chart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding);

		/// <summary>
		/// Renders the javascript chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		StreamResult RenderChartJavascript(Chart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding);

		/// <summary>
		/// Gets the Chart as an html string.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		string GetChartHtml(Chart chart);

		/// <summary>
		/// Gets the list of colors implementented by each Palette available in the provider.
		/// </summary>
		/// <returns></returns>
		List<Tuple<string, List<Color>>> GetPalettes();


		/// <summary>
		/// Renders a micro chart.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="value">The value.</param>
		/// <param name="targetValue">The target value.</param>
		/// <param name="minValue">The min value.</param>
		/// <param name="color">The color.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		StreamResult RenderMicroChart(string text, double value, double targetValue, double? minValue, Color color, int width, int height);

	}
}

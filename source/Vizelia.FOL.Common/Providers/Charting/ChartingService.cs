﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Charting Service static class.
	/// </summary>
	public static class ChartingService {
		private static IChartingProvider provider = null;
		private static GenericProviderCollection<ChartingProvider, IChartingProvider> providers = GenericProviderHelper.LoadProviders<ChartingProvider, GenericProviderSection, IChartingProvider>("VizeliaChartingSection", out provider);

		/// <summary>
		/// Gets a collection of the charting providers.
		/// </summary>
		public static GenericProviderCollection<ChartingProvider, IChartingProvider> Providers {
			get {
				return providers;
			}
		}


		/// <summary>
		/// Renders the chart as a stream.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		public static StreamResult RenderChartStream(Chart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding) {
			return provider.RenderChartStream(chart, width, height, dynamicdisplayStream, chartPadding);
		}

		/// <summary>
		/// Renders the chart as an image map.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		public static ImageMap RenderChartImageMap(Chart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding) {
			return provider.RenderChartImageMap(chart, width, height, dynamicdisplayStream, chartPadding);
		}

		/// <summary>
		/// Renders the chart as a javascript stream.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		public static StreamResult RenderChartJavascript(Chart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding) {
			return provider.RenderChartJavascript(chart, width, height, dynamicdisplayStream, chartPadding);
		}

		/// <summary>
		/// Gets the Chart as an html string.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <returns></returns>
		public static string GetChartHtml(Chart chart) {
			return provider.GetChartHtml(chart);
		}

		/// <summary>
		/// Gets the list of colors implementented by each Palette available in the provider.
		/// </summary>
		/// <returns></returns>
		public static List<Tuple<string, List<Color>>> GetPalettes() {
			return provider.GetPalettes();
		}


		/// <summary>
		/// Renders a micro chart.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="value">The value.</param>
		/// <param name="targetValue">The target value.</param>
		/// <param name="minValue">The min value.</param>
		/// <param name="color">The color.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public static StreamResult RenderMicroChart(string text, double value, double targetValue, double? minValue, Color color, int width, int height) {
			return provider.RenderMicroChart(text, value, targetValue, minValue, color, width, height);
		}



	}

}

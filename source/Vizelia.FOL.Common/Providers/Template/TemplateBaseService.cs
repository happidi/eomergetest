﻿using System;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

    /// <summary>
    /// Template Service static class.
    /// </summary>
    public static class TemplateBaseService {
        private static ITemplateBaseProvider provider = null;
        private static GenericProviderCollection<TemplateBaseProvider, ITemplateBaseProvider> providers = GenericProviderHelper.LoadProviders<TemplateBaseProvider, GenericProviderSection, ITemplateBaseProvider>("VizeliaTemplateBaseSection", out provider);

        /// <summary>
        /// Gets a collection of the template providers.
        /// </summary>
        public static GenericProviderCollection<TemplateBaseProvider, ITemplateBaseProvider> Providers {
            get {
                return providers;
            }
        }



        /// <summary>
        /// Parses the specified template.
        /// </summary>
        /// <param name="templateBaseName">Name of the template base.</param>
        /// <returns></returns>
        public static Type GetTemplateBaseType(string templateBaseName) {
            Type retVal = null;
            foreach (TemplateBaseProvider templateBaseProvider in providers) {
                if (templateBaseProvider.Name.Equals(templateBaseName)) {
                    retVal = templateBaseProvider.GetTemplateBaseType();
                    break;
                }
            }
            return retVal;
        }

    }

}

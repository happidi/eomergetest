﻿using System.Collections.Generic;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Template Service static class.
	/// </summary>
	public static class TemplateService {
		private static ITemplateProvider provider = null;
		private static GenericProviderCollection<TemplateProvider, ITemplateProvider> providers = GenericProviderHelper.LoadProviders<TemplateProvider, GenericProviderSection, ITemplateProvider>("VizeliaTemplateSection", out provider);

		/// <summary>
		/// Gets a collection of the template providers.
		/// </summary>
		public static GenericProviderCollection<TemplateProvider, ITemplateProvider> Providers {
			get {
				return providers;
			}
		}



		/// <summary>
		/// Parses the specified template.
		/// </summary>
		/// <param name="template">The template.</param>
		/// <param name="data">The model.</param>
		/// <param name="baseTemplateName">The base template name.</param>
		/// <returns></returns>
		public static string Parse<T>(string template, T data, string baseTemplateName = null) where T : class {
			return provider.Parse(template, data, baseTemplateName);
		}

		/// <summary>
		/// Parses the specified template.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="template">The template.</param>
		/// <param name="data">The model.</param>
		/// <param name="templateBaseName">Name of the template base.</param>
		/// <returns></returns>
		public static List<string> ParseMany<T>(IEnumerable<string> template, T data, string templateBaseName = null) where T : class {
			return provider.ParseMany(template, data, templateBaseName);
		}
	}
}

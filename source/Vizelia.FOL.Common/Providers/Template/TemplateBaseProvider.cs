﻿using System;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
    /// <summary>
    /// Defines the contract to provide template services using custom template providers.
    /// </summary>
    public class TemplateBaseProvider : GenericProviderBase, ITemplateBaseProvider {

        /// <summary>
        /// Parses the template.
        /// </summary>
        /// <returns>
        /// The result string.
        /// </returns>
        public virtual Type GetTemplateBaseType() {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Defines the public interface for the provider.
    /// </summary>
    public interface ITemplateBaseProvider : IGenericProviderBase {

        /// <summary>
        /// Parses the template.
        /// </summary>
        /// <returns>
        /// The result string.
        /// </returns>
        Type GetTemplateBaseType();

    }
}

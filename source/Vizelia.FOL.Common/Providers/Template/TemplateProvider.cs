﻿using System.Collections.Generic;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide template services using custom template providers.
	/// </summary>
	public abstract class TemplateProvider : GenericProviderBase, ITemplateProvider {

		/// <summary>
		/// Parse template relying provided data and template base type.
		/// </summary>
		/// <param name="template">The template.</param>
		/// <param name="data">The data model.</param>
		/// <param name="baseTemplateName">The base template name.</param>
		/// <returns>
		/// The result string.
		/// </returns>
		public abstract string Parse<T>(string template, T data, string baseTemplateName) where T : class;

		/// <summary>
		/// Parse list of templates.
		/// </summary>
		/// <param name="template">The template.</param>
		/// <param name="data">The data model.</param>
		/// <param name="baseTemplateName">The base template name.</param>
		/// <returns>
		/// The result string.
		/// </returns>
		public abstract List<string> ParseMany<T>(IEnumerable<string> template, T data, string baseTemplateName) where T : class;
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface ITemplateProvider : IGenericProviderBase {

		/// <summary>
		/// Parse template relying provided data and template base type.
		/// </summary>
		/// <param name="template">The template.</param>
		/// <param name="data">The data model.</param>
		/// <param name="baseTemplateName">The base template name.</param>
		/// <returns>
		/// The result string.
		/// </returns>
		string Parse<T>(string template, T data, string baseTemplateName) where T : class;

		/// <summary>
		/// Parse list of templates.
		/// </summary>
		/// <param name="template">The template.</param>
		/// <param name="data">The data model.</param>
		/// <param name="baseTemplateName">The base template name.</param>
		/// <returns>
		/// The result string.
		/// </returns>
		List<string> ParseMany<T>(IEnumerable<string> template, T data, string baseTemplateName) where T : class;
	}
}

﻿using System.Collections.Generic;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide TenantProvisioning services using providers.
	/// </summary>
	public abstract class TenantProvisioningProvider : GenericProviderBase, ITenantProvisioningProvider {
		/// <summary>
		/// Gets a list for the business entity Tenant.
		/// </summary>
		/// <returns></returns>
		public abstract List<Tenant> GetTenants();

		/// <summary>
		/// Gets a specific item for the business entity Tenant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public abstract Tenant GetTenant(string Key);

		/// <summary>
		/// Creates a new business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public abstract Tenant CreateTenant(Tenant item);

		/// <summary>
		/// Updates an existing business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public abstract Tenant UpdateTenant(Tenant item);

		/// <summary>
		/// Deletes an existing business entity Tenant.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public abstract bool DeleteTenant(Tenant item);

		/// <summary>
		/// Gets the Multitenant hosting mode.
		/// </summary>
		public abstract MultitenantHostingMode Mode { get; }
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface ITenantProvisioningProvider : IGenericProviderBase {

		/// <summary>
		/// Gets a list for the business entity Tenant. 
		/// </summary>
		/// <returns></returns>
		List<Tenant> GetTenants();

		/// <summary>
		/// Gets a specific item for the business entity Tenant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		Tenant GetTenant(string Key);

		/// <summary>
		/// Creates a new business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		Tenant CreateTenant(Tenant item);

		/// <summary>
		/// Updates an existing business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		Tenant UpdateTenant(Tenant item);


		/// <summary>
		/// Deletes an existing business entity Tenant.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool DeleteTenant(Tenant item);

		/// <summary>
		/// Gets the Multitenant hosting mode.
		/// </summary>
		MultitenantHostingMode Mode { get; }

	}
}
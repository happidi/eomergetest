﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;
using System.Collections;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// TenantProvisioning Service static class.
	/// </summary>
	public static class TenantProvisioningService {
		private static ITenantProvisioningProvider provider = null;
		private static GenericProviderCollection<TenantProvisioningProvider, ITenantProvisioningProvider> providers = GenericProviderHelper.LoadProviders<TenantProvisioningProvider, GenericProviderSection, ITenantProvisioningProvider>("VizeliaTenantProvisioningSection", out provider);

		/// <summary>
		/// Gets a collection of the TenantProvisioning providers.
		/// </summary>
		public static GenericProviderCollection<TenantProvisioningProvider, ITenantProvisioningProvider> Providers {
			get {
				return providers;
			}
		}


		/// <summary>
		/// Gets a list for the business entity Tenant.
		/// </summary>
		/// <returns></returns>
		public static List<Tenant> GetTenants() {
			return provider.GetTenants();
		}

		/// <summary>
		/// Gets a specific item for the business entity Tenant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public static Tenant GetTenant(string Key) {
			return provider.GetTenant(Key);
		}

		/// <summary>
		/// Creates a new business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public static Tenant CreateTenant(Tenant item) {
			return provider.CreateTenant(item);
		}

		/// <summary>
		/// Updates an existing business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public static Tenant UpdateTenant(Tenant item) {
			return provider.UpdateTenant(item);
		}

		/// <summary>
		/// Deletes an existing business entity Tenant.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public static bool DeleteTenant(Tenant item) {
			Tenant tenantWithAllProperties = GetTenant(item.KeyTenant);
			return provider.DeleteTenant(tenantWithAllProperties);
		}


		/// <summary>
		/// Gets the Multitenant hosting mode.
		/// </summary>
		public static MultitenantHostingMode Mode {
			get { return provider.Mode; }
		}
	}
}
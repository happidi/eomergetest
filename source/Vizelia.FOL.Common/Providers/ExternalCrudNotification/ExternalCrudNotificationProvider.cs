﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration.Provider;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide ExternalCrudNotification services using providers.
	/// </summary>
	public abstract class ExternalCrudNotificationProvider : GenericProviderBase, IExternalCrudNotificationProvider
	{

	    private const string const_attribute_is_asynchronous = "async";

        /// <summary>
        /// Initializes the provider.
        /// </summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
        /// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
        ///   
        /// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
        ///   
        /// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.</exception>
        public override void Initialize(string name, NameValueCollection config) {
            base.Initialize(name, config);

            if (String.IsNullOrEmpty(config[const_attribute_is_asynchronous]))
            {
                throw new ProviderException(const_attribute_is_asynchronous + " is missing");
            }

            IsAsynchronous = bool.Parse(config[const_attribute_is_asynchronous]);

            config.Remove(const_attribute_is_asynchronous);

            if (config.Count > 0)
                throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
        }

		/// <summary>
		/// Determines whether the CRUD notification provider is ready to receive notifications.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the CRUD notification provider is ready to receive notifications; otherwise, <c>false</c>.
		/// </returns>
		public abstract bool IsReadyForNotifications();

		/// <summary>
		/// Send a notification to an external crud notification service.
		/// </summary>
		/// <param name="notifications">a list of entity and the crud operation that was performed</param>
		public abstract void SendNotification(List<CrudInformation> notifications);

        /// <summary>
        /// Flushes the waiting notifications.
        /// </summary>
	    public abstract void FlushWaitingNotifications();

        /// <summary>
        /// Gets or sets a value indicating whether this instance is asynchronous.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is asynchronous; otherwise, <c>false</c>.
        /// </value>
        public bool IsAsynchronous { get; set; }

	}

    /// <summary>
    /// Defines the public interface for the provider.
    /// </summary>
	public interface IExternalCrudNotificationProvider : IGenericProviderBase {

    	/// <summary>
    	/// Determines whether the CRUD notification provider is ready to receive notifications.
    	/// </summary>
    	/// <returns>
    	///   <c>true</c> if the CRUD notification provider is ready to receive notifications; otherwise, <c>false</c>.
    	/// </returns>
    	bool IsReadyForNotifications();

		/// <summary>
		/// Send a notification to an external crud notification service.
		/// </summary>
		/// <param name="notifications">a list of entity and the crud operation that was performed</param>
		void SendNotification(List<CrudInformation> notifications);

        /// <summary>
        /// Flushes the waiting notifications (more relevant in async provider implementation)
        /// </summary>
        void FlushWaitingNotifications();

        /// <summary>
        /// Gets or sets a value indicating whether this instance is asynchronous.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is asynchronous; otherwise, <c>false</c>.
        /// </value>
        bool IsAsynchronous { get; set; }
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// External Crud Notification Service static class.
	/// </summary>
	public static class ExternalCrudNotificationService {

		private const string const_is_asynchronous_mode_cachekey = "asyncronousmodeflag";

		private static IExternalCrudNotificationProvider provider;

		private static GenericProviderCollection<ExternalCrudNotificationProvider, IExternalCrudNotificationProvider>
			providers = GenericProviderHelper.LoadProviders<ExternalCrudNotificationProvider, GenericProviderSection, IExternalCrudNotificationProvider>("VizeliaExternalCrudNotificationSection", out provider);


		/// <summary>
		/// Gets a collection of the ExternalCrudNotification providers.
		/// </summary>
		public static GenericProviderCollection<ExternalCrudNotificationProvider, IExternalCrudNotificationProvider>
			Providers {
			get { return providers; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is asynchronous mode.
		/// Can be only set once per request.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is asynchronous mode; otherwise, <c>false</c>.
		/// </value>
		public static bool IsAsynchronousMode {
			get {
				object isAsyncObject = ContextHelper.Get(const_is_asynchronous_mode_cachekey);

				bool isAsync = isAsyncObject != null && (bool)isAsyncObject;
				return isAsync;
			}
			set {
				if (value == IsAsynchronousMode) return;

				// defensive. make sure this property is only set once per request
				if (ContextHelper.Get(const_is_asynchronous_mode_cachekey) != null)
					throw new VizeliaException("IsAsynchronousMode can only be set once per request");

				ContextHelper.Add(const_is_asynchronous_mode_cachekey, value);
			}
		}

		/// <summary>
		/// Send a notification to an external crud notification service.
		/// </summary>
		/// <param name="notifications">The notifications.</param>
		public static void SendNotification(List<CrudInformation> notifications) {
			if (notifications.Count == 0) return;

			IEnumerable<IExternalCrudNotificationProvider> providersForAsyncMode = GetProvidersForCurrentAsyncMode();

			foreach (var currentProvider in providersForAsyncMode) {
				currentProvider.SendNotification(notifications);
			}
		}

		/// <summary>
		/// Gets the providers for current async mode.
		/// </summary>
		/// <returns></returns>
		private static IEnumerable<IExternalCrudNotificationProvider> GetProvidersForCurrentAsyncMode() {
			IEnumerable<IExternalCrudNotificationProvider> providersForAsyncMode =
				providers.Cast<IExternalCrudNotificationProvider>().Where(p => p.IsAsynchronous == IsAsynchronousMode);

			return providersForAsyncMode;
		}

		/// <summary>
		/// Sends the notification.
		/// </summary>
		/// <param name="Entity">The entity.</param>
		/// <param name="crudAction">The crud action.</param>
		public static void SendNotification(BaseBusinessEntity Entity, string crudAction) {
			SendNotification(new List<CrudInformation> { new CrudInformation { CrudAction = crudAction, Entity = Entity } });
		}

		/// <summary>
		/// Flushes the waiting notifications.
		/// </summary>
		public static void FlushWaitingNotifications() {
			IEnumerable<IExternalCrudNotificationProvider> providersForAsyncMode = GetProvidersForCurrentAsyncMode();

			foreach (var currentProvider in providersForAsyncMode) {
				currentProvider.FlushWaitingNotifications();
			}
		}

		/// <summary>
		/// Determines whether the CRUD notification provider is ready to receive notifications.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the CRUD notification provider is ready to receive notifications; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsReadyForNotifications() {
			IEnumerable<IExternalCrudNotificationProvider> providersForAsyncMode = GetProvidersForCurrentAsyncMode();
			bool isReady = providersForAsyncMode.All(p => p.IsReadyForNotifications());

			return isReady;
		}

		/// <summary>
		/// Throws if the external crud notification service is not ready.
		/// </summary>
		public static void ThrowOnNotReady() {
			if (!IsReadyForNotifications()) {
				throw new VizeliaException(Langue.error_chart_energyaggregatorloadingmeterdata);
			}
		}

	}
}
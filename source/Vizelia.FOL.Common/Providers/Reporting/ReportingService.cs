﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.Common;
using System.Collections;
using System.IO;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Reporting Service static class.
	/// </summary>
	public static class ReportingService {
		private static IReportingProvider provider;
		private static GenericProviderCollection<ReportingProvider, IReportingProvider> providers = GenericProviderHelper.LoadProviders<ReportingProvider, GenericProviderSection, IReportingProvider>("VizeliaReportingSection", out provider);

		/// <summary>
		/// Gets a collection of the reporting providers.
		/// </summary>
		public static GenericProviderCollection<ReportingProvider, IReportingProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Renders the report as a stream.
		/// </summary>
		/// <param name="data">The report data.</param>
		/// <param name="columns">The columns of the data that should be exposed in the report.</param>
		/// <param name="title">The report's title.</param>
		/// <param name="exportType">The export type of the report.</param>
		/// <param name="pageFormat">The page format of the report.</param>
		/// <param name="mimeType">The mime type.</param>
		/// <param name="extension">The resulting extension of the file which content is returned as a stream.</param>
		/// <returns>The report content rendered as stream.</returns>
		public static MemoryStream RenderReport(IList data, IList<Column> columns, string title, ExportType exportType, FormatPageType pageFormat, out string mimeType, out string extension) {
			return provider.RenderReport(data, columns, title, exportType, pageFormat, out mimeType, out extension);

		}

		/// <summary>
		/// Renders the report RDLC.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="dataSources">The dictionary of data sources. Each entry in the dictionary should be a List of business entities.</param>
		/// <param name="reportParameters">The dictionary of report parameters. Each entry in the dictionary should be a string.</param>
		/// <param name="exportType">Type of the export.</param>
		/// <param name="pageFormat">The page format.</param>
		/// <param name="mimeType">Type of the MIME.</param>
		/// <param name="extension">The extension.</param>
		/// <returns></returns>
		public static MemoryStream RenderReportRdlc(string reportPath, Dictionary<string, object> dataSources, Dictionary<string, object> reportParameters, ExportType exportType, FormatPageType pageFormat, out string mimeType, out string extension) {
			return provider.RenderReportRdlc(reportPath, dataSources, reportParameters, exportType, pageFormat, out mimeType, out extension);
		}


		/// <summary>
		/// Returns the List child Report's tree node.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public static List<TreeNode> GetReportHierarchy(string key) {
			List<TreeNode> reportHierarchy = provider.GetReportHierarchy(key);
			return reportHierarchy;
		}

		/// <summary>
		/// Gets the report parameters.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <returns></returns>
		public static List<ReportParameter> GetReportParameters(string reportPath) {
			List<ReportParameter> reportParameters = provider.GetReportParameters(reportPath);
			return reportParameters;
		}

		/// <summary>
		/// Gets the report parameter values.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <returns></returns>
		public static JsonStore<ReportParameterValue> GetReportParameterValues(string key, string parameterName) {
			JsonStore<ReportParameterValue> values = provider.GetReportParameterValues(key, parameterName);
			return values;
		}

		/// <summary>
		/// Returns the URL of the report server.
		/// </summary>
		/// <returns>
		/// A Uri object containing the URL of the report server.
		/// </returns>
		public static Uri ReportServerUrl {
			get {
				Uri uri = provider.ReportServerUrl;
				return uri;
			}
		}

		/// <summary>
		/// Gets the number of milliseconds to wait for server communications.
		/// </summary>
		/// <returns>
		/// An integer value containing the number of milliseconds to wait for server communications.
		/// </returns>
		public static int Timeout {
			get {
				int timeout = provider.Timeout;
				return timeout;
			}
		}

		/// <summary>
		/// Creates the folder.
		/// </summary>
		/// <param name="path">The path.</param>
		public static void CreateFolder(string path) {
			provider.CreateFolder(path);
		}

		/// <summary>
		/// Deletes the folder. All content will be deleted as well.
		/// </summary>
		/// <param name="path">The path.</param>
		public static void DeleteFolder(string path) {
			provider.DeleteFolder(path);
		}

		/// <summary>
		/// Deletes the item.
		/// </summary>
		/// <param name="path">The path.</param>
		public static void DeleteItem(string path) {
			provider.DeleteItem(path);
		}

		/// <summary>
		/// Renames or moves the item.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="newPath">The new path.</param>
		public static void MoveItem(string path, string newPath) {
			provider.MoveItem(path, newPath);
		}

		/// <summary>
		/// Creates the subscription.
		/// </summary>
		/// <param name="subscription">The subscription.</param>
		public static void CreateSubscription(ReportSubscription subscription) {
			provider.CreateSubscription(subscription);
		}

		/// <summary>
		/// Gets the subscriptions.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="forKeyActor">For key actor.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="totalCount">The total count.</param>
		/// <returns></returns>
		public static List<ReportSubscription> GetSubscriptions(string reportPath, string forKeyActor, PagingParameter paging, out int totalCount) {
			var subscriptions = provider.GetSubscriptions(reportPath, forKeyActor, paging, out totalCount);
			return subscriptions;
		}

		/// <summary>
		/// Deletes the subscription.
		/// </summary>
		/// <param name="keyReportSubscription">The key report subscription.</param>
		public static void DeleteSubscription(string keyReportSubscription) {
			provider.DeleteSubscription(keyReportSubscription);
		}
	}

}

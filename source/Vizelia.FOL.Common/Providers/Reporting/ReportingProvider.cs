﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide reporting services using custom reporting providers.
	/// </summary>
	public abstract class ReportingProvider : GenericProviderBase, IReportingProvider {

		/// <summary>
		/// Renders the report as a stream.
		/// </summary>
		/// <param name="data">The report data.</param>
		/// <param name="columns">The columns of the data passed to the report.</param>
		/// <param name="title">The report's title.</param>
		/// <param name="exportType">The export type of the report.</param>
		/// <param name="pageFormat">The page format of the report.</param>
		/// <param name="mimeType">The resulting mime type.</param>
		/// <param name="extension">The resulting extension of the file which content is returned as a stream.</param>
		/// <returns></returns>
		public abstract MemoryStream RenderReport(IList data, IList<Column> columns, string title, ExportType exportType, FormatPageType pageFormat, out string mimeType, out string extension);


		/// <summary>
		/// Renders the report RDLC.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="dataSources">The dictionary of data sources. Each entry in the dictionary should be a List of business entities.</param>
		/// <param name="reportParameters">The dictionary of report parameters. Each entry in the dictionary should be a string.</param>
		/// <param name="exportType">Type of the export.</param>
		/// <param name="pageFormat">The page format.</param>
		/// <param name="mimeType">Type of the MIME.</param>
		/// <param name="extension">The extension.</param>
		/// <returns></returns>
		public abstract MemoryStream RenderReportRdlc(string reportPath, Dictionary<string, object> dataSources, Dictionary<string, object> reportParameters, ExportType exportType, FormatPageType pageFormat, out string mimeType, out string extension);


		/// <summary>
		/// Returns the List child Report's tree node.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public abstract List<TreeNode> GetReportHierarchy(string key);

		/// <summary>
		/// Gets the report parameters.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <returns></returns>
		public abstract List<ReportParameter> GetReportParameters(string reportPath);

		/// <summary>
		/// Gets the report parameter values.
		/// </summary>
		/// <param name="reportPath"></param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <returns></returns>
		public abstract JsonStore<ReportParameterValue> GetReportParameterValues(string reportPath, string parameterName);

		/// <summary>
		/// Returns the URL of the report server.
		/// </summary>
		/// <returns>
		/// A Uri object containing the URL of the report server.
		/// </returns>
		public abstract Uri ReportServerUrl { get; }

		/// <summary>
		/// Gets the number of milliseconds to wait for server communications.
		/// </summary>
		/// <returns>
		/// An integer value containing the number of milliseconds to wait for server communications.
		/// </returns>
		public abstract int Timeout { get; }

		/// <summary>
		/// Creates the folder.
		/// </summary>
		/// <param name="path">The path.</param>
		public abstract void CreateFolder(string path);

		/// <summary>
		/// Deletes the item.
		/// </summary>
		/// <param name="path">The path.</param>
		public abstract void DeleteItem(string path);

		/// <summary>
		/// Renames or moves the item.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="newPath">The new path.</param>
		public abstract void MoveItem(string path, string newPath);

		/// <summary>
		/// Deletes the folder. All content will be deleted as well.
		/// </summary>
		/// <param name="path">The path.</param>
		public abstract void DeleteFolder(string path);

		/// <summary>
		/// Creates the subscription.
		/// </summary>
		/// <param name="subscription">The subscription.</param>
		public abstract void CreateSubscription(ReportSubscription subscription);

		/// <summary>
		/// Gets the subscriptions.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="forKeyActor">For key actor.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="totalCount">The total count.</param>
		/// <returns></returns>
		public abstract List<ReportSubscription> GetSubscriptions(string reportPath, string forKeyActor, PagingParameter paging, out int totalCount);

		/// <summary>
		/// Deletes the subscription.
		/// </summary>
		/// <param name="keyReportSubscription">The key report subscription.</param>
		public abstract void DeleteSubscription(string keyReportSubscription);
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IReportingProvider : IGenericProviderBase {
		/// <summary>
		/// Renders the report as a stream.
		/// </summary>
		/// <param name="data">The report data.</param>
		/// <param name="columns">The columns of the data passed to the report.</param>
		/// <param name="title">The report's title.</param>
		/// <param name="exportType">The export type of the report.</param>
		/// <param name="pageFormat">The page format of the report.</param>
		/// <param name="mimeType">The resulting mime type.</param>
		/// <param name="extension">The resulting extension of the file which content is returned as a stream.</param>
		/// <returns>The report rendered as a stream.</returns>
		MemoryStream RenderReport(IList data, IList<Column> columns, string title, ExportType exportType, FormatPageType pageFormat, out string mimeType, out string extension);

		/// <summary>
		/// Renders the report RDLC.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="dataSources">The dictionary of data sources. Each entry in the dictionary should be a List of business entities.</param>
		/// <param name="reportParameters">The dictionary of report parameters. Each entry in the dictionary should be a string.</param>
		/// <param name="exportType">Type of the export.</param>
		/// <param name="pageFormat">The page format.</param>
		/// <param name="mimeType">Type of the MIME.</param>
		/// <param name="extension">The extension.</param>
		/// <returns></returns>
		MemoryStream RenderReportRdlc(string reportPath, Dictionary<string, object> dataSources, Dictionary<string, object> reportParameters, ExportType exportType, FormatPageType pageFormat, out string mimeType, out string extension);


		/// <summary>
		/// Returns the List child Report's tree node.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		List<TreeNode> GetReportHierarchy(string key);

		/// <summary>
		/// Gets the report parameters.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <returns></returns>
		List<ReportParameter> GetReportParameters(string reportPath);

		/// <summary>
		/// Gets the report parameter values.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <returns></returns>
		JsonStore<ReportParameterValue> GetReportParameterValues(string reportPath, string parameterName);

		/// <summary>
		/// Returns the URL of the report server.
		/// </summary>
		/// <returns>
		/// A Uri object containing the URL of the report server.
		/// </returns>
		Uri ReportServerUrl { get; }

		/// <summary>
		/// Gets the number of milliseconds to wait for server communications.
		/// </summary>
		/// <returns>
		/// An integer value containing the number of milliseconds to wait for server communications.
		/// </returns>
		int Timeout { get; }

		/// <summary>
		/// Creates the folder.
		/// </summary>
		/// <param name="path">The path.</param>
		void CreateFolder(string path);

		/// <summary>
		/// Deletes the item.
		/// </summary>
		/// <param name="path">The path.</param>
		void DeleteItem(string path);

		/// <summary>
		/// Renames or moves the item.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="newPath">The new path.</param>
		void MoveItem(string path, string newPath);

		/// <summary>
		/// Deletes the folder. All content will be deleted as well.
		/// </summary>
		/// <param name="path">The path.</param>
		void DeleteFolder(string path);

		/// <summary>
		/// Creates the subscription.
		/// </summary>
		/// <param name="subscription">The subscription.</param>
		void CreateSubscription(ReportSubscription subscription);

		/// <summary>
		/// Gets the subscriptions.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="forKeyActor">For key actor.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="totalCount">The total count.</param>
		/// <returns></returns>
		List<ReportSubscription> GetSubscriptions(string reportPath, string forKeyActor, PagingParameter paging, out int totalCount);

		/// <summary>
		/// Deletes the subscription.
		/// </summary>
		/// <param name="keyReportSubscription">The key report subscription.</param>
		void DeleteSubscription(string keyReportSubscription);
	}
}

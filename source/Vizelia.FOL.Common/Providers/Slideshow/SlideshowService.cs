﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using Vizelia.FOL.Common;
using System.Collections;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Provide screenshot services
	/// </summary>
	public static class SlideshowService {
		private static ISlideshowProvider provider = null;
		private static GenericProviderCollection<SlideshowProvider, ISlideshowProvider> providers;


		static SlideshowService() {
			providers = GenericProviderHelper.LoadProviders<SlideshowProvider, GenericProviderSection, ISlideshowProvider>("VizeliaSlideshowSection", out provider);
		}

		/// <summary>
		/// Gets a collection of the Slideshow providers.
		/// </summary>
		public static GenericProviderCollection<SlideshowProvider, ISlideshowProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Get a string result which contains the Slideshow code for the list of pages provided.
		/// </summary>
		/// <param name="pages">The pages.</param>
		/// <param name="keyCulture">The key culture.</param>
		/// <returns></returns>
		public static string GetSlideshow(List<SlideshowPage> pages, string keyCulture) {
			var retVal = provider.GetSlideshow(pages, keyCulture);
			return retVal;
		}


	}

}
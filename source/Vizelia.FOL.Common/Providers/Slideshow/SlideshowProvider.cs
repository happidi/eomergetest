﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Implement to create a custom Slideshow provider
	/// 
	/// </summary>
	public abstract class SlideshowProvider : GenericProviderBase, ISlideshowProvider {

		/// <summary>
		/// Get a string result which contains the Slideshow code for the list of pages provided.
		/// </summary>
		/// <param name="pages">The pages.</param>
		/// <param name="keyCulture">The key culture.</param>
		/// <returns></returns>
		public abstract string GetSlideshow(List<SlideshowPage> pages, string keyCulture);

		/// <summary>
		/// Gets the localized title if it is surrounded by %.
		/// </summary>
		/// <param name="keyCulture">The key culture.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		public string GetLocalizedTitle(string keyCulture, string title) {
			string result = title;

			try {
				result = (title.StartsWith("%") && title.EndsWith("%"))
				         	? Langue.ResourceManager.GetString(title.Trim('%'), new CultureInfo(keyCulture))
				         	: title;
			}
			catch {

			}

			return result;
		}
	}
	/// <summary>
	/// Interface to implement to create a custom Slideshow provider
	/// </summary>
	public interface ISlideshowProvider : IGenericProviderBase {

		/// <summary>
		/// Get a string result which contains the Slideshow code for the list of pages provided.
		/// </summary>
		/// <param name="pages">The pages.</param>
		/// <param name="keyCulture">The key culture.</param>
		/// <returns></returns>
		string GetSlideshow(List<SlideshowPage> pages, string keyCulture);
	}


}
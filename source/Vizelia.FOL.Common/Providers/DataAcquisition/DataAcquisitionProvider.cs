﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide DataAcquisitionProvider services using providers.
	/// </summary>
	public abstract class DataAcquisitionProvider : GenericProviderBase, IDataAcquisitionProvider {

		/// <summary>
		/// Get the current value in the endpoint.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">the endpoint.</param>
		/// <returns></returns>
		public abstract List<DataAcquisitionEndpointValue> GetCurrentValue(DataAcquisitionContainer container,DataAcquisitionEndpoint endpoint);

		/// <summary>
		/// Gets the hierarchy from the container.
		/// e.g. get all the meters that are connected to a specific box. or - get all the geo locations a given webservice supports
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="parentItem">The parent item.</param>
		/// <returns></returns>
		public abstract List<TreeNode> GetHierarchy(DataAcquisitionContainer container, DataAcquisitionItem parentItem = null);

		/// <summary>
		/// Perform any other changes to the object such as property manipulation or additions that come from the various DataAcquisition Providers
		/// e.g. Return additional data that came from a web service call in addition to data stored in the data layer.
		/// </summary>
		/// <param name="container">The container</param>
		/// <returns></returns>
		public abstract DataAcquisitionContainer FillObject(DataAcquisitionContainer container);

	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IDataAcquisitionProvider : IGenericProviderBase
	{

		/// <summary>
		/// Get the current value in the endpoint.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">the endpoint.</param>
		/// <returns></returns>
		List<DataAcquisitionEndpointValue> GetCurrentValue(DataAcquisitionContainer container, DataAcquisitionEndpoint endpoint);

		/// <summary>
		/// Gets the hierarchy from the container.
		/// e.g. get all the meters that are connected to a specific box. or - get all the geo locations a given webservice supports
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="parentItem">The parent item.</param>
		/// <returns></returns>
		List<TreeNode> GetHierarchy(DataAcquisitionContainer container, DataAcquisitionItem parentItem = null);

		/// <summary>
		/// Perform any other changes to the object such as property manipulation or additions that come from the various DataAcquisition Providers
		/// e.g. Return additional data that came from a web service call in addition to data stored in the data layer.
		/// </summary>
		/// <param name="container">The container</param>
		/// <returns></returns>
		DataAcquisitionContainer FillObject(DataAcquisitionContainer container);


	}
}
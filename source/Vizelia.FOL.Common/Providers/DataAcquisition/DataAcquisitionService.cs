﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// DataAcquisition Service static class.
	/// </summary>
	public static class DataAcquisitionService {
		private static IDataAcquisitionProvider provider = null;
		private static GenericProviderCollection<DataAcquisitionProvider, IDataAcquisitionProvider> providers = GenericProviderHelper.LoadProviders<DataAcquisitionProvider, GenericProviderSection, IDataAcquisitionProvider>("VizeliaDataAcquisitionSection", out provider);

		/// <summary>
		/// Gets a collection of the DataAcquisition providers.
		/// </summary>
		public static GenericProviderCollection<DataAcquisitionProvider, IDataAcquisitionProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Gets the DataAcquisitionContainer types.
		/// </summary>
		/// <returns></returns>
		public static List<Type> GetDataAcquisitionContainerTypes() {
			var retVal =
				(from type in Helper.GetBaseBusinessEntityTypes()
				 where type.IsSubclassOf(typeof(DataAcquisitionContainer))
				 select type).ToList();
			return retVal;
		}

		/// <summary>
		/// Gets the DataAcquisitionEndpoint types.
		/// </summary>
		/// <returns></returns>
		public static List<Type> GetDataAcquisitionEndpointTypes() {
			var retVal =
				(from type in Helper.GetBaseBusinessEntityTypes()
				 where type.IsSubclassOf(typeof(DataAcquisitionEndpoint))
				 select type).ToList();
			return retVal;
		}

		/// <summary>
		/// Gets the RESTDataAcquisitionProviders types.
		/// </summary>
		/// <returns></returns>
		public static IEnumerable<Type> GetRESTDataAcquisitionProviders() {
			var providerType = Type.GetType("Vizelia.FOL.ConcreteProviders.RESTDataAcquisitionProvider, Vizelia.FOL.ConcreteProviders");
			var retVal =
				(from type in Assembly.GetAssembly(providerType).GetTypes()
				 where type.IsSubclassOf(providerType) && type.IsAbstract == false
				 select type);
			return retVal;
		}


		/// <summary>
		/// Gets the provider.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <returns></returns>
		private static IDataAcquisitionProvider GetProvider(DataAcquisitionContainer container) {
			return Providers.Cast<IDataAcquisitionProvider>().FirstOrDefault(p => p.GetType() == Type.GetType(container.ProviderType));
		}


		/// <summary>
		/// Get the current value in the endpoint.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">the endpoint.</param>
		/// <returns></returns>
		public static List<DataAcquisitionEndpointValue> GetCurrentValue(DataAcquisitionContainer container, DataAcquisitionEndpoint endpoint) {
			var retVal = GetProvider(container).GetCurrentValue(container, endpoint);
			return retVal;
		}

		/// <summary>
		/// Gets the hierarchy from the container.
		/// e.g. get all the meters that are connected to a specific box. or - get all the geo locations a given webservice supports
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="parentItem">The parent item.</param>
		/// <returns></returns>
		public static List<TreeNode> GetHierarchy(DataAcquisitionContainer container, DataAcquisitionItem parentItem = null) {
			var retVal = GetProvider(container).GetHierarchy(container, parentItem);
			return retVal;
		}

		/// <summary>
		/// Perform any other changes to the object such as property manipulation or additions that come from the various DataAcquisition Providers
		/// e.g. Return additional data that came from a web service call in addition to data stored in the data layer.
		/// </summary>
		/// <param name="container">The container</param>
		/// <returns></returns>
		public static DataAcquisitionContainer FillObject(DataAcquisitionContainer container) {
			var retVal = GetProvider(container).FillObject(container);
			return retVal;
		}

	}

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide Scheduler services using providers.
	/// </summary>
	public abstract class SchedulerProvider : GenericProviderBase, ISchedulerProvider {


		/// <summary>
		/// Gets a list for the business entity Job. 
		/// </summary>
		/// <returns></returns>
		public abstract List<Job> GetJobs();

		/// <summary>
		/// Gets a specific item for the business entity Job.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public abstract Job GetJob(string Key);

		/// <summary>
		/// Creates a new business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public abstract Job CreateJob(Job item);

		/// <summary>
		/// Updates an existing business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public abstract Job UpdateJob(Job item);


		/// <summary>
		/// Deletes an existing business entity Job.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public abstract bool DeleteJob(Job item);

		/// <summary>
		/// Deletes all jobs.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		public abstract void DeleteAllJobs(string applicationName);
		
		/// <summary>
		/// Runs the job.
		/// </summary>
		/// <param name="item">The item.</param>
		public abstract void ExecuteJob(Job item);

		/// <summary>
		/// Pauses the item.
		/// </summary>
		/// <param name="item">The item.</param>
		public abstract void PauseItem(Job item);

		/// <summary>
		/// Resumes the item.
		/// </summary>
		/// <param name="item">The item.</param>
		public abstract void ResumeItem(Job item);

		/// <summary>
		/// Starts this instance.
		/// </summary>
		public abstract void Start();

	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface ISchedulerProvider : IGenericProviderBase {
		/// <summary>
		/// Gets a list for the business entity Job. 
		/// </summary>
		/// <returns></returns>
		 List<Job> GetJobs();

		/// <summary>
		/// Gets a specific item for the business entity Job.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		Job GetJob(string Key);

		/// <summary>
		/// Creates a new business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		Job CreateJob(Job item);

		/// <summary>
		/// Updates an existing business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		Job UpdateJob(Job item);


		/// <summary>
		/// Deletes an existing business entity Job.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		bool DeleteJob(Job item);

		/// <summary>
		/// Deletes all jobs.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		void DeleteAllJobs(string applicationName);

		/// <summary>
		/// Runs the job.
		/// </summary>
		/// <param name="item">The item.</param>
		void ExecuteJob(Job item);

		/// <summary>
		/// Starts this instance.
		/// </summary>
		void Start();

		/// <summary>
		/// Pauses the item.
		/// </summary>
		/// <param name="item">The item.</param>
		void PauseItem(Job item);

		/// <summary>
		/// Resumes the item.
		/// </summary>
		/// <param name="item">The item.</param>
		void ResumeItem(Job item);
	}
}
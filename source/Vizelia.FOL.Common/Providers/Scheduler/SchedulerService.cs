﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Vizelia.FOL.Common;
using System.Collections;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Scheduler Service static class.
	/// </summary>
	public static class SchedulerService {
		private static ISchedulerProvider provider = null;
		private static GenericProviderCollection<SchedulerProvider, ISchedulerProvider> providers = GenericProviderHelper.LoadProviders<SchedulerProvider, GenericProviderSection, ISchedulerProvider>("VizeliaSchedulerSection", out provider);

		/// <summary>
		/// Gets a collection of the Job providers.
		/// </summary>
		public static GenericProviderCollection<SchedulerProvider, ISchedulerProvider> Providers {
			get {
				return providers;
			}
		}


		/// <summary>
		/// Gets a list for the business entity Job. 
		/// </summary>
		/// <returns></returns>
		public static List<Job> GetJobs() {
			return provider.GetJobs();
		}

		/// <summary>
		/// Gets a specific item for the business entity Job.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public static Job GetJob(string Key) {
			return provider.GetJob(Key);
		}

		/// <summary>
		/// Creates a new business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public static Job CreateJob(Job item) {
			return provider.CreateJob(item);
		}

		/// <summary>
		/// Updates an existing business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public static Job UpdateJob(Job item) {
			return provider.UpdateJob(item);

		}


		/// <summary>
		/// Deletes an existing business entity Job. 
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public static bool DeleteJob(Job item) {
			return provider.DeleteJob(item);
		}

		/// <summary>
		/// Deletes all jobs.
		/// </summary>
		/// <param name="applicationName">The application name.</param>
		public static void DeleteAllJobs(string applicationName) {
			provider.DeleteAllJobs(applicationName);
		}
		

		/// <summary>
		/// Runs the job.
		/// </summary>
		/// <param name="item">The item.</param>
		public static void ExecuteJob(Job item) {
			provider.ExecuteJob(item);
		}

		/// <summary>
		/// Pauses the selected job
		/// </summary>
		/// <param name="item">The item.</param>
		public static void PauseItem(Job item) {
			provider.PauseItem(item);
		}


		/// <summary>
		/// Resumes the selected job
		/// </summary>
		/// <param name="item">The item.</param>
		public static void ResumeItem(Job item) {
			provider.ResumeItem(item);

		}


		/// <summary>
		/// Starts this instance.
		/// </summary>
		public static void Start() {
			provider.Start();
		}

		}

}
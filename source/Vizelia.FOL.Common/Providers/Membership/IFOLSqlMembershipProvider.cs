﻿using System;
using System.Collections.Generic;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// The FOL SQL Membership Provider interface.
	/// </summary>
	public interface IFOLSqlMembershipProvider {
		/// <summary>
		/// Gets or sets the name of the application to store and retrieve membership information for.
		/// </summary>
		/// <returns>The name of the application to store and retrieve membership information for. The default is the <see cref="P:System.Web.HttpRequest.ApplicationPath"/> property value for the current <see cref="P:System.Web.HttpContext.Request"/>.</returns>
		///   
		/// <exception cref="T:System.ArgumentException">An attempt was made to set the <see cref="P:System.Web.Security.SqlMembershipProvider.ApplicationName"/> property to an empty string or null.</exception>
		///   
		/// <exception cref="T:System.Configuration.Provider.ProviderException">An attempt was made to set the <see cref="P:System.Web.Security.SqlMembershipProvider.ApplicationName"/> property to a string that is longer than 256 characters.</exception>
		string ApplicationName { get; set; }

		/// <summary>
		/// The time in minutes that should cause an automatic unlock.
		/// A value of 0 indicates that the behavior is disabled and there should not be any automatic unlock.
		/// </summary>
		int AutoUnlockTimeout { get; set; }

		/// <summary>
		/// The time in minutes that should cause an automatic lock.
		/// A value of 0 indicates that the behavior is disabled and there should not be any automatic lock.
		/// </summary>
		int AutoLockTimeout { get; set; }

		/// <summary>
		/// The number of last passwords that should be check when assigning a new password.
		/// </summary>
		int PasswordHistoryCount { get; set; }

		/// <summary>
		/// The time in minutes after which password should expire.
		/// </summary>
		int PasswordExpiration { get; set; }

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		void Initialize(string name, System.Collections.Specialized.NameValueCollection config);

		/// <summary>
		/// Validates the credential of a user. 
		/// </summary>
		/// <param name="username">Name of the user to validate.</param>
		/// <param name="password">Password of the user to validate.</param>
		/// <returns>True if successfull, false otherwise.</returns>
		bool ValidateUser(string username, string password);

		/// <summary>
		/// Logs the user in LoginHistory.
		/// </summary>
		/// <param name="username"></param>
		/// <param name="sessionID"></param>
		/// <param name="loginDate"></param>
		/// <param name="logoutDate"></param>
		void AddLoginHistory(string username, string sessionID, DateTime? loginDate, DateTime? logoutDate);

		/// <summary>
		/// Gets the user list filtered.
		/// </summary>
		/// <param name="locations">The locations.</param>
		/// <param name="roles">The roles.</param>
		/// <returns></returns>
		List<FOLMembershipUser> GetUserListFiltered(List<string> locations, List<string> roles);

		/// <summary>
		/// Deletes a user.
		/// </summary>
		/// <param name="username">Name of the user.</param>
		/// <param name="deleteAllRelatedData">True to delete related data, false otherwise.</param>
		/// <returns>True if operation successfull, false otherwise.</returns>
		bool DeleteUser(string username, bool deleteAllRelatedData);

		/// <summary>
		/// Gets a user.
		/// </summary>
		/// <param name="providerUserKey">The provider user key.</param>
		/// <param name="userIsOnline">True to update last date connection, false otherwise.</param>
		/// <returns>The membership user.</returns>
		MembershipUser GetUser(object providerUserKey, bool userIsOnline);

		/// <summary>
		/// Gets a user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="userIsOnline">True top update last date connection, false otherwise.</param>
		/// <returns>The membership user.</returns>
		MembershipUser GetUser(string username, bool userIsOnline);

		/// <summary>
		/// Updates the UserName of a user.
		/// </summary>
		/// <param name="oldUserName">The old UserName.</param>
		/// <param name="newUserName">The new UserName.</param>
		/// <returns>True if the action was successfull, false otherwise.</returns>
		bool ChangeUserName(string oldUserName, string newUserName);

		/// <summary>
		/// Updates a user.
		/// </summary>
		/// <param name="user">The user.</param>
		void UpdateUser(MembershipUser user);

		/// <summary>
		/// Creates the user.
		/// </summary>
		/// <param name="item">The user.</param>
		/// <param name="status">The status.</param>
		/// <returns></returns>
		MembershipUser CreateUser(FOLMembershipUser item, out MembershipCreateStatus status);

		/// <summary>
		/// Creates a new user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="password">The password</param>
		/// <param name="email">The email address.</param>
		/// <param name="passwordQuestion">The password question.</param>
		/// <param name="passwordAnswer">The password answer.</param>
		/// <param name="isApproved">True if user is approved, false otherwise.</param>
		/// <param name="providerUserKey">The provider user key.</param>
		/// <param name="status">The membership status of the operation.</param>
		/// <returns>The membership user.</returns>
		MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status);

		/// <summary>
		/// Changes the password format for an existing user.
		/// This is necessary when modifying web.config because existing password are not updated when modifying passwordFormat.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		bool ChangePasswordFormat(string username);

		/// <summary>
		/// Changes the password for a membership user.
		/// This method works regardless of the password format (clear, encrypted, hashed).
		/// It requires that requiresPasswordAndQuestion is set to false.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="newPassword">The new password.</param>
		/// <returns>True if operation was successfull, false otherwise.</returns>
		bool ChangePassword(string username, string newPassword);

		/// <summary>
		/// Changes the password for a membership user.
		/// </summary>
		/// <param name="username"></param>
		/// <param name="oldPassword"></param>
		/// <param name="newPassword"></param>
		/// <returns></returns>
		bool ChangePassword(string username, string oldPassword, string newPassword);

		/// <summary>
		/// Resets the password for a membership user.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <param name="isForgotPassword">The is forgot password.</param>
		/// <returns>
		/// The reseted password.
		/// </returns>
		string ResetPassword(string username, bool? isForgotPassword = null);

		/// <summary>
		/// Locks a user account.
		/// </summary>
		/// <param name="username">The user name.</param>
		/// <returns>True if successfull, false otherwise.</returns>
		bool LockUser(string username);

		/// <summary>
		/// Switches the is forgot password off.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <returns></returns>
		bool SwitchIsForgotPasswordOff(string username);

		/// <summary>
		/// Gets the users store.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		JsonStore<FOLMembershipUser> GetUsersStore(PagingParameter paging, params string[] fields);
	}
}
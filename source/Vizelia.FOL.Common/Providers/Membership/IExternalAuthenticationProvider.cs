﻿namespace Vizelia.FOL.Providers {
	/// <summary>
	/// An external authentication provider.
	/// </summary>
	public interface IExternalAuthenticationProvider {
		/// <summary>
		/// Synchronizes the provider with the external authentication source.
		/// </summary>
		void Synchronize();
	}
}
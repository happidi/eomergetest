﻿using System;
using System.Collections.Generic;
using System.IO;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// An interface that represents a client for remote file access.
	/// </summary>
	public interface IRemoteFileAccessClient : IDisposable {
		/// <summary>
		/// Determines whether the specified file source has files.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the specified file source has files; otherwise, <c>false</c>.
		/// </returns>
		bool HasFiles();

		/// <summary>
		/// Downloads the file from the file source to the local destination file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="localDestinationFilename">The local destination filename.</param>
		void DownloadFile(string filename, string localDestinationFilename);

		/// <summary>
		/// Deletes the specified file from the file source.
		/// </summary>
		/// <param name="filename">The filename.</param>
		void Delete(string filename);

		/// <summary>
		/// Tests the connection to the file source.
		/// </summary>
		/// <returns></returns>
		RemoteFileAccessConnectionTestResult TestConnection();

		/// <summary>
		/// Returns a boolean value indicating whether the file exists or not.
		/// </summary>
		/// <param name="filename">The filename.</param>
		bool Exists(string filename);

		/// <summary>
		/// Renames the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="newFilename">The new filename.</param>
		void RenameFile(string filename, string newFilename);

		/// <summary>
		/// Connects to the file source.
		/// </summary>
		void Connect();

		/// <summary>
		/// Gets the file names sorted by date.
		/// </summary>
		/// <returns></returns>
		IEnumerable<string> GetFileNamesSortedByDate();

		/// <summary>
		/// Uploads the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="stream">The stream.</param>
		void UploadFile(string filename, Stream stream);

		/// <summary>
		/// Creates the directory.
		/// </summary>
		/// <param name="path">The path.</param>
		void CreateDirectory(string path);

		/// <summary>
		/// Deletes directories older than the specified time.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="time">The time.</param>
		void DeleteOldDirectories(string path, DateTime time);

		/// <summary>
		/// Validates the client settings.
		/// </summary>
		void Validate();
	}
}
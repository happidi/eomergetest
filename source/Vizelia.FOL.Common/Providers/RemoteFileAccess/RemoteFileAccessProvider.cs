using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Defines the contract to provide Remote File Access services using custom Mapping providers.
	/// </summary>
	public abstract class RemoteFileAccessProvider : GenericProviderBase, IRemoteFileAccessProvider {
		/// <summary>
		/// Gets the remote file access client.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <returns>The remote file access client for the file source.</returns>
		public abstract IRemoteFileAccessClient GetClient(IFileSource fileSource);

	}
}
﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	
	/// <summary>
	/// An interface for a remote file access provider.
	/// </summary>
	public interface IRemoteFileAccessProvider : IGenericProviderBase {
		/// <summary>
		/// Gets the remote file access client.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <returns>The remote file access client for the file source.</returns>
		IRemoteFileAccessClient GetClient(IFileSource fileSource);
		
	}
}
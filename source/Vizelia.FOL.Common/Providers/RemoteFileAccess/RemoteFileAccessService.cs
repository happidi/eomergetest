﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// A service that calls the Remote File Access provider.
	/// </summary>
	public class RemoteFileAccessService {
		private static IRemoteFileAccessProvider defaultProvider = null;
		private static readonly GenericProviderCollection<RemoteFileAccessProvider, IRemoteFileAccessProvider> providers = GenericProviderHelper.LoadProviders<RemoteFileAccessProvider, GenericProviderSection, IRemoteFileAccessProvider>("VizeliaRemoteFileAccessSection", out defaultProvider);

		/// <summary>
		/// Gets a collection of the long-running operation providers.
		/// </summary>
		public static GenericProviderCollection<RemoteFileAccessProvider, IRemoteFileAccessProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Gets the provider for the given file source.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		private static IRemoteFileAccessProvider GetProvider(IFileSource fileSource) {
			IRemoteFileAccessProvider selectedProvider;

			switch (fileSource.FileSourceType) {
				case FileSourceType.UNC:
					selectedProvider = Providers["UNC"];
					break;
				case FileSourceType.FTP:
					selectedProvider = Providers["FTP"];
					break;
				case FileSourceType.SFTP:
					selectedProvider = Providers["SFTP"];
					break;
				default:
					throw new ArgumentOutOfRangeException("fileSource.FileSourceType");
			}

			return selectedProvider;
		}

		/// <summary>
		/// Gets the remote file access client.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <returns>The remote file access client for the file source.</returns>
		public static IRemoteFileAccessClient GetClient(IFileSource fileSource) {
			var selectedProvider = GetProvider(fileSource);
			IRemoteFileAccessClient client = selectedProvider.GetClient(fileSource);

			return client;
		}

	}
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// We overload the resource manager so we can check the DB(or any other provider) before going to the resx file.
	/// </summary>
	public class VizeliaResourceManager : ResourceManager {


		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaResourceManager"/> class.
		/// </summary>
		/// <param name="baseName">Name of the base.</param>
		/// <param name="assembly">The assembly.</param>
		public VizeliaResourceManager(string baseName, Assembly assembly)
			: base(baseName, assembly) {

		}


		/// <summary>
		/// Gets the value of the <see cref="T:System.String"/> resource localized for the specified culture.
		/// </summary>
		/// <param name="name">The name of the resource to get.</param>
		/// <param name="culture">The <see cref="T:System.Globalization.CultureInfo"/> object that represents the culture for which the resource is localized. Note that if the resource is not localized for this culture, the lookup will fall back using the current thread's <see cref="P:System.Globalization.CultureInfo.Parent"/> property, stopping after looking in the neutral culture.If this value is null, the <see cref="T:System.Globalization.CultureInfo"/> is obtained using the current thread's <see cref="P:System.Globalization.CultureInfo.CurrentUICulture"/> property.</param>
		/// <returns>
		/// The value of the resource localized for the specified culture. If a best match is not possible, null is returned.
		/// </returns>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="name"/> parameter is null. </exception>
		///   
		/// <exception cref="T:System.InvalidOperationException">The value of the specified resource is not a <see cref="T:System.String"/>. </exception>
		///   
		/// <exception cref="T:System.Resources.MissingManifestResourceException">No usable set of resources has been found, and there are no neutral culture resources. </exception>
		public override string GetString(string name, System.Globalization.CultureInfo culture) {
			return LocalizationService.GetLocalizedText(this, name, culture,false);
		}

		/// <summary>
		/// Gets the base string.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public string GetStringFromResx(string name, System.Globalization.CultureInfo culture) {
			return base.GetString(name, culture);
		}
	}
}

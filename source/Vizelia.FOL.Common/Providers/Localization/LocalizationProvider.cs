﻿using System;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Diagnostics;
using System.Reflection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Microsoft.Practices.Unity.InterceptionExtension;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Defines the contract to provide localization services using the the selected provider..
	/// </summary>
	[ApplyNoPolicies]
	public abstract class LocalizationProvider : GenericProviderBase, ILocalizationProvider {
	


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		/// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
		///   
		/// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		///   
		/// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.</exception>
		public override void Initialize(string name, NameValueCollection config) {
			base.Initialize(name, config);

			
			//We need to override the ResourceManager in the Langue with our ResourceManager that uses the provider.
			Type myType = typeof(Langue);
			FieldInfo myFieldInfo = myType.GetField("resourceMan", BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.FlattenHierarchy);
			myFieldInfo.SetValue(null, new VizeliaResourceManager("Vizelia.FOL.Common.Localization.Langue", typeof(Langue).Assembly));
			
		}


		/// <summary>
		/// Gets the localized text.
		/// </summary>
		/// <param name="msgCode">The message code.</param>
		/// <param name="cultureCode">The culture code.</param>
		/// <returns></returns>
		public abstract string GetLocalizedText(string msgCode, string cultureCode);

		/// <summary>
		/// Adds the localized text to cache.
		/// </summary>
		/// <param name="msgCode">The MSG code.</param>
		/// <param name="cultureCode">The culture code.</param>
		/// <param name="value">The value.</param>
		public abstract void AddLocalizedTextToCache(string msgCode, string cultureCode, string value);
		
		/// <summary>
		/// Removes the localized text from cache.
		/// </summary>
		/// <param name="msgCode">The MSG code.</param>
		/// <param name="cultureCode">The culture code.</param>
		public abstract void RemoveLocalizedTextFromCache(string msgCode, string cultureCode);
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface ILocalizationProvider : IGenericProviderBase {


		/// <summary>
		/// Gets the localized text.
		/// </summary>
		/// <param name="msgCode">The message code.</param>
		/// <param name="cultureCode">The culture code.</param>
		/// <returns></returns>
		[ApplyNoPolicies]
		string GetLocalizedText(string msgCode, string cultureCode);


		/// <summary>
		/// Adds the localized text to cache.
		/// </summary>
		/// <param name="msgCode">The MSG code.</param>
		/// <param name="cultureCode">The culture code.</param>
		/// <param name="value">The value.</param>
		void AddLocalizedTextToCache(string msgCode, string cultureCode, string value);

		/// <summary>
		/// Removes the localized text from cache.
		/// </summary>
		/// <param name="msgCode">The MSG code.</param>
		/// <param name="cultureCode">The culture code.</param>
		void RemoveLocalizedTextFromCache(string msgCode, string cultureCode);
	}
}

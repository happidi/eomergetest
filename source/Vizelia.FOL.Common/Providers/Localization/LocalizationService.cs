﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Localization Service static class.
	/// </summary>
	public static class LocalizationService {
		private static ILocalizationProvider provider;
		private static GenericProviderCollection<LocalizationProvider, ILocalizationProvider> providers = GenericProviderHelper.LoadProviders<LocalizationProvider, GenericProviderSection, ILocalizationProvider>("VizeliaLocalizationSection", out provider);


		static LocalizationService() {
		}

		/// <summary>
		/// Gets a collection of the localization providers.
		/// </summary>
		public static GenericProviderCollection<LocalizationProvider, ILocalizationProvider> Providers {
			get { return providers; }
		}


		/// <summary>
		/// Gets the localized text.
		/// </summary>
		/// <param name="vizeliaResourceManager">The vizelia resource manager.</param>
		/// <param name="msgCode">The MSG code.</param>
		/// <param name="culture">The culture code.</param>
		/// <param name="shouldReturnNullIfNotFound">if set to <c>true</c> [should return null if not found].</param>
		/// <returns></returns>
		public static string GetLocalizedText(VizeliaResourceManager vizeliaResourceManager, string msgCode, CultureInfo culture = null, bool shouldReturnNullIfNotFound = false) {

			string cultureString;
			if (culture == null) {
				cultureString = Helper.GetCurrentApplicableCulture();
			}
			else {
				cultureString = culture.Name;
			}

			string localizedText = null;

			if (!string.IsNullOrWhiteSpace(msgCode)) {
				try {
					localizedText =  provider.GetLocalizedText(msgCode, cultureString);
				}
				catch { }

				if (!String.IsNullOrEmpty(localizedText)) {
					return localizedText;
				}

				try {
					localizedText = vizeliaResourceManager.GetStringFromResx(msgCode, culture);
				}
				catch { }
			}
			if (String.IsNullOrEmpty(localizedText) && !shouldReturnNullIfNotFound)
				localizedText = msgCode;
			return localizedText;
		}

		/// <summary>
		/// Gets the supported cultures from the DB.
		/// </summary>
		/// <returns></returns>
		private static List<string> GetSupportedCultures() {
			var assembly = Assembly.Load("Vizelia.FOL.Common.Localization.ResourceManager");
			var method = assembly.GetType("Vizelia.FOL.Common.Localization.DatabaseResourceHelper").GetMethod("GetSupportedCultures");
			var supportedCultures = method.Invoke(null, new object[] { }) as List<string>;
			return supportedCultures;
		}

		//We need this method even empty because calling it will cause the Initialize to run.
		/// <summary>
		/// Inits this instance.
		/// </summary>
		public static void Init() {
		}

		/// <summary>
		/// Adds the localized text to cache.
		/// </summary>
		/// <param name="msgCode">The MSG code.</param>
		/// <param name="cultureCode">The culture code.</param>
		/// <param name="value">The value.</param>
		public static void AddLocalizedTextToCache(string msgCode, string cultureCode, string value) {
			provider.AddLocalizedTextToCache(msgCode, cultureCode, value);
		}

		/// <summary>
		/// Removes the localized text from cache.
		/// </summary>
		/// <param name="msgCode">The MSG code.</param>
		/// <param name="cultureCode">The culture code.</param>
		public static void RemoveLocalizedTextFromCache(string msgCode, string cultureCode) {
			provider.RemoveLocalizedTextFromCache(msgCode, cultureCode);
		}
	}
}

﻿using Vizelia.FOL.Common;
using System.Net.Mail;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace Vizelia.FOL.Providers {
	/// <summary>
	/// Defines the contract to provide Mail services using custom mail providers.
	/// </summary>
	[ApplyNoPolicies]
	public abstract class MailProvider : GenericProviderBase, IMailProvider {

		/// <summary>
		/// Sends a mail message.
		/// </summary>
		/// <param name="message">The mail message</param>
		[ApplyNoPolicies]
		public abstract void Send(MailMessage message);
	}

	/// <summary>
	/// Defines the public interface for the provider.
	/// </summary>
	public interface IMailProvider : IGenericProviderBase {

		/// <summary>
		/// Sends a mail message.
		/// </summary>
		/// <param name="message">The mail message.</param>
		[ApplyNoPolicies]
		void Send(MailMessage message);
	}
}

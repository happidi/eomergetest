﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;

namespace Vizelia.FOL.Providers {

	/// <summary>
	/// Mail Service static class.
	/// </summary>
	public static class MailService {
		private static IMailProvider provider = null;
		private static GenericProviderCollection<MailProvider, IMailProvider> providers = GenericProviderHelper.LoadProviders<MailProvider, GenericProviderSection, IMailProvider>("VizeliaMailSection", out provider);

		/// <summary>
		/// Gets a collection of the mail providers.
		/// </summary>
		public static GenericProviderCollection<MailProvider, IMailProvider> Providers {
			get {
				return providers;
			}
		}

		/// <summary>
		/// Sends a mail message.
		/// </summary>
		/// <param name="message">The mail message.</param>
		public static void Send(MailMessage message) {
			if (string.IsNullOrEmpty(message.Body.Trim()) || string.IsNullOrEmpty(message.Subject.Trim())) {
				string errorMessage = "Can't send Mail message without body and subject.";
				TracingService.Write(TraceEntrySeverity.Error, errorMessage);
				throw new ArgumentException(errorMessage);
			}
			switch (VizeliaConfiguration.Instance.Mail.Behavior) {
				case MailBehavior.Inactive:
					// Don't send.
					return;
				case MailBehavior.Debug:
					// Send with different parameters.
					ModifyMessageForDebug(message);
					SendMessage(message);
					break;
				case MailBehavior.Release:
					// Send as is.
					SendMessage(message);
					break;
				default:
					throw new ArgumentOutOfRangeException("message");
			}

		}

		/// <summary>
		/// Sends the message.
		/// </summary>
		/// <param name="message">The message.</param>
		private static void SendMessage(MailMessage message) {
			provider.Send(message);
		}

		/// <summary>
		/// Modifies the message for debug scenario.
		/// </summary>
		/// <param name="message">The message.</param>
		private static void ModifyMessageForDebug(MailMessage message) {
			if (!String.IsNullOrEmpty(VizeliaConfiguration.Instance.Mail.OverrideTo)) {
				message.To.Clear();
				message.CC.Clear();
				message.To.Add(VizeliaConfiguration.Instance.Mail.OverrideTo);
			}

			if (!String.IsNullOrEmpty(VizeliaConfiguration.Instance.Mail.AddCC)) {
				message.CC.Add(VizeliaConfiguration.Instance.Mail.AddCC);
			}

			if (!String.IsNullOrEmpty(VizeliaConfiguration.Instance.Mail.SenderMail)) {
				message.From = new MailAddress(VizeliaConfiguration.Instance.Mail.SenderMail, message.From.DisplayName);
			}

			if (!String.IsNullOrEmpty(VizeliaConfiguration.Instance.Mail.SenderName)) {
				message.From = new MailAddress(message.From.Address, VizeliaConfiguration.Instance.Mail.SenderName);
			}
		}

		/// <summary>
		/// Converts the mails to mail messages.
		/// </summary>
		/// <param name="modelData">The model data.</param>
		/// <param name="mails">The mails.</param>
		/// <returns></returns>
		public static IEnumerable<MailMessage> ConvertMailsToMailMessages<T>(T modelData, IEnumerable<Mail> mails) where T : class {
			var mailMessages = new List<MailMessage>();
			var keyMail = "";
			try {

				var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

				foreach (var mail in mails) {
					keyMail = mail.KeyMail;
					var mailFields = new List<string> { mail.To, mail.CC, mail.Bcc, mail.From, mail.Subject, mail.Body };
					var parsedFields = TemplateService.ParseMany(mailFields, modelData);
					mail.To = parsedFields[0];
					mail.CC = parsedFields[1];
					mail.Bcc = parsedFields[2];
					mail.From = parsedFields[3];
					mail.Subject = parsedFields[4];
					mail.Body = parsedFields[5];

					mail.From = IsMailAddress(mail.From) ? mail.From : mail.From + "<" + smtpSection.From + ">";

					var message = new MailMessage {
						From = new MailAddress(mail.From),
						IsBodyHtml = true,
						Priority = mail.Priority,
						Subject = mail.Subject,
						Body = mail.Body
					};

					ConvertMailAddresses(message.To, mail.To);
					ConvertMailAddresses(message.CC, mail.CC);
					ConvertMailAddresses(message.Bcc, mail.Bcc);

					mailMessages.Add(message);
				}
			}
			catch (Exception exception) {
				TracingService.Write(TraceEntrySeverity.Error, exception.Message, "MailService", keyMail);
			}
			return mailMessages;
		}

		/// <summary>
		/// Converts the mail adresses.
		/// </summary>
		/// <param name="addressCollection">The address collection.</param>
		/// <param name="adresses">The adresses.</param>
		private static void ConvertMailAddresses(MailAddressCollection addressCollection, string adresses) {
			if (String.IsNullOrEmpty(adresses))
				return;
			foreach (var ads in adresses.Split(';', ' ', ',').Where(IsMailAddress)) {
				addressCollection.Add(ads);
			}
		}

		/// <summary>
		/// Determines whether the input string is email format.
		/// </summary>
		/// <param name="address">The address.</param>
		private static bool IsMailAddress(string address) {
			var retVal = false;
			if (!string.IsNullOrWhiteSpace(address)) {
				var delimiterRegex = new Regex(@"<(?<email>.*@.*)>", RegexOptions.Compiled);
				var regexMatches = delimiterRegex.Matches(address);
				string emailAddress = null;
				switch (regexMatches.Count) {
					case 0:
						emailAddress = address;
						break;
					case 1:
						var match = regexMatches.Cast<Match>().First();
						emailAddress = match.Groups["email"].Value;
						break;
				}
				if (emailAddress != null) {
					const string mailExpression =
						@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
					// @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
					retVal = Regex.IsMatch(emailAddress, mailExpression);
				}
			}
			return retVal;
		}
	}

}

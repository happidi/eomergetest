﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration.Provider;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;
using Vizelia.FOL.Common.Configuration;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Base class for any provider collection.
	/// </summary>
	/// <typeparam name="T">
	/// The type of the provider.
	/// T is a subclass of ProviderBase.
	/// </typeparam>
	/// <typeparam name="I">
	/// The interface of the provider.
	/// I is a subclass of IGenericProviderbase
	/// </typeparam>
	public class GenericProviderCollection<T, I> : ProviderCollection
		where T : ProviderBase
		where I : class, IGenericProviderBase {
		/// <summary>
		/// Adds a provider to the collection.
		/// </summary>
		/// <param name="provider">The provider to add.</param>
		public override void Add(ProviderBase provider) {
			if (provider == null)
				throw new ArgumentException("provider");
			if (!(provider is T))
				throw new ArgumentException(String.Format("provider is not of type {0}", typeof(T).FullName));
			base.Add(provider);
		}

		/// <summary>
		/// Gets the provider from its name.
		/// We use policy injection here.
		/// </summary>
		/// <param name="name">The name of the provider</param>
		/// <returns>The provider</returns>
		public new I this[string name] {
			get {
				//return PolicyInjection.Wrap<I>(base[name]);
				var attribute = Helper.GetAttribute<EnterpriseLibraryNotWrappableAttribute>(typeof(I));
                if (attribute == null) {
                    if (VizeliaConfiguration.Instance.Deployment.UsePolicyInjection)
                        return PolicyInjection.Wrap<I>(base[name]);
                    else
                        return base[name] as I;
                }
                else
                    return base[name] as I;
			}
		}
	}

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Vizelia.FOL.Common {
    /// <summary>
    /// A generic implementation of a configuration section
    /// </summary>
    public class GenericProviderSection : ConfigurationSection {
        /// <summary>
        /// Returns the collection of sections.
        /// </summary>
        [ConfigurationProperty("providers")]
        public ProviderSettingsCollection Providers {
            get { return (ProviderSettingsCollection)base["providers"]; }
        }

        /// <summary>
        /// Returns the default provider.
        /// </summary>
        [ConfigurationProperty("defaultProvider", IsRequired = true)]
        public string DefaultProvider {
            get { return (string)base["defaultProvider"]; }
            set { base["defaultProvider"] = value; }
        }

        /// <summary>
        /// Returns the xmlns attribute.
        /// </summary>
        [ConfigurationProperty("xmlns", IsRequired = false)]
        public string Xmlns {
            get { return (string)base["xmlns"]; }
            set { base["xmlns"] = value; }
        }
    }

}

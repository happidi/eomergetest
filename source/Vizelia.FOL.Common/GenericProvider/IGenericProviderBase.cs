﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Common {
    /// <summary>
    /// Generic parent interface for any provider.
    /// </summary>
    public interface IGenericProviderBase {
		/// <summary>
		/// Gets the name.
		/// </summary>
    	string Name { get; }
    }

}

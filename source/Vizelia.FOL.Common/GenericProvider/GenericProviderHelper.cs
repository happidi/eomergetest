﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using System.Configuration.Provider;
using System.Configuration;

namespace Vizelia.FOL.Common {
  /// <summary>
    /// Helper static class for generic provider.
    /// </summary>
    public static class GenericProviderHelper {

        static object LoadLock = new object();
        /// <summary>
        /// Helper method for populating a provider collection 
        /// from a Provider section handler. 
        /// </summary>
        /// <typeparam name="T">Provider class. T is a subclass of ProviderBase.</typeparam>
        /// <typeparam name="K">The provider section class.</typeparam>
        /// <typeparam name="I">The interface of the provider. I is a subclass of IProviderBase.</typeparam>
        /// <param name="sectionName"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public static GenericProviderCollection<T, I> LoadProviders<T, K, I>(string sectionName, out I provider)
            where T : ProviderBase
            where K : GenericProviderSection
            where I : class, IGenericProviderBase {

			var providers = new GenericProviderCollection<T, I>();
            lock (LoadLock) {
                // Get a reference to the provider section
                K section = (K)ConfigurationManager.GetSection(sectionName);
				if (section == null) {
					// the section was not even specified in the web.config; we assume it's not needed (will fail upon first request
					// if one is made)
					provider = null;
				}
				else
				{
            		// Load registered providers and point _provider
					// to the default provider
					ProvidersHelper.InstantiateProviders(section.Providers, providers, typeof (T));
					provider = providers[section.DefaultProvider];
					if (provider == null)
						throw new ProviderException(
							string.Format(
								"Unable to load default '{0}' provider",
								sectionName));
				}
            }
			return providers;
        }

    }
}

﻿using System;
using System.Configuration.Provider;

namespace Vizelia.FOL.Common {
    /// <summary>
    /// Generic parent class for any provider.
    /// In addition of deriving form ProviderBase, any subclass of GenericProviderBase should also implements a subclass of IGenericProviderBase.
    /// This interface is used for policy injection.
    /// </summary>
    public class GenericProviderBase : ProviderBase {
		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
		/// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
		/// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		/// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.</exception>
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
            if (config == null)
                throw new ArgumentNullException("config");
            if (String.IsNullOrEmpty(name)) {
                throw new ProviderException("name is missing");
            }
            if (String.IsNullOrEmpty(config["description"])) {
                config.Remove("description");
                config.Add("description", "");
            }
            if (String.IsNullOrEmpty(config["xmlns"])) {
                config.Remove("xmlns");
                //config.Add("xmlns", "");
            }
            base.Initialize(name, config);
        }
    }

}

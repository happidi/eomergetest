﻿using System;
using System.Configuration;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// The Map Reduce configuration section Entity.
	/// </summary>
	public class MapReduceSection : ConfigurationSection {

		/// <summary>
		/// Gets the different instances of Map Reduce.
		/// </summary>
		[ConfigurationProperty("instances")]
		public MapReduceInstanceCollection Instances {
			get {
				return this["instances"] as MapReduceInstanceCollection;
			}
		}

		/// <summary>
		/// Returns the xmlns attribute.
		/// </summary>
		[ConfigurationProperty("xmlns", IsRequired = false)]
		public string Xmlns {
			get { return (string)base["xmlns"]; }
			set { base["xmlns"] = value; }
		}
	}


	/// <summary>
	/// Configuration entity for an Map Reduce instance.
	/// </summary>
	public class MapReduceInstance : ConfigurationElement {
		/// <summary>
		/// Gets the name.
		/// </summary>
		[ConfigurationProperty("name", IsRequired = true)]
		public string Name {
			get {
				return this["name"] as string;
			}
		}

		/// <summary>
		/// Gets the protocol.
		/// </summary>
		[ConfigurationProperty("protocol", IsRequired = true)]
		public ServiceInvocationProtocol Protocol {
			get {
				return this["protocol"].ToString().ParseAsEnum<ServiceInvocationProtocol>();
			}
		}

		/// <summary>
		/// Gets the end point (used when protocol is WCFHTTP).
		/// </summary>
		[ConfigurationProperty("endpoint", IsRequired = false)]
		public string EndPoint {
			get {
				return this["endpoint"] as string;
			}
		}


		/// <summary>
		/// Gets the login to use with the authorization proxy when protocol is WCFHTTP.
		/// </summary>
		[ConfigurationProperty("login", IsRequired = false)]
		public string Login {
			get {
				return this["login"] as string;
			}
		}

		/// <summary>
		/// Gets the password to use with the authorization proxy when protocol is WCFHTTP.
		/// </summary>
		[ConfigurationProperty("password", IsRequired = false)]
		public string Password {
			get {
				return this["password"] as string;
			}
		}

		/// <summary>
		/// Gets the provider to use with the authorization proxy when protocol is WCFHTTP.
		/// </summary>
		[ConfigurationProperty("provider", IsRequired = false)]
		public string Provider {
			get {
				return this["provider"] as string;
			}
		}
		/// <summary>
		/// Gets the type (used when protocol is InMemory).
		/// </summary>
		[ConfigurationProperty("type", IsRequired = false)]
		private string type {
			get {
				return this["type"] as string;
			}
		}
		/// <summary>
		/// Gets the type (used when protocol is InMemory).
		/// </summary>
		public Type Type {
			get {
				return Type.GetType(type);
			}
		}

        /// <summary>
        /// Gets the application name.
        /// </summary>
        [ConfigurationProperty("applicationName", IsRequired = false)]
        public string ApplicationName {
			get { return this["applicationName"] as string; }
        }
	}

	/// <summary>
	/// A collection of Map Reduce configuration.
	/// </summary>
	public class MapReduceInstanceCollection : ConfigurationElementCollection {
		/// <summary>
		/// Gets or sets a property, attribute, or child element of this configuration element.
		/// </summary>
		/// <returns>The specified property, attribute, or child element</returns>
		public MapReduceInstance this[int index] {
			get {
				return base.BaseGet(index) as MapReduceInstance;
			}
			set {
				if (base.BaseGet(index) != null) {
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}


		/// <summary>
		/// When overridden in a derived class, creates a new <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </summary>
		/// <returns>
		/// A new <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </returns>
		protected override ConfigurationElement CreateNewElement() {
			return new MapReduceInstance();
		}

		/// <summary>
		/// Gets the element key for a specified configuration element when overridden in a derived class.
		/// </summary>
		/// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement"/> to return the key for.</param>
		/// <returns>
		/// An <see cref="T:System.Object"/> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </returns>
		protected override object GetElementKey(ConfigurationElement element) {
			return ((MapReduceInstance)element).Name;
		}
	}
}

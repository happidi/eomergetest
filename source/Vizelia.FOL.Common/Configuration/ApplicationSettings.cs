﻿

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// 
	/// </summary>
	public class ApplicationSettings {
		/// <summary>
		/// Gets or sets the help URL.
		/// </summary>
		/// <value>
		/// The help URL.
		/// </value>
		public string HelpUrl { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [raw data used in meter grid by default].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [raw data used in meter grid by default]; otherwise, <c>false</c>.
		/// </value>
		public bool RawDataUsedInMeterGridByDefault { get; set; }

		/// <summary>
		/// Gets or sets the default meter data validity.
		/// </summary>
		/// <value>
		/// The default meter data validity.
		/// </value>
		public int DefaultMeterDataValidity { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [disable permissions].
		/// </summary>
		/// <value>
		///   <c>true</c> if [disable permissions]; otherwise, <c>false</c>.
		/// </value>
		public bool DisablePermissions { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [allow remember me].
		/// </summary>
		/// <value>
		///   <c>true</c> if [allow remember me]; otherwise, <c>false</c>.
		/// </value>
		public bool AllowRememberMe { get; set; }

		/// <summary>
		/// Gets or sets the session ASP time out in minutes.
		/// </summary>
		/// <value>
		/// The session ASP time out in minutes.
		/// </value>
		public double SessionAspTimeOutInMinutes { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [allow forgot password].
		/// </summary>
		/// <value>
		///   <c>true</c> if [allow forgot password]; otherwise, <c>false</c>.
		/// </value>
		public bool AllowForgotPassword { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [EnableGoogleAnalytics].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [EnableGoogleAnalytics]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableGoogleAnalytics { get; set; }

        /// <summary>
        /// Gets or sets a value of [GoogleAnalyticsAccountId].
        /// </summary>
        /// <value>
        ///   <c>account id</c> 
        /// </value>
        public string GoogleAnalyticsAccountId { get; set; }
	}
}

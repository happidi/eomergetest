﻿using System.Configuration;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common.Configuration {

	/// <summary>
	/// Configuration Script Element.
	/// </summary>
	public class ScriptElement : ConfigurationElement {
		private const string const_attribute_extjs = "extjs";
		private const string const_attribute_vizjs = "vizjs";

		/// <summary>
		/// Gets or sets the extjs.
		/// </summary>
		/// <value>
		/// The extjs.
		/// </value>
		[ConfigurationProperty(const_attribute_extjs, IsRequired = true)]
		public ExtjsVersion Extjs {
			get {
				return (ExtjsVersion)this[const_attribute_extjs];
			}
			set {
				this[const_attribute_extjs] = value;
			}
		}

		/// <summary>
		/// Gets or sets the vizjs.
		/// </summary>
		/// <value>
		/// The vizjs.
		/// </value>
		[ConfigurationProperty(const_attribute_vizjs, IsRequired = true)]
		public VizjsVersion Vizjs {
			get {
				return (VizjsVersion)this[const_attribute_vizjs];
			}
			set {
				this[const_attribute_vizjs] = value;
			}
		}
	}
}

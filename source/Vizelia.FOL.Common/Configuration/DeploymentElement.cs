﻿using System;
using System.Configuration;
using System.IO;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common.Configuration {

	/// <summary>
	/// Describes deployment information such as the hostname and defaultport to use.
	/// </summary>
	public class DeploymentElement : ConfigurationElement {

		private const string const_attribute_frontendurl = "frontendUrl";
		private const string const_attribute_admintenantemail = "adminTenantEmail";
		private const string const_attribute_applicationname = "applicationName";
		private const string const_attribute_tempdirectory = "tempDirectory";
		private const string const_attribute_dotnetchartinglicence = "dotNetChartingLicence";
		private const string const_attribute_vizelialicence = "VizeliaLicence";
		private const string const_attribute_usepolicyinjection = "usePolicyInjection";
		private const string const_attribute_meter_start_year = "meterDataStartYear";
		private const string const_attribute_meter_data_end_year_offset_from_current_date = "meterDataEndYearOffsetFromCurrentDate";
		private const string const_attribute_obtuseLoginResponses = "obtuseLoginResponses";
		private const string const_attribute_protocol= "protocol";
		private const string const_attribute_helpurl = "helpUrl";
		private const string const_attribute_rawdatausedinmetergridbydefault = "rawDataUsedInMeterGridByDefault";
		private const string const_attribute_defaultmeterdatavalidity = "defaultMeterDataValidity";
	    private const string const_attribute_fixedcurrenttime = "fixedCurrentTime";



		/// <summary>
		/// Gets or sets the default meter data validity.
		/// </summary>
		/// <value>
		/// The default meter data validity.
		/// </value>
		[ConfigurationProperty(const_attribute_defaultmeterdatavalidity, IsRequired = false, DefaultValue = MeterDataValidity.Valid)]
		public MeterDataValidity DefaultMeterDataValidity {
			get {
				MeterDataValidity meterDataValidity;
				var parsed = MeterDataValidity.TryParse(this[const_attribute_defaultmeterdatavalidity].ToString(),out meterDataValidity);
				if (parsed)
					return meterDataValidity;
				else {
					return MeterDataValidity.Valid;
				}
			}
			set {
				this[const_attribute_defaultmeterdatavalidity] = value;
			}
		}

        /// <summary>
        /// Gets or sets the time which is treated as "now" for dynamic charts. This is intended to be
        /// used for upgrade validation, so that identical charts can be produced. Use this (rather than
        /// DateTime.Now) if you have a dynamic time range, or are otherwise talking about chart data. Use the 
        /// real DateTime.Now if you need a unique filename or cache key.
		/// This property returns the value from TimeFreezeOffDateTimeValue in case it is not configured or there was a problem reading the configuration.
        /// </summary>
        [ConfigurationProperty(const_attribute_fixedcurrenttime, IsRequired = false)]
        public DateTime DateTimeNow {
	        get {
				// For some reason, this[const_attribute_fixedcurrenttime] sometimes produces an exception 
                // in System.Configuration.ConfigurationElement if the attribute doesn't exist. This try
                // block detects that situation and returns the real now, as expected.
                try {
                    var date = (DateTime)this[const_attribute_fixedcurrenttime];

                    // Sometimes if the element isn't there, we get the default value of DateTime, which
                    // is DateTime.MinValue.
	                if (date == DateTime.MinValue) {
		                return TimeFreezeOffDateTimeValue;
	                }

	                DateTime dateTimeNow = DateTime.SpecifyKind(date, DateTimeKind.Local);
                    return dateTimeNow;

                }
                catch (Exception) {
					return TimeFreezeOffDateTimeValue;
                }
            }
	        set {
	            this[const_attribute_fixedcurrenttime] = DateTime.SpecifyKind(value, DateTimeKind.Local);
	        }
	    }

		/// <summary>
		/// Gets the DateTimeNow property value indicating that the time freeze feature is off.
		/// </summary>
		public DateTime TimeFreezeOffDateTimeValue {
			get { return DateTime.MinValue; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [raw data used in meter grid by default].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [raw data used in meter grid by default]; otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_rawdatausedinmetergridbydefault, IsRequired = false, DefaultValue = false)]
		public bool RawDataUsedInMeterGridByDefault {
			get {
				return (bool)this[const_attribute_rawdatausedinmetergridbydefault];
			}
			set {
				this[const_attribute_rawdatausedinmetergridbydefault] = value;
			}
		}

		/// <summary>
		/// Gets or sets protocol
		/// </summary>
		/// <value>
		/// The frontend URL.
		/// </value>
		[ConfigurationProperty(const_attribute_helpurl, IsRequired = false)]
		public string HelpURL {
			get {
				return (string)this[const_attribute_helpurl];
			}
			set {
				this[const_attribute_helpurl] = value;
			}
		}


		/// <summary>
		/// Gets or sets protocol
		/// </summary>
		/// <value>
		/// The frontend URL.
		/// </value>
		[ConfigurationProperty(const_attribute_protocol, IsRequired = false)]
		public string Protocol {
			get {
				return (string)this[const_attribute_protocol];
			}
			set {
				this[const_attribute_protocol] = value;
			}
		}




		/// <summary>
		/// Gets or sets the frontend URL.
		/// </summary>
		/// <value>
		/// The frontend URL.
		/// </value>
		[ConfigurationProperty(const_attribute_frontendurl, IsRequired = false)]
		public string FrontendUrl {
			get {
				return (string)this[const_attribute_frontendurl];
			}
			set {
				this[const_attribute_frontendurl] = value;
			}
		}



		/// <summary>
		/// Gets or sets the meter start year.
		/// </summary>
		/// <value>
		/// The meter start year.
		/// </value>
		[ConfigurationProperty(const_attribute_meter_start_year, IsRequired = false, DefaultValue = 1990)]
		public int MeterStartYear {
			get {
				return (int)this[const_attribute_meter_start_year];
			}
			set {
				this[const_attribute_meter_start_year] = value;
			}
		}


		/// <summary>
		/// Gets or sets the meter data end year offset.
		/// </summary>
		/// <value>
		/// The meter data end year offset.
		/// </value>
		[ConfigurationProperty(const_attribute_meter_data_end_year_offset_from_current_date, IsRequired = false, DefaultValue = 3)]
		public int MeterDataEndYearOffsetFromCurrentDate {
			get {
				return (int)this[const_attribute_meter_data_end_year_offset_from_current_date];
			}
			set {
				this[const_attribute_meter_data_end_year_offset_from_current_date] = value;
			}
		}


		
		/// <summary>
		/// Gets or sets the frontend URL.
		/// </summary>
		/// <value>
		/// The frontend URL.
		/// </value>
		[ConfigurationProperty(const_attribute_usepolicyinjection, IsRequired = false, DefaultValue = true)]
		public bool UsePolicyInjection {
			get {
				return (bool)this[const_attribute_usepolicyinjection];
			}
			set {
				this[const_attribute_usepolicyinjection] = value;
			}
		}

		

		/// <summary>
		/// Gets or sets if all login responses should be the same and uninformative
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [obtuse login responses]; otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_obtuseLoginResponses, IsRequired = false, DefaultValue=true)]
		public bool ObtuseLoginResponses {
			get {
				return (bool)this[const_attribute_obtuseLoginResponses];
			}
			set {
				this[const_attribute_obtuseLoginResponses] = value;
			}
		}


		/// <summary>
		/// Gets or sets the admin tenant email.
		/// </summary>
		/// <value>
		/// The admin tenant email.
		/// </value>
		[ConfigurationProperty(const_attribute_admintenantemail, IsRequired = false)]
		public string AdminTenantEmail {
			get {
				return (string)this[const_attribute_admintenantemail];
			}
			set {
				this[const_attribute_admintenantemail] = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of the application.
		/// </summary>
		/// <value>
		/// The name of the application.
		/// </value>
		[ConfigurationProperty(const_attribute_applicationname, IsRequired = false)]
		public string ApplicationName {
			get {
				return (string)this[const_attribute_applicationname];
			}
			set {
				this[const_attribute_applicationname] = value;
			}
		}

		/// <summary>
		/// Gets or sets the temp directory.
		/// </summary>
		/// <value>
		/// The temp directory.
		/// </value>
		[ConfigurationProperty(const_attribute_tempdirectory, IsRequired = false)]
		public string TempDirectory {
			get {
				return (string)this[const_attribute_tempdirectory];
			}
			set {
				if (!Directory.Exists(value))
					throw new Exception(const_attribute_tempdirectory + " does not exist");
				this[const_attribute_tempdirectory] = Path.GetFullPath(value);
			}
		}

		/// <summary>
		/// Gets or sets the licence file for dotnet charting.
		/// </summary>
		[ConfigurationProperty(const_attribute_dotnetchartinglicence, IsRequired = false)]
		public string DotNetChartingLicence {
			get {
				return (string)this[const_attribute_dotnetchartinglicence];
			}
			set {
				if (!File.Exists(value))
					throw new Exception(const_attribute_dotnetchartinglicence + " does not exist");
				this[const_attribute_dotnetchartinglicence] = value;
			}
		}


		/// <summary>
		/// Gets or sets the licence file for the application.
		/// </summary>
		[ConfigurationProperty(const_attribute_vizelialicence, IsRequired = false)]
		public string VizeliaLicence
		{
			get { return (string)this[const_attribute_vizelialicence]; }
			set
			{
				if (!File.Exists(value))
					throw new Exception(const_attribute_vizelialicence + " does not exist");
				this[const_attribute_vizelialicence] = value;
			}
		}

	}
}

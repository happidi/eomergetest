﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// Represents a configuration element for ApiCorsAllowedUrlsElement.
	/// </summary>
	public class ApiCorsAllowedUrlsElement : ConfigurationElement {
		private const string const_site_url = "siteUrl";

		/// <summary>
		/// Gets or sets the site url.
		/// </summary>
		/// <value>
		/// The site's url.
		/// </value>
		[ConfigurationProperty(const_site_url, IsRequired = false)]
		public string SiteUrl {
			get {
				return (string)this[const_site_url];
			}
			set {
				this[const_site_url] = value;
			}
		}
	}
}
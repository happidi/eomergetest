﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// Configuration Authentication Provider Element.
	/// </summary>
	public class AuthenticationProviderElement : ConfigurationElement {
		private const string const_attribute_name = "name";
		private const string const_attribute_msg = "msg";

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[ConfigurationProperty(const_attribute_name, IsRequired = true)]
		public string Name {
			get {
				return (string)this[const_attribute_name];
			}
			set {
				this[const_attribute_name] = value;
			}
		}

		/// <summary>
		/// Gets or sets the MSG.
		/// </summary>
		/// <value>
		/// The MSG.
		/// </value>
		[ConfigurationProperty(const_attribute_msg, IsRequired = true)]
		public string Msg {
			get {
				return (string)this[const_attribute_msg];
			}
			set {
				this[const_attribute_msg] = value;
			}
		}

	}
}

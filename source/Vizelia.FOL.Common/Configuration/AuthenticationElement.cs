﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {

	/// <summary>
	/// Configuration Authentication Element.
	/// </summary>
	public class AuthenticationElement : ConfigurationElement {
		private const string const_attribute_defaultprovider = "defaultProvider";
		private const string const_attribute_providers = "providers";
		private const string const_attribute_disable_permissions = "disablePermissions";
		private const string const_attribute_scheduler_username = "schedulerUsername";
		private const string const_attribute_scheduler_password = "schedulerPassword";
		private const string const_attribute_printscreen_username = "printscreenUsername";
		private const string const_attribute_allow_remember_me = "allowRememberMe";
		private const string const_attribute_allow_forgot_password = "allowForgotPassword";
		private const string const_attribute_allow_login_as_user = "allowLoginAsUser";
		private const string const_attribute_allow_auto_complete_on_login = "allowAutoCompleteOnLogin";



		/// <summary>
		/// Gets or sets the default provider.
		/// </summary>
		[ConfigurationProperty(const_attribute_defaultprovider, IsRequired = true)]
		public string DefaultProvider {
			get {
				return (string)this[const_attribute_defaultprovider];
			}
			set {
				this[const_attribute_defaultprovider] = value;
			}
		}


		/// <summary>
		/// Gets or sets a value indicating wheither to enforce permissions.
		/// </summary>
		/// <value>
		///   <c>true</c> if enforce permissions otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_disable_permissions, IsRequired = false, DefaultValue = false)]
		public bool DisablePermissions {
			get {
				return (bool)this[const_attribute_disable_permissions];
			}
			set {
				this[const_attribute_disable_permissions] = value;
			}
		}


		/// <summary>
		/// Gets or sets a value indicating whether to allow the remember me checkbox on login.
		/// </summary>
		/// <value>
		///   <c>true</c> if [allow remember me]; otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_allow_remember_me, IsRequired = false, DefaultValue = false)]
		public bool AllowRememberMe {
			get {
				return (bool)this[const_attribute_allow_remember_me];
			}
			set {
				this[const_attribute_allow_remember_me] = value;
			}
		}


		/// <summary>
		/// Gets or sets a value indicating whether [allow forgot password].
		/// </summary>
		/// <value>
		///   <c>true</c> if [allow forgot password]; otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_allow_forgot_password, IsRequired = false, DefaultValue = false)]
		public bool AllowForgotPassword {
			get {
				return (bool)this[const_attribute_allow_forgot_password];
			}
			set {
				this[const_attribute_allow_forgot_password] = value;
			}
		}


		/// <summary>
		/// Gets or sets a value indicating whether [allow login as user].
		/// </summary>
		/// <value>
		///   <c>true</c> if [allow login as user]; otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_allow_login_as_user, IsRequired = false, DefaultValue = false)]
		public bool AllowLoginAsUser {
			get {
				return (bool)this[const_attribute_allow_login_as_user];
			}
			set {
				this[const_attribute_allow_login_as_user] = value;
			}
		}


		/// <summary>
		/// Gets or sets a value indicating whether to allow the login details to be autocompleted
		/// </summary>
		/// <value>
		///   <c>true</c> if allowed; otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_allow_auto_complete_on_login, IsRequired = false, DefaultValue = true)]
		public bool AllowAutoCompleteOnLogin {
			get {
				return ((bool)this[const_attribute_allow_auto_complete_on_login]);
				//var boolAllow = ((bool) this[const_attribute_allow_auto_complete_on_login]);
				//return boolAllow ? "on" : "off";
			}
			set {
				this[const_attribute_allow_auto_complete_on_login] = value;
			}
		}

		/// <summary>
		/// Gets the providers.
		/// </summary>
		[ConfigurationProperty(const_attribute_providers, IsRequired = true)]
		public AuthenticationProviderElementCollection Providers {
			get {
				return (AuthenticationProviderElementCollection)base[const_attribute_providers];
			}
		}



		/// <summary>
		/// Gets or sets the scheduler username.
		/// </summary>
		/// <value>
		/// The scheduler username.
		/// </value>
		[ConfigurationProperty(const_attribute_scheduler_username, IsRequired = true)]
		public string SchedulerUsername {
			get {
				return (string)this[const_attribute_scheduler_username];
			}
			set {
				this[const_attribute_scheduler_username] = value;
			}

		}


		/// <summary>
		/// Gets or sets the scheduler password.
		/// </summary>
		/// <value>
		/// The scheduler password.
		/// </value>
		[ConfigurationProperty(const_attribute_scheduler_password, IsRequired = false)]
		public string SchedulerPassword {
			get {
				return (string)this[const_attribute_scheduler_password];
			}
			set {
				this[const_attribute_scheduler_password] = value;
			}
		}



		/// <summary>
		/// Gets or sets the printscreen username.
		/// </summary>
		[ConfigurationProperty(const_attribute_printscreen_username, IsRequired = false)]
		public string PrintscreenUsername {
			get {
				return (string)this[const_attribute_printscreen_username];
			}
			set {
				this[const_attribute_printscreen_username] = value;
			}

		}
	}
}

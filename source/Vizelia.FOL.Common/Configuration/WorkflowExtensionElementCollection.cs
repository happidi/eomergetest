﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {

	/// <summary>
	/// Represents a configuration element containing a collection of WorkflowExtensionElement.
	/// </summary>
	[ConfigurationCollection(typeof(WorkflowExtensionElement))]
	public class WorkflowExtensionElementCollection : ConfigurationElementCollection {

		/// <summary>
		/// Gets or sets the <see cref="WorkflowExtensionElement"/> at the specified index.
		/// </summary>
		/// <value></value>
		public WorkflowExtensionElement this[int index] {
			get {
				return base.BaseGet(index) as WorkflowExtensionElement;
			}
			set {
				if (base.BaseGet(index) != null) {
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}

		/// <summary>
		/// Gets the <see cref="WorkflowExtensionElement"/> with the specified key.
		/// </summary>
		/// <value></value>
		public new WorkflowExtensionElement this[string key] {
			get {
				return (WorkflowExtensionElement)base.BaseGet(key);
			}
		}

		/// <summary>
		/// When overridden in a derived class, creates <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </summary>
		/// <returns>
		/// New <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </returns>
		protected override ConfigurationElement CreateNewElement() {
			return new WorkflowExtensionElement();
		}

		/// <summary>
		/// Gets the element key for a specified configuration element when overridden in a derived class.
		/// </summary>
		/// <param name="element"><see cref="T:System.Configuration.ConfigurationElement"/> for which to return the key.</param>
		/// <returns>
		/// A <see cref="T:System.Object"/> that acts as key for the <see cref="T:System.Configuration.ConfigurationElement"/> specified.
		/// </returns>
		protected override object GetElementKey(ConfigurationElement element) {
			return ((WorkflowExtensionElement)element).Type;
		}

	}
}

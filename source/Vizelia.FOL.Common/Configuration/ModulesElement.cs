﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {

	/// <summary>
	/// Configuration Modules Element.
	/// </summary>
	public class ModulesElement : ConfigurationElement {
		private const string const_attribute_max_concurrent_users = "max_concurrent_users";
		private const string const_element_servicedesk = "servicedesk";

		/// <summary>
		/// Gets or sets the max concurrent users.
		/// </summary>
		/// <value>
		/// The max concurrent users.
		/// </value>
		[ConfigurationProperty(const_attribute_max_concurrent_users , DefaultValue=25)]
		public int MaxConcurrentUsers {
			get {
				return (int)this[const_attribute_max_concurrent_users];
			}
			set {
				this[const_attribute_max_concurrent_users] = value;
			}
		}

		/// <summary>
		/// Gets or sets the service desk.
		/// </summary>
		/// <value>
		/// The service desk.
		/// </value>
		[ConfigurationProperty(const_element_servicedesk)]
		public ServiceDeskElement ServiceDesk {
			get {
				return (ServiceDeskElement)this[const_element_servicedesk];
			}
			set {
				this[const_element_servicedesk] = value;
			}
		}

	}
}

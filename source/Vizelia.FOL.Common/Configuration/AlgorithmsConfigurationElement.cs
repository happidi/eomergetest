using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// The configuration element for algorithms.
	/// </summary>
	public class AlgorithmsConfigurationElement : ConfigurationElement {
		private const string const_strong_name_attribute = "publicKeyToken";

		/// <summary>
		/// Gets or sets the strong name.
		/// </summary>
		/// <value>
		/// The strong name.
		/// </value>
		[ConfigurationProperty(const_strong_name_attribute, IsRequired = false)]
		public string StrongName {
			get {
				return (string)this[const_strong_name_attribute];
			}
			set {
				this[const_strong_name_attribute] = value;
			}
		}

	}
}
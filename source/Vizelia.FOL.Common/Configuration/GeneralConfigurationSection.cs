﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {

	/// <summary>
	/// This classes is used as base class for custom section
	/// It contains an xmlns attribute to attach an xsd file so we don't have to do that on each new custom section
	/// </summary>
	public class GeneralConfigurationSection : ConfigurationSection {
		
		private const string const_attribute_xmlns = "xmlns";

		/// <summary>
		/// Gets or sets the XMLNS.
		/// </summary>
		/// <value>
		/// The XMLNS.
		/// </value>
		[ConfigurationProperty(const_attribute_xmlns)]
		public string Xmlns {
			get {
				return (string)this[const_attribute_xmlns];
			}
			set {
				this[const_attribute_xmlns] = value;
			}
		}
	}
}

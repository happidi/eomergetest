using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// Represents a configuration element containing a collection of LoggingCategoryElement.
	/// </summary>
	[ConfigurationCollection(typeof(LoggingCategoryElement))]
	public class LoggingCategoryElementCollection : ConfigurationElementCollection {
		/// <summary>
		/// Gets or sets the <see cref="LoggingCategoryElement"/> at the specified index.
		/// </summary>
		/// <value></value>
		public LoggingCategoryElement this[int index] {
			get {
				return BaseGet(index) as LoggingCategoryElement;
			}
			set {
				if (BaseGet(index) != null) {
					BaseRemoveAt(index);
				}
				BaseAdd(index, value);
			}
		}

		/// <summary>
		/// Gets the <see cref="LoggingCategoryElement"/> with the specified key.
		/// </summary>
		/// <value></value>
		public new LoggingCategoryElement this[string key] {
			get {
				return (LoggingCategoryElement)BaseGet(key);
			}
		}

		/// <summary>
		/// When overridden in a derived class, creates <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </summary>
		/// <returns>
		/// New <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </returns>
		protected override ConfigurationElement CreateNewElement() {
			return new LoggingCategoryElement();
		}

		/// <summary>
		/// Gets the element key for a specified configuration element when overridden in a derived class.
		/// </summary>
		/// <param name="element"><see cref="T:System.Configuration.ConfigurationElement"/> for which to return the key.</param>
		/// <returns>
		/// A <see cref="T:System.Object"/> that acts as key for the <see cref="T:System.Configuration.ConfigurationElement"/> specified.
		/// </returns>
		protected override object GetElementKey(ConfigurationElement element) {
			return ((LoggingCategoryElement)element).SourceCategory;
		}
	}
}
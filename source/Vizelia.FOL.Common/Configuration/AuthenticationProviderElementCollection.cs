﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {

	/// <summary>
	/// Configuration Authentication Provider Element Collection.
	/// </summary>
	[ConfigurationCollection(typeof(AuthenticationProviderElement))]
	public class AuthenticationProviderElementCollection : ConfigurationElementCollection {

		/// <summary>
		/// Gets or sets a property, attribute, or child element of this configuration element.
		/// </summary>
		/// <returns>The specified property, attribute, or child element</returns>
		public AuthenticationProviderElement this[int index] {
			get {
				return base.BaseGet(index) as AuthenticationProviderElement;
			}
			set {
				if (base.BaseGet(index) != null) {
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}

		/// <summary>
		/// Gets or sets a property, attribute, or child element of this configuration element.
		/// </summary>
		/// <returns>The specified property, attribute, or child element</returns>
		public new AuthenticationProviderElement this[string key] {
			get {
				return (AuthenticationProviderElement)base.BaseGet(key);
			}
		}

		/// <summary>
		/// When overridden in a derived class, creates a new <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </summary>
		/// <returns>
		/// A new <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </returns>
		protected override ConfigurationElement CreateNewElement() {
			return new AuthenticationProviderElement();
		}

		/// <summary>
		/// Gets the element key for a specified configuration element when overridden in a derived class.
		/// </summary>
		/// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement"/> to return the key for.</param>
		/// <returns>
		/// An <see cref="T:System.Object"/> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </returns>
		protected override object GetElementKey(ConfigurationElement element) {
			return ((AuthenticationProviderElement)element).Name;
		}

	}
}

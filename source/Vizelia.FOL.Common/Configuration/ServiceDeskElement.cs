﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {

	/// <summary>
	/// Spatial level.
	/// </summary>
	public enum SpatialLevel {
		/// <summary>
		/// None
		/// </summary>
		None,

		/// <summary>
		/// Site
		/// </summary>
		IfcSite,

		/// <summary>
		/// Building
		/// </summary>
		IfcBuilding,

		/// <summary>
		/// Building Storey
		/// </summary>
		IfcBuildingStorey,

		/// <summary>
		/// Space
		/// </summary>
		IfcSpace
	}

	/// <summary>
	/// Configuration Service Desk Element.
	/// </summary>
	public class ServiceDeskElement : ModuleElement {

		private const string const_attribute_spatialLevelRequest = "spatialLevelRequest";

		/// <summary>
		/// Gets or sets the spatial level request.
		/// </summary>
		/// <value>
		/// The spatial level request.
		/// </value>
		[ConfigurationProperty(const_attribute_spatialLevelRequest, DefaultValue = SpatialLevel.IfcBuilding)]
		public SpatialLevel SpatialLevelRequest {
			get {
				return (SpatialLevel)this[const_attribute_spatialLevelRequest];
			}
			set {
				this[const_attribute_spatialLevelRequest] = value;
			}
		}
	}
}

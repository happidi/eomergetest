﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// Represents a configuration element containing a collection of NSoupConfigurationElement.
	/// </summary>
	[ConfigurationCollection(typeof(NSoupConfigurationElement))]
	public class NSoupConfigurationElementCollection : ConfigurationElementCollection {

		private const string const_preconfigured_whitelist = "preConfiguredWhiteList";

		/// <summary>
		/// Gets the pre configured white list.
		/// </summary>
		[ConfigurationProperty(const_preconfigured_whitelist, IsRequired = false)]
		public string PreConfiguredWhiteList {
			get {
				return (string)base[const_preconfigured_whitelist];
			}
		}

	    /// <summary>
		/// Gets or sets the <see cref="NSoupConfigurationElement"/> at the specified index.
		/// </summary>
		/// <value></value>
		public NSoupConfigurationElement this[int index] {
			get {
				return base.BaseGet(index) as NSoupConfigurationElement;
			}
			set {
				if (base.BaseGet(index) != null) {
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}

		/// <summary>
		/// Gets the <see cref="NSoupConfigurationElement"/> with the specified key.
		/// </summary>
		/// <value></value>
		public new NSoupConfigurationElement this[string key] {
			get {
				return (NSoupConfigurationElement)base.BaseGet(key);
			}
		}

		/// <summary>
		/// When overridden in a derived class, creates <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </summary>
		/// <returns>
		/// New <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </returns>
		protected override ConfigurationElement CreateNewElement() {
			return new NSoupConfigurationElement();
		}

		/// <summary>
		/// Gets the element key for a specified configuration element when overridden in a derived class.
		/// </summary>
		/// <param name="element"><see cref="T:System.Configuration.ConfigurationElement"/> for which to return the key.</param>
		/// <returns>
		/// A <see cref="T:System.Object"/> that acts as key for the <see cref="T:System.Configuration.ConfigurationElement"/> specified.
		/// </returns>
		protected override object GetElementKey(ConfigurationElement element) {
			return ((NSoupConfigurationElement)element).TagName;
		}
	}
}

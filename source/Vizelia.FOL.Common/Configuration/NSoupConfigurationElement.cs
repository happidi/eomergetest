﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// Represents a configuration element for a NSoup configuration.
	/// </summary>
	public class NSoupConfigurationElement : ConfigurationElement {
		private const string const_tag_name = "tagName";
		private const string const_attributes = "attributes";


		/// <summary>
		/// Gets or sets the tag name.
		/// </summary>
		/// <value>
		/// The tag name.
		/// </value>
		[ConfigurationProperty(const_tag_name, IsRequired = false)]
		public string TagName {
			get {
				return (string)this[const_tag_name];
			}
			set {
				this[const_tag_name] = value;
			}
		}

		/// <summary>
		/// Gets or sets the attributes.
		/// </summary>
		/// <value>
		/// The attributes.
		/// </value>
		[ConfigurationProperty(const_attributes, IsRequired = false)]
		public string Attributes {
			get {
				return (string)this[const_attributes];
			}
			set {
				this[const_attributes] = value;
			}
		}
	}
}

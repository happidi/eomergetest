using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// The configuration element collection for algorithms.
	/// </summary>
	[ConfigurationCollection(typeof(AlgorithmsConfigurationElement))]
	public class AlgorithmsConfigurationElementCollection : ConfigurationElementCollection {
		private const string const_attribute_run_algorithms_in_sandbox = "runAlgorithmsInSandbox";
		private const string const_attribute_validate_strong_name = "validatePublicKey";
		private const string const_attribute_allow_editable_source = "allowEditableSource";
		private const string const_attribute_timeout = "timeoutInSeconds";
		private const int const_default_timeout = 3 * 60;
		

		/// <summary>
		/// Gets or sets a value indicating whether we should allow editable source for algorithms.
		/// </summary>
		/// <value>
		///   <c>true</c> if we should allow editable source for algorithms; otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_allow_editable_source, IsRequired = false, DefaultValue = false)]
		public bool AllowEditableSource {
			get {
				var o = base[const_attribute_run_algorithms_in_sandbox];
				bool allow = GetBooleanValue(o, false);

				return allow;
			}
			set {
				base[const_attribute_allow_editable_source] = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether we should run algorithms in sandbox.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if we should run algorithms in sandbox; otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_run_algorithms_in_sandbox, IsRequired = false)]
		public bool ShouldRunAlgorithmsInSandbox {
			get {
				var o = base[const_attribute_run_algorithms_in_sandbox];
				bool should = GetBooleanValue(o, true);

				return should;
			}
			set {
				base[const_attribute_run_algorithms_in_sandbox] = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether we should validate strong name.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if we should validate strong name; otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_validate_strong_name, IsRequired = false)]
		public bool ShouldValidateStrongName {
			get {
				var o = base[const_attribute_validate_strong_name];
				bool should = GetBooleanValue(o, false);

				return should;
			}
			set {
				base[const_attribute_validate_strong_name] = value;
			}
		}

		/// <summary>
		/// Gets or sets the timeout in seconds.
		/// </summary>
		/// <value>
		/// The timeout in seconds.
		/// </value>
		[ConfigurationProperty(const_attribute_timeout, IsRequired = false)]
		public int TimeoutInSeconds {
			get {
				var o = base[const_attribute_timeout];

				int timeout = const_default_timeout; 

				if(o is int) {
					timeout = (int) o;
				}
				else if(o is string) {
					int.TryParse((string) o, out timeout);
				}

				if(timeout <= 0) {
					// We need time to run!
					timeout = const_default_timeout;
				}

				return timeout;
			}
			set {
				base[const_attribute_timeout] = value;
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="AlgorithmsConfigurationElement"/> at the specified index.
		/// </summary>
		/// <value></value>
		public AlgorithmsConfigurationElement this[int index] {
			get {
				return base.BaseGet(index) as AlgorithmsConfigurationElement;
			}
			set {
				if (base.BaseGet(index) != null) {
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}

		/// <summary>
		/// Gets the <see cref="AlgorithmsConfigurationElement"/> with the specified key.
		/// </summary>
		/// <value></value>
		public new AlgorithmsConfigurationElement this[string key] {
			get {
				return (AlgorithmsConfigurationElement)base.BaseGet(key);
			}
		}

		/// <summary>
		/// When overridden in a derived class, creates <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </summary>
		/// <returns>
		/// New <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </returns>
		protected override ConfigurationElement CreateNewElement() {
			return new AlgorithmsConfigurationElement();
		}

		/// <summary>
		/// Gets the element key for a specified configuration element when overridden in a derived class.
		/// </summary>
		/// <param name="element"><see cref="T:System.Configuration.ConfigurationElement"/> for which to return the key.</param>
		/// <returns>
		/// A <see cref="T:System.Object"/> that acts as key for the <see cref="T:System.Configuration.ConfigurationElement"/> specified.
		/// </returns>
		protected override object GetElementKey(ConfigurationElement element) {
			return ((AlgorithmsConfigurationElement)element).StrongName;
		}

		/// <summary>
		/// Gets the boolean value. If not present, the default value is returned.
		/// </summary>
		/// <param name="o">The o.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		private static bool GetBooleanValue(object o, bool defaultValue) {
			bool should;

			if (o is bool) {
				should = (bool)o;
			}
			else {
				should = defaultValue;
			}

			return should;
		}

	}
}
using System.Configuration;
using System.IO;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// The vizelia configuration element of mapping.
	/// Used to configure the mapping business layer and to define parameters to providers.
	/// </summary>
	public class MappingElement : ConfigurationElement {
		private const string const_attribute_days_to_keep_processed_files = "daysToKeepProcessedFiles";
		private const string const_attribute_work_directory = "workDirectory";
		private const string const_attribute_skip_file_on_error = "skipFileOnError";
		private const string const_attribute_processed_files_relative_path = "processedFilesRelativePath";
		private const string const_attribute_import_meterdata_batch_size = "importMeterDataBatchSize";
		private const string const_attribute_log_success_messages = "logSuccessMessages";
		private const string const_attribute_file_source_fetching_order = "fetchingOrder";

		/// <summary>
		/// Gets or sets the size of the import meter data batch.
		/// </summary>
		/// <value>
		/// The size of the import meter data batch.
		/// </value>
		[ConfigurationProperty(const_attribute_import_meterdata_batch_size, IsRequired = false, DefaultValue = "1000")]
		public int ImportMeterDataBatchSize {
			get {
				int size = (int)this[const_attribute_import_meterdata_batch_size];
				return size;
			}
			set {
				this[const_attribute_import_meterdata_batch_size] = value;
			}
		}

		/// <summary>
		/// Gets or sets the work directory path.
		/// </summary>
		/// <value>
		/// The work directory path.
		/// </value>
		[ConfigurationProperty(const_attribute_work_directory, IsRequired = false, DefaultValue = "")]
		public string WorkDirectoryPath {
			get {
				string assignedPath = (string)this[const_attribute_work_directory];

				if (string.IsNullOrWhiteSpace(assignedPath)) {
					string tempPath = Path.GetTempPath();
					return tempPath;
				}

				return assignedPath;
			}
			set {
				this[const_attribute_work_directory] = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether a bad mapping file should be skipped or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if a bad mapping file should be skipped; otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_skip_file_on_error, IsRequired = false, DefaultValue = true)]
		public bool ShouldSkipFileOnError {
			get {
				bool skip = (bool)this[const_attribute_skip_file_on_error];

				return skip;
			}
			set {
				this[const_attribute_skip_file_on_error] = value;
			}
		}

		/// <summary>
		/// Gets or sets the processed file relative path on the file source.
		/// </summary>
		/// <value>
		/// The quarantine path for bad files.
		/// </value>
		[ConfigurationProperty(const_attribute_processed_files_relative_path, IsRequired = false, DefaultValue = "ProcessedFiles")]
		public string ProcessedFilesRelativePath {
			get {
				string path = (string)this[const_attribute_processed_files_relative_path];

				return path;
			}
			set {
				this[const_attribute_processed_files_relative_path] = value;
			}
		}

		/// <summary>
		/// Gets or sets the days to keep processed files.
		/// </summary>
		/// <value>
		/// The days to keep processed files.
		/// </value>
		[ConfigurationProperty(const_attribute_days_to_keep_processed_files, IsRequired = false, DefaultValue = 7)]
		public int DaysToKeepProcessedFiles {
			get {
				int days = (int)this[const_attribute_days_to_keep_processed_files];
				return days;
			}
			set {
				this[const_attribute_days_to_keep_processed_files] = value;
			}
		}

		/// <summary>
		/// Gets or sets the fetching order of files from the file source.
		/// </summary>
		/// <value>
		/// The fetching order.
		/// </value>
		[ConfigurationProperty(const_attribute_file_source_fetching_order, IsRequired = false, DefaultValue = MappingFetchingOrder.ByTime)]
		public MappingFetchingOrder FetchingOrder {
			get {
				var order = (MappingFetchingOrder)this[const_attribute_file_source_fetching_order];
				return order;
			}
			set {
				this[const_attribute_file_source_fetching_order] = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether [log success messages].
		/// </summary>
		/// <value>
		///   <c>true</c> if [log success messages]; otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_log_success_messages, IsRequired = false, DefaultValue = false)]
		public bool LogSuccessMessages {
			get {
				return (bool)this[const_attribute_log_success_messages];
			}
			set {
				this[const_attribute_log_success_messages] = value;
			}
		}

	}
}
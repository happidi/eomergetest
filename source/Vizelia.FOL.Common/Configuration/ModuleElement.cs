﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {

	/// <summary>
	/// Configuration Module Element.
	/// </summary>
	public class ModuleElement : ConfigurationElement{
		private const string const_attribute_enabled = "enabled";

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="ModuleElement"/> is enabled.
		/// </summary>
		/// <value>
		///   <c>true</c> if enabled; otherwise, <c>false</c>.
		/// </value>
		[ConfigurationProperty(const_attribute_enabled , DefaultValue=true)]
		public bool Enabled {
			get {
				return (bool)this[const_attribute_enabled];
			}
			set {
				this[const_attribute_enabled] = value;
			}
		}
	}

}

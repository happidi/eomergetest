﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {

	/// <summary>
	/// Represents a configuration element for a WorkflowExtension.
	/// </summary>
	public class WorkflowExtensionElement : ConfigurationElement {
		private const string const_attribute_type = "type";


		/// <summary>
		/// Gets or sets the type of the workflow extension.
		/// </summary>
		/// <value>The name.</value>
		[ConfigurationProperty(const_attribute_type, IsRequired = false)]
		public string Type {
			get {
				return (string)this[const_attribute_type];
			}
			set {
				this[const_attribute_type] = value;
			}
		}



	}
}

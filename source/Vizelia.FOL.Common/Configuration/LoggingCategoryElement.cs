using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// Represents a configuration element for a LoggingCategory.
	/// </summary>
	public class LoggingCategoryElement : ConfigurationElement {
		private const string const_attribute_source_category = "source";
		private const string const_attribute_destination_category = "destination";

		/// <summary>
		/// Gets or sets the source category.
		/// </summary>
		/// <value>The name.</value>
		[ConfigurationProperty(const_attribute_source_category, IsRequired = true)]
		public string SourceCategory {
			get {
				return (string)this[const_attribute_source_category];
			}
			set {
				this[const_attribute_source_category] = value;
			}
		}

		/// <summary>
		/// Gets or sets the Destination category.
		/// </summary>
		/// <value>The name.</value>
		[ConfigurationProperty(const_attribute_destination_category, IsRequired = true)]
		public string DestinationCategory {
			get {
				return (string)this[const_attribute_destination_category];
			}
			set {
				this[const_attribute_destination_category] = value;
			}
		}

	}
}
﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// Represents a configuration element containing a collection of ClassificationToDataModelTypeMappingElement.
	/// </summary>
	[ConfigurationCollection(typeof(ApiCorsAllowedUrlsElement))]
	public class ApiCorsAllowedUrlsElementCollection : ConfigurationElementCollection {
		/// <summary>
		/// Gets or sets the <see cref="ApiCorsAllowedUrlsElement"/> at the specified index.
		/// </summary>
		/// <value></value>
		public ApiCorsAllowedUrlsElement this[int index] {
			get {
				return base.BaseGet(index) as ApiCorsAllowedUrlsElement;
			}
			set {
				if (base.BaseGet(index) != null) {
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}

		/// <summary>
		/// Gets the <see cref="ApiCorsAllowedUrlsElement"/> with the specified key.
		/// </summary>
		/// <value></value>
		public new ApiCorsAllowedUrlsElement this[string key] {
			get {
				return (ApiCorsAllowedUrlsElement)base.BaseGet(key);
			}
		}

		/// <summary>
		/// When overridden in a derived class, creates <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </summary>
		/// <returns>
		/// New <see cref="T:System.Configuration.ConfigurationElement"/>.
		/// </returns>
		protected override ConfigurationElement CreateNewElement() {
			return new ApiCorsAllowedUrlsElement();
		}

		/// <summary>
		/// Gets the element key for a specified configuration element when overridden in a derived class.
		/// </summary>
		/// <param name="element"><see cref="T:System.Configuration.ConfigurationElement"/> for which to return the key.</param>
		/// <returns>
		/// A <see cref="T:System.Object"/> that acts as key for the <see cref="T:System.Configuration.ConfigurationElement"/> specified.
		/// </returns>
		protected override object GetElementKey(ConfigurationElement element) {
			return ((ApiCorsAllowedUrlsElement)element).SiteUrl;
		}
	}
}
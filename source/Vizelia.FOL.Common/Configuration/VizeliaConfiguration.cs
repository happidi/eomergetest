﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {

	/// <summary>
	/// Gives access to the Vizelia xml node in web.config
	/// Singleton
	/// </summary>
	public sealed class VizeliaConfiguration : GeneralConfigurationSection {
		private const string const_element_vizelia = "VizeliaConfiguration";
		private const string const_element_mail = "mail";
		private const string const_element_modules = "modules";
		private const string const_element_script = "script";
		private const string const_element_authentication = "authentication";
		private const string const_element_classification = "classification";
		private const string const_element_workflowextension = "workflowExtension";
		private const string const_element_classificationtodatamodeltypemapping = "classificationToDataModelTypeMapping";
		private const string const_element_mapping = "mapping";
		private const string const_element_cryptography = "cryptography";
		private const string const_element_logging_categories = "loggingCategories";
		private const string const_element_deployment = "deployment";
		private const string const_element_nsoup_configuration = "nSoupConfiguration";
		private const string const_element_algorithms_configuration = "algorithmsConfiguration";
		private const string const_element_apicorsallowedurls = "apiCorsAllowedUrls";
		private const string const_attribute_days_to_keep_log = "deleteLogsAfterDays";
	    private const string const_element_analytics = "analytics";
		private const int const_default_days_to_keep_log_value = 7;
		private static VizeliaConfiguration _instance;

		private static readonly object padlock = new object();

		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaConfiguration"/> class.
		/// </summary>
		private VizeliaConfiguration() {

		}

		/// <summary>
		/// Singleton accessor
		/// </summary>
		public static VizeliaConfiguration Instance {
			get {
				lock (padlock) {
					if (_instance == null) {
						_instance = (VizeliaConfiguration)ConfigurationManager.GetSection(const_element_vizelia);
						//BuildClassificationHelper();

					}
					return _instance;
				}
			}
		}


		///// <summary>
		///// Builds the ClassificationHelper.
		///// </summary>
		//public static void BuildClassificationHelper() {
		//    List<ClassificationItem> list = new List<ClassificationItem>();
		//    ClassificationItemBrokerDB broker = Helper.CreateInstance<ClassificationItemBrokerDB>();
		//    var enumerator = _instance.Classification.GetEnumerator();
		//    while (enumerator.MoveNext()) {
		//        ClassificationElement el = (ClassificationElement)enumerator.Current;
		//        ClassificationItem item = broker.GetItemByLocalId(el.LocalId);
		//        if (item != null) {
		//            item.Category = el.Name;
		//            if (!String.IsNullOrEmpty(el.IconCls)) {
		//                item.overridedIconCls = el.IconCls;
		//            }
		//            list.Add(item);
		//        }
		//    }
		//    ClassificationHelper.Classifications = list;
		//}

		/// <summary>
		/// Gets or sets the days to keep log files.
		/// </summary>
		/// <value>
		/// The days to keep log files.
		/// </value>
		[ConfigurationProperty(const_attribute_days_to_keep_log, IsRequired = false)]
		public int DaysToKeepLogs {
			get {
				object o = this[const_attribute_days_to_keep_log];

				if (o == null) {
					return const_default_days_to_keep_log_value;
				}

				return (int)o;
			}
			set {
				this[const_attribute_days_to_keep_log] = value;
			}
		}

		/// <summary>
		/// Gets or sets the Authentication element.
		/// </summary>
		/// <value>The authentication.</value>
		[ConfigurationProperty(const_element_authentication, IsRequired = false)]
		public AuthenticationElement Authentication {
			get {
				return (AuthenticationElement)this[const_element_authentication];
			}
			set {
				this[const_element_authentication] = value;
			}
		}


        /// <summary>
        /// Gets or sets the Authentication element.
        /// </summary>
        /// <value>The authentication.</value>
        [ConfigurationProperty(const_element_analytics, IsRequired = false)]
        public AnalyticsElement Analytics
        {
            get
            {
                return (AnalyticsElement)this[const_element_analytics];
            }
            set
            {
                this[const_element_analytics] = value;
            }
        }

		/// <summary>
		/// Gets or sets the Script element.
		/// </summary>
		/// <value>The script.</value>
		[ConfigurationProperty(const_element_script, IsRequired = false)]
		public ScriptElement Script {
			get {
				return (ScriptElement)this[const_element_script];
			}
			set {
				this[const_element_script] = value;
			}
		}

		/// <summary>
		/// Gets or sets the Mail element.
		/// </summary>
		/// <value>The mail.</value>
		[ConfigurationProperty(const_element_mail, IsRequired = false)]
		public MailElement Mail {
			get {
				return (MailElement)this[const_element_mail];
			}
			set {
				this[const_element_mail] = value;
			}
		}

		/// <summary>
		/// Gets or sets the Modules element.
		/// </summary>
		/// <value>The modules.</value>
		[ConfigurationProperty(const_element_modules, IsRequired = false)]
		public ModulesElement Modules {
			get {
				return (ModulesElement)this[const_element_modules];
			}
			set {
				this[const_element_modules] = value;
			}
		}

		
		/// <summary>
		/// Gets the WorkflowExtension element.
		/// </summary>
		/// <value>The WorkflowExtension element.</value>
		[ConfigurationProperty(const_element_workflowextension, IsRequired = false)]
		public WorkflowExtensionElementCollection WorkflowExtension {
			get {
				return (WorkflowExtensionElementCollection)base[const_element_workflowextension];
			}
		}

		/// <summary>
		/// Gets the WorkflowExtension element.
		/// </summary>
		/// <value>The WorkflowExtension element.</value>
		[ConfigurationProperty(const_element_logging_categories, IsRequired = false)]
		public LoggingCategoryElementCollection LoggingCategories {
			get {
				return (LoggingCategoryElementCollection)base[const_element_logging_categories];
			}
		}

		/// <summary>
		/// Gets or sets the Mapping element.
		/// </summary>
		/// <value>The mapping.</value>
		[ConfigurationProperty(const_element_mapping, IsRequired = false)]
		public MappingElement Mapping {
			get {
				return (MappingElement)this[const_element_mapping];
			}
			set {
				this[const_element_mapping] = value;
			}
		}

		/// <summary>
		/// Gets or sets the Mapping element.
		/// </summary>
		/// <value>The mapping.</value>
		[ConfigurationProperty(const_element_cryptography, IsRequired = false)]
		public CryptographyElement Cryptography {
			get {
				return (CryptographyElement)this[const_element_cryptography];
			}
			set {
				this[const_element_cryptography] = value;
			}
		}

		/// <summary>
		/// Gets the Deployment element.
		/// </summary>
		/// <value>The Deployment element.</value>
		[ConfigurationProperty(const_element_deployment, IsRequired = true)]
		public DeploymentElement Deployment {
			get {
				return (DeploymentElement)base[const_element_deployment];
			}
			set {
				this[const_element_deployment] = value;
			}
		}
		
		/// <summary>
		/// Gets the data model type to classification mapping.
		/// </summary>
		[ConfigurationProperty(const_element_classificationtodatamodeltypemapping, IsRequired = false)]
		public ClassificationToDataModelTypeMappingElementCollection ClassificationToDataModelTypeMapping {
			get {
				return (ClassificationToDataModelTypeMappingElementCollection)base[const_element_classificationtodatamodeltypemapping];
			}
		}

		/// <summary>
		/// Gets the NSoupConfiguration WhiteList.
		/// </summary>
		[ConfigurationProperty(const_element_nsoup_configuration, IsRequired = false)]
		public NSoupConfigurationElementCollection NSoupConfiguration {
			get {
				return (NSoupConfigurationElementCollection)base[const_element_nsoup_configuration];
			}
		}

		/// <summary>
		/// Gets the algorithms configuration.
		/// </summary>
		[ConfigurationProperty(const_element_algorithms_configuration, IsRequired = false)]
		public AlgorithmsConfigurationElementCollection AlgorithmsConfiguration {
			get {
				return (AlgorithmsConfigurationElementCollection)base[const_element_algorithms_configuration];
			}
		}

		/// <summary>
		/// Gets the allowed CORS sites configuration.
		/// </summary>
		[ConfigurationProperty(const_element_apicorsallowedurls, IsRequired = false)]
		public ApiCorsAllowedUrlsElementCollection ApiCorsAllowedUrls {
			get {
				return (ApiCorsAllowedUrlsElementCollection)base[const_element_apicorsallowedurls];
			}
		}
	}
}

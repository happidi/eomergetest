﻿To generate the xsd file follow the next steps:

1 - Create a new folder for example: 
		d:\temp\extractor

2 - Copy the following assemblies: 
		Vizelia.FOL.Infrastructure.dll
		Vizelia.FOL.Common.dll
		Vizelia.FOL.Common.Localization.dll
		Vizelia.FOL.Common.Localization.ResourceManager.dll

3 - Copy all thirdparties to the folder too.

4 - Copy the file from D:\TFS\Vizelia\Utilities\XSDExtractor\XSDExtractor.exe

5 - Execute via a msdos console the following command:
		xsdextractor /A Vizelia.FOL.Common.dll

6 - Once XSDExtractor.exe has generated the xsd file, this file should be manually modified so that the element :
		<xs:element name="Vizelia.FOL.Common.Configuration.VizeliaConfiguration" type="tns:Vizelia.FOL.Common.Configuration.VizeliaConfigurationCT" />
	is rename as :
		<xs:element name="VizeliaConfiguration" type="tns:Vizelia.FOL.Common.Configuration.VizeliaConfigurationCT" />
	(Just change the element name, the type stays the same)
	(The element can be anywher in the file)

7 - Then the resulting file should be copied into the xsd folder of the website structure:
		D:\TFS\Vizelia\dev\Vizelia.FOL\source\Vizelia.FOL.Web\xsd


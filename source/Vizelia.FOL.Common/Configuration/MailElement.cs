﻿using System.Configuration;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common.Configuration {

	/// <summary>
	/// Configuration Mail Element.
	/// </summary>
	public class MailElement : ConfigurationElement {
		private const string const_attribute_behavior = "behavior";
		private const string const_attribute_add_cc = "addCC";
		private const string const_attribute_override_to = "overrideTo";
		private const string const_attribute_sendermail = "senderMail";
		private const string const_attribute_sendername = "senderName";
		private const string const_attribute_log_path = "logPath";
		private const string const_attribute_days_to_keep_log = "deleteLogsAfterDays";

		/// <summary>
		/// Gets or sets the log path.
		/// </summary>
		/// <value>
		/// The log path.
		/// </value>
		[ConfigurationProperty(const_attribute_log_path, IsRequired = false)]
		public string LogPath {
			get {
				return (string)this[const_attribute_log_path];
			}
			set {
				this[const_attribute_log_path] = value;
			}
		}

		/// <summary>
		/// Gets or sets the days to keep log files.
		/// </summary>
		/// <value>
		/// The days to keep log files.
		/// </value>
		[ConfigurationProperty(const_attribute_days_to_keep_log, IsRequired = false)]
		public int DaysToKeepLogFiles {
			get {
				object o = this[const_attribute_days_to_keep_log];
				
				if(o == null) {
					return int.MaxValue;
				}

				return (int)o;
			}
			set {
				this[const_attribute_days_to_keep_log] = value;
			}
		}


		/// <summary>
		/// Gets or sets the override To address.
		/// </summary>
		/// <value>
		/// The override To address.
		/// </value>
		[ConfigurationProperty(const_attribute_override_to, IsRequired = false)]
		public string OverrideTo {
			get {
				return (string)this[const_attribute_override_to];
			}
			set {
				this[const_attribute_override_to] = value;
			}
		}

		/// <summary>
		/// Gets or sets the CC address to add.
		/// </summary>
		/// <value>
		/// The CC address to add.
		/// </value>
		[ConfigurationProperty(const_attribute_add_cc, IsRequired = false)]
		public string AddCC {
			get {
				return (string)this[const_attribute_add_cc];
			}
			set {
				this[const_attribute_add_cc] = value;
			}
		}

		/// <summary>
		/// Gets or sets the sender mail.
		/// </summary>
		/// <value>
		/// The sender mail.
		/// </value>
		[ConfigurationProperty(const_attribute_sendermail, IsRequired = false)]
		public string SenderMail {
			get {
				return (string)this[const_attribute_sendermail];
			}
			set {
				this[const_attribute_sendermail] = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of the sender.
		/// </summary>
		/// <value>
		/// The name of the sender.
		/// </value>
		[ConfigurationProperty(const_attribute_sendername, IsRequired = false)]
		public string SenderName {
			get {
				return (string)this[const_attribute_sendername];
			}
			set {
				this[const_attribute_sendername] = value;
			}
		}

		/// <summary>
		/// Gets or sets the behavior.
		/// </summary>
		/// <value>
		/// The behavior.
		/// </value>
		[ConfigurationProperty(const_attribute_behavior, DefaultValue = MailBehavior.Release)]
		public MailBehavior Behavior {
			get {
				return (MailBehavior)this[const_attribute_behavior];
			}
			set {
				this[const_attribute_behavior] = value;
			}
		}
	}
}

using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// Configuration Cryptography Element.
	/// </summary>
	public class CryptographyElement : ConfigurationElement {
		private const string const_attribute_key = "key";
		private const string const_attribute_algorithm = "algorithm";

		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>
		/// The key.
		/// </value>
		[ConfigurationProperty(const_attribute_key, IsRequired = true)]
		public string Key {
			get {
				string key = (string)this[const_attribute_key];

				return key;
			}
			set {
				this[const_attribute_key] = value;
			}
		}

		/// <summary>
		/// Gets or sets the algorithm.
		/// </summary>
		/// <value>
		/// The algorithm.
		/// </value>
		[ConfigurationProperty(const_attribute_algorithm, IsRequired = true)]
		public string Algorithm {
			get {
				string key = (string)this[const_attribute_algorithm];

				return key;
			}
			set {
				this[const_attribute_algorithm] = value;
			}
		}

	}
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Common.Configuration
{
    /// <summary>
    /// The configuration element for Analytics.
    /// </summary>
    public class AnalyticsElement : ConfigurationElement
    {
        private const string const_attribute_enable_google_analytics = "EnableGoogleAnalytics";
        private const string const_attribute_google_analytics_accountId = "GoogleAnalyticsAccountId";
        /// <summary>
        /// Gets or sets the EnableGoogleAnalytics.
        /// </summary>
        [ConfigurationProperty(const_attribute_enable_google_analytics, IsRequired = true)]
        public bool EnableGoogleAnalytics
        {
            get
            {
                return (bool)this[const_attribute_enable_google_analytics];
            }
            set
            {
                this[const_attribute_enable_google_analytics] = value;
            }
        }

        /// <summary>
        /// Gets or sets the Google Analytics Account Id.
        /// </summary>
        [ConfigurationProperty(const_attribute_google_analytics_accountId, IsRequired = true)]
        public string GoogleAnalyticsAccountId
        {
            get
            {
                return (string)this[const_attribute_google_analytics_accountId];
            }
            set
            {
                this[const_attribute_google_analytics_accountId] = value;
            }
        }
    }
}

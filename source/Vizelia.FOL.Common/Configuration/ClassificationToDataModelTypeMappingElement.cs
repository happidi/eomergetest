﻿using System.Configuration;

namespace Vizelia.FOL.Common.Configuration {
	/// <summary>
	/// Represents a configuration element for ClassificationToDataModelTypeMapping.
	/// </summary>
	public class ClassificationToDataModelTypeMappingElement : ConfigurationElement {
		private const string const_classification_item_local_id_path = "classificationItemLocalIdPath";
		private const string const_model_data_type_names = "modelDataTypeNames";


		/// <summary>
		/// Gets or sets the classification item local id path.
		/// </summary>
		/// <value>
		/// The classification item local id path.
		/// </value>
		[ConfigurationProperty(const_classification_item_local_id_path, IsRequired = false)]
		public string ClassificationItemLocalIdPath {
			get {
				return (string)this[const_classification_item_local_id_path];
			}
			set {
				this[const_classification_item_local_id_path] = value;
			}
		}

		/// <summary>
		/// Gets or sets the model data type names.
		/// </summary>
		/// <value>
		/// The model data type names.
		/// </value>
		[ConfigurationProperty(const_model_data_type_names, IsRequired = false)]
		public string ModelDataTypeNames {
			get {
				return (string)this[const_model_data_type_names];
			}
			set {
				this[const_model_data_type_names] = value;
			}
		}
	}
}

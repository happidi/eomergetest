﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Common {

    /// <summary>
    /// VizeliaValidationException
    /// </summary>
	public class VizeliaValidationException : VizeliaException {
        /// <summary>
        /// Initializes a new instance of the <see cref="VizeliaValidationException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
		public VizeliaValidationException(string message) : base(message) {

		}
	}
}

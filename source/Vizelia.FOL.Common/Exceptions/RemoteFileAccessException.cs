﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// An exception that represents an error accessing a remote file.
	/// </summary>
	[Serializable]
	public class RemoteFileAccessException : VizeliaException {
		private const string const_default_error_message = "An error occured while accessing a remote file.";

		private readonly string m_Message;

		/// <summary>
		/// Gets or sets the name of the file that is related to the error.
		/// </summary>
		/// <value>
		/// The name of the file that is related to the error.
		/// </value>
		public string FileName { get; set; }

		/// <summary>
		/// Gets a message that describes the current exception.
		/// </summary>
		/// <returns>The error message that explains the reason for the exception, or an empty string("").</returns>
		public override string Message {
			get { return m_Message; }
		}

		/// <summary>
		/// Gets or sets the file source description.
		/// </summary>
		/// <value>
		/// The file source description.
		/// </value>
		public string FileSourceDescription { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="RemoteFileAccessException"/> class.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <param name="filename">The filename.</param>
		/// <param name="inner">The inner.</param>
		/// <param name="message">The message.</param>
		public RemoteFileAccessException(IFileSource fileSource, string filename, Exception inner, string message)
			: base(string.Empty, inner) {
			FileSourceDescription = GetFileSourceDescription(fileSource);
			m_Message = message + " " + FileSourceDescription;
			FileName = filename;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RemoteFileAccessException"/> class.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <param name="filename">The filename.</param>
		/// <param name="message">The message.</param>
		public RemoteFileAccessException(IFileSource fileSource, string filename, string message) {
			FileSourceDescription = GetFileSourceDescription(fileSource);
			m_Message = message + " " + FileSourceDescription;
			FileName = filename;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RemoteFileAccessException"/> class.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <param name="inner">The inner.</param>
		public RemoteFileAccessException(IFileSource fileSource, Exception inner)
			: this(fileSource, string.Empty, inner, const_default_error_message) {
			// Do nothing.
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RemoteFileAccessException"/> class.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <param name="filename">The filename.</param>
		/// <param name="inner">The inner.</param>
		public RemoteFileAccessException(IFileSource fileSource, string filename, Exception inner)
			: this(fileSource, filename, inner, const_default_error_message) {
			// Do nothing.
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RemoteFileAccessException"/> class.
		/// </summary>
		public RemoteFileAccessException()
			: base(const_default_error_message) {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RemoteFileAccessException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public RemoteFileAccessException(string message)
			: base(message) {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RemoteFileAccessException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="inner">The inner.</param>
		public RemoteFileAccessException(string message, Exception inner)
			: base(message, inner) {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RemoteFileAccessException"/> class.
		/// </summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info"/> parameter is null. </exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is null or <see cref="P:System.Exception.HResult"/> is zero (0). </exception>
		public RemoteFileAccessException(SerializationInfo info, StreamingContext context)
			: base(info, context) {
			// Do nothing.	
		}

		/// <summary>
		/// Gets the file source description.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		private static string GetFileSourceDescription(IFileSource fileSource) {
			return string.Format("FileSourceType: \"{0}\", Address: \"{1}\", Custom Credentials: {2}, Username: \"{3}\"",
								 fileSource.FileSourceType.ToString(),
								 fileSource.Address,
								 fileSource.UseCustomCredentials,
								 fileSource.Username);
		}

	}
}
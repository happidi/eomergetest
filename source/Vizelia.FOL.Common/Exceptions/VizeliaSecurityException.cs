﻿using System;
using System.Runtime.Serialization;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A security exception to replace the .net security exception
	/// This is because sometimes the .net security exception is turned into a fault and we want consistant behavior for all our exceptions.
	/// </summary>
	public class VizeliaSecurityException : VizeliaException{

		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaSecurityException"/> class.
		/// </summary>
		public VizeliaSecurityException() {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaSecurityException"/> class.
		/// </summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info"/> parameter is null. </exception>
		///   
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is null or <see cref="P:System.Exception.HResult"/> is zero (0). </exception>
		public VizeliaSecurityException(SerializationInfo info, StreamingContext context) : base(info, context) {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaSecurityException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public VizeliaSecurityException(string message) : base(message) {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaSecurityException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="inner">The inner.</param>
		public VizeliaSecurityException(string message, Exception inner)
			: base(message, inner) {
			// Do nothing.	
		} 
		 
	}
}
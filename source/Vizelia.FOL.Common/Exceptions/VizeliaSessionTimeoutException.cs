﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Generic class for session timeout exceptions.
	/// </summary>
	[Serializable]
	public class VizeliaSessionTimeoutException : VizeliaException {

		/// <summary>
		/// Initializes a new instance of the VizeliaException class.
		/// </summary>
		public VizeliaSessionTimeoutException()
			: this(Langue.error_session_timeout) {
		}

		/// <summary>
		/// Initializes a new instance of the VizeliaSessionTimeoutException class
		/// with a specified error message.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		public VizeliaSessionTimeoutException(string message)
			: base(message) {
		}
		/// <summary>
		/// Initializes a new instance of the VizeliaSessionTimeoutException class
		/// with a specified error message and and a reference to the inner exception that is
		/// the cause of this exception.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="inner">The exception that is the cause of the current exception.</param>
		public VizeliaSessionTimeoutException(string message, Exception inner)
			: base(message, inner) {
		}
		/// <summary>
		/// Initializes a new instance of the VizeliaSessionTimeoutException class with
		/// serialized data.
		/// </summary>
		/// <param name="info">The object that holds the serialized object data.</param>
		/// <param name="context">The contextual information about the source or destination.</param>
		protected VizeliaSessionTimeoutException(SerializationInfo info, StreamingContext context)
			: base(info, context) {
		}

	}
}

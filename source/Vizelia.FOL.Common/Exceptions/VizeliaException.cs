﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Represents parent custom errors that occur during application execution
	/// </summary>
	[Serializable]
	public class VizeliaException : Exception {
		/// <summary>
		/// Initializes a new instance of the VizeliaException class.
		/// </summary>
		public VizeliaException()
			: this(Langue.error_msg_unknown) {
		}

		/// <summary>
		/// Initializes a new instance of the VizeliaException class
		/// with a specified error message.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		public VizeliaException(string message)
			: base(message) {
		}
		/// <summary>
		/// Initializes a new instance of the VizeliaException class
		/// with a specified error message and and a reference to the inner exception that is
		/// the cause of this exception.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="inner">The exception that is the cause of the current exception.</param>
		public VizeliaException(string message, Exception inner)
			: base(message, inner) {
		}
		/// <summary>
		/// Initializes a new instance of the VizeliaException class with
		/// serialized data.
		/// </summary>
		/// <param name="info">The object that holds the serialized object data.</param>
		/// <param name="context">The contextual information about the source or destination.</param>
		protected VizeliaException(SerializationInfo info, StreamingContext context)
			: base(info, context) {
		}
	}


}

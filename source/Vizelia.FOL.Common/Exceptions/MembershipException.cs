﻿using System;
using System.Runtime.Serialization;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	public class MembershipException : VizeliaException {
		/// <summary>
		/// Initializes a new instance of the <see cref="MembershipException"/> class.
		/// </summary>
		public MembershipException() {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MembershipException"/> class.
		/// </summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info"/> parameter is null. </exception>
		///   
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is null or <see cref="P:System.Exception.HResult"/> is zero (0). </exception>
		public MembershipException(SerializationInfo info, StreamingContext context)
			: base(info, context) {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MembershipException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public MembershipException(string message)
			: base(message) {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MembershipException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="inner">The inner.</param>
		public MembershipException(string message, Exception inner)
			: base(message, inner) {
			// Do nothing.	
		}

		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>
		/// The id.
		/// </value>
		public string Id { get; set; }
	}
}

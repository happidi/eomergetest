using System;
using System.Runtime.Serialization;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// An exception that represents an error performing mapping that is caused by a file that is not mappable.
	/// </summary>
	[Serializable]
	public class UnmappableFileException : MappingException {
		/// <summary>
		/// Initializes a new instance of the <see cref="UnmappableFileException"/> class.
		/// </summary>
		public UnmappableFileException() {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="UnmappableFileException"/> class.
		/// </summary>
		/// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> that holds the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext"/> that contains contextual information about the source or destination.</param>
		/// <exception cref="T:System.ArgumentNullException">The <paramref name="info"/> parameter is null. </exception>
		/// <exception cref="T:System.Runtime.Serialization.SerializationException">The class name is null or <see cref="P:System.Exception.HResult"/> is zero (0). </exception>
		public UnmappableFileException(SerializationInfo info, StreamingContext context) : base(info, context) {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="UnmappableFileException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		public UnmappableFileException(string message) : base(message) {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="UnmappableFileException"/> class.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="inner">The inner.</param>
		public UnmappableFileException(string message, Exception inner) : base(message, inner) {
			// Do nothing.	
		}
		
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Common {


	/// <summary>
	/// Generic class for database exceptions.
	/// </summary>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable")]
	public class VizeliaDatabaseException : VizeliaException {

		int _status;

		/// <summary>
		/// Public ctor.
		/// </summary>
		/// <param name="status">The returned value from the procedure.</param>
		public VizeliaDatabaseException(int status)
			: base() {
			_status = status;
		}

		/// <summary>
		/// Public ctor.
		/// </summary>
		/// <param name="message">The error message.</param>
		/// <param name="status">The returned value from the procedure.</param>
		public VizeliaDatabaseException(string message, int status)
			: base(message) {
			_status = status;
		}

		/// <summary>
		/// Gets the status of the operation.
		/// </summary>
		public int Status {
			get {
				return _status;
			}
		}
	}
}

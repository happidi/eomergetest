﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Common.Validators {

	/// <summary>
	/// Performs validations on array or list of strings for AntiXss attacks.
	/// </summary>
	public class AntiXssListValidator : ValueValidator<IEnumerable<string>> {

		/// <summary>
		/// Initializes a new instance of the <see cref="AntiXssValidator"/> class.
		/// </summary>
		public AntiXssListValidator() : this(null) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="AntiXssValidator"/> class.
		/// </summary>
		/// <param name="negated">if set to <c>true</c> [negated].</param>
		public AntiXssListValidator(bool negated) : this(null, negated) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="AntiXssValidator"/> class.
		/// </summary>
		/// <param name="messageTemplate">The message template.</param>
		public AntiXssListValidator(string messageTemplate) : base(messageTemplate, null, false) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="AntiXssValidator"/> class.
		/// </summary>
		/// <param name="messageTemplate">The message template.</param>
		/// <param name="negated">if set to <c>true</c> [negated].</param>
		public AntiXssListValidator(string messageTemplate, bool negated) : base(messageTemplate, null, negated) { }

		/// <summary>
		/// Gets the default negated message template.
		/// </summary>
		protected override string DefaultNegatedMessageTemplate {
			get { return Langue.error_msg_antixssvalidator; }
		}

		/// <summary>
		/// Gets the default non negated message template.
		/// </summary>
		protected override string DefaultNonNegatedMessageTemplate {
			get { return Langue.error_msg_antixssvalidator; }
		}


		/// <summary>
		/// Does the validate.
		/// </summary>
		/// <param name="objectToValidate">The object to validate.</param>
		/// <param name="currentTarget">The current target.</param>
		/// <param name="key">The key.</param>
		/// <param name="validationResults">The validation results.</param>
		protected override void DoValidate(IEnumerable<string> objectToValidate, object currentTarget, string key, ValidationResults validationResults) {
			bool isSanitized;
			string sanitizedHtml;
			List<string> invalidTags;

			if (objectToValidate != null) {
				var toValidate = objectToValidate as List<string> ?? objectToValidate.ToList();
				if (toValidate.Count > 0) {
					var message = MessageTemplate;
					foreach (string bodyHtml in toValidate) {
						if (!AntiXssValidationService.Disable && !AntiXssValidationService.IsValid(bodyHtml, out isSanitized, out invalidTags, out sanitizedHtml)) {
							if (AntiXssValidationService.AutoClean) {
								SetValue(currentTarget, key, AntiXssValidationService.Clean(sanitizedHtml));
							}
							else {
								// we log the error.
								LogValidationResult(validationResults, message, currentTarget, key);
							}
							WriteToTrace(bodyHtml, key, invalidTags, isSanitized);
						}
					}
				}
			}
		}

		/// <summary>
		/// Writes to trace.
		/// </summary>
		/// <param name="objectToValidate">The object to validate.</param>
		/// <param name="key">The key.</param>
		/// <param name="invalidTags">The invalid tags.</param>
		/// <param name="isSanitized">if set to <c>true</c> [is sanitized].</param>
		private static void WriteToTrace(string objectToValidate, string key, List<string> invalidTags, bool isSanitized) {
			string warning = string.Format(Langue.error_malicious_html, objectToValidate, isSanitized ? "AntiXssProvider" : "HtmlSanitizer");
			if (invalidTags.Count > 0) {
				warning += string.Format(Langue.msg_invalid_tags, string.Join(",", invalidTags.ToArray()));
			}
			TracingService.Write(TraceEntrySeverity.Warning, warning, "Html Validation", key);
		}

		/// <summary>
		/// Sets the value.
		/// </summary>
		/// <param name="currentTarget">The current target.</param>
		/// <param name="key">The key.</param>
		/// <param name="cleanedValue">The cleaned value.</param>
		private static void SetValue(object currentTarget, string key, string cleanedValue) {
			Type type = currentTarget.GetType();
			PropertyInfo prop = type.GetProperty(key);
			prop.SetValue(currentTarget, cleanedValue, null);
		}
	}


	/// <summary>
	/// Performs validations on array or list of strings for AntiXss attacks.
	/// </summary>
	[AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	public sealed class AntiXssListValidatorAttribute : ValueValidatorAttribute {

		/// <summary>
		/// Does the create validator.
		/// </summary>
		/// <param name="targetType">Type of the target.</param>
		/// <returns></returns>
		protected override Validator DoCreateValidator(Type targetType) {
			return new AntiXssListValidator();
		}
	}
}

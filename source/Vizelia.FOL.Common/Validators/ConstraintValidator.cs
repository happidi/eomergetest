﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;

namespace Vizelia.FOL.Common.Validators {

	/// <summary>
	/// Indicates there was a violation of a database constraint.
	/// Use the static MakeValidationException method to generate it inside
	/// an ArgumentValidationException.
	/// </summary>
	/// <remarks>
	/// <para>This class should not be used for actual validation. It is merely 
	/// a placeholder that is added to the ValidationResults when there is 
	/// a VizeliaDatabaseException thrown. Do not create an Attribute wrapper.</para>
	/// <para></para>
	/// </remarks>
	public class ConstraintValidator : Microsoft.Practices.EnterpriseLibrary.Validation.Validator {

		/// <summary>
		/// Initializes a new instance of the <see cref="ConstraintValidator"/> class.
		/// </summary>
		/// <param name="messageTemplate">The message template.</param>
		/// <param name="tag">The tag.</param>
		/// <exception cref="System.ArgumentException">Always assign messageTemplate.</exception>
		public ConstraintValidator(string messageTemplate, string tag)
			: base(messageTemplate, tag) {
			if (String.IsNullOrEmpty(messageTemplate))
				throw new ArgumentException("Always assign messageTemplate.");
		}

		/// <summary>
		/// Gets the default message template.
		/// </summary>
		/// <value>
		/// The default message template.
		/// </value>
		protected override string DefaultMessageTemplate {
			get { return "Constrain violation"; }	// not localized because this is not intended to be used. Instead, a message is always assigned
		}

		/// <summary>
		/// Does the validate.
		/// </summary>
		/// <param name="objectToValidate">The object to validate.</param>
		/// <param name="currentTarget">The current target.</param>
		/// <param name="key">The key.</param>
		/// <param name="validationResults">The validation results.</param>
		public override void DoValidate(object objectToValidate, object currentTarget, string key, Microsoft.Practices.EnterpriseLibrary.Validation.ValidationResults validationResults) {

		}

		/// <summary>
		/// Creates an ArgumentValidationException for when a VizeliaDatabaseException
		/// occurs. The ArgumentValidationException lets the error be reported
		/// as a validation error instead of a more severe error.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <param name="message">The message.</param>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public static ArgumentValidationException MakeValidationException(string field, string message, object item) {
			// These errors are really a kind of validation error and should 
			// report as such.
			ValidationResults valResults =	new ValidationResults();
			ValidationResult valResult = new ValidationResult(
					message, item, field,
					"",
					new ConstraintValidator(message, ""));
			valResults.AddResult(valResult);
			return new ArgumentValidationException(valResults, field);
		}
	}
}
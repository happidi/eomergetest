﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Common.Validators {

	/// <summary>
	/// Performs validations on string length.
	/// </summary>
	public class VizStringLengthValidator : StringLengthValidator {


		/// <summary>
		/// Initializes a new instance of the <see cref="VizStringLengthValidator"/> class.
		/// </summary>
		/// <param name="upperBound">The upper bound.</param>
		public VizStringLengthValidator(int upperBound) : base(upperBound) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="VizStringLengthValidator"/> class.
		/// </summary>
		/// <param name="upperBound">The upper bound.</param>
		/// <param name="negated">if set to <c>true</c> [negated].</param>
		public VizStringLengthValidator(int upperBound, bool negated) : base(upperBound, negated) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="VizStringLengthValidator"/> class.
		/// </summary>
		/// <param name="lowerBound">The lower bound.</param>
		/// <param name="upperBound">The upper bound.</param>
		public VizStringLengthValidator(int lowerBound, int upperBound) : base(lowerBound, upperBound) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="VizStringLengthValidator"/> class.
		/// </summary>
		/// <param name="lowerBound">The lower bound.</param>
		/// <param name="upperBound">The upper bound.</param>
		/// <param name="negated">if set to <c>true</c> [negated].</param>
		public VizStringLengthValidator(int lowerBound, int upperBound, bool negated) : base(lowerBound, upperBound, negated) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="VizStringLengthValidator"/> class.
		/// </summary>
		/// <param name="lowerBound">The lower bound.</param>
		/// <param name="lowerBoundType">Type of the lower bound.</param>
		/// <param name="upperBound">The upper bound.</param>
		/// <param name="upperBoundType">Type of the upper bound.</param>
		public VizStringLengthValidator(int lowerBound, RangeBoundaryType lowerBoundType, int upperBound, RangeBoundaryType upperBoundType) : base(lowerBound, lowerBoundType, upperBound, upperBoundType) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="VizStringLengthValidator"/> class.
		/// </summary>
		/// <param name="lowerBound">The lower bound.</param>
		/// <param name="lowerBoundType">Type of the lower bound.</param>
		/// <param name="upperBound">The upper bound.</param>
		/// <param name="upperBoundType">Type of the upper bound.</param>
		/// <param name="negated">if set to <c>true</c> [negated].</param>
		public VizStringLengthValidator(int lowerBound, RangeBoundaryType lowerBoundType, int upperBound, RangeBoundaryType upperBoundType, bool negated) : base(lowerBound, lowerBoundType, upperBound, upperBoundType, negated) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="VizStringLengthValidator"/> class.
		/// </summary>
		/// <param name="lowerBound">The lower bound.</param>
		/// <param name="lowerBoundType">Type of the lower bound.</param>
		/// <param name="upperBound">The upper bound.</param>
		/// <param name="upperBoundType">Type of the upper bound.</param>
		/// <param name="messageTemplate">The message template.</param>
		public VizStringLengthValidator(int lowerBound, RangeBoundaryType lowerBoundType, int upperBound, RangeBoundaryType upperBoundType, string messageTemplate) : base(lowerBound, lowerBoundType, upperBound, upperBoundType, messageTemplate) { }


		/// <summary>
		/// Initializes a new instance of the <see cref="VizStringLengthValidator"/> class.
		/// </summary>
		/// <param name="lowerBound">The lower bound.</param>
		/// <param name="lowerBoundType">The indication of how to perform the lower bound check.</param>
		/// <param name="upperBound">The upper bound.</param>
		/// <param name="upperBoundType">The indication of how to perform the upper bound check.</param>
		/// <param name="messageTemplate">The message template to use when logging results.</param>
		/// <param name="negated">True if the validator must negate the result of the validation.</param>
		/// <seealso cref="T:Microsoft.Practices.EnterpriseLibrary.Validation.Validators.RangeBoundaryType"/>
		public VizStringLengthValidator(int lowerBound, RangeBoundaryType lowerBoundType, int upperBound, RangeBoundaryType upperBoundType, string messageTemplate, bool negated) : base(lowerBound, lowerBoundType, upperBound, upperBoundType, messageTemplate, negated) { }


		/// <summary>
		/// Gets the message representing a failed validation.
		/// </summary>
		/// <param name="objectToValidate">The object for which validation was performed.</param>
		/// <param name="key">The key representing the value being validated for <paramref name="objectToValidate"/>.</param>
		/// <returns>
		/// The message representing the validation failure.
		/// </returns>
		protected override string GetMessage(object objectToValidate, string key) {
			MessageTemplate = Langue.error_msg_stringlengthvalidator;
			return base.GetMessage(objectToValidate, key);
		}

	}

	/// <summary>
	/// 
	/// </summary>
	[AttributeUsage(AttributeTargets.Parameter | AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	public sealed class VizStringLengthValidatorAttribute : ValueValidatorAttribute {
		// Fields
		private int lowerBound;
		private RangeBoundaryType lowerBoundType;
		private int upperBound;
		private RangeBoundaryType upperBoundType;

		/// <summary>
		/// Initializes a new instance of the <see cref="VizStringLengthValidatorAttribute"/> class.
		/// </summary>
		/// <param name="upperBound">The upper bound.</param>
		public VizStringLengthValidatorAttribute(int upperBound)
			: this(0, RangeBoundaryType.Ignore, upperBound, RangeBoundaryType.Inclusive) {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="VizStringLengthValidatorAttribute"/> class.
		/// </summary>
		/// <param name="lowerBound">The lower bound.</param>
		/// <param name="upperBound">The upper bound.</param>
		public VizStringLengthValidatorAttribute(int lowerBound, int upperBound)
			: this(lowerBound, RangeBoundaryType.Inclusive, upperBound, RangeBoundaryType.Inclusive) {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="VizStringLengthValidatorAttribute"/> class.
		/// </summary>
		/// <param name="lowerBound">The lower bound.</param>
		/// <param name="lowerBoundType">Type of the lower bound.</param>
		/// <param name="upperBound">The upper bound.</param>
		/// <param name="upperBoundType">Type of the upper bound.</param>
		public VizStringLengthValidatorAttribute(int lowerBound, RangeBoundaryType lowerBoundType, int upperBound, RangeBoundaryType upperBoundType) {
			this.lowerBound = lowerBound;
			this.lowerBoundType = lowerBoundType;
			this.upperBound = upperBound;
			this.upperBoundType = upperBoundType;
		}

		/// <summary>
		/// Creates the <see cref="T:Microsoft.Practices.EnterpriseLibrary.Validation.Validator"/> described by the attribute object providing validator specific
		/// information.
		/// </summary>
		/// <param name="targetType">The type of object that will be validated by the validator.</param>
		/// <returns>
		/// The created <see cref="T:Microsoft.Practices.EnterpriseLibrary.Validation.Validator"/>.
		/// </returns>
		/// <remarks>This operation must be overriden by subclasses.</remarks>
		protected override Validator DoCreateValidator(Type targetType) {
			return new VizStringLengthValidator(lowerBound, lowerBoundType, upperBound, upperBoundType, Negated);
		}
	}


}

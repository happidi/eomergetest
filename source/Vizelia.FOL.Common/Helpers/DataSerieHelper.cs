﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Encapsulates extension for the DataSerie class.
	/// </summary>
	public static class DataSerieHelper {

		#region Math Projection
		/// <summary>
		/// Projects the specified serie based on the grouping operation.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static DataPoint Project(DataSerie serie) {
			var p = new DataPoint { Name = serie.Name, Color = serie.Color, YValue = 0 };
			switch (serie.GroupOperation) {
				case MathematicOperator.AVG:
					p.YValue = Avg(serie);
					break;
				case MathematicOperator.MAX:
					p.YValue = Max(serie);
					break;
				case MathematicOperator.MIN:
					p.YValue = Min(serie);
					break;
				case MathematicOperator.SUM:
					p.YValue = Sum(serie);
					break;
			}
			return p;
		}


		/// <summary>
		/// Sum of the series
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static double Sum(DataSerie serie) {
			var points = serie.PointsWithValue;
			if (points != null && points.Count > 0) {
				return points.Sum(p => p.YValue.Value);
			}
			return 0;
		}

		/// <summary>
		/// Average of the series
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static double Avg(DataSerie serie) {
			var points = serie.PointsWithValue;
			if (points != null && points.Count > 0) {
				return points.Average(p => p.YValue.Value);
			}
			return 0;
		}
		/// <summary>
		/// Minimum of the series
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static double Min(DataSerie serie) {
			var points = serie.PointsWithValue;
			if (points != null && points.Count > 0) {
				return points.Min(p => p.YValue.Value);
			}
			return 0;
		}

		/// <summary>
		/// Maximum of the series
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static double Max(DataSerie serie) {
			var points = serie.PointsWithValue;
			if (points != null && points.Count > 0) {
				return points.Max(p => p.YValue.Value);
			}
			return 0;
		}

		/// <summary>
		/// Gets the first value of the specified serie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static DataPoint FirstDataPoint(DataSerie serie) {
			DataPoint retVal = null;
			var points = serie.PointsWithValue.OrderBy(p => p.XDateTime.HasValue ? p.XDateTime.Value : DateTime.MaxValue).ToList();
			if (points != null && points.Count > 0) {
				retVal = points.OrderBy(p => p.XDateTime).First();
			}
			return retVal;
		}

		/// <summary>
		/// Gets the first value of the specified serie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static double First(DataSerie serie) {
			var p = FirstDataPoint(serie);
			return p != null && p.YValue.HasValue ? p.YValue.Value : 0;
		}


		/// <summary>
		/// Gets the first value of the specified serie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static DataPoint LastDataPoint(DataSerie serie) {
			DataPoint retVal = null;
			var points = serie.PointsWithValue.OrderBy(p => p.XDateTime.HasValue ? p.XDateTime.Value : DateTime.MinValue).ToList();
			if (points != null && points.Count > 0) {
				retVal = points.Last();
			}
			return retVal;
		}

		/// <summary>
		/// Gets the last value of the specified serie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static double Last(DataSerie serie) {
			var p = LastDataPoint(serie);
			return p != null && p.YValue.HasValue ? p.YValue.Value : 0;
		}

		/// <summary>
		/// Gets the penultimate value of the specified serie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static DataPoint PenultimateDataPoint(DataSerie serie) {
			DataPoint retVal = null;
			var points = serie.PointsWithValue.OrderBy(p => p.XDateTime.HasValue ? p.XDateTime.Value : DateTime.MinValue).ToList();
			if (points != null && points.Count > 0) {
				var lastIndex = points.Count > 1 ? points.Count - 2 : 0;
				retVal = points[lastIndex];
			}
			return retVal;
		}

		/// <summary>
		/// Gets the index value of the specified serie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <param name="i">The i.</param>
		/// <returns></returns>
		public static DataPoint IndexDataPoint(DataSerie serie, int i) {
			DataPoint retVal = null;
			var points = serie.PointsWithValue.OrderBy(p => p.XDateTime.HasValue ? p.XDateTime.Value : DateTime.MinValue).ToList();
			if (points != null && points.Count > 0 && i < points.Count) {
				retVal = points[i];
			}
			return retVal;
		}

		/// <summary>
		/// Gets the penultimate value of the specified serie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static double Penultimate(DataSerie serie) {
			var p = PenultimateDataPoint(serie);
			return p != null && p.YValue.HasValue ? p.YValue.Value : 0;
		}

		/// <summary>
		/// Gets the index value of the specified serie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <param name="i">The i.</param>
		/// <returns></returns>
		public static double Index(DataSerie serie, int i) {
			var p = IndexDataPoint(serie, i);
			return p != null && p.YValue.HasValue ? p.YValue.Value : 0;
		}

		/// <summary>
		/// Minimum of the series DateTime.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static DateTime MinDate(DataSerie serie) {
			DateTime retVal = DateTime.Now;
			var points = serie.PointsWithValue;
			if (points != null && points.Count > 0) {
				var date = points.OrderBy(p => p.YValue.Value).First().XDateTime;
				if (date.HasValue)
					retVal = date.Value;
			}
			return retVal;
		}

		/// <summary>
		/// Maximum of the series DateTime
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static DateTime MaxDate(DataSerie serie) {
			DateTime retVal = DateTime.Now;
			var points = serie.PointsWithValue;
			if (points != null && points.Count > 0) {
				var date = points.OrderBy(p => p.YValue.Value).Last().XDateTime;
				if (date.HasValue)
					retVal = date.Value;
			}
			return retVal;
		}

		/// <summary>
		/// Gets the first DateTime of the specified serie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static DateTime FirstDate(DataSerie serie) {
			DateTime retVal = DateTime.Now;
			var p = FirstDataPoint(serie);
			return p != null && p.XDateTime.HasValue ? p.XDateTime.Value : DateTime.Now;
		}

		/// <summary>
		/// Gets the last DateTime of the specified serie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static DateTime LastDate(DataSerie serie) {
			DateTime retVal = DateTime.Now;
			var p = LastDataPoint(serie);
			return p != null && p.XDateTime.HasValue ? p.XDateTime.Value : DateTime.Now;
		}
		/// <summary>
		/// Gets the penultimate DateTime of the specified serie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public static DateTime PenultimateDate(DataSerie serie) {
			DateTime retVal = DateTime.Now;
			var p = PenultimateDataPoint(serie);
			return p != null && p.XDateTime.HasValue ? p.XDateTime.Value : DateTime.Now;
		}

		/// <summary>
		/// Gets the penultimate DateTime of the specified serie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <param name="i">The i.</param>
		/// <returns></returns>
		public static DateTime IndexDate(DataSerie serie, int i) {
			DateTime retVal = DateTime.Now;
			var p = IndexDataPoint(serie, i);
			return p != null && p.XDateTime.HasValue ? p.XDateTime.Value : DateTime.Now;
		}

		#endregion

		#region CleanData
		/// <summary>
		/// Cleans the data of a DataSerie. (MeterData and DataPoint)
		/// </summary>
		/// <param name="serie">The serie.</param>
		public static void CleanData(DataSerie serie) {
			if (serie != null) {
				serie.Points = new List<DataPoint>();
				serie.MDData = new List<MD>();
			}
		}

		/// <summary>
		/// Cleans the data of a DataSerie. (MeterData and DataPoint)
		/// </summary>
		/// <param name="series">The series collection.</param>
		public static void CleanData(DataSerieCollection series) {
			if (series != null && series.Count > 0) {
				series.ToList().ForEach(CleanData);
			}
		}
		#endregion

		#region MathematicOperation
		/// <summary>
		/// Apply Mathematic operation to 2 dataseries.
		/// </summary>
		/// <param name="s1">The s1.</param>
		/// <param name="s2">The s2.</param>
		/// <param name="op">The op.</param>
		/// <returns></returns>
		public static DataSerie MathematicOperation(DataSerie s1, DataSerie s2, MathematicOperator op) {
			DataSerie r = s1.Copy(copyDataPoints: false);

			if ((!s1.HasPointsWithValue) && (!s2.HasPointsWithValue))
				return r;

			bool isDateTime = s1.HasPointsWithValue ? s1.IsDateTime : s2.IsDateTime;

			Dictionary<object, DataPoint> dict = new Dictionary<object, DataPoint>();
			foreach (DataPoint p in s1.Points) {
				if (p.YValue.HasValue) {
					if (isDateTime && p.XDateTime.HasValue) {
						dict.Add(p.XDateTime.Value, p.Copy());
					}
					else {
						if (!string.IsNullOrEmpty(p.Name)) {
							dict.Add(p.Name, p.Copy());
						}
					}
				}
			}

			foreach (DataPoint p in s2.Points) {
				if (p.YValue.HasValue) {
					object key = null;
					if (isDateTime && p.XDateTime.HasValue) {
						key = p.XDateTime.Value;
					}
					else {
						key = p.Name;
					}
					if (key != null) {
						if (dict.ContainsKey(key)) {
							var p1 = dict[key];
							switch (op) {
								case MathematicOperator.SUM:
									p1.YValue += p.YValue;
									break;
								case MathematicOperator.AVG:
									p1.YValue = (p1.YValue + p.YValue) / 2d;
									break;
								case MathematicOperator.MAX:
									p1.YValue = Math.Max(p1.YValue.Value, p.YValue.Value);
									break;
								case MathematicOperator.MIN:
									p1.YValue = Math.Min(p1.YValue.Value, p.YValue.Value);
									break;
							}
						}
						else {
							dict.Add(key, p.Copy());

						}
					}
				}
			}
			r.Points = dict.Values.ToList();
			SortPoints(r);
			return r;
		}
		#endregion


		#region ArithmeticOperation
		/// <summary>
		/// Private arithmetic operation between 2 dataseries.
		/// </summary>
		/// <param name="s1">The dataserie 1.</param>
		/// <param name="s2">The dataserie 2.</param>
		/// <param name="op">The mathematic operator.</param>
		/// <returns></returns>
		public static DataSerie ArithmeticOperation(DataSerie s1, DataSerie s2, ArithmeticOperator op) {
			DataSerie r = s1.Copy(copyDataPoints: false);

			if ((!s1.HasPointsWithValue) && (!s2.HasPointsWithValue))
				return r;

			bool isDateTime = s1.HasPointsWithValue ? s1.IsDateTime : s2.IsDateTime;

			Dictionary<object, DataPoint> dict = new Dictionary<object, DataPoint>();
			foreach (DataPoint p in s1.Points) {
				if (p.YValue.HasValue) {
					if (isDateTime && p.XDateTime.HasValue) {
						dict.Add(p.XDateTime.Value, p.Copy());
					}
					else {
						if (!string.IsNullOrEmpty(p.Name)) {
							dict.Add(p.Name, p.Copy());
						}
					}
				}
			}

			foreach (DataPoint p in s2.Points) {
				if (p.YValue.HasValue) {
					object key = null;
					if (isDateTime && p.XDateTime.HasValue) {
						key = p.XDateTime.Value;
					}
					else {
						key = p.Name;
					}
					if (key != null) {
						if (dict.ContainsKey(key)) {
							var p1 = dict[key];
							switch (op) {
								case ArithmeticOperator.Add:
									p1.YValue += p.YValue;
									break;
								case ArithmeticOperator.Subtract:
									p1.YValue -= p.YValue;
									break;
								case ArithmeticOperator.Multiply:
									p1.YValue *= p.YValue;
									break;
								case ArithmeticOperator.Divide:
									if (Math.Abs(p.YValue.Value) > 0) {
										p1.YValue /= p.YValue;
									}
									else {
										p1.YValue = null;
									}
									break;
							}
						}
						else {
							switch (op) {
								case ArithmeticOperator.Add:
									dict.Add(key, p.Copy());
									break;
								case ArithmeticOperator.Subtract:
									var np = p.Copy();
									np.YValue = -np.YValue;
									dict.Add(key, np);
									break;
							}
						}
					}
				}
			}
			r.Points = dict.Values.ToList();
			SortPoints(r);
			return r;
		}

		/// <summary>
		/// Arithmetic operation between a dataserie and a double.
		/// </summary>
		/// <param name="s">The dataserie.</param>
		/// <param name="d">The number.</param>
		/// <param name="op">The mathematic operator.</param>
		/// <returns></returns>
		public static DataSerie ArithmeticOperation(DataSerie s, double d, ArithmeticOperator op) {
			DataSerie r = s.Copy(copyDataPoints: false);

			foreach (DataPoint p in s.Points) {
				DataPoint np = p.Copy();
				if (np.YValue.HasValue) {
					switch (op) {
						case ArithmeticOperator.Add:
							np.YValue = np.YValue + d;
							break;

						case ArithmeticOperator.Subtract:
							np.YValue = np.YValue - d;
							break;

						case ArithmeticOperator.Multiply:
							np.YValue = np.YValue * d;
							break;

						case ArithmeticOperator.Divide:
							if (d != 0)
								np.YValue = np.YValue / d;
							else
								np.YValue = double.NaN;
							break;

						case ArithmeticOperator.Power:
							np.YValue = Math.Pow(np.YValue.Value, d);
							break;


					}
				}
				if (np.YValue.HasValue && double.IsNaN(np.YValue.Value) == false)
					r.Points.Add(np);
			}
			return r;
		}

		/// <summary>
		/// Arithmetic operation between a dataserie and a double.
		/// </summary>
		/// <param name="s">The dataserie.</param>
		/// <param name="d">The number.</param>
		/// <param name="op">The mathematic operator.</param>
		/// <returns></returns>
		public static DataSerie ArithmeticOperation(double d, DataSerie s, ArithmeticOperator op) {
			DataSerie r = s.Copy(copyDataPoints: false);

			foreach (DataPoint p in s.Points) {
				DataPoint np = p.Copy();
				if (np.YValue.HasValue) {
					switch (op) {
						case ArithmeticOperator.Add:
							np.YValue = np.YValue + d;
							break;

						case ArithmeticOperator.Subtract:
							np.YValue = d - np.YValue;
							break;

						case ArithmeticOperator.Multiply:
							np.YValue = np.YValue * d;
							break;

						case ArithmeticOperator.Divide:
							if (np.YValue != 0)
								np.YValue = d / np.YValue;
							else
								np.YValue = double.NaN;
							break;

						case ArithmeticOperator.Power:
							np.YValue = Math.Pow(d, np.YValue.Value);
							break;


					}
				}
				if (np.YValue.HasValue && double.IsNaN(np.YValue.Value) == false)
					r.Points.Add(np);
			}
			return r;
		}

		/// <summary>
		/// Arithmetic operation between 2 doubles.
		/// </summary>
		/// <param name="v1">The value 1.</param>
		/// <param name="v2">The value 2.</param>
		/// <param name="op">The mathematic operator.</param>
		/// <returns></returns>
		public static double ArithmeticOperation(double v1, double v2, ArithmeticOperator op) {
			double retVal = 0;
			switch (op) {
				case ArithmeticOperator.Multiply:
					retVal = v1 * v2;
					;
					break;
				case ArithmeticOperator.Divide:
					retVal = v1 / v2;
					break;
				case ArithmeticOperator.Add:
					retVal = v1 + v2;
					break;
				case ArithmeticOperator.Subtract:
					retVal = v1 - v2;
					break;
				case ArithmeticOperator.Power:
					retVal = Math.Pow(v1, v2);
					break;
			}
			return retVal;
		}
		#endregion

		#region OneParameterFunction
		/// <summary>
		/// Called when [1 parameter function].
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <param name="extraParam">The extra param.</param>
		/// <param name="func">The func.</param>
		/// <returns></returns>
		public static DataSerie OneParameterFunction(DataSerie serie, double extraParam, OneParameterFunction func) {
			DataSerie r = serie.Copy(copyDataPoints: false);

			if (!serie.HasPointsWithValue)
				return r;

			switch (func) {

				case FOL.BusinessEntities.OneParameterFunction.Higher: {
						r.Points = serie.Points.Where(p => p.YValue.HasValue && p.YValue.Value >= extraParam).ToList();
					}
					break;

				case FOL.BusinessEntities.OneParameterFunction.Lower: {
						r.Points = serie.Points.Where(p => p.YValue.HasValue && p.YValue.Value <= extraParam).ToList();
					}
					break;
			}
			return r;
		}
		#endregion

		#region AlgebricFunction
		/// <summary>
		/// Algebric operation on a dataserie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <param name="func">The algebric function.</param>
		/// <returns></returns>
		public static DataSerie AlgebricFunction(DataSerie serie, AlgebricFunction func) {
			DataSerie r = serie.Copy(copyDataPoints: false);

			if (!serie.HasPointsWithValue)
				return r;

			switch (func) {

				case FOL.BusinessEntities.AlgebricFunction.Percentage: {
						SortPoints(serie);
						var np = serie.PointsWithValue[0].Copy();
						var lastValue = serie.PointsWithValue[0].YValue.Value;
						np.YValue = 0;
						r.Points.Add(np);
						for (var i = 1; i < serie.PointsWithValue.Count; i++) {
							np = serie.PointsWithValue[i].Copy();
							if (Math.Abs(lastValue) > 0) {
								np.YValue = (np.YValue - lastValue) / lastValue;
								r.Points.Add(np);
							}
							lastValue = serie.PointsWithValue[i].YValue.Value;

						}
					}
					break;

				case FOL.BusinessEntities.AlgebricFunction.Delta: {
						SortPoints(serie);
						var np = serie.PointsWithValue[0].Copy();
						var lastValue = serie.PointsWithValue[0].YValue.Value;
						np.YValue = 0;
						r.Points.Add(np);

						for (var i = 1; i < serie.PointsWithValue.Count; i++) {
							np = serie.PointsWithValue[i].Copy();
							if (Math.Abs(lastValue) > 0) {
								np.YValue = (np.YValue - lastValue);
								r.Points.Add(np);
							}
							lastValue = serie.PointsWithValue[i].YValue.Value;
						}
					}
					break;

				case FOL.BusinessEntities.AlgebricFunction.YTD: {
						SortPoints(serie);
						var points = serie.PointsWithValue;
						if (points[0].XDateTime.HasValue) {
							//var currentYear = points[0].XDateTime.Value.Year;
							var currentYtd = points[0].YValue.Value;
							r.Points.Add(points[0].Copy());
							for (var i = 1; i < points.Count; i++) {
								var np = points[i].Copy();
								//if (np.XDateTime.Value.Year == currentYear) {
								np.YValue += currentYtd;
								currentYtd = np.YValue.Value;
								//}
								//else {
								//		currentYear = np.XDateTime.Value.Year;
								//		currentYtd = np.YValue.Value;
								//}
								r.Points.Add(np);
							}
						}

					}
					break;

				case FOL.BusinessEntities.AlgebricFunction.Count: {
						foreach (DataPoint p in serie.Points) {
							var np = p.Copy();
							if (np.YValue.HasValue) {
								np.YValue = 1;
							}
							r.Points.Add(np);
						}
					}
					break;

				default: {
						foreach (DataPoint p in serie.Points) {
							var np = p.Copy();
							if (np.YValue.HasValue) {
								np.YValue = AlgebricFunction(np.YValue.Value, func);
							}
							r.Points.Add(np);
						}
					}
					break;
			}

			return r;
		}

		/// <summary>
		/// Algebric operation on a double.
		/// </summary>
		/// <param name="v">The value.</param>
		/// <param name="func">The algebric function.</param>
		/// <returns></returns>
		public static double AlgebricFunction(double v, AlgebricFunction func) {
			var retVal = v;
			switch (func) {
				case FOL.BusinessEntities.AlgebricFunction.Sqrt:
					retVal = Math.Sqrt(v);
					break;

				case Vizelia.FOL.BusinessEntities.AlgebricFunction.Abs:
					retVal = Math.Abs(v);
					break;

				case Vizelia.FOL.BusinessEntities.AlgebricFunction.Cos:
					retVal = Math.Cos(v);
					break;

				case Vizelia.FOL.BusinessEntities.AlgebricFunction.Sin:
					retVal = Math.Sin(v);
					break;

				case Vizelia.FOL.BusinessEntities.AlgebricFunction.Log:
					retVal = Math.Log(v);
					break;

				case Vizelia.FOL.BusinessEntities.AlgebricFunction.Tan:
					retVal = Math.Tan(v);
					break;

				case Vizelia.FOL.BusinessEntities.AlgebricFunction.Inverse:
					if (Math.Abs(v) > 0)
						retVal = 1 / v;
					else
						retVal = double.NaN;
					break;
				case FOL.BusinessEntities.AlgebricFunction.ErrorCorrectToZero:
					if (Double.IsNaN(v))
						retVal = 0;
					else
						retVal = v;
					break;
				case FOL.BusinessEntities.AlgebricFunction.ErrorCorrectToOne:
					if (Double.IsNaN(v))
						retVal = 1;
					else
						retVal = v;
					break;
			}

			return retVal;
		}
		#endregion


		#region DataSerieProjection
		/// <summary>
		/// Algebric operation on a dataserie.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <param name="func">The mathematic operator function.</param>
		/// <returns></returns>
		public static DataSerie DataSerieProjection(DataSerie serie, DataSerieProjection func) {
			DataSerie r = serie.Copy(copyDataPoints: false);

			if (!serie.HasPointsWithValue)
				return r;

			DataPoint np = null;
			switch (func) {
				case FOL.BusinessEntities.DataSerieProjection.Average:
					np = new DataPoint() {
						YValue = Avg(serie),
						XDateTime = LastDate(serie)
					};
					break;

				case FOL.BusinessEntities.DataSerieProjection.First:
					np = new DataPoint() {
						YValue = First(serie),
						XDateTime = FirstDate(serie)
					};
					break;

				case FOL.BusinessEntities.DataSerieProjection.Last:
					np = new DataPoint() {
						YValue = Last(serie),
						XDateTime = LastDate(serie)
					};
					break;

				case FOL.BusinessEntities.DataSerieProjection.Maximum:
					np = new DataPoint() {
						YValue = Max(serie),
						XDateTime = MaxDate(serie)
					};
					break;

				case FOL.BusinessEntities.DataSerieProjection.Minimum:
					np = new DataPoint() {
						YValue = Min(serie),
						XDateTime = MinDate(serie)
					};
					break;

				case FOL.BusinessEntities.DataSerieProjection.Penultimate:
					np = new DataPoint() {
						YValue = Penultimate(serie),
						XDateTime = PenultimateDate(serie)
					};
					break;

				case FOL.BusinessEntities.DataSerieProjection.Sum:
					np = new DataPoint() {
						YValue = Sum(serie),
						XDateTime = LastDate(serie)
					};
					break;
			}
			if (np != null) {
				r.Points.Add(np);
			}
			return r;
		}


		#endregion

		#region LimitPoints
		/// <summary>
		/// Limits the max number of points for a DataSerie (used for optimizing performances in Chart generation)
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <param name="max">The max.</param>
		public static void LimitPoints(DataSerie serie, int max) {
			if (serie.Points.Count > 0) {
				var pointsFinal = new List<DataPoint>();
				var pointsWithValue = serie.PointsWithValue;


				if (max > 0 && pointsWithValue.Count > max && serie.IsDateTime) {
					var j = 0;
					var groupCount = Math.Max(2, pointsWithValue.Count / max);
					while (j < pointsWithValue.Count) {

						DateTime startDate = pointsWithValue[j].XDateTime.Value;
						double value = pointsWithValue[j].YValue.Value;

						var ts = new TimeSpan();
						int count = 1;
						for (int i = 1; i < groupCount; i++) {
							if (j + i < pointsWithValue.Count) {
								var pointCurrent = pointsWithValue[j + i];
								var pointPrevious = pointsWithValue[j + i - 1];
								switch (serie.GroupOperation) {
									case MathematicOperator.SUM:
										if (pointCurrent.YValue != null)
											value += pointCurrent.YValue.Value;
										break;
									case MathematicOperator.AVG:
										if (pointCurrent.YValue != null)
											value += pointCurrent.YValue.Value;
										break;
									case MathematicOperator.MAX:
										if (pointCurrent.YValue != null)
											value = Math.Max(value, pointCurrent.YValue.Value);
										break;
									case MathematicOperator.MIN:
										if (pointCurrent.YValue != null)
											value = Math.Min(value, pointCurrent.YValue.Value);
										break;
								}
								if (pointCurrent.XDateTime != null && pointPrevious.XDateTime != null)
									ts += pointCurrent.XDateTime.Value - pointPrevious.XDateTime.Value;
								count++;
							}
						}
						if (serie.GroupOperation == MathematicOperator.AVG)
							value = value / count;

						j += groupCount;
						var point = new DataPoint() {
							XDateTime = startDate + new TimeSpan(ts.Ticks / count),
							YValue = value,
							SeriesName = serie.Name,
						};
						pointsFinal.Add(point);
					}
					serie.Points = pointsFinal;
				}
			}
		}
		#endregion

		#region GetMinMax
		/// <summary>
		/// Gets the min max of XValues and YValues of a Serie.
		/// </summary>
		/// <param name="s">The s.</param>
		/// <param name="MinX">The min X.</param>
		/// <param name="MaxX">The max X.</param>
		/// <param name="MinY">The min Y.</param>
		/// <param name="MaxY">The max Y.</param>
		public static void GetMinMax(DataSerie s, out double MinX, out double MaxX, out double MinY, out double MaxY) {
			var minX = double.MaxValue;
			var minY = double.MaxValue;
			var maxX = double.MinValue;
			var maxY = double.MinValue;

			List<DataPoint> pointsWithValue = s.PointsWithValue;

			foreach (var e in pointsWithValue) {
				if (e.YValue != null) {
					minY = Math.Min((double)e.YValue, minY);
					maxY = Math.Max((double)e.YValue, maxY);
				}
				if (e.XValue != null) {
					minX = Math.Min((double)e.XValue, minX);
					maxX = Math.Max((double)e.XValue, maxX);
				}
			}

			MaxX = maxX;
			MinX = minX;
			MinY = minY;
			MaxY = maxY;
		}
		#endregion

		#region GroupByDateTime
		/// <summary>
		/// Groups 2 different DataSeries by their DateTime in order to easily compare them.
		/// </summary>
		/// <param name="serie1">The serie1.</param>
		/// <param name="serie2">The serie2.</param>
		/// <returns></returns>
		public static Dictionary<DateTime, Tuple<double?, double?>> GroupByDateTime(DataSerie serie1, DataSerie serie2) {
			var dictionary = new Dictionary<DateTime, Tuple<double?, double?>>();
			serie1.PointsWithValue.ForEach(p => {
				if (p.XDateTime != null && !dictionary.ContainsKey(p.XDateTime.Value)) {
					dictionary.Add(p.XDateTime.Value, new Tuple<double?, double?>(p.YValue, null));
				}

			});
			serie2.PointsWithValue.ForEach(p => {
				Tuple<double?, double?> t = null;
				if (p.XDateTime != null && dictionary.TryGetValue(p.XDateTime.Value, out t)) {
					t = new Tuple<double?, double?>(t.Item1, p.YValue);
					dictionary.Remove(p.XDateTime.Value);
					dictionary.Add(p.XDateTime.Value, t);
				}
			});
			return dictionary;
		}

		/// <summary>
		/// Groups 2 different DataSeries by their DateTime in order to easily compare them, only return tuple where both dataseries have values.
		/// </summary>
		/// <param name="serie1">The serie1.</param>
		/// <param name="serie2">The serie2.</param>
		public static Dictionary<DateTime, Tuple<double, double>> GroupByDateTimeExact(DataSerie serie1, DataSerie serie2) {
			var dictionary = GroupByDateTime(serie1, serie2);
			var dictionaryExact = new Dictionary<DateTime, Tuple<double, double>>();
			foreach (var kvp in dictionary) {
				if (kvp.Value.Item1.HasValue && kvp.Value.Item2.HasValue) {
					dictionaryExact.Add(kvp.Key, new Tuple<double, double>(kvp.Value.Item1.Value, kvp.Value.Item2.Value));
				}
			}
			return dictionaryExact;
		}

		#endregion

		#region FillMissingValues
		/// <summary>
		/// Fill missing values based on the startdate and enddate of the Chart.
		/// </summary>
		/// <param name="serie">the serie.</param>
		/// <param name="timeinterval">the time interval.</param>
		/// <param name="startDate">the start date.</param>
		/// <param name="endDate">the end date.</param>
		/// <param name="fillMissingValues">if set to <c>true</c> [fill missing values].</param>
		/// <param name="finalTimeZone">The final time zone.</param>
		public static void FillMissingValues(DataSerie serie, AxisTimeInterval timeinterval, DateTime startDate, DateTime endDate, bool fillMissingValues = false, TimeZoneInfo finalTimeZone = null) {
			if (timeinterval != AxisTimeInterval.None && timeinterval != AxisTimeInterval.Global) {
				var elements = serie.Points.Where(p => p.XDateTime.HasValue).ToDictionary(p => p.XDateTime != null ? p.XDateTime.Value : new DateTime());
				//We create here the missing elements with null value
				var date = startDate.UpdateByInterval(timeinterval);
				while (date <= endDate) {
					if (elements.Keys.Contains(date) == false) {
						serie.Points.Add(new DataPoint {
							SeriesName = serie.Name,
							XDateTime = date,
							YValue = null,
							Name = date.ToString(timeinterval.GetFormat())
						});
					}
					date = date.AddInterval(timeinterval, 1);
				}
				//we sort the serie by Acquisition Datetime.
				SortPoints(serie);

				//we fill the missing values if the option is activated.
				List<DataPoint> pointsWithValue = serie.PointsWithValue;

				if (fillMissingValues && pointsWithValue.Count > 0) {
					var value = pointsWithValue[0].YValue.Value;
					foreach (var p in serie.Points) {
						if (p.YValue.HasValue) {
							value = p.YValue.Value;
						}
						else {
							p.YValue = value;
						}
					}
				}
			}
		}


		#endregion

		#region Sort
		/// <summary>
		/// Sorts the specified series.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="displayLegendSort">The display legend sort.</param>
		/// <param name="limit">The limit.</param>
		/// <returns></returns>
		public static List<DataSerie> Sort(List<DataSerie> series, ChartLegendSort displayLegendSort, ChartLimitSeriesNumber limit = ChartLimitSeriesNumber.DisplayAll) {
			var retVal = new List<DataSerie>();
			if (series != null && series.Count > 0) {
				//we only keep the series that are not hidden
				if (limit != ChartLimitSeriesNumber.DisplayAll)
					series = series.Where(s => !s.Hidden && s.HasPointsWithValue).ToList();
				switch (displayLegendSort) {
					case ChartLegendSort.ByNameAsc:
						retVal = series.OrderBy(s => s.Name).ToList();
						break;

					case ChartLegendSort.ByNameDesc:
						retVal = series.OrderByDescending(s => s.Name).ToList();
						break;

					case ChartLegendSort.ByLegendEntryValueAsc:
						retVal = series.OrderBy(s => Project(s).YValue).ToList();
						break;

					case ChartLegendSort.ByLegendEntryValueDesc:
						retVal = series.OrderByDescending(s => Project(s).YValue).ToList();
						break;
				}
			}
			if (limit != ChartLimitSeriesNumber.DisplayAll && retVal.Count > 0) {
				var count = Math.Min(limit.ParseAsInt(), retVal.Count);
				retVal = retVal.GetRange(0, count);
			}

			return retVal;
		}


		/// <summary>
		/// Sort the list of Points by XDateTime or Name
		/// </summary>
		/// <param name="serie">the serie.</param>
		/// 
		public static void SortPoints(DataSerie serie) {

			if (serie.Points != null) {
				if (serie.IsDateTime) {
					serie.Points = (from p in serie.Points orderby p.XDateTime select p).ToList();
				}
				else {
					serie.Points = (from p in serie.Points orderby p.Name select p).ToList();
				}
			}
		}


		/// <summary>
		/// Sort the list of points for each serie in the collection.
		/// </summary>
		/// <param name="series">the series list.</param>
		/// 
		public static void SortPoints(List<DataSerie> series) {
			series.ForEach(s => SortPoints(s));
		}
		#endregion

		#region FixName
		/// <summary>
		/// Fixes the name of a DataSerie that could contains token like caracteres ie :%saved...
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		public static string FixName(string name) {
			if (string.IsNullOrEmpty(name) == false) {
				var retVal = name.Replace("%", "\\% ");
				return retVal;
			}
			return name;
		}
		#endregion

		#region OffsetByTimeinterval
		/// <summary>
		/// Offsets by timeinterval.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="frequency">The frequency.</param>
		/// <param name="interval">The interval.</param>
		public static void OffsetByTimeinterval(DataSerieCollection series, int frequency, AxisTimeInterval interval) {
			if (series == null) return;
			var list = series.ToList();
			foreach (var s in list) {
				foreach (var p in s.Points) {
					if (p.XDateTime.HasValue)
						p.XDateTime = p.XDateTime.Value.AddInterval(interval, frequency);
				}
			}
		}
		#endregion
	}
}

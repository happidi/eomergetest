﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Singleton Helper for getting class descriptions at run time.
	/// </summary>
	public static class ClassFinder {
		/// <summary>
		/// Gets or sets the path.
		/// </summary>
		/// <value>
		/// The path.
		/// </value>
		public static string BinPath { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="ClassFinder"/> class.
		/// </summary>
		static ClassFinder() {
			BinPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
		}


		/// <summary>
		/// Gets the workflows from path.
		/// </summary>
		/// <returns></returns>
		public static List<Type> GetClassFromPathThatInheritFromType(string typeName) {
			AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += CurrentDomain_ReflectionOnlyAssemblyResolve;
			List<Type> retVal = new List<Type>();
			DirectoryInfo directoryInfo = new DirectoryInfo(BinPath);
			var filesInfos = directoryInfo.GetFiles("Vizelia*.dll");
			foreach (var fileInfo in filesInfos) {
				try {
					var assembly = Assembly.ReflectionOnlyLoadFrom(fileInfo.FullName);
					IEnumerable<Type> collection = assembly.GetTypes().Where(type => IsTypeOf(type, typeName)
					                                                           && !type.IsAbstract && !type.IsInterface);
					if (collection.Count() != 0) {
						 assembly = Assembly.LoadFrom(fileInfo.FullName);
						 collection = assembly.GetTypes().Where(type => IsTypeOf(type, typeName)
																				   && !type.IsAbstract && !type.IsInterface);
						retVal.AddRange(collection);
					}
					

				}
				catch { }
			}


			return retVal;
		}

		/// <summary>
		/// Determines whether [is type of] [the specified type].
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="typeName">Name of the type.</param>
		/// <returns>
		///   <c>true</c> if [is type of] [the specified type]; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsTypeOf(Type type, string typeName) {
			var currentType = type;
			Type[] typeInterfaces = currentType.GetInterfaces();
			if (typeInterfaces.Count(typeInterface => typeInterface.FullName == typeName) != 0) {
				return true;
			}
			while (currentType != null) {
				if (currentType.FullName == typeName) {
					return true;
				}
				currentType = currentType.BaseType;
			}
			return false;
		}


		/// <summary>
		/// Handles the ReflectionOnlyAssemblyResolve event of the CurrentDomain control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="args">The <see cref="System.ResolveEventArgs"/> instance containing the event data.</param>
		/// <returns></returns>
		private static Assembly CurrentDomain_ReflectionOnlyAssemblyResolve(object sender, ResolveEventArgs args) {
			string[] parts = args.Name.Split(',');
			string file = BinPath + "\\" + parts[0].Trim() + ".dll";
			if (File.Exists(file)) {
				Assembly asm = Assembly.ReflectionOnlyLoadFrom(file);
				return asm;
			}
			return Assembly.ReflectionOnlyLoad(args.Name);
		}
	}
}
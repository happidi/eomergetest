﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// StreamResult helper class
	/// </summary>
	public static class StreamResultHelper {


		/// <summary>
		/// Gets the stream from HTML.
		/// </summary>
		/// <param name="htmlContent">Content of the HTML.</param>
		/// <returns></returns>
		public static StreamResult GetStreamFromHtml(string htmlContent) {
			var ms = new MemoryStream();
			var sw = new StreamWriter(ms);
			sw.Write(htmlContent);
			sw.Flush();
			ms.Seek(0, SeekOrigin.Begin);
			var retVal = new StreamResult(ms, MimeType.Html);
			return retVal;
		}

		/// <summary>
		/// Encodes the specified stream.
		/// </summary>
		/// <param name="stream">The stream.</param>
		/// <returns></returns>
		[Obsolete("The stream result can be saved directly into the cache, no need for encode/decode")]
		public static byte[] Encode(StreamResult stream) {
			byte[] retVal = null;
			if (stream != null && stream.ContentStream != null) {
				retVal = stream.ContentStream.ToArray();
			}
			return retVal;
		}

		/// <summary>
		/// Encodes the specified stream.
		/// </summary>
		/// <param name="array">The bytes array.</param>
		/// <param name="mimetype">The mimetype.</param>
		/// <returns></returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
		[Obsolete("The stream result can be saved directly into the cache, no need for encode/decode")]
		public static StreamResult Decode(byte[] array, string mimetype) {
			MemoryStream retVal = null;
			if (array != null) {
				retVal = new MemoryStream(array, 0, array.Length, true, true);
			}
			return new StreamResult(retVal, mimetype);
		}
	}
}

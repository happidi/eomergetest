﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Used by the KnownType attribute on BaseBusinessEntity.
	/// </summary>
	public static class KnownTypeFinder {

		private static readonly List<Type> knownTypes;

		static KnownTypeFinder() {
			knownTypes = BuildListOfKnownTypes();
		}

		/// <summary>
		/// Builds the list of known types.
		/// </summary>
		/// <returns></returns>
		private static List<Type> BuildListOfKnownTypes() {
			Assembly containingAssembly = typeof(KnownTypeFinder).Assembly;
			return containingAssembly.GetTypes()
				.Select(t => t)
				.Where(t => t.IsSubclassOf(typeof(BaseBusinessEntity)) && t.IsGenericType == false && t.GetCustomAttributes(typeof(DataContractAttribute), false).Length > 0 )
				.ToList();
		}


		/// <summary>
		/// Gets all the known types for the WCF service. called autmatically by the ServiceKnownType attribute which is placed on IBaseWCF
		/// </summary>
		/// <param name="customAttributeProvider">The custom attribute provider.</param>
		/// <returns>List of all known types</returns>
		public static IEnumerable<Type> GetKnownTypes(ICustomAttributeProvider customAttributeProvider) {
			return knownTypes;
		}
	}

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A static helper for Unit conversion.
	/// </summary>
	public static class UnitHelper {

		/// <summary>
		/// Converts a Temperature between 2 Units.
		/// </summary>
		/// <param name="value">The input value.</param>
		/// <param name="inputUnit">The input unit.</param>
		/// <param name="outputUnit">The output unit.</param>
		/// <returns></returns>
		public static double ConvertTemperature(double value, WeatherUnit inputUnit, WeatherUnit outputUnit) {
			double retVal = value;
			if (inputUnit != outputUnit) {
				if (inputUnit == WeatherUnit.Celsius && outputUnit == WeatherUnit.Fahrenheit) {
					retVal = retVal * 5d / 9d + 32;
				}
				if (inputUnit == WeatherUnit.Fahrenheit && outputUnit == WeatherUnit.Celsius) {
					retVal = (retVal - 32d) * 5d / 9d;
				}
			}
			return retVal;
		}
	}
}

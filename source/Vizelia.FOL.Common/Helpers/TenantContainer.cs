﻿using System.Collections.Concurrent;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// The tenant container holds a dictionnary of T object for a specific tenant.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class TenantContainer<T> where T : new() {
		private readonly ConcurrentDictionary<string, T> m_Dictionary = new ConcurrentDictionary<string, T>();
		
		/// <summary>
		/// Gets the entry for a specific tenant.
		/// </summary>
		public T Entry {
			get {
				string applicationName = ContextHelper.ApplicationName;
				T item = m_Dictionary.GetOrAdd(applicationName, s => new T());

				return item;
			}
			set {
				string applicationName = ContextHelper.ApplicationName;
				m_Dictionary[applicationName] = value;
			}
		}
	}
}

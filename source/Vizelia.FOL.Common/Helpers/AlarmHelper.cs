﻿using System;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Encapsulates extension for the AlarmDefinition and AlarmInstance class.
	/// </summary>
	public static class AlarmHelper {

		/// <summary>
		/// Gets the alarm instance body description (to use in mails or actionrequest).
		/// </summary>
		/// <param name="instance">The instance.</param>
		/// <returns></returns>
		public static string GetAlarmInstanceBody(AlarmInstance instance) {

			var sb = new StringBuilder();
			var alarm = instance.AlarmDefinition;
			if (alarm != null) {

			    var alarmSource = DetermineAlarmSource(instance);

				sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_name, instance.Name));
				sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_alarmdefinition_classification, alarm.ClassificationItemLongPath));
                sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_alarminstance_alarmsource, alarmSource == AlarmDefinitionSource.Chart ? Langue.msg_chart : Langue.msg_meter));
			    if (alarmSource == AlarmDefinitionSource.Meter) {
                    sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_alarminstance_meterspatialpath, instance.LocationLongPath));
			    }
			    sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_alarminstance_datetime, GetInstanceDateTime(instance, alarm)));
				sb.Append(string.Format("<b>{0}</b> : {1}", Langue.msg_alarminstance_value, instance.Value.ToString("f")));
				if (alarm.UsePercentageDelta) {
					sb.Append(string.Format(" ({0})", instance.Description));
				}
				sb.AppendLine("<br/>");
				if (alarm.Condition == FilterCondition.Script) {
					sb.Append(string.Format("<b>{0}</b> : {1}", Langue.msg_alarminstance_rule, instance.Description));
				}
				else {
					sb.Append(string.Format("<b>{0}</b> : {1}", Langue.msg_alarminstance_rule, alarm.Condition.GetLocalizedText()));

					if (alarm.Threshold.HasValue) {
						sb.Append(" " + alarm.Threshold.Value.ToString("f") + (alarm.UsePercentageDelta ? " %" : ""));
					}
					if (alarm.Threshold2.HasValue) {
						sb.Append(" " + Langue.msg_enum_logicaloperator_and + " " + alarm.Threshold2.Value.ToString("f") + (alarm.UsePercentageDelta ? " %" : ""));
					}
				}
				sb.AppendLine("<br/>");
				sb.AppendLine("<br/>");
			}
			else {
				var rule = instance.MeterValidationRule;
				if (rule != null) {
					//sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_metervalidationrule, rule.Title));
					sb.AppendLine(string.Format("<b>{0}</b> : {1}  <br/>", Langue.msg_meter, instance.Name));
                    sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_alarminstance_meterspatialpath, instance.LocationLongPath));
					sb.AppendLine(string.Format("<b>{0}</b> :  {1} <br/> ", Langue.msg_alarminstance_metervalidationrulealarmtype, instance.MeterValidationRuleAlarmType.GetLocalizedText()));

					switch (instance.MeterValidationRuleAlarmType) {
						case MeterValidationRuleAlarmType.Missing:
							if (instance.MeterDataAcquisitionDateTimeStart.HasValue)
								sb.AppendLine(string.Format("<b>{0} - {1}</b> : {2} <br/>", Langue.msg_enum_metervalidationrulealarmtype_missing, Langue.msg_alarminstance_startdate, instance.MeterDataAcquisitionDateTimeStart.Value.ToString(AxisTimeInterval.Minutes.GetFormat())));
							if (instance.MeterDataAcquisitionDateTimeEnd.HasValue)
								sb.AppendLine(string.Format("<b>{0} - {1}</b> : {2} <br/>", Langue.msg_enum_metervalidationrulealarmtype_missing, Langue.msg_alarminstance_enddate, instance.MeterDataAcquisitionDateTimeEnd.Value.ToString(AxisTimeInterval.Minutes.GetFormat())));
							break;

						case MeterValidationRuleAlarmType.Late:
							sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_meterdatavalidationrule_lastdatadate, instance.InstanceDateTime.ToString(AxisTimeInterval.Minutes.GetFormat())));
							break;

						default:
							sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_alarminstance_datetime, instance.InstanceDateTime.ToString(AxisTimeInterval.Minutes.GetFormat())));
							break;
					}

					switch (instance.MeterValidationRuleAlarmType) {
						case MeterValidationRuleAlarmType.IndexDecrease:
						case MeterValidationRuleAlarmType.RecurringValues:
						case MeterValidationRuleAlarmType.Spike:
						case MeterValidationRuleAlarmType.ValueOutsideBoundary:
							sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_alarminstance_value, instance.Value.ToString("f")));
							break;

						case MeterValidationRuleAlarmType.Validity:
							sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_meterdata_validity, rule.Validity.Value.GetLocalizedText()));
							break;
					}

					switch (instance.MeterValidationRuleAlarmType) {
						case MeterValidationRuleAlarmType.ValueOutsideBoundary:
							if (rule.MinValue.HasValue)
								sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_metervalidationrule_minvalue, rule.MinValue.Value.ToString("f")));
							if (rule.MaxValue.HasValue)
								sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_metervalidationrule_maxvalue, rule.MaxValue.Value.ToString("f")));
							break;
					}

					switch (instance.MeterValidationRuleAlarmType) {
						case MeterValidationRuleAlarmType.Missing:
							sb.AppendLine(string.Format("<b>{0}</b> : {1} {2} <br/>", Langue.msg_metervalidationrule_datafrequency, rule.AcquisitionFrequency, rule.AcquisitionInterval.GetLocalizedText()));
							sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_enum_metervalidationrulealarmtype_missing, instance.Description));
							break;
						case MeterValidationRuleAlarmType.Spike:
							sb.AppendLine(string.Format("<b>{0}</b> : {1} > {2} <br/>", Langue.msg_metervalidationrule_spike, rule.Description, rule.SpikeDetection.HasValue ? rule.SpikeDetection.Value.ToString() : ""));
							break;

						case MeterValidationRuleAlarmType.RecurringValues:
							sb.AppendLine(string.Format("<b>{0}</b> : {1} <br/>", Langue.msg_enum_metervalidationrulealarmtype_recurringvalues, instance.Description));
							break;
					}
					sb.AppendLine("<br/>");
				}
			}
			var retVal = sb.ToString();
			return retVal;
		}

        /// <summary>
        /// Determines the alarm source.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns></returns>
        private static AlarmDefinitionSource DetermineAlarmSource(AlarmInstance instance) {
            AlarmDefinitionSource source = String.IsNullOrEmpty(instance.ChartInstanceDateTime) ? AlarmDefinitionSource.Meter : AlarmDefinitionSource.Chart;            
            return source;
        }

	    /// <summary>
        /// Gets the inctacnce date time string according to the source kind
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="alarm"></param>
        /// <returns></returns>
	    private static string GetInstanceDateTime(AlarmInstance instance, AlarmDefinition alarm) {
            if (!String.IsNullOrEmpty(instance.KeyChart) && !String.IsNullOrEmpty(instance.ChartInstanceDateTime)) {
                return instance.ChartInstanceDateTime;
            }
	        return String.Format(instance.InstanceDateTime.ConvertTimeFromUtc(alarm.GetTimeZone()).ToString(AxisTimeInterval.Minutes.GetFormat()) + " ({0}{1})",
	                alarm.GetTimeZone().BaseUtcOffset.CompareTo(TimeSpan.Zero) > 0 ? "+" : "",
	                alarm.GetTimeZone().BaseUtcOffset);
	    }
	}



}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A helper class for working with documents in entities
	/// </summary>
	public class DocumentHelper {


		/// <summary>
		/// Checks the difference between new and old items and gets the documents that need to be deleted
		/// </summary>
		/// <param name="oldItem">The old item.</param>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public static List<string> GetDocumentIdsToDelete(ISupportDocuments oldItem, ISupportDocuments item) {
			var documentIdsToDelete = new List<string>();
			if ((oldItem == null) || (item == null)) {
				return documentIdsToDelete;
			}

			var documentProperties = GetDocumentProperties(item);
			foreach (var documentProperty in documentProperties) {
				if (IsNewDocument(documentProperty, item, oldItem)) {
					var oldDocumentId =
						GetDocumentIds(oldItem, new List<PropertyInfo>() { documentProperty }).First();
					documentIdsToDelete.Add(oldDocumentId);
				}
			}
			return documentIdsToDelete;
		}

		/// <summary>
		/// Determines whether the specified document property is a new document .
		/// </summary>
		/// <param name="documentProperty">The document property.</param>
		/// <param name="item">The item.</param>
		/// <param name="oldItem">The old item.</param>
		public static bool IsNewDocument(PropertyInfo documentProperty, ISupportDocuments item, ISupportDocuments oldItem) {
			var oldDocumentId = GetDocumentIds(oldItem, new List<PropertyInfo>() { documentProperty }).FirstOrDefault();
			var documentId = GetDocumentIds(item, new List<PropertyInfo>() { documentProperty }).FirstOrDefault();

			if ((oldDocumentId == null) || (oldDocumentId == documentId) || (string.IsNullOrEmpty(documentId)))
				return false;
			else
				return true;

		}

		/// <summary>
		/// Saves the document to session.
		/// </summary>
		/// <param name="documentContents">The document contents.</param>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="contentType">Type of the content.</param>
		/// <returns>
		/// A coded key for the document
		/// </returns>
		public static string PrepareDocument(byte[] documentContents, string fileName, string contentType) {
			var document = new Document(documentContents, fileName, contentType);
			return PrepareDocument(document);
		}

		/// <summary>
		/// Prepares the document.
		/// </summary>
		/// <param name="document">The document.</param>
		/// <returns></returns>
		public static string PrepareDocument(Document document) {
			Guid guid = Guid.NewGuid();
			var documentIdentifier = EncodeDocumentKey(guid.ToString());
			SessionService.Add(documentIdentifier, document);
			return documentIdentifier;
		}

		/// <summary>
		/// Saves the document to session.
		/// </summary>
		/// <param name="documentStream">The document stream.</param>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="contentType">Type of the content.</param>
		/// <returns>
		/// A coded key for the document
		/// </returns>
		public static string PrepareDocument(Stream documentStream, string fileName, string contentType) {
			var documentContents = documentStream.GetBytes();
			var retVal = PrepareDocument(documentContents, fileName, contentType);
			return retVal;
		}

		/// <summary>
		/// Retrieves the document from session.
		/// </summary>
		/// <param name="documentUniqueIdentifier">The document unique identifier.</param>
		/// <param name="removeFromSession">if set to <c>true</c>, remove the document from the session.</param>
		/// <returns>
		/// The document
		/// </returns>
		public static Document RetrieveDocument(string documentUniqueIdentifier, bool removeFromSession = true) {
			if (IsValidDocumentIdentifier(documentUniqueIdentifier)) {
				var retVal = SessionService.Get(documentUniqueIdentifier) as Document;

				if (removeFromSession) {
					SessionService.Remove(documentUniqueIdentifier);
				}

				return retVal;
			}

			throw new KeyNotFoundException();
		}

		/// <summary>
		/// Determines whether the specified document identifier is a valid document identifier .
		/// </summary>
		/// <param name="documentIdentifier">The document identifier.</param>
		/// <returns>
		///   <c>true</c> if [is valid document identifier] [the specified document identifier]; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsValidDocumentIdentifier(string documentIdentifier) {
			if (documentIdentifier == null) {
				throw new ArgumentNullException("documentIdentifier");
			}

			var retVal = documentIdentifier.StartsWith(SessionKey.DocumentSessionPrefix);
			return retVal;
		}



		/// <summary>
		/// Adds the document prefix to document identifier.
		/// </summary>
		/// <param name="documentIdentifier">The document identifier.</param>
		/// <returns></returns>
		public static string EncodeDocumentKey(string documentIdentifier) {
			var retVal = SessionKey.DocumentSessionPrefix + documentIdentifier;
			return retVal;
		}

		/// <summary>
		/// Removes the document prefix from document identifier.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static string DecodeDocumentKey(string value) {
			if (IsValidDocumentIdentifier(value)) {
				string retVal = value.Replace(SessionKey.DocumentSessionPrefix, "");
				return retVal;
			}
			throw new KeyNotFoundException();

		}

		/// <summary>
		/// Gets the document properties.
		/// </summary>
		/// <param name="itemWithDocuments">The item with documents.</param>
		/// <returns></returns>
		public static IEnumerable<PropertyInfo> GetDocumentProperties(ISupportDocuments itemWithDocuments) {
			var documentPropertiesNames = itemWithDocuments.GetDocumentFields();
			var documentProperties = itemWithDocuments.GetType().GetProperties().Where(
				prop => documentPropertiesNames.Contains(prop.Name));
			return documentProperties;
		}

		/// <summary>
		/// Gets the document ids based on the doucment properties
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="documentProperties">The document properties.</param>
		/// <returns></returns>
		public static IEnumerable<string> GetDocumentIds(ISupportDocuments item, IEnumerable<PropertyInfo> documentProperties) {
			var retVal = documentProperties.Select(documentProperty => documentProperty.GetValue(item, null))
				.Where(documentValue => documentValue != null)
				.OfType<string>()
				.Where(docId => !String.IsNullOrEmpty(docId) && docId.Trim() != string.Empty);
			return retVal;
		}

		/// <summary>
		/// Gets the type of the pset document data.
		/// </summary>
		/// <value>
		/// The type of the pset document data.
		/// </value>
		public static string PsetDocumentDataType {
			get { return "document"; }
		}

		
	}
}

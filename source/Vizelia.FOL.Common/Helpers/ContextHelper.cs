﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ServiceModel;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A helper class for context tasks.
	/// </summary>
	public static class ContextHelper {
		private const string cont_key_url = "contexthelper_url";
		private const string cont_key_applicationname = "contexthelper_applicationname";
		private const string cont_key_requestid = "contexthelper_requestid";
		private const string cont_key_jobruninstancerequestid = "contexthelper_jobruninstancerequestid";
		private const string cont_key_keyjob = "contexthelper_keyjob";
		/// <summary>
		/// Gets the current request.
		/// </summary>
		private static ICurrentRequestState Current {
			get {
				if (OperationContext.Current != null) {
					return new WCFOperationContextRequestState();
				}

				if (HttpContext.Current != null) {
					return new AspNetHttpContextRequestState();
				}

				return new ThreadStaticRequestState();
			}
		}

		/// <summary>
		/// Gets the items.
		/// </summary>
		public static IDictionary<string, object> Items {
			get { return Current.Items; }
		}

		/// <summary>
		/// Gets the stack for the given type by its key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		public static Stack<T> GetStack<T>(string key) {
			Stack<T> stack;
			object stackObject = Current.Get<object>(key);

			if (stackObject != null) {
				stack = (Stack<T>)stackObject;
			}
			else {
				stack = new Stack<T>();
				Current.Add(key, stack);
			}

			return stack;
		}

		/// <summary>
		/// 
		/// Add a value to the context
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		public static void Add(string key, object value) {
			Current.Add(key, value);
		}

		/// <summary>
		/// Gets the <see cref="System.Object"/> with the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <value></value>
		public static object Get(string key) {
			try {
				return Current.Get<object>(key);
			}
			catch { return null; }
		}

		/// <summary>
		/// Removes the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		public static void Remove(string key){
			Current.Remove(key);
		}

		/// <summary>
		/// Gets the URL of the context.
		/// </summary>
		public static string Url {
			get {
				// we first check in the current implementation of the context
				var url = Current.Url;
				if (!String.IsNullOrEmpty(url))
					return url;
				// if null we then check if a context variable has been set 
				url = Get(cont_key_url) as string;
				return url;
			}
			set { Add(cont_key_url, value); }
		}

		/// <summary>
		/// Gets the requestId.
		/// </summary>
		public static string RequestId {
			get {
				var requestId = Get(cont_key_requestid) as string;
				if (requestId == null) {
					requestId = Guid.NewGuid().ToString();
					Add(cont_key_requestid, requestId);
				}
				return requestId;
			}
			set {
				Add(cont_key_requestid, value);
			}
		}

		/// <summary>
		/// Gets the ApplicationName from the context.
		/// </summary>
		public static string ApplicationName {
			get {
				if (TenantProvisioningService.Mode == MultitenantHostingMode.Isolated) {
					return VizeliaConfiguration.Instance.Deployment.ApplicationName;
				}

				string defaultApplicationName = VizeliaConfiguration.Instance == null ? null : VizeliaConfiguration.Instance.Deployment.ApplicationName;

				string retval = Get(cont_key_applicationname) as string ?? defaultApplicationName;
				return retval;
			}
			set {
				if (value != null)
					Add(cont_key_applicationname, value);
			}
		}

		/// <summary>
		/// Gets or sets the job run instance request id.
		/// </summary>
		/// <value>
		/// The job run instance request id.
		/// </value>
		public static string JobRunInstanceRequestId {
			get {
				var jobRunInstanceRequestId = Get(cont_key_jobruninstancerequestid) as string;
				if (jobRunInstanceRequestId == null) {
					return string.Empty;
				}
				return jobRunInstanceRequestId;
			}
			set {
				Add(cont_key_jobruninstancerequestid, value);
			}
		}

		/// <summary>
		/// Gets or sets the key job.
		/// </summary>
		/// <value>
		/// The key job.
		/// </value>
		public static string KeyJob {
			get {
				var keyJob = Get(cont_key_keyjob) as string;
				if (keyJob == null) {
					return string.Empty;
				}
				return keyJob;
			}
			set {
				Add(cont_key_keyjob, value);
			}
		}
	}


	/// <summary>
	/// 
	/// </summary>
	public interface ICurrentRequestState {
		/// <summary>
		/// Gets the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		T Get<T>(string key);

		/// <summary>
		/// Adds the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		void Add(string key, object value);


		/// <summary>
		/// Removes the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		void Remove(string key);


		/// <summary>
		/// Gets the URL of the context.
		/// </summary>
		string Url { get; }

		/// <summary>
		/// Gets the items.
		/// </summary>
		IDictionary<string, object> Items { get; }
	}

	/// <summary>
	/// Aspnet Context wrapper
	/// </summary>
	public class AspNetHttpContextRequestState : ICurrentRequestState {
		private const string const_contexthelper_dictionary = "contexthelperdictionary";
		private static readonly object m_SyncObject = new object();

		private IDictionary<string, object> internalDictionary {
			get {
				if (HttpContext.Current.Items.Contains(const_contexthelper_dictionary))
					return HttpContext.Current.Items[const_contexthelper_dictionary] as IDictionary<string, object>;
				else {
					lock (m_SyncObject) {
						if (HttpContext.Current.Items.Contains(const_contexthelper_dictionary)) {
							return HttpContext.Current.Items[const_contexthelper_dictionary] as IDictionary<string, object>;
						}

						IDictionary<string, object> dictionary = new ConcurrentDictionary<string, object>();
						HttpContext.Current.Items.Add(const_contexthelper_dictionary, dictionary);
						return dictionary;
					}
				}
			}
		}
		/// <summary>
		/// Gets the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public T Get<T>(string key) {
			if (!internalDictionary.ContainsKey(key)) {
				return default(T);
			}
			else {
				return (T)internalDictionary[key];
			}
		}

		/// <summary>
		/// Adds the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		public void Add(string key, object value) {
			internalDictionary[key] = value;
		}

		/// <summary>
		/// Removes the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		public void Remove(string key){
			internalDictionary.Remove(key);
		}


		/// <summary>
		/// Gets the URL of the context.
		/// </summary>
		public string Url {
			get { return HttpContext.Current.Request.Url.AbsoluteUri; }
		}


		/// <summary>
		/// Gets the items.
		/// </summary>
		public IDictionary<string, object> Items {
			get { return internalDictionary; }
		}
	}

	/// <summary>
	/// WCF Operation context
	/// </summary>
	public class WCFOperationContextRequestState : ICurrentRequestState {

		private static readonly object m_SyncObject = new object();

		private IDictionary<string, object> State {

			get {
				var extension = OperationContext.Current.Extensions.Find<WCFContextItemsExtension>();

				if (extension == null) {
					lock (m_SyncObject) {
						extension = OperationContext.Current.Extensions.Find<WCFContextItemsExtension>();

						if (extension == null) {
							extension = new WCFContextItemsExtension();
							OperationContext.Current.Extensions.Add(extension);
						}

					}
				}
				return extension.State;
			}
		}

		/// <summary>
		/// Gets the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public T Get<T>(string key) {
			object result;
			if (State.TryGetValue(key, out result)) {
				return (T)result;
			}
			return default(T);
		}



		/// <summary>
		/// Adds the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		public void Add(string key, object value) {
			State[key] = value;
		}

		/// <summary>
		/// Removes the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		public void Remove(string key){
			State.Remove(key);
		}


		/// <summary>
		/// Gets the URL of the context.
		/// </summary>
		public string Url {
			get {
				return OperationContext.Current.Channel.LocalAddress.Uri.AbsoluteUri;
			}
		}

		/// <summary>
		/// Gets the items.
		/// </summary>
		public IDictionary<string, object> Items {
			get { return State; }
		}
	}

	/// <summary>
	/// Request State when no WCF context and no http context inplace (e.g. when spinning a new thread explicitly from asp.net or WCF)
	/// </summary>
	public class ThreadStaticRequestState : ICurrentRequestState {

		private Dictionary<string, object> internalDictionary {
			get { return m_dictionary ?? (m_dictionary = new Dictionary<string, object>()); }
		}

		[ThreadStatic]
		private static Dictionary<string, object> m_dictionary;

		/// <summary>
		/// Gets the specified key.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public T Get<T>(string key) {
			object result;
			if (internalDictionary.TryGetValue(key, out result)) {
				return (T)result;
			}
			return default(T);
		}

		/// <summary>
		/// Adds the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		public void Add(string key, object value) {
			internalDictionary[key] = value;
		}

		/// <summary>
		/// Removes the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		public void Remove(string key){
			internalDictionary.Remove(key);
		}


		/// <summary>
		/// Gets the URL of the context.
		/// </summary>
		public string Url { get; set; }


		/// <summary>
		/// Gets the items.
		/// </summary>
		public IDictionary<string, object> Items {
			get { return internalDictionary; }
		}
	}


	/// <summary>
	/// WCF Context Items Extension
	/// </summary>
	internal class WCFContextItemsExtension : IExtension<OperationContext> {

		public WCFContextItemsExtension() {
			State = new ConcurrentDictionary<string, object>();
		}

		/// <summary>
		/// Gets the state.
		/// </summary>
		public IDictionary<string, object> State { get; private set; }

		/// <summary>
		/// Attaches the specified owner.
		/// </summary>
		/// <param name="owner">The owner.</param>
		public void Attach(OperationContext owner) { }
		/// <summary>
		/// Detaches the specified owner.
		/// </summary>
		/// <param name="owner">The owner.</param>
		public void Detach(OperationContext owner) { }

	}

}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A class for tenant helper methods
	/// </summary>
	public static class TenantHelper {


		/// <summary>
		/// Gets the current tenant logo.
		/// </summary>
		/// <param name="keyTenant">The key tenant.</param>
		/// <returns></returns>
		public static String GetCurrentTenantLogoPath(string keyTenant) {

			string path = Path.Combine(Helper.GetPhysicalPath(), @"Images\logo_" + ContextHelper.ApplicationName + ".png");
			if (File.Exists(path)) {
				return path;
			}
			string safePath = Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"Images\logo_se_300.png");
			return File.Exists(safePath) ? safePath : string.Empty;
		}

		/// <summary>
		/// Gets the current tenant desktop image list.
		/// </summary>
		/// <returns></returns>
		public static List<ListElement> GetCurrentTenantDesktopImageList() {
			var path = Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"images\desktop");
			var retVal = new List<ListElement>();
			Directory.EnumerateFiles(path, "*.jpg").ToList().ForEach(file => {
				var finfo = new FileInfo(file);
				retVal.Add(new ListElement {
					Id = file,
					MsgCode = finfo.Name,
					Value = finfo.Name
				});

			});
			return retVal;
		}

		/// <summary>
		/// Gets the initiale dynamic display images path.
		/// </summary>
		/// <returns></returns>
		public static string GetInitialeDynamicDisplayImagesPath() {
			return Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"images\dynamicdisplay");
		}


		/// <summary>
		/// Runs the in different tenant context.
		/// </summary>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="action">The action.</param>
		/// <returns></returns>
		public static TResult RunInDifferentTenantContext<TResult>(string applicationName, Func<TResult> action) {
			string originalApplicationName = ContextHelper.ApplicationName;
			try {
				TResult retval;
				ContextHelper.ApplicationName = applicationName;
				retval = action();
				return retval;
			}
			finally {
				ContextHelper.ApplicationName = originalApplicationName;
			}
		}

		/// <summary>
		/// Runs the in different tenant context.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="action">The action.</param>
		public static void RunInDifferentTenantContext(string applicationName, Action action) {
			RunInDifferentTenantContext<object>(applicationName, () => {
				action();
				return null;
			});
		}

		/// <summary>
		/// Runs the in different tenant context under a specified username.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <param name="action">The action.</param>
		/// <param name="username">The username.</param>
		public static void RunInDifferentTenantContext(string applicationName, Action action, string username) {
			HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
			var currentPrincipal = System.Threading.Thread.CurrentPrincipal;
			string pageId;
			SessionService.SetAuthCookie(username, false, out pageId);


			try {
				RunInDifferentTenantContext<object>(applicationName, () => {
					System.Threading.Thread.CurrentPrincipal = HttpContext.Current.User;
					action();
					return null;
				});
			}
			finally {
				HttpContext.Current.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
				if (authCookie != null) {
					HttpContext.Current.Response.SetCookie(authCookie);
				}
				System.Threading.Thread.CurrentPrincipal = currentPrincipal;	
			}
			

			
		}
	}
}

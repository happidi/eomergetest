﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Net;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;
using System.Reflection;
using Vizelia.FOL.Infrastructure;

namespace Vizelia.FOL.Common.Helpers {
	/// <summary>
	/// WCF Ajax Javascript Helper class
	/// </summary>
	public static class WCFAjaxJavascriptHelper {



		/// <summary>
		/// cache suffix
		/// </summary>
		private const string const_cachekey_javascript = "ajaxjavascript";/// <summary>
		/// header to mark this is a real request that requires the /jsdebug or /js behavior to behave as normal and compile and return the js file
		/// </summary>
		private const string const_header_ajaxjavascript_rebuild = "rebuildajaxjavascript";



		/// <summary>
		/// Initializes the <see cref="WCFAjaxJavascriptHelper"/> class.
		/// </summary>
		static WCFAjaxJavascriptHelper() {
			ServicePointManager.ServerCertificateValidationCallback = (sender2, certificate, chain, sslPolicyErrors) => true;
		}


		/// <summary>
		/// Generates the Ajax Javascript file by leveraging cache mechanisms on top of WCFContracts assembly.
		/// </summary>
		public static void GenerateAjaxJavascript() {
			// when the request is asking for a rebuild version of the javascript we follow the normal path (i.e we let asp.net manage the request)
			if (IsAskingForRebuildRequest())
				return;

			// when the request does not ask explicitly for a rebuild we look into the cache and shortcut asp.net pipeline
            string result = GetJavascriptFromPersistedStore();
			if (string.IsNullOrEmpty(result)) {
				result = AskForRebuildRequest();
			}
			PersistJavascript(result);

			HttpContext.Current.Response.ContentType = MimeType.Json;
			HttpContext.Current.Response.Write(result);
			HttpContext.Current.Response.End();
		}


		/// <summary>
		/// Issues a request from the server in order to ask for the rebuilt version of the ajax javascript file.
		/// </summary>
		/// <returns></returns>
		private static string AskForRebuildRequest() {
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpContext.Current.Request.Url.AbsoluteUri);
			request.Headers.Add(const_header_ajaxjavascript_rebuild, HttpContext.Current.Request.Url.AbsolutePath);
			request.Timeout = 1000 * 60 * 10; // 10 minutes

			var httpresponse = request.GetResponse();
			StreamReader reader = new StreamReader(httpresponse.GetResponseStream());
			var retval = reader.ReadToEnd();
			reader.Close();
			httpresponse.Close();
			return retval;
		}

		/// <summary>
		/// Persist the javascript locally, does not use cache to avoid in memory mode expiration.
		/// </summary>
		/// <param name="javascript">The javascript.</param>
		private static void PersistJavascript(string javascript) {
            // delete all previous javascript files
            string prefix = EncodePreCacheKey();
            var files = Directory.GetFiles(VizeliaConfiguration.Instance.Deployment.TempDirectory, prefix + "*");
            foreach (var file in files){
                try{
                    File.Delete(file);
                }
                catch { }
            }

			string key = EncodeCacheKey();
		    string filePath = Path.Combine(VizeliaConfiguration.Instance.Deployment.TempDirectory, key);
            File.WriteAllText(filePath, javascript);
 

		}


        /// <summary>
        /// Gets the ajax javascript from cache.
        /// </summary>
        /// <returns></returns>
        private static string GetJavascriptFromPersistedStore() {
            string key = EncodeCacheKey();
            string filePath = Path.Combine(VizeliaConfiguration.Instance.Deployment.TempDirectory, key);

            string retval = null;
            
            if ( File.Exists(filePath)) {
                retval = File.ReadAllText(filePath);
            }
            return retval;
        }

		
		/// <summary>
		/// Encodes the cache key.
		/// </summary>
		/// <returns></returns>
		private static string EncodeCacheKey() {
			var assembly = Assembly.Load("Vizelia.FOL.WCFService.Contracts");
			string assemblyModuleVersionID = WCFHelper.GetAssemblyGuid(assembly).ToString();
            string retval = EncodePreCacheKey() + "_" + assemblyModuleVersionID;
			return retval;
		}

		/// <summary>
		/// Returns the protocol prefix
		/// </summary>
		/// <returns></returns>
		private static string ProtocolPrefix() {
			return HttpContext.Current.Request.IsSecureConnection ? "https" : "http";
		}

		/// <summary>
		/// Encodes the pre cache key.
		/// </summary>
		/// <returns></returns>
		private static string EncodePreCacheKey() {
		    string urlAsFileName = HttpContext.Current.Request.Url.AbsoluteUri.Replace("/", "_").Replace(":", "_");
		    return ProtocolPrefix() + "_" + const_cachekey_javascript + "_" + urlAsFileName;
		}

		/// <summary>
		/// Determines whether this request is asking for the dynamically generated version of the file.
		/// </summary>
		private static bool IsAskingForRebuildRequest() {
			string lockedFlag = HttpContext.Current.Request.Headers[const_header_ajaxjavascript_rebuild];
			var retval = !string.IsNullOrEmpty(lockedFlag);
			return retval;
		}
	}
}

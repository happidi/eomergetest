﻿
using System;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
	
	/// <summary>
	/// Helper for DateTime.
	/// </summary>
	public static class DateTimeHelper {

		/// <summary>
		/// Format to date and time.
		/// </summary>
		/// <param name="isWithSeconds">if set to <c>true</c> [is with seconds].</param>
		/// <returns></returns>
		public static string DateTimeFormat(bool isWithSeconds) {
			var format = Helper.LocalizeText("langue_dotnet_date_format") + " " + Helper.LocalizeText("langue_dotnet_time_format");
			var retVal = isWithSeconds ? format + Helper.LocalizeText("langue_dotnet_seconds_format") : format;
			return retVal;
		}

		/// <summary>
		/// Format to date.
		/// </summary>
		/// <returns></returns>
		public static string DateFormat() {
			return Helper.LocalizeText("langue_dotnet_date_format");
		}


		/// <summary>
		/// Format to time.
		/// </summary>
		/// <param name="isWithSeconds">if set to <c>true</c> [is with seconds].</param>
		/// <returns></returns>
		public static string TimeFormat(bool isWithSeconds) {
			var format = Helper.LocalizeText("langue_dotnet_time_format");
			var retVal = isWithSeconds ? format + Helper.LocalizeText("langue_dotnet_seconds_format") : format;
			return retVal;
		}

		/// <summary>
		/// Format to use for DateTime in GridFilter (Universal sortable date/time pattern).
		/// </summary>
		/// <returns></returns>
		public const string const_date_time_format_for_filter = "u";


		/// <summary>
		/// Calculates a dynamic start date relative to current time.
		/// </summary>
		/// <param name="timeInterval">The time interval.</param>
		/// <param name="frequency">The frequency.</param>
		/// <param name="isCalendarPeriod">if set to <c>true</c> [is calendar period].</param>
		/// <param name="isCompleteInterval">if set to <c>true</c> [is complete interval].</param>
		/// <param name="timeZoneInfo">The time zone information.</param>
		/// <param name="startDayOfTheWeek">The start day of the week.</param>
		/// <returns></returns>
		public static DateTime CalculateDynamicStartDate(AxisTimeInterval timeInterval,
			int frequency,
			TimeZoneInfo timeZoneInfo,
			bool isCalendarPeriod = false,
			bool isCompleteInterval = false,
			int startDayOfTheWeek = 1) {

			var start = Helper.DateTimeUtcNow;
			start = TimeZoneInfo.ConvertTimeFromUtc(start, timeZoneInfo);

			if (frequency < 0)
				frequency += 1;

			switch (timeInterval) {
				case AxisTimeInterval.Years:
					start = isCalendarPeriod ? new DateTime(start.Year - (frequency - 1), 1, 1) : start.AddYears(-frequency);
					break;

				case AxisTimeInterval.Quarters:
					if (isCalendarPeriod) {
						var month = (start.Month - 1) / 3 + 1;
						start = new DateTime(start.Year, 3 * month - 2, 1);
						if (frequency > 1 || frequency <= 0)
							start = start.AddMonths(-3 * (frequency - 1));
					}
					else {
						start = start.AddMonths(-3 * frequency);
					}
					break;

				case AxisTimeInterval.Semesters:
					if (isCalendarPeriod) {
						var month = (start.Month - 1) / 6 + 1;
						start = new DateTime(start.Year, 6 * month - 5, 1);
						if (frequency > 1 || frequency <= 0)
							start = start.AddMonths(-6 * (frequency - 1));
					}
					else
						start = start.AddMonths(-6 * frequency);
					break;

				case AxisTimeInterval.Months:
					if (isCalendarPeriod)
						start = new DateTime(start.AddMonths(-frequency + 1).Year, start.AddMonths(-frequency + 1).Month, 1);
					else
						start = start.AddMonths(-frequency);
					break;

				case AxisTimeInterval.Weeks:
					if (isCalendarPeriod) {
						start = new DateTime(start.Year, start.Month, start.Day);
						switch (start.DayOfWeek) {
							case DayOfWeek.Monday:
								start = start.AddDays(-7 * (frequency - 1));
								break;
							case DayOfWeek.Tuesday:
								start = start.AddDays(-1 - 7 * (frequency - 1));
								break;
							case DayOfWeek.Wednesday:
								start = start.AddDays(-2 - 7 * (frequency - 1));
								break;
							case DayOfWeek.Thursday:
								start = start.AddDays(-3 - 7 * (frequency - 1));
								break;
							case DayOfWeek.Friday:
								start = start.AddDays(-4 - 7 * (frequency - 1));
								break;
							case DayOfWeek.Saturday:
								start = start.AddDays(-5 - 7 * (frequency - 1));
								break;
							case DayOfWeek.Sunday:
								start = start.AddDays(-6 - 7 * (frequency - 1));
								break;
						}
						//Adjust start day of the week.
						start = start.AddDays(startDayOfTheWeek - 1);
					}
					else {
						start = start.AddDays(-7 * frequency);
					}
					break;

				case AxisTimeInterval.Days:
					if (isCalendarPeriod)
						start = new DateTime(start.AddDays(-(frequency - 1)).Year, start.AddDays(-(frequency - 1)).Month,
							start.AddDays(-(frequency - 1)).Day);
					else
						start = start.AddDays(-frequency);
					break;

				case AxisTimeInterval.Hours:
					if (isCalendarPeriod)
						start = new DateTime(start.AddHours(-(frequency - 1)).Year, start.AddHours(-(frequency - 1)).Month,
							start.AddHours(-(frequency - 1)).Day, start.AddHours(-(frequency - 1)).Hour, 0, 0);
					else
						start = start.AddHours(-frequency);
					break;
				case AxisTimeInterval.Minutes:
					if (isCalendarPeriod)
						start = new DateTime(start.AddMinutes(-(frequency - 1)).Year, start.AddMinutes(-(frequency - 1)).Month,
							start.AddMinutes(-(frequency - 1)).Day, start.AddMinutes(-(frequency - 1)).Hour,
							start.AddMinutes(-(frequency - 1)).Minute, 0);
					else
						start = start.AddMinutes(-frequency);
					break;

				case AxisTimeInterval.Seconds:
					start = start.AddSeconds(-frequency);
					break;

				default:
					start = new DateTime(start.Year, 1, 1);
					break;
			}

			if (isCompleteInterval) {
				start = start.AddInterval(timeInterval, -1);
			}
			try {
				start = DateTime.SpecifyKind(start, DateTimeKind.Unspecified);
				start = TimeZoneInfo.ConvertTimeToUtc(start, timeZoneInfo);
			}
			catch (SystemException) { }
			return start;

		}


		/// <summary>
		/// Calculates the dynamic end date relative to a given startDate.
		/// </summary>
		/// <param name="timeInterval">The time interval.</param>
		/// <param name="frequency">The frequency.</param>
		/// <param name="startDateUTC">The start date UTC.</param>
		/// <param name="timeZoneInfo">The time zone information.</param>
		/// <param name="isEndToNow">if set to <c>true</c> [is end to now].</param>
		/// <returns></returns>
		public static DateTime CalculateDynamicEndDate(AxisTimeInterval timeInterval,
			int frequency,
			DateTime startDateUTC,
			TimeZoneInfo timeZoneInfo,
			bool isEndToNow = false) {

			if (isEndToNow) {
				return Helper.DateTimeUtcNow;
			}

			DateTime startDateLocal = startDateUTC.ConvertTimeFromUtc(timeZoneInfo);
			DateTime endDateLocal = startDateLocal.AddInterval(timeInterval, Math.Abs(frequency));

			DateTime endDateTime = endDateLocal.ConvertTimeToUtc(timeZoneInfo);

			return endDateTime;
		}


		/// <summary>
		/// Add a TimeInterval to a DateTime 
		/// </summary>
		/// <param name="dateTime">the datetime</param>
		/// <param name="timeinterval">the time interval</param>
		/// <param name="value">the value to add.</param>
		/// <returns></returns>
		public static DateTime AddTimeInterval(DateTime dateTime, AxisTimeInterval timeinterval, int value) {
			DateTime finalDateTime;

			switch (timeinterval) {

				case AxisTimeInterval.Years:
					finalDateTime = dateTime.AddYears(value);
					break;

				case AxisTimeInterval.Quarters:
					finalDateTime = dateTime.AddMonths(3 * value);
					break;

				case AxisTimeInterval.Semesters:
					finalDateTime = dateTime.AddMonths(6 * value);
					break;

				case AxisTimeInterval.Months:
					finalDateTime = dateTime.AddMonths(value);
					break;

				case AxisTimeInterval.Weeks:
					finalDateTime = dateTime.AddDays(7 * value);
					break;

				case AxisTimeInterval.Days:
					finalDateTime = dateTime.AddDays(value);
					break;

				case AxisTimeInterval.Hours:
					finalDateTime = dateTime.AddHours(value);
					break;

				case AxisTimeInterval.Minutes:
					finalDateTime = dateTime.AddMinutes(value);
					break;

				case AxisTimeInterval.Seconds:
					finalDateTime = dateTime.AddSeconds(value);
					break;

				default:
					finalDateTime = dateTime;
					break;
			}
			return finalDateTime;
		}
	}
}

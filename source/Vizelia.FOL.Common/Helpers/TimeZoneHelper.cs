﻿using System;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Static helper class for TimeZone.
	/// </summary>
	public static class TimeZoneHelper {

		private const string const_time_zone_key = "CurrentUserTimeZone";

		/// <summary>
		/// Gets the MsgCode for a specific TimeZoneInfo.
		/// </summary>
		/// <param name="timeZone">The time zone.</param>
		/// <returns></returns>
		public static string GetMsgCode(TimeZoneInfo timeZone) {
			var retVal = "msg_timezone_" + timeZone.Id.ToLower().Replace(" ", "_").Replace(".", "").Replace("+", "_plus_").Replace("-", "_minus_").Replace("(", "").Replace(")", "");
			return retVal;
		}

		/// <summary>
		/// Gets the localized display name of timezone.
		/// </summary>
		/// <param name="displayName">The display name.</param>
		/// <param name="msgCode">The MSG code.</param>
		/// <returns></returns>
		public static string GetLocalizedDisplayName(string displayName, string msgCode) {
			return displayName.Substring(0, displayName.IndexOf(')') + 1) + " " + Helper.LocalizeText(msgCode);
		}

		/// <summary>
		/// Gets the localized display name of timezone.
		/// </summary>
		/// <param name="timeZone">The time zone.</param>
		/// <returns></returns>
		public static string GetLocalizedDisplayName(TimeZoneInfo timeZone) {
			return GetLocalizedDisplayName(timeZone.DisplayName, GetMsgCode(timeZone));
		}

		/// <summary>
		/// Gets or sets the current user time zone.
		/// </summary>
		/// <value>
		/// The current user time zone.
		/// </value>
		public static TimeZoneInfo CurrentUserTimeZone {
			get {
				// This will one day support getting the time zone from the user.
				// Until then, we only check whether one was assigned manually in the context or not.
				var timeZoneInfo = ContextHelper.Get(const_time_zone_key) as TimeZoneInfo;
				timeZoneInfo = timeZoneInfo ?? TimeZoneInfo.Utc;

				return timeZoneInfo;
			}
			set {
				if (value == null) {
					ContextHelper.Items.Remove(const_time_zone_key);
				}
				else {
					ContextHelper.Add(const_time_zone_key, value);
				}
			}
		}

		/// <summary>
		/// Return a valid timestamped filename from an existing title.
		/// </summary>
		/// <param name="title">the existing title.</param>
		/// <returns></returns>
		public static string GetFilenameTimestamped(string title) {
			if (String.IsNullOrEmpty(title)) {
				title = Langue.msg_report;
			}

			TimeZoneInfo currentUserTimeZone = CurrentUserTimeZone;
			DateTime time = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, currentUserTimeZone);
			string timestamped = String.Format("{0}_{1:yyyy-MM-dd_HH-mm-ss}", title.Replace(" ", "_"), time);

			return timestamped;
		}

	}
}

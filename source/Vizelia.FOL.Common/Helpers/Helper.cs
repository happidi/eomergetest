﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Security;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Infrastructure;
using Vizelia.FOL.Providers;
using Task = System.Threading.Tasks.Task;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Static helper class
	/// </summary>
	public static class Helper {
		private static readonly Regex m_HasQuestionMarkRegEx = new Regex(@"\?", RegexOptions.Compiled);
		private static readonly Regex m_IllegalCharactersRegex = new Regex("[" + @"\/:<>|" + "\"]", RegexOptions.Compiled);
		private static readonly Regex m_CatchExtensionRegex = new Regex(@"^\s*.+\.([^\.]+)\s*$", RegexOptions.Compiled);


		/// <summary>
		/// Gets or sets the resolver.
		/// </summary>
		/// <value>
		/// The resolver.
		/// </value>
		public static IResolver Resolver { get; set; }

		/// <summary>
		/// The name of the extended property of logEntry containing the string builder when used in conjonction with VizeliaStringBuilderTraceListener.
		/// </summary>
		public const string const_extendedproperty_output = "output";

		static Helper() {
			Resolver = new Resolver();
		}



		/// <summary>
		/// Sets the UI culture.
		/// </summary>
		/// <param name="culture">The culture.</param>
		public static void SetUICulture(string culture) {
			if (!String.IsNullOrEmpty(culture)) {
				Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
			}
		}

        /// <summary>
        /// Sets the culture.
        /// </summary>
        /// <param name="culture">The culture.</param>
        public static void SetCulture(string culture) {
            if (!String.IsNullOrEmpty(culture)) {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
            }
        }

		/// <summary>
		/// Casts the specified obj.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj">The obj.</param>
		/// <returns></returns>
		public static T Cast<T>(object obj) {
			if (obj is T)
				return (T)obj;
			throw new ArgumentException(String.Format("{0} is not of type {1}", obj.GetType(), typeof(T).Name));
		}

		/// <summary>
		/// A helper function to retrieve config values from the configuration file
		/// </summary>
		/// <param name="configValue">The configuration value</param>
		/// <param name="defaultValue">The default value</param>
		/// <returns></returns>
		public static string GetConfigValue(string configValue, string defaultValue) {
			if (String.IsNullOrEmpty(configValue))
				return defaultValue;
			return configValue;
		}

		/// <summary>
		/// A helper function to retrieve the attribute type Service from an .svc file
		/// </summary>
		/// <param name="serviceText">The content of the .svc file</param>
		/// <returns></returns>
		public static string GetSvcTypeFromContent(string serviceText) {
			return WCFHelper.GetSvcTypeFromContent(serviceText);

			#region old code

			/*
			Assembly asmServiceModel = Assembly.Load(Assembly_System_ServiceModel);
			Assembly asmWeb = Assembly.Load(Assembly_System_Web);

			Type ServiceParserType = asmServiceModel.GetType("System.ServiceModel.Activation.ServiceParser");
			Type ServiceBuildProviderType = asmServiceModel.GetType("System.ServiceModel.Activation.ServiceBuildProvider");
			Type VirtualPathType = asmWeb.GetType("System.Web.VirtualPath");

			// serviceBuild = new ServiceBuildProvider();
			object serviceBuild = Activator.CreateInstance(ServiceBuildProviderType);

			// serviceBuild._referenceAssemblies = new SortedList();
			ServiceBuildProviderType.BaseType.GetField("_referencedAssemblies", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(serviceBuild, new SortedList(), BindingFlags.ExactBinding, null, null);

			// serviceParserInstance = new ServiceParser("", serviceBuild);
			object serviceParserInstance = ServiceParserType.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic)[0].Invoke(new object[] { "", serviceBuild });

			// serviceBuild.parser = serviceParserInstance;
			ServiceBuildProviderType.GetField("parser", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(serviceBuild, serviceParserInstance);
			object serviceParser = ServiceBuildProviderType.GetField("parser", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(serviceBuild);

			// serviceBuild.parser.serviceText = serviceText;
			ServiceParserType.GetField("serviceText", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(serviceParser, serviceText);

			// serviceBuild.parser.ParseString();
			ServiceParserType.GetMethod("ParseString", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(serviceParser, null);

			// serviceAttributeValue = serviceBuild.parser.serviceAttributeValue;
			string serviceAttributeValue = (string)ServiceParserType.GetField("serviceAttributeValue", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(serviceParser);
			return serviceAttributeValue;
			*/

			#endregion
		}

		/// <summary>
		/// A helper function to convert a wcf namespace into it's javascript notation
		/// </summary>
		/// <param name="ns">The namespace</param>
		/// <returns>The converted namespace, if namespace is null returns tempuri.org</returns>
		public static string GetClientTypeNamespace(string ns) {
			return WCFHelper.GetClientTypeNamespace(ns);

			#region old code

			/*
			Assembly asm = Assembly.Load(Assembly_System_Web_Extensions);
			Type ProxyGeneratorType = asm.GetType("System.Web.Script.Services.WCFServiceClientProxyGenerator");
			object ProxyGeneratorInstance;
			ProxyGeneratorInstance = ProxyGeneratorType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[] { typeof(String), typeof(Boolean) }, null).Invoke(new object[] { "", true });
			return (string)ProxyGeneratorType.GetMethod("GetClientTypeNamespace", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(ProxyGeneratorInstance, new object[] { ns });
			*/

			#endregion
		}

		/// <summary>
		/// A helper function to convert a DotNet Type into it's equivalent Extjs type string.
		/// We return "auto" when the type is nullable (converted to TypeCode.DBNull) because the record is already decoded through AJAX.NET, and we need to preserve null.
		/// Extjs uses an automatic convert function on the object field that otherwise replaces null values with an empty string.
		/// </summary>
		/// <param name="typeCode">The DotNet TypeCode.</param>
		/// <returns>The string resulting Extjs type.</returns>
		public static string ConvertTypeToExtjs(TypeCode typeCode) {
			string typeExtjs;
			switch (typeCode) {
				case TypeCode.Int32:
				case TypeCode.Int16:
				case TypeCode.Int64:
					typeExtjs = "int";
					break;

				case TypeCode.Double:
				case TypeCode.Decimal:
					typeExtjs = "float";
					break;

				case TypeCode.DateTime:
					typeExtjs = "date";
					break;

				case TypeCode.Boolean:
					typeExtjs = "boolean";
					break;

				case TypeCode.String:
					typeExtjs = "string";
					break;

				case TypeCode.Object:
					typeExtjs = "object";
					break;

				default:
					typeExtjs = "auto";
					break;
			}
			return typeExtjs;
		}

		/// <summary>
		/// A helper function to convert a C# Type into it's equivalent Extjs type string.
		/// </summary>
		/// <param name="type">The DotNet type.</param>
		/// <returns>The string resulting Extjs type.</returns>
		public static string ConvertTypeToExtjs(Type type) {
			return ConvertTypeToExtjs(GetTypeCode(type));
		}

		/// <summary>
		/// Gets the updatable batch properties.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public static List<string> GetUpdatableBatchProperties(Type type) {
			List<string> result = new List<string>();
			PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
			foreach (PropertyInfo p in properties) {
				// we retreive the datamember attribute
				Attribute datamember = Attribute.GetCustomAttribute(p, typeof(DataMemberAttribute));
				BatchUpdatableAttribute batchupdatable =
					(BatchUpdatableAttribute)Attribute.GetCustomAttribute(p, typeof(BatchUpdatableAttribute));
				// we exclude property that are not marked as [DataMember]
				if (datamember == null || batchupdatable == null || !batchupdatable.IsUpdatable ||
					Attribute.GetCustomAttribute(p, typeof(ExcludeMetaDataAttribute)) != null) continue;
				string propertyType = ConvertTypeToExtjs(p.PropertyType);
				if (propertyType != "object") {
					result.Add(p.Name);
				}
			}
			return result;
		}

		/// <summary>
		/// A helper function to convert a DotNet type to a TypeCode.
		/// We had to define this function so it works with Nullable type (like DateTime?, etc...).
		/// All Nullable are returned as DBNull.
		/// </summary>
		/// <param name="type">The DotNet type.</param>
		/// <returns>The TypeCode type.</returns>
		public static TypeCode GetTypeCode(Type type) {
			if (type.IsGenericType) {
				// case of a Nullable type.
				//return Type.GetTypeCode(type.GetGenericArguments().First());
				return TypeCode.DBNull;
			}
			// special case for Guid that would return object otherwise
			return Type.GetTypeCode(type == typeof(Guid) || type == typeof(TimeSpan) ? typeof(String) : type);
		}

		/// <summary>
		/// Gets the configuration section.
		/// </summary>
		/// <typeparam name="T">the type of configuration.</typeparam>
		/// <param name="sectionName">Name of the section.</param>
		/// <returns></returns>
		public static T GetConfigurationSection<T>(string sectionName) {
			return (T)ConfigurationManager.GetSection(sectionName);
		}

		/// <summary>
		/// Resolves the given type T to the first concrete implementation of it found using reflection.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static T Resolve<T>() {
			return Resolver.Resolve<T>();
		}

		/// <summary>
		/// Resolves the requested type to its concrete type which is a class.
		/// </summary>
		/// <param name="abstractType">Type of the abstract.</param>
		/// <returns></returns>
		public static Type ResolveToConcreteClassType(Type abstractType) {
			return Resolver.ResolveToConcreteClassType(abstractType);
		}

		/// <summary>
		/// Resolves the type by the given logical test.
		/// </summary>
		/// <param name="logicalTest">The logical test.</param>
		/// <returns></returns>
		public static Type ResolveType(Func<Type, bool> logicalTest) {
			return Resolver.ResolveType(logicalTest);
		}



		/// <summary>
		/// Gets the assembly qualified name no version.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public static string GetAssemblyQualifiedNameNoVersion(Type type) {
			var retVal = type.AssemblyQualifiedName;
			retVal = retVal.Remove(retVal.IndexOf(", Version", StringComparison.Ordinal));
			return retVal;
		}

		/// <summary>
		/// Creates an object from a type.
		/// </summary>
		/// <typeparam name="T">The type.</typeparam>
		/// <returns></returns>
		public static T CreateFromType<T>() {
			return typeof(T).GetConstructor(new Type[] { }) == null ? default(T) : Activator.CreateInstance<T>();
		}

		/// <summary>
		/// A helper function for injection.
		/// </summary>
		/// <typeparam name="TObject">The Concrete class.</typeparam>
		/// <typeparam name="TInterface">The Interface.</typeparam>
		/// <param name="args">Optional params for ctor.</param>
		/// <returns>A TInterface implementation</returns>
		public static TInterface CreateInstance<TObject, TInterface>(params object[] args) where TObject : TInterface {
			if (VizeliaConfiguration.Instance.Deployment.UsePolicyInjection) {
				return PolicyInjection.Create<TObject, TInterface>(args);
			}
			return (TInterface)Activator.CreateInstance(typeof(TObject), args);
		}

		/// <summary>
		/// Creates the instance with policy injection.
		/// </summary>
		/// <typeparam name="TObject">The type of the object.</typeparam>
		/// <typeparam name="TInterface">The type of the interface.</typeparam>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		public static TInterface CreateInstanceWithPolicyInjection<TObject, TInterface>(params object[] args)
			where TObject : TInterface {
			return PolicyInjection.Create<TObject, TInterface>(args);
		}


		/// <summary>
		/// A helper function for injection.
		/// </summary>
		/// <typeparam name="TObject">Concrete class.</typeparam>
		/// <param name="args">Optional params for ctor.</param>
		/// <returns>A TObject instance.</returns>
		public static TObject CreateInstance<TObject>(params object[] args) {
			if (VizeliaConfiguration.Instance.Deployment.UsePolicyInjection) {
				return PolicyInjection.Create<TObject>(args);
			}
			return (TObject)Activator.CreateInstance(typeof(TObject), args);
		}

		/// <summary>
		/// A helper function that return a specific attribute on a class.
		/// </summary>
		/// <typeparam name="T">The class Attribute to retreive.</typeparam>
		/// <param name="t">The class type.</param>
		/// <returns>The T Attribute.</returns>
		public static T GetAttribute<T>(Type t) where T : Attribute {
			return (from attribute in Attribute.GetCustomAttributes(t)
					where attribute is T
					select (T)attribute).FirstOrDefault();
		}

		/// <summary>
		/// Determine if a property exists in an type
		/// </summary>
		/// <param name="propertyName">Name of the property </param>
		/// <param name="type">the type of object to inspect</param>
		/// <returns>true if the property exists, false otherwise</returns>
		/// <exception cref="ArgumentNullException">if type is null</exception>
		/// <exception cref="ArgumentException">if propertName is empty or null </exception>
		public static bool HasProperty(string propertyName, Type type) {
			if (type == null)
				throw new ArgumentNullException("type");
			if (String.IsNullOrEmpty(propertyName))
				throw new ArgumentException("Property name cannot be empty or null.");
			PropertyInfo propInfoSrcObj = type.GetProperty(propertyName);
			return (propInfoSrcObj != null);
		}

		/// <summary>
		/// A helper class that returns the value of a specific attribute on a class.
		/// </summary>
		/// <param name="t">The class type.</param>
		/// <returns>
		/// The value of the attribute. Returns null when does not exist.
		/// </returns>
		public static string GetLocalizedText(Type t) {
			LocalizedTextAttribute attribute =
				(from att in t.GetCustomAttributes(false)
				 where att is LocalizedTextAttribute
				 select att).FirstOrDefault() as LocalizedTextAttribute;

			if (attribute == null)
				return String.Empty;
			var retval = Langue.ResourceManager.GetString(attribute.PropertyNameId);
			return retval;
		}

		/// <summary>
		/// Localizes the text.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="delimiter">The delimiter.</param>
		/// <returns></returns>
		public static string LocalizeInText(string text, string delimiter) {
			if (!String.IsNullOrWhiteSpace(text)) {
				foreach (string match in text.Split(new[] { delimiter }, StringSplitOptions.RemoveEmptyEntries).Distinct()) {
					var localizedText = LocalizeText(match);
					if (localizedText != match)
						text = text.Replace(string.Format("{0}{1}{0}", delimiter, match), localizedText);
				}
			}
			return text;
		}

		/// <summary>
		/// Localizes the text.
		/// </summary>
		/// <param name="msgCode">The MsgCode.</param>
		/// <param name="returnNullIfNotFound">if set to <c>true</c> [return null if not found].</param>
		/// <returns></returns>
		public static string LocalizeText(string msgCode, bool returnNullIfNotFound = false) {
			var localizedText = Langue.ResourceManager.GetString(msgCode);
			if ((localizedText == msgCode) && returnNullIfNotFound)
				return null;
			return localizedText;
		}

        /// <summary>
        /// Localizes the text.
        /// </summary>
        /// <param name="msgCode">the MsgCode</param>
        /// <param name="culture">the culture we want to get the text</param>
        /// <param name="returnNullIfNotFound">if set to <c>true</c> [return null if not found].</param>
        /// <returns></returns>
        public static string LocalizeText(string msgCode, CultureInfo culture, bool returnNullIfNotFound = false) {
            var localizedText = Langue.ResourceManager.GetString(msgCode, culture);
            if ((localizedText == msgCode) && returnNullIfNotFound)
                return null;
            return localizedText;
        }

		/// <summary>
		/// A helper class that returns the value of a specific attribute on a class.
		/// </summary>
		/// <typeparam name="T">The class attribute that we want to retreive the value. Must derive from Vizelia.FOL.Common.BaseAttribute</typeparam>
		/// <param name="t">The class type.</param>
		/// <returns>The value of the attribute. Returns null when does not exist.</returns>
		public static string GetAttributeValue<T>(Type t) where T : BaseAttribute {
			T attribute = GetAttribute<T>(t);
			if (attribute != null)
				return attribute.PropertyNameId;
			return null;
		}

		/// <summary>
		/// A helper function that retreives the property id of a business entity.
		/// </summary>
		/// <param name="t">The type of the business entity.</param>
		/// <returns>The property id name or null if not defined.</returns>
		public static string GetPropertyNameId(Type t) {
			return GetAttributeValue<KeyAttribute>(t);
		}

		/// <summary>
		/// Gets the current user IP.
		/// </summary>
		/// <returns>The IP.</returns>
		public static string GetUserIP() {
			string userIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
			//Trim and lowercase IP if not null
			if (userIP != null) {
				userIP = userIP.ToLower();
				userIP = userIP.Trim();
			}
			
            //If IP is null use different detection method. Else pull the correct IP from list.
			if ((userIP == null) || (userIP == "unknown")) {
				userIP = HttpContext.Current.Request.UserHostAddress; // HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
			}
			else if (userIP.Contains(',')) {
				string[] IPs = userIP.Split(',');
				userIP = IPs.GetValue(0).ToString().Trim();
			}
			return userIP;
		}

		/// <summary>
		/// Gets the current user. 
		/// Uses session internally to cache the results.
		/// </summary>
		/// <returns></returns>
		public static string GetCurrentActor() {
			// In a call to IPublicWCF there is no current actor since no authentication is required.
			if (SessionService.GetSessionID() == null) return null;

			const string const_key_currentactor = "CurrentActor";
			string retVal = SessionService.Get(const_key_currentactor) as string;
			if (retVal == null) {
				var currentUser = GetCurrentFOLProviderUser();
				if (currentUser != null) {
					retVal = currentUser.KeyUser;
					SessionService.Add(const_key_currentactor, retVal);
				}
			}

			return retVal;
		}

		/// <summary>
		/// Gets the current FOL provider user.
		/// </summary>
		public static FOLProviderUserKey GetCurrentFOLProviderUser() {
			const string const_key_currentuser = "CurrentUser";
			// If we can't get the current user, just return null.
			FOLProviderUserKey retVal = null;
			try {
				retVal = SessionService.Get(const_key_currentuser) as FOLProviderUserKey;
			}
			catch { }
			if (retVal == null) {
				// Try getting the current user
				string userName = GetCurrentUserName();
				if (!String.IsNullOrEmpty(userName)) {
					MembershipUser user = Membership.GetUser(userName);
					if (user != null && user.ProviderUserKey != null && user.ProviderUserKey is FOLProviderUserKey) {
						retVal = ((FOLProviderUserKey)user.ProviderUserKey);
						SessionService.Add(const_key_currentuser, retVal);
					}
				}
			}
			return retVal;
		}

		/// <summary>
		/// Gets the current FOL membership user.
		/// </summary>
		/// <returns></returns>
		public static FOLMembershipUser GetCurrentFOLMembershipUser() {
			const string const_key_currentfolmembershipuser = "CurrentFOLMembershipUser";
			// If we can't get the current user, just return null.
			FOLMembershipUser retVal = null;
			try {
				retVal = SessionService.Get(const_key_currentfolmembershipuser) as FOLMembershipUser;
			}
			catch { }
			if (retVal == null) {
				// Try getting the current user
				string userName = GetCurrentUserName();
				if (!String.IsNullOrEmpty(userName)) {
					retVal = GetFOLMembershipUser(userName);
					if (retVal != null)
						SessionService.Add(const_key_currentfolmembershipuser, retVal);
				}
			}
			return retVal;
		}

		/// <summary>
		/// Gets the FOL membership user.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <returns></returns>
		public static FOLMembershipUser GetFOLMembershipUser(string userName) {
			FOLMembershipUser retVal = null;
			MembershipUser user = Membership.GetUser(userName, false);
			if (user != null)
				retVal = new FOLMembershipUser(user);
			return retVal;
		}

		/// <summary>
		/// Gets the name of the current user.
		/// Uses the current thread.
		/// </summary>
		/// <returns></returns>
		public static string GetCurrentUserName() {
			try {
				var retVal = Thread.CurrentPrincipal.Identity;
				return retVal.Name;
			}
			catch {
				return null;
			}
		}

		/// <summary>
		/// A helper method for deserializing json string.
		/// </summary>
		/// <param name="json">The json string value.</param>
		/// <param name="objectType">The type of the object to rehydrate.</param>
		/// <returns>The object rehydrated from the json string</returns>
		public static object DeserializeJson(string json, Type objectType) {
			return SerializationHelper.DeserializeJson(json, objectType);
		}

		/// <summary>
		/// A helper method for deserializing json strings. 
		/// </summary>
		/// <typeparam name="T">Genric type.</typeparam>
		/// <param name="json">The json string value.</param>
		/// <returns>The object rehydrated from the json string</returns>
		public static T DeserializeJson<T>(string json) {
			return SerializationHelper.DeserializeJson<T>(json);
		}

		/// <summary>
		/// A helper method for serializing object to json strings.
		/// </summary>
		/// <param name="obj">The object to serialize.</param>
		/// <param name="objectType">The type of the object to dehydrate.</param>
		/// <returns>The json string value of the object.</returns>
		public static string SerializeJson(object obj, Type objectType) {
			return SerializationHelper.SerializeJson(obj, objectType);
		}

		/// <summary>
		/// A helper method for serializing object to json strings.
		/// </summary>
		/// <param name="obj">The object to serialize.</param>
		/// <returns>The json string value of the object.</returns>
		public static string SerializeJson(object obj) {
			return SerializationHelper.SerializeJson(obj);
		}

		/// <summary>
		/// A helper method for serializing object to xml strings.
		/// </summary>
		/// <param name="obj">The object to serialize.</param>
		/// <returns>The xml string value of the object.</returns>
		public static string SerializeXml(object obj) {
			return SerializeXml(obj, obj.GetType(), null);
		}

		/// <summary>
		/// A helper method for serializing object to an xml document.
		/// </summary>
		/// <param name="obj">The object to serialize.</param>
		/// <param name="fileName">The name of the file to save the result serialization.</param>
		public static void SerializeXml(object obj, string fileName) {
			SerializeXml(obj, obj.GetType(), fileName);
		}

		/// <summary>
		/// A helper method for serializing object to xml strings.
		/// </summary>
		/// <param name="obj">The object to serialize.</param>
		/// <param name="objectType">The type of the object to serialize.</param>
		/// <param name="fileName">The name of the file (can be null) to save the serializated document.</param>
		/// <returns>The xml string value of the object.</returns>
		[SuppressMessage("Microsoft.Usage", "CA2202:Ne pas supprimer d'objets plusieurs fois")]
		private static string SerializeXml(object obj, Type objectType, string fileName) {
			XmlTextWriter xmlTextWriter = null;
			try {
				using (var memoryStream = new MemoryStream()) {
					xmlTextWriter = fileName == null ? new XmlTextWriter(memoryStream, Encoding.UTF8) : new XmlTextWriter(fileName, Encoding.UTF8);
					xmlTextWriter.Formatting = Formatting.Indented;
					var xs = new XmlSerializer(objectType);
					xs.Serialize(xmlTextWriter, obj);
					string xml = memoryStream.GetString();
					return xml;
				}
			}
			finally {
				if (xmlTextWriter != null) {
					xmlTextWriter.Close();
				}
			}
		}

		/// <summary>
		/// A helper method for deserializing xml string.
		/// </summary>
		/// <typeparam name="T">Generic type.</typeparam>
		/// <param name="xml">The xml string value of the object.</param>
		/// <returns>The object rehydrated from the xml string.</returns>
		public static T DeserializeXml<T>(string xml) where T : class {
			var deserialized = DeserializeXml(xml, typeof(T)) as T;
			return deserialized;
		}

		/// <summary>
		/// A helper method for deserializing xml string.
		/// If you want to serialize an entity and only get a it's parents properties
		/// then you can write a decorator for it's parent entity instead of implementing IXmlSerializer
		/// </summary>
		/// <typeparam name="T">Generic type.</typeparam>
		/// <typeparam name="D">The decorator</typeparam>
		/// <param name="xml">The xml string value of the object.</param>
		/// <returns>
		/// The object rehydrated from the xml string.
		/// </returns>
		public static T DeserializeXml<T, D>(string xml)
			where T : class
			where D : class {
			var deserialized = DeserializeXml(xml, typeof(D)) as T;
			return deserialized;
		}

		/// <summary>
		/// A helper method for deserializing xml string.
		/// </summary>
		/// <param name="xml">The xml string value of the object</param>
		/// <param name="type">The type of the object to deserialize.</param>
		/// <returns>The object rehydrated from the xml string.</returns>
		public static object DeserializeXml(string xml, Type type) {
			var serializer = new XmlSerializer(type);
			using (Stream memoryStream = xml.GetStream()) {
				object deserializedObject = serializer.Deserialize(memoryStream);
				return deserializedObject;
			}
		}

		/// <summary>
		/// Parses the specified source value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceValue">The source value.</param>
		/// <returns></returns>
		public static T Parse<T>(string sourceValue) where T : IConvertible {
			return (T)Convert.ChangeType(sourceValue, typeof(T));
		}

		/// <summary>
		/// Parses the specified source value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceValue">The source value.</param>
		/// <param name="provider">The provider.</param>ubl
		/// <returns></returns>
		public static T Parse<T>(string sourceValue, IFormatProvider provider) where T : IConvertible {
			return (T)Convert.ChangeType(sourceValue, typeof(T), provider);
		}

		/// <summary>
		/// Builds an xml string from an array of Keys string.
		/// </summary>
		/// <param name="Keys">The array of Keys string.</param>
		/// <returns>The xml string.</returns>
		public static string BuildXml(string[] Keys) {
			if (Keys == null) return null;
			if (Keys.Count(p => p != null) > 0)
				return new XElement("XML",
									from key in Keys
									where key != null
									select new XElement("OBJECT",
														new XAttribute("key", key))
					).ToString(SaveOptions.DisableFormatting);
			return null;
		}


		/// <summary>
		/// Builds an xml string from an array of string dictionary.
		/// </summary>
		/// <param name="keysDictionary"></param>
		/// <returns></returns>
		public static string BuildXml(Dictionary<string, string> keysDictionary) {
			string retXml = null;
			if (keysDictionary.Any()) {
				retXml = new XElement("XML",
					from kvp in keysDictionary
					select new XElement("OBJECT",
						new XAttribute("key", kvp.Key),
						new XAttribute("value", kvp.Value))
					).ToString(SaveOptions.DisableFormatting);
			}

			return retXml;
		}

		/// <summary>
		/// Gets the current UI culture.
		/// </summary>
		/// <returns></returns>
		public static string GetCurrentUICulture() {
			return Thread.CurrentThread.CurrentUICulture.ToString();
		}

        /// <summary>
        /// Gets the current culture.
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentCulture() {
            return Thread.CurrentThread.CurrentCulture.ToString();
        }

		/// <summary>
		/// Gets the current supported culture, will either be 2 letter culture or a supported 4 letter culture.
		/// </summary>
		/// <returns></returns>
		public static string GetCurrentApplicableCulture() {
			List<String> cultures = CacheService.Get("supportedCultures", GetSupportedCultures);
			string currentCulture = GetCurrentUICulture();
			var retVal = cultures.Select(c => c.ToLower()).Contains(currentCulture.ToLower()) ? currentCulture.ToLower() : currentCulture.Substring(0, 2).ToLower();
			return retVal;

		}

		/// <summary>
		/// Gets the supported cultures from the DB.
		/// </summary>
		/// <returns></returns>
		private static List<string> GetSupportedCultures() {
			var assembly = Assembly.Load("Vizelia.FOL.Common.Localization.ResourceManager");
			var method = assembly.GetType("Vizelia.FOL.Common.Localization.DatabaseResourceHelper").GetMethod("GetSupportedCultures");
			var supportedCultures = method.Invoke(null, new object[] { }) as List<string>;
			return supportedCultures;
		}

		/// <summary>
		/// Gets the current culture in short format (2 letters).
		/// </summary>
		/// <returns></returns>
		/// <remarks></remarks>
		public static string GetCurrentCultureShort() {
			CultureInfo culture = Thread.CurrentThread.CurrentUICulture;
			string shortCulture = culture.TwoLetterISOLanguageName;
			return shortCulture;
		}

		/// <summary>
		/// Gets the clr version.
		/// </summary>
		/// <returns></returns>
		public static string GetClrVersion() {
			return RuntimeEnvironment.GetSystemVersion();
		}

		/// <summary>
		/// Gets the IconCls from an IfcType.
		/// </summary>
		/// <param name="ifcTypeName">Ifc TypeName</param>
		public static string GetIconClsByIfcType(string ifcTypeName) {
			var key = String.Format("GetIconClsByIfcType_{0}", ifcTypeName);

			var retVal = CacheService.Get(key, () => {
				string iconCls = null;
				var assembly = Assembly.GetAssembly(typeof(IfcBaseBusinessEntity));

				Type[] types = assembly.GetTypes();

				Type foundType = types.FirstOrDefault(t => IsOfIfcType(t, ifcTypeName));
				if (foundType != null) {
					iconCls = GetAttributeValue<IconClsAttribute>(foundType);
				}
				return iconCls;
			}, iconCls => iconCls != null);
			return retVal;
		}

		/// <summary>
		/// Determines whether a given .NET CLR type is of the given Ifc TypeName.
		/// </summary>
		/// <param name="type">The .NET CLR type.</param>
		/// <param name="ifcTypeName">Name of the ifc Type.</param>
		/// <returns>
		///   <c>true</c> if the given .NET CLR type is of the given Ifc TypeName; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsOfIfcType(Type type, string ifcTypeName) {
			string attributeValue = GetAttributeValue<IfcTypeNameAttribute>(type);
			var retVal = (attributeValue != null && attributeValue.Equals(ifcTypeName));
			return retVal;
		}

		/// <summary>
		/// Gets the securable types.
		/// The result list is cached.
		/// </summary>
		/// <returns></returns>
		public static List<Type> GetSecurableTypes() {
			List<Type> retval = CacheService.Get(CacheKey.const_cache_securabletypes, GetBaseBusinessEntityTypes().Where(IsSecurableType).ToList, location: CacheLocation.Memory);

			return retval;
		}

		/// <summary>
		/// Check if a type is securable.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static bool IsSecurableType(Type type) {
			//we dont need to get Location type object since the Location entity handles all filtering.
			bool isSecurableType = (type.IsClass) && (!type.IsAbstract) && (typeof(ISecurableObject).IsAssignableFrom(type)) && (!typeof(ILocation).IsAssignableFrom(type));

			return isSecurableType;
		}


		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <returns></returns>
		public static Type GetPortletEntityType(string typeEntity) {
			var portletType = !string.IsNullOrEmpty(typeEntity) ? typeof(BaseBusinessEntity).Assembly.GetType(typeEntity) : null;
			return portletType ?? typeof(Chart);
		}

		/// <summary>
		/// Gets the IPortlet types.
		/// </summary>
		/// <returns></returns>
		public static List<Type> GetIPortletTypes() {
			var typeOfIPortlet = typeof(IPortlet);
			List<Type> retval = CacheService.Get(CacheKey.const_cache_portlettypes, () => {
				var portletTypes = (from type in GetBaseBusinessEntityTypes()
									where typeOfIPortlet.IsAssignableFrom(type) && (type != typeOfIPortlet)
									select type).ToList();
				return portletTypes;
			}, location: CacheLocation.Memory);
			return retval;
		}

		/// <summary>
		/// Gets the base business entity types.
		/// </summary>
		/// <returns></returns>
		public static Type[] GetBaseBusinessEntityTypes() {
			var retVal = CacheService.Get(CacheKey.const_cache_basebusinessentity_types, Assembly.GetAssembly(typeof(BaseBusinessEntity)).GetTypes, location: CacheLocation.Memory);
			return retVal;
		}

		/// <summary>
		/// Gets the MemberInfo of a public member from a lambda expression in the form of "e =&gt; e.Property".
		/// </summary>
		/// <typeparam name="TBusinessEntity">The type of the business entity.</typeparam>
		/// <typeparam name="TValue">The type of the member value.</typeparam>
		/// <param name="memberNameIndicatingExpression">The expression indicating the member name.</param>
		/// <returns></returns>
		public static MemberInfo GetMemberFromExpression<TBusinessEntity, TValue>(Expression<Func<TBusinessEntity, TValue>> memberNameIndicatingExpression) {
			MemberExpression memberExpression = GetMemberExpression(memberNameIndicatingExpression);

			return memberExpression.Member;
		}

		/// <summary>
		/// Gets the member expression from an expression.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="expression">The expression.</param>
		private static MemberExpression GetMemberExpression<T, TValue>(Expression<Func<T, TValue>> expression) {
			if (expression == null) {
				throw new ArgumentNullException("expression");
			}

			if (expression.Body is MemberExpression) {
				return (MemberExpression)expression.Body;
			}

			if (expression.Body is UnaryExpression) {
				var operand = ((UnaryExpression)expression.Body).Operand;
				if (operand is MemberExpression) {
					return (MemberExpression)operand;
				}
				if (operand is MethodCallExpression) {
					return ((MethodCallExpression)operand).Object as MemberExpression;
				}
			}

			throw new ArgumentException("Cannot understand expression.");
		}

		/// <summary>
		/// Determines whether the filename matches the specified pattern or not.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="pattern">The pattern.</param>
		/// <returns>
		///   <c>true</c> if the filename matches the specified pattern; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsFilenameMatch(string filename, string pattern) {
			if (pattern == null) {
				throw new ArgumentNullException();
			}

			pattern = pattern.Trim();
			if (pattern.Length == 0) {
				throw new ArgumentException("Pattern is empty.");
			}

			if (m_IllegalCharactersRegex.IsMatch(pattern)) {
				throw new ArgumentException("Patterns contains illegal characters.");
			}

			bool hasExtension = m_CatchExtensionRegex.IsMatch(pattern);
			bool matchExact = false;

			if (m_HasQuestionMarkRegEx.IsMatch(pattern)) {
				matchExact = true;
			}
			else if (hasExtension) {
				matchExact = m_CatchExtensionRegex.Match(pattern).Groups[1].Length != 3;
			}

			string regexString = Regex.Escape(pattern);
			regexString = "^" + Regex.Replace(regexString, @"\\\*", ".*");
			regexString = Regex.Replace(regexString, @"\\\?", ".");
			if (!matchExact && hasExtension) {
				regexString += @"[^.]*"; // non-dot characters.
			}

			regexString += "$";
			var regex = new Regex(regexString, RegexOptions.IgnoreCase);

			bool isMatch = regex.IsMatch(filename);

			return isMatch;
		}

		/// <summary>
		/// Executes a WCF method via reflection.
		/// </summary>
		/// <param name="serviceTypeName">The full qualified name of the WCF service.</param>
		/// <param name="methodName">The name of the method.</param>
		/// <param name="collectionJsonStringParams">The collection of parameters values expected by the method. The values should be passed as a json string and rehydrated automatically.</param>
		/// <returns></returns>
		public static object ExecuteServiceByReflection(string serviceTypeName, string methodName, NameValueCollection collectionJsonStringParams) {
			Type serviceType = Assembly.Load(serviceTypeName).GetType(serviceTypeName);
			//Type serviceType = Type.GetType(serviceTypeName);
			object service = serviceType.Assembly.CreateInstance(serviceTypeName);
			MethodInfo method = serviceType.GetMethod(methodName);
			List<object> parameters = new List<object>();
			ParameterInfo[] parametersInfo = method.GetParameters();
			foreach (ParameterInfo parameterInfo in parametersInfo) {
				string parameterValue = collectionJsonStringParams[parameterInfo.Name];
				if (parameterInfo.GetType().IsPrimitive) {
					parameters.Add(parameterValue);
				}
				else {
					if (String.IsNullOrEmpty(parameterValue))
						try {
							parameters.Add(Activator.CreateInstance(parameterInfo.ParameterType));
						}
						catch {
							parameters.Add(null);
						}
					else
						parameters.Add(DeserializeJson(parameterValue, parameterInfo.ParameterType));
				}
			}
			return method.Invoke(service, parameters.ToArray());
		}


		/// <summary>
		/// Gets the frontend URL.
		/// </summary>
		/// <param name="path">The path to create the frontend url to</param>
		/// <param name="frontendUrl">if frontendUrl is specified the result would be frontendUrl + path, otherwise this method will use the current request context to determine the frontendUrl</param>
		/// <param name="withScheme">if set to <c>true</c> [with scheme].</param>
		/// <returns></returns>
		public static string GetFrontendUrl(string path, string frontendUrl = null, bool withScheme = true) {
			if (!path.StartsWith("~/"))
				throw new ArgumentException("path must start with \"~\\\" ");

			Uri url;

			if (String.IsNullOrEmpty(frontendUrl)) {
				// resolve from context
				string SchemeAndServer = HttpContext.Current.Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.UriEscaped);
				string virtualPath = HttpRuntime.AppDomainAppVirtualPath;
				if (virtualPath == "/")
					virtualPath = "";
				url = new Uri(SchemeAndServer.TrimEnd('/') + virtualPath + path.TrimStart('~'), UriKind.Absolute);
			}
			else {
				string scheme = HttpContext.Current.Request.Url.GetComponents(UriComponents.Scheme, UriFormat.UriEscaped);
				// contact and return
				url = new Uri(scheme + Uri.SchemeDelimiter + frontendUrl.TrimEnd('/') + path.TrimStart('~'));
			}

			string retVal = withScheme ? url.ToString() : url.ToString().Replace(url.Scheme + Uri.SchemeDelimiter, "");
			return retVal;
		}

		/// <summary>
		/// Gets the frontend URL.
		/// </summary>
		/// <returns></returns>
		public static string GetFrontendUrl() {
			return GetFrontendUrl("~/", VizeliaConfiguration.Instance.Deployment.FrontendUrl, false);
		}

		/// <summary>
		/// Recursively deletes a directory and content, even read only or arhive.
		/// </summary>
		/// <param name="directory">The FileSystemInfo representing the directory.</param>
		public static void DeleteRecursive(string directory) {
			DirectoryInfo di = new DirectoryInfo(directory);
			DirectoryInfo parentDirectory = di.Parent ?? di;
			var fsi = parentDirectory.GetFileSystemInfos(di.Name)[0];

			DeleteRecursive(fsi);

		}


		/// <summary>
		/// Recursively deletes a directory and content, even read only or arhive.
		/// </summary>
		/// <param name="fsi">The FileSystemInfo representing the directory.</param>
		public static void DeleteRecursive(FileSystemInfo fsi) {
			fsi.Attributes = FileAttributes.Normal;
			var di = fsi as DirectoryInfo;

			if (di != null) {
				foreach (var dirInfo in di.GetFileSystemInfos())
					DeleteRecursive(dirInfo);
			}

			fsi.Delete();
		}

		/// <summary>
		/// Starts the frontend.
		/// </summary>
		public static void StartFrontend() {
			try {
				var type = Type.GetType("Vizelia.FOL.BusinessLayer.CoreBusinessLayer, Vizelia.FOL.BusinessLayer.CoreBusinessLayer");
				if (type != null) {
					MethodInfo initMethod = type.GetMethod("Init", BindingFlags.Static | BindingFlags.Public);
					initMethod.Invoke(null, null);
				}
				ExecuteServiceByReflection("Vizelia.FOL.WCFService.EnergyWCF", "InitializeEnergyAggregatorServiceAgent", null);
			}
			catch { }
		}

		/// <summary>
		/// Prevents duplicate names in the given objects by renaming the string in Item2 of the tuple with numbering if duplicates exist.
		/// Returns a list of duplicate-free names and objects.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="objectsWithNames">The objects with names.</param>
		/// <returns></returns>
		public static List<Tuple<T, string>> PreventDuplicateNames<T>(IEnumerable<Tuple<T, string>> objectsWithNames) {
			var objectsWithDistinctNames = PreventDuplicateNames(objectsWithNames, Int32.MaxValue, " ");

			return objectsWithDistinctNames;
		}

		/// <summary>
		/// Prevents duplicate names in the given objects by renaming the string in Item2 of the tuple with numbering if duplicates exist.
		/// Returns a list of duplicate-free names and objects.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="objectsWithNames">The objects with names.</param>
		/// <param name="maxLength">Maximum length of the string.</param>
		/// <param name="separator">The separator between the shortened name and the number.</param>
		/// <returns></returns>
		public static List<Tuple<T, string>> PreventDuplicateNames<T>(IEnumerable<Tuple<T, string>> objectsWithNames, int maxLength, string separator) {
			// Find groups of sheets with the same name in order to number them.
			var objectsWithDistinctNames = new List<Tuple<T, string>>();
			var duplicateNameGroups = objectsWithNames.GroupBy(s => s.Item2);

			foreach (var objectsWithSameName in duplicateNameGroups) {
				if (objectsWithSameName.Count() > 1) {
					// Found several objects with same name. Number them.
					int i = 1;

					foreach (var objectWithName in objectsWithSameName) {
						int shortenedNameMaxLength = maxLength - separator.Length - i.ToString(CultureInfo.InvariantCulture).Length;
						string shortenedName = objectWithName.Item2.Substring(0, Math.Min(objectWithName.Item2.Length, shortenedNameMaxLength));
						string newName = String.Format("{0}{1}{2}", shortenedName, separator, i);

						objectsWithDistinctNames.Add(new Tuple<T, string>(objectWithName.Item1, newName));
						i++;
					}
				}
				else {
					// Found object with unique name. No modification made.
					var singleObject = objectsWithSameName.First();
					objectsWithDistinctNames.Add(new Tuple<T, string>(singleObject.Item1, singleObject.Item2));
				}
			}

			return objectsWithDistinctNames;
		}

		/// <summary>
		/// Retries the given action if it has failed with the specified exception.
		/// </summary>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <typeparam name="TException">The type of the exception.</typeparam>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval (in milliseconds).</param>
		/// <param name="action">The action.</param>
		/// <returns></returns>
		public static TResult RetryIfFailed<TResult, TException>(int maxRetries, int retryInterval, Func<TResult> action) where TException : Exception {
			int currentAttemptNumber = 1;
			TResult result;

			while (true) {
				try {
					result = action();
					break; // No need to retry.
				}
				catch (TException exception) {
					if (currentAttemptNumber > maxRetries) {
						// We retried all the times we were allowed, let the caller handle this.
						TracingService.Write(
							String.Format(
								"Exception occured in given operation, attempted {0} times. Will not retry further.\r\nException details:{1}",
								maxRetries, exception));

						throw;
					}

					TracingService.Write(TraceEntrySeverity.Warning,
										 String.Format(
											"Exception occured in given operation, at attempt {0} out of {1}. Will retry.\r\nException details:{2}",
											currentAttemptNumber, maxRetries, exception));

					currentAttemptNumber++;
					Thread.Sleep(retryInterval);
				}
			}

			return result;
		}

		/// <summary>
		/// Retries the given action if it has failed with the specified exception.
		/// </summary>
		/// <typeparam name="TException">The type of the exception.</typeparam>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval (in milliseconds).</param>
		/// <param name="action">The action.</param>
		public static void RetryIfFailed<TException>(int maxRetries, int retryInterval, Action action) where TException : Exception {
			RetryIfFailed<object, TException>(maxRetries, retryInterval, () => {
				action();
				return null;
			});
		}

		/// <summary>
		/// Retries the given action if it has failed for any reason.
		/// </summary>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval (in milliseconds).</param>
		/// <param name="action">The action.</param>
		public static void RetryIfFailed(int maxRetries, int retryInterval, Action action) {
			RetryIfFailed<object, Exception>(maxRetries, retryInterval, () => {
				action();
				return null;
			});
		}

		/// <summary>
		/// Retries the given action if it has failed for any reason.
		/// </summary>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval (in milliseconds).</param>
		/// <param name="action">The action.</param>
		public static TResult RetryIfFailed<TResult>(int maxRetries, int retryInterval, Func<TResult> action) {
			TResult result = RetryIfFailed<TResult, Exception>(maxRetries, retryInterval, action);
			return result;
		}

		/// <summary>
		/// Gets the physical path.
		/// </summary>
		/// <returns></returns>
		public static string GetPhysicalPath() {
			return HttpContext.Current.Request.PhysicalApplicationPath;
		}


		/// <summary>
		/// Retries the given action if the retun value is false.
		/// </summary>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval (in milliseconds).</param>
		/// <param name="action">The action.</param>
		/// <param name="tracingRelatedKeyEntity">The tracing related key entity.</param>
		public static void RetryIfFalse(int maxRetries, int retryInterval, Func<bool> action, string tracingRelatedKeyEntity = null) {
			int currentAttemptNumber = 1;

			while (true) {
				// Try performing the action.
				bool success = action();

				if (success) {
					break;
				}

				if (currentAttemptNumber > maxRetries) {
					// We retried all the times we were allowed, let the caller handle this.
					throw new VizeliaException(String.Format("Operation returned false, attempted {0} times. Will not retry further.", maxRetries));
				}

				// Log the attempt to the trace.
				string message = String.Format("Operation returned false at attempt {0} out of {1}. Will retry.", currentAttemptNumber, maxRetries);

				if (tracingRelatedKeyEntity != null) {
					TracingService.Write(TraceEntrySeverity.Warning, message, "RetryIfFailed", tracingRelatedKeyEntity);
				}
				else {
					TracingService.Write(TraceEntrySeverity.Warning, message);
				}

				// Retry after a while.
				currentAttemptNumber++;
				Thread.Sleep(retryInterval);
			}
		}

		/// <summary>
		/// Gets the report header text. Is in format "Generated By: ,Created On:"
		/// </summary>
		/// <returns></returns>
		public static string GetReportHeaderText() {
			return LocalizeText("msg_generated_by") + " " + GetCurrentUserName() + ", " + LocalizeText("msg_generated_on_date") + " " + Helper.DateTimeNow.ToString(DateTimeHelper.DateTimeFormat(false));
		}

		/// <summary>
		/// Transforms the logging category.
		/// </summary>
		/// <param name="sourceCategory">The source category.</param>
		/// <returns></returns>
		public static string TransformLoggingCategory(string sourceCategory) {
			string destinationCategory = null;

			if (VizeliaConfiguration.Instance != null && VizeliaConfiguration.Instance.LoggingCategories != null) {
				LoggingCategoryElement transformation =
					VizeliaConfiguration.Instance.LoggingCategories.OfType<LoggingCategoryElement>().SingleOrDefault(
						c => c.SourceCategory.Equals(sourceCategory));

				if (transformation != null) {
					destinationCategory = transformation.DestinationCategory;
				}
			}

			return destinationCategory;
		}

		/// <summary>
		/// Creates a new TransactionScope
		/// </summary>
		/// <returns></returns>
		public static TransactionScope CreateTransactionScope() {
			// Due to maintance of distributed transactions (MSDTC); We changed the TransactionScopeOptions from Reuired to Supress
			// However; the method stub is still in place if future implementation will require distributed TX.
			var retVal = new TransactionScope(TransactionScopeOption.Suppress, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted });
			return retVal;
		}

		/// <summary>
		/// Gets the resource stream.
		/// </summary>
		/// <param name="resource">The resource.</param>
		/// <returns></returns>
		public static Stream GetResourceStream(string resource) {
			var assembly = Assembly.Load("Vizelia.FOL.Web");
			var stream = assembly.GetManifestResourceStream(resource);

			return stream;
		}

		/// <summary>
		/// Starts a new thread with but copy the HttpContext and all context helper variables
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="copyContext">if set to <c>true</c> [copy context].</param>
		public static Task StartNewThread(Action action, bool copyContext = true) {
			return StartNewThread<object>(() => {
				action();
				return null;
			}, copyContext);
		}

		/// <summary>
		/// Starts a new thread with but copy the HttpContext and all context helper variables
		/// </summary>
		public static Task<TPartialResult> StartNewThread<TPartialResult>(Func<TPartialResult> action, bool copyContext = true) {
			Task<TPartialResult> task;

			if (copyContext) {
				task = StartNewThreadWithContext(action);
			}
			else {
				task = StartNewThreadWithoutContext(action);
			}

			return task;
		}

		private static Task<TPartialResult> StartNewThreadWithContext<TPartialResult>(Func<TPartialResult> action) {
			string applicationName = ContextHelper.ApplicationName;
			string url = ContextHelper.Url;
			string requestId = ContextHelper.RequestId;
			var currentCulture = Thread.CurrentThread.CurrentCulture;
			var currentUICulture = Thread.CurrentThread.CurrentUICulture;
			var traceScope = TracingService.GetCurrentTraceScope();

			HttpContext httpcontext = HttpContext.Current;

			Task<TPartialResult> task = new TaskFactory().StartNew(() => {
				if (httpcontext != null) {
					HttpContext.Current = httpcontext;
				}

				ContextHelper.ApplicationName = applicationName;
				ContextHelper.Url = url;
				ContextHelper.RequestId = requestId;
				Thread.CurrentThread.CurrentCulture = currentCulture;
				Thread.CurrentThread.CurrentUICulture = currentUICulture;

				ITraceScope threadTraceScope = null;

				if (traceScope != null) {
					threadTraceScope = TracingService.ContinueTracing(traceScope);
				}

				TPartialResult retVal = default(TPartialResult);

				try {
					retVal = action();
				}
				catch (Exception exception) {
					try {
						TracingService.Write(exception);
					}
					catch {
						// Do nothing. The background thread must never crash!
					}
				}
				finally {
					if (threadTraceScope != null) {
						threadTraceScope.Dispose();
					}
				}

				return retVal;
			});

			return task;
		}

		private static Task<TPartialResult> StartNewThreadWithoutContext<TPartialResult>(Func<TPartialResult> action) {
			Task<TPartialResult> task = new TaskFactory().StartNew(() => {
				TPartialResult retVal = default(TPartialResult);

				try {
					retVal = action();
				}
				catch (Exception exception) {
					try {
						TracingService.Write(exception);
					}
					catch {
						// Do nothing. The background thread must never crash!
					}
				}

				return retVal;
			});

			return task;
		}

		/// <summary>
		/// Gets the property info MSG key.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="propertyInfo">The property info.</param>
		/// <returns></returns>
		public static string GetPropertyLocalizedName(Type type, PropertyInfo propertyInfo) {
			var msgKey = propertyInfo.GetAttributeValue<InformationAttribute>("MsgCode") ??
				   string.Format("msg_{0}_{1}", type.Name, propertyInfo.Name).ToLower();
			return LocalizeText(msgKey);
		}


		/// <summary>
		/// Uses the official EC2 documentation to grab the instance-id.
		/// </summary>
		/// <param name="instanceID">instanceID of the machine.</param>
		/// <returns>true if running in EC2 instance and was able go grab the instanceID in timely manner (5 seconds).</returns>
		internal static bool IsRunningInEC2(out string instanceID) {
			bool retval = false;
			try {
				var request = WebRequest.Create("http://169.254.169.254/latest/meta-data/instance-id");
				request.Timeout = 5000;
				StreamReader sr = new StreamReader(request.GetResponse().GetResponseStream());
				instanceID = sr.ReadLine();
				retval = true;
			}
			catch (Exception) {
				instanceID = string.Empty;
				retval = false;
			}
			return retval;
		}

		/// <summary>
		/// Determines whether a string represents a valid culture.
		/// </summary>
		/// <param name="Culture">The culture.</param>
		/// <returns>
		///   <c>true</c> if [is valid culture] [the specified culture]; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsValidCulture(string Culture) {
			if (string.IsNullOrWhiteSpace(Culture))
				return false;
			
			try {
				CultureInfo.GetCultureInfo(Culture);
			}
			catch {
				return false;
			}

			return true;
		}

		/// <summary>
		/// Converts the date time to a semester string. - For example: H2 - 2012
		/// You can change the "Q" by changing Langue.msg_chart_semester_label_prefix
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <returns></returns>
		public static string ConvertDateTimeToSemesterString(DateTime dateTime) {
			return Langue.msg_chart_semester_label_prefix + Math.Ceiling(dateTime.Month / 6d) + " " + dateTime.Year;
		}


		/// <summary>
		/// Converts the date time to a quarter string. - For example: Q2 - 2012
		/// You can change the "Q" by changing Langue.msg_chart_quarter_label_prefix
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <returns></returns>
		public static string ConvertDateTimeToQuarterString(DateTime dateTime) {
			return Langue.msg_chart_quarter_label_prefix + Math.Ceiling(dateTime.Month / 3d) + " " + dateTime.Year;
		}

		/// <summary>
		/// Returns the local time to be used as "current" (in place of DateTime.Now) for the purpose of 
		/// drawing charts and interpreting data. Use the real DateTime.Now to generate unique
		/// cache keys or file names.
		/// </summary>
		public static DateTime DateTimeNow {
			get {
				var configTime = VizeliaConfiguration.Instance.Deployment.DateTimeNow;

				if (configTime == VizeliaConfiguration.Instance.Deployment.TimeFreezeOffDateTimeValue) {
					return DateTime.Now;
				}

				return configTime;
			}
		}

		/// <summary>
		/// Returns the UTC time to be used as "current" (in plase of DateTime.UtcNow) for the purpose
		/// of drawing charts and interpreting data. Use the real DateTime.UtcNow to generate unique
		/// cache keys or file names.
		/// </summary>
		public static DateTime DateTimeUtcNow {
			get {
				var configTime = VizeliaConfiguration.Instance.Deployment.DateTimeNow;

				if (configTime == VizeliaConfiguration.Instance.Deployment.TimeFreezeOffDateTimeValue) {
					return DateTime.UtcNow;
				}

				DateTime utcTime = configTime.ConvertTimeToUtc(TimeZoneInfo.Local);

				return utcTime;
			}
		}

		/// <summary>
		/// Gets the specific store resulting entities.The result will merge the saved results with those that are not saved.
		/// </summary>
		/// <typeparam name="T">The type of the store.</typeparam>
		/// <param name="store">The store.</param>
		/// <param name="existingEntities">Already saved records.</param>
		/// <param name="predicate">Lambda expression key predicate.</param>
		/// <returns>All the entities in the result.</returns>
		public static IEnumerable<string> GetResultingEntities<T>(CrudStore<T> store, IEnumerable<T> existingEntities, Func<T, string> predicate) where T : BaseBusinessEntity {

			var result = new List<string>();
			if (existingEntities != null) {
				var existingValues = existingEntities.Select(predicate);
				var exisitingValuesEnumerable = existingValues as IList<string> ?? existingValues.ToList();
				if (exisitingValuesEnumerable.Any()) {
					result.AddRange(exisitingValuesEnumerable);
				}
			}

			if (store != null) {
				if (store.create != null) {
					result.AddRange(store.create.Select(predicate));
				}

				if (store.update != null) {
					result.AddRange(store.update.Select(predicate));
				}

				if (store.destroy != null) {
					foreach (var m in store.destroy.Select(predicate)) {
						result.Remove(m);
					}
				}
			}

			//We should never send null because we want the values be taken from the code and not from the database
			//That because the database changes after the save of the entity and the state here is not always relevant.
			if (result.Count == 0) {
				result.Add("-1");
			}
			return result;
		}
	}
}

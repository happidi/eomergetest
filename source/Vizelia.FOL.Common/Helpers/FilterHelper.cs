﻿using System;
using System.Collections.Generic;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;
using System.Linq;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A static helper for filtering data.
	/// </summary>
	public static class FilterHelper {
		private const string const_filter_initialized = "filter_initialized";

		/// <summary>
		/// Marks the filters as initialized for the current user session.
		/// </summary>
		public static void InitializeFilters() {
			SessionService.Add(const_filter_initialized, true);
		}

		/// <summary>
		/// Gets a value indicating whether filters were initialized for the current user session..
		/// </summary>
		/// <param name="data">The session data.</param>
		/// <returns>
		///   <c>true</c> if filter initialized; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsInitialized(IDictionary<string, object> data) {
			var retval = ContextHelper.Get(const_filter_initialized);
			if (retval != null) {
				return Convert.ToBoolean(retval);
			}

			if (data.ContainsKey(const_filter_initialized)) {
				retval = data[const_filter_initialized];
			}

			ContextHelper.Add(const_filter_initialized, retval);
			return Convert.ToBoolean(retval);
		}


		/// <summary>
		/// Clears the filters.
		/// </summary>
		public static void ClearFilters() {
			SessionService.Remove(const_filter_initialized);
			var filterKeys = Helper.GetSecurableTypes().Select(type => SessionKey.FilterPrefix + type.Name).ToList();
			SessionService.Remove(filterKeys);
		}


		/// <summary>
		/// Clears a specific filter
		/// </summary>
		public static void ClearFilter<T>() {
			var filterKey= SessionKey.FilterPrefix + typeof(T).Name;
			SessionService.Remove(filterKey);
		}

		/// <summary>
		/// Adds the filter.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="filter">The key.</param>
		public static void AddFilter<T>(Filter filter) {
			AddFilter(filter, typeof(T));
		}

		/// <summary>
		/// Adds the filters.
		/// </summary>
		/// <param name="dictionary">The dictionary.</param>
		public static void AddFilters(Dictionary<Type, List<Filter>> dictionary) {
			var KeyValueDictionary = new Dictionary<string, object>(dictionary.Count);
			foreach (var item in dictionary) {
				item.Value.AddRange(GetFilter(item.Key));
				KeyValueDictionary.Add(SessionKey.FilterPrefix + item.Key.Name, item.Value);
			}
			SessionService.Add(KeyValueDictionary);
		}

		/// <summary>
		/// Adds the filters.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="filters">The filters.</param>
		public static void AddFilters<T>(List<Filter> filters) {
			AddFilters(typeof(T), filters);
		}

		/// <summary>
		/// Adds the filters.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="filters">The filters.</param>
		/// <param name="sessionID">The session id.</param>
		public static void AddFilters<T>(List<Filter> filters, string sessionID) {
			AddFilters(typeof(T), filters, sessionID);
		}

		/// <summary>
		/// Adds the filters.
		/// </summary>
		/// <param name="entityType">Type of the entity.</param>
		/// <param name="filters">The filters.</param>
		/// <param name="sessionID">The session id.</param>
		public static void AddFilters(Type entityType, List<Filter> filters, string sessionID) {
			List<Filter> filterList = GetFilter(entityType); // SessionHelper.Get(SessionKey.FilterPrefix + entityType.Name) as List<string> ?? new List<string>();
			filterList.AddRange(filters);
			SessionService.Add(SessionKey.FilterPrefix + entityType.Name, filterList);
		}


		/// <summary>
		/// Adds the filters.
		/// </summary>
		/// <param name="entityType">Type of the entity.</param>
		/// <param name="filters">The filters.</param>
		public static void AddFilters(Type entityType, List<Filter> filters) {
			AddFilters(entityType, filters, SessionService.GetSessionID());
		}

		/// <summary>
		/// Adds a new key to the current Filter using the entityType
		/// </summary>
		/// <param name="filter">The Key to add.</param>
		/// <param name="entityType">Type of the entity.</param>
		public static void AddFilter(Filter filter, Type entityType) {
			// auto start provider defensive chat
			if (HttpContext.Current == null)
				return;
			// Only do this if we have a current session
		    if (HttpContext.Current.User.Identity.IsAuthenticated) {
                List<Filter> filterList = GetFilter(entityType); //SessionHelper.Get(SessionKey.FilterPrefix + entityType.Name) as List<string> ?? new List<string>();
                filterList.Add(filter);

                SessionService.Add(SessionKey.FilterPrefix + entityType.Name, filterList);
            }		    
		}

		/// <summary>
		/// Gets the filter.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static List<Filter> GetFilter<T>() {
			return GetFilter(typeof(T));
		}

		/// <summary>
		/// Gets the filter.
		/// </summary>
		/// <param name="entityType">Type of the entity.</param>
		/// <returns></returns>
		public static List<Filter> GetFilter(Type entityType) {
			//auto start provider defensive chat

			if (typeof(ILocation).IsAssignableFrom(entityType)) {
				entityType = typeof(Location);
			}

			if (HttpContext.Current == null)
				return new List<Filter>();

			//We do two calls at the same time for session items in order to improve performance
			var filterKey = SessionKey.FilterPrefix + entityType.Name;
			var data = SessionService.GetData(new List<string>() { filterKey, const_filter_initialized });
			// we 'reload' all Filters in case the filter was not initialized (e.g. server restarted, but user still has a valid authentication cookie)
			if (HttpContext.Current.User != null && HttpContext.Current.User.Identity != null && HttpContext.Current.User.Identity.IsAuthenticated && !IsInitialized(data)) {
				Helper.ExecuteServiceByReflection("Vizelia.FOL.WCFService.AuthenticationWCF", "User_GetCurrent", null);
				data = SessionService.GetData(new List<string>() { filterKey, const_filter_initialized });
			}

			List<Filter> filter;
			if ((HttpContext.Current.User.Identity.IsAuthenticated) && (data.ContainsKey(filterKey))) {
				filter = data[filterKey] as List<Filter> ?? new List<Filter>();
			}

			else {
				filter = new List<Filter>();
			}
			return filter;
		}

		/// <summary>
		/// Gets all the filters.
		/// </summary>
		/// <returns></returns>
		public static Dictionary<Type, List<Filter>> GetAll() {
			var retVal = new Dictionary<Type, List<Filter>>();
			Helper.GetSecurableTypes().ForEach(type => {
				retVal.Add(type, GetFilter(type));
			});
			return retVal;
		}


	}
}

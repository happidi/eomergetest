using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Ionic.Zip;
using Ionic.Zlib;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A Zip file compression utility
	/// </summary>
	internal class ZipCompressionUtility : ICompressionUtility {

		/// <summary>
		/// Determines whether the specified file is an archive.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <returns>
		///   <c>true</c> if the specified file is an archive; otherwise, <c>false</c>.
		/// </returns>
		public bool IsArchive(string fileName) {
			bool endsWithZip = fileName.ToLower().EndsWith(".zip");

			return endsWithZip;
		}

		/// <summary>
		/// Determines whether the specified file is an archive.
		/// </summary>
		/// <param name="archiveFile">The archive file.</param>
		/// <returns>
		///   <c>true</c> if the specified file is an archive; otherwise, <c>false</c>.
		/// </returns>
		public bool IsArchive(Stream archiveFile) {
			archiveFile.Seek(0, SeekOrigin.Begin);
			bool isZipFile = ZipFile.IsZipFile(archiveFile, false);
			return isZipFile;
		}

		/// <summary>
		/// Extracts the specified archive file name to the output directory.
		/// </summary>
		/// <param name="archiveFileName">Name of the archive file.</param>
		/// <param name="outputDirectory">The output directory.</param>
		/// <param name="overwriteExistingFiles">if set to <c>true</c> overwrites existing files.</param>
		public void Extract(string archiveFileName, string outputDirectory, bool overwriteExistingFiles) {
			using (FileStream zipStream = File.Open(archiveFileName, FileMode.Open, FileAccess.Read, FileShare.None)) {
				Extract(zipStream, outputDirectory, overwriteExistingFiles);
			}

		}

		/// <summary>
		/// Extracts the specified archive file to the output directory.
		/// </summary>
		/// <param name="archive">The archive.</param>
		/// <param name="outputDirectory">The output directory.</param>
		/// <param name="overwriteExistingFiles">if set to <c>true</c> overwrites existing files.</param>
		public void Extract(Stream archive, string outputDirectory, bool overwriteExistingFiles) {
			archive.Seek(0, SeekOrigin.Begin);

			using (ZipFile zipFile = ZipFile.Read(archive)) {
				var existingFileAction = overwriteExistingFiles ? ExtractExistingFileAction.OverwriteSilently : ExtractExistingFileAction.Throw;
				zipFile.ExtractAll(outputDirectory, existingFileAction);
			}
		}

		/// <summary>
		/// Gets the files from the archive.
		/// </summary>
		/// <param name="archive">The archive.</param>
		/// <returns>A pair of file name and contents. Each file content stream is disposed when the next file is returned.</returns>
		public IEnumerable<Tuple<string, Stream>> GetFiles(Stream archive) {
			archive.Seek(0, SeekOrigin.Begin);

			using (ZipFile zipFile = ZipFile.Read(archive)) {
				foreach (ZipEntry entry in zipFile) {
					if(entry.IsDirectory || entry.IsText) continue;

					using(var fileContents = new MemoryStream()) {
						entry.Extract(fileContents);
						// This is intentionally a yield return so that deferred execution will be performed 
						// and disposal of streams will happen only in the end.
						yield return new Tuple<string, Stream>(entry.FileName, fileContents);
					}
				}
			}
		}


		/// <summary>
		/// Compresses the specified files with names into the archive.
		/// </summary>
		/// <param name="filesWithNames">The files with names.</param>
		/// <returns>A stream containing the archive file.</returns>
		public Stream Compress(IEnumerable<Tuple<string, Stream>> filesWithNames) {
			var zipFile = new ZipFile {
				CompressionLevel = CompressionLevel.BestCompression,
				AlternateEncoding = Encoding.UTF8 // for international file names.
			};

			foreach (var fileWithName in filesWithNames) {
				zipFile.AddEntry(fileWithName.Item1, fileWithName.Item2);
			}

			var outputStream = new MemoryStream();
			zipFile.Save(outputStream);

			// Reset the stream for reading by caller.
			outputStream.Seek(0, SeekOrigin.Begin);

			return outputStream;
		}

		/// <summary>
		/// Compresses the specified files with names into the archive.
		/// </summary>
		/// <param name="directories">The directories.</param>
		/// <param name="zipFileName">Name of the zip file.</param>
		/// <param name="level">The level.</param>
		public void Compress(IEnumerable<string> directories, string zipFileName, CompressionLevel level) {
			var zipFile = new ZipFile {
				CompressionLevel = level,
				AlternateEncoding = Encoding.UTF8 // for international file names.
			};

			foreach (var directory in directories) {
				string path = directory;
				if (!directory.EndsWith("\\")) {
					path += "\\";
				}
				zipFile.AddDirectory(path, Path.GetFileName(Path.GetDirectoryName(path)));
			}

			zipFile.Save(zipFileName);
		}
	}
}
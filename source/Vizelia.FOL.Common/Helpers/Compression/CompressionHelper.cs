﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Ionic.Zlib;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A static helper for compression tasks.
	/// </summary>
	public class CompressionHelper {
		private static readonly List<ICompressionUtility> compressionUtilities = FindCompressionUtilities();

		/// <summary>
		/// Finds compression utilities using Reflection.
		/// </summary>
		/// <returns></returns>
		private static List<ICompressionUtility> FindCompressionUtilities() {
			var utilities = 
				typeof (CompressionHelper).Assembly.GetTypes()
					.Where(t => t.IsClass && !t.IsAbstract && typeof (ICompressionUtility).IsAssignableFrom(t))
					.Select(Activator.CreateInstance)
					.OfType<ICompressionUtility>()
					.ToList();

			return utilities;
		}


		/// <summary>
		/// Decompresses the specified zipped data.
		/// </summary>
		/// <param name="zippedData">The zipped data.</param>
		/// <returns></returns>
		public static byte[] Decompress(byte[] zippedData) {
			byte[] decompressedData = null;
			using (MemoryStream outputStream = new MemoryStream()) {
				using (MemoryStream inputStream = new MemoryStream(zippedData)) {
					using (GZipStream zip = new GZipStream(inputStream, CompressionMode.Decompress)) {
						zip.CopyTo(outputStream);
					}
				}
				decompressedData = outputStream.ToArray();
			}

			return decompressedData;
		}

		/// <summary>
		/// Compresses the specified plain data.
		/// </summary>
		/// <param name="plainData">The plain data.</param>
		/// <returns></returns>
		public static byte[] Compress(byte[] plainData) {
			byte[] compressesData = null;
			using (MemoryStream outputStream = new MemoryStream()) {
				using (GZipStream zip = new GZipStream(outputStream, CompressionMode.Compress)) {
					zip.Write(plainData, 0, plainData.Length);
				}
				//Dont get the MemoryStream data before the GZipStream is closed
				//since it doesn’t yet contain complete compressed data.
				//GZipStream writes additional data including footer information when its been disposed
				compressesData = outputStream.ToArray();
			}

			return compressesData;
		}


		/// <summary>
		/// Determines whether the specified file is an archive.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <returns>
		///   <c>true</c> if the specified file is an archive; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsArchive(string fileName) {
			bool isArchive = compressionUtilities.Any(u => u.IsArchive(fileName));

			return isArchive;
		}

		/// <summary>
		/// Extracts the specified archive file name to the output directory.
		/// </summary>
		/// <param name="archiveFileName">Name of the archive file.</param>
		/// <param name="outputDirectory">The output directory.</param>
		/// <param name="overwriteExistingFiles">if set to <c>true</c> overwrite existing files.</param>
		public static void Extract(string archiveFileName, string outputDirectory, bool overwriteExistingFiles) {
			ICompressionUtility compressionUtility = GetCompressionUtility(archiveFileName);
			compressionUtility.Extract(archiveFileName, outputDirectory, overwriteExistingFiles);
		}

		/// <summary>
		/// Extracts the specified archive file to the output directory.
		/// </summary>
		/// <param name="archive">The archive.</param>
		/// <param name="outputDirectory">The output directory.</param>
		/// <param name="overwriteExistingFiles">if set to <c>true</c> overwrite existing files.</param>
		public static void Extract(Stream archive, string outputDirectory, bool overwriteExistingFiles) {
			ICompressionUtility compressionUtility = GetCompressionUtility(archive);
			compressionUtility.Extract(archive, outputDirectory, overwriteExistingFiles);
		}

		/// <summary>
		/// Gets the files from the archive.
		/// </summary>
		/// <param name="archive">The archive.</param>
		/// <returns>A pair of file name and contents. Each file content stream is disposed when the next file is returned.</returns>
		public static IEnumerable<Tuple<string, Stream>> GetFiles(Stream archive) {
			ICompressionUtility compressionUtility = GetCompressionUtility(archive);
			var files = compressionUtility.GetFiles(archive);

			// This is intentionally a yield return so that deferred execution will be performed 
			// and disposal of streams will happen only in the end.
			foreach (var file in files) {
				yield return file;
			}
		}

		/// <summary>
		/// Compresses the specified files with names into the archive.
		/// </summary>
		/// <param name="filesWithNames">The files with names.</param>
		/// <param name="archiveFileName">Name of the archive file.</param>
		/// <returns>
		/// A stream containing the archive file.
		/// </returns>
		public static Stream Compress(IEnumerable<Tuple<string, Stream>> filesWithNames, string archiveFileName) {
			ICompressionUtility compressionUtility = GetCompressionUtility(archiveFileName);
			Stream compressedFile = compressionUtility.Compress(filesWithNames);
			
			return compressedFile;
		}

		/// <summary>
		/// Compresses the specified directories.
		/// </summary>
		/// <param name="directories">The directories.</param>
		/// <param name="archiveFileName">Name of the archive file.</param>
		/// <param name="level">The level.</param>
		public static void Compress(IEnumerable<string> directories, string archiveFileName, CompressionLevel level) {
			ICompressionUtility compressionUtility = GetCompressionUtility(archiveFileName);
			compressionUtility.Compress(directories, archiveFileName, level);
		}

		/// <summary>
		/// Gets the compression utility for the given archive name.
		/// </summary>
		/// <param name="archiveFileName">Name of the archive file.</param>
		private static ICompressionUtility GetCompressionUtility(string archiveFileName) {
			ICompressionUtility compressionUtility = compressionUtilities.FirstOrDefault(u => u.IsArchive(archiveFileName));

			if (compressionUtility == null) {
				throw new ArgumentException("archiveFileName does not represent an archive");
			}
			return compressionUtility;
		}

		/// <summary>
		/// Gets the compression utility for the given archive contents.
		/// </summary>
		/// <param name="archiveFile">The archive file.</param>
		/// <returns></returns>
		private static ICompressionUtility GetCompressionUtility(Stream archiveFile) {
			ICompressionUtility compressionUtility = compressionUtilities.FirstOrDefault(u => u.IsArchive(archiveFile));

			if (compressionUtility == null) {
				throw new ArgumentException("archive stream does not represent an archive");
			}
			return compressionUtility;
		}

		/// <summary>
		/// Extracts all archives in the working directory,
		/// and extracts any contained archives up to the specified amount of nested archives levels deep.
		/// Deletes the original archives.
		/// </summary>
		/// <param name="workingDirectoryPath">The working directory path.</param>
		/// <param name="overwriteExistingFiles">if set to <c>true</c> overwrite existing files.</param>
		/// <param name="depth">The maximum nested archives depth.</param>
		/// <returns>True if archives were found and extracted, false if no archives were found.</returns>
		public static bool ExtractAllArchives(string workingDirectoryPath, bool overwriteExistingFiles, int depth) {
			var workingDirectory = new DirectoryInfo(workingDirectoryPath);

			// Iteratively extract all archives.
			bool isDone = false;
			bool foundArchives = false;

			for (int i = 0; i < depth + 1; i++) {
				FileInfo[] allFiles = workingDirectory.GetFiles("*.*", SearchOption.AllDirectories);
				FileInfo[] archives = allFiles.Where(file => IsArchive(file.Name)).ToArray();

				if (archives.Length == 0) {
					isDone = true;
					break;
				}

				foreach (FileInfo archive in archives) {
					foundArchives = true;
					string pathToExtractTo = Path.Combine(archive.DirectoryName, string.Format("archive_{0}", archive.Name));
					Extract(archive.FullName, pathToExtractTo, overwriteExistingFiles);

					archive.Delete();
				}
			}

			if (!isDone) {
				// This means we did all the iterations and still have zip files.
				// This is a very deeply nested zip file! We don't process that. Could be a zip bomb.
				throw new Exception("Archive files contains too many levels of nested archive files.");
			}

			return foundArchives;
		}
	}
}
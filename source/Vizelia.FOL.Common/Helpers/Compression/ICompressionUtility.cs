using System;
using System.Collections.Generic;
using System.IO;
using Ionic.Zlib;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Represents a compression utility.
	/// </summary>
	internal interface ICompressionUtility {

		/// <summary>
		/// Determines whether the specified file is an archive.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <returns>
		///   <c>true</c> if the specified file is an archive; otherwise, <c>false</c>.
		/// </returns>
		bool IsArchive(string fileName);

		/// <summary>
		/// Determines whether the specified file is an archive.
		/// </summary>
		/// <param name="archiveFile">The archive file.</param>
		/// <returns>
		///   <c>true</c> if the specified file is an archive; otherwise, <c>false</c>.
		/// </returns>
		bool IsArchive(Stream archiveFile);

		/// <summary>
		/// Extracts the specified archive file name to the output directory.
		/// </summary>
		/// <param name="archiveFileName">Name of the archive file.</param>
		/// <param name="outputDirectory">The output directory.</param>
		/// <param name="overwriteExistingFiles">if set to <c>true</c> [overwrite existing files].</param>
		void Extract(string archiveFileName, string outputDirectory, bool overwriteExistingFiles);

		/// <summary>
		/// Compresses the specified files with names into the archive.
		/// </summary>
		/// <param name="filesWithNames">The files with names.</param>
		/// <returns>A stream containing the archive file.</returns>
		Stream Compress(IEnumerable<Tuple<string, Stream>> filesWithNames);

		/// <summary>
		/// Extracts the specified archive file to the output directory.
		/// </summary>
		/// <param name="archive">The archive.</param>
		/// <param name="outputDirectory">The output directory.</param>
		/// <param name="overwriteExistingFiles">if set to <c>true</c> overwrites existing files.</param>
		void Extract(Stream archive, string outputDirectory, bool overwriteExistingFiles);

		/// <summary>
		/// Gets the files from the archive.
		/// </summary>
		/// <param name="archive">The archive.</param>
		/// <returns>A pair of file name and contents.</returns>
		IEnumerable<Tuple<string, Stream>> GetFiles(Stream archive);

		/// <summary>
		/// Compresses the specified target directory.
		/// </summary>
		/// <param name="directories">The directories.</param>
		/// <param name="zipFileName">Name of the zip file.</param>
		/// <param name="level">The level.</param>
		void Compress(IEnumerable<string> directories, string zipFileName, CompressionLevel level);
	}
}
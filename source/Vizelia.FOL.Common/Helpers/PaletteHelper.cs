﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Vizelia.FOL.BusinessEntities;
using System.Drawing;


namespace Vizelia.FOL.Common {
	/// <summary>
	/// A helper class for working with image
	/// </summary>
	public class PaletteHelper {

		/// <summary>
		/// Generates a random Palette (always the same).
		/// </summary>
		/// <param name="colorCount">The color count.</param>
		/// <returns></returns>
		public static Palette GeneratePalette(int colorCount = 20) {

			var random = new Random(1);
			var retVal = new Palette { MsgCode="", Colors = new List<PaletteColor>()};

			for (var i = 0; i < colorCount; i++) {
				var color = new PaletteColor() {
					ColorB = random.Next(255),
					ColorR = random.Next(255),
					ColorG = random.Next(255),
					Order = i
				};
				retVal.Colors.Add(color);
			}
			return retVal;
		}
	}
}

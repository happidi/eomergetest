﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A helper class for working with Meter
	/// </summary>
	public class MeterHelper {

		#region Key Conversions
		/// <summary>
		/// Converts the meter key to int.
		/// </summary>
		/// <param name="KeyMeter">The Meter key.</param>
		/// <returns></returns>
		public static int ConvertMeterKeyToInt(string KeyMeter) {
			var retVal = 0;
			if (!string.IsNullOrEmpty(KeyMeter)) {
				retVal = Convert.ToInt32(KeyMeter.Replace("#", ""));
			}
			return retVal;
		}

		/// <summary>
		/// Converts the meter key to string.
		/// </summary>
		/// <param name="KeyMeter">The Meter key.</param>
		/// <returns></returns>
		public static string ConvertMeterKeyToString(int KeyMeter) {
			var retVal = "#" + KeyMeter.ToString();
			return retVal;
		}
		#endregion

		#region FillMissingValues
		/// <summary>
		/// Fills the missing values.
		/// </summary>
		/// <param name="meter">The meter.</param>
		/// <param name="timeinterval">The timeinterval.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="finalTimeZone">The final time zone.</param>
		public static void FillMissingValues(Meter meter, AxisTimeInterval timeinterval, DateTime startDate, DateTime endDate, TimeZoneInfo finalTimeZone = null) {
		    if (timeinterval != AxisTimeInterval.None && timeinterval != AxisTimeInterval.Global && meter.MDData != null) {
		        if (finalTimeZone == null)
		            finalTimeZone = TimeZoneInfo.Utc;
		        var elements = new Dictionary<DateTime, List<MD>>();

		        meter.MDData = meter.MDData.OrderBy(md => md.AcquisitionDateTime).ToList();
		        foreach (MD md in meter.MDData) {
		            var simplifyDate = md.AcquisitionDateTime.ConvertTimeFromUtc(finalTimeZone).UpdateByInterval(timeinterval);
		            List<MD> list;
		            if (!elements.TryGetValue(simplifyDate, out list)) {
		                list = new List<MD>();
		                elements.Add(simplifyDate, list);
		            }
		            list.Add(md);
		        }

		        //We create here the missing elements with null value
		        var date = startDate.ConvertTimeFromUtc(finalTimeZone).UpdateByInterval(timeinterval);
		        //we have to keep the real date for timezone issues.
		        var dateReal = startDate;

		        var value = meter.MDData[0].Value;
		        if (meter.MDData.Count > 1) {
		            var lastMD = meter.MDData.LastOrDefault(md => md.AcquisitionDateTime <= dateReal);
		            if (lastMD != null)
		                value = lastMD.Value;
		        }

		        while (date <= endDate) {
		            if (elements.Keys.Contains(date) == false || elements[date] == null || elements[date].Count == 0) {
		                MD md = new MD {AcquisitionDateTime = date.ConvertTimeToUtc(finalTimeZone), Value = value};
		                meter.MDData.Add(md);
		            }
		            else {
		                value = elements[date].Last().Value;
		                dateReal = elements[date].Last().AcquisitionDateTime;
		            }
		            date = date.AddInterval(timeinterval, 1);
		            dateReal = dateReal.AddInterval(timeinterval, 1);
		        }
		        //we sort the meter data by Acquisition Datetime.
		        meter.MDData = meter.MDData.OrderBy(md => md.AcquisitionDateTime).ToList();

		    }
		    else {
		        if (meter.MDData != null) {
		            MD last = meter.MDData.OrderBy(md => md.AcquisitionDateTime).Last();
		            if (last.AcquisitionDateTime < startDate) { // if the last date of the meter data is before the chart start date add one point to fill the missing values.
		                MD newMD = new MD { AcquisitionDateTime = startDate, Value = last.Value };
		                meter.MDData.Add(newMD);    
		            }
		        }
		    }
		}
		#endregion


		/// <summary>
		/// Generate MeterData for testing purposes.
		/// </summary>
		/// <param name="numberOfMeter">The number of meter.</param>
		/// <param name="maxCount">The max count.</param>
		/// <param name="meterKey">The meter key.</param>
		/// <param name="meterDataKey">The meter data key.</param>
		/// <returns></returns>
		public static ConcurrentDictionary<int, SortedList<DateTime, MD>> GenerateMeterData(int numberOfMeter, int maxCount = 0, int meterKey = 0, int meterDataKey = 0) {
			var retVal = new ConcurrentDictionary<int, SortedList<DateTime, MD>>();

			int count = 0;
			Parallel.For(1, numberOfMeter + 1, (i, loopState) => {
				//for (var i = 1; i <= numberOfMeter; i++) {
				var dictionary = new SortedList<DateTime, MD>(35137);
				var startDate = new DateTime(2012, 1, 1);
				var endDate = new DateTime(2013, 1, 1);
				while (startDate <= endDate) {
					if (maxCount > 0 && count > maxCount)
						break;

					var md = new MD() {
						AcquisitionDateTime = DateTime.SpecifyKind(startDate, DateTimeKind.Utc),
						Value = ThreadSafeRandom.Next(1, 100),
						KeyMeter = meterKey + i,
						KeyMeterData = meterDataKey + count,
						//Validity = 1
					};
					dictionary.Add(DateTime.SpecifyKind(startDate, DateTimeKind.Utc), md);
					count += 1;
					startDate = startDate.AddMinutes(15);
				}
				retVal.TryAdd(i, dictionary);

				if (maxCount > 0 && count > maxCount)
					loopState.Stop();
			});

			return retVal;
		}

		/// <summary>
		/// ThreadSafeRandom
		/// </summary>
		public class ThreadSafeRandom {
			private static Random random = new Random();

			/// <summary>
			/// Nexts this instance.
			/// </summary>
			/// <returns></returns>
			public static int Next() {
				lock (random) {
					return random.Next();
				}
			}

			/// <summary>
			/// Nexts the specified min value.
			/// </summary>
			/// <param name="minValue">The min value.</param>
			/// <param name="maxValue">The max value.</param>
			/// <returns></returns>
			public static int Next(int minValue, int maxValue) {
				lock (random) {
					return random.Next(minValue, maxValue);
				}
			}
		}

		/// <summary>
		/// Changes the validity field string value to int.
		/// Used in cases of generic paging.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="fieldName">Name of the field.</param>
		public static void ChangeValidityFieldStringValueToInt(PagingParameter paging, string fieldName) {
			if (paging.filters == null) return;

			foreach (GridFilter filter in paging.filters) {
				if (filter.field.Equals(fieldName)) {
					try {
						filter.data.value = filter.data.value.ParseAsEnum<MeterDataValidity>().ParseAsInt().ToString(CultureInfo.InvariantCulture);
					}
					catch {
						// Do nothing.						
					}
				}
			}
		}



	}
}

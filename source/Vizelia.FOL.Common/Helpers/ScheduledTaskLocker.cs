using System;
using System.Collections.Concurrent;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A mechanism for locking tasks, under the assumption that tasks are only run on a single server, and
	/// locking scope is at the AppDomain level. Meaning, the lock is not multi-tenant.
	/// </summary>
	/// <typeparam name="TEntity">The type of the entity.</typeparam>
	internal class ScheduledTaskLocker<TEntity> : IDisposable {
		// This in-memory lock is good while tasks are always run on the same AppDomain.
		// If we are moving to a server farm scenario and tasks may run on several computers,
		// then we need to change the storage, to for example, A database, and updating using 
		// pessimistic insert/update of records.
		private static readonly ConcurrentDictionary<string, string> m_Locks = new ConcurrentDictionary<string, string>();
	
		private readonly string m_CacheKey;
		private readonly string m_LockValue;
		private bool m_IsLocking;

		/// <summary>
		/// Initializes a new instance of the <see cref="ScheduledTaskLocker&lt;TEntity&gt;"/> class.
		/// </summary>
		/// <param name="key">The key.</param>
		public ScheduledTaskLocker(string key) {
			// The cache key must contain the tenant name for a case of multiple tenants in the same AppDomain (shared multi-tenant mode).
			m_CacheKey = string.Format("scheduled_task_lock_{0}_{1}_{2}", ContextHelper.ApplicationName, typeof(TEntity).Name, key);
			m_LockValue = Guid.NewGuid().ToString();
		}

		/// <summary>
		/// Locks the task.
		/// </summary>
		/// <returns>True if lock success, false if not.</returns>
		public bool Lock() {
			TracingService.Write(TraceEntrySeverity.Information, "Locking task.", "ScheduledTaskLocker", m_CacheKey);
			m_IsLocking = m_Locks.TryAdd(m_CacheKey, m_LockValue);

			if (!m_IsLocking) {
				// There is a cache record for this task. The task is already running on another thread.
				TracingService.Write(TraceEntrySeverity.Warning, "Already locked. An instance of this task is already running, quitting this task to allow the previous to complete.", "ScheduledTaskLocker", m_CacheKey);
			}

			return m_IsLocking;
		}

		/// <summary>
		/// Unlocks the mapping task.
		/// </summary>
		public void Unlock() {
			TracingService.Write(TraceEntrySeverity.Information, "Unlocking task", "ScheduledTaskLocker", m_CacheKey);
			bool success = DisposeInternal();

			if(!success) {
				throw new VizeliaException(
					"Lock codes do not match. This task is possibly running twice concurrently and causing errors.");
			}
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose() {
			DisposeInternal();
		}

		private bool DisposeInternal() {
			if (!m_IsLocking) return true;

			string removedValue;
			bool removeSuccess = m_Locks.TryRemove(m_CacheKey, out removedValue);
			bool success = removeSuccess && m_LockValue.Equals(removedValue);

			return success;
		}
	}
}
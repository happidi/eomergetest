﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Vizelia.FOL.BusinessEntities;

using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Encapsulates extension for the DataSerie class.
	/// </summary>
	public static class ChartHelper {

		#region Private
		/// <summary>
		/// Key for storing in the cache the Available DataSerie Localids.
		/// </summary>
		private const string const_cache_key_dataserie_localid = "Chart_GetAvailableDataSerieLocalId_";

		/// <summary>
		/// Key for storing in the cache the Chart Image.
		/// </summary>
		private const string const_cache_key_chart_stream = "Chart_ImageStream_";
		
		/// <summary>
		/// Key for storing in the cache the Chart with data.
		/// </summary>
		private const string const_cache_key_chart_withdata = "Chart_WithData_";

		/// <summary>
		/// Key for storing in the cache the DynamicDisplay with Chart.
		/// </summary>
		private const string const_cache_key_dynamicdisplay_withchart = "DynamicDisplay_WithChart_";
		
		/// <summary>
		/// Key for storing in the cache the Dynamic Displays.
		/// </summary>
		private const string const_cache_key_dynamicdisplay_stream = "DynamicDisplay_ImageStream_";
		
		/// <summary>
		/// Key for storing in the cache the Dynamic Displays with chart stream.
		/// </summary>
		private const string const_cache_key_dynamicdisplaywithchart_stream_and_padding = "DynamicDisplayWithChart_ImageStreamAndPadding_";

		/// <summary>
		/// Correlation cloud dataserie localid.
		/// </summary>
		private const string const_correlation_cloud_dataserie_localid = "Cloud";

		/// <summary>
		/// Correlation line dataserie localid.
		/// </summary>
		private const string const_correlation_line_dataserie_localid = "Line";

		/// <summary>
		/// Difference highlight dataserie localid.
		/// </summary>
		private const string const_differencehighlight_localid = "Difference";

		/// <summary>
		/// The default font family.
		/// </summary>
		private const string const_font_family = "Verdana";

		/// <summary>
		/// The start color of a gradient for a chart background
		/// </summary>
		private static readonly Color const_chart_background_gradient_start_color = Color.White;

		/// <summary>
		/// The end color of a gradient for a chart background
		/// </summary>
		private static readonly Color const_chart_background_gradient_end_color = Color.FromArgb(207, 243, 152);
		#endregion

		#region StartDate and EndDate

		/// <summary>
		/// Check if the Dynamic Timescale is enabled and calculated the appropriate start date and time.
		/// </summary>
		/// <param name="chart"></param>
		/// <returns></returns>
		public static DateTime GetDynamicStartDate(Chart chart){
			if (chart.DynamicTimeScaleEnabled){

				DateTime start = DateTimeHelper.CalculateDynamicStartDate(chart.DynamicTimeScaleInterval,
					chart.DynamicTimeScaleFrequency,
					chart.GetTimeZone(),
					chart.DynamicTimeScaleCalendar,
					chart.DynamicTimeScaleCompleteInterval,
					chart.XAxisStartDayOfWeek);

				return start;
			}

			return chart.StartDate;
		}

		/// <summary>
		/// Gets the dynamic end date.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public static DateTime GetDynamicEndDate(Chart chart) {
			if (!chart.DynamicTimeScaleEnabled) {
				return chart.EndDate;
			}

			var endDateTime = DateTimeHelper.CalculateDynamicEndDate(chart.DynamicTimeScaleInterval,
				chart.DynamicTimeScaleFrequency,
				chart.StartDate,
				chart.GetTimeZone(),
				chart.DynamicTimeScaleCalendarEndToNow);
		
			return endDateTime;
		}


		/// <summary>
		/// Get display format for a specific timeinterval
		/// </summary>
		/// <param name="timeinterval">the timeinterval</param>
		/// <param name="simple">if set to <c>true</c> [simple].</param>
		/// <returns>
		/// the format
		/// </returns>
		public static string GetFormat(this AxisTimeInterval timeinterval, bool simple = false) {
			var formatInfo = CultureInfo.GetCultureInfo(Helper.GetCurrentUICulture()).DateTimeFormat;
			string result = formatInfo.ShortDatePattern + " " + formatInfo.LongTimePattern;

			var shortDatePattern = formatInfo.ShortDatePattern.Replace("yyyy", "yy");//"MM/dd/yy";
			var yearMonthPattern = formatInfo.YearMonthPattern.Replace("MMMM", "MMM").Replace(",", "").Replace("yyyy", "yy");//"MMM yy"
			var dayPattern = "ddd dd"; //"ddd dd";
			switch (timeinterval) {
				case AxisTimeInterval.Seconds:
					result = simple ? dayPattern + " " + formatInfo.LongTimePattern : shortDatePattern + " " + formatInfo.LongTimePattern;//"ddd dd HH:mm:ss" : "MM/dd/yy HH:mm:ss";
					break;
				case AxisTimeInterval.Minutes:
				case AxisTimeInterval.Hours:
					result = simple ? dayPattern + " " + formatInfo.ShortTimePattern : shortDatePattern + " " + formatInfo.ShortTimePattern;//"ddd dd HH:mm" : "MM/dd/yy HH:mm";
					break;
				case AxisTimeInterval.Days:
				case AxisTimeInterval.None:
					result = shortDatePattern;//"MM/dd/yy";
					break;
				case AxisTimeInterval.Weeks:
					result = shortDatePattern;//"MM/dd/yy";
					break;
				case AxisTimeInterval.Months:
				case AxisTimeInterval.Quarters:
				case AxisTimeInterval.Semesters:
					result = yearMonthPattern;//"MMM yy";
					break;
				case AxisTimeInterval.Years:
				case AxisTimeInterval.Global:
					result = "yyyy";
					break;
			}

			return result;
			#region Old
			//string result = "dd/MM/yyyy HH:mm:ss";

			//if (Helper.GetCurrentCulture().StartsWith("en")) {

			//    switch (timeinterval) {
			//        case AxisTimeInterval.Seconds:
			//            result = simple ? "ddd dd HH:mm:ss" : "MM/dd/yy HH:mm:ss";
			//            break;
			//        case AxisTimeInterval.Minutes:
			//            result = simple ? "ddd dd HH:mm" : "MM/dd/yy HH:mm";
			//            break;
			//        case AxisTimeInterval.Hours:
			//            result = simple ? "ddd dd HH:mm" : "MM/dd/yy HH:mm";
			//            break;
			//        case AxisTimeInterval.Days:
			//        case AxisTimeInterval.None:
			//            result = "MM/dd/yy";
			//            break;
			//        case AxisTimeInterval.Weeks:
			//            result = "MM/dd/yy";
			//            break;
			//        case AxisTimeInterval.Months:
			//        case AxisTimeInterval.Quarters:
			//            result = "MMM yy";
			//            break;
			//        case AxisTimeInterval.Years:
			//        case AxisTimeInterval.Global:
			//            result = "yyyy";
			//            break;
			//    }
			//}
			//else {
			//    switch (timeinterval) {
			//        case AxisTimeInterval.Seconds:
			//            result = simple ? "ddd dd HH:mm:ss" : "dd/MM/yy HH:mm:ss";
			//            break;
			//        case AxisTimeInterval.Minutes:
			//            result = simple ? "ddd dd HH:mm" : "dd/MM/yy HH:mm";
			//            break;
			//        case AxisTimeInterval.Hours:
			//            result = simple ? "ddd dd HH:mm" : "dd/MM/yy HH:mm";
			//            break;
			//        case AxisTimeInterval.Days:
			//        case AxisTimeInterval.None:
			//            result = "dd/MM/yy";
			//            break;
			//        case AxisTimeInterval.Weeks:
			//            result = "dd/MM/yy";
			//            break;
			//        case AxisTimeInterval.Months:
			//        case AxisTimeInterval.Quarters:
			//            result = "MMM yy";
			//            break;
			//        case AxisTimeInterval.Years:
			//        case AxisTimeInterval.Global:
			//            result = "yyyy";
			//            break;
			//    }
			//}
			//return result;
			#endregion
		}

		/// <summary>
		/// Gets the actual dates based on the data in the Chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public static Tuple<DateTime, DateTime> GetActualDateRange(Chart chart) {
			var actualStartDate = DateTime.MaxValue;
			var actualEndDate = DateTime.MinValue;
			if (chart.Series != null && chart.Series.Count > 0) {
				foreach (var s in chart.Series.ToList()) {
					foreach (var p in s.Points) {
						if (p.XDateTime.HasValue) {
							if (p.XDateTime.Value < actualStartDate)
								actualStartDate = p.XDateTime.Value;
							if (p.XDateTime.Value > actualEndDate)
								actualEndDate = p.XDateTime.Value;
						}
					}
				}
			}

			if (actualStartDate < DateTime.MaxValue && actualEndDate > DateTime.MinValue) {
				return new Tuple<DateTime, DateTime>(actualStartDate, actualEndDate);
			}
			return new Tuple<DateTime, DateTime>(chart.StartDate, chart.EndDate);
		}
		#endregion

		#region Increment Title
		/// <summary>
		/// Increment a string that is an existing Title for example in case of a Copy in order to have a different Title.
		/// </summary>
		/// <param name="title">the string to update</param>
		public static string Increment(string title) {
			if (string.IsNullOrEmpty(title) == false && title.StartsWith(Langue.msg_copyof)) {
				var arr = title.Split(' ');
				var last = arr[arr.Length - 1];
				int index = 1;
				if (Int32.TryParse(last, out index)) {
					index += 1;
					title = title.Substring(0, title.Length - last.Length - 1);
				}
				else
					index = 1;
				return string.Format("{0} {1}", title, index);
			}
			else
				return string.Format("{0} {1}", Langue.msg_copyof, title);
		}
		#endregion

		#region ToDataTable
		/// <summary>
		/// Return the datatable corresponding to this chart.
		/// </summary>
		/// <param name="chart">the Chart.</param>
		/// <param name="shouldShowDifference">if set to <c>true</c> [should show difference] as percentage.</param>
		/// <param name="start">Where to start paging</param>
		/// <param name="pageSize">Size of the page</param>
		/// <param name="formatNumberAsString">if set to <c>true</c> [format number as string].</param>
		/// <param name="dataSerieCollection">The data serie collection.</param>
		/// <returns></returns>
		public static DataTable ToDataTablePaged(Chart chart, bool shouldShowDifference, int start, int pageSize, bool formatNumberAsString = true,
			DataSerieCollection dataSerieCollection = null) {
			if (!HasData(chart)) {
				return null;
			}

			if (IsTransposed(chart)) {
				DataTable transposedTable = ToDataTableTransposedPaged(chart, shouldShowDifference, start, pageSize, formatNumberAsString);
				return transposedTable;
			}

			var table = new DataTable();
			table.Columns.Add(Helper.LocalizeText("msg_datatype_date"));

			List<Tuple<string, DateTime>> categories;
			List<DataSerie> series;

			if (dataSerieCollection == null) {
				categories = GetCategories(chart);
				series = chart.Series.AsEnumerable().Where(s => !s.Hidden).ToList();
			}
			else {
				categories = GetCategories(chart, dataSerieCollection);
				series = dataSerieCollection.GetVisibleDataSeries().ToList();
			}

			DataColumn[] columns = series.Select(serie => new DataColumn {
				Caption = serie.Name,
				DataType = typeof(string),
				ColumnName = Guid.NewGuid().ToString()
			}).ToArray();

			table.Columns.AddRange(columns);

			var rowCategoryIndex = new Dictionary<string, int>();

			for (var i = start; i < Math.Min(categories.Count, start + pageSize); i++) {
				var row = table.NewRow();
				row[0] = categories[i].Item1;
				rowCategoryIndex.Add(categories[i].Item1, i - start);
				table.Rows.Add(row);
			}

			for (var j = 0; j < series.Count; j++) {
				for (var i = 0; i < series[j].Points.Count; i++) {
					Tuple<string, DateTime> category = GetCategory(series[j].Points[i], series[j], chart);

					if (!rowCategoryIndex.ContainsKey(category.Item1))
						continue;

					var rowIndex = rowCategoryIndex[category.Item1];
					var row = table.Rows[rowIndex];

					if (i < series[j].Points.Count && series[j].Points[i].YValue.HasValue) {
						double? currentPointYValue = series[j].Points[i].YValue;

						var value = currentPointYValue.Value.ToString(chart.NumericFormat);

						if (!shouldShowDifference || (i == 0) || series[j].Points[i - 1].YValue.HasValue == false) {
							if (formatNumberAsString) {
								row[j + 1] = value;
							}
							else {
								row[j + 1] = currentPointYValue.Value;
							}
						}
						else {
							var percentage = GetDifferenceAsPercentage(currentPointYValue, series[j].Points[i - 1].YValue);
							row[j + 1] = string.Format("{0} ({1})", value, percentage);
						}
					}
				}
			}
			return table;
		}

		/// <summary>
		/// Encodes the dictionary key.
		/// </summary>
		/// <param name="tuple">The tuple.</param>
		/// <returns></returns>
		private static string EncodeDictionaryKey(Tuple<string, DateTime> tuple) {
			string key = tuple.Item2.Equals(DateTime.MinValue)
							? tuple.Item1
							: tuple.Item2.ToString("o", CultureInfo.InvariantCulture);
			return key;
		}

		/// <summary>
		/// Return the datatable corresponding to this chart.
		/// </summary>
		/// <param name="chart">the Chart.</param>
		/// <param name="shouldShowDifference">if set to <c>true</c> [should show difference] as percentage.</param>
		/// <param name="start">Where to start paging</param>
		/// <param name="pageSize">Size of the page</param>
		/// <param name="formatNumberAsString">if set to <c>true</c> [format number as string].</param>
		/// <returns></returns>
		public static DataTable ToDataTableTransposedPaged(Chart chart, bool shouldShowDifference, int start, int pageSize, bool formatNumberAsString = true) {
			if (!HasData(chart)) {
				return null;
			}

			var table = new DataTable();
			table.Columns.Add(Helper.LocalizeText("msg_datatype_date"));

			List<Tuple<string, DateTime>> categories = GetCategories(chart);

			DataColumn[] columns = categories.Select(
				c => new DataColumn {
					Caption = c.Item1,
					DataType = typeof(string),
					ColumnName = Guid.NewGuid().ToString()
				}).ToArray();
			table.Columns.AddRange(columns);

			List<DataSerie> series = chart.Series.ToList().Where(s => !s.Hidden).ToList();

			for (var i = start; i < Math.Min(series.Count, start + pageSize); i++) {
				var row = table.NewRow();
				row[0] = series[i].Name;

				// now for each serie get the value in the category of the column.
				for (int j = 0; j < categories.Count; j++) {
					for (int k = 0; k < series[i].Points.Count; k++) {
						if ((series[i].Points[k].XDateTime == categories[j].Item2) && (series[i].Points[k].YValue.HasValue)) {
							row[j + 1] = series[i].Points[k].YValue.Value.ToString(chart.NumericFormat);
						}
					}
				}
				table.Rows.Add(row);
			}


			return table;
		}

		/// <summary>
		/// Gets the difference as percentage.
		/// </summary>
		/// <param name="newValue">The new value.</param>
		/// <param name="oldValue">The old value.</param>
		/// <returns></returns>
		public static string GetDifferenceAsPercentage(double? newValue, double? oldValue) {
			double delta;
			return GetDifferenceAsPercentage(newValue, oldValue, out delta);
		}

		/// <summary>
		/// Gets the difference as percentage.
		/// </summary>
		/// <param name="newValue">The new value.</param>
		/// <param name="oldValue">The old value.</param>
		/// <param name="delta">The delta.</param>
		/// <returns></returns>
		public static string GetDifferenceAsPercentage(double? newValue, double? oldValue, out double delta) {
			if (oldValue.HasValue == false || newValue.HasValue == false || oldValue.Value == 0) {
				delta = double.NaN;
				return "NA";
			}
			delta = ((newValue.Value - oldValue.Value) / oldValue.Value) * 100;
			return Convert.ToInt32(Math.Floor(delta)).ToString(CultureInfo.InvariantCulture) + "%";
		}
		/// <summary>
		/// Determines whether the specified chart has data.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns>
		///   <c>true</c> if the specified chart has data; otherwise, <c>false</c>.
		/// </returns>
		public static bool HasData(Chart chart) {
			bool hasData = chart != null && chart.Series != null && chart.Series.ToList().Any(s => s.Points.Any(p => p.YValue.HasValue));

			return hasData;
		}

		/// <summary>
		/// Convert a datatable to HTML.
		/// </summary>
		/// <param name="dt">The datatable.</param>
		/// <returns></returns>
		public static string ToHtml(DataTable dt) {
			StringWriter sw = new StringWriter();
			HtmlTextWriter w = new HtmlTextWriter(sw);

			//Create a table
			Table tbl = new Table();
			//styling
			//tbl.Height = new Unit(100, UnitType.Percentage);
			//tbl.Width = new Unit(100, UnitType.Percentage);
			//tbl.Style.Add("border-collapse", "separate");
			//tbl.Style.Add(HtmlTextWriterStyle.Margin, "0 0 20px");
			//Create column header row
			TableHeaderRow thr = new TableHeaderRow();
			//thr.BorderColor = Color.Black;
			//thr.BorderWidth = 1;
			//thr.BorderStyle = BorderStyle.Solid;
			if (dt != null) {

				foreach (DataColumn col in dt.Columns) {
					TableHeaderCell th = new TableHeaderCell();
					/*th.Style.Add(HtmlTextWriterStyle.TextAlign, "left");
					th.Style.Add("display", "table-cell");
					th.Style.Add("vertical-align", "bottom");
					th.Style.Add("padding-bottom", "5px");
					th.Style.Add("padding-top", "10px");
					th.Style.Add("padding-left", "5px");
					th.Style.Add("border-bottom", "1px #ddd solid");
					th.Style.Add("border-right", "1px #ddd solid");
					th.Style.Add("border-left", "1px transparent solid");
					th.Style.Add("border-top", "1px transparent solid");
					th.Style.Add("font-family", "'Segoe UI', 'Open Sans', Verdana, Arial, Helvetica, sans-serif");
					th.Style.Add("font-weight", "400");
					th.Style.Add("font-size", "11pt");
					th.Style.Add("letter-spacing", "0.01em");
					th.Style.Add("line-height", "14pt");
					//th.Style.Add("color", "rgba(0, 0, 0, 0.6)");
					th.Style.Add("text-align", "left");
					*/
					th.Text = col.Caption;
					thr.Controls.Add(th);
				}
				tbl.Controls.Add(thr);

				//Create table rows
				foreach (DataRow row in dt.Rows) {
					TableRow tr = new TableRow();
					foreach (var value in row.ItemArray) {
						TableCell td = new TableCell();
						/*td.BorderColor = Color.Transparent;
						td.Style.Add(HtmlTextWriterStyle.TextAlign, "left");
						td.Style.Add(HtmlTextWriterStyle.FontFamily, "'Segoe UI Semilight', 'Open Sans', Verdana, Arial, Helvetica, sans-serif");
						td.Style.Add(HtmlTextWriterStyle.FontWeight, "300");
						td.Style.Add(HtmlTextWriterStyle.FontSize, "11pt");
						td.Style.Add(HtmlTextWriterStyle.Padding, "3px 10px");
						td.Style.Add("line-height", "20px");
						td.Style.Add("border-right", "1px #ddd solid");
						td.Style.Add("border-bottom", "1px #ddd solid");
						td.Style.Add("box-sizing", "border-box");
						 */
						td.Text = value.ToString();
						tr.Controls.Add(td);
					}
					tbl.Controls.Add(tr);
				}
			}
			tbl.RenderControl(w);
			return sw.ToString();
		}
		#endregion

		#region GetCategories
		/// <summary>
		/// Return the XAxis Categories as a list of string.
		/// </summary>
		/// <param name="chart">The Chart.</param>
		/// <returns></returns>
		public static List<Tuple<string, DateTime>> GetCategories(Chart chart) {
			DataSerieCollection dataSerieCollection = chart.Series;

			return GetCategories(chart, dataSerieCollection);
		}

		/// <summary>
		/// Return the XAxis Categories as a list of string.
		/// Use this for secondary data serie collections, such as for historical analysis.
		/// </summary>
		/// <param name="chart">The Chart.</param>
		/// <param name="dataSerieCollection">The data serie collection.</param>
		/// <returns>A list of tuples of string and datetime containing the list of categories.
		/// The string part is populated with the string representation of the category.
		/// The DateTime part is populated with the item original datetime if this is a DateTime series.
		/// Otherwise, the DateTime part is populated with DateTime.MinValue.</returns>
		public static List<Tuple<string, DateTime>> GetCategories(Chart chart, DataSerieCollection dataSerieCollection) {
			DateTime dummyValue = DateTime.MinValue;
			var dates = new SortedSet<DateTime>();
			var categories = new List<Tuple<string, DateTime>>();

			foreach (DataSerie s in dataSerieCollection.GetVisibleDataSeries()) {
				if (s.IsDateTime) {
					// for is faster than foreach, plus we use a hashset.
					for (int i = 0; i < s.Points.Count; i++) {
						var point = s.Points[i];

						if (point.YValue.HasValue && point.XDateTime.HasValue) {
							dates.Add(point.XDateTime.Value);
						}
					}
				}
				else {
					foreach (DataPoint p in s.PointsWithValue) {
						if (!string.IsNullOrEmpty(p.Name) && !categories.Any(c => c.Item1.Equals(p.Name))) {
							categories.Add(new Tuple<string, DateTime>(p.Name, dummyValue));
						}
					}
				}
			}

			if (dates.Count > 0) {
				var a = dates.Select(d => new Tuple<string, DateTime>(GetCategoryName(d,chart), d));
				categories.AddRange(a);
			}

			return categories;
		}


		/// <summary>
		/// Gets the name of the category.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		private static string GetCategoryName(DateTime dateTime, Chart chart) {
			if (chart.TimeInterval == AxisTimeInterval.Semesters) 
			{
				return Helper.ConvertDateTimeToSemesterString(dateTime);
			}
			if (chart.TimeInterval == AxisTimeInterval.Quarters) {
				return Helper.ConvertDateTimeToQuarterString(dateTime);
			}
			
			return dateTime.ToString(chart.TimeIntervalFormat);
			
		}

		/// <summary>
		/// Gets the category of the data point.
		/// </summary>
		/// <param name="point">The point.</param>
		/// <param name="serie">The serie.</param>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public static Tuple<string, DateTime> GetCategory(DataPoint point, DataSerie serie, Chart chart) {
			Tuple<string, DateTime> category;

			if (serie.IsDateTime) {
				if (!point.XDateTime.HasValue) {
					throw new ArgumentException("Point is of a DateTime serie but has no value.");
				}
				
				category = new Tuple<string, DateTime>( GetCategoryName(point.XDateTime.Value,chart), point.XDateTime.Value);
			}
			else {
				string categoryName;

				if (point.Name != null) {
					categoryName = point.Name;
				}
				else if (point.XValue.HasValue) {
					categoryName = point.XValue.Value.ToString(CultureInfo.InvariantCulture);
				}
				else {
					throw new VizeliaException("Point is not of a DateTime serie, and has no name or x value. Cannot create category name.");
				}

				category = new Tuple<string, DateTime>(categoryName, DateTime.MinValue);
			}

			return category;
		}

		#endregion


		#region LocalIds
		/// <summary>
		/// Returns the Correlation cloud dataserie localid.
		/// </summary>
		public static string GetCorrelationCloudDataSerieLocalId() {
			return const_correlation_cloud_dataserie_localid;
		}

		/// <summary>
		/// Returns the Correlation line dataserie localid.
		/// </summary>
		public static string GetCorrelationLineDataSerieLocalId() {
			return const_correlation_line_dataserie_localid;
		}

		/// <summary>
		/// Returns the Correlation line dataserie localid.
		/// </summary>
		public static string GetDifferenceHighlightLocalId() {
			return const_differencehighlight_localid;
		}

		/// <summary>
		/// Gets the cache key for a chart available data serie local id list.
		/// </summary>
		/// <param name="key">The key of the Chart.</param>
		/// <param name="includeExisting">if set to <c>true</c> [include existing].</param>
		/// <param name="cultureInfo">The CultureInfo</param>
		/// <returns></returns>
		public static string GetCacheKey_AvailableDataSerieLocalId(string key, bool includeExisting, string cultureInfo = null) {
			var retVal = string.Format("{0}{1}_{2}_{3}", const_cache_key_dataserie_localid, key, includeExisting, cultureInfo ?? Helper.GetCurrentUICulture());
			return retVal;
		}

		/// <summary>
		/// Gets the cache key for a Chart image stream.
		/// </summary>
		/// <param name="key">The key of the Chart.</param>
		/// <param name="cultureInfo">The culture info</param>
		/// <returns></returns>
		private static string GetCacheKey_ChartImageStream(string key, string cultureInfo = null) {
			string retVal = string.Format("{0}{1}{2}", const_cache_key_chart_stream, key, cultureInfo ?? Helper.GetCurrentUICulture());
			return retVal;
		}
		
		/// <summary>
		/// Gets the cache key for a Chart data.
		/// </summary>
		/// <param name="key">The key of the Chart.</param>
		/// <param name="cultureInfo">The culture.</param>
		/// <returns></returns>
		public static string GetCacheKey_ChartWithData(string key, string cultureInfo = null) {
			string retVal = string.Format("{0}{1}{2}", const_cache_key_chart_withdata, key, cultureInfo ?? Helper.GetCurrentUICulture());
			return retVal;
		}

		/// <summary>
		/// Gets the cache key for a Dynamic display data.
		/// </summary>
		/// <param name="keyChart">The key chart.</param>
		/// <param name="keyDynamicDisplay">The key dynamic display.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public static string GetCacheKey_DynamicDisplayWithChart(string keyChart, string keyDynamicDisplay, string culture = null) {
			string retVal = string.Format("{0}{1}_{2}_{3}", const_cache_key_dynamicdisplay_withchart, keyDynamicDisplay, keyChart, culture ?? Helper.GetCurrentUICulture());
			return retVal;
		}

		/// <summary>
		/// Gets the cache key for a Dynamic Display image stream.
		/// </summary>
		/// <param name="key">The key of the Dynamic Display.</param>
		/// <param name="cultureInfo"></param>
		/// <returns></returns>
		private static string GetCacheKey_DynamicDisplayImageStream(string key, string cultureInfo = null) {
			string retVal = string.Format("{0}{1}{2}", const_cache_key_dynamicdisplay_stream, key, cultureInfo ?? Helper.GetCurrentUICulture());
			return retVal;
		}

		/// <summary>
		/// Gets the cache key for a Dynamic Display with a Chart.
		/// </summary>
		/// <param name="keyDynamicDisplay">The key dynamic display.</param>
		/// <param name="keyChart">The key chart.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		private static string GetCacheKey_DynamicDisplayWithChartImageStreamAndPadding(string keyDynamicDisplay, string keyChart, string culture) {
			string retVal = string.Format("{0}{1}_{2}_{3}", const_cache_key_dynamicdisplaywithchart_stream_and_padding, keyDynamicDisplay, keyChart, culture);
			return retVal;
		}
		
		#endregion

		#region Cache

		/// <summary>
		/// Clears the dynamic display cache.
		/// </summary>
		/// <param name="keyDynamicDisplay">The key dynamic display.</param>
		/// <param name="chartKeys">The list of all KeyCharts using this dynamic display.</param>
		public static void DynamicDisplay_ClearCache(string keyDynamicDisplay, IEnumerable<string> chartKeys) {
			var cacheKey = GetCacheKey_DynamicDisplayImageStream(keyDynamicDisplay);
			CacheService.Remove(cacheKey);

			foreach (var keyChart in chartKeys) {

				cacheKey = GetCacheKey_DynamicDisplayWithChart(keyChart, keyDynamicDisplay);
				CacheService.Remove(cacheKey);

				foreach (var cultureInfo in CultureInfo.GetCultures(CultureTypes.AllCultures).Select(x => x.ToString())) {
					
					cacheKey = GetCacheKey_DynamicDisplayWithChartImageStreamAndPadding(keyDynamicDisplay, keyChart, cultureInfo);
					CacheService.Remove(cacheKey);
				}
			}
		}

		/// <summary>
		/// Clears the chart cache (image,data and dynamic display).
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="keyDynamicDisplay">The key dynamic display.</param>
		public static void Chart_ClearCache(string key, string keyDynamicDisplay = null) {
			Chart_ClearDataSerie(key);
			Chart_ClearChartWithData(key);
			Chart_ClearChartImageCache(key);
			Chart_ClearChartDynamicDisplay(key, keyDynamicDisplay);
		}

		/// <summary>
		/// Clear the chart's dynamic display associated with it.
		/// </summary>
		/// <param name="key">The key</param>
		private static void Chart_ClearChartWithData(string key) {
			if (string.IsNullOrEmpty(key))
				return;

			CacheService.Remove(GetCacheKey_ChartWithData(key));

			foreach (var cultureInfo in CultureInfo.GetCultures(CultureTypes.AllCultures).Select(x => x.ToString())) {
				CacheService.Remove(GetCacheKey_ChartWithData(key, cultureInfo));
			}
		}

		/// <summary>
		/// Clear the chart's dynamic display associated with it.
		/// </summary>
		/// <param name="key">The key.</param>
		private static void Chart_ClearDataSerie(string key) {
			if (string.IsNullOrEmpty(key))
				return;

			CacheService.Remove(GetCacheKey_AvailableDataSerieLocalId(key, true));
			CacheService.Remove(GetCacheKey_AvailableDataSerieLocalId(key, false));

			foreach (var cultureInfo in CultureInfo.GetCultures(CultureTypes.AllCultures).Select(x => x.ToString())) {
				CacheService.Remove(GetCacheKey_AvailableDataSerieLocalId(key, true, cultureInfo));
				CacheService.Remove(GetCacheKey_AvailableDataSerieLocalId(key, false, cultureInfo));
			}
		}

		/// <summary>
		/// Clear chart image cache (not the data cached).
		/// </summary>
		/// <param name="key">The key.</param>
		private static void Chart_ClearChartImageCache(string key) {

			CacheService.Remove(GetCacheKey_ChartImageStream(key));

			foreach (var cultureInfo in CultureInfo.GetCultures(CultureTypes.AllCultures).Select(x => x.ToString())) {
				CacheService.Remove(GetCacheKey_ChartImageStream(key, cultureInfo));
			}
		}

		/// <summary>
		/// Clear the chart's dynamic display associated with it.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="keyDynamicDisplay">The key dynamic display.</param>
		public static void Chart_ClearChartDynamicDisplay(string key, string keyDynamicDisplay) {

			if (string.IsNullOrEmpty(keyDynamicDisplay))
				return;

			foreach (string cultureInfo in CultureInfo.GetCultures(CultureTypes.AllCultures).Select(x => x.ToString())) {
				CacheService.Remove(GetCacheKey_DynamicDisplayWithChart(key, keyDynamicDisplay, cultureInfo));

				var cacheKey = GetCacheKey_DynamicDisplayWithChartImageStreamAndPadding(keyDynamicDisplay, key, cultureInfo);
				CacheService.Remove(cacheKey);
			}
		}

		/// <summary>
		///  Get the Chart stream image from cache.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public static StreamResult Chart_GetStreamImageFromCache(string Key, int width, int height) {
			var cacheKey = GetCacheKey_ChartImageStream(Key);
			var retVal = GetStreamImageFromCache(cacheKey, width, height);
			return retVal;
		}

		/// <summary>
		///  Get the DynamicDisplay stream image from cache.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public static StreamResult DynamicDisplay_GetStreamImageFromCache(string Key, int width, int height) {
			var cacheKey = GetCacheKey_DynamicDisplayImageStream(Key);
			var retVal = GetStreamImageFromCache(cacheKey, width, height);
			return retVal;
		}

		/// <summary>
		/// Get the DynamicDisplay with chart image and padding from cache.
		/// </summary>
		/// <param name="KeyDynamicDisplay">The key dynamic display.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public static Tuple<StreamResult, Padding> DynamicDisplay_GetStreamImageAndPaddingFromCache(string KeyDynamicDisplay, string KeyChart, int width, int height, string culture) {
			var cacheKey = GetCacheKey_DynamicDisplayWithChartImageStreamAndPadding(KeyDynamicDisplay, KeyChart, culture);
			var retVal = GetStreamImageAndPaddingFromCache(cacheKey, width, height);
			return retVal;
		}
		
		/// <summary>
		/// Add the Chart stream image to cache.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="stream">The stream.</param>
		public static void Chart_AddStreamImageToCache(string Key, int width, int height, StreamResult stream) {
			var cacheKey = GetCacheKey_ChartImageStream(Key);
			AddStreamImageToCache(cacheKey, width, height, stream);
		}


		/// <summary>
		/// Add the DynamicDisplay stream image to cache.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="stream">The stream.</param>
		public static void DynamicDisplay_AddStreamImageToCache(string Key, int width, int height, StreamResult stream) {
			var cacheKey = GetCacheKey_DynamicDisplayImageStream(Key);
			AddStreamImageToCache(cacheKey, width, height, stream);
		}
		
		/// <summary>
		/// Add the DynamicDisplay stream image to cache.
		/// </summary>
		/// <param name="KeyDynamicDisplay">The key dynamic display.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="tuple">The tuple.</param>
		public static void DynamicDisplay_AddStreamImageAndPaddingToCache(string KeyDynamicDisplay, string KeyChart, int width, int height, string culture, Tuple<StreamResult, Padding> tuple) {
			var cacheKey = GetCacheKey_DynamicDisplayWithChartImageStreamAndPadding(KeyDynamicDisplay, KeyChart, culture);
			AddStreamImageAndPaddingToCache(cacheKey, width, height, tuple);
		}

		/// <summary>
		/// Adds the stream image to cache.
		/// </summary>
		/// <param name="cacheKey">The cache key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="stream">The stream.</param>
		public static void AddStreamImageToCache(string cacheKey, int width, int height, StreamResult stream) {
			var collection = CacheService.Get<StreamResultCollection>(cacheKey, true);
			collection.Add(stream, width, height);
			CacheService.Add(cacheKey, collection, 120);
		}

		/// <summary>
		/// Adds the stream image and padding to cache.
		/// </summary>
		/// <param name="cacheKey">The cache key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="tuple">The tuple.</param>
		public static void AddStreamImageAndPaddingToCache(string cacheKey, int width, int height, Tuple<StreamResult, Padding> tuple) {
			var collection = CacheService.Get<ObjectResultCollection<Tuple<StreamResult, Padding>>>(cacheKey, true);
			collection.Add(tuple, width, height);
			CacheService.Add(cacheKey, collection, 120);
		}

		/// <summary>
		/// Gets the stream image from cache.
		/// </summary>
		/// <param name="cacheKey">The cache key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		private static StreamResult GetStreamImageFromCache(string cacheKey, int width, int height) {
			var collection = CacheService.Get<StreamResultCollection>(cacheKey, () => new StreamResultCollection());
			StreamResult retVal = null;
			if (collection != null)
				collection.TryGetStreamResult(width, height, out retVal);
			return retVal;
		}

		/// <summary>
		/// Gets the stream image from cache.
		/// </summary>
		/// <param name="cacheKey">The cache key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		private static Tuple<StreamResult, Padding> GetStreamImageAndPaddingFromCache(string cacheKey, int width, int height) {
			var collection = CacheService.Get<ObjectResultCollection<Tuple<StreamResult, Padding>>>(cacheKey, () => new ObjectResultCollection<Tuple<StreamResult, Padding>>());
			Tuple<StreamResult, Padding> retVal = null;
			if (collection != null)
				collection.TryGetResult(width, height, out retVal);
			return retVal;
		}
		#endregion

		#region  GenerateDateRangeLabel
		/// <summary>
		/// Generates the date range label.
		/// </summary>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="timezone">The timezone.</param>
		/// <returns></returns>
		public static string GenerateDateRangeLabel(DateTime startDate, DateTime endDate, TimeZoneInfo timezone) {
			string dateRangeLabel = startDate.ConvertTimeFromUtc(timezone).ToString(AxisTimeInterval.Minutes.GetFormat());
			dateRangeLabel += " - ";
			dateRangeLabel += endDate.ConvertTimeFromUtc(timezone).ToString(AxisTimeInterval.Minutes.GetFormat());
			return dateRangeLabel;
		}
		#endregion

		#region Old
		///// <summary>
		///// Generate Dataseries based on the list of calculatedseries and add them to the chart.
		///// </summary>
		///// <param name="chart">The Chart.</param>
		//public static void GenerateCalculatedSeries(this Chart chart) {

		//    if (chart.CalculatedSeries != null && chart.CalculatedSeries.Count > 0) {

		//        /*foreach (var s in chart.Series) {
		//            s.GroupByTimeInterval(AxisTimeInterval.None);
		//        }
		//        */
		//        ChartFormulaEvaluator formula = new ChartFormulaEvaluator(chart.Series.ToList());

		//        foreach (var calculatedserie in chart.CalculatedSeries) {
		//            try {
		//                var r = formula.Evaluate(calculatedserie.Name, calculatedserie.Formula);
		//                var existing = (from s in chart.Series where s.Name == calculatedserie.Name select s).FirstOrDefault();
		//                if (existing != null) {
		//                    existing.Points = r.Points;
		//                    chart.Series.Add(existing);
		//                }
		//                else {
		//                    chart.Series.Add(r);
		//                }
		//            }
		//            catch (System.Exception e) {
		//                chart.Errors.Add(e.Message+" "+e.StackTrace);
		//            }
		//        }
		//    }
		//}






		///// <summary>
		///// Gets the X axis markers (Night,Weekend, Vacations...).
		///// </summary>
		///// <param name="chart">The chart.</param>
		///// <returns></returns>
		//public static  List<ChartTimeMarker> GetXAxisMarkers(this Chart chart) {
		//    List<ChartTimeMarker> result = new List<ChartTimeMarker>();
		//    ChartTimeMarker current = null;

		//    if (chart.TimeInterval != AxisTimeInterval.Global && chart.TimeInterval != AxisTimeInterval.None) {
		//        var date = chart.StartDate;
		//        var enddate = chart.EndDate.AddByTimeInterval(chart.TimeInterval, 1);

		//        switch (chart.TimeInterval) {
		//            case AxisTimeInterval.Days:
		//                while (date <= enddate) {
		//                    if (date.DayOfWeek==DayOfWeek.Saturday || date.DayOfWeek==DayOfWeek.Sunday) {
		//                        if (current == null) {
		//                            current = new ChartTimeMarker(chart.TimeInterval) {
		//                                ColorR = 255,
		//                                ColorB = 0,
		//                                ColorG = 0,
		//                                Min = date
		//                            };
		//                        }
		//                    }
		//                    else {
		//                        if (current != null) {
		//                            current.Max = date;
		//                            result.Add(current);
		//                            current = null;
		//                        }
		//                    }
		//                    date = date.AddByTimeInterval(chart.TimeInterval, 1);
		//                }
		//                break;


		//            case AxisTimeInterval.Hours:
		//                TimeSpan nightStartTime, nightEndTime;
		//                if (!string.IsNullOrEmpty(chart.NightStartTime) && !string.IsNullOrEmpty(chart.NightStartTime) && chart.NightColorR.HasValue && chart.NightColorG.HasValue && chart.NightColorB.HasValue && TimeSpan.TryParse(chart.NightStartTime, out nightStartTime) && TimeSpan.TryParse(chart.NightEndTime, out  nightEndTime)) {
		//                    while (date <= enddate) {
		//                        if (date.Hour >= nightStartTime.Hours || date.Hour <= nightEndTime.Hours) {
		//                            if (current == null) {
		//                                current = new ChartTimeMarker(chart.TimeInterval) {
		//                                    ColorR = chart.NightColorR,
		//                                    ColorG = chart.NightColorG,
		//                                    ColorB = chart.NightColorB,
		//                                    Min = date
		//                                };
		//                            }
		//                        }
		//                        else {
		//                            if (current != null) {
		//                                current.Max = date;
		//                                result.Add(current);
		//                                current = null;
		//                            }
		//                        }
		//                        date = date.AddByTimeInterval(chart.TimeInterval, 1);
		//                    }
		//                }						
		//                break;
		//        }
		//    }
		//    return result;
		//}
		#endregion

		#region Chart Tags Projection
		/// <summary>
		/// Determines whether the chart has at least a single point with value.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns>
		///   <c>true</c> if the chart has at least a single point with value; otherwise, <c>false</c>.
		/// </returns>
		private static bool HasPointsWithValues(Chart chart) {
			return chart.Series != null && chart.Series.ToList().Any(s => s.Points != null && s.Points.Count > 0 && s.PointsWithValue.Any());
		}

		/// <summary>
		/// Gets the min element of the chart.
		/// </summary>
		/// <param name="chart">The chart entity.</param>
		/// <returns></returns>
		public static double GetMinElement(Chart chart) {
			if (!HasPointsWithValues(chart))
				return 0;

			IEnumerable<double> points = chart.Series.ToList().SelectMany(s => s.PointsWithValue).Select(p => p.YValue.Value);
			double min = points.Min();

			return min;
		}

		/// <summary>
		/// Gets the max element of the chart.
		/// </summary>
		/// <param name="chart">The chart entity.</param>
		/// <returns></returns>
		public static double GetMaxElement(Chart chart) {
			if (!HasPointsWithValues(chart))
				return 0;

			IEnumerable<double> points = chart.Series.ToList().SelectMany(s => s.PointsWithValue).Select(p => p.YValue.Value);
			double max = points.Max();

			return max;
		}

		/// <summary>
		/// Gets the avg element of the chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public static double GetAvgElement(Chart chart) {
			if (!HasPointsWithValues(chart))
				return 0;

			IEnumerable<double> points = chart.Series.ToList().SelectMany(s => s.PointsWithValue).Select(p => p.YValue.Value);
			var avg = points.Average();

			return avg;
		}

		/// <summary>
		/// Gets the sum of all elements of the chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public static double GetSumElement(Chart chart) {
			if (!HasPointsWithValues(chart))
				return 0;

			IEnumerable<double> points = chart.Series.ToList().SelectMany(s => s.PointsWithValue).Select(p => p.YValue.Value);
			var sum = points.Sum();

			return sum;
		}
		#endregion

		#region Bogus Chart
		/// <summary>
		/// Gets a bogus chart.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="fontSize">Size of the font.</param>
		/// <param name="fontColor">Color of the font.</param>
		/// <param name="metroStyle">if set to <c>true</c> [metro style].</param>
		/// <returns></returns>
		public static Chart GetBogusChart(ChartType type = ChartType.Combo, int fontSize = 9, string fontColor = null, bool metroStyle = false) {
			var r = new Random();
			var chart = new Chart() {
				ChartType = type,
				DisplayFontSize = fontSize,
				DisplayLegendFontSize = fontSize,
				DisplayStartEndDateFontSize = fontSize,
				DisplayLegendVisible = type == ChartType.Combo
			};
			var s = new DataSerie() {
				Name = Langue.msg_chart_bogusdata,
				LocalId = Langue.msg_chart_bogusdata,
				Points = new List<DataPoint>()
			};
			chart.Series.Add(s);

			if (type == ChartType.Combo || type == ChartType.ComboHorizontal || type == ChartType.Radar || type == ChartType.TreeMap) {
				var startDate = new DateTime(DateTime.Now.Year, 1, 1);
				var endDate = new DateTime(DateTime.Now.Year + 1, 1, 1);
				while (startDate < endDate) {
					s.Points.Add(new DataPoint() {
						XDateTime = startDate,
						YValue = r.Next(100)
					});
					startDate = startDate.AddMonths(1);
				}

			}
			else if (type == ChartType.Donut || type == ChartType.Pie) {
				chart.Series.Clear();
				for (var i = 1; i < 4; i++) {
					s = new DataSerie() {
						Name = Langue.msg_chart_bogusdata + " " + i,
						LocalId = Langue.msg_chart_bogusdata + i,
						Points = new List<DataPoint>()
						{
						    new DataPoint() {
								Name = Langue.msg_chart_bogusdata + " " + i,
								YValue = r.Next(100)
							}
						}
					};
					chart.Series.Add(s);
				}
			}
			else if (type == ChartType.Gauge) {
				s.Points.Add(new DataPoint() {
					XDateTime = DateTime.UtcNow,
					YValue = r.Next(100)
				});
			}

			if (metroStyle) {
				chart.DisplayChartAreaBackgroundStart = null;
				chart.DisplayChartAreaBackgroundEnd = null;
				chart.LegendPosition = ChartLegendPosition.BottomMiddle;
				chart.DisplayShaddingEffect = ChartShaddingEffect.None;
				chart.DisplayFontColor = fontColor;
				chart.DisplayLegendPlain = true;
			}
			return chart;
		}
		#endregion

		#region Defaults values
		/// <summary>
		/// Gets the start color of the background.
		/// </summary>
		/// <returns></returns>
		public static Color GetDefaultBackgroundStartColor() {
			return const_chart_background_gradient_start_color;
		}

		/// <summary>
		/// Gets the end color of the background.
		/// </summary>
		/// <returns></returns>
		public static Color GetDefaultBackgroundEndColor() {
			return const_chart_background_gradient_end_color;
		}

		/// <summary>
		/// Gets the default font family.
		/// </summary>
		/// <returns></returns>
		public static string GetDefaultFontFamily() {
			return const_font_family;
		}
		#endregion

		#region UpdateChartHistoricalAnalysisDates

		/// <summary>
		/// Updates the chart historical analysis dates.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="historicalAnalysis">The historical analysis.</param>
		public static void UpdateChartHistoricalAnalysisDates(Chart chart, ChartHistoricalAnalysis historicalAnalysis){
			historicalAnalysis.StartDate = DateTimeHelper.AddTimeInterval(chart.StartDate, historicalAnalysis.Interval, -historicalAnalysis.Frequency);
			historicalAnalysis.EndDate = DateTimeHelper.AddTimeInterval(chart.EndDate, historicalAnalysis.Interval, -historicalAnalysis.Frequency);
		}

		#endregion

		#region Grid Mode

		/// <summary>
		/// Determines whether the chart is in grid mode or not.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns>
		///   <c>true</c> if the chart is in grid mode; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsGridMode(Chart chart) {
			var dataOnlyModes = new[] { ChartDisplayMode.Grid, ChartDisplayMode.CustomGrid };
			bool isGridMode = dataOnlyModes.Contains(chart.DisplayMode);

			return isGridMode;
		}

		/// <summary>
		/// Determines whether the specified chart is transposed.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns>
		///   <c>true</c> if the specified chart is transposed; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsTransposed(Chart chart) {
			bool isTransposed = chart.DisplayMode == ChartDisplayMode.Grid && chart.GridTranspose;
			return isTransposed;
		}

		#endregion

		#region IsTextAreaEmpty
		/// <summary>
		/// Determines whether a text area is empty.
		/// </summary>
		/// <param name="text">The text.</param>
		public static bool IsTextAreaEmpty(string text) {
			if (string.IsNullOrEmpty(text))
				return true;

			text = text.Replace("<br/>", "").Replace("<br>", "").Replace(" ", "").Trim();
			return text.Length <= 0;
		}
		#endregion

		#region UpdateCachedChart
		/// <summary>
		/// Updates the cached chart.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="chart">The chart.</param>
		public static void UpdateCachedChart(string KeyChart, Chart chart) {
			if (chart != null) {
				//we need to update the cached values
				var cacheKey = GetCacheKey_ChartWithData(KeyChart);
				//the cached chart contains all the "*" fields, not the chart
				var cachedChart = CacheService.GetData(cacheKey) as Chart;

				if (cachedChart != null) {
					//chart.Series = cachedChart.Series;
					//chart.HistoricalAnalysis = cachedChart.HistoricalAnalysis;
					//chart.Meters = cachedChart.Meters;
					var properties = chart.GetType().GetProperties();
					foreach (var propertyInfo in properties) {
						if (propertyInfo.Name != "Series" && propertyInfo.Name != "HistoricalAnalysis" && propertyInfo.Name != "Meters") {
							if (propertyInfo.HasAttribute<ChartDisplayOnlyAttribute>() && propertyInfo.CanWrite) {
								propertyInfo.SetValue(cachedChart, propertyInfo.GetValue(chart, null), null);
							}
						}
					}
					CacheService.Add(cacheKey, cachedChart);//,15
				}
			}
		}


		/// <summary>
		/// Determines whether [contains dynamic tags] [the specified chart].
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns>
		///   <c>true</c> if [contains dynamic tags] [the specified chart]; otherwise, <c>false</c>.
		/// </returns>
		public static bool ContainsDynamicTags(Chart chart) {
			var retVal = false;
			if (string.IsNullOrEmpty(chart.DynamicDisplayHeaderText) == false) {
				retVal = retVal || chart.DynamicDisplayHeaderText.Contains("@Model");
			}
			if (string.IsNullOrEmpty(chart.DynamicDisplayMainText) == false) {
				retVal = retVal || chart.DynamicDisplayMainText.Contains("@Model");
			}
			if (string.IsNullOrEmpty(chart.DynamicDisplayFooterText) == false) {
				retVal = retVal || chart.DynamicDisplayFooterText.Contains("@Model");
			}
			return retVal;
		}
		#endregion

		#region DrillDown
		/// <summary>
		/// Drills down time interval.
		/// </summary>
		/// <param name="timeinterval">The timeinterval.</param>
		/// <returns></returns>
		public static AxisTimeInterval DrillDownTimeInterval(AxisTimeInterval timeinterval) {
			switch (timeinterval) {
				case AxisTimeInterval.Days:
					return AxisTimeInterval.Hours;

				case AxisTimeInterval.Hours:
					return AxisTimeInterval.Minutes;

				case AxisTimeInterval.Minutes:
					return AxisTimeInterval.Seconds;

				case AxisTimeInterval.Months:
					return AxisTimeInterval.Weeks;

				case AxisTimeInterval.Weeks:
					return AxisTimeInterval.Days;

				case AxisTimeInterval.Quarters:
					return AxisTimeInterval.Months;

				case AxisTimeInterval.Years:
					return AxisTimeInterval.Quarters;

				case AxisTimeInterval.Global:
					return AxisTimeInterval.Years;

				case AxisTimeInterval.None:
				case AxisTimeInterval.Seconds:
				default:
					return timeinterval;
			}
		}


		/// <summary>
		/// Drills up time interval.
		/// </summary>
		/// <param name="timeinterval">The timeinterval.</param>
		/// <returns></returns>
		public static AxisTimeInterval DrillUpTimeInterval(AxisTimeInterval timeinterval) {
			switch (timeinterval) {
				case AxisTimeInterval.Hours:
					return AxisTimeInterval.Days;

				case AxisTimeInterval.Minutes:
					return AxisTimeInterval.Hours;

				case AxisTimeInterval.Seconds:
					return AxisTimeInterval.Minutes;

				case AxisTimeInterval.Weeks:
					return AxisTimeInterval.Months;

				case AxisTimeInterval.Days:
					return AxisTimeInterval.Weeks;

				case AxisTimeInterval.Months:
					return AxisTimeInterval.Quarters;

				case AxisTimeInterval.Years:
					return AxisTimeInterval.Global;

				case AxisTimeInterval.Global:
				case AxisTimeInterval.None:
				default:
					return timeinterval;
			}
		}


		/// <summary>
		/// Drills down localisation.
		/// </summary>
		/// <param name="loc">The loc.</param>
		/// <param name="level">The level.</param>
		/// <returns></returns>
		public static Tuple<ChartLocalisation, int> DrillDownLocalisation(ChartLocalisation loc, int level) {
			switch (loc) {
				case ChartLocalisation.ByFurniture:
					return new Tuple<ChartLocalisation, int>(ChartLocalisation.ByMeter, 0);

				case ChartLocalisation.BySpace:
					return new Tuple<ChartLocalisation, int>(ChartLocalisation.ByFurniture, 0);

				case ChartLocalisation.ByFloor:
					return new Tuple<ChartLocalisation, int>(ChartLocalisation.BySpace, 0);

				case ChartLocalisation.ByBuilding:
					return new Tuple<ChartLocalisation, int>(ChartLocalisation.ByFloor, 0);

				case ChartLocalisation.BySite:
					if (level > 0) {
						level -= 1;
						return new Tuple<ChartLocalisation, int>(ChartLocalisation.BySite, level);
					}
					else
						return new Tuple<ChartLocalisation, int>(ChartLocalisation.ByBuilding, 0);
			}

			return new Tuple<ChartLocalisation, int>(loc, level);
		}

		/// <summary>
		/// Drills up localisation.
		/// </summary>
		/// <param name="loc">The loc.</param>
		/// <param name="level">The level.</param>
		/// <returns></returns>
		public static Tuple<ChartLocalisation, int> DrillUpLocalisation(ChartLocalisation loc, int level) {
			switch (loc) {
				case ChartLocalisation.ByMeter:
					return new Tuple<ChartLocalisation, int>(ChartLocalisation.ByFurniture, 0);
				case ChartLocalisation.ByFurniture:
					return new Tuple<ChartLocalisation, int>(ChartLocalisation.BySpace, 0);
				case ChartLocalisation.BySpace:
					return new Tuple<ChartLocalisation, int>(ChartLocalisation.ByFloor, 0);
				case ChartLocalisation.ByFloor:
					return new Tuple<ChartLocalisation, int>(ChartLocalisation.ByBuilding, 0);
				case ChartLocalisation.ByBuilding:
					return new Tuple<ChartLocalisation, int>(ChartLocalisation.BySite, 0);
				case ChartLocalisation.BySite:
					level += 1;
					return new Tuple<ChartLocalisation, int>(ChartLocalisation.BySite, level);
			}

			return new Tuple<ChartLocalisation, int>(loc, level);
		}
		#endregion

		#region ModernUIStyle
		/// <summary>
		/// Gets the color of the modern UI.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <returns></returns>
		public static string GetModernUIColor(ModernUIColor color) {
			var retVal = "FFFFFF";
			switch (color) {
				case ModernUIColor.Blue:
					retVal = "1BA1E2";
					break;

				case ModernUIColor.Brown:
					retVal = "A05000";
					break;

				case ModernUIColor.Green:
					retVal = "339933";
					break;

				case ModernUIColor.Lime:
					retVal = "8CBF26";
					break;

				case ModernUIColor.Magenta:
					retVal = "FF0097";
					break;

				case ModernUIColor.Orange:
					retVal = "F09609";
					break;

				case ModernUIColor.Pink:
					retVal = "E671B8";
					break;

				case ModernUIColor.Purple:
					retVal = "A200FF";
					break;

				case ModernUIColor.Red:
					retVal = "E51400";
					break;

				case ModernUIColor.Teal:
					retVal = "00ABA9";
					break;

				case ModernUIColor.White:
					retVal = "FFFFFF";
					break;

			}
			return retVal;
		}
		/// <summary>
		/// Applies the modern UI style.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="color">The color.</param>
		public static void ApplyModernUIStyle(Chart chart, ModernUIColor color) {

			chart.DisplayFontFamily = "Segoe UI";

			chart.DisplayFontSize = 16;
			chart.DisplayLegendFontSize = 24;

			chart.LegendPosition = ChartLegendPosition.BottomMiddle;
			chart.DisplayLegendPlain = true;

			chart.DisplayShaddingEffect = ChartShaddingEffect.None;
			chart.DisplayTransparency = 0;


			if (color == ModernUIColor.White)
				chart.DisplayFontColor = "000000";
			else
				chart.DisplayFontColor = "FFFFFF";

			chart.DisplayChartAreaBackgroundStart = null;
			chart.DisplayChartAreaBackgroundEnd = null;

			chart.DisplayBackground = GetModernUIColor(color);

		}

		/// <summary>
		/// Generates the dynamic display modern UI style.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <param name="includeChart">if set to <c>true</c> [include chart].</param>
		/// <returns></returns>
		public static DynamicDisplay GenerateDynamicDisplayModernUIStyle(ModernUIColor color, bool includeChart) {
			var fontColor = "FFFFFF";
			if (color == ModernUIColor.White)
				fontColor = "000000";

			var dd = new DynamicDisplay();
			dd.Type = DynamicDisplayType.Tile;
			dd.Name = color.GetLocalizedText() + " " + (includeChart ? Langue.msg_dynamicdisplay_mainchartvisible : "");
			dd.LocalId = "ModernUI_" + color.GetLocalizedText() + "_" + includeChart;
			dd.MainChartVisible = includeChart;
			dd.MainPictureSize = 15;

			dd.HeaderVisible = true;
			dd.HeaderTextDefaultFontFamily = "Segoe UI";
			dd.HeaderTextDefaultFontSize = "20";
			dd.HeaderTextDefaultFontColor = fontColor;

			dd.MainTextDefaultFontFamily = "Segoe UI";
			dd.MainTextDefaultFontSize = "10";
			dd.MainTextDefaultFontColor = fontColor;

			dd.FooterVisible = false;
			dd.FooterTextDefaultFontFamily = "Segoe UI";
			dd.FooterTextDefaultFontSize = "6";
			dd.FooterTextDefaultFontColor = fontColor;

			dd.ColorTemplate = "GreenYellow";
			dd.IsCustomColorTemplate = true;

			var customColorTemplate = new DynamicDisplayColorTemplate();
			customColorTemplate.Name = color.GetLocalizedText();
			customColorTemplate.LocalId = "ModernUI_" + color.GetLocalizedText();
			var c = ColorHelper.GetColor(GetModernUIColor(color));

			customColorTemplate.StartColorR = customColorTemplate.EndColorR = customColorTemplate.BorderColorR = c.R;
			customColorTemplate.StartColorG = customColorTemplate.EndColorG = customColorTemplate.BorderColorG = c.G;
			customColorTemplate.StartColorB = customColorTemplate.EndColorB = customColorTemplate.BorderColorB = c.B;


			dd.CustomColorTemplate = customColorTemplate;

			return dd;
		}
		#endregion

		#region IsDateTime
		/// <summary>
		/// Determines whether [is date time] [the specified chart].
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns>
		///   <c>true</c> if [is date time] [the specified chart]; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsDateTime(Chart chart) {

			if (chart.Series == null || chart.Series.Count == 0)
				return true;

			var isDateTime = true;
			var series = chart.Series.ToList();
			foreach (var s in series) {
				isDateTime = isDateTime && (s.IsDateTime || s.Points==null || s.Points.Count==0);
			}

			return isDateTime;
		}
		#endregion

	}
}

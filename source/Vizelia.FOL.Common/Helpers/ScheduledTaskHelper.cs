using System;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// A helper class for scheduled tasks.
	/// </summary>
	public static class ScheduledTaskHelper {
		/// <summary>
		/// Runs the action within a task lock.
		/// </summary>
		/// <typeparam name="T">The task type</typeparam>
		/// <param name="taskKey">The task key.</param>
		/// <param name="action">The action.</param>
		public static void RunLocked<T>(string taskKey, Action action) {
			bool lockSuccess;

			Func<object> wrapper = () => {
			            	action();
			            	return null;
			            };

			RunLockedInner<T, object>(taskKey, wrapper, out lockSuccess);
		}

		/// <summary>
		/// Runs the action within a task lock.
		/// </summary>
		/// <typeparam name="T">The task type</typeparam>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <param name="taskKey">The task key.</param>
		/// <param name="action">The action.</param>
		/// <returns>The action result.</returns>
		public static TResult RunLocked<T, TResult>(string taskKey, Func<TResult> action) {
			bool lockSuccess;

			TResult result = RunLockedInner<T, TResult>(taskKey, action, out lockSuccess);

			if(!lockSuccess) {
				throw new VizeliaException(
					"An instance of this task is already running, quitting this task to allow the previous to complete.");
			}

			return result;
		}


		/// <summary>
		/// Runs the action within a task lock.
		/// </summary>
		/// <typeparam name="T">The task type</typeparam>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <param name="taskKey">The task key.</param>
		/// <param name="action">The action.</param>
		/// <param name="lockSuccess">if set to <c>true</c>, locking was successful.</param>
		/// <returns>
		/// The action result.
		/// </returns>
		private static TResult RunLockedInner<T, TResult>(string taskKey, Func<TResult> action, out bool lockSuccess) {
			// The locker must be disposed since it contains a Timer.
			using (var locker = new ScheduledTaskLocker<T>(taskKey)) {
				if (locker.Lock()) {
					var result = action();
					locker.Unlock();

					lockSuccess = true;
					return result;
				}
			}

			lockSuccess = false;
			return default(TResult);
		}
	}
}
﻿using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Color Helper class.
	/// </summary>
	public static class ColorHelper {

		/// <summary>
		/// Convert an html color (ie : #FFAA00) to a Drawing Color.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <returns></returns>
		public static Color GetColor(string color) {
			color = CheckHash(color);
			var retVal = ColorTranslator.FromHtml(color);
			return retVal;
		}

		/// <summary>
		/// Checks the hash.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <returns></returns>
		public static string CheckHash(string color) {
			if (string.IsNullOrEmpty(color) || color.Replace("#", "").Length < 6 || color.Replace("#", "").Length > 8)
				color = "#FFFFFFFF";
			else if (color.StartsWith("#") == false)
				color = "#" + color;
			return color;
		}

		/// <summary>
		/// Gets the color as an html code.
		/// </summary>
		/// <param name="c">The color.</param>
		/// <param name="includeHash">if set to <c>true</c> [include hash].</param>
		/// <returns></returns>
		public static string GetHTMLColor(Color c, bool includeHash = true) {
			var retVal = c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
			if (includeHash)
				retVal = "#" + retVal;
			return retVal;
		}

		/// <summary>
		/// Return HeatMap value.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="min">The min.</param>
		/// <param name="max">The max.</param>
		/// <returns></returns>
		public static Color HeatMap(double value, double min, double max) {
			double val = (value - min) / (max - min);
			return Color.FromArgb(
				255,
				 Convert.ToByte((decimal)(255 * val)),
				 Convert.ToByte((decimal)(255 * (1 - val))),
				0);
		}
	}
}

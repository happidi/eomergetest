﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Vizelia.FOL.BusinessEntities;
using System.Drawing;
using System.Drawing.Imaging;
namespace Vizelia.FOL.Common {
	/// <summary>
	/// A helper class for working with image
	/// </summary>
	public class ImageHelper {

		/// <summary>
		/// Creates the thumbnail image.
		/// </summary>
		/// <param name="inputImage">The input image stream.</param>
		/// <param name="thumbWidth">Width of the thumb.</param>
		/// <param name="thumbHeight">Height of the thumb.</param>
		/// <returns></returns>
		public static StreamResult CreateThumbnailImage(StreamResult inputImage, int thumbWidth, int thumbHeight) {
			MemoryStream ms = CreateThumbnailImage(inputImage.ContentStream, thumbWidth, thumbHeight);
			StreamResult retVal = new StreamResult(ms, inputImage.MimeType);
			return retVal;
		}

		/// <summary>
		/// Creates the thumbnail image.
		/// </summary>
		/// <param name="inputStream">The input image stream.</param>
		/// <param name="thumbWidth">Width of the thumb.</param>
		/// <param name="thumbHeight">Height of the thumb.</param>
		/// <returns></returns>
		public static MemoryStream CreateThumbnailImage(Stream inputStream, int thumbWidth, int thumbHeight) {
			MemoryStream ms = new MemoryStream();
			if (inputStream != null && inputStream.Length > 0) {
				Image img = Image.FromStream(inputStream);
				Image thumbnail = img.GetThumbnailImage(thumbWidth, thumbHeight, null, IntPtr.Zero);
				thumbnail.Save(ms, img.RawFormat);
				ms.Position = 0;
			}
			return ms;
		}

		/// <summary>
		/// Converts array of bytes to Memoey Stream.
		/// </summary>
		/// <param name="imageData">The array of bytes which contains image binary data.</param>
		/// <returns></returns>
		/// <remarks></remarks>
		public static MemoryStream ConvertBytesToMemoryStream(byte[] imageData) {
			try {
				return (imageData == null) ? null : new MemoryStream(imageData, 0, imageData.Length, false, true);
			}
			catch (Exception) {
				return null;
			}
		}

		/// <summary>
		/// Converts the Image File to Memory Stream.
		/// </summary>
		/// <param name="imageFilePath">The image path.</param>
		/// <param name="deleteFile">True to delete the image from disk after converting it to the result memory stream., false otherwise.</param>
		/// <returns></returns>
		/// <remarks></remarks>
		public static MemoryStream ConvertImageFileToMemoryStream(string imageFilePath, bool deleteFile) {
			if (String.IsNullOrEmpty(imageFilePath))
				return null;
			var retVal = ConvertBytesToMemoryStream(ConvertImageFileToBytes(imageFilePath));
			if (deleteFile)
				File.Delete(imageFilePath);
			return retVal;
		}

		/// <summary>
		/// Converts the Image File to array of Bytes
		/// </summary>
		/// <param name="imageFilePath">The path of the image file</param>
		/// <returns></returns>
		/// <remarks></remarks>
		public static byte[] ConvertImageFileToBytes(string imageFilePath) {
			if (string.IsNullOrEmpty(imageFilePath)) {
				throw new ArgumentNullException("imageFilePath");
			}
			FileStream fStream = null;
			try {
				FileInfo fileInfo = new FileInfo(imageFilePath);
				long numBytes = fileInfo.Length;
				fStream = new FileStream(imageFilePath, FileMode.Open, FileAccess.Read);
				BinaryReader binaryReader = new BinaryReader(fStream);
				byte[] tempByte = binaryReader.ReadBytes(Convert.ToInt32(numBytes));

				return tempByte;
			}
			catch (Exception) {
				return null;
			}
			finally {
				if (fStream != null) {
					fStream.Dispose();
				}
				//if (binaryReader != null)
				//	binaryReader.Dispose();
			}
		}

		/// <summary>
		/// Converts array of Bytes to Image File
		/// </summary>
		/// <param name="imageData">The array of bytes which contains image binary data</param>
		/// <param name="filePath">The destination file path.</param>
		/// <returns></returns>
		/// <remarks></remarks>
		public static bool ConvertBytesToImageFile(byte[] imageData, string filePath) {
			if ((imageData == null)) {
				return false;
			}
			FileStream fs = null;
			try {
				fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
				BinaryWriter bw = new BinaryWriter(fs);
				bw.Write(imageData);
				bw.Flush();

				return true;
			}
			catch (Exception) {
				return false;
			}
			finally {
				if (fs != null)
					fs.Dispose();
			}
		}

		/// <summary>
		/// Converts the image to bytes.
		/// </summary>
		/// <param name="image">The image.</param>
		/// <param name="format">The format.</param>
		/// <returns></returns>
		public static byte[] ConvertImageToBytes(System.Drawing.Image image, ImageFormat format) {
			MemoryStream ms = new MemoryStream();
			image.Save(ms, format);
			return ms.ToArray();
		}

		/// <summary>
		/// Converts the image to bytes.
		/// </summary>
		/// <param name="image">The image.</param>
		/// <returns></returns>
		public static byte[] ConvertImageToBytes(System.Drawing.Image image) {
			return ConvertImageToBytes(image, ImageFormat.Png);
		}


		/// <summary>
		/// Resizes an image to best fit a specific rectangle.
		/// </summary>
		/// <param name="imageData">The image data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="factor">The factor.</param>
		/// <returns></returns>
		public static byte[] ResizeBestFit(byte[] imageData, double width, double height, double factor) {
			Image fullSizeImg = Image.FromStream(ConvertBytesToMemoryStream(imageData));
			int size = Convert.ToInt16(Math.Min(width, height) * factor);
			double ratio = Convert.ToDouble(fullSizeImg.Width) / Convert.ToDouble(fullSizeImg.Height);
			var thumbnailImg = fullSizeImg.GetThumbnailImage(size, Convert.ToInt16(size * ratio), null, IntPtr.Zero);
			var retVal = ConvertImageToBytes(thumbnailImg);
			return retVal;
		}

	}
}

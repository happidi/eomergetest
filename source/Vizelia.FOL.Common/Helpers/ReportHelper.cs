﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Encapsulates extension for the Report class.
	/// </summary>
	public class ReportHelper {
		private const string const_item_type_folder = "Folder";
		private const string const_item_type_report = "Report";

		/// <summary>
		/// Determines whether the report is allowed.
		/// This method should be called in a scenario of checking permissions for a single report.
		/// If multiple reports permissions are checked, use the other overload, since this one will query
		/// for the filters on each call.
		/// </summary>
		/// <param name="report">The report.</param>
		/// <returns>
		///   <c>true</c> if the report is allowed; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsAllowed(Report report) {
			List<Filter> securityFilter = FilterHelper.GetFilter<Report>();
			bool isAllowed;

			if (securityFilter.Count == 0) {
				isAllowed = true;
			}
			else {
				isAllowed = IsAllowed(report, securityFilter);
			}

			return isAllowed;

		}

		/// <summary>
		/// Determines whether the report is allowed by examining the given path filters.
		/// </summary>
		/// <param name="report">The report.</param>
		/// <param name="pathFilters">The path filters.</param>
		/// <returns>
		///   <c>true</c> if the report is allowed; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsAllowed(Report report, IEnumerable<Filter> pathFilters) {
			// apply security:
			// the key is the report path (e.g. /Building/WaterReports for folder or /Building/WaterReports/May2011WaterReport for report)
			// the FilterHelper.GetFilter<Report>() will return all the paths the user has access to 
			bool isAllowed = false;

			foreach (Filter filter in pathFilters) {
				string pathFilter = filter.Key;
				if (report.TypeName.Equals(const_item_type_folder)) {
					// This enables giving permission to a subfolder
					int minLength = Math.Min(pathFilter.Length, report.Path.Length);

					if (pathFilter.Substring(0, minLength) == report.Path.Substring(0, minLength)) {
						isAllowed = true;
						break;
					}
				}
				else if (report.TypeName.Equals(const_item_type_report) && report.Path.StartsWith(pathFilter + "/")) {
					isAllowed = true;
					break;
				}
			}

			return isAllowed;
		}

		/// <summary>
		/// Determines whether the specified report is folder.
		/// </summary>
		/// <param name="report">The report.</param>
		/// <returns>
		///   <c>true</c> if the specified report is folder; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsFolder(Report report) {
			bool isFolder = report.TypeName.Equals(const_item_type_folder);

			return isFolder;
		}

		/// <summary>
		/// Determines whether the report is a report file or not.
		/// </summary>
		/// <param name="report">The report.</param>
		/// <returns>
		///   <c>true</c> if the report is a report file; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsReportFile(Report report) {
			bool isReport = report.TypeName.Equals(const_item_type_report);

			return isReport;
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using DDay.iCal;
using DDay.iCal.Serialization.iCalendar;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A Helper class for interfacing DDay.iCal library.
	/// </summary>
	public static class CalendarHelper {

		/// <summary>
		/// private constante to handle the IsAllDay Property
		/// </summary>
		public const string const_isallday_property = "X-VIZ-ISALLDAY";

		/// <summary>
		/// private constante to handle the factor Property
		/// </summary>
		public const string const_factor_property = "X-VIZ-FACTOR";


		/// <summary>
		/// private constante to handle the unit Property
		/// </summary>
		public const string const_unit_property = "X-VIZ-UNIT";

		private static readonly object lockObj = new object();

		/// <summary>
		/// Adds a time zone to an iCalendar object.
		/// </summary>
		/// <param name="iCal">The iCalendar object.</param>
		/// <param name="timeZone">The time zone.</param>
		public static void AddTimeZone(iCalendar iCal, TimeZoneInfo timeZone) {
			iCal.TimeZones.Clear();
			var timezone = iCalTimeZone.FromSystemTimeZone(timeZone, new DateTime(1990, 1, 1), false);
			iCal.AddChild(timezone);
		}

		/// <summary>
		/// Adds a time zone to an iCalendar object.
		/// </summary>
		/// <param name="iCal">The iCalendar object.</param>
		public static void AddTimeZone(iCalendar iCal) {
			string tzid = GetServerTimeZoneId();
			AddTimeZone(tzid, iCal);
		}

		/// <summary>
		/// Adds a time zone to an iCalendar object.
		/// </summary>
		/// <param name="tzid">The id of the timezone.</param>
		/// <param name="iCal">The iCalendar object.</param>
		public static void AddTimeZone(string tzid, iCalendar iCal) {
			var tzinfo = TimeZoneInfo.FindSystemTimeZoneById(tzid);
			var timezone = iCalTimeZone.FromSystemTimeZone(tzinfo);
			if (!iCal.TimeZones.Contains(timezone))
				iCal.AddChild(timezone);
		}

		/// <summary>
		/// Takes an iCalendar datetime and exposes it as a datetime.
		/// </summary>
		/// <param name="datetime">The datetime.</param>
		/// <returns></returns>
		public static DateTime DateTimeFromiCal(iCalDateTime datetime) {
			var utcDate = datetime.UTC;
			return utcDate;
		}


		/// <summary>
		/// Takes an iCalendar datetime and exposes it as a datetime.
		/// </summary>
		/// <param name="datetime">The datetime.</param>
		/// <returns></returns>
		public static DateTime DateTimeFromiCal(IDateTime datetime) {
			return datetime.UTC;
		}


		/// <summary>
		/// Dates the time from iCal event.
		/// </summary>
		/// <param name="occurrence">The occurrence.</param>
		/// <param name="timeZone">The time zone.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		public static void DateTimeFromiCal(Occurrence occurrence, TimeZoneInfo timeZone, out DateTime startDate, out DateTime endDate) {
			Event eEvent = occurrence.Source as Event;
			var calEvent = eEvent.ToBusinessEntity((Period)occurrence.Period);
			calEvent.TimeZoneId = timeZone.Id;
			startDate = calEvent.StartDate.Value;
			endDate = calEvent.EndDate.Value;
		}


		/// <summary>
		/// Creates a exdate exception for the item
		/// </summary>
		/// <param name="iCal">The iCalendar object.</param>
		/// <param name="item">The calendar item.</param>
		public static void MarkOccurrenceAsDeleted(CalendarEvent item, iCalendar iCal) {
			Event ev = (Event)iCal.Events[item.GetInstanceKey()];
			if (ev.ExceptionDates == null) {
				ev.ExceptionDates = (IList<IPeriodList>)new List<PeriodList>();
			}

			if (item.InitialStartDate != null) {
				item.InitialStartDate = GetDateFromClientDate(item, (DateTime)item.InitialStartDate, item.TimeZoneId, item.TimeZoneOffset[0]);
				var exludeDate = item.InitialStartDate.Value;
				PeriodList period = new PeriodList { new iCalDateTime(exludeDate.Year, exludeDate.Month, exludeDate.Day, false) };
				/*
				PeriodList period = new PeriodList { DateTimeToiCal(item.InitialStartDate.Value, iCal) };
				*/
				ev.ExceptionDates.Add(period);
			}
		}

		/// <summary>
		/// Takes a datetime and stores is as a iCalendar datetime.
		/// </summary>
		/// <param name="datetime">The datetime.</param>
		/// <param name="iCal">The iCalendar objetc.</param>
		/// <param name="useUtcTimeZone">if set to <c>true</c> [use UTC time zone].</param>
		/// <returns></returns>
		public static iCalDateTime DateTimeToiCal(DateTime datetime, iCalendar iCal, bool useUtcTimeZone = false) {
			var tzid = useUtcTimeZone ? TimeZoneInfo.Utc.Id : GetServerTimeZoneId();
			return DateTimeToiCal(datetime, tzid, iCal, useUtcTimeZone);
		}

		/// <summary>
		/// Takes a datetime and stores is as a iCalendar datetime.
		/// </summary>
		/// <param name="datetime">The datetime.</param>
		/// <param name="tzid">The id of the timezone.</param>
		/// <param name="iCal">The iCalendar object.</param>
		/// <param name="useUtcTimeZone">if set to <c>true</c> [use UTC time zone].</param>
		/// <returns></returns>
		public static iCalDateTime DateTimeToiCal(DateTime datetime, string tzid, iCalendar iCal, bool useUtcTimeZone = false) {
			if (useUtcTimeZone) {
				return new iCalDateTime(datetime);
			}
			return new iCalDateTime(datetime.Year, datetime.Month, datetime.Day, datetime.Hour, datetime.Minute, datetime.Second, tzid, iCal);
		}

		/// <summary>
		/// Deserializes the specified string into a iCalendar.
		/// </summary>
		/// <param name="iCalData">The string of calendar data.</param>
		/// <returns></returns>
		public static iCalendar Deserialize(string iCalData) {
			if (string.IsNullOrWhiteSpace(iCalData))
				return new iCalendar();
			using (StringReader sr = new StringReader(iCalData)) {
				lock (lockObj) {
					IICalendarCollection col = iCalendar.LoadFromStream(sr);
					return col.Count > 0 ? (iCalendar)col[0] : null;
				}
			}
		}

		/// <summary>
		/// Gets the server TimeZone id.
		/// </summary>
		/// <returns>The id of the local timezone.</returns>
		public static string GetServerTimeZoneId() {
			return TimeZoneInfo.Local.Id;
		}

		/// <summary>
		/// Determines whether a duration spans for one or more days.
		/// </summary>
		/// <param name="duration">The duration.</param>
		/// <returns>
		/// 	<c>true</c> if the duration spans for one or more days, otherwise <c>false</c>.
		/// </returns>
		public static bool IsAllDay(TimeSpan duration) {
			return duration.CompareTo(new TimeSpan(0, 23, 59, 0)) > 0 && (duration.Hours + duration.Minutes + duration.Milliseconds == 0);
		}

		/// <summary>
		/// Serializes the specified iCalendar into a string.
		/// </summary>
		/// <param name="iCal">The iCal.</param>
		/// <returns></returns>
		public static string Serialize(iCalendar iCal) {
			iCalendarSerializer serializer = new iCalendarSerializer(iCal);
			string iCalData = serializer.SerializeToString(iCal);
			return iCalData;
		}

		/// <summary>
		/// Determines whether a duration spans for a full number of days.
		/// </summary>
		/// <param name="duration">The duration.</param>
		/// <returns>
		/// <c>true</c> if the duration is a full number of days, otherwise <c>false</c>.
		/// </returns>
		public static bool SpanFullDays(TimeSpan duration) {
			return duration.TotalDays % 1 == 0;
		}

		/// <summary>
		/// Converts a iCal event to its equivalent business entity.
		/// </summary>
		/// <param name="ev">The event.</param>
		/// <returns></returns>
		public static CalendarEvent ToBusinessEntity(this Event ev) {
			bool isAllDay = IsAllDay(ev.Duration);
			DateTime startDate;
			DateTime endDate;

			var timezoneId = GetEventTimeZone(ev);
			if (ev.Start.TimeZoneObservance != null) {
				startDate = ((iCalDateTime)ev.Start).UTC;
				endDate = ((iCalDateTime)ev.End).UTC;
			}
			else {
				startDate = ConvertClientDateToUTC(ev.Start.Value, timezoneId);
				endDate = ConvertClientDateToUTC(ev.End.Value, timezoneId);
			}

			var retVal = new CalendarEvent {
				Key = ev.UID,
				Location = ev.Location,
				TimeZoneId = timezoneId,
				ColorId = "",
				Category = ev.Categories.Count > 0 ? ev.Categories[0] : null,
				Title = ev.Summary,
				IsAllDay = isAllDay,
				Factor = ev.GetFactor(),
				Unit = ev.GetUnit(),
				RelatedTo = ev.RelatedComponents == null ? null : ev.RelatedComponents[0],
				RecurrencePattern = ev.RecurrenceRules.Count > 0 ? ((RecurrencePattern)ev.RecurrenceRules[0]).ToBusinessEntity() : null,
				StartDate = startDate,
				InitialStartDate = DateTimeFromiCal((iCalDateTime)ev.Start), //ev.Start.UTC, //ev.Start.Value.ToLocalTime(),
				// we don't change the end date here substracting 1 minute because each save will have side effect.
				EndDate = endDate
			};
			return retVal;
		}

		/// <summary>
		/// Shortcut to get the first Item in the Categories collection.
		/// </summary>
		/// <param name="ev">The event.</param>
		/// <returns></returns>
		public static string Category(this Event ev) {
			string retVal = null;
			if (ev.Categories != null && ev.Categories.Count > 0) {
				retVal = ev.Categories[0];
			}
			return retVal;
		}
		/// <summary>
		/// Converts a list of iCal events to its equivalent list of business entities.
		/// </summary>
		/// <param name="events"></param>
		/// <returns></returns>
		public static List<CalendarEvent> ToBusinessEntity(this IList<Event> events) {
			var q = from p in events
					select p.ToBusinessEntity();
			return q.ToList();
		}

		/// <summary>
		/// Converts a list of iCal events to its equivalent list of business entities.
		/// </summary>
		/// <param name="events"></param>
		/// <returns></returns>
		public static List<CalendarEvent> ToBusinessEntity(this IList<IEvent> events) {
			var q = from p in events
					select ((Event)p).ToBusinessEntity();
			return q.ToList();
		}

		/// <summary>
		/// Converts a list of iCal events to its equivalent list of business entities.
		/// </summary>
		/// <param name="events"></param>
		/// <returns></returns>
		public static List<CalendarEvent> ToBusinessEntity(this IUniqueComponentList<Event> events) {
			var q = from p in events
					select p.ToBusinessEntity();
			return q.ToList();
		}

		/// <summary>
		/// Converts a list of iCal events to its equivalent list of business entities.
		/// </summary>
		/// <param name="events"></param>
		/// <returns></returns>
		public static List<CalendarEvent> ToBusinessEntity(this IUniqueComponentList<IEvent> events) {
			var q = from p in events
					select ((Event)p).ToBusinessEntity();
			return q.ToList();
		}

		/// <summary>
		/// Converts a recurrence pattern iCal to its equivalent business entity.
		/// </summary>
		/// <param name="rp">The rp.</param>
		/// <returns></returns>
		public static CalendarRecurrencePattern ToBusinessEntity(this RecurrencePattern rp) {
			var result = new CalendarRecurrencePattern {
				Frequency = (CalendarFrequencyType)Enum.Parse(typeof(CalendarFrequencyType), rp.Frequency.ToString()),
				Count = rp.Count == int.MinValue ? (int?)null : rp.Count,
				Interval = rp.Interval,
				ByHour = rp.ByHour.ToList(),
				ByMinute = rp.ByMinute.ToList(),
				ByMonth = rp.ByMonth.ToList(),
				ByMonthDay = rp.ByMonthDay.ToList(),
				BySecond = rp.BySecond.ToList(),
				BySetPosition = rp.BySetPosition.ToList(),
				ByWeekNo = rp.ByWeekNo.ToList(),
				ByYearDay = rp.ByYearDay.ToList()
			};

			if (rp.ByDay != null && rp.ByDay.Count > 0) {
				result.ByDay = new List<CalendarDaySpecifier>();
				foreach (var day in rp.ByDay) {
					int offset = day.Offset;
					result.ByDay.Add(new CalendarDaySpecifier {
						DayOfWeek = day.DayOfWeek,
						Offset = (CalendarFrequencyOccurrence)offset
					});
				}
			}
			if (rp.Until > DateTime.MinValue && rp.Until < DateTime.MaxValue) {
				result.Until = DateTimeFromiCal(rp.Until);
			}

			return result;
		}

		/// <summary>
		/// Converts a iCal event to its equivalent business entity.
		/// </summary>
		/// <param name="ev">The event.</param>
		/// <param name="KeyLocation">The Key of the location.</param>
		/// <returns></returns>
		public static CalendarEvent ToBusinessEntity(this Event ev, string KeyLocation) {
			CalendarEvent calendarEvent = ev.ToBusinessEntity();
			calendarEvent.KeyLocation = KeyLocation;
			return calendarEvent;
		}

		/// <summary>
		/// Gets the event time zone.
		/// </summary>
		/// <param name="ev">The ev.</param>
		/// <returns></returns>
		public static string GetEventTimeZone(Event ev) {
			var timezoneId = ev.DTStart.TZID;
			timezoneId = !String.IsNullOrWhiteSpace(timezoneId) ? timezoneId : (ev.iCalendar.TimeZones.Count > 0 ? ev.iCalendar.TimeZones[0].TZID : "UTC");
			return timezoneId;
		}

		/// <summary>
		/// Converts a iCal event occurrence to its equivalent business entity.
		/// </summary>
		/// <param name="ev">The event.</param>
		/// <param name="period">The period of the occurrence.</param>
		/// <returns></returns>
		public static CalendarEvent ToBusinessEntity(this Event ev, Period period) {
			DateTime startDate;
			DateTime endDate;
			var tzObs = period.StartTime.TimeZoneObservance;

			CalendarEvent calendarEvent = ev.ToBusinessEntity();
			var timezoneId = GetEventTimeZone(ev);

			calendarEvent.TimeZoneId = timezoneId;

			if (tzObs != null) {
				var offset = tzObs.Value.TimeZoneInfo.OffsetTo;
				var coeff = (offset.Positive ? 1 : -1);
				calendarEvent.TimeZoneOffset[0] = new TimeSpan(coeff * offset.Hours, coeff * offset.Minutes, 0);
				calendarEvent.TimeZoneOffset[1] = calendarEvent.TimeZoneOffset[0];
				startDate = ((iCalDateTime)period.StartTime).UTC;
				endDate = ((iCalDateTime)period.EndTime).UTC;
			}
			else {
				calendarEvent.TimeZoneOffset[0] = calendarEvent.TimeZone.GetUtcOffset(period.StartTime.UTC);
				calendarEvent.TimeZoneOffset[1] = calendarEvent.TimeZone.GetUtcOffset(period.EndTime.UTC);
				startDate = ConvertClientDateToUTC(period.StartTime.Value, timezoneId);
				endDate = ConvertClientDateToUTC(period.EndTime.Value, timezoneId);
			}
			calendarEvent.Key += CalendarEvent.const_occurrence_separator + Guid.NewGuid().ToString();


			if (!ev.IsAllDay) {
				calendarEvent.StartDate = startDate;
				calendarEvent.InitialStartDate = DateTimeFromiCal((iCalDateTime)period.StartTime); // period.StartTime.UTC;
				calendarEvent.EndDate = calendarEvent.IsAllDay && SpanFullDays(ev.Duration) ? endDate.AddSeconds(-1) : endDate; // calendarEvent.IsAllDay ? period.EndTime.UTC.AddMinutes(-1) : period.EndTime.UTC;
				calendarEvent.EndDate = calendarEvent.StartDate > calendarEvent.EndDate ? calendarEvent.StartDate : calendarEvent.EndDate;
			}
			else {
				calendarEvent.StartDate = startDate;
				calendarEvent.InitialStartDate = DateTimeFromiCal((iCalDateTime)period.StartTime); // period.StartTime.UTC;
				calendarEvent.EndDate = endDate.AddSeconds(-1);
			}

			return calendarEvent;
		}

		/// <summary>
		/// Converts a list of iCal events to its equivalent list of business entities.
		/// </summary>
		/// <param name="events"></param>
		/// <param name="KeyLocation">The key of the location.</param>
		/// <returns></returns>
		public static List<CalendarEvent> ToBusinessEntity(this IUniqueComponentList<Event> events, string KeyLocation) {
			var q = from p in events
					select p.ToBusinessEntity(KeyLocation);
			return q.ToList();
		}

		/// <summary>
		/// Converts a list of iCal events to its equivalent list of business entities.
		/// </summary>
		/// <param name="events"></param>
		/// <param name="KeyLocation">The key of the location.</param>
		/// <returns></returns>
		public static List<CalendarEvent> ToBusinessEntity(this IUniqueComponentList<IEvent> events, string KeyLocation) {
			var q = from p in events
					select ((Event)p).ToBusinessEntity(KeyLocation);
			return q.ToList();
		}

		/// <summary>
		/// Checks weither 2 time spans are equivalent.
		/// </summary>
		/// <param name="t1"></param>
		/// <param name="t2"></param>
		/// <returns></returns>
		private static bool TimeSpanAreEquivalent(TimeSpan t1, TimeSpan t2) {
			TimeSpan tMin;
			TimeSpan tMax;
			if (t1 < t2) {
				tMin = t1;
				tMax = t2;
			}
			else {
				tMin = t2;
				tMax = t1;
			}
			return tMax.Subtract(tMin).TotalMinutes < 1;
		}

		/// <summary>
		/// Calculate the next open schedule date.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="minutes"></param>
		/// <param name="calendars"></param>
		/// <returns></returns>
		public static DateTime CalculateScheduleDate(DateTime start, double minutes, iCalendarCollection calendars) {
			TimeSpan timeSpan = TimeSpan.FromMinutes(minutes);
			DateTime end = start.Add(timeSpan);
			DateTime left = start;
			DateTime? right = null;
			int i = 0;
			TimeSpan calculatedTimeSpan = CalculateOpenHours(start, end, calendars);
			while (!TimeSpanAreEquivalent(calculatedTimeSpan, timeSpan)) {
				i++;
				long duration;
				if (calculatedTimeSpan < timeSpan) {
					left = end;
					if (right == null)
						end = end.Add(end.Subtract(start));
					else {
						duration = Convert.ToInt64(right.Value.Subtract(left).Ticks / 2);
						end = left.Add(new TimeSpan(duration));
					}
				}
				else {
					right = end;
					duration = Convert.ToInt64(end.Subtract(left).Ticks / 2);
					end = left.Add(new TimeSpan(duration));
				}
				calculatedTimeSpan = CalculateOpenHours(start, end, calendars);
			}
			return end.AddSeconds(-end.Second).AddMinutes(1);
		}



		/// <summary>
		/// Calculates the open duration between two dates
		/// </summary>
		/// <param name="start">The start date.</param>
		/// <param name="end">The end date.</param>
		/// <param name="calendars">A collection of calendar.</param>
		/// <returns></returns>
		public static TimeSpan CalculateOpenHours(DateTime start, DateTime end, iCalendarCollection calendars) {

			// we compulse beginning of day for start and end of day for end.
			DateTime startDate = start.Date.AddDays(-1);
			DateTime endDate = end.Date.AddDays(1).AddSeconds(-1);

			IList<Occurrence> occurrences = calendars.GetOccurrences<Event>(startDate, endDate);

			var queryHolidays = from p in occurrences
								where (((Event)(p.Source)).Categories != null && ((Event)(p.Source)).Categories.Count > 0 && ((Event)(p.Source)).Categories[0] == "Holiday")
								select p;


			List<Period> trimmedPeriods = new List<Period> { new Period(new iCalDateTime(start), new iCalDateTime(end)) };
			foreach (Occurrence occurrence in queryHolidays) {

				List<Period> tmpPeriod = new List<Period>();
				foreach (Period period in trimmedPeriods) {
					tmpPeriod.AddRange(PeriodBooleanRemove(period, (Period)occurrence.Period));
				}
				trimmedPeriods.Clear();
				trimmedPeriods.AddRange(tmpPeriod);

			}
			TimeSpan result = new TimeSpan();
			return trimmedPeriods.Aggregate(result, (current, period) => current + (period.EndTime.Value - period.StartTime.Value));
		}

		/// <summary>
		/// Calculate the "Boolean" remove operation between 2 periods.
		/// </summary>
		/// <param name="period">The reference period</param>
		/// <param name="periodToRemove">The period to remove</param>
		/// <returns></returns>
		public static List<Period> PeriodBooleanRemove(Period period, Period periodToRemove) {
			List<Period> result = new List<Period>();
			if (periodToRemove.EndTime.Value <= period.StartTime.Value || periodToRemove.StartTime.Value > period.EndTime.Value) {
				result.Add(period);
			}
			else if (PeriodContainsDate(period, periodToRemove.StartTime) && PeriodContainsDate(period, periodToRemove.EndTime)) {
				result.Add(new Period(period.StartTime, periodToRemove.StartTime));
				result.Add(new Period(periodToRemove.EndTime, period.EndTime));
			}
			else if (PeriodContainsDate(period, periodToRemove.StartTime) && !PeriodContainsDate(period, periodToRemove.EndTime)) {
				result.Add(new Period(period.StartTime, periodToRemove.StartTime));
			}
			else if (!PeriodContainsDate(period, periodToRemove.StartTime) && PeriodContainsDate(period, periodToRemove.EndTime)) {
				result.Add(new Period(periodToRemove.EndTime, period.EndTime));
			}
			else if (periodToRemove.StartTime.Value <= period.StartTime.Value && periodToRemove.EndTime.Value > period.EndTime.Value) {

			}
			return result;

		}

		/// <summary>
		/// Check if a period contains a specific date.
		/// </summary>
		/// <param name="period"></param>
		/// <param name="date"></param>
		/// <returns></returns>
		public static bool PeriodContainsDate(Period period, iCalDateTime date) {
			if (period.StartTime.Value <= date && period.EndTime.Value >= date) {
				return true;
			}
			return false;
		}

		/// <summary>
		/// Check if a period contains a specific date.
		/// </summary>
		/// <param name="period"></param>
		/// <param name="date"></param>
		/// <returns></returns>
		public static bool PeriodContainsDate(Period period, IDateTime date) {
			if (period.StartTime.Value <= date.Value && period.EndTime.Value >= date.Value) {
				return true;
			}
			return false;
		}


		/// <summary>
		/// Converts the date by daylight.
		/// </summary>
		/// <param name="dt">The input date.</param>
		/// <param name="timeZoneId">The time zone id.</param>
		/// <param name="timezoneLocalOffset">The timezone local offset.</param>
		/// <param name="forward">if set to <c>true</c> [forward].</param>
		/// <returns></returns>
		public static DateTime ConvertDateByDaylight(DateTime dt, string timeZoneId, TimeSpan timezoneLocalOffset, Boolean forward) {
			if (timeZoneId != null) {
				var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
				if (forward && TimeZoneInfo.Local.BaseUtcOffset != new TimeSpan(0, 0, 0)) dt = dt.Add(timezoneLocalOffset);
				if (timeZoneInfo.IsDaylightSavingTime(dt)) {
					var rules = timeZoneInfo.GetAdjustmentRules();
					dt = forward ? dt.Add(-rules[0].DaylightDelta) : dt.Add(rules[0].DaylightDelta);
				}
			}
			return dt;
		}


		/// <summary>
		/// Converts the client date to UTC.
		/// </summary>
		/// <param name="dt">The dt.</param>
		/// <param name="fromTimeZoneId">From time zone id.</param>
		/// <returns></returns>
		public static DateTime ConvertClientDateToUTC(DateTime dt, string fromTimeZoneId) {
			if (fromTimeZoneId != null) {
				DateTime retDate;
				dt = DateTime.SpecifyKind(dt, DateTimeKind.Unspecified);
				try {
					retDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dt, fromTimeZoneId, "UTC");
				}
				catch (Exception) {

					retDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dt.AddHours(1), fromTimeZoneId, "UTC");
				}

				return retDate;
			}
			return dt;
		}

		/// <summary>
		/// Gets the UTC offset.
		/// </summary>
		/// <param name="dt">The date.</param>
		/// <param name="timeZoneId">The time zone id.</param>
		/// <returns></returns>
		public static TimeSpan GetUtcOffset(DateTime dt, string timeZoneId) {
			if (timeZoneId != null) {
				var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
				return timeZoneInfo.GetUtcOffset(dt);
			}
			return new TimeSpan(0, 0, 0);
		}

		/// <summary>
		/// Update dates from client dates.
		/// </summary>
		/// <param name="ev">The ev.</param>
		/// <returns></returns>
		public static void UpdateEventClientDates<TCalendarEvent>(TCalendarEvent ev) where TCalendarEvent : CalendarEvent {

			if (ev.TimeZoneOffset == null)
				ev.TimeZoneOffset = new List<TimeSpan> { new TimeSpan(0, 0, 0), new TimeSpan(0, 0, 0), new TimeSpan(0, 0, 0) };

			ev.StartDate = GetDateFromClientDate(ev, (DateTime)ev.StartDate, ev.TimeZoneId, ev.TimeZoneOffset[0]);
			ev.EndDate = GetDateFromClientDate(ev, (DateTime)ev.EndDate, ev.TimeZoneId, ev.TimeZoneOffset[1]);
			if (ev.RecurrencePattern != null && ev.RecurrencePattern.Until != null)
				ev.RecurrencePattern.Until = GetDateFromClientDate(ev, (DateTime)ev.RecurrencePattern.Until, ev.TimeZoneId, ev.TimeZoneOffset[2]);
		}

		/// <summary>
		/// Update dates to client dates.
		/// </summary>
		/// <param name="ev">The ev.</param>
		/// <param name="flgOffset">if set to <c>true</c> [FLG offset].</param>
		/// <returns></returns>
		public static TCalendarEvent UpdateEventDates<TCalendarEvent>(TCalendarEvent ev, bool flgOffset) where TCalendarEvent : CalendarEvent {
			ev.TimeZoneOffset[0] = ev.TimeZone.GetUtcOffset(ev.StartDate.Value);
			var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(ev.TimeZoneId);


			if (ev.IsAllDay && (TimeZoneInfo.Local.BaseUtcOffset != new TimeSpan(0, 0, 0) || ev.StartDate.Value.TimeOfDay == new TimeSpan(0, 0, 0))) {
				ev.StartDate = ev.StartDate.Value.ToTimeZone(timeZoneInfo);
				ev.EndDate = ev.EndDate.Value.ToTimeZone(timeZoneInfo);
				return ev;
			}

			if (flgOffset) {
				var duration = ((DateTime)ev.EndDate).Subtract((DateTime)ev.StartDate);
				var startDate = UpdateDateByOffset((DateTime)ev.StartDate, ev.TimeZoneId);
				ev.StartDate = startDate;

				if (ev.IsAllDay) {
					var tempDate = TimeZoneInfo.ConvertTimeFromUtc(startDate, timeZoneInfo);
					startDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 0, 0, 0, DateTimeKind.Unspecified);
					ev.StartDate = TimeZoneInfo.ConvertTimeToUtc(startDate, timeZoneInfo);
					var endDate = startDate.Add(duration);
					ev.EndDate = TimeZoneInfo.ConvertTimeToUtc(endDate, timeZoneInfo);
					return ev;
				}
				else
					ev.EndDate = ev.EndDate;
			}


			ev.StartDate = ConvertDateByDaylight((DateTime)ev.StartDate, ev.TimeZoneId, TimeZoneInfo.Local.GetUtcOffset((DateTime)ev.StartDate), true);
			ev.EndDate = ConvertDateByDaylight((DateTime)ev.EndDate, ev.TimeZoneId, TimeZoneInfo.Local.GetUtcOffset((DateTime)ev.EndDate), true);
			if (flgOffset) {
				var delta = GetDeltaDateByOffset((DateTime)ev.StartDate, timeZoneInfo);
				ev.StartDate = ((DateTime)ev.StartDate).AddDays(delta);
				ev.EndDate = ((DateTime)ev.EndDate).AddDays(delta);
			}
			return ev;
		}

		/// <summary>
		/// Get the date offset.
		/// </summary>
		/// <param name="dt">The input date.</param>
		/// <param name="timeZoneInfo">The time zone info.</param>
		/// <returns></returns>
		public static int GetDeltaDateByOffset(DateTime dt, TimeZoneInfo timeZoneInfo) {
			var tempDate = TimeZoneInfo.ConvertTimeFromUtc(dt, timeZoneInfo);
			if (tempDate.Hour - timeZoneInfo.GetUtcOffset(dt).Hours >= 24)
				return 1;
			else if (tempDate.Hour <= timeZoneInfo.GetUtcOffset(dt).Hours)
				return -1;
			return 0;
		}

		/// <summary>
		/// Updates the date by offset.
		/// </summary>
		/// <param name="dt">The dt.</param>
		/// <param name="timeZoneId">The time zone id.</param>
		/// <returns></returns>
		public static DateTime UpdateDateByOffset(DateTime dt, string timeZoneId) {
			dt = ConvertDateByDaylight(dt, timeZoneId, new TimeSpan(0, 0, 0), true);
			var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
			var tempDate = TimeZoneInfo.ConvertTimeFromUtc(dt, timeZoneInfo);
			var timeSpan = new TimeSpan(tempDate.Hour, tempDate.Minute, tempDate.Second);

			tempDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, 0, 0, 0, DateTimeKind.Unspecified);

			tempDate = tempDate.Add(timeSpan);
			tempDate = TimeZoneInfo.ConvertTimeToUtc(tempDate, timeZoneInfo);
			if (timeZoneInfo.IsDaylightSavingTime(tempDate))
				tempDate = ConvertDateByDaylight(tempDate, timeZoneId, new TimeSpan(0, 0, 0), false);
			return tempDate;
		}



		/// <summary>
		/// Converts a client date to a local date.
		/// </summary>
		/// <param name="ev">The ev.</param>
		/// <param name="dt">The date.</param>
		/// <param name="timeZoneId">The time zone id.</param>
		/// <param name="timezoneOffset">The timezone offset.</param>
		/// <returns></returns>
		public static DateTime GetDateFromClientDate(CalendarEvent ev, DateTime dt, string timeZoneId, TimeSpan timezoneOffset) {
			timeZoneId = timeZoneId ?? "UTC";
			var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
			var utcDate = dt.Add(timezoneOffset);
			utcDate = utcDate.Add(-TimeZoneInfo.Local.GetUtcOffset(dt));
			if (ev.IsAllDay) {
				var tempDate = new DateTime(utcDate.Year, utcDate.Month, utcDate.Day, 0, 0, 0, DateTimeKind.Utc);
				return tempDate;
			}

			if (ev as Schedule == null && ev as TaskScheduler == null) {
				return DateTime.SpecifyKind(utcDate, DateTimeKind.Utc);
			}
			else {
				if (TimeSpan.Compare(timezoneOffset, new TimeSpan(0, 0, 0)) != 0) {
					return DateTime.SpecifyKind(utcDate.Add(-timeZoneInfo.GetUtcOffset(utcDate)), DateTimeKind.Utc);
				}
				else {
					return DateTime.SpecifyKind(dt.Add(-timeZoneInfo.GetUtcOffset(dt)), DateTimeKind.Utc);
				}
			}
		}

		/// <summary>
		/// Converts the business entity representing a event to its  iCal equivalent.
		/// </summary>
		/// <param name="ev">The business entity representing an event.</param>
		/// <param name="iCal">The iCal calendar to which the event should be added.</param>
		/// <param name="useUtcTimeZone">if set to <c>true</c> [use UTC time zone].</param>
		/// <returns></returns>
		public static Event ToiCal(this CalendarEvent ev, iCalendar iCal, bool useUtcTimeZone = false) {
			Event result = iCal.Create<Event>();

			if (String.IsNullOrEmpty(ev.Key)) {
				ev.Key = Guid.NewGuid().ToString();
			}
			result.UID = ev.Key;
			result.Categories = new List<string>();
			if (!string.IsNullOrEmpty(ev.Category))
				result.Categories.Add(ev.Category);
			result.Location = ev.Location;
			result.Summary = ev.Title;

			if (ev.IsAllDay) {
				if (ev.StartDate != null) {
					ev.StartDate = ev.StartDate.Value.Date;
				}
				if (ev.EndDate != null) {
					ev.EndDate = ev.EndDate.Value.Date.AddDays(1);
				}
			}

			if (ev.StartDate != null)
				result.Start = DateTimeToiCal(ev.StartDate.Value, iCal, useUtcTimeZone); // new iCalDateTime(ev.StartDate.Value, "Romance Standard Time", iCal);
			if (ev.EndDate != null)
				result.End = DateTimeToiCal(ev.EndDate.Value, iCal, useUtcTimeZone); // new iCalDateTime(ev.EndDate.Value, "Romance Standard Time", iCal);

			result.Start.TZID = useUtcTimeZone ? TimeZoneInfo.Utc.Id : ev.TimeZoneId;
			result.End.TZID = useUtcTimeZone ? TimeZoneInfo.Utc.Id : ev.TimeZoneId;
			result.IsAllDay = ev.IsAllDay;

			result.SetFactor(ev.Factor);
			result.SetUnit(ev.Unit);
			if (ev.RelatedTo != null) {
				result.RelatedComponents.Add(ev.RelatedTo);
			}

			if (ev.RecurrencePattern != null) {
				if (ev.RecurrencePattern.Until != null) {
					ev.RecurrencePattern.Until = DateTimeToiCal(ev.RecurrencePattern.Until.Value, iCal).Value;
				}
				result.RecurrenceRules.Add(ev.RecurrencePattern.ToiCal(iCal));
			}
			return result;
		}

		/// <summary>
		/// Serialise a CalendarEvent to it's own iCalData equivalent.
		/// </summary>
		/// <param name="ev">The business entity representing an event.</param>
		/// <returns></returns>
		public static string ToiCalData(this CalendarEvent ev) {
			using (iCalendar iCal = new iCalendar()) {
				AddTimeZone(iCal, ev.TimeZone);
				ev.ToiCal(iCal);
				string iCalData = Serialize(iCal);
				return iCalData;
			}
		}

		/// <summary>
		/// Gets the next occurences for a specific CalendarEvent.
		/// </summary>
		/// <param name="ev">The ev.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="useUtcTimeZone">if set to <c>true</c> [use UTC time zone].</param>
		/// <returns></returns>
		public static IList<Occurrence> GetNextOccurences(this CalendarEvent ev, DateTime startDate, DateTime endDate, bool useUtcTimeZone = false) {
			using (iCalendar iCal = new iCalendar()) {
				AddTimeZone(iCal, ev.TimeZone);
				ev.ToiCal(iCal, useUtcTimeZone);
				IList<Occurrence> occurrences = iCal.GetOccurrences<Event>(startDate, endDate);
				return occurrences;
			}
		}

		/// <summary>
		/// Deserialize an iCalData and return the first event as a CalendarEvent business entity.
		/// </summary>
		/// <param name="iCalData">the iCalData.</param>
		/// <param name="timeZoneId"> </param>
		/// <param name="nextOccurrenceDate">The next occurrence date.</param>
		/// <returns></returns>
		public static CalendarEvent ToCalendarEvent(this string iCalData, string timeZoneId, out DateTime? nextOccurrenceDate) {
			iCalendar iCal = Deserialize(iCalData);
			nextOccurrenceDate = null;
			if (iCal.Events.Count > 0) {
				Event ev = (Event)iCal.Events[0];

				CalendarEvent item = ev.ToBusinessEntity();

				if (ev.RecurrenceRules != null && ev.RecurrenceRules.Count > 0) {
					CalendarRecurrencePattern rp = ((RecurrencePattern)ev.RecurrenceRules[0]).ToBusinessEntity();
					item.RecurrencePattern = rp;

					List<Occurrence> occ = null;
					switch (ev.RecurrenceRules[0].Frequency) {
						case FrequencyType.Daily:
							occ = (List<Occurrence>)ev.GetOccurrences(DateTimeToiCal(DateTime.Now, iCal), DateTimeToiCal(DateTime.Now.AddDays(10), iCal));
							break;
						case FrequencyType.Hourly:
							occ = (List<Occurrence>)ev.GetOccurrences(DateTime.Now, DateTime.Now.AddHours(10));
							break;
						case FrequencyType.Minutely:
							occ = (List<Occurrence>)ev.GetOccurrences(DateTime.Now, DateTime.Now.AddMinutes(10));
							break;
						case FrequencyType.Monthly:
							occ = (List<Occurrence>)ev.GetOccurrences(DateTime.Now, DateTime.Now.AddMonths(10));
							break;
						case FrequencyType.Secondly:
							occ = (List<Occurrence>)ev.GetOccurrences(DateTime.Now, DateTime.Now.AddSeconds(10));
							break;
						case FrequencyType.Weekly:
							occ = (List<Occurrence>)ev.GetOccurrences(DateTime.Now, DateTime.Now.AddDays(7 * 10));
							break;
						case FrequencyType.Yearly:
							occ = (List<Occurrence>)ev.GetOccurrences(DateTime.Now, DateTime.Now.AddYears(10));
							break;
					}
					if (occ != null && occ.Count > 0) {
						var pe = occ[0].Period;
						nextOccurrenceDate = pe.StartTime.TimeZoneObservance != null ? ((iCalDateTime)pe.StartTime).UTC : ConvertClientDateToUTC(pe.StartTime.Value, ev.Start.TZID);
					}
				}
				else {
					nextOccurrenceDate = item.StartDate;
				}

				return item;
			}

			return null;
		}

		/// <summary>
		/// Converts the business entity representing a recurrence pattern to its  iCal equivalent.
		/// </summary>
		/// <param name="rp">The rp.</param>
		/// <param name="iCal">The iCal.</param>
		/// <returns></returns>
		public static RecurrencePattern ToiCal(this CalendarRecurrencePattern rp, iCalendar iCal) {
			RecurrencePattern result = new RecurrencePattern {
				Frequency = (FrequencyType)Enum.Parse(typeof(FrequencyType),
				rp.Frequency.ToString())
			};
			if (rp.Count.HasValue && rp.Count != 0)
				result.Count = rp.Count.Value;
			if (rp.Interval != 0)
				result.Interval = rp.Interval;
			if (rp.ByHour != null)
				result.ByHour = rp.ByHour.ToList();

			if (rp.ByMinute != null)
				result.ByMinute = rp.ByMinute.ToList();

			if (rp.ByMonth != null)
				result.ByMonth = rp.ByMonth.ToList();

			if (rp.ByMonthDay != null)
				result.ByMonthDay = rp.ByMonthDay.ToList();

			if (rp.BySecond != null)
				result.BySecond = rp.BySecond.ToList();

			if (rp.BySetPosition != null)
				result.BySetPosition = rp.BySetPosition.ToList();

			if (rp.ByWeekNo != null)
				result.ByWeekNo = rp.ByWeekNo.ToList();

			if (rp.ByYearDay != null)
				result.ByYearDay = rp.ByYearDay.ToList();
			if (rp.ByDay != null) {
				foreach (var day in rp.ByDay) {
					int offset = Convert.ToInt32(day.Offset);
					result.ByDay.Add(new WeekDay(day.DayOfWeek, offset));
				}
			}
			if (rp.Until != null) {
				// TODO : Bug when saving Until removes a day each time. 
				//result.Until = new iCalDateTime(rp.Until.Value.Year, rp.Until.Value.Month, rp.Until.Value.Day, rp.Until.Value.Hour, rp.Until.Value.Minute, 0, "(GMT+0100) FLE%2C normaltid", iCal);
				result.Until = rp.Until.Value;
				//result.Until = CalendarHelper.DateTimeToiCal(rp.Until.Value, iCal).Value; // new iCalDateTime(rp.Until.Value, "Romance Standard Time", iCal);
				//result.Until = new iCalDateTime(rp.Until.Value.ToUniversalTime());
			}

			return result;
		}



		/// <summary>
		/// Determines if the event has a recurrence pattern.
		/// </summary>
		/// <param name="uid">The id of the event.</param>
		/// <param name="iCal">The iCalendar object.</param>
		/// <returns>
		/// <c>true</c> if the event as a recurrence pattern, otherwise <c>false</c>.
		/// </returns>
		public static bool EventHasRecurrence(string uid, iCalendar iCal) {
			//if (iCal.Events.ContainsKey(uid)) => the method ContainsKey has not been implemented in the iCal specific collections.
			Event ev = (Event)iCal.Events[uid];
			if (ev != null) {
				return (ev.RecurrenceRules != null && ev.RecurrenceRules.Count > 0);
			}
			return false;
		}


		/// <summary>
		/// Gets the time zone from id.
		/// </summary>
		/// <param name="timeZoneId">The time zone id.</param>
		/// <returns></returns>
		public static TimeZoneInfo GetTimeZoneFromId(string timeZoneId) {
			TimeZoneInfo retVal = TimeZoneInfo.Utc;
			if (!string.IsNullOrEmpty(timeZoneId)) {
				retVal = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
			}
			return retVal;
		}

		/// <summary>
		/// Gets the factor.
		/// </summary>
		/// <param name="ev">The ev.</param>
		/// <returns></returns>
		public static double? GetFactor(this Event ev) {
			var retVal = ev.Properties.ContainsKey(const_factor_property) ? double.Parse(ev.Properties[const_factor_property].Value.ToString(), CultureInfo.InvariantCulture) : new double?();
			return retVal;
		}


		/// <summary>
		/// Sets the factor.
		/// </summary>
		/// <param name="ev">The ev.</param>
		/// <param name="factor">The factor.</param>
		public static void SetFactor(this Event ev, double? factor) {
			if (factor.HasValue)
				ev.Properties.Add(new CalendarProperty(const_factor_property, factor.Value.ToString(CultureInfo.InvariantCulture)));
		}

		/// <summary>
		/// Gets the unit.
		/// </summary>
		/// <param name="ev">The ev.</param>
		/// <returns></returns>
		public static string GetUnit(this Event ev) {
			string retVal = ev.Properties.ContainsKey(const_unit_property) ? ev.Properties[const_unit_property].Value.ToString() : null;
			return retVal;
		}

		/// <summary>
		/// Sets the unit.
		/// </summary>
		/// <param name="ev">The ev.</param>
		/// <param name="unit">The unit.</param>
		public static void SetUnit(this Event ev, string unit) {
			if (!String.IsNullOrEmpty(unit))
				ev.Properties.Add(new CalendarProperty(const_unit_property, unit));
		}

		/// <summary>
		/// Gets the next run time for job grid.
		/// </summary>
		/// <param name="schedules">The schedules.</param>
		/// <returns></returns>
		public static DateTime? GetNextRunTimeForJob(List<Schedule> schedules) {
			DateTime? nextOccurrence = DateTime.MaxValue;
			foreach (var sched in schedules) {
				if (sched.StartDate > DateTime.UtcNow) {
					nextOccurrence = sched.StartDate.Value;
				}

				if (sched.RecurrencePattern == null) continue;

				IList<Occurrence> occ = new List<Occurrence>();
				switch (sched.RecurrencePattern.Frequency) {
					case CalendarFrequencyType.Minutely:
						occ = sched.GetNextOccurences(DateTime.Now, DateTime.Now.AddMinutes(sched.RecurrencePattern.Interval + 1), true);
						break;
					case CalendarFrequencyType.Hourly:
						occ = sched.GetNextOccurences(DateTime.Now, DateTime.Now.AddHours(sched.RecurrencePattern.Interval + 1), true);
						break;
					case CalendarFrequencyType.Weekly:
						occ = sched.GetNextOccurences(DateTime.Now, DateTime.Now.AddDays(7), true);
						break;
					case CalendarFrequencyType.Monthly:
						occ = sched.GetNextOccurences(DateTime.Now, DateTime.Now.AddYears(1), true);
						break;
					case CalendarFrequencyType.Yearly:
						occ = sched.GetNextOccurences(DateTime.Now, DateTime.Now.AddYears(sched.RecurrencePattern.Interval + 1), true);
						break;
				}

				if (!occ.Any()) continue;

				foreach (var occurrence in occ) {
					if (occurrence.Period.StartTime.Value < nextOccurrence && occurrence.Period.StartTime.UTC > DateTime.UtcNow) {
						nextOccurrence = occurrence.Period.StartTime.UTC;
					}
				}
			}

			if (nextOccurrence == DateTime.MaxValue) {
				nextOccurrence = null;
			}

			if (nextOccurrence.HasValue) {
				return nextOccurrence.Value.ToUniversalTime();
			}

			return null;
		}

		/// <summary>
		/// Gets the triggers string for job.
		/// </summary>
		/// <param name="schedules">The schedules.</param>
		/// <returns></returns>
		public static string GetTriggersStringForJob(List<Schedule> schedules) {
			var sb = new StringBuilder();

			try {
				foreach (var schedule in schedules) {
					// Handle reccurence pattern
					if (schedule.RecurrencePattern != null && schedule.RecurrencePattern.Frequency != CalendarFrequencyType.None) {

						Func<int, string> ConvertToMonth = i => {
							var result = CultureInfo.InvariantCulture.DateTimeFormat.MonthNames[i];
							return result;
						};

						Func<int, string> ConvertToFrequencyOccurrence = i => {
							var enumValue = (CalendarFrequencyOccurrence)i;
							var result = Langue.ResourceManager.GetString("msg_calendar_frequencyoccurrence_" + enumValue.ToString().ToLower()).ToLower();
							return result;
						};

						switch (schedule.RecurrencePattern.Frequency) {
							case CalendarFrequencyType.Yearly:
								sb.Append("Every ");
								sb.Append(schedule.RecurrencePattern.Interval);
								sb.Append(" year" + ((schedule.RecurrencePattern.Interval > 1) ? "s" : "") + ". ");
								if (schedule.RecurrencePattern.ByMonthDay != null && schedule.RecurrencePattern.ByMonthDay.Any()) {
									sb.Append("The ");
									sb.Append(schedule.RecurrencePattern.ByMonthDay[0]);
									sb.Append(" ");
									sb.Append(ConvertToMonth(schedule.RecurrencePattern.ByMonth[0]));
								}
								else {
									sb.Append("The ");
									// convert number to name
									sb.Append(ConvertToFrequencyOccurrence(schedule.RecurrencePattern.BySetPosition[0]));
									sb.Append(" ");
									sb.Append(schedule.RecurrencePattern.ByDay[0]);
									sb.Append(" of ");
									sb.Append(ConvertToMonth(schedule.RecurrencePattern.ByMonth[0]));
								}
								sb.Append(". ");
								break;
							case CalendarFrequencyType.Monthly:
								if (schedule.RecurrencePattern.ByMonthDay != null && schedule.RecurrencePattern.ByMonthDay.Any()) {
									sb.Append("The ");
									sb.Append(schedule.RecurrencePattern.ByMonthDay[0]);
									sb.Append(" of every ");
									var clonedByMonth = schedule.RecurrencePattern.ByMonth.Clone();
									clonedByMonth.Sort();
									foreach (var month in clonedByMonth) {
										sb.Append(ConvertToMonth(month));
										sb.Append(", ");
									}
									sb.Remove(sb.Length - 2, 2);
									sb.Append(". ");
								}
								else {
									sb.Append("The ");
									sb.Append(ConvertToFrequencyOccurrence(schedule.RecurrencePattern.BySetPosition[0]));
									sb.Append(" ");
									sb.Append(schedule.RecurrencePattern.ByDay[0]);
									sb.Append(" of every ");

									var clonedByMonth = schedule.RecurrencePattern.ByMonth.Clone();
									clonedByMonth.Sort();
									foreach (var month in clonedByMonth) {
										sb.Append(ConvertToMonth(month));
										sb.Append(", ");
									}
									sb.Remove(sb.Length - 2, 2);
									sb.Append(". ");
								}
								break;
							case CalendarFrequencyType.Weekly:
								sb.Append("Every ");

								var clonedByDay = schedule.RecurrencePattern.ByDay.Clone();
								clonedByDay.Sort((specifier, daySpecifier) => specifier.DayOfWeek - daySpecifier.DayOfWeek);
								foreach (var day in clonedByDay) {
									sb.Append(day);
									sb.Append(", ");
								}
								sb.Remove(sb.Length - 2, 2);
								sb.Append(". ");
								break;
							case CalendarFrequencyType.Hourly:
								sb.Append("Every ");
								if (schedule.RecurrencePattern.Interval > 1) {
									sb.Append(schedule.RecurrencePattern.Interval);
									sb.Append(" hours. ");
								}
								else {
									sb.Append("hour. ");
								}
								break;
							case CalendarFrequencyType.Minutely:
								sb.Append("Every ");
								if (schedule.RecurrencePattern.Interval > 1) {
									sb.Append(schedule.RecurrencePattern.Interval);
									sb.Append(" minutes. ");
								}
								else {
									sb.Append("minute. ");
								}
								break;
						}
					}

					// Handle start time
					sb.Append("Starting ");
					sb.Append(TimeZoneInfo.ConvertTime(schedule.StartDate.Value, TimeZoneInfo.Utc, schedule.TimeZone));
					sb.AppendLine(" " + schedule.TimeZone + ". ");
				}
			}
			catch (Exception) {

			}

			return sb.ToString();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// Html Sanitizer Hepler.
	/// </summary>
	public class HtmlSanitizerHelper {
		/// <summary>
		/// forbidden tags
		/// </summary>
		public static Regex ForbiddenTags = new Regex("^(script|object|embed|style|link|form|input)$");

		/// <summary>
		/// a string with the aloowed tags
		/// </summary>
		public static string AllowedTagsString =
			"b|p|i|s|a|img|table|thead|tbody|tfoot|tr|th|td|dd|dl|dt|em|h1|h2|h3|h4|h5|h6|li|ul|ol|span|div|strike|strong|" +
			"sub|sup|pre|del|code|blockquote|strike|kbd|br|hr|area|map|object|embed|param|link|form|small|big|u|html|head|body";

		/// <summary>
		/// allowed tags
		/// </summary>
		public static Regex AllowedTags = new Regex("^(" + AllowedTagsString + ")$");
		/// <summary>
		/// comment pattern
		/// </summary>
		private static readonly Regex CommentPattern = new Regex("<!--.*");  // <!--.........> 

		/// <summary>
		/// tag start pattern
		/// </summary>
		private static readonly Regex tagStartPattern = new Regex("<(?i)(\\w+\\b)\\s*(.*)/?>$");  // <tag ....props.....> 

		/// <summary>
		/// tag close pattern
		/// </summary>
		private static readonly Regex tagClosePattern = new Regex("</(?i)(\\w+\\b)\\s*>$");  // </tag .........> 

		/// <summary>
		/// stand alone tags
		/// </summary>
		private static readonly Regex standAloneTags = new Regex("^(img|br|hr)$");

		/// <summary>
		/// self closed
		/// </summary>
		private static readonly Regex selfClosed = new Regex("<.+/>");

		/// <summary>
		/// attributes pattern
		/// </summary>
		private static readonly Regex attributesPattern = new Regex("(\\w*)\\s*=\\s*\"([^\"]*)\"");  // prop="...." 

		/// <summary>
		/// style pattern
		/// </summary>
		private static readonly Regex stylePattern = new Regex("([^\\s^:]+)\\s*:\\s*([^;]+);?");  // color:red; 

		/// <summary>
		/// url style pattern
		/// </summary>
		private static readonly Regex urlStylePattern = new Regex("(?i).*\\b\\s*url\\s*\\(['\"]([^)]*)['\"]\\)");  // url('....')" 

		/// <summary>
		/// forbbiden style pattern
		/// </summary>
		public static Regex ForbiddenStylePattern = new Regex("(?:(expression|eval|javascript))\\s*\\(");  // expression(....)"   

		/// <summary>
		/// validates height width value
		/// </summary>
		public static readonly Regex validHeightWidthValue = new Regex("\\d+%|\\d+$|\\d+px");

		/// <summary>
		/// Determines whether the specified HTML is sanitized.
		/// </summary>
		/// <param name="html">The HTML.</param>
		/// <returns>
		///   <c>true</c> if the specified HTML is sanitized; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsSanitized(String html) {
			return Sanitizer(html).IsValid;
		}


		/// <summary>
		/// Sanitizes the specified HTML.
		/// </summary>
		/// <param name="html">The HTML.</param>
		/// <param name="isValid">if set to <c>true</c> [is valid].</param>
		/// <param name="invalidTags">The invalid tags.</param>
		/// <returns></returns>
		public static String Sanitize(String html, out bool isValid, out List<string> invalidTags) {
			var retVal = Sanitizer(html);
			isValid = retVal.IsValid;
    		invalidTags = retVal.InvalidTags; 
			return retVal.Html;
		}

		/// <summary>
		/// Gets the text.
		/// </summary>
		/// <param name="html">The HTML.</param>
		/// <returns></returns>
		public static String GetText(String html) {
			var sanitizerResult = Sanitizer(html);
			return sanitizerResult.Text;
		}

		/// <summary>
		/// Sanitizers the specified HTML.
		/// </summary>
		/// <param name="html">The HTML.</param>
		/// <returns></returns>
		private static SanitizeResult Sanitizer(String html) {
			return Sanitizer(html, AllowedTags, ForbiddenTags);
		}

		/// <summary>
		/// Sanitizers the specified HTML.
		/// </summary>
		/// <param name="html">The HTML.</param>
		/// <param name="allowedTags">The allowed tags.</param>
		/// <param name="forbiddenTags">The forbidden tags.</param>
		/// <returns></returns>
		private static SanitizeResult Sanitizer(String html, Regex allowedTags, Regex forbiddenTags) {
			var ret = new SanitizeResult();
			var openTags = new Stack<string>();

			if (String.IsNullOrEmpty(html))
				return ret;

			List<String> tokens = Tokenize(html);

			// -------------------   LOOP for every token -------------------------- 
			for (int i = 0; i < tokens.Count; i++) {
				String token = tokens[i];
				bool isAcceptedToken = false;

				Match startMatcher = tagStartPattern.Match(token);
				Match endMatcher = tagClosePattern.Match(token);

				//--------------------------------------------------------------------------------  COMMENT    <!-- ......... --> 
				if (CommentPattern.Match(token).Success) {
					ret.Val = ret.Val + token + (token.EndsWith("-->") ? "" : "-->");
					ret.InvalidTags.Add(token + (token.EndsWith("-->") ? "" : "-->"));
					continue;

					//--------------------------------------------------------------------------------  OPEN TAG    <tag .........> 
				}
				else if (startMatcher.Success) {

					//tag name extraction 
					String tag = startMatcher.Groups[1].Value.ToLower();

					//-----------------------------------------------------  FORBIDDEN TAG   <script .........> 
					if (forbiddenTags.Match(tag).Success) {
						ret.InvalidTags.Add("<" + tag + ">");
						continue;

						// --------------------------------------------------  WELL KNOWN TAG 
					}
					else if (allowedTags.Match(tag).Success) {

						String cleanToken = "<" + tag;
						String tokenBody = startMatcher.Groups[2].Value;

						//first test table consistency 
						//table tbody tfoot thead th tr td 
						if ("thead".Equals(tag) || "tbody".Equals(tag) || "tfoot".Equals(tag) || "tr".Equals(tag)) {
							if (openTags.Select(t => t == "table").Count() <= 0) {
								ret.InvalidTags.Add("<" + tag + ">");
								continue;
							}
						}
						else if ("td".Equals(tag) || "th".Equals(tag)) {
							if (openTags.Count(t => t == "tr") <= 0) {
								ret.InvalidTags.Add("<" + tag + ">");
								continue;
							}
						}

						// then test properties 
						//Match attributes = attributesPattern.Match(tokenBody);
						var attributes = attributesPattern.Matches(tokenBody);

						bool foundURL = false; // URL flag

						foreach (Match attribute in attributes)
						//while (attributes.find())
                        {
							String attr = attribute.Groups[1].Value.ToLower();
							String val = attribute.Groups[2].Value;

							// we will accept href in case of <A> 
							if ("a".Equals(tag) && "href".Equals(attr)) {    // <a href="......">
								try {
									var url = new Uri(val);

									if (url.Scheme == Uri.UriSchemeHttp || url.Scheme == Uri.UriSchemeHttps || url.Scheme == Uri.UriSchemeMailto) {
										foundURL = true;
									}
									else {
										ret.InvalidTags.Add(attr + "->" + val);
										val = "";
									}
								}
								catch {
									ret.InvalidTags.Add(attr + "->" + val);
									val = "";
								}
							}
							else if ((tag == "img" || tag == "embed") && "src".Equals(attr)) { // <img src="......"> 
								try {
									var url = new Uri(val);

									if (url.Scheme == Uri.UriSchemeHttp || url.Scheme == Uri.UriSchemeHttps) {
										foundURL = true;
									}
									else {
										ret.InvalidTags.Add(attr + "->" + val);
										val = "";
									}
								}
								catch {
									ret.InvalidTags.Add(attr + "->" + val);
									val = "";
								}

							}
							else if ("href".Equals(attr) || "src".Equals(attr)) { // <tag src/href="......">   skipped 
								ret.InvalidTags.Add(tag + "->" + attr + "->" + val);
								continue;

							}
							else if (attr == "width" || attr == "height") { // <tag width/height="......">
								if (!validHeightWidthValue.Match(val.ToLower()).Success) { // test numeric values 
									ret.InvalidTags.Add(tag + "->" + attr + "->" + val);
									continue;
								}
							}
							else if ("style".Equals(attr)) { // <tag style="......"> 

								// then test properties 
								var styles = stylePattern.Matches(val);
								String cleanStyle = "";

								foreach (Match style in styles)
								//while (styles.find())
                                {
									String styleName = style.Groups[1].Value.ToLower();
									String styleValue = style.Groups[2].Value;

									// suppress invalid styles values 
									if (ForbiddenStylePattern.Match(styleValue).Success) {
										ret.InvalidTags.Add(tag + "->" + attr + "->" + styleValue);
										continue;
									}

									// check if valid url 
									Match urlStyleMatcher = urlStylePattern.Match(styleValue);
									if (urlStyleMatcher.Success) {
										try {
											String url = urlStyleMatcher.Groups[1].Value;
											var uri = new Uri(url);

											if (!(uri.Scheme == Uri.UriSchemeHttp || uri.Scheme == Uri.UriSchemeHttps)) {
												ret.InvalidTags.Add(tag + "->" + attr + "->" + styleValue);
												continue;
											}
										}
										catch {
											ret.InvalidTags.Add(tag + "->" + attr + "->" + styleValue);
											continue;
										}
									}

									cleanStyle = cleanStyle + styleName + ":" + Encode(styleValue) + ";";
								}
								val = cleanStyle;

							}
							else if (attr.StartsWith("on")) {  // skip all javascript events 
								ret.InvalidTags.Add(tag + "->" + attr + "->" + val);
								continue;
							}
							else {  // by default encode all properies 
								val = Encode(val);
							}

							cleanToken = cleanToken + " " + attr + "=\"" + val + "\"";
						}
						if (selfClosed.Match(token).Success)
							cleanToken = cleanToken + "/>";
						else
							cleanToken = cleanToken + ">";
						isAcceptedToken = true;

						// for <img> and <a>
						if ((tag == "a" || tag == "img" || tag == "embed") && !foundURL) {
							isAcceptedToken = false;
							cleanToken = "";
						}

						token = cleanToken;

						// push the tag if require closure and it is accepted (otherwise is encoded) 
						if (isAcceptedToken && !(standAloneTags.Match(tag).Success || selfClosed.Match(token).Success))
							openTags.Push(tag);

						// --------------------------------------------------------------------------------  UNKNOWN TAG 
					}
					else {
						ret.InvalidTags.Add(token);
						ret.Val = ret.Val + token;
						continue;
					}

					// --------------------------------------------------------------------------------  CLOSE TAG </tag> 
				}
				else if (endMatcher.Success) {
					String tag = endMatcher.Groups[1].Value.ToLower();

					//is self closing 
					if (selfClosed.Match(tag).Success) {
						ret.InvalidTags.Add(token);
						continue;
					}
					if (forbiddenTags.Match(tag).Success) {
						ret.InvalidTags.Add("/" + tag);
						continue;
					}
					if (!allowedTags.Match(tag).Success) {
						ret.InvalidTags.Add(token);
						ret.Val = ret.Val + token;
						continue;
					}
					else {
						String cleanToken = "";

						// check tag position in the stack 
						int pos = -1;
						bool found = false;

						foreach (var item in openTags) {
							pos++;
							if (item == tag) {
								found = true;
								break;
							}
						}

						// if found on top ok 
						if (found) {
							for (int k = 0; k <= pos; k++) {
								//pop all elements before tag and close it 
								String poppedTag = openTags.Pop();
								cleanToken = cleanToken + "</" + poppedTag + ">";
								isAcceptedToken = true;
							}
						}

						token = cleanToken;
					}
				}

				ret.Val = ret.Val + token;

				if (isAcceptedToken) {
					ret.Html = ret.Html + token;
					//ret.text = ret.text + " "; 
				}
				else {
					String sanToken = HtmlEncodeApexesAndTags(token);
					ret.Html = ret.Html + sanToken;
					ret.Text = ret.Text + HtmlEncodeApexesAndTags(RemoveLineFeed(token));
				}
			}

			// must close remaining tags 
			while (openTags.Count() > 0) {
				//pop all elements before tag and close it 
				String poppedTag = openTags.Pop();
				ret.Html = ret.Html + "</" + poppedTag + ">";
				ret.Val = ret.Val + "</" + poppedTag + ">";
			}

			//set boolean value 
			ret.IsValid = ret.InvalidTags.Count == 0;

			return ret;
		}

		/// <summary>
		/// Tokenizes the specified HTML.
		/// </summary>
		/// <param name="html">The HTML.</param>
		/// <returns></returns>
		private static List<String> Tokenize(String html) {
			//ArrayList tokens = new ArrayList();
			List<String> tokens = new List<string>();
			int pos = 0;
			String token = "";
			int len = html.Length;
			while (pos < len) {
				char c = html[pos];
				// BBB String ahead = html.Substring(pos, pos > len - 4 ? len : pos + 4);
				String ahead = html.Substring(pos, pos > len - 4 ? len - pos : 4);
				//a comment is starting 
				if ("<!--".Equals(ahead)) {
					//store the current token 
					if (token.Length > 0)
						tokens.Add(token);
					//clear the token 
					token = "";
					// serch the end of <......> 
					int end = MoveToMarkerEnd(pos, "-->", html);

					// BBB tokens.Add(html.Substring(pos, end));
					tokens.Add(html.Substring(pos, end - pos));
					pos = end;
					// a new "<" token is starting 
				}
				else if ('<' == c) {
					//store the current token 
					if (token.Length > 0)
						tokens.Add(token);
					//clear the token 
					token = "";
					// serch the end of <......> 
					int end = MoveToMarkerEnd(pos, ">", html);
					// BBB tokens.Add(html.Substring(pos, end));
					tokens.Add(html.Substring(pos, end - pos));
					pos = end;
				}
				else {
					token = token + c;
					pos++;
				}
			}

			//store the last token 
			if (token.Length > 0)
				tokens.Add(token);

			return tokens;
		}

		/// <summary>
		/// Moves to marker end.
		/// </summary>
		/// <param name="pos">The pos.</param>
		/// <param name="marker">The marker.</param>
		/// <param name="s">The s.</param>
		/// <returns></returns>
		private static int MoveToMarkerEnd(int pos, String marker, String s) {
			int i = s.IndexOf(marker, pos);
			if (i > -1)
				pos = i + marker.Length;
			else
				pos = s.Length;
			return pos;
		}


		/// <summary>
		/// sanitize results class
		/// </summary>
		private class SanitizeResult {
			/// <summary>
			/// the html after sanitize
			/// </summary>
			public String Html = "";
			/// <summary>
			/// text
			/// </summary>
			public String Text = "";
			/// <summary>
			/// Value
			/// </summary>
			public String Val = "";
			/// <summary>
			/// Is valid before sanitization
			/// </summary>
			public bool IsValid = true;
			/// <summary>
			/// Invalid tags found in the sanitize
			/// </summary>
			public readonly List<String> InvalidTags = new List<string>();
		}

		/// <summary>
		/// Encodes the specified string.
		/// </summary>
		/// <param name="s">The s.</param>
		/// <returns></returns>
		public static String Encode(String s) {
			return ConvertLineFeedToBr(HtmlEncodeApexesAndTags(s ?? ""));
		}

		/// <summary>
		/// HTMLs the encode apexes and tags.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <returns></returns>
		public static String HtmlEncodeApexesAndTags(String source) {
			return HtmlEncodeTag(HtmlEncodeApexes(source));
		}

		/// <summary>
		/// HTMLs the encode apexes.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <returns></returns>
		public static String HtmlEncodeApexes(String source) {
			/*if (source != null)
			{
				String result = replaceAllNoRegex(source, new String[] { "&", "\"", "'" }, new String[] { "&amp;", "&quot;", "&#39;" });
				return result;
			}
			else
				return null;*/
			if (source != null) {
				String result = ReplaceAllNoRegex(source, new String[] { "\"", "'" }, new String[] { "&quot;", "&#39;" });
				return result;
			}
			else
				return null;
		}

		/// <summary>
		/// HTMLs the encode tag.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <returns></returns>
		public static String HtmlEncodeTag(String source) {
			if (source != null) {
				String result = ReplaceAllNoRegex(source, new String[] { "<", ">" }, new String[] { "&lt;", "&gt;" });
				return result;
			}
			else
				return null;
		}

		/// <summary>
		/// Converts the line feed to BR.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		public static String ConvertLineFeedToBr(String text) {
			if (text != null)
				return ReplaceAllNoRegex(text, new String[] { "\n", "\f", "\r" }, new String[] { "<br>", "<br>", " " });
			else
				return null;
		}

		/// <summary>
		/// Removes the line feed.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		public static String RemoveLineFeed(String text) {

			if (text != null)
				return ReplaceAllNoRegex(text, new String[] { "\n", "\f", "\r" }, new String[] { " ", " ", " " });
			else
				return null;
		}

		/// <summary>
		/// Replaces all no regex.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="searches">The searches.</param>
		/// <param name="replaces">The replaces.</param>
		/// <returns></returns>
		public static String ReplaceAllNoRegex(String source, String[] searches, String[] replaces) {
			int k;
			String tmp = source;
			for (k = 0; k < searches.Length; k++)
				tmp = ReplaceAllNoRegex(tmp, searches[k], replaces[k]);
			return tmp;
		}

		/// <summary>
		/// Replaces all no regex.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <param name="search">The search.</param>
		/// <param name="replace">The replace.</param>
		/// <returns></returns>
		public static String ReplaceAllNoRegex(String source, String search, String replace) {
			StringBuilder buffer = new StringBuilder();
			if (source != null) {
				if (search.Length == 0)
					return source;
				int oldPos, pos;
				for (oldPos = 0, pos = source.IndexOf(search, oldPos); pos != -1; oldPos = pos + search.Length, pos = source.IndexOf(search, oldPos)) {
					buffer.Append(source.Substring(oldPos, pos - oldPos));
					buffer.Append(replace);
				}
				if (oldPos < source.Length)
					buffer.Append(source.Substring(oldPos));
			}
			return buffer.ToString();
		}
	}
}

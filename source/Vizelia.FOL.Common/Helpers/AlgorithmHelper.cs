﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Runtime.Serialization;
using System.Security.Policy;
using System.Text;
using System.Threading;
using Vizelia.FOL.BusinessEntities;
using Microsoft.CSharp;
using System.Web;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Algorithm Helper.
	/// </summary>
	public static class AlgorithmHelper {
		/*
		 * How to develop an Algorithm plugin interface?
		 * 1. Create the Algorithm interface.
		 * 2. Create the adapter interface that takes data in and out. The interface should extend your Algorithm interface.
		 * 3. Create the guest adapter class, that will be invoked on the guest AppDomain. The class should implement the adapter interface.
		 * 4. Create the host adapter class. It should extend the base HostAdapter<TAdapter> and implement the Algorithm interface.
		 * 
		 * How does the sandboxing work?
		 * 1. We search for entry points in the DLL
		 * 2. We instantiate Algorithm entry point class and the adapter class in the guest AppDomain. We wrap the Algorithm with the adapter.
		 * 3. In the host AppDomain, we get the adapter interface instance for the adapter in the guest AppDomain.
		 * 4. We create the host adapter in the host AppDomain, giving it the adapter interface instance so it can call it.
		 * 5. We give the user the host adapter as the Algorithm interface so the adapter implementation will be encapsulated.
		 */

		private const byte const_mask = 15;
		private const string const_hex = "0123456789ABCDEF";

		private const string const_builtin_algorithms_namespace = "Vizelia.FOL.ConcreteProviders";
		private const string const_appdomain_prefix = "Algorithm:";
		private const string const_compiled_editable_algorithm_filename_format = "CompiledAlgorithm.{0}.dll";
		private const string const_trace_category = "Algorithm loading";
		private static readonly object m_CacheLock = new object();
		private static readonly object m_EntryPointLock = new object();
		private static readonly ConcurrentDictionary<int, bool> m_AppDomainResolveRegistrations = new ConcurrentDictionary<int, bool>();

		/// <summary>
		/// Gets the built in Algorithms.
		/// </summary>
		/// <typeparam name="TAlgorithmInterface">The type of the algorithm interface.</typeparam>
		/// <returns></returns>
		public static List<Type> GetBuiltInAlgorithms<TAlgorithmInterface>() {
			string key = string.Format(CacheKey.const_cache_algorithm_types, typeof(TAlgorithmInterface).Name);
			List<Type> retval = CacheService.Get(key, GetBuiltInAlgorithmsInternal<TAlgorithmInterface>, location: CacheLocation.Memory);

			return retval;
		}

		/// <summary>
		/// Gets the built in Algorithms internal (cache miss function).
		/// </summary>
		/// <typeparam name="TAlgorithmInterface">The type of the Algorithm interface.</typeparam>
		/// <returns></returns>
		private static List<Type> GetBuiltInAlgorithmsInternal<TAlgorithmInterface>() {
			var abstractType = typeof(TAlgorithmInterface);

			var analyticsExtensionTypes = new List<Type>();
			var assemblys = AppDomain.CurrentDomain.GetAssemblies().ToList();

			foreach (var assembly in assemblys) {
				try {
					var types = assembly.GetTypes();
					foreach (var type in types) {
						// We only take built-in algorithms from Vizelia.FOL.ConcreteProviders assembly.
						//TODO: Validate public key token of that assembly here that it's ours. DOR.
						if (abstractType.IsAssignableFrom(type) && type.Assembly.FullName.StartsWith(const_builtin_algorithms_namespace) && !type.IsAbstract && type.IsClass) {
							analyticsExtensionTypes.Add(type);
						}
					}
				}
				catch (Exception) {
					// Do nothing.					
				}
			}

			return analyticsExtensionTypes;
		}


		/// <summary>
		/// Creates the instance from uploaded file.
		/// </summary>
		/// <typeparam name="TAbstract">The type of the abstract.</typeparam>
		/// <param name="algorithm">The algorithm.</param>
		/// <param name="errors">The errors.</param>
		/// <returns></returns>
		public static AlgorithmInstance<TAbstract> CreateExternalLibraryAlgorithmInstance<TAbstract>(Algorithm algorithm, out List<string> errors) {
			try {
				DirectoryInfo directory = GetAlgorithmDirectory(algorithm.UniqueId);
				AlgorithmEntryPoint entryPoint = DeployAlgorithm<TAbstract>(algorithm, directory);
				entryPoint.AssemblyFileName = Path.Combine(directory.FullName, entryPoint.AssemblyFileName);

				AlgorithmInstance<TAbstract> algorithmInstance;

				if (VizeliaConfiguration.Instance.AlgorithmsConfiguration.ShouldRunAlgorithmsInSandbox) {
					algorithmInstance = CreateSandboxedExternalLibraryAlgorithmInstance<TAbstract>(algorithm, entryPoint);
				}
				else {
					algorithmInstance = CreateSimpleExternalLibraryAlgorithmInstance<TAbstract>(entryPoint);
				}

				errors = new List<string>();
				return algorithmInstance;
			}
			catch (Exception e) {
				errors = new List<string> { e.ToString() };
				return null;
			}
		}

		private static AlgorithmInstance<TAbstract> CreateSandboxedExternalLibraryAlgorithmInstance<TAbstract>(Algorithm algorithm, AlgorithmEntryPoint entryPoint) {
			// Create sandbox
			var evidence = new Evidence(AppDomain.CurrentDomain.Evidence);
			AppDomain sandbox = AppDomain.CreateDomain(const_appdomain_prefix + algorithm.UniqueId, evidence, AppDomain.CurrentDomain.SetupInformation);
			sandbox.AssemblyResolve += ResolveAssembly;

			// Create algorithm
			AlgorithmInstance<TAbstract> algorithmInstance = CreateSandboxedAlgorithmInstance<TAbstract>(entryPoint, sandbox);

			return algorithmInstance;
		}

		private static AlgorithmEntryPoint DeployAlgorithm<TAbstract>(Algorithm algorithm, DirectoryInfo directory) {
			if (algorithm.Source == AlgorithmSource.ExternalLibrary) {
				// In case of editable, it should already be there.
				CacheAlgorithmBinaries(algorithm);
			}

			var entryPoint = GetBinaryEntryPoint<TAbstract>(directory);
			return entryPoint;
		}

		private static AlgorithmInstance<TAbstract> CreateSimpleExternalLibraryAlgorithmInstance<TAbstract>(AlgorithmEntryPoint entryPoint) {
			// Only one ResolveAssembly registration per appdomain. It doesn't protect against multiple registrations by the same handler.
			m_AppDomainResolveRegistrations.GetOrAdd(AppDomain.CurrentDomain.Id, i => {
				AppDomain.CurrentDomain.AssemblyResolve += ResolveAssembly;
				return true;
			});

			Assembly assembly = Assembly.LoadFile(entryPoint.AssemblyFileName);
			Type type = assembly.GetType(entryPoint.TypeFullName);

			var instance = (TAbstract)Activator.CreateInstance(type);
			var algorithmHost = new AlgorithmInstance<TAbstract>(instance, null);

			return algorithmHost;
		}

		private static AlgorithmInstance<TAbstract> CreateSandboxedAlgorithmInstance<TAbstract>(AlgorithmEntryPoint entryPoint, AppDomain sandbox) {
			try {
				Type algorithmInterface = typeof(TAbstract);

				// Get the interface of the adapter.
				Type adapterInterface = Helper.ResolveType(t => IsAdapterInterface(t, algorithmInterface));

				// Get the guest adapter type.
				Type concreteAdapterType = Helper.ResolveToConcreteClassType(adapterInterface);

				// Create the guest adapter in guest AppDomain.
				var ctorParameters = new object[] { entryPoint };
				object guestAdapterProxy = sandbox.CreateInstanceAndUnwrap(concreteAdapterType.Assembly.FullName, concreteAdapterType.FullName, true, BindingFlags.Default, null, ctorParameters, null, null);

				// Get the host adapter type.
				Type hostAdapterAbstractType = typeof(HostAdapter<>).MakeGenericType(adapterInterface);
				Type hostAdapterType = Helper.ResolveType(t => !t.IsInterface && !t.IsAbstract && hostAdapterAbstractType.IsAssignableFrom(t) && algorithmInterface.IsAssignableFrom(t));

				// Create the host adapter in the host AppDomain.
				var hostAdapterInstance = (TAbstract)Activator.CreateInstance(hostAdapterType, guestAdapterProxy);

				var algorithmHost = new AlgorithmInstance<TAbstract>(hostAdapterInstance, sandbox);
				return algorithmHost;
			}
			catch (Exception e) {
				using (TracingService.StartTracing(const_trace_category, entryPoint.AssemblyFileName)) {
					TracingService.Write(e);
				}

				if (sandbox != null) {
					try {
						sandbox.AssemblyResolve -= ResolveAssembly;
						AppDomain.Unload(sandbox);
					}
					catch (Exception) {
						// Do nothing.
					}
				}
				throw;
			}
		}


		internal static Assembly ResolveAssembly(object sender, ResolveEventArgs args) {
			Assembly assembly = null;
			var assemblyName = args.Name.Replace("\\", "");

			try {
				// Try loading an assembly that is deployed together with the requesting assmebly.
				if (args.RequestingAssembly != null) {
					assembly = TryLoadAssemblyByRequestingAssembly(args.RequestingAssembly, assemblyName);
				}

				// Or try load from the algorithm directory, in case it is a sandboxed algorithm.
				if (assembly == null) {
					assembly = TryLoadSandboxedAlgorithmAssembly(sender, assemblyName);
				}
			}
			catch {
				// Do nothing.
			}

			return assembly;
		}


		private static Assembly TryLoadAssemblyByRequestingAssembly(Assembly requestingAssembly, string assemblyName) {
			Assembly assembly = null;
			string requestingAssemblyDirectory = Path.GetDirectoryName(requestingAssembly.CodeBase);

			if (requestingAssemblyDirectory.ToLower().StartsWith("file:\\")) {
				requestingAssemblyDirectory = requestingAssemblyDirectory.Substring(6);
			}

			string algorithmsRootDirectory = GetAlgorithmsRootDirectory();

			// For security reasons, we make sure that the requesting assembly is only run within the algorithm folder.
			if (requestingAssemblyDirectory.ToLower().StartsWith(algorithmsRootDirectory.ToLower())) {
				// Then the requesting assembly file is in the algorithms folder. Search for its dependencies there.
				assembly = FindAssembliesInDirectory(assemblyName, requestingAssemblyDirectory);

				if (assembly == null) {
					assembly = TryLoadLocalizedAssembly(assemblyName, requestingAssemblyDirectory);
				}
			}

			// Or maybe it's already loaded?
			if (assembly == null) {
				Assembly[] currentAssemblies = AppDomain.CurrentDomain.GetAssemblies();
				// We only allow loading of assmeblies that aren't deployed with algorithms, because they might be replaced in runtime but still loaded into memory.
				string algorithmsRootDirectoryUri = new Uri(algorithmsRootDirectory).AbsoluteUri;
				assembly = currentAssemblies.FirstOrDefault(a => a.FullName.Equals(assemblyName) && !a.CodeBase.StartsWith(algorithmsRootDirectoryUri, StringComparison.InvariantCultureIgnoreCase));
			}



			return assembly;
		}

		private static Assembly TryLoadLocalizedAssembly(string assemblyName, string requestingAssemblyDirectory) {
			// Isolate Culture=en-us, ... part of the string.
			int startOfCulture = assemblyName.IndexOf("Culture=", StringComparison.InvariantCulture);
			int startOfCultureValue = startOfCulture + 8;
			int comma = assemblyName.IndexOf(",", startOfCultureValue, StringComparison.InvariantCultureIgnoreCase);
			string culture = assemblyName.Substring(startOfCultureValue, comma - startOfCultureValue);
			
			// Look for it in the directory.
			Assembly assembly = FindAssembliesInDirectory(assemblyName, Path.Combine(requestingAssemblyDirectory, culture));
			return assembly;

		}

		private static Assembly TryLoadSandboxedAlgorithmAssembly(object sender, string assemblyName) {
			Assembly assembly = null;
			var guestAppDomain = sender as AppDomain;

			if (guestAppDomain != null && guestAppDomain.FriendlyName.Contains(const_appdomain_prefix)) {
				// Try finding it near the algorithm.
				string uniqueId = guestAppDomain.FriendlyName.Replace(const_appdomain_prefix, "");
				DirectoryInfo directory = GetAlgorithmDirectory(uniqueId);
				assembly = FindAssembliesInDirectory(assemblyName, directory.FullName);

				// Or maybe it's already loaded? This test is OK because it only works on a sandboxed AppDomain.
				if (assembly == null) {
					Assembly[] currentAssemblies = guestAppDomain.GetAssemblies();
					assembly = currentAssemblies.FirstOrDefault(a => a.FullName.Equals(assemblyName));
				}
			}

			return assembly;
		}

		private static Assembly FindAssembliesInDirectory(string assemblyName, string directory) {
			if (!Directory.Exists(directory)) {
				return null;
			}

			string[] files = Directory.GetFiles(directory, "*.dll");

			foreach (string file in files) {
				Assembly assembly;

				if (TryLoadAssemblyFromFile(file, assemblyName, out assembly))
					return assembly;
			}

			return null;
		}

		private static bool TryLoadAssemblyFromFile(string file, string assemblyName, out Assembly assembly) {
			try {
				// Convert the filename into an absolute file name for use with LoadFile. 
				file = new FileInfo(file).FullName;

				if (AssemblyName.GetAssemblyName(file).FullName.Equals(assemblyName)) {
					ValidateAssemblyIsAllowed(file);
					assembly = Assembly.LoadFile(file);
					return true;
				}
			}
			catch (Exception e) {
				if (e is VizeliaException) {
					throw;
				}

				// Else, do nothing.
				using (TracingService.StartTracing(const_trace_category, file)) {
					TracingService.Write(e);
				}
			}

			assembly = null;
			return false;
		}

		/// <summary>
		/// Validates the assembly is allowed.
		/// </summary>
		/// <param name="file">The file.</param>
		private static void ValidateAssemblyIsAllowed(string file) {
			bool allowed = IsAssemblyAllowed(file);

			if (!allowed) {
				string fileName = Path.GetFileName(file);
				string message = string.Format("Assembly file '{0}' is not explicitly allowed by public key token. Edit configuration to allow.", fileName);
				throw new VizeliaException(message);
			}
		}

		/// <summary>
		/// Determines whether the type t is an adapter interface.
		/// </summary>
		/// <param name="t">The t.</param>
		/// <param name="algorithmInterface">The algorithm interface.</param>
		/// <returns>
		///   <c>true</c> if [is adapter interface] [the specified t]; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsAdapterInterface(Type t, Type algorithmInterface) {
			bool found = false;

			if (t.IsInterface) {
				AlgorithmAdapterAttribute attribute = t.GetCustomAttributes(typeof(AlgorithmAdapterAttribute), false).OfType<AlgorithmAdapterAttribute>().FirstOrDefault();
				found = (attribute != null && attribute.InterfaceType == algorithmInterface);
			}

			return found;
		}

		/// <summary>
		/// Caches to disk instead of querying from the database every time.
		/// Returns a boolean value indicating whether the Algorithm is cached or not.
		/// </summary>
		/// <typeparam name="TAbstractAlgorithm">The type of the abstract algorithm.</typeparam>
		/// <param name="algorithm">The algorithm.</param>
		/// <param name="errors">The errors.</param>
		public static void CacheEditableAlgorithm<TAbstractAlgorithm>(Algorithm algorithm, out List<string> errors) {
			lock (m_CacheLock) {
				DirectoryInfo directory = GetAlgorithmDirectory(algorithm.UniqueId);

				string cachedFlagFilename = Path.Combine(directory.FullName, "__Cache.txt");

				if (!File.Exists(cachedFlagFilename)) {
					// Lets recreate the cache.
					directory.DeleteAllContent();
					CompileAlgorithmFromCode<TAbstractAlgorithm>(algorithm, out errors);
					File.WriteAllText(cachedFlagFilename, DateTime.UtcNow.ToString("o"));
				}
				else {
					errors = new List<string>();
				}
			}
		}

		/// <summary>
		/// Caches to disk instead of querying from the database every time.
		/// Returns a boolean value indicating whether the Algorithm is cached or not.
		/// </summary>
		/// <param name="algorithm">The algorithm.</param>
		/// <returns></returns>
		public static bool CacheAlgorithmBinaries(Algorithm algorithm) {
			lock (m_CacheLock) {
				var binaryFile = algorithm.Binary;
				DirectoryInfo directory = GetAlgorithmDirectory(algorithm.UniqueId);
				bool isCached;

				string cachedFlagFilename = Path.Combine(directory.FullName, "__Cache.txt");

				if (File.Exists(cachedFlagFilename)) {
					isCached = true;
				}
				else if (binaryFile != null && binaryFile.DocumentContents != null) {
					// Make sure the filename isn't malicious.
					if (binaryFile.DocumentName.StartsWith("\\") || binaryFile.DocumentName.StartsWith("..\\")) {
						throw new VizeliaException("Malicious filename detected!");
					}

					// Lets recreate the cache.
					directory.DeleteAllContent();

					if (binaryFile.DocumentName.ToLower().EndsWith(".dll")) {
						// Write the file to the directory.
						string fullFileName = Path.Combine(directory.FullName, binaryFile.DocumentName);
						File.WriteAllBytes(fullFileName, binaryFile.DocumentContents);
					}
					else if (CompressionHelper.IsArchive(binaryFile.DocumentName)) {
						// Extract the files in the archive to the directory.
						using (var stream = new MemoryStream(binaryFile.DocumentContents)) {
							CompressionHelper.Extract(stream, directory.FullName, false);
						}
					}

					DeleteSystemAssemblies(directory);


					File.WriteAllText(cachedFlagFilename, DateTime.UtcNow.ToString("o"));
					isCached = true;
				}
				else {
					isCached = false;
				}

				return isCached;
			}
		}

		/// <summary>
		/// Deletes the system assemblies from the algorithm folder.
		/// We do not allow developers to upload assemblies from the system itself.
		/// Instead, we change the assembly binding redirection in the web.config file to support these dependencies.
		/// </summary>
		/// <param name="algorithmDirectory">The algorithm directory.</param>
		private static void DeleteSystemAssemblies(DirectoryInfo algorithmDirectory) {
			FileInfo[] systemAssemblies = algorithmDirectory.GetFiles("Vizelia.FOL.*.dll", SearchOption.AllDirectories);

			foreach (var assemblyFile in systemAssemblies) {
				DirectoryInfo assemblyDirectory = assemblyFile.Directory;
				assemblyFile.Delete();
				
				if (assemblyDirectory != null && !assemblyDirectory.FullName.Equals(algorithmDirectory.FullName, StringComparison.InvariantCultureIgnoreCase) && !assemblyDirectory.GetFiles("*.*", SearchOption.AllDirectories).Any()) {
					assemblyDirectory.Delete(true);
				}
			}
		}

		/// <summary>
		/// Gets the Algorithm directory.
		/// </summary>
		/// <param name="uniqueId">The unique id.</param>
		/// <returns></returns>
		private static DirectoryInfo GetAlgorithmDirectory(string uniqueId) {
			string algorithmsRootDirectory = GetAlgorithmsRootDirectory();
			string algorithmPath = string.Format("{0}\\{1}", ContextHelper.ApplicationName, uniqueId);
			DirectoryInfo directory = Directory.CreateDirectory(Path.Combine(algorithmsRootDirectory, algorithmPath));

			return directory;
		}

		/// <summary>
		/// Gets the algorithms root directory.
		/// </summary>
		/// <returns></returns>
		private static string GetAlgorithmsRootDirectory() {
			string algorithmsRootDirectory = Path.Combine(VizeliaConfiguration.Instance.Deployment.TempDirectory, "AlgorithmCache");
			return algorithmsRootDirectory;
		}

		[DllImport("mscoree.dll", CharSet = CharSet.Unicode)]
		static extern bool StrongNameSignatureVerificationEx(string wszFilePath, bool fForceVerification, ref bool pfWasVerified);

		/// <summary>
		/// Determines whether the assembly is allowed or not.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <returns>
		///   <c>true</c> if the assembly is allowed; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsAssemblyAllowed(string filePath) {
			//TODO: Remove support of editable algorithms. DOR.
			if (!VizeliaConfiguration.Instance.AlgorithmsConfiguration.ShouldValidateStrongName) return true;

			// Get allowed public key tokens from configuration.
			var allowedPublicKeyTokens =
				VizeliaConfiguration.Instance.AlgorithmsConfiguration
					.Cast<AlgorithmsConfigurationElement>()
					.Where(e => !string.IsNullOrWhiteSpace(e.StrongName))
					.Select(e => e.StrongName.ToLower())
				.ToList();

			bool isAllowed = false;

			try {
				// Verify the strong name.
				bool wasVerified = false;
				bool isValidStrongName = StrongNameSignatureVerificationEx(filePath, true, ref wasVerified);

				if (isValidStrongName) {
					// Check whether the strong name token is in whitelist or not.
					string token = GetAssemblyToken(filePath).ToLower();
					isAllowed = allowedPublicKeyTokens.Any(t => t.Equals(token));
				}
			}
			catch (Exception e) {
				using (TracingService.StartTracing(const_trace_category, filePath)) {
					TracingService.Write(e);
				}
			}

			return isAllowed;
		}

		/// <summary>
		/// Gets the assembly token as a string.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <returns></returns>
		private static string GetAssemblyToken(string filePath) {
			AssemblyName assemblyName = AssemblyName.GetAssemblyName(filePath);
			byte[] assemblyToken = assemblyName.GetPublicKeyToken();

			// Convert the token to string representation.
			var token = new StringBuilder();

			foreach (byte b in assemblyToken) {
				token.Append(const_hex[b / 16 & const_mask]);
				token.Append(const_hex[b & const_mask]);
			}

			return token.ToString();
		}

		/// <summary>
		/// Reflection helper interface.
		/// </summary>
		public interface IReflectionHelper {
			/// <summary>
			/// Gets the binary entry point.
			/// </summary>
			/// <param name="path">The path.</param>
			/// <returns></returns>
			AlgorithmEntryPoint GetBinaryEntryPoint(string path);
		}

		/// <summary>
		/// An entry point to an algorithm.
		/// </summary>
		[Serializable]
		[DataContract]
		public class AlgorithmEntryPoint {
			/// <summary>
			/// Initializes a new instance of the <see cref="AlgorithmEntryPoint"/> class.
			/// </summary>
			public AlgorithmEntryPoint() {

			}

			/// <summary>
			/// Initializes a new instance of the <see cref="AlgorithmEntryPoint"/> class.
			/// </summary>
			/// <param name="typeFullName">Full name of the type.</param>
			/// <param name="assemblyQualifiedName">Name of the assembly qualified.</param>
			/// <param name="assemblyFileName">Name of the assembly file.</param>
			public AlgorithmEntryPoint(string typeFullName, string assemblyQualifiedName, string assemblyFileName) {
				TypeFullName = typeFullName;
				AssemblyQualifiedName = assemblyQualifiedName;
				AssemblyFileName = assemblyFileName;
			}

			/// <summary>
			/// Gets or sets the full name of the type.
			/// </summary>
			/// <value>
			/// The full name of the type.
			/// </value>
			[DataMember]
			public string TypeFullName { get; set; }

			/// <summary>
			/// Gets or sets the name of the assembly qualified.
			/// </summary>
			/// <value>
			/// The name of the assembly qualified.
			/// </value>
			[DataMember]
			public string AssemblyQualifiedName { get; set; }

			/// <summary>
			/// Gets or sets the name of the assembly file.
			/// </summary>
			/// <value>
			/// The name of the assembly file.
			/// </value>
			[DataMember]
			public string AssemblyFileName { get; set; }
		}

		/// <summary>
		/// Reflection helper.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		public class ReflectionHelper<T> : MarshalByRefObject, IReflectionHelper {
			/// <summary>
			/// Gets the binary entry point.
			/// </summary>
			/// <param name="path">The path.</param>
			/// <returns></returns>
			public AlgorithmEntryPoint GetBinaryEntryPoint(string path) {
				Type abstractType = typeof(T);
				var directory = new DirectoryInfo(path);

				foreach (FileInfo fileInfo in directory.GetFiles("*.dll")) {
					try {
						ValidateAssemblyIsAllowed(fileInfo.FullName);
						Assembly assembly = Assembly.LoadFrom(fileInfo.FullName);
						Type concrete = assembly.GetTypes().FirstOrDefault(t => t.IsClass && !t.IsAbstract && abstractType.IsAssignableFrom(t));

						if (concrete != null) {
							var entryPoint = new AlgorithmEntryPoint(concrete.FullName, concrete.Assembly.FullName, fileInfo.Name);
							return entryPoint;
						}
					}
					catch (Exception e) {
						using (TracingService.StartTracing(const_trace_category, path)) {
							TracingService.Write(e);
						}

						if (e is VizeliaException) {
							throw;
						}
					}
				}

				return null;
			}
		}

		private static AlgorithmEntryPoint GetBinaryEntryPoint<TAbstract>(DirectoryInfo directory) {
			lock (m_EntryPointLock) {
				// The entry point is composed of: 1. Type full name, 2. Assembly full name, 3. Assembly file name.

				string entryPointFilePath = Path.Combine(directory.FullName, "__EntryPoint.txt");
				AlgorithmEntryPoint entryPoint;

				if (File.Exists(entryPointFilePath)) {
					string entryPointData = File.ReadAllText(entryPointFilePath);
					entryPoint = Helper.DeserializeJson<AlgorithmEntryPoint>(entryPointData);
				}
				else {
					AppDomain discoveryDomain = null;

					try {
						var evidence = new Evidence(AppDomain.CurrentDomain.Evidence);
						discoveryDomain = AppDomain.CreateDomain("DiscoveryDomain_" + Guid.NewGuid().ToString(), evidence, AppDomain.CurrentDomain.SetupInformation);
						Type helperType = typeof(ReflectionHelper<>).MakeGenericType(typeof(TAbstract));
						ObjectHandle proxy = Activator.CreateInstance(discoveryDomain, helperType.Assembly.FullName, helperType.FullName);
						var helper = (IReflectionHelper)proxy.Unwrap();
						entryPoint = helper.GetBinaryEntryPoint(directory.FullName);
					}
					finally {
						if (discoveryDomain != null) {
							AppDomain.Unload(discoveryDomain);
						}
					}

					if (entryPoint != null) {
						string entryPointData = Helper.SerializeJson(entryPoint);
						File.WriteAllText(entryPointFilePath, entryPointData);
					}
					else {
						throw new VizeliaException("Could not find entry point.");
					}
				}

				return entryPoint;
			}
		}


		/// <summary>
		/// Gets the analytics class code.
		/// </summary>
		/// <param name="className">Name of the class.</param>
		/// <returns></returns>
		public static string GetAnalyticsClassCode(string className) {
			#region Imports
			var codeBase = new CodeCompileUnit();
			var nameSpace = new CodeNamespace();
			codeBase.Namespaces.Add(nameSpace);

			nameSpace.Imports.Add(new CodeNamespaceImport("System"));
			nameSpace.Imports.Add(new CodeNamespaceImport("System.Collections.Generic"));
			//nameSpace.Imports.Add(new CodeNamespaceImport("System.Drawing"));
			nameSpace.Imports.Add(new CodeNamespaceImport("System.Linq"));
			//nameSpace.Imports.Add(new CodeNamespaceImport("System.Text"));
			//nameSpace.Imports.Add(new CodeNamespaceImport("System.Threading"));
			nameSpace.Imports.Add(new CodeNamespaceImport("Vizelia.FOL.Common"));
			nameSpace.Imports.Add(new CodeNamespaceImport("Vizelia.FOL.BusinessEntities"));
			#endregion
			#region Class
			var analytics = typeof(IAnalyticsExtensibility);
			var classNameSpace = new CodeNamespace(const_builtin_algorithms_namespace);
			codeBase.Namespaces.Add(classNameSpace);
			var typedef = new CodeTypeDeclaration(className);//Create class
			typedef.BaseTypes.Add(new CodeTypeReference(analytics));
			classNameSpace.Types.Add(typedef);//Add the class to namespace defined above
			#endregion
			#region Methods
			MethodInfo[] methodInfos = analytics.GetMethods();

			foreach (MethodInfo m in methodInfos) {
				CodeMemberMethod method = new CodeMemberMethod();
				method.Name = m.Name;
				method.Attributes = MemberAttributes.Public | MemberAttributes.Final;
				m.GetParameters().ToList().ForEach(p => {
					method.Parameters.Add(new CodeParameterDeclarationExpression(p.ParameterType, p.Name));
				});
				method.ReturnType = new CodeTypeReference(m.ReturnType);

				if (method.Name == "Execute") {
					CodeMethodReturnStatement returnStatement = new CodeMethodReturnStatement();
					method.Statements.Add(new CodeSnippetStatement("\t\t\t return new Tuple<Chart, List<DataSerie>, Dictionary<string, string>>(chart, series, inputs);"));
				}
				else if (method.Name == "GetPublicInputs") {
					CodeMethodReturnStatement returnStatement = new CodeMethodReturnStatement();
					method.Statements.Add(new CodeSnippetStatement("\t\t\t return new Dictionary<string, string>();"));
				}
				else if (method.Name == "GetDataSeriesLocalId") {
					CodeMethodReturnStatement returnStatement = new CodeMethodReturnStatement();
					method.Statements.Add(new CodeSnippetStatement("\t\t\t return new List<ListElement>();"));
				}

				typedef.Members.Add(method);
			}
			#endregion
			#region Output code
			CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");
			//On définit les options de génération de code
			CodeGeneratorOptions options = new CodeGeneratorOptions();
			//On demande a ce que le code généré soit dans le même ordre que le code inséré
			options.VerbatimOrder = true;
			options.BracingStyle = "C";
			options.BlankLinesBetweenMembers = false;
			StringWriter sw = new StringWriter();
			IndentedTextWriter tw = new IndentedTextWriter(sw, "\t");

			//On demande la génération proprement dite
			provider.GenerateCodeFromCompileUnit(codeBase, tw, options);

			tw.Close();
			#endregion
			return sw.GetStringBuilder().ToString();
		}

		/// <summary>
		/// Determines whether the specified class name is analytics, by checking the namespace.
		/// </summary>
		/// <param name="className">Name of the class.</param>
		/// <returns>
		///   <c>true</c> if the specified class name is analytics; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsAnalytics(string className) {
			if (className != null)
				return className.StartsWith(const_builtin_algorithms_namespace);
			return false;
		}

		/// <summary>
		/// Create the instance.
		/// </summary>
		/// <typeparam name="TAbstractAlgorithm">The type of the abstract algorithm.</typeparam>
		/// <param name="className">Name of the class.</param>
		/// <param name="errors">The errors.</param>
		/// <returns></returns>
		public static AlgorithmInstance<TAbstractAlgorithm> CreateBuiltinAlgorithmInstance<TAbstractAlgorithm>(string className, out List<string> errors) {
			try {
				errors = new List<string>();

				if (!className.StartsWith(const_builtin_algorithms_namespace)) {
					className = const_builtin_algorithms_namespace + "." + className;
				}

				var type = Type.GetType(className, false);

				if (type != null) {
					var instance = (TAbstractAlgorithm)Activator.CreateInstance(type);
					var algorithmInstance = new AlgorithmInstance<TAbstractAlgorithm>(instance);

					return algorithmInstance;
				}
			}
			catch (Exception e) {
				errors = new List<string> { e.ToString() };
			}

			return null;
		}

		/// <summary>
		/// Creates the instance.
		/// </summary>
		/// <typeparam name="TAlgorithmInterface">The type of the algorithm interface.</typeparam>
		/// <param name="chartAlgorithm">The chart algorithm.</param>
		/// <param name="errors">The errors.</param>
		/// <returns></returns>
		public static AlgorithmInstance<TAlgorithmInterface> CreateInstance<TAlgorithmInterface>(ChartAlgorithm chartAlgorithm, out List<string> errors) {
			AlgorithmInstance<TAlgorithmInterface> instance;

			if (chartAlgorithm.Algorithm == null)
				instance = CreateBuiltinAlgorithmInstance<TAlgorithmInterface>(chartAlgorithm.KeyAlgorithm, out errors);
			else
				instance = CreateInstance<TAlgorithmInterface>(chartAlgorithm.Algorithm, out errors);

			return instance;
		}

		/// <summary>
		/// Creates the instance.
		/// </summary>
		/// <typeparam name="TAlgorithmInterface">The type of the algorithm interface.</typeparam>
		/// <param name="algorithm">The algorithm.</param>
		/// <param name="errors">The errors.</param>
		/// <returns></returns>
		public static AlgorithmInstance<TAlgorithmInterface> CreateInstance<TAlgorithmInterface>(Algorithm algorithm, out List<string> errors) {
			AlgorithmInstance<TAlgorithmInterface> instance;

			switch (algorithm.Source) {
				case AlgorithmSource.BuiltIn:
					instance = CreateBuiltinAlgorithmInstance<TAlgorithmInterface>(algorithm.Name, out errors);
					break;

				case AlgorithmSource.Editable:
					instance = CreateEditableAlgorithmInstance<TAlgorithmInterface>(algorithm, out errors);
					break;

				case AlgorithmSource.ExternalLibrary:
					instance = CreateExternalLibraryAlgorithmInstance<TAlgorithmInterface>(algorithm, out errors);
					break;
				default:
					throw new ArgumentOutOfRangeException("Source");
			}

			return instance;
		}

		/// <summary>
		/// Creates the instance. This is a wrapper that throws the errors as an exception instead of returning them
		/// as out parameters.
		/// </summary>
		/// <typeparam name="TAbstractAlgorithm">The type of the abstract algorithm.</typeparam>
		/// <param name="algorithm">The algorithm.</param>
		/// <returns></returns>
		public static AlgorithmInstance<TAbstractAlgorithm> CreateInstance<TAbstractAlgorithm>(Algorithm algorithm) {
			List<string> errors;
			var instance = CreateInstance<TAbstractAlgorithm>(algorithm, out errors);

			if (errors.Any()) {
				StringBuilder builder = new StringBuilder();
				builder.AppendLine("Errors at creation of algorithm instance:");
				foreach (string error in errors) {
					builder.AppendLine(error);
				}

				string message = builder.ToString();
				throw new VizeliaException(message);
			}

			return instance;
		}

		/// <summary>
		/// Creates the editable algorithm instance.
		/// </summary>
		/// <typeparam name="TAbstractAlgorithm">The type of the abstract algorithm.</typeparam>
		/// <param name="algorithm">The algorithm.</param>
		/// <param name="errors">The errors.</param>
		/// <returns></returns>
		private static AlgorithmInstance<TAbstractAlgorithm> CreateEditableAlgorithmInstance<TAbstractAlgorithm>(Algorithm algorithm, out List<string> errors) {
			AlgorithmInstance<TAbstractAlgorithm> algorithmInstance = null;

			// Create a DLL for the code.
			CacheEditableAlgorithm<TAbstractAlgorithm>(algorithm, out errors);

			if (!errors.Any()) {
				// Treat it as an external DLL from now.
				algorithmInstance = CreateExternalLibraryAlgorithmInstance<TAbstractAlgorithm>(algorithm, out errors);
			}

			return algorithmInstance;
		}

		private static void CompileAlgorithmFromCode<TAbstractAlgorithm>(Algorithm algorithm, out List<string> errors) {
			errors = new List<string>();
			CSharpCodeProvider c = new CSharpCodeProvider(new Dictionary<String, String> { { "CompilerVersion", "v4.0" } });
			CompilerParameters cp = new CompilerParameters();

			cp.ReferencedAssemblies.Add("system.dll");
			cp.ReferencedAssemblies.Add("system.xml.dll");
			cp.ReferencedAssemblies.Add("system.data.dll");
			cp.ReferencedAssemblies.Add("system.windows.forms.dll");
			cp.ReferencedAssemblies.Add("system.drawing.dll");
			cp.ReferencedAssemblies.Add("system.core.dll");
			cp.ReferencedAssemblies.Add("System.ServiceModel.dll");
			cp.ReferencedAssemblies.Add(HttpRuntime.BinDirectory + "Vizelia.FOL.Common.dll");
			cp.ReferencedAssemblies.Add(HttpRuntime.BinDirectory + "Vizelia.FOL.Utilities.WCFClient.dll");
			cp.ReferencedAssemblies.Add(HttpRuntime.BinDirectory + "Vizelia.FOL.WCFService.Contracts.dll");

			cp.CompilerOptions = "/t:library";
			try {
				var provider = CodeDomProvider.CreateProvider("CSharp");

				DirectoryInfo algorithmDirectory = GetAlgorithmDirectory(algorithm.UniqueId);
				string assemblyFullFilename = Path.Combine(algorithmDirectory.FullName, string.Format(const_compiled_editable_algorithm_filename_format, algorithm.UniqueId));
				cp.OutputAssembly = assemblyFullFilename;
				CompilerResults cr = provider.CompileAssemblyFromSource(cp, algorithm.Code);

				if (cr.Errors.Count == 0) {
					Assembly a = cr.CompiledAssembly;
					List<Type> runnableTypes =
						a.GetTypes().Where(t => t.IsClass && !t.IsAbstract && typeof(TAbstractAlgorithm).IsAssignableFrom(t)).ToList();

					if (runnableTypes.Count == 0) {
						errors.Add("Could not find entry point code in editable algorithm.");
					}
					else if (runnableTypes.Count > 1) {
						errors.Add("Found more than one entry point in editable algorithm, only one is allowed.");
					}
				}
				else {
					foreach (CompilerError error in cr.Errors) {
						errors.Add(string.Format("Error {0} at line #{1}: {2}", error.ErrorNumber, error.Line, error.ErrorText));
					}
				}
			}
			catch (Exception ex) {
				TracingService.Write(ex);
				errors.Add(ex.Message);
			}
		}

		/// <summary>
		/// Validates the Algorithm.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="errorMessage">The error message.</param>
		/// <returns></returns>
		public static bool ValidateAlgorithm<TAlgorithmInterface>(Algorithm item, out string errorMessage) {
			bool isValid;

			switch (item.Source) {
				case AlgorithmSource.BuiltIn:
					// Of course it's valid! We wrote it!
					isValid = true;
					errorMessage = string.Empty;
					break;

				case AlgorithmSource.Editable:
					isValid = ValidateEditableAlgorithm<TAlgorithmInterface>(item, out errorMessage);
					break;

				case AlgorithmSource.ExternalLibrary:
					isValid = ValidateExternalLibraryAlgorithm<TAlgorithmInterface>(item, out errorMessage);
					break;
				default:
					throw new ArgumentOutOfRangeException("item.Source");
			}

			return isValid;
		}

		private static bool ValidateExternalLibraryAlgorithm<TAlgorithmInterface>(Algorithm item, out string errorMessage) {
			bool isValid;

			if (string.IsNullOrEmpty(item.KeyBinary)) {
				errorMessage = Langue.error_algorithm_missing_binary;
				isValid = false;
			}
			else {
				item.Binary = DocumentHelper.RetrieveDocument(item.KeyBinary, false);

				try {
					DirectoryInfo directory = GetAlgorithmDirectory(item.UniqueId);
					DeployAlgorithm<TAlgorithmInterface>(item, directory);
					errorMessage = string.Empty;
					isValid = true;
				}
				catch (VizeliaException ve) {
					errorMessage = ve.Message;
					isValid = false;
				}
				catch (Exception e) {
					errorMessage = e.ToString();
					isValid = false;
				}
			}

			return isValid;
		}

		private static bool ValidateEditableAlgorithm<TAlgorithmInterface>(Algorithm item, out string errorMessage) {
			bool isValid;

			if (string.IsNullOrWhiteSpace(item.Code)) {
				errorMessage = Langue.error_algorithm_empty;
				isValid = false;
			}
			else {
				switch (item.Language) {
					case AlgorithmLanguage.CSharp:
						List<string> errors;
						var instance = CreateInstance<TAlgorithmInterface>(item, out errors);

						if (instance != null) {
							instance.Dispose();
						}

						var errorMessageBuilder = new StringBuilder();

						foreach (var error in errors) {
							errorMessageBuilder.AppendLine(error);
						}

						errorMessage = errorMessageBuilder.ToString();
						isValid = (errors.Count == 0);
						break;

					case AlgorithmLanguage.Python:
						isValid = true; //TODO: Validate python Algorithm.
						errorMessage = string.Empty;
						break;


					default:
						throw new ArgumentOutOfRangeException("item.Language");
				}
			}

			return isValid;
		}

		/// <summary>
		/// Clears the value of the password input from a list of inputs.
		/// </summary>
		/// <param name="inputs">The inputs.</param>
		public static void ClearPasswordValue(List<AlgorithmInputValue> inputs) {
			if (inputs == null) return;

			IEnumerable<AlgorithmInputValue> inputValues = inputs.Where(p => p.Name.ToLower().Equals("password"));

			foreach (AlgorithmInputValue inputValue in inputValues) {
				inputValue.Value = string.Empty;
			}
		}

		/// <summary>
		/// Clears the value of the password input, if it is it.
		/// </summary>
		/// <param name="input">The input.</param>
		public static void ClearPasswordValue(AlgorithmInputValue input) {
			if (input != null && input.Name.ToLower().Equals("password")) {
				input.Value = string.Empty;
			}

		}

		/// <summary>
		/// Removes the password value from a store that is being updated.
		/// </summary>
		/// <param name="store">The store.</param>
		public static void RemovePasswordValueForUpdate(CrudStore<AlgorithmInputValue> store) {
			if (store != null && store.update != null) {
				store.update.RemoveAll(v => v.Name.ToLower().Equals("password") && string.IsNullOrEmpty(v.Value));
			}
		}

	}

}

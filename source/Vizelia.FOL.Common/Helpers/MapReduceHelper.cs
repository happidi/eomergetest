﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// MapReduce Helper class.
	/// </summary>
	public static class MapReduceHelper {
		/// <summary>
		/// Starts the specified map.
		/// </summary>
		/// <typeparam name="TInput">The type of the input.</typeparam>
		/// <typeparam name="TPartialResult">The type of the partial result.</typeparam>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <param name="map">The map.</param>
		/// <param name="reduce">The reduce.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		public static Task<TResult> Start<TInput, TPartialResult, TResult>(Func<TInput, TPartialResult> map, Func<TPartialResult[], TResult> reduce, params TInput[] inputs) {
			var mapTasks = CreateMapTasks(map, inputs);
			var reduceTask = CreateReduceTask(reduce, mapTasks);
			return reduceTask;
		}

		/// <summary>
		/// Creates the reduce task.
		/// </summary>
		/// <typeparam name="TPartialResult">The type of the partial result.</typeparam>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <param name="reduce">The reduce.</param>
		/// <param name="mapTasks">The map tasks.</param>
		/// <returns></returns>
		private static Task<TResult> CreateReduceTask<TPartialResult, TResult>(Func<TPartialResult[], TResult> reduce, Task<TPartialResult>[] mapTasks) {
			return Task.Factory.ContinueWhenAll(mapTasks, tasks => PerformReduce(reduce, tasks));
		}

		/// <summary>
		/// Performs the reduce.
		/// </summary>
		/// <typeparam name="TPartialResult">The type of the partial result.</typeparam>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <param name="reduce">The reduce.</param>
		/// <param name="tasks">The tasks.</param>
		/// <returns></returns>
		private static TResult PerformReduce<TPartialResult, TResult>(Func<TPartialResult[], TResult> reduce, Task<TPartialResult>[] tasks) {
			var results = from task in tasks select task.Result;
			return reduce(results.ToArray());
		}

		/// <summary>
		/// Creates the map tasks.
		/// </summary>
		/// <typeparam name="TInput">The type of the input.</typeparam>
		/// <typeparam name="TPartialResult">The type of the partial result.</typeparam>
		/// <param name="map">The map.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		private static Task<TPartialResult>[] CreateMapTasks<TInput, TPartialResult>(Func<TInput, TPartialResult> map, TInput[] inputs) {
			var tasks = new Task<TPartialResult>[inputs.Length];
			
			for (int i = 0; i < inputs.Length; ++i) {
				var input = inputs[i];
				tasks[i] = Helper.StartNewThread(() => map(input));
			}

			return tasks;
		}
	}
}

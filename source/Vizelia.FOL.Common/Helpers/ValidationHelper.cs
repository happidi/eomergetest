﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common.Helpers {
	/// <summary>
	/// A helper class for performing validation tasks.
	/// </summary>
	public class ValidationHelper {
		/// <summary>
		/// Validates the entity.
		/// </summary>
		/// <param name="item">The item.</param>
		public static void Validate<T>(T item) where T : BaseBusinessEntity {
			ValidationResults validationResults = Validation.Validate(item);
			if (!validationResults.IsValid) {
				throw new ArgumentValidationException(validationResults, "item");
			}
		}

		/// <summary>
		/// Throws if the argument is null.
		/// </summary>
		/// <param name="arg">The arg.</param>
		/// <param name="argName">Name of the arg.</param>
		public static void ThrowOnArgumentNull(object arg, string argName) {
			if (arg == null) {
				throw new ArgumentNullException(argName);
			}
		}

		/// <summary>
		/// Throws if the argument is null.
		/// </summary>
		/// <param name="test">The test.</param>
		/// <param name="testFailMessage">The test fail message.</param>
		public static void ThrowOn(Func<bool> test, string testFailMessage) {
			if (!test()) {
				throw new ArgumentException(testFailMessage);
			}
		}
		
	}
}
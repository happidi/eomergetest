﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Vizelia.FOL.Common.Configuration;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A helper class for cryptography tasks.
	/// </summary>
	public static class CryptoHelper {
		/*
		 * In case you were wondering, the Initialization Vector is random and used to salt the first block of encrypted data.
		 * Next blocks are encrypted by previous cipher texts. The IV must be random and can never repeat itself, or an attacker 
		 * may be able to decrypt everything by examining multiple encryptions of the first block.
		 */

		/// <summary>
		/// Encrypts the specified clear text. Returns the encrypted text and outputs the initialization vector.
		/// </summary>
		/// <param name="cleartext">The cleartext.</param>
		/// <param name="initializationVector">The initialization vector.</param>
		public static string Encrypt(string cleartext, out string initializationVector) {
			var encoding = new UTF8Encoding();
			byte[] valueToEncrypt = encoding.GetBytes(cleartext);

			byte[] iv;
			byte[] encryptedContent = Encrypt(valueToEncrypt, out iv);

			initializationVector = Convert.ToBase64String(iv);
			string encryptedString = Convert.ToBase64String(encryptedContent);

			return encryptedString;
		}


		/// <summary>
		/// Decrypts the specified encrypted text using the given initialization vector.
		/// </summary>
		/// <param name="encryptedText">The encrypted text.</param>
		/// <param name="initializationVector">The initialization vector.</param>
		public static string Decrypt(string encryptedText, string initializationVector) {
			byte[] binaryEncryptedContent = Convert.FromBase64String(encryptedText);
			byte[] iv = Convert.FromBase64String(initializationVector);

			byte[] decryptedContent = Decrypt(binaryEncryptedContent, iv);

			var encoding = new UTF8Encoding();
			string decryptedString = encoding.GetString(decryptedContent);

			return decryptedString;
		}

		/// <summary>
		/// Encrypts the specified value. Returns the encrypted value and outputs the initialization vector.
		/// </summary>
		/// <param name="valueToEncrypt">The value to encrypt.</param>
		/// <param name="initializationVector">The initialization vector.</param>
		public static byte[] Encrypt(byte[] valueToEncrypt, out byte[] initializationVector) {
			using (SymmetricAlgorithm provider = GetCryptoProvider()) {
				ICryptoTransform encryptor = provider.CreateEncryptor();

				byte[] encryptedContent = TransformContent(valueToEncrypt, encryptor);
				initializationVector = provider.IV;

				return encryptedContent;
			}
		}

		/// <summary>
		/// Decrypts the specified encrypted content using the given initialization vector.
		/// </summary>
		/// <param name="encryptedContent">Content of the encrypted.</param>
		/// <param name="initializationVector">The initialization vector.</param>
		public static byte[] Decrypt(byte[] encryptedContent, byte[] initializationVector) {
			using (SymmetricAlgorithm provider = GetCryptoProvider()) {
				provider.IV = initializationVector;
				ICryptoTransform decryptor = provider.CreateDecryptor();

				byte[] decryptedContent = TransformContent(encryptedContent, decryptor);

				return decryptedContent;
			}
		}

		/// <summary>
		/// Transforms the content by using the given transformer, to either encrypt or decrypt the content.
		/// </summary>
		/// <param name="content">The content.</param>
		/// <param name="transformer">The transformer.</param>
		/// <returns>The transformed content (encrypted or decrypted).</returns>
		private static byte[] TransformContent(byte[] content, ICryptoTransform transformer) {
			// Define memory stream which will be used to hold encrypted data.
			MemoryStream memoryStream = new MemoryStream();
			// Define cryptographic stream (always use Write mode for encryption).
			CryptoStream cryptoStream = new CryptoStream(memoryStream, transformer, CryptoStreamMode.Write);
			// Start encrypting.
			cryptoStream.Write(content, 0, content.Length);
			// Finish encrypting.
			cryptoStream.FlushFinalBlock();
			// Convert our encrypted data from a memory stream into a byte array.
			byte[] transformedContent = memoryStream.ToArray();
			// Close stream.
			memoryStream.Close();
			return transformedContent;
		}


		/// <summary>
		/// Gets the crypto provider.
		/// </summary>
		/// <returns></returns>
		private static SymmetricAlgorithm GetCryptoProvider() {
			var provider = SymmetricAlgorithm.Create(VizeliaConfiguration.Instance.Cryptography.Algorithm);
			var encoding = new UTF8Encoding();
			var keyBytes = encoding.GetBytes(VizeliaConfiguration.Instance.Cryptography.Key);
			provider.Key = keyBytes;

			return provider;
		}

		/// <summary>
		/// Generate a hash key for a specific text.
		/// </summary>
		/// <param name="text">the text.</param>
		/// <returns></returns>
		public static string CalculateHash(string text) {
			return CalculateHash(text, Encoding.UTF8, "SHA1");
		}

		/// <summary>
		/// Calculates the hash.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="enc">The enconding</param>
		/// <param name="hashType">Type of the hash.</param>
		/// <returns></returns>
		private static string CalculateHash(string text, Encoding enc, string hashType) {
			byte[] buffer = enc.GetBytes(text);
			using (HashAlgorithm hashAlgorithm = HashAlgorithm.Create(hashType)) {
				byte[] hashBuffer = hashAlgorithm.ComputeHash(buffer);
				return ConvertHexByteArrayToString(hashBuffer);
			}
		}


		/// <summary>
		/// Converts the hex byte array to string.
		/// </summary>
		/// <param name="buffer">The buffer.</param>
		/// <returns></returns>
		private static string ConvertHexByteArrayToString(byte[] buffer) {
			char[] result = new char[buffer.Length * 2];

			int i = 0;
			foreach (byte b in buffer) {
				result[i] = GetHexValue(b / 0x10);
				result[i + 1] = GetHexValue(b % 0x10);
				i += 2;
			}
			return new string(result, 0, result.Length);
		}

		/// <summary>
		/// Gets a hex value from an int.
		/// </summary>
		/// <param name="X">The int value.</param>
		/// <returns></returns>
		private static char GetHexValue(int X) {
			return (char)((X < 10) ? (X + 0x30) : ((X - 10) + 0x41));
		}
	}
}
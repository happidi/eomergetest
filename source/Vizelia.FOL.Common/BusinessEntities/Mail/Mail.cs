﻿using System.Net.Mail;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an Mail.
	/// </summary>
	[DataContract]
	[Key("KeyMail")]
	[IfcTypeName("IfcMail")]
	[IconCls("viz-icon-small-mail")]
	[LocalizedText("msg_mail")]
	[AuditableEntityAttribute]
	public class Mail : IfcBaseBusinessEntity, ITreeNode, IMappableEntity {


		/// <summary>
		/// Key of the Mail.
		/// </summary>
		[Information("msg_key", null)]
		[AntiXssValidator]
		[DataMember]
		public string KeyMail { get; set; }


		/// <summary>
		/// The Subject of the Mail.
		/// </summary>
		[Information("msg_mail_subject", null)]
		[NotNullValidator]
		[VizStringLengthValidator(1, 255)]
		[AntiXssValidator]
		[DataMember]
		public string Subject { get; set; }

		/// <summary>
		/// The Description of the Mail.
		/// </summary>
		[Information("msg_name", null)]
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// The Body of the Mail.
		/// </summary>
		//[NotNullValidator(MessageTemplateResourceName="error_ajax", MessageTemplateResourceType=typeof(Langue))]
		[Information("msg_mail_body", null)]
		[NotNullValidator]
		[AntiXssValidator]
		[DataMember]
		public string Body { get; set; }

		/// <summary>
		/// True if the Mail is active, false otherwise.
		/// </summary>
		[Information("msg_isactive", null)]
		[DataMember]
		public bool IsActive { get; set; }


		/// <summary>
		/// The priority of this e-mail message.
		/// </summary>
		[Information("msg_priority", null)]
		[DataMember]
		public MailPriority Priority { get; set; }

		/// <summary>
		/// The from address for this e-mail message.
		/// </summary>
		[Information("msg_mail_from", null)]
		[AntiXssValidator]
		[DataMember]
		public string From { get; set; }

		/// <summary>
		/// The address collection that contains the recipients of this e-mail message.
		/// </summary>
		[Information("msg_mail_to", null)]
		[AntiXssValidator]
		[DataMember]
		public string To { get; set; }

		/// <summary>
		/// The address collection that contains the carbon copy (CC) recipients for this e-mail message.
		/// </summary>
		[Information("msg_mail_cc", null)]
		[AntiXssValidator]
		[DataMember]
		public string CC { get; set; }

		/// <summary>
		/// The address collection that contains the blind carbon copy (BCC) recipients for this e-mail message.
		/// </summary>
		[Information("msg_mail_bcc", null)]
		[AntiXssValidator]
		[DataMember]
		public string Bcc { get; set; }

		/// <summary>
		/// Convert this Building into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				Key = this.KeyMail,
				text = this.Subject,
				leaf = false,
				entity = this,
				qtip = this.Name,
				iconCls = this.iconCls
			};
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		[Information("msg_localid", null)]
		public string LocalId {
			get { return Name; }
			set { }
		}

		/// <summary>
		/// The key of the classification of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItem { get; set; }

		/// <summary>
		/// The key path of the classification of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItemPath { get; set; }

		/// <summary>
		/// The classification title of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemTitle { get; set; }

		/// <summary>
		/// The classification level of the Meter.
		/// </summary>
		[DataMember]
		public int ClassificationItemLevel { get; set; }

		/// <summary>
		/// The classification path of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLongPath { get; set; }

		/// <summary>
		/// The classification local id path of the Meter.
		/// </summary>
		[Information("msg_mail_classification", null)]
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLocalIdPath { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorR { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorG { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorB { get; set; }
	}
}

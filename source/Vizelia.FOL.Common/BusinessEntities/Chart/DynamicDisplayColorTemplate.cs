﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity representing a Dynamic display color template.
	/// </summary>
	[Serializable]
	[Key("KeyDynamicDisplayColorTemplate")]
	[DataContract]
	public class DynamicDisplayColorTemplate : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Key of the Dynamic Display Color template.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyDynamicDisplayColorTemplate { get; set; }

		/// <summary>
		/// Name of the Dynamic Display Color Template.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Red color component of the StartColor.
		/// </summary>
		[DataMember]
		public int StartColorR { get; set; }

		/// <summary>
		/// Green color component of the StartColor.
		/// </summary>
		[DataMember]
		public int StartColorG { get; set; }

		/// <summary>
		/// Blue color component of the StartColor.
		/// </summary>
		[DataMember]
		public int StartColorB { get; set; }

		/// <summary>
		/// Red color component of the EndColor.
		/// </summary>
		[DataMember]
		public int EndColorR { get; set; }

		/// <summary>
		/// Green color component of the EndColor.
		/// </summary>
		[DataMember]
		public int EndColorG { get; set; }

		/// <summary>
		/// Blue color component of the EndColor.
		/// </summary>
		[DataMember]
		public int EndColorB { get; set; }

		/// <summary>
		/// Red color component of the BorderColor.
		/// </summary>
		[DataMember]
		public int BorderColorR { get; set; }

		/// <summary>
		/// Green color component of the BorderColor.
		/// </summary>
		[DataMember]
		public int BorderColorG { get; set; }

		/// <summary>
		/// Blue color component of the BorderColor.
		/// </summary>
		[DataMember]
		public int BorderColorB { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [use predefined color].
		/// </summary>
		/// <value>
		///   <c>true</c> if [use predefined color]; otherwise, <c>false</c>.
		/// </value>
		public bool UsePredefinedColor { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }
	}
}

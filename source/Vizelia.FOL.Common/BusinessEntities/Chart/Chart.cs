﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Chart.
	/// </summary>
	[Serializable]
	[Key("KeyChart")]
	[DataContract]
	[IconCls("viz-icon-small-chart")]
	[LocalizedText("msg_chart")]
	[AuditableEntityAttribute]
	public class Chart : BaseBusinessEntity, ISecurableObject, ITreeNode, IMappableEntity, IPortlet {

		private List<TimeSpan> m_TimeZoneOffset = new List<TimeSpan> { new TimeSpan(0, 0, -1), new TimeSpan(0, 0, -1), new TimeSpan(0, 0, -1) };

        /// <summary>
        /// The const_max_meter_default_value
        /// </summary>
		public const int const_max_meter_default_value = 100;

		/// <summary>
		/// Contructor
		/// </summary>
		public Chart() {
			OpenOnStartup = false;
			StartDate = DateTime.UtcNow;
			EndDate = DateTime.UtcNow;
			TimeInterval = AxisTimeInterval.Months;
			Localisation = ChartLocalisation.ByMeter;
			Axis = new Dictionary<string, ChartAxis>();
			Series = new DataSerieCollection();
			Errors = new List<string>();
			Warnings = new List<string>();
			CalculatedSeries = new List<CalculatedSerie>();
			ChartType = ChartType.Combo;
			DataSerieType = DataSerieType.Column;
			Display3D = false;
			DisplayLineThickness = 8;
			DisplayMarkerSize = 5;
			DisplayShaddingEffect = ChartShaddingEffect.One;
			DisplayStartEndDateFontSize = 7;
			DisplayStartEndDate = false;
			DisplayTransparency = 30;
			DisplayValues = false;
			DisplayLegendFontSize = 9;
			DisplayLegendVisible = true;
			DisplayStackSeries = true;
			DisplayFontSize = 9;
			DisplayFontFamily = ChartHelper.GetDefaultFontFamily();
			DisplayDecimalPrecision = 2;
			DisplayClassifications = true;
			DynamicTimeScaleFrequency = 1;
			DynamicTimeScaleEnabled = true;
			DynamicTimeScaleInterval = AxisTimeInterval.Years;
			DynamicTimeScaleCalendar = true;
			DynamicTimeScaleCompleteInterval = false;
			DynamicTimeScaleCalendarEndToNow = false;
			UseDynamicDisplay = false;
			LockFilterSpatial = false;
			ClassificationLevel = 2;
			IsEnergyAggregatorLoadingMeterData = false;
			DisplayChartAreaBackgroundStart = ColorHelper.GetHTMLColor(ChartHelper.GetDefaultBackgroundStartColor(), false);
			DisplayChartAreaBackgroundEnd = ColorHelper.GetHTMLColor(ChartHelper.GetDefaultBackgroundEndColor(), false);
			CustomGridColumnCount = 5;
			CustomGridRowCount = 5;
			LimitSeriesNumber = ChartLimitSeriesNumber.DisplayAll;
			IsFavorite = false;
			MaxDataPointPerDataSerie = 500;
			UseSpatialPath = true;
			HideFullSpatialPath = true;
			UseClassificationPath = true;
			IsKPI = false;
			DrillDownMode = ChartDrillDownMode.Autonomous;
			CalendarViewTimeInterval = AxisTimeInterval.Days;
			CalendarViewSelections = new List<ChartCalendarViewSelection>();
			LegendPosition = ChartLegendPosition.ChartTitle;
			InfoIconPosition = ChartInfoIconPosition.BottomRight;
			Algorithms = new List<ChartAlgorithm>();
			ScriptAlarmInstances = new List<AlarmInstance>();
			DisplayLegendPlain = false;
			XAxisStartDayOfWeek = 1;
			Palette = PaletteHelper.GeneratePalette();
			MaxMeter = const_max_meter_default_value;
			HideDatagridInExport = false;
			GridAggregateByCalculatedSerieClassification = true;
			DataSeries = new List<DataSerie>();
			EventLogs = new List<EventLog>();
			StatisticalSeries = new List<StatisticalSerie>();
			FilterSpatial = new List<Location>();
			FilterSpatialPset = new List<FilterSpatialPset>();
			MetersClassifications = new List<ClassificationItem>();
			AlarmDefinitionsClassifications = new List<ClassificationItem>();
			EventLogClassifications = new List<ClassificationItem>();
			HistoricalAnalysisDefinitions = new List<ChartHistoricalAnalysis>();
			CorrelationRemovedPoints = new List<ChartCorrelationRemovedDataPoint>();
			HistoricalPsets = new List<ChartPsetAttributeHistorical>();
			ChartCustomGridCells = new List<ChartCustomGridCell>();
			AlarmDefinitions = new List<AlarmDefinition>();
			Roles = new List<AuthorizationItem>();
			Meters = new List<Meter>();
			ChartMarkers = new List<ChartMarker>();
			ResultingMeters = new List<Meter>();
			DetailedStatusIconEnabled = true;
		}


		/// <summary>
		/// Gets the application.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Application { get; set; }

		/// <summary>
		/// Key of the Chart.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(Chart), false)]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyChart { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		[Information("msg_title", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		[Information("msg_description", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string Description { get; set; }

		/// <summary>
		/// private startdate attribute
		/// </summary>
		private DateTime m_StartDate;

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		[Information("msg_chart_startdate", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public DateTime StartDate {
			get { return DynamicTimeScaleEnabled ? ChartHelper.GetDynamicStartDate(this) : m_StartDate; }
			set { m_StartDate = value; }
		}

		/// <summary>
		/// private enddate attribute
		/// </summary>
		private DateTime m_EndDate;

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		[Information("msg_chart_enddate", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public DateTime EndDate {
			get { return DynamicTimeScaleEnabled ? ChartHelper.GetDynamicEndDate(this) : m_EndDate; }
			set { m_EndDate = value; }
		}

		/// <summary>
		/// Gets or sets the Key of the CalendarEventCategory associated to this Chart.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[BatchUpdatable(true)]
		public string KeyCalendarEventCategory { get; set; }

		/// <summary>
		/// Gets or sets the CalendarEventCategory associated to this Chart.
		/// </summary>
		[Information("msg_calendar", null)]
		[DataMember]
		public CalendarEventCategory CalendarEventCategory { get; set; }

		/// <summary>
		/// Calendar mode : Include or Exclude.
		/// </summary>
		[Information("msg_chart_calendarmode", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public ChartCalendarMode CalendarMode { get; set; }

		///// <summary>
		///// Gets or sets the meters.
		///// </summary>
		//[DataMember]
		//public List<Meter> Meters { get; set; }

		/// <summary>
		/// Gets or sets the meters count.
		/// </summary>
		/// <value>
		/// The meters count.
		/// </value>
		[DataMember]
		public int MetersCount { get; set; }

		/// <summary>
		/// Gets or sets the series.
		/// </summary>
		[DataMember]
		public DataSerieCollection Series { get; set; }

		/// <summary>
		/// Gets or sets the data series.
		/// </summary>
		[DataMember]
		public virtual List<DataSerie> DataSeries { get; set; }

		/// <summary>
		/// Gets or sets the EventLogs.
		/// </summary>
		[DataMember]
		public List<EventLog> EventLogs { get; set; }

		/// <summary>
		/// Gets or sets the AlarmInstances.
		/// </summary>
		[DataMember]
		public List<AlarmInstanceAsDataSource> AlarmInstances { get; set; }

		/// <summary>
		/// Gets or sets the list of CalculatedSeries.
		/// </summary>
		[DataMember]
		public List<CalculatedSerie> CalculatedSeries { get; set; }

		/// <summary>
		/// Gets or sets the list of StatisticalSeries.
		/// </summary>
		[DataMember]
		public List<StatisticalSerie> StatisticalSeries { get; set; }

		/// <summary>
		/// Gets or sets the list of extra axis.
		/// </summary>
		[DataMember]
		public Dictionary<string, ChartAxis> Axis { get; set; }

		/// <summary>
		/// Gets or sets the list of extra axis.
		/// </summary>
		[DataMember]
		public List<ChartAxis> ChartAxis { get; set; }

		/// <summary>
		/// Gets or sets the X axis format. ie : dd/MM/YY, HH:ss
		/// </summary>
		/// <value>
		/// The X axis format.
		/// </value>
		[Information("msg_chart_xaxisformat", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public string XAxisFormat { get; set; }


		/// <summary>
		/// YAxis FullStacked.
		/// </summary>
		[Information("msg_chart_yaxisfullstacked", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public bool YAxisFullStacked { get; set; }

		/// <summary>
		/// Gets or sets the chart type
		/// </summary>
		[Information("msg_chart_type", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public ChartType ChartType { get; set; }

		/// <summary>
		/// Gets or sets the chart default dataserie type
		/// </summary>
		[Information("msg_dataserie_type", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public DataSerieType DataSerieType { get; set; }


		/// <summary>
		/// Gets or sets the chart default dataserie type
		/// </summary>
		[Information("msg_gauge_type", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public GaugeType GaugeType { get; set; }

		/// <summary>
		/// Gets or sets the chart default timeinterval
		/// </summary>
		[Information("msg_chart_timeinterval", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public AxisTimeInterval TimeInterval { get; set; }

		/// <summary>
		/// Gets or sets the chart localisation
		/// </summary>
		[Information("msg_chart_localisation", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public ChartLocalisation Localisation { get; set; }

		/// <summary>
		/// Gets the name of the localisation spatial typename (IfcSite, IfcBuilding...).
		/// </summary>
		[DataMember]
		public HierarchySpatialTypeName? LocalisationSpatialTypeName {
			get {
				switch (Localisation) {
					case ChartLocalisation.ByBuilding:
						return HierarchySpatialTypeName.IfcBuilding;

					case ChartLocalisation.ByFloor:
						return HierarchySpatialTypeName.IfcBuildingStorey;

					case ChartLocalisation.BySite:
						return HierarchySpatialTypeName.IfcSite;

					case ChartLocalisation.BySpace:
						return HierarchySpatialTypeName.IfcSpace;

					case ChartLocalisation.ByFurniture:
						return HierarchySpatialTypeName.IfcFurniture;
				}

				return null;
			}
			private set { }
		}


		/// <summary>
		/// Gets or sets the chart localisation site level
		/// </summary>
		[Information("msg_chart_localisationsitelevel", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public int LocalisationSiteLevel { get; set; }

		/// <summary>
		/// Gets or sets the chart classification level
		/// </summary>
		[Information("msg_chart_classificationlevel", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public int ClassificationLevel { get; set; }


		/// <summary>
		/// Gets or sets if the Chart is displayed in 3D
		/// </summary>
		[Information("msg_chart_display_3d", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public bool Display3D { get; set; }

		/// <summary>
		/// Get or sets if values are written as text on the Chart
		/// </summary>
		[Information("msg_chart_display_values", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public bool DisplayValues { get; set; }

		/// <summary>
		/// Gets or sets if startdate and enddate are written as text on the Chart
		/// </summary>
		[Information("msg_chart_display_startdateenddate", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public bool DisplayStartEndDate { get; set; }

		/// <summary>
		/// Gets or sets the font size of the startdate and enddate text
		/// </summary>
		[Information("msg_chart_display_startdateenddate_fontsize", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public int DisplayStartEndDateFontSize { get; set; }

		/// <summary>
		/// Gets or sets the default marker size
		/// </summary>
		[Information("msg_chart_display_markersize", null)]
		[DataMember]
		[RangeValidator(0, RangeBoundaryType.Inclusive, 200, RangeBoundaryType.Inclusive)]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public int DisplayMarkerSize { get; set; }

		/// <summary>
		/// Gets or sets the default line thickness size
		/// </summary>
		[Information("msg_chart_display_line_thickness", null)]
		[DataMember]
		[RangeValidator(0, RangeBoundaryType.Inclusive, 200, RangeBoundaryType.Inclusive)]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public int DisplayLineThickness { get; set; }

		/// <summary>
		/// Gets or sets the shadding effect apply on the chart
		/// </summary>
		[Information("msg_chart_display_line_thickness", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public ChartShaddingEffect DisplayShaddingEffect { get; set; }

		/// <summary>
		/// Gets or sets the default transparency
		/// </summary>
		[Information("msg_chart_display_line_thickness", null)]
		[DataMember]
		[RangeValidator(0, RangeBoundaryType.Inclusive, 100, RangeBoundaryType.Inclusive)]
		[BatchUpdatable(true)]
		public int DisplayTransparency { get; set; }

		/// <summary>
		/// Gets or sets if DataSeries are stack on the XAxis
		/// </summary>
		[Information("msg_chart_display_line_thickness", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public bool DisplayStackSeries { get; set; }

		/// <summary>
		/// Gets or sets the default font size
		/// </summary>
		[Information("msg_chart_display_line_thickness", null)]
		[DataMember]
		[RangeValidator(1, RangeBoundaryType.Inclusive, 200, RangeBoundaryType.Inclusive)]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public int DisplayFontSize { get; set; }

		/// <summary>
		/// Gets or sets the default font family
		/// </summary>
		[Information("msg_chart_display_line_thickness", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public string DisplayFontFamily { get; set; }

		/// <summary>
		/// Gets or sets the default decimal precision
		/// </summary>
		[Information("msg_chart_display_line_thickness", null)]
		[DataMember]
		[RangeValidator(0, RangeBoundaryType.Inclusive, 5, RangeBoundaryType.Inclusive)]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public int DisplayDecimalPrecision { get; set; }

		/// <summary>
		/// Gets or sets the legend visibility
		/// </summary>
		[Information("msg_chart_display_line_thickness", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public bool DisplayLegendVisible { get; set; }

		/// <summary>
		/// Gets or sets the legend font size
		/// </summary>
		[Information("msg_chart_display_line_thickness", null)]
		[DataMember]
		[RangeValidator(1, RangeBoundaryType.Inclusive, 200, RangeBoundaryType.Inclusive)]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public int DisplayLegendFontSize { get; set; }

		/// <summary>
		/// Gets or sets the X Axis font size
		/// </summary>
		[Information("msg_chart_display_line_thickness", null)]
		[DataMember]
		[ChartDisplayOnly]
		//[RangeValidator(1, RangeBoundaryType.Inclusive, 200, RangeBoundaryType.Inclusive)]
		[BatchUpdatable(true)]
		public int? DisplayXAxisFontSize { get; set; }

		/// <summary>
		/// Get or sets the Label Truncation Length (for axis).
		/// </summary>
		[Information("msg_chart_display_line_thickness", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public int? DisplayLabelTruncationLength { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the chart should display classifications or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if the chart should display classifications; otherwise, <c>false</c>.
		/// </value>
		[Information("msg_chart_display_classifications", null)]
		[DataMember]
		[ChartDynamicDisplay]
		[BatchUpdatable(true)]
		public bool DisplayClassifications { get; set; }

		/// <summary>
		/// Gets or sets the display chart area background start.
		/// </summary>
		/// <value>
		/// The display  background color.
		/// </value>
		[Information("msg_chart_display_line_thickness", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public string DisplayBackground { get; set; }

		/// <summary>
		/// Gets or sets the display chart area background start.
		/// </summary>
		/// <value>
		/// The display chart area background start.
		/// </value>
		[Information("msg_chart_display_line_thickness", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public string DisplayChartAreaBackgroundStart { get; set; }

		/// <summary>
		/// Gets or sets the display chart area background end.
		/// </summary>
		/// <value>
		/// The display chart area background end.
		/// </value>
		[Information("msg_chart_display_line_thickness", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public string DisplayChartAreaBackgroundEnd { get; set; }

		/// <summary>
		/// Gets or sets the display legend sort.
		/// </summary>
		/// <value>
		/// The display legend sort.
		/// </value>
		[Information("msg_chart_display_line_thickness", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public ChartLegendSort DisplayLegendSort { get; set; }

		/// <summary>
		/// Gets or sets if the chart display mode.
		/// </summary>
		[Information("msg_chart_displaymode", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public ChartDisplayMode DisplayMode { get; set; }

		/// <summary>
		/// Return the numerical format to display values
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string NumericFormat {
			get { return "N" + DisplayDecimalPrecision.ToString(CultureInfo.InvariantCulture); }
			private set { }
		}

		/// <summary>
		/// Return the time interval format
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string TimeIntervalFormat {
			get {
				if (!string.IsNullOrEmpty(XAxisFormat))
					return XAxisFormat;
				return TimeInterval.GetFormat();
			}
			private set { }
		}

		/// <summary>
		/// Get or sets if the dynamic timescale is enabled
		/// </summary>
		[Information("msg_chart_dynamic_timescale", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public bool DynamicTimeScaleEnabled { get; set; }


		/// <summary>
		/// Get or sets the number of 'time interval' to select (the last 3 months ...)
		/// </summary>
		[Information("msg_chart_dynamic_timescale_uselast", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public int DynamicTimeScaleFrequency { get; set; }

		/// <summary>
		/// Get or sets the dynamic timescam interval : Month, Days, Hours...
		/// </summary>
		[Information("msg_chart_dynamic_timescale_interval", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public AxisTimeInterval DynamicTimeScaleInterval { get; set; }

		/// <summary>
		/// Get or sets if we use the start at the beginning of the month or if we use a time period.
		/// </summary>
		[Information("msg_chart_dynamic_timescale_calendar", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public bool DynamicTimeScaleCalendar { get; set; }


		/// <summary>
		/// Get or sets if we use the end at the end of the month or if we use now as the end date.
		/// </summary>
		[Information("msg_chart_dynamic_timescale_calendarendtonow", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public bool DynamicTimeScaleCalendarEndToNow { get; set; }


		/// <summary>
		/// Get or sets if we use we complete intervals only (i.e until the end of last month, last day...).
		/// Affect the end date calculation.
		/// </summary>
		[Information("msg_chart_dynamic_timescale_completeinterval", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public bool DynamicTimeScaleCompleteInterval { get; set; }


		/// <summary>
		/// Get or sets the time the night starts for the X Markers.
		/// </summary>
		[Information("msg_chart_night_starttime", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public string NightStartTime { get; set; }

		/// <summary>
		/// Get or sets the time the night ends for the X Markers.
		/// </summary>
		[Information("msg_chart_night_endtime", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public string NightEndTime { get; set; }

		/// <summary>
		/// Gets the color of the night.
		/// </summary>
		/// <value>
		/// The color of the night.
		/// </value>
		[Information("msg_color", null)]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public string NightColor {
			get { return string.Format("{0}{1}{2}", NightColorR, NightColorG, NightColorB); }
		}

		/// <summary>
		/// Red color composant of the night marker.
		/// </summary>
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public int? NightColorR { get; set; }

		/// <summary>
		/// Green color composant of the night marker.
		/// </summary>
		[DataMember]
		[ChartDisplayOnly]
		public int? NightColorG { get; set; }

		/// <summary>
		/// Blue color composant of the night marker.
		/// </summary>
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public int? NightColorB { get; set; }

		/// <summary>
		/// Key of the DynamicDisplay the Chart is associated with.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ChartDynamicDisplay]
		[BatchUpdatable(true)]
		public string KeyDynamicDisplay { get; set; }

		/// <summary>
		/// True to render the chart with the DynamicDisplay background. False to ignore it.
		/// </summary>
		[Information("msg_chart_use_dynamicdisplay", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool UseDynamicDisplay { get; set; }

		/// <summary>
		/// The DynamicDisplay associated with this chart.
		/// </summary>
		[DataMember]
		[ChartDynamicDisplay]
		[BatchUpdatable(true)]
		public DynamicDisplay DynamicDisplay { get; set; }

		/// <summary>
		/// True to prevent the LocationFilters to be modified from the PortalTab, False otherwise.
		/// </summary>
		[Information("msg_chart_lockfilterspatial", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool LockFilterSpatial { get; set; }

		/// <summary>
		/// Gets or sets the filter spatial.
		/// </summary>
		[DataMember]
		public List<Location> FilterSpatial { get; set; }

		/// <summary>
		/// Gets or sets the filter spatial pset.
		/// </summary>
		[DataMember]
		public List<FilterSpatialPset> FilterSpatialPset { get; set; }

		/// <summary>
		/// Gets or sets the meters classifications.
		/// </summary>
		[DataMember]
		public List<ClassificationItem> MetersClassifications { get; set; }

		/// <summary>
		/// Gets or sets the alarm definitions classifications.
		/// </summary>
		[DataMember]
		public List<ClassificationItem> AlarmDefinitionsClassifications { get; set; }

		/// <summary>
		/// Gets or sets the event log classifications.
		/// </summary>
		[DataMember]
		public List<ClassificationItem> EventLogClassifications { get; set; }

		/// <summary>
		/// List of error messages generated during the rendering of the Chart.
		/// </summary>
		[AntiXssListValidator]
		[DataMember]
		public List<string> Errors { get; set; }

		/// <summary>
		/// List of warnings messages generated during the rendering of the Chart.
		/// </summary>
		[AntiXssListValidator]
		[DataMember]
		public List<string> Warnings { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether the detailed status icon is enabled.
		/// </summary>
		[DataMember]
		public bool DetailedStatusIconEnabled{ get; set; }

		/// <summary>
		/// The VirtualFile of the Chart.
		/// </summary>
		[DataMember]
		public VirtualFile VirtualFile { get; set; }


		/// <summary>
		/// Gets or sets the data grid percentage options.
		/// </summary>
		/// <value>
		/// The data grid percentage options.
		/// </value>
		[Information("msg_chart_datagridpercentage", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public ChartDataGridPercentage DataGridPercentage { get; set; }

		/// <summary>
		/// Gets or sets the historical analysis definitions.
		/// </summary>
		/// <value>
		/// The historical analysis definitions.
		/// </value>
		[DataMember]
		public List<ChartHistoricalAnalysis> HistoricalAnalysisDefinitions { get; set; }

		/// <summary>
		/// The Dictionary of ChartHistoricalAnalysis and  Charts generated by the ChartHistoricalAnalysis associated with this Chart.
		/// </summary>
		[DataMember]
		public Dictionary<ChartHistoricalAnalysis, DataSerieCollection> HistoricalAnalysis { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }


		/// <summary>
		/// Gets or sets the key palette.
		/// </summary>
		/// <value>
		/// The key palette.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public string KeyPalette { get; set; }

		/// <summary>
		/// Gets or sets the palette.
		/// </summary>
		/// <value>
		/// The palette.
		/// </value>
		[DataMember]
		public Palette Palette { get; set; }

		/// <summary>
		/// The chart specific analysis
		/// </summary>
		/// <value>
		/// The specific analysis.
		/// </value>
		[Information("msg_chart_specificanalysis", null)]
		[DataMember]
		public ChartSpecificAnalysis SpecificAnalysis { get; set; }


		/// <summary>
		/// Gets or sets the color of the heat map min.
		/// </summary>
		/// <value>
		/// The color of the heat map min.
		/// </value>
		[Information("msg_chart_heatmapmincolor", null)]
		[AntiXssValidator]
		[DataMember]
		public string HeatMapMinColor { get; set; }

		/// <summary>
		/// Gets or sets the color of the heat map max.
		/// </summary>
		/// <value>
		/// The color of the heat map max.
		/// </value>
		[Information("msg_chart_heatmapmaxcolor", null)]
		[AntiXssValidator]
		[DataMember]
		public string HeatMapMaxColor { get; set; }


		/// <summary>
		/// Gets or sets the correlation X axis serie local id.
		/// </summary>
		/// <value>
		/// The correlation X axis serie local id.
		/// </value>
		[Information("msg_chart_correlationxaxisserie", null)]
		[AntiXssValidator]
		[DataMember]
		public string CorrelationXSerieLocalId { get; set; }


		/// <summary>
		/// Gets or sets the correlation Y axis serie local id.
		/// </summary>
		/// <value>
		/// The correlation Y axis serie local id.
		/// </value>
		[Information("msg_chart_correlationyaxisserie", null)]
		[AntiXssValidator]
		[DataMember]
		public string CorrelationYSerieLocalId { get; set; }


		/// <summary>
		/// Gets or sets the correlation X axis serie local id 2.
		/// </summary>
		/// <value>
		/// The correlation X axis serie local id.
		/// </value>
		[Information("msg_chart_correlationxaxisserie", null)]
		[AntiXssValidator]
		[DataMember]
		public string CorrelationXSerieLocalId2 { get; set; }


		/// <summary>
		/// Gets or sets the correlation Y axis serie local id 2.
		/// </summary>
		/// <value>
		/// The correlation Y axis serie local id.
		/// </value>
		[Information("msg_chart_correlationyaxisserie", null)]
		[AntiXssValidator]
		[DataMember]
		public string CorrelationYSerieLocalId2 { get; set; }

		/// <summary>
		/// Gets or sets the difference highlight serie1 local id.
		/// </summary>
		/// <value>
		/// The difference highlight serie1 local id.
		/// </value>
		[Information("msg_chart_differencehighlightserie1", null)]
		[AntiXssValidator]
		[DataMember]
		public string DifferenceHighlightSerie1LocalId { get; set; }


		/// <summary>
		/// Gets or sets the difference highlight serie2 local id.
		/// </summary>
		/// <value>
		/// The difference highlight serie2 local id.
		/// </value>
		[Information("msg_chart_differencehighlightserie2", null)]
		[AntiXssValidator]
		[DataMember]
		public string DifferenceHighlightSerie2LocalId { get; set; }


		/// <summary>
		/// Gets or sets the difference highlight serie2 local id.
		/// </summary>
		/// <value>
		/// The difference highlight serie2 local id.
		/// </value>
		[Information("msg_chart_differencehighlightthreshold", null)]
		[DataMember]
		public int DifferenceHighlightSerieThreshold { get; set; }


		/// <summary>
		/// Gets or sets the key energy certificate.
		/// </summary>
		/// <value>
		/// The key energy certificate.
		/// </value>
		[Information("msg_energycertificate", null)]
		[AntiXssValidator]
		[DataMember]
		public string KeyEnergyCertificate { get; set; }


		/// <summary>
		/// Gets or sets the energy certificate.
		/// </summary>
		/// <value>
		/// The energy certificate.
		/// </value>
		[DataMember]
		public EnergyCertificate EnergyCertificate { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether we display each element in the Energy certificate view..
		/// </summary>
		[Information("msg_chart_energycertificate_displayelements", null)]
		[DataMember]
		public bool EnergyCertificateDisplayElements { get; set; }

		/// <summary>
		/// Gets or sets the energy certificate target.
		/// </summary>
		/// <value>
		/// The energy certificate target.
		/// </value>
		[Information("msg_chart_energycertificate_target", null)]
		[DataMember]
		public double? EnergyCertificateTarget { get; set; }

		/// <summary>
		/// Gets or sets the correlation removed points.
		/// </summary>
		/// <value>
		/// The correlation removed points.
		/// </value>
		[DataMember]
		public List<ChartCorrelationRemovedDataPoint> CorrelationRemovedPoints { get; set; }

		/// <summary>
		/// Gets or sets the micro chart target.
		/// </summary>
		/// <value>
		/// The micro chart target.
		/// </value>
		[Information("msg_chart_microcharttarget", null)]
		[DataMember]
		public double? MicroChartTarget { get; set; }


		/// <summary>
		/// Gets or sets the micro chart target element.
		/// </summary>
		/// <value>
		/// The micro chart target element.
		/// </value>
		[Information("msg_chart_microcharttargetelement", null)]
		[DataMember]
		public double? MicroChartTargetElement { get; set; }

		/// <summary>
		/// Gets or sets if we display elements in the microchart or just the serie projection.
		/// </summary>
		[Information("msg_chart_microchart_displayelements", null)]
		[DataMember]
		public bool MicroChartDisplayElements { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the energy aggregator instances are loading meter data.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is energy aggregator loading meter data; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsEnergyAggregatorLoadingMeterData { get; set; }

		/// <summary>
		/// Gets or sets the time zone id in which we want to render the chart.
		/// </summary>
		/// <value>
		/// The time zone id.
		/// </value>
		[Information("msg_chart_timezone", null)]
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string TimeZoneId { get; set; }

		/// <summary>
		/// Gets or sets the time zone offset.
		/// </summary>
		/// <value>
		/// The time zone offset.
		/// </value>
		[DataMember]
		public List<TimeSpan> TimeZoneOffset {
			get {
				if ((m_TimeZoneOffset == null) || (m_TimeZoneOffset.Count <= 0))
					m_TimeZoneOffset = new List<TimeSpan> { new TimeSpan(0, 0, -1), new TimeSpan(0, 0, -1), new TimeSpan(0, 0, -1) };
				if (m_TimeZoneOffset[0] == new TimeSpan(0, 0, -1)) {
					m_TimeZoneOffset[0] = GetTimeZone().GetUtcOffset(StartDate.ToLocalTime());
					m_TimeZoneOffset[1] = GetTimeZone().GetUtcOffset(StartDate.ToLocalTime());
					return m_TimeZoneOffset;
				}
				else {
					return m_TimeZoneOffset;
				}
			}
			set { m_TimeZoneOffset = value; }
		}

		/// <summary>
		/// Gets or sets the historical psets.
		/// </summary>
		/// <value>
		/// The historical psets.
		/// </value>
		[DataMember]
		public List<ChartPsetAttributeHistorical> HistoricalPsets { get; set; }

		/// <summary>
		/// Gets the time zone.
		/// </summary>
		/// <returns></returns>
		public TimeZoneInfo GetTimeZone() {
			var retVal = CalendarHelper.GetTimeZoneFromId(TimeZoneId);
			return retVal;
		}

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity {
				KeySecurable = KeyChart,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(GetType()),
				SecurableName = Title,
				SecurableTypeName = GetType().FullName
			};
		}

		/// <summary>
		/// Convert this Chart into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				id = KeyChart,
				Key = KeyChart,
				text = Title,
				leaf = true,
				entity = this,
				qtip = Description,
				iconCls =
					IsKPI
						? "viz-icon-small-chart-kpi"
						: EnumExtension.GetEnumValueIconCls(typeof(ChartType), ChartType.ParseAsInt())
			};
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>

		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the dynamic display main text.
		/// </summary>
		/// <value>
		/// The dynamic display main text.
		/// </value>
		[Information("msg_dynamicdisplay_maintext", null)]
		[AntiXssValidator]
		[ViewableByDataModel]
		[DataMember]
		[ChartDisplayOnly]
		[ChartDynamicDisplay]
		[BatchUpdatable(true)]
		public string DynamicDisplayMainText { get; set; }

		/// <summary>
		/// Gets or sets the dynamic display header text.
		/// </summary>
		/// <value>
		/// The dynamic display header text.
		/// </value>
		[Information("msg_dynamicdisplay_headertext", null)]
		[AntiXssValidator]
		[ViewableByDataModel]
		[DataMember]
		[ChartDisplayOnly]
		[ChartDynamicDisplay]
		[BatchUpdatable(true)]
		public string DynamicDisplayHeaderText { get; set; }

		/// <summary>
		/// Gets or sets the dynamic display footer text.
		/// </summary>
		/// <value>
		/// The dynamic display footer text.
		/// </value>
		[Information("msg_dynamicdisplay_footertext", null)]
		[AntiXssValidator]
		[ViewableByDataModel]
		[DataMember]
		[ChartDisplayOnly]
		[ChartDynamicDisplay]
		[BatchUpdatable(true)]
		public string DynamicDisplayFooterText { get; set; }

		/// <summary>
		/// Gets or sets the chart custom grid cells.
		/// </summary>
		/// <value>
		/// The chart custom grid cells.
		/// </value>
		[DataMember]
		public List<ChartCustomGridCell> ChartCustomGridCells { get; set; }

		/// <summary>
		/// Gets or sets the custom grid row count.
		/// </summary>
		/// <value>
		/// The custom grid row count.
		/// </value>
		[Information("msg_chart_customgridrowcount", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public int CustomGridRowCount { get; set; }

		/// <summary>
		/// Gets or sets the custom grid column count.
		/// </summary>
		/// <value>
		/// The custom grid column count.
		/// </value>
		[Information("msg_chart_customgridcolumncount", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public int CustomGridColumnCount { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [custom grid hide lines].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [custom grid hide lines]; otherwise, <c>false</c>.
		/// </value>
		[Information("msg_chart_customgridhidelines", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public bool CustomGridHideLines { get; set; }


		/// <summary>
		/// The Key of the DynamicDisplayImage to display in the Chart Area Background.
		/// </summary>
		[Information("msg_chart_areapicture", null)]
		[AntiXssValidator]
		[DataMember]
		[BatchUpdatable(true)]
		public string ChartAreaPicture { get; set; }

		/// <summary>
		/// The DynamicDisplayImage entity to display in the chart area background.
		/// </summary>
		[DataMember]
		public DynamicDisplayImage ChartAreaPictureImage { get; set; }

		/// <summary>
		/// Gets or sets the max number of series to display.
		/// </summary>
		/// <value>
		/// The limit series number.
		/// </value>
		[Information("msg_chart_limitseriesnumber", null)]
		[DataMember]
		public ChartLimitSeriesNumber LimitSeriesNumber { get; set; }


		/// <summary>
		/// Gets or sets the alarm definitions associated to this Chart.
		/// </summary>
		/// <value>
		/// The alarm definitions.
		/// </value>
		[DataMember]
		public List<AlarmDefinition> AlarmDefinitions { get; set; }


		/// <summary>
		/// Gets or sets the calendar view time interval.
		/// </summary>
		/// <value>
		/// The calendar view time interval.
		/// </value>
		[Information("msg_chart_timeinterval", null)]
		[DataMember]
		public AxisTimeInterval CalendarViewTimeInterval { get; set; }


		/// <summary>
		/// Gets or sets the max data point per data serie.
		/// </summary>
		/// <value>
		/// The max data point per data serie.
		/// </value>
		[Information("msg_chart_maxdatapointsperdataserie", null)]
		[DataMember]
		[RangeValidator(1, RangeBoundaryType.Inclusive, 5000, RangeBoundaryType.Inclusive)]
		public int MaxDataPointPerDataSerie { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether the grid representation of the Chart is transposed (DateTime in columns) or not.
		/// </summary>
		/// <value>
		///   <c>true</c> to transpose the Grid,  otherwise, <c>false</c>.
		/// </value>
		[Information("msg_chart_gridtranspose", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public bool GridTranspose { get; set; }

		/// <summary>
		/// Sets the title.
		/// </summary>
		/// <param name="title">The title.</param>
		public void SetTitle(string title) {
			Title = title;
		}

		/// <summary>
		/// Gets the title.
		/// </summary>
		/// <returns></returns>
		public string GetTitle() {
			return Title;
		}

		/// <summary>
		/// Gets or sets the dynamic display header picture.
		/// </summary>
		/// <value>
		/// The dynamic display header picture.
		/// </value>
		[Information("msg_chart_dynamicdisplayheaderpicture", null)]
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		[ChartDisplayOnly]
		[ChartDynamicDisplay]
		[BatchUpdatable(true)]
		public string DynamicDisplayHeaderPicture { get; set; }

		/// <summary>
		/// Gets or sets the dynamic display header picture image.
		/// </summary>
		/// <value>
		/// The dynamic display header picture image.
		/// </value>
		[DataMember]
		[ChartDisplayOnly]
		[ChartDynamicDisplay]
		public DynamicDisplayImage DynamicDisplayHeaderPictureImage { get; set; }

		/// <summary>
		/// Gets or sets the dynamic display main picture.
		/// </summary>
		/// <value>
		/// The dynamic display main picture.
		/// </value>
		[Information("msg_chart_dynamicdisplaymainpicture", null)]
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		[ChartDisplayOnly]
		[ChartDynamicDisplay]
		[BatchUpdatable(true)]
		public string DynamicDisplayMainPicture { get; set; }

		/// <summary>
		/// Gets or sets the dynamic display main picture image.
		/// </summary>
		/// <value>
		/// The dynamic display main picture image.
		/// </value>
		[DataMember]
		[ChartDisplayOnly]
		[ChartDynamicDisplay]
		public DynamicDisplayImage DynamicDisplayMainPictureImage { get; set; }

		/// <summary>
		/// Gets or sets the dynamic display footer picture.
		/// </summary>
		/// <value>
		/// The dynamic display footer picture.
		/// </value>
		[Information("msg_chart_dynamicdisplayfooterpicture", null)]
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		[ChartDisplayOnly]
		[ChartDynamicDisplay]
		[BatchUpdatable(true)]
		public string DynamicDisplayFooterPicture { get; set; }

		/// <summary>
		/// Gets or sets the dynamic display footer picture image.
		/// </summary>
		/// <value>
		/// The dynamic display footer picture image.
		/// </value>
		[DataMember]
		[ChartDisplayOnly]
		[ChartDynamicDisplay]
		public DynamicDisplayImage DynamicDisplayFooterPictureImage { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		[Information("msg_chart_favorite", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool IsFavorite { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		[Information("msg_portalwindow_openonstartup", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool OpenOnStartup { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether we use spatial path, or just the spatial element name in the series generation.
		/// </summary>
		/// <value>
		///   <c>true</c> if [use spatial path]; otherwise, <c>false</c>.
		/// </value>
		[Information("msg_chart_usespatialpath", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public bool UseSpatialPath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether we hide full spatial path.
        /// </summary>
        /// <value>
        /// <c>true</c> if [hide full spatial path]; otherwise, <c>false</c>.
        /// </value>
		[Information("msg_chart_hide_full_spatial_path", null)]
		[DataMember]
		public bool HideFullSpatialPath { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether we use classification path, or just the classification item title in the series generation.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [use classification path]; otherwise, <c>false</c>.
		/// </value>
		[Information("msg_chart_useclassificationpath", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public bool UseClassificationPath { get; set; }

		/// <summary>
		/// Gets or sets the X axis start day of week.
		/// </summary>
		/// <value>
		/// The X axis start day of week.
		/// </value>
		[Information("msg_chart_xaxis_startdayofweek", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public int XAxisStartDayOfWeek { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is KPI.
		/// </summary>
		[Information("msg_chart_kpi_define", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool IsKPI { get; set; }


		/// <summary>
		/// Gets or sets the display color of the font.
		/// </summary>
		/// <value>
		/// The display color of the font.
		/// </value>
		[Information("msg_chart_display_fontcolor", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public string DisplayFontColor { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable localisation slider].
		/// </summary>
		[Information("msg_chart_kpi_disablelocalisationslider", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool KPIDisableLocalisationSlider { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable time interval slider].
		/// </summary>
		[Information("msg_chart_kpi_disabletimeintervalslider", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool KPIDisableTimeIntervalSlider { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable classification level slider].
		/// </summary>
		[Information("msg_chart_kpi_disableclassificationlevelslider", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool KPIDisableClassificationLevelSlider { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable display mode slider].
		/// </summary>
		[Information("msg_chart_kpi_disabledisplaymodeslider", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool KPIDisableDisplayModeSlider { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable limit series number slider].
		/// </summary>
		[Information("msg_chart_kpi_disablelimitseriesnumberslider", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool KPIDisableLimitSeriesNumberSlider { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable meter classification selection].
		/// </summary>
		[Information("msg_chart_kpi_disablemeterclassificationselection", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool KPIDisableMeterClassificationSelection { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable spatial selection].
		/// </summary>
		[Information("msg_chart_kpi_disablespatialselection", null)]
		[DataMember]
		public bool KPIDisableSpatialSelection { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable meter selection].
		/// </summary>
		[Information("msg_chart_kpi_disablemeterselection", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool KPIDisableMeterSelection { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable date range menu].
		/// </summary>
		[Information("msg_chart_kpi_disabledaterangemenu", null)]
		[DataMember]
		[ChartDisplayOnly]
		public bool KPIDisableDateRangeMenu { get; set; }

		/// <summary>
		/// Gets or sets the KPI id.
		/// </summary>		
		[Information("msg_chart_kpi_id", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string KPIId { get; set; }

		/// <summary>
		/// Gets or sets the KPI setup instructions.
		/// </summary>
		[Information("msg_chart_kpi_setupinstructions", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string KPISetupInstructions { get; set; }

		/// <summary>
		/// Gets or sets the KPI copy date.
		/// </summary>
		[Information("msg_chart_kpi_copydate", null)]
		[DataMember]
		public DateTime? KPICopyDate { get; set; }

		/// <summary>
		/// The Key of the DynamicDisplayImage to display for KPI.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyKPIPreviewPicture { get; set; }

		/// <summary>
		/// The Key of the Tenant default DynamicDisplayImage to display for KPI.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string TenantKeyKPIPreviewPicture { get; set; }

		/// <summary>
		/// Gets or sets the KPI build.
		/// </summary>
		[Information("msg_chart_kpi_build", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string KPIBuild { get; set; }

		/// <summary>
		/// Gets the virtual file path.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string VirtualFilePath { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether drill down is enabled.
		/// </summary>
		[DataMember]
		[ChartDisplayOnly]
		public bool DrillDownEnabled { get; set; }

		/// <summary>
		/// Gets or sets the drill down.
		/// </summary>
		[DataMember]
		[ChartDisplayOnly]
		public ChartDrillDown DrillDown { get; set; }

		/// <summary>
		/// Gets or sets the drill down mode.
		/// </summary>
		[Information("msg_chartdrilldown", null)]
		[DataMember]
		[ChartDisplayOnly]
		public ChartDrillDownMode DrillDownMode { get; set; }

		/// <summary>
		/// if set to <c>true</c>, the chart will use the Meter EndPoint (if defined) to get realtime values instead of going to the historical values.
		/// </summary>
		[Information("msg_chart_usedataacquisitionendpoint", null)]
		[DataMember]
		public bool UseDataAcquisitionEndPoint { get; set; }

		/// <summary>
		/// Gets or sets the calendar view selections.
		/// </summary>
		/// <value>
		/// The calendar view selections.
		/// </value>
		[DataMember]
		public List<ChartCalendarViewSelection> CalendarViewSelections { get; set; }

		/// <summary>
		/// The key of the classification of the KPI Definition.
		/// </summary>
		[Information("msg_chart_kpi_classification", null)]
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string KeyClassificationItemKPI { get; set; }

		/// <summary>
		/// The key path of the classification of the KPI.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string KeyClassificationItemPathKPI { get; set; }

		/// <summary>
		/// The classification title of the KPI.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string ClassificationItemKPITitle { get; set; }

		/// <summary>
		/// The classification level of the KPI.
		/// </summary>
		[DataMember]
		[ChartDisplayOnly]
		public int ClassificationItemKPILevel { get; set; }

		/// <summary>
		/// The classification path of the KPI.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string ClassificationItemKPILongPath { get; set; }

		/// <summary>
		/// The classification local id path of the KPI.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ChartDisplayOnly]
		public string ClassificationItemKPILocalIdPath { get; set; }

		/// <summary>
		/// The list of roles of the ApplicationGroup.
		/// This field is defined only in order to get the list from a xml mapping document.
		/// Otherwise it should remain null.
		/// </summary>
		public List<AuthorizationItem> Roles { get; set; }

		/// <summary>
		/// Gets or sets the list of Algorithms.
		/// </summary>
		[DataMember]
		public List<ChartAlgorithm> Algorithms { get; set; }

		/// <summary>
		/// Gets or sets the legend position.
		/// </summary>
		/// <value>
		/// The legend position.
		/// </value>
		[Information("msg_chart_display_legend_position", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public ChartLegendPosition LegendPosition { get; set; }

		/// <summary>
		/// Gets or sets the info icon position.
		/// </summary>
		/// <value>
		/// The info icon position.
		/// </value>
		[Information("msg_chart_display_infoicon_position", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public ChartInfoIconPosition InfoIconPosition { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [display legend plain].
		/// </summary>
		/// <value>
		///   <c>true</c> if [display legend plain]; otherwise, <c>false</c>.
		/// </value>
		[Information("msg_chart_display_legend_plain", null)]
		[DataMember]
		[ChartDisplayOnly]
		[BatchUpdatable(true)]
		public bool DisplayLegendPlain { get; set; }


		/// <summary>
		/// Gets or sets the max meters number a chart can process.
		/// </summary>
		/// <value>
		/// The max meter.
		/// </value>
		[DataMember]
		public int MaxMeter { get; set; }


		/// <summary>
		/// Number of portlet that are linked to this chart, if 0 the chart is not used.
		/// </summary>
		/// <value>
		/// The used count.
		/// </value>
		[DataMember]
		public int UsedCount { get; set; }

		/// <summary>
		/// Gets or sets the AlarmInstances generated by the script.
		/// </summary>
		[DataMember]
		public List<AlarmInstance> ScriptAlarmInstances { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether [hide meter data validity].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [hide meter data validity]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool HideMeterDataValidity { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether [hide datagrid in export].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [hide datagrid in export]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		[ChartDisplayOnly]
		public bool HideDatagridInExport { get; set; }

		/// <summary>
		/// Gets or sets the HTML.
		/// </summary>
		/// <value>
		/// The HTML.
		/// </value>
		[DataMember]
		public string Html { get; set; }

		/// <summary>
		/// Gets or sets the algorithms count.
		/// </summary>
		/// <value>
		/// The algorithms count.
		/// </value>
		[DataMember]
		public int AlgorithmsCount { get; set; }


		/// <summary>
		/// Gets or sets the width of the grid transpose location.
		/// </summary>
		[DataMember]
		[ChartDisplayOnly]
		public int? GridTransposeLocationWidth { get; set; }

		/// <summary>
		/// Gets or sets the width of the grid transpose classification.
		/// </summary>
		[DataMember]
		[ChartDisplayOnly]
		public int? GridTransposeClassificationWidth { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether [grid hide unit].
		/// </summary>
		[ChartDisplayOnly]
		[DataMember]
		public bool GridHideUnit { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [grid aggregate by calculated serie classification].
		/// </summary>
		[DataMember]
		[ChartDisplayOnly]
		public bool GridAggregateByCalculatedSerieClassification { get; set; }

		/// <summary>
		/// Gets or sets the direct linked meters keys.
		/// </summary>
		/// <value>
		/// The direct linked meters keys.
		/// </value>
		[DataMember]
		public List<Meter> Meters { get; set; }

		/// <summary>
		/// Gets or sets the direct linked meters keys.
		/// </summary>
		/// <value>
		/// The direct linked meters keys.
		/// </value>
		[DataMember]
		public List<Meter> ResultingMeters { get; set; }

		/// <summary>
		/// Gets or sets the chart markers.
		/// </summary>
		/// <value>
		/// The chart markers.
		/// </value>
		[DataMember]
		public List<ChartMarker> ChartMarkers { get; set; }

		/// <summary>
		/// Max Meter Overflow Error.
		/// </summary>
		[DataMember]
		public string MaxMeterOverflowError { get; set; }

        /// <summary>
        /// Gets or sets the dynamic display key localization culture.
        /// </summary>
        [AntiXssValidator]
        [DataMember]
        public string DynamicDisplayKeyLocalizationCulture { get; set; }
	}
}

﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a specifing time period to display on the Chart Calendar View Bottom Area.
	/// </summary>
	[Serializable]
	[Key("KeyChartCalendarViewSelection")]
	[DataContract]
	public class ChartCalendarViewSelection : BaseBusinessEntity {
		/// <summary>
		/// Key of the ChartCalendarViewSelection.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChartCalendarViewSelection { get; set; }

		/// <summary>
		/// Key of the Chart.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		[DataMember]
		public DateTime StartDate { get; set; }

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		[DataMember]
		public DateTime EndDate { get; set; }
	}
}

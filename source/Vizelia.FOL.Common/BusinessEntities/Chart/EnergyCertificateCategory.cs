﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a EnergyCertificateCategory.
	/// </summary>
	[Serializable]
	[Key("KeyEnergyCertificateCategory")]
	[DataContract]
	[IconCls("viz-icon-small-energycertificate")]
	public class EnergyCertificateCategory : BaseBusinessEntity, IMappableEntity {

		//TODO : Make mappable

		/// <summary>
		/// Key of the KeyEnergyCertificateCategory.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyEnergyCertificateCategory { get; set; }

		/// <summary>
		/// Key of the EnergyCertificate.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyEnergyCertificate { get; set; }

		/// <summary>
		/// Gets or sets the Name.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the Min value.
		/// </summary>
		[DataMember]
		public double? MinValue { get; set; }


		/// <summary>
		/// Gets or sets the Min value.
		/// </summary>
		[DataMember]
		public double? MaxValue { get; set; }


		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }
	}
}

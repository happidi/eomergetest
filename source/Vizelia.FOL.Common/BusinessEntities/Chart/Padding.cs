﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System.Collections;
using System.ComponentModel;
using System.Runtime;
using System.Globalization;


namespace Vizelia.FOL.BusinessEntities {
	/// <summary>Represents padding or margin information associated with a user interface (UI) element.</summary>
	/// <filterpriority>2</filterpriority>
	[Serializable]
	public struct Padding {
		private bool _all;
		private int _top;
		private int _left;
		private int _right;
		private int _bottom;
		/// <summary>Provides a <see cref="T:Padding" /> object with no padding.</summary>
		/// <filterpriority>1</filterpriority>
		public static readonly Padding Empty = new Padding(0);
		/// <summary>Gets or sets the padding value for all the edges.</summary>
		/// <returns>The padding, in pixels, for all edges if the same; otherwise, -1.</returns>
		/// <filterpriority>1</filterpriority>
		public int All {
			get {
				if (!this._all) {
					return -1;
				}
				return this._top;
			}
			set {
				if (!this._all || this._top != value) {
					this._all = true;
					this._bottom = value;
					this._right = value;
					this._left = value;
					this._top = value;
				}
			}
		}
		/// <summary>Gets or sets the padding value for the bottom edge.</summary>
		/// <returns>The padding, in pixels, for the bottom edge.</returns>
		/// <filterpriority>1</filterpriority>
		[RefreshProperties(RefreshProperties.All)]
		public int Bottom {
			get {
				if (this._all) {
					return this._top;
				}
				return this._bottom;
			}
			set {
				if (this._all || this._bottom != value) {
					this._all = false;
					this._bottom = value;
				}
			}
		}
		/// <summary>Gets or sets the padding value for the left edge.</summary>
		/// <returns>The padding, in pixels, for the left edge.</returns>
		/// <filterpriority>1</filterpriority>
		[RefreshProperties(RefreshProperties.All)]
		public int Left {
			get {
				if (this._all) {
					return this._top;
				}
				return this._left;
			}
			set {
				if (this._all || this._left != value) {
					this._all = false;
					this._left = value;
				}
			}
		}
		/// <summary>Gets or sets the padding value for the right edge.</summary>
		/// <returns>The padding, in pixels, for the right edge.</returns>
		/// <filterpriority>1</filterpriority>
		[RefreshProperties(RefreshProperties.All)]
		public int Right {
			get {
				if (this._all) {
					return this._top;
				}
				return this._right;
			}
			set {
				if (this._all || this._right != value) {
					this._all = false;
					this._right = value;
				}
			}
		}
		/// <summary>Gets or sets the padding value for the top edge.</summary>
		/// <returns>The padding, in pixels, for the top edge.</returns>
		/// <filterpriority>1</filterpriority>
		[RefreshProperties(RefreshProperties.All)]
		public int Top {
			get {
				return this._top;
			}
			set {
				if (this._all || this._top != value) {
					this._all = false;
					this._top = value;
				}
			}
		}
		/// <summary>Gets the combined padding for the right and left edges.</summary>
		/// <returns>Gets the sum, in pixels, of the <see cref="P:Padding.Left" /> and <see cref="P:Padding.Right" /> padding values.</returns>
		/// <filterpriority>1</filterpriority>
		[Browsable(false)]
		public int Horizontal {
			get {
				return this.Left + this.Right;
			}
		}
		/// <summary>Gets the combined padding for the top and bottom edges.</summary>
		/// <returns>Gets the sum, in pixels, of the <see cref="P:Padding.Top" /> and <see cref="P:Padding.Bottom" /> padding values.</returns>
		/// <filterpriority>1</filterpriority>
		[Browsable(false)]
		public int Vertical {
			get {
				return this.Top + this.Bottom;
			}
		}
		
		/// <summary>Initializes a new instance of the <see cref="T:Padding" /> class using the supplied padding size for all edges.</summary>
		/// <param name="all">The number of pixels to be used for padding for all edges.</param>
		public Padding(int all) {
			this._all = true;
			this._bottom = all;
			this._right = all;
			this._left = all;
			this._top = all;
		}
		/// <summary>Initializes a new instance of the <see cref="T:Padding" /> class using a separate padding size for each edge.</summary>
		/// <param name="left">The padding size, in pixels, for the left edge.</param>
		/// <param name="top">The padding size, in pixels, for the top edge.</param>
		/// <param name="right">The padding size, in pixels, for the right edge.</param>
		/// <param name="bottom">The padding size, in pixels, for the bottom edge.</param>
		public Padding(int left, int top, int right, int bottom) {
			this._top = top;
			this._left = left;
			this._right = right;
			this._bottom = bottom;
			this._all = (this._top == this._left && this._top == this._right && this._top == this._bottom);
		}
		/// <summary>Computes the sum of the two specified <see cref="T:Padding" /> values.</summary>
		/// <returns>A <see cref="T:Padding" /> that contains the sum of the two specified <see cref="T:Padding" /> values.</returns>
		/// <param name="p1">A <see cref="T:Padding" />.</param>
		/// <param name="p2">A <see cref="T:Padding" />.</param>
		public static Padding Add(Padding p1, Padding p2) {
			return p1 + p2;
		}
		/// <summary>Subtracts one specified <see cref="T:Padding" /> value from another.</summary>
		/// <returns>A <see cref="T:Padding" /> that contains the result of the subtraction of one specified <see cref="T:Padding" /> value from another.</returns>
		/// <param name="p1">A <see cref="T:Padding" />.</param>
		/// <param name="p2">A <see cref="T:Padding" />.</param>
		public static Padding Subtract(Padding p1, Padding p2) {
			return p1 - p2;
		}
		
		/// <summary>Performs vector addition on the two specified <see cref="T:Padding" /> objects, resulting in a new <see cref="T:Padding" />.</summary>
		/// <returns>A new <see cref="T:Padding" /> that results from adding <paramref name="p1" /> and <paramref name="p2" />.</returns>
		/// <param name="p1">The first <see cref="T:Padding" /> to add.</param>
		/// <param name="p2">The second <see cref="T:Padding" /> to add.</param>
		/// <filterpriority>3</filterpriority>
		public static Padding operator +(Padding p1, Padding p2) {
			return new Padding(p1.Left + p2.Left, p1.Top + p2.Top, p1.Right + p2.Right, p1.Bottom + p2.Bottom);
		}
		/// <summary>Performs vector subtraction on the two specified <see cref="T:Padding" /> objects, resulting in a new <see cref="T:Padding" />.</summary>
		/// <returns>The <see cref="T:Padding" /> result of subtracting <paramref name="p2" /> from <paramref name="p1" />.</returns>
		/// <param name="p1">The <see cref="T:Padding" /> to subtract from (the minuend).</param>
		/// <param name="p2">The <see cref="T:Padding" /> to subtract from (the subtrahend).</param>
		/// <filterpriority>3</filterpriority>
		public static Padding operator -(Padding p1, Padding p2) {
			return new Padding(p1.Left - p2.Left, p1.Top - p2.Top, p1.Right - p2.Right, p1.Bottom - p2.Bottom);
		}
		/// <summary>Tests whether two specified <see cref="T:Padding" /> objects are equivalent.</summary>
		/// <returns>true if the two <see cref="T:Padding" /> objects are equal; otherwise, false.</returns>
		/// <param name="p1">A <see cref="T:Padding" /> to test.</param>
		/// <param name="p2">A <see cref="T:Padding" /> to test.</param>
		/// <filterpriority>3</filterpriority>
		public static bool operator ==(Padding p1, Padding p2) {
			return p1.Left == p2.Left && p1.Top == p2.Top && p1.Right == p2.Right && p1.Bottom == p2.Bottom;
		}

		/// <summary>Generates a hash code for the current <see cref="T:Padding" />. </summary>
		/// <returns>A 32-bit signed integer hash code.</returns>
		/// <filterpriority>1</filterpriority>
		public override int GetHashCode() {
			return this.Left ^ this.Top ^ this.Right ^ this.Bottom;
		}
		/// <summary>Determines whether the value of the specified object is equivalent to the current <see cref="T:Padding" />.</summary>
		/// <returns>true if the <see cref="T:Padding" /> objects are equivalent; otherwise, false.</returns>
		/// <param name="other">The object to compare to the current <see cref="T:Padding" />.</param>
		/// <filterpriority>1</filterpriority>
		public override bool Equals(object other) {
			return other is Padding && (Padding)other == this;
		}
		/// <summary>Tests whether two specified <see cref="T:Padding" /> objects are not equivalent.</summary>
		/// <returns>true if the two <see cref="T:Padding" /> objects are different; otherwise, false.</returns>
		/// <param name="p1">A <see cref="T:Padding" /> to test.</param>
		/// <param name="p2">A <see cref="T:Padding" /> to test.</param>
		/// <filterpriority>3</filterpriority>
		public static bool operator !=(Padding p1, Padding p2) {
			return !(p1 == p2);
		}
		
		/// <summary>Returns a string that represents the current <see cref="T:Padding" />.</summary>
		/// <returns>A <see cref="T:System.String" /> that represents the current <see cref="T:Padding" />.</returns>
		/// <filterpriority>1</filterpriority>
		public override string ToString() {
			return string.Concat(new string[]
			{
				"{Left=", 
				this.Left.ToString(CultureInfo.CurrentCulture), 
				",Top=", 
				this.Top.ToString(CultureInfo.CurrentCulture), 
				",Right=", 
				this.Right.ToString(CultureInfo.CurrentCulture), 
				",Bottom=", 
				this.Bottom.ToString(CultureInfo.CurrentCulture), 
				"}"
			});
		}
		private void ResetAll() {
			this.All = 0;
		}
		private void ResetBottom() {
			this.Bottom = 0;
		}
		private void ResetLeft() {
			this.Left = 0;
		}
		private void ResetRight() {
			this.Right = 0;
		}
		private void ResetTop() {
			this.Top = 0;
		}
		internal void Scale(float dx, float dy) {
			this._top = (int)((float)this._top * dy);
			this._left = (int)((float)this._left * dx);
			this._right = (int)((float)this._right * dx);
			this._bottom = (int)((float)this._bottom * dy);
		}
		internal bool ShouldSerializeAll() {
			return this._all;
		}
		
	}
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity representing a Dynamic display image.
	/// </summary>
	[Serializable]
	[Key("KeyDynamicDisplayImage")]
	[DataContract]
	[IconCls("viz-icon-small-dynamicdisplayimage")]
	public class DynamicDisplayImage : BaseBusinessEntity, ISupportDocuments, IMappableEntity {

	    /// <summary>
	    /// Gets the application.
	    /// </summary>
		[AntiXssValidator]
		[DataMember]
        public string Application { get; set; }

		/// <summary>
		/// Key of the Dynamic Display Image.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyDynamicDisplayImage { get; set; }

		/// <summary>
		/// Name of the Dynamic Display Image.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets or Sets the Key of the document attached to it.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[NotNullValidator]
		public string KeyImage { get; set; }

		/// <summary>
		/// Gets the Image document associated.
		/// </summary>
		[DataMember]
		public Document Image { get; set; }

		/// <summary>
		/// Gets or sets the type.
		/// </summary>
		[DataMember]
		public DynamicDisplayImageType Type { get; set; }

		/// <summary>
		/// Gets the propreties that have document ids
		/// </summary>
		/// <returns></returns>
		public List<string> GetDocumentFields() {
			return new List<string> { "KeyImage" };
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		[DataMember]
		public string LocalId {
			get { return Name; }
			set { Name = value; }
		}
	}
}

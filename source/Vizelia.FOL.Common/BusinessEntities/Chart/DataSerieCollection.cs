﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Represents a Collection of DataSerie with fast access by LocalId.
	/// </summary>
	[Serializable]
	public class DataSerieCollection {

		/// <summary>
		/// Private dictionary to store the list of Dataserie.
		/// </summary>
		private readonly Dictionary<string, DataSerie> m_Dictionary;


		/// <summary>
		/// Initializes a new instance of the <see cref="DataSerieCollection"/> class.
		/// </summary>
		public DataSerieCollection() {
			m_Dictionary = new Dictionary<string, DataSerie>();

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DataSerieCollection"/> class.
		/// </summary>
		/// <param name="series">The series.</param>
		public DataSerieCollection(List<DataSerie> series)
			: this() {
			series.ForEach(s => m_Dictionary.Add(s.LocalId, s));
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DataSerieCollection"/> class.
		/// </summary>
		/// <param name="dataSerieCollection">The data serie collection.</param>
		public DataSerieCollection(DataSerieCollection dataSerieCollection) {
			m_Dictionary = new Dictionary<string, DataSerie>(dataSerieCollection.m_Dictionary);
		}

		/// <summary>
		/// Findsa dataserie in the collection based on the LocalId
		/// </summary>
		/// <param name="localId">The LocalId.</param>
		/// <returns></returns>
		public DataSerie FindByLocalId(string localId) {
			DataSerie retVal;
			m_Dictionary.TryGetValue(localId, out retVal);
			return retVal;
		}

		/// <summary>
		/// Determines whether the specified localId is in the collection.
		/// </summary>
		/// <param name="localId">The local id.</param>
		public bool ContainsLocalid(string localId) {
			var retVal = m_Dictionary.ContainsKey(localId);
			return retVal;
		}

		/// <summary>
		/// Adds the specified serie to the collection.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public bool Add(DataSerie serie) {
			var retVal = false;

			if (serie.IsClassificationItem) {
				if (!m_Dictionary.ContainsKey(serie.ClassificationItemLocalIdPath)) {
					m_Dictionary.Add(serie.ClassificationItemLocalIdPath, serie);
					retVal = true;
				}
			}
			else {
				if (!m_Dictionary.ContainsKey(serie.LocalId)) {
					m_Dictionary.Add(serie.LocalId, serie);
					retVal = true;
				}
			}
			return retVal;
		}

		/// <summary>
		/// Adds the specified series to the collection.
		/// </summary>
		/// <param name="series">The series.</param>
		public void AddRange(List<DataSerie> series) {
			if (series != null)
				series.ForEach(s => Add(s));
		}


		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear() {
			m_Dictionary.Clear();
		}

		/// <summary>
		/// Tries the get data serie.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public bool TryGetDataSerie(string localId, out DataSerie serie) {
			return m_Dictionary.TryGetValue(localId, out serie);
		}

		/// <summary>
		/// Determines whether the series collection contains a serie customization for a given localId
		/// </summary>
		/// <param name="localId">The local identifier of the serie</param>
		public bool HasSeriesCustomizationFor(string localId){
			return m_Dictionary.ContainsKey(localId);
		}


		/// <summary>
		/// Tries the get data serie.
		/// </summary>
		/// <param name="localIdPath">The LocalIdPath of the classification to look for.</param>
		/// <param name="serie">The serie.</param>
		/// <returns></returns>
		public bool TryGetDataSerieByClassificationLocalIdPath(string localIdPath, out DataSerie serie) {
			serie = (from s in m_Dictionary.Values
					 where
					 s.IsClassificationItem &&
					 s.ClassificationItemLocalIdPath == localIdPath.Substring(0, Math.Min(localIdPath.Length, s.ClassificationItemLocalIdPath.Length))
					 orderby
					 s.ClassificationItemLocalIdPath.Length descending
					 select s).FirstOrDefault();
			return serie != null;
		}

		/// <summary>
		/// Return a simple list of DataSerie.
		/// </summary>
		/// <returns></returns>
		public List<DataSerie> ToList() {
			return m_Dictionary.Count > 0 ? m_Dictionary.Values.ToList() : new List<DataSerie>();
		}

		/// <summary>
		/// Gets the count.
		/// </summary>
		public int Count {
			get {
				return m_Dictionary.Count;
			}
		}

		/// <summary>
		/// Gets only the visible data series that have points with values in them.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<DataSerie> GetVisibleDataSeries() {
			var visibleSeries = m_Dictionary.Values.Where(s => !s.Hidden && s.HasPointsWithValue);
			return visibleSeries;
		}

		/// <summary>
		/// Gets a value indicating whether any point with value exists in the data serie collection.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if any point with value exists in the data serie collection; otherwise, <c>false</c>.
		/// </value>
		public bool HasPointsWithValue {
			get {
				bool anyPointWithValue = m_Dictionary.Values.Any(s => s.HasPointsWithValue);
				return anyPointWithValue;
			}
		}

		/// <summary>
		/// Return the collection as enumerable.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<DataSerie> AsEnumerable() {
			return m_Dictionary.Values.AsEnumerable();
		}
	}
}

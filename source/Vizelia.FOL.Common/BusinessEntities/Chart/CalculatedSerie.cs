﻿using System;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a calculated Serie.
	/// </summary>
	[Key("KeyCalculatedSerie")]
	[DataContract]
	[Serializable]
	[LocalizedText("msg_calculatedseries")]
	[IconCls("viz-icon-small-calculatedserie")]
	public class CalculatedSerie : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Key of the CalculatedSerie.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyCalculatedSerie { get; set; }

		/// <summary>
		/// Key of the Chart.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the order of the calculated serie.
		/// </summary>
		[DataMember]
		public int Order { get; set; }

		/// <summary>
		/// Gets or sets the formula of the calculated Serie.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Formula { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the Unit 
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Unit { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the series used in the calculation will be hidden.
		/// </summary>
		/// <value>
		///   if <c>true</c> hides the used series; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool HideUsedSeries { get; set; }

		/// <summary>
		/// Gets or sets the Key of the LocalizationCulture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocalizationCulture { get; set; }

		/// <summary>
		/// Gets or sets the Key of the Meter Classification Item the series applys to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItem { get; set; }

		/// <summary>
		/// The key path of the classification of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItemPath { get; set; }

		/// <summary>
		/// The classification title of the Meter Classification Item the series applys to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemTitle { get; set; }

		/// <summary>
		/// The classification path of the Meter Classification Item the series applys to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLongPath { get; set; }

		/// <summary>
		/// The classification local id path of the Meter Classification Item the series applys to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLocalIdPath { get; set; }

		/// <summary>
		/// The Key of the Calculated Serie location. Used when doing Calculation with Classification.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocation { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether [run successfully].
		/// </summary>
		[DataMember]
		public bool RunSuccessfully { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether [run only in main timeline].
		/// </summary>
		[DataMember]
		public bool RunOnlyInMainTimeline { get; set; }
	}
}

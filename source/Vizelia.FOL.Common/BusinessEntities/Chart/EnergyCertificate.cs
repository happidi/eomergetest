﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing an EnergyCertificate.
	/// </summary>
	[Serializable]
	[Key("KeyEnergyCertificate")]
	[DataContract]
	[IconCls("viz-icon-small-energycertificate")]
	public class EnergyCertificate : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Initializes a new instance of the <see cref="EnergyCertificate"/> class.
		/// </summary>
		public EnergyCertificate() {
			Categories = new List<EnergyCertificateCategory>();
		}

		/// <summary>
		/// Key of the EnergyCertificate.
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		//[FilterType(typeof(EnergyCertificate), false)]
		public string KeyEnergyCertificate { get; set; }

		/// <summary>
		/// Gets or sets the Name.
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the StartColor.
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string StartColor { get; set; }

		/// <summary>
		/// Gets or sets the EndColor.
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string EndColor { get; set; }


		/// <summary>
		/// Gets or sets the categories.
		/// </summary>
		/// <value>
		/// The categories.
		/// </value>
		[DataMember]
		public List<EnergyCertificateCategory> Categories { get; set; }

		/*/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity() {
				KeySecurable = this.KeyEnergyCertificate,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(this.GetType()),
				SecurableName = this.Name,
				SecurableTypeName = this.GetType().FullName
			};
		}
		*/

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		public string Creator { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId { get; set; }
	}
}

﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Chart Marker based on time.
	/// </summary>
	[Key("KeyChartTimeMarker")]
	[DataContract]
	[Serializable]
	public class ChartTimeMarker : BaseBusinessEntity {

		/// <summary>
		/// Contructor
		/// </summary>
		public ChartTimeMarker(AxisTimeInterval timeinterval) {
				_timeintervale = timeinterval;
		}

		private AxisTimeInterval _timeintervale;
		/// <summary>
		/// Key of the Chart Marker.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChartTimeMarker { get; set; }

		/// <summary>
		/// Gets or sets the name of the marker.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the minimum value of the marker.
		/// </summary>
		[DataMember]
		public DateTime Min { get; set; }

		/// <summary>
		/// Gets or sets the maximum value of the marker.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MinCategorie { 
			get {
				return Min.ToString(_timeintervale.GetFormat());
			}
			set {}
		}

		/// <summary>
		/// Gets or sets the maximum value of the marker.
		/// </summary>
		[DataMember]
		public DateTime Max { get; set; }

		/// <summary>
		/// Gets or sets the maximum value of the marker.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MaxCategorie {
			get {
				return Max.ToString(_timeintervale.GetFormat());
			}
			set {}
		}

		/// <summary>
		/// Red color composant.
		/// </summary>
		[DataMember]
		public int? ColorR { get; set; }

		/// <summary>
		/// Green color composant.
		/// </summary>
		[DataMember]
		public int? ColorG { get; set; }

		/// <summary>
		/// Blue color composant.
		/// </summary>
		[DataMember]
		public int? ColorB { get; set; }
	}
}

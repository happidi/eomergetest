﻿using System;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity representing a Chart Axis
	/// </summary>
	[Key("KeyChartHistoricalAnalysis")]
	[DataContract]
	[Serializable]
	public class ChartHistoricalAnalysis : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Ctor
		/// </summary>
		public ChartHistoricalAnalysis() {
			ExtraChartArea = false;
		}

		/// <summary>
		/// Key of the ChartHistoricalAnalysis.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyChartHistoricalAnalysis { get; set; }

		/// <summary>
		/// Key of the Chart.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string KeyChart { get; set; }

		/// <summary>
		/// Get or sets the frequency to slide the chart time scale
		/// </summary>
		[DataMember]
		public int Frequency { get; set; }

		/// <summary>
		/// Get or sets the frequency to slide the chart time scale
		/// </summary>
		[DataMember]
		public AxisTimeInterval Interval { get; set; }

		/// <summary>
		/// True to render the historical analysis on another chart area, false otherwise.
		/// </summary>
		[DataMember]
		public bool ExtraChartArea { get; set; }

		/// <summary>
		/// Gets or sets the historical analysis  dataserie type, applys only if the chart is a combo.
		/// </summary>
		[DataMember]
		public DataSerieType DataSerieType { get; set; }

		/// <summary>
		/// True to enable the analysis, false otherwise.
		/// </summary>
		[DataMember]
		public bool Enabled { get; set; }

		/// <summary>
		/// True to display the extra XAxis false otherwise.
		/// </summary>
		[DataMember]
		public bool XAxisVisible { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		[DataMember]
		public DateTime? StartDate { get; set; }

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		[DataMember]
		public DateTime? EndDate { get; set; }

		/// <summary>
		/// Gets the name.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether [set X axis min].
		/// </summary>
		[DataMember]
		public bool SetXAxisMin { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [set X axis max].
		/// </summary>
		[DataMember]
		public bool SetXAxisMax { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [include in main timeline].
		/// </summary>
		[DataMember]
		public bool IncludeInMainTimeline { get; set; }

	}
}

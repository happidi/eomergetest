﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;


namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Helper class to simplify generating dynamic images from stream.
	/// </summary>
	public class BitmapWithGraphics : IDisposable {

		/// <summary>
		/// private Bitmap object.
		/// </summary>
		private Bitmap _bitmap;

		/// <summary>
		/// Private Graphics context.
		/// </summary>
		private Graphics _graphics;

		/// <summary>
		/// Width.
		/// </summary>
		public int Width {
			get {
				return _bitmap.Width;
			}
		}

		/// <summary>
		/// Height.
		/// </summary>
		public int Height {
			get {
				return _bitmap.Height;
			}
		}


		/// <summary>
		/// Contructor.
		/// </summary>
		/// <param name="width">the width.</param>
		/// <param name="height">the height.</param>
		public BitmapWithGraphics(int width, int height) {
			_bitmap = new Bitmap(width, height);
			InitGraphics();
		}

		/// <summary>
		/// Contructor.
		/// </summary>
		/// <param name="stream">The original stream.</param>
		public BitmapWithGraphics(Stream stream) {
			_bitmap = new Bitmap(stream);
			InitGraphics();
		}

		/// <summary>
		/// Init the Graphic context.
		/// </summary>
		private void InitGraphics() {
			_graphics = Graphics.FromImage(_bitmap);
			_graphics.PageUnit = GraphicsUnit.Pixel;
			_graphics.SmoothingMode = SmoothingMode.AntiAlias;
			_graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
			_graphics.CompositingQuality = CompositingQuality.HighQuality;
		}

		/// <summary>
		/// Realease all the ressources used by this BitmapWithGraphics.
		/// </summary>
		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Releases unmanaged resources and performs other cleanup operations before the
		/// <see cref="BitmapWithGraphics"/> is reclaimed by garbage collection.
		/// </summary>
		~BitmapWithGraphics() {
			// Finalizer calls Dispose(false)
			Dispose(false);
		}

		/// <summary>
		/// Releases unmanaged and - optionally - managed resources
		/// </summary>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected virtual void Dispose(bool disposing) {
			if (disposing) {
				if (_graphics != null) {
					_graphics.Dispose();
					_graphics = null;
				}
				if (_bitmap != null) {
					_bitmap.Dispose();
					_bitmap = null;
				}
			}
		}



		/// <summary>
		/// Fills the bitmap background with  a plain color.
		/// </summary>
		/// <param name="color">the background color.</param>
		public void SetBackgroundColor(Color color) {
			using (var brush = new SolidBrush(color)) {
				_graphics.FillRectangle(brush, 0, 0, Width, Height);
			}
		}

		/// <summary>
		/// Draws the specified stream of a bitmap at the specified location with the specified size.
		/// </summary>
		/// <param name="stream">The bitmap stream to draw.</param>
		/// <param name="rect">The size and location.</param>
		public void DrawStream(Stream stream, Rectangle rect) {
			using (var bitmap = new Bitmap(stream)) {
				_graphics.DrawImage(bitmap, rect);
				//this._graphics.DrawRectangle(new Pen(Color.Red, 1), rect);
			}
		}

		/// <summary>
		/// Draws the specified stream of a bitmap at the specified location with the specified size.
		/// </summary>
		/// <param name="stream">The bitmap stream to draw.</param>
		/// <param name="p">The p.</param>
		public void DrawStream(Stream stream, Point p) {
			using (var bitmap = new Bitmap(stream)) {
				_graphics.DrawImage(bitmap, p);
				//this._graphics.DrawRectangle(new Pen(Color.Red, 1), rect);
			}
		}


		/// <summary>
		/// Draws the specified stream of a bitmap in fullsize..
		/// </summary>
		/// <param name="stream">The bitmap stream to draw.</param>
		public void DrawStreamFullSize(Stream stream) {
			Rectangle rect = new Rectangle(0, 0, Width, Height);
			DrawStream(stream, rect);
		}

		/// <summary>
		/// Draws a rectangle with rounded corners and a linear gradient.
		/// </summary>
		/// <param name="xpos">the X position.</param>
		/// <param name="ypos">the Y position.</param>
		/// <param name="width">the width.</param>
		/// <param name="height">the height.</param>
		/// <param name="radius">the corners radius.</param>
		/// <param name="startColor">the startcolor.</param>
		/// <param name="endColor">the endcolor.</param>
		public GraphicsPath DrawRoundedRectangleWithLinearGradient(int xpos, int ypos, int width, int height, int radius, Color startColor, Color endColor) {
			return DrawRoundedRectangleWithLinearGradient(xpos, ypos, width, height, radius, startColor, endColor, 0, Color.Transparent);
		}

		/// <summary>
		/// Draws a rectangle with rounded corners and a linear gradient.
		/// </summary>
		/// <param name="xpos">the X position.</param>
		/// <param name="ypos">the Y position.</param>
		/// <param name="width">the width.</param>
		/// <param name="height">the height.</param>
		/// <param name="radius">the corners radius.</param>
		/// <param name="startColor">the startcolor.</param>
		/// <param name="endColor">the endcolor.</param>
		/// <param name="borderWidth">Width of the border.</param>
		/// <param name="borderColor">Color of the border.</param>
		/// <returns></returns>
		public GraphicsPath DrawRoundedRectangleWithLinearGradient(int xpos, int ypos, int width, int height, int radius, Color startColor, Color endColor, int borderWidth, Color borderColor) {
			Rectangle rect = new Rectangle(xpos, ypos, width, height);
			GraphicsPath path = rect.GetRoundedPath(radius);
			using (LinearGradientBrush brush = new LinearGradientBrush(rect, startColor, endColor, LinearGradientMode.Vertical)) {
				_graphics.FillPath(brush, path);
			}
			if (borderWidth > 0) {
				using (var pen = new Pen(borderColor, borderWidth)) {
					_graphics.DrawPath(pen, path);
				}
			}
			return path;
		}

		/// <summary>
		/// Draws the rectangle.
		/// </summary>
		/// <param name="xpos">The xpos.</param>
		/// <param name="ypos">The ypos.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="color">The color.</param>
		/// <param name="borderWidth">Width of the border.</param>
		/// <param name="borderColor">Color of the border.</param>
		public void DrawRectangle(int xpos, int ypos, int width, int height, Color color, int borderWidth, Color borderColor) {
			using (SolidBrush brush = new SolidBrush(color)) {
				_graphics.FillRectangle(brush, xpos, ypos, width, height);
				if (borderWidth > 0) {
					using (var pen = new Pen(borderColor, borderWidth)) {
						_graphics.DrawRectangle(pen, xpos, ypos, width, height);
					}
				}
			}
		}


		/// <summary>
		/// Draws a circle with rounded corners and a linear gradient.
		/// </summary>
		/// <param name="xpos">the X position.</param>
		/// <param name="ypos">the Y position.</param>
		/// <param name="radius">the radius.</param>
		/// <param name="startColor">the startcolor.</param>
		/// <param name="endColor">the endcolor.</param>
		/// <returns></returns>
		public GraphicsPath DrawCircleWithLinearGradient(int xpos, int ypos, int radius, Color startColor, Color endColor) {
			return DrawCircleWithLinearGradient(xpos, ypos, radius, startColor, endColor, 0, Color.Transparent);
		}


		/// <summary>
		/// Draws a circle with rounded corners and a linear gradient.
		/// </summary>
		/// <param name="xpos">the X position.</param>
		/// <param name="ypos">the Y position.</param>
		/// <param name="radius">the radius.</param>
		/// <param name="startColor">the startcolor.</param>
		/// <param name="endColor">the endcolor.</param>
		/// <param name="borderWidth">Width of the border.</param>
		/// <param name="borderColor">Color of the border.</param>
		/// <returns></returns>
		public GraphicsPath DrawCircleWithLinearGradient(int xpos, int ypos, int radius, Color startColor, Color endColor, int borderWidth, Color borderColor) {
			GraphicsPath circle = null;
			try {
				circle = new GraphicsPath();
				Rectangle rect = new Rectangle(xpos, ypos, radius, radius);
				circle.StartFigure();
				circle.AddArc(xpos, ypos, radius, radius, 0, 360);
				circle.CloseFigure();

				using (LinearGradientBrush brush = new LinearGradientBrush(rect, startColor, endColor, LinearGradientMode.Vertical)) {
					_graphics.FillPath(brush, circle);
				}

				if (borderWidth > 0) {
					using (var pen = new Pen(borderColor, borderWidth)) {
						_graphics.DrawPath(pen, circle);
					}
				}
				return circle;
			}
			finally {
				if (circle != null)
					circle.Dispose();
			}
		}


		/// <summary>
		/// Sets the clipping region of this System.Drawing.Graphics to the Clip property
		/// of the specified System.Drawing.Graphics.
		/// </summary>
		/// <param name="path">System.Drawing.GraphicsPath from which to take the new clip region</param>
		public void SetClip(GraphicsPath path) {
			_graphics.SetClip(path);
		}

		/// <summary>
		/// Resets the clip region of this System.Drawing.Graphics to an infinite region.
		/// </summary>
		public void ResetClip() {
			_graphics.ResetClip();
		}

		/// <summary>
		/// Returns the current bitmap as a MemoryStream.
		/// </summary>
		/// <returns></returns>
		public MemoryStream GetStream() {
			MemoryStream retVal = new MemoryStream();
			_graphics.Flush();
			_bitmap.Save(retVal, ImageFormat.Png);
			retVal.Position = 0;
			return retVal;
		}
	}


	/// <summary>
	/// Encapsulates extension for the Rectangle class.
	/// </summary>
	public static class RectangleExtensions {

		/// <summary>
		/// Return a rectangular path with rounded angles.
		/// </summary>
		/// <param name="rect">the base rectangle.</param>
		/// <param name="radius">the radius of the rounded corners.</param>
		/// <returns></returns>
		public static GraphicsPath GetRoundedPath(this Rectangle rect, int radius) {
			GraphicsPath retVal = CreateRoundedPath(rect.X, rect.Y, rect.Width, rect.Height, radius, radius <= 0 ? RectangleCorners.None : RectangleCorners.All);
			return retVal;
		}

		[Flags]
		private enum RectangleCorners {
			None = 0,
			TopLeft = 1,
			TopRight = 2,
			BottomLeft = 4,
			BottomRight = 8,
			All = TopLeft | TopRight | BottomLeft | BottomRight
		}

		/// <summary>
		/// Return a rectangular path with rounded angles.
		/// </summary>
		/// <param name="x">The x position.</param>
		/// <param name="y">The y position.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="radius">The radius of the rounded corners.</param>
		/// <param name="corners">The corners to round.</param>
		/// <returns></returns>
		private static GraphicsPath CreateRoundedPath(int x, int y, int width, int height, int radius, RectangleCorners corners) {
			int xw = x + width;
			int yh = y + height;
			int xwr = xw - radius;
			int yhr = yh - radius;
			int xr = x + radius;
			int yr = y + radius;
			int r2 = radius * 2;
			int xwr2 = xw - r2;
			int yhr2 = yh - r2;

			using (GraphicsPath p = new GraphicsPath()) {
				p.StartFigure();

				//Top Left Corner
				if ((RectangleCorners.TopLeft & corners) == RectangleCorners.TopLeft) {
					p.AddArc(x, y, r2, r2, 180, 90);
				}
				else {
					p.AddLine(x, yr, x, y);
					p.AddLine(x, y, xr, y);
				}

				//Top Edge
				p.AddLine(xr, y, xwr, y);

				//Top Right Corner
				if ((RectangleCorners.TopRight & corners) == RectangleCorners.TopRight) {
					p.AddArc(xwr2, y, r2, r2, 270, 90);
				}
				else {
					p.AddLine(xwr, y, xw, y);
					p.AddLine(xw, y, xw, yr);
				}

				//Right Edge
				p.AddLine(xw, yr, xw, yhr);

				//Bottom Right Corner
				if ((RectangleCorners.BottomRight & corners) == RectangleCorners.BottomRight) {
					p.AddArc(xwr2, yhr2, r2, r2, 0, 90);
				}
				else {
					p.AddLine(xw, yhr, xw, yh);
					p.AddLine(xw, yh, xwr, yh);
				}

				//Bottom Edge
				p.AddLine(xwr, yh, xr, yh);

				//Bottom Left Corner
				if ((RectangleCorners.BottomLeft & corners) == RectangleCorners.BottomLeft) {
					p.AddArc(x, yhr2, r2, r2, 90, 90);
				}
				else {
					p.AddLine(xr, yh, x, yh);
					p.AddLine(x, yh, x, yhr);
				}

				//Left Edge
				p.AddLine(x, yhr, x, yr);

				p.CloseFigure();
				return p;
			}
		}
	}
}

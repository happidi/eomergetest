﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Chart DrillDown parameters.
	/// </summary>
	[Key("KeyChartDrillDown")]
	[DataContract]
	[Serializable]
	public class ChartDrillDown : BaseBusinessEntity {

		/// <summary>
		/// Key of the Chart Drill Down.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChartDrillDown { get; set; }

		/// <summary>
		/// Key of the Chart.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="ChartDrillDown"/> is enabled.
		/// </summary>
		[DataMember]
		public bool Enabled { get; set; }

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		[DataMember]
		public DateTime? StartDate { get; set; }

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		[DataMember]
		public DateTime? EndDate { get; set; }
		/// <summary>
		/// Gets or sets the chart localisation
		/// </summary>
		[DataMember]
		public ChartLocalisation Localisation { get; set; }

		/// <summary>
		/// Gets or sets the chart localisation site level
		/// </summary>
		[DataMember]
		public int LocalisationSiteLevel { get; set; }

		/// <summary>
		/// Gets or sets the chart classification level
		/// </summary>
		[DataMember]
		public int ClassificationLevel { get; set; }

		/// <summary>
		/// Gets or sets the chart default timeinterval
		/// </summary>
		[DataMember]
		public AxisTimeInterval TimeInterval { get; set; }

		/// <summary>
		/// Gets or sets the key location.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocation { get; set; }

		/// <summary>
		/// Gets or sets the key classification item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItem { get; set; }

	}
}

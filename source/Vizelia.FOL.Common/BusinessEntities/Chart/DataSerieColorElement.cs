﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity representing a color element rule for a  DataSerie.
	/// </summary>
	[Key("KeyDataSerieColorElement")]
	[DataContract]
	[Serializable]
	[IconCls("viz-icon-small-dataseriepsetratio")]
	public class DataSerieColorElement : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Key of the Entity
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyDataSerieColorElement { get; set; }

		/// <summary>
		/// The key of the DataSerie this filter is attached too.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyDataSerie { get; set; }

		/// <summary>
		/// Gets or sets the color.
		/// </summary>
		/// <value>
		/// The color.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Color { get; set; }

		/// <summary>
		/// Gets or sets the highlight method.
		/// </summary>
		/// <value>
		/// The highlight.
		/// </value>
		[DataMember]
		public DataPointHighlight Highlight { get; set; }

		/// <summary>
		/// Gets or sets the element min value.
		/// </summary>
		/// <value>
		/// The element min value.
		/// </value>
		[DataMember]
		public double? MinValue { get; set; }

		/// <summary>
		/// Gets or sets the element max value.
		/// </summary>
		/// <value>
		/// The element max value.
		/// </value>
		[DataMember]
		public double? MaxValue { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }
	}
}

﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for  Palette Color
	/// </summary>
	[DataContract]
	[Key("KeyPaletteColor")]
	[Serializable]
	public class PaletteColor : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Id of the  Palette Color.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPaletteColor { get; set; }
		
		/// <summary>
		/// Id of the  Palette.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPalette { get; set; }

		/// <summary>
		/// Order of the Color in the Palette
		/// </summary>
		[DataMember]
		public int Order { get; set; }

		/// <summary>
		/// Red color composant.
		/// </summary>
		[DataMember]
		public int ColorR { get; set; }

		/// <summary>
		/// Green color composant.
		/// </summary>
		[DataMember]
		public int ColorG { get; set; }

		/// <summary>
		/// Blue color composant.
		/// </summary>
		[DataMember]
		public int ColorB { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId { get; set; }
	}
}

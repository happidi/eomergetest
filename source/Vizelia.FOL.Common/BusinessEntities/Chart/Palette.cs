﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for  Palette
	/// </summary>
	[DataContract]
	[Key("KeyPalette")]
	[Serializable]
	[LocalizedText("msg_palette")]
	[IconCls("viz-icon-small-palette")]
	public class Palette : BaseBusinessEntity, IMappableEntity { //ISecurableObject,

		/// <summary>
		/// Id of the  Palette.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		//[FilterType(typeof(Palette))]
		public string KeyPalette { get; set; }

		/// <summary>
		/// Gets or sets the name of the localized label of the  Palette.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MsgCode { get; set; }

		/// <summary>
		/// Gets the localized label of the   Palette.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Label {
			get {
				return Helper.LocalizeText(MsgCode);
			}
			set { }
		}

		/// <summary>
		/// List of  Palette Colors.
		/// </summary>
		[DataMember]
		public List<PaletteColor> Colors { get; set; }


		///// <summary>
		///// Returns the SecurableEntity object.
		///// </summary>
		///// <returns>
		///// The SecurableEntity object.
		///// </returns>
		//public SecurableEntity GetSecurableObject() {
		//    return new SecurableEntity() {
		//        KeySecurable = KeyPalette,
		//        SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(this.GetType()),
		//        SecurableName = this.Label,
		//        SecurableTypeName = this.GetType().FullName
		//    };
		//}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Playlist.
	/// </summary>
	[Serializable]
	[Key("KeyPlaylist")]
	[DataContract]
	[IconCls("viz-icon-small-playlist")]
	[LocalizedText("msg_playlist")]
	public class Playlist : BaseBusinessEntity, ISecurableObject, IMappableEntity {


		/// <summary>
		/// Gets the application.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Application { get; set; }

		/// <summary>
		/// Key of the Playlist.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(Playlist), false)]
		public string KeyPlaylist { get; set; }

		/// <summary>
		/// Gets or sets the Name.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the Key of the LocalizationCulture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocalizationCulture { get; set; }

		/// <summary>
		/// Gets or sets the width of the Playlist.
		/// </summary>
		[DataMember]
		public int Width { get; set; }

		/// <summary>
		/// Gets or sets the width of the Playlist.
		/// </summary>
		[DataMember]
		public int Height { get; set; }


		/// <summary>
		/// List of  Playlist Page.
		/// </summary>
		[DataMember]
		public List<PlaylistPage> Pages { get; set; }


		/// <summary>
		/// The VirtualFile of the Chart.
		/// </summary>
		[DataMember]
		public VirtualFile VirtualFile { get; set; }

		/// <summary>
		/// Gets the total duration of the Pages collection.
		/// </summary>
		[DataMember]
		public int TotalDuration {
			get {
				int retVal = 0;
				if (Pages != null) {
					retVal = Pages.Sum(x => x.Duration);
				}
				return retVal;
			}
			set { }
		}


		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity {
				KeySecurable = KeyPlaylist,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(GetType()),
				SecurableName = Name,
				SecurableTypeName = GetType().FullName
			};
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets the virtual file path.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string VirtualFilePath { get; set; }
	}
}

﻿using System;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Chart Marker
	/// </summary>
	[Key("KeyChartMarker")]
	[DataContract]
	[Serializable]
	public class ChartMarker : BaseBusinessEntity, IMappableEntity {
		/// <summary>
		/// Key of the Chart Marker.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChartMarker { get; set; }

		/// <summary>
		/// Key of the Chart Axis.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChartAxis { get; set; }

		/// <summary>
		/// Name of the Chart Axis.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ChartAxisName { get; set; }


		/// <summary>
		/// Gets or sets the name of the marker.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the minimum value of the marker.
		/// </summary>
		[DataMember]
		public double? Min { get; set; }

		/// <summary>
		/// Gets or sets the maximum value of the marker.
		/// </summary>
		[DataMember]
		public double? Max { get; set; }


		/// <summary>
		/// Red color composant.
		/// </summary>
		[DataMember]
		public int? ColorR { get; set; }

		/// <summary>
		/// Green color composant.
		/// </summary>
		[DataMember]
		public int? ColorG { get; set; }

		/// <summary>
		/// Blue color composant.
		/// </summary>
		[DataMember]
		public int? ColorB { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Chart.
	/// </summary>
	[Serializable]
	[DataContract]
	public class Analytics : BaseBusinessEntity {
		/// <summary>
		/// Initializes a new instance of the <see cref="Analytics"/> class.
		/// </summary>
		public Analytics() {

		}

		/// <summary>
		/// Gets or sets the key analytics.
		/// </summary>
		/// <value>
		/// The key analytics.
		/// </value>
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyAnalytics { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		[DataMember]
		public string Title { get; set; }
		
		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		[DataMember]
		public DateTime? StartDate { get; set; }

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		[DataMember]
		public DateTime? EndDate { get; set; }

		/// <summary>
		/// Gets or sets the chart localisation
		/// </summary>
		[DataMember]
		public ChartLocalisation? Localisation { get; set; }

		/// <summary>
		/// Gets or sets the chart localisation site level
		/// </summary>
		[DataMember]
		public int? LocalisationSiteLevel { get; set; }

		/// <summary>
		/// Gets or sets the chart classification level
		/// </summary>
		[DataMember]
		public int? ClassificationLevel { get; set; }

		/// <summary>
		/// Gets or sets the time zone id in which we want to render the chart.
		/// </summary>
		/// <value>
		/// The time zone id.
		/// </value>
		[DataMember]
		public string TimeZoneId { get; set; }

		/// <summary>
		/// Gets or sets the chart default timeinterval
		/// </summary>
		[DataMember]
		public AxisTimeInterval? TimeInterval { get; set; }

		/// <summary>
		/// Gets or sets the filter spatial keys.
		/// </summary>
		/// <value>
		/// The filter spatial keys.
		/// </value>
		[DataMember]
		public List<string> FilterSpatialKeys { get; set; }


		/// <summary>
		///  Gets or sets the filter spatial local id's keys.
		/// </summary>
		[DataMember]
		public List<string> FilterSpatialLocalIds { get; set; }

		/// <summary>
		/// Gets or sets the meters classifications keys.
		/// </summary>
		/// <value>
		/// The meters classifications keys.
		/// </value>
		[DataMember]
		public List<string> MetersClassificationsKeys { get; set; }
	
		/// <summary>
		/// Gets or sets the meters keys.
		/// </summary>
		/// <value>
		/// The meters keys.
		/// </value>
		[DataMember]
		public List<string> MetersKeys { get; set; }

		/// <summary>
		/// Gets or sets the historical psets.
		/// </summary>
		/// <value>
		/// The historical psets.
		/// </value>
		[DataMember]
		public List<ChartPsetAttributeHistorical> HistoricalPsets { get; set; }
	}
}


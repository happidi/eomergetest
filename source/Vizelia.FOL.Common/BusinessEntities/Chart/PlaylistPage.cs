﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Playlist Page.
	/// </summary>
	[Serializable]
	[Key("KeyPlaylistPage")]
	[DataContract]
	public class PlaylistPage : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Key of the Playlist.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPlaylistPage { get; set; }


		/// <summary>
		/// Key of the Playlist.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPlaylist { get; set; }

		/// <summary>
		/// Gets or sets the order of the page in the playlist.
		/// </summary>
		[DataMember]
		public int Order { get; set; }


		/// <summary>
		/// Gets or sets the duration in second the page will be displayed in the playlist.
		/// </summary>
		[DataMember]
		public int Duration { get; set; }

		/// <summary>
		/// Gets or sets the Key of the PortalTab the page is representing.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPortalTab { get; set; }

		/// <summary>
		/// Gets  the title of the PortalTab.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PortalTabTitle { get; set; }

		/// <summary>
		/// Gets  the title of the parent PortalWindow.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PortalWindowTitle { get; set; }

		/// <summary>
		/// Gets or sets the width of the Playlist.
		/// </summary>
		[DataMember]
		public int PlaylistWidth { get; set; }

		/// <summary>
		/// Gets or sets the width of the Playlist.
		/// </summary>
		[DataMember]
		public int PlaylistHeight { get; set; }


		/// <summary>
		/// Gets or sets the key of the document storing the last iamge representation of this page.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyDocument { get; set; }


		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }



	}
}

﻿using System;
using System.Collections.Generic;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Represents a Collection of StreamResult with fast access by dimensions.
	/// </summary>
	[Serializable]
	public class StreamResultCollection:ObjectResultCollection<StreamResult> {

		/// <summary>
		/// Tries the get streamresult.
		/// </summary>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="streamResult">The stream result.</param>
		/// <returns></returns>
		public bool TryGetStreamResult(int width, int height, out StreamResult streamResult) {
			return TryGetResult(width, height, out streamResult);
		}

	}


	/// <summary>
	/// Generic collection with fast access by dimensions.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[Serializable]
	public class ObjectResultCollection<T> {

		/// <summary>
		/// Private dictionary to store the list of StreamResult.
		/// </summary>
		internal Dictionary<Tuple<int, int>, T> m_Dictionary;

		/// <summary>
		/// Initializes a new instance of the <see cref="DataSerieCollection"/> class.
		/// </summary>
		public ObjectResultCollection() {
			m_Dictionary = new Dictionary<Tuple<int, int>, T>();
		}

		/// <summary>
		/// Gets the tuple.
		/// </summary>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		private Tuple<int, int> GetTuple(int width, int height) {
			var tuple = Tuple.Create(width, height);
			return tuple;
		}

		/// <summary>
		/// Adds the specified streamresult to the collection.
		/// </summary>
		/// <param name="result">The result.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public bool Add(T result, int width, int height) {
			var retVal = false;
			var tuple = GetTuple(width, height);
			if (!m_Dictionary.ContainsKey(tuple)) {
				m_Dictionary.Add(tuple, result);
				retVal = true;
			}
			return retVal;
		}



		/// <summary>
		/// Clears this instance.
		/// </summary>
		public void Clear() {
			m_Dictionary.Clear();
		}


		/// <summary>
		/// Tries the get streamresult.
		/// </summary>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="result">The result.</param>
		/// <returns></returns>
		public bool TryGetResult(int width, int height, out T result) {
			var tuple = GetTuple(width, height);
			if (m_Dictionary.TryGetValue(tuple, out result)) {
				return true;
			}
			return false;

		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using System.Drawing;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common.Validators;


namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Series of data point.
	/// </summary>
	[Key("KeyDataSerie")]
	[DataContract]
	[Serializable]
	[IconCls("viz-icon-small-dataserie-areaspline")]
	public class DataSerie : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Contructor
		/// </summary>
		public DataSerie() {
			Hidden = false;
			DisplayLegendEntry = DataSerieLegendEntry.SUM;
			Points = new List<DataPoint>();
			MDData = new List<MD>();
			ColorElements = new List<DataSerieColorElement>();
			PsetRatios = new List<DataSeriePsetRatio>();
		}

		/// <summary>
		/// Key of the DataSerie.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyDataSerie { get; set; }

		/// <summary>
		/// Key of the Chart.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		[ContainsCharactersValidator("\"", ContainsCharacters.Any, Negated = true)]
		[ChartDynamicDisplay]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the Key of the Meter Classification Item the series applys to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItem { get; set; }


		/// <summary>
		/// The key path of the classification of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItemPath { get; set; }

		/// <summary>
		/// The classification title of the Meter Classification Item the series applys to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemTitle { get; set; }

		/// <summary>
		/// The classification path of the Meter Classification Item the series applys to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLongPath { get; set; }

		/// <summary>
		/// The classification local id path of the Meter Classification Item the series applys to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLocalIdPath { get; set; }


		/// <summary>
		/// Gets or sets the list of MD associated to this DataSerie. (used in EA).
		/// </summary>
		public List<MD> MDData { get; set; }

		/// <summary>
		/// Gets or sets the list of data points.
		/// </summary>
		[DataMember]
		[ViewableByDataModel]
		public List<DataPoint> Points { get; set; }


		/// <summary>
		/// Gets or sets the mathematical GroupOperation
		/// </summary>
		[DataMember]
		public MathematicOperator GroupOperation { get; set; }

		/// <summary>
		/// Gets or sets the DataSerieType.
		/// </summary>
		[DataMember]
		public DataSerieType Type { get; set; }

		/// <summary>
		/// Gets or sets the Transparency 
		/// </summary>
		[DataMember]
		public int? DisplayTransparency { get; set; }

		/// <summary>
		/// Gets or sets the LegendEntry value 
		/// </summary>
		[DataMember]
		public DataSerieLegendEntry DisplayLegendEntry { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether we [hide legend entry].
		/// </summary>
		[DataMember]
		public bool HideLegendEntry { get; set; }

		/// <summary>
		/// Gets or sets the Unit 
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string Unit { get; set; }

		/// <summary>
		/// Red color composant.
		/// </summary>
		[DataMember]
		public int? ColorR { get; set; }

		/// <summary>
		/// Green color composant.
		/// </summary>
		[DataMember]
		public int? ColorG { get; set; }

		/// <summary>
		/// Blue color composant.
		/// </summary>
		[DataMember]
		public int? ColorB { get; set; }

		/// <summary>
		/// Key of the Y Axis of the Serie
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyYAxis { get; set; }

		/// <summary>
		/// Name of the Y Axis.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string YAxisName { get; set; }


		/// <summary>
		/// Gets or sets the limit element min value.
		/// </summary>
		/// <value>
		/// The limit element min value.
		/// </value>
		[DataMember]
		public double? LimitElementMinValue { get; set; }

		/// <summary>
		/// Gets or sets the limit element max value.
		/// </summary>
		/// <value>
		/// The limit element max value.
		/// </value>
		[DataMember]
		public double? LimitElementMaxValue { get; set; }


		/// <summary>
		/// Gets or sets the min limit to filter series by their legend entry value.
		/// </summary>
		/// <value>
		/// The limit element min value.
		/// </value>
		[DataMember]
		public double? LimitByLegendEntryMinValue { get; set; }

		/// <summary>
		/// Gets or sets the max limit to filter series by their legend entry value.
		/// </summary>
		/// <value>
		/// The limit element max value.
		/// </value>
		[DataMember]
		public double? LimitByLegendEntryMaxValue { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether we replace the value of each datapoint by 1, in order to count them.
		/// </summary>
		[DataMember]
		public bool LimitElementCount { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether we limit elements before or after time interval grouping.
		/// </summary>
		[DataMember]
		public bool LimitElementRawData { get; set; }

		/// <summary>
		/// Gets or sets the target.
		/// </summary>
		/// <value>
		/// The target.
		/// </value>
		[DataMember]
		public double? Target { get; set; }

		/// <summary>
		/// Gets or sets the target.
		/// </summary>
		/// <value>
		/// The target.
		/// </value>
		[DataMember]
		public double? TargetElement { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="DataSerie"/> is hidden.
		/// </summary>
		/// <value>
		///   <c>true</c> if hidden; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		[ViewableByDataModel]
		public bool Hidden { get; set; }

		/// <summary>
		/// True if the series is a GroupName Series, false otherwise.
		/// </summary>
		[DataMember]
		public bool IsClassificationItem {
			get {
				if (!string.IsNullOrEmpty(KeyClassificationItem)) {
					return true;
				}
				return false;
			}
			private set {

			}
		}

		/// <summary>
		/// A division ratio applied to a the DataSerie.
		/// </summary>
		[DataMember]
		public double? Ratio { get; set; }


		/// <summary>
		/// Gets or sets the pset ratios.
		/// </summary>
		/// <value>
		/// The pset ratios.
		/// </value>
		[DataMember]
		public List<DataSeriePsetRatio> PsetRatios { get; set; }

		/// <summary>
		/// Collection of color rule for the datapoint collection.
		/// </summary>
		[DataMember]
		public List<DataSerieColorElement> ColorElements { get; set; }
		/// <summary>
		/// Gets the list of points with an YValue
		/// </summary>
		/// <returns></returns>
		[DataMember]
		public List<DataPoint> PointsWithValue {
			get {
				if (Points != null) {
					List<DataPoint> pointsWithValues = Points.Where(p => p.YValue.HasValue).ToList();
					return pointsWithValues;
				}
				return new List<DataPoint>();
			}
			private set {

			}
		}



		/// <summary>
		/// True if this is a datetime serie, false if it s a category serie.
		/// </summary>
		public bool IsDateTime {
			get {
				if (Points != null && Points.Count > 0) {
					return Points[0].XDateTime.HasValue;
				}
				return false;
			}
			private set { }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string LocalId { get; set; }


		/// <summary>
		/// Gets the IconCls.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconCls {
			get {
				string retVal = Helper.GetAttributeValue<IconClsAttribute>(GetType());
				if (IsClassificationItem) {
					retVal = "viz-icon-small-classificationitem";
				}
				else if (Type != DataSerieType.None) {
					retVal = EnumExtension.GetEnumValueIconCls(typeof(DataSerieType), (int)Type);
				}
				return retVal;
			}
			private set {
			}
		}

		/// <summary>
		/// Return the Series color.
		/// </summary>
		public Color Color {
			get {
				var retVal = Color.Empty;
				if (ColorR.HasValue && ColorG.HasValue && ColorB.HasValue) {
					retVal = Color.FromArgb(ColorR.Value, ColorG.Value, ColorB.Value);
				}
				return retVal;
			}
			set {
				this.ColorR = value.R;
				this.ColorG = value.G;
				this.ColorB = value.B;
			}
		}
		/// <summary>
		/// Gets or sets the name of the final location. (based on the Chart Localisation)
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationName { get; set; }


		/// <summary>
		/// Gets or sets the key of the final location. (based on the Chart Localisation)
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocation { get; set; }


		/// <summary>
		/// Gets or sets the name of the final Classification. (based on the Chart Classification level)
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationName { get; set; }

		/// <summary>
		/// Gets or sets the key of the final Classification, used in drill down.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationDrillDown { get; set; }

		/// <summary>
		/// Gets or sets the key of the meter if the Chart has Localistion=ByMeter
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyMeter { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether we hide the values label.
		/// </summary>
		/// <value>
		///   <c>true</c> if [hide label]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool HideLabel { get; set; }

		/// <summary>
		/// Gets or sets the line dash style.
		/// </summary>
		/// <value>
		/// The line dash style.
		/// </value>
		[DataMember]
		public LineDashStyle LineDashStyle { get; set; }

		/// <summary>
		/// Gets or sets the width of the line.
		/// </summary>
		/// <value>
		/// The width of the line.
		/// </value>
		[DataMember]
		public int? LineWidth { get; set; }

		/// <summary>
		/// Gets or sets the size of the marker.
		/// </summary>
		/// <value>
		/// The size of the marker.
		/// </value>
		[DataMember]
		public int? MarkerSize { get; set; }

		/// <summary>
		/// Gets or sets the type of the marker.
		/// </summary>
		/// <value>
		/// The type of the marker.
		/// </value>
		[DataMember]
		public MarkerType MarkerType { get; set; }

		/// <summary>
		/// Gets or sets the zindex.
		/// </summary>
		/// <value>
		/// The zindex.
		/// </value>
		[DataMember]
		public int? ZIndex { get; set; }

		/// <summary>
		/// Gets a value indicating whether this instance has points with value.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance has points with value; otherwise, <c>false</c>.
		/// </value>
		public bool HasPointsWithValue {
			get {
				bool has = Points != null && Points.Any(p => p.YValue.HasValue);
				return has;
			}
		}

		/// <summary>
		/// Copies this Serie.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <param name="copyDataPoints">if set to <c>true</c> [copy data points].</param>
		/// <returns></returns>
		public DataSerie Copy(string localId = null, bool copyDataPoints = true) {
			var retVal = new DataSerie {
				KeyChart = KeyChart,
				Name = Name,
				LocalId = localId ?? LocalId,
				GroupOperation = GroupOperation,
				Unit = Unit,
				Hidden = Hidden,
				DisplayLegendEntry = DisplayLegendEntry,
				DisplayTransparency = DisplayTransparency,
				Type = Type,
				KeyYAxis = KeyYAxis,
				ColorR = ColorR,
				ColorG = ColorG,
				ColorB = ColorB,
				Ratio = Ratio,
				Target = Target,
				LimitByLegendEntryMaxValue = LimitByLegendEntryMaxValue,
				LimitByLegendEntryMinValue = LimitByLegendEntryMinValue,
				LimitElementMaxValue = LimitElementMaxValue,
				LimitElementMinValue = LimitElementMinValue,
				LimitElementCount = LimitElementCount,
				LimitElementRawData = LimitElementRawData,
				YAxisName = YAxisName,
				LocationName = LocationName,
				KeyLocation = KeyLocation,
				KeyClassificationItem = KeyClassificationItem,
				KeyClassificationItemPath = KeyClassificationItemPath,
				HideLegendEntry = HideLegendEntry,
				ClassificationItemTitle = ClassificationItemTitle,
				ClassificationItemLongPath = ClassificationItemLongPath,
				ClassificationItemLocalIdPath = ClassificationItemLocalIdPath,
				PsetRatios = PsetRatios,
				ColorElements = ColorElements,
				KeyClassificationDrillDown = KeyClassificationDrillDown,
				KeyMeter = KeyMeter,
				HideLabel = HideLabel,
				LineDashStyle = LineDashStyle,
				LineWidth = LineWidth,
				MarkerType = MarkerType,
				MarkerSize = MarkerSize,
				ZIndex = ZIndex
			};
			if (Points != null && Points.Count > 0 && copyDataPoints) {
				retVal.Points = new List<DataPoint>();
				Points.ForEach(p => retVal.Points.Add(p.Copy()));
			}
			else {
				retVal.Points = new List<DataPoint>();
			}
			return retVal;
		}
	}
}

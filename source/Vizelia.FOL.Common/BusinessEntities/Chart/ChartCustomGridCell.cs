﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Chart Custom Grid Cell.
	/// </summary>
	[Serializable]
	[Key("KeyChartCustomGridCell")]
	[DataContract]
	public class ChartCustomGridCell : BaseBusinessEntity {

		/// <summary>
		/// Key of the ChartCustomGridCell.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChartCustomGridCell { get; set; }

		/// <summary>
		/// Key of the Chart.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }

		/// <summary>
		/// Gets or sets the index of the row.
		/// </summary>
		/// <value>
		/// The index of the row.
		/// </value>
		[DataMember]
		public int RowIndex { get; set; }

		/// <summary>
		/// Gets or sets the index of the column.
		/// </summary>
		/// <value>
		/// The index of the column.
		/// </value>
		[DataMember]
		public int ColumnIndex { get; set; }

		/// <summary>
		/// Gets or sets the tag.
		/// </summary>
		/// <value>
		/// The tag.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Tag { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Value { get; set; }
	}
}

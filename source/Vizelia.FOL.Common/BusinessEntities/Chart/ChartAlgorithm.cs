﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity representing an Algorithm attached to a Chart.
	/// </summary>
	[Key("KeyChartAlgorithm")]
	[DataContract]
	[Serializable]
	public class ChartAlgorithm : BaseBusinessEntity {
		/// <summary>
		/// Key of the ChartAlgorithm.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChartAlgorithm { get; set; }
		
		/// <summary>
		/// Key of the Chart.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }


		/// <summary>
		/// Key of the Algorithm.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyAlgorithm { get; set; }


		/// <summary>
		/// Order of the Algorithm.
		/// </summary>
		[DataMember]
		public int Order { get; set; }

		/// <summary>
		/// Gets or sets the Algorithm.
		/// </summary>
		/// <value>
		/// The script.
		/// </value>
		[DataMember]
		public Algorithm Algorithm { get; set; }


		/// <summary>
		/// Key of the Algorithm.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AlgorithmName { get; set; }

		/// <summary>
		/// Gets or sets the Algorithm input values.
		/// </summary>
		/// <value>
		/// The Algorithm input values.
		/// </value>
		[DataMember]
		public List<AlgorithmInputValue> AlgorithmInputValues { get; set; }


		/// <summary>
		/// Gets the Algorithm input values dictionary.
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, string> GetAlgorithmInputValuesDictionary() {
			var inputs = new Dictionary<string, string>();

			if (AlgorithmInputValues != null) {
				foreach (var input in AlgorithmInputValues) {
					inputs[input.Name] =  input.Value;
				}
			}

			return inputs;
		}
	}
}

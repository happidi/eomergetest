﻿using System;
using System.Drawing;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a data point.
	/// </summary>
	[DataContract]
	[Serializable]
	[Key("KeyDataPoint")]
	[IconCls("viz-icon-small-dataserie")]
	public class DataPoint : BaseBusinessEntity {
		
		/// <summary>
		/// Initializes a new instance of the <see cref="DataPoint"/> class.
		/// </summary>
		public DataPoint() {
			KeyDataPoint = Guid.NewGuid().ToString();
		}

		/// <summary>
		/// Gets or sets the key data point.
		/// This key is fictive and not used for persistance. It is only used for performance considerations
		/// in the ExtJS client.
		/// </summary>
		/// <value>
		/// The key data point.
		/// </value>
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyDataPoint { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the name of the parent series.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string SeriesName { get; set; }

		/// <summary>
		/// Determines the position of the DataPoint on axis X.
		/// </summary>
		[DataMember]
		[ViewableByDataModel]
		public DateTime? XDateTime { get; set; }

		/// <summary>
		/// Determines the position of the DataPoint on axis Y.
		/// </summary>
		[DataMember]
		public DateTime? YDateTime { get; set; }

		/// <summary>
		/// Determines the position of the DataPoint on axis X.
		/// </summary>
		[DataMember]
		public double? XValue { get; set; }

		/// <summary>
		/// Determines the position of the DataPoint on axis Y.
		/// </summary>
		[DataMember]
		[ViewableByDataModel]
		public double? YValue { get; set; }

		/// <summary>
		/// Determines the position of the DataPoint on axis Y when we want to display an area.
		/// </summary>
		[DataMember]
		public double? YValueStart { get; set; }

		/// <summary>
		/// Gets or sets the overriden Y value (use in specific analysis).
		/// </summary>
		/// <value>
		/// The overriden Y value.
		/// </value>
		[DataMember]
		public double? OverridenYValue { get; set; }

		/// <summary>
		/// Specific color for this particular point. Used in specific analysis.
		/// </summary>
		[DataMember]
		[ViewableByDataModel]
		public Color? Color { get; set; }

		/// <summary>
		/// Gets or sets the highlight.
		/// </summary>
		/// <value>
		/// The highlight.
		/// </value>
		[DataMember]
		public DataPointHighlight Highlight { get; set; }

		/// <summary>
		/// Gets or sets the color of the highlight.
		/// </summary>
		/// <value>
		/// The color of the highlight.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string HighlightColor { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="DataPoint"/> is hidden.
		/// </summary>
		/// <value>
		///   <c>true</c> if hidden; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool Hidden { get; set; }

		/// <summary>
		/// Gets or sets the tooltip.
		/// </summary>
		/// <value>
		/// The tooltip.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string Tooltip { get; set; }
		/// <summary>
		/// Copy a DataPoint
		/// </summary>
		/// <returns></returns>
		public DataPoint Copy() {
			return new DataPoint {
				Name = Name,
				Color = Color,
				Hidden = Hidden,
				OverridenYValue = OverridenYValue,
				Tooltip = Tooltip,
				SeriesName = SeriesName,
				XDateTime = XDateTime,
				XValue = XValue,
				YDateTime = YDateTime,
				YValue = YValue,
				YValueStart = YValueStart
			};


		}

		/// <summary>
		/// Gets or sets the classification.
		/// </summary>
		/// <value>
		/// The classification.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Classification { get; set; }

		/// <summary>
		/// Gets or sets the localisation.
		/// </summary>
		/// <value>
		/// The localisation.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Location { get; set; }
	}
}

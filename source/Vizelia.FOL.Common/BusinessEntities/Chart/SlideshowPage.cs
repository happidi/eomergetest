﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Slideshow Page.
	/// </summary>
	[Serializable]
	[DataContract]
	public class SlideshowPage {

		///// <summary>
		///// Gets or sets the stream.
		///// </summary>
		///// <value>
		///// The stream.
		///// </value>
		//public MemoryStream Stream { get; set; }

		/// <summary>
		/// Gets or sets the URL.
		/// </summary>
		/// <value>
		/// The URL.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Url { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the duration. in seconds.
		/// </summary>
		/// <value>
		/// The duration.
		/// </value>
		[DataMember]
		public int Duration { get; set; }


		/// <summary>
		/// Gets or sets the width.
		/// </summary>
		/// <value>
		/// The width.
		/// </value>
		[DataMember]
		public int Width { get; set; }

		/// <summary>
		/// Gets or sets the height.
		/// </summary>
		/// <value>
		/// The height.
		/// </value>
		[DataMember]
		public int Height { get; set; }
	}
}

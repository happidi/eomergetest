﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Chart Axis
	/// </summary>
	[Key("KeyChartAxis")]
	[DataContract]
	[Serializable]
	public class ChartAxis : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Initializes a new instance of the <see cref="ChartAxis"/> class.
		/// </summary>
		public ChartAxis() {
			Markers = new List<ChartMarker>();
		}
		/// <summary>
		/// Key of the Chart Axis.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyChartAxis { get; set; }

		/// <summary>
		/// Key of the Chart.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }

		/// <summary>
		/// Gets or sets the name of the axis.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the minimum value of the axis.
		/// </summary>
		[DataMember]
		public double? Min { get; set; }

		/// <summary>
		/// Gets or sets the maximum value of the axis.
		/// </summary>
		[DataMember]
		public double? Max { get; set; }

		/// <summary>
		/// Gets or sets the markers.
		/// </summary>
		[DataMember]
		public List<ChartMarker> Markers { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this axis is the primary Y axis of the chart.
		/// </summary>
		[DataMember]
		public bool IsPrimary { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether FullStacked.
		/// </summary>
		[DataMember]
		public bool FullStacked { get; set; }
	}
}

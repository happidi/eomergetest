﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a pset filter for the locations attached to a Chart or an AlarmDefinition.
	/// In order for example to select only the buildings that have a PsetAttributeXXX >100
	/// </summary>
	[Key("KeyFilterSpatialPset")]
	[DataContract]
	[Serializable]
	[IconCls("viz-icon-small-filter")]
	public class FilterSpatialPset : BaseBusinessEntity, IMappableEntity {
		/// <summary>
		/// Key of the Entity
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyFilterSpatialPset { get; set; }

		/// <summary>
		/// The key of the Chart this filter is attached too.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }

		/// <summary>
		/// The key of the AlarmDefinition this filter is attached too.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyAlarmDefinition { get; set; }

		/// <summary>
		/// The Name of the Pset to use to filter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string PsetName { get; set; }

		/// <summary>
		/// The Name of the Attribute  to use to filter
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string AttributeName { get; set; }

		/// <summary>
		/// The type of the object the Pset is linked to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string UsageName { get; set; }

		/// <summary>
		/// The  PropertySingle long path (used in the treecombo).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PropertySingleValueLongPath { get; set; }

		/// <summary>
		/// Gets or sets the pset MSG code.
		/// </summary>
		/// <value>
		/// The pset MSG code.
		/// </value>
		public string PsetMsgCode { get; set; }

		/// <summary>
		/// Gets or sets the attribute MSG code.
		/// </summary>
		/// <value>
		/// The attribute MSG code.
		/// </value>
		public string AttributeMsgCode { get; set; }

		/// <summary>
		/// The Value of the PsetAttribute to use to filter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string Value { get; set; }

		/// <summary>
		/// The Value2 of the PsetAttribute to use to filter (in case of a between Condition).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Value2 { get; set; }

		/// <summary>
		/// the filtering condition to use : =, !=, like
		/// </summary>
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public FilterCondition Condition { get; set; }


		/// <summary>
		/// The order to apply the filter in case there are more than one.
		/// </summary>
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public int Order { get; set; }

		/// <summary>
		///  the operator to use in case there are more than one filter attached to a chart.
		/// </summary>
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public LogicalOperator Operator { get; set; }


		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }


		/// <summary>
		/// The IconCls of the Location.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconCls {
			get {
				var retVal = Helper.GetIconClsByIfcType(UsageName);
				return retVal;
			}
			set { }
		}

	}
}

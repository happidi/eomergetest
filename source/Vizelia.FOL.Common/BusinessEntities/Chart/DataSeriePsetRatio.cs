﻿using System;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity representing a pset ratio calculation for a DataSerie.
	/// </summary>
	[Key("KeyDataSeriePsetRatio")]
	[DataContract]
	[Serializable]
	[IconCls("viz-icon-small-dataseriepsetratio")]
	public class DataSeriePsetRatio : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Key of the Entity
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyDataSeriePsetRatio { get; set; }

		/// <summary>
		/// The key of the DataSerie this ratio is attached too.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyDataSerie { get; set; }


		/// <summary>
		/// The key of the Chart this ratio is attached too.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }


		/// <summary>
		/// The Name of the Pset to use to ratio.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PsetName { get; set; }

		/// <summary>
		/// The Name of the Attribute  to use to ratio
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeName { get; set; }

		/// <summary>
		/// The  PropertySingle long path (used in the treecombo).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PropertySingleValueLongPath { get; set; }

		/// <summary>
		/// Gets or sets the pset MSG code.
		/// </summary>
		/// <value>
		/// The pset MSG code.
		/// </value>
		public string PsetMsgCode { get; set; }

		/// <summary>
		/// Gets or sets the attribute MSG code.
		/// </summary>
		/// <value>
		/// The attribute MSG code.
		/// </value>
		public string AttributeMsgCode { get; set; }

		/// <summary>
		/// The Key of the KeyPropertySingleValue the ratio is attached to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPropertySingleValue { get; set; }

		/// <summary>
		/// The math operator: multiply or divide
		/// </summary>
		[DataMember]
		public ArithmeticOperator Operator { get; set; }


		/// <summary>
		/// The group operation that applies to the pset historical data when grouped over time.
		/// </summary>
		[NotNullValidator]
		[DataMember]
		public MathematicOperator GroupOperation { get; set; }

		/// <summary>
		/// The order to apply the ratio in case there are more than one.
		/// </summary>
		[DataMember]
		public int Order { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// The type of the object the Pset is linked to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string UsageName { get; set; }

		/// <summary>
		/// True to fill pset missing values when calculating the ratio.
		/// </summary>
		[DataMember]
		public bool FillMissingValues { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [stop at first level encountered].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [stop at first level encountered]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool StopAtFirstLevelEncountered { get; set; }


		/// <summary>
		/// The IconCls of the Location.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconCls {
			get {
				var retVal = Helper.GetIconClsByIfcType(UsageName);
				return retVal;
			}
			set { }
		}

		/// <summary>
		/// Gets the pset attribute local id.
		/// </summary>
		/// <returns></returns>
		public string PsetAttributeLocalId {
			get {
				return string.Format("PsetName={0}PsetAttributeName={1}", PsetName, AttributeName);
			}
			set {
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Chart (or Portal Window) Scheduler.
	/// </summary>
	[Serializable]
	[Key("KeyChartScheduler")]
	[DataContract]
	[IconCls("viz-icon-small-chartscheduler")]
	[LocalizedText("msg_chartscheduler")]
	[AuditableEntityAttribute]
	public class ChartScheduler : CalendarEvent, IMappableEntity, ISecurableObject {

		/// <summary>
		/// Contructor
		/// </summary>
		public ChartScheduler() {
			Charts = new List<Chart>();
			PortalWindows = new List<PortalWindow>();
			Category = "ChartScheduler";
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="ev">a CalendarEvent.</param>
		public ChartScheduler(CalendarEvent ev)
			: this() {
			if (ev == null) return;
			Category = ev.Category;
			EndDate = ev.EndDate;
			InitialStartDate = ev.InitialStartDate;
			IsAllDay = ev.IsAllDay;
			Key = ev.Key;
			KeyLocation = ev.KeyLocation;
			Location = ev.Location;
			qtip = ev.qtip;
			RecurrencePattern = ev.RecurrencePattern;
			RelatedTo = ev.RelatedTo;
			StartDate = ev.StartDate;
			Title = ev.Title;
			TimeZoneOffset = ev.TimeZoneOffset;
		}

		/// <summary>
		/// Key of the Chart Scheduler.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(ChartScheduler), false)]
		public string KeyChartScheduler { get; set; }

		/// <summary>
		/// List of emails to send the report to.
		/// </summary>
		[Information("msg_chartscheduler_emails", null)]
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Emails { get; set; }

		/// <summary>
		/// Format of the report to be sent.
		/// </summary>
		[Information("msg_chartscheduler_exporttype", null)]
		[ViewableByDataModel]
		[DataMember]
		public ExportType ExportType { get; set; }

		/// <summary>
		/// The list of Charts associated to this Scheduler.
		/// </summary>
		[DataMember]
		public List<Chart> Charts { get; set; }

		/// <summary>
		/// The list of PortalWindows associated to this Scheduler.
		/// </summary>
		[DataMember]
		public List<PortalWindow> PortalWindows { get; set; }

		/// <summary>
		/// The next occurence DateTime.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public DateTime? NextOccurenceDate { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Gets or sets the subject of the email that is going to be sent.
		/// </summary>
		[Information("msg_chartscheduler_subject", null)]
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Subject { get; set; }

		/// <summary>
		/// Gets or sets the body of the email that is going to be sent.
		/// </summary>
		[Information("msg_chartscheduler_body", null)]
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Body { get; set; }

		/// <summary>
		/// Gets or sets the Key of the LocalizationCulture.
		/// </summary>
		[Information("msg_chartscheduler_culture", null)]
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string KeyLocalizationCulture { get; set; }

		/// <summary>
		/// <c>true</c> if enabled; otherwise, <c>false</c>.
		/// </summary>
		[Information("msg_alarmdefinition_enabled", null)]
		[ViewableByDataModel]
		[DataMember]
		public bool Enabled { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity {
				KeySecurable = KeyChartScheduler,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(GetType()),
				SecurableName = Title,
				SecurableTypeName = GetType().FullName
			};
		}
	}
}

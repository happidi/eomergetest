﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a ImageMap usually saved in cache for later retreival.
	/// </summary>
	[Serializable]
	[Key("Guid")]
	[DataContract]
	public class ImageMap : BaseBusinessEntity {

		/// <summary>
		/// Guid of the image.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string  Guid { get; set; }

		/// <summary>
		/// Gets or sets the html map.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MapText { get; set; }


		/// <summary>
		/// Gets or sets the map name.
		/// </summary>
		/// <value>The map name.</value>
		[AntiXssValidator]
		[DataMember]
		public string MapName { get; set; }

		/// <summary>
		/// Gets or sets the stream as a 64 bits string.
		/// </summary>
		/// <value>The stream.</value>
		public string Base64String { get; set; }

		/// <summary>
		/// Gets or sets the Chart that generated the Image.
		/// </summary>
		[DataMember]
		public Chart Chart { get; set; }
	}
}


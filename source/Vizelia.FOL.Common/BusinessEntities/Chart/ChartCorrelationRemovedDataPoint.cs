﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a removed data point from the chart correlation analysis.
	/// </summary>
	[Key("KeyChartCorrelationRemovedDataPoint")]
	[DataContract]
	[Serializable]
	public class ChartCorrelationRemovedDataPoint : BaseBusinessEntity {

		/// <summary>
		/// Gets or sets the key chart correlation removed data point.
		/// </summary>
		/// <value>
		/// The key chart correlation removed data point.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyChartCorrelationRemovedDataPoint { get; set; }

		/// <summary>
		/// Gets or sets the key chart.
		/// </summary>
		/// <value>
		/// The key chart.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }

		/// <summary>
		/// Gets or sets the X value.
		/// </summary>
		/// <value>
		/// The X value.
		/// </value>
		[DataMember]
		public double XValue { get; set; }

		/// <summary>
		/// Gets or sets the Y value.
		/// </summary>
		/// <value>
		/// The Y value.
		/// </value>
		[DataMember]
		public double YValue { get; set; }

		/// <summary>
		/// Gets or sets the Y value.
		/// </summary>
		/// <value>
		/// The Y value.
		/// </value>
		[DataMember]
		public string LocalId { get; set; }
	}

}

﻿using System;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity to represent the association between a Chart and a Pset Historical in order to display historical values.
	/// </summary>
	[Key("KeyChartPsetAttributeHistorical")]
	[DataContract]
	[Serializable]
	public class ChartPsetAttributeHistorical : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Gets or sets the key chart pset attribute historical.
		/// </summary>
		/// <value>
		/// The key chart pset attribute historical.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyChartPsetAttributeHistorical { get; set; }

		/// <summary>
		/// Gets or sets the key chart.
		/// </summary>
		/// <value>
		/// The key chart.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }

		/// <summary>
		/// The Name of the Pset to use to filter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PsetName { get; set; }

		/// <summary>
		/// Gets or sets the pset MSG code.
		/// </summary>
		/// <value>
		/// The pset MSG code.
		/// </value>
		public string PsetMsgCode { get; set; }

		/// <summary>
		/// The Name of the Attribute  to use to filter
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeName { get; set; }

		/// <summary>
		/// Gets or sets the name of the localized label of the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeMsgCode { get; set; }

		/// <summary>
		/// Gets or sets the name of the localized label of the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeLabel { get; set; }

		/// <summary>
		/// The  PropertySingle long path (used in the treecombo).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PropertySingleValueLongPath { get; set; }

		/// <summary>
		/// The group operation that applies to the data when grouped over time or spatial hierarchy.
		/// </summary>
		[NotNullValidator]
		[DataMember]
		public MathematicOperator GroupOperation { get; set; }

		/// <summary>
		/// The Key of the KeyPropertySingleValue the attribute is attached to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPropertySingleValue { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether we [fill missing values].
		/// </summary>
		/// <value>
		///   <c>true</c> if [fill missing values]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool FillMissingValues { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }
	}
}

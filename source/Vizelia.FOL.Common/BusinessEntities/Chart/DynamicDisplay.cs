﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity representing a Dynamic display image.
	/// </summary>
	[Serializable]
	[Key("KeyDynamicDisplay")]
	[DataContract]
	[IconCls("viz-icon-small-dynamicdisplay")]
	[LocalizedText("msg_dynamicdisplay")]
	public class DynamicDisplay : BaseBusinessEntity, IMappableEntity, ISecurableObject {

		/// <summary>
		/// Gets the application.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Application { get; set; }

		/// <summary>
		/// Key of the Dynamic Display.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(DynamicDisplay), false)]
		public string KeyDynamicDisplay { get; set; }

		/// <summary>
		/// Name of the Dynamic Display.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Description of the Dynamic Display.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Type of the Dynamic Display.
		/// </summary>
		[DataMember]
		public DynamicDisplayType Type { get; set; }

		/// <summary>
		/// Color template of the Dynamic Display.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ColorTemplate { get; set; }

		/// <summary>
		/// <c>True</c> to have rounded corners, <c>False</c> otherwise.
		/// </summary>
		[DataMember]
		public bool RoundedCorners { get; set; }

		/// <summary>
		/// Key of the custom Color template of the Dynamic Display. Used only if ColorTemplate is set to Custom.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyCustomColorTemplate { get; set; }

		/// <summary>
		/// The custom Color template of the Dynamic Display.
		/// </summary>
		[DataMember]
		public DynamicDisplayColorTemplate CustomColorTemplate { get; set; }

		/// <summary>
		/// True to display a Header. False to hide it.
		/// </summary>
		[DataMember]
		public bool HeaderVisible { get; set; }

		/// <summary>
		/// The Key of the DynamicDisplayImage to display in the Header.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string HeaderPicture { get; set; }

		/// <summary>
		/// The DynamicDisplayImage entity to display in the Header.
		/// </summary>
		[DataMember]
		public DynamicDisplayImage HeaderPictureImage { get; set; }

		/// <summary>
		/// Text to display in the Header.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string HeaderText { get; set; }

		/// <summary>
		/// True to display the Chart that it is associated with. False to hide it.
		/// </summary>
		[DataMember]
		public bool MainChartVisible { get; set; }

		/// <summary>
		/// Gets a value indicating whether this instance is main chart visible.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is main chart visible; otherwise, <c>false</c>.
		/// </value>
		public bool IsMainChartVisible {
			get { return Type != DynamicDisplayType.Simple && MainChartVisible; }
		}

		/// <summary>
		/// The Key of the DynamicDisplayImage to display in the Main area.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MainPicture { get; set; }

		/// <summary>
		/// The DynamicDisplayImage entity to display in the  Main area.
		/// </summary>
		[DataMember]
		public DynamicDisplayImage MainPictureImage { get; set; }

		/// <summary>
		/// Size of the picture to display in the Main area.
		/// </summary>
		[DataMember]
		public int? MainPictureSize { get; set; }

		/// <summary>
		/// Text to display in the Main area.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MainText { get; set; }

		/// <summary>
		/// True to display the Footer, False to hide it.
		/// </summary>
		[DataMember]
		public bool FooterVisible { get; set; }

		/// <summary>
		/// The Key of the DynamicDisplayImage  to display in the Footer area.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string FooterPicture { get; set; }


		/// <summary>
		/// The DynamicDisplayImage entity to display in the  Footer area.
		/// </summary>
		[DataMember]
		public DynamicDisplayImage FooterPictureImage { get; set; }

		/// <summary>
		/// Text to display in the Footer area.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string FooterText { get; set; }

		/// <summary>
		/// Gets or sets the header empty text.
		/// </summary>
		/// <value>
		/// The header empty text.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string HeaderEmptyText { get; set; }

		/// <summary>
		/// Gets or sets the main empty text.
		/// </summary>
		/// <value>
		/// The main empty text.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string MainEmptyText { get; set; }

		/// <summary>
		/// Gets or sets the footer empty text.
		/// </summary>
		/// <value>
		/// The footer empty text.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string FooterEmptyText { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }


		/// <summary>
		/// Gets or sets the type of the bogus chart.
		/// </summary>
		/// <value>
		/// The type of the bogus chart.
		/// </value>
		[DataMember]
		public ChartType BogusChartType { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity {
				KeySecurable = KeyDynamicDisplay,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(GetType()),
				SecurableName = Name,
				SecurableTypeName = GetType().FullName
			};
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is custom color template.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is custom color template; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsCustomColorTemplate { get; set; }

		/// <summary>
		/// Gets or sets the header text default font family.
		/// </summary>
		/// <value>
		/// The header text default font family.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string HeaderTextDefaultFontFamily { get; set; }

		/// <summary>
		/// Gets or sets the size of the header text default font.
		/// </summary>
		/// <value>
		/// The size of the header text default font.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string HeaderTextDefaultFontSize { get; set; }

		/// <summary>
		/// Gets or sets the color of the header text default font.
		/// </summary>
		/// <value>
		/// The color of the header text default font.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string HeaderTextDefaultFontColor { get; set; }

		/// <summary>
		/// Gets or sets the main text default font family.
		/// </summary>
		/// <value>
		/// The main text default font family.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string MainTextDefaultFontFamily { get; set; }

		/// <summary>
		/// Gets or sets the size of the main text default font.
		/// </summary>
		/// <value>
		/// The size of the main text default font.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string MainTextDefaultFontSize { get; set; }

		/// <summary>
		/// Gets or sets the color of the main text default font.
		/// </summary>
		/// <value>
		/// The color of the main text default font.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string MainTextDefaultFontColor { get; set; }

		/// <summary>
		/// Gets or sets the footer text default font family.
		/// </summary>
		/// <value>
		/// The footer text default font family.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string FooterTextDefaultFontFamily { get; set; }

		/// <summary>
		/// Gets or sets the size of the footer text default font.
		/// </summary>
		/// <value>
		/// The size of the footer text default font.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string FooterTextDefaultFontSize { get; set; }

		/// <summary>
		/// Gets or sets the color of the footer text default font.
		/// </summary>
		/// <value>
		/// The color of the footer text default font.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string FooterTextDefaultFontColor { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is display main grid.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is display main grid; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsDisplayMainGrid { get; set; }
	}
}

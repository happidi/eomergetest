﻿using System;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a statistical Serie.
	/// </summary>
	[Key("KeyStatisticalSerie")]
	[DataContract]
	[Serializable]
	[IconCls("viz-icon-small-statisticalserie")]
	public class StatisticalSerie : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Key of the StatisticalSerie.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyStatisticalSerie { get; set; }

		/// <summary>
		/// Key of the Chart.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string KeyChart { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the operator.
		/// </summary>
		/// <value>
		/// The operator.
		/// </value>
		[DataMember]
		public MathematicOperator Operator { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether  we use hidden series for statistical calculation.
		/// </summary>
		[DataMember]
		public bool IncludeHiddenSeries { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether  we run the statistical calculation before or after CalculatedSeries.
		/// </summary>
		[DataMember]
		public bool RunAfterCalculatedSeries { get; set; }

		/// <summary>
		/// Gets or sets the Key of the Meter Classification Item we ll use to select dataseries to do the statistical calculation.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string KeyClassificationItem { get; set; }

		/// <summary>
		/// The key path of the classification of the Meter we ll use to select dataseries to do the statistical calculation.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItemPath { get; set; }

		/// <summary>
		/// The classification title of the Meter Classification Item we ll use to select dataseries to do the statistical calculation.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemTitle { get; set; }

		/// <summary>
		/// The classification path of the Meter Classification Item we ll use to select dataseries to do the statistical calculation.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLongPath { get; set; }

		/// <summary>
		/// The classification local id path of the Meter Classification Item we ll use to select dataseries to do the statistical calculation.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLocalIdPath { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorR { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorG { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorB { get; set; }

	}
}

﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.BusinessEntities {


	/// <summary>
	/// Business entity for an IfcOrganization.
	/// </summary>
	[DataContract]
	[Key("KeyOrganization")]
	[IfcTypeName("IfcOrganization")]
	[LocalizedText("msg_organization")]
	[IconCls("viz-icon-small-organization")]
	public class Organization : IfcBaseBusinessEntity, ITreeNode, IHierarchy, IMappableEntity {

		/// <summary>
		/// The Key of the Organization.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyOrganization {
			get;
			set;
		}

		/// <summary>
		/// The id of the Organization.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Id {
			get;
			set;
		}

		/// <summary>
		/// The LocalId of the Organization.
		/// The dummy setter is there only for serialization purpose.
		/// </summary>
		public string LocalId {
			get {
				return Id;
			}
			set { }
		}

		/// <summary>
		/// The name of the Organization.
		/// </summary>
		[NotNullValidator]
		[VizStringLengthValidator(1, 510)]
		[AntiXssValidator]
		[DataMember]
		public string Name {
			get;
			set;
		}

		/// <summary>
		/// The description of the Organization.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Description {
			get;
			set;
		}

		/// <summary>
		/// The Key of the parent Organization.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[IgnoreNulls] // Don't validate using the next validator if the value of the property is null.
		[PropertyComparisonValidator("KeyOrganization", ComparisonOperator.NotEqual, MessageTemplateResourceName = "error_entity_contains_cyclic_reference", MessageTemplateResourceType = typeof(Langue))]
		public string KeyParent {
			get;
			set;
		}

		/// <summary>
		/// The path of titles.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LongPath { get; set; }

		/// <summary>
		/// The path of local ids.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IdPath { get; set; }

		/// <summary>
		/// The level of the classification.
		/// </summary>
		[DataMember]
		public int Level { get; set; }

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				//id = this.KeyClassificationItem,
				Key = this.KeyOrganization,
				text = this.Name,
				leaf = false,
				qtip = this.Name,
				iconCls = this.iconCls,
				entity = this
			};
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString() {
			return Name;
		}
	}
}

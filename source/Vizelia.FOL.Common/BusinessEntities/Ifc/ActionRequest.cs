﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {


	/// <summary>
	/// Business entity for an IfcActionRequest.
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyActionRequest")]
	[IfcTypeName("IfcActionRequest")]
	[IconCls("viz-icon-small-actionrequest")]
	[PsetClassificationItemKey("ClassificationKeyParent")]
	public class ActionRequest : IfcBaseBusinessEntity, IWorkflowEntity, IPset, IAuditEntity, IMappableEntity, ISupportLocation {
		private List<Pset> _propertySetList;

		/// <summary>
		/// The Key of the ActionRequest.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyActionRequest {
			get;
			set;
		}

		/// <summary>
		/// The external identifier of the ActionRequest.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the instance id.
		/// </summary>
		/// <value>The instance id.</value>
		[DataMember]
		public Guid InstanceId {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the state.
		/// </summary>
		/// <value>The state.</value>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string State { get; set; }

		/// <summary>
		/// The Id of the ActionRequest.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string RequestID {
			get;
			set;
		}

		/// <summary>
		/// The priority id of the ActionRequest.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public int PriorityID {
			get;
			set;
		}

		/// <summary>
		/// The description of the ActionRequest.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string Description {
			get;
			set;
		}

		/// <summary>
		/// The KeyActor of the requestor.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Requestor {
			get;
			set;
		}

		/// <summary>
		/// The Key of the parent ClassificationItem.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationKeyParent {
			get;
			set;
		}

		//public ClassificationItem ClassificationParent { get; set; }

		/// <summary>
		/// The Key of the child ClassificationItem.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationKeyChildren {
			get;
			set;
		}

		/*
		/// <summary>
		/// The current status of the ActionRequest.
		/// </summary>
		[DataMember]
		public string CurrentStatus {
			get { return ((IWorkflowEntity)this).State; }
			set {
				((IWorkflowEntity)this).State = value;
			}
		}
		*/


		/// <summary>
		/// The creation date of the ActionRequest.
		/// </summary>
		[DataMember]
		[ViewableByDataModel]
		public DateTime? ActualStart {
			get;
			set;
		}

		/// <summary>
		/// The schedule date of the ActionRequest.
		/// </summary>
		[DataMember]
		[ViewableByDataModel]
		public DateTime? ScheduleStart {
			get;
			set;
		}

		/// <summary
		/// >
		/// The close date of the ActionRequest.
		/// </summary>
		[DataMember]
		[ViewableByDataModel]
		public DateTime? CloseDateTime {
			get;
			set;
		}

		/// <summary>
		/// The KeyLocation of the ActionRequest.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(Location))]
		public string KeyLocation {
			get;
			set;
		}


		/// <summary>
		/// The location long path of the ActionRequest.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string LocationLongPath { get; set; }

		/// <summary>
		/// The location short path of the ActionRequest.
		/// Contains only the deepest site level and further levels.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string LocationShortPath { get; set; }

		/// <summary>
		/// The location level. Starts at level 0.
		/// </summary>
		[DataMember]
		public int? LocationLevel { get; set; }

		/// <summary>
		/// The location name of the ActionRequest.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string LocationName { get; set; }

		/// <summary>
		/// The location typename of the ActionRequest.
		/// </summary>
		[DataMember]
		public HierarchySpatialTypeName LocationTypeName { get; set; }

		/// <summary>
		/// The classification path of the ActionRequest.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationLongPath { get; set; }

		/// <summary>
		/// The value of the priority.
		/// </summary>
		[DataMember]
		[ViewableByDataModel]
		public int PriorityValue { get; set; }

		/// <summary>
		/// The message of the priority.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string PriorityMsgCode { get; set; }

		/// <summary>
		/// The first name of the requestor.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string RequestorFirstName { get; set; }


		/// <summary>
		/// The last name of the requestor.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string RequestorLastName { get; set; }


		/// <summary>
		/// The mail of the requestor.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string RequestorMail { get; set; }


		/// <summary>
		/// The key of the approver.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyApprover { get; set; }

		/// <summary>
		/// The first name of the approver.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string ApproverFirstName { get; set; }


		/// <summary>
		/// The last name of the approver.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string ApproverLastName { get; set; }


		/// <summary>
		/// The mail of the approver.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string ApproverMail { get; set; }

		/// <summary>
		/// The psets of the ActionRequest.
		/// </summary>
		[DataMember]
		[System.Xml.Serialization.XmlIgnore]
		public List<Pset> PropertySetList {
			get {
				return _propertySetList;
			}
			set {
				_propertySetList = value;
			}
		}

		/// <summary>
		/// PsetValues list.
		/// </summary>
		public List<PsetValue> PsetValues { get; set; }

		/// <summary>
		/// The Key of the user that last modified the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AuditModifiedBy {
			get;
			set;
		}

		/// <summary>
		/// The date and time of the last modification.
		/// </summary>
		[DataMember]
		public DateTime? AuditTimeLastModified {
			get;
			set;
		}

		/// <summary>
		/// The login of the user that last modified the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AuditModifiedByLogin {
			get;
			set;
		}

		/// <summary>
		/// The first name of the user that last modified the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AuditModifiedByFirstName {
			get;
			set;
		}

		/// <summary>
		/// The last name of the user that last modified the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AuditModifiedByLastName {
			get;
			set;
		}

		///// <summary>
		///// Gets or sets the state label.
		///// </summary>
		///// <value>The state label.</value>
		//[DataMember]
		//public string StateLabel {
		//    get {
		//        var retVal = Helper.LocalizeText("msg_workflow_state_" + this.State.ToLower(), true);
		//        return retVal == null ? this.State : retVal;
		//    }
		//    set {
		//        ;
		//    }
		//}

		/// <summary>
		/// Gets or sets the state label.
		/// </summary>
		/// <value>The state label.</value>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string StateLabel {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets a value indicating whether the ActionRequest entity is readonly.
		/// </summary>
		/// <value>
		///   <c>true</c> if read only; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool ReadOnly {
			get {
				var currentUser = Helper.GetCurrentFOLProviderUser();
				if (currentUser != null)
					return (Requestor != currentUser.KeyOccupant) && (KeyApprover != currentUser.KeyUser);
				return true;
			}
			set {

			}
		}
	}
}

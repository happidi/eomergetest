﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {


	/// <summary>
	/// Business entity for Priority
	/// </summary>
	[DataContract]
	[Key("KeyPriority")]
	[IfcTypeName("IfcPriority")]
	[IconCls("viz-icon-small-priority")]
	public class Priority : IfcBaseBusinessEntity, ITreeNode, IMappableEntity {

		/// <summary>
		/// Id of the Priority.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPriority { get; set; }

		/// <summary>
		/// Gets or sets the name of the localized label of the Priority.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MsgCode { get; set; }

		/// <summary>
		/// Gets the localized label of the Priority.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Label {
			get {
				return Helper.LocalizeText(MsgCode);
			}
			set { ;}
		}

		/// <summary>
		/// The external identifier of the Priority.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// The value in minutes of the Priority.
		/// </summary>
		[DataMember]
		public int Value { get; set; }

		/// <summary>
		/// Formats the value in HH:mm:ss.
		/// </summary>
		/// <returns></returns>
		private string FormatValue() {
			var ts = TimeSpan.FromMinutes(this.Value);
			return string.Format("Full time: {0}", new DateTime(ts.Ticks).ToString("HH:mm:ss"));
		}

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				Key = this.KeyPriority,
				text = this.Label,
				leaf = true,
				entity = this,
				qtip = this.FormatValue(),
				iconCls = this.iconCls
			};
		}
	}
}

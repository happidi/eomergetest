﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {


	/// <summary>
	/// Business entity for an IfcTask.
	/// </summary>
	[DataContract]
	[Key("KeyTask")]
	[IfcTypeName("IfcTask")]
	[IconCls("viz-icon-small-task")]
	[PsetClassificationItemKey("ClassificationKeyParent")]
	public class Task : IfcBaseBusinessEntity, IWorkflowEntity, IPset { //
		private List<Pset> _propertySetList;

		/// <summary>
		/// The Key of the Task.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyTask {
			get;
			set;
		}

		/// <summary>
		/// The Key of the ActionRequest.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyActionRequest {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the value of IsCompleted.
		/// </summary>
		[DataMember]
		public bool IsCompleted { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		[AntiXssValidator]
		[DataMember]
		public string Name {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the instance id.
		/// </summary>
		/// <value>The instance id.</value>
		[DataMember]
		public Guid InstanceId {
			get;
			set;
		}

		/// <summary>
		/// The description of the Task.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Description {
			get;
			set;
		}

		/// <summary>
		/// The priority of the Task.
		/// </summary>
		[DataMember]
		public int? Priority {
			get;
			set;
		}


		/// <summary>
		/// The duration of the Task.
		/// </summary>
		[DataMember]
		public int? Duration {
			get;
			set;
		}

		/// <summary>
		/// Gets the duration minutes.
		/// </summary>
		[DataMember]
		public int? DurationHours {
			get {
				if (Duration != null)
					return (int)Math.Floor((double)Duration.Value / 60);
				return null;
			}
			set { }
		}

		/// <summary>
		/// Gets the duration hours.
		/// </summary>
		[DataMember]
		public int? DurationMinutes {
			get {
				if (Duration != null)
					return Duration - DurationHours * 60;
				return null;
			}
			set { }
		}


		/// <summary>
		/// The KeyActor owner of the task.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyActor {
			get;
			set;
		}

		/// <summary>
		/// The Key of the ClassificationItem.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItem {
			get;
			set;
		}

		/// <summary>
		/// The classification path of the Task.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationLongPath { get; set; }

		/// <summary>
		/// The creation date of the Task.
		/// </summary>
		[DataMember]
		public DateTime? ActualStart {
			get;
			set;
		}

		/// <summary>
		/// The schedule date of the Task.
		/// </summary>
		[DataMember]
		public DateTime? ScheduleStart {
			get;
			set;
		}

		/// <summary
		/// >
		/// The close date of the Task.
		/// </summary>
		[DataMember]
		public DateTime? ActualFinish {
			get;
			set;
		}



		/// <summary>
		/// The first name of the approver.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ActorFirstName { get; set; }


		/// <summary>
		/// The last name of the approver.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ActorLastName { get; set; }


		/// <summary>
		/// The mail of the approver.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ActorMail { get; set; }

		/// <summary>
		/// The psets of the ActionRequest.
		/// </summary>
		[DataMember]
		[System.Xml.Serialization.XmlIgnore]
		public List<Pset> PropertySetList {
			get {
				return _propertySetList;
			}
			set {
				_propertySetList = value;
			}
		}

		/// <summary>
		/// PsetValues list.
		/// </summary>
		public List<PsetValue> PsetValues { get; set; }

		/// <summary>
		/// Gets or sets the status of the Task.
		/// </summary>
		/// <value>The IsCompleted.</value>
		[AntiXssValidator]
		[DataMember]
		public string State {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the state label.
		/// </summary>
		/// <value>The state label.</value>
		[AntiXssValidator]
		[DataMember]
		public string StateLabel {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the location (from parent ActionRequest).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocation {
			get; set;
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for an association between a dataacquisition endpoint and an historical pset attribute instance.
	/// </summary>
	[DataContract]
	[Serializable]
	[Key("KeyPsetAttributeHistoricalEndpoint")]
	public class PsetAttributeHistoricalEndpoint : IfcBaseBusinessEntity{

		/// <summary>
		/// The Key of the PsetAttributeHistoricalEndpoint.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPsetAttributeHistoricalEndpoint { get; set; }

		/// <summary>
		/// The Key of the KeyPropertySingleValue.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPropertySingleValue { get; set; }

		/// <summary>
		/// The Key of the Object.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyObject { get; set; }


		/// <summary>
		/// The key of the endpoint..
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyEndpoint { get; set; }

		/// <summary>
		/// The key of the endpoint.r.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string EndpointType { get; set; }


		/// <summary>
		/// The frequency to fetch the data from the end point in minutes;
		/// </summary>
		[DataMember]
		public int? EndpointFrequency { get; set; }

	}

}

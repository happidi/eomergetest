﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for IfcBuildingStorey
	/// </summary>
	[DataContract]
	[Key("KeyBuildingStorey")]
	[IfcTypeName("IfcBuildingStorey")]
	[IconCls("viz-icon-small-buildingstorey")]
	[LocalizedText("msg_floor")]
	[AuditableEntityAttribute]
	public class BuildingStorey : IfcBaseBusinessEntity, ITreeNode, IPset, ILocation, IMappableEntity {
		/// <summary>
		/// Initializes a new instance of the <see cref="BuildingStorey"/> class.
		/// </summary>
		public BuildingStorey() {
			PropertySetList = new List<Pset>();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="BuildingStorey"/> class.
		/// </summary>
		/// <param name="location">The location.</param>
		public BuildingStorey(Location location) {
			this.KeyBuildingStorey = location.KeyLocation;
			this.Name = location.Name;
			this.KeyBuilding = location.KeyParent;
			this.Description = location.Description;
			this.LocalId = location.LocalId;
			this.Elevation = location.Elevation;
		}

		/// <summary>
		/// Key of the BuildingStorey.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(Location))]
		public string KeyBuildingStorey { get; set; }

		/// <summary>
		/// The external identifier of the BuildingStorey.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// The CAD file of the BuildingStorey.
		/// </summary>
		[DataMember]
		public string CADFile { get; set; }

		/// <summary>
		/// Name of the BuildingStorey.
		/// </summary>
		[Information("msg_name", null)]
		[NotNullValidator]
		[VizStringLengthValidator(1, 255)]
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Description of the BuildingStorey.
		/// </summary>
		[Information("msg_description", null)]
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Elevation of the BuildingStorey.
		/// </summary>
		[Information("msg_elevation", null)]
		[DataMember]
		public double? Elevation { get; set; }

		/// <summary>
		/// Key of the parent Building.
		/// </summary>
		[IgnoreNulls] // Don't validate using the next validator if the value of the property is null.
		[PropertyComparisonValidator("KeyBuildingStorey", ComparisonOperator.NotEqual, MessageTemplateResourceName = "error_entity_contains_cyclic_reference", MessageTemplateResourceType = typeof(Langue))]
		[NotNullValidator]
		[AntiXssValidator]
		[DataMember]
		public string KeyBuilding { get; set; }

		/// <summary>
		/// ObjectType of the BuildingStorey.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ObjectType { get; set; }


		/// <summary>
		/// Convert this BuildingStorey into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				id = this.KeyBuildingStorey,
				Key = this.KeyBuildingStorey,
				text = this.Name,
				qtip = String.Format("{0}\n{1} : {2:n}", this.Description, Langue.msg_elevation, this.Elevation),
				leaf = false,
				entity = this,
				iconCls = this.iconCls
			};
		}

		/// <summary>
		/// The list of pset of the BuildingStorey.
		/// </summary>
		[DataMember]
		//[System.Xml.Serialization.XmlIgnore]
		public List<Pset> PropertySetList { get; set; }

		/// <summary>
		/// PsetValues list.
		/// </summary>
		public List<PsetValue> PsetValues { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			Type type = GetType();

			var securableObject = new SecurableEntity {
				KeySecurable = KeyBuildingStorey,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(type),
				SecurableName = Name,
				SecurableTypeName = type.FullName
			};

			return securableObject;
		}

		/// <summary>
		/// Return a Location Entity.
		/// </summary>
		/// <returns></returns>
		public Location GetLocation() {
			return new Location {
				IconCls = this.iconCls,
				KeyLocation = this.KeyBuildingStorey,
				Name = this.Name,
				TypeName = this.IfcType.ParseAsEnum<HierarchySpatialTypeName>(),
				KeyParent = this.KeyBuilding,
				Elevation = this.Elevation,
				Description = this.Description,
				LocalId = this.LocalId
			};
		}
	}
}

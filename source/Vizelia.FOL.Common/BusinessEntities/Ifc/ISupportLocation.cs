﻿using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// An entity that supports location.
	/// </summary>
	public interface ISupportLocation {
		/// <summary>
		/// The Key of the location of the Meter.
		/// </summary>
		[NotNullValidator]
		[DataMember]
		string KeyLocation { get; set; }

		/// <summary>
		/// The location long path of the Meter.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		string LocationLongPath { get; set; }

		/// <summary>
		/// The location short path of the Meter.
		/// Contains only the deepest site level and further levels.
		/// </summary>
		[DataMember]
		string LocationShortPath { get; set; }

		/// <summary>
		/// The location level. Starts at level 0.
		/// </summary>
		[DataMember]
		int? LocationLevel { get; set; }

		/// <summary>
		/// The location name of the Meter.
		/// </summary>
		[DataMember]
		string LocationName { get; set; }

		/// <summary>
		/// The location typename of the Meter.
		/// </summary>
		[DataMember]
		HierarchySpatialTypeName LocationTypeName { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for a Pset.
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyPset")]
	[IfcTypeName("IfcPropertySet")]
	[DebuggerDisplay("Pset: PsetName={PsetName}, Key={KeyPset}, Attributes={Attributes.Count}")]
	public class Pset : IfcBaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// The Key of the Pset.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyPset { get; set; }

		/// <summary>
		/// The Name of the Pset.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PsetName { get; set; }

		/// <summary>
		/// The Key of the object the Pset is linked to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyObject { get; set; }

		/// <summary>
		/// The list of attributes.
		/// </summary>
		[DataMember]
		//public SerializableDictionary<string, string> Attributes { get; set; }
		public List<PsetAttribute> Attributes { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return null; }
			set {
				// This entity has no local ID since it doesn't exist on its own.
				// Yet we need to implement IMappableEntity so it can be used in the mapping process.
			}
		}
	}
}

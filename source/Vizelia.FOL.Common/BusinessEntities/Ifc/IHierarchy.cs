﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Vizelia.FOL.Common.Validators;


namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Interface for entity exposing a Hierarchy attribute.
	/// </summary>
	public interface IHierarchy {
		/// <summary>
		/// KeyParent.
		/// </summary>8
		[DataMember]
		string KeyParent { get; set; }
		/// <summary>
		/// The level of the classification.
		/// </summary>
		[DataMember]
		int Level { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an IfcBuilding.
	/// </summary>
	[DataContract]
	[Key("KeyBuilding")]
	[IfcTypeName("IfcBuilding")]
	[IconCls("viz-icon-small-building")]
	[LocalizedText("msg_building")]
	[AuditableEntityAttribute]
	public class Building : IfcBaseBusinessEntity, ITreeNode, IPset, ILocation, IMappableEntity {
		/// <summary>
		/// Initializes a new instance of the <see cref="Building"/> class.
		/// </summary>
		public Building() {
			PropertySetList = new List<Pset>();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Building"/> class.
		/// </summary>
		/// <param name="location">The location.</param>
		public Building(Location location) {
			this.KeyBuilding = location.KeyLocation;
			this.Name = location.Name;
			this.KeySite = location.KeyParent;
			this.Description = location.Description;
			this.LocalId = location.LocalId;
		}

		/// <summary>
		/// Key of the Building.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(Location))]
		public string KeyBuilding { get; set; }

		/// <summary>
		/// The external identifier of the Building.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// The Name of the Building.
		/// </summary>
		[Information("msg_name", null)]
		[NotNullValidator]
		[VizStringLengthValidator(1, 255)]
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// The Description of the Building.
		/// </summary>
		[Information("msg_description", null)]
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// The Key of the parent Site.
		/// </summary>
		//[NotNullValidator(MessageTemplateResourceName="error_ajax", MessageTemplateResourceType=typeof(Langue))]
		[NotNullValidator]
		[IgnoreNulls] // Don't validate using the next validator if the value of the property is null.
		[PropertyComparisonValidator("KeyBuilding", ComparisonOperator.NotEqual, MessageTemplateResourceName = "error_entity_contains_cyclic_reference", MessageTemplateResourceType = typeof(Langue))]
		[AntiXssValidator]
		[DataMember]
		public string KeySite { get; set; }

		/// <summary>
		/// The ObjectType of the Building.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ObjectType { get; set; }

		/// <summary>
		/// Convert this Building into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				id = this.KeyBuilding,
				Key = this.KeyBuilding,
				text = this.Name,
				leaf = false,
				entity = this,
				qtip = this.Description,
				iconCls = this.iconCls
			};
		}

		/// <summary>
		/// The list of pset of the Building.
		/// </summary>
		[DataMember]
		public List<Pset> PropertySetList { get; set; }

		/// <summary>
		/// PsetValues list.
		/// </summary>
		public List<PsetValue> PsetValues { get; set; }

		/// <summary>
		/// Return a Location Entity.
		/// </summary>
		/// <returns></returns>
		public Location GetLocation() {
			return new Location {
				IconCls = this.iconCls,
				KeyLocation = this.KeyBuilding,
				Name = this.Name,
				TypeName = this.IfcType.ParseAsEnum<HierarchySpatialTypeName>(),
				KeyParent = this.KeySite,
				Description = this.Description,
				LocalId = this.LocalId
			};
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			Type type = GetType();

			var securableObject = new SecurableEntity {
				KeySecurable = KeyBuilding,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(type),
				SecurableName = Name,
				SecurableTypeName = type.FullName
			};

			return securableObject;
		}

	}
}

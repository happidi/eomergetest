﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an Meter operation.
	/// </summary>
	[DataContract]
	[Key("KeyMeterOperation")]
	[IfcTypeName("IfcMeterOperation")]
	[Serializable]
	public class MeterOperation : IfcBaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Key of the Operation.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyMeterOperation { get; set; }

		/// <summary>
		/// Local Id of the MeterOperation
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Key of the Meter the operation applys to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyMeter { get; set; }

		/// <summary>
		/// Order of the MeterOperation
		/// </summary>
		[DataMember]
		public int Order { get; set; }

		/// <summary>
		/// Factor that will multiply data coming from Meter1
		/// </summary>
		[DataMember]
		public double Factor1 { get; set; }

		/// <summary>
		/// Function to be applied to the Meter 1 or Meter Operation 1.
		/// </summary>
		[DataMember]
		public AlgebricFunction Function1 { get; set; }

		/// <summary>
		/// Offset that will be added to data coming from Meter1
		/// </summary>
		[DataMember]
		public double? Offset1 { get; set; }

		/// <summary>
		/// Key of the Meter 1.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyMeter1 { get; set; }

		/// <summary>
		/// Name of the Meter 1.
		/// </summary>
		[DataMember]
		public string NameMeter1 { get; set; }

		/// <summary>
		/// Key of the Meter Operation 1.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyMeterOperation1 { get; set; }

		/// <summary>
		/// Name of the Meter 1.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalIdMeterOperation1 { get; set; }

		/// <summary>
		/// Arithmetic Operator beetween the 2 Meters.
		/// </summary>
		[DataMember]
		public ArithmeticOperator Operator { get; set; }

		/// <summary>
		/// Factor that will multiply data coming from Meter2
		/// </summary>
		[DataMember]
		public double Factor2 { get; set; }

		/// <summary>
		/// Function to be applied to the Meter 2 or Meter Operation 2.
		/// </summary>
		[DataMember]
		public AlgebricFunction Function2 { get; set; }

		/// <summary>
		/// Offset that will be added to data coming from Meter2
		/// </summary>
		[DataMember]
		public double? Offset2 { get; set; }

		/// <summary>
		/// Key of the Meter 2.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyMeter2 { get; set; }

		/// <summary>
		/// Name of the Meter 2.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string NameMeter2 { get; set; }

		/// <summary>
		/// Key of the Meter Operation 2.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyMeterOperation2 { get; set; }

		/// <summary>
		/// Name of the Meter 1.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalIdMeterOperation2 { get; set; }
	}
}

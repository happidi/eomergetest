﻿using System.Diagnostics;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for a PsetAttribute.
	/// </summary>
	[DataContract]
	[Key("Key")]
	[IfcTypeName("IfcPropertySingleValue")]
	[DebuggerDisplay("PsetAttribute: Key={Key}, Value={Value}")]
	[Serializable]
	public class PsetAttribute : PsetAttributeDefinition, IMappableEntity {

		/// <summary>
		/// The Key of the PsetAttribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Key { get; set; }

		/// <summary>
		/// The Key of the associated principal object.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyObject { get; set; }

		/// <summary>
		/// The Value of the PsetAttribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Value { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		string IMappableEntity.LocalId {
			get { return null; }
			set {
				// This entity has no local ID since it doesn't exist on its own.
				// Yet we need to implement IMappableEntity so it can be used in the mapping process.
			}
		}

		/// <summary>
		/// Gets or sets the name of the localized label of the associated list value.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ListValueMsgCode { get; set; }

		/// <summary>
		/// Gets or sets the attribute list label.
		/// </summary>
		/// <value>
		/// The attribute list label.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string ListValueLabel {
			get {
				return string.IsNullOrEmpty(this.ListValueMsgCode) ? this.ListValueMsgCode : Helper.LocalizeText(this.ListValueMsgCode);
			}
			set { }
		}

		/// <summary>
		/// Gets the value for reporting.
		/// </summary>
		public string ReportValue {
			get {
				string retVal = null;
				switch (this.AttributeComponent) {
					case "vizDateTime":
					case "datefield":
						var dateVal = Helper.DeserializeJson(this.Value, typeof(DateTime));
						if (dateVal != null)
							retVal = ((DateTime)dateVal).ToString(DateTimeHelper.DateTimeFormat(false));
						break;
					case "timefield":
						retVal = ((DateTime)Helper.DeserializeJson(this.Value, typeof(DateTime))).ToString(DateTimeHelper.TimeFormat(false));
						break;
					case "numberfield":
						retVal = this.Value;
						break;
					case "combo":
						retVal = this.ListValueLabel;
						break;
					case "checkbox":
						retVal = bool.Parse(this.Value) ? "[x]" : "[ ]";
						break;
					default:
						retVal = this.Value;
						break;
				}
				return retVal;
			}
			set { }
		}


	}
}

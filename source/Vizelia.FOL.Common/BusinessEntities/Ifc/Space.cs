﻿
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for IfcSpace
	/// </summary>
	[DataContract]
	[Key("KeySpace")]
	[IfcTypeName("IfcSpace")]
	[IconCls("viz-icon-small-space")]
	[LocalizedText("msg_space")]
	[AuditableEntityAttribute]
	public class Space : IfcBaseBusinessEntity, ITreeNode, IPset, ILocation, IMappableEntity {
		/// <summary>
		/// Initializes a new instance of the <see cref="Space"/> class.
		/// </summary>
		public Space() {
			PropertySetList = new List<Pset>();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Space"/> class.
		/// </summary>
		/// <param name="location">The location.</param>
		public Space(Location location) {
			this.KeySpace = location.KeyLocation;
			this.Name = location.Name;
			this.KeyBuildingStorey = location.KeyParent;
			this.Description = location.Description;
			this.LocalId = location.LocalId;
			this.ElevationWithFlooring = location.Elevation.GetValueOrDefault();
			this.AreaValue = location.AreaValue;
		}

		/// <summary>
		/// Key of the Space.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(Location))]
		public string KeySpace { get; set; }

		/// <summary>
		/// The external identifier of the Space.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// The GlobalId of the Space.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string GlobalId { get; set; }

		/// <summary>
		/// Name of the Space.
		/// </summary>
		[Information("msg_name", null)]
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Description of the Space.
		/// </summary>
		[Information("msg_description", null)]
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Key of the parent BuildingStorey.
		/// </summary>
		[IgnoreNulls] // Don't validate using the next validator if the value of the property is null.
		[PropertyComparisonValidator("KeySpace", ComparisonOperator.NotEqual, MessageTemplateResourceName = "error_entity_contains_cyclic_reference", MessageTemplateResourceType = typeof(Langue))]
		[AntiXssValidator]
		[DataMember]
		public string KeyBuildingStorey { get; set; }

		/// <summary>
		/// ObjectType of the Space.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ObjectType { get; set; }

		/// <summary>
		/// The elevation of the Space.
		/// </summary>
		[Information("msg_space_elevationwithflooring", null)]
		[DataMember]
		public double ElevationWithFlooring { get; set; }

		/// <summary>
		/// The area of the Space.
		/// </summary>
		[Information("msg_space_areavalue", null)]
		[DataMember]
		public double AreaValue { get; set; }

		/// <summary>
		/// The Zoning information.
		/// </summary>
		[DataMember]
		public ClassificationItem Zoning { get; set; }

		/// <summary>
		/// Convert this Site into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				id = this.KeySpace,
				Key = this.KeySpace,
				text = this.Name,
				leaf = false,
				iconCls = this.iconCls,
				qtip = this.Description,
				entity = this
			};
		}

		/// <summary>
		/// The list of pset of the Space.
		/// </summary>
		[DataMember]
		public List<Pset> PropertySetList { get; set; }

		/// <summary>
		/// PsetValues list.
		/// </summary>
		public List<PsetValue> PsetValues { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			Type type = GetType();

			var securableObject = new SecurableEntity {
				KeySecurable = KeySpace,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(type),
				SecurableName = Name,
				SecurableTypeName = type.FullName
			};

			return securableObject;
		}

		/// <summary>
		/// Return a Location Entity.
		/// </summary>
		/// <returns></returns>
		public Location GetLocation() {
			return new Location {
				IconCls = this.iconCls,
				KeyLocation = this.KeySpace,
				Name = this.Name,
				TypeName = HierarchySpatialTypeName.IfcSpace,
				KeyParent = this.KeyBuildingStorey,
				Elevation = this.ElevationWithFlooring,
				AreaValue = this.AreaValue,
				Description = this.Description,
				LocalId = this.LocalId
			};
		}
	}
}
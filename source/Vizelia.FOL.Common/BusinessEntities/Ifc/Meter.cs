﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an Meter.
	/// </summary>
	[Serializable]
	[DataContract]
	[Common.Key("KeyMeter")]
	[IfcTypeName("IfcMeter")]
	[IconCls("viz-icon-small-meter")]
	[LocalizedText("msg_meter")]
	[AuditableEntity]
	public class Meter : IfcBaseBusinessEntity, ITreeNode, IPset, IMappableEntity, ISupportLocation, IEnergyAggregatorNotifyableEntity {

		private const string iconClsVirtual = "viz-icon-small-meteroperation";

		private List<Pset> m_PropertySetList;

		/// <summary>
		/// Initializes a new instance of the <see cref="Meter"/> class.
		/// </summary>
		public Meter() {
			Data = new List<MeterData>();
			PropertySetList = new List<Pset>();
			Factor = 1;
			ManualInputTimeInterval = ManualInputTimeInterval.Minutes;
		}

		/// <summary>
		/// Key of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyMeter { get; set; }


		/// <summary>
		/// The external identifier of the Meter.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string LocalId { get; set; }

		/// <summary>
		/// The factor conversion that is applied to data from the unit input to the unit output.
		/// Value is 1 per default.
		/// </summary>
		[Information("msg_meter_factor", null)]
		[DataMember]
		[DefaultValue(1)]
		//TODO: check why default value doesn't affect when serialization happens
		public double Factor { get; set; }

		/// <summary>
		/// The offset conversion that is applied to data from the unit input to the unit output.
		/// Value is 0 per default.
		/// </summary>
		[Information("msg_meter_offset", null)]
		[DataMember]
		public double? Offset { get; set; }

		/// <summary>
		/// The Name of the Meter.
		/// </summary>
		[Information("msg_name", null)]
		[NotNullValidator]
		[VizStringLengthValidator(1, 255)]
		[AntiXssValidator]
		[DataMember]
		[ContainsCharactersValidator("\"", ContainsCharacters.Any, Negated = true)]
		public string Name { get; set; }

		/// <summary>
		/// The Description of the Meter.
		/// </summary>
		[Information("msg_description", null)]
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// The Key of the location of the Meter.
		/// </summary>
		//[IgnoreNulls] // Don't validate using the next validator if the value of the property is null.
		//[PropertyComparisonValidator("KeyMeter", ComparisonOperator.NotEqual, MessageTemplateResourceName = "error_entity_contains_cyclic_reference", MessageTemplateResourceType = typeof(Langue))]
		[NotNullValidator]
		[VizStringLengthValidator(1, 40)]
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(Location))]
		[System.ComponentModel.DataAnnotations.Required]
		public string KeyLocation { get; set; }

		/// <summary>
		/// The path of all locations key
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocationPath { get; set; }

		/// <summary>
		/// The location long path of the Meter.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[Information("msg_location_path", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocationLongPath { get; set; }

		/// <summary>
		/// The location short path of the Meter.
		/// Contains only the deepest site level and further levels.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationShortPath { get; set; }

		/// <summary>
		/// The location level. Starts at level 0.
		/// </summary>
		[DataMember]
		public int? LocationLevel { get; set; }

		/// <summary>
		/// The location name of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationName { get; set; }

		/// <summary>
		/// The location typename of the Meter.
		/// </summary>
		[DataMember]
		public HierarchySpatialTypeName LocationTypeName { get; set; }

		/// <summary>
		/// The location type.
		/// </summary>
		[DataMember]
		public LocationType LocationType
		{
			get { return (LocationType)LocationTypeName; }
			set { this.LocationTypeName = (HierarchySpatialTypeName)value; }
		}

		/// <summary>
		/// The group operation that applies to the data when grouped over time.
		/// </summary>
		[Information("msg_meter_groupoperation", null)]
		[NotNullValidator]
		[DataMember]
		public MathematicOperator GroupOperation { get; set; }

		/// <summary>
		/// The input unit of the Meter.
		/// </summary>
		[Information("msg_meter_unitinput", null)]
		[AntiXssValidator]
		[DataMember]
		public string UnitInput { get; set; }

		/// <summary>
		/// The output unit of the Meter.
		/// </summary>
		[Information("msg_meter_unitoutput", null)]
		[AntiXssValidator]
		[DataMember]
		public string UnitOutput { get; set; }

		/// <summary>
		/// true if the meter data acquisitiondatetime represent the end of the interval (IE : true for elec meters, false for temperature).
		/// </summary>
		[Information("msg_meter_isacquisitiondatetimeendinterval", null)]
		[DataMember]
		public bool IsAcquisitionDateTimeEndInterval { get; set; }

		/// <summary>
		/// true if the meter data are cumulated, false otherwise.
		/// </summary>
		[Information("msg_meter_isaccumulated", null)]
		[DataMember]
		public bool IsAccumulated { get; set; }

		/// <summary>
		/// The list of MeterData for this Meter.
		/// </summary>
		[DataMember]
		public virtual ICollection<MeterData> Data { get; set; }

		/// <summary>
		/// A collection to store MD data (used in EA for temporary aggregation).
		/// </summary>
		public List<MD> MDData { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the  meter data rollup has been fixed (used in EA)
		/// </summary>
		[DataMember]
		public bool IsMeterDataRollupFixed { get; set; }

		/// <summary>
		/// Gets or sets the last meter data.
		/// </summary>
		/// <value>
		/// The last meter data.
		/// </value>
		[DataMember]
		public MeterData LastData { get; set; }

		/// <summary>
		///A dictionary of category,MeterData for this Meter. 
		/// (used when a Calendar has been applied to the chart and we are in split by event mode).
		/// </summary>
		public Dictionary<string, List<MD>> DataByCalendarEvent { get; set; }

		/// <summary>
		/// The rollover value of the meter when it's accumulated.
		/// </summary>
		[Information("msg_meter_rollovervalue", null)]
		[DataMember]
		public double? RolloverValue { get; set; }

		/// <summary>
		/// The frequency of the meter readings
		/// </summary>
		[Information("msg_meter_expected_interval", null)]
		[Range(1, int.MaxValue)]
		[DataMember]        
		public int? FrequencyInput { get; set; }

		/// <summary>
		/// The key of the endpoint resource of the meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyEndpoint { get; set; }

		/// <summary>
		/// The key of the endpoint resource of the meter.
		/// </summary>
		[Information("msg_meter_endpoint", null)]
		[AntiXssValidator]
		[DataMember]
		public string EndpointType { get; set; }

		/// <summary>
		/// The frequency to fetch the data from the end point in minutes;
		/// </summary>
		[Information("msg_meter_endpoint_frequency", null)]
		[DataMember]
		public int? EndpointFrequency { get; set; }

		/// <summary>
		/// The calibration date time of the meter.
		/// </summary>
		//[Information("msg_meter_calibrationdatetime", null)]
		[DataMember]
		public DateTime? CalibrationDateTime { get; set; }

		/// <summary>
		/// The calibration value of the meter.
		/// </summary>
		//[Information("msg_meter_calibrationvalue", null)]
		[DataMember]
		public double? CalibrationValue { get; set; }

		/// <summary>
		/// true if the meter accepts input through a mobile device, false otherwise.
		/// </summary>
		//[Information("msg_meter_acceptmobileinput", null)]
		[DataMember]
		public bool AcceptMobileInput { get; set; }

		/// <summary>
		/// The key of the classification of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[NotNullValidator]
		[VizStringLengthValidator(1, 40)]
		[System.ComponentModel.DataAnnotations.Required]
		public string KeyClassificationItem { get; set; }

		/// <summary>
		/// The key path of the classification of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItemPath { get; set; }

		/// <summary>
		/// The classification title of the Meter.
		/// </summary>
		[Information("msg_meter_classification", null)]
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemTitle { get; set; }

		/// <summary>
		/// The classification level of the Meter.
		/// </summary>
		[DataMember]
		public int ClassificationItemLevel { get; set; }

		/// <summary>
		/// The classification path of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLongPath { get; set; }

		/// <summary>
		/// The classification local id path of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLocalIdPath { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorR { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorG { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorB { get; set; }

		/// <summary>
		/// The Timeinterval that will be applied to the meter operations when fetching data.
		/// </summary>
		[Information("msg_meter_operation_timescale_interval", null)]
		[DataMember]
		public AxisTimeInterval MeterOperationTimeScaleInterval { get; set; }

		/// <summary>
		/// The Key of the Final MeterOperation that will retrieve the data.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyMeterOperationResult { get; set; }

		/// <summary>
		/// The list of MeterOperations associated with the Current Meter. (Used for Virtual meter operations)
		/// </summary>
		[DataMember]
		public List<MeterOperation> MeterOperations { get; set; }

		/// <summary>
		/// True if the Meter is virtual (is the result of other Meters operations) , False otherwise
		/// </summary>
		[Information("msg_meter_isvirtual", null)]
		[DataMember]
		public bool IsVirtual {
			get {
				return !string.IsNullOrEmpty(this.KeyMeterOperationResult);
			}
			private set {
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this meter is a result of a historical pset
		/// </summary>
		public bool IsHistoricalPset { get; set; }

		/// <summary>
		/// Gets or sets the type of the degree day.
		/// </summary>
		/// <value>
		/// The type of the degree day.
		/// </value>
		[Information("msg_meter_degreeday_type", null)]
		[DataMember]
		public DegreeDayType DegreeDayType { get; set; }

		/// <summary>
		/// Gets or sets the degree day base.
		/// </summary>
		/// <value>
		/// The degree day base.
		/// </value>
		[Information("msg_meter_degreeday_base", null)]
		[DataMember]
		public double? DegreeDayBase { get; set; }

		/// <summary>
		/// Number of alarm instances created by a MeterValidationRule
		/// </summary>
		[DataMember]
		public int AlarmInstanceFromMeterValidationRuleCount { get; set; }


		/// <summary>
		/// Gets the icon CLS.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconCls {
			get {
				return IsVirtual ? iconClsVirtual : iconCls;
			}
			private set {
			}
		}

		/// <summary>
		/// Gets or sets the manual input time interval.
		/// </summary>
		/// <value>
		/// The manual input time interval.
		/// </value>
		[Information("msg_meter_manual_input_time_interval", null)]
		[DataMember]
		public ManualInputTimeInterval ManualInputTimeInterval { get; set; }


		/// <summary>
		/// Convert this Meter into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				id = this.KeyMeter,
				Key = this.KeyMeter,
				text = this.Name,
				leaf = true,
				entity = this,
				iconCls = this.IconCls,
				qtip = GetTreeTooltip()
			};
		}

		/// <summary>
		/// Gets the tree tooltip.
		/// </summary>
		/// <returns></returns>
		private string GetTreeTooltip() {
			var src = "~/color.asmx?grid=true&R=" + this.ClassificationItemColorR + "&G=" + this.ClassificationItemColorG + "&B=" + this.ClassificationItemColorB;
			var retVal = "<img class='viz-grid-img-row' src='" + src + "' /> " + this.ClassificationItemLongPath;
			retVal += "<BR/>";
			retVal += this.LocalId;
			return retVal;
		}

		/// <summary>
		/// The list of pset of the Meter.
		/// </summary>
		[DataMember]
		//[System.Xml.Serialization.XmlIgnore]
		public List<Pset> PropertySetList {
			get {
				return m_PropertySetList;
			}
			set {
				m_PropertySetList = value;
			}
		}

		/// <summary>
		/// PsetValues list.
		/// </summary>
		public List<PsetValue> PsetValues { get; set; }

		/// <summary>
		/// Gets or sets the time zone id in which we want to calculate MeterOperation (Degree day or TimeInterval grouping).
		/// </summary>
		/// <value>
		/// The time zone id.
		/// </value>
		[Information("msg_chart_timezone", null)]
		[AntiXssValidator]
		[DataMember]
		public string TimeZoneId { get; set; }

		/// <summary>
		/// Gets the time zone in which we want to calculate MeterOperation (Degree day or TimeInterval grouping).
		/// </summary>
		/// <returns></returns>
		public TimeZoneInfo GetTimeZone() {
			var retVal = CalendarHelper.GetTimeZoneFromId(this.TimeZoneId);
			return retVal;
		}


		/// <summary>
		/// Gets or sets the meter meter data count.
		/// </summary>
		[DataMember]
		public long MeterDataCount { get; set; }


		/// <summary>
		/// Gets or sets the acquisition date time min.
		/// </summary>
		[Information("msg_meter_acquisitiondatetimemin", null)]
		[DataMember]
		public DateTime? AcquisitionDateTimeMin { get; set; }


		/// <summary>
		/// Gets or sets the acquisition date time max.
		/// </summary>
		[Information("msg_meter_acquisitiondatetimemax", null)]
		[DataMember]
		public DateTime? AcquisitionDateTimeMax { get; set; }

		/// <summary>
		/// Gets the application.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Application { get; set; }

		/// <summary>
		/// Gets or sets the key calendar event category.
		/// </summary>
		/// <value>
		/// The key calendar event category.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyCalendarEventCategory { get; set; }

		/// <summary>
		/// Gets or sets the calendar event category title.
		/// </summary>
		/// <value>
		/// The calendar event category title.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string CalendarEventCategoryTitle { get; set; }

		/// <summary>
		/// Gets or sets the calendar mode.
		/// </summary>
		/// <value>
		/// The calendar mode.
		/// </value>
		[Information("msg_meter_calendarmode", null)]
		[DataMember]
		public ChartCalendarMode CalendarMode { get; set; }


		/// <summary>
		/// Gets or sets the calendar event category.
		/// </summary>
		/// <value>
		/// The calendar event category.
		/// </value>
		[DataMember]
		public CalendarEventCategory CalendarEventCategory { get; set; }


		/// <summary>
		/// The Name of the Pset to use to ratio.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PsetName { get; set; }

		/// <summary>
		/// The Name of the Attribute  to use to ratio
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeName { get; set; }

		/// <summary>
		/// The  PropertySingle long path (used in the treecombo).
		/// </summary>
		[Information("msg_meter_psetfactor", null)]
		[AntiXssValidator]
		[DataMember]
		public string PropertySingleValueLongPath { get; set; }

		/// <summary>
		/// Gets or sets the pset MSG code.
		/// </summary>
		/// <value>
		/// The pset MSG code.
		/// </value>
		public string PsetMsgCode { get; set; }

		/// <summary>
		/// Gets or sets the attribute MSG code.
		/// </summary>
		/// <value>
		/// The attribute MSG code.
		/// </value>
		public string AttributeMsgCode { get; set; }

		/// <summary>
		/// The Key of the KeyPropertySingleValue the ratio is attached to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPropertySingleValue { get; set; }

		/// <summary>
		/// The type of the object the Pset is linked to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string UsageName { get; set; }


		/// <summary>
		/// Gets or sets the pset factor operator.
		/// </summary>
		/// <value>
		/// The pset factor operator.
		/// </value>
		[Information("msg_chartfilterspatialpset_operator", null)]
		[DataMember]
		public ArithmeticOperator PsetFactorOperator { get; set; }

		/// <summary>
		/// The last value of the data - comes from the DB so its the raw data.
		/// </summary>
		[DataMember]
		public double? LastRawValue { get; set; }

		/// <summary>
		/// The last validity of the data
		/// </summary>
		[DataMember]
		public MeterDataValidity? LastValidity { get; set; }

	}
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for IfcFurniture
	/// </summary>
	[DataContract]
	[Key("KeyFurniture")]
	[IfcTypeName("IfcFurniture")]
	[IconCls("viz-icon-small-furniture")]
	[LocalizedText("msg_equipment")]
	[AuditableEntityAttribute]
	public class Furniture : IfcBaseBusinessEntity, ITreeNode, IPset, ILocation, IMappableEntity, ISupportLocation {
		/// <summary>
		/// Initializes a new instance of the <see cref="Furniture"/> class.
		/// </summary>
		public Furniture() {
			PropertySetList = new List<Pset>();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Furniture"/> class.
		/// </summary>
		/// <param name="location">The location.</param>
		public Furniture(Location location) {
			this.KeyFurniture = location.KeyLocation;
			this.Name = location.Name;
			this.KeyLocation = location.KeyParent;
			this.Description = location.Description;
			this.LocalId = location.LocalId;
			this.KeyClassificationItem = location.KeyClassificationItem;

		}
		/// <summary>
		/// Key of the Furniture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(Location))]
		public string KeyFurniture { get; set; }

		/// <summary>
		/// The external identifier of the Furniture.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// The GlobalId of the Furniture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string GlobalId { get; set; }

		/// <summary>
		/// Name of the Furniture.
		/// </summary>
		[Information("msg_name", null)]
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Description of the Furniture.
		/// </summary>
		[Information("msg_description", null)]
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Key of the parent Location.
		/// </summary>
		[IgnoreNulls] // Don't validate using the next validator if the value of the property is null.
		[PropertyComparisonValidator("KeyFurniture", ComparisonOperator.NotEqual, MessageTemplateResourceName = "error_entity_contains_cyclic_reference", MessageTemplateResourceType = typeof(Langue))]
		[AntiXssValidator]
		[DataMember]
		public string KeyLocation { get; set; }

		/// <summary>
		/// The location long path of the Furniture.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[Information("msg_location_path", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocationLongPath { get; set; }

		/// <summary>
		/// The location short path of the Furniture.
		/// Contains only the deepest site level and further levels.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationShortPath { get; set; }

		/// <summary>
		/// The location level. Starts at level 0.
		/// </summary>
		[DataMember]
		public int? LocationLevel { get; set; }

		/// <summary>
		/// The location name of the Furniture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationName { get; set; }

		/// <summary>
		/// The location typename of the Furniture.
		/// </summary>
		[DataMember]
		public HierarchySpatialTypeName LocationTypeName { get; set; }

		/// <summary>
		/// ObjectType of the Furniture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ObjectType { get; set; }

		/// <summary>
		/// The tag of the Furniture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Tag { get; set; }

		/// <summary>
		/// Convert this Site into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				id = KeyFurniture,
				Key = KeyFurniture,
				text = Name,
				leaf = false,
				iconCls = iconCls,
				qtip = Description,
				entity = this
			};
		}

		/// <summary>
		/// The list of pset of the Furniture.
		/// </summary>
		[DataMember]
		public List<Pset> PropertySetList { get; set; }

		/// <summary>
		/// PsetValues list.
		/// </summary>
		public List<PsetValue> PsetValues { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			Type type = GetType();

			var securableObject = new SecurableEntity {
				KeySecurable = KeyFurniture,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(type),
				SecurableName = Name,
				SecurableTypeName = type.FullName
			};

			return securableObject;
		}

		/// <summary>
		/// Return a Location Entity.
		/// </summary>
		/// <returns></returns>
		public Location GetLocation() {
			return new Location {
				IconCls = iconCls,
				KeyLocation = KeyFurniture,
				Name = Name,
				Description = Description,
				TypeName = IfcType.ParseAsEnum<HierarchySpatialTypeName>(),
				KeyParent = KeyLocation,
				LocalId = LocalId,
				KeyClassificationItem = KeyClassificationItem
			};
		}

		/// <summary>
		/// The key of the classification of the Furniture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItem { get; set; }

		/// <summary>
		/// The key path of the classification of the Furniture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItemPath { get; set; }

		/// <summary>
		/// The classification title of the Furniture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemTitle { get; set; }

		/// <summary>
		/// The classification level of the Furniture.
		/// </summary>
		[DataMember]
		public int ClassificationItemLevel { get; set; }

		/// <summary>
		/// The classification path of the Furniture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLongPath { get; set; }

		/// <summary>
		/// The classification local id path of the Furniture.
		/// </summary>
		[Information("msg_classificationitem_localidpath", null)]
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLocalIdPath { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorR { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorG { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorB { get; set; }
	}
}
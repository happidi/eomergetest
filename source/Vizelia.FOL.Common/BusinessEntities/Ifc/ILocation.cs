﻿using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Interface for Location entity (ie IfcSite, IfcBuilding,...)
	/// </summary>
	public interface ILocation : IEnergyAggregatorNotifyableEntity, ISecurableObject {
		/// <summary>
		/// Returns the Location entity.
		/// </summary>
		Location GetLocation();
	}
}

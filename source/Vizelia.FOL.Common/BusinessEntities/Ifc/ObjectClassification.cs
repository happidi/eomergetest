﻿using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an IfcClassificationReference.
	/// </summary>
	[DataContract]
	[Key("KeyClassificationReference")]
	[IfcTypeName("IfcClassificationReference")]
	public class ObjectClassification : IfcBaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Gets or sets the key classification reference.
		/// </summary>
		/// <value>The key classification reference.</value>
		public string KeyClassificationReference { get; set; }

		/// <summary>
		/// Gets or sets the key object.
		/// </summary>
		/// <value>The key object.</value>
		public string KeyObject { get; set; }

		/// <summary>
		/// Gets or sets the type of the object.
		/// </summary>
		/// <value>The type of the object.</value>
		public string TypeName { get; set; }


		/// <summary>
		/// Gets or sets the key classification item.
		/// </summary>
		/// <value>The key classification item.</value>
		public string KeyClassificationItem { get; set; }

		/// <summary>
		/// Gets or sets the location.
		/// </summary>
		/// <value>The location.</value>
		public string Location { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return null; }
			set { }
		}
	}
}

﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for a PsetDefinition.
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyPsetValue")]
	public class PsetValue : BaseBusinessEntity {

		/// <summary>
		/// Application name.
		/// </summary>
		[AntiXssValidator]
		public string App { get; set; }

		/// <summary>
		/// Reflects the key(KeyPropertySingleValue) of ObjectPset table. 
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyPsetValue { get; set; }

		/// <summary>
		/// The key of the containing object.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyObject { get; set; }

		/// <summary>
		/// Pset name.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PsetName { get; set; }

		/// <summary>
		/// Attribute name.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeName { get; set; }

		/// <summary>
		/// Attribute value.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeValue { get; set; }
	}
}
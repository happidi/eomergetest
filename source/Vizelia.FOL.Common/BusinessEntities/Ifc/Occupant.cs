﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for an IfcOccupant.
	/// </summary>
	[Serializable]	// added for cache support
	[DataContract]
	[Key("KeyOccupant")]
	[IfcTypeName("IfcOccupant")]
	[IconCls("viz-icon-small-occupant")]
	[PsetClassificationItemKey("KeyClassificationItemWorktype")]
	[LocalizedText("msg_occupant")]
	[AuditableEntityAttribute]
	public class Occupant : IfcBaseBusinessEntity, IPset, IMappableEntity, ISupportLocation {
		/// <summary>
		/// Initializes a new instance of the <see cref="Occupant"/> class.
		/// </summary>
		public Occupant() {
			PropertySetList = new List<Pset>();
		}

		/// <summary>
		/// The Key.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyOccupant { get; set; }

		/// <summary>
		/// The id of the Occupant.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string Id { get; set; }

		/// <summary>
		/// The LocalId of the Occupant.
		/// The dummy setter is there only for serialization purpose.
		/// </summary>
		[Information("msg_localid", null)]
		public string LocalId {
			get { return Id; }
			set { }
		}

		/// <summary>
		/// The first name of the Occupant.
		/// </summary>
		[Information("msg_occupant_firstname", null)]
		[DataMember]
		[ViewableByDataModel]
		[AntiXssValidator]
		public string FirstName { get; set; }

		/// <summary>
		/// The last name of the Occupant.
		/// </summary>
		[Information("msg_occupant_lastname", null)]
		[DataMember]
		[ViewableByDataModel]
		[AntiXssValidator]
		public string LastName { get; set; }

		/// <summary>
		/// The full name of the Occupant.
		/// The dummy setter is there only for serialization purpose.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string FullName {
			get {
				if (String.IsNullOrEmpty(FirstName))
					return LastName;
				if (String.IsNullOrEmpty(LastName))
					return FirstName;
				// return LastName + ", " + FirstName;
				return FirstName + " " + LastName;
			}
			set {
			}
		}

		/// <summary>
		/// The job title of the Occupant.
		/// </summary>
		[Information("msg_occupant_jobtitle", null)]
		[AntiXssValidator]
		[DataMember]
		[BatchUpdatable(true)]
		[ViewableByDataModel]
		public string JobTitle { get; set; }

		/// <summary>
		/// The telephone number of the Occupant.
		/// </summary>
		[Information("msg_occupant_telephone", null)]
		[AntiXssValidator]
		[DataMember]
		[BatchUpdatable(true)]
		[ViewableByDataModel]
		public string TelephoneNumbers { get; set; }

		/// <summary>
		/// The fax number of the Occupant.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[BatchUpdatable(true)]
		[ViewableByDataModel]
		public string FacsimileNumbers { get; set; }

		/// <summary>
		/// The pager number of the Occupant.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[BatchUpdatable(true)]
		[ViewableByDataModel]
		public string PagerNumber { get; set; }

		/// <summary>
		/// The Key of the Organization.
		/// </summary>
		[NotNullValidator]
		[AntiXssValidator]
		[DataMember]
		[BatchUpdatable(true)]
		[System.ComponentModel.DataAnnotations.Required]
		public string KeyOrganization { get; set; }

		/// <summary>
		/// The Id of the Organization.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string OrganizationId { get; set; }

		/// <summary>
		/// The Name of the Organization.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string OrganizationName { get; set; }

		/// <summary>
		/// The id path of the Organization.
		/// </summary>
		[Information("msg_occupant_company", null)]
		[AntiXssValidator]
		[DataMember]
		public string OrganizationIdPath { get; set; }

		/// <summary>
		/// The long path of the Organization.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string OrganizationLongPath { get; set; }

		/// <summary>
		/// The psets of the Occupant.
		/// </summary>
		[DataMember, System.Xml.Serialization.XmlIgnore]
		public List<Pset> PropertySetList { get; set; }

		/// <summary>
		/// PsetValues list.
		/// </summary>
		public List<PsetValue> PsetValues { get; set; }

		/// <summary>
		/// The KeyLocation of the Occupant.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[BatchUpdatable(true)]
		[FilterType(typeof(Location))]
		public string KeyLocation { get; set; }

		/// <summary>
		/// The location id path of the Occupant.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationLocalIdPath { get; set; }

		/// <summary>
		/// The location long path of the Occupant.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[Information("msg_location_path", null)]
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string LocationLongPath { get; set; }

		/// <summary>
		/// The location short path of the Occupant.
		/// Contains only the deepest site level and further levels.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string LocationShortPath { get; set; }

		/// <summary>
		/// The location level. Starts at level 0.
		/// </summary>
		[DataMember]
		public int? LocationLevel { get; set; }

		/// <summary>
		/// The location name of the Occupant.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string LocationName { get; set; }

		/// <summary>
		/// The location typename of the Occupant.
		/// </summary>
		[DataMember]
		public HierarchySpatialTypeName LocationTypeName { get; set; }


		/// <summary>
		/// Gets or sets the key classification item worktype.
		/// </summary>
		/// <value>The key classification item worktype.</value>
		[AntiXssValidator]
		[DataMember]
		[BatchUpdatable(true)]
		public string KeyClassificationItemWorktype { get; set; }

		/// <summary>
		/// Gets or sets the title worktype.
		/// </summary>
		/// <value>The title worktype.</value>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string ClassificationItemWorktypeTitle { get; set; }

		/// <summary>
		/// Gets or sets the id path for the worktype.
		/// </summary>
		/// <value>The id path for the worktype.</value>
		[Information("msg_occupant_worktype", null)]
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemWorktypeLocalIdPath { get; set; }

		/// <summary>
		/// The location long path of the worktype.
		/// </summary>
		/// <value>The long path ofthe worktype.</value>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string ClassificationItemWorktypeLongPath { get; set; }


		/// <summary>
		/// Gets or sets the key classification item organization.
		/// </summary>
		/// <value>The key classification item organization.</value>
		[AntiXssValidator]
		[DataMember]
		[BatchUpdatable(true)]
		public string KeyClassificationItemOrganization { get; set; }


		/// <summary>
		/// Gets or sets the title organization.
		/// </summary>
		/// <value>The title organization.</value>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string ClassificationItemOrganizationTitle { get; set; }


		/// <summary>
		/// Gets or sets the local id path of the organization.
		/// </summary>
		/// <value>The local id path of the organization.</value>
		[Information("msg_occupant_organization", null)]
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemOrganizationLocalIdPath { get; set; }

		/// <summary>
		/// Gets or sets the long path of the organization.
		/// </summary>
		/// <value>The long path of the organization.</value>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string ClassificationItemOrganizationLongPath { get; set; }
	}
}

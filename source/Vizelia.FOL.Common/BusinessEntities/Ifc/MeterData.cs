﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an meter data entry.
	/// </summary>
	[DataContract]
	[Key("KeyMeterData")]
	[IfcTypeName("IfcMeterData")]
	[Serializable]
	[IconCls("viz-icon-small-meterdata")]
	[LocalizedText("msg_meterdata")]
	[AuditableEntityAttribute]
	public class MeterData : IfcBaseBusinessEntity, IMappableEntity, IEnergyAggregatorNotifyableEntity {
		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>
		/// The key.
		/// </value>
		[System.ComponentModel.DataAnnotations.Key]
		public long Key { get; set; }

		/// <summary>
		/// Key of the meter data.
		/// </summary>
		[Information("msg_key", null)]
		[AntiXssValidator]
		[DataMember]
		public string KeyMeterData {
			get { return Key == 0 ? null : Key.ToString(); }
			set { Key = string.IsNullOrWhiteSpace(value) ? 0 : long.Parse(value); }
		}

		//[DataMember]
		//public virtual Meter Meter { get; set; }

		/// <summary>
		/// The key of the meter the data is attached to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string KeyMeter { get; set; }

		/// <summary>
		/// The localid of the meter the data is attached to.
		/// </summary>
		[Information("msg_meterdata_meterlocalid", null)]
		[DataMember]
		public string MeterLocalId { get; set; }

		/// <summary>
		/// The acquisition date time of the data.
		/// </summary>
		[Information("msg_meterdata_acquisitiondatetime", null)]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public DateTime AcquisitionDateTime { get; set; }

		/// <summary>
		/// The acquisition date time of the data as a string in order to easily change the timezone.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AcquisitionDateTimeLocal { get; set; }

		/// <summary>
		/// Gets or sets the time zone in which the AcquisitionDateTimeLocal is displayed.
		/// </summary>
		/// <value>
		/// The time zone local.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string TimeZoneLocal { get; set; }

		/// <summary>
		/// The value of the data.
		/// </summary>
		[Information("msg_meterdata_value", null)]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public double Value { get; set; }


		/// <summary>
		/// Gets or sets the validity.
		/// </summary>
		[Information("msg_meterdata_validity", null)]
		[DataMember]
		[BatchUpdatable(true)]
		public MeterDataValidity Validity { get; set; }

		///// <summary>
		///// The source (file for example) of the data.
		///// </summary>
		//[DataMember]
		//public string Source { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get {
				return FormatLocalId(AcquisitionDateTime, KeyMeter);
			}
			set {
				// This entity has no local ID since it doesn't exist on its own.
				// Yet we need to implement IMappableEntity so it can be used in the mapping process.
				// An existing meter data can be updated by mapping a meter data entity with the same KeyMeter and date/time.
			}
		}

		/// <summary>
		/// Formats the local id by the acquisition date time and the key meter.
		/// </summary>
		/// <param name="acquisitionDateTime">The acquisition date time.</param>
		/// <param name="keyMeter">The key meter.</param>
		/// <returns></returns>
		public static string FormatLocalId(DateTime acquisitionDateTime, string keyMeter) {
			return (keyMeter ?? "KeyMeter not set") + ":" + acquisitionDateTime.ToString("o");
		}


		/// <summary>
		/// Performs an implicit conversion from <see cref="Vizelia.FOL.BusinessEntities.MD"/> to <see cref="Vizelia.FOL.BusinessEntities.MeterData"/>.
		/// </summary>
		/// <param name="md">The md.</param>
		/// <returns>
		/// The result of the conversion.
		/// </returns>
		public static explicit operator MeterData(MD md) {
			return new MeterData {
				KeyMeter = MeterHelper.ConvertMeterKeyToString(md.KeyMeter),
				KeyMeterData = md.KeyMeterData.ToString(),
				AcquisitionDateTime = md.AcquisitionDateTime,
				Value = md.Value,
				Validity = ((int)md.Validity).ParseAsEnum<MeterDataValidity>()
			};
		}

		/// <summary>
		/// Performs an implicit conversion from <see cref="Vizelia.FOL.BusinessEntities.MeterData"/> to <see cref="Vizelia.FOL.BusinessEntities.MD"/>.
		/// </summary>
		/// <param name="meterData">The meter data.</param>
		/// <returns>
		/// The result of the conversion.
		/// </returns>
		public static explicit operator MD(MeterData meterData) {
			return new MD {
				KeyMeter = MeterHelper.ConvertMeterKeyToInt(meterData.KeyMeter),
				KeyMeterData = Convert.ToInt64(meterData.KeyMeterData),
				AcquisitionDateTime = meterData.AcquisitionDateTime,
				Value = meterData.Value,
				Validity = (byte)meterData.Validity.ParseAsInt()
			};
		}
	}



	/// <summary>
	/// internal class to hold MeterData in memory with the smallest footprint possible. (ONLY USE IN EA)
	/// </summary>
	[Serializable]
	public class MD : BaseEntity {
		/// <summary>
		/// The Key Meter
		/// </summary>
		public int KeyMeter;
		/// <summary>
		/// The Key MeterData
		/// </summary>
		public long KeyMeterData;
		/// <summary>
		/// The Acquisition DateTime.
		/// </summary>
		public DateTime AcquisitionDateTime;
		/// <summary>
		/// The Value.
		/// </summary>
		public double Value;
		/// <summary>
		/// The Validity
		/// </summary>
		public byte Validity;
	}


	/// <summary>
	/// Meter Data collection as a binary stream.
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyMeter")]
	[IfcTypeName("IfcMeterDataBinary")]
	[IconCls("viz-icon-small-meterdata")]
	[LocalizedText("msg_meterdata")]
	public class MeterDataBinary : BaseBusinessEntity {

		/// <summary>
		/// The key of the meter the data is attached to.
		/// </summary>
		[DataMember]
		public int KeyMeter { get; set; }


		/// <summary>
		/// Gets or sets the count.
		/// </summary>
		/// <value>
		/// The count.
		/// </value>
		[DataMember]
		public int Count { get; set; }

		/// <summary>
		/// Gets or sets the requested.
		/// </summary>
		/// <value>
		/// The requested.
		/// </value>
		[DataMember]
		public int Requested { get; set; }

		/// <summary>
		/// Gets or sets the size.
		/// </summary>
		/// <value>
		/// The size.
		/// </value>
		[DataMember]
		public double Size {
			get {
				var retVal = 0d;
				if (Values != null)
					retVal = Values.Length / 1024d / 1024d;
				return retVal;
			}

			private set {
			}
		}


		/// <summary>
		/// Gets or sets the values.
		/// </summary>
		/// <value>
		/// The values.
		/// </value>
		[DataMember]
		public byte[] Values { get; set; }

	}
}


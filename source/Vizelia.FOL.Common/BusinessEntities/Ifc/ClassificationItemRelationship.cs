﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for IfcClassificationItemRelationship
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyClassificationItemRelationship")]
	[IfcTypeName("IfcClassificationItemRelationship")]
	public class ClassificationItemRelationship : IfcBaseBusinessEntity, IMappableEntity {

        /// <summary>
        /// Key.
        /// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItemRelationship { get; set; }

		/// <summary>
		/// The Key of the parent ClassificationItem.
		/// </summary>
		[NotNullValidator]
		[AntiXssValidator]
		[DataMember]
		public string KeyParent { get; set; }

        /// <summary>
		///  The Key of the child ClassificationItem..
        /// </summary>
		[NotNullValidator]
		[AntiXssValidator]
		[DataMember]
		public string KeyChildren { get; set; }

        /// <summary>
		/// The RelationPath of the ClassificationItemRelationship.
        /// </summary>
		[NotNullValidator]
		[AntiXssValidator]
		[DataMember]
		public string RelationPath { get; set; }

        /// <summary>
        /// The type of the Relation.
        /// </summary>
		[AntiXssValidator]
		[DataMember]
		public string RelationType { get; set; }

		/// <summary>
		/// The order of the Relation, so we can sort children in a specific order.
		/// </summary>
		[DataMember]
		public int RelationOrder { get; set; }

		/// <summary>
		/// The local id of the Relation.
		/// It is a dummy property since updating instances of this entity is not allowed.
		/// Therefore, instances can only be deleted by specifying their property values.
		/// </summary>
		public string LocalId {
			get { return null; }
			set { }
		}
	}
}

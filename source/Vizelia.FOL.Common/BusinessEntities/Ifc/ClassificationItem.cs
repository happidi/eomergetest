﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for IfcClassificationItem
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyClassificationItem")]
	[IfcTypeName("IfcClassificationItem")]
	[IconCls("viz-icon-small-classificationitem")]
	[LocalizedText("msg_classificationitem")]
	public class ClassificationItem : IfcBaseBusinessEntity, ISecurableObject, ITreeNode, IHierarchy, IMappableEntity, IEnergyAggregatorNotifyableEntity {

		/// <summary>
		/// The Key of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyClassificationItem { get; set; }

		/// <summary>
		/// The relation.
		/// </summary>
		[DataMember]
		[ExcludeMetaData]
		public ClassificationItemRelationship Relation { get; set; }

		/// <summary>
		/// The external identifier of the ClassificationItem.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// The Title.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[VizStringLengthValidator(1, 255)]
		[System.ComponentModel.DataAnnotations.Required]
		public string Title { get; set; }

		/// <summary>
		/// The Code.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Code { get; set; }

		/// <summary>
		/// The path of titles.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LongPath { get; set; }

		/// <summary>
		/// The path of local ids.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalIdPath { get; set; }

		/// <summary>
		/// KeyParent.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		//[IgnoreNulls] // Don't validate using the next validator if the value of the property is null.
		//[PropertyComparisonValidator("KeyClassificationItem", ComparisonOperator.NotEqual, MessageTemplateResourceName = "error_entity_contains_cyclic_reference", MessageTemplateResourceType = typeof(Langue))]
		public string KeyParent { get; set; }

		/// <summary>
		/// Red color composant.
		/// </summary>
		[DataMember]
		[RangeValidator(0, RangeBoundaryType.Inclusive, 255, RangeBoundaryType.Inclusive)]
		public int ColorR { get; set; }

		/// <summary>
		/// Green color composant.
		/// </summary>
		[DataMember]
		[RangeValidator(0, RangeBoundaryType.Inclusive, 255, RangeBoundaryType.Inclusive)]
		public int ColorG { get; set; }

		/// <summary>
		/// Blue color composant.
		/// </summary>
		[DataMember]
		[RangeValidator(0, RangeBoundaryType.Inclusive, 255, RangeBoundaryType.Inclusive)]
		public int ColorB { get; set; }

		/// <summary>
		/// The level of the classification.
		/// </summary>
		[DataMember]
		public int Level { get; set; }

		/// <summary>
		/// Gets or sets the IconCls.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconCls { get; set; }


		/// <summary>
		/// Convert this ClassificationItem into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				//id = this.KeyClassificationItem,
				Key = KeyClassificationItem,
				text = Title,
				leaf = false,
				//qtip = this.Code,
				cls = Relation != null ? "viz-tree-node-classificationitemrelationship" : null,
				icon = String.IsNullOrEmpty(IconCls) ? String.Format("~/color.asmx?R={0}&G={1}&B={2}", ColorR, ColorG, ColorB) : null,
				iconCls = IconCls,
				entity = this,
				draggable = Category != "mail"
			};
		}


		/// <summary>
		/// Gets or sets the Category.
		/// </summary>
		/// <value>The Category.</value>
		[DataMember]
		public virtual string Category { get; set; }

		/// <summary>
		/// Gets or sets if we display an icon only for the root node. 
		/// This attribute is herited from the ClassificationDefinition entity.
		/// </summary>
		public virtual bool IconRootOnly { get; set; }


		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>The SecurableEntity object.</returns>
		public SecurableEntity GetSecurableObject() {
			var securableObject = new SecurableEntity {
				KeySecurable = KeyClassificationItem,
				SecurableIconCls = iconCls,
				SecurableName = Title,
				SecurableTypeName = GetType().FullName
			};
			return securableObject;
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }


		/// <summary>
		/// Gets or sets the children.
		/// </summary>
		/// <value>
		/// The children.
		/// </value>
		[DataMember]
		public List<ClassificationItem> Children { get; set; }
	}
}

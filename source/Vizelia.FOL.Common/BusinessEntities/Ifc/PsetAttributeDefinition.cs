﻿using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common.Validators;
using System;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for a PsetAttribute Definition.
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyPropertySingleValue")]
	[IconCls("viz-icon-small-psetattribute")]
	[IfcTypeName("PsetAttributeDefinition")]
	public class PsetAttributeDefinition : IfcBaseBusinessEntity, ITreeNode, IMappableEntity {

		/// <summary>
		/// The Key of the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyPropertySingleValue { get; set; }


		/// <summary>
		/// The Key of the Pset.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPropertySet { get; set; }

		/// <summary>
		/// The Name of the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[RegexValidator(@"^[a-zA-Z0-9_]*$")]
		public string AttributeName { get; set; }

		/// <summary>
		/// The order of the Attribute in the pset.
		/// </summary>
		[DataMember]
		public int AttributeOrder { get; set; }

		/// <summary>
		/// The Attribute default value.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeDefaultValue { get; set; }


		/// <summary>
		/// Gets or sets the description of the Attribute.
		/// </summary>
		/// <value>The Description.</value>
		[AntiXssValidator]
		[DataMember]
		public string AttributeDescription { get; set; }


		/// <summary>
		/// Gets or sets if the Attribute can be left blank.
		/// </summary>
		[DataMember]
		public bool? AttributeAllowBlank { get; set; }

		/// <summary>
		/// Gets or sets the datatype of the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeDataType { get; set; }

		/// <summary>
		/// Gets or sets the component used to edit the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeComponent { get; set; }

		/// <summary>
		/// Gets or sets the max value of the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeMaxValue { get; set; }

		/// <summary>
		/// Gets or sets the min value of the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeMinValue { get; set; }

		/// <summary>
		/// Gets or sets the Mask input of the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeMask { get; set; }

		/// <summary>
		/// Gets or sets the name of the validation function of the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeVType { get; set; }

		/// <summary>
		/// Gets or sets the name of the list of values for the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeList { get; set; }

		/// <summary>
		/// Gets or sets the name of the localized label of the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeMsgCode { get; set; }

		/// <summary>
		/// Gets or sets the value of the localized label of the Attribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeLabel {
			get {
				if (this.AttributeMsgCode == null)
					return null;
				return Helper.LocalizeText(this.AttributeMsgCode);
			}
			set { }
		}



		/// <summary>
		/// Gets sets the width of the Attribute.
		/// </summary>
		[DataMember]
		public int? AttributeWidth { get; set; }


		/// <summary>
		/// Gets or sets the fact that the attribute should appear on its own tab, or directly on the entity.
		/// </summary>
		[DataMember]
		public bool? AttributeHasTab { get; set; }

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				id = this.KeyPropertySingleValue,
				entity = this,
				leaf = true,
				iconCls = this.iconCls,
				qtip = this.AttributeDescription,
				text = string.IsNullOrWhiteSpace(this.AttributeLabel) ? this.AttributeName : this.AttributeLabel
			};
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }
	}
}

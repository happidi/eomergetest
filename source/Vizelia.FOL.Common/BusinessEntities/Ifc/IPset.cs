﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Interface for entity exposing a PropertySetList attribute.
	/// </summary>
	public interface IPset {

		/// <summary>
		/// PsetValues list.
		/// </summary>
		List<PsetValue> PsetValues { get; set; }

		/// <summary>
		/// The property set list attribute.
		/// </summary>
		List<Pset> PropertySetList { get; set; }
	}
}

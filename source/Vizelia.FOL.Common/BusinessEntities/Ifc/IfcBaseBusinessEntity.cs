﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Base class for Ifc entities.
	/// </summary>
	[Serializable]
	[DataContract]
	public class IfcBaseBusinessEntity : BaseBusinessEntity {

		//private string _IfcType;
		///// <summary>
		///// Converts the object to an Element.
		///// </summary>
		///// <returns></returns>
		//public Location ToLocation() {
		//    return new Location {
		//        KeyLocation = (string)this.GetPropertyValue(Helper.GetPropertyNameId(this.GetType())),
		//        TypeName = this.IfcType,
		//        Name = (string)this.GetPropertyValue("Name"),
		//        IconCls = this.iconCls
		//    };
		//}

		/// <summary>
		/// The Ifc type of the entity
		/// </summary>
		[DataMember]
		public string IfcType {
			get {
				return Helper.GetAttributeValue<IfcTypeNameAttribute>(this.GetType());
			}
			set { }
		}

		/// <summary>
		/// The iconCls of the entity. The value is retreived from the attribute IconClsAttribute that decorates the entity.
		/// This property is not exposed to client.
		/// </summary>
		protected override string iconCls {
			get {
				return Helper.GetAttributeValue<IconClsAttribute>(this.GetType());
			}
		}

	}
}

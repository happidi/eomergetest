﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for a PsetDefinition.
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyPropertySet")]
	[IconCls("viz-icon-small-pset")]
	[IfcTypeName("PsetDefinition")]
	public class PsetDefinition : IfcBaseBusinessEntity, ITreeNode, IMappableEntity
	{

		/// <summary>
		/// PsetDefinition Constructor.
		/// </summary>
		public PsetDefinition() {
			this.Attributes = new List<PsetAttributeDefinition>();
		}

		/// <summary>
		/// The Key of the Pset.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyPropertySet { get; set; }

		/// <summary>
		/// The Name of the Pset.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[RegexValidator(@"^[a-zA-Z0-9_]*$")]
		//[RegexValidator(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")]
		public string PsetName { get; set; }

		/// <summary>
		/// The name of the object the Pset is linked to.
		/// </summary>
		[DomainValidator("IfcTask", "IfcActionRequest", "IfcSite", "IfcBuilding", "IfcBuildingStorey", "IfcSpace", "IfcFurniture", "IfcOccupant", "IfcClassificationItem", "IfcMeter", MessageTemplate = "{0} is not valid for {1}")]
		[AntiXssValidator]
		[DataMember]
		public string UsageName { get; set; }


		/// <summary>
		/// Gets or sets the description of the pset.
		/// </summary>
		/// <value>The Description.</value>
		[AntiXssValidator]
		[DataMember]
		public string PsetDescription { get; set; }

		/// <summary>
		/// Gets or sets the attributes of the pset.
		/// </summary>
		[DataMember]
		public List<PsetAttributeDefinition> Attributes { get; set; }

		/// <summary>
		/// The iconCls of the entity. The value is retreived from the special classifications stored in ClassificationHelper.
		/// This property is not exposed to client.
		/// </summary>
		protected override string iconCls {
			get {
				string value = Helper.GetAttributeValue<IconClsAttribute>(this.GetType());
				if (!String.IsNullOrEmpty(UsageName)) {
					Type t = Type.GetType(this.GetType().Namespace + "." + this.UsageName.Replace("Ifc", ""));
					if (t != null) {
						string newIcon = Helper.GetAttributeValue<IconClsAttribute>(t);
						if (!String.IsNullOrEmpty(newIcon))
							value = newIcon;
					}

				}
				return value;
			}
		}

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				id = this.KeyPropertySet,
				entity = this,
				leaf = false,
				iconCls = this.iconCls,
				qtip = this.PsetName + '\n' + this.PsetDescription,
				text = string.IsNullOrWhiteSpace(this.Label) ? this.PsetName : this.Label
			};
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the name of the localized label of the CalendarEventCategory.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MsgCode { get; set; }

		/// <summary>
		/// Gets the localized label of the CalendarEventCategory.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Label {
			get {
				return Helper.LocalizeText(MsgCode);
			}
			set { ;}
		}
	}
}

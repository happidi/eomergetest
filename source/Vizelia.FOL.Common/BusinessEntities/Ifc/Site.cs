﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {


	/// <summary>
	/// Business entity for IfcSite
	/// </summary>
	[DataContract]
	[Key("KeySite")]
	[IfcTypeName("IfcSite")]
	[IconCls("viz-icon-small-site")]
	[LocalizedText("msg_site")]
	[AuditableEntityAttribute]
	public class Site : IfcBaseBusinessEntity, ITreeNode, IPset, ILocation, IMappableEntity {
		/// <summary>
		/// Initializes a new instance of the <see cref="Site"/> class.
		/// </summary>
		public Site() {
			PropertySetList = new List<Pset>();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Site"/> class.
		/// </summary>
		/// <param name="location">The location.</param>
		public Site(Location location) {
			this.KeySite = location.KeyLocation;
			this.Name = location.Name;
			this.KeySiteParent = location.KeyParent;
			this.Description = location.Description;
			this.LocalId = location.LocalId;
		}

		/// <summary>
		/// Key of the Site.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(Location))]
		public string KeySite { get; set; }

		/// <summary>
		/// The external identifier of the Site.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Name of the Site.
		/// </summary>
		[Information("msg_name", null)]
		[AntiXssValidator]
		[DataMember]
		[NotNullValidator]
		//[StringLengthValidator(1, RangeBoundaryType.Inclusive, 255, RangeBoundaryType.Inclusive, MessageTemplateResourceName = "error_msg_stringlengthvalidator", MessageTemplateResourceType = typeof(Langue))]
		[VizStringLengthValidator(1, 255)]
		public string Name { get; set; }

		/// <summary>
		/// Description of the Site.
		/// </summary>
		[Information("msg_description", null)]
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Key of the parent Site.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[IgnoreNulls] // Don't validate using the next validator if the value of the property is null.
		[PropertyComparisonValidator("KeySite", ComparisonOperator.NotEqual, MessageTemplateResourceName = "error_entity_contains_cyclic_reference", MessageTemplateResourceType = typeof(Langue))]
		public string KeySiteParent { get; set; }

		/// <summary>
		/// ObjectType of the Site.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ObjectType { get; set; }

		/// <summary>
		/// Convert this Site into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				id = this.KeySite,
				Key = this.KeySite,
				text = this.Name,
				leaf = false,
				iconCls = this.iconCls,
				qtip = this.Description,
				entity = this
			};
		}

		/// <summary>
		/// The list of pset of the Site.
		/// </summary>
		[DataMember]
		public List<Pset> PropertySetList { get; set; }

		/// <summary>
		/// PsetValues list.
		/// </summary>
		public List<PsetValue> PsetValues { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			Type type = GetType();

			var securableObject = new SecurableEntity {
				KeySecurable = KeySite,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(type),
				SecurableName = Name,
				SecurableTypeName = type.FullName
			};

			return securableObject;
		}

		/// <summary>
		/// Return a Location Entity.
		/// </summary>
		/// <returns></returns>
		public Location GetLocation() {
			return new Location {
				IconCls = this.iconCls,
				KeyLocation = this.KeySite,
				Name = this.Name,
				TypeName = this.IfcType.ParseAsEnum<HierarchySpatialTypeName>(),
				KeyParent = this.KeySiteParent,
				Description = this.Description,
				LocalId = this.LocalId
			};
		}
	}
}
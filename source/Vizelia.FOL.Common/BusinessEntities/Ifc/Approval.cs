﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {


	/// <summary>
	/// Business entity for an IfcApproval.
	/// </summary>
	[DataContract]
	[Key("KeyApproval")]
	[IfcTypeName("IfcApproval")]
	public class Approval : IfcBaseBusinessEntity {
		/// <summary>
		/// The Key of the Approval.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyApproval {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>The identifier.</value>
		[AntiXssValidator]
		[DataMember]
		public string Identifier { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		[AntiXssValidator]
		[DataMember]
		public string Name {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>The decription.</value>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the history.
		/// </summary>
		/// <value>The history.</value>
		[AntiXssValidator]
		[DataMember]
		public string History { get; set; }

		/// <summary>
		/// Gets or sets the status.
		/// </summary>
		/// <value>The status.</value>
		[AntiXssValidator]
		[DataMember]
		public string Status { get; set; }

		/// <summary>
		/// Gets or sets the level.
		/// </summary>
		/// <value>The level.</value>
		[AntiXssValidator]
		[DataMember]
		public string Level { get; set; }

		/// <summary>
		/// The Key of the ActionRequest.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyActionRequest {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the key actor.
		/// </summary>
		/// <value>The key actor.</value>
		[AntiXssValidator]
		[DataMember]
		public string KeyActor { get; set; }
		
		/// <summary>
		/// The first name of the approver.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ApproverFirstName { get; set; }


		/// <summary>
		/// The last name of the approver.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ApproverLastName { get; set; }


		/// <summary>
		/// The mail of the approver.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ApproverMail { get; set; }
		
		/// <summary>
		/// Gets or sets the approval date time.
		/// </summary>
		/// <value>The approval date time.</value>
		[DataMember]
		public DateTime? ApprovalDateTime { get; set; }
	}
}

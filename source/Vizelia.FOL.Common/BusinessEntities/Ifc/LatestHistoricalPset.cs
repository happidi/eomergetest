﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Latest Historical Pset
	/// </summary>
	[Serializable]
	[DataContract]
	public class LatestHistoricalPset {

		/// <summary>
		/// The key of the object the pset applies to.
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string KeyObject { get; set; }

		/// <summary>
		/// The name of the object the pset applies to.
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string ObjectName { get; set; }
		
		/// <summary>
		/// The pset name
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string PsetMsgCode { get; set; }

		/// <summary>
		/// The pset key
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string KeyPropertySet { get; set; }

		/// <summary>
		/// The pset attribute name
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string AttributeMsgCode { get; set; }

		/// <summary>
		/// The timestamp of the historical pset value
		/// </summary>
		[DataMember]
		public DateTime AttributeAcquisitionDateTime { get; set; }

		/// <summary>
		/// The historical pset value
		/// </summary>
		[DataMember]
		public double AttributeValue { get; set; }
	}
}
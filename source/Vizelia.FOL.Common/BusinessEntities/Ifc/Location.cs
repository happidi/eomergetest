﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an Ifc Location (Site, Building, BuildingStorey, Space, etc...)
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyLocation")]
	[IconCls("viz-icon-small-site")]
	[LocalizedText("msg_azman_filterspatial")]
	public class Location : BaseBusinessEntity, ISecurableObject, ITreeNode, IHierarchy, IMappableEntity, IPset {
		/// <summary>
		/// Initializes a new instance of the <see cref="Location"/> class.
		/// </summary>
		public Location() {
			PropertySetList = new List<Pset>();
			PsetValues = new List<PsetValue>();
		}
		/// <summary>
		/// The Key of the Location.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyLocation { get; set; }

		/// <summary>
		/// Gets or sets the key children.
		/// Used only for filtering Location by user projects permissions!
		/// </summary>
		/// <value>
		/// The key children.
		/// </value>
		[FilterType(typeof(Location))]
		public string KeyChildren { get; set; }

		/// <summary>
		/// The TypeName of the Location.
		/// </summary>
		[DataMember]
		public HierarchySpatialTypeName TypeName { get; set; }

		/// <summary>
		/// The TypeName of the Location.
		/// </summary>
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public LocationType LocationType {
			get { return (LocationType)TypeName; }
			set { this.TypeName = (HierarchySpatialTypeName)value; }
		}

		/// <summary>
		/// The Name of the Location.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string Name { get; set; }

		/// <summary>
		/// The Ifc type of the entity
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IfcType {
			get;
			set;
		}

		/// <summary>
		/// The IconCls of the Location.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconCls {
			get {
				var retVal = Helper.GetIconClsByIfcType(TypeName.ToString());
				return retVal;
			}
			set { }
		}


		/// <summary>
		/// The location id path of the Location.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalIdPath { get; set; }


		/// <summary>
		/// The Key location path of the Location.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocationPath { get; set; }

		/// <summary>
		/// The location long path of the Location.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LongPath { get; set; }

		/// <summary>
		/// The location short path of the Location.
		/// Contains only the deepest site level and further levels.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ShortPath { get; set; }

		/// <summary>
		/// Gets or sets the level.
		/// </summary>
		/// <value>
		/// The level.
		/// </value>
		[DataMember]
		public int Level { get; set; }


		/// <summary>
		/// Gets or sets the key of the  parent Location.
		/// </summary>
		/// <value>
		/// The key parent.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyParent { get; set; }

		/// <summary>
		/// Convert this Location into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				Key = this.KeyLocation,
				text = this.Name,
				iconCls = this.IconCls,
				id = this.KeyLocation,
				leaf = false,
				entity = this
			};
		}

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>The SecurableEntity object.</returns>
		public SecurableEntity GetSecurableObject() {
			var retVal = new SecurableEntity() {
				KeySecurable = this.KeyLocation,
				SecurableIconCls = this.iconCls,
				SecurableName = this.Name,
				SecurableTypeName = this.GetType().FullName

			};
			return retVal;
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Description of the Location.
		/// </summary>
		[Information("msg_description", null)]
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Elevation of the BuildingStorey.
		/// </summary>
		[Information("msg_elevation", null)]
		[DataMember]
		public double? Elevation { get; set; }

		/// <summary>
		/// The area of the Space.
		/// </summary>
		[Information("msg_space_areavalue", null)]
		[DataMember]
		public double AreaValue { get; set; }

		/// <summary>
		/// The key of the classification of the Furniture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItem { get; set; }

		/// <summary>
		/// The list of pset of the Space.
		/// </summary>
		[DataMember]
		public List<Pset> PropertySetList { get; set; }

		/// <summary>
		/// PsetValues list.
		/// </summary>
		public List<PsetValue> PsetValues { get; set; }

		/// <summary>
		/// Gets or sets the children.
		/// </summary>
		/// <value>
		/// The children.
		/// </value>
		[DataMember]
		public List<Location> Children { get; set; }
	}
}

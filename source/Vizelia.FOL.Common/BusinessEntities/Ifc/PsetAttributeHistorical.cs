﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for a historical pset attribute. (A collection of Date/Values).
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyPsetAttributeHistorical")]
	[IfcTypeName("IfcPsetHistorical")]
	public class PsetAttributeHistorical : IfcBaseBusinessEntity, IEnergyAggregatorNotifyableEntity, IMappableEntity
	{
		/// <summary>
		/// The Key of the PsetAttributeHistorical.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyPsetAttributeHistorical { get; set; }

		/// <summary>
		/// The Key of the PsetAttributeDefinition the attribute is attached to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPropertySingleValue { get; set; }

		/// <summary>
		/// The Key of the Object the attribute is attached to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyObject { get; set; }

		/// <summary>
		/// The name of the object type.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string UsageName { get; set; }

		/// <summary>
		/// The AcquisitionDateTime of the PsetAttribute.
		/// </summary>
		[DataMember]
		public DateTime AttributeAcquisitionDateTime { get; set; }

		/// <summary>
		/// The Valeu of the PsetAttribute.
		/// </summary>
		[DataMember]
		public double AttributeValue { get; set; }

		/// <summary>
		/// The Comment of the PsetAttribute.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeComment { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId { get; set; }
	}
}

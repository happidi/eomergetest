﻿using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;


namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for a ClassificationDefinition.
	/// This class exposes the definition for a specific branch of classification (i.e space, organization, request, worktype etc...)
	/// </summary>
	[DataContract]
	[Key("Category")]
	public class ClassificationDefinition : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// The category of the ClassificationDefinition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Category { get; set; }

		/// <summary>
		/// The LocalId of the root ClassificationItem the definition is referring to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// The iconCls that the branch should use.
		/// If null the standard colored icon based of the R, G, B value of the ClassificationItem will be used.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconCls { get; set; }

		/// <summary>
		/// True to display the iconCls only on the Root Node, false to always display it.
		/// </summary>
		[DataMember]
		public bool IconRootOnly { get; set; }
	}
}



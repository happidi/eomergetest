﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Container class for all audit entities.	
	/// </summary>
	[DataContract]
	[Key("AuditId")]
	public class Audit : BaseBusinessEntity {
		/// <summary>
		/// Gets or sets the audit id.
		/// </summary>
		/// <value>
		/// The audit id.
		/// </value>
		[DataMember]
		public string AuditId { get; set; }

		/// <summary>
		/// Gets or sets the change time.
		/// </summary>
		/// <value>
		/// The change time.
		/// </value>
		[DataMember]
		public DateTime ChangeTime { get; set; }

		/// <summary>
		/// Gets or sets the modified by.
		/// </summary>
		/// <value>
		/// The modified by.
		/// </value>
		[DataMember]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the changed fields.
		/// </summary>
		/// <value>
		/// The changed fields.
		/// </value>
		[DataMember]
		public string ChangedFields { get; set; }

		/// <summary>
		/// Gets or sets the type of the DML.
		/// </summary>
		/// <value>
		/// The type of the DML.
		/// </value>
		[DataMember]
		public string DMLType { get; set; }

		/// <summary>
		/// Gets or sets the operation.
		/// </summary>
		/// <value>
		/// The operation.
		/// </value>
		[DataMember]
		public AuditOperation Operation { get; set; }

		/// <summary>
		/// The IconCls of the operation.
		/// </summary>
		[DataMember]
		public string IconCls {
			get {
				var retVal = "viz-icon-small-";
				switch (DMLType) {
					case "I":
						retVal += "add";
						break;
					case "U":
						retVal += "update";
						break;
					case "D":
						retVal += "delete";
						break;
				}
				return retVal;
			}
			private set { }
		}

		/// <summary>
		/// Gets or sets the entity.
		/// </summary>
		/// <value>
		/// The entity.
		/// </value>
		[DataMember]
		public List<SimpleListElementGeneric<object>> Entity { get; set; }
	}
}

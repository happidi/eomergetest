﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Generic Detailed Audit entity
	/// </summary>
	[DataContract]
	[Key("AuditId")]
	public class DetailedAudit : BaseBusinessEntity {
		/// <summary>
		/// Gets or sets the audit id.
		/// </summary>
		/// <value>
		/// The audit id.
		/// </value>
		[DataMember]
		public string AuditId { get; set; }

		/// <summary>
		/// Gets or sets the change time.
		/// </summary>
		/// <value>
		/// The change time.
		/// </value>
		[DataMember]
		public DateTime ChangeTime { get; set; }

		/// <summary>
		/// Gets or sets the modified by.
		/// </summary>
		/// <value>
		/// The modified by.
		/// </value>
		[DataMember]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the changed fields.
		/// </summary>
		/// <value>
		/// The changed fields.
		/// </value>
		[DataMember]
		public List<AuditValues> ChangedFields { get; set; }
	}
}

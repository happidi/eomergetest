﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// 
	/// </summary>
	[Key("KeyAuditEntity")]
	[DataContract]
	[Serializable]
	public class AuditEntity : BaseBusinessEntity {

		/// <summary>
		/// Gets or sets the key audit entity.
		/// </summary>
		/// <value>
		/// The key audit entity.
		/// </value>
		[DataMember]
		public string KeyAuditEntity { get; set; }

		/// <summary>
		/// Gets the application.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Application { get; set; }

		/// <summary>
		/// Gets or sets the name of the entity.
		/// </summary>
		/// <value>
		/// The name of the entity.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string EntityName { get; set; }

		/// <summary>
		/// Gets or sets the icon CLS.
		/// </summary>
		/// <value>
		/// The icon CLS.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string IconCls {
			get {
				var retVal = "";
				if (EntityName.StartsWith("AuthorizationItem")) {
					switch (EntityName.Split('.')[1]) {
						case "Filter":
							retVal = AuthorizationItem.IconClsFilter;
							break;
						case "Role":
							retVal = AuthorizationItem.IconClsRole;
							break;
						case "Task":
							retVal = AuthorizationItem.IconClsTask;
							break;
						case "Operation":
							retVal = AuthorizationItem.IconClsOperation;
							break;
					}
				}
				else if (EntityType != null) {
					retVal = Helper.GetAttributeValue<IconClsAttribute>(EntityType);
				}
				return retVal;
			}
			private set { }
		}

		/// <summary>
		/// Gets or sets the entity name localized.
		/// </summary>
		/// <value>
		/// The entity name localized.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string EntityNameLocalized {
			get {
				var retVal = "";
				if (EntityName.StartsWith("AuthorizationItem")) {
					switch (EntityName.Split('.')[1]) {
						case "Filter":
							retVal = AuthorizationItem.MsgCodeFilter;
							break;
						case "Role":
							retVal = AuthorizationItem.MsgCodeRole;
							break;
						case "Task":
							retVal = AuthorizationItem.MsgCodeTask;
							break;
						case "Operation":
							retVal = AuthorizationItem.MsgCodeOperation;
							break;
					}
				}
				else if (EntityType != null) {
					retVal = Helper.GetAttributeValue<LocalizedTextAttribute>(EntityType);
				}
				return string.IsNullOrWhiteSpace(retVal) ? EntityName : Langue.ResourceManager.GetString(retVal);
			}
			private set { }
		}

		private Type m_EntityType;

		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <value>
		/// The type of the entity.
		/// </value>
		public Type EntityType {
			get {
				if (m_EntityType == null) {
					var baseBusinessEntityType = typeof(BaseBusinessEntity);
					var name = baseBusinessEntityType.FullName.Replace(baseBusinessEntityType.Name, EntityName);
					m_EntityType = baseBusinessEntityType.Assembly.GetType(name);
				}
				return m_EntityType;
			}
			set { m_EntityType = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is audit enabled.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is audit enabled; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsAuditEnabled { get; set; }
	}
}

﻿using System;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Interface for entity that any changes are audited.
	/// </summary>
	public interface IAuditEntity {
		/// <summary>
		/// The Key of the user that last modified the entity.
		/// </summary>
		string AuditModifiedBy { get; set; }

		/// <summary>
		/// The login of the user that last modified the entity.
		/// </summary>
		string AuditModifiedByLogin { get; set; }

		/// <summary>
		/// The first name of the user that last modified the entity.
		/// </summary>
		string AuditModifiedByFirstName { get; set; }

		/// <summary>
		/// The last name of the user that last modified the entity.
		/// </summary>
		string AuditModifiedByLastName { get; set;}


		/// <summary>
		/// The date and time of the last modification.
		/// </summary>
		DateTime? AuditTimeLastModified { get; set; }

		

	}
}

﻿using System.Runtime.Serialization;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Generic audit record with Field, Old and New Values.
	/// </summary>
	[DataContract]
	public class AuditValues {
		/// <summary>
		/// Gets or sets the name of the field.
		/// </summary>
		/// <value>
		/// The name of the field.
		/// </value>
		[DataMember]
		public string FieldName { get; set; }

		/// <summary>
		/// Gets or sets the old value.
		/// </summary>
		/// <value>
		/// The old value.
		/// </value>
		[DataMember]
		public object OldValue { get; set; }

		/// <summary>
		/// Gets or sets the new value.
		/// </summary>
		/// <value>
		/// The new value.
		/// </value>
		[DataMember]
		public object NewValue { get; set; }
	}
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for getting running versions of components
	/// </summary>
	[DataContract]
	public class Versions : BaseBusinessEntity {
		private string _extjs;
		private string _vizjs;
		private string _clr;
		private string _culture;

		/// <summary>
		/// Public ctor.
		/// </summary>
		public Versions() {
			//We need to get the cultures as a param to avoid a circular reference
			_clr = Helper.GetClrVersion();
			_culture = Helper.GetCurrentApplicableCulture();
		}

		/// <summary>
		/// Current culture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Culture {
			get {
				return _culture;
			}
			set {
				_culture = value;
			}
		}

		/// <summary>
		/// Version of CLR.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string CLR {
			get {
				return _clr;
			}
			set {
				_clr = value;
			}
		}

		/// <summary>
		/// Version of Extjs.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Extjs {
			get {
				return _extjs;
			}
			set {
				_extjs = value.Replace("_", ".");
			}
		}

		/// <summary>
		/// Version of Vizjs.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Vizjs {
			get {
				return _vizjs;
			}
			set {
				_vizjs = value.Replace("_", ".");
			}
		}

	}
}

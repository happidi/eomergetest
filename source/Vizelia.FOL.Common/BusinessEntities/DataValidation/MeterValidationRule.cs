﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;


namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A meter validation rule.
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyMeterValidationRule")]
	[IconCls("viz-icon-small-metervalidationrule")]
	public class MeterValidationRule : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Key of the MeterValidation.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyMeterValidationRule { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		[NotNullValidator]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="MeterValidationRule"/> is enable.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public bool Enabled { get; set; }

		/// <summary>
		/// Gets or sets the acquisition frequency.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public int? AcquisitionFrequency { get; set; }

		/// <summary>
		/// Gets or sets the acquisition interval.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public AxisTimeInterval AcquisitionInterval { get; set; }

		/// <summary>
		/// Gets or sets the delay.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public int? Delay { get; set; }

		/// <summary>
		/// Gets or sets the delay interval.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public AxisTimeInterval DelayInterval { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [increase only].
		/// </summary>
		/// <value>
		///   <c>true</c> if [increase only]; otherwise, <c>false</c>.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public bool IncreaseOnly { get; set; }

		/// <summary>
		/// Gets or sets the spike detection percentage.
		/// </summary>
		/// <value>
		/// The spike detection.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public double? SpikeDetection { get; set; }


		/// <summary>
		/// Gets or sets the spike detection absolute thresold to begin calculation the %.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public double? SpikeDetectionAbsThreshold { get; set; }

		/// <summary>
		/// Gets or sets the max recurring values.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public int? MaxRecurringValues { get; set; }

		/// <summary>
		/// Gets or sets the min value.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public double? MinValue { get; set; }

		/// <summary>
		/// Gets or sets the max value.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public double? MaxValue { get; set; }

		/// <summary>
		/// List of emails to send notification to. 
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Emails { get; set; }

		/// <summary>
		/// Gets or sets the meters.
		/// </summary>
		[DataMember]
		public List<Meter> Meters { get; set; }

		/// <summary>
		/// Gets or sets the Key of the LocalizationCulture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocalizationCulture { get; set; }

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		[DataMember]
		[ViewableByDataModel]
		public DateTime? StartDate { get; set; }

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		[DataMember]
		[ViewableByDataModel]
		public DateTime? EndDate { get; set; }

		/// <summary>
		/// Gets or sets the last processe datetime
		/// </summary>
		[DataMember]
		[ViewableByDataModel]
		public DateTime? LastProcessDateTime { get; set; }

		/// <summary>
		/// Gets or sets the validity.
		/// </summary>
		/// <value>
		/// The validity.
		/// </value>
		[DataMember]
		[ViewableByDataModel]
		public MeterDataValidity? Validity { get; set; }

		/// <summary>
		/// Gets or sets the filter meter classifications.
		/// </summary>
		/// <value>
		/// The filter meter classifications.
		/// </value>
		public List<ClassificationItem> FilterMeterClassifications { get; set; }

		/// <summary>
		/// Gets or sets the filter spatial.
		/// </summary>
		/// <value>
		/// The filter spatial.
		/// </value>
		public List<Location> FilterSpatial { get; set; }
	}
}

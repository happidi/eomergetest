﻿namespace Vizelia.FOL.BusinessEntities {
	
	/// <summary>
	/// Represents an entity that can be serialized and deserialized to/from a flat file in a delimited format, such as CSV.
	/// </summary>
	public interface ICsvSerializable {
		/// <summary>
		/// Sets the value for the specified property name.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		void Set(string propertyName, string value);
	}
}
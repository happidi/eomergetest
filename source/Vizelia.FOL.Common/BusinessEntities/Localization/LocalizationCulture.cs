﻿using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for a localization culture.
	/// </summary>
	[Key("Key")]
	[DataContract]
	public class LocalizationCulture : BaseBusinessEntity {
		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>The key.</value>
		[AntiXssValidator]
		[DataMember]
		public string Key { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		[AntiXssValidator]
		[DataMember]
		public string Value { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for a localization resource.
	/// </summary>
	[Key("Id")]
	[DataContract]
	public class LocalizationResource : BaseBusinessEntity, IMappableEntity, IEnergyAggregatorNotifyableEntity {
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string Id { get; set; }

		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>The key.</value>
		[AntiXssValidator]
		[DataMember]
		public string Key { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		[AntiXssValidator]
		[DataMember]
		public string Value { get; set; }

		/// <summary>
		/// Gets or sets the culture.
		/// </summary>
		/// <value>
		/// The culture.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Culture { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [is user defined].
		/// </summary>
		/// <value>
		///   <c>true</c> if [is user defined]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsUserDefined { get; set; }
	}
}

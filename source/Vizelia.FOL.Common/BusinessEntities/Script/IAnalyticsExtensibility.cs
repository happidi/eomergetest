﻿using System;
using System.Collections.Generic;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Interface to implement for C# scripting
	/// </summary>
	public interface IAnalyticsExtensibility {
		/// <summary>
		/// Executes the specified chart.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		/// <param name="inputs">The inputs.</param>
		Tuple<Chart, List<DataSerie>, Dictionary<string, string>> Execute(EnergyAggregatorContext context, Chart chart, List<DataSerie> series, Dictionary<string, string> inputs);


		/// <summary>
		/// Gets the public inputs.
		/// </summary>
		/// <returns></returns>
		Dictionary<string, string> GetPublicInputs();

		/// <summary>
		/// Gets the data series local id.
		/// </summary>
		/// <returns></returns>
		List<ListElement> GetDataSeriesLocalId();

	}
}

using System;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A Algorithm host class for holding the algorithm instance and its sandbox, if such exists.
	/// </summary>
	/// <typeparam name="TAlgorithmInterface">The type of the algorithm interface.</typeparam>
	public class AlgorithmInstance<TAlgorithmInterface> : IDisposable {
		private readonly AppDomain m_SandboxAppDomain;

		/// <summary>
		/// Gets or sets the algorithm.
		/// </summary>
		/// <value>
		/// The algorithm.
		/// </value>
		public TAlgorithmInterface Algorithm { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="AlgorithmInstance{TScriptInterface}"/> class.
		/// </summary>
		/// <param name="algorithm">The algorithm.</param>
		/// <param name="appDomain">The app domain.</param>
		public AlgorithmInstance(TAlgorithmInterface algorithm, AppDomain appDomain) {
			Algorithm = algorithm;
			m_SandboxAppDomain = appDomain;

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AlgorithmInstance{TScriptInterface}"/> class.
		/// </summary>
		/// <param name="algorithm">The algorithm.</param>
		public AlgorithmInstance(TAlgorithmInterface algorithm) {
			Algorithm = algorithm;
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public void Dispose() {
			if(m_SandboxAppDomain != null) {
				try {
					var disposable = Algorithm as IDisposable;
					
					if(disposable != null) {
						disposable.Dispose();
					}
					m_SandboxAppDomain.AssemblyResolve -= AlgorithmHelper.ResolveAssembly;
					AppDomain.Unload(m_SandboxAppDomain);
				}
				catch (Exception e) {
					TracingService.Write(e);
				}
			}
		}
	}
}
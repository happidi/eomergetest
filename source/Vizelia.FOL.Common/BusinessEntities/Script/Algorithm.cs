﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;
using System.Collections.Generic;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an Algorithm item
	/// </summary>
	[DataContract]
	[Serializable]
	[Key("KeyAlgorithm")]
	public class Algorithm : BaseBusinessEntity, ISupportDocuments, IEnergyAggregatorNotifyableEntity {
		/// <summary>
		/// Key of the Algorithm.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyAlgorithm { get; set; }


		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }


		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }


		/// <summary>
		/// Gets or sets the code.
		/// </summary>
		/// <value>
		/// The code.
		/// </value>
		//[AntiXssValidator]
		[DataMember]
		public string Code { get; set; }


		/// <summary>
		/// Gets or sets the language.
		/// </summary>
		/// <value>
		/// The language.
		/// </value>
		[DataMember]
		public AlgorithmLanguage Language { get; set; }

		/// <summary>
		/// Gets or sets the source.
		/// </summary>
		/// <value>
		/// The source.
		/// </value>
		[DataMember]
		public AlgorithmSource Source { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is not dynamically compiled.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is not dynamically compiled; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsNotDynamicallyCompiled { get; set; }

		/// <summary>
		/// Gets or sets the unique id.
		/// </summary>
		/// <value>
		/// The unique id.
		/// </value>
		[DataMember]
		public string UniqueId { get; set; }

		/// <summary>
		/// Gets the Binary document associated (A DLL or archive that contains DLLs).
		/// </summary>
		[DataMember]
		public Document Binary { get; set; }

		/// <summary>
		/// Gets or sets the key of the binary file.
		/// </summary>
		/// <value>
		/// The key binary.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyBinary { get; set; }

		/// <summary>
		/// Gets the propreties that have document ids
		/// </summary>
		/// <returns></returns>
		public List<string> GetDocumentFields() {
			return new List<string> { "KeyBinary" };
		}

		/// <summary>
		/// Renews the unique id.
		/// </summary>
		public void RenewUniqueId() {
			UniqueId = Guid.NewGuid().ToString();
		}
	}
}
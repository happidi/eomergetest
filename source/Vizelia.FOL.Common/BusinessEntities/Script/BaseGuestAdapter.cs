using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Lifetime;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A base guest adapter.
	/// </summary>
	public abstract class BaseGuestAdapter : MarshalByRefObject, IDisposable {

		/// <summary>
		/// Obtains a lifetime service object to control the lifetime policy for this instance.
		/// </summary>
		/// <returns>
		/// An object of type <see cref="T:System.Runtime.Remoting.Lifetime.ILease"/> used to control the lifetime policy for this instance. This is the current lifetime service object for this instance if one exists; otherwise, a new lifetime service object initialized to the value of the <see cref="P:System.Runtime.Remoting.Lifetime.LifetimeServices.LeaseManagerPollTime"/> property.
		/// </returns>
		/// <exception cref="T:System.Security.SecurityException">The immediate caller does not have infrastructure permission. </exception>
		///   
		/// <PermissionSet>
		///   <IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="RemotingConfiguration, Infrastructure"/>
		///   </PermissionSet>
		public override object InitializeLifetimeService() {
			var lease = (ILease)base.InitializeLifetimeService();

			if(lease.CurrentState == LeaseState.Initial) {
				lease.InitialLeaseTime = TimeSpan.Zero;
			}

			return lease;
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public void Dispose() {
			try {
				RemotingServices.Disconnect(this);
			}
			catch (Exception e) {
				TracingService.Write(e);
			}
		}

	}
}
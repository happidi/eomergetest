﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an Algorithm input variable definition.
	/// </summary>
	[DataContract]
	[Serializable]
	[Key("KeyAlgorithmInputDefinition")]
	public class AlgorithmInputDefinition : BaseBusinessEntity {

		/// <summary>
		/// Key of the Algorithm Input Definition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyAlgorithmInputDefinition { get; set; }

		/// <summary>
		/// Key of the Algorithm the input variable is associated to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyAlgorithm { get; set; }
		
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[RegexValidator(@"^[a-zA-Z0-9_]*$")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }
	}

	/// <summary>
	/// Business entity for an Algorithm input variable value.
	/// </summary>
	[DataContract]
	[Serializable]
	[Key("KeyAlgorithmInputValue")]
	public class AlgorithmInputValue : BaseBusinessEntity {
		/// <summary>
		/// Key of the Algorithm input value.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyAlgorithmInputValue { get; set; }

		/// <summary>
		/// Key of the Algorithm input definition, the value is associated to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyAlgorithmInputDefinition { get; set; }


		/// <summary>
		/// Key of the Algorithm (when there is no definition (IAnalyticsExtension, this will represent the type full name).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyAlgorithm { get; set; }
		

		/// <summary>
		/// Gets or sets the key chart.
		/// </summary>
		/// <value>
		/// The key chart.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }

		/// <summary>
		/// Gets the name (readonly).
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[RegexValidator(@"^[a-zA-Z0-9_]*$")]
		public string Name { get; set; }

		/// <summary>
		/// Gets the description (readonly)	.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Value { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether this instance is dirty.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is dirty; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsDirty { get; set; }
	}




}

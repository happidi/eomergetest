using System;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// A base Adapter that runs in the host AppDomain.
	/// </summary>
	/// <typeparam name="TAdapter"></typeparam>
	public abstract class HostAdapter<TAdapter> : IDisposable {
		/// <summary>
		/// The wrapped instance.
		/// </summary>
		protected TAdapter WrappedInstance { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="HostAdapter{TAdapter}"/> class.
		/// </summary>
		/// <param name="instance">The instance.</param>
		protected HostAdapter(TAdapter instance) {
			WrappedInstance = instance;
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public void Dispose() {
			var disposable = WrappedInstance as IDisposable;

			if (disposable != null) {
					disposable.Dispose();
			}
		}
	}
}
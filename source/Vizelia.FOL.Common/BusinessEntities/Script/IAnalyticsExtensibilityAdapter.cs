using System.Globalization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// An adapter interface for the IAnalyticsExtensibility interface, used for cross AppDomain communication
	/// so additional method can perform the communication of objects more efficiently than regular serialization.
	/// This is what the host AppDomain will see.
	/// </summary>
	[AlgorithmAdapter(typeof(IAnalyticsExtensibility))]
	public interface IAnalyticsExtensibilityAdapter : IAnalyticsExtensibility {
		/// <summary>
		/// Sets the name of the context application.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		void SetContextApplicationName(string applicationName);
		/// <summary>
		/// Sets the context URL.
		/// </summary>
		/// <param name="url">The URL.</param>
		void SetContextUrl(string url);
		/// <summary>
		/// Sets the context request id.
		/// </summary>
		/// <param name="requestId">The request id.</param>
		void SetContextRequestId(string requestId);
		/// <summary>
		/// Sets the context culture.
		/// </summary>
		/// <param name="currentCulture">The current culture.</param>
		void SetContextCulture(CultureInfo currentCulture);
		/// <summary>
		/// Sets the context UI culture.
		/// </summary>
		/// <param name="currentUICulture">The current UI culture.</param>
		void SetContextUICulture(CultureInfo currentUICulture);
	}
}
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// The host adapter for the IAnalyticsExtensibility interface.
	/// This is the class the hosting AppDomain will instantiate as IAnalyticsExtensibility, which, in turn, will call
	/// extended functionality on the guest AppDomain to communicate and then run the requested action.
	/// </summary>
	public class AnalyticsExtensibilityHostAdapter : HostAdapter<IAnalyticsExtensibilityAdapter>, IAnalyticsExtensibility {

		/// <summary>
		/// Initializes a new instance of the <see cref="AnalyticsExtensibilityHostAdapter"/> class.
		/// </summary>
		/// <param name="instance">The instance.</param>
		public AnalyticsExtensibilityHostAdapter(IAnalyticsExtensibilityAdapter instance) : base(instance) {
			// Call base.
		}

		/// <summary>
		/// Executes the specified chart.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		public Tuple<Chart, List<DataSerie>, Dictionary<string, string>> Execute(EnergyAggregatorContext context, Chart chart, List<DataSerie> series, Dictionary<string, string> inputs) {
			// Set context stuff.
			WrappedInstance.SetContextApplicationName(ContextHelper.ApplicationName);
			WrappedInstance.SetContextUrl(ContextHelper.Url);
			WrappedInstance.SetContextRequestId(ContextHelper.RequestId);
			WrappedInstance.SetContextCulture(Thread.CurrentThread.CurrentCulture);
			WrappedInstance.SetContextUICulture(Thread.CurrentThread.CurrentUICulture);

			Tuple<Chart, List<DataSerie>, Dictionary<string, string>> tuple = WrappedInstance.Execute(context, chart, series, inputs);
			
			return tuple;
		}

		/// <summary>
		/// Gets the public inputs.
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, string> GetPublicInputs() {
			Dictionary<string, string> inputs = WrappedInstance.GetPublicInputs();
			return inputs;
		}

		/// <summary>
		/// Gets the data series local id.
		/// </summary>
		/// <returns></returns>
		public List<ListElement> GetDataSeriesLocalId() {
			List<ListElement> dataSeriesLocalId = WrappedInstance.GetDataSeriesLocalId();
			return dataSeriesLocalId;
		}
	}
}
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Remoting;
using System.Threading;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// The guest adapter for the IAnalyticsExtensibility interface.
	/// It will be instantiated in the guest AppDomain and exposed via interface to the host AppDomain.
	/// </summary>
	public class AnalyticsExtensibilityGuestAdapter : BaseGuestAdapter, IAnalyticsExtensibilityAdapter {
		private readonly IAnalyticsExtensibility m_Wrapped;
		private readonly string m_AlgorithmClass;

		/// <summary>
		/// Initializes a new instance of the <see cref="AnalyticsExtensibilityGuestAdapter"/> class.
		/// </summary>
		/// <param name="entryPoint">The entry point.</param>
		public AnalyticsExtensibilityGuestAdapter(AlgorithmHelper.AlgorithmEntryPoint entryPoint) {
			ObjectHandle instance = Activator.CreateInstanceFrom(entryPoint.AssemblyFileName, entryPoint.TypeFullName);
			m_Wrapped = (IAnalyticsExtensibility)instance.Unwrap();
			m_AlgorithmClass = entryPoint.TypeFullName;
		}

		/// <summary>
		/// Executes the specified chart.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		public Tuple<Chart, List<DataSerie>, Dictionary<string, string>> Execute(EnergyAggregatorContext context, Chart chart, List<DataSerie> series, Dictionary<string, string> inputs) {
			using (TracingService.StartTracing("Algorithm Execution", m_AlgorithmClass)) {
				var retVal = m_Wrapped.Execute(context, chart, series, inputs);
				return retVal; 
			}
		}

		/// <summary>
		/// Gets the public inputs.
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, string> GetPublicInputs() {
			return m_Wrapped.GetPublicInputs();
		}

		/// <summary>
		/// Gets the data series local id.
		/// </summary>
		/// <returns></returns>
		public List<ListElement> GetDataSeriesLocalId() {
			return m_Wrapped.GetDataSeriesLocalId();
		}

		/// <summary>
		/// Sets the name of the context application.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		public void SetContextApplicationName(string applicationName) {
			ContextHelper.ApplicationName = applicationName;
		}

		/// <summary>
		/// Sets the context URL.
		/// </summary>
		/// <param name="url">The URL.</param>
		public void SetContextUrl(string url) {
			ContextHelper.Url = url;
		}

		/// <summary>
		/// Sets the context request id.
		/// </summary>
		/// <param name="requestId">The request id.</param>
		public void SetContextRequestId(string requestId) {
			ContextHelper.RequestId = requestId;
		}

		/// <summary>
		/// Sets the context culture.
		/// </summary>
		/// <param name="currentCulture">The current culture.</param>
		public void SetContextCulture(CultureInfo currentCulture) {
			Thread.CurrentThread.CurrentCulture = currentCulture;
		}

		/// <summary>
		/// Sets the context UI culture.
		/// </summary>
		/// <param name="currentUICulture">The current UI culture.</param>
		public void SetContextUICulture(CultureInfo currentUICulture) {
			Thread.CurrentThread.CurrentUICulture = currentUICulture;
		}

	}
}
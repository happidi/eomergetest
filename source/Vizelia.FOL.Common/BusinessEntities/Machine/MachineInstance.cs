﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a generic Machine Instance.
	/// </summary>
	[Serializable]
	[Key("KeyMachineInstance")]
	[DataContract]
	[IconCls("viz-icon-small-machineinstance")]
	[LocalizedText("msg_machineinstance")]
	public class MachineInstance : BaseBusinessEntity {
		/// <summary>
		/// Gets or sets the key machine instance.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyMachineInstance { get; set; }

		/// <summary>
		/// Gets or sets the instance name  (in Azure, this is the InstanceName).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string InstanceName { get; set; }

		/// <summary>
		/// Gets or sets the status.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Status { get; set; }

		/// <summary>
		/// Gets or sets the number of in memory objects.
		/// </summary>
		[DataMember]
		public int InMemoryObjectCount { get; set; }

		/// <summary>
		/// Gets or sets the Absolute Uri.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AbsoluteUri { get; set; }

		/// <summary>
		/// Gets or sets the machine purpose (in Azure, this is the RoleName).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MachinePurpose { get; set; }

		/// <summary>
		/// Gets or sets the name of the environment (in Azure, this is the ServiceName).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string EnvironmentName { get; set; }

		/// <summary>
		/// Gets or sets the size.
		/// </summary>
		[DataMember]
		public MachineInstanceSize Size { get; set; }
	}
}

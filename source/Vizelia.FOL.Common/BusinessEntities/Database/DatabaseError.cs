﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing error messages from database.
	/// </summary>
	public class DatabaseError {
		/// <summary>
		/// The returned value of the error.
		/// </summary>
		public int error { get; set; }

		/// <summary>
		/// The message as either a code message or a translated message.
		/// </summary>
		public string msg { get; set; }

		/// <summary>
		/// The field responsible for the error.
		/// </summary>
		public string field { get; set; }
	}

}

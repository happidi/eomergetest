﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.BusinessEntities {
	
	/// <summary>
	/// Business entity for storing the possible errors from database.
	/// This class should be initialized in the ctor of each broker.
	/// </summary>
	public class DatabaseErrors {
		private Dictionary<int, DatabaseError> m_errors;
		/// <summary>
		/// Public ctor.
		/// </summary>
		public DatabaseErrors() {
			m_errors = new Dictionary<int, DatabaseError>();
		}

		/// <summary>
		/// Adds the specified item.
		/// </summary>
		/// <param name="item">The item.</param>
		[Obsolete]
		public void Add(DatabaseError item) {
			m_errors.Add(item.error, item);
		}
		/// <summary>
		/// Adds an error definition.
		/// </summary>
		/// <param name="errorCode">The error value returned by the database.</param>
		/// <param name="errorMessage">The error message.</param>
		/// <param name="fieldName">The name of the field that caused the error.</param>
		public void Add(int errorCode, string errorMessage, string fieldName) {
			m_errors.Add(errorCode, new DatabaseError { error = errorCode, msg = errorMessage, field = fieldName });
		}

		/// <summary>
		/// Determines whether the specified error code is already in the collection.
		/// </summary>
		/// <param name="errorCode">The error code.</param>
		/// <returns>
		/// 	<c>true</c> if the specified error code already exists otherwise, <c>false</c>.
		/// </returns>
		public bool ContainsKey(int errorCode) {
			return m_errors.ContainsKey(errorCode);
		}

		/// <summary>
		/// Gets the <see cref="Vizelia.FOL.BusinessEntities.DatabaseError"/> at the specified index.
		/// </summary>
		/// <value></value>
		public DatabaseError this[int index] {
			get {
				return m_errors[index];
			}
		}

	}
}

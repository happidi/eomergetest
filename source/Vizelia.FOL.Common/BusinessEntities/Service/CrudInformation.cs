﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {


    /// <summary>
    /// Represents CRUD information.
    /// </summary>
	[Serializable]
    [DataContract]
	public class CrudInformation  {

        /// <summary>
        /// Gets or sets the entity.
        /// </summary>
        /// <value>
        /// The entity.
        /// </value>
        [DataMember]
	    public BaseBusinessEntity Entity  { get; set; }

        /// <summary>
        /// Gets or sets the crud action.
        /// </summary>
        /// <value>
        /// The crud action.
        /// </value>
		[DataMember]
	    public string CrudAction { get; set; }


	}
}

﻿using System;
using System.IO;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for a result that contains a stream.
	/// Most common use of this will be to store a document for downloading.
	/// This class is not marked with DataContract as it is not meant to be exposed on client side.
	/// </summary>
	[Serializable]
	public class StreamResult : BaseBusinessEntity {

		private string m_Extension;
		private byte[] m_Content;

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamResult"/> class.
		/// </summary>
		/// <param name="contentStream">The content stream.</param>
		/// <param name="mimeType">The MIME type.</param>
		public StreamResult(MemoryStream contentStream, string mimeType) {
			ContentStream = contentStream;
			MimeType = mimeType;
		}


		/// <summary>
		/// Initializes a new instance of the <see cref="StreamResult"/> class.
		/// </summary>
		/// <param name="contentBytes">The content bytes.</param>
		/// <param name="mimeType">Type of the MIME.</param>
		public StreamResult(byte[] contentBytes, string mimeType) {
			m_Content = contentBytes;
			MimeType = mimeType;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamResult"/> class.
		/// </summary>
		/// <param name="contentBytes">The content bytes.</param>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="mimeType">The MIME type.</param>
		public StreamResult(byte[] contentBytes, string fileName, string fileExtension, string mimeType) {
			m_Content = contentBytes;
			MimeType = mimeType;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamResult"/> class.
		/// </summary>
		/// <param name="filePath">The file path.</param>
		/// <param name="mimeType">Type of the MIME.</param>
		/// <param name="deleteFile">if set to <c>true</c> [delete file].</param>
		public StreamResult(string filePath, string mimeType, bool deleteFile) {
			MimeType = mimeType;
			FileStream fs = new FileStream(filePath,FileMode.Open, FileAccess.Read);
			BinaryReader br = new BinaryReader(fs);
			m_Content = br.ReadBytes((Int32)fs.Length);
			br.Close();
			fs.Close();
			if (deleteFile)
				File.Delete(filePath);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamResult"/> class.
		/// </summary>
		/// <param name="contentStream">The content stream.</param>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="mimeType">The MIME type.</param>
		public StreamResult(MemoryStream contentStream, string fileName, string fileExtension, string mimeType) {
			ContentStream = contentStream;
			FileName = fileName;
			FileExtension = fileExtension;
			MimeType = mimeType;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamResult"/> class.
		/// </summary>
		/// <param name="contentStream">The content stream.</param>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="fileExtension">The file extension.</param>
		public StreamResult(MemoryStream contentStream, string fileName, string fileExtension) {
			ContentStream = contentStream;
			FileName = fileName;
			FileExtension = fileExtension;
		}


		/// <summary>
		/// The content of the report as a stream.
		/// </summary>
		public MemoryStream ContentStream {
			get {
				return m_Content.ToStream();
			}
			set {
				m_Content = value.GetBytes();
			}
		}

		/// <summary>
		/// The mime type of the report.
		/// </summary>
		public string MimeType { get; set; }

		/// <summary>
		/// Gets or sets the name of the file (without extension).
		/// </summary>
		/// <value>
		/// The name of the file (without extension).
		/// </value>
		public string FileName { get; set; }

		/// <summary>
		/// The extension of the file.
		/// </summary>
		public string FileExtension {
			get {
				return m_Extension;
			}
			set {
				m_Extension = (value != null) ? value.TrimStart('.') : null;
			}
		}

		/// <summary>
		/// Gets the full name of the file, containing both the name and extension.
		/// </summary>
		/// <value>
		/// The full name of the file, containing both the name and extension.
		/// </value>
		public string FullFileName {
			get {
				return string.Format("{0}.{1}", FileName, FileExtension);
			}
		}

	}
}

﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for Virtual file that can be attached to a object in order to access it via a VirtualPath.
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyVirtualFile")]
	[LocalizedText("msg_virtualfile")]
	[IconCls("viz-icon-small-virtualfile")]
	public class VirtualFile : BaseBusinessEntity, IMappableEntity, ISecurableObject {

		/// <summary>
		/// Gets the application.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Application { get; set; }

		/// <summary>
		/// The Key of the VirtualFile.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(VirtualFile), false)]
		public string KeyVirtualFile { get; set; }

		/// <summary>
		/// The Key of the entity it is attached to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyEntity { get; set; }

		/// <summary>
		/// The type of the entity it is attached to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string EntityType { get; set; }

		/// <summary>
		/// The path to access the entity directly.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Path { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// The width of the output image.
		/// </summary>
		[DataMember]
		public int Width { get; set; }

		/// <summary>
		/// The height of the output image.
		/// </summary>
		[DataMember]
		public int Height { get; set; }

		/// <summary>
		/// The height of the output image.
		/// </summary>
		[DataMember]
		public int MaxRecordCount { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity() {
				KeySecurable = this.KeyVirtualFile,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(this.GetType()),
				SecurableName = this.Path,
				SecurableTypeName = this.GetType().FullName
			};
		}

	}
}

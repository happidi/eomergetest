﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for the state of a long running operation.
	/// </summary>
	[Serializable]
	[DataContract]
	[KeyAttribute("Id")]
	public class LongRunningOperationState : BaseBusinessEntity {

		/// <summary>
		/// Initializes a new instance of the <see cref="LongRunningOperationState"/> class.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		public LongRunningOperationState(Guid operationId) {
			Id = operationId;
			Status = LongRunningOperationStatus.Initialized;
			ClientRequest = LongRunningOperationClientRequest.None;
		}

		/// <summary>
		/// The id of the operation.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public Guid Id { get; set; }

		/// <summary>
		/// The progress value.
		/// </summary>
		[DataMember]
		public int PercentDone { get; set; }

		/// <summary>
		/// The label of the operation.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Label { get; set; }

		/// <summary>
		/// The status of the operation.
		/// </summary>
		[DataMember]
		public LongRunningOperationStatus Status { get; set; }

		/// <summary>
		/// Gets or sets the client request.
		/// </summary>
		/// <value>
		/// The client request.
		/// </value>
		[DataMember]
		public LongRunningOperationClientRequest ClientRequest { get; set; }

		/// <summary>
		/// The result of the operation.
		/// Not exposed to client.
		/// </summary>
        public object Result { get; set; }

		/// <summary>
		/// Gets or sets the state of the client.
		/// This is a payload property, used to store the state of the caller so when done,
		/// it can be retreived. It is only accessible on server side.
		/// </summary>
		/// <value>
		/// The state of the client.
		/// </value>
		public object ClientState { get; set; }
	}

}

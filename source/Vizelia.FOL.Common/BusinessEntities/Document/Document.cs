﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A class which represents a document which can be associated to any entity or pset
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyDocument")]
	[LocalizedText("msg_component_document")]
	[IconCls("viz-icon-small-document")]
	public class Document : DocumentMetadata {
		private byte[] m_DocumentContents;

		/// <summary>
		/// Initializes a new instance of the <see cref="Document"/> class.
		/// </summary>
		/// <param name="fileStream">The file stream.</param>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="contentType">Type of the content.</param>
		public Document(Stream fileStream, string fileName, string contentType) {
			DocumentContents = fileStream.GetBytes();
			DocumentName = fileName;
			DocumentType = contentType;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Document"/> class.
		/// </summary>
		/// <param name="stream">The stream.</param>
		public Document(StreamResult  stream) {
			DocumentContents = stream.ContentStream.GetBytes();
			DocumentName = stream.FullFileName;
			DocumentType = stream.MimeType;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Document"/> class.
		/// </summary>
		/// <param name="file">The file.</param>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="contentType">Type of the content.</param>
		public Document(byte[] file, string fileName, string contentType) {
			DocumentContents = file;
			DocumentName = fileName;
			DocumentType = contentType;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Document"/> class.
		/// </summary>
		public Document() {
			
		}
		
		/// <summary>
		/// Gets or sets the document contents.
		/// </summary>
		/// <value>
		/// The document contents.
		/// </value>
		[DataMember]
		public byte[] DocumentContents {
			get { return m_DocumentContents; }
			set {
				m_DocumentContents = value;
				DocumentContentsLength = value.Length;
			}
		}
	}
}

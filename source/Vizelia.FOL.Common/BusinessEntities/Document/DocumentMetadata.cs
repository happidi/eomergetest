using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A class that has all the metadata for a document
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyDocument")]
	public class DocumentMetadata : BaseBusinessEntity, IAuditEntity {
		/// <summary>
		/// Gets or sets the id of the document.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyDocument { get; set; }

		/// <summary>
		/// Gets or sets the name of the document.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string DocumentName { get; set; }

		/// <summary>
		/// Gets or sets the type of the document.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string DocumentType { get; set; }

		/// <summary>
		/// The Key of the user that last modified the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AuditModifiedBy { get; set; }

		/// <summary>
		/// The date and time of the last modification.
		/// </summary>
		[DataMember]
		public DateTime? AuditTimeLastModified { get; set; }

		/// <summary>
		/// Gets or sets the length of the contents of the document.
		/// </summary>
		[DataMember]
		public int DocumentContentsLength { get; set; }

		/// <summary>
		/// The login of the user that last modified the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AuditModifiedByLogin {
			get;
			set;
		}

		/// <summary>
		/// The first name of the user that last modified the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AuditModifiedByFirstName {
			get;
			set;
		}

		/// <summary>
		/// The last name of the user that last modified the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AuditModifiedByLastName {
			get;
			set;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Marks an entity that has document properties
	/// </summary>
	public interface ISupportDocuments
	{
		/// <summary>
		/// Gets the propreties that have document ids
		/// </summary>
		/// <returns></returns>
		List<string> GetDocumentFields();
	}
}

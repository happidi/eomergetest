﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// ExternalCrudProviders can choose if to update the energy aggregator based on testing if the entity implements this interface.
	/// </summary>
	public interface IEnergyAggregatorNotifyableEntity
	{
		
	}
}

using System;
using System.Xml.Serialization;

namespace Vizelia.FOL.BusinessEntities.EWS {
	/// <summary>
	/// A simple container item.
	/// </summary>
	[Serializable]
	[XmlType(Namespace = "http://www.schneider-electric.com/common/dataexchange/2011/05")]
	public class ContainerItemSimpleType : object, System.ComponentModel.INotifyPropertyChanged {

		private string idField;

		private string nameField;

		private string descriptionField;

		private string typeField;

		/// <remarks/>
		[XmlElement(Order = 0)]
		public string Id {
			get {
				return this.idField;
			}
			set {
				this.idField = value;
				this.RaisePropertyChanged("Id");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 1)]
		public string Name {
			get {
				return this.nameField;
			}
			set {
				this.nameField = value;
				this.RaisePropertyChanged("Name");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 2)]
		public string Description {
			get {
				return this.descriptionField;
			}
			set {
				this.descriptionField = value;
				this.RaisePropertyChanged("Description");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 3)]
		public string Type {
			get {
				return this.typeField;
			}
			set {
				this.typeField = value;
				this.RaisePropertyChanged("Type");
			}
		}

		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raises the property changed.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		protected void RaisePropertyChanged(string propertyName) {
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ((propertyChanged != null)) {
				propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
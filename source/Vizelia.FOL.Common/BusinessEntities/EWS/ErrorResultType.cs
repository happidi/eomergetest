using System;
using System.Xml.Serialization;

namespace Vizelia.FOL.BusinessEntities.EWS {
	/// <summary>
	/// An error result.
	/// </summary>
	[Serializable]
	[XmlType(Namespace = "http://www.schneider-electric.com/common/dataexchange/2011/05")]
	public class ErrorResultType : object, System.ComponentModel.INotifyPropertyChanged {

		private string idField;

		private string messageField;

		/// <remarks/>
		[XmlElement(Order = 0)]
		public string Id {
			get {
				return this.idField;
			}
			set {
				this.idField = value;
				this.RaisePropertyChanged("Id");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 1)]
		public string Message {
			get {
				return this.messageField;
			}
			set {
				this.messageField = value;
				this.RaisePropertyChanged("Message");
			}
		}

		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raises the property changed.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		protected void RaisePropertyChanged(string propertyName) {
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ((propertyChanged != null)) {
				propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
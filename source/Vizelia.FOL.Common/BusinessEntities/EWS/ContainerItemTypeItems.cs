using System;
using System.Xml.Serialization;

namespace Vizelia.FOL.BusinessEntities.EWS {
	/// <summary>
	/// A container item.
	/// </summary>
	[Serializable]
	[XmlType(AnonymousType = true, Namespace = "http://www.schneider-electric.com/common/dataexchange/2011/05")]
	public class ContainerItemTypeItems : object, System.ComponentModel.INotifyPropertyChanged {

		private ContainerItemSimpleType[] containerItemsField;

		private ValueItemTypeBase[] valueItemsField;

		private HistoryItemType[] historyItemsField;

		private AlarmItemType[] alarmItemsField;

		/// <remarks/>
		[XmlArray(Order = 0)]
		[XmlArrayItem("ContainerItem", IsNullable = false)]
		public ContainerItemSimpleType[] ContainerItems {
			get {
				return this.containerItemsField;
			}
			set {
				this.containerItemsField = value;
				this.RaisePropertyChanged("ContainerItems");
			}
		}

		/// <remarks/>
		[XmlArray(Order = 1)]
		[XmlArrayItem("ValueItem", IsNullable = false)]
		public ValueItemTypeBase[] ValueItems {
			get {
				return this.valueItemsField;
			}
			set {
				this.valueItemsField = value;
				this.RaisePropertyChanged("ValueItems");
			}
		}

		/// <remarks/>
		[XmlArray(Order = 2)]
		[XmlArrayItem("HistoryItem", IsNullable = false)]
		public HistoryItemType[] HistoryItems {
			get {
				return this.historyItemsField;
			}
			set {
				this.historyItemsField = value;
				this.RaisePropertyChanged("HistoryItems");
			}
		}

		/// <remarks/>
		[XmlArray(Order = 3)]
		[XmlArrayItem("AlarmItem", IsNullable = false)]
		public AlarmItemType[] AlarmItems {
			get {
				return this.alarmItemsField;
			}
			set {
				this.alarmItemsField = value;
				this.RaisePropertyChanged("AlarmItems");
			}
		}

		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raises the property changed.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		protected void RaisePropertyChanged(string propertyName) {
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ((propertyChanged != null)) {
				propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
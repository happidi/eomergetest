using System;
using System.Xml.Serialization;

namespace Vizelia.FOL.BusinessEntities.EWS {
	/// <summary>
	/// A value item.
	/// </summary>
	[Serializable]
	[XmlType(Namespace = "http://www.schneider-electric.com/common/dataexchange/2011/05")]
	public class ValueItemType : object, System.ComponentModel.INotifyPropertyChanged {

		private string idField;

		private string nameField;

		private string descriptionField;

		private string typeField;

		private string valueField;

		private string unitField;

		private string writeableField;

		private string stateField;

		/// <remarks/>
		[XmlElement(Order = 0)]
		public string Id {
			get {
				return this.idField;
			}
			set {
				this.idField = value;
				this.RaisePropertyChanged("Id");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 1)]
		public string Name {
			get {
				return this.nameField;
			}
			set {
				this.nameField = value;
				this.RaisePropertyChanged("Name");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 2)]
		public string Description {
			get {
				return this.descriptionField;
			}
			set {
				this.descriptionField = value;
				this.RaisePropertyChanged("Description");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 3)]
		public string Type {
			get {
				return this.typeField;
			}
			set {
				this.typeField = value;
				this.RaisePropertyChanged("Type");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 4)]
		public string Value {
			get {
				return this.valueField;
			}
			set {
				this.valueField = value;
				this.RaisePropertyChanged("Value");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 5)]
		public string Unit {
			get {
				return this.unitField;
			}
			set {
				this.unitField = value;
				this.RaisePropertyChanged("Unit");
			}
		}

		/// <remarks/>
		[XmlElement(DataType = "integer", Order = 6)]
		public string Writeable {
			get {
				return this.writeableField;
			}
			set {
				this.writeableField = value;
				this.RaisePropertyChanged("Writeable");
			}
		}

		/// <remarks/>
		[XmlElement(DataType = "integer", Order = 7)]
		public string State {
			get {
				return this.stateField;
			}
			set {
				this.stateField = value;
				this.RaisePropertyChanged("State");
			}
		}

		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raises the property changed.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		protected void RaisePropertyChanged(string propertyName) {
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ((propertyChanged != null)) {
				propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
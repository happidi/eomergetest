using System;
using System.Xml.Serialization;

namespace Vizelia.FOL.BusinessEntities.EWS {
	/// <summary>
	/// A history record.
	/// </summary>
	[Serializable]
	[XmlType(Namespace = "http://www.schneider-electric.com/common/dataexchange/2011/05")]
	public class HistoryRecordType : object, System.ComponentModel.INotifyPropertyChanged {

		private string valueField;

		private string stateField;

		private System.DateTime timeStampField;

		/// <remarks/>
		[XmlElement(Order = 0)]
		public string Value {
			get {
				return this.valueField;
			}
			set {
				this.valueField = value;
				this.RaisePropertyChanged("Value");
			}
		}

		/// <remarks/>
		[XmlElement(DataType = "integer", Order = 1)]
		public string State {
			get {
				return this.stateField;
			}
			set {
				this.stateField = value;
				this.RaisePropertyChanged("State");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 2)]
		public System.DateTime TimeStamp {
			get {
				return this.timeStampField;
			}
			set {
				this.timeStampField = value;
				this.RaisePropertyChanged("TimeStamp");
			}
		}

		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raises the property changed.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		protected void RaisePropertyChanged(string propertyName) {
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ((propertyChanged != null)) {
				propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
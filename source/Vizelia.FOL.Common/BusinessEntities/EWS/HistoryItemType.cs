﻿using System;
using System.Xml.Serialization;

namespace Vizelia.FOL.BusinessEntities.EWS {
	/// <summary>
	/// A history item.
	/// </summary>
	[Serializable]
	[XmlTypeAttribute(Namespace = "http://www.schneider-electric.com/common/dataexchange/2011/05")]
	public class HistoryItemType : object, System.ComponentModel.INotifyPropertyChanged {

		private string idField;

		private string nameField;

		private string descriptionField;

		private string typeField;

		private string unitField;

		private string valueItemIdField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Order = 0)]
		public string Id {
			get {
				return this.idField;
			}
			set {
				this.idField = value;
				this.RaisePropertyChanged("Id");
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Order = 1)]
		public string Name {
			get {
				return this.nameField;
			}
			set {
				this.nameField = value;
				this.RaisePropertyChanged("Name");
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Order = 2)]
		public string Description {
			get {
				return this.descriptionField;
			}
			set {
				this.descriptionField = value;
				this.RaisePropertyChanged("Description");
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Order = 3)]
		public string Type {
			get {
				return this.typeField;
			}
			set {
				this.typeField = value;
				this.RaisePropertyChanged("Type");
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Order = 4)]
		public string Unit {
			get {
				return this.unitField;
			}
			set {
				this.unitField = value;
				this.RaisePropertyChanged("Unit");
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Order = 5)]
		public string ValueItemId {
			get {
				return this.valueItemIdField;
			}
			set {
				this.valueItemIdField = value;
				this.RaisePropertyChanged("ValueItemId");
			}
		}

		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raises the property changed.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		protected void RaisePropertyChanged(string propertyName) {
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ((propertyChanged != null)) {
				propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
using System;
using System.Xml.Serialization;

namespace Vizelia.FOL.BusinessEntities.EWS {
	/// <summary>
	/// A container of history records.
	/// </summary>
	[Serializable]
	[XmlType(Namespace = "http://www.schneider-electric.com/common/dataexchange/2011/05")]
	public class HistoryRecordsType : object, System.ComponentModel.INotifyPropertyChanged {

		private string valueItemIdField;

		private string unitField;

		private string typeField;

		private HistoryRecordType[] listField;

		/// <remarks/>
		[XmlElement(Order = 0)]
		public string ValueItemId {
			get {
				return this.valueItemIdField;
			}
			set {
				this.valueItemIdField = value;
				this.RaisePropertyChanged("ValueItemId");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 1)]
		public string Unit {
			get {
				return this.unitField;
			}
			set {
				this.unitField = value;
				this.RaisePropertyChanged("Unit");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 2)]
		public string Type {
			get {
				return this.typeField;
			}
			set {
				this.typeField = value;
				this.RaisePropertyChanged("Type");
			}
		}

		/// <remarks/>
		[XmlArray(Order = 3)]
		[XmlArrayItem("HistoryRecord", IsNullable = false)]
		public HistoryRecordType[] List {
			get {
				return this.listField;
			}
			set {
				this.listField = value;
				this.RaisePropertyChanged("List");
			}
		}

		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raises the property changed.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		protected void RaisePropertyChanged(string propertyName) {
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ((propertyChanged != null)) {
				propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
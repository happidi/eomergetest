using System;
using System.Xml.Serialization;

namespace Vizelia.FOL.BusinessEntities.EWS {
	/// <summary>
	/// An alarm item.
	/// </summary>
	[Serializable]
	[XmlType(Namespace = "http://www.schneider-electric.com/common/dataexchange/2011/05")]
	public class AlarmItemType : object, System.ComponentModel.INotifyPropertyChanged {

		private string idField;

		private string nameField;

		private string descriptionField;

		private string stateField;

		private string valueItemIdField;

		/// <remarks/>
		[XmlElement(Order = 0)]
		public string Id {
			get {
				return this.idField;
			}
			set {
				this.idField = value;
				this.RaisePropertyChanged("Id");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 1)]
		public string Name {
			get {
				return this.nameField;
			}
			set {
				this.nameField = value;
				this.RaisePropertyChanged("Name");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 2)]
		public string Description {
			get {
				return this.descriptionField;
			}
			set {
				this.descriptionField = value;
				this.RaisePropertyChanged("Description");
			}
		}

		/// <remarks/>
		[XmlElement(DataType = "integer", Order = 3)]
		public string State {
			get {
				return this.stateField;
			}
			set {
				this.stateField = value;
				this.RaisePropertyChanged("State");
			}
		}

		/// <remarks/>
		[XmlElement(Order = 4)]
		public string ValueItemId {
			get {
				return this.valueItemIdField;
			}
			set {
				this.valueItemIdField = value;
				this.RaisePropertyChanged("ValueItemId");
			}
		}

		/// <summary>
		/// Occurs when a property value changes.
		/// </summary>
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raises the property changed.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		protected void RaisePropertyChanged(string propertyName) {
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ((propertyChanged != null)) {
				propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
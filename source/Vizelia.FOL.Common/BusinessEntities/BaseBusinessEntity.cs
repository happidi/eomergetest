﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Microsoft.Practices.EnterpriseLibrary.Validation;


namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Base class for all business entities.
	/// The framework is based on POCO objects.
	/// Nullable columns within a database table should be represented within the model class as Nullable properties if they are value types.
	/// We indicates the KnownType so that we can define a DataMember of type BaseBusinessEntity. 
	/// This is the case for TreeNode and for FormResponse that exposes a BaseBusinessEntity, so for serialization to occur all possible knowntypes should be exposed manually here.
	/// Known types are automatically detected using the KnownTypesFinder
	/// </summary>
	[Serializable]
	[DataContract]
	[KnownType("GetKnownTypes")]
	public abstract class BaseBusinessEntity : BaseEntity {


		/// <summary>
		/// Gets the known types.
		/// </summary>
		/// <returns></returns>
		static IEnumerable<Type> GetKnownTypes() {
			return KnownTypeFinder.GetKnownTypes(null);
		}

		/// <summary>
		/// Gets a value indicating whether this instance is valid.
		/// </summary>
		/// <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
		[IgnoreDataMember]
		public virtual bool IsValid {
			get {
				return this.Validate().IsValid;
			}
		}

		/// <summary>
		/// Validates this instance.
		/// </summary>
		/// <returns></returns>
		public ValidationResults Validate() {
			Validator validator = ValidationFactory.CreateValidator(this.GetType());
			ValidationResults results = new ValidationResults();
			validator.Validate(this, results);
			return results;
		}

		/// <summary>
		/// The iconCls of the entity. The value is retreived from the attribute IconClsAttribute that decorates the entity.
		/// This property is not exposed to client.
		/// </summary>
		protected virtual string iconCls {
			get {
				return Helper.GetAttributeValue<IconClsAttribute>(this.GetType());
			}
		}

		/// <summary>
		/// Helper function for finding a specific pset attribute on the entity.
		/// Used for example in mail template through razor @occupant.FindPsetValue("OccupantPset", "Manager").
		/// </summary>
		/// <param name="psetName"></param>
		/// <param name="psetAttributeName"></param>
		/// <returns></returns>
		public string FindPsetValue(string psetName, string psetAttributeName) {
			IPset pset = this as IPset;
			if (pset == null) {
				return null;
			}

			var queryPset = (from p in pset.PropertySetList
							 where p.PsetName == psetName
							 select p).FirstOrDefault();
			if (queryPset == null)
				return null;

			var queryAttribute = (from p in queryPset.Attributes
								  where p.Key == psetAttributeName
								  select p.Value).FirstOrDefault();

			return queryAttribute;
		}
	}

}


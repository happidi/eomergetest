﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// The base class for ModelData entities.
	/// </summary>
	[DataContract]
	[IconCls("viz-icon-small-module")]
	public abstract class ModelData : BaseBusinessEntity {

		/// <summary>
		/// Gets or sets the key model data.
		/// </summary>
		/// <value>
		/// The key model data.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyModelData { get; set; }

	}
}

﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for the model data of a LongRunningOperationResult.
	/// </summary>
	[DataContract]
	public class ModelDataLongRunningOperationResult : ModelDataMail {
		/// <summary>
		/// Gets or sets the user email address.
		/// </summary>
		/// <value>
		/// The user email address.
		/// </value>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string UserEmailAddress { get; set; }
	}
}

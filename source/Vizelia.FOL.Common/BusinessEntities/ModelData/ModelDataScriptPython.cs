﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using System.Collections.Generic;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for the model data of a script Python.
	/// </summary>
	public class ModelDataScriptPython : ModelData {
		/// <summary>
		/// Gets or sets the chart.
		/// </summary>
		/// <value>
		/// The chart.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public Chart chart { get; set; }

		/// <summary>
		/// Gets or sets the series.
		/// </summary>
		/// <value>
		/// The series.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public List<DataSerie> series { get; set; }

		
		/// <summary>
		/// Gets or sets the series.
		/// </summary>
		/// <value>
		/// The series.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public ModelDataScriptPythonHelper helper { get; set; }

	}

	/// <summary>
	/// Business entity for the model data of a script Python helper.
	/// </summary>
	public class ModelDataScriptPythonHelper : ModelData {
		/// <summary>
		/// Gets or sets the sum.
		/// </summary>
		/// <value>
		/// The sum.
		/// </value>
		[ViewableByDataModel] 
		public double Sum(DataSerie serie){
			return DataSerieHelper.Sum(serie);
		}

		/// <summary>
		/// Gets or sets the avg.
		/// </summary>
		/// <value>
		/// The avg.
		/// </value>
		[ViewableByDataModel]
		public double Avg(DataSerie serie) {
			return DataSerieHelper.Avg(serie);
		}

		/// <summary>
		/// Gets or sets the min.
		/// </summary>
		/// <value>
		/// The min.
		/// </value>
		[ViewableByDataModel]
		public double Min(DataSerie serie) {
			return DataSerieHelper.Min(serie);
		}

		/// <summary>
		/// Gets or sets the max.
		/// </summary>
		/// <value>
		/// The max.
		/// </value>
		[ViewableByDataModel]
		public double Max(DataSerie serie) {
			return DataSerieHelper.Max(serie);
		}
	}
}

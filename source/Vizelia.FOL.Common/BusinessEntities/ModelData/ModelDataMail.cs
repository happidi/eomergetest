﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;
using Version = Vizelia.FOL.Common.Version;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// The base class for ModelData Mail entities.
	/// </summary>
	[DataContract]
	public abstract class ModelDataMail : ModelData {
		/// <summary>
		/// Gets or sets the name of the machine.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string MachineName {
			get { return Environment.MachineName; }
			set { }
		}

		/// <summary>
		/// Gets or sets the name of the application.
		/// </summary>
		/// <value>
		/// The name of the application.
		/// </value>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string ApplicationName {
			get { return ContextHelper.ApplicationName; }
			set { }
		}

		/// <summary>
		/// Gets the user id.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string UserId {
			get { return Helper.GetCurrentUserName() ?? ""; }
			set { }
		}

		/// <summary>
		/// Gets the URL.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Url {
			get {
				var url = Helper.GetFrontendUrl("~/", VizeliaConfiguration.Instance.Deployment.FrontendUrl);
				return url;
			}
			set { }
		}

		/// <summary>
		/// Gets or sets the application title.
		/// </summary>
		/// <value>
		/// The application title.
		/// </value>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string ApplicationTitle {
			get { return Langue.msg_application_title; }
			set { }
		}

		/// <summary>
		/// Gets or sets the application version number.
		/// </summary>
		/// <value>
		/// The application version number.
		/// </value>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string ApplicationVersionNumber {
			get { return (Version.Number); }
			set { }
		}
	}
}

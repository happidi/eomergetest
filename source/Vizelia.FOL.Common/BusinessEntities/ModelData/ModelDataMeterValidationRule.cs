﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for the model data of an Alarm.
	/// </summary>
	[DataContract]
	public class ModelDataMeterValidationRule : ModelDataMail {
		/// <summary>
		/// Gets or sets the meter validation rule.
		/// </summary>
		/// <value>
		/// The meter validation rule.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public MeterValidationRule MeterValidationRule { get; set; }

		/// <summary>
		/// Gets or sets the instance count.
		/// </summary>
		/// <value>
		/// The instance count.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public int InstanceCount { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }
	}
}

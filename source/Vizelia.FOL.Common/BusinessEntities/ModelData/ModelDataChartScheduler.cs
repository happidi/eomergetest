﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for the model data of a ChartScheduler.
	/// </summary>
	[DataContract]
	public class ModelDataChartScheduler : ModelDataMail {
		/// <summary>
		/// Gets or sets the chart scheduler.
		/// </summary>
		/// <value>
		/// The chart scheduler.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public ChartScheduler ChartScheduler { get; set; }
	}
}

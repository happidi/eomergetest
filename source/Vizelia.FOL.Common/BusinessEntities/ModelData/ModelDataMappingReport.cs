﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for the model data of an MappingReport
	/// </summary>
	[DataContract]
	public class ModelDataMappingReport : ModelDataMail {
		/// <summary>
		/// Gets or sets the mapping summary.
		/// </summary>
		/// <value>
		/// The mapping summary.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public IMappingSummary MappingSummary { get; set; }

		/// <summary>
		/// Gets or sets the date time.
		/// </summary>
		/// <value>
		/// The date time.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public DateTime DateTime { get; set; }
	}
}

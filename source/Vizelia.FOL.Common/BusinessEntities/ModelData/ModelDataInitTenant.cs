﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for the model data of Initialize Tenant.
	/// </summary>
	[DataContract]
	public class ModelDataInitTenant : ModelDataMail {
		/// <summary>
		/// Gets or sets the tenant.
		/// </summary>
		/// <value>
		/// The tenant.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public Tenant Tenant { get; set; }

		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>
		/// The password.
		/// </value>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Password { get; set; }
	}
}

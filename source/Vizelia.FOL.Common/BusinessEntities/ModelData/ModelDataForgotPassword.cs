﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for the model data of a ResetPassword.
	/// </summary>
	[DataContract]
	public class ModelDataForgotPassword : ModelDataMail {
		/// <summary>
		/// Gets or sets the occupant.
		/// </summary>
		/// <value>
		/// The occupant.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public Occupant Occupant { get; set; }

		/// <summary>
		/// Gets or sets the name of the user.
		/// </summary>
		/// <value>
		/// The name of the user.
		/// </value>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string UserName { get; set; }

		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>
		/// The password.
		/// </value>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string NewPassword { get; set; }

		/// <summary>
		/// Gets or sets the email.
		/// </summary>
		/// <value>
		/// The email.
		/// </value>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Email { get; set; }
	}
}

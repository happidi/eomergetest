﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for the model data of Service Desk.
	/// </summary>
	[DataContract]
	public class ModelDataServiceDesk : ModelDataMail {

		/// <summary>
		/// Gets or sets the action request.
		/// </summary>
		/// <value>The action request.</value>
		[ViewableByDataModel]
		[DataMember]
		public ActionRequest ActionRequest { get; set; }


		/// <summary>
		/// Gets or sets the requestor.
		/// </summary>
		/// <value>The requestor.</value>
		[ViewableByDataModel]
		[DataMember]
		public Occupant Requestor { get; set; }

		/// <summary>
		/// Gets or sets the owner.
		/// </summary>
		/// <value>The owner.</value>
		[ViewableByDataModel]
		[DataMember]
		public Occupant Owner { get; set; }
	}
}
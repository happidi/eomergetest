﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Portal Window.
	/// </summary>
	[Key("KeyPortalWindow")]
	[DataContract]
	[IconCls("viz-icon-small-portalwindow")]
	[LocalizedText("msg_portalwindow")]
	[AuditableEntityAttribute]
	public class PortalWindow : BaseBusinessEntity, ISecurableObject, IMappableEntity, IAuditEntity {

		/// <summary>
		/// Initializes a new instance of the <see cref="PortalWindow"/> class.
		/// </summary>
		public PortalWindow() {
			this.Tabs = new List<PortalTab>();
		}

		/// <summary>
		/// Key of the Portal.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(PortalWindow), false)]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyPortalWindow { get; set; }

		/// <summary>
		/// The local id of the Portal Window.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the title of the portal.
		/// </summary>
		[Information("msg_title", null)]
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the icon css of the portal.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconCls { get; set; }


		/// <summary>
		/// Gets the icon small css of the portal.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconClsSmall {
			get {
				if (string.IsNullOrEmpty(this.IconCls))
					return this.IconCls;

				return this.IconCls.Replace("-large-", "-small-");
			}
			private set {

			}
		}

		/// <summary>
		/// True to display the portal window in the Energy toolbar with the IconCss, false to hide it.
		/// </summary>
		[Information("msg_portalwindow_displayintoolbar", null)]
		[DataMember]
		public bool DisplayInToolbar { get; set; }

		/// <summary>
		/// The X position of the portal window shortcut icon when displayed in desktop mode.
		/// </summary>
		[DataMember]
		public int? DesktopShortcutIconX { get; set; }

		/// <summary>
		/// The Y position of the portal window shortcut icon when displayed in desktop mode.
		/// </summary>
		[DataMember]
		public int? DesktopShortcutIconY { get; set; }

		/// <summary>
		/// Gets os sets the list of tabs of the portal.
		/// </summary>
		[DataMember]
		public List<PortalTab> Tabs { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity() {
				KeySecurable = this.KeyPortalWindow,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(this.GetType()),
				SecurableName = this.Title,
				SecurableTypeName = this.GetType().FullName

			};
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// True to open the portal window on start up.
		/// </summary>
		[Information("msg_portalwindow_openonstartup", null)]
		[DataMember]
		public bool OpenOnStartup { get; set; }

		/// <summary>
		/// The default portaltab columns config of this portal window.
		/// </summary>
		[Information("msg_portalwindow_defaulttabcolumnconfig", null)]
		[DataMember]
		public PortalTabColumnConfig PortalTabColumnConfig { get; set; }

		/// <summary>
		///The Key of the PortalTemplate that has been used to create this portal. 
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPortalTemplateOrigin { get; set; }

		/// <summary>
		/// Gets  the portal window tab number.
		/// </summary>
		/// <value>
		/// The portal window tab number.
		/// </value>
		[Information("msg_portalwindow_tabnumber", null)]
		[DataMember]
		public int PortalWindowTabNumber { get; set; }


		/// <summary>
		/// True to preload all charts in a tab before displaying it.
		/// </summary>
		[Information("msg_portalwindow_preloadtab", null)]
		[DataMember]
		public bool PreloadTab { get; set; }


		/// <summary>
		/// Gets or sets the creator login.
		/// </summary>
		/// <value>
		/// The creator login.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string CreatorLogin { get; set; }

		/// <summary>
		/// Gets or sets the key image (custom icon instead of css).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyImage { get; set; }


		/// <summary>
		/// Gets or sets the width of the print.
		/// </summary>
		[DataMember]
		public int? PrintWidth { get; set; }

		/// <summary>
		/// Gets or sets the height of the print.
		/// </summary>
		[DataMember]
		public int? PrintHeight { get; set; }


        /// <summary>
        /// Gets or sets the last date modified.
        /// </summary>
        public DateTime LastDateModifie { get; set; }

        /// <summary>
        /// The Key of the user that last modified the entity.
        /// </summary>
	    public string AuditModifiedBy { get; set; }
        /// <summary>
        /// The login of the user that last modified the entity.
        /// </summary>
	    public string AuditModifiedByLogin { get; set; }
        /// <summary>
        /// The first name of the user that last modified the entity.
        /// </summary>
	    public string AuditModifiedByFirstName { get; set; }
        /// <summary>
        /// The last name of the user that last modified the entity.
        /// </summary>
	    public string AuditModifiedByLastName { get; set; }
        /// <summary>
        /// The date and time of the last modification.
        /// </summary>
	    public DateTime? AuditTimeLastModified { get; set; }

        /// <summary>
        /// Key of the PortalWindow origin from which this portal was created in the push portals process.
        /// </summary>
        [AntiXssValidator]
        [DataMember]      
        public string KeyPortalWindowOrigin { get; set; }
	}
}

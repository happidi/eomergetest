﻿using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;


namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Map.
	/// </summary>
	[Key("KeyMap")]
	[IconCls("viz-icon-small-map")]
	[DataContract]
	[LocalizedText("msg_map")]
	public class Map : BaseBusinessEntity, ITreeNode, IMappableEntity, ISecurableObject, IPortlet, ISupportLocation {
		/// <summary>
		/// Key of the Map.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(Map), false)]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyMap { get; set; }

		/// <summary>
		/// Gets or sets the title of the Map.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }
		/// <summary>
		/// Gets or sets the zoom level of the Map.
		/// </summary>
		[DataMember]
		public int Zoom { get; set; }

		/// <summary>
		/// Gets or sets the location latitude.
		/// </summary>
		[DataMember]
		public double LocationLatitude { get; set; }

		/// <summary>
		/// Gets or sets the location longitude.
		/// </summary>
		[DataMember]
		public double LocationLongitude { get; set; }

		/// <summary>
		/// Gets or sets the Map type.
		/// </summary>
		[DataMember]
		public MapType MapType { get; set; }


		/// <summary>
		/// The PsetName  that  contains the address to display as a push pin.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PsetName { get; set; }

		/// <summary>
		/// The AttributeName  that  contains the address to display as a push pin.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeName { get; set; }

		/// <summary>
		/// The  PropertySingle long path (used in the treecombo).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PropertySingleValueLongPath { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to show the toolbar.
		/// </summary>
		[DataMember]
		public bool ShowDashboard { get; set; }

		/// <summary>
		/// The key of the classification of the AlarmDefinition .
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItemAlarmDefinition { get; set; }

		/// <summary>
		/// The key path of the classification of the AlarmDefinition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItemAlarmDefinitionPath { get; set; }

		/// <summary>
		/// The classification title of the AlarmDefinition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemAlarmDefinitionTitle { get; set; }

		/// <summary>
		/// The classification level of the AlarmDefinition.
		/// </summary>
		[DataMember]
		public int ClassificationItemAlarmDefinitionLevel { get; set; }

		/// <summary>
		/// The classification path of the AlarmDefinition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemAlarmDefinitionLongPath { get; set; }

		/// <summary>
		/// The classification local id path of the AlarmDefinition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemAlarmDefinitionLocalIdPath { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int? ClassificationItemAlarmDefinitionColorR { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int? ClassificationItemAlarmDefinitionColorG { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int? ClassificationItemAlarmDefinitionColorB { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Convert this Map into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				Key = this.KeyMap,
				text = this.Title,
				leaf = true,
				entity = this,
				qtip = this.Title,
				iconCls = this.iconCls
			};
		}

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity() {
				KeySecurable = this.KeyMap,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(this.GetType()),
				SecurableName = this.Title,
				SecurableTypeName = this.GetType().FullName
			};
		}

		/// <summary>
		/// Sets the title.
		/// </summary>
		/// <param name="title">The title.</param>
		public void SetTitle(string title) {
			this.Title = title;
		}

		/// <summary>
		/// Gets the title.
		/// </summary>
		/// <returns></returns>
		public string GetTitle() {
			return Title;
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool OpenOnStartup { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsFavorite { get; set; }

		/// <summary>
		/// The Key of the location of the map in order to filter the pushpins.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocation { get; set; }

		/// <summary>
		/// The location name of the Meter.
		/// </summary>
		public string LocationName { get; set; }

		/// <summary>
		/// The location typename of the Map.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public HierarchySpatialTypeName LocationTypeName { get; set; }

		/// <summary>
		/// The Location Long Path of the map in order to filter the pushpins.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationLongPath { get; set; }

		/// <summary>
		/// The location short path of the Meter.
		/// Contains only the deepest site level and further levels.
		/// </summary>
		public string LocationShortPath { get; set; }

		/// <summary>
		/// The location level. Starts at level 0.
		/// </summary>
		public int? LocationLevel { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [use spatial icons].
		/// </summary>
		/// <value>
		///   <c>true</c> if [use spatial icons]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool UseSpatialIcons { get; set; }

		/// <summary>
		/// Gets or sets the pset MSG code.
		/// </summary>
		/// <value>
		/// The pset MSG code.
		/// </value>
		public string PsetMsgCode { get; set; }

		/// <summary>
		/// Gets or sets the attribute MSG code.
		/// </summary>
		/// <value>
		/// The attribute MSG code.
		/// </value>
		public string AttributeMsgCode { get; set; }
	}
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Represent a Location on a Map
	/// </summary>
	[DataContract]
	[Serializable]
	public class MapLocation {

		/// <summary>
		/// Gets or sets the Address.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Address { get; set; }
		/// <summary>
		/// Gets or sets the latitude.
		/// </summary>
		[DataMember]
		public double Latitude { get; set; }

		/// <summary>
		/// Gets or sets the longitude.
		/// </summary>
		[DataMember]
		public double Longitude { get; set; }

        /// <summary>
        /// Indicate if the location is Valid (found in the API).
        /// </summary>
        public bool IsValid { get; set; }
	}
}

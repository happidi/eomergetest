﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Represent a Pushpin on the map.
	/// </summary>
	[DataContract]
	public class MapPushpin {

		/// <summary>
		/// Gets or sets the tooltip.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Tooltip { get; set; }

		/// <summary>
		/// Gets or sets the icon CLS.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconCls { get; set; }

		/// <summary>
		/// Gets or sets the location.
		/// </summary>
		[DataMember]
		public MapLocation Location { get; set; }

		/// <summary>
		/// The entity associated to the pushpin.
		/// </summary>
		[DataMember]
		public BaseBusinessEntity Entity { get; set; }

		/// <summary>
		/// The number of created alarms attached to this pushpin.
		/// </summary>
		[DataMember]
		public int AlarmNumber { get; set; }

	}
}

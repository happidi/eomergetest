﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessEntities
{
    /// <summary>
    /// Collection for map pushpins
    /// </summary>
    [DataContract]
    public class MapPushpinsCollection : BaseBusinessEntity 
    {
        /// <summary>
        /// List of MapPushpins
        /// </summary>
        [DataMember]
        public List<MapPushpin> MapPushpinsList { get; set; } 
    }
}

﻿namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Interface for all "Porlet" entities that will be displayed in the Portal.
	/// </summary>
	public interface IPortlet {
		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		bool OpenOnStartup { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		bool IsFavorite { get; set; }

		/// <summary>
		/// Sets the title.
		/// </summary>
		/// <param name="title">The title.</param>
		void SetTitle(string title);

		/// <summary>
		/// Gets the title.
		/// </summary>
		string GetTitle();
	}
}

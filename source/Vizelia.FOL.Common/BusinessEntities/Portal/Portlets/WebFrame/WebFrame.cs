﻿using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a WebFrame.
	/// </summary>
	[Key("KeyWebFrame")]
	[IconCls("viz-icon-small-webframe")]
	[DataContract]
	[LocalizedText("msg_webframe")]
	public class WebFrame : BaseBusinessEntity, ITreeNode, IMappableEntity, ISecurableObject, IPortlet {

		/// <summary>
		/// Initializes a new instance of the <see cref="WebFrame"/> class.
		/// </summary>
		public WebFrame() {
			this.Body = "";
		}

		/// <summary>
		/// Key of the WebFrame
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(WebFrame), false)]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyWebFrame { get; set; }

		/// <summary>
		/// Gets or sets the title of the WebFrame.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the Url of the WebFrame.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Url { get; set; }

		/// <summary>
		/// Gets or sets the HTML Body of the WebFrame.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Body { get; set; }

		/// <summary>
		/// Convert this WebFrame into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				Key = this.KeyWebFrame,
				text = this.Title,
				leaf = true,
				entity = this,
				qtip = this.Url,
				iconCls = this.iconCls
			};
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }


		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity() {
				KeySecurable = this.KeyWebFrame,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(this.GetType()),
				SecurableName = this.Title,
				SecurableTypeName = this.GetType().FullName
			};
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }



		/// <summary>
		/// Sets the title.
		/// </summary>
		/// <param name="title">The title.</param>
		public void SetTitle(string title) {
			this.Title = title;
		}

		/// <summary>
		/// Gets the title.
		/// </summary>
		/// <returns></returns>
		public string GetTitle() {
			return Title;
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool OpenOnStartup { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsFavorite { get; set; }
		
	}
}

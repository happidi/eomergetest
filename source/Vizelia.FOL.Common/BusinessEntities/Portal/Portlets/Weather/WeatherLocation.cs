﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business Entity to represent a  WeatherLocation Entity.
	/// </summary>
	[DataContract]
	[Serializable]
	[Key("KeyWeatherLocation")]
	[IconCls("viz-icon-small-weatherlocation")]
	[LocalizedText("msg_weatherlocation")]
	public class WeatherLocation : BaseBusinessEntity, ITreeNode, IMappableEntity, ISecurableObject, IPortlet {

		/// <summary>
		/// Initializes a new instance of the <see cref="WeatherLocation"/> class.
		/// </summary>
		public WeatherLocation() {
			this.Name = "Paris, France";
			this.LocationId = "FRXX0076";
			this.Unit = WeatherUnit.Celsius;
			this.DisplayType = WeatherDisplayType.Horizontal;
		}

		/// <summary>
		/// Key of the WeatherLocation.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(WeatherLocation), false)]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyWeatherLocation { get; set; }

		/// <summary>
		/// The Weather Title.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// The Weather Location id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationId { get; set; }

		/// <summary>
		/// The Weather Location search string.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// The Weather Unit.
		/// </summary>
		[DataMember]
		public WeatherUnit Unit { get; set; }

		/// <summary>
		/// Convert this Weather Location into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				Key = this.KeyWeatherLocation,
				text = this.Title,
				leaf = true,
				entity = this,
				qtip = this.LocationId,
				iconCls = this.iconCls
			};
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity() {
				KeySecurable = this.KeyWeatherLocation,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(this.GetType()),
				SecurableName = this.Name,
				SecurableTypeName = this.GetType().FullName
			};
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Gets or sets the DisplayFontFamily.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string DisplayFontFamily { get; set; }

		/// <summary>
		/// Gets or sets the TextColor.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string TextColor { get; set; }

		/// <summary>
		/// Gets or sets the MainBackgroundColor.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MainBackgroundColor { get; set; }

		/// <summary>
		/// Gets or sets the AlternativeBackgroundColor.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AlternativeBackgroundColor { get; set; }

		/// <summary>
		/// Gets or sets the display type.
		/// </summary>
		/// <value>
		/// The display type.
		/// </value>
		[DataMember]
		public WeatherDisplayType DisplayType { get; set; }


		/// <summary>
		/// Sets the title.
		/// </summary>
		/// <param name="title">The title.</param>
		public void SetTitle(string title) {
			this.Title = title;
		}

		/// <summary>
		/// Gets the title.
		/// </summary>
		/// <returns></returns>
		public string GetTitle() {
			return Title;
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool OpenOnStartup { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsFavorite { get; set; }
	}
}

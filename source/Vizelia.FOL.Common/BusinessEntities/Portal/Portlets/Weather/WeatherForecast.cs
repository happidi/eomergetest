﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business Entity to represent a  WeatherForecast Entity.
	/// </summary>
	[DataContract]
	[Serializable]
	public class WeatherForecast : BaseBusinessEntity {

		/// <summary>
		/// The Weather Forecast location title.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationTitle { get; set; }

		/// <summary>
		/// The Weather Forecast date.
		/// </summary>
		[DataMember]
		public DateTime Date { get; set; }

		/// <summary>
		/// The Weather Forecast title.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// The WeatherForecast Day Icon Number.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string DayIconNumber { get; set; }

		/// <summary>
		/// The WeatherForecast Night Icon Number.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string NightIconNumber { get; set; }

		/// <summary>
		/// Gets or sets the current temperature.
		/// </summary>
		/// <value>
		/// The current temperature.
		/// </value>
		[DataMember]
		public double CurrentTemperature { get; set; }

		/// <summary>
		/// Gets or sets the high temperature.
		/// </summary>
		/// <value>
		/// The high temperature.
		/// </value>
		[DataMember]
		public double HighTemperature { get; set; }

		/// <summary>
		/// Gets or sets the low temperature.
		/// </summary>
		/// <value>
		/// The low temperature.
		/// </value>
		[DataMember]
		public double LowTemperature { get; set; }

		/// <summary>
		/// Gets or sets the unit.
		/// </summary>
		/// <value>
		/// The unit.
		/// </value>
		[DataMember]
		public WeatherUnit Unit { get; set; }

		/// <summary>
		/// Gets or sets the humidity.
		/// </summary>
		/// <value>
		/// The humidity.
		/// </value>
		[DataMember]
		public int Humidity { get; set; }

		/// <summary>
		/// Gets or sets the current time.
		/// </summary>
		/// <value>
		/// The current time.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string CurrentTime { get; set; }

		/// <summary>
		/// Gets or sets the sunrise time.
		/// </summary>
		/// <value>
		/// The sunrise time.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string SunriseTime { get; set; }

		/// <summary>
		/// Gets or sets the sunset time.
		/// </summary>
		/// <value>
		/// The sunset time.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string SunsetTime { get; set; }
	}
}

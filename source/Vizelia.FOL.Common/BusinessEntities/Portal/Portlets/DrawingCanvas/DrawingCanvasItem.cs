﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Base class to represent an item drawned on the Canvas.
	/// </summary>
	[DataContract]
	public abstract class DrawingCanvasItem : BaseBusinessEntity {
		/// <summary>
		/// Gets the application.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Application { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the width.
		/// </summary>
		[DataMember]
		public int Width { get; set; }

		/// <summary>
		/// Gets or sets the height.
		/// </summary>
		[DataMember]
		public int Height { get; set; }

		/// <summary>
		/// Gets or sets the X position.
		/// </summary>
		[DataMember]
		public int X { get; set; }

		/// <summary>
		/// Gets or sets the Y position.
		/// </summary>
		[DataMember]
		public int Y { get; set; }

		/// <summary>
		/// Gets or sets the ZIndex position.
		/// </summary>
		[DataMember]
		public int ZIndex { get; set; }
	}
}

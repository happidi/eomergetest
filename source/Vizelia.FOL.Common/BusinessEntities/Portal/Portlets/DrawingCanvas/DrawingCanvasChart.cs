﻿using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a DrawingCanvas Chart.
	/// </summary>
	[Key("KeyDrawingCanvasChart")]
	[IconCls("viz-icon-small-chart")]
	[DataContract]
	[LocalizedText("msg_drawingcanvaschart")]
	public class DrawingCanvasChart : DrawingCanvasItem, IMappableEntity {

		/// <summary>
		/// Key of the DrawingCanvas Chart
		/// </summary>
		[AntiXssValidator]
		[DataMember]
        [System.ComponentModel.DataAnnotations.Key]
		public string KeyDrawingCanvasChart { get; set; }

		/// <summary>
		/// Key of the DrawingCanvas
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyDrawingCanvas { get; set; }

		/// <summary>
		/// Key of the  Chart
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }

		/// <summary>
		/// The Chart's title
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ChartTitle { get; set; }

		/// <summary>
		/// Disable effects of the Drawing Canvas sliders on this instance.
		/// </summary>
		[DataMember]
		public bool DisableAllSliders { get; set; }

		/// <summary>
		/// The Chart entity.
		/// </summary>
		[DataMember]
		public Chart Chart { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

        /// <summary>
        /// Gets or sets the key dynamic display.
        /// </summary>
        [AntiXssValidator]
        [DataMember]
        public string KeyDynamicDisplay { get; set; }
	}
}
﻿using System.Linq;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using System.Collections.Generic;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a DrawingCanvas.
	/// </summary>
	[Key("KeyDrawingCanvas")]
	[IconCls("viz-icon-small-drawingcanvas")]
	[DataContract]
	[LocalizedText("msg_drawingcanvas")]
	public class DrawingCanvas : BaseBusinessEntity, ITreeNode, IMappableEntity, ISecurableObject, IPortlet {
		/// <summary>
		/// Initializes a new instance of the <see cref="DrawingCanvas"/> class.
		/// </summary>
		public DrawingCanvas() {
			Items = new List<DrawingCanvasItem>();
		}

		/// <summary>
		/// Gets the application.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Application { get; set; }

		/// <summary>
		/// Key of the DrawingCanvas
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(DrawingCanvas), false)]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyDrawingCanvas { get; set; }

		/// <summary>
		/// Gets or sets the title of the Drawing Canvas.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Gets or sets the items (image or chart) associated with this DrawingCanvas.
		/// </summary>
		/// <value>
		/// The items.
		/// </value>
		[DataMember]
		public List<DrawingCanvasItem> Items { get; set; }

		/// <summary>
		/// Gets the drawing canvas charts.
		/// </summary>
		/// <value>
		/// The drawing canvas charts.
		/// </value>
		[DataMember]
		public List<DrawingCanvasChart> DrawingCanvasCharts {
			get { return Items.OfType<DrawingCanvasChart>().ToList(); }
			private set { } 
		}

		/// <summary>
		/// Gets the drawing canvas images.
		/// </summary>
		/// <value>
		/// The drawing canvas images.
		/// </value>
		[DataMember]
		public List<DrawingCanvasImage> DrawingCanvasImages {
			get { return Items.OfType<DrawingCanvasImage>().ToList(); }
			private set { }
		}

		/// <summary>
		/// Convert this DrawingCanvas into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				Key = this.KeyDrawingCanvas,
				text = this.Title,
				leaf = true,
				entity = this,
				qtip = this.Title,
				iconCls = this.iconCls
			};
		}

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity() {
				KeySecurable = this.KeyDrawingCanvas,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(this.GetType()),
				SecurableName = this.Title,
				SecurableTypeName = this.GetType().FullName
			};
		}

		/// <summary>
		/// Sets the title.
		/// </summary>
		/// <param name="title">The title.</param>
		public void SetTitle(string title) {
			this.Title = title;
		}

		/// <summary>
		/// Gets the title.
		/// </summary>
		/// <returns></returns>
		public string GetTitle() {
			return Title;
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool OpenOnStartup { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsFavorite { get; set; }

		/// <summary>
		/// True to display sliders.
		/// </summary>
		[DataMember]
		public bool DisplaySliders { get; set; }
	}
}


﻿using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a DrawingCanvas Chart.
	/// </summary>
	[Key("KeyDrawingCanvasImage")]
	[IconCls("viz-icon-small-image")]
	[DataContract]
	[LocalizedText("msg_drawingcanvasimage")]
	public class DrawingCanvasImage : DrawingCanvasItem, IMappableEntity {

		/// <summary>
		/// Key of the DrawingCanvas Image
		/// </summary>
		[AntiXssValidator]
		[DataMember]
        [System.ComponentModel.DataAnnotations.Key]
		public string KeyDrawingCanvasImage { get; set; }

		/// <summary>
		/// Key of the DrawingCanvas
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyDrawingCanvas { get; set; }

		/// <summary>
		/// Key of the  Image
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyImage { get; set; }

		/// <summary>
		/// The Image's title
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ImageTitle { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }
	}
}

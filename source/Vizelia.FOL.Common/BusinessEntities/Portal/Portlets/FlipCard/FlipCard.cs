﻿using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;
using System.Collections.Generic;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a FlipCard.
	/// </summary>
	[Key("KeyFlipCard")]
	[IconCls("viz-icon-small-flipcard")]
	[DataContract]
	[LocalizedText("msg_flipcard")]
	public class FlipCard : BaseBusinessEntity, ITreeNode, ISecurableObject, IPortlet, IMappableEntity {
		/// <summary>
		/// Initializes a new instance of the <see cref="FlipCard"/> class.
		/// </summary>
		public FlipCard() {
			this.Cards = new List<FlipCardEntry>();
		}
		/// <summary>
		/// Key of the FlipCard
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(FlipCard), false)]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyFlipCard { get; set; }

		/// <summary>
		/// Gets or sets the title of the WebFrame.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="FlipCard"/> is random.
		/// </summary>
		[DataMember]
		public bool Random { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="FlipCard"/> is flipped on refresh.
		/// </summary>
		[DataMember]
		public bool FlipOnRefresh { get; set; }


		/// <summary>
		/// Gets or sets the active card.
		/// </summary>
		[DataMember]
		public int ActiveCard { get; set; }

		/// <summary>
		/// Gets or sets the cards.
		/// </summary>
		[DataMember]
		public List<FlipCardEntry> Cards { get; set; }

		/// <summary>
		/// Convert this WebFrame into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				Key = this.KeyFlipCard,
				text = this.Title,
				leaf = true,
				entity = this,
				qtip = this.Title,
				iconCls = this.iconCls
			};
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }


		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity() {
				KeySecurable = this.KeyFlipCard,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(this.GetType()),
				SecurableName = this.Title,
				SecurableTypeName = this.GetType().FullName
			};
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }



		/// <summary>
		/// Sets the title.
		/// </summary>
		/// <param name="title">The title.</param>
		public void SetTitle(string title) {
			this.Title = title;
		}

		/// <summary>
		/// Gets the title.
		/// </summary>
		/// <returns></returns>
		public string GetTitle() {
			return Title;
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool OpenOnStartup { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsFavorite { get; set; }

	}
}

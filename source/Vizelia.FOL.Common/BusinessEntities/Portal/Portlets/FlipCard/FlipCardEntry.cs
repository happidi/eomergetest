﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Represent an entry of the FlipCard.
	/// </summary>
	[Key("KeyFlipCardEntry")]
	[DataContract]
	[LocalizedText("msg_flipcardentry")]
	public class FlipCardEntry : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Gets or sets the tooltip.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyFlipCardEntry { get; set; }

		/// <summary>
		/// Key of the Playlist.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyFlipCard { get; set; }


		/// <summary>
		/// Gets or sets the order.
		/// </summary>
		[DataMember]
		public int Order { get; set; }

		/// <summary>
		/// Gets or sets the key of the Portlet entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyEntity { get; set; }

		/// <summary>
		/// Gets or sets the type of the Portlet entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string TypeEntity { get; set; }

		/// <summary>
		/// The localid of the FlipCard the card is attached to.
		/// </summary>
		[Information("msg_flipcardentry_flipcardlocalid", null)]
		[DataMember]
		public string FlipCardLocalId { get; set; }


		/// <summary>
		/// Gets the icon of the entity.
		/// </summary>
		[DataMember]
		public string IconClsEntity {
			get {
				return Helper.GetAttributeValue<IconClsAttribute>(Helper.GetPortletEntityType(this.TypeEntity));
			}
			private set {
				;
			}
		}

		/// <summary>
		/// Gets or sets the title entity.
		/// </summary>
		/// <value>
		/// The title entity.
		/// </value>
		[DataMember]
		public string TitleEntity { get; set; }

		/// <summary>
		/// Gets or sets  the portlet entity.
		/// </summary>
		[DataMember]
		public BaseBusinessEntity Entity { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the longpath of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string EntityLongPath {
			get {
				var type = Langue.ResourceManager.GetString(Helper.GetAttributeValue<LocalizedTextAttribute>(this.GetEntityType()));
				return type + " / " + this.TitleEntity;
			}
			private set {

			}
		}

		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <returns></returns>
		public Type GetEntityType() {
			return Helper.GetPortletEntityType(this.TypeEntity);
		}
	}
}

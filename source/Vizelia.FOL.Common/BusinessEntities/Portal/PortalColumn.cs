﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.BusinessEntities;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Portal Column.
	/// </summary>
	[Key("KeyPortalColumn")]
	[DataContract]
    public class PortalColumn : BaseBusinessEntity, IMappableEntity, IPortalWindowElement {
		/// <summary>
		/// Initializes a new instance of the <see cref="PortalColumn"/> class.
		/// </summary>
		public PortalColumn() {
			this.Portlets = new List<Portlet>();
		}

		/// <summary>
		/// Key of the Portal Column.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyPortalColumn { get; set; }


		/// <summary>
		/// The local id of the Portal Column.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }


		/// <summary>
		/// Key of the Portal Tab.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPortalTab { get; set; }

		/// <summary>
		/// Key of the Portal Window.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(PortalWindow), false)]
		public string KeyPortalWindow { get; set; }


		/// <summary>
		/// Gets or sets the relative width of the column.
		/// </summary>
		[DataMember]
		public int Flex { get; set; }

		/// <summary>
		/// Gets or sets the order of the portal column in the portal tab.
		/// </summary>
		[DataMember]
		public int Order { get; set; }


		/// <summary>
		/// Gets os sets the list of portlets of the portal column.
		/// </summary>
		[DataMember]
		public List<Portlet> Portlets { get; set; }


		/// <summary>
		/// Gets or sets the name of the parent PortalTab title.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PortalTabTitle { get; set; }


		/// <summary>
		/// Gets or sets the name of the parent PortalWindow title.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PortalWindowTitle { get; set; }

		/// <summary>
		/// Gets or sets the name of the parent PortalWindow IconCls .
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PortalWindowIconCls { get; set; }

		/// <summary>
		/// Gets or sets the type of column.
		/// </summary>
		/// <value>
		/// The type.
		/// </value>
		[DataMember]
		public PortalColumnType Type { get; set; }

		/// <summary>
		/// The full name of the PortalColumn.
		/// The dummy setter is there only for serialization purpose.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string FullTitle {
			get {
				return string.Format("{0} - {1} - {2} {3}", PortalWindowTitle, PortalTabTitle, Langue.msg_portalcolumn, Order);
			}
			set {
			}
		}


		/// <summary>
		/// Gets the total flex of the Portlet collection
		/// </summary>
		[DataMember]
		public int TotalFlex {
			get {
				int _totalflex = 0;
				if (Portlets != null) {
					foreach (Portlet portlet in Portlets) {
						_totalflex += portlet.Flex;
					}
				}
				return _totalflex;
			}
			set {
			}
		}

	}
}

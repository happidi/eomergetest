﻿using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using System.Reflection;
using System;
using Vizelia.FOL.Common.BusinessEntities;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Portlet.
	/// </summary>
	[Key("KeyPortlet")]
	[DataContract]
	[LocalizedText("msg_portlet")]
	[IconCls("viz-icon-small-portlet")]
    public class Portlet : BaseBusinessEntity, IMappableEntity, IPortalWindowElement {
		/// <summary>
		/// Key of the Portlet
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyPortlet { get; set; }

		/// <summary>
		/// The local id of the Portlet.
		/// </summary>
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Key of the PortalColumn.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPortalColumn { get; set; }

		/// <summary>
		/// Key of the Portal Tab.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyPortalTab { get; set; }

		/// <summary>
		/// Key of the Portal Window.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(PortalWindow), false)]
		public string KeyPortalWindow { get; set; }

		/// <summary>
		/// Gets or sets the key of the Portlet entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyEntity { get; set; }

		/// <summary>
		/// Gets or sets the type of the Portlet entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string TypeEntity { get; set; }

		/// <summary>
		/// Gets or sets  the portlet entity.
		/// </summary>
		[DataMember]
		public BaseBusinessEntity Entity { get; set; }
		/// <summary>
		/// Gets or sets the alarm table entity.
		/// </summary>
		/// <value>
		/// The alarm table entity.
		/// </value>
		[DataMember]
		public AlarmTable AlarmTableEntity { get; set; }

		/// <summary>
		/// Gets or sets the chart entity.
		/// </summary>
		/// <value>
		/// The chart entity.
		/// </value>
		[DataMember]
		public Chart ChartEntity { get; set; }

		/// <summary>
		/// Gets or sets the drawing canvas entity.
		/// </summary>
		/// <value>
		/// The drawing canvas entity.
		/// </value>
		[DataMember]
		public DrawingCanvas DrawingCanvasEntity { get; set; }

		/// <summary>
		/// Gets or sets the flip card entity.
		/// </summary>
		/// <value>
		/// The flip card entity.
		/// </value>
		[DataMember]
		public FlipCard FlipCardEntity { get; set; }
		
		/// <summary>
		/// Gets or sets the map entity.
		/// </summary>
		/// <value>
		/// The map entity.
		/// </value>
		[DataMember]
		public Map MapEntity { get; set; }
		
		/// <summary>
		/// Gets or sets the weather location entity.
		/// </summary>
		/// <value>
		/// The weather location entity.
		/// </value>
		[DataMember]
		public WeatherLocation WeatherLocationEntity { get; set; }
		
		/// <summary>
		/// Gets or sets the web frame entity.
		/// </summary>
		/// <value>
		/// The web frame entity.
		/// </value>
		[DataMember]
		public WebFrame WebFrameEntity { get; set; }
		
		/// <summary>
		/// Gets or sets the longpath of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string EntityLongPath {
			get {
				var type = Langue.ResourceManager.GetString(Helper.GetAttributeValue<LocalizedTextAttribute>(this.GetEntityType()));
				return  type + " / " + this.TitleEntity ;
			}
			private set {

			}
		}

		/// <summary>
		/// Gets or sets the title of the Portlet.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the title of the Portlet contained entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string TitleEntity { get; set; }

		/// <summary>
		/// Get or sets the collapsed state of the Portlet.
		/// </summary>
		[DataMember]
		public bool Collapsed { get; set; }

		/// <summary>
		/// Get or sets the visible state of the Portlet header and toolbar.
		/// </summary>
		[DataMember]
		public bool HeaderAndToolbarVisible { get; set; }


		/// <summary>
		/// Gets or sets the relative height of the Portlet.
		/// </summary>
		[DataMember]
		public int Flex { get; set; }

		/// <summary>
		/// Gets or sets the order of the Portlet in the portal column.
		/// </summary>
		[DataMember]
		public int Order { get; set; }

		/// <summary>
		/// Gets or sets the height of the fixed.
		/// </summary>
		[DataMember]
		public int FixedHeight { get; set; }

		/// <summary>
		/// Gets or sets the refresh frequency of the portlet to manage auto refresh
		/// </summary>
		[DataMember]
		public int? RefreshFrequency { get; set; }

		/// <summary>
		/// Gets or sets the name of the parent PortalTab title.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PortalTabTitle { get; set; }


		/// <summary>
		/// Gets or sets the name of the parent PortalWindow title.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PortalWindowTitle { get; set; }

		/// <summary>
		/// Gets or sets the name of the parent PortalWindow IconCls .
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PortalWindowIconCls { get; set; }


		/// <summary>
		/// Gets or sets the refresh animation direction.
		/// </summary>
		/// <value>
		/// The refresh animation direction.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string RefreshAnimationDirection { get; set; }

		/// <summary>
		/// Gets or sets the associated portal tab key.
		/// </summary>
		/// <value>
		/// The associated portal tab key.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string AssociatedPortalTabKey { get; set; }


		/// <summary>
		/// Gets the type of the entity.
		/// </summary>
		/// <returns></returns>
		public Type GetEntityType() {
			return Helper.GetPortletEntityType(this.TypeEntity);
		}
	}
}

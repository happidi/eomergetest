﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.BusinessEntities;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Portal Tab.
	/// </summary>
	[Key("KeyPortalTab")]
	[DataContract]
	[LocalizedText("msg_portaltab")]
	[IconCls("viz-icon-small-portaltab-column3-255025")]
    public class PortalTab : BaseBusinessEntity, IMappableEntity, IPortalWindowElement {

		/// <summary>
		/// Initializes a new instance of the <see cref="PortalTab"/> class.
		/// </summary>
		public PortalTab() {
			this.Columns = new List<PortalColumn>();
		}

		/// <summary>
		/// Key of the Portal Tab.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyPortalTab { get; set; }

		/// <summary>
		/// Key of the Portal Window.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(PortalWindow), false)]
		public string KeyPortalWindow { get; set; }

		/// <summary>
		/// Gets or sets the name of this portal tab.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// The local id of the Portal Tab.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the name of the parent PortalWindow title.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PortalWindowTitle { get; set; }

		/// <summary>
		/// Gets or sets the name of the parent PortalWindow IconCls .
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PortalWindowIconCls { get; set; }

		/// <summary>
		/// Gets the name of the parent PortalWindow KeyImage  .
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string PortalWindowKeyImage { get; set; }

		/// <summary>
		/// The full name of the PortalTab.
		/// The dummy setter is there only for serialization purpose.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string FullTitle {
			get {
				return PortalWindowTitle + " - " + Title;
			}
			set {
			}
		}
		
		/// <summary>
		/// Gets or sets the list of column of the portal tab.
		/// </summary>
		[DataMember]
		public List<PortalColumn> Columns { get; set; }

		/// <summary>
		/// Gets or sets the order of the portal tab in the portal window.
		/// </summary>
		[DataMember]
		public int Order { get; set; }

		/// <summary>
		/// Gets the total flex of the Column collection
		/// </summary>
		[DataMember]
		public int TotalFlex {
			get {
				return GetTotalFlex(PortalColumnType.Main);
			}
			set {
			}
		}

		/// <summary>
		/// Gets the total flex of the header Column collection
		/// </summary>
		[DataMember]
		public int HeaderTotalFlex {
			get {
				return GetTotalFlex(PortalColumnType.Header);
			}
			set {
			}
		}

		/// <summary>
		/// Gets the total flex of the footer Column collection
		/// </summary>
		[DataMember]
		public int FooterTotalFlex {
			get {
				return GetTotalFlex(PortalColumnType.Footer);
			}
			set {
			}
		}


		/// <summary>
		/// Gets the total flex.
		/// </summary>
		/// <param name="type">The type of column.</param>
		/// <returns></returns>
		private int GetTotalFlex(PortalColumnType type) {
			int totalflex = 0;
			if (Columns != null) {
				totalflex += Columns.Where(col => col.Type == type).Sum(col => col.Flex);
			}
			return totalflex;
		}

		/// <summary>
		/// The VirtualFile of the PortalTab.
		/// </summary>
		[DataMember]
		public VirtualFile VirtualFile { get; set; }

		/// <summary>
		/// Gets  or sets the background Cls of the PortalTab.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string BackgroundCls { get; set; }

		/// <summary>
		/// The portaltab columns config.
		/// </summary>
		[DataMember]
		public PortalTabColumnConfig ColumnConfig { get; set; }

		/// <summary>
		/// Gets or sets the height of the header.
		/// </summary>
		/// <value>
		/// The height of the header.
		/// </value>
		[DataMember]
		public int? HeaderHeight { get; set; }

		/// <summary>
		/// Gets or sets the height of the footer.
		/// </summary>
		/// <value>
		/// The height of the footer.
		/// </value>
		[DataMember]
		public int? FooterHeight { get; set; }
	}
}

﻿using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Localization;
using System;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Portal Template.
	/// </summary>
	[Key("KeyPortalTemplate")]
	[DataContract]
	[Serializable]
	[IconCls("viz-icon-small-portaltemplate")]
	[LocalizedText("msg_portaltemplate")]
	public class PortalTemplate : BaseBusinessEntity, ISecurableObject, IMappableEntity {

		/// <summary>
		/// Key of the Portal.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(PortalTemplate), false)]
		public string KeyPortalTemplate { get; set; }

		/// <summary>
		/// The local id of the PortalTemplate.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the title of the PortalTemplate.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }


		/// <summary>
		/// Gets or sets the Description of the PortalTemplate.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets wheter the modification to the associated portals are automatically pushed to the associated users.
		/// </summary>
		[DataMember]
		public bool AutoUpdate { get; set; }

		/// <summary>
		/// Gets or sets the time zone id in which we want to export the portal templates.
		/// </summary>
		/// <value>
		/// The time zone id.
		/// </value>
		[Information("msg_chart_timezone", null)]
		[AntiXssValidator]
		[DataMember]
		public string TimeZoneId { get; set; }

		/// <summary>
		/// The list of PortalWindows associated to this PortalTemplate.
		/// </summary>
		[DataMember]
		public List<PortalWindow> PortalWindows { get; set; }


		/// <summary>
		/// The list of Users associated to this PortalTemplate.
		/// </summary>
		[DataMember]
		public List<FOLMembershipUser> Users { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Gets or sets the number of columns.
		/// </summary>
		[DataMember]
		public int? NumberOfColumns { get; set; }

		/// <summary>
		/// Gets or sets the start X.
		/// </summary>
		[DataMember]
		public int? StartX { get; set; }

		/// <summary>
		/// Gets or sets the start Y.
		/// </summary>
		[DataMember]
		public int? StartY { get; set; }
		
		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity() {
				KeySecurable = this.KeyPortalTemplate,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(this.GetType()),
				SecurableName = this.Title,
				SecurableTypeName = this.GetType().FullName
			};
		}


	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Common.BusinessEntities {
    /// <summary>
    /// Interface for entities contained in a portal window
    /// </summary>
    public interface IPortalWindowElement {
        /// <summary>
        /// Gets or sets the key portal window.
        /// </summary>
        string KeyPortalWindow { get; set; }

        /// <summary>
        /// Gets or sets the portal window icon CLS.
        /// </summary>
        /// <value>
        /// The portal window icon CLS.
        /// </value>
        string PortalWindowIconCls { get; set; }

        /// <summary>
        /// Gets or sets the portal window title.
        /// </summary>
        /// <value>
        /// The portal window title.
        /// </value>
        string PortalWindowTitle { get; set; }
    }
}

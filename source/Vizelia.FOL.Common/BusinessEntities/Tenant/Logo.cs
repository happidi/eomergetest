﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for tenant logo
	/// </summary>
	[Serializable]
	[Key("KeyLogo")]
	[DataContract]
	[LocalizedText("msg_logo_image")]
	[IconCls("viz-icon-small-logo")]
	public class Logo : BaseBusinessEntity, ISupportDocuments {

		/// <summary>
		/// Gets the application.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Application { get; set; }

		/// <summary>
		/// Gets or sets the key logo.
		/// </summary>
		/// <value>
		/// The key logo.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyLogo { get; set; }

		/// <summary>
		/// Gets or Sets the Key of the document attached to it.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyImage { get; set; }

		/// <summary>
		/// Gets the Image document associated.
		/// </summary>
		public Document Image { get; set; }

		/// <summary>
		/// The Key of the DynamicDisplayImage to display in the Header.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyKPIPreviewPicture { get; set; }

		/// <summary>
		/// Gets the propreties that have document ids
		/// </summary>
		/// <returns></returns>
		public List<string> GetDocumentFields() {
			return new List<string> { "KeyImage" };
		}
	}
}

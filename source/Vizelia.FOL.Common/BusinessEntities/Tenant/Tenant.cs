﻿using System;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Tenant.
	/// </summary>
	[Key("KeyTenant")]
	[DataContract]
	[Serializable]
	[IconCls("viz-icon-small-tenant")]
	public class Tenant : BaseBusinessEntity {
		/// <summary>
		/// Gets or sets the key tenant.
		/// </summary>
		/// <value>
		/// The key tenant.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[VizStringLengthValidator(1, RangeBoundaryType.Inclusive, 40, RangeBoundaryType.Inclusive)]
		public string KeyTenant {
			get { return Name; }
			set { Name = value; }
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the URL.
		/// </summary>
		/// <value>
		/// The URL.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string Url { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this tenant is initialized.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this tenant is initialized; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsInitialized { get; set; }

		/// <summary>
		/// Gets or sets the admin email.
		/// </summary>
		/// <value>
		/// The admin email.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string AdminEmail { get; set; }

		/// <summary>
		/// Gets or sets the admin password
		/// </summary>
		/// <value>
		/// The admin password.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string AdminPassword { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is isolated.
		/// This property does not serialize as tenants from now on are always running in shared mode.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is isolated; otherwise, <c>false</c>.
		/// </value>
		public bool IsIsolated { get; set; }

		/// <summary>
		/// Gets or sets the hosting container. (e.g. ApplicationPool in local IIS provider, machine instance in Cloud Provider).
		/// </summary>
		/// <value>
		/// The hosting container.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[ViewableByDataModel]
		public string HostingContainer { get; set; }
	}
}


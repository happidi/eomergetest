﻿// Shared constants

using System;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Struct for mime types.
	/// </summary>
	public struct MimeType {
		/// <summary>
		/// Gets the image MIME type by extension.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <returns></returns>
		public static string GetImageMimeTypeByExtension(string filename) {
			if (string.IsNullOrEmpty(filename)) {
				throw new ArgumentException("Empty filename");
			}

			string lowerFilename = filename.ToLower();

			if (lowerFilename.EndsWith(".bmp")) {
				return Bmp;
			}
			if (lowerFilename.EndsWith(".jpg") || lowerFilename.EndsWith(".jpeg")) {
				return Jpeg;
			}
			if (lowerFilename.EndsWith(".png")) {
				return Png;
			}
			if (lowerFilename.EndsWith(".gif")) {
				return Gif;
			}

			throw new ArgumentException("filename extension cannot be recognized as image: " + filename);
		}

		/// <summary>
		/// Generic binary mime type.
		/// </summary>
		public const string Binary = "application/octet-stream";
		/// <summary>
		/// Bmp mime type.
		/// </summary>
		public const string Bmp = "image/bmp";
		/// <summary>
		/// Excel mime type.
		/// </summary>
		public const string Excel = "application/vnd.ms-excel";
		/// <summary>
		/// Excel spreadsheet mime type.
		/// </summary>
		public const string ExcelSpreadSheet = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		/// <summary>
		/// Gif mime type.
		/// </summary>
		public const string Gif = "image/gif";
		/// <summary>
		/// Html mime type.
		/// </summary>
		public const string Html = "text/html";
		/// <summary>
		/// Form mime type.
		/// </summary>
		public const string HttpForm = "application/x-www-form-urlencoded";
		/// <summary>
		/// Jpeg mime type.
		/// </summary>
		public const string Jpeg = "image/jpeg";
		/// <summary>
		/// Json mime type.
		/// </summary>
		public const string Json = "application/json";
		/// <summary>
		/// Pdf mime type.
		/// </summary>
		public const string Pdf = "application/pdf";
		/// <summary>
		/// Png mime type.
		/// </summary>
		public const string Png = "image/png";
		/// <summary>
		/// Svg mime type.
		/// </summary>
		public const string Svg = "image/svg+xml";
		/// <summary>
		/// Text mime type.
		/// </summary>
		public const string Text = "text/plain";
		/// <summary>
		/// Xml mime type.
		/// </summary>
		public const string Xml = "text/xml";
		/// <summary>
		/// Xml mime type.
		/// </summary>
		public const string Csv = "text/csv";
		/// <summary>
		/// Zip mime type.
		/// </summary>
		public const string Zip = "application/zip";
	}

	/// <summary>
	/// Struct for CRUD actions.
	/// </summary>
	public struct CrudAction {

		/// <summary>
		/// Create action.
		/// </summary>
		public const string Create = "create";
		/// <summary>
		/// Destroy action.
		/// </summary>
		public const string Destroy = "destroy";
		/// <summary>
		/// Read action.
		/// </summary>
		public const string Read = "read";
		/// <summary>
		/// Update action.
		/// </summary>
		public const string Update = "update";
	}

	/// <summary>
	/// Struct for context keys.
	/// </summary>
	public struct ContextKey {
		/// <summary>
		/// The name of the context key for renewing session.
		/// </summary>
		public const string const_context_renewsession = "renewsession";

		/// <summary>
		/// The key of a context property that if set, causes some brokers to bypass security checks.
		/// </summary>
		public const string const_bypass_security = "BypassSecurity";
	}

	/// <summary>
	/// Struct for cache keys.
	/// </summary>
	public struct CacheKey {
	
		/// <summary>
		/// The name of the cache key for securable types.
		/// </summary>
		public const string const_cache_basebusinessentity_types = "BaseBusinessEntityTypes";

		/// <summary>
		/// The name of the cache key for securable types.
		/// </summary>
		public const string const_cache_securabletypes = "SecurableTypes";

		/// <summary>
		/// The name of the cache key for portlet types.
		/// </summary>
		public const string const_cache_portlettypes = "PortletTypes";

		/// <summary>
		/// The name of the cache key for Algorithm types.
		/// </summary>
		public const string const_cache_algorithm_types = "AlgorithmsForType:{0}";

		/// <summary>
		/// The name of the cache key for classification roots.
		/// </summary>
		public const string const_cache_classifications = "Classifications";

		/// <summary>
		/// The name of the cache key for classification roots.
		/// </summary>
		public const string const_cache_calendareventoccurances = "_calendarEventOccurances_";

		/// <summary>
		/// The name of the cache key for the user's ssid.
		/// </summary>
		public const string const_cache_usercustomsid = "_UserSSID";

		/// <summary>
		/// The name of the cache key for the azman cached guid.
		/// </summary>
		public const string const_cache_azmancacheguid = "AzmanCacheGuid";

		/// <summary>
		/// The name of the cache key for active sessions.
		/// </summary>
		public const string const_cache_activesessions = "ActiveSessions";

		/// <summary>
		/// The name of the cache key for azure machine instance.
		/// </summary>
		public const string const_cache_azure_machineinstances = "AzureMachineInstances";

		/// <summary>
		/// The name of the cache key for bing map location.
		/// </summary>
		public const string const_cache_bing_map_location = "BingMapLocation";

		/// <summary>
		/// The name of the cache key for razor template guid.
		/// </summary>
		public const string const_cache_razor_template_guid = "RazorTemplateGuid";

		/// <summary>
		/// The name of the cache key for EA context.
		/// </summary>
		public const string const_cache_ea_context = "EAcontext";

		/// <summary>
		/// The name of the cache key for localization messages cache.
		/// </summary>
		public const string const_cache_localization = "Localization";

		/// <summary>
		/// All Azman Tasks.
		/// </summary>
		public const string const_cache_all_tasks = "AllTasks";
	}

	/// <summary>
	/// Struct for session keys
	/// </summary>
	public struct SessionKey {

		/// <summary>
		/// The name of the session key for documents.
		/// </summary>
		public const string DocumentSessionPrefix = "document_";
		/// <summary>
		/// The name of the session cookie for filter data.
		/// </summary>
		public const string FilterPrefix = "Filter";

		/// <summary>
		/// The key of the item used to store session context.
		/// </summary>
		public const string const_session_context = "context";

		/// <summary>
		/// The key for holding the variable which tells us if we need to refresh the current user's last login date.
		/// </summary>
		public const string const_session_refreshLastLogin = "refreshLastLogin";
	}

	/// <summary>
	/// Struct for application modules. 
	/// For example it is used in BrokerDB to reference the name of stored procedures.
	/// </summary>
	public struct AppModule {

		/// <summary>
		/// The Core module.
		/// </summary>
		public const string Core = "Core";

		/// <summary>
		/// The Energy module.
		/// </summary>
		public const string Energy = "Energy";

		/// <summary>
		/// The Audit module.
		/// </summary>
		public const string Audit = "Audit";

		/// <summary>
		/// The Mapping module.
		/// </summary>
		public const string Mapping = "Mapping";

		/// <summary>
		/// The Membership module.
		/// </summary>
		public const string Membership = "Membership";

		/// <summary>
		/// The Reporting module.
		/// </summary>
		public const string Reporting = "Reporting";

		/// <summary>
		/// The ServiceDesk module.
		/// </summary>
		public const string ServiceDesk = "ServiceDesk";

		/// <summary>
		/// The SpaceManagement module.
		/// </summary>
		public const string SpaceManagement = "SpaceManagement";

		/// <summary>
		/// The Tenancy module.
		/// </summary>
		public const string Tenancy = "Tenancy";
	}

	/// <summary>
	/// Struct for classification definition categories.
	/// </summary>
	public struct ClassificationCategory {
		/// <summary>
		/// The Meter catogory.
		/// </summary>
		public const string Meter = "meter";

		/// <summary>
		/// The Organization category.
		/// </summary>
		public const string Organization = "organization";

		/// <summary>
		/// The Work category.
		/// </summary>
		public const string Work = "work";

		/// <summary>
		/// The ActionRequest category.
		/// </summary>
		public const string ActionRequest = "actionrequest";

		/// <summary>
		/// The Space category.
		/// </summary>
		public const string Space = "space";

		/// <summary>
		/// The Equipment category.
		/// </summary>
		public const string Equipment = "equipment";

		/// <summary>
		/// The Worktype category.
		/// </summary>
		public const string Worktype = "worktype";

		/// <summary>
		/// The Alarm category.
		/// </summary>
		public const string Alarm = "alarm";

		/// <summary>
		/// The Task category.
		/// </summary>
		public const string Task = "task";
	}

	/// <summary>
	/// Struct for default chart size.
	/// </summary>
	public struct ChartSize {

		/// <summary>
		/// The default image height to generate when no size has been specified.
		/// </summary>
		public const int DefaultImageHeight = 600;

		/// <summary>
		/// The default image width to generate when no size has been specified.
		/// </summary>
		public const int DefaultImageWidth = 800;
	}
}

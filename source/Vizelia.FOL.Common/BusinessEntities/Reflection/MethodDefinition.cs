﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A class representing a method definition in an assembly.
	/// </summary>
	[Key("KeyMethodDefinition")]
	[IconCls("viz-icon-small-method")]
	[DataContract]
	public class MethodDefinition : ClassDefinition {

		/// <summary>
		/// Gets or sets the key element.
		/// </summary>
		[DataMember]
		public string KeyMethodDefinition {
			get { return this.KeyClassDefinition + "." + this.MethodName; }
			set { }
		}

		/// <summary>
		/// Gets or sets the assembly short name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[DataMember]
		public string MethodName { get; set; }

		/// <summary>
		/// Gets or sets the qualified name of the method.
		/// </summary>
		[DataMember]
		public string MethodQualifiedName {
			get {
				return this.KeyClassDefinition + "." + this.MethodName;
			}
			set { }
		}

		/// <summary>
		/// Gets or sets the xtype of the grid for this method
		/// </summary>
		/// <value>
		/// The grid xtype.
		/// </value>
		[DataMember]
		public string GridXType { get; set; }

		/// <summary>
		/// Gets or sets the xtype of the store for this method
		/// </summary>
		/// <value>
		/// The store xtype.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string StoreXType { get; set; }

		/// <summary>
		/// Gets or sets the key entity.
		/// </summary>
		/// <value>
		/// The key entity.
		/// </value>
		[DataMember]
		public string KeyEntity { get; set; }

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>
		/// The TreeNode object.
		/// </returns>
		public new TreeNode GetTree() {
			return new TreeNode {
				Key = this.Id,
				text = this.Label,
				leaf = true,
				entity = this,
				qtip = this.ClassName + '\n' + this.MethodName + '\n' + this.AssemblyQualifiedName,
				iconCls = this.iconCls
			};
		}
	}
}

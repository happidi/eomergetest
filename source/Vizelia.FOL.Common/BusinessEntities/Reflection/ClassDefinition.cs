﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A class representing a class definition.
	/// </summary>
	[Key("KeyClassDefinition")]
	[IconCls("viz-icon-small-class")]
	[DataContract]
	public class ClassDefinition : ListElement, ITreeNode {

		/// <summary>
		/// Gets or sets the key element.
		/// </summary>
		[DataMember]
		public string KeyClassDefinition {
			get {
				return this.ClassName;
			}
			set { }
		}

		/// <summary>
		/// The resource message of the element description.
		/// </summary>
		[DataMember]
		public string MsgCodeDescription { get; set; }

		/// <summary>
		/// The translated value of the element description.
		/// </summary>
		[DataMember]
		public string LabelDescription {
			get {
				return Helper.LocalizeText(this.MsgCodeDescription);
			}
			set { }
		}

		/// <summary>
		/// Gets or sets the Class full name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[DataMember]
		public string ClassName { get; set; }

		/// <summary>
		/// Gets or sets the full name of the assembly.
		/// </summary>
		/// <value>
		/// The full name.
		/// </value>
		[DataMember]
		public string AssemblyQualifiedName { get; set; }

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>
		/// The TreeNode object.
		/// </returns>
		public TreeNode GetTree() {
			return new TreeNode {
				Key = this.Id,
				text = this.Label,
				leaf = true,
				entity = this,
				qtip = this.ClassName + '\n' + this.AssemblyQualifiedName,
				iconCls = this.iconCls
			};
		}
	}
}

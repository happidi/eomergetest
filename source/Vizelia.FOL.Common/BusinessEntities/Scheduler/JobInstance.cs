﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using DDay.iCal;
using Vizelia.FOL.BusinessEntities.Mapping;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// The Job business entity.
	/// </summary>
	[DataContract]
	[Key("KeyJobInstance")]
	public class JobInstance : BaseBusinessEntity {

		/// <summary>
		/// Gets or sets the key job instance.
		/// </summary>
		/// <value>
		/// The key job instance.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyJobInstance { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[DataMember]
		public TraceEntrySeverity? Severity { get; set; }
		
		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		/// <value>
		/// The start date.
		/// </value>
		[DataMember]
		public DateTime? StartDate { get; set; }
		
		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		/// <value>
		/// The end date.
		/// </value>
		[DataMember]
		public DateTime? EndDate { get; set; }
		
		/// <summary>
		/// Gets or sets the duration of the run.
		/// </summary>
		/// <value>
		/// The last duration of the run.
		/// </value>
		[DataMember]
		public string Duration { get; set; }
		
		/// <summary>
		/// Gets or sets the last run error message.
		/// </summary>
		/// <value>
		/// The last run error message.
		/// </value>
		[DataMember]
		public string FirstErrorMessage { get; set; }
		
		/// <summary>
		/// Gets or sets the job run instance request id.
		/// </summary>
		/// <value>
		/// The job run instance request id.
		/// </value>
		[DataMember]
		public string JobRunInstanceRequestId { get; set; }

		/// <summary>
		/// Gets or sets the key job.
		/// </summary>
		/// <value>
		/// The key job.
		/// </value>
		[DataMember]
		public string KeyJob { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// The interface for runnable jobs.
	/// </summary>
	public interface IRunnableJob {
		/// <summary>
		/// Runs the job with the specified context.
		/// </summary>
		/// <param name="context">The context.</param>
		void Run( JobContext context);
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;


namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Schedule entry.
	/// </summary>
	[Serializable]
	[Key("KeySchedule")]
	[DataContract]
	[IconCls("viz-icon-small-scheduler")] 
	public class Schedule : CalendarEvent {

		/// <summary>
		/// Contructor
		/// </summary>
		public Schedule()
			: base() {
			Category = "Schedule";
		}
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="ev">a CalendarEvent.</param>
		public Schedule(CalendarEvent ev) {

			if (ev != null) {
				this.Category = ev.Category;
				this.EndDate = ev.EndDate;
				this.InitialStartDate = ev.InitialStartDate;
				this.IsAllDay = ev.IsAllDay;
				this.Key = ev.Key;
				this.KeyLocation = ev.KeyLocation;
				this.Location = ev.Location;
				this.qtip = ev.qtip;
				this.RecurrencePattern = ev.RecurrencePattern;
				this.RelatedTo = ev.RelatedTo;
				this.StartDate = ev.StartDate;
				this.Title = ev.Title;
			}
		}
		/// <summary>
		/// Key of the the Schedule.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeySchedule { get; set; }

		

	}
}



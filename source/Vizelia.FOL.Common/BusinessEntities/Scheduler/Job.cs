﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using DDay.iCal;
using Vizelia.FOL.BusinessEntities.Mapping;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// The Job business entity.
	/// </summary>
	[DataContract]
	[Key("KeyJob")]
	public class Job : BaseBusinessEntity {
		/// <summary>
		/// Gets or sets the job key.
		/// </summary>
		/// <value>
		/// The job key.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyJob { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the steps.
		/// </summary>
		/// <value>
		/// The tasks.
		/// </value>
		[DataMember]
		public List<Step> Steps { get; set; }

		/// <summary>
		/// Gets or sets the schedules.
		/// </summary>
		/// <value>
		/// The schedules.
		/// </value>
		[DataMember]
		public List<Schedule> Schedules { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is active, meaning that it is currently scheduled to run in the future.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is active; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsScheduledToRun {
			get {
				if ((Schedules != null) && (Schedules.Exists(sched => sched.RecurrencePattern != null)))
					return true;
				var maxStartDate = Schedules.Select(sched => sched.StartDate.ToUniversalTime()).Max();
				return (maxStartDate > DateTime.UtcNow);
			}
			set { } 
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is paused.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is paused; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsPaused { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[DataMember]
		public TraceEntrySeverity? LastRunSeverity { get; set; }

		/// <summary>
		/// Gets or sets the last run time.
		/// </summary>
		/// <value>
		/// The last run time.
		/// </value>
		[DataMember]
		public DateTime? LastRunTime { get; set; }
		
		/// <summary>
		/// Gets or sets the last duration of the run.
		/// </summary>
		/// <value>
		/// The last duration of the run.
		/// </value>
		[DataMember]
		public string LastRunDuration { get; set; }
		
		/// <summary>
		/// Gets or sets the next run time.
		/// </summary>
		/// <value>
		/// The next run time.
		/// </value>
		[DataMember]
		public DateTime? NextRunTime {
			get {
				var result = CalendarHelper.GetNextRunTimeForJob(Schedules);
				return result;
			}
			set { } 
		}

		/// <summary>
		/// Gets or sets the last run error message.
		/// </summary>
		/// <value>
		/// The last run error message.
		/// </value>
		[DataMember]
		public string LastRunErrorMessage { get; set; }

		/// <summary>
		/// Gets or sets the triggers.
		/// </summary>
		/// <value>
		/// The triggers.
		/// </value>
		[DataMember]
		public string Triggers {
			get { 
				var result = CalendarHelper.GetTriggersStringForJob(Schedules);
				return result;
			}
				
			set { }
		}

		/// <summary>
		/// Gets or sets the job run instance request id.
		/// </summary>
		/// <value>
		/// The job run instance request id.
		/// </value>
		[DataMember]
		public string JobRunInstanceRequestId { get; set; }
	}
}
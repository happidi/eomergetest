﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// The Step business entity.
	/// </summary>
	[Serializable]
	[DataContract]
	public class Step : BaseBusinessEntity {
		/// <summary>
		/// Key of the Step.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyStep { get; set; }

		/// <summary>
		/// Key of the parent Job.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyJob { get; set; }

		/// <summary>
		/// Gets or sets the data model keys.
		/// </summary>
		/// <value>
		/// The data model keys.
		/// </value>
		[AntiXssListValidator]
		[DataMember]
		public List<string> ModelDataKeys { get; set; }

		/// <summary>
		/// Gets or sets the type of the task.
		/// </summary>
		/// <value>
		/// The type of the task.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string MethodQualifiedName { get; set; }

		/// <summary>
		/// Gets or sets the name of the method.
		/// </summary>
		/// <value>
		/// The name of the method.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string MethodName { get; set; }


		/// <summary>
		/// Gets or sets Method MsgCode.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MethodMsgCode { get; set; }



		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the type of the executor.
		/// </summary>
		/// <value>
		/// The type of the executor.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string ExecutorType {
			get { return "Vizelia.FOL.ConcreteProviders.WCFRunnableJob, Vizelia.FOL.ConcreteProviders"; }
			set { }
		}
	}
}

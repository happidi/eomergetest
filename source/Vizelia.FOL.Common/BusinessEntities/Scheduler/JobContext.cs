﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Linq;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Passed as a parameter to the Job Scheduling infrastructure.
	/// </summary>
	[DataContract]
	public class JobContext : BaseBusinessEntity {

		/// <summary>
		/// Gets or sets the job run instance request id.
		/// </summary>
		/// <value>
		/// The job run instance request id.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string JobRunInstanceRequestId { get; set; }

		/// <summary>
		/// The Key of the job
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string JobKey { get; set; }

		/// <summary>
		/// The Key of the Step caller.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string CallerKey { get; set; }

		/// <summary>
		/// The name of the Step  caller.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string CallerName { get; set; }


		/// <summary>
		/// The name of the application that created the job (multitenancy).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ApplicationName { get; set; }


		/// <summary>
		/// Gets or sets the keys.
		/// </summary>
		/// <value>
		/// The keys.
		/// </value>
		[AntiXssListValidator]
		[DataMember]
		public List<string> Keys { get; set; }


		/// <summary>
		/// Gets or sets the name of the method.
		/// </summary>
		/// <value>
		/// The name of the method.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string MethodName { get; set; }


		/// <summary>
		/// Gets or sets the qualified name of the method.
		/// </summary>
		/// <value>
		/// The name of the method.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string MethodQualifiedName { get; set; }

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// Used by EntLib formatter and then by view Logging for persisting the context argument information in log as xml.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString() {
			string requestId = ContextHelper.RequestId;

			var xml = new XElement("context",
								   new XElement("callerkey", CallerKey),
								   new XElement("callername", CallerName),
								   new XElement("methodname", MethodName),
								   new XElement("methodqualifiedname", MethodQualifiedName),
								   new XElement("keys", Keys != null ? String.Join(",", Keys) : null),
								   new XElement("requestId", requestId)
				);

			return xml.ToString(SaveOptions.None);
		}
	}
}

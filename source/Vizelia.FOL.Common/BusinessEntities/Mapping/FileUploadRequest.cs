﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A file upload request business entity.
	/// </summary>
	[DataContract]
	public class FileUploadRequest : BaseBusinessEntity {

		/// <summary>
		/// Gets or sets the input file.
		/// This field should be represented in the client-side as a file upload field,
		/// however we expect it to contain an ID of a document that was just uploaded so we can
		/// retreive it using the DocumentHelper.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string InputFile { get; set; }

		/// <summary>
		/// Gets or sets the import options. (create/overwrite/delete)
		/// </summary>
		/// <value>
		/// The import options.
		/// </value>
		[DataMember]
		public AzmanImportOptions ImportOptions { get; set; } 
	}
}
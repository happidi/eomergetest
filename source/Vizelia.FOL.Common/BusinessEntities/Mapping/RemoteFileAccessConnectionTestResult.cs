using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A business entity representing a remote file access connection test result.
	/// </summary>
	[Serializable]
	[DataContract]
	public class RemoteFileAccessConnectionTestResult : BaseBusinessEntity {
		/// <summary>
		/// Initializes a new instance of the <see cref="RemoteFileAccessConnectionTestResult"/> class.
		/// </summary>
		public RemoteFileAccessConnectionTestResult() {
			Connect = Langue.msg_mapping_task_test_action_untested;
			CreateFile = Langue.msg_mapping_task_test_action_untested;
			CreateDirectory = Langue.msg_mapping_task_test_action_untested;
			DeleteFile = Langue.msg_mapping_task_test_action_untested;
			DeleteDirectory = Langue.msg_mapping_task_test_action_untested;
		}

		/// <summary>
		/// Gets or sets a value indicating whether the remote file access source can be connected to or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if the remote file access source can be connected to; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool CanConnect { get; set; }

		/// <summary>
		/// Gets or sets the connect description.
		/// </summary>
		/// <value>
		/// The connect exception.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Connect { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this we can create a file or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this we can create file; otherwise, <c>false</c>.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public bool CanCreateFile { get; set; }

		/// <summary>
		/// Gets or sets the create file description.
		/// </summary>
		/// <value>
		/// The create file description.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string CreateFile { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the remote file access source can have files deleted from it or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if the remote file access source can have files deleted from it; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool CanDeleteFile { get; set; }

		/// <summary>
		/// Gets or sets the delete description.
		/// </summary>
		/// <value>
		/// The delete description.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string DeleteFile { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance can create directory.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance can create directory; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool CanCreateDirectory { get; set; }

		/// <summary>
		/// Gets or sets the create directory description.
		/// </summary>
		/// <value>
		/// The create directory description.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string CreateDirectory { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance can delete directory.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance can delete directory; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool CanDeleteDirectory { get; set; }

		/// <summary>
		/// Gets or sets the delete directory description.
		/// </summary>
		/// <value>
		/// The delete directory description.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string DeleteDirectory { get; set; }
	}
}
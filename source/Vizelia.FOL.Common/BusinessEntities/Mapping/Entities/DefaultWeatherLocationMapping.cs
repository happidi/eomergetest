﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the Weather Location entity.
	/// </summary>
	[MappingSchema("WeatherLocation", "Default", "1.0.0")]
	public class DefaultWeatherLocationMapping : Mapping<WeatherLocation> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultWeatherLocationMapping"/> class.
		/// </summary>
		public DefaultWeatherLocationMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultWeatherLocationMapping"/> class.
		/// </summary>
		/// <param name="weatherLocation">The WeatherLocation.</param>
		public DefaultWeatherLocationMapping(WeatherLocation weatherLocation)
			: base(weatherLocation) {
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// The Weather Location id.
		/// </summary>
		public string LocationId {
			get { return Get(e => e.LocationId); }
			set { Set(value, e => e.LocationId); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		public bool IsFavorite {
			get { return Get(e => e.IsFavorite); }
			set { Set(value, e => e.IsFavorite); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		public bool OpenOnStartup {
			get { return Get(e => e.OpenOnStartup); }
			set { Set(value, e => e.OpenOnStartup); }
		}

		/// <summary>
		/// Gets or sets the display type.
		/// </summary>
		/// <value>
		/// The display type.
		/// </value>
		public WeatherDisplayType DisplayType {
			get { return Get(e => e.DisplayType); }
			set { Set(value, e => e.DisplayType); }
		}

		/// <summary>
		/// The Weather Unit.
		/// </summary>
		public WeatherUnit Unit {
			get { return Get(e => e.Unit); }
			set { Set(value, e => e.Unit); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
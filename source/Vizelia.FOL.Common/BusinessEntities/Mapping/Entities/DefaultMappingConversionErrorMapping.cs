﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping{
	/// <summary>
	/// A mapping for the MappingConversionError entity.
	/// </summary>
	[MappingSchema("MappingConversionError", "Default", "1.0.0")]
	public class DefaultMappingConversionErrorMapping : Mapping<MappingConversionError>{
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultMappingConversionErrorMapping"/> class.
		/// </summary>
		public DefaultMappingConversionErrorMapping()
		{
			// Calls base.
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultMappingConversionErrorMapping"/> class.
		/// </summary>
		/// <param name="mappingConvertionError">The mapping convertion error.</param>
		public DefaultMappingConversionErrorMapping(MappingConversionError mappingConvertionError) : base(mappingConvertionError)
		{
			// Calls base.
		}

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		/// <value>
		/// The error message.
		/// </value>
		public string ErrorMessage {
			get { return Get(e => e.ErrorMessage); }
			set { Set(value, e => e.ErrorMessage); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings()
		{
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the AuthroizationItem entity.
	/// </summary>
	[MappingSchema("AuthorizationItem", "Default", "1.0.0")]
	public class DefaultAuthorizationItemMapping : Mapping<AuthorizationItem> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultAuthorizationItemMapping"/> class.
		/// </summary>
		public DefaultAuthorizationItemMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultAuthorizationItemMapping"/> class.
		/// </summary>
		/// <param name="authorizationItem">The authorization item.</param>
		public DefaultAuthorizationItemMapping(AuthorizationItem authorizationItem)
			: base(authorizationItem) {
			// Many to many
			if (authorizationItem.ApplicationGroups != null) {
				var applicationGroupLocalIds = authorizationItem.ApplicationGroups.Select(x => x.LocalId).ToList();
				ApplicationGroups = new DefaultMappingCrudStoreMapping<AuthorizationItem, ApplicationGroup>(applicationGroupLocalIds, LocalId);
			}

			if (authorizationItem.Users != null) {
				var usersLocalIds = authorizationItem.Users.Select(x => x.LocalId).ToList();
				Users = new DefaultMappingCrudStoreMapping<AuthorizationItem, FOLMembershipUser>(usersLocalIds, LocalId);
			}

			if (authorizationItem.Tasks != null) {
				var tasksLocalIds = authorizationItem.Tasks.Select(x => x.LocalId).ToList();
				Tasks = new DefaultMappingCrudStoreMapping<AuthorizationItem, AuthorizationItem>(tasksLocalIds, LocalId);
			}

			// One to many, special case, these entities saved internally in AuthorizationItem entity.

			FilterSpatial = authorizationItem.FilterSpatial;

			FilterChart = authorizationItem.FilterChart;

			FilterClassificationItem = authorizationItem.FilterClassificationItem;

			FilterPortalWindow = authorizationItem.FilterPortalWindow;

			FilterReport = authorizationItem.FilterReport;

			FilterAlarmDefinition = authorizationItem.FilterAlarmDefinition;

			FilterDynamicDisplay = authorizationItem.FilterDynamicDisplay;

			FilterPlaylist = authorizationItem.FilterPlaylist;

			FilterChartScheduler = authorizationItem.FilterChartScheduler;

			FilterPortalTemplate = authorizationItem.FilterPortalTemplate;
		}

		/// <summary>
		/// The LocalId of the item.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The parent id of the authorization item.
		/// </summary>
		public string ParentId {
			get { return Get(e => e.ParentId); }
			set { Set(value, e => e.ParentId); }
		}

		/// <summary>
		/// The id of the authorization item.
		/// </summary>
		public string Id {
			get { return Get(e => e.Id); }
			set { Set(value, e => e.Id); }
		}

		/// <summary>
		/// The type of the authorization item.
		/// </summary>
		public string ItemType {
			get { return Get(e => e.ItemType); }
			set { Set(value, e => e.ItemType); }
		}

		/// <summary>
		/// The name of the authorization item.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// Gets or sets the application groups.
		/// </summary>
		/// <value>
		/// The application groups.
		/// </value>
		public DefaultMappingCrudStoreMapping<AuthorizationItem, ApplicationGroup> ApplicationGroups { get; set; }

		/// <summary>
		/// Gets or sets the users.
		/// </summary>
		/// <value>
		/// The users.
		/// </value>
		public DefaultMappingCrudStoreMapping<AuthorizationItem, FOLMembershipUser> Users { get; set; }

		/// <summary>
		/// Gets or sets the tasks.
		/// </summary>
		/// <value>
		/// The tasks.
		/// </value>
		public DefaultMappingCrudStoreMapping<AuthorizationItem, AuthorizationItem> Tasks { get; set; }

		/// <summary>
		/// Gets or sets the filter spatial.
		/// </summary>
		/// <value>
		/// The filter spatial.
		/// </value>
		[XmlArray("FilterSpatial")]
		[XmlArrayItem("SecurableEntity", typeof(SecurableEntity))]
		public List<SecurableEntity> FilterSpatial {
			get { return Get(x => x.FilterSpatial); }
			set { Set(value, x => x.FilterSpatial); }
		}

		/// <summary>
		/// Gets or sets the filter chart.
		/// </summary>
		/// <value>
		/// The filter chart.
		/// </value>
		[XmlArray("FilterChart")]
		[XmlArrayItem("SecurableEntity", typeof(SecurableEntity))]
		public List<SecurableEntity> FilterChart {
			get { return Get(x => x.FilterChart); }
			set { Set(value, x => x.FilterChart); }
		}

		/// <summary>
		/// Gets or sets the filter classification item.
		/// </summary>
		/// <value>
		/// The filter classification item.
		/// </value>
		[XmlArray("FilterClassificationItem")]
		[XmlArrayItem("SecurableEntity", typeof(SecurableEntity))]
		public List<SecurableEntity> FilterClassificationItem {
			get { return Get(x => x.FilterClassificationItem); }
			set { Set(value, x => x.FilterClassificationItem); }
		}

		/// <summary>
		/// Gets or sets the filter portal window.
		/// </summary>
		/// <value>
		/// The filter portal window.
		/// </value>
		[XmlArray("FilterPortalWindow")]
		[XmlArrayItem("SecurableEntity", typeof(SecurableEntity))]
		public List<SecurableEntity> FilterPortalWindow {
			get { return Get(x => x.FilterPortalWindow); }
			set { Set(value, x => x.FilterPortalWindow); }
		}

		/// <summary>
		/// Gets or sets the filter report.
		/// </summary>
		/// <value>
		/// The filter report.
		/// </value>
		[XmlArray("FilterReport")]
		[XmlArrayItem("SecurableEntity", typeof(SecurableEntity))]
		public List<SecurableEntity> FilterReport {
			get { return Get(x => x.FilterReport); }
			set { Set(value, x => x.FilterReport); }
		}

		/// <summary>
		/// Gets or sets the filter alarm definition.
		/// </summary>
		/// <value>
		/// The filter alarm definition.
		/// </value>
		[XmlArray("FilterAlarmDefinition")]
		[XmlArrayItem("SecurableEntity", typeof(SecurableEntity))]
		public List<SecurableEntity> FilterAlarmDefinition {
			get { return Get(x => x.FilterAlarmDefinition); }
			set { Set(value, x => x.FilterAlarmDefinition); }
		}

		/// <summary>
		/// Gets or sets the filter dynamic display.
		/// </summary>
		/// <value>
		/// The filter dynamic display.
		/// </value>
		[XmlArray("FilterDynamicDisplay")]
		[XmlArrayItem("SecurableEntity", typeof(SecurableEntity))]
		public List<SecurableEntity> FilterDynamicDisplay {
			get { return Get(x => x.FilterDynamicDisplay); }
			set { Set(value, x => x.FilterDynamicDisplay); }
		}

		/// <summary>
		/// Gets or sets the filter playlist.
		/// </summary>
		/// <value>
		/// The filter playlist.
		/// </value>
		[XmlArray("FilterPlaylist")]
		[XmlArrayItem("SecurableEntity", typeof(SecurableEntity))]
		public List<SecurableEntity> FilterPlaylist {
			get { return Get(x => x.FilterPlaylist); }
			set { Set(value, x => x.FilterPlaylist); }
		}

		/// <summary>
		/// Gets or sets the filter chart scheduler.
		/// </summary>
		/// <value>
		/// The filter chart scheduler.
		/// </value>
		[XmlArray("FilterChartScheduler")]
		[XmlArrayItem("SecurableEntity", typeof(SecurableEntity))]
		public List<SecurableEntity> FilterChartScheduler {
			get { return Get(x => x.FilterChartScheduler); }
			set { Set(value, x => x.FilterChartScheduler); }
		}

		/// <summary>
		/// Gets or sets the filter portal template.
		/// </summary>
		/// <value>
		/// The filter portal template.
		/// </value>
		[XmlArray("FilterPortalTemplate")]
		[XmlArrayItem("SecurableEntity", typeof(SecurableEntity))]
		public List<SecurableEntity> FilterPortalTemplate {
			get { return Get(x => x.FilterPortalTemplate); }
			set { Set(value, x => x.FilterPortalTemplate); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			var entities = new List<IMappingRecordConverter>();

			if (ApplicationGroups != null) {
				ApplicationGroups.ParentLocalId = LocalId;
				entities.Add(ApplicationGroups);
			}

			if (Users != null) {
				Users.ParentLocalId = LocalId;
				entities.Add(Users);
			}

			if (Tasks != null) {
				Tasks.ParentLocalId = LocalId;
				entities.Add(Tasks);
			}

			return entities;
		}
	}
}
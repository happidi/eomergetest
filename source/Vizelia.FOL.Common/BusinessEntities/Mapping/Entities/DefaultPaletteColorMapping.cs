using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the Palette Color entity.
	/// </summary>
	[MappingSchema("PaletteColor", "Default", "1.0.0")]
	public class DefaultPaletteColorMapping : Mapping<PaletteColor> {

		/// <summary>
		/// Local id of the Palette.
		/// </summary>
		public string PaletteLocalId {
			get { return Get(e => e.KeyPalette); }
			set { Set(value, e => e.KeyPalette); }
		}

		/// <summary>
		/// Order of the Color in the Palette
		/// </summary>
		public int Order {
			get { return Get(e => e.Order); }
			set { Set(value, e => e.Order); }
		}

		/// <summary>
		/// Red color composant.
		/// </summary>
		public int ColorR {
			get { return Get(e => e.ColorR); }
			set { Set(value, e => e.ColorR); }
		}

		/// <summary>
		/// Green color composant.
		/// </summary>
		public int ColorG {
			get { return Get(e => e.ColorG); }
			set { Set(value, e => e.ColorG); }
		}

		/// <summary>
		/// Blue color composant.
		/// </summary>
		public int ColorB {
			get { return Get(e => e.ColorB); }
			set { Set(value, e => e.ColorB); }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
﻿using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the Meter entity for the RSP provider.
	/// It does not mark some fields as assigned, so on update scenario, the existing persisted values will be kept.
	/// </summary>
	[MappingSchema("Meter", "RSP", "1.0.0")]
	public class RSPMeterMapping : DefaultMeterMapping {
		/// <summary>
		/// The local ID of the location of the Meter.
		/// </summary>
		public override string LocationLocalId {
			get { return Get(e => e.KeyLocation); }
			set { Set(value, e => e.KeyLocation, false); }
		}

		/// <summary>
		/// The location typename of the Meter.
		/// </summary>
		public override HierarchySpatialTypeName LocationTypeName {
			get { return Get(e => e.LocationTypeName); }
			set { Set(value, e => e.LocationTypeName, false); }
		}

		/// <summary>
		/// The local ID of the classification of the Meter.
		/// </summary>
		public override string ClassificationItemLocalId {
			get { return Get(e => e.KeyClassificationItem); }
			set { Set(value, e => e.KeyClassificationItem, false); }
		}

	}
}
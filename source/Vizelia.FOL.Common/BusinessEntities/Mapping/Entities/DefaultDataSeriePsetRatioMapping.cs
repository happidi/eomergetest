﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a Data Serie Pset Ratio entity.
	/// </summary>
	[MappingSchema("DataSeriePsetRatio", "Default", "1.0.0")]
	public class DefaultDataSeriePsetRatioMapping : Mapping<DataSeriePsetRatio> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDataSeriePsetRatioMapping"/> class.
		/// </summary>
		public DefaultDataSeriePsetRatioMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDataSeriePsetRatioMapping"/> class.
		/// </summary>
		/// <param name="dataSeriePsetRatio">The data serie pset ratio.</param>
		public DefaultDataSeriePsetRatioMapping(DataSeriePsetRatio dataSeriePsetRatio)
			: base(dataSeriePsetRatio) {

		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the key data serie pset ratio.
		/// </summary>
		/// <value>
		/// The key data serie pset ratio.
		/// </value>
		public string KeyDataSeriePsetRatio {
			get { return Get(e => e.KeyDataSeriePsetRatio); }
			set { Set(value, e => e.KeyDataSeriePsetRatio); }
		}

		/// <summary>
		/// The key of the DataSerie this ratio is attached too.
		/// </summary>
		public string KeyDataSerie {
			get { return Get(e => e.KeyDataSerie); }
			set { Set(value, e => e.KeyDataSerie); }
		}


		/// <summary>
		/// The key of the Chart this ratio is attached too.
		/// </summary>
		public string KeyChart {
			get { return Get(e => e.KeyChart); }
			set { Set(value, e => e.KeyChart); }
		}


		/// <summary>
		/// The Name of the Pset to use to ratio.
		/// </summary>
		public string PsetName {
			get { return Get(e => e.PsetName); }
			set { Set(value, e => e.PsetName); }
		}

		/// <summary>
		/// The Name of the Attribute  to use to ratio
		/// </summary>
		public string AttributeName {
			get { return Get(e => e.AttributeName); }
			set { Set(value, e => e.AttributeName); }
		}

		/// <summary>
		/// The math operator: multiply or divide
		/// </summary>
		public ArithmeticOperator Operator {
			get { return Get(e => e.Operator); }
			set { Set(value, e => e.Operator); }
		}


		/// <summary>
		/// The group operation that applies to the pset historical data when grouped over time.
		/// </summary>
		public MathematicOperator GroupOperation {
			get { return Get(e => e.GroupOperation); }
			set { Set(value, e => e.GroupOperation); }
		}

		/// <summary>
		/// The order to apply the ratio in case there are more than one.
		/// </summary>
		public int Order {
			get { return Get(e => e.Order); }
			set { Set(value, e => e.Order); }
		}

		/// <summary>
		/// The type of the object the Pset is linked to.
		/// </summary>
		public string UsageName {
			get { return Get(e => e.UsageName); }
			set { Set(value, e => e.UsageName); }
		}

		/// <summary>
		/// True to fill pset missing values when calculating the ratio.
		/// </summary>
		public bool FillMissingValues {
			get { return Get(e => e.FillMissingValues); }
			set { Set(value, e => e.FillMissingValues); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [stop at first level encountered].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [stop at first level encountered]; otherwise, <c>false</c>.
		/// </value>
		public bool StopAtFirstLevelEncountered {
			get { return Get(e => e.StopAtFirstLevelEncountered); }
			set { Set(value, e => e.StopAtFirstLevelEncountered); }
		}
		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
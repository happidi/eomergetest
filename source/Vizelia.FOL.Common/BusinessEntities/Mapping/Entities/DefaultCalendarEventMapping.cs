﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of a Calendar Event entity.
	/// </summary>
	[MappingSchema("CalendarEvent", "Default", "1.0.0")]
	public class DefaultCalendarEventMapping : Mapping<CalendarEvent> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultCalendarEventMapping"/> class.
		/// </summary>
		public DefaultCalendarEventMapping() {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultCalendarEventMapping"/> class.
		/// </summary>
		/// <param name="calendarEvent">The calendar event.</param>
		public DefaultCalendarEventMapping(CalendarEvent calendarEvent)
			: base(calendarEvent) {
		}

		/// <summary>
		/// The external identifier of the CalendarEvent.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Gets or sets the key location.
		/// </summary>
		public string KeyLocation {
			get { return Get(e => e.KeyLocation); }
			set { Set(value, e => e.KeyLocation); }
		}

		/// <summary>
		/// Gets or sets the name of the location type.
		/// </summary>
		public HierarchySpatialTypeName LocationTypeName {
			get { return Get(e => e.LocationTypeName); }
			set { Set(value, e => e.LocationTypeName); }
		}

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		public DateTime? StartDate {
			get { return Get(e => e.StartDate); }
			set { Set(value, e => e.StartDate); }
		}

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		public DateTime? EndDate {
			get { return Get(e => e.EndDate); }
			set { Set(value, e => e.EndDate); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is all day.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is all day; otherwise, <c>false</c>.
		/// </value>
		public bool IsAllDay {
			get { return Get(e => e.IsAllDay); }
			set { Set(value, e => e.IsAllDay); }
		}

		/// <summary>
		/// The ColorId to use with the Ext.ensible.calendar
		/// </summary>
		public string ColorId {
			get { return Get(e => e.ColorId); }
			set { Set(value, e => e.ColorId); }
		}

		/// <summary>
		/// Gets or sets the time zone id.
		/// </summary>
		/// <value>
		/// The time zone id.
		/// </value>
		public string TimeZoneId {
			get { return Get(e => e.TimeZoneId); }
			set { Set(value, e => e.TimeZoneId); }
		}

		/// <summary>
		/// Gets or sets the factor.
		/// </summary>
		/// <value>
		/// The factor.
		/// </value>
		public double? Factor {
			get { return Get(e => e.Factor); }
			set { Set(value, e => e.Factor); }
		}

		/// <summary>
		/// Gets or sets the recurrence pattern.
		/// </summary>
		/// <value>
		/// The recurrence pattern.
		/// </value>
		public CalendarRecurrencePattern RecurrencePattern {
			get { return Get(e => e.RecurrencePattern); }
			set { Set(value, e => e.RecurrencePattern); }
		}

		/// <summary>
		/// Gets or sets the category.
		/// </summary>
		/// <value>
		/// The category.
		/// </value>
		public string Category {
			get { return Get(e => e.Category); }
			set { Set(value, e => e.Category); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of the Drawing Canvas Image entity.
	/// </summary>
	[MappingSchema("DrawingCanvasImage", "Default", "1.0.0")]
	public class DefaultDrawingCanvasImageMapping : Mapping<DrawingCanvasImage> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDrawingCanvasImageMapping"/> class.
		/// </summary>
		public DefaultDrawingCanvasImageMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDrawingCanvasImageMapping"/> class.
		/// </summary>
		/// <param name="drawingCanvasImage">The DrawingCanvasImage.</param>
		public DefaultDrawingCanvasImageMapping(DrawingCanvasImage drawingCanvasImage)
			: base(drawingCanvasImage) {
		}


		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the key drawing canvas.
		/// </summary>
		/// <value>
		/// The key drawing canvas.
		/// </value>
		public string KeyDrawingCanvas {
			get { return Get(e => e.KeyDrawingCanvas); }
			set { Set(value, e => e.KeyDrawingCanvas); }
		}

		/// <summary>
		/// Gets or sets the key image.
		/// </summary>
		/// <value>
		/// The key image.
		/// </value>
		public string KeyImage {
			get { return Get(e => e.KeyImage); }
			set { Set(value, e => e.KeyImage); }
		}

		/// <summary>
		/// Gets or sets the height.
		/// </summary>
		/// <value>
		/// The height.
		/// </value>
		public int Height {
			get { return Get(e => e.Height); }
			set { Set(value, e => e.Height); }
		}

		/// <summary>
		/// Gets or sets the width.
		/// </summary>
		/// <value>
		/// The width.
		/// </value>
		public int Width {
			get { return Get(e => e.Width); }
			set { Set(value, e => e.Width); }
		}

		/// <summary>
		/// Gets or sets the X.
		/// </summary>
		/// <value>
		/// The X.
		/// </value>
		public int X {
			get { return Get(e => e.X); }
			set { Set(value, e => e.X); }
		}

		/// <summary>
		/// Gets or sets the Y.
		/// </summary>
		/// <value>
		/// The Y.
		/// </value>
		public int Y {
			get { return Get(e => e.Y); }
			set { Set(value, e => e.Y); }
		}

		/// <summary>
		/// Gets or sets the index of the Z.
		/// </summary>
		/// <value>
		/// The index of the Z.
		/// </value>
		public int ZIndex {
			get { return Get(e => e.ZIndex); }
			set { Set(value, e => e.ZIndex); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
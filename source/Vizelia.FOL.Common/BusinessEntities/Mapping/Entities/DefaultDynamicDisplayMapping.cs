using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a Dynamic Display entity.
	/// </summary>
	[MappingSchema("DynamicDisplay", "Default", "1.0.0")]
	public class DefaultDynamicDisplayMapping : Mapping<DynamicDisplay> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDynamicDisplayMapping"/> class.
		/// </summary>
		public DefaultDynamicDisplayMapping() {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDynamicDisplayMapping"/> class.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public DefaultDynamicDisplayMapping(DynamicDisplay entity) : base(entity) {
			// Do nothing.
		}

		/// <summary>
		/// Name of the Dynamic Display.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Local id of the Dynamic Display.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Type of the Dynamic Display.
		/// </summary>
		public DynamicDisplayType Type {
			get { return Get(e => e.Type); }
			set { Set(value, e => e.Type); }
		}

		/// <summary>
		/// Color template of the Dynamic Display.
		/// </summary>
		public string ColorTemplate {
			get { return Get(e => e.ColorTemplate); }
			set { Set(value, e => e.ColorTemplate); }
		}

		/// <summary>
		/// Local id of the custom Color template of the Dynamic Display. Used only if ColorTemplate is set to Custom.
		/// </summary>
		public string CustomColorTemplateLocalId {
			get { return Get(e => e.KeyCustomColorTemplate); }
			set { Set(value, e => e.KeyCustomColorTemplate); }
		}

		/// <summary>
		/// True to display a Header. False to hide it.
		/// </summary>
		public bool HeaderVisible {
			get { return Get(e => e.HeaderVisible); }
			set { Set(value, e => e.HeaderVisible); }
		}

		/// <summary>
		/// Picture to display in the Header.
		/// </summary>
		public string HeaderPicture {
			get { return Get(e => e.HeaderPicture); }
			set { Set(value, e => e.HeaderPicture); }
		}

		/// <summary>
		/// Text to display in the Header.
		/// </summary>
		public string HeaderText {
			get { return Get(e => e.HeaderText); }
			set { Set(value, e => e.HeaderText); }
		}

		/// <summary>
		/// True to display the Chart that it is associated with. False to hide it.
		/// </summary>
		public bool MainChartVisible {
			get { return Get(e => e.MainChartVisible); }
			set { Set(value, e => e.MainChartVisible); }
		}

		/// <summary>
		/// Picture to display in the Main area.
		/// </summary>
		public string MainPicture {
			get { return Get(e => e.MainPicture); }
			set { Set(value, e => e.MainPicture); }
		}

		/// <summary>
		/// Size of the picture to display in the Main area.
		/// </summary>
		public int? MainPictureSize {
			get { return Get(e => e.MainPictureSize); }
			set { Set(value, e => e.MainPictureSize); }
		}

		/// <summary>
		/// Text to display in the Main area.
		/// </summary>
		public string MainText {
			get { return Get(e => e.MainText); }
			set { Set(value, e => e.MainText); }
		}

		/// <summary>
		/// True to display the Footer, False to hide it.
		/// </summary>
		public bool FooterVisible {
			get { return Get(e => e.FooterVisible); }
			set { Set(value, e => e.FooterVisible); }
		}

		/// <summary>
		/// Picture to display in the Footer area.
		/// </summary>
		public string FooterPicture {
			get { return Get(e => e.FooterPicture); }
			set { Set(value, e => e.FooterPicture); }
		}

		/// <summary>
		/// Text to display in the Footer area.
		/// </summary>
		public string FooterText {
			get { return Get(e => e.FooterText); }
			set { Set(value, e => e.FooterText); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is custom color template.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is custom color template; otherwise, <c>false</c>.
		/// </value>
		public bool IsCustomColorTemplate {
			get { return Get(e => e.IsCustomColorTemplate); }
			set { Set(value, e => e.IsCustomColorTemplate); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}

	}
}
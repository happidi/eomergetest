using System;

namespace Vizelia.FOL.BusinessEntities.Mapping
{
	/// <summary>
	/// A deserialized mappable entity interface.
	/// </summary>
	public interface IDeserializedMappableEntity {
		/// <summary>
		/// Gets the local id.
		/// </summary>
		string LocalId { get; }

		/// <summary>
		/// Gets the type of the business entity.
		/// </summary>
		/// <value>
		/// The type of the business entity.
		/// </value>
		Type BusinessEntityType { get; }

		/// <summary>
		/// Gets the entity.
		/// </summary>
		IMappableEntity Entity { get; }

		/// <summary>
		/// Determines whether the given field has been assigned with a value or not.
		/// </summary>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns>
		///   <c>true</c> if the given field has been assigned with a value; otherwise, <c>false</c>.
		/// </returns>
		bool IsFieldAssigned(string fieldName);
	}
}
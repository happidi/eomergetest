using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a ChartPsetAttributeHistorical entity.
	/// </summary>
	[MappingSchema("ChartPsetAttributeHistorical", "Default", "1.0.0")]
	public class DefaultChartPsetAttributeHistoricalMapping : Mapping<ChartPsetAttributeHistorical> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultChartPsetAttributeHistoricalMapping"/> class.
		/// </summary>
		public DefaultChartPsetAttributeHistoricalMapping() {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultChartPsetAttributeHistoricalMapping"/> class.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public DefaultChartPsetAttributeHistoricalMapping(ChartPsetAttributeHistorical entity) : base(entity) {
			// Do nothing.
		}

		/// <summary>
		/// Gets or sets the key chart pset attribute historical.
		/// </summary>
		/// <value>
		/// The key chart pset attribute historical.
		/// </value>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the chart local id.
		/// </summary>
		/// <value>
		/// The key chart.
		/// </value>
		public string ChartLocalId {
			get { return Get(e => e.KeyChart); }
			set { Set(value, e => e.KeyChart); }
		}

		/// <summary>
		/// The Name of the Pset to use to filter.
		/// </summary>
		public string PsetName {
			get { return Get(e => e.PsetName); }
			set { Set(value, e => e.PsetName); }
		}

		/// <summary>
		/// The Name of the Attribute  to use to filter
		/// </summary>
		public string AttributeName {
			get { return Get(e => e.AttributeName); }
			set { Set(value, e => e.AttributeName); }
		}

		/// <summary>
		/// Gets or sets the name of the localized label of the Attribute.
		/// </summary>
		public string AttributeMsgCode {
			get { return Get(e => e.AttributeMsgCode); }
			set { Set(value, e => e.AttributeMsgCode); }
		}

		/// <summary>
		/// Gets or sets the name of the localized label of the Attribute.
		/// </summary>
		public string AttributeLabel {
			get { return Get(e => e.AttributeLabel); }
			set { Set(value, e => e.AttributeLabel); }
		}

		/// <summary>
		/// The  PropertySingle long path (used in the treecombo).
		/// </summary>
		public string PropertySingleValueLongPath {
			get { return Get(e => e.PropertySingleValueLongPath); }
			set { Set(value, e => e.PropertySingleValueLongPath); }
		}

		/// <summary>
		/// The group operation that applies to the data when grouped over time or spatial hierarchy.
		/// </summary>
		public MathematicOperator GroupOperation {
			get { return Get(e => e.GroupOperation); }
			set { Set(value, e => e.GroupOperation); }
		}

		/// <summary>
		/// The Key of the PropertySingleValue local id the attribute is attached to.
		/// </summary>
		public string PropertySingleValueLocalId {
			get { return Get(e => e.KeyPropertySingleValue); }
			set { Set(value, e => e.KeyPropertySingleValue); }
		}
		
		/// <summary>
		/// Gets or sets a value indicating whether we [fill missing values].
		/// </summary>
		/// <value>
		///   <c>true</c> if [fill missing values]; otherwise, <c>false</c>.
		/// </value>
		public bool FillMissingValues {
			get { return Get(e => e.FillMissingValues); }
			set { Set(value, e => e.FillMissingValues); }
		}
	
		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
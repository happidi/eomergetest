using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the StatisticalSerie entity.
	/// </summary>
	[MappingSchema("StatisticalSerie", "Default", "1.0.0")]
	public class DefaultStatisticalSerieMapping : Mapping<StatisticalSerie> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultStatisticalSerieMapping"/> class.
		/// </summary>
		public DefaultStatisticalSerieMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultStatisticalSerieMapping"/> class.
		/// </summary>
		/// <param name="serie">The serie.</param>
		public DefaultStatisticalSerieMapping(StatisticalSerie serie)
			: base(serie) {

		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Local id of the Chart.
		/// </summary>
		public string ChartLocalId {
			get { return Get(e => e.KeyChart); }
			set { Set(value, e => e.KeyChart); }
		}

		/// <summary>
		/// Gets or sets the operator.
		/// </summary>
		/// <value>
		/// The operator.
		/// </value>
		public MathematicOperator Operator {
			get { return Get(e => e.Operator); }
			set { Set(value, e => e.Operator); }
		}


		/// <summary>
		/// Local id of ClassificationItem 
		/// </summary>
		public string ClassificationItemLocalId {
			get { return Get(e => e.KeyClassificationItem); }
			set { Set(value, e => e.KeyClassificationItem); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether  we use hidden series for statistical calculation.
		/// </summary>
		public bool IncludeHiddenSeries {
			get { return Get(e => e.IncludeHiddenSeries); }
			set { Set(value, e => e.IncludeHiddenSeries); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether  we run the statistical calculation before or after CalculatedSeries.
		/// </summary>
		public bool RunAfterCalculatedSeries {
			get { return Get(e => e.RunAfterCalculatedSeries); }
			set { Set(value, e => e.RunAfterCalculatedSeries); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
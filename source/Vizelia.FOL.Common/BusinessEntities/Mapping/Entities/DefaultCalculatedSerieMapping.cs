using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the CalculatedSerie entity.
	/// </summary>
	[MappingSchema("CalculatedSerie", "Default", "1.0.0")]
	public class DefaultCalculatedSerieMapping : Mapping<CalculatedSerie> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultCalculatedSerieMapping"/> class.
		/// </summary>
		public DefaultCalculatedSerieMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultCalculatedSerieMapping"/> class.
		/// </summary>
		/// <param name="serie">The serie.</param>
		public DefaultCalculatedSerieMapping(CalculatedSerie serie)
			: base(serie) {

		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Local id of the Chart.
		/// </summary>
		public string ChartLocalId {
			get { return Get(e => e.KeyChart); }
			set { Set(value, e => e.KeyChart); }
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Gets or sets the order of the calculated serie.
		/// </summary>
		public int Order {
			get { return Get(e => e.Order); }
			set { Set(value, e => e.Order); }
		}

		/// <summary>
		/// Gets or sets the formula of the calculated Serie.
		/// </summary>
		public string Formula {
			get { return Get(e => e.Formula); }
			set { Set(value, e => e.Formula); }
		}

		/// <summary>
		/// Gets or sets the Unit 
		/// </summary>
		public string Unit {
			get { return Get(e => e.Unit); }
			set { Set(value, e => e.Unit); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether the series used in the calculation will be hidden.
		/// </summary>
		/// <value>
		///   if <c>true</c> hides the used series; otherwise, <c>false</c>.
		/// </value>
		public bool HideUsedSeries {
			get { return Get(e => e.HideUsedSeries); }
			set { Set(value, e => e.HideUsedSeries); }
		}

		/// <summary>
		/// Gets or sets the Key of the LocalizationCulture.
		/// </summary>
		public string KeyLocalizationCulture {
			get { return Get(e => e.KeyLocalizationCulture); }
			set { Set(value, e => e.KeyLocalizationCulture); }
		}

		/// <summary>
		/// The key of the classification of the KPI Definition.
		/// </summary>
		public string KeyClassificationItemLocalId {
			get { return Get(e => e.KeyClassificationItem); }
			set { Set(value, e => e.KeyClassificationItem); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
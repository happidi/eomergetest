﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the Classification Item entity.
	/// </summary>
	[MappingSchema("ClassificationItem", "Default", "1.0.0")]
	public class DefaultClassificationItemMapping : Mapping<ClassificationItem> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultClassificationItemMapping"/> class.
		/// </summary>
		public DefaultClassificationItemMapping()
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultClassificationItemMapping"/> class.
		/// </summary>
		/// <param name="classificationItem">The classification item.</param>
		public DefaultClassificationItemMapping(ClassificationItem classificationItem):base(classificationItem)
		{
			
		}
		/// <summary>
		/// The external identifier of the ClassificationItem.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The Title.
		/// </summary>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// The Code.
		/// </summary>
		public string Code {
			get { return Get(e => e.Code); }
			set { Set(value, e => e.Code); }
		}

		/// <summary>
		/// The parent local id.
		/// </summary>
		public string ParentLocalId {
			get { return Get(e => e.KeyParent); }
			set { Set(value, e => e.KeyParent); }
		}

		/// <summary>
		/// Red color composant.
		/// </summary>
		public int ColorR {
			get { return Get(e => e.ColorR); }
			set { Set(value, e => e.ColorR); }
		}

		/// <summary>
		/// Green color composant.
		/// </summary>
		public int ColorG {
			get { return Get(e => e.ColorG); }
			set { Set(value, e => e.ColorG); }
		}

		/// <summary>
		/// Blue color composant.
		/// </summary>
		public int ColorB {
			get { return Get(e => e.ColorB); }
			set { Set(value, e => e.ColorB); }
		}

		/// <summary>
		/// Gets or sets the Category.
		/// </summary>
		/// <value>The Category.</value>
		public string Category {
			get { return Get(e => e.Category); }
			set { Set(value, e => e.Category); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
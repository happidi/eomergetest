﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of the Drawing Canvas entity.
	/// </summary>
	[MappingSchema("DrawingCanvas", "Default", "1.0.0")]
	public class DefaultDrawingCanvasMapping : Mapping<DrawingCanvas> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDrawingCanvasMapping"/> class.
		/// </summary>
		public DefaultDrawingCanvasMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDrawingCanvasMapping"/> class.
		/// </summary>
		/// <param name="drawingCanvas">The DrawingCanvas.</param>
		public DefaultDrawingCanvasMapping(DrawingCanvas drawingCanvas)
			: base(drawingCanvas) {
		}


		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the title of the DrawingCanvas.
		/// </summary>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		public bool IsFavorite {
			get { return Get(e => e.IsFavorite); }
			set { Set(value, e => e.IsFavorite); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		public bool OpenOnStartup {
			get { return Get(e => e.OpenOnStartup); }
			set { Set(value, e => e.OpenOnStartup); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [display sliders].
		/// </summary>
		/// <value>
		///   <c>true</c> if [display sliders]; otherwise, <c>false</c>.
		/// </value>
		public bool DisplaySliders {
			get { return Get(e => e.DisplaySliders); }
			set { Set(value, e => e.DisplaySliders); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
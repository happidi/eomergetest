using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using Vizelia.FOL.Common;


namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping record.
	/// </summary>
	/// <typeparam name="TEntity">The type of the entity.</typeparam>
	public abstract class MappingRecord<TEntity> : IMappingRecord where TEntity : IMappableEntity {

		/// <summary>
		/// Gets the entity.
		/// </summary>
		public TEntity Entity { get; private set; }

		/// <summary>
		/// Gets the entity.
		/// </summary>
		IMappableEntity IMappingRecord.Entity {
			get { return Entity; }
		}

		/// <summary>
		/// Gets the local id.
		/// </summary>
		public string LocalId {
			get { return Entity.LocalId; }
		}

		/// <summary>
		/// Gets the type of the business entity.
		/// </summary>
		/// <value>
		/// The type of the business entity.
		/// </value>
		public Type BusinessEntityType {
			get { return typeof(TEntity); }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MappingRecord&lt;TEntity&gt;"/> class.
		/// </summary>
		/// <param name="entity">The entity.</param>
		protected MappingRecord(TEntity entity) {
			Entity = entity;
			AdditionalFields = new Dictionary<string, string>();
		}

		/// <summary>
		/// Determines whether the given property has been assigned with a value or not.
		/// </summary>
		/// <param name="propertyName">Name of the field.</param>
		/// <returns>
		///   <c>true</c> if the given field has been assigned with a value; otherwise, <c>false</c>.
		/// </returns>
		public abstract bool IsPropertyAssigned(string propertyName);

		/// <summary>
		/// Gets or sets the additional fields of the mapping record.
		/// These fields represent fields that exist on the mapping class, but not on the entity itself.
		/// They can be used for resolving fields on the entity.
		/// </summary>
		/// <value>
		/// The additional fields.
		/// </value>
		public Dictionary<string, string> AdditionalFields { get; set; }

		/// <summary>
		/// Determines whether the given property has been assigned with a value or not.
		/// </summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="propertyNameIndicatingExpression">The expression indicating the property name.</param>
		/// <returns>
		///   <c>true</c> if the given field has been assigned with a value; otherwise, <c>false</c>.
		/// </returns>
		public bool IsPropertyAssigned<TValue>(Expression<Func<TEntity, TValue>> propertyNameIndicatingExpression) {
			MemberInfo member = Helper.GetMemberFromExpression(propertyNameIndicatingExpression);
			bool isFieldAssigned = IsPropertyAssigned(member.Name);

			return isFieldAssigned;
		}

		/// <summary>
		/// Sets the property value and marks it as assigned.
		/// </summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="propertyNameIndicatingExpression">The property name indicating expression.</param>
		/// <param name="value">The value.</param>
		public bool SetPropertyValue<TValue>(Expression<Func<TEntity, TValue>> propertyNameIndicatingExpression, TValue value) {
			MemberInfo member = Helper.GetMemberFromExpression(propertyNameIndicatingExpression);
			bool isFieldAssigned = IsPropertyAssigned(member.Name);
			MarkPropertyAsAssigned(member.Name);
			member.SetValue(Entity, value);

			return isFieldAssigned;
		}

		/// <summary>
		/// Marks the property as assigned.
		/// </summary>
		/// <param name="name">The property name.</param>
		protected abstract void MarkPropertyAsAssigned(string name);
	}
}
﻿using System.Collections.Generic;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A deserialized mapping record.
	/// </summary>
	public class DeserializedMappingRecord<TEntity> : MappingRecord<TEntity> where TEntity : IMappableEntity {

		private readonly List<string> m_AssignedFields;
		private readonly bool m_AllFieldsAssigned;

		/// <summary>
		/// Initializes a new instance of the <see cref="DeserializedMappingRecord&lt;TEntity&gt;"/> class.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="assignedFields">The assigned fields.</param>
		/// <param name="allFieldsAssigned">if set to <c>true</c> [all fields assigned].</param>
		public DeserializedMappingRecord(TEntity entity, List<string> assignedFields, bool allFieldsAssigned)
			: base(entity) {
			m_AllFieldsAssigned = allFieldsAssigned;
			m_AssignedFields = assignedFields;
		}

		/// <summary>
		/// Determines whether the given field has been assigned with a value or not.
		/// </summary>
		/// <param name="propertyName">Name of the field.</param>
		/// <returns>
		///   <c>true</c> if the given field has been assigned with a value; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsPropertyAssigned(string propertyName) {
			bool isAssigned = m_AllFieldsAssigned || m_AssignedFields.Contains(propertyName);
			return isAssigned;
		}

		/// <summary>
		/// Marks the property as assigned.
		/// </summary>
		/// <param name="name">The property name.</param>
		protected override void MarkPropertyAsAssigned(string name) {
			if (!IsPropertyAssigned(name)) {
				m_AssignedFields.Add(name);
			}
		}
	}
}
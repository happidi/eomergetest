﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping.Entities {
	/// <summary>
	/// A mapping for the StruxureWareDataAcquisitionContainer entity.
	/// </summary>
	[MappingSchema("StruxureWareDataAcquisitionContainer", "Default", "1.0.0")]
	public class DefaultStruxureWareDataAcquisitionContainerMapping : Mapping<StruxureWareDataAcquisitionContainer> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultStruxureWareDataAcquisitionContainerMapping"/> class.
		/// </summary>
		public DefaultStruxureWareDataAcquisitionContainerMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultStruxureWareDataAcquisitionContainerMapping"/> class.
		/// </summary>
		/// <param name="struxureWareDataAcquisitionContainer">The struxure ware data acquisition container.</param>
		public DefaultStruxureWareDataAcquisitionContainerMapping(StruxureWareDataAcquisitionContainer struxureWareDataAcquisitionContainer)
			: base(struxureWareDataAcquisitionContainer) {

		}
		
		/// <summary>
		/// Gets or sets the key data acquisition container.
		/// </summary>
		/// <value>
		/// The key data acquisition container.
		/// </value>
		public string KeyDataAcquisitionContainer {
			get { return Get(e => e.KeyDataAcquisitionContainer); }
			set { Set(value, e => e.KeyDataAcquisitionContainer); }
		}


		/// <summary>
		/// The external identifier..
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}


		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Gets the fully qualified name of the provider.
		/// </summary>
		/// <returns></returns>
		public string ProviderType {
			get { return Get(e => e.ProviderType); }
			set { Set(value, e => e.ProviderType); }
		}

		/// <summary>
		/// Gets or sets the URL.
		/// </summary>
		/// <value>
		/// The URL.
		/// </value>
		public string Url {
			get { return Get(e => e.Url); }
			set { Set(value, e => e.Url); }
		}

		/// <summary>
		/// Gets or sets the username.
		/// </summary>
		/// <value>
		/// The username.
		/// </value>
		public string Username {
			get { return Get(e => e.Username); }
			set { Set(value, e => e.Username); }
		}

		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>
		/// The password.
		/// </value>
		public string Password {
			get { return Get(e => e.Password); }
			set { Set(value, e => e.Password); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}

	}
}

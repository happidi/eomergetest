﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping for the Building entity.
	/// </summary>
	[MappingSchema("Building", "Default", "1.0.0")]
	public class DefaultBuildingMapping : PsetEnabledMapping<Building> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultBuildingMapping"/> class.
		/// </summary>
		public DefaultBuildingMapping()
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultBuildingMapping"/> class.
		/// </summary>
		/// <param name="building">The building.</param>
		public DefaultBuildingMapping(Building building):base(building)
		{
			
		}
		/// <summary>
		/// The external identifier of the Building.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The Name of the Building.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// The Description of the Building.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// The site local id.
		/// </summary>
		/// <value>
		/// The site local id.
		/// </value>
		public string SiteLocalId {
			get { return Get(e => e.KeySite); }
			set { Set(value, e => e.KeySite); }
		}

		/// <summary>
		/// The ObjectType of the Building.
		/// </summary>
		public string ObjectType {
			get { return Get(e => e.ObjectType); }
			set { Set(value, e => e.ObjectType); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}

	}

}
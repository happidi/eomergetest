﻿namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A Crud store used within the mapping process.
	/// </summary>
	/// <typeparam name="TParent">The type of the parent.</typeparam>
	/// <typeparam name="TChildren">The type of the children.</typeparam>
	public class MappingCrudStore<TParent, TChildren> : CrudStore<TChildren>, IMappableEntity 
		where TParent : BaseBusinessEntity, new()
		where TChildren : BaseBusinessEntity {
		
		/// <summary>
		/// Gets or sets the name of the property of the object in which this is an instance.
		/// </summary>
		/// <value>
		/// The name of the property.
		/// </value>
		public string PropertyName { get; set; }

		/// <summary>
		/// Gets or sets the parent local id.
		/// </summary>
		public string ParentLocalId { get; set; }

		/// <summary>
		/// Gets the parent entity.
		/// </summary>
		/// <returns></returns>
		public TParent GetParentEntity() {
			var parent = new TParent();
			parent.SetKey(ParentLocalId);
			return parent;
		}

		/// <summary>
		/// Gets or sets the local id.
		/// Dummy property for mapping support.
		/// </summary>
		public string LocalId {
			get { return null; }
			set {  }
		}
	}
}
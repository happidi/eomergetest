﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;

namespace Vizelia.FOL.Common.BusinessEntities.Mappings {
	/// <summary>
	/// A mapping for the LocalizationResource entity.
	/// </summary>
	[MappingSchema("LocalizationResource", "Default", "1.0.0")]
	public class DefaultLocalizationResourceMapping : Mapping<LocalizationResource> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultLocalizationResourceMapping"/> class.
		/// </summary>
		public DefaultLocalizationResourceMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultLocalizationResourceMapping"/> class.
		/// </summary>
		/// <param name="localizationResource">The localization resource.</param>
		public DefaultLocalizationResourceMapping(LocalizationResource localizationResource)
			: base(localizationResource) {

		}

		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>
		/// The id.
		/// </value>
		public string Id {
			get { return Get(e => e.Id); }
			set { Set(value, e => e.Id); }
		}

		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>
		/// The key.
		/// </value>
		public string Key {
			get { return Get(e => e.Key); }
			set { Set(value, e => e.Key); }
		}

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		public string Value {
			get { return Get(e => e.Value); }
			set { Set(value, e => e.Value); }
		}

		/// <summary>
		/// Gets or sets the culture.
		/// </summary>
		/// <value>
		/// The culture.
		/// </value>
		public string Culture {
			get { return Get(e => e.Culture); }
			set { Set(value, e => e.Culture); }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}

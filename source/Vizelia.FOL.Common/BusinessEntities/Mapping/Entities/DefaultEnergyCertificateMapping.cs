﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of a EnergyCertificate entity.
	/// </summary>
	[MappingSchema("EnergyCertificate", "Default", "1.0.0")]
	public class DefaultEnergyCertificateMapping : Mapping<EnergyCertificate> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultEnergyCertificateMapping"/> class.
		/// </summary>
		public DefaultEnergyCertificateMapping() {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultEnergyCertificateMapping"/> class.
		/// </summary>
		/// <param name="energyCertificate">The energy certificate.</param>
		public DefaultEnergyCertificateMapping(EnergyCertificate energyCertificate)
			: base(energyCertificate) {

				if (energyCertificate.Categories != null) {
					Categories = new List<DefaultEnergyCertificateCategoryMapping>();
					Categories.AddRange(energyCertificate.Categories.Select(d => new DefaultEnergyCertificateCategoryMapping(d) { EnergyCertificateLocalId = LocalId }));
			}
		}

		/// <summary>
		/// Key of the EnergyCertificate.
		/// </summary>
		public string KeyEnergyCertificate {
			get { return Get(e => e.KeyEnergyCertificate); }
			set { Set(value, e => e.KeyEnergyCertificate); }
		}

		/// <summary>
		/// Gets or sets the meter local id.
		/// </summary>
		/// <value>
		/// The meter local id.
		/// </value>
		public string EnergyCertificateLocalId {
			get { return Get(e => e.KeyEnergyCertificate); }
			set { Set(value, e => e.KeyEnergyCertificate); }
		}

		/// <summary>
		/// Gets or sets the Name.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Gets or sets the StartColor.
		/// </summary>
		public string StartColor {
			get { return Get(e => e.StartColor); }
			set { Set(value, e => e.StartColor); }
		}

		/// <summary>
		/// Gets or sets the EndColor.
		/// </summary>
		public string EndColor {
			get { return Get(e => e.EndColor); }
			set { Set(value, e => e.EndColor); }
		}


		// public DefaultMappingCrudStoreMapping<EnergyCertificate, EnergyCertificateCategory> Categories { get; set; }

		/// <summary>
		/// Gets or sets the categories.
		/// </summary>
		/// <value>
		/// The categories.
		/// </value>
		[XmlArray("Categories")]
		[XmlArrayItem("EnergyCertificateCategory", typeof(DefaultEnergyCertificateCategoryMapping))]
		public List<DefaultEnergyCertificateCategoryMapping> Categories { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		public string Creator {
			get { return Get(e => e.Creator); }
			set { Set(value, e => e.Creator); }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Befores the get business entity.
		/// </summary>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			if (Categories != null) {
				List<EnergyCertificateCategory> energyCertificateCategories = Categories.Select(energyCertificateMapping => energyCertificateMapping.GetBusinessEntity()).ToList();

				foreach (var category in energyCertificateCategories) {
					category.KeyEnergyCertificate = LocalId;
				}

				Set(energyCertificateCategories, e => e.Categories);
			}
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			if (Categories == null) {
				return Enumerable.Empty<IMappingRecordConverter>();
			}

			foreach (var category in Categories) {
				category.EnergyCertificateLocalId = LocalId;
			}

			return Categories;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the AlarmDefinition entity.
	/// </summary>
	[MappingSchema("AlarmDefinition", "Default", "1.0.0")]
	public class DefaultAlarmDefinitionMapping : Mapping<AlarmDefinition> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultAlarmDefinitionMapping"/> class.
		/// </summary>
		public DefaultAlarmDefinitionMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultAlarmDefinitionMapping"/> class.
		/// </summary>
		/// <param name="alarmDefinition">The alarm definition.</param>
		public DefaultAlarmDefinitionMapping(AlarmDefinition alarmDefinition)
			: base(alarmDefinition) {

			// Many to many
			if (alarmDefinition.FilterCharts != null) {
				var chartsLocalIds = alarmDefinition.FilterCharts.Select(x => x.LocalId).ToList();
				Charts = new DefaultMappingCrudStoreMapping<AlarmDefinition, Chart>(chartsLocalIds, LocalId);
			}

			if (alarmDefinition.Meters != null) {
				var meterLocalIds = alarmDefinition.Meters.Select(m => m.LocalId).ToList();
				Meters = new DefaultMappingCrudStoreMapping<AlarmDefinition, Meter>(meterLocalIds, LocalId);
			}

			if (alarmDefinition.FilterMeterClassifications != null) {
				List<string> classificationItemsLocalIds = alarmDefinition.FilterMeterClassifications.Select(c => c.LocalId).ToList();
				FilterMeterClassification = new DefaultMappingCrudStoreMapping<AlarmDefinition, ClassificationItem>(classificationItemsLocalIds, LocalId);
			}

			if (alarmDefinition.FilterSpatial != null) {
				FilterSpatial = new DefaultLocationMappingCrudStoreMapping<AlarmDefinition>(alarmDefinition.FilterSpatial, LocalId);
			}
		}

		/// <summary>
		/// Key of the AlarmDefinition.
		/// </summary>
		public string KeyAlarmDefinition {
			get { return Get(e => e.KeyAlarmDefinition); }
			set { Set(value, e => e.KeyAlarmDefinition); }
		}

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// The key of the classification of the Alarm Definition.
		/// </summary>
		public string KeyClassificationItem {
			get { return Get(e => e.KeyClassificationItem); }
			set { Set(value, e => e.KeyClassificationItem); }
		}

		/// <summary>
		/// The Key of the location of the Meter.
		/// </summary>
		public string KeyLocation {
			get { return Get(e => e.KeyLocation); }
			set { Set(value, e => e.KeyLocation); }
		}

		/// <summary>
		/// The location typename of the Meter.
		/// </summary>
		public HierarchySpatialTypeName LocationTypeName {
			get { return Get(e => e.LocationTypeName); }
			set { Set(value, e => e.LocationTypeName); }
		}

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		public DateTime? StartDate {
			get { return Get(e => e.StartDate); }
			set { Set(value, e => e.StartDate); }
		}

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		public DateTime? EndDate {
			get { return Get(e => e.EndDate); }
			set { Set(value, e => e.EndDate); }
		}

		/// <summary>
		/// Gets or sets the Key of the CalendarEventCategory associated to this Chart.
		/// </summary>
		public string KeyCalendarEventCategory {
			get { return Get(e => e.KeyCalendarEventCategory); }
			set { Set(value, e => e.KeyCalendarEventCategory); }
		}

		/// <summary>
		/// Gets or sets the CalendarEventCategory associated to this Chart.
		/// </summary>
		public CalendarEventCategory CalendarEventCategory {
			get { return Get(e => e.CalendarEventCategory); }
			set { Set(value, e => e.CalendarEventCategory); }
		}

		/// <summary>
		/// Calendar mode : Include or Exclude.
		/// </summary>
		public ChartCalendarMode CalendarMode {
			get { return Get(e => e.CalendarMode); }
			set { Set(value, e => e.CalendarMode); }
		}

		/// <summary>
		/// Gets or sets the Meter grouping Timeinterval when processing the AlarmDefinition.
		/// </summary>
		public AxisTimeInterval TimeInterval {
			get { return Get(e => e.TimeInterval); }
			set { Set(value, e => e.TimeInterval); }
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		public string Creator {
			get { return Get(e => e.Creator); }
			set { Set(value, e => e.Creator); }
		}

		/// <summary>
		/// <c>true</c> if enabled; otherwise, <c>false</c>.
		/// </summary>
		public bool Enabled {
			get { return Get(e => e.Enabled); }
			set { Set(value, e => e.Enabled); }
		}

		/// <summary>
		/// The Threshold
		/// </summary>
		public double? Threshold {
			get { return Get(e => e.Threshold); }
			set { Set(value, e => e.Threshold); }
		}

		/// <summary>
		/// The Threshold high value (in case of a Between condition).
		/// </summary>
		public double? Threshold2 {
			get { return Get(e => e.Threshold2); }
			set { Set(value, e => e.Threshold2); }
		}

		/// <summary>
		/// the filtering condition to use : Equals, Higher, Lower, Between, Outside
		/// </summary>
		public FilterCondition Condition {
			get { return Get(e => e.Condition); }
			set { Set(value, e => e.Condition); }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// <c>true</c> if true; we calculate the delta beetween the current value and the previous one in %, else we apply the condition to the current value.
		/// </summary>
		public bool UsePercentageDelta {
			get { return Get(e => e.UsePercentageDelta); }
			set { Set(value, e => e.UsePercentageDelta); }
		}

		/// <summary>
		/// List of emails to send notification when no ActionRequest as been defined. 
		/// </summary>
		public string Emails {
			get { return Get(e => e.Emails); }
			set { Set(value, e => e.Emails); }
		}

		/// <summary>
		/// Gets or sets the charts.
		/// This is a many-to-many mapping.
		/// </summary>
		public DefaultMappingCrudStoreMapping<AlarmDefinition, Chart> Charts { get; set; }

		/// <summary>
		/// Gets or sets the filter meter classification.
		/// This is a many-to-many mapping.
		/// </summary>
		public DefaultMappingCrudStoreMapping<AlarmDefinition, ClassificationItem> FilterMeterClassification { get; set; }

		/// <summary>
		/// Gets or sets the filter spatial.
		/// This is a many-to-many mapping.
		/// </summary>
		public DefaultLocationMappingCrudStoreMapping<AlarmDefinition> FilterSpatial { get; set; }

		/// <summary>
		/// Gets or sets the meters.
		/// This is a many-to-many mapping.
		/// </summary>
		public DefaultMappingCrudStoreMapping<AlarmDefinition, Meter> Meters { get; set; }

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			var entities = new List<IMappingRecordConverter>();

			if (Charts != null) {
				Charts.ParentLocalId = LocalId;
				entities.Add(Charts);
			}

			if (FilterMeterClassification != null) {
				FilterMeterClassification.ParentLocalId = LocalId;
				FilterMeterClassification.PropertyName = "FilterMeterClassification";
				entities.Add(FilterMeterClassification);
			}

			if (FilterSpatial != null) {
				FilterSpatial.ParentLocalId = LocalId;
				entities.Add(FilterSpatial);
			}

			if (Meters != null) {
				Meters.ParentLocalId = LocalId;
				entities.Add(Meters);
			}

			return entities;
		}
	}
}

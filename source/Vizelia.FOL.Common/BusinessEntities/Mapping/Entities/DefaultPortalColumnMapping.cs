﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a Portal Column entity.
	/// </summary>
	[MappingSchema("PortalColumn", "Default", "1.0.0")]
	public class DefaultPortalColumnMapping : Mapping<PortalColumn> {

				/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPortalColumnMapping"/> class.
		/// </summary>
		public DefaultPortalColumnMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPortalColumnMapping"/> class.
		/// </summary>
		/// <param name="portalColumn">The PortalColumn.</param>
		public DefaultPortalColumnMapping(PortalColumn portalColumn)
			: base(portalColumn) {
		}

		/// <summary>
		/// The local id of the Portal Column.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Local id of the Portal Tab.
		/// </summary>
		public string PortalTabLocalId {
			get { return Get(e => e.KeyPortalTab); }
			set { Set(value, e => e.KeyPortalTab); }
		}

		/// <summary>
		/// Gets or sets the relative width of the column.
		/// </summary>
		public int Flex {
			get { return Get(e => e.Flex); }
			set { Set(value, e => e.Flex); }
		}

		/// <summary>
		/// Gets or sets the order of the portal column in the portal tab.
		/// </summary>
		public int Order {
			get { return Get(e => e.Order); }
			set { Set(value, e => e.Order); }
		}

		/// <summary>
		/// Gets or sets the type.
		/// </summary>
		/// <value>
		/// The type.
		/// </value>
		public PortalColumnType Type {
			get { return Get(e => e.Type); }
			set { Set(value, e => e.Type); }
		}

		/// <summary>
		/// Gets or sets the list of portlets of the portal column.
		/// </summary>
		[XmlArray("Portlets")]
		[XmlArrayItem("Portlet", typeof(DefaultPortletMapping))]
		public List<DefaultPortletMapping> Portlets { get; set; }

		/// <summary>
		/// Populates properties before the business entity is outputted.
		/// </summary>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			List<Portlet> portlets = Portlets.Select(p => p.GetBusinessEntity()).ToList();

			foreach (Portlet portlet in portlets) {
				portlet.KeyPortalColumn = LocalId;
			}

			Set(portlets, e => e.Portlets);

		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Portlets;
		}
	}
}
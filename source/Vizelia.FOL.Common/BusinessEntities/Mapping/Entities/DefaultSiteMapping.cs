﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping for the Site entity.
	/// </summary>
	[MappingSchema("Site", "Default", "1.0.0")]
	public class DefaultSiteMapping : PsetEnabledMapping<Site> {
		
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultSiteMapping"/> class.
		/// </summary>
		/// <param name="site">The site.</param>
		public DefaultSiteMapping(Site site) : base(site) {
			// Calls base.
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultSiteMapping"/> class.
		/// </summary>
		public DefaultSiteMapping() {
			// Calls base.
		}

		/// <summary>
		/// The external identifier of the Site.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Name of the Site.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Description of the Site.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// Key of the parent Site.
		/// </summary>
		public string SiteParentLocalId {
			get { return Get(e => e.KeySiteParent); }
			set { Set(value, e => e.KeySiteParent); }
		}

		/// <summary>
		/// ObjectType of the Site.
		/// </summary>
		public string ObjectType {
			get { return Get(e => e.ObjectType); }
			set { Set(value, e => e.ObjectType); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
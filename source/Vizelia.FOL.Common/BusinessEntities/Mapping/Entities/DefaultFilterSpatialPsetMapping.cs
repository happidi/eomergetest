using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a FilterSpatialPset entity.
	/// </summary>
	[MappingSchema("FilterSpatialPset", "Default", "1.0.0")]
	public class DefaultFilterSpatialPsetMapping : Mapping<FilterSpatialPset> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultFilterSpatialPsetMapping"/> class.
		/// </summary>
		public DefaultFilterSpatialPsetMapping() {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultFilterSpatialPsetMapping"/> class.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public DefaultFilterSpatialPsetMapping(FilterSpatialPset entity) : base(entity) {
			// Do nothing.
		}

		/// <summary>
		/// The key of the Chart this filter is attached too.
		/// </summary>
		public string ChartLocalId {
			get { return Get(e => e.KeyChart); }
			set { Set(value, e => e.KeyChart); }
		}

		/// <summary>
		/// The key of the AlarmDefinition this filter is attached too.
		/// </summary>
		public string AlarmDefinitionLocalId {
			get { return Get(e => e.KeyAlarmDefinition); }
			set { Set(value, e => e.KeyAlarmDefinition); }
		}

		/// <summary>
		/// The Name of the Pset to use to filter.
		/// </summary>
		public string PsetName {
			get { return Get(e => e.PsetName); }
			set { Set(value, e => e.PsetName); }
		}

		/// <summary>
		/// The Name of the Attribute  to use to filter
		/// </summary>
		public string AttributeName {
			get { return Get(e => e.AttributeName); }
			set { Set(value, e => e.AttributeName); }
		}

		/// <summary>
		/// The type of the object the Pset is linked to.
		/// </summary>
		public string UsageName {
			get { return Get(e => e.UsageName); }
			set { Set(value, e => e.UsageName); }
		}

		/// <summary>
		/// The Value of the PsetAttribute to use to filter.
		/// </summary>
		public string Value {
			get { return Get(e => e.Value); }
			set { Set(value, e => e.Value); }
		}

		/// <summary>
		/// The Value2 of the PsetAttribute to use to filter (in case of a between Condition).
		/// </summary>
		public string Value2 {
			get { return Get(e => e.Value2); }
			set { Set(value, e => e.Value2); }
		}

		/// <summary>
		/// the filtering condition to use : =, !=, like
		/// </summary>
		public FilterCondition Condition {
			get { return Get(e => e.Condition); }
			set { Set(value, e => e.Condition); }
		}
		
		/// <summary>
		/// The order to apply the filter in case there are more than one.
		/// </summary>
		public int Order {
			get { return Get(e => e.Order); }
			set { Set(value, e => e.Order); }
		}

		/// <summary>
		///  the operator to use in case there are more than one filter attached to a chart.
		/// </summary>
		public LogicalOperator Operator {
			get { return Get(e => e.Operator); }
			set { Set(value, e => e.Operator); }
		}
		
		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
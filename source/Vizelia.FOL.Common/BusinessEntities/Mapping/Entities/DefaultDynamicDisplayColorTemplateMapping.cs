using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a Dynamic Display Color Template entity.
	/// </summary>
	[MappingSchema("DynamicDisplayColorTemplate", "Default", "1.0.0")]
	public class DefaultDynamicDisplayColorTemplateMapping : Mapping<DynamicDisplayColorTemplate> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDynamicDisplayColorTemplateMapping"/> class.
		/// </summary>
		public DefaultDynamicDisplayColorTemplateMapping() {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDynamicDisplayColorTemplateMapping"/> class.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public DefaultDynamicDisplayColorTemplateMapping(DynamicDisplayColorTemplate entity) : base(entity) {
			// Do nothing.		
		}

		/// <summary>
		/// Name of the Dynamic Display Color Template.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Red color component of the StartColor.
		/// </summary>
		public int StartColorR {
			get { return Get(e => e.StartColorR); }
			set { Set(value, e => e.StartColorR); }
		}

		/// <summary>
		/// Green color component of the StartColor.
		/// </summary>
		public int StartColorG {
			get { return Get(e => e.StartColorG); }
			set { Set(value, e => e.StartColorG); }
		}

		/// <summary>
		/// Blue color component of the StartColor.
		/// </summary>
		public int StartColorB {
			get { return Get(e => e.StartColorB); }
			set { Set(value, e => e.StartColorB); }
		}

		/// <summary>
		/// Red color component of the EndColor.
		/// </summary>
		public int EndColorR {
			get { return Get(e => e.EndColorR); }
			set { Set(value, e => e.EndColorR); }
		}

		/// <summary>
		/// Green color component of the EndColor.
		/// </summary>
		public int EndColorG {
			get { return Get(e => e.EndColorG); }
			set { Set(value, e => e.EndColorG); }
		}

		/// <summary>
		/// Blue color component of the EndColor.
		/// </summary>
		public int EndColorB {
			get { return Get(e => e.EndColorB); }
			set { Set(value, e => e.EndColorB); }
		}

		/// <summary>
		/// Red color component of the BorderColor.
		/// </summary>
		public int BorderColorR {
			get { return Get(e => e.BorderColorR); }
			set { Set(value, e => e.BorderColorR); }
		}

		/// <summary>
		/// Green color component of the BorderColor.
		/// </summary>
		public int BorderColorG {
			get { return Get(e => e.BorderColorG); }
			set { Set(value, e => e.BorderColorG); }
		}

		/// <summary>
		/// Blue color component of the BorderColor.
		/// </summary>
		public int BorderColorB {
			get { return Get(e => e.BorderColorB); }
			set { Set(value, e => e.BorderColorB); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [use predefined color].
		/// </summary>
		/// <value>
		///   <c>true</c> if [use predefined color]; otherwise, <c>false</c>.
		/// </value>
		public bool UsePredefinedColor {
			get { return Get(e => e.UsePredefinedColor); }
			set { Set(value, e => e.UsePredefinedColor); }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}
		
		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
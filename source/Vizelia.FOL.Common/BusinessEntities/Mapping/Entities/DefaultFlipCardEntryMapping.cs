﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the Flip Card Entry entity.
	/// </summary>
	[MappingSchema("FlipCardEntry", "Default", "1.0.0")]
	public class DefaultFlipCardEntryMapping : Mapping<FlipCardEntry> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultFlipCardEntryMapping"/> class.
		/// </summary>
		public DefaultFlipCardEntryMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultFlipCardEntryMapping"/> class.
		/// </summary>
		/// <param name="flipCardEntry">The FlipCardEntry.</param>
		public DefaultFlipCardEntryMapping(FlipCardEntry flipCardEntry)
			: base(flipCardEntry) {
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the key flip card entry.
		/// </summary>
		/// <value>
		/// The key flip card entry.
		/// </value>
		public string KeyFlipCardEntry {
			get { return Get(e => e.KeyFlipCardEntry); }
			set { Set(value, e => e.KeyFlipCardEntry); }
		}


		/// <summary>
		/// Gets or sets the key flip card.
		/// </summary>
		/// <value>
		/// The key flip card.
		/// </value>
		public string KeyFlipCard {
			get { return Get(e => e.KeyFlipCard); }
			set { Set(value, e => e.KeyFlipCard); }
		}

		/// <summary>
		/// Gets or sets the flip card local id.
		/// </summary>
		/// <value>
		/// The flip card local id.
		/// </value>
		public string FlipCardLocalId {
			get { return Get(e => e.FlipCardLocalId); }
			set { Set(value, e => e.FlipCardLocalId); }
		}

		/// <summary>
		/// Gets or sets the order.
		/// </summary>
		/// <value>
		/// The order.
		/// </value>
		public int Order {
			get { return Get(e => e.Order); }
			set { Set(value, e => e.Order); }
		}

		/// <summary>
		/// Gets or sets the type entity.
		/// </summary>
		/// <value>
		/// The type entity.
		/// </value>
		public string TypeEntity {
			get { return Get(e => e.TypeEntity); }
			set { Set(value, e => e.TypeEntity); }
		}

		/// <summary>
		/// Gets or sets the key entity.
		/// </summary>
		/// <value>
		/// The key entity.
		/// </value>
		public string KeyEntity {
			get { return Get(e => e.KeyEntity); }
			set { Set(value, e => e.KeyEntity); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
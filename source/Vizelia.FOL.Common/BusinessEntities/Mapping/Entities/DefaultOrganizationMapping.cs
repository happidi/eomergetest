﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the Organization entity.
	/// </summary>
	[MappingSchema("Organization", "Default", "1.0.0")]
	public class DefaultOrganizationMapping : Mapping<Organization> {


		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultOrganizationMapping"/> class.
		/// </summary>
		public DefaultOrganizationMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultOrganizationMapping"/> class.
		/// </summary>
		/// <param name="organization">The organization.</param>
		public DefaultOrganizationMapping(Organization organization)
			: base(organization) {
		}

		/// <summary>
		/// The id of the Organization.
		/// </summary>
		public string Id {
			get { return Get(e => e.Id); }
			set { Set(value, e => e.Id); }
		}

		/// <summary>
		/// The name of the Organization.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// The description of the Organization.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// The id of the parent Organization.
		/// </summary>
		public string ParentOrganizationId {
			get { return Get(e => e.KeyParent); }
			set { Set(value, e => e.KeyParent); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
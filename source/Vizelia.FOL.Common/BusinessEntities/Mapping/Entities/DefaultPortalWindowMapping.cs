﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a Portal Window entity.
	/// </summary>
	[MappingSchema("PortalWindow", "Default", "1.0.0")]
	public class DefaultPortalWindowMapping : Mapping<PortalWindow> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPortalWindowMapping"/> class.
		/// </summary>
		public DefaultPortalWindowMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPortalWindowMapping"/> class.
		/// </summary>
		/// <param name="portalWindow">The PortalWindow.</param>
		public DefaultPortalWindowMapping(PortalWindow portalWindow)
			: base(portalWindow) {
		}

		/// <summary>
		/// The local id of the Portal Window.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the title of the portal.
		/// </summary>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Gets or sets the icon css of the portal.
		/// </summary>
		public string IconCls {
			get { return Get(e => e.IconCls); }
			set { Set(value, e => e.IconCls); }
		}

		/// <summary>
		/// True to display the portal window in the Energy toolbar with the IconCss, false to hide it.
		/// </summary>
		public bool DisplayInToolbar {
			get { return Get(e => e.DisplayInToolbar); }
			set { Set(value, e => e.DisplayInToolbar); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		public bool OpenOnStartup {
			get { return Get(e => e.OpenOnStartup); }
			set { Set(value, e => e.OpenOnStartup); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [preload tab].
		/// </summary>
		/// <value>
		///   <c>true</c> if [preload tab]; otherwise, <c>false</c>.
		/// </value>
		public bool PreloadTab {
			get { return Get(e => e.PreloadTab); }
			set { Set(value, e => e.PreloadTab); }
		}

		/// <summary>
		/// The X position of the portal window shortcut icon when displayed in desktop mode.
		/// </summary>
		public int? DesktopShortcutIconX {
			get { return Get(e => e.DesktopShortcutIconX); }
			set { Set(value, e => e.DesktopShortcutIconX); }
		}

		/// <summary>
		/// The Y position of the portal window shortcut icon when displayed in desktop mode.
		/// </summary>
		public int? DesktopShortcutIconY {
			get { return Get(e => e.DesktopShortcutIconY); }
			set { Set(value, e => e.DesktopShortcutIconY); }
		}

		/// <summary>
		/// Gets or sets the portal tab column config.
		/// </summary>
		/// <value>
		/// The portal tab column config.
		/// </value>
		public PortalTabColumnConfig PortalTabColumnConfig {
			get { return Get(e => e.PortalTabColumnConfig); }
			set { Set(value, e => e.PortalTabColumnConfig); }
		}

		/// <summary>
		/// Gets os sets the list of tabs of the portal.
		/// </summary>
		[XmlArray("Tabs")]
		[XmlArrayItem("Tab", typeof(DefaultPortalTabMapping))]
		public List<DefaultPortalTabMapping> Tabs { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		public string Creator {
			get { return Get(e => e.Creator); }
			set { Set(value, e => e.Creator); }
		}

		/// <summary>
		/// Populates properties before the business entity is outputted.
		/// </summary>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			List<PortalTab> tabs = Tabs.Select(t => t.GetBusinessEntity()).ToList();
			foreach (PortalTab tab in tabs) {
				tab.KeyPortalWindow = LocalId;
			}

			Set(tabs, e => e.Tabs);
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Tabs;
		}
	}
}
using System;

namespace Vizelia.FOL.BusinessEntities.Mapping
{
	/// <summary>
	/// An interface for the mapping record class that provides common funcitonality.
	/// </summary>
	public interface IMappingRecord {
		/// <summary>
		/// Gets the entity.
		/// </summary>
		IMappableEntity Entity { get; }

		/// <summary>
		/// Gets the local id.
		/// </summary>
		string LocalId { get; }

		/// <summary>
		/// Gets the type of the business entity.
		/// </summary>
		/// <value>
		/// The type of the business entity.
		/// </value>
		Type BusinessEntityType { get; }

		/// <summary>
		/// Determines whether the given field has been assigned with a value or not.
		/// </summary>
		/// <param name="propertyName">Name of the field.</param>
		/// <returns>
		///   <c>true</c> if the given field has been assigned with a value; otherwise, <c>false</c>.
		/// </returns>
		bool IsPropertyAssigned(string propertyName);
	}
}
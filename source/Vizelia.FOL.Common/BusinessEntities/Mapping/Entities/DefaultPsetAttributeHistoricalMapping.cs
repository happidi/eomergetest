﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the PsetAttributeHistorical entity.
	/// </summary>
	[MappingSchema("PsetAttributeHistorical", "Default", "1.0.0")]
	public class DefaultPsetAttributeHistoricalMapping : Mapping<PsetAttributeHistorical> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPsetAttributeHistoricalMapping"/> class.
		/// </summary>
		public DefaultPsetAttributeHistoricalMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPsetAttributeHistoricalMapping"/> class.
		/// </summary>
		/// <param name="psetAttributeHistorical">The pset attribute historical.</param>
		public DefaultPsetAttributeHistoricalMapping(PsetAttributeHistorical psetAttributeHistorical)
			: base(psetAttributeHistorical) {

		}

		/// <summary>
		/// The Key of the PsetAttributeHistorical.
		/// </summary>
		public string KeyPsetAttributeHistorical {
			get { return Get(e => e.KeyPsetAttributeHistorical); }
			set { Set(value, e => e.KeyPsetAttributeHistorical); }
		}

		/// <summary>
		/// The Key of the KeyPropertySingleValue the attribute is attached to.
		/// </summary>
		public string KeyPropertySingleValue {
			get { return Get(e => e.KeyPropertySingleValue); }
			set { Set(value, e => e.KeyPropertySingleValue); }
		}


		/// <summary>
		/// The Key of the Object the attribute is attached to.
		/// </summary>
		public string KeyObject {
			get { return Get(e => e.KeyObject); }
			set { Set(value, e => e.KeyObject); }
		}

		/// <summary>
		/// The name of the object type.
		/// </summary>
		public string UsageName {
			get { return Get(e => e.UsageName); }
			set { Set(value, e => e.UsageName); }
		}

		/// <summary>
		/// The AcquisitionDateTime of the PsetAttribute.
		/// </summary>
		public DateTime AttributeAcquisitionDateTime {
			get { return Get(e => e.AttributeAcquisitionDateTime); }
			set { Set(value, e => e.AttributeAcquisitionDateTime); }
		}


		/// <summary>
		/// The Valeu of the PsetAttribute.
		/// </summary>
		public double AttributeValue {
			get { return Get(e => e.AttributeValue); }
			set { Set(value, e => e.AttributeValue); }
		}


		/// <summary>
		/// The Comment of the PsetAttribute.
		/// </summary>
		public string AttributeComment {
			get { return Get(e => e.AttributeComment); }
			set { Set(value, e => e.AttributeComment); }
		}


		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}


		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}

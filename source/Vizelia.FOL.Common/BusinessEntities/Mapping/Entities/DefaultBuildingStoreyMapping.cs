﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping for the Building Storey entity.
	/// </summary>
	[MappingSchema("BuildingStorey", "Default", "1.0.0")]
	public class DefaultBuildingStoreyMapping : PsetEnabledMapping<BuildingStorey> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultBuildingStoreyMapping"/> class.
		/// </summary>
		public DefaultBuildingStoreyMapping()
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultBuildingStoreyMapping"/> class.
		/// </summary>
		/// <param name="buildingStorey">The building storey.</param>
		public DefaultBuildingStoreyMapping(BuildingStorey buildingStorey):base(buildingStorey)
		{
			
		}
		/// <summary>
		/// The external identifier of the BuildingStorey.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The CAD file of the BuildingStorey.
		/// </summary>
		public string CADFile {
			get { return Get(e => e.CADFile); }
			set { Set(value, e => e.CADFile); }
		}

		/// <summary>
		/// Name of the BuildingStorey.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Description of the BuildingStorey.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// Elevation of the BuildingStorey.
		/// </summary>
		public double? Elevation {
			get { return Get(e => e.Elevation); }
			set { Set(value, e => e.Elevation); }
		}

		/// <summary>
		/// Local id of the parent Building.
		/// </summary>
		public string BuildingLocalId {
			get { return Get(e => e.KeyBuilding); }
			set { Set(value, e => e.KeyBuilding); }
		}

		/// <summary>
		/// ObjectType of the BuildingStorey.
		/// </summary>
		public string ObjectType {
			get { return Get(e => e.ObjectType); }
			set { Set(value, e => e.ObjectType); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a Dynamic Display Image entity.
	/// </summary>
	//[MappingSchema("DynamicDisplayImage", "Default", "1.0.0")]
	public class DefaultDynamicDisplayImageMapping : Mapping<DynamicDisplayImage> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDynamicDisplayImageMapping"/> class.
		/// </summary>
		public DefaultDynamicDisplayImageMapping() {
			// Do nothing.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDynamicDisplayImageMapping"/> class.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public DefaultDynamicDisplayImageMapping(DynamicDisplayImage entity) : base(entity) {
			// Do nothing.	
		}

		/// <summary>
		/// Gets or Sets the local id of the document attached to it.
		/// </summary>
		public string ImageLocalId {
			get { return Get(e => e.KeyImage); }
			set { Set(value, e => e.KeyImage); }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
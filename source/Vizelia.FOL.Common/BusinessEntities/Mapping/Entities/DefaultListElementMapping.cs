﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the ListElement entity.
	/// </summary>
	[MappingSchema("ListElement", "Default", "1.0.0")]
	public class DefaultListElementMapping : Mapping<ListElement> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultListElementMapping"/> class.
		/// </summary>
		public DefaultListElementMapping() {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultListElementMapping"/> class.
		/// </summary>
		/// <param name="listElement">The list element.</param>
		public DefaultListElementMapping(ListElement listElement)
			: base(listElement) {
		}

		/// <summary>
		/// MsgCode of the ListElement.
		/// </summary>
		public string MsgCode {
			get { return Get(e => e.MsgCode); }
			set { Set(value, e => e.MsgCode); }
		}

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		public string Value {
			get { return Get(e => e.Value); }
			set { Set(value, e => e.Value); }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}

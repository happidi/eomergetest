﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of the VirtualFile entity.
	/// </summary>
	[MappingSchema("VirtualFile", "Default", "1.0.0")]
	public class DefaultVirtualFileMapping : Mapping<VirtualFile> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultVirtualFileMapping"/> class.
		/// </summary>
		public DefaultVirtualFileMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultVirtualFileMapping"/> class.
		/// </summary>
		/// <param name="virtualFile">The VirtualFile.</param>
		public DefaultVirtualFileMapping(VirtualFile virtualFile)
			: base(virtualFile) {
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the width of the Playlist.
		/// </summary>
		public int Width {
			get { return Get(e => e.Width); }
			set { Set(value, e => e.Width); }
		}

		/// <summary>
		/// Gets or sets the height of the Playlist.
		/// </summary>
		public int Height {
			get { return Get(e => e.Height); }
			set { Set(value, e => e.Height); }
		}

		/// <summary>
		/// Gets or sets the path.
		/// </summary>
		/// <value>
		/// The path.
		/// </value>
		public string Path {
			get { return Get(e => e.Path); }
			set { Set(value, e => e.Path); }
		}

		/// <summary>
		/// Gets or sets the type of the entity.
		/// </summary>
		/// <value>
		/// The type of the entity.
		/// </value>
		public string EntityType {
			get { return Get(e => e.EntityType); }
			set { Set(value, e => e.EntityType); }
		}

		/// <summary>
		/// Gets or sets the key entity.
		/// </summary>
		/// <value>
		/// The key entity.
		/// </value>
		public string KeyEntity {
			get { return Get(e => e.KeyEntity); }
			set { Set(value, e => e.KeyEntity); }
		}

		/// <summary>
		/// Gets or sets the max record count.
		/// </summary>
		/// <value>
		/// The max record count.
		/// </value>
		public int MaxRecordCount {
			get { return Get(e => e.MaxRecordCount); }
			set { Set(value, e => e.MaxRecordCount); }
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		public string Creator {
			get { return Get(e => e.Creator); }
			set { Set(value, e => e.Creator); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of a Portal Tab entity.
	/// </summary>
	[MappingSchema("PortalTab", "Default", "1.0.0")]
	public class DefaultPortalTabMapping : Mapping<PortalTab> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPortalTabMapping"/> class.
		/// </summary>
		public DefaultPortalTabMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPortalTabMapping"/> class.
		/// </summary>
		/// <param name="portalTab">The PortalTab.</param>
		public DefaultPortalTabMapping(PortalTab portalTab)
			: base(portalTab) {
		}

		/// <summary>
		/// Local id of the Portal Window.
		/// </summary>
		public string PortalWindowLocalId {
			get { return Get(e => e.KeyPortalWindow); }
			set { Set(value, e => e.KeyPortalWindow); }
		}

		/// <summary>
		/// Gets or sets the name of this portal tab.
		/// </summary>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// The local id of the Portal Tab.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the column config.
		/// </summary>
		/// <value>
		/// The column config.
		/// </value>
		public PortalTabColumnConfig ColumnConfig {
			get { return Get(e => e.ColumnConfig); }
			set { Set(value, e => e.ColumnConfig); }
		}

		/// <summary>
		/// Gets or sets the height of the footer.
		/// </summary>
		/// <value>
		/// The height of the footer.
		/// </value>
		public int? FooterHeight {
			get { return Get(e => e.FooterHeight); }
			set { Set(value, e => e.FooterHeight); }
		}

		/// <summary>
		/// Gets or sets the height of the header.
		/// </summary>
		/// <value>
		/// The height of the header.
		/// </value>
		public int? HeaderHeight {
			get { return Get(e => e.HeaderHeight); }
			set { Set(value, e => e.HeaderHeight); }
		}

		/// <summary>
		/// Gets or sets the list of column of the portal tab.
		/// </summary>
		[XmlArray("Columns")]
		[XmlArrayItem("Column", typeof(DefaultPortalColumnMapping))]
		public List<DefaultPortalColumnMapping> Columns { get; set; }

		/// <summary>
		/// Gets or sets the order of the portal tab in the portal window.
		/// </summary>
		public int Order {
			get { return Get(e => e.Order); }
			set { Set(value, e => e.Order); }
		}

		/// <summary>
		/// Gets or sets the background CLS.
		/// </summary>
		/// <value>
		/// The background CLS.
		/// </value>
		public string BackgroundCls {
			get { return Get(e => e.BackgroundCls); }
			set { Set(value, e => e.BackgroundCls); }
		}

		/// <summary>
		/// Populates properties before the business entity is outputted.
		/// </summary>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			List<PortalColumn> portalColumns = Columns.Select(c => c.GetBusinessEntity()).ToList();
			foreach (PortalColumn portalColumn in portalColumns) {
				portalColumn.KeyPortalTab = LocalId;
			}

			Set(portalColumns, e => e.Columns);
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Columns;
		}
	}
}
namespace Vizelia.FOL.BusinessEntities.Mapping
{
	/// <summary>
	/// Represents an entity that supports mapping.
	/// </summary>
	public interface IMappableEntity
	{
		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		string LocalId { get; set; }
	}
}
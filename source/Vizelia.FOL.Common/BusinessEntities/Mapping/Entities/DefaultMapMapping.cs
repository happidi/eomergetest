﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of the Map entity.
	/// </summary>
	[MappingSchema("Map", "Default", "1.0.0")]
	public class DefaultMapMapping : Mapping<Map> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultMapMapping"/> class.
		/// </summary>
		public DefaultMapMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultMapMapping"/> class.
		/// </summary>
		/// <param name="map">The Map.</param>
		public DefaultMapMapping(Map map)
			: base(map) {
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the title of the Map.
		/// </summary>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}


		/// <summary>
		/// Gets or sets the type of the map.
		/// </summary>
		/// <value>
		/// The type of the map.
		/// </value>
		public MapType MapType {
			get { return Get(e => e.MapType); }
			set { Set(value, e => e.MapType); }
		}

		/// <summary>
		/// Gets or sets the name of the pset.
		/// </summary>
		/// <value>
		/// The name of the pset.
		/// </value>
		public string PsetName {
			get { return Get(e => e.PsetName); }
			set { Set(value, e => e.PsetName); }
		}

		/// <summary>
		/// Gets or sets the name of the attribute.
		/// </summary>
		/// <value>
		/// The name of the attribute.
		/// </value>
		public string AttributeName {
			get { return Get(e => e.AttributeName); }
			set { Set(value, e => e.AttributeName); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		public bool IsFavorite {
			get { return Get(e => e.IsFavorite); }
			set { Set(value, e => e.IsFavorite); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [show dashboard].
		/// </summary>
		/// <value>
		///   <c>true</c> if [show dashboard]; otherwise, <c>false</c>.
		/// </value>
		public bool ShowDashboard {
			get { return Get(e => e.ShowDashboard); }
			set { Set(value, e => e.ShowDashboard); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [use spatial icons].
		/// </summary>
		/// <value>
		///   <c>true</c> if [use spatial icons]; otherwise, <c>false</c>.
		/// </value>
		public bool UseSpatialIcons {
			get { return Get(e => e.UseSpatialIcons); }
			set { Set(value, e => e.UseSpatialIcons); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		public bool OpenOnStartup {
			get { return Get(e => e.OpenOnStartup); }
			set { Set(value, e => e.OpenOnStartup); }
		}

		/// <summary>
		/// Gets or sets the Zoom of the Map.
		/// </summary>
		public int Zoom {
			get { return Get(e => e.Zoom); }
			set { Set(value, e => e.Zoom); }
		}

		/// <summary>
		/// Gets or sets the key classification item alarm definition.
		/// </summary>
		/// <value>
		/// The key classification item alarm definition.
		/// </value>
		public string KeyClassificationItemAlarmDefinition {
			get { return Get(e => e.KeyClassificationItemAlarmDefinition); }
			set { Set(value, e => e.KeyClassificationItemAlarmDefinition); }
		}

		/// <summary>
		/// Gets or sets the key location.
		/// </summary>
		/// <value>
		/// The key location.
		/// </value>
		public string KeyLocation {
			get { return Get(e => e.KeyLocation); }
			set { Set(value, e => e.KeyLocation); }
		}

		/// <summary>
		/// The local ID of the location of the Meter.
		/// </summary>
		public virtual string LocationLocalId {
			get { return Get(e => e.KeyLocation); }
			set { Set(value, e => e.KeyLocation); }
		}

		/// <summary>
		/// The location typename of the Map.
		/// </summary>
		public HierarchySpatialTypeName LocationTypeName {
			get { return Get(e => e.LocationTypeName); }
			set { Set(value, e => e.LocationTypeName); }
		}

		/// <summary>
		/// Gets or sets the Location Longitude.
		/// </summary>
		public double LocationLongitude {
			get { return Get(e => e.LocationLongitude); }
			set { Set(value, e => e.LocationLongitude); }
		}

		/// <summary>
		/// Gets or sets the Location Latitude.
		/// </summary>
		public double LocationLatitude {
			get { return Get(e => e.LocationLatitude); }
			set { Set(value, e => e.LocationLatitude); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
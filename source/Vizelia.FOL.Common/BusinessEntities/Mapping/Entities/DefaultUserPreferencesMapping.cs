﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping for a User Preferences.
	/// </summary>
	[MappingSchema("UserPreferences", "Default", "1.0.0")]
	public class DefaultUserPreferencesMapping : Mapping<UserPreferences> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultUserPreferencesMapping"/> class.
		/// </summary>
		public DefaultUserPreferencesMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultFOLMembershipUserMapping"/> class.
		/// </summary>
		/// <param name="userPreferences">User preferences.</param>
		public DefaultUserPreferencesMapping(UserPreferences userPreferences)
			: base(userPreferences) {
		}

		/// <summary>
		/// Returns true if the user is locked.
		/// </summary>
		public int GridsPageSize {
			get { return Get(e => e.GridsPageSize); }
			set { Set(value, e => e.GridsPageSize); }
		}

		/// <summary>
		/// Preload chart user preference
		/// </summary>
		public bool PreloadCharts {
			get { return Get(e => e.PreloadCharts); }
			set { Set(value, e => e.PreloadCharts); }
		}

		/// <summary>
		/// Custom Desktop Background
		/// </summary>
		public string CustomDesktopBackground {
			get { return Get(e => e.CustomDesktopBackground); }
			set { Set(value, e => e.CustomDesktopBackground); }
		}

		/// <summary>
		/// Desktop Text Color
		/// </summary>
		public string DesktopTextColor {
			get { return Get(e => e.DesktopTextColor); }
			set { Set(value, e => e.DesktopTextColor); }
		}

		/// <summary>
		/// TimeZone Id
		/// </summary>
		public string TimeZoneId {
			get { return Get(e => e.TimeZoneId); }
			set { Set(value, e => e.TimeZoneId); }
		}

		/// <summary>
		/// Default Chart KPI
		/// </summary>
		public string DefaultChartKPI {
			get { return Get(e => e.DefaultChartKPI); }
			set { Set(value, e => e.DefaultChartKPI); }
		}

		/// <summary>
		/// The LocalId of the User.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// gets chlid mappings
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the Meter entity.
	/// </summary>
	[MappingSchema("Meter", "Default", "1.0.0")]
	public class DefaultMeterMapping : PsetEnabledMapping<Meter> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultMeterMapping"/> class.
		/// </summary>
		public DefaultMeterMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultMeterMapping"/> class.
		/// </summary>
		/// <param name="meter">The meter.</param>
		public DefaultMeterMapping(Meter meter)
			: base(meter) {

			if (meter.Data != null) {
				Data = new List<DefaultMeterDataMapping>();
				Data.AddRange(meter.Data.Select(d => new DefaultMeterDataMapping(d) { MeterLocalId = LocalId }));
			}
		}

		/// <summary>
		/// The external identifier of the Meter.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The Name of the Meter.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// The Description of the Meter.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}


		/// <summary>
		/// The local ID of the location of the Meter.
		/// </summary>
		public virtual string LocationLocalId {
			get { return Get(e => e.KeyLocation); }
			set { Set(value, e => e.KeyLocation); }
		}

		/// <summary>
		/// The location typename of the Meter.
		/// </summary>
		public virtual HierarchySpatialTypeName LocationTypeName {
			get { return Get(e => e.LocationTypeName); }
			set { Set(value, e => e.LocationTypeName); }
		}

		/// <summary>
		/// The factor conversion that is applied to data from the unit input to the unit output.
		/// Value is 1 per default.
		/// </summary>
		public double Factor {
			get { return Get(e => e.Factor); }
			set { Set(value, e => e.Factor); }
		}

		/// <summary>
		/// The group operation that applies to the data when grouped over time.
		/// </summary>
		public MathematicOperator GroupOperation {
			get { return Get(e => e.GroupOperation); }
			set { Set(value, e => e.GroupOperation); }
		}

		/// <summary>
		/// The input unit of the Meter.
		/// </summary>
		public string UnitInput {
			get { return Get(e => e.UnitInput); }
			set { Set(value, e => e.UnitInput); }
		}

		/// <summary>
		/// The output unit of the Meter.
		/// </summary>
		public string UnitOutput {
			get { return Get(e => e.UnitOutput); }
			set { Set(value, e => e.UnitOutput); }
		}

		/// <summary>
		/// true if the meter data are cumulated, false otherwise.
		/// </summary>
		public bool IsAccumulated {
			get { return Get(e => e.IsAccumulated); }
			set { Set(value, e => e.IsAccumulated); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is acquisition date time end interval.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is acquisition date time end interval; otherwise, <c>false</c>.
		/// </value>
		public bool IsAcquisitionDateTimeEndInterval {
			get { return Get(e => e.IsAcquisitionDateTimeEndInterval); }
			set { Set(value, e => e.IsAcquisitionDateTimeEndInterval); }
		}

		/// <summary>
		/// The rollover value of the meter when it's accumulated.
		/// </summary>
		public double? RolloverValue {
			get { return Get(e => e.RolloverValue); }
			set { Set(value, e => e.RolloverValue); }
		}

		/// <summary>
		/// The frequency input in minutes.
		/// </summary>
		public int? FrequencyInput {
			get { return Get(e => e.FrequencyInput); }
			set { Set(value, e => e.FrequencyInput); }
		}

		/// <summary>
		/// The EndpointFrequency polling in minutes.
		/// </summary>
		public int? EndpointFrequency {
			get { return Get(e => e.EndpointFrequency); }
			set { Set(value, e => e.EndpointFrequency); }
		}

		/// <summary>
		/// The endpoint resource of the meter.
		/// </summary>
		public string KeyEndPoint {
			get { return Get(e => e.KeyEndpoint); }
			set { Set(value, e => e.KeyEndpoint); }
		}

		/// <summary>
		/// The endpoint type resource of the meter.
		/// </summary>
		public string EndPointType {
			get { return Get(e => e.EndpointType); }
			set { Set(value, e => e.EndpointType); }
		}

		/// <summary>
		/// The calibration date time of the meter.
		/// </summary>
		public DateTime? CalibrationDateTime {
			get { return Get(e => e.CalibrationDateTime); }
			set { Set(value, e => e.CalibrationDateTime); }
		}

		/// <summary>
		/// The calibration value of the meter.
		/// </summary>
		public double? CalibrationValue {
			get { return Get(e => e.CalibrationValue); }
			set { Set(value, e => e.CalibrationValue); }
		}

		/// <summary>
		/// true if the meter accepts input through a mobile device, false otherwise.
		/// </summary>
		public bool AcceptMobileInput {
			get { return Get(e => e.AcceptMobileInput); }
			set { Set(value, e => e.AcceptMobileInput); }
		}

		/// <summary>
		/// The local ID of the classification of the Meter.
		/// </summary>
		public virtual string ClassificationItemLocalId {
			get { return Get(e => e.KeyClassificationItem); }
			set { Set(value, e => e.KeyClassificationItem); }
		}

		/// <summary>
		/// The Timeinterval that will be applied to the meter operations when fetching data.
		/// </summary>
		public AxisTimeInterval MeterOperationTimeScaleInterval {
			get { return Get(e => e.MeterOperationTimeScaleInterval); }
			set { Set(value, e => e.MeterOperationTimeScaleInterval); }
		}

		/// <summary>
		/// The local ID of the Final MeterOperation that will retrieve the data.
		/// </summary>
		public string MeterOperationResultLocalId {
			get { return Get(e => e.KeyMeterOperationResult); }
			set { Set(value, e => e.KeyMeterOperationResult); }
		}

		/// <summary>
		/// Gets or sets the type of the degree day.
		/// </summary>
		/// <value>
		/// The type of the degree day.
		/// </value>
		public DegreeDayType DegreeDayType {
			get { return Get(e => e.DegreeDayType); }
			set { Set(value, e => e.DegreeDayType); }
		}

		/// <summary>
		/// Gets or sets the degree day base.
		/// </summary>
		/// <value>
		/// The degree day base.
		/// </value>
		public double? DegreeDayBase {
			get { return Get(e => e.DegreeDayBase); }
			set { Set(value, e => e.DegreeDayBase); }
		}

		/// <summary>
		/// Gets or sets the time zone id.
		/// </summary>
		/// <value>
		/// The time zone id.
		/// </value>
		public string TimeZoneId {
			get { return Get(e => e.TimeZoneId); }
			set { Set(value, e => e.TimeZoneId); }
		}

		/// <summary>
		/// Gets or sets the offset.
		/// </summary>
		/// <value>
		/// The offset.
		/// </value>
		public double? Offset {
			get { return Get(e => e.Offset); }
			set { Set(value, e => e.Offset); }
		}

		/// <summary>
		/// Gets or sets the key calendar event category.
		/// </summary>
		/// <value>
		/// The key calendar event category.
		/// </value>
		public string KeyCalendarEventCategory {
			get { return Get(e => e.KeyCalendarEventCategory); }
			set { Set(value, e => e.KeyCalendarEventCategory); }
		}

		/// <summary>
		/// Gets or sets the calendar event category title.
		/// </summary>
		/// <value>
		/// The calendar event category title.
		/// </value>
		public string CalendarEventCategoryTitle {
			get { return Get(e => e.CalendarEventCategoryTitle); }
			set { Set(value, e => e.CalendarEventCategoryTitle); }
		}

		/// <summary>
		/// Gets or sets the calendar mode.
		/// </summary>
		/// <value>
		/// The calendar mode.
		/// </value>
		public ChartCalendarMode CalendarMode {
			get { return Get(e => e.CalendarMode); }
			set { Set(value, e => e.CalendarMode); }
		}

		/// <summary>
		/// Gets or sets the name of the pset.
		/// </summary>
		/// <value>
		/// The name of the pset.
		/// </value>
		public string PsetName {
			get { return Get(e => e.PsetName); }
			set { Set(value, e => e.PsetName); }
		}

		/// <summary>
		/// Gets or sets the name of the attribute.
		/// </summary>
		/// <value>
		/// The name of the attribute.
		/// </value>
		public string AttributeName {
			get { return Get(e => e.AttributeName); }
			set { Set(value, e => e.AttributeName); }
		}

		/// <summary>
		/// Gets or sets the pset factor operator.
		/// </summary>
		/// <value>
		/// The pset factor operator.
		/// </value>
		public ArithmeticOperator PsetFactorOperator {
			get { return Get(e => e.PsetFactorOperator); }
			set { Set(value, e => e.PsetFactorOperator); }
		}

		/// <summary>
		/// Gets or sets the manual input time interval.
		/// </summary>
		/// <value>
		/// The manual input time interval.
		/// </value>
		public ManualInputTimeInterval ManualInputTimeInterval {
			get { return Get(e => e.ManualInputTimeInterval); }
			set { Set(value, e => e.ManualInputTimeInterval); }
		}

		/// <summary>
		/// The list of MeterData for this Meter
		/// </summary>
		[XmlArray("Data")]
		[XmlArrayItem("MeterData", typeof(DefaultMeterDataMapping))]
		public List<DefaultMeterDataMapping> Data { get; set; }

		/// <summary>
		/// Gets the business entity.
		/// </summary>
		/// <returns></returns>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			if (Data != null) {
				List<MeterData> meterDatas = Data.Select(meterDataMapping => meterDataMapping.GetBusinessEntity()).ToList();

				foreach (var meterData in meterDatas) {
					meterData.KeyMeter = LocalId;
				}

				Set(meterDatas, e => e.Data);
			}
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			if (Data == null) {
				return Enumerable.Empty<IMappingRecordConverter>();
			}

			foreach (DefaultMeterDataMapping meterDataMapping in Data) {
				meterDataMapping.MeterLocalId = LocalId;
			}

			return Data;
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the Priority entity.
	/// </summary>
	[MappingSchema("Priority", "Default", "1.0.0")]
	public class DefaultPriorityMapping : Mapping<Priority> {

		/// <summary>
		/// Gets or sets the name of the localized label of the Priority.
		/// </summary>
		public string MsgCode {
			get { return Get(e => e.MsgCode); }
			set { Set(value, e => e.MsgCode); }
		}

		/// <summary>
		/// The external identifier of the Priority.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The value in minutes of the Priority.
		/// </summary>
		public int Value {
			get { return Get(e => e.Value); }
			set { Set(value, e => e.Value); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
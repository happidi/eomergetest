﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping for the Classification Item Relationship entity.
	/// </summary>
	[MappingSchema("ClassificationItemRelationship", "Default", "1.0.0")]
	public class DefaultClassificationItemRelationshipMapping : Mapping<ClassificationItemRelationship> {

		/// <summary>
		/// The local id of the Relation.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The local id of the parent ClassificationItem.
		/// </summary>
		public string ParentLocalId {
			get { return Get(e => e.KeyParent); }
			set { Set(value, e => e.KeyParent); }
		}

		/// <summary>
		///  The local id of the child ClassificationItem..
		/// </summary>
		public string ChildrenLocalId {
			get { return Get(e => e.KeyChildren); }
			set { Set(value, e => e.KeyChildren); }
		}

		/// <summary>
		/// The RelationPath of the ClassificationItemRelationship.
		/// </summary>
		public string RelationPath {
			get { return Get(e => e.RelationPath); }
			set { Set(value, e => e.RelationPath); }
		}

		/// <summary>
		/// The type of the Relation.
		/// </summary>
		public string RelationType {
			get { return Get(e => e.RelationType); }
			set { Set(value, e => e.RelationType); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
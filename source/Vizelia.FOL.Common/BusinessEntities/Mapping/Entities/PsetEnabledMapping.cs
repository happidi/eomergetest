using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping that inlcudes a Pset.
	/// </summary>
	/// <typeparam name="TBusinessEntity">The type of the business entity.</typeparam>
	public abstract class PsetEnabledMapping<TBusinessEntity> : Mapping<TBusinessEntity> where TBusinessEntity : BaseBusinessEntity, IMappableEntity, IPset, new() {

		/// <summary>
		/// Initializes a new instance of the <see cref="PsetEnabledMapping&lt;TBusinessEntity&gt;"/> class.
		/// </summary>
		protected PsetEnabledMapping() {
			// Calls base.
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PsetEnabledMapping&lt;TBusinessEntity&gt;"/> class.
		/// </summary>
		/// <param name="businessEntity">The business entity.</param>
		protected PsetEnabledMapping(TBusinessEntity businessEntity)
			: base(businessEntity) {
			if (businessEntity.PropertySetList != null) {
				Psets = new List<PsetMapping>();
				Psets.AddRange(businessEntity.PropertySetList.Select(pset => new PsetMapping(pset)));
			}
		}

		/// <summary>
		/// The list of pset of the Building.
		/// </summary>
		[XmlArray("Psets")]
		[XmlArrayItem("Pset", typeof(PsetMapping))]
		public List<PsetMapping> Psets { get; set; }

		/// <summary>
		/// Gets the business entity Psets corresponding to the mapping's Psets property.
		/// </summary>
		/// <returns></returns>
		private List<Pset> GetPsets() {

			List<Pset> psets = (Psets != null)
								? Psets.Select(p => p.GetBusinessEntity()).ToList()
								: new List<Pset>();

			return psets;
		}

		/// <summary>
		/// Adds psets to the entity before it is get by the consumer.
		/// </summary>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			List<Pset> psets = GetPsets();

			Set(psets, e => e.PropertySetList);
		}

		/// <summary>
		/// Sets the value for the specified property name.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		public override void Set(string propertyName, string value) {
			// We want a property name such as "psetName.psetAttributeName". There can be only one "." in the string.
			Match match = Regex.Match(propertyName, @"^(?<psetName>[^.]+)\.(?<psetAttributeName>[^.]+)$");

			if (match.Success) {
				SetPsetAttribute(match.Groups["psetName"].Value, match.Groups["psetAttributeName"].Value, value);
			}
			else {
				base.Set(propertyName, value);
			}
		}

		/// <summary>
		/// Sets the pset attribute to the given value.
		/// </summary>
		/// <param name="psetName">Name of the pset.</param>
		/// <param name="psetAttributeName">Name of the pset attribute.</param>
		/// <param name="value">The value.</param>
		private void SetPsetAttribute(string psetName, string psetAttributeName, string value) {
			if (Psets == null) {
				Psets = new List<PsetMapping>();
			}

			PsetMapping mapping = Psets.FirstOrDefault(m => m.Name.Equals(psetName));
			if (mapping == null) {
				mapping = new PsetMapping { Name = psetName, Attributes = new List<PsetAttributeMapping>() };
				Psets.Add(mapping);
			}

			PsetAttributeMapping attributeMapping = mapping.Attributes.FirstOrDefault(a => a.Name.Equals(psetAttributeName));
			if (attributeMapping == null) {
				attributeMapping = new PsetAttributeMapping() { Name = psetAttributeName, Value = value };
				mapping.Attributes.Add(attributeMapping);
			}
			else {
				attributeMapping.Value = value;
			}
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping for the Furniture entity.
	/// </summary>
	[MappingSchema("Furniture", "Default", "1.0.0")]
	public class DefaultFurnitureMapping : PsetEnabledMapping<Furniture> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultFurnitureMapping"/> class.
		/// </summary>
		public DefaultFurnitureMapping()
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultFurnitureMapping"/> class.
		/// </summary>
		/// <param name="furniture">The furniture.</param>
		public DefaultFurnitureMapping(Furniture furniture) : base(furniture)
		{
			
		}
		/// <summary>
		/// The local id of the Furniture.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The global id of the Furniture.
		/// </summary>
		public string GlobalId {
			get { return Get(e => e.GlobalId); }
			set { Set(value, e => e.GlobalId); }
		}

		/// <summary>
		/// The name of the Furniture.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// The description of the Furniture.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// The local id of the parent Space.
		/// </summary>
		public string LocationLocalId {
			get { return Get(e => e.KeyLocation); }
			set { Set(value, e => e.KeyLocation); }
		}

		/// <summary>
		/// The ObjectType of the Furniture.
		/// </summary>
		public string ObjectType {
			get { return Get(e => e.ObjectType); }
			set { Set(value, e => e.ObjectType); }
		}

		/// <summary>
		/// The tag of the Furniture.
		/// </summary>
		public string Tag {
			get { return Get(e => e.Tag); }
			set { Set(value, e => e.Tag); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}

		/// <summary>
		/// Gets or sets the key classification item.
		/// </summary>
		/// <value>
		/// The key classification item.
		/// </value>
		public string ClassificationItemLocalId {
			get { return Get(e => e.KeyClassificationItem); }
			set { Set(value, e => e.KeyClassificationItem); }
		}

		/// <summary>
		/// The location typename of the Furniture.
		/// </summary>
		public HierarchySpatialTypeName LocationTypeName {
			get { return Get(e => e.LocationTypeName); }
			set { Set(value, e => e.LocationTypeName); }
		}
	}
}
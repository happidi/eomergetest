﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of a Portal Template entity.
	/// </summary>
	[MappingSchema("PortalTemplate", "Default", "1.0.0")]
	public class DefaultPortalTemplateMapping : Mapping<PortalTemplate> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPortalTemplateMapping"/> class.
		/// </summary>
		public DefaultPortalTemplateMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPortalTemplateMapping"/> class.
		/// </summary>
		/// <param name="portalTemplate">The PortalTemplate.</param>
		public DefaultPortalTemplateMapping(PortalTemplate portalTemplate)
			: base(portalTemplate) {

			// Many to many
			if (portalTemplate.Users != null) {
				var usersLocalIds = portalTemplate.Users.Select(x => x.LocalId).ToList();
				Users = new DefaultMappingCrudStoreMapping<PortalTemplate, FOLMembershipUser>(usersLocalIds, LocalId);
			}

			if (portalTemplate.PortalWindows != null) {
				var portalWindowsLocalIds = portalTemplate.PortalWindows.Select(x => x.LocalId).ToList();
				PortalWindows = new DefaultMappingCrudStoreMapping<PortalTemplate, PortalWindow>(portalWindowsLocalIds, LocalId);
			}
		}

		/// <summary>
		/// Gets or sets the name of this portal tab.
		/// </summary>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// The local id of the Portal Tab.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the column config.
		/// </summary>
		/// <value>
		/// The column config.
		/// </value>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// Gets or sets the users.
		/// </summary>
		/// <value>
		/// The users.
		/// </value>
		public DefaultMappingCrudStoreMapping<PortalTemplate, FOLMembershipUser> Users { get; set; }

		/// <summary>
		/// Gets or sets the portal windows.
		/// </summary>
		/// <value>
		/// The portal windows.
		/// </value>
		public DefaultMappingCrudStoreMapping<PortalTemplate, PortalWindow> PortalWindows { get; set; }


		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			var entities = new List<IMappingRecordConverter>();

			if (Users != null) {
				Users.ParentLocalId = LocalId;
				entities.Add(Users);
			}

			if (PortalWindows != null) {
				PortalWindows.ParentLocalId = LocalId;
				entities.Add(PortalWindows);
			}

			return entities;
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a Classification Definition entity.
	/// </summary>
	[MappingSchema("ClassificationDefinition", "Default", "1.0.0")]
	public class DefaultClassificationDefinition : Mapping<ClassificationDefinition> {
		/// <summary>
		/// The category of the ClassificationDefinition.
		/// </summary>
		public string Category {
			get { return Get(e => e.Category); }
			set { Set(value, e => e.Category); }
		}

		/// <summary>
		/// The LocalId of the root ClassificationItem the definition is referring to.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The iconCls that the branch should use.
		/// If null the standard colored icon based of the R, G, B value of the ClassificationItem will be used.
		/// </summary>
		public string IconCls {
			get { return Get(e => e.IconCls); }
			set { Set(value, e => e.IconCls); }
		}

		/// <summary>
		/// True to display the iconCls only on the Root Node, false to always display it.
		/// </summary>
		public bool IconRootOnly {
			get { return Get(e => e.IconRootOnly); }
			set { Set(value, e => e.IconRootOnly); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
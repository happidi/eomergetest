﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the MeterValidationRule entity.
	/// </summary>
	[MappingSchema("MeterValidationRule", "Default", "1.0.0")]
	public class DefaultMeterValidationRuleMapping : Mapping<MeterValidationRule> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultMeterValidationRuleMapping"/> class.
		/// </summary>
		public DefaultMeterValidationRuleMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultMeterValidationRuleMapping"/> class.
		/// </summary>
		/// <param name="meterValidationRule">The meter validation rule.</param>
		public DefaultMeterValidationRuleMapping(MeterValidationRule meterValidationRule)
			: base(meterValidationRule) {

				if (meterValidationRule.Meters != null) {
					var meterLocalIds = meterValidationRule.Meters.Select(m => m.LocalId).ToList();
				Meters = new DefaultMappingCrudStoreMapping<MeterValidationRule, Meter>(meterLocalIds, LocalId);
			}

				if (meterValidationRule.FilterMeterClassifications != null) {
					List<string> classificationItemsLocalIds = meterValidationRule.FilterMeterClassifications.Select(c => c.LocalId).ToList();
					FilterMeterClassification = new DefaultMappingCrudStoreMapping<MeterValidationRule, ClassificationItem>(classificationItemsLocalIds, LocalId);
			}

				if (meterValidationRule.FilterSpatial != null) {
					FilterSpatial = new DefaultLocationMappingCrudStoreMapping<MeterValidationRule>(meterValidationRule.FilterSpatial, LocalId);
			}
		}

		/// <summary>
		/// The LocalId of the item.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="DefaultMeterValidationRuleMapping"/> is enabled.
		/// </summary>
		/// <value>
		///   <c>true</c> if enabled; otherwise, <c>false</c>.
		/// </value>
		public bool Enabled {
			get { return Get(e => e.Enabled); }
			set { Set(value, e => e.Enabled); }
		}

		/// <summary>
		/// Gets or sets the acquisition frequency.
		/// </summary>
		/// <value>
		/// The acquisition frequency.
		/// </value>
		public int? AcquisitionFrequency {
			get { return Get(e => e.AcquisitionFrequency); }
			set { Set(value, e => e.AcquisitionFrequency); }
		}

		/// <summary>
		/// Gets or sets the acquisition interval.
		/// </summary>
		/// <value>
		/// The acquisition interval.
		/// </value>
		public AxisTimeInterval AcquisitionInterval {
			get { return Get(e => e.AcquisitionInterval); }
			set { Set(value, e => e.AcquisitionInterval); }
		}

		/// <summary>
		/// Gets or sets the delay.
		/// </summary>
		/// <value>
		/// The delay.
		/// </value>
		public int? Delay {
			get { return Get(e => e.Delay); }
			set { Set(value, e => e.Delay); }
		}

		/// <summary>
		/// Gets or sets the delay interval.
		/// </summary>
		/// <value>
		/// The delay interval.
		/// </value>
		public AxisTimeInterval DelayInterval {
			get { return Get(e => e.DelayInterval); }
			set { Set(value, e => e.DelayInterval); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [increase only].
		/// </summary>
		/// <value>
		///   <c>true</c> if [increase only]; otherwise, <c>false</c>.
		/// </value>
		public bool IncreaseOnly {
			get { return Get(e => e.IncreaseOnly); }
			set { Set(value, e => e.IncreaseOnly); }
		}

		/// <summary>
		/// Gets or sets the spike detection.
		/// </summary>
		/// <value>
		/// The spike detection.
		/// </value>
		public double? SpikeDetection {
			get { return Get(e => e.SpikeDetection); }
			set { Set(value, e => e.SpikeDetection); }
		}

		/// <summary>
		/// Gets or sets the spike detection abs threshold.
		/// </summary>
		/// <value>
		/// The spike detection abs threshold.
		/// </value>
		public double? SpikeDetectionAbsThreshold {
			get { return Get(e => e.SpikeDetectionAbsThreshold); }
			set { Set(value, e => e.SpikeDetectionAbsThreshold); }
		}

		/// <summary>
		/// Gets or sets the max recurring values.
		/// </summary>
		/// <value>
		/// The max recurring values.
		/// </value>
		public int? MaxRecurringValues {
			get { return Get(e => e.MaxRecurringValues); }
			set { Set(value, e => e.MaxRecurringValues); }
		}

		/// <summary>
		/// Gets or sets the min value.
		/// </summary>
		/// <value>
		/// The min value.
		/// </value>
		public double? MinValue {
			get { return Get(e => e.MinValue); }
			set { Set(value, e => e.MinValue); }
		}

		/// <summary>
		/// Gets or sets the max value.
		/// </summary>
		/// <value>
		/// The max value.
		/// </value>
		public double? MaxValue {
			get { return Get(e => e.MaxValue); }
			set { Set(value, e => e.MaxValue); }
		}

		/// <summary>
		/// Gets or sets the emails.
		/// </summary>
		/// <value>
		/// The emails.
		/// </value>
		public string Emails {
			get { return Get(e => e.Emails); }
			set { Set(value, e => e.Emails); }
		}

		/// <summary>
		/// Gets or sets the key localization culture.
		/// </summary>
		/// <value>
		/// The key localization culture.
		/// </value>
		public string KeyLocalizationCulture {
			get { return Get(e => e.KeyLocalizationCulture); }
			set { Set(value, e => e.KeyLocalizationCulture); }
		}

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		/// <value>
		/// The start date.
		/// </value>
		public DateTime? StartDate {
			get { return Get(e => e.StartDate); }
			set { Set(value, e => e.StartDate); }
		}

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		/// <value>
		/// The end date.
		/// </value>
		public DateTime? EndDate {
			get { return Get(e => e.EndDate); }
			set { Set(value, e => e.EndDate); }
		}

		/// <summary>
		/// Gets or sets the last process date time.
		/// </summary>
		/// <value>
		/// The last process date time.
		/// </value>
		public DateTime? LastProcessDateTime {
			get { return Get(e => e.LastProcessDateTime); }
			set { Set(value, e => e.LastProcessDateTime); }
		}

		/// <summary>
		/// Gets or sets the filter meter classification.
		/// This is a many-to-many mapping.
		/// </summary>
		public DefaultMappingCrudStoreMapping<MeterValidationRule, ClassificationItem> FilterMeterClassification { get; set; }

		/// <summary>
		/// Gets or sets the filter spatial.
		/// This is a many-to-many mapping.
		/// </summary>
		public DefaultLocationMappingCrudStoreMapping<MeterValidationRule> FilterSpatial { get; set; }

		/// <summary>
		/// Gets or sets the meters.
		/// This is a many-to-many mapping.
		/// </summary>
		public DefaultMappingCrudStoreMapping<MeterValidationRule, Meter> Meters { get; set; }

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			var entities = new List<IMappingRecordConverter>();

			if (FilterMeterClassification != null) {
				FilterMeterClassification.ParentLocalId = LocalId;
				FilterMeterClassification.PropertyName = "FilterMeterClassification";
				entities.Add(FilterMeterClassification);
			}

			if (FilterSpatial != null) {
				FilterSpatial.ParentLocalId = LocalId;
				entities.Add(FilterSpatial);
			}

			if (Meters != null) {
				Meters.ParentLocalId = LocalId;
				entities.Add(Meters);
			}

			return entities;
		}
	}
}

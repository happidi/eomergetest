﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the Palette entity.
	/// </summary>
	[MappingSchema("Palette", "Default", "1.0.0")]
	public class DefaultPaletteMapping : Mapping<Palette> {

		/// <summary>
		/// Gets or sets the name of the localized label of the  Palette.
		/// </summary>
		public string MsgCode {
			get { return Get(e => e.MsgCode); }
			set { Set(value, e => e.MsgCode); }
		}

		/// <summary>
		/// List of  Palette Colors.
		/// </summary>
		public List<DefaultPaletteColorMapping> Colors { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		public string Creator {
			get { return Get(e => e.Creator); }
			set { Set(value, e => e.Creator); }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Populates the business entity before it is outputted.
		/// </summary>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			SetColorsPaletteLocalId();
			List<PaletteColor> colors = Colors.Select(c => c.GetBusinessEntity()).ToList();

			Set(colors, e => e.Colors);
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			SetColorsPaletteLocalId();

			return Colors;
		}

		private void SetColorsPaletteLocalId() {
			foreach (DefaultPaletteColorMapping color in Colors) {
				color.PaletteLocalId = LocalId;
			}
		}
	}
}
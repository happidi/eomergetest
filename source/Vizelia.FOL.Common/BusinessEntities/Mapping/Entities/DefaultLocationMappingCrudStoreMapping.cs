using System.Collections.Generic;
using System.Linq;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the MappingCrudStore entity that its children are Location entities.
	/// </summary>
	/// <typeparam name="TParent">The type of the parent.</typeparam>
	// This mapping is not described using the MappingSchema attribute as it is not mappable as a standalone object.
	// It is mappable only as a property of another object.
	public class DefaultLocationMappingCrudStoreMapping<TParent> : DefaultMappingCrudStoreMapping<TParent, Location> where TParent : BaseBusinessEntity, new() {
		
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultLocationMappingCrudStoreMapping&lt;TParent&gt;"/> class.
		/// Do not use this constructor, use the other one instead.
		/// </summary>
		public DefaultLocationMappingCrudStoreMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultLocationMappingCrudStoreMapping&lt;TParent&gt;"/> class.
		/// </summary>
		/// <param name="locations">The locations.</param>
		/// <param name="parentLocalId">The parent local id.</param>
		public DefaultLocationMappingCrudStoreMapping(IEnumerable<Location> locations, string parentLocalId) : base(Enumerable.Empty<string>(), parentLocalId) {
			AddAssociatedLocations(locations);
		}

		/// <summary>
		/// Adds the associated locations to the AssociatedEntities property.
		/// </summary>
		/// <param name="locations">The locations.</param>
		public void AddAssociatedLocations(IEnumerable<Location> locations) {
			var locationRepresentations = locations.Select(FormatLocationRepresentation);
			AssociatedEntities.AddRange(locationRepresentations);
		}

		/// <summary>
		/// Formats the location representation.
		/// </summary>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		private static string FormatLocationRepresentation(Location location) {
			string representation = string.Format("{0}:{1}", location.TypeName, location.LocalId);

			return representation;
		}

		/// <summary>
		/// Creates the child entity.
		/// </summary>
		/// <param name="localId">The local id.</param>
		protected override Location CreateChildEntity(string localId) {
			var location = new Location {KeyLocation = localId};
			return location;
		}
	}
}
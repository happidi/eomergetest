﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the Application Group entity.
	/// </summary>
	[MappingSchema("ApplicationGroup", "Default", "1.0.0")]
	public class DefaultApplicationGroupMapping : Mapping<ApplicationGroup> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultApplicationGroupMapping"/> class.
		/// </summary>
		public DefaultApplicationGroupMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultApplicationGroupMapping"/> class.
		/// </summary>
		/// <param name="applicationGroup">The application group.</param>
		public DefaultApplicationGroupMapping(ApplicationGroup applicationGroup)
			: base(applicationGroup) {

			// Many to many
			if (applicationGroup.Users != null) {
				var usersLocalIds = applicationGroup.Users.Select(x => x.LocalId).ToList();
				Users = new DefaultMappingCrudStoreMapping<ApplicationGroup, FOLMembershipUser>(usersLocalIds, LocalId);
			}

			if (applicationGroup.Roles != null) {
				var rolesLocalIds = applicationGroup.Roles.Select(x => x.LocalId).ToList();
				Roles = new DefaultMappingCrudStoreMapping<ApplicationGroup, AuthorizationItem>(rolesLocalIds, LocalId);
			}
		}

		/// <summary>
		/// The id of the authorization item.
		/// </summary>
		public string Id {
			get { return Get(e => e.Id); }
			set { Set(value, e => e.Id); }
		}

		/// <summary>
		/// The LocalId of the item.
		/// The dummy setter is there only for serialization purpose.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The name of the authorization item.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// The description of the authorization item.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// The list of users of the ApplicationGroup.
		/// This field is defined only in order to get the list from a xml mapping document.
		/// Otherwise it should remain null.
		/// </summary>
		public DefaultMappingCrudStoreMapping<ApplicationGroup, FOLMembershipUser> Users { get; set; }

		/// <summary>
		/// The list of roles of the ApplicationGroup.
		/// This field is defined only in order to get the list from a xml mapping document.
		/// Otherwise it should remain null.
		/// </summary>
		public DefaultMappingCrudStoreMapping<ApplicationGroup, AuthorizationItem> Roles { get; set; }

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			var entities = new List<IMappingRecordConverter>();

			if (Users != null) {
				Users.ParentLocalId = LocalId;
				entities.Add(Users);
			}

			if (Roles != null) {
				Roles.ParentLocalId = LocalId;
				entities.Add(Roles);
			}

			return entities;
		}
	}
}
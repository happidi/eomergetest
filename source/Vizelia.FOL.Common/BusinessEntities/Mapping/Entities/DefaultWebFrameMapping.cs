﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of the Web Frame entity.
	/// </summary>
	[MappingSchema("WebFrame", "Default", "1.0.0")]
	public class DefaultWebFrameMapping : Mapping<WebFrame> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultWebFrameMapping"/> class.
		/// </summary>
		public DefaultWebFrameMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultWebFrameMapping"/> class.
		/// </summary>
		/// <param name="webFrame">The WebFrame.</param>
		public DefaultWebFrameMapping(WebFrame webFrame)
			: base(webFrame) {
		}


		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the title of the WebFrame.
		/// </summary>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Gets or sets the Url of the WebFrame.
		/// </summary>
		public string Url {
			get { return Get(e => e.Url); }
			set { Set(value, e => e.Url); }
		}

		/// <summary>
		/// Gets or sets the HTML Body of the WebFrame.
		/// </summary>
		public string Body {
			get { return Get(e => e.Body); }
			set { Set(value, e => e.Body); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		public bool IsFavorite {
			get { return Get(e => e.IsFavorite); }
			set { Set(value, e => e.IsFavorite); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		public bool OpenOnStartup {
			get { return Get(e => e.OpenOnStartup); }
			set { Set(value, e => e.OpenOnStartup); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
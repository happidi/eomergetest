﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of an ActionRequest entity.
	/// </summary>
	[MappingSchema("ActionRequest", "Default", "1.0.0")]
	public class DefaultActionRequestMapping : PsetEnabledMapping<ActionRequest> {

		/// <summary>
		/// The external identifier of the ActionRequest.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the state.
		/// </summary>
		/// <value>The state.</value>
		public string State {
			get { return Get(e => e.State); }
			set { Set(value, e => e.State); }
		}

		/// <summary>
		/// The Id of the ActionRequest.
		/// </summary>
		public string RequestID {
			get { return Get(e => e.RequestID); }
			set { Set(value, e => e.RequestID); }
		}

		/// <summary>
		/// The local id of the ActionRequest priority.
		/// </summary>
		public int PriorityLocalId {
			get { return Get(e => e.PriorityID); }
			set { Set(value, e => e.PriorityID); }
		}

		/// <summary>
		/// The description of the ActionRequest.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// The KeyActor of the requestor.
		/// </summary>
		public string Requestor {
			get { return Get(e => e.Requestor); }
			set { Set(value, e => e.Requestor); }
		}

		/// <summary>
		/// The local id of the parent ClassificationItem.
		/// </summary>
		public string ClassificationParentLocalId {
			get { return Get(e => e.ClassificationKeyParent); }
			set { Set(value, e => e.ClassificationKeyParent); }
		}

		/// <summary>
		/// The local id of the child ClassificationItem.
		/// </summary>
		public string ClassificationChildrenLocalId {
			get { return Get(e => e.ClassificationKeyChildren); }
			set { Set(value, e => e.ClassificationKeyChildren); }
		}

		/// <summary>
		/// The creation date of the ActionRequest.
		/// </summary>
		public DateTime? ActualStart {
			get { return Get(e => e.ActualStart); }
			set { Set(value, e => e.ActualStart); }
		}

		/// <summary>
		/// The schedule date of the ActionRequest.
		/// </summary>
		public DateTime? ScheduleStart {
			get { return Get(e => e.ScheduleStart); }
			set { Set(value, e => e.ScheduleStart); }
		}

		/// <summary
		/// >
		/// The close date of the ActionRequest.
		/// </summary>
		public DateTime? CloseDateTime {
			get { return Get(e => e.CloseDateTime); }
			set { Set(value, e => e.CloseDateTime); }
		}

		/// <summary>
		/// The local id of the location of the ActionRequest.
		/// </summary>
		public string LocationLocalId {
			get { return Get(e => e.KeyLocation); }
			set { Set(value, e => e.KeyLocation); }
		}


		/// <summary>
		/// The location typename of the ActionRequest.
		/// </summary>
		public HierarchySpatialTypeName LocationTypeName {
			get { return Get(e => e.LocationTypeName); }
			set { Set(value, e => e.LocationTypeName); }
		}

		/// <summary>
		/// The key of the approver.
		/// </summary>
		public string ApproverLocalId {
			get { return Get(e => e.KeyApprover); }
			set { Set(value, e => e.KeyApprover); }
		}

		/// <summary>
		/// The local id of the user that last modified the entity.
		/// </summary>
		/// <value></value>
		public string ModifiedBy {
			get { return Get(e => e.AuditModifiedBy); }
			set { Set(value, e => e.AuditModifiedBy); }
		}

		/// <summary>
		/// The date and time of the last modification.
		/// </summary>
		/// <value></value>
		public DateTime? TimeLastModified {
			get { return Get(e => e.AuditTimeLastModified); }
			set { Set(value, e => e.AuditTimeLastModified); }
		}

		/// <summary>
		/// Gets or sets the state label.
		/// </summary>
		/// <value>The state label.</value>
		public string StateLabel {
			get { return Get(e => e.StateLabel); }
			set { Set(value, e => e.StateLabel); }
		}
		
		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
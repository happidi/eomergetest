﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the Flip Card entity.
	/// </summary>
	[MappingSchema("FlipCard", "Default", "1.0.0")]
	public class DefaultFlipCardMapping : Mapping<FlipCard> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultFlipCardMapping"/> class.
		/// </summary>
		public DefaultFlipCardMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultFlipCardMapping"/> class.
		/// </summary>
		/// <param name="flipCard">The FlipCard.</param>
		public DefaultFlipCardMapping(FlipCard flipCard)
			: base(flipCard) {
				
			if (flipCard.Cards != null) {
					Cards = new List<DefaultFlipCardEntryMapping>();
					Cards.AddRange(flipCard.Cards.Select(d => new DefaultFlipCardEntryMapping(d) { FlipCardLocalId = LocalId }));
				}
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		public bool IsFavorite {
			get { return Get(e => e.IsFavorite); }
			set { Set(value, e => e.IsFavorite); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		public bool OpenOnStartup {
			get { return Get(e => e.OpenOnStartup); }
			set { Set(value, e => e.OpenOnStartup); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="DefaultFlipCardMapping"/> is random.
		/// </summary>
		/// <value>
		///   <c>true</c> if random; otherwise, <c>false</c>.
		/// </value>
		public bool Random {
			get { return Get(e => e.Random); }
			set { Set(value, e => e.Random); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [flip on refresh].
		/// </summary>
		/// <value>
		///   <c>true</c> if [flip on refresh]; otherwise, <c>false</c>.
		/// </value>
		public bool FlipOnRefresh {
			get { return Get(e => e.FlipOnRefresh); }
			set { Set(value, e => e.FlipOnRefresh); }
		}

		/// <summary>
		/// Gets or sets the cards.
		/// </summary>
		/// <value>
		/// The cards.
		/// </value>
		[XmlArray("Cards")]
		[XmlArrayItem("FlipCardEntry", typeof(DefaultFlipCardEntryMapping))]
		public List<DefaultFlipCardEntryMapping> Cards { get; set; }

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			if (Cards != null) {
				List<FlipCardEntry> flipCardEntrys = Cards.Select(flipCardEntryMapping => flipCardEntryMapping.GetBusinessEntity()).ToList();

				foreach (var flipCardEntry in flipCardEntrys) {
					flipCardEntry.KeyFlipCard = LocalId;
				}

				Set(flipCardEntrys, e => e.Cards);
			}
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			if (Cards == null) {
				return Enumerable.Empty<IMappingRecordConverter>();
			}

			foreach (DefaultFlipCardEntryMapping flipCardEntryMapping in Cards) {
				flipCardEntryMapping.FlipCardLocalId = LocalId;
			}

			return Cards;
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping.Entities {
	/// <summary>
	/// A mapping for the RESTDataAcquisitionEndpoint entity.
	/// </summary>
	[MappingSchema("RESTDataAcquisitionEndpoint", "Default", "1.0.0")]
	public class DefaultRESTDataAcquisitionEndpointMapping : Mapping<RESTDataAcquisitionEndpoint> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultRESTDataAcquisitionEndpointMapping"/> class.
		/// </summary>
		public DefaultRESTDataAcquisitionEndpointMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultRESTDataAcquisitionEndpointMapping"/> class.
		/// </summary>
		/// <param name="restDataAcquisitionEndpoint">The rest data acquisition endpoint.</param>
		public DefaultRESTDataAcquisitionEndpointMapping(RESTDataAcquisitionEndpoint restDataAcquisitionEndpoint)
			: base(restDataAcquisitionEndpoint) {

		}

		/// <summary>
		/// The internal identifier.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the query string that will be appended to the URL when a query is issued.
		/// The Url for the REST query is made out of container.Url+Path+QueryString
		/// </summary>
		/// <value>
		/// The query.
		/// </value>
		public string QueryString {
			get { return Get(e => e.QueryString); }
			set { Set(value, e => e.QueryString); }
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Gets or sets the key data acquisition endpoint.
		/// </summary>
		/// <value>
		/// The key data acquisition endpoint.
		/// </value>
		public string KeyDataAcquisitionContainer {
			get { return Get(e => e.KeyDataAcquisitionContainer); }
			set { Set(value, e => e.KeyDataAcquisitionContainer); }
		}

		/// <summary>
		/// Gets or sets the key data acquisition endpoint.
		/// </summary>
		/// <value>
		/// The key data acquisition endpoint.
		/// </value>
		public string KeyDataAcquisitionEndpoint {
			get { return Get(e => e.KeyDataAcquisitionEndpoint); }
			set { Set(value, e => e.KeyDataAcquisitionEndpoint); }
		}

		/// <summary>
		/// Gets or sets the path.
		/// </summary>
		/// <value>
		/// The path.
		/// </value>
		public string Path {
			get { return Get(e => e.Path); }
			set { Set(value, e => e.Path); }
		}

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// Gets or sets the unit.
		/// </summary>
		public string Unit {
			get { return Get(e => e.Unit); }
			set { Set(value, e => e.Unit); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}

using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a Chart Axis entity.
	/// </summary>
	[MappingSchema("ChartAxis", "Default", "1.0.0")]
	public class DefaultChartAxisMapping : Mapping<ChartAxis> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultChartAxisMapping"/> class.
		/// </summary>
		public DefaultChartAxisMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultChartAxisMapping"/> class.
		/// </summary>
		/// <param name="chartAxis">The chart axis.</param>
		public DefaultChartAxisMapping(ChartAxis chartAxis)
			: base(chartAxis) {
			if (chartAxis.Markers != null) {
				Markers = chartAxis.Markers.Select(m => new DefaultChartMarkerMapping(m) { ChartAxisLocalId = LocalId }).ToList();
			}
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the parent chart local id.
		/// </summary>
		public string ChartLocalId {
			get { return Get(e => e.KeyChart); }
			set { Set(value, e => e.KeyChart); }
		}

		/// <summary>
		/// Gets or sets the name of the axis.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Gets or sets the minimum value of the axis.
		/// </summary>
		public double? Min {
			get { return Get(e => e.Min); }
			set { Set(value, e => e.Min); }
		}

		/// <summary>
		/// Gets or sets the maximum value of the axis.
		/// </summary>
		public double? Max {
			get { return Get(e => e.Max); }
			set { Set(value, e => e.Max); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this axis is primary.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this axis is primary; otherwise, <c>false</c>.
		/// </value>
		public bool IsPrimary {
			get { return Get(e => e.IsPrimary); }
			set { Set(value, e => e.IsPrimary); }
		}

		/// <summary>
		/// Gets or sets the markers.
		/// </summary>
		[XmlArray("Markers")]
		[XmlArrayItem("Marker", typeof(DefaultChartMarkerMapping))]
		public List<DefaultChartMarkerMapping> Markers { get; set; }

		/// <summary>
		/// Populates the business entity before it is outputted.
		/// </summary>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			List<ChartMarker> markers = Markers.Select(m => m.GetBusinessEntity()).ToList();
			foreach (ChartMarker marker in markers) {
				marker.KeyChartAxis = LocalId;
			}

			Set(markers, e => e.Markers);
		}


		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Markers;
		}
	}
}
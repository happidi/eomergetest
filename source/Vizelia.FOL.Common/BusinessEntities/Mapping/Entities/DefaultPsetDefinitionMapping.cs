﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping{
	/// <summary>
	/// A mapping for the PsetDefinition entity.
	/// </summary>
	[MappingSchema("PsetDefinition", "Default", "1.0.0")]
	public class DefaultPsetDefinitionMapping : Mapping<PsetDefinition>{
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPsetDefinitionMapping"/> class.
		/// </summary>
		public DefaultPsetDefinitionMapping()
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPsetDefinitionMapping"/> class.
		/// </summary>
		/// <param name="psetDefinition">The pset definition.</param>
		public DefaultPsetDefinitionMapping(PsetDefinition psetDefinition):base(psetDefinition)
		{
			
		}

		/// <summary>
		/// The Key of the Pset.
		/// </summary>
		public string KeyPropertySet
		{
			get { return Get(e => e.KeyPropertySet); }
			set { Set(value, e => e.KeyPropertySet); }
		}

		/// <summary>
		/// The Name of the Pset.
		/// </summary>
		public string PsetName {
			get { return Get(e => e.PsetName); }
			set { Set(value, e => e.PsetName); }
		}


		/// <summary>
		/// The name of the object the Pset is linked to.
		/// </summary>
		public string UsageName {
			get { return Get(e => e.UsageName); }
			set { Set(value, e => e.UsageName); }
		}



		/// <summary>
		/// Gets or sets the description of the pset.
		/// </summary>
		/// <value>The Description.</value>
		public string PsetDescription {
			get { return Get(e => e.PsetDescription); }
			set { Set(value, e => e.PsetDescription); }
		}

		

		/// <summary>
		/// Gets or sets the attributes of the pset.
		/// </summary>
		public List<PsetAttributeDefinition> Attributes { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the MSG code.
		/// </summary>
		/// <value>
		/// The MSG code.
		/// </value>
		public string MsgCode {
			get { return Get(e => e.MsgCode); }
			set { Set(value, e => e.MsgCode); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings()
		{
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}

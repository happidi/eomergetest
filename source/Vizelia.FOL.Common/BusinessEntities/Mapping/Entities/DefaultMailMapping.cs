﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of the Map entity.
	/// </summary>
	[MappingSchema("Mail", "Default", "1.0.0")]
	public class DefaultMailMapping : Mapping<Mail> {

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the key mail.
		/// </summary>
		public string KeyMail {
			get { return Get(e => e.KeyMail); }
			set { Set(value, e => e.KeyMail); }
		}

		/// <summary>
		/// The Subject of the Mail.
		/// </summary>
		public string Subject {
			get { return Get(e => e.Subject); }
			set { Set(value, e => e.Subject); }
		}

		/// <summary>
		/// The Name of the Mail.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// The Body of the Mail.
		/// </summary>
		public string Body {
			get { return Get(e => e.Body); }
			set { Set(value, e => e.Body); }
		}

		/// <summary>
		/// True if the mail is active, false otherwise.
		/// </summary>
		public bool IsActive {
			get { return Get(e => e.IsActive); }
			set { Set(value, e => e.IsActive); }
		}

		/// <summary>
		/// The priority of this e-mail message.
		/// </summary>
		public MailPriority Priority {
			get { return Get(e => e.Priority); }
			set { Set(value, e => e.Priority); }
		}

		/// <summary>
		/// The from address for this e-mail message.
		/// </summary>
		public string From {
			get { return Get(e => e.From); }
			set { Set(value, e => e.From); }
		}

		/// <summary>
		/// The address collection that contains the recipients of this e-mail message.
		/// </summary>
		public string To {
			get { return Get(e => e.To); }
			set { Set(value, e => e.To); }
		}

		/// <summary>
		/// The address collection that contains the carbon copy (CC) recipients for this e-mail message.
		/// </summary>
		public string CC {
			get { return Get(e => e.CC); }
			set { Set(value, e => e.CC); }
		}

		/// <summary>
		/// The address collection that contains the blind carbon copy (BCC) recipients for this e-mail message.
		/// </summary>
		public string Bcc {
			get { return Get(e => e.Bcc); }
			set { Set(value, e => e.Bcc); }
		}

		/// <summary>
		/// The key of the classification of the Meter.
		/// </summary>
		public string KeyClassificationItem {
			get { return Get(e => e.KeyClassificationItem); }
			set { Set(value, e => e.KeyClassificationItem); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
    /// <summary>
    /// A mapping of a Meter Operation.
    /// </summary>
    [MappingSchema("MeterOperation", "Default", "1.0.0")]
    public class DefaultMeterOperationMapping : Mapping<MeterOperation> {

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultMeterOperationMapping"/> class.
        /// </summary>
        public DefaultMeterOperationMapping() {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultMeterOperationMapping"/> class.
        /// </summary>
        /// <param name="meterOperation">The meter operation.</param>
        public DefaultMeterOperationMapping(MeterOperation meterOperation)
            : base(meterOperation) {

        }

        /// <summary>
        /// Local Id of the MeterOperation
        /// </summary>
        public string LocalId {
            get { return Get(e => e.LocalId); }
            set { Set(value, e => e.LocalId); }
        }

        /// <summary>
        /// Local id of the Meter the operation applys to.
        /// </summary>
        public string MeterLocalId {
            get { return Get(e => e.KeyMeter); }
            set { Set(value, e => e.KeyMeter); }
        }

        /// <summary>
        /// Order of the MeterOperation
        /// </summary>
        public int Order {
            get { return Get(e => e.Order); }
            set { Set(value, e => e.Order); }
        }

        /// <summary>
        /// Factor that will multiply data coming from Meter1
        /// </summary>
        public double Factor1 {
            get { return Get(e => e.Factor1); }
            set { Set(value, e => e.Factor1); }
        }

        /// <summary>
        /// Function to be applied to the Meter 1 or Meter Operation 1.
        /// </summary>
        public AlgebricFunction Function1 {
            get { return Get(e => e.Function1); }
            set { Set(value, e => e.Function1); }
        }

		/// <summary>
		/// Offset that will be added to data coming from Meter1
		/// </summary>
		public double? Offset1 {
			get { return Get(e => e.Offset1); }
			set { Set(value, e => e.Offset1); }
		}
		
		/// <summary>
        /// Local id of the Meter 1.
        /// </summary>
        public string Meter1LocalId {
            get { return Get(e => e.KeyMeter1); }
            set { Set(value, e => e.KeyMeter1); }
        }

        /// <summary>
        /// Key of the Meter Operation 1.
        /// </summary>
        public string KeyMeterOperation1 {
            get { return Get(e => e.KeyMeterOperation1); }
            set { Set(value, e => e.KeyMeterOperation1); }
        }

        /// <summary>
        /// Arithmetic Operator beetween the 2 Meters.
        /// </summary>
        public ArithmeticOperator Operator {
            get { return Get(e => e.Operator); }
            set { Set(value, e => e.Operator); }
        }

        /// <summary>
        /// Factor that will multiply data coming from Meter1
        /// </summary>
        public double Factor2 {
            get { return Get(e => e.Factor2); }
            set { Set(value, e => e.Factor2); }
        }

        /// <summary>
        /// Function to be applied to the Meter 2 or Meter Operation 2.
        /// </summary>
        public AlgebricFunction Function2 {
            get { return Get(e => e.Function2); }
            set { Set(value, e => e.Function2); }
        }

		/// <summary>
		/// Offset that will be added to data coming from Meter2
		/// </summary>
		public double? Offset2 {
			get { return Get(e => e.Offset2); }
			set { Set(value, e => e.Offset2); }
		}
		
		/// <summary>
        /// Key of the Meter 2.
        /// </summary>
        public string Meter2LocalId {
            get { return Get(e => e.KeyMeter2); }
            set { Set(value, e => e.KeyMeter2); }
        }

        /// <summary>
        /// Key of the Meter Operation 2.
        /// </summary>
        public string KeyMeterOperation2 {
            get { return Get(e => e.KeyMeterOperation2); }
            set { Set(value, e => e.KeyMeterOperation2); }
        }

        /// <summary>
        /// Gets the additional fields.
        /// </summary>
        protected override Dictionary<string, string> GetAdditionalFields() {
            return new Dictionary<string, string>();
        }

        /// <summary>
        /// Gets the child mappings.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
            return Enumerable.Empty<IMappingRecordConverter>();
        }
    }
}
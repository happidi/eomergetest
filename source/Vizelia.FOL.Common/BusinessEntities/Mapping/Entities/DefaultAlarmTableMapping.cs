﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the Alarm Table entity.
	/// </summary>
	[MappingSchema("AlarmTable", "Default", "1.0.0")]
	public class DefaultAlarmTableMapping : Mapping<AlarmTable> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultAlarmTableMapping"/> class.
		/// </summary>
		public DefaultAlarmTableMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultAlarmTableMapping"/> class.
		/// </summary>
		/// <param name="alarmTable">The AlarmTable.</param>
		public DefaultAlarmTableMapping(AlarmTable alarmTable)
			: base(alarmTable) {
				if (alarmTable.AlarmDefinitions != null) {
					var alarmDefinitionsLocalIds = alarmTable.AlarmDefinitions.Select(m => m.LocalId).ToList();
					AlarmDefinitions = new DefaultMappingCrudStoreMapping<AlarmTable, AlarmDefinition>(alarmDefinitionsLocalIds, LocalId);
				}
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Gets or sets the alarm instance status.
		/// </summary>
		/// <value>
		/// The alarm instance status.
		/// </value>
		public AlarmInstanceStatus AlarmInstanceStatus {
			get { return Get(e => e.AlarmInstanceStatus); }
			set { Set(value, e => e.AlarmInstanceStatus); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		public bool IsFavorite {
			get { return Get(e => e.IsFavorite); }
			set { Set(value, e => e.IsFavorite); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		public bool OpenOnStartup {
			get { return Get(e => e.OpenOnStartup); }
			set { Set(value, e => e.OpenOnStartup); }
		}

		/// <summary>
		/// Gets or sets the alarm definitions.
		/// </summary>
		/// <value>
		/// The alarm definitions.
		/// </value>
		public DefaultMappingCrudStoreMapping<AlarmTable, AlarmDefinition> AlarmDefinitions { get; set; }

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			var entities = new List<IMappingRecordConverter>();

			if (AlarmDefinitions != null) {
				AlarmDefinitions.ParentLocalId = LocalId;
				entities.Add(AlarmDefinitions);
			}

			return entities;
		}
	}
}
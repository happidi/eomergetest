using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the Playlist Page entity.
	/// </summary>
	[MappingSchema("PlaylistPage", "Default", "1.0.0")]
	public class DefaultPlaylistPageMapping : Mapping<PlaylistPage> {

				/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPlaylistPageMapping"/> class.
		/// </summary>
		public DefaultPlaylistPageMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPlaylistPageMapping"/> class.
		/// </summary>
		/// <param name="playlistPage">The PlaylistPage.</param>
		public DefaultPlaylistPageMapping(PlaylistPage playlistPage)
			: base(playlistPage) {
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Local id of the Playlist.
		/// </summary>
		public string PlaylistLocalId {
			get { return Get(e => e.KeyPlaylist); }
			set { Set(value, e => e.KeyPlaylist); }
		}

		/// <summary>
		/// Gets or sets the order of the page in the playlist.
		/// </summary>
		public int Order {
			get { return Get(e => e.Order); }
			set { Set(value, e => e.Order); }
		}

		/// <summary>
		/// Gets or sets the duration in second the page will be displayed in the playlist.
		/// </summary>
		public int Duration {
			get { return Get(e => e.Duration); }
			set { Set(value, e => e.Duration); }
		}

		/// <summary>
		/// Gets or sets the Key of the PortalTab the page is representing.
		/// </summary>
		public string PortalTabLocalId {
			get { return Get(e => e.KeyPortalTab); }
			set { Set(value, e => e.KeyPortalTab); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
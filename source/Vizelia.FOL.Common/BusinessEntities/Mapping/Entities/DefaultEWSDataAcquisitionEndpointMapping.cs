﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping.Entities {
	/// <summary>
	/// A mapping for the EWSDataAcquisitionEndpoint entity.
	/// </summary>
	[MappingSchema("EWSDataAcquisitionEndpoint", "Default", "1.0.0")]
	public class DefaultEWSDataAcquisitionEndpointMapping : Mapping<EWSDataAcquisitionEndpoint> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultEWSDataAcquisitionEndpointMapping"/> class.
		/// </summary>
		public DefaultEWSDataAcquisitionEndpointMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultEWSDataAcquisitionEndpointMapping"/> class.
		/// </summary>
		/// <param name="ewsDataAcquisitionEndpoint">The ews data acquisition endpoint.</param>
		public DefaultEWSDataAcquisitionEndpointMapping(EWSDataAcquisitionEndpoint ewsDataAcquisitionEndpoint)
			: base(ewsDataAcquisitionEndpoint) {

		}

		/// <summary>
		/// The internal identifier.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The external identifier.
		/// </summary>
		public string ServerLocalId {
			get { return Get(e => e.ServerLocalId); }
			set { Set(value, e => e.ServerLocalId); }
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Gets or sets the key data acquisition endpoint.
		/// </summary>
		/// <value>
		/// The key data acquisition endpoint.
		/// </value>
		public string KeyDataAcquisitionContainer {
			get { return Get(e => e.KeyDataAcquisitionContainer); }
			set { Set(value, e => e.KeyDataAcquisitionContainer); }
		}

		/// <summary>
		/// Gets or sets the key data acquisition endpoint.
		/// </summary>
		/// <value>
		/// The key data acquisition endpoint.
		/// </value>
		public string KeyDataAcquisitionEndpoint {
			get { return Get(e => e.KeyDataAcquisitionEndpoint); }
			set { Set(value, e => e.KeyDataAcquisitionEndpoint); }
		}

		/// <summary>
		/// Gets or sets the path.
		/// </summary>
		/// <value>
		/// The path.
		/// </value>
		public string Path {
			get { return Get(e => e.Path); }
			set { Set(value, e => e.Path); }
		}

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}

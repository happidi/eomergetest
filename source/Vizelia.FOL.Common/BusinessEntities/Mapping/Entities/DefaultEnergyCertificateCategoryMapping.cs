using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the Energy Certificate Category entity.
	/// </summary>
	[MappingSchema("EnergyCertificateCategory", "Default", "1.0.0")]
	public class DefaultEnergyCertificateCategoryMapping : Mapping<EnergyCertificateCategory> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultEnergyCertificateCategoryMapping"/> class.
		/// </summary>
		public DefaultEnergyCertificateCategoryMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultEnergyCertificateCategoryMapping"/> class.
		/// </summary>
		/// <param name="energyCertificateCategory">The EnergyCertificateCategory.</param>
		public DefaultEnergyCertificateCategoryMapping(EnergyCertificateCategory energyCertificateCategory)
			: base(energyCertificateCategory) {
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the key energy certificate.
		/// </summary>
		/// <value>
		/// The key energy certificate.
		/// </value>
		public string KeyEnergyCertificate {
			get { return Get(e => e.KeyEnergyCertificate); }
			set { Set(value, e => e.KeyEnergyCertificate); }
		}

		/// <summary>
		/// Local id of the Playlist.
		/// </summary>
		public string EnergyCertificateLocalId {
			get { return Get(e => e.KeyEnergyCertificate); }
			set { Set(value, e => e.KeyEnergyCertificate); }
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Gets or sets the min value.
		/// </summary>
		/// <value>
		/// The min value.
		/// </value>
		public double? MinValue {
			get { return Get(e => e.MinValue); }
			set { Set(value, e => e.MinValue); }
		}

		/// <summary>
		/// Gets or sets the max value.
		/// </summary>
		/// <value>
		/// The max value.
		/// </value>
		public double? MaxValue {
			get { return Get(e => e.MaxValue); }
			set { Set(value, e => e.MaxValue); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
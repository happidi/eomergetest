﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the RESTDataAcquisitionContainer entity.
	/// </summary>
	[MappingSchema("RESTDataAcquisitionContainer", "Default", "1.0.0")]
	public class DefaultRESTDataAcquisitionContainerMapping : Mapping<RESTDataAcquisitionContainer> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultRESTDataAcquisitionContainerMapping"/> class.
		/// </summary>
		public DefaultRESTDataAcquisitionContainerMapping() {

		}

		/// <summary>
		/// Gets or sets the key data acquisition container.
		/// </summary>
		/// <value>
		/// The key data acquisition container.
		/// </value>
		public string KeyDataAcquisitionContainer {
			get { return Get(e => e.KeyDataAcquisitionContainer); }
			set { Set(value, e => e.KeyDataAcquisitionContainer); }
		}


		/// <summary>
		/// The external identifier..
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}


		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Gets the fully qualified name of the provider.
		/// </summary>
		/// <returns></returns>
		public string ProviderType {
			get { return Get(e => e.ProviderType); }
			set { Set(value, e => e.ProviderType); }
		}

		/// <summary>
		/// Gets or sets the URL.
		/// </summary>
		/// <value>
		/// The URL.
		/// </value>
		public string Url {
			get { return Get(e => e.Url); }
			set { Set(value, e => e.Url); }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultRESTDataAcquisitionContainerMapping"/> class.
		/// </summary>
		/// <param name="restDataAcquisitionContainer">The rest data acquisition container.</param>
		public DefaultRESTDataAcquisitionContainerMapping(RESTDataAcquisitionContainer restDataAcquisitionContainer)
			: base(restDataAcquisitionContainer) {

		}

		/// <summary>
		/// XPath of the MeterId (used in the XPathRESTDataAcquisitionProvider).
		/// </summary>
		public string XPathMeterId {
			get { return Get(e => e.XPathMeterId); }
			set { Set(value, e => e.XPathMeterId); }
		}

		/// <summary>
		/// XPath of the AcquisitionDateTime (used in the XPathRESTDataAcquisitionProvider).
		/// </summary>
		public string XPathAcquisitionDateTime {
			get { return Get(e => e.XPathAcquisitionDateTime); }
			set { Set(value, e => e.XPathAcquisitionDateTime); }
		}

		/// <summary>
		/// XPath of the AcquisitionDateTime (used in the XPathRESTDataAcquisitionProvider).
		/// </summary>
		public string XPathValue {
			get { return Get(e => e.XPathValue); }
			set { Set(value, e => e.XPathValue); }
		}

		/// <summary>
		/// Gets or sets the attribute meter id (if not set, we ll use the inner data).
		/// </summary>
		public string AttributeMeterId {
			get { return Get(e => e.AttributeMeterId); }
			set { Set(value, e => e.AttributeMeterId); }
		}

		/// <summary>
		/// Gets or sets the attribute acquisition date time (if not set, we ll use the inner data).
		/// </summary>
		public string AttributeAcquisitionDateTime {
			get { return Get(e => e.AttributeAcquisitionDateTime); }
			set { Set(value, e => e.AttributeAcquisitionDateTime); }
		}

		/// <summary>
		/// Gets or sets the attribute value (if not set, we ll use the inner data).
		/// </summary>
		public string AttributeValue {
			get { return Get(e => e.AttributeValue); }
			set { Set(value, e => e.AttributeValue); }
		}

		/// <summary>
		/// Gets or sets the format acquisition date time.
		/// </summary>
		public string FormatAcquisitionDateTime {
			get { return Get(e => e.FormatAcquisitionDateTime); }
			set { Set(value, e => e.FormatAcquisitionDateTime); }
		}

		/// <summary>
		/// Gets or sets the Key of the LocalizationCulture.
		/// </summary>
		public string KeyLocalizationCulture {
			get { return Get(e => e.KeyLocalizationCulture); }
			set { Set(value, e => e.KeyLocalizationCulture); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}

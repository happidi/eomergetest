﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of a Calendar Event Category entity.
	/// </summary>
	[MappingSchema("CalendarEventCategory", "Default", "1.0.0")]
	public class DefaultCalendarEventCategory : Mapping<CalendarEventCategory> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultCalendarEventCategory"/> class.
		/// </summary>
		public DefaultCalendarEventCategory() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultCalendarEventCategory"/> class.
		/// </summary>
		/// <param name="calendarEventCategory">The calendar event category.</param>
		public DefaultCalendarEventCategory(CalendarEventCategory calendarEventCategory)
			: base(calendarEventCategory) {

		}

		/// <summary>
		/// The external identifier of the CalendarEventCategory.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the name of the localized label of the CalendarEventCategory.
		/// </summary>
		public string MsgCode {
			get { return Get(e => e.MsgCode); }
			set { Set(value, e => e.MsgCode); }
		}

		/// <summary>
		/// Red color composant.
		/// </summary>
		public int ColorR {
			get { return Get(e => e.ColorR); }
			set { Set(value, e => e.ColorR); }
		}

		/// <summary>
		/// Green color composant.
		/// </summary>
		public int ColorG {
			get { return Get(e => e.ColorG); }
			set { Set(value, e => e.ColorG); }
		}

		/// <summary>
		/// Blue color composant.
		/// </summary>
		public int ColorB {
			get { return Get(e => e.ColorB); }
			set { Set(value, e => e.ColorB); }
		}

		/// <summary>
		/// The ColorId to use with the Ext.ensible.calendar
		/// </summary>
		public string ColorId {
			get { return Get(e => e.ColorId); }
			set { Set(value, e => e.ColorId); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
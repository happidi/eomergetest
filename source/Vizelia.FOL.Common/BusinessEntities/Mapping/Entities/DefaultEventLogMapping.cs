﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of an Event Log entity.
	/// </summary>
	[MappingSchema("EventLog", "Default", "1.0.0")]
	public class DefaultEventLogMapping : Mapping<EventLog> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultEventLogMapping"/> class.
		/// </summary>
		public DefaultEventLogMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultEventLogMapping"/> class.
		/// </summary>
		/// <param name="eventLog">The event log.</param>
		public DefaultEventLogMapping(EventLog eventLog)
			: base(eventLog) {

		}
		/// <summary>
		/// The name of the Event Log item.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// The Description of the Event Log item.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// The local id of the Event Log item.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		public DateTime StartDate {
			get { return Get(e => e.StartDate); }
			set { Set(value, e => e.StartDate); }
		}

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		public DateTime EndDate {
			get { return Get(e => e.EndDate); }
			set { Set(value, e => e.EndDate); }
		}

		/// <summary>
		/// The local id of the location of the Event Log item.
		/// </summary>
		public string LocationLocalId {
			get { return Get(e => e.KeyLocation); }
			set { Set(value, e => e.KeyLocation); }
		}

		/// <summary>
		/// The location typename of the Event Log item.
		/// </summary>
		public HierarchySpatialTypeName LocationTypeName {
			get { return Get(e => e.LocationTypeName); }
			set { Set(value, e => e.LocationTypeName); }
		}


		/// <summary>
		/// The local id of the classification item of the Event Log item.
		/// </summary>
		public string ClassificationItemLocalId {
			get { return Get(e => e.KeyClassificationItem); }
			set { Set(value, e => e.KeyClassificationItem); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
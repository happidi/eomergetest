﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of a Portlet entity.
	/// </summary>
	[MappingSchema("Portlet", "Default", "1.0.0")]
	public class DefaultPortletMapping : Mapping<Portlet> {

		/// <summary>
		/// The local id of the Portlet.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Local id of the PortalColumn.
		/// </summary>
		public string PortalColumnLocalId {
			get { return Get(e => e.KeyPortalColumn); }
			set { Set(value, e => e.KeyPortalColumn); }
		}

		/// <summary>
		/// Gets or sets the key of the Portlet entity.
		/// </summary>
		public string KeyEntity {
			get { return Get(e => e.KeyEntity); }
			set { Set(value, e => e.KeyEntity); }
		}

		/// <summary>
		/// Gets or sets the type of the Portlet entity.
		/// </summary>
		public string TypeEntity {
			get { return Get(e => e.TypeEntity); }
			set { Set(value, e => e.TypeEntity); }
		}

		/// <summary>
		/// Gets or sets the title of the Portlet.
		/// </summary>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Get or sets the collapsed state of the Portlet.
		/// </summary>
		public bool Collapsed {
			get { return Get(e => e.Collapsed); }
			set { Set(value, e => e.Collapsed); }
		}

		/// <summary>
		/// Gets or sets the relative height of the Portlet.
		/// </summary>
		public int Flex {
			get { return Get(e => e.Flex); }
			set { Set(value, e => e.Flex); }
		}

		/// <summary>
		/// Gets or sets the order of the Portlet in the portal column.
		/// </summary>
		public int Order {
			get { return Get(e => e.Order); }
			set { Set(value, e => e.Order); }
		}


		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
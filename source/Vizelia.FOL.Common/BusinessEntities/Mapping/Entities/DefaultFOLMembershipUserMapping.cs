﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping for a FOL Membership User.
	/// </summary>
	[MappingSchema("User", "Default", "1.0.0")]
	public class DefaultFOLMembershipUserMapping : Mapping<FOLMembershipUser> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultFOLMembershipUserMapping"/> class.
		/// </summary>
		public DefaultFOLMembershipUserMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultFOLMembershipUserMapping"/> class.
		/// </summary>
		/// <param name="folMembershipUser">The fol membership user.</param>
		public DefaultFOLMembershipUserMapping(FOLMembershipUser folMembershipUser)
			: base(folMembershipUser) {
		}

		/// <summary>
		/// The user name.
		/// </summary>
		public string UserName {
			get { return Get(e => e.UserName); }
			set { Set(value, e => e.UserName); }
		}

		/// <summary>
		/// The LocalId of the User.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Returns true if the user is locked.
		/// </summary>
		public bool IsLockedOut {
			get { return Get(e => e.IsLockedOut); }
			set { Set(value, e => e.IsLockedOut); }
		}

		/// <summary>
		/// The password. This field is defined only in order to get the password from a xml mapping document.
		/// Otherwise it should never expose the password of the user.
		/// </summary>
		public string Password {
			get { return Get(e => e.Password); }
			set { Set(value, e => e.Password); }
		}

		/// <summary>
		/// Returns true if the user is approved.
		/// </summary>
		public bool IsApproved {
			get { return Get(e => e.IsApproved); }
			set { Set(value, e => e.IsApproved); }
		}

		/// <summary>
		/// The user's email.
		/// </summary>
		public string Email {
			get { return Get(e => e.Email); }
			set { Set(value, e => e.Email); }
		}

		/// <summary>
		/// Comments.
		/// </summary>
		public string Comment {
			get { return Get(e => e.Comment); }
			set { Set(value, e => e.Comment); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [user must change password at next logon].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [user must change password at next logon]; otherwise, <c>false</c>.
		/// </value>
		public bool UserMustChangePasswordAtNextLogon {
			get { return Get(e => e.UserMustChangePasswordAtNextLogon); }
			set { Set(value, e => e.UserMustChangePasswordAtNextLogon); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether the user password [never expires].
		/// </summary>
		/// <value>
		///   <c>true</c> if [never expires]; otherwise, <c>false</c>.
		/// </value>
		public bool NeverExpires {
			get {
				FOLProviderUserKey userKey = Get(e => e.ProviderUserKey);
				return userKey.NeverExpires;
			}
			set {
				FOLProviderUserKey userKey = Get(e => e.ProviderUserKey) ?? new FOLProviderUserKey();

				userKey.NeverExpires = value;
				Set(userKey, e => e.ProviderUserKey);
			}
		}

		/// <summary>
		/// Gets or sets the occupant id.
		/// </summary>
		/// <value>
		/// The occupant.
		/// </value>
		public string OccupantId {
			get {
				FOLProviderUserKey userKey = Get(e => e.ProviderUserKey);
				return userKey.KeyOccupant;
			}
			set {
				FOLProviderUserKey userKey = Get(e => e.ProviderUserKey) ?? new FOLProviderUserKey();

				userKey.KeyOccupant = value;
				Set(userKey, e => e.ProviderUserKey);
			}
		}

        /// <summary>
		/// The user's first name
		/// </summary>
		public string FirstName {
			get { return Get(e => e.FirstName); }
			set { Set(value, e => e.FirstName); }
		}
		
		/// <summary>
		/// The user's last name
		/// </summary>
		public string LastName {
			get { return Get(e => e.LastName); }
			set { Set(value, e => e.LastName); }
		}

		/// <summary>
		/// User preferences mapping
		/// </summary>
		public DefaultUserPreferencesMapping Preferences { get; set; }

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			var entities = new List<IMappingRecordConverter>();

			if (Preferences != null) {
				Preferences.LocalId = LocalId;
				entities.Add(Preferences);
			}

			return entities;
		}
	}
}
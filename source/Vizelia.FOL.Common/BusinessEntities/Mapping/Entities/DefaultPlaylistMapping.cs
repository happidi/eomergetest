﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of the Playlist entity.
	/// </summary>
	[MappingSchema("Playlist", "Default", "1.0.0")]
	public class DefaultPlaylistMapping : Mapping<Playlist> {

				/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPlaylistMapping"/> class.
		/// </summary>
		public DefaultPlaylistMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPlaylistMapping"/> class.
		/// </summary>
		/// <param name="playlist">The Playlist.</param>
		public DefaultPlaylistMapping(Playlist playlist)
			: base(playlist) {
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the Name.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Gets or sets the Key of the LocalizationCulture.
		/// </summary>
		// This is intentional as this will not be resolved by the broker since it's based on .NET CultureInfo.
		public string KeyLocalizationCulture {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Gets or sets the width of the Playlist.
		/// </summary>
		public int Width {
			get { return Get(e => e.Width); }
			set { Set(value, e => e.Width); }
		}

		/// <summary>
		/// Gets or sets the height of the Playlist.
		/// </summary>
		public int Height {
			get { return Get(e => e.Height); }
			set { Set(value, e => e.Height); }
		}

		/// <summary>
		/// List of  Playlist Page.
		/// </summary>
		[XmlArray("Pages")]
		[XmlArrayItem("Page", typeof(DefaultPlaylistPageMapping))]
		public List<DefaultPlaylistPageMapping> Pages { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		public string Creator {
			get { return Get(e => e.Creator); }
			set { Set(value, e => e.Creator); }
		}

		/// <summary>
		/// Populates the business entity before it is outputted.
		/// </summary>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			SetPagesLocalId();
			List<PlaylistPage> pages = Pages.Select(p => p.GetBusinessEntity()).ToList();
			Set(pages, e => e.Pages);
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			SetPagesLocalId();
			return Pages;
		}

		/// <summary>
		/// Sets the local id property on the pages list.
		/// </summary>
		private void SetPagesLocalId() {
			foreach (DefaultPlaylistPageMapping page in Pages) {
				page.PlaylistLocalId = LocalId;
			}
		}


	}
}
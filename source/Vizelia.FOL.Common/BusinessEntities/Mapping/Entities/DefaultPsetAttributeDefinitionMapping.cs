﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;
using Vizelia.FOL.BusinessEntities.Mapping;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the PsetDefinition entity.
	/// </summary>
	[MappingSchema("PsetAttributeDefinition", "Default", "1.0.0")]
	public class DefaultPsetAttributeDefinitionMapping : Mapping<PsetAttributeDefinition> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPsetAttributeDefinitionMapping"/> class.
		/// </summary>
		public DefaultPsetAttributeDefinitionMapping()
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultPsetAttributeDefinitionMapping"/> class.
		/// </summary>
		/// <param name="psetAttributeDefinition">The pset attribute definition.</param>
		public DefaultPsetAttributeDefinitionMapping(PsetAttributeDefinition psetAttributeDefinition)
			: base(psetAttributeDefinition)
		{
			
		}
		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings()
		{
			return Enumerable.Empty<IMappingRecordConverter>();
		}

		/// <summary>
		/// The Key of the Attribute.
		/// </summary>
		public string KeyPropertySingleValue {
			get { return Get(e => e.KeyPropertySingleValue); }
			set { Set(value, e => e.KeyPropertySingleValue); }
		}


		/// <summary>
		/// The Key of the Pset.
		/// </summary>
		public string KeyPropertySet {
			get { return Get(e => e.KeyPropertySet); }
			set { Set(value, e => e.KeyPropertySet); }
		}

		/// <summary>
		/// The Name of the Attribute.
		/// </summary>
		public string AttributeName {
			get { return Get(e => e.AttributeName); }
			set { Set(value, e => e.AttributeName); }
		}

		/// <summary>
		/// The order of the Attribute in the pset.
		/// </summary>
		public int AttributeOrder {
			get { return Get(e => e.AttributeOrder); }
			set { Set(value, e => e.AttributeOrder); }
		}

		/// <summary>
		/// The Attribute default value.
		/// </summary>
		public string AttributeDefaultValue {
			get { return Get(e => e.AttributeDefaultValue); }
			set { Set(value, e => e.AttributeDefaultValue); }
		}


		/// <summary>
		/// Gets or sets the description of the Attribute.
		/// </summary>
		/// <value>The Description.</value>
		public string AttributeDescription {
			get { return Get(e => e.AttributeDescription); }
			set { Set(value, e => e.AttributeDescription); }
		}


		/// <summary>
		/// Gets or sets if the Attribute can be left blank.
		/// </summary>
		public bool? AttributeAllowBlank {
			get { return Get(e => e.AttributeAllowBlank); }
			set { Set(value, e => e.AttributeAllowBlank); }
		}

		/// <summary>
		/// Gets or sets the datatype of the Attribute.
		/// </summary>
		public string AttributeDataType {
			get { return Get(e => e.AttributeDataType); }
			set { Set(value, e => e.AttributeDataType); }
		}

		/// <summary>
		/// Gets or sets the component used to edit the Attribute.
		/// </summary>
		public string AttributeComponent {
			get { return Get(e => e.AttributeComponent); }
			set { Set(value, e => e.AttributeComponent); }
		}

		/// <summary>
		/// Gets or sets the max value of the Attribute.
		/// </summary>
		public string AttributeMaxValue {
			get { return Get(e => e.AttributeMaxValue); }
			set { Set(value, e => e.AttributeMaxValue); }
		}

		/// <summary>
		/// Gets or sets the min value of the Attribute.
		/// </summary>
		public string AttributeMinValue {
			get { return Get(e => e.AttributeMinValue); }
			set { Set(value, e => e.AttributeMinValue); }
		}

		/// <summary>
		/// Gets or sets the Mask input of the Attribute.
		/// </summary>
		public string AttributeMask {
			get { return Get(e => e.AttributeMask); }
			set { Set(value, e => e.AttributeMask); }
		}

		/// <summary>
		/// Gets or sets the name of the validation function of the Attribute.
		/// </summary>
		public string AttributeVType {
			get { return Get(e => e.AttributeVType); }
			set { Set(value, e => e.AttributeVType); }
		}

		/// <summary>
		/// Gets or sets the name of the list of values for the Attribute.
		/// </summary>
		public string AttributeList {
			get { return Get(e => e.AttributeList); }
			set { Set(value, e => e.AttributeList); }
		}

		/// <summary>
		/// Gets or sets the name of the localized label of the Attribute.
		/// </summary>
		public string AttributeMsgCode {
			get { return Get(e => e.AttributeMsgCode); }
			set { Set(value, e => e.AttributeMsgCode); }
		}

		/// <summary>
		/// Gets sets the width of the Attribute.
		/// </summary>
		public int? AttributeWidth {
			get { return Get(e => e.AttributeWidth); }
			set { Set(value, e => e.AttributeWidth); }
		}

		/// <summary>
		/// Gets or sets the fact that the attribute should appear on its own tab, or directly on the entity.
		/// </summary>
		public bool? AttributeHasTab {
			get { return Get(e => e.AttributeHasTab); }
			set { Set(value, e => e.AttributeHasTab); }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string PsetDefinitionLocalId {
			get { return Get(e => e.KeyPropertySet); }
			set { Set(value, e => e.KeyPropertySet); }
		}
	}
}

using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of a PsetAttribute.
	/// </summary>
	public class PsetAttributeMapping : Mapping<PsetAttribute> {
		/// <summary>
		/// Initializes a new instance of the <see cref="PsetAttributeMapping"/> class.
		/// </summary>
		public PsetAttributeMapping() {
			// Calls base.	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PsetAttributeMapping"/> class.
		/// </summary>
		/// <param name="psetAttribute">The pset attribute.</param>
		public PsetAttributeMapping(PsetAttribute psetAttribute) : base(psetAttribute) {
			// Calls base.	
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[XmlAttribute("name")]
		public string Name {
			get { return Get(e => e.Key); }
			set { Set(value, e => e.Key); }
		}

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		[XmlAttribute("value")]
		public string Value {
			get { return Get(e => e.Value); }
			set { Set(value, e => e.Value); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}

	}
}
﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a Data Serie Color Element entity.
	/// </summary>
	[MappingSchema("DataSerieColorElement", "Default", "1.0.0")]
	public class DefaultDataSerieColorElementMapping : Mapping<DataSerieColorElement> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDataSerieColorElementMapping"/> class.
		/// </summary>
		public DefaultDataSerieColorElementMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDataSerieColorElementMapping"/> class.
		/// </summary>
		/// <param name="dataSerieColorElement">The data serie color element.</param>
		public DefaultDataSerieColorElementMapping(DataSerieColorElement dataSerieColorElement)
			: base(dataSerieColorElement) {

		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the key data serie color element.
		/// </summary>
		/// <value>
		/// The key data serie color element.
		/// </value>
		public string KeyDataSerieColorElement {
			get { return Get(e => e.KeyDataSerieColorElement); }
			set { Set(value, e => e.KeyDataSerieColorElement); }
		}

		/// <summary>
		/// The key of the DataSerie this filter is attached too.
		/// </summary>
		public string KeyDataSerie {
			get { return Get(e => e.KeyDataSerie); }
			set { Set(value, e => e.KeyDataSerie); }
		}

		/// <summary>
		/// Gets or sets the color.
		/// </summary>
		/// <value>
		/// The color.
		/// </value>
		public string Color {
			get { return Get(e => e.Color); }
			set { Set(value, e => e.Color); }
		}

		/// <summary>
		/// Gets or sets the highlight method.
		/// </summary>
		/// <value>
		/// The highlight.
		/// </value>
		public DataPointHighlight Highlight {
			get { return Get(e => e.Highlight); }
			set { Set(value, e => e.Highlight); }
		}

		/// <summary>
		/// Gets or sets the element min value.
		/// </summary>
		/// <value>
		/// The element min value.
		/// </value>
		public double? MinValue {
			get { return Get(e => e.MinValue); }
			set { Set(value, e => e.MinValue); }
		}

		/// <summary>
		/// Gets or sets the element max value.
		/// </summary>
		/// <value>
		/// The element max value.
		/// </value>
		public double? MaxValue {
			get { return Get(e => e.MaxValue); }
			set { Set(value, e => e.MaxValue); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
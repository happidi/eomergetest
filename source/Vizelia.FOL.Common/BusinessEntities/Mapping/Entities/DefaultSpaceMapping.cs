﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping for the Space entity.
	/// </summary>
	[MappingSchema("Space", "Default", "1.0.0")]
	public class DefaultSpaceMapping : PsetEnabledMapping<Space> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultSpaceMapping"/> class.
		/// </summary>
		public DefaultSpaceMapping()
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultSpaceMapping"/> class.
		/// </summary>
		/// <param name="space">The space.</param>
		public DefaultSpaceMapping(Space space):base(space)
		{
			
		}
		/// <summary>
		/// The external identifier of the Space.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The GlobalId of the Space.
		/// </summary>
		public string GlobalId {
			get { return Get(e => e.GlobalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Name of the Space.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Description of the Space.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// Local id of the parent BuildingStorey.
		/// </summary>
		public string BuildingStoreyLocalId {
			get { return Get(e => e.KeyBuildingStorey); }
			set { Set(value, e => e.KeyBuildingStorey); }
		}

		/// <summary>
		/// ObjectType of the Space.
		/// </summary>
		public string ObjectType {
			get { return Get(e => e.ObjectType); }
			set { Set(value, e => e.ObjectType); }
		}

		/// <summary>
		/// The elevation of the Space.
		/// </summary>
		public double ElevationWithFlooring {
			get { return Get(e => e.ElevationWithFlooring); }
			set { Set(value, e => e.ElevationWithFlooring); }
		}

		/// <summary>
		/// The area of the Space.
		/// </summary>
		public double AreaValue {
			get { return Get(e => e.AreaValue); }
			set { Set(value, e => e.AreaValue); }
		}

		/// <summary>
		/// The Zoning information.
		/// </summary>
		public DefaultClassificationItemMapping Zoning { get; set; }

		/// <summary>
		/// Populates properties on the business entity before it is outputted.
		/// </summary>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			if (Zoning != null) {
				ClassificationItem zoning = Zoning.GetBusinessEntity();
				Set(zoning, e => e.Zoning);
			}
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
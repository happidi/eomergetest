using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a Data Serie entity.
	/// </summary>
	[MappingSchema("DataSerie", "Default", "1.0.0")]
	public class DefaultDataSerieMapping : Mapping<DataSerie> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDataSerieMapping"/> class.
		/// </summary>
		public DefaultDataSerieMapping() {
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultDataSerieMapping"/> class.
		/// </summary>
		/// <param name="serie">The serie.</param>
		public DefaultDataSerieMapping(DataSerie serie) : base(serie) {

		}

		/// <summary>
		/// Local id of the DataSerie.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Local id of the Chart.
		/// </summary>
		public string ChartLocalId {
			get { return Get(e => e.KeyChart); }
			set { Set(value, e => e.KeyChart); }
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Gets or sets the local id of the Meter Classification Item the series applys to.
		/// </summary>
		public string ClassificationItemLocalId {
			get { return Get(e => e.KeyClassificationItem); }
			set { Set(value, e => e.KeyClassificationItem); }
		}

		/// <summary>
		/// Gets or sets the mathematical GroupOperation
		/// </summary>
		public MathematicOperator GroupOperation {
			get { return Get(e => e.GroupOperation); }
			set { Set(value, e => e.GroupOperation); }
		}

		/// <summary>
		/// Gets or sets the DataSerieType.
		/// </summary>
		public DataSerieType Type {
			get { return Get(e => e.Type); }
			set { Set(value, e => e.Type); }
		}

		/// <summary>
		/// Gets or sets the Transparency 
		/// </summary>
		public int? DisplayTransparency {
			get { return Get(e => e.DisplayTransparency); }
			set { Set(value, e => e.DisplayTransparency); }
		}

		/// <summary>
		/// Gets or sets the Transparency 
		/// </summary>
		public DataSerieLegendEntry DisplayLegendEntry {
			get { return Get(e => e.DisplayLegendEntry); }
			set { Set(value, e => e.DisplayLegendEntry); }
		}

		/// <summary>
		/// Gets or sets the Unit 
		/// </summary>
		public string Unit {
			get { return Get(e => e.Unit); }
			set { Set(value, e => e.Unit); }
		}

		/// <summary>
		/// Red color composant.
		/// </summary>
		public int? ColorR {
			get { return Get(e => e.ColorR); }
			set { Set(value, e => e.ColorR); }
		}

		/// <summary>
		/// Green color composant.
		/// </summary>
		public int? ColorG {
			get { return Get(e => e.ColorG); }
			set { Set(value, e => e.ColorG); }
		}

		/// <summary>
		/// Blue color composant.
		/// </summary>
		public int? ColorB {
			get { return Get(e => e.ColorB); }
			set { Set(value, e => e.ColorB); }
		}

		/// <summary>
		/// True if the serie is visible on the chart, False otherwise.
		/// </summary>
		public bool Hidden {
			get { return Get(e => e.Hidden); }
			set { Set(value, e => e.Hidden); }
		}

		/// <summary>
		/// Local id of the Y Axis of the Serie
		/// </summary>
		public string YAxisLocalId {
			get { return Get(e => e.KeyYAxis); }
			set { Set(value, e => e.KeyYAxis); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
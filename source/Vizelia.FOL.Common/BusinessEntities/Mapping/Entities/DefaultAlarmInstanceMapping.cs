﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the AlarmInstance entity.
	/// </summary>
	[MappingSchema("AlarmInstance", "Default", "1.0.0")]
	public class DefaultAlarmInstanceMapping : Mapping<AlarmInstance> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultAlarmInstanceMapping"/> class.
		/// </summary>
		public DefaultAlarmInstanceMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultAlarmInstanceMapping"/> class.
		/// </summary>
		/// <param name="alarmInstance">The alarm instance.</param>
		public DefaultAlarmInstanceMapping(AlarmInstance alarmInstance)
			: base(alarmInstance) {

		}

		/// <summary>
		/// Key of the AlarmInstance.
		/// </summary>
		public string KeyAlarmInstance {
			get { return Get(e => e.KeyAlarmInstance); }
			set { Set(value, e => e.KeyAlarmInstance); }
		}

		/// <summary>
		/// The key alarm definition.
		/// </summary>
		public string KeyAlarmDefinition {
			get { return Get(e => e.KeyAlarmDefinition); }
			set { Set(value, e => e.KeyAlarmDefinition); }
		}

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// Gets or sets the key meter.
		/// </summary>
		public string KeyMeter {
			get { return Get(e => e.KeyMeter); }
			set { Set(value, e => e.KeyMeter); }
		}

		/// <summary>
		/// The creation DateTime of this instance.
		/// </summary>
		public DateTime CreateDateTime {
			get { return Get(e => e.CreateDateTime); }
			set { Set(value, e => e.CreateDateTime); }
		}

		/// <summary>
		/// The Instance DateTime (the element (meterdata, datapoint) that has generated this alarm.
		/// </summary>
		public DateTime InstanceDateTime {
			get { return Get(e => e.InstanceDateTime); }
			set { Set(value, e => e.InstanceDateTime); }
		}

		/// <summary>
		/// The status.
		/// </summary>
		public AlarmInstanceStatus Status {
			get { return Get(e => e.Status); }
			set { Set(value, e => e.Status); }
		}

		/// <summary>
		/// The value that has triggered the alarm.
		/// </summary>
		public double Value {
			get { return Get(e => e.Value); }
			set { Set(value, e => e.Value); }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether an email has already been sent.
		/// </summary>
		public bool? EmailSent {
			get { return Get(e => e.EmailSent); }
			set { Set(value, e => e.EmailSent); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}

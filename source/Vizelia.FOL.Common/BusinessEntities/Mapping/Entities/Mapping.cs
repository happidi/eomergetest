﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// Represents a class which is a mapping of a business entity.
	/// </summary>
	/// <typeparam name="TBusinessEntity">The type of the business entity.</typeparam>
	public abstract class Mapping<TBusinessEntity> : IMappingRecordConverter, ICsvSerializable where TBusinessEntity : BaseBusinessEntity, IMappableEntity, new() {
		private readonly List<string> m_AssignedProperties;
		private readonly TBusinessEntity m_BusinessEntity;
		private readonly bool m_IsEmptyEntity;

		/// <summary>
		/// Initializes a new instance of the <see cref="Mapping&lt;TBusinessEntity&gt;"/> class.
		/// </summary>
		/// <param name="businessEntity">The business entity.</param>
		protected Mapping(TBusinessEntity businessEntity) {
			m_IsEmptyEntity = false;
			m_AssignedProperties = GetAssignedProperties(businessEntity);
			m_BusinessEntity = businessEntity;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Mapping&lt;TBusinessEntity&gt;"/> class.
		/// </summary>
		protected Mapping() {
			m_IsEmptyEntity = true;
			m_BusinessEntity = new TBusinessEntity();
			m_AssignedProperties = GetAssignedProperties(m_BusinessEntity);
		}

		/// <summary>
		/// Gets the assigned properties.
		/// </summary>
		/// <param name="businessEntity">The business entity.</param>
		/// <returns></returns>
		protected virtual List<string> GetAssignedProperties(TBusinessEntity businessEntity) {
			var assignedProperties = new List<string>();

			return assignedProperties;
		}

		/// <summary>
		/// Gets the deserialized mappable entity.
		/// </summary>
		IMappingRecord IMappingRecordConverter.GetMappingRecord() {
			// This is an explicit interface implementation since if we know the object type,
			// we can just use the strongly typed version of this method.
			return GetMappingRecord();
		}

		/// <summary>
		/// Gets the deserialized mappable entity.
		/// </summary>
		public MappingRecord<TBusinessEntity> GetMappingRecord() {
			var entity = GetBusinessEntity();

			var deserializedMappableEntity =
				new DeserializedMappingRecord<TBusinessEntity>(entity, m_AssignedProperties, !m_IsEmptyEntity) { AdditionalFields = GetAdditionalFields() };

			return deserializedMappableEntity;
		}

		/// <summary>
		/// Gets the additional fields for the entity.
		/// These fields represent fields that exist on the mapping class, but not on the entity itself.
		/// They can be used for resolving fields on the entity.
		/// </summary>
		protected virtual Dictionary<string, string> GetAdditionalFields() {
			return new Dictionary<string, string>();
		}

		/// <summary>
		/// Returns the business entity.
		/// </summary>
		public TBusinessEntity GetBusinessEntity() {
			BeforeGetBusinessEntity();

			return m_BusinessEntity;
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public abstract IEnumerable<IMappingRecordConverter> GetChildMappings();

		/// <summary>
		/// The action of the mapping record.
		/// </summary>
		/// <value>
		/// The action.
		/// </value>
		public string Action { get; set; }

		/// <summary>
		/// Executes before the GetBusinessEntity method runs.
		/// </summary>
		public virtual void BeforeGetBusinessEntity() {
			// Do nothing.
		}

		/// <summary>
		/// Determines if the property name has data assigned.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns>When true, the property was assigned.</returns>
		public bool HasPropertyAssigned(string propertyName) {
			return m_AssignedProperties.Contains(propertyName);
		}

		/// <summary>
		/// Marks the property as assigned.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		protected void MarkPropertyAsAssigned(string propertyName) {
			if (!m_AssignedProperties.Contains(propertyName)) {
				m_AssignedProperties.Add(propertyName);
			}
		}

		/// <summary>
		/// Sets the specified property to the given value.
		/// </summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="value">The value.</param>
		/// <param name="propertyNameIndicatingExpression">The expression indicating the property name.</param>
		/// <param name="markAsAssigned">if set to <c>true</c> mark the property as assigned. Use false value only in special cases.</param>
		protected void Set<TValue>(TValue value, Expression<Func<TBusinessEntity, TValue>> propertyNameIndicatingExpression, bool markAsAssigned = true) {
			MemberInfo member = Helper.GetMemberFromExpression(propertyNameIndicatingExpression);

			if (markAsAssigned) {
				MarkPropertyAsAssigned(member.Name);
			}

			member.SetValue(m_BusinessEntity, value);
		}

		/// <summary>
		/// Gets the specified property value.
		/// </summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="propertyNameIndicatingExpression">The expression indicating the property name.</param>
		protected TValue Get<TValue>(Expression<Func<TBusinessEntity, TValue>> propertyNameIndicatingExpression) {
			MemberInfo member = Common.Helper.GetMemberFromExpression(propertyNameIndicatingExpression);
			TValue value = member.GetValue<TValue>(m_BusinessEntity);

			return value;
		}

		/// <summary>
		/// Sets the value for the specified property name.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		public virtual void Set(string propertyName, string value) {
			// Default implementation - throws an exception.
			throw new ArgumentException(
				string.Format("Cannot assign dynamic property \"{0}\" to type {1}, the property is unknown.",
						propertyName,
						GetType().FullName));

		}
	}
}

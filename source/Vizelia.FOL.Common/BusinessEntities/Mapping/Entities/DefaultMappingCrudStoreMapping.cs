﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of the MappingCrudStore entity.
	/// </summary>
	/// <typeparam name="TParent">The type of the parent.</typeparam>
	/// <typeparam name="TChildren">The type of the children.</typeparam>
	// This mapping is not described using the MappingSchema attribute as it is not mappable as a standalone object.
	// It is mappable only as a property of another object.
	public class DefaultMappingCrudStoreMapping<TParent, TChildren> : Mapping<MappingCrudStore<TParent, TChildren>>
		where TParent : BaseBusinessEntity, new()
		where TChildren : BaseBusinessEntity, new() {
		
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultMappingCrudStoreMapping&lt;TParent, TChildren&gt;"/> class.
		/// Do not use this constructor, use the other one instead.
		/// </summary>
		public DefaultMappingCrudStoreMapping() {
			AssociatedEntities = new List<string>();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultMappingCrudStoreMapping&lt;TParent, TChildren&gt;"/> class.
		/// </summary>
		/// <param name="localIds">The local ids.</param>
		/// <param name="parentLocalId">The parent local id.</param>
		public DefaultMappingCrudStoreMapping(IEnumerable<string> localIds, string parentLocalId) {
			AssociatedEntities = new List<string>(localIds);
			ParentLocalId = parentLocalId;
		}

		/// <summary>
		/// Gets or sets the name of the property of the object in which this is an instance.
		/// </summary>
		/// <value>
		/// The name of the property.
		/// </value>
		public string PropertyName {
			get { return Get(e => e.PropertyName); }
			set { Set(value, e => e.PropertyName); }
		}

		/// <summary>
		/// Gets or sets the children local id's.
		/// </summary>
		[XmlArrayItem("LocalId")]
		public List<string> AssociatedEntities { get; set; }

		/// <summary>
		/// Gets or sets the parent local id.
		/// </summary>
		public string ParentLocalId {
			get { return Get(e => e.ParentLocalId); }
			set { Set(value, e => e.ParentLocalId); }
		}

		/// <summary>
		/// Populates the MappingCrudStore object before it is outputted.
		/// </summary>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();
			List<TChildren> children = AssociatedEntities.Select(CreateChildEntity).ToList();

			// When performing a mapping, we only insert data.
			// Therefore, we only create new associations between entities.
			Set(children, e => e.create);
			Set(new List<TChildren>(), e => e.update);
			Set(new List<TChildren>(), e => e.destroy);
		}

		/// <summary>
		/// Creates the child entity.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		protected virtual TChildren CreateChildEntity(string localId) {
			var child = new TChildren();
			child.SetKey(localId);

			return child;
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString() {
			// We implement this for the purpose of CSV serialization/deserialization, that uses the ToString() method.
			string stringRepresentation = string.Empty;

			if (AssociatedEntities != null) {
				// The format is "localId,localId" WITH the quotes.
				stringRepresentation = string.Join(",", AssociatedEntities.ToArray());
			}

			return stringRepresentation;
		}
	}
}
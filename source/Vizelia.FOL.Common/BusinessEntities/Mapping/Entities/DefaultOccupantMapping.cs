﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping for the Occupant entity.
	/// </summary>
	[MappingSchema("Occupant", "Default", "1.0.0")]
	public class DefaultOccupantMapping : PsetEnabledMapping<Occupant> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultOccupantMapping"/> class.
		/// </summary>
		public DefaultOccupantMapping()
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultOccupantMapping"/> class.
		/// </summary>
		/// <param name="occupant">The occupant.</param>
		public DefaultOccupantMapping(Occupant occupant):base(occupant)
		{
			
		}

		/// <summary>
		/// The id of the Occupant.
		/// </summary>
		public string Id {
			get { return Get(e => e.Id); }
			set { Set(value, e => e.Id); }
		}

		/// <summary>
		/// The local id of the Occupant.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// The first name of the Occupant.
		/// </summary>
		public string FirstName {
			get { return Get(e => e.FirstName); }
			set { Set(value, e => e.FirstName); }
		}

		/// <summary>
		/// The last name of the Occupant.
		/// </summary>
		public string LastName {
			get { return Get(e => e.LastName); }
			set { Set(value, e => e.LastName); }
		}

		/// <summary>
		/// The job title of the Occupant.
		/// </summary>
		public string JobTitle {
			get { return Get(e => e.JobTitle); }
			set { Set(value, e => e.JobTitle); }
		}

		/// <summary>
		/// The telephone number of the Occupant.
		/// </summary>
		public string TelephoneNumbers {
			get { return Get(e => e.TelephoneNumbers); }
			set { Set(value, e => e.TelephoneNumbers); }
		}

		/// <summary>
		/// The fax number of the Occupant.
		/// </summary>
		public string FacsimileNumbers {
			get { return Get(e => e.FacsimileNumbers); }
			set { Set(value, e => e.FacsimileNumbers); }
		}

		/// <summary>
		/// The pager number of the Occupant.
		/// </summary>
		public string PagerNumber {
			get { return Get(e => e.PagerNumber); }
			set { Set(value, e => e.PagerNumber); }
		}

		/// <summary>
		/// The id of the Organization.
		/// </summary>
		public string OrganizationId {
			get { return Get(e => e.KeyOrganization); }
			set { Set(value, e => e.KeyOrganization); }
		}

		/// <summary>
		/// The Location local id of the Occupant.
		/// </summary>
		public string LocationLocalId {
			get { return Get(e => e.KeyLocation); }
			set { Set(value, e => e.KeyLocation); }
		}

		/// <summary>
		/// The location typename of the Occupant.
		/// </summary>
		public HierarchySpatialTypeName LocationTypeName {
			get { return Get(e => e.LocationTypeName); }
			set { Set(value, e => e.LocationTypeName); }
		}
		
		/// <summary>
		/// Gets or sets the local id of the classification item worktype.
		/// </summary>
		/// <value>The key classification item worktype.</value>
		public string ClassificationItemWorktypeLocalId {
			get { return Get(e => e.KeyClassificationItemWorktype); }
			set { Set(value, e => e.KeyClassificationItemWorktype); }
		}

		/// <summary>
		/// Gets or sets the key classification item organization.
		/// </summary>
		/// <value>The key classification item organization.</value>
		public string ClassificationItemOrganizationLocalId {
			get { return Get(e => e.KeyClassificationItemOrganization); }
			set { Set(value, e => e.KeyClassificationItemOrganization); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of a Chart entity.
	/// </summary>
	[MappingSchema("Chart", "Default", "1.0.0")]
	public class DefaultChartMapping : Mapping<Chart> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultChartMapping"/> class.
		/// </summary>
		public DefaultChartMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultChartMapping"/> class.
		/// </summary>
		/// <param name="chart">The chart.</param>
		public DefaultChartMapping(Chart chart)
			: base(chart) {
			if (chart.Axis != null) {
				Axis = chart.Axis.Values.Select(a => new DefaultChartAxisMapping(a) { ChartLocalId = LocalId }).ToList();
			}

			if (chart.HistoricalAnalysisDefinitions != null) {
				HistoricalAnalysis = chart.HistoricalAnalysisDefinitions.Select(h => new DefaultChartHistoricalAnalysisMapping(h) { ChartLocalId = LocalId }).ToList();
			}

			if (chart.Series != null) {
				Series = chart.Series.ToList().Select(s => new DefaultDataSerieMapping(s) { ChartLocalId = LocalId }).ToList();
			}

			if (chart.CalculatedSeries != null) {
				CalculatedSeries = chart.CalculatedSeries.Select(c => new DefaultCalculatedSerieMapping(c) { ChartLocalId = LocalId }).ToList();
			}

			if (chart.HistoricalPsets != null) {
				HistoricalPsets = chart.HistoricalPsets.Select(c => new DefaultChartPsetAttributeHistoricalMapping(c) { ChartLocalId = LocalId }).ToList();
			}

			if (chart.FilterSpatialPset != null) {
				FilterSpatialPsets = chart.FilterSpatialPset.Select(c => new DefaultFilterSpatialPsetMapping(c) { ChartLocalId = LocalId }).ToList();
			}

			
			if (chart.MetersClassifications != null) {
				List<string> classificationItemsLocalIds = chart.MetersClassifications.Select(c => c.LocalId).ToList();
				MetersClassifications = new DefaultMappingCrudStoreMapping<Chart, ClassificationItem>(classificationItemsLocalIds, LocalId);
			}

			if (chart.AlarmDefinitionsClassifications != null) {
				var classificationItemsLocalIds = chart.AlarmDefinitionsClassifications.Select(a => a.LocalId).ToList();
				AlarmDefinitionsClassifications = new DefaultMappingCrudStoreMapping<Chart, ClassificationItem>(classificationItemsLocalIds, LocalId);
			}

			if (chart.EventLogClassifications != null) {
				List<string> classificationItemsLocalIds = chart.EventLogClassifications.Select(c => c.LocalId).ToList();
				EventLogClassifications = new DefaultMappingCrudStoreMapping<Chart, ClassificationItem>(classificationItemsLocalIds, LocalId);
			}

			if (chart.FilterSpatial != null) {
				FilterSpatial = new DefaultLocationMappingCrudStoreMapping<Chart>(chart.FilterSpatial, LocalId);
			}

			if (chart.Roles != null) {
				var rolesLocalIds = chart.Roles.Select(x => x.LocalId).ToList();
				Roles = new DefaultMappingCrudStoreMapping<Chart, AuthorizationItem>(rolesLocalIds, LocalId);
			}
		}

		/// <summary>
		/// Local Id of the Chart
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		public string Title {
			get { return Get(e => e.Title); }
			set { Set(value, e => e.Title); }
		}

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		public string Description {
			get { return Get(e => e.Description); }
			set { Set(value, e => e.Description); }
		}

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		public DateTime StartDate {
			get { return Get(e => e.StartDate); }
			set { Set(value, e => e.StartDate); }
		}

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		public DateTime EndDate {
			get { return Get(e => e.EndDate); }
			set { Set(value, e => e.EndDate); }
		}


		/// <summary>
		/// Gets or sets the local id of the CalendarEventCategory associated to this Chart.
		/// </summary>
		public string CalendarEventCategoryLocalId {
			get { return Get(e => e.KeyCalendarEventCategory); }
			set { Set(value, e => e.KeyCalendarEventCategory); }
		}


		/// <summary>
		/// Gets or sets the chart type
		/// </summary>
		public ChartType ChartType {
			get { return Get(e => e.ChartType); }
			set { Set(value, e => e.ChartType); }
		}

		/// <summary>
		/// Gets or sets the chart default dataserie type
		/// </summary>
		public DataSerieType DataSerieType {
			get { return Get(e => e.DataSerieType); }
			set { Set(value, e => e.DataSerieType); }
		}

		/// <summary>
		/// Gets or sets the chart default timeinterval
		/// </summary>
		public AxisTimeInterval TimeInterval {
			get { return Get(e => e.TimeInterval); }
			set { Set(value, e => e.TimeInterval); }
		}

		/// <summary>
		/// Gets or sets the chart localisation
		/// </summary>
		public ChartLocalisation Localisation {
			get { return Get(e => e.Localisation); }
			set { Set(value, e => e.Localisation); }
		}

		/// <summary>
		/// Gets or sets the chart localisation site level
		/// </summary>
		public int LocalisationSiteLevel {
			get { return Get(e => e.LocalisationSiteLevel); }
			set { Set(value, e => e.LocalisationSiteLevel); }
		}

		/// <summary>
		/// Gets or sets if the Chart is displayed in 3D
		/// </summary>
		public bool Display3D {
			get { return Get(e => e.Display3D); }
			set { Set(value, e => e.Display3D); }
		}

		/// <summary>
		/// Get or sets if values are written as text on the Chart
		/// </summary>
		public bool DisplayValues {
			get { return Get(e => e.DisplayValues); }
			set { Set(value, e => e.DisplayValues); }
		}

		/// <summary>
		/// Gets or sets if startdate and enddate are written as text on the Chart
		/// </summary>
		public bool DisplayStartEndDate {
			get { return Get(e => e.DisplayStartEndDate); }
			set { Set(value, e => e.DisplayStartEndDate); }
		}

		/// <summary>
		/// Gets or sets the font size of the startdate and enddate text
		/// </summary>
		public int DisplayStartEndDateFontSize {
			get { return Get(e => e.DisplayStartEndDateFontSize); }
			set { Set(value, e => e.DisplayStartEndDateFontSize); }
		}

		/// <summary>
		/// Gets or sets the default marker size
		/// </summary>
		public int DisplayMarkerSize {
			get { return Get(e => e.DisplayMarkerSize); }
			set { Set(value, e => e.DisplayMarkerSize); }
		}

		/// <summary>
		/// Gets or sets the default line thickness size
		/// </summary>
		public int DisplayLineThickness {
			get { return Get(e => e.DisplayLineThickness); }
			set { Set(value, e => e.DisplayLineThickness); }
		}

		/// <summary>
		/// Gets or sets the shadding effect apply on the chart
		/// </summary>
		public ChartShaddingEffect DisplayShaddingEffect {
			get { return Get(e => e.DisplayShaddingEffect); }
			set { Set(value, e => e.DisplayShaddingEffect); }
		}

		/// <summary>
		/// Gets or sets the default transparency
		/// </summary>
		public int DisplayTransparency {
			get { return Get(e => e.DisplayTransparency); }
			set { Set(value, e => e.DisplayTransparency); }
		}

		/// <summary>
		/// Gets or sets if DataSeries are stack on the XAxis
		/// </summary>
		public bool DisplayStackSeries {
			get { return Get(e => e.DisplayStackSeries); }
			set { Set(value, e => e.DisplayStackSeries); }
		}

		/// <summary>
		/// Gets or sets the default font size
		/// </summary>
		public int DisplayFontSize {
			get { return Get(e => e.DisplayFontSize); }
			set { Set(value, e => e.DisplayFontSize); }
		}

		/// <summary>
		/// Gets or sets the display color of the font.
		/// </summary>
		/// <value>
		/// The display color of the font.
		/// </value>
		public string DisplayFontColor {
			get { return Get(e => e.DisplayFontColor); }
			set { Set(value, e => e.DisplayFontColor); }
		}

		/// <summary>
		/// Get or sets the Label Truncation Length (for axis).
		/// </summary>
		public int? DisplayLabelTruncationLength {
			get { return Get(e => e.DisplayLabelTruncationLength); }
			set { Set(value, e => e.DisplayLabelTruncationLength); }
		}

		/// <summary>
		/// Gets or sets the default font family
		/// </summary>
		public string DisplayFontFamily {
			get { return Get(e => e.DisplayFontFamily); }
			set { Set(value, e => e.DisplayFontFamily); }
		}

		/// <summary>
		/// Gets or sets the default decimal precision
		/// </summary>
		public int DisplayDecimalPrecision {
			get { return Get(e => e.DisplayDecimalPrecision); }
			set { Set(value, e => e.DisplayDecimalPrecision); }
		}

		/// <summary>
		/// Gets or sets the legend visibility
		/// </summary>
		public bool DisplayLegendVisible {
			get { return Get(e => e.DisplayLegendVisible); }
			set { Set(value, e => e.DisplayLegendVisible); }
		}

		/// <summary>
		/// Gets or sets whether classifications should be displayed or not
		/// </summary>
		public bool DisplayClassifications {
			get { return Get(e => e.DisplayClassifications); }
			set { Set(value, e => e.DisplayClassifications); }
		}

		/// <summary>
		/// Gets or sets the legend font size
		/// </summary>
		public int DisplayLegendFontSize {
			get { return Get(e => e.DisplayLegendFontSize); }
			set { Set(value, e => e.DisplayLegendFontSize); }
		}

		/// <summary>
		/// Gets or sets if the chart display mode.
		/// </summary>
		public ChartDisplayMode DisplayMode {
			get { return Get(e => e.DisplayMode); }
			set { Set(value, e => e.DisplayMode); }
		}

		/// <summary>
		/// Get or sets if the dynamic timescale is enabled
		/// </summary>
		public bool DynamicTimeScaleEnabled {
			get { return Get(e => e.DynamicTimeScaleEnabled); }
			set { Set(value, e => e.DynamicTimeScaleEnabled); }
		}

		/// <summary>
		/// Get or sets the number of 'time interval' to select (the last 3 months ...)
		/// </summary>
		public int DynamicTimeScaleFrequency {
			get { return Get(e => e.DynamicTimeScaleFrequency); }
			set { Set(value, e => e.DynamicTimeScaleFrequency); }
		}

		/// <summary>
		/// Get or sets the dynamic timescam interval : Month, Days, Hours...
		/// </summary>
		public AxisTimeInterval DynamicTimeScaleInterval {
			get { return Get(e => e.DynamicTimeScaleInterval); }
			set { Set(value, e => e.DynamicTimeScaleInterval); }
		}

		/// <summary>
		/// Get or sets if we use we start at the beginning of the month or if we use a time period.
		/// </summary>
		public bool DynamicTimeScaleCalendar {
			get { return Get(e => e.DynamicTimeScaleCalendar); }
			set { Set(value, e => e.DynamicTimeScaleCalendar); }
		}

		/// <summary>
		/// Get or sets the time the night starts for the X Markers.
		/// </summary>
		public string NightStartTime {
			get { return Get(e => e.NightStartTime); }
			set { Set(value, e => e.NightStartTime); }
		}

		/// <summary>
		/// Get or sets the time the night ends for the X Markers.
		/// </summary>
		public string NightEndTime {
			get { return Get(e => e.NightEndTime); }
			set { Set(value, e => e.NightEndTime); }
		}

		/// <summary>
		/// Red color composant of the night marker.
		/// </summary>
		public int? NightColorR {
			get { return Get(e => e.NightColorR); }
			set { Set(value, e => e.NightColorR); }
		}

		/// <summary>
		/// Green color composant of the night marker.
		/// </summary>
		public int? NightColorG {
			get { return Get(e => e.NightColorG); }
			set { Set(value, e => e.NightColorG); }
		}

		/// <summary>
		/// Blue color composant of the night marker.
		/// </summary>
		public int? NightColorB {
			get { return Get(e => e.NightColorB); }
			set { Set(value, e => e.NightColorB); }
		}

		/// <summary>
		/// Local id of the DynamicDisplay the Chart is associated with.
		/// </summary>
		public string DynamicDisplayLocalId {
			get { return Get(e => e.KeyDynamicDisplay); }
			set { Set(value, e => e.KeyDynamicDisplay); }
		}

		/// <summary>
		/// True to render the chart with the DynamicDisplay background. False to ignore it.
		/// </summary>
		public bool UseDynamicDisplay {
			get { return Get(e => e.UseDynamicDisplay); }
			set { Set(value, e => e.UseDynamicDisplay); }
		}

		/// <summary>
		/// True to prevent the LocationFilters to be modified from the PortalTab, False otherwise.
		/// </summary>
		public bool LockFilterSpatial {
			get { return Get(e => e.LockFilterSpatial); }
			set { Set(value, e => e.LockFilterSpatial); }
		}

		/// <summary>
		/// Gets or sets the dynamic display header picture.
		/// </summary>
		/// <value>
		/// The dynamic display header picture.
		/// </value>
		public string DynamicDisplayHeaderPicture {
			get { return Get(e => e.DynamicDisplayHeaderPicture); }
			set { Set(value, e => e.DynamicDisplayHeaderPicture); }
		}

		/// <summary>
		/// Gets or sets the dynamic display main picture.
		/// </summary>
		/// <value>
		/// The dynamic display main picture.
		/// </value>
		public string DynamicDisplayMainPicture {
			get { return Get(e => e.DynamicDisplayMainPicture); }
			set { Set(value, e => e.DynamicDisplayMainPicture); }
		}

		/// <summary>
		/// Gets or sets the dynamic display footer picture.
		/// </summary>
		/// <value>
		/// The dynamic display footer picture.
		/// </value>
		public string DynamicDisplayFooterPicture {
			get { return Get(e => e.DynamicDisplayFooterPicture); }
			set { Set(value, e => e.DynamicDisplayFooterPicture); }
		}

		/// <summary>
		/// Gets or sets the dynamic display main text.
		/// </summary>
		/// <value>
		/// The dynamic display main text.
		/// </value>
		public string DynamicDisplayMainText {
			get { return Get(e => e.DynamicDisplayMainText); }
			set { Set(value, e => e.DynamicDisplayMainText); }
		}

		/// <summary>
		/// Gets or sets the dynamic display header text.
		/// </summary>
		/// <value>
		/// The dynamic display header text.
		/// </value>
		public string DynamicDisplayHeaderText {
			get { return Get(e => e.DynamicDisplayHeaderText); }
			set { Set(value, e => e.DynamicDisplayHeaderText); }
		}

		/// <summary>
		/// Gets or sets the dynamic display footer text.
		/// </summary>
		/// <value>
		/// The dynamic display footer text.
		/// </value>
		public string DynamicDisplayFooterText {
			get { return Get(e => e.DynamicDisplayFooterText); }
			set { Set(value, e => e.DynamicDisplayFooterText); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		public bool IsFavorite {
			get { return Get(e => e.IsFavorite); }
			set { Set(value, e => e.IsFavorite); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		public bool OpenOnStartup {
			get { return Get(e => e.OpenOnStartup); }
			set { Set(value, e => e.OpenOnStartup); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether we use spatial path, or just the spatial element name in the series generation.
		/// </summary>
		/// <value>
		///   <c>true</c> if [use spatial path]; otherwise, <c>false</c>.
		/// </value>
		public bool UseSpatialPath {
			get { return Get(e => e.UseSpatialPath); }
			set { Set(value, e => e.UseSpatialPath); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether we use classification path, or just the classification item title in the series generation.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [use classification path]; otherwise, <c>false</c>.
		/// </value>
		public bool UseClassificationPath {
			get { return Get(e => e.UseClassificationPath); }
			set { Set(value, e => e.UseClassificationPath); }
		}

		/// <summary>
		/// Gets or sets the X axis start day of week.
		/// </summary>
		/// <value>
		/// The X axis start day of week.
		/// </value>
		public int XAxisStartDayOfWeek {
			get { return Get(e => e.XAxisStartDayOfWeek); }
			set { Set(value, e => e.XAxisStartDayOfWeek); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is KPI.
		/// </summary>
		public bool IsKPI {
			get { return Get(e => e.IsKPI); }
			set { Set(value, e => e.IsKPI); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable localisation slider].
		/// </summary>
		public bool KPIDisableLocalisationSlider {
			get { return Get(e => e.KPIDisableLocalisationSlider); }
			set { Set(value, e => e.KPIDisableLocalisationSlider); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable time interval slider].
		/// </summary>
		public bool KPIDisableTimeIntervalSlider {
			get { return Get(e => e.KPIDisableTimeIntervalSlider); }
			set { Set(value, e => e.KPIDisableTimeIntervalSlider); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable classification level slider].
		/// </summary>
		public bool KPIDisableClassificationLevelSlider {
			get { return Get(e => e.KPIDisableClassificationLevelSlider); }
			set { Set(value, e => e.KPIDisableClassificationLevelSlider); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable display mode slider].
		/// </summary>
		public bool KPIDisableDisplayModeSlider {
			get { return Get(e => e.KPIDisableDisplayModeSlider); }
			set { Set(value, e => e.KPIDisableDisplayModeSlider); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable limit series number slider].
		/// </summary>
		public bool KPIDisableLimitSeriesNumberSlider {
			get { return Get(e => e.KPIDisableLimitSeriesNumberSlider); }
			set { Set(value, e => e.KPIDisableLimitSeriesNumberSlider); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable meter classification selection].
		/// </summary>
		public bool KPIDisableMeterClassificationSelection {
			get { return Get(e => e.KPIDisableMeterClassificationSelection); }
			set { Set(value, e => e.KPIDisableMeterClassificationSelection); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable spatial selection].
		/// </summary>
		public bool KPIDisableSpatialSelection {
			get { return Get(e => e.KPIDisableSpatialSelection); }
			set { Set(value, e => e.KPIDisableSpatialSelection); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable meter selection].
		/// </summary>
		public bool KPIDisableMeterSelection {
			get { return Get(e => e.KPIDisableMeterSelection); }
			set { Set(value, e => e.KPIDisableMeterSelection); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [KPI disable date range menu].
		/// </summary>
		public bool KPIDisableDateRangeMenu {
			get { return Get(e => e.KPIDisableDateRangeMenu); }
			set { Set(value, e => e.KPIDisableDateRangeMenu); }
		}

		/// <summary>
		/// The key of the classification of the KPI Definition.
		/// </summary>
		public string KeyClassificationItemKPILocalId {
			get { return Get(e => e.KeyClassificationItemKPI); }
			set { Set(value, e => e.KeyClassificationItemKPI); }
		}

		/// <summary>
		/// Gets the virtual file path.
		/// </summary>
		public string VirtualFilePath {
			get { return Get(e => e.VirtualFilePath); }
			set { Set(value, e => e.VirtualFilePath); }
		}

		/// <summary>
		/// Gets or sets the time zone id in which we want to render the chart.
		/// </summary>
		/// <value>
		/// The time zone id.
		/// </value>
		public string TimeZoneId {
			get { return Get(e => e.TimeZoneId); }
			set { Set(value, e => e.TimeZoneId); }
		}

		/// <summary>
		/// Gets or sets the KPI id.
		/// </summary>
		/// <value>
		/// The KPI id.
		/// </value>
		public string KPIId {
			get { return Get(e => e.KPIId); }
			set { Set(value, e => e.KPIId); }
		}

		/// <summary>
		/// Gets or sets the KPI setup instructions.
		/// </summary>
		/// <value>
		/// The KPI setup instructions.
		/// </value>
		public string KPISetupInstructions {
			get { return Get(e => e.KPISetupInstructions); }
			set { Set(value, e => e.KPISetupInstructions); }
		}

		/// <summary>
		/// Gets or sets the KPI copy date.
		/// </summary>
		/// <value>
		/// The KPI copy date.
		/// </value>
		public DateTime? KPICopyDate {
			get { return Get(e => e.KPICopyDate); }
			set { Set(value, e => e.KPICopyDate); }
		}

		/// <summary>
		/// Gets or sets the KPI build.
		/// </summary>
		/// <value>
		/// The KPI build.
		/// </value>
		public string KPIBuild {
			get { return Get(e => e.KPIBuild); }
			set { Set(value, e => e.KPIBuild); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [dynamic time scale calendar end to now].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [dynamic time scale calendar end to now]; otherwise, <c>false</c>.
		/// </value>
		public bool DynamicTimeScaleCalendarEndToNow {
			get { return Get(e => e.DynamicTimeScaleCalendarEndToNow); }
			set { Set(value, e => e.DynamicTimeScaleCalendarEndToNow); }
		}

		/// <summary>
		/// The historical analysis definitions of this chart.
		/// This is a one-to-many mapping.
		/// </summary>
		[XmlArray("HistoricalAnalysis")]
		[XmlArrayItem("HistoricalAnalysis")]
		public List<DefaultChartHistoricalAnalysisMapping> HistoricalAnalysis { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		public string Creator { get; set; }

		/// <summary>
		/// Gets or sets the meters.
		/// This is a many-to-many mapping.
		/// </summary>
		public DefaultMappingCrudStoreMapping<Chart, Meter> Meters { get; set; }

		/// <summary>
		/// Gets or sets the alarm definitions classifications.
		/// This is a many-to-many mapping.
		/// </summary>
		public DefaultMappingCrudStoreMapping<Chart, ClassificationItem> AlarmDefinitionsClassifications { get; set; }


		/// <summary>
		/// Gets or sets the meters classifications.
		/// This is a many-to-many mapping.
		/// </summary>
		public DefaultMappingCrudStoreMapping<Chart, ClassificationItem> MetersClassifications { get; set; }

		/// <summary>
		/// Gets or sets the event log classifications.
		/// This is a many-to-many mapping.
		/// </summary>
		public DefaultMappingCrudStoreMapping<Chart, ClassificationItem> EventLogClassifications { get; set; }

		/// <summary>
		/// Gets or sets the filter spatial.
		/// This is a many-to-many mapping.
		/// </summary>
		public DefaultLocationMappingCrudStoreMapping<Chart> FilterSpatial { get; set; }

		/// <summary>
		/// The list of roles of the Chart.
		/// This field is defined only in order to get the list from a xml mapping document.
		/// Otherwise it should remain null.
		/// </summary>
		public DefaultMappingCrudStoreMapping<Chart, AuthorizationItem> Roles { get; set; }

		/// <summary>
		/// Gets or sets the filter spatial pset.
		/// </summary>
		[XmlArray("FilterSpatialPsets")]
		[XmlArrayItem("FilterSpatialPset", typeof(DefaultFilterSpatialPsetMapping))]
		public List<DefaultFilterSpatialPsetMapping> FilterSpatialPsets { get; set; }

		/// <summary>
		/// Gets or sets the series.
		/// This is a one-to-many mapping.
		/// </summary>
		[XmlArray("Series")]
		[XmlArrayItem("DataSerie", typeof(DefaultDataSerieMapping))]
		public List<DefaultDataSerieMapping> Series { get; set; }

		/// <summary>
		/// Gets or sets the list of calculatedseries.
		/// This is a one-to-many mapping.
		/// </summary>
		[XmlArray("CalculatedSeries")]
		[XmlArrayItem("CalculatedSerie", typeof(DefaultCalculatedSerieMapping))]
		public List<DefaultCalculatedSerieMapping> CalculatedSeries { get; set; }


		/// <summary>
		/// Gets or sets the historical psets.
		/// This is a one-to-many mapping.
		/// </summary>
		[XmlArray("HistoricalPsets")]
		[XmlArrayItem("HistoricalPset", typeof(DefaultChartPsetAttributeHistoricalMapping))]
		public List<DefaultChartPsetAttributeHistoricalMapping> HistoricalPsets { get; set; }

		/// <summary>
		/// Gets or sets the list of extra axis.
		/// This is a one-to-many property.
		/// </summary>
		[XmlArray("Axis")]
		[XmlArrayItem("ChartAxis")]
		public List<DefaultChartAxisMapping> Axis { get; set; }

		/// <summary>
		/// Gets or sets the legend position.
		/// </summary>
		/// <value>
		/// The legend position.
		/// </value>
		public ChartLegendPosition LegendPosition {
			get { return Get(e => e.LegendPosition); }
			set { Set(value, e => e.LegendPosition); }
		}

		/// <summary>
		/// Gets or sets the info icon position.
		/// </summary>
		/// <value>
		/// The info icon position.
		/// </value>
		public ChartInfoIconPosition InfoIconPosition {
			get { return Get(e => e.InfoIconPosition); }
			set { Set(value, e => e.InfoIconPosition); }
		}

		/// <summary>
		/// Gets or sets the specific analysis.
		/// </summary>
		/// <value>
		/// The specific analysis.
		/// </value>
		public ChartSpecificAnalysis SpecificAnalysis {
			get { return Get(e => e.SpecificAnalysis); }
			set { Set(value, e => e.SpecificAnalysis); }
		}

		/// <summary>
		/// Gets or sets the correlation X serie local id.
		/// </summary>
		/// <value>
		/// The correlation X serie local id.
		/// </value>
		public string CorrelationXSerieLocalId {
			get { return Get(e => e.CorrelationXSerieLocalId); }
			set { Set(value, e => e.CorrelationXSerieLocalId); }
		}

		/// <summary>
		/// Gets or sets the correlation Y serie local id.
		/// </summary>
		/// <value>
		/// The correlation Y serie local id.
		/// </value>
		public string CorrelationYSerieLocalId {
			get { return Get(e => e.CorrelationYSerieLocalId); }
			set { Set(value, e => e.CorrelationYSerieLocalId); }
		}

		/// <summary>
		/// Populates the business entity before it is outputted.
		/// </summary>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			InitializeCollections();

			SetAxisProperty();
			SetSeriesProperty();
			SetCalculatedSeriesProperty();

			// We do not populate the HistoricalAnalysis property as it is only populated when retreiving a Chart from the database.
		}

		/// <summary>
		/// Initializes the collections.
		/// </summary>
		private void InitializeCollections() {
			if (CalculatedSeries == null) {
				CalculatedSeries = new List<DefaultCalculatedSerieMapping>();
			}

			if (Series == null) {
				Series = new List<DefaultDataSerieMapping>();
			}

			if (Axis == null) {
				Axis = new List<DefaultChartAxisMapping>();
			}

			if (HistoricalAnalysis == null) {
				HistoricalAnalysis = new List<DefaultChartHistoricalAnalysisMapping>();
			}
		}

		private void SetCalculatedSeriesProperty() {
			List<CalculatedSerie> calculatedSerie = CalculatedSeries.Select(s => s.GetBusinessEntity()).ToList();
			foreach (CalculatedSerie serie in calculatedSerie) {
				serie.KeyChart = LocalId;
			}

			Set(calculatedSerie, e => e.CalculatedSeries);
		}

		private void SetSeriesProperty() {
			/*List<DataSerie> dataSeries = Series.Select(s => s.GetBusinessEntity()).ToList();
			foreach (DataSerie serie in dataSeries) {
				serie.KeyChart = LocalId;
			}

			Set(dataSeries, e => e.Series);
			 * */
		}

		private void SetAxisProperty() {
			List<ChartAxis> axises = Axis.Select(a => a.GetBusinessEntity()).ToList();
			var axisDictionary = new Dictionary<string, ChartAxis>();

			int i = 0;
			foreach (ChartAxis axis in axises) {
				axis.KeyChart = LocalId;
				axisDictionary.Add(i.ToString(), axis);
				i++;
			}

			Set(axisDictionary, e => e.Axis);
		}


		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			var entities = new List<IMappingRecordConverter>();

			if (Meters != null) {
				Meters.ParentLocalId = LocalId;
				entities.Add(Meters);
			}

			if (MetersClassifications != null) {
				MetersClassifications.ParentLocalId = LocalId;
				MetersClassifications.PropertyName = "MeterClassification";
				entities.Add(MetersClassifications);
			}

			if (AlarmDefinitionsClassifications != null) {
				AlarmDefinitionsClassifications.ParentLocalId = LocalId;
				AlarmDefinitionsClassifications.PropertyName = "AlarmDefinitionClassification";
				entities.Add(AlarmDefinitionsClassifications);
			}

			if (EventLogClassifications != null) {
				EventLogClassifications.ParentLocalId = LocalId;
				EventLogClassifications.PropertyName = "EventLogClassification";
				entities.Add(EventLogClassifications);
			}

			if (FilterSpatial != null) {
				FilterSpatial.ParentLocalId = LocalId;
				entities.Add(FilterSpatial);
			}

			if (Roles != null) {
				Roles.ParentLocalId = LocalId;
				entities.Add(Roles);
			}

			// The axis must be outputted before series because the series refers to axis.
			if (Axis != null) {
				foreach (DefaultChartAxisMapping axis in Axis) {
					axis.ChartLocalId = LocalId;
				}
				entities.AddRange(Axis);
			}

			if (Series != null) {
				foreach (DefaultDataSerieMapping series in Series) {
					series.ChartLocalId = LocalId;
				}
				entities.AddRange(Series);
			}

			if (CalculatedSeries != null) {
				foreach (DefaultCalculatedSerieMapping calculatedSeries in CalculatedSeries) {
					calculatedSeries.ChartLocalId = LocalId;
				}
				entities.AddRange(CalculatedSeries);
			}

			if (HistoricalAnalysis != null) {
				foreach (DefaultChartHistoricalAnalysisMapping historicalAnalysis in HistoricalAnalysis) {
					historicalAnalysis.ChartLocalId = LocalId;
				}
				entities.AddRange(HistoricalAnalysis);
			}

			if (HistoricalPsets != null) {
				foreach (DefaultChartPsetAttributeHistoricalMapping pset in HistoricalPsets) {
					pset.ChartLocalId = LocalId;
				}
				entities.AddRange(HistoricalPsets);
			}

			if (FilterSpatialPsets != null) {
				foreach (DefaultFilterSpatialPsetMapping pset in FilterSpatialPsets) {
					pset.ChartLocalId = LocalId;
				}
				entities.AddRange(FilterSpatialPsets);
			}

			return entities;
		}

	}
}
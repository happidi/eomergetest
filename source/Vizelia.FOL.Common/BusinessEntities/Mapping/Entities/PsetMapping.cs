using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Vizelia.FOL.BusinessEntities.Mapping {

	/// <summary>
	/// A mapping of a Pset. 
	/// Not to be confused with PsetEnabledMapping, which contains this as a property.
	/// </summary>
	public class PsetMapping : Mapping<Pset> {
		/// <summary>
		/// Initializes a new instance of the <see cref="PsetMapping"/> class.
		/// </summary>
		public PsetMapping() {
			// Calls base.
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PsetMapping"/> class.
		/// </summary>
		/// <param name="pset">The pset.</param>
		public PsetMapping(Pset pset)
			: base(pset) {
			Attributes = new List<PsetAttributeMapping>();
			if (pset.Attributes != null) {
				Attributes.AddRange(pset.Attributes.Select(a => new PsetAttributeMapping(a)));
			}
		}

		/// <summary>
		/// Gets or sets the name of the Pset.
		/// </summary>
		/// <value>
		/// The name of the Pset.
		/// </value>
		[XmlAttribute("name")]
		public string Name {
			get { return Get(e => e.PsetName); }
			set { Set(value, e => e.PsetName); }
		}

		/// <summary>
		/// Gets or sets the attributes.
		/// </summary>
		/// <value>
		/// The attributes.
		/// </value>
		[XmlElement("Attribute", typeof(PsetAttributeMapping))]
		public List<PsetAttributeMapping> Attributes { get; set; }

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}

		/// <summary>
		/// Transforms the Psets before getting the entity.
		/// </summary>
		public override void BeforeGetBusinessEntity() {
			base.BeforeGetBusinessEntity();

			List<PsetAttribute> attributes = Attributes.Select(a => a.GetBusinessEntity()).ToList();
			Set(attributes, e => e.Attributes);
		}
	}
}
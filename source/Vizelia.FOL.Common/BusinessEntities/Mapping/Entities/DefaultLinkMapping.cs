﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of a Link entity.
	/// </summary>
	[MappingSchema("Link", "Default", "1.0.0")]
	public class DefaultLinkMapping : Mapping<Link> {


		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultLinkMapping"/> class.
		/// </summary>
		public DefaultLinkMapping() {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultLinkMapping"/> class.
		/// </summary>
		/// <param name="link">The link.</param>
		public DefaultLinkMapping(Link link)
			: base(link) {
		}

		/// <summary>
		/// The local id of the Portal Window.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}
		
		/// <summary>
		/// Gets or sets the icon css of the portal.
		/// </summary>
		public string IconCls {
			get { return Get(e => e.IconCls); }
			set { Set(value, e => e.IconCls); }
		}

		/// <summary>
		/// Gets or sets the actual link
		/// </summary>
		public string LinkValue {
			get { return Get(e => e.LinkValue); }
			set { Set(value, e => e.LinkValue); }
		}

		/// <summary>
		/// Gets or sets the key image (custom icon instead of css).
		/// </summary>
		public string KeyImage {
			get { return Get(e => e.KeyImage); }
			set { Set(value, e => e.KeyImage); }
		}

		/// <summary>
		/// Gets or sets the key parent.
		/// </summary>
		/// <value>
		/// The key parent.
		/// </value>
		public string KeyParent {
			get { return Get(e => e.KeyParent); }
			set { Set(value, e => e.KeyParent); }
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		public string Creator {
			get { return Get(e => e.Creator); }
			set { Set(value, e => e.Creator); }
		}

		/// <summary>
		/// Gets or sets the name of the localized label of the Link.
		/// </summary>
		public string MsgCode {
			get { return Get(e => e.MsgCode); }
			set { Set(value, e => e.MsgCode); }
		}

		/// <summary>
		/// Gets or sets the name of the localized label of the Link group.
		/// </summary>
		public string GroupNameMsgCode {
			get { return Get(e => e.GroupNameMsgCode); }
			set { Set(value, e => e.GroupNameMsgCode); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the Chart Marker entity.
	/// </summary>
	[MappingSchema("ChartMarker", "Default", "1.0.0")]
	public class DefaultChartMarkerMapping : Mapping<ChartMarker> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultChartMarkerMapping"/> class.
		/// </summary>
		public DefaultChartMarkerMapping() {
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultChartMarkerMapping"/> class.
		/// </summary>
		/// <param name="chartMarker">The chart marker.</param>
		public DefaultChartMarkerMapping(ChartMarker chartMarker) : base(chartMarker) {
			
		}

		/// <summary>
		/// Gets or sets the chart axis local id.
		/// </summary>
		/// <value>
		/// The chart axis local id.
		/// </value>
		public string ChartAxisLocalId {
			get { return Get(e => e.KeyChartAxis); }
			set { Set(value, e => e.KeyChartAxis); }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the name of the marker.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Gets or sets the minimum value of the marker.
		/// </summary>
		public double? Min {
			get { return Get(e => e.Min); }
			set { Set(value, e => e.Min); }
		}

		/// <summary>
		/// Gets or sets the maximum value of the marker.
		/// </summary>
		public double? Max {
			get { return Get(e => e.Max); }
			set { Set(value, e => e.Max); }
		}

		/// <summary>
		/// Red color composant.
		/// </summary>
		public int? ColorR {
			get { return Get(e => e.ColorR); }
			set { Set(value, e => e.ColorR); }
		}

		/// <summary>
		/// Green color composant.
		/// </summary>
		public int? ColorG {
			get { return Get(e => e.ColorG); }
			set { Set(value, e => e.ColorG); }
		}

		/// <summary>
		/// Blue color composant.
		/// </summary>
		public int? ColorB {
			get { return Get(e => e.ColorB); }
			set { Set(value, e => e.ColorB); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
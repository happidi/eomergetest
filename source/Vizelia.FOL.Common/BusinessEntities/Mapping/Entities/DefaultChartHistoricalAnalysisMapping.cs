using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping of the ChartHistoricalAnalysis entity.
	/// </summary>
	[MappingSchema("ChartHistoricalAnalysis", "Default", "1.0.0")]
	public class DefaultChartHistoricalAnalysisMapping : Mapping<ChartHistoricalAnalysis> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultChartHistoricalAnalysisMapping"/> class.
		/// </summary>
		public DefaultChartHistoricalAnalysisMapping() {
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultChartHistoricalAnalysisMapping"/> class.
		/// </summary>
		/// <param name="analysis">The analysis.</param>
		public DefaultChartHistoricalAnalysisMapping(ChartHistoricalAnalysis analysis) : base(analysis) {
			
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		public string Name {
			get { return Get(e => e.Name); }
			set { Set(value, e => e.Name); }
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Key of the Chart.
		/// </summary>
		public string ChartLocalId {
			get { return Get(e => e.KeyChart); }
			set { Set(value, e => e.KeyChart); }
		}

		/// <summary>
		/// Get or sets the frequency to slide the chart time scale
		/// </summary>
		public int Frequency {
			get { return Get(e => e.Frequency); }
			set { Set(value, e => e.Frequency); }
		}

		/// <summary>
		/// Get or sets the frequency to slide the chart time scale
		/// </summary>
		public AxisTimeInterval Interval {
			get { return Get(e => e.Interval); }
			set { Set(value, e => e.Interval); }
		}

		/// <summary>
		/// True to render the historical analysis on another chart area, false otherwise.
		/// </summary>
		public bool ExtraChartArea {
			get { return Get(e => e.ExtraChartArea); }
			set { Set(value, e => e.ExtraChartArea); }
		}

		/// <summary>
		/// Gets or sets the historical analysis  dataserie type, applys only if the chart is a combo.
		/// </summary>
		public DataSerieType DataSerieType {
			get { return Get(e => e.DataSerieType); }
			set { Set(value, e => e.DataSerieType); }
		}

		/// <summary>
		/// True to enable the analysis, false otherwise.
		/// </summary>
		public bool Enabled {
			get { return Get(e => e.Enabled); }
			set { Set(value, e => e.Enabled); }
		}

		/// <summary>
		/// True to display the extra XAxis false otherwise.
		/// </summary>
		public bool XAxisVisible {
			get { return Get(e => e.XAxisVisible); }
			set { Set(value, e => e.XAxisVisible); }
		}
		
		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}
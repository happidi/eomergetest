using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the MeterData entity.
	/// </summary>
	[MappingSchema("MeterData", "Default", "1.0.0")]
	public class DefaultMeterDataMapping : Mapping<MeterData> {

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultMeterDataMapping"/> class.
		/// </summary>
		public DefaultMeterDataMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultMeterDataMapping"/> class.
		/// </summary>
		/// <param name="meterData">The meter data.</param>
		public DefaultMeterDataMapping(MeterData meterData)
			: base(meterData) {

		}

		/// <summary>
		/// The acquisition date time of the data.
		/// </summary>
		public DateTime AcquisitionDateTime {
			get { return Get(e => e.AcquisitionDateTime); }
			set { Set(value, e => e.AcquisitionDateTime); }
		}

		/// <summary>
		/// The value of the data.
		/// </summary>
		public double Value {
			get { return Get(e => e.Value); }
			set { Set(value, e => e.Value); }
		}

		/// <summary>
		/// Gets or sets the validity.
		/// </summary>
		/// <value>
		/// The validity.
		/// </value>
		public MeterDataValidity Validity {
			get { return Get(e => e.Validity); }
			set { Set(value, e => e.Validity); }
		}

		/// <summary>
		/// The local ID of the meter the data is attached to.
		/// </summary>
		public string MeterLocalId {
			get { return Get(e => e.KeyMeter); }
			set { Set(value, e => e.KeyMeter); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}

	}
}
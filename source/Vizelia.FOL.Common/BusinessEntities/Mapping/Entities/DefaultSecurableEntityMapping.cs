﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities.Mapping {
	/// <summary>
	/// A mapping for the Securable Entity entity.
	/// </summary>
	[MappingSchema("SecurableEntity", "Default", "1.0.0")]
	public class DefaultSecurableEntityMapping : Mapping<SecurableEntity> {
		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultSecurableEntityMapping"/> class.
		/// </summary>
		public DefaultSecurableEntityMapping() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DefaultSecurableEntityMapping"/> class.
		/// </summary>
		/// <param name="securableEntity">The securable entity.</param>
		public DefaultSecurableEntityMapping(SecurableEntity securableEntity)
			: base(securableEntity) {

		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return Get(e => e.LocalId); }
			set { Set(value, e => e.LocalId); }
		}

		/// <summary>
		/// Gets or sets the name of the securable.
		/// </summary>
		/// <value>
		/// The name of the securable.
		/// </value>
		public string SecurableName {
			get { return Get(e => e.SecurableName); }
			set { Set(value, e => e.SecurableName); }
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}
	}
}

using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities.Mapping;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// An interface for a mapping record.
	/// </summary>
	public interface IMappingRecordConverter {
		/// <summary>
		/// Gets the deserialized mappable entity.
		/// </summary>
		IMappingRecord GetMappingRecord();

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		IEnumerable<IMappingRecordConverter> GetChildMappings();

		/// <summary>
		/// The action of the mapping record.
		/// </summary>
		/// <value>
		/// The action.
		/// </value>
		string Action { get; set; }
	}
}
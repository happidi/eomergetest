﻿using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// A mapping task business entity.
	/// </summary>
	[DataContract]
	[Key("KeyMappingTask")]
	public class MappingTask : BaseBusinessEntity, IFileSource {

		/// <summary>
		/// Gets or sets the key mapping task.
		/// </summary>
		/// <value>
		/// The key mapping task.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyMappingTask { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[NotNullValidator]
		[VizStringLengthValidator(1, 255)]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the type of the file source.
		/// </summary>
		/// <value>
		/// The type of the file source.
		/// </value>
		[DataMember]
		public FileSourceType FileSourceType { get; set; }

		/// <summary>
		/// Gets or sets the username.
		/// </summary>
		/// <value>
		/// The username.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Username { get; set; }

		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>
		/// The password.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Password { get; set; }

		/// <summary>
		/// Gets or sets the type of the FTP security.
		/// </summary>
		/// <value>
		/// The type of the FTP security.
		/// </value>
		[DataMember]
		public FtpSecurityType FtpSecurityType { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether custom credentials should be used to log on to the file source or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if custom credentials should be used to log on to the file source; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool UseCustomCredentials { get; set; }

		/// <summary>
		/// Gets or sets the SFTP authentication mode.
		/// </summary>
		/// <value>
		/// The SFTP authentication mode.
		/// </value>
		[DataMember]
		public SftpAuthenticationMode SftpAuthenticationMode { get; set; }

		/// <summary>
		/// Gets or sets the address of the file source.
		/// This can be either a server name, a URI or a UNC path, depending on the FileSourceType.
		/// </summary>
		/// <value>
		/// The address of the file source.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[NotNullValidator]
		[VizStringLengthValidator(1, 510)]
		public string Address { get; set; }

		/// <summary>
		/// Gets or sets the emails for this mapping task.
		/// </summary>
		/// <value>
		/// The emails for this mapping task.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Emails { get; set; }

		/// <summary>
		/// Gets or sets the private key.
		/// </summary>
		/// <value>
		/// The private key.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string PrivateKey { get; set; }

		/// <summary>
		/// Gets or sets the private key password.
		/// </summary>
		/// <value>
		/// The private key password.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string PrivateKeyPassword { get; set; }


		/// <summary>
		/// Gets or sets the automatic created meter spatial identifier.
		/// </summary>
		/// <value>
		/// The automatic created meter spatial identifier.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string AutoCreatedMetersKeyLocation { get; set; }



		/// <summary>
		/// Gets or sets the automatic created meters location long path.
		/// </summary>
		/// <value>
		/// The automatic created meters location long path.
		/// </value>
		[DataMember]
		public string AutoCreatedMetersLocationLongPath { get; set; }


		/// <summary>
		/// Gets or sets the automatic created meter spatial identifier.
		/// </summary>
		/// <value>
		/// The automatic created meter spatial identifier.
		/// </value>
		[DataMember]
		public AutoCreateMeterOptions AutoCreateMeterOptions { get; set; }



	}
}
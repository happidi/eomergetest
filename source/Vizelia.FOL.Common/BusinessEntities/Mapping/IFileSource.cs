using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	
	/// <summary>
	/// An interface describing a file source.
	/// </summary>
	public interface IFileSource {
		/// <summary>
		/// Gets or sets the type of the file source.
		/// </summary>
		/// <value>
		/// The type of the file source.
		/// </value>
		FileSourceType FileSourceType { get; set; }

		/// <summary>
		/// Gets or sets the username.
		/// </summary>
		/// <value>
		/// The username.
		/// </value>
		string Username { get; set; }

		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>
		/// The password.
		/// </value>
		string Password { get; set; }

		/// <summary>
		/// Gets or sets the address of the file source.
		/// This can be either a server name, a URI or a UNC path, depending on the FileSourceType.
		/// </summary>
		/// <value>
		/// The address of the file source.
		/// </value>
		string Address { get; set; }

		/// <summary>
		/// Gets or sets the type of the FTP security.
		/// </summary>
		/// <value>
		/// The type of the FTP security.
		/// </value>
		FtpSecurityType FtpSecurityType { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether custom credentials should be used to log on to the file source or not.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if custom credentials should be used to log on to the file source; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		bool UseCustomCredentials { get; set; }

		/// <summary>
		/// Gets or sets the SFTP authentication mode.
		/// </summary>
		/// <value>
		/// The SFTP authentication mode.
		/// </value>
		[DataMember]
		SftpAuthenticationMode SftpAuthenticationMode { get; set; }

		/// <summary>
		/// Gets or sets the private key.
		/// </summary>
		/// <value>
		/// The private key.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		string PrivateKey { get; set; }

		/// <summary>
		/// Gets or sets the private key password.
		/// </summary>
		/// <value>
		/// The private key password.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		string PrivateKeyPassword { get; set; }
	}
}
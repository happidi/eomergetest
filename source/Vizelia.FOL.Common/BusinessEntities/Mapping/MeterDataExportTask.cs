﻿using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A meter data export task business entity.
	/// </summary>
	[DataContract]
	[Key("KeyMeterDataExportTask")]
	public class MeterDataExportTask : BaseBusinessEntity {
		/// <summary>
		/// Gets or sets the key meter data export task.
		/// </summary>
		/// <value>
		/// The key meter data export task.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyMeterDataExportTask { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		[NotNullValidator]
		[VizStringLengthValidator(1, 255)]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the key mapping task.
		/// </summary>
		/// <value>
		/// The key mapping task.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyMappingTask { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether we should compress on export or not.
		/// </summary>
		/// <value>
		///   <c>true</c> if we should compress on export; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool Compress { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether there should be only a single output file.
		/// </summary>
		/// <value>
		///   <c>true</c> if there should be a single output file; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool SingleOutputFile { get; set; }

		/// <summary>
		/// Gets or sets the time range to export until now.
		/// </summary>
		/// <value>
		/// The time range.
		/// </value>
		[DataMember]
		public int TimeRange { get; set; }

		/// <summary>
		/// Gets or sets the time range interval.
		/// </summary>
		/// <value>
		/// The time range interval.
		/// </value>
		[DataMember]
		public AxisTimeInterval TimeRangeInterval { get; set; }

		/// <summary>
		/// Gets or sets the filename the data will be exported to.
		/// </summary>
		/// <value>
		/// The filename.
		/// </value>
		[DataMember]
		public string Filename { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether we should export all meters or not.
		/// </summary>
		/// <value>
		///   <c>true</c> if we should export all meters; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool ExportAllMeters { get; set; }
	}
}
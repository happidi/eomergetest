﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Container class for all history entities.	
	/// </summary>
	[DataContract]
	[Key("KeyLogging")]
	public class Logging : LoggedEntry {
		
		/// <summary>
		/// The Key of the logging.
		/// </summary>
		[DataMember]
		public string KeyLogging { get; set; }

		/// <summary>
		/// Gets or sets the duration.
		/// </summary>
		/// <value>
		/// The duration.
		/// </value>
		[DataMember]
		public string Duration { get; set; }

		/// <summary>
		/// Gets or sets the exception.
		/// </summary>
		/// <value>
		/// The exception.
		/// </value>
		[DataMember]
		public string Exception { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is success.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is success; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsSuccess { get; set; }


		/// <summary>
		/// Gets or sets the severity.
		/// </summary>
		/// <value>
		/// The severity.
		/// </value>
		[DataMember]
		public TraceEntrySeverity Severity { get; set; }
	}
}

﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A trace entry business entity.
	/// </summary>
	[DataContract]
	[Key("KeyTraceEntry")]
	[IconCls("viz-icon-small-trace")]
	[LocalizedText("msg_traceentry")]
	public class TraceEntry : LoggedEntry {

		/// <summary>
		/// The key of the trace entry.
		/// </summary>
		[DataMember]
		public string KeyTraceEntry { get; set; }

		/// <summary>
		/// Gets or sets the message.
		/// </summary>
		/// <value>
		/// The message.
		/// </value>
		[DataMember]
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets the name of the method.
		/// </summary>
		/// <value>
		/// The name of the method.
		/// </value>
		[DataMember]
		public string MethodName { get; set; }

		/// <summary>
		/// Gets or sets the related key entity.
		/// </summary>
		/// <value>
		/// The related key entity.
		/// </value>
		[DataMember]
		public string RelatedKeyEntity { get; set; }

		/// <summary>
		/// Gets or sets the trace category.
		/// </summary>
		/// <value>
		/// The trace category.
		/// </value>
		[DataMember]
		public string TraceCategory { get; set; }

		/// <summary>
		/// Gets or sets the severity.
		/// </summary>
		/// <value>
		/// The severity.
		/// </value>
		[DataMember]
		public TraceEntrySeverity Severity { get; set; }
	}
}
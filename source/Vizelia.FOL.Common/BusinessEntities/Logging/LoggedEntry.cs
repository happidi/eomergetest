using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A base class for logged entries that contains general data.
	/// </summary>
	[DataContract]
	public abstract class LoggedEntry : BaseBusinessEntity {
		/// <summary>
		/// Gets or sets the change time.
		/// </summary>
		/// <value>
		/// The change time.
		/// </value>
		[DataMember]
		public DateTime Timestamp { get; set; }

		/// <summary>
		/// Gets or sets the name of the machine.
		/// </summary>
		/// <value>
		/// The name of the machine.
		/// </value>
		[DataMember]
		public string MachineName { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the priority.
		/// </summary>
		/// <value>
		/// The priority.
		/// </value>
		[DataMember]
		public int Priority { get; set; }

		/// <summary>
		/// Gets or sets the process ID.
		/// </summary>
		/// <value>
		/// The process ID.
		/// </value>
		[DataMember]
		public string ProcessID { get; set; }

		/// <summary>
		/// Gets or sets the name of the process.
		/// </summary>
		/// <value>
		/// The name of the process.
		/// </value>
		[DataMember]
		public string ProcessName { get; set; }

		/// <summary>
		/// Gets or sets the request id.
		/// </summary>
		/// <value>
		/// The request id.
		/// </value>
		[DataMember]
		public string RequestId { get; set; }

		/// <summary>
		/// Gets or sets the job run instance.
		/// </summary>
		/// <value>
		/// The request id.
		/// </value>
		[DataMember]
		public string JobRunInstanceRequestId { get; set; }
		
		/// <summary>
		/// Gets or sets the key job.
		/// </summary>
		/// <value>
		/// The key job.
		/// </value>
		[DataMember]
		public string KeyJob { get; set; }
	}
}
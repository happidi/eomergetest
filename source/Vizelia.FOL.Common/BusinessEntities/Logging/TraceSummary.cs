﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A trace entry business entity.
	/// </summary>
	[DataContract]
	[Key("KeyTraceSummary")]
	public class TraceSummary : BaseBusinessEntity {

		/*/// <summary>
		/// The key of the trace summary - dummy.
		/// </summary>
		public string KeyTraceSummary { get; set; }*/

		/// <summary>
		/// Gets or sets the trace category.
		/// </summary>
		/// <value>
		/// The trace category.
		/// </value>
		[DataMember]
		public string TraceCategory { get; set; }

		/// <summary>
		/// Gets or sets the severity.
		/// </summary>
		/// <value>
		/// The severity.
		/// </value>
		[DataMember]
		public TraceEntrySeverity Severity { get; set; }

		/// <summary>
		/// Gets or sets the last hour.
		/// </summary>
		/// <value>
		/// The last hour.
		/// </value>
		[DataMember]
		public int LastHour { get; set; }

		/// <summary>
		/// Gets or sets the last day.
		/// </summary>
		/// <value>
		/// The last day.
		/// </value>
		[DataMember]
		public int LastDay { get; set; }

		/// <summary>
		/// Gets or sets the last week.
		/// </summary>
		/// <value>
		/// The last week.
		/// </value>
		[DataMember]
		public int LastWeek { get; set; }
	}
}

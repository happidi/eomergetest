using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// An abstract Data Acquisition Item, which can be either a folder or an endpoint.
	/// </summary>
	[DataContract]
	public abstract class DataAcquisitionItem : BaseBusinessEntity, ITreeNode, IMappableEntity {
		/// <summary>
		/// The internal identifier.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// The external identifier.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ServerLocalId { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public abstract TreeNode GetTree();

		/// <summary>
		/// Gets the fully qualified name of the provider.
		/// </summary>
		/// <returns></returns>
		[AntiXssValidator]
		[DataMember]
		public abstract string ProviderType { get; set; }

		/// <summary>
		/// Gets the type of the data acquisition container.
		/// </summary>
		/// <returns></returns>
		public abstract Type GetDataAcquisitionContainerType();

		/// <summary>
		/// Gets or sets the key data acquisition endpoint.
		/// </summary>
		/// <value>
		/// The key data acquisition endpoint.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyDataAcquisitionContainer { get; set; }

	}
}
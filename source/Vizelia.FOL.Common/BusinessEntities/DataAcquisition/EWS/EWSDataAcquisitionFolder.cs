using System;
using System.Runtime.Serialization;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A Enterprise Web Services Folder.
	/// </summary>
	[DataContract]
	public class EWSDataAcquisitionFolder : DataAcquisitionFolder {
		private const string const_ews_provider_type = "Vizelia.FOL.ConcreteProviders.EWSDataAcquisitionProvider, Vizelia.FOL.ConcreteProviders";

		/// <summary>
		/// Gets the fully qualified name of the provider.
		/// </summary>
		/// <returns></returns>
		public override string ProviderType {
			get { return const_ews_provider_type; }
			set { }
		}

		/// <summary>
		/// Gets the type of the data acquisition container.
		/// </summary>
		/// <returns></returns>
		public override Type GetDataAcquisitionContainerType() {
			return typeof (EWSDataAcquisitionContainer);
		}

		/// <summary>
		/// Gets or sets the type of the folder.
		/// </summary>
		/// <value>
		/// The type.
		/// </value>
		public string Type { get; set; }
		
		/// <summary>
		/// The iconCls of the entity. The value is retreived from the attribute IconClsAttribute that decorates the entity.
		/// This property is not exposed to client.
		/// </summary>
		protected override string iconCls {
			get {
				string icon;

				// Our interpertation of the EWS entities.
				switch (Type) {
					case "server":
						icon = "viz-icon-small-machineinstance";
						break;
					case "device":
						icon = "viz-icon-small-furniture";
						break;
					case "structure":
						//TODO: Find better icon for this.
						icon = "viz-icon-small-gauge-circular";
						break;
					default:
						//TODO: Find better icon for this.
						//case "folder" is also covered by this.
						icon = "viz-icon-small-folder";
						break;
				}

				return icon;
			}
		}

	}
}
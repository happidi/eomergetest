﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A server containing the Schneider Electric Enterprise Web Services. 
	/// </summary>
	[Key("KeyDataAcquisitionContainer")]
	[IconCls("viz-icon-small-ewsdataacquisitioncontainer")]
	[Serializable]
	[DataContract]
	[LocalizedText("msg_ewsdataacquisitioncontainer")]
	public class EWSDataAcquisitionContainer : DataAcquisitionContainer {
		/// <summary>
		/// Initializes a new instance of the <see cref="EWSDataAcquisitionContainer"/> class.
		/// </summary>
		public EWSDataAcquisitionContainer() {
			this.ProviderType = "Vizelia.FOL.ConcreteProviders.EWSDataAcquisitionProvider, Vizelia.FOL.ConcreteProviders";
		}

		/// <summary>
		/// Gets or sets the username.
		/// </summary>
		/// <value>
		/// The username.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Username { get; set; }

		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>
		/// The password.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Password { get; set; }

		/// <summary>
		/// Gets or sets the authentications scheme.
		/// </summary>
		/// <value>
		/// The authentications scheme.
		/// </value>
		[DataMember]
		public AuthenticationScheme Authentication { get; set; }
	}
}
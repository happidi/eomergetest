﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// An Enterprise Web Services Endpoint.
	/// </summary>
	[Key("KeyDataAcquisitionEndpoint")]
	[IconCls("viz-icon-small-ewsdataacquisitionendpoint")]
	[Serializable]
	[DataContract]
	[LocalizedText("msg_ewsdataacquisitionendpoint")]
	public class EWSDataAcquisitionEndpoint : DataAcquisitionEndpoint {
		private const string const_provider_type = "Vizelia.FOL.ConcreteProviders.EWSDataAcquisitionProvider, Vizelia.FOL.ConcreteProviders";

		/// <summary>
		/// Gets the type of the data acquisition container.
		/// </summary>
		/// <returns></returns>
		public override Type GetDataAcquisitionContainerType() {
			return typeof(EWSDataAcquisitionContainer);
		}

		/// <summary>
		/// Gets the fully qualified name of the provider.
		/// </summary>
		/// <returns></returns>
		public override string ProviderType {
			get { return const_provider_type; }
			set { }
		}
	}
}
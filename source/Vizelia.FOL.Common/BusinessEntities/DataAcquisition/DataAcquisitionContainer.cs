﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// A generic container that contains multiple endpoints that contains data. e.g. Machine with multiple webservices, StruxureWare box with multiple endpoints, etc.
	/// </summary>
	[Key("KeyDataAcquisitionContainer")]
	[Serializable]
	[DataContract]
	public abstract class DataAcquisitionContainer : BaseBusinessEntity, ITreeNode, IMappableEntity {
		/// <summary>
		/// Gets or sets the key data acquisition container.
		/// </summary>
		/// <value>
		/// The key data acquisition container.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyDataAcquisitionContainer { get; set; }


		/// <summary>
		/// The external identifier..
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }


		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>
		/// The TreeNode object.
		/// </returns>
		public TreeNode GetTree() {
			return new TreeNode() {
				Key = this.KeyDataAcquisitionContainer,
				text = this.Title,
				leaf = false,
				entity = this,
				qtip = this.Title,
				iconCls = this.iconCls
			};
		}

		/// <summary>
		/// Gets the fully qualified name of the provider.
		/// </summary>
		/// <returns></returns>
		[AntiXssValidator]
		[DataMember]
		public string ProviderType { get; set; }

		/// <summary>
		/// Gets or sets the URL.
		/// </summary>
		/// <value>
		/// The URL.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Url { get; set; }
	}
}

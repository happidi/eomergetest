﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Endpoint in StruxureWare
	/// </summary>
	[Key("KeyDataAcquisitionEndpoint")]
	[IconCls("viz-icon-small-struxurewaredataacquisitionendpoint")]
	[Serializable]
	[DataContract]
	[LocalizedText("msg_struxurewaredataacquisitionendpoint")]
	public class StruxureWareDataAcquisitionEndpoint : DataAcquisitionEndpoint {
		private const string const_provider_type = "Vizelia.FOL.ConcreteProviders.StruxureWareDataAcquisitionProvider, Vizelia.FOL.ConcreteProviders";

		/// <summary>
		/// Gets the type of the data acquisition container.
		/// </summary>
		/// <returns></returns>
		public override Type GetDataAcquisitionContainerType() {
			return typeof(StruxureWareDataAcquisitionContainer);
		}

		/// <summary>
		/// Gets the fully qualified name of the provider.
		/// </summary>
		/// <returns></returns>
		public override string ProviderType {
			get { return const_provider_type; }
			set { }
		}
	}
}

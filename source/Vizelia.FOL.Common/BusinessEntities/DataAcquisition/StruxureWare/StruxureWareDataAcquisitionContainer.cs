﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Struxureware box that has multiple endpoints
	/// </summary>
	[Key("KeyDataAcquisitionContainer")]
	[IconCls("viz-icon-small-struxurewaredataacquisitioncontainer")]
	[Serializable]
	[DataContract]
	[LocalizedText("msg_struxurewaredataacquisitioncontainer")]
	public class StruxureWareDataAcquisitionContainer : DataAcquisitionContainer {

		/// <summary>
		/// Initializes a new instance of the <see cref="StruxureWareDataAcquisitionContainer"/> class.
		/// </summary>
		public StruxureWareDataAcquisitionContainer() {
			this.ProviderType = "Vizelia.FOL.ConcreteProviders.StruxureWareDataAcquisitionProvider, Vizelia.FOL.ConcreteProviders";
		}
		/// <summary>
		/// Gets or sets the username.
		/// </summary>
		/// <value>
		/// The username.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Username { get; set; }
		/// <summary>
		/// Gets or sets the password.
		/// </summary>
		/// <value>
		/// The password.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Password { get; set; }
	}
}

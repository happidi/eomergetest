﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// REST API with multiple operation
	/// </summary>
	[Key("KeyDataAcquisitionContainer")]
	[IconCls("viz-icon-small-restdataacquisitioncontainer")]
	[Serializable]
	[DataContract]
	[LocalizedText("msg_restdataacquisitioncontainer")]
	public class RESTDataAcquisitionContainer : DataAcquisitionContainer {
		/// <summary>
		/// XPath of the MeterId (used in the XPathRESTDataAcquisitionProvider).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string XPathMeterId { get; set; }

		/// <summary>
		/// XPath of the AcquisitionDateTime (used in the XPathRESTDataAcquisitionProvider).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string XPathAcquisitionDateTime { get; set; }

		/// <summary>
		/// XPath of the AcquisitionDateTime (used in the XPathRESTDataAcquisitionProvider).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string XPathValue { get; set; }

		/// <summary>
		/// Gets or sets the attribute meter id (if not set, we ll use the inner data).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeMeterId { get; set; }

		/// <summary>
		/// Gets or sets the attribute acquisition date time (if not set, we ll use the inner data).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeAcquisitionDateTime { get; set; }

		/// <summary>
		/// Gets or sets the attribute value (if not set, we ll use the inner data).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string AttributeValue { get; set; }

		/// <summary>
		/// Gets or sets the format acquisition date time.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string FormatAcquisitionDateTime { get; set; }

		/// <summary>
		/// Gets or sets the Key of the LocalizationCulture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocalizationCulture { get; set; }

		/// <summary>
		/// Gets or sets the Custom Data
		/// </summary>
		/// <remarks>
		/// This property is used to store proprietary data used by the various provider types of the REST container
		/// </remarks>
		[AntiXssValidator]
		[DataMember]
		public string CustomData { get; set; }
	}
}

﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Endpoint in REST
	/// </summary>
	[Key("KeyDataAcquisitionEndpoint")]
	[IconCls("viz-icon-small-restdataacquisitionendpoint")]
	[Serializable]
	[DataContract]
	[LocalizedText("msg_restdataacquisitionendpoint")]
	public class RESTDataAcquisitionEndpoint : DataAcquisitionEndpoint {
		/// <summary>
		/// Gets or sets the query string that will be appended to the URL when a query is issued.
		/// The Url for the REST query is made out of container.Url+Path+QueryString
		/// </summary>
		/// <value>
		/// The query.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string QueryString { get; set; }

		/// <summary>
		/// Gets the type of the data acquisition container.
		/// </summary>
		/// <returns></returns>
		public override Type GetDataAcquisitionContainerType() {
			return typeof(RESTDataAcquisitionContainer);
		}

		/// <summary>
		/// Gets the fully qualified name of the provider.
		/// </summary>
		/// <returns></returns>
		public override string ProviderType { get; set; }

	}
}

﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Represents an endpoint that supplies data to Vizelia
	/// </summary>
	[Key("KeyDataAcquisitionEndpoint")]
	[Serializable]
	[DataContract]
	public abstract class DataAcquisitionEndpoint : DataAcquisitionItem {

		/// <summary>
		/// Gets or sets the key data acquisition endpoint.
		/// </summary>
		/// <value>
		/// The key data acquisition endpoint.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyDataAcquisitionEndpoint { get; set; }


		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>
		/// The TreeNode object.
		/// </returns>
		public override TreeNode GetTree() {
			return new TreeNode() {
				Key = this.KeyDataAcquisitionEndpoint,
				text = this.Name,
				leaf = true,
				entity = this,
				qtip = this.Name,
				iconCls = this.iconCls
			};
		}

		/// <summary>
		/// Gets the Title of the Container.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string TitleContainer { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		[DataMember]
		public double Value { get; set; }

		/// <summary>
		/// Gets or sets the value string.
		/// </summary>
		/// <value>
		/// The value string.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string ValueString { get; set; }

		/// <summary>
		/// Gets or sets the unit.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Unit { get; set; }

		/// <summary>
		/// Gets or sets the path.
		/// </summary>
		/// <value>
		/// The path.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Path { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }



		/// <summary>
		/// Gets the full name.
		/// </summary>
		[DataMember]
		public string FullName {
			get {
				return string.Format("{0} {1}", TitleContainer, Name);
			}
			private set { ;}
		}
	}
}

﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Represents a folder of endpoints that supply data to Vizelia
	/// </summary>
	[Key("KeyDataAcquisitionFolder")]
	[Serializable]
	[DataContract]
	public abstract class DataAcquisitionFolder : DataAcquisitionItem {

		/// <summary>
		/// Gets or sets the key data acquisition folder.
		/// </summary>
		/// <value>
		/// The key data acquisition folder.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyDataAcquisitionFolder { get; set; }

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>
		/// The TreeNode object.
		/// </returns>
		public override TreeNode GetTree() {
			return new TreeNode() {
				Key = this.KeyDataAcquisitionFolder,
				text = this.Name,
				leaf = false,
				entity = this,
				qtip = this.Name,
				iconCls = this.iconCls
			};
		}

	}
}
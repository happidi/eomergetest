﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Represents an endpoint value;
	/// </summary>
	[Serializable]
	[DataContract]
	public class DataAcquisitionEndpointValue : BaseBusinessEntity {

		/// <summary>
		/// Gets or sets the acquisition date time.
		/// </summary>
		/// <value>
		/// The acquisition date time.
		/// </value>
		public DateTime AcquisitionDateTime { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		public double Value { get; set; }


		/// <summary>
		/// The unit.
		/// </summary>
		public string Unit { get; set; }
	}
}

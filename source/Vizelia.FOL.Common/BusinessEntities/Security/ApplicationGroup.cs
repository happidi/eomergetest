﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using NetSqlAzMan.Interfaces;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an application group item.
	/// </summary>
	[Key("Id")]
	[IconCls("viz-icon-small-applicationgroup")]
	[DataContract]
	[LocalizedText("msg_applicationgroup")]
	[AuditableEntityAttribute]
	public class ApplicationGroup : BaseBusinessEntity, ITreeNode, IMappableEntity {

		/// <summary>
		/// Public ctor.
		/// </summary>
		public ApplicationGroup() {
		}

		/// <summary>
		/// Public ctor.
		/// Used to convert an IAzManItem into a AuthorizationItem.
		/// </summary>
		/// <param name="item">An IAzManItem.</param>
		public ApplicationGroup(IAzManApplicationGroup item) {
			this.Id = item.SID.StringValue;
			this.Name = item.Name;
			this.Description = item.Description;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ApplicationGroup"/> class.
		/// </summary>
		/// <param name="sid">The sid.</param>
		/// <param name="item">The item.</param>
		public ApplicationGroup(IAzManSid sid, IAzManItem item) {
			this.Id = sid.StringValue;
			this.Name = item.Name;
			this.Description = item.Description;
		}

		/// <summary>
		/// The id of the authorization item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Id { get; set; }

		/// <summary>
		/// The LocalId of the item.
		/// The dummy setter is there only for serialization purpose.
		/// </summary>
		public string LocalId {
			get { return Name; }
			set { Name = value; }
		}

		/// <summary>
		/// The name of the authorization item.
		/// </summary>
		[Information("msg_name", null)]
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }


		/// <summary>
		/// The description of the authorization item.
		/// </summary>
		[Information("msg_description", null)]
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// The icon cls of the item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconCls {
			get {
				return this.iconCls;
			}
			set { ;}
		}

		/// <summary>
		/// The list of users of the ApplicationGroup.
		/// This field is defined only in order to get the list from a xml mapping document.
		/// Otherwise it should remain null.
		/// </summary>
		public List<FOLMembershipUser> Users { get; set; }

		/// <summary>
		/// The list of roles of the ApplicationGroup.
		/// This field is defined only in order to get the list from a xml mapping document.
		/// Otherwise it should remain null.
		/// </summary>
		public List<AuthorizationItem> Roles { get; set; }

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				entity = this,
				id = this.Id,
				text = this.Name,
				qtip = this.Description,
				iconCls = this.iconCls,
				leaf = true
			};
		}
	}
}

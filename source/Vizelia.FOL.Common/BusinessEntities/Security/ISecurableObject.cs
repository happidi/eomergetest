﻿namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Interface for the security of objects
	/// </summary>
	public interface ISecurableObject {
		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>The SecurableEntity object.</returns>
		SecurableEntity GetSecurableObject();

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		string Creator { get; set; }
	}
}

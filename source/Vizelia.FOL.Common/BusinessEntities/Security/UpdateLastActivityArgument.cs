﻿using System;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Special argument for the Membership in order to optimize call to update the last user activity.
	/// </summary>
	public class UpdateLastActivityArgument : BaseBusinessEntity {


		/// <summary>
		/// Gets or sets the name of the user.
		/// </summary>
		/// <value>
		/// The name of the user.
		/// </value>
		public string UserName { get; set; }


	}
}

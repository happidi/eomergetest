﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
    /// <summary>
    /// Business entity for the authentication provider.
    /// </summary>
	[DataContract]
	public class AuthenticationProvider : BaseBusinessEntity {
        /// <summary>
        /// The name of the provider.
        /// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

        /// <summary>
        /// The description of the provider.
        /// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A business entity for storing active session information.
	/// It is primarly used to store the expiration date of a fire and forget session.
	/// </summary>
	[Serializable]
	public class ActiveSession : BaseBusinessEntity {

		/// <summary>
		/// Gets or sets the expiration date.
		/// </summary>
		/// <value>
		/// The expiration date.
		/// </value>
		public DateTime ExpirationDate  { get; set; }

		/// <summary>
		/// Gets or sets the name of the user.
		/// </summary>
		/// <value>
		/// The name of the user.
		/// </value>
		public string UserName { get; set; }

		/// <summary>
		/// Gets or sets the session ID.
		/// </summary>
		/// <value>
		/// The session ID.
		/// </value>
		public string SessionID  { get; set; }

	}
}

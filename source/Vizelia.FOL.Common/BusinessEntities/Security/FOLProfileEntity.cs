﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Web.Profile;
using System.Configuration;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Class representing exposing specific properties for Profile.
	/// This class can be sent through WCF.
	/// Each public property should make use of the inner _profile object.
	/// Each public property can make use of the following attributes [DefaultSettingValue("unkown IP")] , [ReadOnly], [SettingsSerializeAs(SettingsSerializeAs.String)]
	/// 
	/// ATTENTION 
	/// For a public DateTime property there is a bug when json is returned. We have to define 
	/// [DefaultSettingValue("01/01/2001")]	
	/// [SettingsSerializeAs(SettingsSerializeAs.String)]
	/// </summary>
	[DataContract]
	[Serializable]
	[KeyAttribute("UserName")]
	public class FOLProfileEntity : BaseBusinessEntity {
		[NonSerialized]
		private ProfileBase _profile;

		private const string propertyname_IP = "IP";
		/// <summary>
		/// Public ctor.
		/// </summary>
		public FOLProfileEntity() {
		}

		/// <summary>
		/// Public ctor to link to the FOLProfile instance.
		/// </summary>
		/// <param name="profile"></param>
		public FOLProfileEntity(ProfileBase profile) {
			_profile = profile;
		}


		/// <summary>
		/// The user name.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string UserName {
			get {
				if (_profile == null)
					return null;
				return _profile.UserName;

			}
			set { ;}
		}

		/// <summary>
		/// The IP of the user when last connected.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[DefaultSettingValue("")]
		[SettingsSerializeAs(SettingsSerializeAs.String)]
		public string IP {
			get {
				if (_profile == null)
					return null;
				return (string)_profile[propertyname_IP];
			}
			set {
				if (_profile != null)
					_profile[propertyname_IP] = value;
			}
		}

	}
}


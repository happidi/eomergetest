﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A business entity for storing session context.
	/// </summary>
	[Serializable]
	public class SessionContext : BaseBusinessEntity {

		/// <summary>
		/// Initializes a new instance of the <see cref="SessionContext"/> class.
		/// </summary>
		public SessionContext() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="SessionContext"/> class.
		/// </summary>
		/// <param name="sessionContextString">The session context string. format is: SessionID, UserName, ExpirationDate.ToString()</param>
		public SessionContext(string sessionContextString) {
			var retVal = Helper.DeserializeJson<SessionContext>(sessionContextString);
			SessionID = retVal.SessionID;
			UserName = retVal.UserName;
			ExpirationDate = retVal.ExpirationDate;
			LastRefreshDate = retVal.LastRefreshDate;
			/*
			string[] values = sessionContextString.Split('|');
			SessionID = values[0];
			UserName = values[1];
			ExpirationDate = DateTime.Parse(values[2]);
			LastRefreshDate = DateTime.Parse(values[3]);
			 */
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString() {
			var retVal = Helper.SerializeJson(this, GetType());
			return retVal;
			//return string.Join("|", SessionID, UserName, ExpirationDate.ToString(CultureInfo.InvariantCulture), LastRefreshDate.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary>
		/// Gets or sets the session ID.
		/// </summary>
		/// <value>
		/// The session ID.
		/// </value>
		public string SessionID { get; set; }

		/// <summary>
		/// Gets or sets the expiration date.
		/// </summary>
		/// <value>
		/// The expiration date.
		/// </value>
		public DateTime ExpirationDate { get; set; }

		/// <summary>
		/// Gets or sets the name of the user.
		/// </summary>
		/// <value>
		/// The name of the user.
		/// </value>
		public string UserName { get; set; }

		/// <summary>
		/// The list of keys of session items.
		/// </summary>
		public List<string> Keys { get; set; }

		/// <summary>
		/// Gets or sets the last refresh date.
		/// </summary>
		/// <value>
		/// The last refresh date.
		/// </value>
		public DateTime LastRefreshDate { get; set; }


	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for FOL providerUserKey.
	/// This class enable to attach together the FOL User to it's Membership information.
	/// </summary>
	[DataContract]
	[Serializable]
	public class FOLProviderUserKey : BaseBusinessEntity {

		/// <summary>
		/// The Membership Guid of the user.
		/// </summary>
		[DataMember]
		public Guid? Guid { get; set; }

		/// <summary>
		/// The FOL KeyOccupant of the user.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyOccupant { get; set; }

		/// <summary>
		/// The FOL KeyUser of the user
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyUser { get; set; }

		/// <summary>
		/// The Occupant attached to the user.
		/// </summary>
		[DataMember]
		public Occupant Occupant { get; set; }

		/// <summary>
		/// The user Profile.
		/// </summary>
		[DataMember]
		public FOLProfileEntity Profile { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [user forgot password].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [user forgot password]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsForgotPassword { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether the password for this user should [never expire].
		/// </summary>
		/// <value>
		///   <c>true</c> if [never expires]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool NeverExpires { get; set; }

        /// <summary>
        /// Gets or sets the time zone identifier.
        /// </summary>
        /// <value>
        /// The time zone identifier.
        /// </value>
        [Information("msg_user_time_zone", null)]
        [AntiXssValidator]
        [DataMember]
		[Obsolete]
        public string TimeZoneId { get; set; }

        /// <summary>
        /// Gets or sets the time zone identifier.
        /// </summary>
        /// <value>
        /// The time zone identifier.
        /// </value>
        [Information("msg_applicationgroup_root", null)]
        [AntiXssValidator]
        [DataMember]
        public string ApplicationGroups { get; set; }

        /// <summary>
        /// The KeyLocation of the Occupant.
        /// </summary>
        [AntiXssValidator]
        [DataMember]
        [BatchUpdatable(true)]
        //[FilterType(typeof(Location))]
        public string KeyLocation { get; set; }

        /// <summary>
        /// The location id path of the Occupant.
        /// Contains the entire hierarchy up to the root level.
        /// </summary>
        [AntiXssValidator]
        [DataMember]
        public string UserLocationLocalIdPath { get; set; }

        /// <summary>
        /// The location long path of the Occupant.
        /// Contains the entire hierarchy up to the root level.
        /// </summary>
        [Information("msg_location_path", null)]
        [AntiXssValidator]
        [DataMember]
        [ViewableByDataModel]
        public string UserLocationLongPath { get; set; }

        /// <summary>
        /// The location short path of the Occupant.
        /// Contains only the deepest site level and further levels.
        /// </summary>
        [AntiXssValidator]
        [DataMember]
        [ViewableByDataModel]
        public string UserLocationShortPath { get; set; }

        /// <summary>
        /// The location level. Starts at level 0.
        /// </summary>
        [DataMember]
        public int? UserLocationLevel { get; set; }

        /// <summary>
        /// The location name of the Occupant.
        /// </summary>
        [AntiXssValidator]
        [DataMember]
        [ViewableByDataModel]
        public string UserLocationName { get; set; }

        /// <summary>
        /// The location typename of the Occupant.
        /// </summary>
        [DataMember]
        public HierarchySpatialTypeName UserLocationTypeName { get; set; }

        /// <summary>
        /// Gets or sets the projects count.
        /// </summary>
        /// <value>
        /// The projects count.
        /// </value>
        [DataMember]
        public int ProjectsCount { get; set; }

		/// <summary>
		/// The User's first name
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string FirstName { get; set; }

		/// <summary>
		/// The User's first name
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LastName { get; set; }
	}

}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using NetSqlAzMan.Interfaces;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an authorization item.
	/// </summary>
	[Key("Id")]
	[DataContract]
	[Serializable]
	[LocalizedText("msg_authorizationitem")]
	[AuditableEntityAttribute]
	public class AuthorizationItem : BaseBusinessEntity, ITreeNode, IMappableEntity {
		private const string const_iconcls_filter = "viz-icon-small-filter";
		private const string const_iconcls_role = "viz-icon-small-role";
		private const string const_iconcls_task = "viz-icon-small-azmantask";
		private const string const_iconcls_operation = "viz-icon-small-azmanoperation";
		private const string const_msgcode_filter = "msg_authorizationitem_filter";
		private const string const_msgcode_role = "msg_authorizationitem_role";
		private const string const_msgcode_task = "msg_authorizationitem_task";
		private const string const_msgcode_operation = "msg_authorizationitem_operation";

		/// <summary>
		/// Gets the icon CLS filter.
		/// </summary>
		public static string IconClsFilter {
			get { return const_iconcls_filter; }
			private set { }
		}

		/// <summary>
		/// Gets the icon CLS role.
		/// </summary>
		public static string IconClsRole {
			get { return const_iconcls_role; }
			private set { }
		}

		/// <summary>
		/// Gets the icon CLS task.
		/// </summary>
		public static string IconClsTask {
			get { return const_iconcls_task; }
			private set { }
		}

		/// <summary>
		/// Gets the icon CLS operation.
		/// </summary>
		public static string IconClsOperation {
			get { return const_iconcls_operation; }
			private set { }
		}

		/// <summary>
		/// Gets the icon CLS filter.
		/// </summary>
		public static string MsgCodeFilter {
			get { return const_msgcode_filter; }
			private set { }
		}

		/// <summary>
		/// Gets the icon CLS role.
		/// </summary>
		public static string MsgCodeRole {
			get { return const_msgcode_role; }
			private set { }
		}

		/// <summary>
		/// Gets the icon CLS task.
		/// </summary>
		public static string MsgCodeTask {
			get { return const_msgcode_task; }
			private set { }
		}

		/// <summary>
		/// Gets the icon CLS operation.
		/// </summary>
		public static string MsgCodeOperation {
			get { return const_msgcode_operation; }
			private set { }
		}

		private const string const_attribute_type = "Type";

		/// <summary>
		/// Gets the item.
		/// </summary>
		public IAzManItem Item { get; set; }

		/// <summary>
		/// Public ctor.
		/// </summary>
		public AuthorizationItem() {
		}
		
		/// <summary>
		/// Public ctor.
		/// Used to convert an IAzManItem into a AuthorizationItem.
		/// </summary>
		/// <param name="item">An IAzManItem.</param>
		/// <param name="calculateLongPath">An IAzManItem.</param>
		public AuthorizationItem(IAzManItem item, bool calculateLongPath = false) {
			this.Item = item;
			this.Id = item.ItemId.ToString(CultureInfo.InvariantCulture);
			this.Name = item.Name;
			this.Description = item.Description;
			this.ItemType = this.GetItemType();
			if (calculateLongPath) {
				this.LongPath = GetFilterLongPath(item);
			}
		}

		/// <summary>
		/// Get azman filter role long path
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		private string GetFilterLongPath(IAzManItem item) {
			string result = item.Name;
			var parents = item.ItemsWhereIAmAMember.Values;

			while (parents.Count > 0) {
				var currItem = parents.First();
				result = currItem.Name + " / " + result;
				parents = currItem.ItemsWhereIAmAMember.Values;
			}

			return result;
		}
		
		/// <summary>
		/// Checks if the attributeFilterName exists, and is equals to the attributeFilterValue if this parameter is not null.
		/// </summary>
		/// <param name="item">The item on which to perform the check.</param>
		/// <param name="attributeFilterName">The name of the attribute.</param>
		/// <param name="attributeFilterValue">The value of the attribute (if null then only the function will only check if the attributeFilterName exists on the item).</param>
		/// <param name="normalCheck">If false, reverse the check to allow getting all nodes except the ones corresponding to the check.</param>
		/// <returns></returns>
		private bool AzManItem_HasAttributeFilter(IAzManItem item, string attributeFilterName, string attributeFilterValue, bool normalCheck) {
			if (String.IsNullOrEmpty(attributeFilterName))
				return normalCheck;
			bool result = item.Attributes.ContainsKey(attributeFilterName);
			if (!result || String.IsNullOrEmpty(attributeFilterValue))
				return normalCheck == result;
			
			return normalCheck == (item.Attributes[attributeFilterName].Value == attributeFilterValue);
		}

		/// <summary>
		/// Gets the type of the authorization item (Role, Filter, Task, Operation, ApplicationGroup, StoreGroup).
		/// </summary>
		/// <returns></returns>
		private string GetItemType() {
			if (this.AzManItem_HasAttributeFilter(this.Item, const_attribute_type, null, true))
				return this.Item.Attributes[const_attribute_type].Value;
			return this.Item.ItemType.ParseAsString();
		}

		/// <summary>
		/// The icon cls of the item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconCls {
			get {
				return this.GetIcon();
			}
			set { ;}
		}

		/// <summary>
		/// The id of the authorization item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Id { get; set; }

		/// <summary>
		/// The name of the authorization item.
		/// </summary>
		[Information("msg_name", null)]
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// The description of the authorization item.
		/// </summary>
		[Information("msg_description", null)]
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// The type of the authorization item (Role, Task, Operation, ApplicationGroup, StoreGroup).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ItemType { get; set; }

		/// <summary>
		/// The Id of the parent.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ParentId { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is filter only.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is filter only; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsFilterOnly { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is tenancy task.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is tenancy task; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsTenancyTask { get; set; }


		/// <summary>
		/// Gets the iconCls for an IAzManItem.
		/// </summary>
		/// <returns>The iconCls for the item as a string.</returns>
		private string GetIcon() {
			string icon_cls = "";
			if (this.Item != null) {
				switch (this.Item.ItemType) {
					case NetSqlAzMan.Interfaces.ItemType.Operation:
						icon_cls = const_iconcls_operation;
						break;
					case NetSqlAzMan.Interfaces.ItemType.Task:
						icon_cls = const_iconcls_task;
						break;
					case NetSqlAzMan.Interfaces.ItemType.Role:
						if (this.Item.Attributes.ContainsKey(const_attribute_type)) {
							var attribute = Item.Attributes[const_attribute_type];
							icon_cls = "viz-icon-small-" + attribute.Value.ToLower();
						}
						else
							icon_cls = const_iconcls_role;
						break;
				}
			}
			else {
				switch (this.ItemType) {
					case "Role":
                        icon_cls = IsFilterOnly ? const_iconcls_filter : const_iconcls_role;
						break;
					case "Task":
						icon_cls = const_iconcls_task;
						break;
					case "Operation":
						icon_cls = const_iconcls_operation;
						break;
					default:
						icon_cls = "viz-icon-small-" + this.ItemType.ToLower();
						break;
				}
			}
			return icon_cls;
		}

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				entity = this,
				//id = this.Id,
				Key = this.Id,
				text = this.Name,
				qtip = this.Description,
				iconCls = this.GetIcon(),
				leaf = false
			};
		}

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <param name="ParentId">The parent Id of the node.</param>
		/// <returns></returns>
		public TreeNode GetTree(string ParentId) {
			this.ParentId = ParentId;
			return this.GetTree();
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		public string LocalId {
			get { return Name; }
			set { Name = value; }
		}

		/// <summary>
		/// Gets or sets the long path for filter searches.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LongPath { get; set; }

		/// <summary>
		/// Gets or sets the application groups.
		/// </summary>
		/// <value>
		/// The application groups.
		/// </value>
		public List<ApplicationGroup> ApplicationGroups { get; set; }

		/// <summary>
		/// Gets or sets the users.
		/// </summary>
		/// <value>
		/// The users.
		/// </value>
		public List<FOLMembershipUser> Users { get; set; }

		/// <summary>
		/// User names
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string UserNames { get; set; }

		/// <summary>
		/// Gets or sets the tasks.
		/// </summary>
		/// <value>
		/// The tasks.
		/// </value>
		public List<AuthorizationItem> Tasks { get; set; }

		/// <summary>
		/// Gets or sets the filter spatial.
		/// </summary>
		/// <value>
		/// The filter spatial.
		/// </value>
		public List<SecurableEntity> FilterSpatial { get; set; }

		/// <summary>
		/// Gets or sets the filter report.
		/// </summary>
		/// <value>
		/// The filter report.
		/// </value>
		public List<SecurableEntity> FilterReport { get; set; }

		/// <summary>
		/// Gets or sets the filter chart.
		/// </summary>
		/// <value>
		/// The filter chart.
		/// </value>
		public List<SecurableEntity> FilterChart { get; set; }

		/// <summary>
		/// Gets or sets the filter alarm definition.
		/// </summary>
		/// <value>
		/// The filter alarm definition.
		/// </value>
		public List<SecurableEntity> FilterAlarmDefinition { get; set; }

		/// <summary>
		/// Gets or sets the filter portal window.
		/// </summary>
		/// <value>
		/// The filter portal window.
		/// </value>
		public List<SecurableEntity> FilterPortalWindow { get; set; }

		/// <summary>
		/// Gets or sets the filter classification item.
		/// </summary>
		/// <value>
		/// The filter classification item.
		/// </value>
		public List<SecurableEntity> FilterClassificationItem { get; set; }

		/// <summary>
		/// Gets or sets the filter dynamic display.
		/// </summary>
		/// <value>
		/// The filter dynamic display.
		/// </value>
		public List<SecurableEntity> FilterDynamicDisplay { get; set; }

		/// <summary>
		/// Gets or sets the filter playlist.
		/// </summary>
		/// <value>
		/// The filter playlist.
		/// </value>
		public List<SecurableEntity> FilterPlaylist { get; set; }

		/// <summary>
		/// Gets or sets the filter playlist.
		/// </summary>
		/// <value>
		/// The filter playlist.
		/// </value>
		public List<SecurableEntity> FilterLink { get; set; }

		/// <summary>
		/// Gets or sets the filter chart scheduler.
		/// </summary>
		/// <value>
		/// The filter chart scheduler.
		/// </value>
		public List<SecurableEntity> FilterChartScheduler { get; set; }

		/// <summary>
		/// Gets or sets the filter portal template.
		/// </summary>
		/// <value>
		/// The filter portal template.
		/// </value>
		public List<SecurableEntity> FilterPortalTemplate { get; set; }
	}
}

﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// The User settings
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeyUser")]
	[IconCls("viz-icon-small-user")]
	[LocalizedText("msg_user_preferences")]
	public class UserPreferences : BaseBusinessEntity, IMappableEntity {
	    /// <summary>
		/// The ctor
		/// </summary>
		public UserPreferences() {
			GridsPageSize = 50;
		}
		/// <summary>
		/// The Application
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string Application { get; set; }

		/// <summary>
		/// The user key
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string KeyUser { get; set; }

		/// <summary>
		/// The row in a grid
		/// </summary>
		[DataMember]
		public int GridsPageSize { get; set; }

		/// <summary>
		/// Whether to preload all the user's charts
		/// </summary>
		[DataMember]
		public bool PreloadCharts { get; set; }

		/// <summary>
		/// The Desktop background
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string DesktopBackground { get; set; }

		/// <summary>
		/// The Custom desktop background
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string CustomDesktopBackground { get; set; }

		/// <summary>
		/// The desktop text color - in Hex
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string DesktopTextColor { get; set; }

	    /// <summary>
	    /// The timezone id
	    /// </summary>
	    [DataMember]
        [AntiXssValidator]
	    public string TimeZoneId { get; set; }

	    /// <summary>
		/// The key chart of the KPI
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string DefaultChartKPI { get; set; }

		/// <summary>
		/// Local Id
		/// </summary>
		public string LocalId { get; set; }
	}
}
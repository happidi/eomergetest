﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.Common.Validators;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for Login history.
	/// </summary>
	[DataContract]
	[Key("KeyLoginHistory")]
	public class LoginHistory : BaseBusinessEntity {


		/// <summary>
		/// Key of the Meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLoginHistory { get; set; }

		/// <summary>
		/// Gets or sets the name of the user.
		/// </summary>
		/// <value>The name of the user.</value>
		[AntiXssValidator]
		[DataMember]
		public string UserName { get; set; }

		/// <summary>
		/// Gets or sets the IP.
		/// </summary>
		/// <value>The IP.</value>
		[AntiXssValidator]
		[DataMember]
		public string IP { get; set; }

		/// <summary>
		/// Gets or sets the login date.
		/// </summary>
		/// <value>The login date.</value>
		[DataMember]
		public DateTime? LoginDate { get; set; }

		/// <summary>
		/// Gets or sets the logout date.
		/// </summary>
		/// <value>The logout date.</value>
		[DataMember]
		public DateTime? LogoutDate { get; set; }

	}
}

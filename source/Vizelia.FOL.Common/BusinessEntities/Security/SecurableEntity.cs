﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Base business entity with security features.
	/// If an entity derives from a SecurableBaseBusinessEntity it means that
	/// it can be associated with ApplicationGroups. you do that from within the Project Hierarchy.
	/// examples for securable entities are : Location, Chart, Report
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("KeySecurable")]
	public class SecurableEntity : BaseBusinessEntity, ITreeNode, IMappableEntity {
		/// <summary>
		/// Using this key and the SecurableTypeName together, we can reterieve the real business
		/// entity.
		/// </summary>
		/// <value>
		/// The Identifier for the entity, e.g. : KeyLocation Value or KeyChart Value
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeySecurable { get; set; }

		/// <summary>
		/// the Fully Qualified Name of the concrete Type. 
		/// E.g. Vizelia.FOL.BusinessEntities.Chart
		/// </summary>
		/// <value>
		/// The name of the securable type.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string SecurableTypeName {
			get;
			set;
		}

		/// <summary>
		/// The display text in the UI of the entity.
		/// </summary>
		/// <value>
		/// The name of the securable.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string SecurableName { get; set; }


		/// <summary>
		/// The display icon in the UI for the entity
		/// </summary>
		/// <value>
		/// The securable icon CLS.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string SecurableIconCls { get; set; }


		/// <summary>
		/// Convert this Location into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				iconCls = this.SecurableIconCls,
				text = this.SecurableName,
				leaf = true,
				id = this.KeySecurable
			};
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId { get; set; }
	}
}

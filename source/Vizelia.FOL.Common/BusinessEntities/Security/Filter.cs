﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using System.Runtime.Serialization;
using NetSqlAzMan.Interfaces;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an application group item.
	/// </summary>
	[Serializable]
	public class Filter : BaseBusinessEntity {

		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>
		/// The key.
		/// </value>
		public string Key { get; set; }

		/// <summary>
		/// Gets or sets the filter type.
		/// </summary>
		/// <value>
		/// The type.
		/// </value>
		public FilterType Type { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="Filter"/> class.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="type">The type.</param>
		public Filter(string key, FilterType type) {
			Key = key;
			Type = type;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Filter"/> class.
		/// </summary>
		public Filter(string key) {
			Key = key;
			Type = FilterType.Direct;
		}
	}
}

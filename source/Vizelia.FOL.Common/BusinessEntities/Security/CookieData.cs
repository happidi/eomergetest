﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// The data stored within FormsAuthenticationTicket.UserData
	/// </summary>
	public class CookieData {


		/// <summary>
		/// Initializes a new instance of the <see cref="CookieData"/> class.
		/// </summary>
		public CookieData() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CookieData"/> class.
		/// </summary>
		/// <param name="userData">The user data.</param>
		public CookieData(string userData) {
			try {
				string[] values = userData.Split(',');
				SessionId = values[0];
				ApplicationName = values[1];
				PageId = values[2];
			}
			catch {
			}
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString() {
			return SessionId + "," + ApplicationName + "," + PageId;
		}

		/// <summary>
		/// Gets or sets the session id.
		/// </summary>
		/// <value>
		/// The session id.
		/// </value>
		public string SessionId { get; set; }
		/// <summary>
		/// Gets or sets the page id.
		/// </summary>
		/// <value>
		/// The page id.
		/// </value>
		public string PageId { get; set; }

		/// <summary>
		/// Gets or sets the name of the application.
		/// </summary>
		/// <value>
		/// The name of the application.
		/// </value>
		public string ApplicationName { get; set; }

	}
}

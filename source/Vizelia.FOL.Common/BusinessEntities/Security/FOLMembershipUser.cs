﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Lightweight class representing the MembershipUser class entity sent from WCF
	/// </summary>
	[Serializable]
	[DataContract]
	[Key("UserName")]
	[IconCls("viz-icon-small-user")]
	[LocalizedText("msg_user")]
	[AuditableEntityAttribute]
	public class FOLMembershipUser : BaseBusinessEntity, ITreeNode, IMappableEntity {
		private const string const_iconcls_offline = "viz-icon-small-status-offline";
		private const string const_iconcls_online = "viz-icon-small-status-online";
		private const string const_iconcls_unapproved = "viz-icon-small-status-unapproved";

		/// <summary>
		/// Public constructor. No parameter as it is needed for serialization to JSON
		/// </summary>
		public FOLMembershipUser() {

		}

		/// <summary>
		/// Public ctor.
		/// </summary>
		/// <param name="user">an instance of MembershipUser</param>
		public FOLMembershipUser(MembershipUser user) {
			UserName = user.UserName;
			IsApproved = user.IsApproved;
			IsLockedOut = user.IsLockedOut;
			IsOnline = user.IsOnline;
			Email = user.Email;
			Comment = user.Comment;
			LastLoginDate = user.LastLoginDate;
			ProviderUserKey = (FOLProviderUserKey)user.ProviderUserKey;
			ApplicationName = ContextHelper.ApplicationName;
			KeyUser = ProviderUserKey != null ? ProviderUserKey.KeyUser : null;
		}

		/// <summary>
		/// Gets or sets the key user.
		/// </summary>
		/// <value>
		/// The key user.
		/// </value>
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyUser { get; set; }

		/// <summary>
		/// The user name.
		/// </summary>
		[Information("msg_username", null)]
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string UserName { get; set; }

		/// <summary>
		/// Gets or sets the name of the application.
		/// </summary>
		/// <value>
		/// The name of the application.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string ApplicationName { get; set; }


		/// <summary>
		/// The LocalId of the Occupant.
		/// The dummy setter is there only for serialization purpose.
		/// </summary>
		public string LocalId {
			get { return UserName; }
			set { UserName = value; }
		}

		/// <summary>
		/// Returns true if the user is locked.
		/// </summary>
		[DataMember]
		[Information("msg_user_islocked", null)]
		public bool IsLockedOut { get; set; }

		/// <summary>
		/// The password. This field is defined only in order to get the password from a xml mapping document.
		/// Otherwise it should never expose the password of the user.
		/// </summary>
		[Information("msg_password", null)]
		public string Password { get; set; }

		/// <summary>
		/// Gets or sets the initial password.
		/// </summary>
		/// <value>
		/// The initial password.
		/// </value>
		[DataMember]
		public string InitialPassword { get; set; }

		/// <summary>
		/// Returns true if the user is approved.
		/// </summary>
		[Information("msg_user_isapproved", null)]
		[DataMember]
		public bool IsApproved { get; set; }

		/// <summary>
		/// Returns true if the user is online.
		/// </summary>
		[Information("msg_user_isonline", null)]
		[DataMember]
		public bool IsOnline { get; set; }

		/// <summary>
		/// The user's email.
		/// </summary>
		[Information("msg_occupant_email", null)]
		[AntiXssValidator]
		[DataMember]
		public string Email { get; set; }

		/// <summary>
		/// Comments.
		/// </summary>
		[Information("msg_comment", null)]
		[AntiXssValidator]
		[DataMember]
		public string Comment { get; set; }

		/// <summary>
		/// the last date the user logged in.
		/// </summary>
		[Information("msg_user_lastlogindate", null)]
		[DataMember]
		public DateTime LastLoginDate { get; set; }

		/// <summary>
		/// The FOL ProviderUserKey.
		/// </summary>
		[DataMember]
		public FOLProviderUserKey ProviderUserKey { get; set; }

		/// <summary>
		/// Gets the key occupant.
		/// </summary>
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string KeyOccupant {
			get {
				return ProviderUserKey != null ? ProviderUserKey.KeyOccupant : null;
			}
			set {
				ProviderUserKey = ProviderUserKey ?? new FOLProviderUserKey();
				ProviderUserKey.KeyOccupant = value;
			}
		}

		/// <summary>
		/// Gets user's Guid.
		/// </summary>
		[DataMember]
		public Guid? Guid {
			get {
				return ProviderUserKey != null ? ProviderUserKey.Guid : null;
			}
			set {
				ProviderUserKey = ProviderUserKey ?? new FOLProviderUserKey();
				ProviderUserKey.Guid = value;
			}
		}

		/// <summary>
		/// Convert this MemberUser into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				id = UserName,
				text = UserName + " - " + ProviderUserKey.Occupant.FullName,
				leaf = true,
				iconCls = GetIconCls(),
				qtip = "Last login :" + LastLoginDate.ToString(CultureInfo.InvariantCulture) + "<br>IP :" + ProviderUserKey.Profile.IP,
				entity = this
			};
		}

		/// <summary>
		/// Gets the cls icon for the user's node.
		/// </summary>
		/// <returns>The cls icon.</returns>
		private string GetIconCls() {
			if (!IsApproved)
				return const_iconcls_unapproved;
			return IsOnline ? const_iconcls_online : const_iconcls_offline;
		}

		/// <summary>
		/// Gets the cls icon for the user's node and expose it to client.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public new string iconCls {
			get {
				return GetIconCls();
			}
			set { }
		}

		/// <summary>
		/// Gets the occupant full name. 
		/// The data is presented here so it can be used by a grid filter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string OccupantFullName {
			get {
				try {
					if (ProviderUserKey != null && ProviderUserKey.Occupant != null) {
						return ProviderUserKey.Occupant.FullName;
					}
				}
				catch { }
				return null;
			}
			set { }
		}

		/// <summary>
		/// Gets or sets the first name of the occupant.
		/// </summary>
		/// <value>
		/// The first name of the occupant.
		/// </value>
		[DataMember]
		public string OccupantFirstName {
			get {
				try {
					if (ProviderUserKey != null && ProviderUserKey.Occupant != null) {
						return ProviderUserKey.Occupant.FirstName;
					}
				}
				catch { }
				return null;
			}
			set { }
		}

		/// <summary>
		/// Gets or sets the last name of the occupant.
		/// </summary>
		/// <value>
		/// The last name of the occupant.
		/// </value>
		[DataMember]
		public string OccupantLastName {
			get {
				try {
					if (ProviderUserKey != null && ProviderUserKey.Occupant != null) {
						return ProviderUserKey.Occupant.LastName;
					}
				}
				catch { }
				return null;
			}
			set { }
		}

		/// <summary>
		/// Gets the occupant key location
		/// The data is presented here so it can be used by a grid filter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(Location))]
		public string KeyLocation {
			get {
				string location = null;
				if (ProviderUserKey != null && ProviderUserKey.Occupant != null) {
					location = ProviderUserKey.Occupant.KeyLocation;
				}
				return location;
			}
			set { }
		}


		/// <summary>
		/// Gets or sets a value indicating whether [user must change password at next logon].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [user must change password at next logon]; otherwise, <c>false</c>.
		/// </value>
		public bool UserMustChangePasswordAtNextLogon { get; set; }

		/// <summary>
		/// Gets or sets the user's preferences.
		/// </summary>
		[DataMember]
		public UserPreferences Preferences { get; set; }

		/// <summary>
		/// Gets or sets the user's first name
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string FirstName {
			get { return ProviderUserKey != null ? ProviderUserKey.FirstName : null; }
			set {
				ProviderUserKey = ProviderUserKey ?? new FOLProviderUserKey();
				ProviderUserKey.FirstName = value;
			}
		}

		/// <summary>
		/// Gets or sets the user's last name
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		public string LastName {
			get { return ProviderUserKey != null ? ProviderUserKey.LastName : null; }
			set {
				ProviderUserKey = ProviderUserKey ?? new FOLProviderUserKey();
				ProviderUserKey.LastName = value;
			}
		}
	}
}

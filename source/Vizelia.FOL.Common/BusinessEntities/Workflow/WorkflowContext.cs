﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A simple class for passing context between workflows.
	/// </summary>
	/// <remarks></remarks>
	public class WorkflowContext : BaseBusinessEntity, IWorkflowEntity {
		/// <summary>
		/// The workflow instance id.
		/// </summary>
		/// <value>The instance id.</value>
		/// <remarks></remarks>
		public Guid InstanceId {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the state.
		/// </summary>
		/// <value>The state.</value>
		/// <remarks></remarks>
		public string State {
			get { return null; }
			set { }
		}

		/// <summary>
		/// Gets or sets the state label.
		/// </summary>
		/// <value>The state label.</value>
		/// <remarks></remarks>
		public string StateLabel {
			get { return null; }
			set { }
		}
	}
}

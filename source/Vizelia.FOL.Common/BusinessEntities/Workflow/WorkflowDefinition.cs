﻿using System;
using System.Activities;
using System.Collections.Generic;


namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Holds the workflow definitions
	/// </summary>
	public class WorkflowDefinition : BaseBusinessEntity {

		/// <summary>
		/// Gets or sets the initial state.
		/// </summary>
		/// <value>The initial state.</value>
		public string  InitialState { get; set; }

		/// <summary>
		/// Gets or sets the type of the workflow.
		/// </summary>
		/// <value>The type of the workflow.</value>
		public Type  WorkflowType { get; set; }

		/// <summary>
		/// Gets or sets the workflow activity.
		/// </summary>
		/// <value>The workflow activity.</value>
		public Activity WorkflowActivity { get; set; }

		/// <summary>
		/// Gets or sets the type of the local service.
		/// </summary>
		/// <value>The type of the local service.</value>
		public Type LocalServiceType { get; set; }

		/// <summary>
		/// Gets or sets the workflow extensions.
		/// </summary>
		/// <value>The workflow extensions.</value>
		public List<Type> WorkflowExtensions { get; set; }

		/// <summary>
		/// Gets or sets the assembly and namespace dictionary.
		/// </summary>
		/// <value>The assembly and namespace dictionary.</value>
		public Dictionary<string, string> AssemblyAndNamespaceDictionary { get; set; }

	}
}

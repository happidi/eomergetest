﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A simple genric class for Workflow instance.
	/// </summary>
	/// <remarks></remarks>
	[Key("KeyWorkflowInstance")]
	public class WorkflowInstance : BaseBusinessEntity {


		/// <summary>
		/// Gets or sets the key workflow instance.
		/// </summary>
		/// <value>
		/// The key workflow instance.
		/// </value>
		public string KeyWorkflowInstance {
			get; 
			set;
		}

		/// <summary>
		/// The workflow instance id.
		/// </summary>
		/// <value>The instance id.</value>
		/// <remarks></remarks>
		public Guid InstanceId {
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the state.
		/// </summary>
		/// <value>The state.</value>
		/// <remarks></remarks>
		public string State {
			get; 
			set;
		}


		/// <summary>
		/// Gets or sets the type of the object.
		/// </summary>
		/// <value>
		/// The type of the object.
		/// </value>
		public string ObjectType {
			get; 
			set;
		}
	}
}

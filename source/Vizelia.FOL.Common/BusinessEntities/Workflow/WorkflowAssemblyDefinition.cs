﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Common.Validators;


namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for a workflow assembly definition.
	/// </summary>
	[Key("AssemblyQualifiedName")]
	[DataContract]
	[IfcTypeName("IfcWorkflowAssemblyDefinition")]
	[IconCls("viz-icon-small-workflow")]
	public class WorkflowAssemblyDefinition : IfcBaseBusinessEntity, ITreeNode {

		/// <summary>
		/// Gets or sets the assembly short name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string AssemblyShortName { get; set; }

		/// <summary>
		/// Gets or sets the full name of the assembly.
		/// </summary>
		/// <value>
		/// The full name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string AssemblyQualifiedName { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Description {
			set;
			get;
		}

		/// <summary>
		/// Gets or sets the short type name of the entity argument.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string EntityShortName {
			set;
			get;
		}

		/// <summary>
		/// Gets or sets the full type name of the entity argument.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string EntityQualifiedName {
			set;
			get;
		}

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>
		/// The TreeNode object.
		/// </returns>
		public TreeNode GetTree() {
			return new TreeNode {
				Key = this.AssemblyQualifiedName,
				text = this.AssemblyShortName,
				leaf = true,
				entity = this,
				qtip = this.Description + (String.IsNullOrEmpty(this.Description) ? "" : "\n") + this.AssemblyQualifiedName,
				iconCls = this.iconCls
			};
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Interface for entity involved as argument of workflow EventArgs
	/// </summary>
	public interface IWorkflowEntity {

		/// <summary>
		/// The workflow instance id.
		/// </summary>
		Guid InstanceId { get; set; }


		/// <summary>
		/// Gets or sets the state.
		/// </summary>
		/// <value>The state.</value>
		string State { get; set; }



		/// <summary>
		/// Gets or sets the state label.
		/// </summary>
		/// <value>The state label.</value>
		string StateLabel { get; set; }
	}

}

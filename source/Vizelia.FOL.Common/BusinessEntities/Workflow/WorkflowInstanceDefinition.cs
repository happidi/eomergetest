﻿using System;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A class representing a workflow instance definition
	/// </summary>
	[Key("KeyWorkflowInstanceDefinition")]
	public class WorkflowInstanceDefinition : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>The key.</value>
		public string KeyWorkflowInstanceDefinition { get; set; }

		/// <summary>
		/// Gets or sets the instance id.
		/// </summary>
		/// <value>The instance id.</value>
		public Guid InstanceId { get; set; }

		/// <summary>
		/// Gets or sets the xaml.
		/// </summary>
		/// <value>The xaml.</value>
		public string Xaml { get; set; }

		/// <summary>
		/// Gets or sets the xaml design
		/// </summary>
		public string XamlDesign { get; set; }

		/// <summary>
		/// Gets or sets the initial state.
		/// </summary>
		/// <value>The initial state.</value>
		public string InitialState { get; set; }

		/// <summary>
		/// Indiciates if the instance is currently attached to a workflow.
		/// </summary>
		public bool HasWorkflow { get; set; }

		/// <summary>
		/// Gets or sets the name of the workflow assembly.
		/// </summary>
		public string WorkflowAssemblyQualifiedName { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId {
			get { return KeyWorkflowInstanceDefinition; }
			set { }
		}


	}
}

﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Drawing;


namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Holds the arguments passed to the workflow viewer utility in order to render an image of the workflow.
	/// </summary>
	public class WorkflowViewerArgument : BaseBusinessEntity {

		/// <summary>
		/// The list (! is the separator) of assemblies to load.
		/// </summary>
		public string[] AssembliesToLoad { get; set; }

		/// <summary>
		/// The full path of the xaml file. Overrided by the XamlDesign if not null.
		/// </summary>
		public string XamlFile { get; set; }


		/// <summary>
		/// The content of the xaml design. It will override the XamlFile when not null.
		/// </summary>
		public string XamlDesign { get; set; }

		/// <summary>
		/// The full path of the result image file.
		/// </summary>
		public string ResultFile { get; set; }

		/// <summary>
		/// The width of the image.
		/// </summary>
		public int Width { get; set; }

		/// <summary>
		/// The height of the image.
		/// </summary>
		public int Height { get; set; }

		/// <summary>
		/// The current state to higlight.
		/// </summary>
		public string CurrentState { get; set; }

		/// <summary>
		/// Gets or sets the color (html format) that the current state should be hilighted with.
		/// </summary>
		public string HtmlColorCurrentState { get; set; }

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A class representing external actions that can be triggered on a workflow.
	/// </summary>
	[Key("KeyWorkflowActionElement")]
	[IconCls("viz-icon-small-event")]
	[DataContract]
	public class WorkflowActionElement : ListElement, ITreeNode {

		/// <summary>
		/// Gets or sets the key workflow action element.
		/// </summary>
		/// <value>
		/// The key workflow action element.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyWorkflowActionElement {
			get {
				return this.Id + "_" + this.AssemblyQualifiedName;
			}
			set {
				;
			}
		}


		/// <summary>
		/// Gets or sets the pre action.
		/// </summary>
		/// <value>
		/// The pre action.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string PreAction { get; set; }

		/// <summary>
		/// Gets or sets the assembly short name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string AssemblyShortName { get; set; }

		/// <summary>
		/// Gets or sets the full name of the assembly.
		/// </summary>
		/// <value>
		/// The full name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string AssemblyQualifiedName { get; set; }

		/// <summary>
		/// Returns the Extjs TreeNode object.
		/// </summary>
		/// <returns>
		/// The TreeNode object.
		/// </returns>
		public TreeNode GetTree() {
			return new TreeNode {
				Key = this.Id,
				text = this.Label,
				leaf = true,
				entity = this,
				qtip = this.AssemblyShortName + '\n' + this.AssemblyQualifiedName,
				iconCls = this.iconCls
			};
		}
	}
}

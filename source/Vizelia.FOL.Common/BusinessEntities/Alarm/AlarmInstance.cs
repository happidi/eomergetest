﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a AlarmInstance.
	/// </summary>
	[Serializable]
	[Key("KeyAlarmInstance")]
	[DataContract]
	[IconCls("viz-icon-small-alarminstance")]
	[LocalizedText("msg_alarminstance")]
	public class AlarmInstance : BaseBusinessEntity, IMappableEntity, ISupportLocation {
		/// <summary>
		/// Key of the AlarmInstance.
		/// </summary>
		[AntiXssValidator]
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyAlarmInstance { get; set; }

		/// <summary>
		/// Key of the AlarmDefinition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyAlarmDefinition { get; set; }

		/// <summary>
		/// Key of the AlarmDefinition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyAlarmDefinitionLocation { get; set; }

		/// <summary>
		/// Gets or sets the alarm definition.
		/// </summary>
		[DataMember]
		public AlarmDefinition AlarmDefinition { get; set; }


		/// <summary>
		/// Key of the MeterValidationRule.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyMeterValidationRule { get; set; }

		/// <summary>
		/// Gets or sets the Meter Validation Rule.
		/// </summary>
		[DataMember]
		public MeterValidationRule MeterValidationRule { get; set; }

		/// <summary>
		/// The creatation DateTime of this instance.
		/// </summary>
		[DataMember]
		public DateTime CreateDateTime { get; set; }

		/// <summary>
		/// The Instance DateTime (the element (meterdata, datapoint) that has generated this alarm.
		/// </summary>
		[DataMember]
		public DateTime InstanceDateTime { get; set; }

		/// <summary>
		/// The status.
		/// </summary>
		[DataMember]
		public AlarmInstanceStatus Status { get; set; }

		/// <summary>
		/// The value that has triggered the alarm.
		/// </summary>
		[DataMember]
		public double Value { get; set; }

		/// <summary>
		/// The description
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the key meter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyMeter { get; set; }

		/// <summary>
		/// Gets or sets the key data serie.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyDataSerie { get; set; }

		/// <summary>
		/// Gets or sets the key chart.
		/// </summary>
		/// <value>
		/// The key chart.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyChart { get; set; }

		/// <summary>
		/// Key of the MeterData the alarm instance is attached too (when instance is coming from a MeterValidationRule).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyMeterData { get; set; }

		/// <summary>
		/// MeterData AcquisitionDateTime start, the alarm instance is attached too (when instance is coming from a MeterValidationRule).
		/// </summary>
		[DataMember]
		public DateTime? MeterDataAcquisitionDateTimeStart { get; set; }

		/// <summary>
		/// MeterData AcquisitionDateTime end, the alarm instance is attached too (when instance is coming from a MeterValidationRule).
		/// </summary>
		[DataMember]
		public DateTime? MeterDataAcquisitionDateTimeEnd { get; set; }

		/// <summary>
		/// Gets or sets the type of the meter validation rule this instance is attached too.
		/// </summary>
		[DataMember]
		public MeterValidationRuleAlarmType MeterValidationRuleAlarmType { get; set; }

		/// <summary>
		/// Gets or sets the data serie local id.(when the KeyDataSerie is null)
		/// </summary>
		/// <value>
		/// The data serie local id.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string DataSerieLocalId { get; set; }

		/// <summary>
		/// The name of the meter or the data serie name , or the data serie local id.(when the KeyDataSerie is null)
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// The Threshold
		/// </summary>
		[DataMember]
		public double? Threshold { get; set; }

		/// <summary>
		/// The Threshold high value (in case of a Between condition).
		/// </summary>
		[DataMember]
		public double? Threshold2 { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// //TODO : Dor look at this . this is similar to MeterData
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the Key of the associated ActionRequest
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyActionRequest { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether an email has already been sent.
		/// </summary>
		[DataMember]
		public bool? EmailSent { get; set; }


		/// <summary>
		/// The key of the classification of the Alarm Definition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItem { get; set; }

		/// <summary>
		/// The key path of the classification of the Alarm Definition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItemPath { get; set; }

		/// <summary>
		/// The classification title of the Alarm Definition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemTitle { get; set; }

		/// <summary>
		/// The classification level of the Alarm Definition.
		/// </summary>
		[DataMember]
		public int ClassificationItemLevel { get; set; }

		/// <summary>
		/// The classification path of the Alarm Definition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLongPath { get; set; }

		/// <summary>
		/// The classification local id path of the Alarm Definition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLocalIdPath { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorR { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorG { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorB { get; set; }


		/// <summary>
		/// Key of the parent Location.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocation { get; set; }

		/// <summary>
		/// The location long path of the Furniture.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[Information("msg_location_path", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocationLongPath { get; set; }

		/// <summary>
		/// The location short path of the Furniture.
		/// Contains only the deepest site level and further levels.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationShortPath { get; set; }

		/// <summary>
		/// The location level. Starts at level 0.
		/// </summary>
		[DataMember]
		public int? LocationLevel { get; set; }

		/// <summary>
		/// The location name of the Furniture.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationName { get; set; }

		/// <summary>
		/// The location typename of the Furniture.
		/// </summary>
		[DataMember]
		public HierarchySpatialTypeName LocationTypeName { get; set; }

        /// <summary>
        /// Indicates wether the instance is a chart instance or not
        /// </summary>
        [DataMember]
        public bool IsChartInstance {
            get { return !String.IsNullOrEmpty(this.KeyChart); }
            set { }
        }

        /// <summary>
        /// The chart instance date time in string format.
        /// </summary>
        [AntiXssValidator]
        [DataMember]
        public string ChartInstanceDateTime { get; set; }

        /// <summary>
        /// The chart instance date time in UTC.
        /// </summary>
        [DataMember]
        public DateTime? UTCInstanceDateTime { get; set; }
	}


	/// <summary>
	/// Business entity that represent the number of AlarmInstance that occured at a specific location for a specific Classification.
	/// Used when we display AlarmInstances as DataSeries.
	/// </summary>
	[Serializable]
	[DataContract]
	public class AlarmInstanceAsDataSource : BaseBusinessEntity {
		/// <summary>
		/// Key of the AlarmDefinition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyAlarmDefinitionLocation { get; set; }

		/// <summary>
		/// The key of the classification of the Alarm Definition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItem { get; set; }

		/// <summary>
		/// The Instance DateTime (the element (meterdata, datapoint) that has generated this alarm.
		/// </summary>
		[DataMember]
		public DateTime InstanceDateTime { get; set; }


		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		[DataMember]
		public int Value { get; set; }
	}
}

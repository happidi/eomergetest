﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a AlarmDefinition.
	/// </summary>
	[Serializable]
	[Key("KeyAlarmDefinition")]
	[DataContract]
	[IconCls("viz-icon-small-alarm")]
	[LocalizedText("msg_alarmdefinition")]
	[IfcTypeName("AlarmDefinition")]
	[AuditableEntity]
	public class AlarmDefinition : BaseBusinessEntity, ISecurableObject, ISupportLocation, ITreeNode, IMappableEntity {
		/// <summary>
		/// Key of the AlarmDefinition.
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		[FilterType(typeof(AlarmDefinition), false)]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyAlarmDefinition { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		[Information("msg_title", null)]
		[ViewableByDataModel]
		[DataMember]
		[AntiXssValidator]
		[NotNullValidator]
		[System.ComponentModel.DataAnnotations.Required]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
        [Information("msg_description", null)]
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// The key of the classification of the Alarm Definition.
		/// </summary>
		[Information("msg_alarmdefinition_enabled", null)]
		[NotNullValidator]
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string KeyClassificationItem { get; set; }

		/// <summary>
		/// The key path of the classification of the Alarm Definition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItemPath { get; set; }

		/// <summary>
		/// The classification title of the Alarm Definition.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemTitle { get; set; }

		/// <summary>
		/// The classification level of the Alarm Definition.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public int ClassificationItemLevel { get; set; }

		/// <summary>
		/// The classification path of the Alarm Definition.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLongPath { get; set; }

		/// <summary>
		/// The classification local id path of the Alarm Definition.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLocalIdPath { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorR { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorG { get; set; }

		/// <summary>
		/// The classification Red Color Component.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorB { get; set; }


		/// <summary>
		/// The Key of the parent ClassificationItem for the ActionRequest.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationKeyParent { get; set; }

		/// <summary>
		/// The Key of the child ClassificationItem for the ActionRequest.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationKeyChildren { get; set; }

		/// <summary>
		/// The classification path of the ActionRequest.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string ClassificationLongPath { get; set; }


		/// <summary>
		/// The Key of the location of the Alarm definition.
		/// </summary>
		[NotNullValidator]
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Required]
		public string KeyLocation { get; set; }

		/// <summary>
		/// The location long path of the Meter.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[Information("msg_alarmdefinition_enabled", null)]
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string LocationLongPath { get; set; }

		/// <summary>
		/// The location short path of the Meter.
		/// Contains only the deepest site level and further levels.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string LocationShortPath { get; set; }

		/// <summary>
		/// The location level. Starts at level 0.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public int? LocationLevel { get; set; }

		/// <summary>
		/// The location name of the Meter.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string LocationName { get; set; }

		/// <summary>
		/// The location typename of the Meter.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public HierarchySpatialTypeName LocationTypeName { get; set; }


		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		[Information("msg_alarmdefinition_startdate", null)]
		[ViewableByDataModel]
		[DataMember]
		public DateTime? StartDate { get; set; }

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		[Information("msg_alarmdefinition_enddate", null)]
		[ViewableByDataModel]
		[DataMember]
		public DateTime? EndDate { get; set; }

		/// <summary>
		/// Gets or sets the Key of the CalendarEventCategory associated to this Chart.
		/// </summary>

		[Information("msg_calendar", null)]
		[DataMember]
		[AntiXssValidator]
		public string KeyCalendarEventCategory { get; set; }

		/// <summary>
		/// Gets or sets the CalendarEventCategory associated to this Chart.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public CalendarEventCategory CalendarEventCategory { get; set; }

		/// <summary>
		/// Calendar mode : Include or Exclude.
		/// </summary>
		[Information("msg_chart_calendarmode", null)]
		[ViewableByDataModel]
		[DataMember]
		public ChartCalendarMode CalendarMode { get; set; }

		/// <summary>
		/// Gets or sets the meters.
		/// </summary>
		[DataMember]
		public List<Meter> Meters { get; set; }

		/// <summary>
		/// Gets or sets the Meter grouping Timeinterval when processing the AlarmDefinition.
		/// </summary>
		[Information("msg_alarmdefinition_timeinterval", null)]
		[DataMember]
		public AxisTimeInterval TimeInterval { get; set; }

		/// <summary>
		/// Gets or sets the Charts.
		/// </summary>
		[DataMember]
		public List<Tuple<Chart, List<Meter>, List<Location>>> Charts { get; set; }

		/// <summary>
		/// Gets or sets the Charts.
		/// </summary>
		// TODO: this should be refactored to Charts
		[DataMember]
		public List<Chart> ConnectedCharts { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// <c>true</c> if enabled; otherwise, <c>false</c>.
		/// </summary>
		[Information("msg_alarmdefinition_enabled", null)]
		[ViewableByDataModel]
		[DataMember]
		public bool Enabled { get; set; }

		/// <summary>
		/// The Threshold
		/// </summary>
		[Information("msg_alarmdefinition_threshold", null)]
		[ViewableByDataModel]
		[DataMember]
		public double? Threshold { get; set; }

		/// <summary>
		/// The Threshold high value (in case of a Between condition).
		/// </summary>
		[Information("msg_alarmdefinition_threshold2", null)]
		[ViewableByDataModel]
		[DataMember]
		public double? Threshold2 { get; set; }

		/// <summary>
		/// the filtering condition to use : Equals, Higher, Lower, Between, Outside
		/// </summary>
		[Information("msg_alarmdefinition_condition", null)]
		[ViewableByDataModel]
		[DataMember]
		public FilterCondition Condition { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity {
				KeySecurable = KeyAlarmDefinition,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(GetType()),
				SecurableName = Title,
				SecurableTypeName = GetType().FullName
			};
		}

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }


		/// <summary>
		/// Convert this AlarmDefinition into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				id = KeyAlarmDefinition,
				Key = KeyAlarmDefinition,
				text = Title,
				leaf = true,
				entity = this,
				iconCls = iconCls,
				qtip = GetTreeTooltip()
			};
		}

		/// <summary>
		/// Gets the tree tooltip.
		/// </summary>
		/// <returns></returns>
		private string GetTreeTooltip() {
			var retVal = ClassificationItemLongPath;
			return retVal;
		}

		/// <summary>
		/// <c>true</c> if true; we calculate the delta beetween the current value and the previous one in %, else we apply the condition to the current value.
		/// </summary>
		[Information("msg_alarmdefinition_usepercentagedelta", null)]
		[ViewableByDataModel]
		[DataMember]
		public bool UsePercentageDelta { get; set; }

		/// <summary>
		/// List of emails to send notification when no ActionRequest as been defined. 
		/// </summary>
		[Information("msg_alarmdefinition_emails", null)]
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Emails { get; set; }

		/// <summary>
		/// If set to false the alarm will only be processed if assigned to a specific sceduled job.
		/// </summary>
		[Information("msg_alarmdefinition_processmanually", null)]
		[ViewableByDataModel]
		[DataMember]
		public bool ProcessManually { get; set; }

		/// <summary>
		/// Gets or sets the Key of the LocalizationCulture.
		/// </summary>
		[Information("msg_playlist_culture", null)]
		[DataMember]
		public string KeyLocalizationCulture { get; set; }

		/// <summary>
		/// Gets or sets the time zone id in which we want to render the chart.
		/// </summary>
		/// <value>
		/// The time zone id.
		/// </value>
		[Information("msg_chart_timezone", null)]
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string TimeZoneId { get; set; }

		/// <summary>
		/// Gets or sets the last processe datetime
		/// </summary>
		[Information("msg_alarmdefinition_lastprocessdatetime", null)]
		[DataMember]
		public DateTime? LastProcessDateTime { get; set; }

		/// <summary>
		/// Gets the time zone.
		/// </summary>
		/// <returns></returns>
		public TimeZoneInfo GetTimeZone() {
			var retVal = CalendarHelper.GetTimeZoneFromId(TimeZoneId);
			return retVal;
		}

		/// <summary>
		/// Gets or sets the filter meter classifications.
		/// </summary>
		/// <value>
		/// The filter meter classifications.
		/// </value>
		public List<ClassificationItem> FilterMeterClassifications { get; set; }

		/// <summary>
		/// Gets or sets the filter spatial.
		/// </summary>
		/// <value>
		/// The filter spatial.
		/// </value>
		public List<Location> FilterSpatial { get; set; }

		/// <summary>
		/// Gets or sets the filter chart. Used for mapping procces.
		/// </summary>
		/// <value>
		/// The filter chart.
		/// </value>
		public List<Chart> FilterCharts { get; set; }

	}
}
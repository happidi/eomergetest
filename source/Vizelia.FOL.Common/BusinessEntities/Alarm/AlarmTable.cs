﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Map.
	/// </summary>
	[Key("KeyAlarmTable")]
	[IconCls("viz-icon-small-alarmtable")]
	[DataContract]
	[LocalizedText("msg_alarmtable")] 
	public class AlarmTable : BaseBusinessEntity, ITreeNode, ISecurableObject, IPortlet, IMappableEntity {

		/// <summary>
		/// Key of the AlarmTable.
		/// </summary>
		[DataMember]
		[AntiXssValidator]
		[FilterType(typeof(AlarmTable), false)]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyAlarmTable { get; set; }

		/// <summary>
		/// Gets or sets the title of the AlarmTable.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }


		/// <summary>
		/// Gets or sets the alarm instance status.
		/// </summary>
		[DataMember]
		public AlarmInstanceStatus AlarmInstanceStatus { get; set; }

		/// <summary>
		/// List of Alarm Definitions associated to this AlarmTable.
		/// </summary>
		[DataMember]
		public List<AlarmDefinition> AlarmDefinitions { get; set; }
	
		/// <summary>
		/// Convert this AlarmTable into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				Key = KeyAlarmTable,
				text = Title,
				leaf = true,
				entity = this,
				qtip = Title,
				iconCls = iconCls
			};
		}

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity {
				KeySecurable = KeyAlarmTable,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(GetType()),
				SecurableName = Title,
				SecurableTypeName = GetType().FullName
			};
		}


		/// <summary>
		/// Sets the title.
		/// </summary>
		/// <param name="title">The title.</param>
		public void SetTitle(string title) {
			Title = title;
		}


		/// <summary>
		/// Gets or sets a value indicating whether [open on startup].
		/// </summary>
		/// <value>
		///   <c>true</c> if [open on startup]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool OpenOnStartup { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether this instance is favorite.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is favorite; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsFavorite { get; set; }


		/// <summary>
		/// Gets the title.
		/// </summary>
		/// <returns></returns>
		public string GetTitle() {
			return Title;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Custom comparer for BaseBusinessEntity.
	/// </summary>
	public class BaseBusinessEntityComparer<T> : IEqualityComparer<T> where T : BaseBusinessEntity {
		/// <summary>
		/// Equals
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		public bool Equals(T x, T y) {
			string idProperty = Helper.GetPropertyNameId(typeof(T));
			if (String.IsNullOrEmpty(idProperty))
				return false;
			if (x == null || y == null)
				return false;
			return x.GetPropertyValueString(idProperty, false, false) == y.GetPropertyValueString(idProperty, false, false);
			//return x.Equals(y); // this.GetHashCode(x) == this.GetHashCode(y);
		}

		/// <summary>
		/// Gets the hash code.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public int GetHashCode(T obj) {
			string idProperty = Helper.GetPropertyNameId(typeof(T));
			if (String.IsNullOrEmpty(idProperty))
				return obj.GetHashCode();

			var propertyValue = obj.GetPropertyValueString(idProperty, false, false);
			return propertyValue == null ? obj.ToString().ToLower().GetHashCode() : propertyValue.GetHashCode();

			//return obj.GetPropertyValueString(idProperty, false, false) == null ? null : obj.GetPropertyValueString(idProperty, false, false).GetHashCode();
		}


	}

}

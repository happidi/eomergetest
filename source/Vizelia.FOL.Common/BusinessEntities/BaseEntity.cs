﻿using System;
using System.Runtime.Serialization;


namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Base class of all entities with no attributes to limit the memory footprint. BaseBusinessEntity derives from this class.
	/// </summary>
	[Serializable]
	[DataContract]
	public abstract class BaseEntity {
	}
}

﻿using System;
using System.Runtime.Serialization;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing an EnergyAggregatorContext; we use this usually as the first param to energy aggregator operations.
	/// </summary>
	[Serializable]
	[DataContract]
	public class EnergyAggregatorContext : BaseBusinessEntity {

		/// <summary>
		/// If the EA should start in a 
		/// </summary>
		private bool m_StartInSeperateThread = true;

		/// <summary>
		/// Gets or sets the username.
		/// </summary>
		/// <value>
		/// The username.
		/// </value>
		[DataMember]
		public string Username { get; set; }

		/// <summary>
		/// Gets or sets the tenant URL.
		/// </summary>
		/// <value>
		/// The tenant URL.
		/// </value>
		[DataMember]
		public string TenantUrl { get; set; }
		/// <summary>
		/// Gets or sets the name of the application.
		/// </summary>
		/// <value>
		/// The name of the application.
		/// </value>
		[DataMember]
		public string ApplicationName { get; set; }

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		/// <value>
		/// The start date.
		/// </value>
		[DataMember]
		public DateTime StartDate { get; set; }

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		/// <value>
		/// The end date.
		/// </value>
		[DataMember]
		public DateTime EndDate { get; set; }

		/// <summary>
		/// Gets or sets the culture.
		/// </summary>
		/// <value>
		/// The culture.
		/// </value>
		[DataMember]
		public string Culture { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether to [start in seperate thread].
		/// </summary>
		/// <value>
		///  <c>true</c> if [start in seperate thread]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool StartInSeperateThread
		{
			get { return m_StartInSeperateThread; }
			set { m_StartInSeperateThread = value; }
		}

        /// <summary>
        /// The user's login cookie. Algorithms running inside EA can use this to log in
        /// as the current user.
        /// </summary>
        [DataMember]
        public string LoginCookie { get; set; }

        /// <summary>
        /// The page id embedded in the login cookie, so that algorithms don't have to extract it.
        /// </summary>
        [DataMember]
        public string PageId { get; set; }
    }
}


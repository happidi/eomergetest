﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for user profile preference
	/// </summary>
	[Key("KeyUserProfilePreference")]
	[DataContract]
	public class UserProfilePreference : BaseBusinessEntity {
		/// <summary>
		/// Gets or sets the key user profile preference.
		/// </summary>
		/// <value>
		/// The key user profile preference.
		/// </value>
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyUserProfilePreference { get; set; }

		/// <summary>
		/// Gets or sets the key user.
		/// </summary>
		/// <value>
		/// The key user.
		/// </value>
		[DataMember]
		public string KeyUser { get; set; }

		/// <summary>
		/// Gets or sets the name of the user.
		/// </summary>
		/// <value>
		/// The name of the user.
		/// </value>
		[DataMember]
		public string UserName {
			// KeyUser field is not really the key user but the user name.
			// This new field will be exposed via OData and will reflect "KeyUser" field.
			get { return KeyUser; } 
			set { KeyUser = value; }
		}

		/// <summary>
		/// Gets or sets the preference key.
		/// </summary>
		/// <value>
		/// The preference key.
		/// </value>
		[DataMember]
		public string PreferenceKey { get; set; }

		/// <summary>
		/// Gets or sets the preference value.
		/// </summary>
		/// <value>
		/// The preference value.
		/// </value>
		[DataMember]
		public string PreferenceValue { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>The SecurableEntity object.</returns>
		public SecurableEntity GetSecurableObject() {
			return new SecurableEntity {
				KeySecurable = KeyUserProfilePreference,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(GetType()),
				SecurableName = PreferenceKey,
				SecurableTypeName = GetType().FullName
			};
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }
	}
}

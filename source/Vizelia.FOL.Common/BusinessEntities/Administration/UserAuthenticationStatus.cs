﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// The response for a IsUserAuthenticated call.
	/// </summary>
	[Serializable]
	[DataContract]
	public class UserAuthenticationStatus {
		/// <summary>
		/// Gets or sets a value indicating whether the user is authenticated.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if the user is authenticated; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsAuthenticated { get; set; }

		/// <summary>
		/// Gets or sets the page id.
		/// </summary>
		/// <value>
		/// The page id.
		/// </value>
		[DataMember]
		[AntiXssValidator]
		public string PageId { get; set; }

	}
}
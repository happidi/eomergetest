﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities{
	/// <summary>
	/// Business entity for CalendarOccurrences class. This class is a persisted to speed up EA load.
	/// </summary>
	[Key("KeyCalendarOccurrences")]
	public class CalendarOccurrences : BaseBusinessEntity {

		/// <summary>
		/// Gets or sets the key calendar occurrences.
		/// </summary>
		/// <value>
		/// The key calendar occurrences.
		/// </value>
		public string KeyCalendarOccurrences { get; set; }

		/// <summary>
		/// Gets or sets the calendar occurrences data. This is a serialized list of VizEventOccurrences for the given calendar/dates/timezone
		/// </summary>
		/// <value>
		/// The calendar occurrences data.
		/// </value>
		public byte[] CalendarOccurrencesData { get; set; }

		/// <summary>
		/// Gets or sets the key calendar that these occurrences were calculated for.
		/// </summary>
		/// <value>
		/// The key calendar.
		/// </value>
		public string KeyCalendar { get; set; }

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		/// <value>
		/// The start date.
		/// </value>
		public DateTime StartDate { get; set; }
		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		/// <value>
		/// The end date.
		/// </value>
		public DateTime EndDate { get; set; }
		/// <summary>
		/// Gets or sets the time zone.
		/// </summary>
		/// <value>
		/// The time zone.
		/// </value>
		public string TimeZone { get; set; }

		/// <summary>
		/// Gets or sets the icaldata string.
		/// </summary>
		/// <value>
		/// The icaldata.
		/// </value>
		public string iCalData { get; set; }
	}
}

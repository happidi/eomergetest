﻿using System.Runtime.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for custom data
	/// </summary>
	[Key("KeyCustomData")]
	[DataContract]
	public class CustomData : BaseBusinessEntity {
		/// <summary>
		/// Gets or sets the key custom data.
		/// </summary>
		/// <value>
		/// The key custom data.
		/// </value>
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyCustomData { get; set; }

		/// <summary>
		/// Gets or sets the name of the container.
		/// </summary>
		/// <value>
		/// The name of the container.
		/// </value>
		[DataMember]
		public string ContainerName { get; set; }

		/// <summary>
		/// Gets or sets the key user.
		/// </summary>
		/// <value>
		/// The key user.
		/// </value>
		[DataMember]
		public string KeyUser { get; set; }

		/// <summary>
		/// Gets or sets the data key.
		/// </summary>
		/// <value>
		/// The data key.
		/// </value>
		[DataMember]
		public string DataKey { get; set; }

		/// <summary>
		/// Gets or sets the data value.
		/// </summary>
		/// <value>
		/// The data value.
		/// </value>
		[DataMember]
		public string DataValue { get; set; }
	}
}

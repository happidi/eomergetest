﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a Portal.
	/// </summary>
	[Key("KeyLink")]
	[DataContract]
	[IconCls("viz-icon-small-link")]
	[LocalizedText("msg_link")]
	public class Link : BaseBusinessEntity,  IMappableEntity, ISecurableObject {

		
		/// <summary>
		/// Key of the Portal.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[FilterType(typeof(Link), false)]
		public string KeyLink { get; set; }

		/// <summary>
		/// Gets or sets the actual link
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LinkValue { get; set; }

		/// <summary>
		/// Gets or sets the key image (custom icon instead of css).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyImage { get; set; }

		/// <summary>
		/// Gets or sets the key parent.
		/// </summary>
		/// <value>
		/// The key parent.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyParent { get; set; }
		
		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the icon CLS.
		/// </summary>
		/// <value>
		/// The icon CLS.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string IconCls { get; set; }


		/// <summary>
		/// Gets or sets the name of the localized label of the Link.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MsgCode { get; set; }

		/// <summary>
		/// Gets the localized name of the link.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Label {
			get {
				return Helper.LocalizeText(MsgCode);
			}
			set { ;}
		}

		/// <summary>
		/// Gets or sets the name of the localized label of the Link group.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string GroupNameMsgCode { get; set; }

		/// <summary>
		/// Gets the localized name of the link group.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string GroupNameLabel {
			get {
				return Helper.LocalizeText(GroupNameMsgCode);
			}
			set { ;}
		}


		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>The SecurableEntity object.</returns>
		public SecurableEntity GetSecurableObject() {
				return new SecurableEntity {
				KeySecurable = KeyLink,
				SecurableIconCls = Helper.GetAttributeValue<IconClsAttribute>(GetType()),
				SecurableName = LocalId,
				SecurableTypeName = GetType().FullName
			};
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }
	}
}

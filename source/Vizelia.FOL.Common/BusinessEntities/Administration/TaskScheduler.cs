﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
		/// <summary>
		/// Business entity representing a Chart (or Portal Window) Scheduler.
		/// </summary>
		[Serializable]
		[Key("KeTaskScheduler")]
		[DataContract]
		[IconCls("viz-icon-small-scheduler")]
		public class TaskScheduler : CalendarEvent  {

			/// <summary>
			/// Contructor
			/// </summary>
			public TaskScheduler() {
				//Category = "ChartScheduler";
			}
			/// <summary>
			/// Constructor
			/// </summary>
			/// <param name="ev">a CalendarEvent.</param>
			public TaskScheduler(CalendarEvent ev) {
				
				if (ev != null) {
					Category = ev.Category;
					EndDate = ev.EndDate;
					InitialStartDate = ev.InitialStartDate;
					IsAllDay = ev.IsAllDay;
					Key = ev.Key;
					KeyLocation = ev.KeyLocation;
					Location = ev.Location;
					qtip = ev.qtip;
					RecurrencePattern = ev.RecurrencePattern;
					RelatedTo = ev.RelatedTo;
					StartDate = ev.StartDate;
					Title = ev.Title;
				}
			}
			/// <summary>
			/// Key of the Chart Scheduler.
			/// </summary>
			[DataMember]
			[AntiXssValidator]
			public string KeyTaskScheduler { get; set; }



			/// <summary>
			/// Gets or sets the type of the scheduled task.
			/// </summary>
			/// <value>
			/// The type of the scheduled task.
			/// </value>
			[DataMember]
			public IScheduledTaskType ScheduledTaskType { get; set; }

			/// <summary>
			/// The next occurence DateTime.
			/// </summary>
			[DataMember]
			public DateTime? NextOccurenceDate { get; set; }

		}
	}



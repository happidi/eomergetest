﻿using System;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// The occurrence type of a recurrence pattern.
	/// </summary>
	[Information("msg_calendar_frequencyoccurrence_", null)]
	public enum CalendarFrequencyOccurrence {
		/// <summary>
		/// The first occurrence.
		/// </summary>
		First = 1,

		/// <summary>
		/// The last occurrence.
		/// </summary>
		Last = -1,
		
		/// <summary>
		/// The second occurrence.
		/// </summary>
		Second = 2,

		/// <summary>
		/// The second to the last occurrence.
		/// </summary>
		SecondToLast = -2,

		/// <summary>
		/// The third occurrence.
		/// </summary>
		Third = 3,

		/// <summary>
		/// The third to the last occurrence.
		/// </summary>
		ThirdToLast = -3,

///////////// Add for DDay.iCal 1.0.0 /////

		/// <summary>
		/// The fifth to the last occurrence.
		/// </summary>
		FifthToLast = -5,

		/// <summary>
		/// The fourth to the last occurrence.
		/// </summary>
		FourthToLast = -4,

		/// <summary>
		/// The fourth occurrence.
		/// </summary>
		Fourth = 4,

		/// <summary>
		/// The fifth occurrence.
		/// </summary>
		Fifth = 5,
		/// <summary>
		/// Nothing selected
		/// </summary>
		None = Int32.MinValue
	}
}

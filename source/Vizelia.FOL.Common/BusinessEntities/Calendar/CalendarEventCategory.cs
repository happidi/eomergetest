﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {


	/// <summary>
	/// Business entity for CalendarEventCategory
	/// </summary>
	[DataContract]
	[Key("KeyCalendarEventCategory")]
	[Serializable]
	public class CalendarEventCategory : BaseBusinessEntity, IMappableEntity {

		/// <summary>
		/// Id of the CalendarEventCategory.
		/// </summary>
		[AntiXssValidator]
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyCalendarEventCategory { get; set; }

		/// <summary>
		/// The external identifier of the CalendarEventCategory.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the name of the localized label of the CalendarEventCategory.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MsgCode { get; set; }

		/// <summary>
		/// Gets the localized label of the CalendarEventCategory.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Label {
			get {
				if (string.IsNullOrEmpty(MsgCode)) return string.Empty;

				string localizeText = Helper.LocalizeText(MsgCode);
				return localizeText;
			}
			set { }
		}

		/// <summary>
		/// Red color composant.
		/// </summary>
		[DataMember]
		public int ColorR { get; set; }

		/// <summary>
		/// Green color composant.
		/// </summary>
		[DataMember]
		public int ColorG { get; set; }

		/// <summary>
		/// Blue color composant.
		/// </summary>
		[DataMember]
		public int ColorB { get; set; }

		/// <summary>
		/// The ColorId to use with the Ext.ensible.calendar
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ColorId { get; set; }

	}
}

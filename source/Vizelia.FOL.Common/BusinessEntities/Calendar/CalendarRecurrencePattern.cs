﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {


	/// <summary>
	/// Business entity for a calendar recurrence pattern.
	/// </summary>
	[DataContract]
	[Serializable]
	public class CalendarRecurrencePattern : BaseBusinessEntity {


		/// <summary>
		/// Gets or sets the by day.
		/// </summary>
		/// <value>The by day.</value>
		[Information("msg_calendarevent_weekday", null)]
		[AuditList]
		[DataMember(Order = 8)]
		public List<CalendarDaySpecifier> ByDay { get; set; }

		/// <summary>
		/// Gets or sets the by hour.
		/// </summary>
		/// <value>The by hour.</value>
		[Information("msg_calendarevent_hours", null)]
		[AuditList]
		[DataMember(Order = 7)]
		public List<int> ByHour { get; set; }

		/// <summary>
		/// Gets or sets the by minute.
		/// </summary>
		/// <value>The by minute.</value>
		[Information("msg_calendarevent_minutes", null)]
		[AuditList]
		[DataMember(Order = 6)]
		public List<int> ByMinute { get; set; }

		/// <summary>
		/// Gets or sets the by month.
		/// </summary>
		/// <value>The by month.</value>
		[Information("msg_calendarevent_months", null)]
		[AuditList]
		[DataMember(Order = 12)]
		public List<int> ByMonth { get; set; }

		/// <summary>
		/// Gets or sets the by month day.
		/// </summary>
		/// <value>The by month day.</value>
		[Information("msg_calendarevent_monthdays", null)]
		[AuditList]
		[DataMember(Order = 9)]
		public List<int> ByMonthDay { get; set; }

		/// <summary>
		/// Gets or sets the by second.
		/// </summary>
		/// <value>The by second.</value>
		[DataMember(Order = 5)]
		public List<int> BySecond { get; set; }

		/// <summary>
		/// Gets or sets the by set position.
		/// </summary>
		/// <value>The by set position.</value>
		[DataMember(Order = 13)]
		public List<int> BySetPosition { get; set; }

		/// <summary>
		/// Gets or sets the by week no.
		/// </summary>
		/// <value>The by week no.</value>
		[DataMember(Order = 11)]
		public List<int> ByWeekNo { get; set; }

		/// <summary>
		/// Gets or sets the by year day.
		/// </summary>
		/// <value>The by year day.</value>
		[DataMember(Order = 10)]
		public List<int> ByYearDay { get; set; }

		/// <summary>
		/// Gets or sets the count.
		/// </summary>
		/// <value>The count.</value>
		[Information("msg_calendarevent_occurrences", null)]
		[DataMember(Order = 3)]
		public int? Count { get; set; }

		/// <summary>
		/// Gets or sets the frequency.
		/// </summary>
		/// <value>The frequency.</value>
		[Information("msg_calendar_recurrence", null)]
		[DataMember(Order = 1)]
		public CalendarFrequencyType Frequency { get; set; }

		/// <summary>
		/// Gets or sets the interval.
		/// </summary>
		/// <value>The interval.</value>
		[Information("msg_calendarevent_interval", null)]
		[DataMember(Order = 4)]
		public int Interval { get; set; }

		/// <summary>
		/// Gets or sets the until.
		/// </summary>
		/// <value>The until.</value>
		[Information("msg_calendarevent_endby", null)]
		[DataMember(Order = 2)]
		public DateTime? Until { get; set; }

		/// <summary>
		/// Gets or sets the WKST.
		/// </summary>
		/// <value>The WKST.</value>
		[DataMember(Order = 14)]
		public DayOfWeek Wkst { get; set; }
	}
}

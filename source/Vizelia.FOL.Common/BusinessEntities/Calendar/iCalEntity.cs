﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Represents iCal operation
	/// </summary>
	[DataContract]
	public class iCalEntity : BaseBusinessEntity, IEnergyAggregatorNotifyableEntity {

		/// <summary>
		/// Gets or sets the key object.
		/// </summary>
		/// <value>
		/// The key object.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyObject { get; set; }

		/// <summary>
		/// Gets or sets the i cal data.
		/// </summary>
		/// <value>
		/// The i cal data.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string iCalData { get; set; }

		/// <summary>
		/// Gets or sets the time zone id in which we want to calculate MeterOperation (Degree day or TimeInterval grouping).
		/// </summary>
		/// <value>
		/// The time zone id.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string TimeZoneId { get; set; }

		/// <summary>
		/// Gets the time zone in which we want to calculate MeterOperation (Degree day or TimeInterval grouping).
		/// </summary>
		/// <returns></returns>
		public TimeZoneInfo GetTimeZone() {
			var retVal = CalendarHelper.GetTimeZoneFromId(TimeZoneId);
			return retVal;
		}
	}
}

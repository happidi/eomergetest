﻿using System;
using System.Runtime.Serialization;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for a calendar recurrence pattern.
	/// </summary>
	[DataContract]
	[Serializable]
	public class CalendarDaySpecifier : BaseBusinessEntity {


		/// <summary>
		/// Gets or sets the day of week.
		/// </summary>
		/// <value>The day of week.</value>
		[DataMember(Order = 2)]
		public DayOfWeek DayOfWeek { get; set; }

		/// <summary>
		/// Gets or sets the num.
		/// </summary>
		/// <value>The num.</value>
		[DataMember(Order = 1)]
		public CalendarFrequencyOccurrence Offset { get; set; }

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString() {
			return DayOfWeek.ToString();
		}
	}
}

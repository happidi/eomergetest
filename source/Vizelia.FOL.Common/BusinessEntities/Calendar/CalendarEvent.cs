﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	///  Business entity for a calendar event.
	/// </summary>
	[DataContract]
	[Key("Key")]
	[Serializable]
	public class CalendarEvent : BaseBusinessEntity, IEnergyAggregatorNotifyableEntity, IMappableEntity, ISupportLocation {

		/// <summary>
		/// The character used to create unique key when the event is an occurrence.
		/// </summary>
		public const string const_occurrence_separator = "--";

		private List<TimeSpan> m_TimeZoneOffset = new List<TimeSpan> { new TimeSpan(0, 0, -1), new TimeSpan(0, 0, -1), new TimeSpan(0, 0, -1) };
		/// <summary>
		/// Gets or sets the EventId.
		/// </summary>
		/// <value>The EventId.</value>
		[AntiXssValidator]
		[DataMember]
		public string Key { get; set; }

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		/// <value>The start date.</value>
		[Information("msg_calendarevent_startdate", null)]
		[ViewableByDataModel]
		[DataMember]
		public DateTime? StartDate { get; set; }

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		/// <value>The start date.</value>
		[ViewableByDataModel]
		[DataMember]
		public DateTime? InitialStartDate { get; set; }


		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		/// <value>The end date.</value>
		[Information("msg_calendarevent_enddate", null)]
		[ViewableByDataModel]
		[DataMember]
		public DateTime? EndDate { get; set; }

		/// <summary>
		/// Gets or sets the Timezone offset.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public List<TimeSpan> TimeZoneOffset {
			get {
				if ((m_TimeZoneOffset == null) || (m_TimeZoneOffset.Count <= 0))
					m_TimeZoneOffset = new List<TimeSpan> { new TimeSpan(0, 0, -1), new TimeSpan(0, 0, -1), new TimeSpan(0, 0, -1) };
				if ((StartDate != null) && (m_TimeZoneOffset[0] == new TimeSpan(0, 0, -1))) {
					m_TimeZoneOffset[0] = TimeZone.GetUtcOffset(StartDate.Value);
					m_TimeZoneOffset[1] = TimeZone.GetUtcOffset(StartDate.Value);
					return m_TimeZoneOffset;
				}
				else {
					return m_TimeZoneOffset;
				}
			}
			set { m_TimeZoneOffset = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is all day.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is all day; otherwise, <c>false</c>.
		/// </value>
		[ViewableByDataModel]
		[DataMember]
		public bool IsAllDay { get; set; }

		/// <summary>
		/// Gets or sets the location.
		/// </summary>
		/// <value>The location.</value>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Location { get; set; }

		/// <summary>
		/// Gets or sets the category. (is equal to the CalendarEventCategory LocalId).
		/// </summary>
		/// <value>The category.</value>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Category { get; set; }

		/// <summary>
		/// The CalendarEventCategory associated.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public CalendarEventCategory CategoryEntity { get; set; }

		/// <summary>
		/// Gets or sets the color Id of the event.
		/// </summary>
		/// <value>The ColorId.</value>
		[AntiXssValidator]
		[DataMember]
		public string ColorId { get; set; }

		/// <summary>
		/// Gets or sets the Title.
		/// </summary>
		/// <value>The Title.</value>
		[Information("msg_calendarevent_title", null)]
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Title { get; set; }


		/// <summary>
		/// Gets or sets the key location.
		/// </summary>
		/// <value>The key location.</value>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocation { get; set; }

		/// <summary>
		/// The location long path of the Meter.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		public string LocationLongPath { get; set; }

		/// <summary>
		/// The location short path of the Meter.
		/// Contains only the deepest site level and further levels.
		/// </summary>
		public string LocationShortPath { get; set; }

		/// <summary>
		/// The location level. Starts at level 0.
		/// </summary>
		public int? LocationLevel { get; set; }

		/// <summary>
		/// The location name of the Meter.
		/// </summary>
		public string LocationName { get; set; }

		/// <summary>
		/// Gets or sets the location type name.
		/// </summary>
		/// <value> The location type name. </value>
		[ViewableByDataModel]
		[DataMember]
		public HierarchySpatialTypeName LocationTypeName { get; set; }

		/// <summary>
		/// Gets or sets the recurrence pattern.
		/// </summary>
		/// <value>The recurrence pattern.</value>
		[AuditedClass]
		[ViewableByDataModel]
		[DataMember]
		[ExcludeMetaData]
		public CalendarRecurrencePattern RecurrencePattern { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance has recurrence pattern.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance has recurrence pattern; otherwise, <c>false</c>.
		/// </value>
		[Information("msg_calendarevent_hasrecurrencepattern", null)]
		public bool HasRecurrencePattern {
			get { return RecurrencePattern != null; }
			// we will only use the get functionallity for audit.
			private set { throw new NotImplementedException(); }
		}

		/// <summary>
		/// True when the event is an instance, false when the event is an occurrence.
		/// </summary>
		[ExcludeMetaData]
		public CalendarEventType EventType {
			get {
				if (String.IsNullOrEmpty(Key))
					return CalendarEventType.Instance;
				if (Key.Contains(const_occurrence_separator))
					return CalendarEventType.Occurrence;
				return CalendarEventType.Instance;
			}
		}

		/// <summary>
		/// Gets the instance key of the calendar event.
		/// </summary>
		/// <returns></returns>
		public string GetInstanceKey() {
			switch (EventType) {
				case CalendarEventType.Instance:
					return Key;

				case CalendarEventType.Occurrence:
					return Key.Substring(0, Key.LastIndexOf(const_occurrence_separator, StringComparison.Ordinal));

				default:
					return null;
			}
		}

		/// <summary>
		/// The qtip for the event.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string qtip {
			get {
				StringBuilder result = new StringBuilder();
				result.AppendFormat("{0}<br/>", Title);
				//result.AppendFormat("{0:HH:mm} - {1:HH:mm}", this.StartDate, this.EndDate);
				return result.ToString();
			}
			set { }
		}

		/// <summary>
		/// The key of the related event.
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string RelatedTo { get; set; }

		/// <summary>
		/// The multiplicative factor to use in energy split.
		/// </summary>
		[ViewableByDataModel]
		[DataMember]
		public double? Factor { get; set; }

		/// <summary>
		/// The final unit to use in energy split (after multiplication by the factor).
		/// </summary>
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string Unit { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

		/// <summary>
		/// Gets or sets the time zone id in which we want to calculate MeterOperation (Degree day or TimeInterval grouping).
		/// </summary>
		/// <value>
		/// The time zone id.
		/// </value>
		[Information("msg_chart_timezone", null)]
		[ViewableByDataModel]
		[AntiXssValidator]
		[DataMember]
		public string TimeZoneId { get; set; }

		private TimeZoneInfo m_TimeZoneInfo;
		/// <summary>
		/// Gets the time zone in which we want to calculate CalendarEvent.
		/// </summary>
		/// <returns></returns>
		public TimeZoneInfo TimeZone {
			get {
				if (!string.IsNullOrEmpty(TimeZoneId))
					return CalendarHelper.GetTimeZoneFromId(TimeZoneId);
				return m_TimeZoneInfo ?? (m_TimeZoneInfo = CalendarHelper.GetTimeZoneFromId(TimeZoneId));
			}
		}
	}
}

﻿
namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// The frequency type of a recurrence pattern.
	/// </summary>
	public enum CalendarFrequencyType {
		/// <summary>
		/// No frequency.
		/// </summary>
		None,
		/// <summary>
		/// Repeats every second.
		/// </summary>
		Secondly,
		/// <summary>
		/// Repeats every minute.
		/// </summary>
		Minutely,
		/// <summary>
		/// Repeats every hour.
		/// </summary>
		Hourly,
		/// <summary>
		/// Repeats every day.
		/// </summary>
		Daily,
		/// <summary>
		/// Repeats every week.
		/// </summary>
		Weekly,
		/// <summary>
		/// Repeats every month.
		/// </summary>
		Monthly,
		/// <summary>
		/// Repeats every year.
		/// </summary>
		Yearly
	}
}

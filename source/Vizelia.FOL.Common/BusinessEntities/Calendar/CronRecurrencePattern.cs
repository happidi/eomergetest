﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace Vizelia.FOL.BusinessEntities {


	/// <summary>
	/// Business entity for a cron recurrence pattern.
	/// </summary>
	[DataContract]
	public class CronRecurrencePattern : BaseBusinessEntity {
		private readonly string m_Seconds;
		private readonly string m_Minutes;
		private readonly string m_Hours;
		private readonly string m_DayOfTheMonth;
		private readonly string m_Month;
		private readonly string m_DayOfTheWeek;
		private readonly string m_MinuteRecurrency;
		private readonly string m_HourRecurrency;
		private readonly string m_YearlyRecurrency;
		private readonly string m_OccuranceInMonth;
		private readonly string m_Year;


		/// <summary>
		/// Initializes a new instance of the <see cref="CronRecurrencePattern"/> class.
		/// see http://en.wikipedia.org/wiki/Cron#CRON_expression
		/// </summary>
		/// <param name="seconds">The seconds - Allowed values:0-59.</param>
		/// <param name="minutes">The minutes - Allowed values:0-59.</param>
		/// <param name="hours">The hours - Allowed values:0-23.</param>
		/// <param name="dayOfTheMonth">The day of the month - Allowed values:0-31.</param>
		/// <param name="month">The month - Allowed values:1-12.</param>
		/// <param name="dayOfTheWeek">The day of the week  - Allowed values:0-6.</param>
		/// <param name="year">The year - Allowed values:1970-2099, Allowed special characters: * / , -</param>
		/// <param name="minuteRecurrency">The minute recurrency.</param>
		/// <param name="hourRecurrency">The hour recurrency.</param>
		/// <param name="yearlyRecurrency">The yearly recurrency.</param>
		/// <param name="occuranceInMonth">The occurance in month -  first/second/third..last/second to last/third to last... in the month should be a number between 5 and -5</param>
		public CronRecurrencePattern(string seconds, string minutes, string hours, string dayOfTheMonth, string month, string dayOfTheWeek, string year, string minuteRecurrency, string hourRecurrency,  string yearlyRecurrency, string occuranceInMonth)
		{
			m_Seconds = seconds;
			m_Minutes = minutes;
			m_Hours = hours;
			m_DayOfTheMonth = dayOfTheMonth;
			m_Month = month;
			m_DayOfTheWeek = dayOfTheWeek;
			m_Year = year;
			m_MinuteRecurrency = minuteRecurrency;
			m_HourRecurrency = hourRecurrency;
			m_YearlyRecurrency = yearlyRecurrency;
			m_OccuranceInMonth = occuranceInMonth;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CronRecurrencePattern"/> class.
		/// see http://en.wikipedia.org/wiki/Cron#CRON_expression
		/// </summary>
		/// <param name="minutes">The minutes - Allowed values:0-59.</param>
		/// <param name="hours">The hours - Allowed values:0-23.</param>
		/// <param name="dayOfTheMonth">The day of the month - Allowed values:0-31.</param>
		/// <param name="month">The month - Allowed values:1-12.</param>
		/// <param name="dayOfTheWeek">The day of the week  - Allowed values:0-6.</param>
		/// <param name="minuteRecurrency">The minute recurrency.</param>
		/// <param name="hourRecurrency">The hour recurrency.</param>
		/// <param name="yearlyRecurrency">The yearly recurrency.</param>
		/// <param name="occuranceInMonth">The occurance in month -  first/second/third..last/second to last/third to last... in the month should be a number between 5 and -5</param>
		public CronRecurrencePattern(string minutes, string hours, string dayOfTheMonth, string month, string dayOfTheWeek
			, string minuteRecurrency, string hourRecurrency,  string yearlyRecurrency, string occuranceInMonth) {

			m_Seconds = "0";
			m_Minutes = minutes;
			m_Hours = hours;
			m_DayOfTheMonth = dayOfTheMonth;
			m_Month = month;
			m_DayOfTheWeek = dayOfTheWeek;
			m_MinuteRecurrency = minuteRecurrency;
			m_HourRecurrency = hourRecurrency;
			m_YearlyRecurrency = yearlyRecurrency;
			m_OccuranceInMonth = occuranceInMonth;
			}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			string cronExpression;
			string minutes = m_Minutes;
			if (!string.IsNullOrWhiteSpace(m_MinuteRecurrency))
			{
				string minutePatternStart = GetPatternStart(minutes, m_MinuteRecurrency);
				minutes = minutePatternStart + "/" + m_MinuteRecurrency;
			}
			string hours = m_Hours;
			if (!string.IsNullOrWhiteSpace(m_HourRecurrency)) {
				string hourPatternStart = GetPatternStart(hours, m_HourRecurrency);
				hours = hourPatternStart + "/" + m_HourRecurrency;
			}

			string year = m_Year;
			if (!string.IsNullOrWhiteSpace(m_YearlyRecurrency)) {

				year = year + "/" + m_YearlyRecurrency;
			}

			string dayOfTheWeek = m_DayOfTheWeek;
			if (!string.IsNullOrWhiteSpace(m_OccuranceInMonth))
			{
				dayOfTheWeek = m_DayOfTheWeek + "#" + m_OccuranceInMonth;
			}

			string months = m_Month;
			
			if (!String.IsNullOrWhiteSpace(m_Year))
			{
				cronExpression = string.Format("{0} {1} {2} {3} {4} {5} {6}", m_Seconds,
																		      minutes,
																			  hours, 
																			  m_DayOfTheMonth,
																			  months,
																			  dayOfTheWeek,
																			  year);
			}
			else
			{
				cronExpression = string.Format("{0} {1} {2} {3} {4} {5}", m_Seconds,
																		  minutes,
																		  hours, 
																		  m_DayOfTheMonth,
																		  months,
																		  dayOfTheWeek);
			}

			return cronExpression;
		}

		/// <summary>
		/// Gets the first occurence of the pattern by the start date.
		/// </summary>
		/// <param name="currentMinute">The current minute.</param>
		/// <param name="intervalString">The interval string.</param>
		/// <returns></returns>
		private string GetPatternStart(string currentMinute, string intervalString)
		{
			var interval = Int32.Parse(intervalString);

			int initialMinute = Int32.Parse(currentMinute);
			while (initialMinute > interval) {
				initialMinute = initialMinute - interval;
			}
			return initialMinute.ToString(CultureInfo.InvariantCulture);
		}

	}
}

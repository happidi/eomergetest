﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business Entity to represent a  ReportingService Report Parameter Value Entity.
	/// </summary>
	[DataContract]
	[Serializable]
	public class ReportParameterValue : BaseBusinessEntity {
		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Value { get; set; }

		/// <summary>
		/// Gets or sets the text.
		/// </summary>
		/// <value>
		/// The text.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Label { get; set; }
	}
}
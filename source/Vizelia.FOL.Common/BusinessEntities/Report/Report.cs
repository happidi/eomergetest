﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business Entity to represent a  ReportingService Entity.
	/// </summary>
	[DataContract]
	[Serializable]
	[Key("Id")]
	[IconCls("viz-icon-small-report")]
	[LocalizedText("msg_report")]
	public class Report : BaseBusinessEntity, ITreeNode, ISecurableObject {

		/// <summary>
		/// The id.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Id { get; set; }

		/// <summary>
		/// Name of the ReportEntity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Path of the ReportEntity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Path { get; set; }

		/// <summary>
		/// Description of the ReportEntity
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// TypeName of the ReportEntity : Folder, Report...
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string TypeName { get; set; }

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Convert this ReportEntity into an Extjs TreeNode object.
		/// </summary>
		/// <returns>The TreeNode object.</returns>
		public TreeNode GetTree() {
			return new TreeNode {
				id = this.Id,
				Key = this.Path,
				text = this.Name,
				leaf = this.TypeName == "Report",
				iconCls = this.TypeName == "Report" ? "viz-icon-small-report" : "viz-icon-small-folder",
				qtip = this.Description,
				entity = this
			};
		}



		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>
		/// The SecurableEntity object.
		/// </returns>
		public SecurableEntity GetSecurableObject() {
			var retVal = new SecurableEntity() {
				KeySecurable = this.Path,
				SecurableIconCls = this.iconCls,
				SecurableName = this.Name,
				SecurableTypeName = this.GetType().FullName
			};
			return retVal;
		}


	
	}
}

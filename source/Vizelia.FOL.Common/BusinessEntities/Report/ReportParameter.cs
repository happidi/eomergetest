﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business Entity to represent a  ReportingService Report Parameter Entity.
	/// </summary>
	[DataContract]
	[Serializable]
	public class ReportParameter : BaseBusinessEntity {
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the name of the parameter type.
		/// </summary>
		/// <value>
		/// The name of the parameter type.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string ParameterTypeName { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="ReportParameter"/> is nullable.
		/// </summary>
		/// <value>
		///   <c>true</c> if nullable; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool Nullable { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [nullable specified].
		/// </summary>
		/// <value>
		///   <c>true</c> if [nullable specified]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool NullableSpecified { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [allow blank].
		/// </summary>
		/// <value>
		///   <c>true</c> if [allow blank]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool AllowBlank { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [allow blank specified].
		/// </summary>
		/// <value>
		///   <c>true</c> if [allow blank specified]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool AllowBlankSpecified { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [multi value].
		/// </summary>
		/// <value>
		///   <c>true</c> if [multi value]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool MultiValue { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [multi value specified].
		/// </summary>
		/// <value>
		///   <c>true</c> if [multi value specified]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool MultiValueSpecified { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [query parameter].
		/// </summary>
		/// <value>
		///   <c>true</c> if [query parameter]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool QueryParameter { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [query parameter specified].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [query parameter specified]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool QueryParameterSpecified { get; set; }

		/// <summary>
		/// Gets or sets the prompt.
		/// </summary>
		/// <value>
		/// The prompt.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Prompt { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [prompt user].
		/// </summary>
		/// <value>
		///   <c>true</c> if [prompt user]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool PromptUser { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [prompt user specified].
		/// </summary>
		/// <value>
		///   <c>true</c> if [prompt user specified]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool PromptUserSpecified { get; set; }

		/// <summary>
		/// Gets or sets the dependencies.
		/// </summary>
		/// <value>
		/// The dependencies.
		/// </value>
		[AntiXssListValidator]
		[DataMember]
		public string[] Dependencies { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [valid values query based].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [valid values query based]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool ValidValuesQueryBased { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [valid values query based specified].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [valid values query based specified]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool ValidValuesQueryBasedSpecified { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [default values query based].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [default values query based]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool DefaultValuesQueryBased { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [default values query based specified].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [default values query based specified]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool DefaultValuesQueryBasedSpecified { get; set; }

		/// <summary>
		/// Gets or sets the default values.
		/// </summary>
		/// <value>
		/// The default values.
		/// </value>
		[AntiXssListValidator]
		[DataMember]
		public string[] DefaultValues { get; set; }

		/// <summary>
		/// Gets or sets the name of the parameter state.
		/// </summary>
		/// <value>
		/// The name of the parameter state.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string ParameterStateName { get; set; }

		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		/// <value>
		/// The error message.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string ErrorMessage { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the value is from a selection.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if the value is from a selection; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsSelection { get; set; }
	}
}
﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business Entity to represent a Report Subscription Entity.
	/// </summary>
	[DataContract]
	[Serializable]
	[Key("KeyReportSubscription")]
	[IconCls("viz-icon-small-report")]
	public class ReportSubscription : BaseBusinessEntity, ISecurableObject {
		/// <summary>
		/// Gets or sets the key report.
		/// </summary>
		/// <value>
		/// The key report.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string KeyReportSubscription { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the report path.
		/// </summary>
		/// <value>
		/// The report path.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string ReportPath { get; set; }

		/// <summary>
		/// Gets or sets the recipients.
		/// </summary>
		/// <value>
		/// The recipients.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Recipients { get; set; }

		/// <summary>
		/// Gets or sets the parameter values.
		/// </summary>
		/// <value>
		/// The parameter values.
		/// </value>
		[DataMember]
		public Dictionary<string, string> ParameterValues { get; set; }

		/// <summary>
		/// Gets or sets the schedule.
		/// </summary>
		/// <value>
		/// The schedule.
		/// </value>
		[DataMember]
		public Schedule Schedule { get; set; }

		/// <summary>
		/// Gets or sets the file format.
		/// </summary>
		/// <value>
		/// The file format.
		/// </value>
		[DataMember]
		public ExportType FileFormat { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Returns the SecurableEntity object.
		/// </summary>
		/// <returns>The SecurableEntity object.</returns>
		public SecurableEntity GetSecurableObject() {
			throw new NotSupportedException();
		}

		/// <summary>
		/// The Creator of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Creator { get; set; }

		/// <summary>
		/// Gets or sets the status.
		/// </summary>
		/// <value>
		/// The status.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string Status { get; set; }

		/// <summary>
		/// Gets or sets the last run.
		/// </summary>
		/// <value>
		/// The last run.
		/// </value>
		[DataMember]
		public DateTime? LastRun { get; set; }
	}
}
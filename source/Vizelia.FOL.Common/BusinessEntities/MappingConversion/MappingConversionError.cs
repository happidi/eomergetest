﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// A mapping convertion error business entity.
	/// </summary>
	[DataContract]
	public class MappingConversionError : BaseBusinessEntity, IMappableEntity {
		/// <summary>
		/// Gets or sets the error message.
		/// </summary>
		/// <value>
		/// The error message.
		/// </value>
		[AntiXssValidator]
		[DataMember]
		public string ErrorMessage { get; set; }

		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId { get; set; }
	}
}

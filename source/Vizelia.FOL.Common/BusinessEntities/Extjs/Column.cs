﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for Extjs column config object.
	/// </summary>
	[DataContract]
	public class Column : BaseBusinessEntity {

		/// <summary>
		/// The header of the column.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string header { get; set; }

		/// <summary>
		/// The name of the field in the datastore the column is bound to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string dataIndex { get; set; }

		/// <summary>
		/// The type enum of the field in the datastore the colum is bound to.
		/// This property is not exposed on the client (so it does not have a DataMember attribute). 
		/// It is used internally in the report process to format the column when rendering the report.
		/// </summary>
		public TypeCode dataType { get; set; }

		/// <summary>
		/// The index of the column in the collection of columns.
		/// This property is not exposed on the client (so it does not have a DataMember attribute). 
		/// This property is used to insure that names of cells in the report are unique, even if the same column is repeated.
		/// </summary>
		public int positionIndex { get; set; }

		/// <summary>
		/// Returns an unique name for the column.
		/// This property is used when building a report.
		/// </summary>
		public string uniqueName {
			get {
				return dataIndex + positionIndex;
			}
		}

		/// <summary>
		/// The width of the column.
		/// </summary>
		[DataMember]
		public double width { get; set; }
	}
}

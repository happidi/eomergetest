﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for Extjs paging parameter config object that is used by the store.
	/// </summary>
	[DataContract]
	public class PagingParameter : BaseBusinessEntity {

		/// <summary>
		/// The limit of the paging.
		/// </summary>
		[DataMember]
		public int limit { get; set; }

		/// <summary>
		/// The start of the paging.
		/// </summary>
		[DataMember]
		public int start { get; set; }

		/// <summary>
		/// The direction of the paging.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string dir { get; set; }

		/// <summary>
		/// The sort field of the paging.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string sort { get; set; }

		/// <summary>
		/// The column name for filtering.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string filter { get; set; }

		/// <summary>
		/// The list of filters resulting from Ext.ux.grid.GridFilters plugin.
		/// </summary>
		[DataMember]
		public List<GridFilter> filters { get; set; }

		/// <summary>
		/// The list of inner join filters.
		/// </summary>
		[DataMember]
		public List<InnerJoinFilter> innerjoinfilters {
			get;
			set;
		}

		/// <summary>
		/// Extracts from the filters collection any filter of type innerjoin and places it in the special innerjoinfilters collection.
		/// </summary>
		public void ExtractInnerJoinFilters() {

			if (this.filters == null)
				return;
			if (this.filters.Count <= 0)
				return;

			List<GridFilter> filtersToDelete = new List<GridFilter>();

			foreach (GridFilter item in this.filters) {
				if (item.data.type == "innerjoin" || item.data.type == "innerpath") {
					if (!String.IsNullOrEmpty(item.data.value)) {
						List<string> values = item.data.value.Split(',').ToList();
						this.AddInnerJoinFilter(item.field, values, item.data.type);
						filtersToDelete.Add(item);
					}
				}
			}

			foreach (GridFilter item in filtersToDelete) {
				this.filters.Remove(item);
			}

		}

		/// <summary>
		/// The filter query.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string query { get; set; }

		/// <summary>
		/// Adds the inner join filter.
		/// </summary>
		/// <param name="field">The field.</param>
		/// <param name="values">The values.</param>
		/// <param name="joinType">The query join type (either innerjoin or innerpath).</param>
		public void AddInnerJoinFilter(string field, List<string> values, string joinType) {
			if (this.innerjoinfilters == null)
				this.innerjoinfilters = new List<InnerJoinFilter>();
			InnerJoinFilter joinFilter = new InnerJoinFilter {
				field = field,
				data = new JoinFilterData { values = values },
				joinType = joinType
			};
			this.innerjoinfilters.Add(joinFilter);
		}

		/// <summary>
		/// Gets the filter.
		/// </summary>
		/// <param name="field">The field name of the filter.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns></returns>
		public static GridFilter GetFilter(string field, PagingParameter paging) {
			GridFilter retVal = null;
			if (paging != null && paging.filters != null && paging.filters.Count > 0) {
				retVal = paging.filters.FirstOrDefault(p => p.field == field);
			}
			return retVal;
		}

		/// <summary>
		/// Joins the values.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public static string JoinValues(List<string> keys) {
			return String.Join(",", keys.ToArray());
		}
	}
}
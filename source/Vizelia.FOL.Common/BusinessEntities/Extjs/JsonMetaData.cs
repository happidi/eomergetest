﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Represents the metadata of the json store.
	/// Because we're using Generics, we use the Name attribute of the DataContract to avoid naming mangling.
	/// </summary>
	/// <typeparam name="T">The business entity the store is based on.</typeparam>
	[DataContract(Name = "JsonMetaDataOf{0}")]
	public class JsonMetaData<T> : BaseBusinessEntity where T : class {
		List<JsonField> _fields;
		string _totalProperty = "recordCount";
		string _idProperty;
		string _root = "records";
		string _successProperty = "success";
		string _messageProperty = "message";
		/// <summary>
		/// Public ctor.
		/// </summary>
		public JsonMetaData() {

			// We first retreive the Key of the business entity T, if defined.
			_idProperty = Helper.GetPropertyNameId(typeof(T));
			_fields = new List<JsonField>();

			// We then create the fields.
			CreateFields(typeof(T), null);
		}

		/// <summary>
		/// Recursive sub to create the metadata fields.
		/// </summary>
		/// <param name="t">The type of the property.</param>
		/// <param name="ancestorName">The name of preceding property. Should be null when first called.</param>
		public void CreateFields(Type t, string ancestorName) {
			List<MemberInfo> properties = new List<MemberInfo>();

			properties.AddRange(t.GetProperties(BindingFlags.Public | BindingFlags.Instance));
			properties.AddRange(t.GetFields(BindingFlags.Public | BindingFlags.Instance));
			ancestorName = String.IsNullOrEmpty(ancestorName) ? "" : ancestorName;


			foreach (MemberInfo p in properties) {
				// we retreive the datamember attribute
				Attribute datamember = Attribute.GetCustomAttribute(p, typeof(DataMemberAttribute));
				// we exclude property that are not marked as [DataMember]
				if (datamember != null && Attribute.GetCustomAttribute(p, typeof(ExcludeMetaDataAttribute)) == null) {
					var propertyType = p.GetType().BaseType == typeof(PropertyInfo) ? ((PropertyInfo)p).PropertyType : ((FieldInfo)p).FieldType;
					string type = Helper.ConvertTypeToExtjs(propertyType);
					if (type == "object") {
						// recursion
						if (p.Name != "PropertySet")
							CreateFields(propertyType, ancestorName + p.Name + ".");
					}
					else {
						_fields.Add(new JsonField {
							//name = (ancestorName + p.Name).Replace(".", "_"),
							name = ancestorName + p.Name,
							mapping = ancestorName + p.Name,
							type = type
						});
					}
				}
			}
		}

		/// <summary>
		/// Creates metadata fields.
		/// </summary>
		/// <param name="columnDefinitionList">The column definition list.</param>
		/// <param name="ancestorName">The name of preceding property. Should be null when first called.</param>
		public void CreateFields(List<KeyValuePair<string, string>> columnDefinitionList, string ancestorName) {
			ancestorName = String.IsNullOrEmpty(ancestorName) ? "" : ancestorName;

			foreach (KeyValuePair<string, string> p in columnDefinitionList) {
				_fields.Add(new JsonField {
					name = ancestorName + p.Value,
					mapping = ancestorName + p.Key,
					type = p.Key == "AcquisitionDateTime" ? "date" : "string"
				});
			}
		}

		/// <summary>
		/// The property which contains the total dataset size.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string totalProperty {
			get {
				return _totalProperty;
			}

			set {
				_totalProperty = value;
			}

		}

		/// <summary>
		/// The property which contains the id of the record.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string idProperty {
			get {
				return _idProperty;
			}

			set {
				_idProperty = value;
			}

		}
		/// <summary>
		/// The property which contains an Array of row objects.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string root {
			get {
				return _root;
			}
			set {
				_root = value;
			}
		}

		/// <summary>
		/// The list of fields.
		/// </summary>
		[DataMember]
		public List<JsonField> fields {
			get {
				return _fields;
			}
			set {
				_fields = value;
			}
		}

		/// <summary>
		/// A config object in the format: {field: "fieldName", direction: "ASC|DESC"} to specify the sort order in the request of a remote Store's load operation. 
		/// Note that for local sorting, the direction property is case-sensitive. 
		/// </summary>
		[DataMember]
		public JsonSortInfo sortInfo {
			get;
			set;
		}

		/// <summary>
		/// The name of the property indicating that the operation was successfull or not.
		/// This property is used in Ext.data.DataProxy when a writer has been defined, to analyze the response of the server after a CRUD operation.
		/// This property should be inline with OperationStatus.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string successProperty {
			get {
				return _successProperty;
			}
			set {
				_successProperty = value;
			}
		}

		/// <summary>
		/// Optional name of a property within a server-response that represents a user-feedback message.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string messageProperty {
			get {
				return _messageProperty;
			}
			set {
				_messageProperty = value;
			}
		}
	}

}

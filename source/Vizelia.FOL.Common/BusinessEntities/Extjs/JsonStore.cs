﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for an Extjs Json Store.
	/// </summary>
	/// <typeparam name="T">The business entity the store is based on.</typeparam>
	[DataContract(Name = "JsonStoreOf{0}")]
	public class JsonStore<T> : BaseBusinessEntity where T : class  {

		/// <summary>
		/// Public ctor.
		/// </summary>
		public JsonStore() {
			success = true;
			metaData = new JsonMetaData<T>();
			records = new List<T>();
		}

		/// <summary>
		/// The total number of records.
		/// </summary>
		[DataMember]
		public int recordCount {
			get;
			set;
		}

		/// <summary>
		/// The list of records.
		/// </summary>
		[DataMember]
		public List<T> records {
			get;
			set;
		}

		/// <summary>
		/// The metadata configuration object of the store.
		/// </summary>
		[DataMember]
		public JsonMetaData<T> metaData {
			get;
			set;
		}

		/// <summary>
		/// True if the store was successfull, false otherwise
		/// </summary>
		[DataMember]
		public bool success {
			get;
			set;
		}

		/// <summary>
		/// Represents a user-feedback message in case of an error.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string message {
			get;
			set;
		}
	}


}

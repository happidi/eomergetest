﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a crud store.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[DataContract]
	public class CrudStore<T> : BaseBusinessEntity where T : BaseBusinessEntity {

		/// <summary>
		/// The list of created records in the store.
		/// </summary>
		[DataMember]
		public List<T> create { get; set; }

		/// <summary>
		/// The list of updated records in the store.
		/// </summary>
		[DataMember]
		public List<T> update { get; set; }

		/// <summary>
		/// The list of deleted records in the store.
		/// </summary>
		[DataMember]
		public List<T> destroy { get; set; }

		
	}
}

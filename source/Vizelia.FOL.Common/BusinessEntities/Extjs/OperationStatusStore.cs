﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Vizelia.FOL.BusinessEntities {

    /// <summary>
    /// Business entity for returning the result of a CRUD operation.
    /// </summary>
    [DataContract(Name = "OperationStatusStoreOf{0}")]
	public class OperationStatusStore<T> : OperationStatus where T : BaseBusinessEntity {

        /// <summary>
        /// The list of records after the operation.
        /// </summary>
        [DataMember]
		public List<T> records { get; set; }

       

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.BusinessEntities {

    /// <summary>
    /// Interface for Treeview access
    /// </summary>
    public interface ITreeNode {
        /// <summary>
        /// Returns the Extjs TreeNode object.
        /// </summary>
        /// <returns>The TreeNode object.</returns>
        TreeNode GetTree();
    }
}

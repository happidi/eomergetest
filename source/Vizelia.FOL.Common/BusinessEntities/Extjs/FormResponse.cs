﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for Extjs Form response object.
	/// </summary>
	[DataContract]
	[Serializable]
	public class FormResponse : OperationStatus {

		/// <summary>
		/// Optional message returned to the user.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string msg { get; set; }

		/// <summary>
		/// The error object in case success is false.
		/// </summary>
		[DataMember]
		public List<FormError> errors { get; set; }

		/// <summary>
		/// The data object in case success is true.
		/// This is usually passed to the oForm.getForm().setValues() function.
		/// </summary>
		[DataMember]
		public BaseEntity data { get; set; }

		/// <summary>
		/// When an Exception is raised, it is attached here for consumption by the caller.
		/// </summary>
		/// <remarks>
		/// <para>This value is never exposed to the API directly, and thus is not a [DataMember].
		/// If a consumer wants to expose it, they must do so explicitly.</para>
		/// </remarks>
		public Exception exception { get; set; }

		/// <summary>
		/// Adds an error to the errors collection.
		/// </summary>
		/// <param name="id">The id of the field causing the error.</param>
		/// <param name="msg">The message of the error.</param>
		public void AddError(string id, string msg) {
			this.success = false;
			if (this.errors == null)
				this.errors = new List<FormError>();
			this.errors.Add(new FormError {
				id = id,
				msg = msg
			});
		}

		/// <summary>
		/// Formats the error using msg and errors properties. 
		/// </summary>
		/// <remarks>
		/// <para>It never includes the exception property
		/// as that may reveal internal information. If the caller elects to show this data, they can call
		/// FormatException().</para>
		/// </remarks>
		/// <returns></returns>
		public string FormatError() {
			string result = "";
			if (!this.success) {
				if (!String.IsNullOrEmpty(this.msg)) {
					result += "General error : " + this.msg + "\n";	//!!!This is not localized
				}
				if (this.errors != null) {
					foreach (var e in this.errors) {
						result += e.id + " : " + e.msg + "\n";
					}
				}
			}
			return result;
		}

		/// <summary>
		/// Creates a message based on the exception.
		/// It uses a template where tokens are replaced by ExceptionType, Message, and Stack Trace.
		/// </summary>
		/// <param name="template">These tokens are replaced: {0} = Exception type,
		/// {1} = Exception.Message, {2} = Stack Trace.
		/// When not assigned, it uses: "Exception: {0}\n{1}\nStack Trace:{2}"</param>
		/// <returns>The message or "" if no exception is setup.</returns>
		public string FormatException(string template = null) {
			if (exception == null)
				return "";
			if (template == null)
				template = Vizelia.FOL.Common.Helper.LocalizeText(m_FormatExceptionTemplateMsgCode, true) 
					?? m_DefaultFormatExceptionTemplate;

			return String.Format(template, exception.GetType().Name, exception.Message, exception.StackTrace);
		}

		/// <summary>
		/// Used by the FormatException method when you omit its parameter.
		/// This is also defined in the Language.resx using the message code shown below.
		/// It is fallback that is probably not needed, but there if LocalizeText returns null.
		/// </summary>
		public const string m_DefaultFormatExceptionTemplate = "Exception: {0}\n{1}\nStack Trace:{2}";

		/// <summary>
		/// MsgCode used to localize the default for the FormatException's template parameter.
		/// </summary>
		public const string m_FormatExceptionTemplateMsgCode = "msg_formresponse_exceptiontemplate";
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

    /// <summary>
    /// Business entity for json sortInfo config object.
    /// </summary>
	public class JsonSortInfo : BaseBusinessEntity {
        /// <summary>
        /// The name of the field.
        /// </summary>
		[AntiXssValidator]
		[DataMember]
        public string field { get; set; }

        /// <summary>
        /// The direction (ASC | DESC) of the order.
        /// </summary>
		[AntiXssValidator]
		[DataMember]
		public string direction { get; set; }
	}
}

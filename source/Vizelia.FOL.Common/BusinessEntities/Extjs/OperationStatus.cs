﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Vizelia.FOL.BusinessEntities {

    /// <summary>
    /// Business entity for returning the result of an operation.
    /// </summary>
    [DataContract]
	[Serializable]
	public class OperationStatus : BaseBusinessEntity {

        /// <summary>
        /// True if the action was successfull, false otherwise.
        /// </summary>
        [DataMember]
		public bool success { get; set; }

       

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for Extjs Form error object.
	/// </summary>
	[DataContract]
	[Serializable]
	public class FormError : BaseBusinessEntity {

		/// <summary>
		/// The id of the field in error.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string id { get; set; }

		/// <summary>
		/// The error message.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string msg { get; set; }

	
	}
}

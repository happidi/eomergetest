﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for Extjs TreeNode
	/// </summary>
	[Serializable]
	[DataContract]
	public class TreeNode : BaseBusinessEntity {

		/// <summary>
		/// Initializes a new instance of the <see cref="TreeNode"/> class.
		/// </summary>
		public TreeNode() {
			draggable = true;
		}


		private string _qtip;

		/// <summary>
		/// The id for this node. If one is not specified, one is generated. 
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string id { get; set; }

		/// <summary>
		/// The key of the entity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Key { get; set; }

		/// <summary>
		/// True if this node is a leaf and does not have children.
		/// </summary>
		[DataMember]
		public bool leaf { get; set; }

		/// <summary>
		/// The text for this node 
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string text { get; set; }

		/// <summary>
		/// A css class to be added to the nodes icon element for applying css background images.
		/// We hide the iconCls property of BaseBusinessEntity.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public new string iconCls { get; set; }

		/// <summary>
		/// The path to an icon for the node. The preferred way to do this is to use the cls or iconCls attributes and add the icon via a CSS background image. 
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string icon { get; set; }

		/// <summary>
		/// A css class to be added to the node
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string cls { get; set; }


		/// <summary>
		/// True if this node is a draggable, false otherwise. (true by default)
		/// </summary>
		[DataMember]
		public bool draggable { get; set; }

		/// <summary>
		/// An Ext QuickTip for the node.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string qtip {
			get {
				if (!String.IsNullOrEmpty(_qtip))
					return _qtip.Replace("\n", "<br>");
				return _qtip;
			}
			set {
				_qtip = value;
			}
		}


		/// <summary>
		/// All child nodes of this node.
		/// </summary>
		[DataMember]
		public List<TreeNode> children { get; set; }

		/// <summary>
		/// The business entity.
		/// </summary>
		[DataMember]
		public BaseBusinessEntity entity { get; set; }


		/// <summary>
		/// Adds the child.
		/// </summary>
		/// <param name="node">The node.</param>
		public void AddChild(TreeNode node) {
			if (children == null)
				children = new List<TreeNode>();
			children.Add(node);

		}

		/// <summary>
		/// Gets or sets the type of the filter.
		/// </summary>
		/// <value>
		/// The type of the filter.
		/// </value>
		[DataMember]
		public FilterType Filtertype { get; set; }

		/// <summary>
		/// Sets the type of the filter.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public TreeNode SetFilterType(FilterType type) {
			Filtertype = type;
			return this;
		}
	}
}

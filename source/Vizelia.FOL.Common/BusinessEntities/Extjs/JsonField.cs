﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Business entity for Extjs json field config object
	/// </summary>
	[DataContract]
	public class JsonField : BaseBusinessEntity {
		/// <summary>
		/// The name by which the field is referenced within the Record.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string name {
			get;
			set;
		}

		/// <summary>
		/// A path specification for use by the Ext.data.Reader implementation that is creating the Record to access the data value from the data object.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string mapping {
			get;
			set;
		}

		/// <summary>
		/// The data type for conversion to displayable value. Possible values are 
		/// auto (Default, implies no conversion)
		/// string
		/// int
		/// float
		/// boolean
		/// date
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string type {
			get;
			set;
		}

		/// <summary>
		/// The header in case the store is dynamic and we use the metadata to create the grid
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string header {
			get;
			set;
		}

	}
}

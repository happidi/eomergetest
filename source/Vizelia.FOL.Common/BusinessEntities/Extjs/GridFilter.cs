﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a grid filter.
	/// </summary>
	[DataContract]
	public class GridFilter : BaseBusinessEntity {

		/// <summary>
		/// The field that the filter applies to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string field { get; set; }

		/// <summary>
		/// The data of the filter.
		/// </summary>
		[DataMember]
		public GridData data { get; set; }

	}

	/// <summary>
	/// Business entity representing a grid data.
	/// </summary>
	[DataContract]
	public class GridData : BaseBusinessEntity {
		/// <summary>
		/// The comparison of the filter to its value (ne, eq, lt, gt).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string comparison { get; set; }

		/// <summary>
		/// The type of data (string, boolean, date, numeric, int,  list).
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string type { get; set; }

		/// <summary>
		/// The value of the filter.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string value { get; set; }

		/// <summary>
		/// Gets the array value.
		/// </summary>
		/// <returns></returns>
		public string[] GetArrayValue() {
			return value.Split(',');
		}

		/// <summary>
		/// Sets the list value.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public string SetListValue(List<string> keys) {
			return value = String.Join(",", keys);
		}

	}
}

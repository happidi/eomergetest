﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity representing a inner join filter.
	/// </summary>
	[DataContract]
	public class InnerJoinFilter : BaseBusinessEntity {

		/// <summary>
		/// The field that the filter applies to.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string field { get; set; }

		/// <summary>
		/// The data of the filter.
		/// </summary>
		[DataMember]
		public JoinFilterData data { get; set; }

		/// <summary>
		/// The query join type, either innerjoin or innerpath in order to control how join should be executed in the case of a hierarchy.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string joinType { get; set; }

	}

	/// <summary>
	/// Business entity representing a grid data.
	/// </summary>
	[DataContract]
	public class JoinFilterData : BaseBusinessEntity {

		/// <summary>
		/// The values of the filter.
		/// </summary>
		[AntiXssListValidator]
		[DataMember]
		public List<string> values { get; set; }

	}
}

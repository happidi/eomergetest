using System;
using System.Runtime.Serialization;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// An element that simply has a key(id) and a Value
	/// </summary>
	[Key("Id")]
	[DataContract]
	[Serializable]
	public class SimpleListElement : SimpleListElementGeneric<string> {
	}

	/// <summary>
	/// An element that simply has a key(id) and a Value
	/// </summary>
	[Key("Id")]
	[DataContract]
	[Serializable]
	public class SimpleListElementGeneric<T> : BaseBusinessEntity {
		/// <summary>
		/// The Id of the list element.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Id { get; set; }

		/// <summary>
		/// The value of the list element.
		/// </summary>
		[DataMember]
		public T Value { get; set; }

		/// <summary>
		/// Gets or sets the display order.
		/// </summary>
		/// <value>
		/// The order.
		/// </value>
		[DataMember]
		public int Order { get; set; }
	}
}
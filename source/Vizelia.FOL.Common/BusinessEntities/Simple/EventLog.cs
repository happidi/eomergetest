﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for an Event Log item
	/// </summary>
	[DataContract]
	[Serializable]
	[Key("KeyEventLog")]
	[LocalizedText("msg_eventlog")]
	[IconCls("viz-icon-small-eventlog")]
	public class EventLog : BaseBusinessEntity, IMappableEntity, ISupportLocation {
		/// <summary>
		/// Key of the Event Log item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		[System.ComponentModel.DataAnnotations.Key]
		public string KeyEventLog { get; set; }

		/// <summary>
		/// The name of the Event Log item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// The Description of the Event Log item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the start date.
		/// </summary>
		/// <value>
		/// The start date.
		/// </value>
		[DataMember]
		public DateTime StartDate { get; set; }

		/// <summary>
		/// Gets or sets the end date.
		/// </summary>
		/// <value>
		/// The end date.
		/// </value>
		[DataMember]
		public DateTime EndDate { get; set; }

		/// <summary>
		/// The KeyLocation of the Event Log item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyLocation {
			get;
			set;
		}

		/// <summary>
		/// The location long path of the Event Log item.
		/// Contains the entire hierarchy up to the root level.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationLongPath { get; set; }

		/// <summary>
		/// The location short path of the Event Log item.
		/// Contains only the deepest site level and further levels.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationShortPath { get; set; }

		/// <summary>
		/// The location level. Starts at level 0.
		/// </summary>
		[DataMember]
		public int? LocationLevel { get; set; }

		/// <summary>
		/// The location name of the Event Log item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationName { get; set; }

		/// <summary>
		/// The location description of the Event Log item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string LocationDescription { get; set; }

		/// <summary>
		/// The location typename of the Event Log item.
		/// </summary>
		[DataMember]
		public HierarchySpatialTypeName LocationTypeName { get; set; }


		/// <summary>
		/// The classification title of the Event Log item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemTitle { get; set; }

		/// <summary>
		/// The classification path of the Event Log item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string ClassificationItemLongPath { get; set; }

		/// <summary>
		/// The key classification item of the Event Log item.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string KeyClassificationItem { get; set; }

		/// <summary>
		/// The Red Color component of the classification of the Event Log item.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorR { get; set; }

		/// <summary>
		/// The Green Color component of the classification of the Event Log item.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorG { get; set; }

		/// <summary>
		/// The Blue Color component of the classification of the Event Log item.
		/// </summary>
		[DataMember]
		public int ClassificationItemColorB { get; set; }

		/// <summary>
		/// The local id of the Event Log item.
		/// </summary>
		[Information("msg_localid", null)]
		[AntiXssValidator]
		[DataMember]
		public string LocalId { get; set; }

	}
}
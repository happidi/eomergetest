﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for a Document Response
	/// </summary>
	[DataContract]
	[Serializable]
	public class DocumentResponse : BaseBusinessEntity {

		/// <summary>
		/// Gets or sets the dictionary.
		/// </summary>
		/// <value>
		/// The dictionary.
		/// </value>
		[DataMember]
		public List<SimpleListElement> FieldsAndGuids { get; set; }
	}
}

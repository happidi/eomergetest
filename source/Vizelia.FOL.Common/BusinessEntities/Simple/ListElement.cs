﻿using System;
using System.Runtime.Serialization;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Validators;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Business entity for a list element.
	/// </summary>
	[Key("Id")]
	[DataContract]
	[Serializable]
	public class ListElement : ListElementGeneric<string>, IMappableEntity {
		/// <summary>
		/// Gets or sets the local id.
		/// </summary>
		/// <value>
		/// The local id.
		/// </value>
		public string LocalId { get; set; }
	}

	/// <summary>
	/// Business entity for a list element.
	/// </summary>
	[Key("Id")]
	[DataContract]
	[Serializable]
	public class ListElementGeneric<T> : SimpleListElementGeneric<T> {
		/// <summary>
		/// The code in resource of the list element.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string MsgCode { get; set; }

		/// <summary>
		/// The translated value of the list element code.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string Label {
			get {
				return Helper.LocalizeText(this.MsgCode);
			}
			set {
				;
			}
		}

		/// <summary>
		/// The IconCls of the element.
		/// </summary>
		[AntiXssValidator]
		[DataMember]
		public string IconCls {
			get;
			set;
		}
	}
}

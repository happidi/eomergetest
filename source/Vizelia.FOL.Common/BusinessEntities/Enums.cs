﻿using Vizelia.FOL.Common;


// Exposes Enums 
// Each enums must be declared as a return type of CoreWCF in order to be availble in javascript
namespace Vizelia.FOL.BusinessEntities {


	/// <summary>
	/// Enum for assembly configuration, used for licensing.
	/// </summary>
	public enum LicenceConfiguration {

		/// <summary>
		/// Don't enforce licencing on developer machines
		/// </summary>
		Debug,
		/// <summary>
		/// enforce licencing
		/// </summary>
		Licenced
	}
	/// <summary>
	/// Enum for Extjs versions.
	/// </summary>
	public enum ExtjsVersion {
		/// <summary>
		/// None value
		/// </summary>
		none,

		/// <summary>
		/// Version 3.4.0
		/// </summary>
		v3_4_0,


		/// <summary>
		/// Version 3.4.1
		/// </summary>
		v3_4_1
	}


	/// <summary>
	/// Enumeration for mail behaviors.
	/// </summary>
	public enum MailBehavior {
		/// <summary>
		/// Inactive.
		/// </summary>
		Inactive,

		/// <summary>
		/// Debug.
		/// </summary>
		Debug,

		/// <summary>
		/// Release.
		/// </summary>
		Release
	}

	/// <summary>
	/// Enum reason for cache refresh
	/// </summary>
	public enum CacheRemoveReason {
		/// <summary>
		/// Expired
		/// </summary>
		Expired = 0,
		/// <summary>
		/// Removed
		/// </summary>
		Removed = 1,
		/// <summary>
		/// Scavenged
		/// </summary>
		Scavenged = 2,
		/// <summary>
		/// Underused
		/// </summary>
		Underused = 3,
		/// <summary>
		/// DependencyChanged
		/// </summary>
		DependencyChanged = 4,
		/// <summary>
		/// Unkown
		/// </summary>
		Unknown = 9999,
	}

	/// <summary>
	/// Enum form Vizjs versions.
	/// </summary>
	public enum VizjsVersion {
		/// <summary>
		/// None value
		/// </summary>
		none,

		/// <summary>
		/// Version 1.0.0
		/// </summary>
		v1_0_0
	}

	/// <summary>
	/// Enum for Extjs themes.
	/// </summary>
	public enum ExtjsTheme {

		/// <summary>
		/// Default theme.
		/// </summary>
		Default,

		/// <summary>
		/// Gray theme.
		/// </summary>
		Gray,

		/// <summary>
		/// Slate theme.
		/// </summary>
		Slate,

		/// <summary>
		/// Blue theme.
		/// </summary>
		Blue,

		/// <summary>
		/// Access theme.
		/// </summary>
		Access
	}

	/// <summary>
	/// Enum for paging location.
	/// </summary>
	public enum PagingLocation {
		/// <summary>
		/// Indicates that paging occurs in memory.
		/// </summary>
		Memory,

		/// <summary>
		/// Indicates that paging occurs in the database.
		/// </summary>
		Database
	}

	/// <summary>
	/// Enum for Cache location.
	/// </summary>
	public enum CacheLocation {
		/// <summary>
		/// Indicates that caching occurs in memory.
		/// </summary>
		Memory,

		/// <summary>
		/// Indicates that caching occurs in a distributed cache.
		/// </summary>
		Distributed
	}



	/// <summary>
	/// Enum for reporting service export types.
	/// </summary>
	public enum ExportType {
		/// <summary>
		/// Export to PDF.
		/// </summary>
		[LocalizedText("msg_enum_exportype_pdf")]
		[IconCls("viz-icon-small-pdf")]
		Pdf,

		/// <summary>
		/// Export to HTML.
		/// </summary>
		[LocalizedText("msg_enum_exportype_html")]
		[IconCls("viz-icon-small-html")]
		Html,

		/// <summary>
		/// Export to Excel with no formatting.
		/// </summary>
		[LocalizedText("msg_enum_exportype_excelbasic")]
		[IconCls("viz-icon-small-excel")]
		ExcelBasic,

		/// <summary>
		/// Export to Excel with formatting.
		/// </summary>
		[LocalizedText("msg_enum_exportype_excel2007")]
		[IconCls("viz-icon-small-excel")]
		Excel2007,

		/// <summary>
		/// Export to CSV.
		/// </summary>
		[LocalizedText("msg_enum_exportype_csv")]
		[IconCls("viz-icon-small-csv")]
		CSV,

		/// <summary>
		/// Export to PNG.
		/// </summary>
		[LocalizedText("msg_enum_exportype_imagepng")]
		[IconCls("viz-icon-small-image")]
		ImagePNG,

		/// <summary>
		/// Export to TIFF.
		/// </summary>
		[LocalizedText("msg_enum_exportype_imagetiff")]
		[IconCls("viz-icon-small-image")]
		ImageTIFF,

		/// <summary>
		/// Export to XML.
		/// </summary>
		[LocalizedText("msg_enum_exportype_xml")]
		[IconCls("viz-icon-small-xml")]
		XML,

		/// <summary>
		/// Export to Word.
		/// </summary>
		[LocalizedText("msg_enum_exportype_doc")]
		[IconCls("viz-icon-small-doc")]
		Word
	}

	/// <summary>
	/// Enum for reporting service format page.
	/// </summary>
	public enum FormatPageType {
		/// <summary>
		/// A4 Portrait.
		/// </summary>
		A4Portrait,

		/// <summary>
		/// A4 Landscape.
		/// </summary>
		A4Landscape,

		/// <summary>
		/// A3 Portrait.
		/// </summary>
		A3Portrait,

		/// <summary>
		/// A3 Landscape.
		/// </summary>
		A3Landscape
	}


	/// <summary>
	/// 
	/// </summary>
	public enum ScreenshotType {
		/// <summary>
		/// SVG
		/// </summary>
		SVG,
		/// <summary>
		/// PDF
		/// </summary>
		PDF,
		/// <summary>
		/// PS
		/// </summary>
		PS,
		/// <summary>
		/// PNG
		/// </summary>
		PNG,
		/// <summary>
		/// JPEG
		/// </summary>
		JPEG,
		/// <summary>
		/// TIFF
		/// </summary>
		TIFF,
		/// <summary>
		/// GIF
		/// </summary>
		GIF,
		/// <summary>
		/// BMP
		/// </summary>
		BMP

	}



	/// <summary>
	/// Enum for long running operation status.
	/// Each of the enum value should appear in Langue to provide for automatic translation of status.
	/// </summary>
	public enum LongRunningOperationStatus {
		/// <summary>
		/// Indicates that the operation has initialized.
		/// </summary>
		Initialized,

		/// <summary>
		/// Indicates that the operation is in progress.
		/// </summary>
		InProgress,

		/// <summary>
		/// Indicates that the operation has raised an error.
		/// </summary>
		Exception,

		/// <summary>
		/// Indicates that a result is available.
		/// </summary>
		ResultAvailable,

		/// <summary>
		/// Indicates that the operation is completed.
		/// </summary>
		Completed,

		/// <summary>
		/// Indicates that the operation has been canceled.
		/// </summary>
		Canceled
	}

	/// <summary>
	/// Enum for characteristing a request of the client-side from a long-running operation.
	/// </summary>
	public enum LongRunningOperationClientRequest {
		/// <summary>
		/// Client has not requested anything.
		/// </summary>
		None,

		/// <summary>
		/// Client has requested to cancel the long-running operation.
		/// </summary>
		Cancel,

		/// <summary>
		/// Client has requested to send the result of the long-running operation by email instead of
		/// waiting for it in a blocking manner at client-side.
		/// </summary>
		EmailResult
	}

	/// <summary>
	/// Enum for caracterising if the event is an occurrence or an instance.
	/// </summary>
	public enum CalendarEventType {
		/// <summary>
		/// Indicates that the event is an occurrence.
		/// </summary>
		Occurrence,

		/// <summary>
		/// Indicates that the event is an instance.
		/// </summary>
		Instance
	}

	/// <summary>
	/// Enum for defining the direction of a hierarchy stored procedure.
	/// </summary>
	public enum HierarchyDirection {

		/// <summary>
		/// Indicates that the hierarchy should be both descendant and ascendant.
		/// </summary>
		ALL,

		/// <summary>
		/// Indicates that the hierarchy should be descendant.
		/// </summary>
		DESC,

		/// <summary>
		/// Indicates that the hierarchy should be ascendant.
		/// </summary>
		ASC
	}

	/// <summary>
	/// Enum for defining a mathematical operation.
	/// Used in meters for defining the group operation over the time.
	/// </summary>
	public enum MathematicOperator {
		/// <summary>
		/// The sum operator.
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_sum")]
		SUM,

		/// <summary>
		/// The max operator.
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_max")]
		MAX,

		/// <summary>
		/// The min operator.
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_min")]
		MIN,

		/// <summary>
		/// The average operator.
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_avg")]
		AVG
	}

	/// <summary>
	/// Enum for defining a mathematical operation.
	/// Used in meters for defining the group operation over the time.
	/// </summary>
	public enum DataSerieLegendEntry {
		/// <summary>
		/// The sum operator.
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_sum")]
		SUM,

		/// <summary>
		/// The max operator.
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_max")]
		MAX,

		/// <summary>
		/// The min operator.
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_min")]
		MIN,

		/// <summary>
		/// The average operator.
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_avg")]
		AVG,


		/// <summary>
		/// Nothing.
		/// </summary>
		[LocalizedText("msg_none")]
		NONE
	}

	/// <summary>
	/// Enum for defining Chart Type
	/// </summary>
	public enum ChartType {
		/// <summary>
		/// Combo
		/// </summary>
		[LocalizedText("msg_enum_charttype_combo")]
		[IconClsAttribute("viz-icon-small-chart-bar")]
		Combo,

		/// <summary>
		/// ComboHorizontal
		/// </summary>
		[LocalizedText("msg_enum_charttype_combohorizontal")]
		[IconClsAttribute("viz-icon-small-chart-hbar")]
		ComboHorizontal,

		/// <summary>
		/// Pie
		/// </summary>
		[LocalizedText("msg_enum_charttype_pie")]
		[IconClsAttribute("viz-icon-small-chart-pie")]
		Pie,

		/// <summary>
		/// Donut
		/// </summary>
		[LocalizedText("msg_enum_charttype_donut")]
		[IconClsAttribute("viz-icon-small-chart-donut")]
		Donut,

		/// <summary>
		/// Radar
		/// </summary>
		[LocalizedText("msg_enum_charttype_radar")]
		[IconClsAttribute("viz-icon-small-chart-radar")]
		Radar,

		/// <summary>
		/// Gauges
		/// </summary>
		[LocalizedText("msg_enum_charttype_gauge")]
		[IconClsAttribute("viz-icon-small-chart-gauge")]
		Gauge,

		/// <summary>
		/// TreeMap
		/// </summary>
		[LocalizedText("msg_enum_charttype_treemap")]
		[IconClsAttribute("viz-icon-small-chart-treemap")]
		TreeMap
	}

	/// <summary>
	/// Chart Legend sort option
	/// </summary>
	public enum ChartLegendSort {
		/// <summary>
		/// By name asc
		/// </summary>
		[LocalizedText("msg_enum_chartlegendsort_bynameasc")]
		ByNameAsc,
		/// <summary>
		/// By name desc
		/// </summary>
		[LocalizedText("msg_enum_chartlegendsort_bynamedesc")]
		ByNameDesc,
		/// <summary>
		/// By legend entry value asc
		/// </summary>
		[LocalizedText("msg_enum_chartlegendsort_bylegendentryvalueasc")]
		ByLegendEntryValueAsc,
		/// <summary>
		/// By legend entry value desc
		/// </summary>
		[LocalizedText("msg_enum_chartlegendsort_bylegendentryvaluedesc")]
		ByLegendEntryValueDesc
	}

	/// <summary>
	/// Enum for defining Gauge Type.
	/// </summary>
	public enum GaugeType {

		/// <summary>
		/// Circular
		/// </summary>
		[LocalizedText("msg_enum_gaugetype_circular")]
		[IconCls("viz-icon-small-gauge-circular")]
		Circular,

		/// <summary>
		/// Horizontal
		/// </summary>
		[LocalizedText("msg_enum_gaugetype_horizontal")]
		[IconCls("viz-icon-small-gauge-horizontal")]
		Horizontal,

		/// <summary>
		/// Vertical
		/// </summary>
		[LocalizedText("msg_enum_gaugetype_vertical")]
		[IconCls("viz-icon-small-gauge-vertical")]
		Vertical,

		/// <summary>
		/// Thermometer
		/// </summary>
		[LocalizedText("msg_enum_gaugetype_thermometer")]
		[IconCls("viz-icon-small-gauge-thermometer")]
		Thermometer,

		/// <summary>
		/// IndicatorLight
		/// </summary>
		[LocalizedText("msg_enum_gaugetype_indicatorlight")]
		[IconCls("viz-icon-small-gauge-indicatorlight")]
		IndicatorLight,

		/// <summary>
		/// Digital
		/// </summary>
		[LocalizedText("msg_enum_gaugetype_digital")]
		[IconCls("viz-icon-small-gauge-digital")]
		Digital
	}

	/// <summary>
	/// Enum for defining DataSerie Type
	/// </summary>
	public enum DataSerieType {
		/// <summary>
		/// None
		/// </summary>
		[LocalizedText("msg_enum_dataserietype_none")]
		[IconCls("viz-icon-small-cancel")]
		None = 0,
		/// <summary>
		/// Marker
		/// </summary>
		[LocalizedText("msg_enum_dataserietype_marker")]
		[IconCls("viz-icon-small-dataserie-marker")]
		Marker = 1,
		/// <summary>
		/// Spline
		/// </summary>
		[LocalizedText("msg_enum_dataserietype_spline")]
		[IconCls("viz-icon-small-dataserie-spline")]
		Spline = 2,
		/// <summary>
		/// Line
		/// </summary>
		[LocalizedText("msg_enum_dataserietype_line")]
		[IconCls("viz-icon-small-dataserie-line")]
		Line = 3,
		/// <summary>
		/// Area Line
		/// </summary>
		[LocalizedText("msg_enum_dataserietype_arealine")]
		[IconCls("viz-icon-small-dataserie-arealine")]
		AreaLine = 4,
		/// <summary>
		/// Column
		/// </summary>
		[LocalizedText("msg_enum_dataserietype_column")]
		[IconCls("viz-icon-small-chart-bar")]
		Column = 5,
		/// <summary>
		/// Area spline
		/// </summary>
		[LocalizedText("msg_enum_dataserietype_areaspline")]
		[IconCls("viz-icon-small-dataserie-areaspline")]
		AreaSpline = 6
		/*/// <summary>
		/// Cylinder
		/// </summary>
		[EnumResourceName("msg_enum_dataserietype_cylinder")]
		Cylinder = 6,
		/// <summary>
		/// Bar
		/// </summary>
		[EnumResourceName("msg_enum_dataserietype_bar")]
		Bar = 7,
		
		/// <summary>
		/// Pyramid
		/// </summary>
		[EnumResourceName("msg_enum_dataserietype_pyramid")]
		Pyramid = 9,
		/// <summary>
		/// Cone
		/// </summary>
		[EnumResourceName("msg_enum_dataserietype_cone")]
		Cone = 10,
		/// <summary>
		/// Bar segmented
		/// </summary>
		[EnumResourceName("msg_enum_dataserietype_barsegmented")]
		BarSegmented = 11
		*/
	}


	/// <summary>
	/// Enum for Chart axis timeinterval
	/// </summary>
	public enum AxisTimeInterval {
		/// <summary>
		/// None
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_none")]
		[IconCls("viz-icon-small-cancel")]
		[Order(0)]
		None = 0,
		/// <summary>
		/// Global
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_global")]
		[IconCls("viz-icon-small-chart-timeinterval-global")]
		[Order(1)]
		Global = 1,
		/// <summary>
		/// Years
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_years")]
		[IconCls("viz-icon-small-chart-timeinterval-year")]
		[Order(2)]
		Years = 2,

		/// <summary>
		/// Semsters
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_semester")]
		[IconCls("viz-icon-small-chart-timeinterval-semester")]
		[Order(3)]
		Semesters = 10,
		
		/// <summary>
		/// Quarters
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_quarters")]
		[IconCls("viz-icon-small-chart-timeinterval-quarter")]
		[Order(4)]
		Quarters = 3,
		/// <summary>
		/// Months
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_months")]
		[IconCls("viz-icon-small-chart-timeinterval-month")]
		[Order(5)]
		Months = 4,
		/// <summary>
		/// Weeks
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_weeks")]
		[IconCls("viz-icon-small-chart-timeinterval-week")]
		[Order(6)]
		Weeks = 5,
		/// <summary>
		/// Days
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_days")]
		[IconCls("viz-icon-small-chart-timeinterval-day")]
		[Order(7)]
		Days = 6,
		/// <summary>
		/// Hours
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_hours")]
		[IconCls("viz-icon-small-chart-timeinterval-hour")]
		[Order(8)]
		Hours = 7,
		/// <summary>
		/// Minutes
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_minutes")]
		[IconCls("viz-icon-small-chart-timeinterval-minute")]
		[Order(9)]
		Minutes = 8,
		/// <summary>
		/// Seconds
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_seconds")]
		[IconCls("viz-icon-small-chart-timeinterval-second")]
		[Order(10)]
		Seconds = 9,

		
	}


	/// <summary>
	/// Enum for Manual input timeinterval
	/// </summary>
	public enum ManualInputTimeInterval {
		/// <summary>
		/// Seconds
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_seconds")]
		[IconCls("viz-icon-small-chart-timeinterval-second")]
		Seconds,
		/// <summary>
		/// Minutes
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_minutes")]
		[IconCls("viz-icon-small-chart-timeinterval-minute")]
		Minutes,
		/// <summary>
		/// Hours
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_hours")]
		[IconCls("viz-icon-small-chart-timeinterval-hour")]
		Hours,
		/// <summary>
		/// Days
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_days")]
		[IconCls("viz-icon-small-chart-timeinterval-day")]
		Days,
		/// <summary>
		/// Months
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_months")]
		[IconCls("viz-icon-small-chart-timeinterval-month")]
		Months,
		/// <summary>
		/// Years
		/// </summary>
		[LocalizedText("msg_enum_axistimeinterval_years")]
		[IconCls("viz-icon-small-chart-timeinterval-year")]
		Years,
	}

	/// <summary>
	/// Differents type of highlight for a DataPoint.
	/// </summary>
	public enum DataPointHighlight {
		/// <summary>
		/// Fill
		/// </summary>
		[LocalizedText("msg_enum_datapointhighlight_fill")]
		Fill,

		/// <summary>
		/// Border
		/// </summary>
		[LocalizedText("msg_enum_datapointhighlight_border")]
		Border,

		/// <summary>
		/// Hatch
		/// </summary>
		[LocalizedText("msg_enum_datapointhighlight_hatch")]
		Hatch

	}

	/// <summary>
	/// Differents type of degree day calculation
	/// </summary>
	public enum DegreeDayType {
		/// <summary>
		/// None
		/// </summary>
		[LocalizedText("msg_enum_degreedaytype_none")]
		None,

		/// <summary>
		/// Cooling DD
		/// </summary>
		[LocalizedText("msg_enum_degreedaytype_cooling")]
		Cooling,

		/// <summary>
		/// Heating DD
		/// </summary>
		[LocalizedText("msg_enum_degreedaytype_heating")]
		Heating,

		/// <summary>
		/// Cooling DD
		/// </summary>
		[LocalizedText("msg_enum_degreedaytype_cooling_hourbased")]
		CoolingHourBased,

		/// <summary>
		/// Heating DD
		/// </summary>
		[LocalizedText("msg_enum_degreedaytype_heating_hourbased")]
		HeatingHourBased,

		/// <summary>
		/// Cooling DD
		/// </summary>
		[LocalizedText("msg_enum_degreedaytype_cooling_enhanced")]
		CoolingEnhanced,

		/// <summary>
		/// Heating DD
		/// </summary>
		[LocalizedText("msg_enum_degreedaytype_heating_enhanced")]
		HeatingEnhanced,

		/// <summary>
		/// Costic DD
		/// </summary>
		[LocalizedText("msg_enum_degreedaytype_costic")]
		Costic
	}

	/// <summary>
	/// Enum for Chart axis localisation
	/// </summary>
	public enum ChartLocalisation {
		/// <summary>
		/// By GroupName
		/// </summary>
		[LocalizedText("msg_enum_axislocalisation_bygroupname")]
		[IconCls("viz-icon-small-classificationitem")]
		ByGroupName = 0,
		/// <summary>
		/// By Meter
		/// </summary>
		[LocalizedText("msg_enum_axislocalisation_bymeter")]
		[IconCls("viz-icon-small-meter")]
		ByMeter = 1,
		/// <summary>
		/// By Furniture
		/// </summary>
		[LocalizedText("msg_enum_axislocalisation_byfurniture")]
		[IconCls("viz-icon-small-furniture")]
		ByFurniture = 2,
		/// <summary>
		/// By Space
		/// </summary>
		[LocalizedText("msg_enum_axislocalisation_byspace")]
		[IconCls("viz-icon-small-space")]
		BySpace = 3,
		/// <summary>
		/// By Floor
		/// </summary>
		[LocalizedText("msg_enum_axislocalisation_byfloor")]
		[IconCls("viz-icon-small-buildingstorey")]
		ByFloor = 4,
		/// <summary>
		/// By Building
		/// </summary>
		[LocalizedText("msg_enum_axislocalisation_bybuilding")]
		[IconCls("viz-icon-small-building")]
		ByBuilding = 5,
		/// <summary>
		/// By Site
		/// </summary>
		[LocalizedText("msg_enum_axislocalisation_bysite")]
		[IconCls("viz-icon-small-site")]
		BySite = 6,

	}

	/// <summary>
	/// Enum for Chart Shadding Effect
	/// </summary>
	public enum ChartShaddingEffect {
		/// <summary>
		/// No Shadding
		/// </summary>
		[LocalizedText("msg_enum_chartshaddingeffect_none")]
		None = 0,
		/// <summary>
		/// Shadding type 1
		/// </summary>
		[LocalizedText("msg_enum_chartshaddingeffect_one")]
		One = 1,
		/// <summary>
		/// Shadding type 2
		/// </summary>
		[LocalizedText("msg_enum_chartshaddingeffect_two")]
		Two = 2,
		/// <summary>
		/// Shadding type 3
		/// </summary>
		[LocalizedText("msg_enum_chartshaddingeffect_three")]
		Three = 3,
		/// <summary>
		/// Shadding type 4
		/// </summary>
		[LocalizedText("msg_enum_chartshaddingeffect_four")]
		Four = 4,
		/// <summary>
		/// Shadding type 5
		/// </summary>
		///
		[LocalizedText("msg_enum_chartshaddingeffect_five")]
		Five = 5,
		/// <summary>
		/// Shadding type 6
		/// </summary>
		[LocalizedText("msg_enum_chartshaddingeffect_six")]
		Six = 6,
		/// <summary>
		/// Shadding type 7
		/// </summary>
		[LocalizedText("msg_enum_chartshaddingeffect_seven")]
		Seven = 7,
		/// <summary>
		/// Shadding type 8
		/// </summary>
		[LocalizedText("msg_enum_chartshaddingeffect_eight")]
		Eight = 8
	}

	/// <summary>
	/// Enum for the Chart Calendar Mode.
	/// </summary>
	public enum ChartCalendarMode {
		/// <summary>
		/// Keep the data that is included in the calendar.
		/// </summary>
		[LocalizedText("msg_enum_chartcalendarmode_include")]
		Include,

		/// <summary>
		/// Keep the data that is included in the calendar and split it by event summary.
		/// </summary>
		[LocalizedText("msg_enum_chartcalendarmode_splitbyevent")]
		SplitByEvent,

		/// <summary>
		/// Keep the data that is exluded from the calendar.
		/// </summary>
		[LocalizedText("msg_enum_chartcalendarmode_exclude")]
		Exclude

	}

	/// <summary>
	/// Enum for Chart Display Mode
	/// </summary>
	public enum ChartDisplayMode {
		/// <summary>
		/// Plain Image
		/// </summary>
		[LocalizedText("msg_enum_chartdisplaymode_image")]
		[IconCls("viz-icon-small-image")]
		Image,
		/// <summary>
		/// HighCharts html5
		/// </summary>
		[LocalizedText("msg_enum_chartdisplaymode_html5")]
		[IconCls("viz-icon-small-html5")]
		HighCharts,

		/// <summary>
		/// Grid
		/// </summary>
		[LocalizedText("msg_enum_chartdisplaymode_grid")]
		[IconCls("viz-icon-small-grid")]
		Grid,

		/// <summary>
		/// CustomGrid
		/// </summary>
		[LocalizedText("msg_enum_chartdisplaymode_customgrid")]
		[IconCls("viz-icon-small-customgrid")]
		CustomGrid,

		/// <summary>
		/// CustomGrid
		/// </summary>
		[LocalizedText("msg_enum_chartdisplaymode_html")]
		[IconCls("viz-icon-small-html")]
		Html

		///// <summary>
		///// Silverlight 5
		///// </summary>
		//[LocalizedText("msg_enum_chartdisplaymode_silverlight")]
		//[IconCls("viz-icon-small-silverlight")]
		//Silverlight,
		///// <summary>
		///// Sencha
		///// </summary>
		//[LocalizedText("msg_enum_chartdisplaymode_sencha")]
		//[IconCls("viz-icon-small-html5")]
		//Sencha ,
		///// <summary>
		///// Zing
		///// </summary>
		//[LocalizedText("msg_enum_chartdisplaymode_zing")]
		//[IconCls("viz-icon-small-html5")]
		//Zing 
		///// <summary>
		///// Zing
		///// </summary>
		//[LocalizedText("msg_enum_chartdisplaymode_zingflash")]
		//[IconCls("viz-icon-small-flash")]
		//ZingFlash ,
	}

	/// <summary>
	/// Enum for the HierarchySpatial TypeName
	/// </summary>
	public enum HierarchySpatialTypeName {
		/// <summary>
		/// None
		/// </summary>
		None = 0,

		/// <summary>
		/// IfcSite
		/// </summary>
		IfcSite = 5,

		/// <summary>
		/// IfcBuilding
		/// </summary>
		IfcBuilding = 4,

		/// <summary>
		/// IfcBuildingStorey
		/// </summary>
		IfcBuildingStorey = 3,

		/// <summary>
		/// IfcSpace
		/// </summary>
		IfcSpace = 2,

		/// <summary>
		/// IfcFurniture
		/// </summary>
		IfcFurniture = 1,

		/// <summary>
		/// IfcMeter
		/// </summary>
		IfcMeter = 6,

		/// <summary>
		/// AlarmDefinition
		/// </summary>
		AlarmDefinition = 7

	}

	/// <summary>
	/// Enum for the Location Type
	/// </summary>
	public enum LocationType {
		/// <summary>
		/// None
		/// </summary>
		None = 0,

		/// <summary>
		/// Site
		/// </summary>
		Site = 5,

		/// <summary>
		/// Building
		/// </summary>
		Building = 4,

		/// <summary>
		/// BuildingStorey
		/// </summary>
		BuildingStorey = 3,

		/// <summary>
		/// Space
		/// </summary>
		Space = 2,

		/// <summary>
		/// Furniture
		/// </summary>
		Furniture = 1,

		/// <summary>
		/// Meter
		/// </summary>
		Meter = 6,

		/// <summary>
		/// AlarmDefinition
		/// </summary>
		AlarmDefinition = 7
	}

	/// <summary>
	/// Enum for Arithmetic operations.
	/// </summary>
	public enum ArithmeticOperator {
		/// <summary>
		/// None
		/// </summary>
		[LocalizedText("msg_enum_arithmeticoperator_none")]
		[IconCls("viz-icon-small-cancel")]
		None,
		/// <summary>
		/// +
		/// </summary>
		[LocalizedText("msg_enum_arithmeticoperator_add")]
		[IconCls("viz-icon-small-arithmeticoperator-add")]
		Add,
		/// <summary>
		/// -
		/// </summary>
		[LocalizedText("msg_enum_arithmeticoperator_substract")]
		[IconCls("viz-icon-small-arithmeticoperator-substract")]
		Subtract,
		/// <summary>
		/// *
		/// </summary>
		[LocalizedText("msg_enum_arithmeticoperator_multiply")]
		[IconCls("viz-icon-small-arithmeticoperator-multiply")]
		Multiply,
		/// <summary>
		/// /
		/// </summary>
		[LocalizedText("msg_enum_arithmeticoperator_divide")]
		[IconCls("viz-icon-small-arithmeticoperator-divide")]
		Divide,
		/// <summary>
		/// ^
		/// </summary>
		[LocalizedText("msg_enum_arithmeticoperator_power")]
		[IconCls("viz-icon-small-arithmeticoperator-power")]
		Power

	}

	/// <summary>
	/// Enum for Algebric functions.
	/// </summary>
	public enum AlgebricFunction {
		/// <summary>
		/// None
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_none")]
		None,
		/// <summary>
		/// cosinus
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_cos")]
		Cos,
		/// <summary>
		/// sinus
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_sin")]
		Sin,
		/// <summary>
		/// tangente
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_tan")]
		Tan,
		/// <summary>
		/// square root
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_sqrt")]
		Sqrt,

		/// <summary>
		/// 1/x
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_abs")]
		Abs,

		/// <summary>
		/// 1/x
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_inverse")]
		Inverse,

		/// <summary>
		/// 1/x
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_log")]
		Log,

		/// <summary>
		/// percentage ((n-(n-1))/n)
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_percentage")]
		Percentage,
		/// <summary>
		/// Replace value by 1.
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_count")]
		Count,

		/// <summary>
		///Year to date
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_delta")]
		Delta,

		/// <summary>
		/// Delta ((n-(n-1))
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_ytd")]
		YTD,

		/// <summary>
		/// When a value that is supposed to be a number is not a number,
		/// return 0. Typically an invalid ID to a DataSeries.
		/// errorcorrecttozero(UNKNOWN_DATASERIES_ID)
		/// When a value that is a DataSerie has no Points with value,
		/// return 0. Typically used with Project Functions Minimum and Maximum 
		/// errorcorrecttozero(minimum(KNOWN_DATASERIES_ID))
		/// When a value inside of a DataSerie is YValue = NaN,
		/// set it to 0. Typically used to clean up a DataSeries:
		/// errorcorrecttozero(KNOWN_DATASERIES_ID)
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_errorcorrecttozero")]
		ErrorCorrectToZero,

		/// <summary>
		/// When a value that is supposed to be a number is not a number,
		/// return 1. Typically an invalid ID to a DataSeries.
		/// errorcorrecttoone(UNKNOWN_DATASERIES_ID)
		/// When a value that is a DataSerie has no Points with value,
		/// return 1. Typically used with Project Functions Minimum and Maximum 
		/// errorcorrecttoone(minimum(KNOWN_DATASERIES_ID))
		/// When a value inside of a DataSerie is YValue = NaN,
		/// set it to 1. Typically used to clean up a DataSeries,
		/// such as when sqrt evaluates a negative YValue, which
		/// it replaces with NaN.
		/// errorcorrecttoone(sqrt(KNOWN_DATASERIES_ID))
		/// </summary>
		[LocalizedText("msg_enum_algebricfunction_errorcorrecttoone")]
		ErrorCorrectToOne
	}


	/// <summary>
	/// Enum for One extra parameter functions.
	/// </summary>
	public enum OneParameterFunction {
		/// <summary>
		///Only keeps elements Higher than
		/// </summary>
		[LocalizedText("msg_enum_oneparameterfunction_higher")]
		Higher,

		/// <summary>
		///Only keeps elements Lower than
		/// </summary>
		[LocalizedText("msg_enum_oneparameterfunction_lower")]
		Lower
	}

	/// <summary>
	/// Enum for the differents types of DynamicDisplay
	/// </summary>
	public enum DynamicDisplayType {

		/// <summary>
		/// Bottom Comment Dynamic Display
		/// </summary>
		[LocalizedText("msg_enum_dynamicdisplaytype_bottomcomment")]
		BottomComment,
		/// <summary>
		/// Right Comment Dynamic Display
		/// </summary>
		[LocalizedText("msg_enum_dynamicdisplaytype_rightcomment")]
		RightComment,
		/// <summary>
		/// Advice Dynamic Display
		/// </summary>
		[LocalizedText("msg_enum_dynamicdisplaytype_advice")]
		Advice,
		/// <summary>
		/// Simple Comment Dynamic Display
		/// </summary>
		[LocalizedText("msg_enum_dynamicdisplaytype_simple")]
		Simple,

		/// <summary>
		/// Tile Dynamic Display
		/// </summary>
		[LocalizedText("msg_enum_dynamicdisplaytype_tile")]
		Tile,

		/// <summary>
		/// DenseText Dynamic Display
		/// </summary>
		[LocalizedText("msg_enum_dynamicdisplaytype_densetext")]
		DenseText
		

	}

	/// <summary>
	/// Enum for the predefined DynamicDisplayColorTemplate
	/// </summary>
	public enum PredefinedDynamicDisplayColorTemplate {

		/// <summary>
		/// Green template for the Dynamic Display
		/// </summary>
		Green,
		/// <summary>
		/// Red template for the Dynamic Display
		/// </summary>
		Red,
		/// <summary>
		/// Orange template for the Dynamic Display
		/// </summary>
		Orange,
		/// <summary>
		/// Orange template for the Dynamic Display
		/// </summary>
		Yellow,
		/// <summary>
		/// Yellow template for the Dynamic Display
		/// </summary>
		Blue,
		/// <summary>
		/// Blue template for the Dynamic Display
		/// </summary>
		Grey,
		/// <summary>
		/// Grey template for the Dynamic Display
		/// </summary>
		Black,
		/// <summary>
		/// Black template for the Dynamic Display
		/// </summary>
		Custom
	}





	/// <summary>
	/// Operation Enum from the CDC mechanizm
	/// </summary>
	public enum AuditOperation {
		/// <summary>
		/// The operation is an insert.
		/// </summary>
		[LocalizedText("msg_add")]
		[IconCls("viz-icon-small-add")]
		Insert,
		/// <summary>
		/// The operation is an update.
		/// </summary>
		[LocalizedText("msg_update")]
		[IconCls("viz-icon-small-update")]
		Update,
		/// <summary>
		/// The operation is a delete.
		/// </summary>
		[LocalizedText("msg_delete")]
		[IconCls("viz-icon-small-delete")]
		Delete,
	}

	/// <summary>
	/// Weather Units
	/// </summary>
	public enum WeatherUnit {
		/// <summary>
		/// Celsius
		/// </summary>
		[LocalizedText("msg_enum_weatherunit_celsius")]
		Celsius,
		/// <summary>
		/// Fahrenheit
		/// </summary>
		[LocalizedText("msg_enum_weatherunit_fahrenheit")]
		Fahrenheit
	}

	/// <summary>
	/// Service Invocation Protocol Enum.
	/// </summary>
	public enum ServiceInvocationProtocol {
		/// <summary>
		/// In Memory Invocation
		/// </summary>
		InMemory,
		/// <summary>
		/// WCF HTTP invocation
		/// </summary>
		WCFHTTP,
		/// <summary>
		/// WCF TCP Invocation
		/// </summary>
		WCFTCP
	}

	/// <summary>
	/// The different Map types.
	/// </summary>
	public enum MapType {
		/// <summary>
		/// Auto
		/// </summary>
		[LocalizedText("msg_enum_maptype_auto")]
		Auto,
		/// <summary>
		/// Aerial
		/// </summary>
		[LocalizedText("msg_enum_maptype_aerial")]
		Aerial,
		/// <summary>
		/// Road
		/// </summary>
		[LocalizedText("msg_enum_maptype_road")]
		Road
	}
	/// <summary>
	/// MachineInstance size
	/// </summary>
	public enum MachineInstanceSize {
		/// <summary>
		/// Small
		/// </summary>
		[LocalizedText("msg_enum_machineinstance_small")]
		Small,

		/// <summary>
		/// Medium
		/// </summary>
		[LocalizedText("msg_enum_machineinstance_medium")]
		Medium,

		/// <summary>
		/// Large
		/// </summary>
		[LocalizedText("msg_enum_machineinstance_large")]
		Large

	}

	/// <summary>
	/// A list of available condition to use with a filter : i.e. ChartFilterSpatialPset
	/// </summary>
	public enum FilterCondition {
		/// <summary>
		/// =
		/// </summary>
		[LocalizedText("msg_enum_filtercondition_equals")]
		Equals,
		/// <summary>
		/// !=
		/// </summary>
		[LocalizedText("msg_enum_filtercondition_notequals")]
		NotEquals,
		/// <summary>
		/// like '% %'
		/// </summary>
		[LocalizedText("msg_enum_filtercondition_like")]
		Like,
		/// <summary>
		/// Higher
		/// </summary>
		[LocalizedText("msg_enum_filtercondition_higher")]
		Higher,
		/// <summary>
		/// Lower
		/// </summary>
		[LocalizedText("msg_enum_filtercondition_lower")]
		Lower,
		/// <summary>
		/// Between
		/// </summary>
		[LocalizedText("msg_enum_filtercondition_between")]
		Between,
		/// <summary>
		/// Between
		/// </summary>
		[LocalizedText("msg_enum_filtercondition_outside")]
		Outside,

		/// <summary>
		/// =
		/// </summary>
		[LocalizedText("msg_scriptpython")]
		Script

	}

	/// <summary>
	/// A list of logical operator
	/// </summary>
	public enum LogicalOperator {
		/// <summary>
		/// AND
		/// </summary>
		[LocalizedText("msg_enum_logicaloperator_and")]
		And,
		/// <summary>
		/// OR
		/// </summary>
		[LocalizedText("msg_enum_logicaloperator_or")]
		Or
	}

	/// <summary>
	/// A list of file source types.
	/// </summary>
	public enum FileSourceType {
		/// <summary>
		/// UNC file source (a network share, ie \\server\folder\)
		/// </summary>
		[LocalizedText("msg_enum_filesourcetype_unc")]
		UNC,

		/// <summary>
		/// FTP file source (an FTP server)
		/// </summary>
		[LocalizedText("msg_enum_filesourcetype_ftp")]
		FTP,

		/// <summary>
		/// SFTP file source (an SSH server)
		/// </summary>
		[LocalizedText("msg_enum_filesourcetype_sftp")]
		SFTP
	}

	/// <summary>
	/// A list of security types for FTP connections.
	/// </summary>
	public enum FtpSecurityType {
		/// <summary>
		/// No security.
		/// </summary>
		[LocalizedText("msg_enum_ftpsecuritytype_none")]
		None = 0,

		/// <summary>
		/// Implicit security.
		/// FTPS protocol was originally assigned a separate port by the IANA. 
		/// Upon connection to this port, an SSL negotiation starts immediately and the control connection is secured. 
		/// All data connections are also secured implicitly in the same way. This is similar to the approach used by HTTPS.
		/// This approach is not favored by the IETF and is deprecated. 
		/// It is supported for interoperability with older servers, but it is strongly recommended 
		/// to use the explicit protection instead whenever possible.
		/// </summary>
		[LocalizedText("msg_enum_ftpsecuritytype_implicit")]
		Implicit = 1,

		/// <summary>
		/// Explicit security.
		/// Client connects to FTP server in a usual non-protected way, usually to port 21 was assigned to FTP protocol. 
		/// When it is desired to protect the connection using SSL, an SSL negotiation is initialized, control connection 
		/// is secured and all following communication is being protected. Protection of data connections is optional.
		/// </summary>
		[LocalizedText("msg_enum_ftpsecuritytype_explicit")]
		Explicit = 2
	}

	/// <summary>
	/// A list of authentication modes for SFTP connections.
	/// </summary>
	public enum 
		SftpAuthenticationMode {
		/// <summary>
		/// Password only.
		/// </summary>
		[LocalizedText("msg_enum_sftpauthenticationmode_password")]
		Password,

		/// <summary>
		/// Public key authentication only.
		/// </summary>
		[LocalizedText("msg_enum_sftpauthenticationmode_privatekey")]
		PrivateKey,

		/// <summary>
		/// Both password and public key.
		/// </summary>
		[LocalizedText("msg_enum_sftpauthenticationmode_both")]
		Both
	}

	/// <summary>
	/// The fetching order of files in the mapping process.
	/// </summary>
	public enum MappingFetchingOrder {
		/// <summary>
		/// Fetches by date and time, oldest first.
		/// </summary>
		ByTime,
		/// <summary>
		/// Fetches by filename lexicographical order. File names with numbers must have leading zeros.
		/// </summary>
		ByFilename
	}

	/// <summary>
	/// A list of AlarmInstance status
	/// </summary>
	public enum AlarmInstanceStatus {
		/// <summary>
		/// Created
		/// </summary>
		[LocalizedText("msg_enum_alarminstancestatus_open")]
		Open,
		/// <summary>
		/// Acquitted
		/// </summary>
		[LocalizedText("msg_enum_alarminstancestatus_acknowledged")]
		Acknowledged,
		/// <summary>
		/// Created
		/// </summary>
		[LocalizedText("msg_enum_alarminstancestatus_fixed")]
		Fixed,
		/// <summary>
		/// Acquitted
		/// </summary>
		[LocalizedText("msg_enum_alarminstancestatus_closed")]
		Closed
	}

	/// <summary>
	/// Chart Specific Analysis enum.
	/// </summary>
	public enum ChartSpecificAnalysis {
		/// <summary>
		/// None
		/// </summary>
		[LocalizedText("msg_enum_chartspecificanalysis_none")]
		[IconCls("viz-icon-small-cancel")]
		None,

		/// <summary>
		/// Correlation
		/// </summary>
		[LocalizedText("msg_enum_chartspecificanalysis_correlation")]
		[IconCls("viz-icon-small-chart-correlation")]
		Correlation,

		/// <summary>
		/// Heat map
		/// </summary>
		[LocalizedText("msg_enum_chartspecificanalysis_heatmap")]
		[IconCls("viz-icon-small-chart-heatmap")]
		HeatMap,

		/// <summary>
		/// Difference highlight
		/// </summary>
		[LocalizedText("msg_enum_chartspecificanalysis_differencehighlight")]
		[IconCls("viz-icon-small-chart-differencehighlight")]
		DifferenceHighlight,

		/// <summary>
		/// Energy certificate
		/// </summary>
		[LocalizedText("msg_enum_chartspecificanalysis_energycertificate")]
		[IconCls("viz-icon-small-chart-energycertificate")]
		EnergyCertificate,

		/// <summary>
		/// MicroChart
		/// </summary>
		[LocalizedText("msg_enum_chartspecificanalysis_microchart")]
		[IconCls("viz-icon-small-chart-microchart")]
		MicroChart,

		/// <summary>
		/// Calendar view
		/// </summary>
		[LocalizedText("msg_enum_chartspecificanalysis_calendarview")]
		[IconCls("viz-icon-small-calendar")]
		CalendarView,

		/// <summary>
		/// LoadDurationCurve
		/// </summary>
		[LocalizedText("msg_enum_chartspecificanalysis_loaddurationcurve")]
		[IconCls("viz-icon-small-chart-loaddurationcurve")]
		LoadDurationCurve
	}

	/// <summary>
	/// Chart Datagrid Percentage options
	/// </summary>
	public enum ChartDataGridPercentage {
		/// <summary>
		/// None
		/// </summary>
		[LocalizedText("msg_enum_chartdatagridpercentage_none")]
		[IconCls("viz-icon-small-cancel")]
		None,

		/// <summary>
		/// Increase in green
		/// </summary>
		[LocalizedText("msg_enum_chartdatagridpercentage_increaseingreen")]
		[IconCls("viz-icon-small-arrowgreen-up")]
		IncreaseInGreen,

		/// <summary>
		/// Decrease in green
		/// </summary>
		[LocalizedText("msg_enum_chartdatagridpercentage_decreaseingreen")]
		[IconCls("viz-icon-small-arrowgreen-down")]
		DecreaseInGreen
	}


	/// <summary>
	/// The severity of a trace entry.
	/// </summary>
	public enum TraceEntrySeverity {
		/// <summary>
		/// Information.
		/// </summary>
		[LocalizedText("msg_enum_traceentryseverity_information")]
		[IconCls("viz-icon-small-information")]
		Information = 0,

		/// <summary>
		/// Warning.
		/// </summary>
		[LocalizedText("msg_enum_traceentryseverity_warning")]
		[IconCls("viz-icon-small-warning")]
		Warning = 1,

		/// <summary>
		/// Error.
		/// </summary>
		[LocalizedText("msg_enum_traceentryseverity_error")]
		[IconCls("viz-icon-small-exception")]
		Error = 2,

		/// <summary>
		/// Error.
		/// </summary>
		[LocalizedText("msg_enum_traceentryseverity_summary")]
		[IconCls("viz-icon-small-summary")]
		Verbose = 3

	}

	/// <summary>
	/// Telvent Daily data Attributes.
	/// </summary>
	public enum TelventDailyAttributes {
		/// <summary>
		/// avgBarometricPressure
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_avgbarometricpressure")]
		avgBarometricPressure,
		/// <summary>
		/// avgDewPoint
		///  </summary>
		[LocalizedText("msg_enum_telvent_daily_avgdewpoint")]
		avgDewPoint,
		/// <summary>
		/// avgHeatIndex
		///  </summary>
		[LocalizedText("msg_enum_telvent_daily_avgheatindex")]
		avgHeatIndex,
		/// <summary>
		/// avgRelativeHumidity
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_avgrelativehumidity")]
		avgRelativeHumidity,
		/// <summary>
		/// avgTemperature
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_avgtemperature")]
		avgTemperature,
		///// <summary>
		///// avgTemperatureDepartureFromTenYrNormal
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_avgtemperaturedeparturefromtenyrnormal")]
		//avgTemperatureDepartureFromTenYrNormal,
		/// <summary>
		/// avgWetBulbTemp
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_avgwetbulbtemp")]
		avgWetBulbTemp,
		///// <summary>
		///// avgWindChill
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_avgwindchill")]
		//avgWindChill,
		/// <summary>
		/// avgWindSpeed
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_avgwindspeed")]
		avgWindSpeed,
		/// <summary>
		/// cloudCoverPercentage
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_cloudcoverpercentage")]
		cloudCoverPercentage,
		/// <summary>
		/// coolingDegreeDay
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_coolingdegreeday")]
		coolingDegreeDay,

		/// <summary>
		/// coolingDegreeDay
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_effectivedegreeday")]
		effectiveDegreeDay,
		/// <summary>
		/// coolingDegreeDay
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_growingdegreeday")]
		growingDegreeDay,
		/// <summary>
		/// coolingDegreeDay
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_heatingdegreeday")]
		heatingDegreeDay,
		///// <summary>
		///// coolingDegreeDayDepartureFromTenYrNormal
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_coolingdegreedaydeparturefromtenyrnormal")]
		//coolingDegreeDayDepartureFromTenYrNormal,
		///// <summary>
		///// effectiveDegreeDayDepartureFromTenYrNormal
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_effectivedegreedaydeparturefromtenyrnormal")]
		//effectiveDegreeDayDepartureFromTenYrNormal,
		/// <summary>
		/// evapotranspiration
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_evapotranspiration")]
		evapotranspiration,
		///// <summary>
		///// heatingDegreeDayDepartureFromTenYrNormal
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_heatingdegreedaydeparturefromtenyrnormal")]
		//heatingDegreeDayDepartureFromTenYrNormal,
		/// <summary>
		/// maxFeelsLike
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_maxfeelslike")]
		maxFeelsLike,
		/// <summary>
		/// maxTemperature
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_maxtemperature")]
		maxTemperature,
		/// <summary>
		/// maxWindSpeed
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_maxwindspeed")]
		maxWindSpeed,
		/// <summary>
		/// minTemperature
		/// </summary>
		[LocalizedText("msg_enum_telvent_daily_mintemperature")]
		minTemperature,
		///// <summary>
		///// minutesOfSunshine
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_minutesofsunshine")]
		//minutesOfSunshine,
		///// <summary>
		///// probabilityOfPrecipitation
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_probabilityofprecipitation")]
		//probabilityOfPrecipitation,
		///// <summary>
		///// snowfall
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_snowfall")]
		//snowfall,
		///// <summary>
		///// tenYrNormalCoolingDegreeDay
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_tenyrnormalcoolingdegreeday")]
		//tenYrNormalCoolingDegreeDay,
		///// <summary>
		///// tenYrNormalEffectiveDegreeDay
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_tenyrnormaleffectivedegreeday")]
		//tenYrNormalEffectiveDegreeDay,
		///// <summary>
		///// tenYrNormalHeatingDegreeDay
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_tenyrnormalheatingdegreeday")]
		//tenYrNormalHeatingDegreeDay,
		///// <summary>
		///// tenYrNormalMaxTemperature
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_tenyrnormalmaxtemperature")]
		//tenYrNormalMaxTemperature,
		///// <summary>
		///// tenYrNormalMinTemperature
		///// </summary>
		//[LocalizedText("msg_enum_telvent_daily_tenyrnormalmintemperature")]
		//tenYrNormalMinTemperature
	}

	/// <summary>
	/// Telvent Hourly data Attributes.
	/// </summary>
	public enum TelventHourlyAttributes {
		/// <summary>
		/// cloudCoveragePercentage
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_cloudcoveragepercentage")]
		cloudCoveragePercentage,
		/// <summary>
		/// feelsLike
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_feelslike")]
		feelsLike,
		/// <summary>
		/// temperature
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_temperature")]
		temperature,
		/// <summary>
		/// dewPoint
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_dewpoint")]
		dewPoint,
		/// <summary>
		/// relativeHumidity
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_relativehumidity")]
		relativeHumidity,
		/// <summary>
		/// heatIndex
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_heatindex")]
		heatIndex,
		/// <summary>
		/// windChill
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_windchill")]
		windChill,
		/// <summary>
		/// windGust
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_windgust")]
		windGust,
		/// <summary>
		/// windDirection
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_winddirection")]
		windDirection,
		/// <summary>
		/// windSpeed
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_windspeed")]
		windSpeed,
		/// <summary>
		/// wetBulbTemperature
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_wetbulbtemperature")]
		wetBulbTemperature,
		/// <summary>
		/// minutesOfSunshine
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_minutesofsunshine")]
		minutesOfSunshine,
		/// <summary>
		/// solarRadiation
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_solarradiation")]
		solarRadiation,
		/// <summary>
		/// precipitationAmount
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_precipitationamount")]
		precipitationAmount,
		/// <summary>
		/// snowfall
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_snowfall")]
		snowfall,
		/// <summary>
		/// seaLevelPressure
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_sealevelpressure")]
		seaLevelPressure,
		/// <summary>
		/// barometricPressure
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_barometricpressure")]
		barometricPressure,
		/// <summary>
		/// Visibility
		/// </summary>
		[LocalizedText("msg_enum_telvent_hourly_visibility")]
		visibility
	}

	/// <summary>
	/// Telvel Hourly Forecast
	/// </summary>
	public enum TelventHourlyForecastAttributes {
		/// <summary> 
		/// cloudCoverPercentage 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_cloudcoverpercentage")]
		cloudCoverPercentage,
		/// <summary> 
		/// dewPoint 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_dewpoint")]
		dewPoint,
		/// <summary> 
		/// feelsLike 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_feelslike")]
		feelsLike,
		/// <summary> 
		/// heatIndex 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_heatindex")]
		heatIndex,
		/// <summary> 
		/// minutesOfSunshine 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_minutesofsunshine")]
		minutesOfSunshine,
		/// <summary> 
		/// precipitationAmount 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_precipitationamount")]
		precipitationAmount,
		/// <summary> 
		/// relativeHumidity 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_relativehumidity")]
		relativeHumidity,
		/// <summary> 
		/// seaLevelPressure 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_sealevelpressure")]
		seaLevelPressure,
		/// <summary> 
		/// solarRadiation 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_solarradiation")]
		solarRadiation,
		/// <summary> 
		/// temperature 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_temperature")]
		temperature,
		/// <summary> 
		/// visibility 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_visibility")]
		visibility,
		/// <summary> 
		/// wetBulbTemperature 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_wetbulbtemperature")]
		wetBulbTemperature,
		/// <summary> 
		/// windGust 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_windgust")]
		windGust,
		/// <summary> 
		/// windSpeed 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_hourlyforecast_windspeed")]
		windSpeed
	}

	/// <summary>
	/// Telvel Daily Forecast
	/// </summary>
	public enum TelventDailyForecastAttributes {

		/// <summary> 
		/// avgBarometricPressure 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_avgbarometricpressure")]
		avgBarometricPressure,
		/// <summary> 
		/// avgDewPoint 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_avgdewpoint")]
		avgDewPoint,
		/// <summary> 
		/// avgHeatIndex 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_avgheatindex")]
		avgHeatIndex,
		/// <summary> 
		/// avgRelativeHumidity 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_avgrelativehumidity")]
		avgRelativeHumidity,
		/// <summary> 
		/// avgTemperature 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_avgtemperature")]
		avgTemperature,
		/// <summary> 
		/// avgWetBulbTemp 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_avgwetbulbtemp")]
		avgWetBulbTemp,
		/// <summary> 
		/// avgWindSpeed 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_avgwindspeed")]
		avgWindSpeed,
		/// <summary> 
		/// cloudCoverPercentage 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_cloudcoverpercentage")]
		cloudCoverPercentage,
		/// <summary> 
		/// coolingDegreeDay 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_coolingdegreeday")]
		coolingDegreeDay,
		/// <summary> 
		/// effectiveDegreeDay 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_effectivedegreeday")]
		effectiveDegreeDay,
		/// <summary> 
		/// evapotranspiration 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_evapotranspiration")]
		evapotranspiration,
		/// <summary> 
		/// growingDegreeDay 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_growingdegreeday")]
		growingDegreeDay,
		/// <summary> 
		/// heatingDegreeDay 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_heatingdegreeday")]
		heatingDegreeDay,
		/// <summary> 
		/// maxFeelsLike 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_maxfeelslike")]
		maxFeelsLike,
		/// <summary> 
		/// maxTemperature 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_maxtemperature")]
		maxTemperature,
		/// <summary> 
		/// maxWindSpeed 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_maxwindspeed")]
		maxWindSpeed,
		/// <summary> 
		/// minTemperature 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_mintemperature")]
		minTemperature,
		/// <summary> 
		/// probabilityOfPrecipitation 
		/// </summary> 
		[LocalizedText("msg_enum_telvent_dailyforecast_probabilityofprecipitation")]
		probabilityOfPrecipitation,
	}

	/// <summary>
	/// Telvent Weather request type
	/// </summary>
	public enum TelventWeatherRequestType {
		/// <summary>
		/// Hourly Observation
		/// </summary>
		HourlyObservation,
		/// <summary>
		/// Hourly Forecast
		/// </summary>
		HourlyForecast,
		/// <summary>
		/// Daily Observation
		/// </summary>
		DailyObservation,
		/// <summary>
		/// Daily Forecast
		/// </summary>
		DailyForecast
	}

	/// <summary>
	/// Options for azman import
	/// </summary>
	public enum AzmanImportOptions {

		/// <summary>
		/// CreateOnly
		/// </summary>
		[LocalizedText("msg_enum_azmanimportoptions_createonly")]
		CreateOnly,
		/// <summary>
		/// CreateAndOverwrite
		/// </summary>
		[LocalizedText("msg_enum_azmanimportoptions_createandoverwrite")]
		CreateAndOverwrite,
		/// <summary>
		/// FullImport
		/// </summary>
		[LocalizedText("msg_enum_azmanimportoptions_creatoverwriteanddelete")]
		FullImport
	}

	/// <summary>
	/// Portal tab columns config enum.
	/// </summary>
	public enum PortalTabColumnConfig {
		/// <summary>
		/// 1 Column
		/// </summary>
		[LocalizedText("msg_enum_portaltabcolumnconfig_column1_100")]
		[IconCls("viz-icon-small-portaltab-column1-100")]
		Column1_100,
		/// <summary>
		/// 2 Columns 50% 50%
		/// </summary>
		[LocalizedText("msg_enum_portaltabcolumnconfig_column2_5050")]
		[IconCls("viz-icon-small-portaltab-column2-5050")]
		Column2_5050,
		/// <summary>
		/// 2 Columns 25% 75%
		/// </summary>
		[LocalizedText("msg_enum_portaltabcolumnconfig_column2_2575")]
		[IconCls("viz-icon-small-portaltab-column2-2575")]
		Column2_2575,
		/// <summary>
		/// 2 Columns 75% 25%
		/// </summary>
		[LocalizedText("msg_enum_portaltabcolumnconfig_column2_7525")]
		[IconCls("viz-icon-small-portaltab-column2-7525")]
		Column2_7525,
		/// <summary>
		/// 3 Columns 25% 50% 25%
		/// </summary>
		[LocalizedText("msg_enum_portaltabcolumnconfig_column3_255025")]
		[IconCls("viz-icon-small-portaltab-column3-255025")]
		Column3_255025,
		/// <summary>
		/// 3 Columns 33% 33% 33%
		/// </summary>
		[LocalizedText("msg_enum_portaltabcolumnconfig_column3_333333")]
		[IconCls("viz-icon-small-portaltab-column3-333333")]
		Column3_333333,
		/// <summary>
		///3 Columns 25% 25% 50%
		/// </summary>
		[LocalizedText("msg_enum_portaltabcolumnconfig_column3_252550")]
		[IconCls("viz-icon-small-portaltab-column3-252550")]
		Column3_252550,
		/// <summary>
		///3 Columns 50% 25% 25%
		/// </summary>
		[LocalizedText("msg_enum_portaltabcolumnconfig_column3_502525")]
		[IconCls("viz-icon-small-portaltab-column3-502525")]
		Column3_502525,
		/// <summary>
		/// 4 Columns 25% 25% 25% 25%
		/// </summary>
		[LocalizedText("msg_enum_portaltabcolumnconfig_column4_25252525")]
		[IconCls("viz-icon-small-portaltab-column4-25252525")]
		Column4_25252525
	}

	/// <summary>
	/// Weather display type enum
	/// </summary>
	public enum WeatherDisplayType {
		/// <summary>
		/// Horizontal
		/// </summary>
		[LocalizedText("msg_enum_weatherdisplaytype_horizontal")]
		Horizontal,
		/// <summary>
		/// Vertical
		/// </summary>
		[LocalizedText("msg_enum_weatherdisplaytype_vertical")]
		Vertical,
		/// <summary>
		/// Metro - one day
		/// </summary>
		[LocalizedText("msg_enum_weatherdisplaytype_metrooneday")]
		MetroOneDay,
		/// <summary>
		/// Metro - full forecast
		/// </summary>
		[LocalizedText("msg_enum_weatherdisplaytype_metrofullforecast")]
		MetroFullForecast
	}

	/// <summary>
	/// Enum for the different type of column in a portal tab.
	/// </summary>
	public enum PortalColumnType {
		/// <summary>
		/// Main column
		/// </summary>
		[LocalizedText("msg_enum_portalcolumntype_main")]
		Main,

		/// <summary>
		/// Header column
		/// </summary>
		[LocalizedText("msg_enum_portalcolumntype_header")]
		Header,

		/// <summary>
		/// Footer column
		/// </summary>
		[LocalizedText("msg_enum_portalcolumntype_footer")]
		Footer
	}

	/// <summary>
	/// The mode of loading an entity.
	/// </summary>
	public enum EntityLoadMode {
		/// <summary>
		/// Standard entity load mode.
		/// </summary>
		Standard,
		/// <summary>
		/// Load mode for export
		/// </summary>
		ForExport
	}
	/// <summary>
	/// Enum for limited the number of series displayed on a Chart.
	/// </summary>
	public enum ChartLimitSeriesNumber {
		/// <summary>
		/// No limit
		/// </summary>
		[LocalizedText("msg_enum_chartlimitseriesnumber_displayall")]
		DisplayAll = 0,
		/// <summary>
		/// 1
		/// </summary>
		[LocalizedText("msg_enum_chartlimitseriesnumber_one")]
		One = 1,
		/// <summary>
		/// 2
		/// </summary>
		[LocalizedText("msg_enum_chartlimitseriesnumber_two")]
		Two = 2,
		/// <summary>
		/// 3
		/// </summary>
		[LocalizedText("msg_enum_chartlimitseriesnumber_three")]
		Three = 3,
		/// <summary>
		/// 4
		/// </summary>
		[LocalizedText("msg_enum_chartlimitseriesnumber_four")]
		Four = 4,
		/// <summary>
		/// 5
		/// </summary>
		[LocalizedText("msg_enum_chartlimitseriesnumber_five")]
		Five = 5,
		/// <summary>
		/// 6
		/// </summary>
		[LocalizedText("msg_enum_chartlimitseriesnumber_six")]
		Six = 6,
		/// <summary>
		/// 7
		/// </summary>
		[LocalizedText("msg_enum_chartlimitseriesnumber_seven")]
		Seven = 7,
		/// <summary>
		/// 8
		/// </summary>
		[LocalizedText("msg_enum_chartlimitseriesnumber_eight")]
		Eight = 8,
		/// <summary>
		/// 9
		/// </summary>
		[LocalizedText("msg_enum_chartlimitseriesnumber_nine")]
		Nine = 9,
		/// <summary>
		/// 10
		/// </summary>
		[LocalizedText("msg_enum_chartlimitseriesnumber_ten")]
		Ten = 10
	}

	/// <summary>
	/// Enum for the Energy Aggregator State
	/// </summary>
	public enum EnergyAggregatorState {
		/// <summary>
		/// Uninitialized
		/// </summary>
		Uninitialized,
		/// <summary>
		/// Initializing
		/// </summary>
		Initializing,
		/// <summary>
		/// Initialized
		/// </summary>
		Initialized
	}

	/// <summary>
	/// Determines if the filter is on the node itself, if the node is a descendant of the node that has permissions, or if it is an ancestor of a node that has permissions.
	/// If the filter is non heirarchical the value should always be Direct.
	/// </summary>
	public enum FilterType {

		/// <summary>
		/// Direct
		/// </summary>
		Direct,
		/// <summary>
		/// Descendant
		/// </summary>
		Descendant,
		/// <summary>
		/// Ascendant
		/// </summary>
		Ascendant
	}


	/// <summary>
	/// List of different MeterValidationRule type an alarm instance can be associated with.
	/// </summary>
	public enum MeterValidationRuleAlarmType {
		/// <summary>
		/// None
		/// </summary>
		[LocalizedText("msg_enum_metervalidationrulealarmtype_none")]
		None,

		/// <summary>
		/// Meter Data has the wrong frequency
		/// </summary>
		[LocalizedText("msg_enum_metervalidationrulealarmtype_wrongacquisitiondatetime")]
		WrongAcquisitionDateTime,

		/// <summary>
		/// Meter Data is missing
		/// </summary>
		[LocalizedText("msg_enum_metervalidationrulealarmtype_missing")]
		Missing,

		/// <summary>
		/// Meter Data index is decreasing
		/// </summary>
		[LocalizedText("msg_enum_metervalidationrulealarmtype_indexdecrease")]
		IndexDecrease,

		/// <summary>
		/// Meter Data has a spike
		/// </summary>
		[LocalizedText("msg_enum_metervalidationrulealarmtype_spike")]
		Spike,

		/// <summary>
		/// Meter Data has too many recurring values
		/// </summary>
		[LocalizedText("msg_enum_metervalidationrulealarmtype_recurringvalues")]
		RecurringValues,

		/// <summary>
		/// Meter Data value is outside of the boundaries
		/// </summary>
		[LocalizedText("msg_enum_metervalidationrulealarmtype_valueoutsideboundary")]
		ValueOutsideBoundary,

		/// <summary>
		/// Meter Data value is outside of the boundaries
		/// </summary>
		[LocalizedText("msg_enum_metervalidationrulealarmtype_late")]
		Late,

		/// <summary>
		/// Meter Data validity
		/// </summary>
		[LocalizedText("msg_enum_metervalidationrulealarmtype_validity")]
		Validity
	}

	/// <summary>
	/// Meter data validity type
	/// </summary>
	public enum MeterDataValidity {
		/// <summary>
		/// Valid meter data (default value)
		/// </summary>
		[LocalizedText("msg_enum_meterdatavalidity_valid")]
		[IconCls("viz-icon-small-success")]
		Valid,

		/// <summary>
		/// Invalid meter Data
		/// </summary>
		[LocalizedText("msg_enum_meterdatavalidity_invalid")]
		[IconCls("viz-icon-small-exception")]
		Invalid,

		/// <summary>
		/// Manually edited meter data
		/// </summary>
		[LocalizedText("msg_enum_meterdatavalidity_manuallyedited")]
		[IconCls("viz-icon-small-occupant")]
		ManuallyEdited,

		/// <summary>
		/// Manually edited meter data
		/// </summary>
		[LocalizedText("msg_enum_meterdatavalidity_tobevalidated")]
		[IconCls("viz-icon-small-about")]
		ToBeValidated,


		/// <summary>
		/// Estimated meter data
		/// </summary>
		[LocalizedText("msg_enum_meterdatavalidity_estimated")]
		[IconCls("viz-icon-small-calculatedserie")]
		Estimated,

		/// <summary>
		/// Skipped
		/// </summary>
		[LocalizedText("msg_enum_meterdatavalidity_skipped")]
		[IconCls("viz-icon-small-flipcard")]
		Skipped
	}

	/// <summary>
	/// Multitenant hosting mode
	/// </summary>
	public enum MultitenantHostingMode {
		/// <summary>
		/// Shared means multiple tenants can perform a login operation to the same host (IIS website).
		/// </summary>
		Shared,
		/// <summary>
		/// Isolated means each tenant can only login to his own host (IIS website)
		/// </summary>
		Isolated
	}

	/// <summary>
	/// Different DrillDown Types.
	/// </summary>
	public enum DrillDownType {
		/// <summary>
		/// Update the StartDate of the Chart
		/// </summary>
		StartDate,
		/// <summary>
		/// Update the EndDate of the Chart
		/// </summary>
		EndDate,
		/// <summary>
		/// Set StartDate and EndDate to Selected Date and Drill Down 1 Level of time interval
		/// </summary>
		FocusDate,
		/// <summary>
		/// Drill down time interval
		/// </summary>
		DrillDownTimeInterval,
		/// <summary>
		/// Drill up time interval
		/// </summary>
		DrillUpTimeInterval,
		/// <summary>
		/// Set the selected location as the new spatial filter.
		/// </summary>
		FocusLocation,
		/// <summary>
		///Drill down location axis.
		/// </summary>
		DrillDownLocalisation,
		/// <summary>
		/// Drill up locatin axis.
		/// </summary>
		DrillUpLocalisation,
		/// <summary>
		/// Set the select classification as the new classification filter.
		/// </summary>
		FocusClassification,
		/// <summary>
		/// Drill down classification axis.
		/// </summary>
		DrillDownClassification,
		/// <summary>
		/// Drill up cladssification axis.
		/// </summary>
		DrillUpClassification
	}

	/// <summary>
	/// Discribe the different mode for a Chart to respond to drill down
	/// </summary>
	public enum ChartDrillDownMode {
		/// <summary>
		/// chart will no be affected by drill down at all.
		/// </summary>
		[LocalizedText("msg_enum_chartdrilldownmode_disabled")]
		Disabled,
		/// <summary>
		/// the drill down will only affect this chart.
		/// </summary>
		[LocalizedText("msg_enum_chartdrilldownmode_autonomous")]
		Autonomous,
		/// <summary>
		/// chart will listeb to other chart drill down.
		/// </summary>
		[LocalizedText("msg_enum_chartdrilldownmode_listener")]
		Listener,
		/// <summary>
		/// chart will not respond to its own drill down, but will affect other chart on the same portal tab.
		/// </summary>
		[LocalizedText("msg_enum_chartdrilldownmode_masternochangeportaltab")]
		MasterNoChangePortalTab,
		/// <summary>
		/// chart will not respond to its own drill down, but will affect other chart on the same portal window.
		/// </summary>
		[LocalizedText("msg_enum_chartdrilldownmode_masternochangeportalwindow")]
		MasterNoChangePortalWindow,
		/// <summary>
		/// chart will respond to its own drill down and will affect other chart on the same portal tab.
		/// </summary>
		[LocalizedText("msg_enum_chartdrilldownmode_masterwithchangeportaltab")]
		MasterWithChangePortalTab,
		/// <summary>
		/// chart will respond to its own drill down and will affect other chart on the same portal tab.
		/// </summary>
		[LocalizedText("msg_enum_chartdrilldownmode_masterwithchangeportalwindow")]
		MasterWithChangePortalWindow
	}


	/// <summary>
	/// Project a DataSerie to ouput a DataSerie with a single DataPoint.
	/// </summary>
	public enum DataSerieProjection {
		/// <summary>
		/// Average
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_avg")]
		Average,
		/// <summary>
		/// First
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_first")]
		First,
		/// <summary>
		/// Last
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_last")]
		Last,
		/// <summary>
		/// Maximum
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_max")]
		Maximum,
		/// <summary>
		/// Minimum
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_min")]
		Minimum,
		/// <summary>
		/// Penultimate
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_penultimate")]
		Penultimate,
		/// <summary>
		/// Sum
		/// </summary>
		[LocalizedText("msg_enum_mathematicoperator_sum")]
		Sum
	}

	/// <summary>
	/// List of available modes for the Main ViewPort.
	/// </summary>
	public enum MainViewPortMode {
		/// <summary>
		/// Minimal
		/// </summary>
		[LocalizedText("msg_enum_mainviewportmode_minimal")]
		[IconCls("viz-icon-small-mainviewportmode-minimal")]
		Minimal,

		/// <summary>
		/// Compact
		/// </summary>
		[LocalizedText("msg_enum_mainviewportmode_compact")]
		[IconCls("viz-icon-small-mainviewportmode-compact")]
		Compact,

		/// <summary>
		/// Extended
		/// </summary>
		[LocalizedText("msg_enum_mainviewportmode_extended")]
		[IconCls("viz-icon-small-mainviewportmode-extended")]
		Extended,

		/// <summary>
		/// Full
		/// </summary>
		[LocalizedText("msg_enum_mainviewportmode_full")]
		[IconCls("viz-icon-small-mainviewportmode-full")]
		Full

	}


	/// <summary>
	/// List of possible Chart legend positions.
	/// </summary>
	public enum ChartLegendPosition {
		/// <summary>
		/// ChartTitle
		/// </summary>
		[LocalizedText("msg_enum_chartlegendposition_charttitle")]
		ChartTitle,

		/// <summary>
		/// Top
		/// </summary>
		[LocalizedText("msg_enum_chartlegendposition_top")]
		Top,

		/// <summary>
		/// Middle
		/// </summary>
		[LocalizedText("msg_enum_chartlegendposition_middle")]
		Middle,

		/// <summary>
		/// Bottom
		/// </summary>
		[LocalizedText("msg_enum_chartlegendposition_bottom")]
		Bottom,

		/// <summary>
		/// BottomMiddle
		/// </summary>
		[LocalizedText("msg_enum_chartlegendposition_bottommiddle")]
		BottomMiddle,

		/// <summary>
		/// ChartArea
		/// </summary>
		[LocalizedText("msg_enum_chartlegendposition_chartarea")]
		ChartArea
	}


	/// <summary>
	/// List of all possible Chart info icon position
	/// </summary>
	public enum ChartInfoIconPosition {
		/// <summary>
		/// None
		/// </summary>
		[LocalizedText("msg_enum_chartinfoiconposition_none")]
		None,

		/// <summary>
		/// TopLeft
		/// </summary>
		[LocalizedText("msg_enum_chartinfoiconposition_topleft")]
		TopLeft,

		/// <summary>
		/// BottomLeft
		/// </summary>
		[LocalizedText("msg_enum_chartinfoiconposition_bottomleft")]
		BottomLeft,

		/// <summary>
		/// TopRight
		/// </summary>
		[LocalizedText("msg_enum_chartinfoiconposition_topright")]
		TopRight,

		/// <summary>
		/// BottomRight
		/// </summary>
		[LocalizedText("msg_enum_chartinfoiconposition_bottomright")]
		BottomRight

	}

	/// <summary>
	/// Algorithm language
	/// </summary>
	public enum AlgorithmLanguage {
		/// <summary>
		/// Unknown
		/// </summary>
		[LocalizedText("msg_enum_algorithmlanguage_unknown")]
		Unknown,

		/// <summary>
		/// Not Applicable
		/// </summary>
		[LocalizedText("msg_enum_algorithmlanguage_notapplicable")]
		NotApplicable,

		/// <summary>
		/// Python
		/// </summary>
		[LocalizedText("msg_enum_algorithmlanguage_python")]
		Python,


		/// <summary>
		/// C#
		/// </summary>
		[LocalizedText("msg_enum_algorithmlanguage_csharp")]
		CSharp

	}


	/// <summary>
	/// The source of the Algorithm.
	/// </summary>
	public enum AlgorithmSource {
		/// <summary>
		/// Built-in script.
		/// </summary>
		[LocalizedText("msg_enum_algorithmsource_builtin")]
		BuiltIn,

		/// <summary>
		/// Editable, typed-in script.
		/// </summary>
		[LocalizedText("msg_enum_algorithmsource_editable")]
		Editable,

		/// <summary>
		/// External libary.
		/// </summary>
		[LocalizedText("msg_enum_algorithmsource_externallibrary")]
		ExternalLibrary
	}

	/// <summary>
	/// ModernUI Colors
	/// </summary>
	public enum ModernUIColor {

		/// <summary>
		/// White
		/// </summary>
		[LocalizedText("msg_enum_modernuicolors_white")]
		White,

		/// <summary>
		/// Purple
		/// </summary>
		[LocalizedText("msg_enum_modernuicolors_purple")]
		Purple,

		/// <summary>
		/// Magenta
		/// </summary>
		[LocalizedText("msg_enum_modernuicolors_magenta")]
		Magenta,

		/// <summary>
		/// Teal
		/// </summary>
		[LocalizedText("msg_enum_modernuicolors_teal")]
		Teal,

		/// <summary>
		/// Lime
		/// </summary>
		[LocalizedText("msg_enum_modernuicolors_lime")]
		Lime,

		/// <summary>
		/// Brown
		/// </summary>
		[LocalizedText("msg_enum_modernuicolors_brown")]
		Brown,

		/// <summary>
		/// Pink
		/// </summary>
		[LocalizedText("msg_enum_modernuicolors_pink")]
		Pink,

		/// <summary>
		/// Orange
		/// </summary>
		[LocalizedText("msg_enum_modernuicolors_orange")]
		Orange,

		/// <summary>
		/// Blue
		/// </summary>
		[LocalizedText("msg_enum_modernuicolors_blue")]
		Blue,

		/// <summary>
		/// Red
		/// </summary>
		[LocalizedText("msg_enum_modernuicolors_red")]
		Red,

		/// <summary>
		/// Green
		/// </summary>
		[LocalizedText("msg_enum_modernuicolors_green")]
		Green

	}

	/// <summary>
	/// Line Dash Style
	/// </summary>
	public enum LineDashStyle {
		/// <summary>
		/// Solid
		/// </summary>
		[LocalizedText("msg_enum_linedashstyle_solid")]
		Solid,
		/// <summary>
		/// Dash
		/// </summary>
		[LocalizedText("msg_enum_linedashstyle_dash")]
		Dash,
		/// <summary>
		/// Dot
		/// </summary>
		[LocalizedText("msg_enum_linedashstyle_dot")]
		Dot,
		/// <summary>
		/// DashDot
		/// </summary>
		[LocalizedText("msg_enum_linedashstyle_dashdot")]
		DashDot,
		/// <summary>
		/// DashDotDot
		/// </summary>
		[LocalizedText("msg_enum_linedashstyle_dashdotdot")]
		DashDotDot
	}

	/// <summary>
	/// Marker Type
	/// </summary>
	public enum MarkerType {
		/// <summary>
		/// Circle
		/// </summary>
		[LocalizedText("msg_enum_markertype_circle")]
		Circle,
		/// <summary>
		/// None
		/// </summary>
		[LocalizedText("msg_enum_markertype_none")]
		None,
		/// <summary>
		/// Square
		/// </summary>
		[LocalizedText("msg_enum_markertype_square")]
		Square,
		/// <summary>
		/// Triangle
		/// </summary>
		[LocalizedText("msg_enum_markertype_triangle")]
		Triangle,

		/// <summary>
		/// Diamond
		/// </summary>
		[LocalizedText("msg_enum_markertype_diamond")]
		Diamond,
		/// <summary>
		/// Star
		/// </summary>
		[LocalizedText("msg_enum_markertype_star")]
		Star,
		/// <summary>
		/// Split
		/// </summary>
		[LocalizedText("msg_enum_markertype_split")]
		Split,
		/// <summary>
		/// ReverseSplit
		/// </summary>
		[LocalizedText("msg_enum_markertype_reversesplit")]
		ReverseSplit,
		/// <summary>
		/// Merger
		/// </summary>
		[LocalizedText("msg_enum_markertype_merger")]
		Merger,
		/// <summary>
		/// Dividend
		/// </summary>
		[LocalizedText("msg_enum_markertype_dividend")]
		Dividend,
		/// <summary>
		/// Spinoff
		/// </summary>
		[LocalizedText("msg_enum_markertype_spinoff")]
		Spinoff,
		/// <summary>
		/// ArrowUp
		/// </summary>
		[LocalizedText("msg_enum_markertype_arrowup")]
		ArrowUp,
		/// <summary>
		/// ArrowDown
		/// </summary>
		[LocalizedText("msg_enum_markertype_arrowdown")]
		ArrowDown,
		/// <summary>
		/// TriangleUpsideDown
		/// </summary>
		[LocalizedText("msg_enum_markertype_triangleupsidedown")]
		TriangleUpsideDown
	}

	/// <summary>
	/// Enum for defining a Authentication Scheme.
	/// Used in EWS DataAcquisition container.
	/// </summary>
	public enum AuthenticationScheme {
		/// <summary>
		/// Anonymous.
		/// </summary>
		[LocalizedText("msg_enum_authenticationscheme_anonymous")]
		Anonymous,

		/// <summary>
		/// Basic.
		/// </summary>
		[LocalizedText("msg_enum_authenticationscheme_basic")]
		Basic,

		/// <summary>
		/// Digest.
		/// </summary>
		[LocalizedText("msg_enum_authenticationscheme_digest")]
		Digest
	}

	/// <summary>
	/// Dynamic display image type
	/// </summary>
	public enum DynamicDisplayImageType {
		/// <summary>
		/// Image
		/// </summary>
		[LocalizedText("msg_enum_dynamicdisplayimagetype_image")]
		Image,

		/// <summary>
		/// Icon
		/// </summary>
		[LocalizedText("msg_enum_dynamicdisplayimagetype_icon")]
		Icon,

		/// <summary>
		/// WallPaper
		/// </summary>
		[LocalizedText("msg_enum_dynamicdisplayimagetype_desktopbackground")]
		DesktopBackground,

		/// <summary>
		/// KPI Preview
		/// </summary>
		[LocalizedText("msg_enum_dynamicdisplayimagetype_kpipreview")]
		KPIPreviewPicture
	}

	/// <summary>
	/// Alarm instance source
	/// </summary>
	public enum AlarmInstanceSource {		
        /// <summary>
		/// AlarmDefinition
		/// </summary>
		AlarmDefinition,
		/// <summary>
		/// MeterValidationRule
		/// </summary>
		MeterValidationRule
	}

    /// <summary>
    /// Alarm Definition Source
    /// </summary>
    public enum AlarmDefinitionSource {
        /// <summary>
        /// The unknown
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Meter
        /// </summary>
        Meter,

        /// <summary>
        /// Chart
        /// </summary>
        Chart
    }

    /// <summary>
	/// Options for auto creating meters
	/// </summary>
	public enum AutoCreateMeterOptions
	{
		/// <summary>
		/// The off
		/// </summary>
		[LocalizedText("msg_auto_create_meters_off")]
		Off,

		/// <summary>
		/// The automatic create in default location
		/// </summary>
		[LocalizedText("msg_auto_create_meters_default_location")]
		AutoCreateInDefaultLocation,

		/// <summary>
		/// The automatic create in custom location
		/// </summary>
		[LocalizedText("msg_auto_create_meters_custom_location")]
		AutoCreateInCustomLocation
	}
}

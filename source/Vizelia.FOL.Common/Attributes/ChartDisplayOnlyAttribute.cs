﻿using System;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A custom attribute for marking Chart attributes that only affects the chart display (in order not to go back to the EA).
	/// </summary>
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
	public class ChartDisplayOnlyAttribute : Attribute {
	}

	/// <summary>
	/// A custom attribute for marking Chart attributes that only affects the chart dynamic display.
	/// </summary>
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
	public class ChartDynamicDisplayAttribute : Attribute {
	}
}



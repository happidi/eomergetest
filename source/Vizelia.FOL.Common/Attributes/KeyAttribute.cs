﻿
namespace Vizelia.FOL.Common {

	/// <summary>
	/// A custom attribute that applies to class and indicates the property name of the Id of the business entity it applies to.
	/// This attribute is used mainly on business entities send as a JsonStore, and exposed throud th idProperty of the metaData config.
	/// </summary>
	public class KeyAttribute : BaseAttribute {
		/// <summary>
		/// Initializes a new instance of the attribute
		/// </summary>
		/// <param name="PropertyNameId">The property name of the id.</param>
		public KeyAttribute(string PropertyNameId) : base(PropertyNameId) { }
	}
}

﻿using System;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A custom attribute that applies to a property and indicates its a class that should be deeply audited.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class AuditedClassAttribute : Attribute {
	}
}

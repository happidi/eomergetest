﻿using System;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A custom attribute that applies to a property and indicates its a List that should be converted to string before audited.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class AuditListAttribute : Attribute {
	}
}

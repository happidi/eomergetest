﻿
namespace Vizelia.FOL.Common {

	/// <summary>
	/// A custom attribute that applies to class and indicates the property name of the attribute controlling the pset for the entity.
	/// For example for IfcOccupant the attribute points to KeyClassificationItemWorktype
	/// </summary>
	public class PsetClassificationItemKeyAttribute : BaseAttribute {
		/// <summary>
		/// Initializes a new instance of the attribute
		/// </summary>
		/// <param name="PropertyNameId">The property name of the id.</param>
		public PsetClassificationItemKeyAttribute(string PropertyNameId) : base(PropertyNameId) { }
	}
}

﻿using System;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// A custom base class attribute.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public abstract class BaseAttribute : Attribute {
		private string propertyNameId;

		/// <summary>
		/// Initializes a new instance of the attribute.
		/// </summary>
		/// <param name="PropertyNameId">The property name of the id.</param>
		protected BaseAttribute(string PropertyNameId) {
			propertyNameId = PropertyNameId;
		}

		/// <summary>
		/// The property name of the id.
		/// </summary>
		public string PropertyNameId {
			get {
				return propertyNameId;
			}
		}

	}
}

﻿using System;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A custom attribute that applies to a class and indicates its audit enabled.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public class AuditableEntityAttribute : Attribute {
	}
}

﻿using System;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A custom attribute that applies to a property and indicates its a class that should be deeply audited.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class AuditKeyAttribute : Attribute {
		/// <summary>
		/// Initializes a new instance of the <see cref="AuditKeyAttribute"/> class.
		/// </summary>
		/// <param name="baseBusinessEntityType">Type of the base business entity.</param>
		public AuditKeyAttribute(Type baseBusinessEntityType) {
			BaseBusinessEntityType = baseBusinessEntityType;
		}

		/// <summary>
		/// Gets the type of the base business entity.
		/// </summary>
		/// <value>
		/// The type of the base business entity.
		/// </value>
		public Type BaseBusinessEntityType { get; private set; }
	}
}

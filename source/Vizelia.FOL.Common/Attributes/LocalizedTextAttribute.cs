﻿using System;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// A custom attribute that applies to an enum field or to a class and indicates what should be the localized message.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Class | AttributeTargets.Event, AllowMultiple = false, Inherited = true)]
	public class LocalizedTextAttribute : BaseAttribute {

		/// <summary>
		/// Initializes a new instance of the attribute
		/// </summary>
		/// <param name="PropertyNameId">The property name of the id.</param>
		public LocalizedTextAttribute(string PropertyNameId) : base(PropertyNameId) { }

	}
}

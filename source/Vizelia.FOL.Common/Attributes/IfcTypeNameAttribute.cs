﻿
namespace Vizelia.FOL.Common {

	/// <summary>
	/// A custom attribute that applies to class and indicates the Ifc TypeName of the business entity it applies to.
	/// </summary>
	public class IfcTypeNameAttribute : BaseAttribute {
		/// <summary>
		/// Initializes a new instance of the attribute
		/// </summary>
		/// <param name="PropertyNameId">The property name of the id.</param>
		public IfcTypeNameAttribute(string PropertyNameId) : base(PropertyNameId) { }
	}
}

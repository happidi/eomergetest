﻿using System;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// A custom attribute that applies to a field and indicates what should be the filter.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class FilterTypeAttribute : BaseAttribute {

		/// <summary>
		/// Initializes a new instance of the attribute
		/// </summary>
		/// <param name="FilterBusinessEntityType">Type of the filter business entity.</param>
		/// <param name="showAllIfEmpty">if set to <c>true</c> [show all if empty].</param>
		public FilterTypeAttribute(Type FilterBusinessEntityType, bool showAllIfEmpty=true) : base(FilterBusinessEntityType.FullName) {
			this.FilterBusinessEntityType = FilterBusinessEntityType;
			ShowAllIfEmpty = showAllIfEmpty;
		}


		/// <summary>
		/// Gets or sets the type of the filter business entity.
		/// </summary>
		/// <value>
		/// The type of the filter business entity.
		/// </value>
		public Type FilterBusinessEntityType { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether we show all entities if the filter is empty, default to true.
		/// </summary>
		/// <value>
		///   <c>true</c> if [show all if empty]; otherwise, <c>false</c>.
		/// </value>
		public bool ShowAllIfEmpty { get; set; }

	}
}

﻿using System;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// A custom attribute that applies to a custom provider interface and indicates the provider should not be wrapped by Enterprise Library.
	/// For example used by WorkflowExtension provider.
	/// </summary>
	[AttributeUsage(AttributeTargets.Interface, AllowMultiple = false, Inherited = true)]
	public class EnterpriseLibraryNotWrappableAttribute : BaseAttribute {
		/// <summary>
		/// Initializes a new instance of the attribute
		/// </summary>
		public EnterpriseLibraryNotWrappableAttribute() : base(null) { }
	}
}

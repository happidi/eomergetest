using System;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// An attribute describing the mapping schema.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
	public class MappingSchemaAttribute : Attribute {
		
		/// <summary>
		/// Gets the entity.
		/// </summary>
		public string Entity { get; private set; }

		/// <summary>
		/// Gets the name.
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Gets the version.
		/// </summary>
		public string Version { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="MappingSchemaAttribute"/> class.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="name">The name.</param>
		/// <param name="version">The version.</param>
		public MappingSchemaAttribute(string entity, string name, string version) {
			Entity = entity;
			Name = name;
			Version = version;
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns>
		///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
		/// </returns>
		public override bool Equals(object obj) {
			var other = obj as MappingSchemaAttribute;

			if (other != null) {
				bool isEqual =
					Entity == other.Entity &&
					Name == other.Name &&
					Version == other.Version;

				return isEqual;
			}

			return base.Equals(obj);
		}

		/// <summary>
		/// Returns a hash code for this instance.
		/// </summary>
		/// <returns>
		/// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
		/// </returns>
		public override int GetHashCode() {
			return base.GetHashCode();
		}
	}
}
﻿using System;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// A custom attribute that applies to class or an field and indicates the icon cls of the business entity it applies to.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Event |AttributeTargets.Method,  AllowMultiple = false, Inherited = true)]
	public class IconClsAttribute : BaseAttribute {

		/// <summary>
		/// Initializes a new instance of the attribute
		/// </summary>
		/// <param name="PropertyNameId">The property name of the id.</param>
		public IconClsAttribute(string PropertyNameId) : base(PropertyNameId) { }

	}
}

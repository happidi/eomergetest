using System;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// An attribute that signifies the class is an Algorithm adapter of the given type.
	/// </summary>
	[AttributeUsage(AttributeTargets.Interface, AllowMultiple = false, Inherited = false)]
	public class AlgorithmAdapterAttribute : Attribute {
		/// <summary>
		/// Gets or sets the type of the interface.
		/// </summary>
		/// <value>
		/// The type of the interface.
		/// </value>
		public Type InterfaceType { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="AlgorithmAdapterAttribute"/> class.
		/// </summary>
		/// <param name="interfaceType">Type of the interface.</param>
		public AlgorithmAdapterAttribute(Type interfaceType) {
			InterfaceType = interfaceType;
		}
	}
}
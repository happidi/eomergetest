﻿using System;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A custom attribute for filtering properties in DataModel entities
	/// </summary>
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
	public class ViewableByDataModelAttribute : Attribute {
	}
}

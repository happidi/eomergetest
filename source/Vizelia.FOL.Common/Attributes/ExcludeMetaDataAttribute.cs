﻿using System;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// A custom attribute that applies to a class property and indicates that the property name should be excluded from Jsonstore metadata.
	/// This is necessary when the property is another Entity and could be null. 
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class ExcludeMetaDataAttribute : Attribute {}
}

using System;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// A custom attribute for providing display information about classes or properties.
	/// </summary>
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
	public class InformationAttribute : Attribute {
		private string m_MsgCode;
		private string m_MsgCodeDescription;

		/// <summary>
		/// Initializes a new instance of the <see cref="InformationAttribute"/> class.
		/// </summary>
		public InformationAttribute() {

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="InformationAttribute"/> class.
		/// </summary>
		/// <param name="msgCode">The name that should displayed if this class is to be shown in the UI. Should be a resource key.</param>
		/// <param name="msgCodeDescription">The description that should accompany this class if it is shown in the UI. Should be a resource key.</param>
		public InformationAttribute(string msgCode, string msgCodeDescription) {
			m_MsgCode = msgCode;
			m_MsgCodeDescription = msgCodeDescription;

		}

		/// <summary>
		/// Gets the display description.
		/// </summary>
		public string MsgCodeDescription {
			get { return m_MsgCodeDescription; }
		}

		/// <summary>
		/// Gets the display name.
		/// </summary>
		public string MsgCode {
			get {
				return m_MsgCode;
			}
		}
	}
}
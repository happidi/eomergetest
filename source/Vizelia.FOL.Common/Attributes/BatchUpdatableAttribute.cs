﻿using System;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// A custom attribute that applies to a class property and indicates that the property name should be excluded from UpdateBatch.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class BatchUpdatableAttribute : Attribute {

		private bool _isUpdatable;

		/// <summary>
		/// Initializes a new instance of the attribute.
		/// </summary>
		public BatchUpdatableAttribute(bool isUpdatable) {
				_isUpdatable = isUpdatable;
		}


		/// <summary>
		/// Gets a value indicating whether this property is updatable.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this property is updatable; otherwise, <c>false</c>.
		/// </value>
		public bool IsUpdatable {
			get {
				return _isUpdatable;
			}
		}
	}
}

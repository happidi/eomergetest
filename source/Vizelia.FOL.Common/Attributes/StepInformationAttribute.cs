﻿using System;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// A custom attribute for providing step information when running a job.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public class StepInformationAttribute : InformationAttribute {
		private readonly string m_GridXType;
		private readonly string m_StoreXType;
		private readonly string m_KeyEntity;

		/// <summary>
		/// Initializes a new instance of the <see cref="StepInformationAttribute"/> class.
		/// </summary>
		/// <param name="msgCode">The name that should displayed if this class is to be shown in the UI. Should be a resource key.</param>
		/// <param name="msgCodeDescription">The description that should accompany this class if it is shown in the UI. Should be a resource key.</param>
		/// <param name="keyEntity">The key entity.</param>
		/// <param name="gridXType">The xtype of the grid for this class..</param>
		/// <param name="storeXType">Type of the store X.</param>
		public StepInformationAttribute(string msgCode, string msgCodeDescription, string keyEntity, string gridXType, string storeXType = "") : base(msgCode, msgCodeDescription) {
			m_GridXType = gridXType;
			m_KeyEntity = keyEntity;
			m_StoreXType = storeXType;
		}

		/// <summary>
		/// Gets the xtype of the grid for this class.
		/// </summary>
		/// <value>
		/// The the xtype of the grid for this class.
		/// </value>
		public string GridXType {
			get { return m_GridXType; }
		}

		/// <summary>
		/// Gets the xtype of the store for this class.
		/// </summary>
		/// <value>
		/// The the xtype of the store for this class.
		/// </value>
		public string StoreXType {
			get { return m_StoreXType; }
		}

		/// <summary>
		/// Gets or sets the key entity.
		/// </summary>
		/// <value>
		/// The key entity.
		/// </value>
		public string KeyEntity {
			get { return m_KeyEntity; }
		}
	}
}
﻿using System;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// A custom attribute that applies to a property and indicates its a class that should be deeply audited.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
	public class OrderAttribute : Attribute {

		/// <summary>
		/// Initializes a new instance of the <see cref="OrderAttribute" /> class.
		/// </summary>
		/// <param name="order">The order.</param>
		public OrderAttribute(int order) {
			Order = order;
		}

		/// <summary>
		/// Gets the type of the base business entity.
		/// </summary>
		/// <value>
		/// The type of the base business entity.
		/// </value>
		public int Order { get; private set; }
	}
}

﻿
using System.Linq;
using System.Reflection;

namespace Vizelia.FOL.Common
{
    /// <summary>
    /// Hardcoded project version; manually change before deploying.
    /// </summary>
    public class Version
    {
    	/// <summary>
    	/// Assembly version.
    	/// </summary>
    	public static string Number {
    		get {
    			var assembly = typeof (Version).Assembly;
    			var hotfixVersion = "";
				var attribute = (AssemblyInformationalVersionAttribute)assembly.GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), false).FirstOrDefault();
    			if (attribute != null)
    				hotfixVersion = attribute.InformationalVersion;
				return assembly.GetName().Version + hotfixVersion;
    		}
    	} 
    }
}

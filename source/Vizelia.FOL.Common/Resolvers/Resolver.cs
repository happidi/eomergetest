﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Practices.EnterpriseLibrary.PolicyInjection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// The normal by-reflection resolver.
	/// </summary>
	public class Resolver : IResolver {

		private static bool m_AreAssembliesLoadedToAppDomain = false;


		private const string const_resolve_type_cache_key = "resolve_type_";


		/// <summary>
		/// Resolves the given type T to the first concrete implementation of it found using reflection.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public T Resolve<T>() {

			var concreteType = CacheService.Get(const_resolve_type_cache_key + typeof(T).FullName,
												() => ResolveToConcreteClassType(typeof(T)), location: CacheLocation.Memory);

			// Create an instance of the concrete type using Policy Injection.
			//if (VizeliaConfiguration.Instance.Deployment.UsePolicyInjection) {
			//    return (T)PolicyInjection.Create(concreteType, typeof(T));
			//}
			return (T)CreateInstance(concreteType, typeof(T)); //, typeof(T));
		}

		/// <summary>
		/// Resolves the requested type to its concrete type which is a class.
		/// </summary>
		/// <param name="abstractType">Type of the abstract.</param>
		/// <returns></returns>
		public Type ResolveToConcreteClassType(Type abstractType) {
			// Poor man's IOC, using reflection. Need to replace this with Unity or other IOC container.
			Func<Type, bool> logicalTest = typeToExamine =>
										   !typeToExamine.IsAbstract && !typeToExamine.IsInterface &&
										   abstractType.IsAssignableFrom(typeToExamine);

			Type resolved = ResolveType(logicalTest);

			return resolved;
		}

		/// <summary>
		/// Resolves the type by the given logical test.
		/// </summary>
		/// <param name="logicalTest">The logical test.</param>
		/// <returns></returns>
		public Type ResolveType(Func<Type, bool> logicalTest) {
			// Get the Vizelia DLLs in the BIN directory, where the Common assembly is located.
			var commonAssemblyPath = new Uri(typeof(Helper).Assembly.CodeBase);
			DirectoryInfo binDirectory = new FileInfo(commonAssemblyPath.LocalPath).Directory;

			if (binDirectory != null) {
				Assembly[] loadedAssemblies = LoadAllAssemblies(binDirectory);

				// Find a concrete implementation of T within the loaded assemblies.
				Type concreteType = null;
				foreach (var loadedAssembly in loadedAssemblies) {
					if (loadedAssembly.FullName.StartsWith("Vizelia.FOL.Common") ||
						loadedAssembly.FullName.StartsWith("Vizelia.FOL.WCFService") ||
						loadedAssembly.FullName.StartsWith("Vizelia.FOL.DataLayer.CoreDataAccess") ||
						loadedAssembly.FullName.StartsWith("Vizelia.FOL.Extensibility.Implementation") ||
						loadedAssembly.FullName.StartsWith("Vizelia.FOL.BusinessLayer") ||
						loadedAssembly.FullName.StartsWith("Vizelia.FOL.DataLayer.EntityFramework")) {

						Type[] types = loadedAssembly.GetTypes();
						foreach (var t in types) {
							if (logicalTest(t)) {
								concreteType = t;
								break;
							}
						}
						if (concreteType != null) break;
					}
				}

				if (concreteType == null) {
					throw new VizeliaException("Cannot find concrete implementation for type.");
				}

				return concreteType;
			}

			throw new VizeliaException("Cannot find concrete implementation for type.");
		}

		private Assembly[] LoadAllAssemblies(DirectoryInfo binDirectory) {
			// Filter only assemblies that are not yet loaded into memory. 
			var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();

			if (!m_AreAssembliesLoadedToAppDomain) {
				List<FileInfo> vizeliaLibraries = binDirectory.GetFiles("Vizelia.FOL.*.dll").ToList();

				while (vizeliaLibraries.Any()) {
					// If not already loaded, and not a dynamic assembly, load it.
					if (!loadedAssemblies.Where(a => !a.IsDynamic).Any(a => a.CodeBase.Equals(vizeliaLibraries[0].FullName))) {
						Assembly.LoadFrom(vizeliaLibraries[0].FullName);
					}

					vizeliaLibraries.RemoveAt(0);

					// We check again whether the assembly has been loaded or not.
					loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
				}
				m_AreAssembliesLoadedToAppDomain = true;
			}

			return loadedAssemblies;
		}

		/// <summary>
		/// A helper function for injection.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="typeToReturn">The type to return.</param>
		/// <param name="args">Optional params for ctor.</param>
		/// <returns>
		/// An instance of the specified type.
		/// </returns>
		private object CreateInstance(Type type, Type typeToReturn, params object[] args) {
			if (VizeliaConfiguration.Instance.Deployment.UsePolicyInjection) {
				object createdInstance = PolicyInjection.Create(type, typeToReturn, args);
				return createdInstance;
			}
			return Activator.CreateInstance(type, args);
		}
	}
}
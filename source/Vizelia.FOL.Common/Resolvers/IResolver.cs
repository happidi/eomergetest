﻿using System;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Resolver interface
	/// </summary>
	public interface IResolver {
		/// <summary>
		/// Resolves the given type T to the first concrete implementation of it found using reflection.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		T Resolve<T>();

		/// <summary>
		/// Resolves the requested type to its concrete type which is a class.
		/// </summary>
		/// <param name="abstractType">Type of the abstract.</param>
		/// <returns></returns>
		Type ResolveToConcreteClassType(Type abstractType);


		/// <summary>
		/// Resolves the type by the given logical test.
		/// </summary>
		/// <param name="logicalTest">The logical test.</param>
		/// <returns></returns>
		Type ResolveType(Func<Type, bool> logicalTest);
	}
}
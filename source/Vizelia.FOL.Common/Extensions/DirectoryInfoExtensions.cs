﻿using System.IO;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// Encapsulates extensions for the System.IO.DirectoryInfo class.
	/// </summary>
	public static class DirectoryInfoExtensions {
		/// <summary>
		/// Deletes all the content of the directory.
		/// </summary>
		/// <param name="directory">The directory.</param>
		public static void DeleteAllContent(this DirectoryInfo directory) {
			// Delete all subdirectories recuresively.
			var subdirectories = directory.GetDirectories();

			foreach (DirectoryInfo subdirectory in subdirectories) {
				Directory.Delete(subdirectory.FullName, true);
			}

			// Delete all first-level files.
			var files = directory.GetFiles();

			foreach (FileInfo file in files) {
				file.Delete();
			}

			// Now the directory is empty. :)
		}

		/// <summary>
		/// Recursively set the directory and its contents to be non-read-only.
		/// </summary>
		/// <param name="directoryInfo">The directory info.</param>
		public static void SetNonReadonlyRecursive(this DirectoryInfo directoryInfo) {
			DirectoryInfo parentDirectory = directoryInfo.Parent ?? directoryInfo;
			FileSystemInfo fsi = parentDirectory.GetFileSystemInfos(directoryInfo.Name)[0];

			SetNonReadonlyRecursive(fsi);
		}

		/// <summary>
		/// Recursively set the directory and its contents to be non-read-only.
		/// </summary>
		/// <param name="fsi">The FileSystemInfo representing the directory.</param>
		private static void SetNonReadonlyRecursive(FileSystemInfo fsi) {
			fsi.Attributes = FileAttributes.Normal;
			var di = fsi as DirectoryInfo;

			if (di != null) {
				foreach (FileSystemInfo dirInfo in di.GetFileSystemInfos()) {
					SetNonReadonlyRecursive(dirInfo);
				}
			}
		}



	}
}
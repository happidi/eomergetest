﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Encapsulates extension for CalendarDaySpecifier.
	/// </summary>
	public static class CalendarDaySpecifierExtensions {
		/// <summary>
		/// Gets the short day string. SUN/MON/TUE...
		/// </summary>
		/// <param name="calendarDaySpecifier">The calendar day specifier.</param>
		/// <returns>The short day string.</returns>
		public static string GetDayShortString(this CalendarDaySpecifier calendarDaySpecifier) {
			return calendarDaySpecifier.DayOfWeek.ToString().Substring(0, 3).ToUpper();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Encapsulates extension for CalendarDaySpecifier.
	/// </summary>
	public static class CalendarEventExtensions {

		private const string Any = "*";
		private const string None = "?";


		/// <summary>
		/// Creates a cron expression string based on the calendarEvent and the RecurrencePattern
		/// </summary>
		/// <param name="calendarEvent">The calendar event.</param>
		/// <returns>A cron expression string</returns>
		public static string ToCron(this CalendarEvent calendarEvent) {
			var currentMinute = calendarEvent.StartDate.Value.Minute.ToString();
			var currentHour = calendarEvent.StartDate.Value.Hour.ToString();
			var currentYear = calendarEvent.StartDate.Value.Year.ToString();

			var recurrencePattern = calendarEvent.RecurrencePattern;
			CronRecurrencePattern cronRecurrencePattern = null;
			if (recurrencePattern.Frequency == CalendarFrequencyType.Minutely) {
				string minuteRecurrency = recurrencePattern.Interval.ToString();
				cronRecurrencePattern = new CronRecurrencePattern(currentMinute, Any, Any, Any, None, minuteRecurrency, "", "", "");
			}

			if (recurrencePattern.Frequency == CalendarFrequencyType.Hourly) {
				string hourRecurrency = recurrencePattern.Interval.ToString();
				cronRecurrencePattern = new CronRecurrencePattern(currentMinute, currentHour, Any, Any, None, "", hourRecurrency, "", "");
			}

			if (recurrencePattern.Frequency == CalendarFrequencyType.Weekly) {
				var dayString = BuildDayString(recurrencePattern.ByDay);
				cronRecurrencePattern = new CronRecurrencePattern(currentMinute, currentHour, None, Any, dayString, "", "", "", "");
			}

			if (recurrencePattern.Frequency == CalendarFrequencyType.Monthly) {

				var months = String.Join(",", recurrencePattern.ByMonth);
				//var months =recurrencePattern.ByMonth.ToCSV();
				if (recurrencePattern.BySetPosition != null) {
					//occuranceInMonth - first/second/third..last/second to last/third to last... in the month
					var occuranceInMonth = GetOccuranceInMonth(recurrencePattern);
					var day = recurrencePattern.ByDay.FirstOrDefault();
					if (day != null) {
						var dayString = day.GetDayShortString();
						cronRecurrencePattern = new CronRecurrencePattern(currentMinute, currentHour, None, months, dayString, "", "", "", occuranceInMonth);
					}
				}
				if (recurrencePattern.ByMonthDay != null) //You shouldnt have both ByMonthDay and BySetPosition != null
				{
					var dayInMonth = recurrencePattern.ByMonthDay.FirstOrDefault().ToString();
					cronRecurrencePattern = new CronRecurrencePattern(currentMinute, currentHour, dayInMonth, months, None, "", "", "", "");
				}
			}
			if (recurrencePattern.Frequency == CalendarFrequencyType.Yearly) {

				var yearRecurrence = recurrencePattern.Interval.ToString();
				if ((recurrencePattern.ByDay != null) && (recurrencePattern.ByMonth != null) && (recurrencePattern.BySetPosition != null)) {
					string dayString = recurrencePattern.ByDay.FirstOrDefault().GetDayShortString();
					string monthString = recurrencePattern.ByMonth.FirstOrDefault().ToString();
					var occuranceInMonth = GetOccuranceInMonth(recurrencePattern);
					cronRecurrencePattern = new CronRecurrencePattern("0", currentMinute, currentHour, None, monthString, dayString, currentYear, "", "", yearRecurrence, occuranceInMonth);
				}

				if ((recurrencePattern.ByMonthDay != null) && (recurrencePattern.ByMonth != null)) {
					string monthDayString = recurrencePattern.ByMonthDay.FirstOrDefault().ToString();
					string monthString = recurrencePattern.ByMonth.FirstOrDefault().ToString();
					cronRecurrencePattern = new CronRecurrencePattern("0", currentMinute, currentHour, monthDayString, monthString, None, currentYear, "", "", yearRecurrence, null);
				}
			}

			if (cronRecurrencePattern == null)
				throw new InvalidOperationException("Invalid calendar recurrence pattern - can not convert to cron expression");
			return cronRecurrencePattern.ToString();
		}

		/// <summary>
		/// Gets the month occurance in number form. A number between 5 and -5, 1 is first, -1 is last, etc
		/// </summary>
		/// <param name="recurrencePattern">The recurrence Pattern.</param>
		/// <returns></returns>
		private static string GetOccuranceInMonth(CalendarRecurrencePattern recurrencePattern) {
			int occuranceInMonth = recurrencePattern.BySetPosition.FirstOrDefault();
			if (occuranceInMonth < 0) {
				occuranceInMonth = 6 + occuranceInMonth; //occurance is between 5 and -5
			}
			return occuranceInMonth.ToString();
		}

		/// <summary>
		/// Builds the day string.  Comma seperated short days.
		/// </summary>
		/// <param name="days">The days.</param>
		/// <returns></returns>
		public static string BuildDayString(IList<CalendarDaySpecifier> days) {
			StringBuilder sb = new StringBuilder();

			foreach (var day in days) {
				sb.Append(day.GetDayShortString());
				sb.Append(",");
			}

			sb.Remove(sb.Length - 1, 1);
			var dayString = sb.ToString();
			return dayString;
		}

		



	}
}

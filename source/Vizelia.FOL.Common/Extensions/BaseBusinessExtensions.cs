﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Encapsulates extension for BaseBusinessEntity class.
	/// </summary>
	public static class BaseBusinessExtensions {

		/// <summary>
		/// Returns the input typed as a data table.
		/// </summary>
		/// <param name="results">The results.</param>
		/// <returns></returns>
		public static DataTable AsDataTable(this List<PsetAttribute> results) {
			DataTable dataTable = new DataTable("PsetAttribute");
			// DataTable is disposable so we have to use try / finally to dispose the object in case of an exception (CA2000)
			try {
				//we create column names as per the type in DB 
				dataTable.Columns.Add("AttributeName", typeof(string));
				dataTable.Columns.Add("AttributeValue", typeof(string));
				//and fill in some values 
				foreach (var result in results) {
					dataTable.Rows.Add(result.Key, result.Value);
				}
				return dataTable;
			}
			finally {
				dataTable.Dispose();
			}
		}

		/// <summary>
		/// Returns the input typed as a data table.
		/// </summary>
		/// <param name="results">The results.</param>
		/// <returns></returns>
		public static DataTable AsDataTable(this Dictionary<string, string> results) {
			DataTable dataTable = new DataTable("PsetAttribute");
			// DataTable is disposable so we have to use try / finally to dispose the object in case of an exception (CA2000)
			try {
				//we create column names as per the type in DB 
				dataTable.Columns.Add("AttributeName", typeof(string));
				dataTable.Columns.Add("AttributeValue", typeof(string));
				//and fill in some values 
				foreach (var result in results) {
					dataTable.Rows.Add(result.Key, result.Value);
				}
				return dataTable;
			}
			finally {
				dataTable.Dispose();
			}
		}

		/// <summary>
		/// Builds the SQL update batch instruction.
		/// </summary>
		/// <param name="obj">The obj.</param>
		/// <returns></returns>
		public static List<string> BuildSQLUpdateBatchInstruction(this BaseEntity obj) {
			List<string> updatelist = new List<string>();
			List<string> properties = Helper.GetUpdatableBatchProperties(obj.GetType());
			foreach (string property in properties) {
				string value = obj.GetPropertyValueString(property, false, false);
				//we need to change the default behavior because MeterData Validity is stored as a tinyint.
				if (obj.GetType() == typeof(MeterData) && property == "Validity")
					value = ((MeterData)obj).Validity.ParseAsInt().ToString(CultureInfo.InvariantCulture);

				if (value != null) {
					updatelist.Add(property + " = '" + value.Replace("'", "''") + "'");
				}
			}
			return updatelist;
		}

		/// <summary>
		/// Returns true if a given string contains any of the characters provided in a params array of strings.
		/// </summary>
		/// <param name="str">the string</param>
		/// <param name="values">the array of strings</param>
		/// <returns></returns>
		public static bool ContainsAny(this string str, params string[] values) {
			if (!string.IsNullOrEmpty(str) || values.Length == 0) {
				return values.Any(value => str.Contains(value));
			}

			return false;
		}

		/// <summary>
		/// Filters a query with both paging.filter + paging.query and paging.filters.
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <param name="query"></param>
		/// <param name="paging"></param>
		/// <returns></returns>
		public static IQueryable<TSource> Filter<TSource>(this IQueryable<TSource> query, PagingParameter paging) where TSource : BaseEntity {
			if (paging != null && !String.IsNullOrEmpty(paging.query)) {
				query = query.Where(p => p.GetPropertyValue(paging.filter).ToString().ToLower().Contains(paging.query.ToLower(CultureInfo.CurrentCulture)));
			}
			if (paging != null && paging.filters != null) {
				query = query.SearchGeneric(paging.filters);
			}
			return query;
		}

		/// <summary>
		/// A helper function used in linq query to get the property value when the property name is passed as a string.
		/// The property can use '.' notation to access inner fields down the hierarchy of the object.
		/// </summary>
		/// <param name="obj">The object for which we want to evaluate the property.</param>
		/// <param name="property">The name of the property.</param>
		/// <returns>the value of the property.</returns>
		public static object GetPropertyValue(this BaseEntity obj, string property) {
			object retVal = obj;
			var properties = property.Split('.');
			var type = obj.GetType();
			foreach (var prop in properties) {
				PropertyInfo propertyInfo = type.GetProperty(prop);
				if (propertyInfo != null) {
					type = propertyInfo.PropertyType;
					retVal = propertyInfo.GetValue(retVal, null);
				}
				else {
					var fieldInfo = type.GetField(prop);
					type = fieldInfo.FieldType;
					retVal = fieldInfo.GetValue(retVal);
				}
			}
			return retVal;
			/*
			//try {
			PropertyInfo propertyInfo = null;
			var properties = property.Split('.');
			var type = obj.GetType();
			foreach (var prop in properties) {
				propertyInfo = type.GetProperty(prop);
				type = propertyInfo.GetType();
			}
			//PropertyInfo propertyInfo = obj.GetType().GetProperty(property);
			return propertyInfo != null ? propertyInfo.GetValue(obj, null) : null;
			//}
			//catch { return null; }
			*/
		}

		/// <summary>
		/// A helper function used in linq query to get the property value of a string when the property name is passed as a string.
		/// </summary>
		/// <param name="obj">The object for which we want to evaluate the property.</param>
		/// <param name="property">The name of the property.</param>
		/// <param name="transformNullToBlank">True if result should returned as blank when null, false otherwise.</param>
		/// <param name="lowerCase">True if result should be returned in lower case, false otherwise.</param>
		/// <returns>the value of the property.</returns>
		public static string GetPropertyValueString(this BaseEntity obj, string property, bool transformNullToBlank, bool lowerCase) {
			var value = obj.GetPropertyValue(property);
			string result = null;
			if (value != null)
				result = value.ToString();
			if (result == null) {
				return transformNullToBlank ? "" : null;
			}
			return lowerCase ? result.ToLower() : result;
		}

		/// <summary>
		/// Gets the key of the entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public static string GetKey(this BaseEntity item) {
			Type itemType = item.GetType();
			string keyName = Helper.GetPropertyNameId(itemType);
			object value = item.GetPropertyValue(keyName);

			string key = value as string;

			if (key == null) {
				// Try it as an int.
				if (value is int) {
					key = ((int)(value)).ToString(CultureInfo.InvariantCulture);
				}
			}

			return key;
		}

		/// <summary>
		/// Sets the key of the entity.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="key">The key.</param>
		public static void SetKey(this BaseEntity item, string key) {
			Type itemType = item.GetType();
			string keyName = Helper.GetPropertyNameId(itemType);
			item.SetPropertyValue(keyName, key);
		}

		/// <summary>
		/// Returns a populated List of TreeNodes from a List of TSource entities. The default order is to sort nodes by text property.
		/// </summary>
		/// <param name="items"></param>
		/// <returns></returns>
		public static List<TreeNode> GetTree<TSource>(this List<TSource> items) where TSource : BaseEntity, ITreeNode {
			var query = from p in items
						select p.GetTree();
			return query.ToList();
		}

		/// <summary>
		/// Returns a populated List of TreeNodes from a List of TSource entities. The default order is to sort nodes by text property.
		/// </summary>
		/// <param name="items"></param>
		/// <param name="ignoreIds">True to ignore ids, false otherwise.</param>
		/// <returns></returns>
		public static List<TreeNode> GetTree<TSource>(this List<TSource> items, bool ignoreIds) where TSource : BaseEntity, ITreeNode {
			List<TreeNode> result = GetTree(items);
			if (ignoreIds)
				result = result.Select(p => new TreeNode {
					children = p.children,
					entity = p.entity,
					icon = p.icon,
					iconCls = p.iconCls,
					leaf = p.leaf,
					qtip = p.qtip,
					text = p.text
				}).ToList();
			return result;
		}

		/// <summary>
		/// Nulls to blank.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <returns></returns>
		private static string NullToBlank(this string input) {
			return String.IsNullOrEmpty(input) ? "" : input;
		}

		/// <summary>
		/// Returns a paged and ordered sub part of a list.
		/// </summary>
		/// <typeparam name="TSource">The type of the element in the list.</typeparam>
		/// <param name="results">The list of elements to rank.</param>
		/// <param name="sort">The name of the column to sort. If sort is null, no sort will be applied.</param>
		/// <param name="direction">the direction (ASC or DESC) to sort the column.</param>
		/// <param name="start">The number of the first record to start with.</param>
		/// <param name="limit">The number of records to return.</param>
		/// <returns></returns>
		public static List<TSource> Rank<TSource>(this List<TSource> results, string sort, string direction, int start, int limit) where TSource : BaseEntity {
			var query = results.AsQueryable();
			return query.Rank(sort, direction, start, limit);
		}

		/// <summary>
		/// Returns a paged and ordered sub part of a list.
		/// </summary>
		/// <typeparam name="TSource">The type of the element in the list.</typeparam>
		/// <param name="results">The list of elements to rank.</param>
		/// <param name="sort">The name of the column to sort. If sort is null, no sort will be applied.</param>
		/// <param name="direction">the direction (ASC or DESC) to sort the column.</param>
		/// <param name="start">The number of the first record to start with.</param>
		/// <param name="limit">The number of records to return.</param>
		/// <returns></returns>
		private static List<TSource> Rank<TSource>(this IQueryable<TSource> results, string sort, string direction, int start, int limit) where TSource : BaseEntity {
			var query = results;
			List<TSource> retVal = results.ToList();
			//We need to try-catch this code because if the sort column is not part of the entity (happens because of user prefrence)
			//an exception is thrown.
			try {

				// we start by ordering the resultset, if asked
				if (!String.IsNullOrEmpty(sort)) {
					// we accept multi column sorting
					var sortColumns = sort.Split(',').Select(p => p.Trim()).ToArray();
					switch (direction.ToUpper()) {
						case "ASC":
							query = query.OrderBy(p => p.GetPropertyValue(sortColumns[0]));
							break;
						case "DESC":
							query = query.OrderByDescending(p => p.GetPropertyValue(sortColumns[0]));
							break;
					}
					for (int i = 1; i < sortColumns.Length; i++) {
						switch (direction.ToUpper()) {
							case "ASC":
								int i1 = i;
								query = ((IOrderedQueryable<TSource>)query).ThenBy(p => p.GetPropertyValue(sortColumns[i1]));
								break;
							case "DESC":
								int i2 = i;
								query = ((IOrderedQueryable<TSource>)query).ThenByDescending(p => p.GetPropertyValue(sortColumns[i2]));
								break;
						}

					}
					retVal = query.ToList();
				}

			}
			catch (Exception) {
				// in case the sorting didn't work, we try to continue with paging

			}
			// we then filter the resultset
			var query2 = retVal.Skip(start);
			if (limit > 0)
				query2 = query2.Take(limit);
			return query2.ToList();
		}

		/// <summary>
		/// Returns a paged and ordered sub part of a list as a json store.
		/// </summary>
		/// <typeparam name="TSource">The type of the element in the list.</typeparam>
		/// <param name="results">The list of elements to rank.</param>
		/// <param name="sort">The name of the column to sort. If sort is null, no sort will be applied.</param>
		/// <param name="direction">the direction (ASC or DESC) to sort the column.</param>
		/// <param name="start">The number of the first record to start with.</param>
		/// <param name="limit">The number of records to return.</param>
		/// <returns></returns>
		public static JsonStore<TSource> RankToJson<TSource>(this List<TSource> results, string sort, string direction, int start, int limit) where TSource : BaseEntity {
			JsonStore<TSource> store = results.Rank(sort, direction, start, limit).ToJsonStore();
			store.recordCount = results.Count;
			if (!String.IsNullOrEmpty(sort)) {
				store.metaData.sortInfo = new JsonSortInfo { direction = direction, field = sort };
			}

			return store;
		}

		/// <summary>
		/// Generic Search function. 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="query"></param>
		/// <param name="filters"></param>
		/// <returns></returns>
		public static IQueryable<T> SearchGeneric<T>(this IQueryable<T> query, List<GridFilter> filters) where T : BaseEntity {
			// we ignore filters with no value.
			var queryFilter = from p in filters
							  where !String.IsNullOrEmpty(p.data.value)
							  select p;


			foreach (GridFilter filter in queryFilter) {
				// The temporary variable in the loop is required to avoid the outer variable trap, 
				// where the same variable is captured for each iteration of the foreach loop.
				// for more info see : http://www.albahari.com/nutshell/predicatebuilder.aspx
				string value = filter.data.value;
				string field = filter.field;
				var property = typeof(T).GetProperty(field);
				switch (filter.data.type) {

					case "string":
						switch (filter.data.comparison) {
							case "start":
								query = query.Where(p => p.GetPropertyValueString(field, true, true).StartsWith(value.ToLower()));
								break;

							case "end":
								query = query.Where(p => p.GetPropertyValueString(field, true, true).EndsWith(value.ToLower()));
								break;

							case "eq":
								query = query.Where(p => p.GetPropertyValueString(field, true, true) == value.ToLower());
								break;

							case "ne":
								query = query.Where(p => p.GetPropertyValueString(field, true, true) != value.ToLower());
								break;

							default:
								//if the property is an enum we check for the complete value, not for contains.
								if (property != null && property.PropertyType.BaseType != null && property.PropertyType.BaseType.Name.Equals("Enum")) {
									query = query.Where(p => p.GetPropertyValueString(field, true, true) == value.ToLower());
								}
								else {
									query = query.Where(p => p.GetPropertyValueString(field, true, true).Contains(value.ToLower()));
								}
								break;
						}
						break;

					case "boolean":
						query = query.Where(p => ((bool)p.GetPropertyValue(field)) == bool.Parse(value));
						break;

					case "date":
						var dateValue = (DateTime.Parse(value)).ToUniversalTime();
						var endValue = dateValue.AddHours(24);
						switch (filter.data.comparison) {
							case "lt":
								query = query.Where(p => ((DateTime)p.GetPropertyValue(field)) <= dateValue);
								break;
							case "gt":
								query = query.Where(p => ((DateTime)p.GetPropertyValue(field)) >= dateValue);
								break;
							case "ne": {
									Func<T, bool> whereDelegate = (p => {
										var d = ((DateTime)p.GetPropertyValue(field));
										return d < dateValue || d >= endValue;
									});
									query = query.Where(p => whereDelegate(p));
									//query = query.Where(p => ((DateTime)p.GetPropertyValue(field)).Date != dateValue.Date);
								}
								break;
							case "eq":
							default: {
									Func<T, bool> whereDelegate = (p => {
										var d = ((DateTime)p.GetPropertyValue(field));
										return d >= dateValue && d < endValue;
									});
									query = query.Where(p => whereDelegate(p));
									//query = query.Where(p => (Convert.ChangeType(p.GetPropertyValue(filter.field), type) == (Convert.ChangeType(filter.data.value, type))));
								}
								break;
						}
						break;

					case "numeric":
						var dblValue = double.Parse(value, CultureInfo.InvariantCulture);
						switch (filter.data.comparison) {
							case "eq":
								query = query.Where(p => ((double)Convert.ChangeType(p.GetPropertyValue(field), typeof(double)) == (dblValue)));
								break;
							case "ne":
								query = query.Where(p => ((double)Convert.ChangeType(p.GetPropertyValue(field), typeof(double)) != (dblValue)));
								break;
							case "lt":
								query = query.Where(p => ((double)Convert.ChangeType(p.GetPropertyValue(field), typeof(double)) <= (dblValue)));
								break;
							case "gt":
								query = query.Where(p => ((double)Convert.ChangeType(p.GetPropertyValue(field), typeof(double)) >= (dblValue)));
								break;

							default:
								query = query.Where(p => ((double)Convert.ChangeType(p.GetPropertyValue(field), typeof(double)) == (dblValue)));
								//query = query.Where(p => (Convert.ChangeType(p.GetPropertyValue(filter.field), type) == (Convert.ChangeType(filter.data.value, type))));

								break;
						}
						break;

					case "list":
						var list = filter.data.value.Split(',');
						if (list.Any()) {
							query = from p in query
									where list.Contains(p.GetPropertyValue(field))
									select p;
						}
						break;

				}
				// former code when we needed to expand result after each filter. This is not required anymore because of the declaration of local variable in the loop.
				// query = query.ToList().Select(p => p);
			}
			return query;

		}

		/// <summary>
		/// Generic Search function.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="records">The records.</param>
		/// <param name="filters">The filters.</param>
		/// <returns></returns>
		public static IEnumerable<T> SearchGeneric<T>(this List<T> records, List<GridFilter> filters) where T : BaseEntity {
			var query = records.AsQueryable(); // from r in records select r;
			return query.SearchGeneric(filters);
		}

		/// <summary>
		/// A helper function to set the property value of an object.
		/// </summary>
		/// <param name="obj">The object for which we want to set the property value.</param>
		/// <param name="property">The name of the property.</param>
		/// <param name="value">The value of the property.</param>
		public static void SetPropertyValue(this BaseEntity obj, string property, object value) {
			var propertyInfo = obj.GetType().GetProperty(property);
			propertyInfo.SetValue(obj, value, null);
		}

		/// <summary>
		/// Returns a json form response encapsulating a business entity.
		/// </summary>
		/// <typeparam name="TSource">The type of the business entity.</typeparam>
		/// <param name="item">The business entity.</param>
		/// <returns>The json form response.</returns>
		public static FormResponse ToFormResponse<TSource>(this TSource item) where TSource : BaseEntity {
			return new FormResponse {
				data = item,
				success = true
			};
		}

		/// <summary>
		/// Transforms a audit list into a json store.
		/// </summary>
		/// <typeparam name="TSource">The type of the source.</typeparam>
		/// <param name="results">The results.</param>
		/// <param name="recordCount">The record count.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="columnDefinitionList">The column definition list.</param>
		/// <returns></returns>
		public static JsonStore<TSource> ToAuditJsonStore<TSource>(this List<TSource> results, int recordCount, PagingParameter paging, List<KeyValuePair<string, string>> columnDefinitionList) where TSource : BaseEntity {
			JsonStore<TSource> retVal = results.ToJsonStore(recordCount, paging);
			// we have a concrete business entity that derrived from BaseEntity and we need to create it fields manually.
			retVal.metaData.CreateFields(columnDefinitionList, "Entity.");
			return retVal;
		}

		/// <summary>
		/// Transforms a list into a json store.
		/// </summary>
		/// <typeparam name="TSource">The type of element in the list.</typeparam>
		/// <param name="results">The list of elements to transform to a json store.</param>
		/// <returns>The json store.</returns>
		public static JsonStore<TSource> ToJsonStore<TSource>(this List<TSource> results) where TSource : BaseEntity {
			return new JsonStore<TSource> {
				recordCount = results.Count,
				records = results,
				metaData = new JsonMetaData<TSource>()
			};
		}

		/// <summary>
		/// Transforms a list into a json store.
		/// </summary>
		/// <typeparam name="TSource">The type of element in the list.</typeparam>
		/// <param name="results">The list of elements to transform to a json store.</param>
		/// <param name="recordCount">forces the recordCount of the store.</param>
		/// <returns>The json store.</returns>
		public static JsonStore<TSource> ToJsonStore<TSource>(this List<TSource> results, int recordCount) where TSource : BaseEntity {
			JsonStore<TSource> store = results.ToJsonStore();
			store.recordCount = recordCount;
			return store;
		}

		/// <summary>
		/// Transforms a list into a json store.
		/// </summary>
		/// <typeparam name="TSource">The type of element in the list.</typeparam>
		/// <param name="results">The list of elements to transform to a json store.</param>
		/// <param name="recordCount">forces the recordCount of the store.</param>
		/// <param name="paging">The paging information so sortInfo can be return.</param>
		/// <returns>The json store.</returns>
		public static JsonStore<TSource> ToJsonStore<TSource>(this List<TSource> results, int recordCount, PagingParameter paging) where TSource : BaseEntity {
			JsonStore<TSource> store = results.ToJsonStore(recordCount);
			if (paging != null) {
				if (!String.IsNullOrEmpty(paging.sort)) {
					store.metaData.sortInfo = new JsonSortInfo { direction = paging.dir, field = paging.sort };
				}
			}
			return store;
		}

		/// <summary>
		/// Transforms a list in a json store with paging and filter parameters.
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <param name="results"></param>
		/// <param name="paging"></param>
		/// <returns></returns>
		public static JsonStore<TSource> ToJsonStoreWithFilter<TSource>(this List<TSource> results, PagingParameter paging) where TSource : BaseEntity {
			JsonStore<TSource> store;
			var query = results.AsQueryable();
			query = query.Filter(paging);
			/*
			if (paging != null && !String.IsNullOrEmpty(paging.query)) {
				query = query.Where(p => p.GetPropertyValue(paging.filter).ToString().StartsWith(paging.query));
			}
			if (paging != null && paging.filters != null) {
				query = query.SearchGeneric(paging.filters);
			}
			*/
			int count = query.Count();
			if (paging != null)
				store = query.ToList().RankToJson(paging.sort, paging.dir, paging.start, paging.limit);
			else
				store = query.ToList().ToJsonStore();
			store.recordCount = count;
			return store;
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Encapsulates extension for the PortalTab class.
	/// </summary>
	public static class PortalTabExtensions {


		/// <summary>
		/// Return a list of rectangle and chart in order to generate a picture that represents the entire PortalTab.
		/// </summary>
		/// <param name="portalTab">the PortalTab.</param>
		/// <param name="width">the width of the final image.</param>
		/// <param name="height">the height of the final image.</param>
		/// <returns></returns>
		public static Dictionary<Chart, Rectangle> GetLayout(this PortalTab portalTab, int width, int height) {

			Dictionary<Chart, Rectangle> retVal = new Dictionary<Chart, Rectangle>();

			int currentX = 0;
			foreach (var column in portalTab.Columns) {

				var columnWidth = width * column.Flex / portalTab.TotalFlex;
				int currentY = 0;

				foreach (var portlet in column.Portlets) {
					var portletHeight = height * portlet.Flex / column.TotalFlex;
					if (portlet.Entity != null && portlet.Entity.GetType() == typeof(Chart)) {
						Rectangle rect = new Rectangle(currentX, currentY, columnWidth, portletHeight);
						retVal.Add((Chart)portlet.Entity, rect);
					}
					currentY += portletHeight;
				}
				currentX += columnWidth;
			}
			return retVal;
		}

		/// <summary>
		/// Return a list charts contained in the PortalTab.
		/// </summary>
		/// <param name="portalTab">the PortalTab.</param>
		/// <returns></returns>
		public static List<Chart> GetCharts(this PortalTab portalTab) {
			var retVal = new List<Chart>();
			if (portalTab != null && portalTab.Columns != null) {
				var portalColumns = portalTab.Columns.Where(column => column != null && column.Portlets != null);
				foreach (var column in portalColumns) {
					retVal.AddRange(column.Portlets.Select(portlet => portlet.Entity).OfType<Chart>());
				}
			}
			return retVal;
		}

		/// <summary>
		/// Return a list charts contained in the PortalWindow.
		/// </summary>
		/// <param name="portalWindow">the PortalWindow.</param>
		/// <returns></returns>
		public static List<Chart> GetCharts(this PortalWindow portalWindow) {
			var retVal = new List<Chart>();
			if (portalWindow != null && portalWindow.Tabs != null) {
				foreach (var portalTab in portalWindow.Tabs) {
					retVal.AddRange(portalTab.GetCharts());
				}
			}
			return retVal;
		}

		/// <summary>
		/// Return a list of  portlet content contained in the PortalWindow.
		/// </summary>
		/// <param name="portalWindow">the PortalWindow.</param>
		/// <returns></returns>
		public static List<BaseBusinessEntity> GetPortletsContent(this PortalWindow portalWindow) {
			var retVal = new List<BaseBusinessEntity>();
			if (portalWindow != null && portalWindow.Tabs != null) {
				foreach (var portalTab in portalWindow.Tabs) {
					retVal.AddRange(portalTab.GetPortletsContent());
				}
			}
			return retVal;
		}

		/// <summary>
		/// Return a list of portlet content contained in the PortalTab.
		/// </summary>
		/// <param name="portalTab">the PortalTab.</param>
		/// <returns></returns>
		public static List<BaseBusinessEntity> GetPortletsContent(this PortalTab portalTab) {
			var retVal = new List<BaseBusinessEntity>();
			if (portalTab != null && portalTab.Columns != null) {
				var portalColumns = portalTab.Columns.Where(column => column != null && column.Portlets != null);
				foreach (var column in portalColumns) {
					retVal.AddRange(column.Portlets.Where(portlet => portlet.Entity is IPortlet).Select(portlet => portlet.Entity));
				}
			}
			return retVal;
		}


		/// <summary>
		/// Return a list of portlets contained in the PortalWindow.
		/// </summary>
		/// <param name="portalWindow">the PortalWindow.</param>
		/// <returns></returns>
		public static List<Portlet> GetPortlets(this PortalWindow portalWindow) {
			var retVal = new List<Portlet>();
			if (portalWindow != null && portalWindow.Tabs != null) {
				foreach (var portalTab in portalWindow.Tabs) {
					retVal.AddRange(portalTab.GetPortlets());
				}
			}
			return retVal;
		}

		/// <summary>
		/// Return a list Portlet contained in the PortalTab.
		/// </summary>
		/// <param name="portalTab">the PortalTab.</param>
		/// <returns></returns>
		public static List<Portlet> GetPortlets(this PortalTab portalTab) {
			var retVal = new List<Portlet>();
			if (portalTab != null && portalTab.Columns != null) {
				foreach (var column in portalTab.Columns.Where(x => x.Portlets != null)) {
					retVal.AddRange(column.Portlets);
				}
			}
			return retVal;
		}

	}
}

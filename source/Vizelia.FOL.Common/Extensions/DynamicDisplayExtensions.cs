﻿
using System.Drawing;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Encapsulates extension for the DynamicDisplay and DynamicDisplayColorTemplate classes.
	/// </summary>
	public static class DynamicDisplayExtensions {

		/// <summary>
		/// Return the StartColor.
		/// </summary>
		/// <param name="template"></param>
		/// <returns></returns>
		public static System.Drawing.Color GetStartColor(this DynamicDisplayColorTemplate template) {
			Color startColor;
			if (!template.UsePredefinedColor) {
				startColor = Color.FromArgb(template.StartColorR,template.StartColorG, template.StartColorB);
			}
			else {
				var color = System.Drawing.Color.FromName(template.Name);
				startColor = Color.FromArgb(color.A, color.R, color.G, color.B);
			}
			return startColor;
		}

		/// <summary>
		/// Return the EndColor.
		/// </summary>
		/// <param name="template"></param>
		/// <returns></returns>
		public static System.Drawing.Color GetEndColor(this DynamicDisplayColorTemplate template) {
			Color endColor;
			if (!template.UsePredefinedColor) {
				endColor = Color.FromArgb(template.EndColorR, template.EndColorG, template.EndColorB);
			}
			else {
				var color = System.Drawing.Color.FromName(template.Name);
				var darkColor = color.Lerp(System.Drawing.Color.White, 0.5f);
				endColor = Color.FromArgb(color.A, darkColor.R, darkColor.G, darkColor.B);
			}
			return endColor;
		}

		/// <summary>
		/// Return the BorderColor.
		/// </summary>
		/// <param name="template"></param>
		/// <returns></returns>
		public static System.Drawing.Color GetBorderColor(this DynamicDisplayColorTemplate template) {
			Color borderColor;
			if (!template.UsePredefinedColor) {
				borderColor = Color.FromArgb(template.BorderColorR, template.BorderColorG, template.BorderColorB);
			}
			else {
				var color = System.Drawing.Color.FromName(template.Name);
				var lightColor = color.Lerp(System.Drawing.Color.Black, 0.25f);
				borderColor = Color.FromArgb(color.A, lightColor.R, lightColor.G, lightColor.B);
			}
			return borderColor;
		}



		/// <summary>
		/// Return the  DynamicDisplayColorTemplate from a DynamicDisplay.
		/// </summary>
		/// <returns></returns>
		public static DynamicDisplayColorTemplate GetColorTemplate(this DynamicDisplay display) {
			DynamicDisplayColorTemplate retVal = null;
			retVal = display.IsCustomColorTemplate ? display.CustomColorTemplate : new DynamicDisplayColorTemplate { Name = display.ColorTemplate, UsePredefinedColor = true };
			return retVal;
		}

		/// <summary>
		/// Returns if the Footer will be displayed
		/// </summary>
		/// <param name="display">The display.</param>
		/// <returns></returns>
		public static bool IsDisplayFooter(this DynamicDisplay display) {
			//return display.FooterVisible && IsDisplayFooterImage(display);
			return display.FooterVisible;
		}

		/// <summary>
		/// Returns if the Header will be displayed
		/// </summary>
		/// <param name="display">The display.</param>
		/// <returns></returns>
		public static bool IsDisplayHeader(this  DynamicDisplay display) {
			//return display.HeaderVisible && IsDisplayHeaderImage(display);
			return display.HeaderVisible;
		}

		/// <summary>
		/// Returns if the Footer image will be displayed
		/// </summary>
		/// <param name="display">The display.</param>
		/// <returns></returns>
		public static bool IsDisplayFooterImage(this  DynamicDisplay display) {
			return display.FooterPictureImage != null && display.FooterPictureImage.Image != null;
		}

		/// <summary>
		/// Returns if the Main Image will be displayed
		/// </summary>
		/// <param name="display">The display.</param>
		/// <returns></returns>
		public static bool IsDisplayMainImage(this  DynamicDisplay display) {
			return display.MainPictureImage != null && display.MainPictureImage.Image != null && display.MainPictureSize.HasValue && display.MainPictureSize.Value > 0;
		}

		/// <summary>
		/// Returns if the Header Image will be displayed
		/// </summary>
		/// <param name="display">The display.</param>
		/// <returns></returns>
		public static bool IsDisplayHeaderImage(this  DynamicDisplay display) {
			return display.HeaderPictureImage != null && display.HeaderPictureImage.Image != null;
		}
	}
}

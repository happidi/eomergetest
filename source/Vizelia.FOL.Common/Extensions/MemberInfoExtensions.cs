﻿using System;
using System.Linq;
using System.Reflection;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Encapsulates extensions for the MemberInfo class.
	/// </summary>
	public static class MemberInfoExtensions {
		private const string const_member_does_not_have_get_set_methods = "member is not a PropertyInfo or a FieldInfo";

		/// <summary>
		/// Sets the given value to the member of the given object.
		/// </summary>
		/// <param name="member">The member.</param>
		/// <param name="objectToSetTo">The object to set the value to its member.</param>
		/// <param name="value">The value.</param>
		public static void SetValue(this MemberInfo member, object objectToSetTo, object value) {
			var property = member as PropertyInfo;
			if (property != null) {
				property.SetValue(objectToSetTo, value, null);
				return;
			}

			var field = member as FieldInfo;
			if (field != null) {
				field.SetValue(objectToSetTo, value);
				return;
			}

			// If we got here, it means the MemberInfo does not contain a value to set.
			throw new ArgumentException(const_member_does_not_have_get_set_methods);
		}

		/// <summary>
		/// Gets the given value from the member of the given object.
		/// </summary>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="member">The member.</param>
		/// <param name="objectToGetFrom">The object to get the value from its member.</param>
		/// <returns></returns>
		public static TValue GetValue<TValue>(this MemberInfo member, object objectToGetFrom) {
			var property = member as PropertyInfo;
			if (property != null) {
				var returnedValue = (TValue)property.GetValue(objectToGetFrom, null);
				return returnedValue;
			}

			var field = member as FieldInfo;
			if (field != null) {
				var returnedValue = (TValue)field.GetValue(objectToGetFrom);
				return returnedValue;
			}

			// If we got here, it means the MemberInfo does not contain a value to get.
			throw new ArgumentException(const_member_does_not_have_get_set_methods);
		}

		/// <summary>
		/// Gets the attribute value.
		/// </summary>
		/// <typeparam name="T">The attribute used to return method information.</typeparam>
		/// <param name="member">The member.</param>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns></returns>
		public static string GetAttributeValue<T>(this MemberInfo member, string propertyName) where T : Attribute {
			return (string)GetAttributeValueAsObject<T>(member, propertyName);
		}

		/// <summary>
		/// Gets the attribute value as object.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="member">The member.</param>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns></returns>
		public static object GetAttributeValueAsObject<T>(this MemberInfo member, string propertyName) where T : Attribute {
			var att = (T)member.GetCustomAttributes(typeof(T), true).FirstOrDefault();
			return att == null ? null : att.GetType().GetProperty(propertyName).GetValue(att, null);
		}

		/// <summary>
		/// Determines whether the specified member has attribute.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="member">The member.</param>
		/// <returns>
		///   <c>true</c> if the specified member has attribute; otherwise, <c>false</c>.
		/// </returns>
		public static bool HasAttribute<T>(this MemberInfo member) where T : Attribute {
			return member.GetCustomAttributes(typeof(T), true).FirstOrDefault() != null;
		}
	}
}
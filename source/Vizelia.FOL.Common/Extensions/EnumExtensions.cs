﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Reflection;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// Encapsulates extension for Enums
	/// </summary>
	public static class EnumExtension {

		/// <summary>
		/// Converts a string to an enum.
		/// </summary>
		/// <typeparam name="T">The enum type.</typeparam>
		/// <param name="value">The value of the enum as a string.</param>
		/// <returns>The value returned as an enum.</returns>
		public static T ParseAsEnum<T>(this string value) {
			CheckEnum<T>();

			if (string.IsNullOrWhiteSpace(value)) {
				return default(T);
			}

			if (Enum.IsDefined(typeof(T), value)) {
				return (T)Enum.Parse(typeof(T), value);
			}

			throw new ArgumentException(String.Format("The value {0} could not be found in Enum {1}", value, typeof(T).FullName));
		}

		private static void CheckEnum<T>() {
			if (!typeof(T).IsEnum) {
				throw new NotSupportedException("T must be an Enum");
			}
		}

		/// <summary>
		/// Converts a int to an enum.
		/// </summary>
		/// <typeparam name="T">The enum type.</typeparam>
		/// <param name="value">The value of the enum as a int.</param>
		/// <returns>The value returned as an enum.</returns>
		public static T ParseAsEnum<T>(this int value) {
			CheckEnum<T>();
			return (T)(Enum.ToObject(typeof(T), value));
		}


		/// <summary>
		/// Gets the localized text.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static string GetLocalizedText(this Enum value) {
			var retVal = Helper.LocalizeText(EnumExtension.GetEnumValueLocalizedText(value.GetType(), value.ParseAsInt()));
			return retVal;
		}

		/// <summary>
		/// Convert an enum to a string.
		/// </summary>
		/// <param name="value">The enum value.</param>
		/// <returns>The enum as a string.</returns>
		public static string ParseAsString(this Enum value) {
			return value.ToString();
		}

		/// <summary>
		/// Parses an enum as an int.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static int ParseAsInt(this Enum value) {
			return ((int)((object)value));
		}

		/// <summary>
		/// Convets an enum type into a list of ListElement.
		/// </summary>
		/// <param name="type">The enum type.</param>
		/// <returns></returns>
		public static List<ListElement> GetListElement(Type type) {
			var retVal = GetListElementGeneric<string>(type);
			return retVal.Cast<ListElement>().ToList();
		}

		/// <summary>
		/// Convets an enum type into a list of ListElement with numeric values (useful for sorting on client side).
		/// </summary>
		/// <param name="type">The enum type.</param>
		/// <returns></returns>
		public static List<ListElementGeneric<int>> GetListElementNumeric(Type type) {
			var retVal = GetListElementGeneric<int>(type);
			return retVal;
		}

		private static List<ListElementGeneric<T>> GetListElementGeneric<T>(Type type) {
			string[] enumNames;
			string[] enumResources;
			string[] iconResources;
			Array enumValues;
			int[] enumOrder;

			EnumExtension.GetCustomEnumData(type, out enumNames, out enumResources, out enumValues, out iconResources,out enumOrder);


			var info = (from attribute in Attribute.GetCustomAttributes(type)
						where attribute is InformationAttribute
						select (InformationAttribute)attribute).FirstOrDefault();

			List<ListElementGeneric<T>> results = new List<ListElementGeneric<T>>();
			for (int i = 0; i < enumNames.Length; i++) {
				var el = new ListElementGeneric<T> {
					Id = enumNames[i],
					MsgCode = info != null ? Langue.ResourceManager.GetString(info.MsgCode + enumResources[i].ToLower()) : enumResources[i],
					IconCls = iconResources[i],
					Value = (T)enumValues.GetValue(i),
					Order = enumOrder[i]

				};
				results.Add(el);
			}
			return results;

		}

		/// <summary>
		/// Gets the IconCls for a specific enum value.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static string GetEnumValueIconCls(Type type, int value) {
			var retVal = "";
			List<ListElementGeneric<int>> list = GetListElementGeneric<int>(type);

			list.ForEach(el => {
				if (el.Value == value) {
					retVal = el.IconCls;
				}
			});
			return retVal;
		}

		/// <summary>
		/// Gets the IconCls for a specific enum value.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static string GetEnumValueLocalizedText(Type type, int value) {
			var retVal = "";
			List<ListElementGeneric<int>> list = GetListElementGeneric<int>(type);

			list.ForEach(el => {
				if (el.Value == value) {
					retVal = el.MsgCode;
				}
			});
			return retVal;
		}

		/// <summary>
		/// Gets the enums values with the resource name when exists.
		/// </summary>
		/// <param name="type">The enum type.</param>
		/// <param name="enumNames">The result array of enum names.</param>
		/// <param name="enumResources">The result array of enum resource names.</param>
		/// <param name="enumValues">The result array of enum values.</param>
		/// <param name="enumIcons">The result array of enum icons.</param>
		/// <param name="enumOrder">The enum order.</param>
		private static void GetCustomEnumData(Type type, out string[] enumNames, out string[] enumResources, out Array enumValues, out string[] enumIcons, out int[] enumOrder) {

			FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
			object[] objArray = new object[fields.Length];
			string[] strArray = new string[fields.Length];
			string[] resourcesArray = new string[fields.Length];
			string[] iconsArray = new string[fields.Length];
			int[] orderArray = new int[fields.Length];

			for (int i = 0; i < fields.Length; i++) {
				strArray[i] = fields[i].Name;
				objArray[i] = fields[i].GetRawConstantValue();
				var resourceNameAttribute = (LocalizedTextAttribute)fields[i].GetCustomAttributes(typeof(LocalizedTextAttribute), false).FirstOrDefault();
				if (resourceNameAttribute != null) {
					resourcesArray[i] = resourceNameAttribute.PropertyNameId;
				}
				else {
					resourcesArray[i] = fields[i].Name;
				}

				var iconNameAttribute = (IconClsAttribute)fields[i].GetCustomAttributes(typeof(IconClsAttribute), false).FirstOrDefault();
				if (iconNameAttribute != null) {
					iconsArray[i] = iconNameAttribute.PropertyNameId;
				}
				else {
					iconsArray[i] = "";
				}

				var orderAttribute = (OrderAttribute)fields[i].GetCustomAttributes(typeof(OrderAttribute), false).FirstOrDefault();
				if (orderAttribute != null) {
					orderArray[i] = orderAttribute.Order;
				}
				else {
					if (objArray[i] is int)
						orderArray[i] = (int)objArray[i] ;
					else {
						orderArray[i] = i;
					}
				}

			}
			IComparer comparer = Comparer.Default;
			for (int j = 1; j < objArray.Length; j++) {
				int index = j;
				string str = strArray[j];
				object y = objArray[j];
				string z = resourcesArray[j];
				string icon = iconsArray[j];
				int order = orderArray[j];

				bool flag = false;
				while (comparer.Compare(objArray[index - 1], y) > 0) {
					strArray[index] = strArray[index - 1];
					objArray[index] = objArray[index - 1];
					resourcesArray[index] = resourcesArray[index - 1];
					iconsArray[index] = iconsArray[index - 1];
					orderArray[index] = orderArray[index - 1];
					index--;
					flag = true;
					if (index == 0) {
						break;
					}
				}
				if (flag) {
					strArray[index] = str;
					objArray[index] = y;
					resourcesArray[index] = z;
					iconsArray[index] = icon;
					orderArray[index] = order;
				}
			}
			enumNames = strArray;
			enumValues = objArray;
			enumResources = resourcesArray;
			enumIcons = iconsArray;
			enumOrder = orderArray;
		}
	}


}

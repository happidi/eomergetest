﻿
using System.Drawing;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Encapsulates extension for the AlarmDefinition class.
	/// </summary>
	public static class AlarmDefinitionExtensions {

		/// <summary>
		/// Return the color of the AlarmDefinition.
		/// </summary>
		/// <param name="alarm">The alarm.</param>
		/// <returns></returns>
		public static System.Drawing.Color GetColor(this AlarmDefinition alarm) {
			return Color.FromArgb(alarm.ClassificationItemColorR, alarm.ClassificationItemColorG, alarm.ClassificationItemColorB);
		}
	}
}
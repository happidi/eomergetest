﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// Encapsulates extension for Stream class.
	/// </summary>
	public static class StreamExtensions {

		/// <summary>
		/// Shortcut to return the content of a stream to a string.
		/// </summary>
		/// <param name="stream">The stream to read.</param>
		/// <returns>The string content of the stream.</returns>
		public static string GetString(this Stream stream) {
			stream.Position = 0;
			using (StreamReader reader = new StreamReader(stream)) {
				return reader.ReadToEnd();
			}

		}

		/// <summary>
		/// Shortcut to convert a string to a stream.
		/// </summary>
		/// <param name="s">The input string.</param>
		/// <returns>The string converted as a stream.</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
		public static MemoryStream GetStream(this string s) {
			byte[] buf = s.GetBytes();
			// ReSharper disable UseObjectOrCollectionInitializer
			MemoryStream ms = new MemoryStream(buf);
			// ReSharper restore UseObjectOrCollectionInitializer
			ms.Position = 0;
			return ms;
		}

		/// <summary>
		/// Shortcut to convert a string to a byte array.
		/// </summary>
		/// <param name="s">The input string.</param>
		/// <returns></returns>
		public static byte[] GetBytes(this string s) {
			byte[] buf = Encoding.UTF8.GetBytes(s);
			return buf;
		}

		/// <summary>
		/// Shortcut to convert a byte array to a string.
		/// </summary>
		/// <param name="bytes">The bytes.</param>
		/// <returns></returns>
		public static string GetString(this byte[] bytes) {
			return Encoding.UTF8.GetString(bytes);
		}

		/// <summary>
		/// Shortcut to return the content of a stream as a byte array.
		/// </summary>
		/// <param name="stream">The stream to read.</param>
		/// <returns>The byte array.</returns>
		public static byte[] GetBytes(this Stream stream) {
			if (stream != null) {
				stream.Position = 0;
				byte[] buffer = new byte[stream.Length];
				stream.Read(buffer, 0, (int)stream.Length);
				return buffer;
			}
			return null;
		}

		/// <summary>
		/// Converts a byte array to a memory stream.
		/// </summary>
		/// <param name="content">The byte array.</param>
		/// <returns></returns>
		public static MemoryStream ToStream(this byte[] content) {
			if (content == null)
				return null;
			return new MemoryStream(content);
		}




		/// <summary>
		/// Deserializes the specified stream.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stream">The stream.</param>
		/// <returns></returns>
		public static T Deserialize<T>(this MemoryStream stream) where T : class {
			stream.Seek(0, SeekOrigin.Begin);
			BinaryFormatter bf = new BinaryFormatter();
			var retVal = bf.Deserialize(stream) as T;
			return retVal;
		}



		/// <summary>
		/// Deserializes the specified content.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="content">The content.</param>
		/// <returns></returns>
		public static T Deserialize<T>(this byte[] content) where T : class {
			MemoryStream stream = new MemoryStream(content);
			stream.Seek(0, SeekOrigin.Begin);
			BinaryFormatter bf = new BinaryFormatter();
			var retval = bf.Deserialize(stream) as T;
			stream.Close();
			return retval;
		}

		/// <summary>
		/// Gets the base64 string.
		/// </summary>
		/// <param name="stream">The stream.</param>
		/// <returns></returns>
		public static string GetBase64String(this MemoryStream stream) {
			byte[] bitmapBytes = stream.ToArray();
			return Convert.ToBase64String(bitmapBytes, Base64FormattingOptions.InsertLineBreaks);

		}


		/// <summary>
		/// Gets the stream from base64 string.
		/// </summary>
		/// <param name="s">The s.</param>
		/// <returns></returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
		public static Stream GetStreamFromBase64String(this string s) {
			byte[] bitmapBytes = Convert.FromBase64String(s);
			// ReSharper disable UseObjectOrCollectionInitializer
			MemoryStream memoryStream = new MemoryStream(bitmapBytes);
			// ReSharper restore UseObjectOrCollectionInitializer
			memoryStream.Position = 0;
			return memoryStream;
		}

	}
}

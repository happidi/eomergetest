﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Encapsulates extension for linq.
	/// </summary>
	public static class LinqExtensions {

		/// <summary>
		/// Splits a list into chuncks of smaller lists.
		/// </summary>
		/// <typeparam name="T">The generic type.</typeparam>
		/// <param name="listToSplit">The list to split.</param>
		/// <param name="countToTake">The number of elements to take in each chunck.</param>
		/// <returns></returns>
		public static IList<IList<T>> SplitList<T>(this IList<T> listToSplit, Int32 countToTake) {
			IList<IList<T>> splitList = new List<IList<T>>();

			Int32 countToSkip = 0;
			do {
				splitList.Add(listToSplit.Skip(countToSkip)
				 .Take(countToTake).ToList());
				countToSkip += countToTake;
			} while (countToSkip < listToSplit.Count);
			return splitList;
		}


		/// <summary>
		/// Determines whether the specified string contains any of the array values.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <param name="caseSensitive">True to indicate that the comparison should be case sensitive, false otherwise.</param>
		/// <param name="values">The array values.</param>
		/// <returns>
		/// 	<c>true</c> if the specified string contains any; otherwise, <c>false</c>.
		/// </returns>
		public static bool ContainsAny(this string str, bool caseSensitive, params string[] values) {
			if (!string.IsNullOrEmpty(str) || values.Length > 0) {
				foreach (string value in values) {
					if (caseSensitive) {
					}
					else {
					}
					if (str.ToLower().Contains(value.ToLower()))
						return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Flattens the entity tree.
		/// This is done in an iterative manner in order to prevent a stack overflow due to a very
		/// deep object tree that needs to be processed.
		/// </summary>
		/// <param name="rootObject">The root object.</param>
		/// <param name="getChildrenFunction">The get children function.</param>
		private static IEnumerable<T> FlattenTree<T>(this T rootObject, Func<T, IEnumerable<T>> getChildrenFunction) {
			var queue = new Queue<T>();
			queue.Enqueue(rootObject);

			while (queue.Count > 0) {
				var currentEntity = queue.Dequeue();

				yield return currentEntity;

				IEnumerable<T> entities = getChildrenFunction(currentEntity);
				foreach (var childEntity in entities) {
					queue.Enqueue(childEntity);
				}
			}
		}

		/// <summary>
		/// Flattens the entity trees.
		/// This is done in an iterative manner in order to prevent a stack overflow due to a very
		/// deep object tree that needs to be processed.
		/// </summary>
		/// <param name="rootObjects">The root objects.</param>
		/// <param name="getChildrenFunction">The get children function.</param>
		public static IEnumerable<T> FlattenTree<T>(this IEnumerable<T> rootObjects, Func<T, IEnumerable<T>> getChildrenFunction) {
			return rootObjects.SelectMany(rootObject => FlattenTree(rootObject, getChildrenFunction));
		}


		/// <summary>
		/// Gets sequences out of the enumerable, by the function.
		/// Consider refactoring this out to be an extension method.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <param name="list">The list.</param>
		/// <param name="func">The func.</param>
		/// <returns></returns>
		public static List<Tuple<TResult, List<T>>> GetSequences<T, TResult>(this IEnumerable<T> list, Func<T, TResult> func) {
			var sequences = new List<Tuple<TResult, List<T>>>();
			bool isFinishedEnumerating = false;

			IEnumerator<T> enumerator = list.GetEnumerator();
			enumerator.MoveNext();

			while (!isFinishedEnumerating) {
				TResult itemResult = func(enumerator.Current);
				List<T> sequence = EnumerateSequence(enumerator, func, out isFinishedEnumerating);

				sequences.Add(new Tuple<TResult, List<T>>(itemResult, sequence));
			}

			return sequences;
		}

		/// <summary>
		/// Enumerates the enumerable as a sequence while the result remains the same.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <param name="enumerator">The enumerator.</param>
		/// <param name="func">The func.</param>
		/// <param name="finishedEnumerating">if set to <c>true</c> the enumerable has reached its end, otherwise there are still items to enumerate, but as a different sequence.</param>
		/// <returns></returns>
		private static List<T> EnumerateSequence<T, TResult>(IEnumerator<T> enumerator, Func<T, TResult> func, out bool finishedEnumerating) {
			// This method enumerates the enumerator while the function output remains the same.
			var sequence = new List<T>();
			bool isEndOfEnumeration = true;

			TResult currentResult = func(enumerator.Current);

			do {
				TResult itemResult = func(enumerator.Current);

				if (!itemResult.Equals(currentResult)) {
					isEndOfEnumeration = false;
					break;
				}

				sequence.Add(enumerator.Current);

			} while (enumerator.MoveNext());

			finishedEnumerating = isEndOfEnumeration;
			return sequence;
		}

		/// <summary>
		/// IEnumerable extension that allows to select a unique list of object based on a specific property.
		/// For example, listOfObjects.Distinct(obj=>obj.KeyObject).
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source">The source.</param>
		/// <param name="uniqueCheckerMethod">The unique checker method.</param>
		/// <returns></returns>
		public static IEnumerable<T> Distinct<T>(this IEnumerable<T> source, Func<T, object> uniqueCheckerMethod) {
			return source.Distinct(new GenericComparer<T>(uniqueCheckerMethod));
		}

		/// <summary>
		/// Private generic comparer used in the Distinct extension.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		private class GenericComparer<T> : IEqualityComparer<T> {
			public GenericComparer(Func<T, object> uniqueCheckerMethod) {
				this._uniqueCheckerMethod = uniqueCheckerMethod;
			}

			private Func<T, object> _uniqueCheckerMethod;

			bool IEqualityComparer<T>.Equals(T x, T y) {
				return this._uniqueCheckerMethod(x).Equals(this._uniqueCheckerMethod(y));
			}

			int IEqualityComparer<T>.GetHashCode(T obj) {
				return this._uniqueCheckerMethod(obj).GetHashCode();
			}
		}

		/// <summary>
		/// Returns list to crud store.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source">The source.</param>
		/// <param name="action">The action.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentException"></exception>
		public static CrudStore<T> AsCrudStore<T>(this List<T> source, string action = CrudAction.Create) where T : BaseBusinessEntity {
			CrudStore<T> retVal;
			switch (action) {
				case CrudAction.Create:
					retVal = new CrudStore<T> { create = source };
					break;
				case CrudAction.Update:
					retVal = new CrudStore<T> { update = source };
					break;
				case CrudAction.Destroy:
					retVal = new CrudStore<T> { destroy = source };
					break;
				default:
					throw new ArgumentException(string.Format("action parameter should be {0}, {1} or {2}", CrudAction.Create, CrudAction.Update, CrudAction.Destroy));
			}
			return retVal;
		}
	}
}

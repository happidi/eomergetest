﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.Common {
		/// <summary>
	/// Encapsulates extension for the Palette and Palette Color class.
	/// </summary>
	public static class PaletteExtensions {

		/// <summary>
		/// Gets the System.Drawing.Color from a PaletteColor entity.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <returns></returns>
		public static Color GetColor(this PaletteColor color) {

			var retVal = Color.FromArgb(255, color.ColorR, color.ColorG, color.ColorB);
			return retVal;
		}

	}
}

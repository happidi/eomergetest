﻿using System;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Encapsulates extension for Action.
	/// </summary>
	public static class ActionExtensions {

		/// <summary>
		/// Converts the current Action to a Func&lt;TResult&gt; that returns the default value of TResult.
		/// </summary>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <param name="action">The action.</param>
		/// <returns>The default value of TResult</returns>
		public static Func<TResult> ConvertToFunc<TResult>(this Action action) {
			Func<TResult> func = () => {
				action();
				return default(TResult);
			};

			return func;
		}

		/// <summary>
		/// Converts the current Action&lt;T1&gt; to a Func&lt;TResult&gt; that returns the default value of TResult.
		/// </summary>
		/// <typeparam name="T1">The type of the parameter.</typeparam>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <param name="action">The action.</param>
		/// <returns>
		/// The default value of TResult
		/// </returns>
		public static Func<T1, TResult> ConvertToFunc<T1, TResult>(this Action<T1> action) {
			Func<T1, TResult> func = t => {
				action(t);
				return default(TResult);
			};

			return func;
		}


	}
}
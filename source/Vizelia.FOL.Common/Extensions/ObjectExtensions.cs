﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Vizelia.FOL.BusinessEntities {
	/// <summary>
	/// Encapsulates extension for the DataSerie class.
	/// </summary>
	public static class ObjectExtensions {

		/// <summary>
		/// Toes the memory stream.
		/// </summary>
		/// <param name="obj">The obj.</param>
		/// <returns></returns>
		public static byte[] ToByteArray(this object obj) {
			using (MemoryStream ms = new MemoryStream()) {
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.Serialize(ms, obj);
				ms.Seek(0, SeekOrigin.Begin);
				return ms.ToArray();
			}
		}

		/// <summary>
		/// Gets the memory size of an object (based on a binary serializer).
		/// </summary>
		/// <param name="obj">The obj.</param>
		/// <returns></returns>
		public static long GetMemoryFootprint(this Object obj) {
			using (MemoryStream ms = new MemoryStream()) {
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.Serialize(ms, obj);
				long length = ms.Length;
				return length;
			}
		}

		/// <summary>
		/// Clones the specified source.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source">The source.</param>
		/// <returns></returns>
		public static T Clone<T>(this T source) {
			if (!typeof(T).IsSerializable) {
				throw new ArgumentException("The type must be serializable.", "source");
			}

			// Don't serialize a null object, simply return the default for that object
			if (ReferenceEquals(source, null)) {
				return default(T);
			}

			IFormatter formatter = new BinaryFormatter();
			Stream stream = new MemoryStream();
			using (stream) {
				formatter.Serialize(stream, source);
				stream.Seek(0, SeekOrigin.Begin);
				return (T)formatter.Deserialize(stream);
			}
		}

		/// <summary>
		/// Lerps the specified start.
		/// </summary>
		/// <param name="start">The start.</param>
		/// <param name="end">The end.</param>
		/// <param name="amount">The amount.</param>
		/// <returns></returns>
		public static float Lerp(this float start, float end, float amount) {
			var difference = end - start;
			var adjusted = difference * amount;
			return start + adjusted;
		}

		/// <summary>
		/// Lerps the specified colour.
		/// </summary>
		/// <param name="colour">The colour.</param>
		/// <param name="to">To.</param>
		/// <param name="amount">The amount.</param>
		/// <returns></returns>
		public static Color Lerp(this Color colour, Color to, float amount) {
			// start colours as lerp-able floats
			float sr = colour.R, sg = colour.G, sb = colour.B;

			// end colours as lerp-able floats
			float er = to.R, eg = to.G, eb = to.B;

			// lerp the colours to get the difference
			byte r = (byte)sr.Lerp(er, amount),
				 g = (byte)sg.Lerp(eg, amount),
				 b = (byte)sb.Lerp(eb, amount);

			// return the new colour
			return Color.FromArgb(r, g, b);
		}
	}
}

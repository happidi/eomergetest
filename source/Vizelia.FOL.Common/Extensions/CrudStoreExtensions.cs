﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Encapsulates extension for CrudStore
	/// </summary>
	public static class CrudStoreExtensions {
	
		/// <summary>
		/// True if the crud store doesnt contains any records, False otherwise.
		/// </summary>
		public static bool IsEmpty<T>(this CrudStore<T> crudstore) where T:BaseBusinessEntity {	
				var count = 0;
				if (crudstore != null) {
					if (crudstore.create != null)
						count += crudstore.create.Count;
					if (crudstore.update != null)
						count += crudstore.update.Count;
					if (crudstore.destroy != null)
						count += crudstore.destroy.Count;
				}
				return count > 0 ? false : true;
			
		}

	}
}

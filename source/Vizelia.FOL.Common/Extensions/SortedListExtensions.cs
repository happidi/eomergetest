﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Common {
	/// <summary>
	/// Encapsulates extension for the PortalTab class.
	/// </summary>
	public static class SortedListExtensions {

		/// <summary>
		/// Binaries the search.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">The list.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		private static int BinarySearch<T>(IList<T> list, T value) {
			if (list == null) return -1;
			if (list.Count == 0) return -1;
			var comp = Comparer<T>.Default;
			int lo = 0, hi = list.Count - 1;
			while (lo < hi) {
				int m = (hi + lo) / 2;  // this might overflow; be careful.
				if (comp.Compare(list[m], value) < 0) lo = m + 1;
				else hi = m - 1;
			}
			if (comp.Compare(list[lo], value) < 0) lo++;
			return lo;
		}

		/// <summary>
		/// Finds the first index greater than or equal to.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="U"></typeparam>
		/// <param name="sortedList">The sorted list.</param>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public static int FindFirstIndexGreaterThanOrEqualTo<T, U>(this SortedList<T, U> sortedList, T key) {
			return BinarySearch(sortedList.Keys, key);
		}

	}
}

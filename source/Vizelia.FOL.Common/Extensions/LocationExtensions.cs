﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Encapsulates extension for Location
	/// </summary>
	public static class LocationExtensions {

		/// <summary>
		/// Return a list of Location Entities based on a list of BaseBusinessEntity (Site, Building,..)
		/// </summary>
		/// <param name="list">The BaseBusinessEntity list.</param>
		/// <returns></returns>
		public static List<Location> ToLocation(this List<BaseBusinessEntity> list) {
			var retVal = new List<Location>();
			foreach (var entity in list) {
				var location = entity as Location;

				if (location != null) {
					retVal.Add(location);

				}
				else {
					var ilocation = entity as ILocation;
					if (ilocation != null) {
						retVal.Add(ilocation.GetLocation());
					}
				}
			}
			return retVal;
		}
	}
}

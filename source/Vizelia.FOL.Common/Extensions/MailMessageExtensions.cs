﻿using System.Linq;
using System.Net.Mail;

namespace Vizelia.FOL.Common.Extensions {
	/// <summary>
	/// Encapsulates extensions for the MailMessage class.
	/// </summary>
	public static class MailMessageExtensions {

		/// <summary>
		/// Creates the message identifier that will be used to describe the message for the log.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <returns></returns>
		public static string CreateMessageSummary(this MailMessage message) {
			// Only get 20 first recipients since we don't want to create a too big log entry.
			string[] recipientEmais = message.To.Take(20).Select(r => r.Address).ToArray();
			string recipients = string.Join(", ", recipientEmais);
			int attachmentsCount = message.Attachments.Count;
			long attachmentsTotalSize = message.Attachments.Sum(a => a.ContentStream.Length);

			string messageIdentifier = string.Format("MailMessage Subject: \"{0}\", To recipients (listing {1} out of {2}): {3}, AttachmentsCount: {4}, AttachmentsTotalSize: {5}", message.Subject, recipientEmais.Length, message.To.Count, recipients, attachmentsCount, attachmentsTotalSize);

			return messageIdentifier;
		}
		 
	}
}
﻿using System;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// Encapsulates extension for DateTime structures.
	/// </summary>
	public static class DateTimeExtensions {

		/// <summary>
		/// Converts from date time offset to date time.
		/// </summary>
		/// <param name="dateTime">The date time offset.</param>
		/// <returns></returns>
		public static DateTime? ToDateTime(this DateTimeOffset? dateTime) {
			if (dateTime == null)
				return null;
			else
				return ToDateTime(dateTime.Value);
		}

		/// <summary>
		/// Converts from date time offset to date time.
		/// </summary>
		/// <param name="dateTime">The date time offset.</param>
		/// <returns></returns>
		public static DateTime ToDateTime(this DateTimeOffset dateTime) {
			if (dateTime.Offset.Equals(TimeSpan.Zero))
				return dateTime.UtcDateTime;
			else if (dateTime.Offset.Equals(TimeZoneInfo.Local.GetUtcOffset(dateTime.DateTime)))
				return DateTime.SpecifyKind(dateTime.DateTime, DateTimeKind.Local);
			else
				return dateTime.DateTime;
		}

		/// <summary>
		/// Converts to the UTC universal time.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <returns></returns>
		public static DateTime? ToUniversalTime(this DateTime? dateTime) {
			if (dateTime.HasValue)
				return dateTime.Value.ToUniversalTime();
			else
				return dateTime;
		}

		/// <summary>
		/// Add an interval to an existing DateTime.
		/// </summary>
		/// <param name="d">the datetime</param>
		/// <param name="timeinterval">the time interval</param>
		/// <param name="Frequency">The frequency.</param>
		/// <returns></returns>
		public static DateTime AddInterval(this DateTime d, AxisTimeInterval timeinterval, double Frequency) {
			var retVal = d;
			switch (timeinterval) {
				case AxisTimeInterval.Days:
					retVal = retVal.AddDays(Frequency);
					break;
				case AxisTimeInterval.Hours:
					retVal = retVal.AddHours(Frequency);
					break;

				case AxisTimeInterval.Minutes:
					retVal = retVal.AddMinutes(Frequency);
					break;

				case AxisTimeInterval.Months:
					retVal = retVal.AddMonths((int)Frequency);
					break;

				case AxisTimeInterval.Quarters:
					retVal = retVal.AddMonths((int)Frequency * 3);
					break;
				case AxisTimeInterval.Semesters:
					retVal = retVal.AddMonths((int)Frequency * 6);
					break;
				case AxisTimeInterval.Seconds:
					retVal = retVal.AddSeconds((int)Frequency);
					break;

				case AxisTimeInterval.Weeks:
					retVal = retVal.AddDays(Frequency * 7);
					break;
				case AxisTimeInterval.Years:
					retVal = retVal.AddYears((int)Frequency);
					break;

			}
			return retVal;
		}

		/// <summary>
		/// Update a DateTime based on a specific timeinterval to keep only relevant parts
		/// </summary>
		/// <param name="d">the datetime</param>
		/// <param name="timeinterval">the time interval</param>
		/// <returns></returns>
		public static DateTime UpdateByInterval(this DateTime d, AxisTimeInterval timeinterval) {
			switch (timeinterval) {
				case AxisTimeInterval.None:
					break;

				case AxisTimeInterval.Seconds:
				case AxisTimeInterval.Minutes:
					d = new DateTime(d.Year, d.Month, d.Day, d.Hour, d.Minute, 0);
					break;
				case AxisTimeInterval.Hours:
					if (d.Minute > 0)
						d = new DateTime(d.Year, d.Month, d.Day, d.Hour, 0, 0).AddHours(1);
					else
						d = new DateTime(d.Year, d.Month, d.Day, d.Hour, 0, 0);
					break;
				case AxisTimeInterval.Days:
					d = new DateTime(d.Year, d.Month, d.Day, 0, 0, 0);
					break;

				case AxisTimeInterval.Weeks:
					d = new DateTime(d.Year, d.Month, d.Day, 0, 0, 0);
					switch (d.DayOfWeek) {
						case DayOfWeek.Tuesday:
							d = d.AddDays(-1);
							break;
						case DayOfWeek.Wednesday:
							d = d.AddDays(-2);
							break;
						case DayOfWeek.Thursday:
							d = d.AddDays(-3);
							break;
						case DayOfWeek.Friday:
							d = d.AddDays(-4);
							break;
						case DayOfWeek.Saturday:
							d = d.AddDays(-5);
							break;
						case DayOfWeek.Sunday:
							d = d.AddDays(-6);
							break;
					}
					break;


				case AxisTimeInterval.Months:
					d = new DateTime(d.Year, d.Month, 1, 0, 0, 0);
					break;

				case AxisTimeInterval.Quarters:
					switch (d.Month) {
						case 1:
						case 2:
						case 3:
							d = new DateTime(d.Year, 1, 1, 0, 0, 0);
							break;
						case 4:
						case 5:
						case 6:
							d = new DateTime(d.Year, 4, 1, 0, 0, 0);
							break;
						case 7:
						case 8:
						case 9:
							d = new DateTime(d.Year, 7, 1, 0, 0, 0);
							break;
						case 10:
						case 11:
						case 12:
							d = new DateTime(d.Year, 10, 1, 0, 0, 0);
							break;
					}
					break;


				case AxisTimeInterval.Semesters:
					switch (d.Month) {
						case 1:
						case 2:
						case 3:
						case 4:
						case 5:
						case 6:
							d = new DateTime(d.Year, 1, 1, 0, 0, 0);
							break;
						case 7:
						case 8:
						case 9:
						case 10:
						case 11:
						case 12:
							d = new DateTime(d.Year, 7, 1, 0, 0, 0);
							break;
					}
					break;


				case AxisTimeInterval.Years:
					d = new DateTime(d.Year, 1, 1, 0, 0, 0);
					break;

				case AxisTimeInterval.Global:
					d = new DateTime(DateTime.Now.Year, 1, 1);
					break;
			}
			return d;
		}
		/// <summary>
		/// returns the number of milliseconds since Jan 1, 1970 (useful for converting C# dates to JS dates).
		/// </summary>
		/// <param name="dateTime">The datetime.</param>
		/// <returns></returns>
		public static double UnixTicks(this DateTime dateTime) {
			var d1 = new DateTime(1970, 1, 1);
			var d2 = dateTime.ToUniversalTime();
			var ts = new TimeSpan(d2.Ticks - d1.Ticks);
			return ts.TotalMilliseconds;
		}

		/// <summary>
		/// Pretties the time span.
		/// </summary>
		/// <param name="timeSpan">The time span.</param>
		/// <returns></returns>
		public static string ToPrettyTimeSpanString(this TimeSpan timeSpan) {

			if (timeSpan == TimeSpan.Zero) return "0 minutes";

			var sb = new StringBuilder();
			if (timeSpan.Days > 0)
				sb.Append(timeSpan.Days + " " + Langue.msg_calendarevent_days + " ");
			if (timeSpan.Hours > 0)
				sb.Append(timeSpan.Hours + " " + Langue.msg_calendarevent_hours + " ");
			if (timeSpan.Minutes > 0)
				sb.Append(timeSpan.Minutes + " " + Langue.msg_calendarevent_minutes + " ");
			return sb.ToString();
		}

		/// <summary>
		/// Returns the Json format of the date.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <returns></returns>
		public static string JsonDate(this DateTime dateTime) {
			return String.Format("/Date({0})/", dateTime.UnixTicks());
		}

		/// <summary>
		/// Converts the date to a new time zone from UTC.
		/// </summary>
		/// <param name="date">The date.</param>
		/// <param name="timeZone">The time zone.</param>
		/// <returns></returns>
		public static DateTime ConvertTimeFromUtc(this DateTime date, TimeZoneInfo timeZone) {
			var changedDate = date;
			try {
				changedDate = TimeZoneInfo.ConvertTime(changedDate, TimeZoneInfo.Utc, timeZone);
			}
			catch {
				try {
					changedDate = TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(changedDate, DateTimeKind.Unspecified), timeZone);
				}
				catch {
					;
				}
			}
			return changedDate;
		}

		/// <summary>
		/// Converts the date to UTC.
		/// </summary>
		/// <param name="date">The date.</param>
		/// <param name="timeZone">The time zone.</param>
		/// <returns></returns>
		public static DateTime ConvertTimeToUtc(this DateTime date, TimeZoneInfo timeZone) {
			var changedDate = date;
			try {
				changedDate = TimeZoneInfo.ConvertTime(changedDate, timeZone, TimeZoneInfo.Utc);
			}
			catch {
				try {
					changedDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(changedDate, DateTimeKind.Unspecified), timeZone);
				}
				catch {
					;
				}
			}
			return changedDate;
		}

		/// <summary>
		/// Convert a date by the time zone.
		/// </summary>
		/// <param name="dt">The dt.</param>
		/// <param name="timeZone">The time zone.</param>
		/// <returns></returns>
		public static DateTime ToTimeZone(this DateTime dt, TimeZoneInfo timeZone) {
			dt = dt.Add(TimeZoneInfo.Local.GetUtcOffset(dt));
			var tempDate = new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second, dt.Millisecond, DateTimeKind.Unspecified);

			if (timeZone.IsInvalidTime(tempDate)) {
				var rules = timeZone.GetAdjustmentRules();
				tempDate = timeZone.IsDaylightSavingTime(dt) ? tempDate.Add(+rules[0].DaylightDelta) : tempDate.Add(-rules[0].DaylightDelta);
			}
			tempDate = TimeZoneInfo.ConvertTimeToUtc(tempDate, timeZone);
			return tempDate;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// Encapsulates extension for data
	/// </summary>
	public static class DataExtensions {

		/// <summary>
		/// Gets the value of field for a reader.
		/// </summary>
		/// <typeparam name="T">The type of the field.</typeparam>
		/// <param name="reader">The reader.</param>
		/// <param name="name">The name of the field.</param>
		/// <param name="defaultValue">The default value of the field that should be returned</param>
		/// <returns>The value of the field.</returns>
		public static T GetValue<T>(this IDataReader reader, string name, T defaultValue) {
			object value = reader.GetValue(reader.GetOrdinal(name));
			return (value != DBNull.Value) ? (T)value : defaultValue;
		}

		/// <summary>
		/// Gets the value of field for a reader.
		/// </summary>
		/// <typeparam name="T">The type of the field.</typeparam>
		/// <param name="reader">The reader.</param>
		/// <param name="name">The name of the field.</param>
		/// <returns>The value of the field.</returns>
		public static T GetValue<T>(this IDataReader reader, string name) {
			object value = reader.GetValue(reader.GetOrdinal(name));
			return (value != DBNull.Value) ? (T)value : default(T);
		}


		/// <summary>
		/// Checks the existence of a field in a DataReader.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns></returns>
		public static bool FieldExists(this IDataReader reader, string fieldName) {
			reader.GetSchemaTable().DefaultView.RowFilter = string.Format("ColumnName= '{0}'", fieldName);
			return (reader.GetSchemaTable().DefaultView.Count > 0);
		}


		/// <summary>
		/// Checks the existence of a field in a DataSet row.
		/// </summary>
		/// <param name="row">The row.</param>
		/// <param name="fieldName">Name of the field.</param>
		/// <returns></returns>
		public static bool FieldExists(this DataRow row, string fieldName) {
			return row.Table.Columns.Contains(fieldName);
		}


	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Vizelia.FOL.Common;
using System.IO;


namespace Vizelia.FOL.BusinessEntities {

	/// <summary>
	/// Encapsulates extension for the Document class.
	/// </summary>
	public static class DocumentExtensions {

		/// <summary>
		/// Extension Method to return a MemoryStream from the document;
		/// </summary>
		/// <param name="document"></param>
		/// <returns></returns>
		public static MemoryStream GetStream(this Document document) {
			MemoryStream retVal = null;
			if (document != null && document.DocumentContentsLength > 0) {
				retVal = new MemoryStream(document.DocumentContents);
			}
			return retVal;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vizelia.FOL.Common {


	/// <summary>
	/// Dictionary extenstions
	/// </summary>
	public static class DictionaryExtensions {
		/// <summary>
		/// Merges two dictionaries.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="original">The original.</param>
		/// <param name="additional">The additional.</param>
		/// <param name="distinct">if set to <c>true</c> [distinct].</param>
		/// <remarks>
		/// Values in original dictionary will be concatened with values from additional dictionary with same keys.
		/// </remarks>
		public static void Merge<TKey, TValue>(this IDictionary<TKey, List<TValue>> original, IDictionary<TKey, List<TValue>> additional, bool distinct = false) {
			if (original == null) {
				return;
			}
			if (additional == null) {
				return;
			}
			foreach (var pair in additional) {
				if (original.ContainsKey(pair.Key) && pair.Value != null) {
					original[pair.Key] = original[pair.Key].Union(pair.Value).ToList();
					if (distinct)
						original[pair.Key] = original[pair.Key].Distinct().ToList();
				}
				else {
					original[pair.Key] = pair.Value;
				}
			}
		}

		/// <summary>
		/// Add or merge the new list to the existing dictionary.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="original">The original.</param>
		/// <param name="Key">The key.</param>
		/// <param name="Values">The values.</param>
		/// <param name="distinct">if set to <c>true</c> [distinct].</param>
		public static void AddOrMerge<TKey, TValue>(this IDictionary<TKey, List<TValue>> original, TKey Key, List<TValue> Values, bool distinct = false) {
			if (original != null) {
				List<TValue> list = null;
				if (!original.TryGetValue(Key, out list)) {
					list = new List<TValue>();
					original.Add(Key, list);
				}
				list.AddRange(Values);
				if (distinct)
					list = list.Distinct().ToList();
			}
		}

		/// <summary>
		/// Add or merge the new list to the existing dictionary.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="original">The original.</param>
		/// <param name="Key">The key.</param>
		/// <param name="Value">The value.</param>
		public static void AddOrMerge<TKey, TValue>(this IDictionary<TKey, List<TValue>> original, TKey Key, TValue Value) {
			AddOrMerge(original, Key, new List<TValue>() { Value });
		}
		/// <summary>
		/// Clones the specified dict.
		/// </summary>
		/// <typeparam name="TKey">The type of the key.</typeparam>
		/// <typeparam name="TValue">The type of the value.</typeparam>
		/// <param name="dict">The dict.</param>
		/// <returns></returns>
		public static IDictionary<TKey, TValue> Clone<TKey, TValue>(this IDictionary<TKey, TValue> dict) {
			if (dict != null) {
				return dict.ToDictionary(entry => entry.Key, entry => entry.Value);

			}
			return dict;
		}
	}

}

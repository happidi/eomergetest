﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace Vizelia.FOL.Common {
	
	/// <summary>
	/// Encapsulates extension for Mail class.
	/// </summary>
	public static class MailExtensions {
		/// <summary>
		/// Adds a notification address to the mail message.
		/// </summary>
		/// <param name="message">The mail message.</param>
		/// <param name="address">The address for notification.</param>
		public static void AddNotification(this MailMessage message, MailAddress address) {
			message.Headers.Add("disposition-notification-to" , address.Address);
			message.Headers.Add("return-receipt-to", address.Address);
		}
	}
}

﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;

namespace Vizelia.FOL.Common {

	/// <summary>
	/// Encapsulates extension for String class.
	/// </summary>
	public static class StringExtensions {

		/// <summary>
		/// Counts the number of words in a string.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <returns></returns>
		public static int WordCount(this string str) {
			var reg = new Regex(@"\[.+\]+|[\w\+]+");
			var mc = reg.Matches(str);
			var words = mc.Count > 0 ? mc.Count : 0;
			return words;
		}

		/// <summary>
		/// Safes the SQL literal.
		/// </summary>
		/// <param name="str">The STR.</param>
		/// <returns></returns>
		public static string SafeSqlLiteral(this string str) {
			return String.IsNullOrEmpty(str)
					   ? str
					   : str.RemoveSqlComments()
							.Replace("`", "'")
							.Replace("´", "'")
							.Replace("]", "]]")
							.Replace("[", "[[")
							.Replace("'", "''");
		}


		/// <summary>
		/// Removes the SQL comments by performing several passes on the string.
		/// Prevents strings such as -/*-/*/*-*/- -/*-*//*-*/- ///******///
		/// </summary>
		/// <param name="str">The STR.</param>
		/// <returns></returns>
		private static string RemoveSqlComments(this string str) {
			string oldString = String.Empty;
			string newString = str;

			while (!oldString.Equals(newString)) {
				oldString = newString;

				newString = newString
					.Replace("--", "")
					.Replace("/*", "")
					.Replace("*/", "");
			}

			return newString;
		}

		/// <summary>
		/// Determines whether the string represents a valid order by clause.
		/// </summary>
		/// <param name="str">The STR.</param>
		public static bool IsOrderByValid(this string str) {
			var reg = new Regex(@"[^,\s][^\,]*[^,\s]*");
			var mc = reg.Matches(str);
			foreach (var w in mc) {
				if (w.ToString().WordCount() > 1)
					return false;
			}
			return true;
		}


		/// <summary>
		/// Converts the null or empty string to the default value.
		/// </summary>
		/// <param name="str">The string.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static string ConvertNullOrEmptyTo(this string str, string defaultValue) {
			if (String.IsNullOrEmpty(str))
				return defaultValue;
			return str;
		}

		/// <summary>
		/// Check if a text can be parsed as a double.
		/// </summary>
		/// <param name="str">The STR.</param>
		/// <param name="numberFormatInfo">The number format info.</param>
		/// <returns>
		///   <c>true</c> if this string is double; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsDouble(this string str, NumberFormatInfo numberFormatInfo) {
			double v;
			return Double.TryParse(str, NumberStyles.Any, numberFormatInfo, out v);
		}

		/// <summary>
		/// Remove HTML tags from string using char array.
		/// </summary>
		public static string StripTagsCharArray(this string str) {
			char[] array = new char[str.Length];
			int arrayIndex = 0;
			bool inside = false;

			for (int i = 0; i < str.Length; i++) {
				char let = str[i];
				if (let.Equals('<')) {
					inside = true;
					continue;
				}
				if (let.Equals('>')) {
					inside = false;
					continue;
				}
				if (inside) continue;

				array[arrayIndex] = @let;
				arrayIndex++;
			}
			return new string(array, 0, arrayIndex);
		}

		/// <summary>
		/// Decode an html encoded string.
		/// </summary>
		/// <param name="html">The HTML.</param>
		/// <returns></returns>
		public static string HtmlDecode(this string html) {
			var retVal = HttpUtility.HtmlDecode(html);
			return retVal;
		}

		/// <summary>
		/// Encode a string to Html format.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static string HtmlEncode(this string value) {
			var retVal = HttpUtility.HtmlEncode(value);
			return retVal;
		}

		/// <summary>
		/// Removes all namespaces.
		/// </summary>
		/// <param name="xmlDocument">The XML document.</param>
		/// <returns></returns>
		public static string RemoveAllNamespaces(this string xmlDocument) {
			XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

			return xmlDocumentWithoutNs.ToString();
		}

		/// <summary>
		/// Removes all namespaces.
		/// </summary>
		/// <param name="xmlDocument">The XML document.</param>
		/// <returns></returns>
		private static XElement RemoveAllNamespaces(XElement xmlDocument) {
			if (!xmlDocument.HasElements) {
				XElement xElement = new XElement(xmlDocument.Name.LocalName) { Value = xmlDocument.Value };

				foreach (XAttribute attribute in xmlDocument.Attributes())
					xElement.Add(attribute);

				return xElement;
			}
			return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
		}

        /// <summary>
        /// Check if source contains toCheck
        /// </summary>
        /// <param name="source">The Source String</param>
        /// <param name="toCheck">The Input to Check if the source contains</param>
        /// <param name="comp">Comparison Values</param>
        /// <returns></returns>
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }
	}
}
﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.BusinessLayer {

	/// <summary>
	/// Performs validation of max meter business logic.
	/// </summary>
	public class MaxMeterValidator {

		#region Properties

		/// <summary>
		/// Chart Entity.
		/// </summary>
		public Chart Chart { get; set; }
		
		/// <summary>
		/// Resulting FormResponse.
		/// </summary>
		protected FormResponse FormResponse { get; private set; }
	
		/// <summary>
		/// Total count of the resulting meters.
		/// </summary>
		protected int ResultingMetersCount { get; private set; }

		/// <summary>
		/// CustomErrorHandling.
		/// </summary>
		protected bool CustomErrorHandling { get; private set; }

		#endregion

		#region Constructors
	
		/// <summary>
		/// Intitilizaes the MaxMeterValidator
		/// </summary>
		/// <param name="chart"></param>
		/// <param name="resultingMetersCount"></param>
		/// <param name="customErrorHandling"></param>
		/// <param name="formResponse"></param>
		public MaxMeterValidator(Chart chart, int resultingMetersCount, bool customErrorHandling = false, FormResponse formResponse = null) {
			if (chart == null) {
				throw new ArgumentNullException("chart");
			}
			Chart = chart;
			FormResponse = formResponse ?? new FormResponse();
			FormResponse.success = true;
			ResultingMetersCount = resultingMetersCount;
			CustomErrorHandling = customErrorHandling;
		}
		#endregion

		#region Methods

		/// <summary>
		/// Handles errors of the validation.
		/// </summary>
		protected virtual void HandleError() {
			var errorMessage = string.Format(Langue.msg_resulting_meters_overflow, ResultingMetersCount, Chart.MaxMeter);

			//If validation fails, then adding the error message on the chart.
			if (Chart.Errors != null) {
				Chart.Errors.Add(errorMessage);
			}
			if (!CustomErrorHandling) {
				FormResponse.success = false;
				FormResponse.AddError("MaxMeter", errorMessage);
			}
			else {
				Chart.MaxMeterOverflowError = errorMessage;
			}
		}
	
		/// <summary>
		/// Main validation method.
		/// </summary>
		/// <returns></returns>
		public FormResponse ValidateFormResponse() {

			if (ResultingMetersCount > Chart.MaxMeter) {
				HandleError();
			}
			return FormResponse;
		}
		#endregion
	}
}
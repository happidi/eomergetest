﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.DataLayer.EntityFramework.Entities;
using Vizelia.FOL.DataLayer.EntityFramework.Repositories;
using Vizelia.FOL.Providers;
using Vizelia.FOL.ServiceAgents;
using EventLog = Vizelia.FOL.BusinessEntities.EventLog;


namespace Vizelia.FOL.BusinessLayer {
	/// <summary>
	/// Business Layer implementation for Energy.
	/// </summary>
	public class EnergyBusinessLayer : IEnergyBusinessLayer {

		private const int const_meter_data_bulk_page_size = 10000;
		private const string const_energy_aggregator_service_reference = "Vizelia.FOL.EnergyAggregatorService";

		/// <summary>
		/// The number of rows to delete in the DeleteLimitedNumberRowsByKeySource method
		/// </summary>
		const int const_numberofrowstodelete = 20000;

		private readonly Dictionary<CalendarFrequencyType, int> m_FrequencyInSeconds = new Dictionary<CalendarFrequencyType, int> { { CalendarFrequencyType.None, 0 }, { CalendarFrequencyType.Secondly, 1 }, { CalendarFrequencyType.Minutely, 60 }, { CalendarFrequencyType.Hourly, 60 * 60 }, { CalendarFrequencyType.Daily, 60 * 60 * 24 } };
		/// <summary>
		/// Defines in milliseconds the time cutycapt will wait before rendering each PortalTab.
		/// </summary>
		private readonly ICoreBusinessLayer m_CoreBusinessLayer;
		private readonly IServiceDeskBusinessLayer m_ServiceDeskBusinessLayer;



		/// <summary>
		/// Public ctor.
		/// </summary>
		public EnergyBusinessLayer() {
			m_CoreBusinessLayer = Helper.Resolve<ICoreBusinessLayer>();
			m_ServiceDeskBusinessLayer = Helper.Resolve<IServiceDeskBusinessLayer>();

		}


		/// <summary>
		/// Adds the frequency to the date.
		/// </summary>
		/// <param name="date">The date.</param>
		/// <param name="frequencyNumber">The frequency number.</param>
		/// <param name="frequency">The frequency.</param>
		/// <returns></returns>
		private DateTime AddFrequency(DateTime date, int frequencyNumber, AxisTimeInterval frequency) {
			DateTime newDate;

			switch (frequency) {
				case AxisTimeInterval.Years:
					newDate = date.AddYears(1 * frequencyNumber);
					break;
				case AxisTimeInterval.Quarters:
					newDate = date.AddMonths(3 * frequencyNumber);
					break;
				case AxisTimeInterval.Semesters:
					newDate = date.AddMonths(6 * frequencyNumber);
					break;
				case AxisTimeInterval.Months:
					newDate = date.AddMonths(1 * frequencyNumber);
					break;
				case AxisTimeInterval.Weeks:
					newDate = date.AddDays(7 * frequencyNumber);
					break;
				case AxisTimeInterval.Days:
					newDate = date.AddDays(1 * frequencyNumber);
					break;
				case AxisTimeInterval.Hours:
					newDate = date.AddHours(1 * frequencyNumber);
					break;
				case AxisTimeInterval.Minutes:
					newDate = date.AddMinutes(1 * frequencyNumber);
					break;
				case AxisTimeInterval.Seconds:
					newDate = date.AddSeconds(1 * frequencyNumber);
					break;
				default:
					throw new ArgumentOutOfRangeException("frequency");
			}

			return newDate;
		}

		/// <summary>
		/// Deletes an existing business entity AlarmDefinition.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool AlarmDefinition_Delete(AlarmDefinition item) {
			AlarmDefinitionBroker broker = Helper.CreateInstance<AlarmDefinitionBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity AlarmDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse AlarmDefinition_FormCreate(AlarmDefinition item) {
			return AlarmDefinition_FormCreate(item, item.Meters.AsCrudStore(), item.FilterSpatial.AsCrudStore(),
				item.FilterMeterClassifications.AsCrudStore(), item.ConnectedCharts.AsCrudStore());
		}

		/// <summary>
		/// Creates a new business entity AlarmDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <param name="charts">The charts.</param>
		/// <returns></returns>
		public FormResponse AlarmDefinition_FormCreate(AlarmDefinition item, CrudStore<Meter> meters, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification, CrudStore<Chart> charts) {
			FormResponse response;

			var broker = Helper.CreateInstance<AlarmDefinitionBroker>();
			var brokerMeter = Helper.CreateInstance<MeterBroker>();
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			var brokerClassification = Helper.CreateInstance<ClassificationItemBroker>();
			var brokerChart = Helper.CreateInstance<ChartBroker>();

			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						var alarmDef = response.data as AlarmDefinition;
						if (alarmDef != null) {
							brokerMeter.SaveAlarmDefinitionMeters(alarmDef, meters);
							brokerLocation.SaveAlarmDefinitionFilterSpatial(alarmDef, filterSpatial);
							brokerClassification.SaveAlarmDefinitionFilterMeterClassification(alarmDef, filterMeterClassification);
							brokerChart.SaveChartAlarmDefinition(alarmDef, charts);
						}
						t.Complete();

					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse AlarmDefinition_FormLoad(string Key) {
			AlarmDefinitionBroker broker = Helper.CreateInstance<AlarmDefinitionBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity AlarmDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse AlarmDefinition_FormUpdate(AlarmDefinition item) {
			return AlarmDefinition_FormUpdate(item, item.Meters.AsCrudStore(CrudAction.Update), item.FilterSpatial.AsCrudStore(CrudAction.Update),
				item.FilterMeterClassifications.AsCrudStore(CrudAction.Update), null, item.ConnectedCharts.AsCrudStore(CrudAction.Update));
		}

		/// <summary>
		/// Updates an existing business entity AlarmDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <param name="filterSpatialPset">The filter spatial pset.</param>
		/// <param name="charts">The charts.</param>
		/// <returns></returns>
		public FormResponse AlarmDefinition_FormUpdate(AlarmDefinition item, CrudStore<Meter> meters, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification, CrudStore<FilterSpatialPset> filterSpatialPset, CrudStore<Chart> charts) {
			FormResponse response;

			var broker = Helper.CreateInstance<AlarmDefinitionBroker>();
			var brokerMeter = Helper.CreateInstance<MeterBroker>();
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			var brokerClassification = Helper.CreateInstance<ClassificationItemBroker>();
			var brokerFilterSpatialPset = Helper.CreateInstance<FilterSpatialPsetBroker>();
			var brokerChart = Helper.CreateInstance<ChartBroker>();

			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						var alarmDef = response.data as AlarmDefinition;
						if (alarmDef != null) {
							brokerMeter.SaveAlarmDefinitionMeters(alarmDef, meters);
							brokerLocation.SaveAlarmDefinitionFilterSpatial(alarmDef, filterSpatial);
							brokerClassification.SaveAlarmDefinitionFilterMeterClassification(alarmDef, filterMeterClassification);
							brokerFilterSpatialPset.SaveAlarmDefinitionFilterSpatialPset(alarmDef, filterSpatialPset);
							brokerChart.SaveChartAlarmDefinition(alarmDef, charts);
						}
						t.Complete();

					}
					catch (Exception ex) {
						response.msg = ex.Message;
						response.success = false;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse AlarmDefinition_FormUpdateBatch(string[] keys, AlarmDefinition item) {
			AlarmDefinitionBroker broker = Helper.CreateInstance<AlarmDefinitionBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<AlarmDefinition> AlarmDefinition_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			AlarmDefinitionBroker broker = Helper.CreateInstance<AlarmDefinitionBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public AlarmDefinition AlarmDefinition_GetItem(string Key, params string[] fields) {
			AlarmDefinitionBroker broker = Helper.CreateInstance<AlarmDefinitionBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Return a json store of the complete list of Meters associated with a AlarmDefinition.
		/// including meters associated via SpatialFilter cross SpatialFilterPset cross MeterClassification
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key of the AlarmDefinition.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<Meter> AlarmDefinition_GetMetersStore(string KeyAlarmDefinition, PagingParameter paging) {
			var meterBroker = Helper.CreateInstance<MeterBroker>();
			List<Meter> meters = meterBroker.GetCompleteListByKeyAlarmDefinition(KeyAlarmDefinition);
			meterBroker.PopulateList(meters, EntityLoadMode.Standard, "*");
			return meters.ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Gets a json store for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<AlarmDefinition> AlarmDefinition_GetStore(PagingParameter paging, PagingLocation location) {
			var broker = Helper.CreateInstance<AlarmDefinitionBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets the AlarmDefinition store by a job and a step.
		/// </summary>
		/// <param name="keyJob">The key job.</param>
		/// <param name="keyStep">The key step.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<AlarmDefinition> AlarmDefinition_GetStoreByJobStep(string keyJob, string keyStep, PagingParameter paging, PagingLocation location) {
			var coreBusinessLayer = Helper.Resolve<ICoreBusinessLayer>();
			var alarmDefinitions = coreBusinessLayer.GetChildEntitiesByParentJobStep<AlarmDefinition>(keyJob, keyStep, paging);

			return alarmDefinitions;
		}

		/// <summary>
		/// Gets a json store of AlarmDefinition for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmTable">The Key of the AlarmTable.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<AlarmDefinition> AlarmDefinition_GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable) {
			AlarmDefinitionBroker broker = Helper.CreateInstance<AlarmDefinitionBroker>();
			return broker.GetStoreByKeyAlarmTable(paging, KeyAlarmTable);
		}


		/// <summary>
		/// Gets all the alarm keys that are not processed manually (used in the job when no alarm is assigned).
		/// </summary>
		/// <returns></returns>
		public string[] AlarmDefinition_GetAllToProcess() {
			int total;
			var broker = Helper.CreateInstance<AlarmDefinitionBroker>();

			var list = broker.GetAll(new PagingParameter {
				filters = new List<GridFilter> {
						new GridFilter {
							field="ProcessManually",
							data=new GridData {
								comparison="eq",
								type="boolean",
								value="false"
							}
						}
					}
			}, PagingLocation.Database, out total, false);
			var retVal = list.Select(alarm => alarm.KeyAlarmDefinition).ToArray();
			return retVal;
		}
		/// <summary>
		/// Gets all the enabled AlarmDefinition and process them to generate the AlarmInstances
		/// </summary>
		/// <param name="Keys">The keys.</param>
		/// <param name="username">The username.</param>
		public void AlarmDefinition_Process(string[] Keys, string username = null) {
			var sw = new Stopwatch();
			var currentCulture = Thread.CurrentThread.CurrentCulture;
			var currentUICulture = Thread.CurrentThread.CurrentUICulture;

			var sessionId = SessionService.GetSessionID();
			if (string.IsNullOrEmpty(username))
				username = Helper.GetCurrentUserName();

			var broker = Helper.CreateInstance<AlarmDefinitionBroker>();
			var brokerInstance = Helper.CreateInstance<AlarmInstanceBroker>();
			var brokerChart = Helper.CreateInstance<ChartBroker>();

			try {

				#region Getting all enabled alarm definition or the context ones
				//If the job doesnt have any alarm assigned, we go and select all alarm that are not processed manually
				//if (Keys == null || Keys.Length == 0) {

				//    int total;
				//    var list = broker.GetAll(new PagingParameter {
				//        filters = new List<GridFilter> {
				//        new GridFilter {
				//            field="ProcessManually",
				//            data=new GridData {
				//                comparison="eq",
				//                type="boolean",
				//                value="false"
				//            }
				//        }
				//    }
				//    }, PagingLocation.Database, out total, false);
				//    Keys = list.Select(alarm => alarm.KeyAlarmDefinition).ToArray();
				//}


				if (Keys.Length == 0)
					return;

				var alarms = new Dictionary<string, AlarmDefinition>();
				foreach (var key in Keys) {
					var alarm = broker.GetItem(key, "*");
					if (alarm != null && alarm.Enabled) {
						var charts = brokerChart.GetListByKeyAlarmDefinition(alarm.KeyAlarmDefinition);
						alarm.Charts = new List<Tuple<Chart, List<Meter>, List<Location>>>();
						foreach (var c in charts) {
							var chart = Chart_GetItem(c.KeyChart, "*");
							var meters = Chart_GetMeters(c.KeyChart, false);
							var locations = Chart_GetLocations(c.KeyChart, meters);
							alarm.Charts.Add(new Tuple<Chart, List<Meter>, List<Location>>(chart, meters, locations));
						}
						alarms.Add(key, alarm);
					}
				}
				#endregion

				using (var energyAggregator = GetEnergyAggregatorServiceAgent()) {
					//We generate the alarm instances.
					sw.Start();
					var dictionary = energyAggregator.AlarmDefinition_Process(alarms.Values.ToList());
					sw.Stop();
					if (sw.Elapsed.TotalSeconds > 90)
						TracingService.Write(TraceEntrySeverity.Warning, "AlarmDefinition Process took " + sw.Elapsed.TotalSeconds + " for alarms : " + String.Join(",", Keys), "Alarm", "");
					var count = 0;
					var totalCount = dictionary.Sum(kvp => kvp.Value != null ? kvp.Value.Count : 0);

					var batchEmails = new Dictionary<string, List<string>>();
					foreach (var kvp in dictionary) {
						sw.Restart();
						var alarm = kvp.Key;
						var alarmInstances = kvp.Value;
						alarmInstances.ForEach(instance => {
							//Parallel.ForEach(alarmInstances, instance => {
							#region Culture
							if (!string.IsNullOrEmpty(alarm.KeyLocalizationCulture)) {
								Thread.CurrentThread.CurrentCulture = new CultureInfo(alarm.KeyLocalizationCulture, false);
								Thread.CurrentThread.CurrentUICulture = new CultureInfo(alarm.KeyLocalizationCulture, false);
							}
							#endregion
							//we save or update each instance.
							instance = brokerInstance.Save(instance);
							if (instance != null) {
								#region Getting AlarmInstance description
								instance.AlarmDefinition = alarm;

								#endregion
								#region Action Request
								//if not actionrequest is already assigned and the alarmdefinition is attached to one
								if (string.IsNullOrEmpty(instance.KeyActionRequest) && !string.IsNullOrEmpty(alarm.ClassificationKeyChildren) && !string.IsNullOrEmpty(alarm.ClassificationKeyParent)) {
									var description = AlarmHelper.GetAlarmInstanceBody(instance);
									//we generate the action request
									var requestForm = AlarmDefinition_RaiseActionRequest(alarm, description);
									if (requestForm != null && requestForm.success && requestForm.data != null) {
										//we save the KeyActionRequest back to the alarm instance.
										instance.KeyActionRequest = ((ActionRequest)requestForm.data).KeyActionRequest;
										brokerInstance.Save(instance);
									}
								}
								#endregion
								#region Emails batch
								//We batch the emails in order to send only one per instance of AlarmDefinition
								else if (!string.IsNullOrEmpty(alarm.Emails) && instance.EmailSent != true) {
									var description = AlarmHelper.GetAlarmInstanceBody(instance);
									var instancesDescription = new List<string>();
									if (batchEmails.ContainsKey(alarm.KeyAlarmDefinition)) {
										instancesDescription = batchEmails[alarm.KeyAlarmDefinition];
										batchEmails.Remove(alarm.KeyAlarmDefinition);
									}
									instancesDescription.Add(description);
									batchEmails.Add(alarm.KeyAlarmDefinition, instancesDescription);
									instance.EmailSent = true;
									brokerInstance.Save(instance);
								}
								#endregion
							}
							count += 1;
							LongRunningOperationService.ReportProgress(count * 100 / totalCount);
						});
						#region LastProcessDateTime
						alarm.LastProcessDateTime = DateTime.UtcNow;
						broker.Save(alarm);
						#endregion
						#region Performances
						sw.Stop();
						if (sw.Elapsed.TotalSeconds > 60)
							TracingService.Write(TraceEntrySeverity.Warning, "AlarmDefinition Saving took " + sw.Elapsed.TotalSeconds + " for " + alarmInstances.Count + " instances", "Alarm", alarm.KeyAlarmDefinition);
						#endregion
						#region Session
						try {
							SessionService.CreateOrSlideSession(sessionId, username, true);
						}
						catch (SystemException e) {
							TracingService.Write(e);
						}
						#endregion
					}
					#region Emails sending
					foreach (var kv in batchEmails) {
						var alarm = alarms[kv.Key];
						var instancesDescription = kv.Value;

						var description = instancesDescription.Aggregate("", (current, desc) => current + (desc + Environment.NewLine));

						var mailBroker = Helper.CreateInstance<MailBroker>();
						var mails = mailBroker.GetListByClassificationItemLocalId(MailBroker.const_alarm);
						var modelDataAlarm = new ModelDataAlarm {
							Alarm = alarm,
							InstanceCount = instancesDescription.Count,
							Description = description
						};
						var mailMessages = MailService.ConvertMailsToMailMessages(modelDataAlarm, mails);
						foreach (var mailMessage in mailMessages) {
							MailService.Send(mailMessage);
						}
					}
					#endregion
				}
			}
			finally {
				Thread.CurrentThread.CurrentCulture = currentCulture;
				Thread.CurrentThread.CurrentUICulture = currentUICulture;
			}

		}

		/// <summary>
		/// Gets all the enabled AlarmDefinition and process them to generate the AlarmInstanceswith a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
		public void AlarmDefinition_ProcessBegin(Guid operationId, string[] Keys) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				AlarmDefinition_Process(Keys);
				var result = new LongRunningOperationResult(LongRunningOperationStatus.Completed);
				return result;
			});
		}

		/// <summary>
		/// Alarm raises a action request.
		/// </summary>
		/// <param name="alarm">The alarm.</param>
		/// <param name="description">The description.</param>
		/// <returns></returns>
		private FormResponse AlarmDefinition_RaiseActionRequest(AlarmDefinition alarm, string description) {
			var actualStart = DateTime.Now;
			var priority = m_CoreBusinessLayer.Priority_GetItemByClassificationAscendant(alarm.ClassificationKeyChildren, alarm.ClassificationKeyParent);
			var scheduleDate = m_ServiceDeskBusinessLayer.Calendar_CalculateScheduleDate(actualStart, priority.Value, alarm.KeyLocation);

			var brokerUser = Helper.CreateInstance<FOLMembershipUserBroker>();
			var user = brokerUser.GetItem(alarm.Creator);

			ActionRequest request = new ActionRequest {
				ActualStart = actualStart,
				ClassificationKeyParent = alarm.ClassificationKeyParent,
				ClassificationKeyChildren = alarm.ClassificationKeyChildren,
				Requestor = user.ProviderUserKey.KeyOccupant,
				PriorityID = int.Parse(priority.KeyPriority),
				ScheduleStart = scheduleDate,
				Description = description,
				KeyLocation = alarm.KeyLocation
			};
			var retVal = m_ServiceDeskBusinessLayer.Workflow_RaiseEvent(request, string.Empty, string.Empty);
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AlarmDefinition> AlarmDefinition_SaveStore(CrudStore<AlarmDefinition> store) {
			AlarmDefinitionBroker broker = Helper.CreateInstance<AlarmDefinitionBroker>();
			AlarmInstanceBroker instancebroker = Helper.CreateInstance<AlarmInstanceBroker>();

			var keys = store.destroy.Where(mvr => mvr.KeyAlarmDefinition != null).Select(mvr => mvr.KeyAlarmDefinition);
			var retVal = new JsonStore<AlarmDefinition>();

			// using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {IsolationLevel = IsolationLevel.ReadCommitted})) {
			instancebroker.DeleteLimitedNumberRowsByKeySource(const_numberofrowstodelete, keys, AlarmInstanceSource.AlarmDefinition);
			retVal = broker.SaveStore(store);
			//scope.Complete();
			//}

			return retVal;
		}

		/// <summary>
		/// Begins long running operation to Save a crud store for the business entity AlarmDefinition.
		/// </summary>
		/// <param name="operationId"></param>
		/// <param name="store">The crud store.</param>
		public void AlarmDefinition_BeginSaveStore(Guid operationId, CrudStore<AlarmDefinition> store) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				AlarmDefinition_SaveStore(store);
				return new LongRunningOperationResult(LongRunningOperationStatus.Completed);
			});
		}

		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview for the AlarmDefinition Classification..
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The parent entity.</param>
		/// <returns></returns>
		public List<TreeNode> AlarmDefinitionClassification_GetTree(string Key, ClassificationItem entity) {
			var broker = Helper.CreateInstance<ClassificationItemBroker>();
			List<TreeNode> retVal = broker.GetChildren(Key).ToList().GetTree();
			return retVal;
		}

		/// <summary>
		/// Deletes an existing business entity AlarmInstance.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool AlarmInstance_Delete(AlarmInstance item) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Deletes all existing alarm instances for a specific AlarmDefinition or MeterValidationRule.
		/// </summary>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <returns></returns>
		public bool AlarmInstance_DeleteAll(string KeyAlarmDefinition, string KeyMeterValidationRule) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			return broker.DeleteAll(KeyAlarmDefinition, KeyMeterValidationRule);
		}

		/// <summary>
		/// Begins long running operation to deletes all existing alarm instances for a specific AlarmDefinition or MeterValidationRule.
		/// </summary>
		/// <param name="operationId">The operation identifier.</param>
		/// <param name="KeyAlarmDefinition">The key alarm definition.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		public void AlarmInstance_BeginDeleteAll(Guid operationId, string KeyAlarmDefinition, string KeyMeterValidationRule) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				string keySource;
				AlarmInstanceSource alarmInstanceSource;
				if (!String.IsNullOrEmpty(KeyAlarmDefinition)) {
					keySource = KeyAlarmDefinition;
					alarmInstanceSource = AlarmInstanceSource.AlarmDefinition;
				}
				else {
					keySource = KeyMeterValidationRule;
					alarmInstanceSource = AlarmInstanceSource.MeterValidationRule;
				}
				var broker = Helper.CreateInstance<AlarmInstanceBroker>();
				broker.DeleteLimitedNumberRowsByKeySource(const_numberofrowstodelete, new List<string> { keySource }, alarmInstanceSource);
				return new LongRunningOperationResult(LongRunningOperationStatus.Completed);
			});
		}

		/// <summary>
		/// Creates a new business entity AlarmInstance and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse AlarmInstance_FormCreate(AlarmInstance item) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity AlarmInstance.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse AlarmInstance_FormLoad(string Key) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity AlarmInstance and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse AlarmInstance_FormUpdate(AlarmInstance item) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse AlarmInstance_FormUpdateBatch(string[] keys, AlarmInstance item) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity AlarmInstance.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<AlarmInstance> AlarmInstance_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity AlarmInstance.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public AlarmInstance AlarmInstance_GetItem(string Key, params string[] fields) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Get the latest AlarmInstances with an InstanceDatetime newer than Now - seconds.
		/// </summary>
		/// <param name="seconds">The seconds.</param>
		/// <returns></returns>
		public List<AlarmInstance> AlarmInstance_GetLatest(int seconds) {
			try {
				var broker = Helper.CreateInstance<AlarmInstanceBroker>();
				var paging = new PagingParameter { limit = 50 };
				return broker.GetLatest(seconds, paging);
			}
			catch (Exception) { ;}
			return new List<AlarmInstance>();
		}

		/// <summary>
		/// Gets a json store for the business entity AlarmInstance.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<AlarmInstance> AlarmInstance_GetStore(PagingParameter paging, PagingLocation location) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmDefinition">The Key of the AlarmDefinition.</param>
		public JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			var retVal = broker.GetStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
			foreach (var instance in retVal.records) {
				broker.PopulateList(instance, "AlarmDefinition");
			}
			return retVal;
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyAlarmTable">The Key of the AlarmTable.</param>
		public JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			var retVal = broker.GetStoreByKeyAlarmTable(paging, KeyAlarmTable);
			foreach (var instance in retVal.records) {
				broker.PopulateList(instance, "AlarmDefinition");
			}
			return retVal;
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific Meter.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeter">The key meter.</param>
		/// <returns></returns>
		public JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyMeter(PagingParameter paging, string KeyMeter) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			if (string.IsNullOrWhiteSpace(KeyMeter))
				return new JsonStore<AlarmInstance>();
			var retVal = broker.GetStoreByKeyMeter(paging, KeyMeter);
			foreach (var instance in retVal.records) {
				broker.PopulateList(instance, "MeterValidationRule");
			}
			return retVal;
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific MeterData.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterData">The key meter data.</param>
		/// <returns></returns>
		public JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyMeterData(PagingParameter paging, string KeyMeterData) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			if (string.IsNullOrWhiteSpace(KeyMeterData))
				return new JsonStore<AlarmInstance>();
			var retVal = broker.GetStoreByKeyMeterData(paging, KeyMeterData);
			foreach (var instance in retVal.records) {
				broker.PopulateList(instance, "MeterValidationRule");
			}
			return retVal;
		}

		/// <summary>
		/// Gets all the AlarmInstance for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameters.</param>
		/// <param name="KeyMeterValidationRule">The key meter validation rule.</param>
		/// <returns></returns>
		public JsonStore<AlarmInstance> AlarmInstance_GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			if (string.IsNullOrWhiteSpace(KeyMeterValidationRule))
				return new JsonStore<AlarmInstance>();
			var retVal = broker.GetStoreByKeyMeterValidationRule(paging, KeyMeterValidationRule);
			foreach (var instance in retVal.records) {
				broker.PopulateList(instance, "MeterValidationRule");
			}
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity AlarmInstance.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AlarmInstance> AlarmInstance_SaveStore(CrudStore<AlarmInstance> store) {
			AlarmInstanceBroker broker = Helper.CreateInstance<AlarmInstanceBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity AlarmTable.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool AlarmTable_Delete(AlarmTable item) {
			AlarmTableBroker broker = Helper.CreateInstance<AlarmTableBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity AlarmTable and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="alarmdefinitions">The alarmdefinitions.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <returns></returns>
		public FormResponse AlarmTable_FormCreate(AlarmTable item, CrudStore<AlarmDefinition> alarmdefinitions, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterAlarmDefinitionClassification) {
			FormResponse response;

			var broker = Helper.CreateInstance<AlarmTableBroker>();
			var brokerAlarmDefinition = Helper.CreateInstance<AlarmDefinitionBroker>();
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			var brokerClassification = Helper.CreateInstance<ClassificationItemBroker>();

			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						var alarmTable = response.data as AlarmTable;
						if (alarmTable != null) {
							brokerAlarmDefinition.SaveAlarmTableAlarmDefinitions(alarmTable, alarmdefinitions);
							brokerLocation.SaveAlarmTableFilterSpatial(alarmTable, filterSpatial);
							brokerClassification.SaveAlarmTableFilterAlarmDefinitionClassification(alarmTable, filterAlarmDefinitionClassification);
						}
						t.Complete();

					}
					catch (Exception ex) {
						response.msg = ex.Message;
						response.success = false;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity AlarmTable.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse AlarmTable_FormLoad(string Key) {
			AlarmTableBroker broker = Helper.CreateInstance<AlarmTableBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity AlarmTable and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="alarmdefinitions">The alarmdefinitions.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <returns></returns>
		public FormResponse AlarmTable_FormUpdate(AlarmTable item, CrudStore<AlarmDefinition> alarmdefinitions, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterAlarmDefinitionClassification) {
			FormResponse response;

			var broker = Helper.CreateInstance<AlarmTableBroker>();
			var brokerAlarmDefinition = Helper.CreateInstance<AlarmDefinitionBroker>();
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			var brokerClassification = Helper.CreateInstance<ClassificationItemBroker>();

			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						var alarmTable = response.data as AlarmTable;
						if (alarmTable != null) {
							brokerAlarmDefinition.SaveAlarmTableAlarmDefinitions(alarmTable, alarmdefinitions);
							brokerLocation.SaveAlarmTableFilterSpatial(alarmTable, filterSpatial);
							brokerClassification.SaveAlarmTableFilterAlarmDefinitionClassification(alarmTable, filterAlarmDefinitionClassification);
						}
						t.Complete();

					}
					catch (Exception ex) {
						response.msg = ex.Message;
						response.success = false;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse AlarmTable_FormUpdateBatch(string[] keys, AlarmTable item) {
			AlarmTableBroker broker = Helper.CreateInstance<AlarmTableBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Return a json store of the complete list of AlarmDefinitions associated with a AlarmTable
		/// including AlarmDefinitions associated via SpatialFilter  cross AlarmDefinitionClassification.
		/// </summary>
		/// <param name="KeyAlarmTable">The key alarm table.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<AlarmDefinition> AlarmTable_GetAlarmDefinitionsStore(string KeyAlarmTable, PagingParameter paging) {
			var meterAlarmDefinition = Helper.CreateInstance<AlarmDefinitionBroker>();
			var retVal = meterAlarmDefinition.GetCompleteListByKeyAlarmTable(KeyAlarmTable, "*");
			return retVal.ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Gets a list for the business entity AlarmTable. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<AlarmTable> AlarmTable_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			AlarmTableBroker broker = Helper.CreateInstance<AlarmTableBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity AlarmTable.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public AlarmTable AlarmTable_GetItem(string Key, params string[] fields) {
			AlarmTableBroker broker = Helper.CreateInstance<AlarmTableBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity AlarmTable.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<AlarmTable> AlarmTable_GetStore(PagingParameter paging, PagingLocation location) {
			AlarmTableBroker broker = Helper.CreateInstance<AlarmTableBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity AlarmTable.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AlarmTable> AlarmTable_SaveStore(CrudStore<AlarmTable> store) {
			AlarmTableBroker broker = Helper.CreateInstance<AlarmTableBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Checks if the formula is valid.
		/// </summary>
		/// <param name="KeyChart">The Key of the Chart the calculatedseries belong to.</param>
		/// <param name="field">the name of the field.</param>
		/// <param name="value">the value of the field.</param>
		/// <returns></returns>
		public FormResponse CalculatedSerie_CheckFormula(string KeyChart, string field, string value) {
			// TODO: finish this
			FormResponse response = new FormResponse { success = true };
			return response;
		}

		/// <summary>
		/// Deletes an existing business entity CalculatedSerie.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool CalculatedSerie_Delete(CalculatedSerie item) {
			CalculatedSerieBroker broker = Helper.CreateInstance<CalculatedSerieBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity CalculatedSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse CalculatedSerie_FormCreate(CalculatedSerie item) {
			CalculatedSerieBroker broker = Helper.CreateInstance<CalculatedSerieBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse CalculatedSerie_FormLoad(string Key) {
			CalculatedSerieBroker broker = Helper.CreateInstance<CalculatedSerieBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity CalculatedSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse CalculatedSerie_FormUpdate(CalculatedSerie item) {
			CalculatedSerieBroker broker = Helper.CreateInstance<CalculatedSerieBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse CalculatedSerie_FormUpdateBatch(string[] keys, CalculatedSerie item) {
			CalculatedSerieBroker broker = Helper.CreateInstance<CalculatedSerieBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<CalculatedSerie> CalculatedSerie_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			CalculatedSerieBroker broker = Helper.CreateInstance<CalculatedSerieBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public CalculatedSerie CalculatedSerie_GetItem(string Key, params string[] fields) {
			CalculatedSerieBroker broker = Helper.CreateInstance<CalculatedSerieBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<CalculatedSerie> CalculatedSerie_GetStore(PagingParameter paging, PagingLocation location) {
			CalculatedSerieBroker broker = Helper.CreateInstance<CalculatedSerieBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of calculatedSeries for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<CalculatedSerie> CalculatedSerie_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			CalculatedSerieBroker broker = Helper.CreateInstance<CalculatedSerieBroker>();
			return broker.GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity CalculatedSerie.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<CalculatedSerie> CalculatedSerie_SaveStore(CrudStore<CalculatedSerie> store) {
			CalculatedSerieBroker broker = Helper.CreateInstance<CalculatedSerieBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Add a classification item to an existing Chart(used in dragdrop of classification tree to chart).
		/// </summary>
		/// <param name="keyChart">The key chart.</param>
		/// <param name="key">The key of the entity.</param>
		/// <param name="type">The type of entity.</param>
		/// <returns></returns>
		public Chart Chart_AddClassificationItemEntity(string keyChart, string key, string type) {
			var entityType = Assembly.GetAssembly(typeof(BaseBusinessEntity)).GetType(type);
			var brokerChart = Helper.CreateInstance<ChartBroker>();
			var chart = brokerChart.GetItem(keyChart);

			Chart_ClearCache(keyChart, chart.KeyDynamicDisplay);

			if (entityType == typeof(ClassificationItem)) {
				var brokersContext = new ChartBrokersContext();
				var classification = brokersContext.ClassificationItemBroker.GetItem(key);
				if (brokersContext.ClassificationItemBroker.IsClassificationItemOfCategory(classification, ClassificationCategory.Meter)) {
					var filterMeterClassification = new CrudStore<ClassificationItem> { create = new List<ClassificationItem> { classification } };
					brokersContext.ClassificationItemBroker.SaveChartFilterMeterClassification(chart, filterMeterClassification);
				}
			}
			chart = brokerChart.GetItem(keyChart);

			ValidateMaxMetersFormResponse(chart);
			return chart;

		}

		private FormResponse ValidateMaxMetersFormResponse(Chart chart,
			CrudStore<Location> filterSpatial = null, CrudStore<ClassificationItem> filterMeterClassification = null, CrudStore<Meter> meters = null,
			FormResponse formResponse = null, bool customErrorHandling = false) {

			int resultingMetersCount = GetResultingMetersCount(chart, filterSpatial, filterMeterClassification, meters);
			var validator = new MaxMeterValidator(chart, resultingMetersCount, customErrorHandling, formResponse);
			return validator.ValidateFormResponse();
		}

		/// <summary>
		/// Add a spatial entity (Site, Building, Meter) to an existing Chart(used in dragdrop of spatial tree to chart).
		/// </summary>
		/// <param name="keyChart">The key chart.</param>
		/// <param name="key">The key of the entity.</param>
		/// <param name="type">The type of entity.</param>
		/// <returns></returns>
		public Chart Chart_AddSpatialEntity(string keyChart, string key, string type) {
			var entityType = Assembly.GetAssembly(typeof(BaseBusinessEntity)).GetType(type);
			var brokerChart = Helper.CreateInstance<ChartBroker>();
			var chart = brokerChart.GetItem(keyChart);

			Chart_ClearCache(keyChart, chart.KeyDynamicDisplay);
			var chartBrokersContext = new ChartBrokersContext();
			CrudStore<Location> crudSpatialEntities = null;
			CrudStore<Meter> crudMeters = null;

			if (typeof(ILocation).IsAssignableFrom(entityType)) {
				object brokerObject = BrokerFactory.CreateBroker(entityType);
				var broker = (IBusinessEntityBroker)brokerObject;
				var spatialEntity = ((ILocation)broker.GetItem(key)).GetLocation();
				crudSpatialEntities = new CrudStore<Location> {create = new List<Location> {spatialEntity}};
				chartBrokersContext.LocationBroker.SaveChartFilterSpatial(chart, crudSpatialEntities);
			}
			else if (entityType == typeof(Meter)) {
				var meter = chartBrokersContext.MeterBroker.GetItem(key);
				crudMeters = new CrudStore<Meter> {create = new List<Meter> {meter}};
				chartBrokersContext.MeterBroker.SaveChartMeters(chart, crudMeters);
			}
			chart = brokerChart.GetItem(keyChart);
			ValidateMaxMetersFormResponse(chart, meters: crudMeters, filterSpatial:crudSpatialEntities);
			return chart;
		}

		/// <summary>
		/// Check for the chart if AlarmInstance exists and highlight the corresponding datapoints.
		/// </summary>
		/// <param name="chart">The chart.</param>
		public void Chart_AssociateAlarmInstance(Chart chart) {
			var alarminstanceIconCls = Helper.GetAttributeValue<IconClsAttribute>(typeof(AlarmInstance));
			var brokerAlarmInsance = Helper.CreateInstance<AlarmInstanceBroker>();
			var brokerAlarmDefinition = Helper.CreateInstance<AlarmDefinitionBroker>();
			var instances = brokerAlarmInsance.GetListByKeyChart(chart.KeyChart);
			var defs = new Dictionary<string, AlarmDefinition>();
			foreach (var instance in instances) {
				AlarmDefinition alarmDefinition;
				if (!defs.TryGetValue(instance.KeyAlarmDefinition, out  alarmDefinition)) {
					alarmDefinition = brokerAlarmDefinition.GetItem(instance.KeyAlarmDefinition);
					defs.Add(instance.KeyAlarmDefinition, alarmDefinition);
				}

				var serie = chart.Series.FindByLocalId(instance.DataSerieLocalId);
				if (serie != null) {
					var points = serie.PointsWithValue.Where(p => p.XDateTime != null && p.XDateTime.Value.ToUniversalTime() == instance.InstanceDateTime && Math.Abs(p.YValue.Value - instance.Value) < 0.001).ToList();
					foreach (var p in points) {
						p.Highlight = DataPointHighlight.Hatch;
						p.HighlightColor = ColorHelper.GetHTMLColor(alarmDefinition.GetColor());
						if (string.IsNullOrEmpty(p.Tooltip)) {
							p.Tooltip = "";
						}
						p.Tooltip += string.Format("{1} <div class='viz-icon-cell {0}'/>      ", alarminstanceIconCls, instance.Title);
					}
				}
			}
		}

		/// <summary>
		/// Builds the image (with map) from a chart.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public ImageMap Chart_BuildImage(string Key, int width, int height) {
			var tuple = Chart_GetStreamBase(Key, width, height);
			return ChartingService.RenderChartImageMap(tuple.Item1, width, height, tuple.Item2, tuple.Item3);
		}

		/// <summary>
		/// Clear the cache of any information related to this Charts.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="KeyDynamicDisplay">The key dynamic display.</param>
		public void Chart_ClearCache(string Key, string KeyDynamicDisplay = null) {
			ChartHelper.Chart_ClearCache(Key, KeyDynamicDisplay);
		}

		/// <summary>
		/// Copy existing business entity Chart.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy </param>
		/// <returns></returns>
		public FormResponse Chart_Copy(string[] keys) {
			var response = new FormResponse { success = true };
			var broker = Helper.CreateInstance<ChartBroker>();
			try {
				foreach (string key in keys) {
					var item = broker.GetItem(key, "*");
					item = broker.Copy(item, out response, incrementTitle: true);
				}
			}
			catch (Exception ex) {
				response.success = false;
				response.msg = ex.Message;
			}
			return response;
		}

		/// <summary>
		/// Remove (or add back)  a point from the Correlation analysis.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="xValue">The x value.</param>
		/// <param name="yValue">The y value.</param>
		/// <param name="LocalId">The local id of the cloud serie.</param>
		/// <returns></returns>
		public Chart Chart_CorrelationRemovePoint(string KeyChart, double xValue, double yValue, string LocalId) {
			Chart_ClearCache(KeyChart);
			var broker = Helper.CreateInstance<ChartCorrelationRemovedDataPointBroker>();
			var dataPoint = new ChartCorrelationRemovedDataPoint {
				XValue = xValue,
				YValue = yValue,
				KeyChart = KeyChart,
				LocalId = LocalId
			};
			var list = broker.GetListByKeyChart(KeyChart);

			ChartCorrelationRemovedDataPoint found = null;
			foreach (var dp in list.Where(dp => dp.LocalId == LocalId && Math.Abs(dp.YValue - yValue) < 0.001 && Math.Abs(dp.XValue - xValue) < 0.001)) {
				found = dp;
			}
			if (found != null) {
				broker.Delete(found.KeyChartCorrelationRemovedDataPoint);
			}
			else {
				broker.Save(dataPoint);
			}

			var brokerChart = Helper.CreateInstance<ChartBroker>();
			var retVal = brokerChart.GetItem(KeyChart);
			return retVal;
		}

		/// <summary>
		/// Get a chart from a single Classification.
		/// </summary>
		/// <param name="Key">The key of the ClassificationItem.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="KeyChartToCopy">The key chart to copy.</param>
		/// <returns></returns>
		private Chart Chart_CreateFromClassificationItem(string Key, string TimeZoneId = null, string KeyChartToCopy = null) {
			var brokerClassificationItem = Helper.CreateInstance<ClassificationItemBroker>();
			var brokerChart = Helper.CreateInstance<ChartBroker>();
			var classifications = new List<ClassificationItem> { brokerClassificationItem.GetItem(Key) };

			var chart = Chart_CreateFrom(KeyChartToCopy, true);

			chart.DynamicTimeScaleEnabled = true;
			chart.DynamicTimeScaleFrequency = 1;
			chart.DynamicTimeScaleInterval = AxisTimeInterval.Years;
			chart.TimeInterval = AxisTimeInterval.Months;
			chart.Title = classifications[0].Title;
			chart.ClassificationLevel = ClassificationItem_GetMeterMaxLevel();
			chart.TimeZoneId = TimeZoneId;

			chart = brokerChart.Save(chart);
			brokerClassificationItem.SaveChartFilterMeterClassification(chart, new CrudStore<ClassificationItem> { create = classifications });
			return chart;
		}

		/// <summary>
		/// Get a chart from a single Location.
		/// </summary>
		/// <param name="Key">The key of the Location.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="KeyChartToCopy">The key chart to copy.</param>
		/// <returns></returns>
		private Chart Chart_CreateFromLocation(string Key, string TimeZoneId = null, string KeyChartToCopy = null) {
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			var brokerChart = Helper.CreateInstance<ChartBroker>();
			var locations = new List<Location> { brokerLocation.GetItem(Key) };

			var chart = Chart_CreateFrom(KeyChartToCopy, true);

			chart.DynamicTimeScaleEnabled = true;
			chart.DynamicTimeScaleFrequency = 1;
			chart.DynamicTimeScaleInterval = AxisTimeInterval.Years;
			chart.TimeInterval = AxisTimeInterval.Months;
			chart.Title = locations[0].Name;
			chart.ClassificationLevel = ClassificationItem_GetMeterMaxLevel();
			chart.TimeZoneId = TimeZoneId;

			chart = brokerChart.Save(chart);
			brokerLocation.SaveChartFilterSpatial(chart, new CrudStore<Location>() { create = locations });
			return chart;
		}

		/// <summary>
		/// Get a chart from a single Meter.
		/// </summary>
		/// <param name="Key">The key of the Meter.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="save">if set to <c>true</c> [save].</param>
		/// <param name="KeyChartToCopy">The key chart to copy.</param>
		/// <returns></returns>
		private Chart Chart_CreateFromMeter(string Key, out List<Meter> meters, string TimeZoneId = null, bool save = true, string KeyChartToCopy = null) {
			var brokerMeter = Helper.CreateInstance<MeterBroker>();
			var brokerChart = Helper.CreateInstance<ChartBroker>();
			meters = new List<Meter> { brokerMeter.GetItem(Key, "*") };

			var chart = Chart_CreateFrom(KeyChartToCopy, save);

			chart.DynamicTimeScaleEnabled = true;
			chart.DynamicTimeScaleFrequency = 1;
			chart.DynamicTimeScaleInterval = AxisTimeInterval.Years;
			chart.ClassificationLevel = ClassificationItem_GetMeterMaxLevel();
			chart.TimeInterval = AxisTimeInterval.Months;
			chart.Title = meters[0].Name;
			chart.TimeZoneId = TimeZoneId;

			if (save) {
				chart = brokerChart.Save(chart);
				brokerMeter.SaveChartMeters(chart, new CrudStore<Meter>() { create = meters });
			}
			chart.MetersCount = meters.Count;
			return chart;
		}


		/// <summary>
		/// Either create a new empty Chart or use the passed KPI key to create.
		/// </summary>
		/// <param name="KeyChartToCopy">The key chart to copy.</param>
		/// <param name="save">if set to <c>true</c> [save].</param>
		/// <returns></returns>
		private Chart Chart_CreateFrom(string KeyChartToCopy, bool save = true) {
			var chart = new Chart();
			var brokerChart = Helper.CreateInstance<ChartBroker>();
			if (string.IsNullOrEmpty(KeyChartToCopy) == false) {
				chart = brokerChart.GetItem(KeyChartToCopy, "*");
				if (chart != null) {
					chart.IsKPI = false;
					chart.LocalId = "";
					chart.Creator = null;
					if (save) {
						FormResponse response;
						chart = brokerChart.Copy(chart, out response);
						chart = brokerChart.ClearDataSource(chart, true, true, true);
					}
				}
			}
			return chart;
		}
		/// <summary>
		/// Deletes an existing business entity Chart.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Chart_Delete(Chart item) {
			ChartBroker broker = Helper.CreateInstance<ChartBroker>();
			return broker.Delete(item);
		}


		/// <summary>
		/// Creates a new business entity Chart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <param name="filterEventLogClassification">The filter event log classification.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <param name="filterAzManRoles">The filter KPI az man roles.</param>
		/// <param name="meters">The store of the meters of the chart.</param>
		/// <param name="dataseries">The store of the dataseries of the Chart.</param>
		/// <param name="calculatedseries">The store of the calculatedseries of the Chart.</param>
		/// <param name="statisticalseries">The statisticalseries.</param>
		/// <param name="chartaxis">The store of chartaxis of the Chart.</param>
		/// <param name="chartmarker">The store of chartmarker of the Chart.</param>
		/// <param name="charthistoricalanalysis">the store of ChartHistoricalAnalysis of the Chart.</param>
		/// <param name="historicals">The historicals.</param>
		/// <param name="algorithms">The algorithms.</param>
		/// <param name="keyChartToCopy">The key chart to copy (coming from the Chart wizard KPI).</param>
		/// <returns></returns>
		public FormResponse Chart_FormCreate(Chart item, CrudStore<Location> filterSpatial,
			CrudStore<ClassificationItem> filterMeterClassification,
			CrudStore<ClassificationItem> filterEventLogClassification,
			CrudStore<ClassificationItem> filterAlarmDefinitionClassification,
			CrudStore<AuthorizationItem> filterAzManRoles, CrudStore<Meter> meters, CrudStore<DataSerie> dataseries,
			CrudStore<CalculatedSerie> calculatedseries, CrudStore<StatisticalSerie> statisticalseries,
			CrudStore<ChartAxis> chartaxis,
			CrudStore<ChartMarker> chartmarker, CrudStore<ChartHistoricalAnalysis> charthistoricalanalysis,
			CrudStore<ChartPsetAttributeHistorical> historicals,
			CrudStore<ChartAlgorithm> algorithms,
			string keyChartToCopy) {

			//If MaxMeter is sent with 0 value, we assume null was sent and set it to it's default value(100).
			if (item.MaxMeter == 0) {
				item.MaxMeter = Chart.const_max_meter_default_value;
			}

			FormResponse response;

			if (string.IsNullOrEmpty(keyChartToCopy)) {
				response = ValidateMaxMetersFormResponse(item, filterSpatial, filterMeterClassification, meters);
				if (response != null && !response.success) {
					return response;
				}
			}

			using (var t = Helper.CreateTransactionScope()) {
				response = CreateOrGetChartForm(item, keyChartToCopy);

				SaveChartAdditionalContext(filterSpatial, filterMeterClassification, filterEventLogClassification,
					filterAlarmDefinitionClassification,
					filterAzManRoles, meters, dataseries, calculatedseries, statisticalseries, chartaxis, chartmarker,
					charthistoricalanalysis,
					null, historicals, algorithms, response, t);
			}
			return response;
		}

		/// <summary>
		/// Creates or gets Chart Form.
		/// </summary>
		/// <param name="item"></param>
		/// <param name="keyChartToCopy"></param>
		/// <returns></returns>
		private static FormResponse CreateOrGetChartForm(Chart item, string keyChartToCopy) {
			var chartBrokersContext = new ChartBrokersContext();
			FormResponse response;
			if (string.IsNullOrEmpty(keyChartToCopy)) {
				response = chartBrokersContext.ChartBroker.FormCreate(item);
			}
			else {
				var chartToCopy = chartBrokersContext.ChartBroker.GetItem(keyChartToCopy, "*");
				chartToCopy.StartDate = item.StartDate;
				chartToCopy.EndDate = item.EndDate;
				chartToCopy.TimeZoneId = item.TimeZoneId;
				chartToCopy.Description = item.Description;
				chartToCopy.IsFavorite = item.IsFavorite;
				chartToCopy.IsKPI = false;
				chartToCopy.LocalId = "";
				chartToCopy.Creator = null;
				var chartNew = chartBrokersContext.ChartBroker.Copy(chartToCopy, out response);

				chartNew.Title = item.Title;
				chartBrokersContext.ChartBroker.Save(chartNew);

				response.data = chartBrokersContext.ChartBroker.ClearDataSource(chartNew, !chartToCopy.KPIDisableMeterSelection,
					!chartToCopy.KPIDisableSpatialSelection, !chartToCopy.KPIDisableMeterClassificationSelection);
			}
			return response;
		}




		/// <summary>
		/// Creates a new business entity Chart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public FormResponse Chart_FormCreate(Chart chart) {
			return Chart_FormCreate(chart,
				chart.FilterSpatial.AsCrudStore(), chart.MetersClassifications.AsCrudStore(), chart.EventLogClassifications.AsCrudStore(),
				chart.AlarmDefinitionsClassifications.AsCrudStore(), chart.Roles.AsCrudStore(), chart.Meters.AsCrudStore(), chart.Series.ToList().AsCrudStore(),
				chart.CalculatedSeries.AsCrudStore(), chart.StatisticalSeries.AsCrudStore(), chart.Axis.Values.ToList().AsCrudStore(),
				chart.ChartMarkers.AsCrudStore(), chart.HistoricalAnalysisDefinitions.AsCrudStore(), chart.HistoricalPsets.AsCrudStore(),
				chart.Algorithms.AsCrudStore(), null);
		}

		/// <summary>
		/// Loads a specific item for the business entity Chart.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Chart_FormLoad(string Key) {
			var broker = Helper.CreateInstance<ChartBroker>();
			var retVal = broker.FormLoad(Key);
			if (retVal.success) {
				ValidateMaxMetersFormResponse(retVal.data as Chart, formResponse: retVal, customErrorHandling: true);
			}
			return retVal;
		}

		/// <summary>
		/// Holds the chart brokers entities.
		/// </summary>
		private class ChartBrokersContext {

			private ChartBroker m_ChartBroker;
			public ChartBroker ChartBroker {
				get { return m_ChartBroker ?? (m_ChartBroker = Helper.CreateInstance<ChartBroker>()); }
			}

			private MeterBroker m_MeterBroker;
			public MeterBroker MeterBroker {
				get { return m_MeterBroker ?? (m_MeterBroker = Helper.CreateInstance<MeterBroker>()); }
			}

			private DataSerieBroker m_DataSerieBroker;
			public DataSerieBroker DataSerieBroker {
				get { return m_DataSerieBroker ?? (m_DataSerieBroker = Helper.CreateInstance<DataSerieBroker>()); }
			}

			private ChartAxisBroker m_ChartAxisBroker;
			public ChartAxisBroker ChartAxisBroker {
				get { return m_ChartAxisBroker ?? (m_ChartAxisBroker = Helper.CreateInstance<ChartAxisBroker>()); }
			}

			private ChartMarkerBroker m_ChartMarkerBroker;
			public ChartMarkerBroker ChartMarkerBroker {
				get { return m_ChartMarkerBroker ?? (m_ChartMarkerBroker = Helper.CreateInstance<ChartMarkerBroker>()); }
			}


			private CalculatedSerieBroker m_CalculatedSerieBroker;
			public CalculatedSerieBroker CalculatedSerieBroker {
				get { return m_CalculatedSerieBroker ?? (m_CalculatedSerieBroker = Helper.CreateInstance<CalculatedSerieBroker>()); }
			}


			private StatisticalSerieBroker m_StatisticalSerieBroker;
			public StatisticalSerieBroker StatisticalSerieBroker {
				get { return m_StatisticalSerieBroker ?? (m_StatisticalSerieBroker = Helper.CreateInstance<StatisticalSerieBroker>()); }
			}


			private LocationBroker m_LocationBroker;
			public LocationBroker LocationBroker {
				get { return m_LocationBroker ?? (m_LocationBroker = Helper.CreateInstance<LocationBroker>()); }
			}


			private ClassificationItemBroker m_ClassificationItemBroker;
			public ClassificationItemBroker ClassificationItemBroker {
				get { return m_ClassificationItemBroker ?? (m_ClassificationItemBroker = Helper.CreateInstance<ClassificationItemBroker>()); }
			}

			private ChartHistoricalAnalysisBroker m_ChartHistoricalAnalysisBroker;
			public ChartHistoricalAnalysisBroker ChartHistoricalAnalysisBroker {
				get { return m_ChartHistoricalAnalysisBroker ?? (m_ChartHistoricalAnalysisBroker = Helper.CreateInstance<ChartHistoricalAnalysisBroker>()); }
			}


			private FilterSpatialPsetBroker m_FilterSpatialPsetBroker;
			public FilterSpatialPsetBroker FilterSpatialPsetBroker {
				get { return m_FilterSpatialPsetBroker ?? (m_FilterSpatialPsetBroker = Helper.CreateInstance<FilterSpatialPsetBroker>()); }
			}


			private ChartPsetAttributeHistoricalBroker m_ChartPsetAttributeHistoricalBroker;
			public ChartPsetAttributeHistoricalBroker ChartPsetAttributeHistoricalBroker {
				get {
					return m_ChartPsetAttributeHistoricalBroker ??
						   (m_ChartPsetAttributeHistoricalBroker = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>());
				}
		}

			private AuthorizationItemBroker m_AuthorizationItemBroker;
			public AuthorizationItemBroker AuthorizationItemBroker {
				get {
					return m_AuthorizationItemBroker ?? (m_AuthorizationItemBroker = Helper.CreateInstance<AuthorizationItemBroker>());
				}
			}

			private ChartAlgorithmBroker m_ChartAlgorithmBroker;
			public ChartAlgorithmBroker ChartAlgorithmBroker {
				get { return m_ChartAlgorithmBroker ?? (m_ChartAlgorithmBroker = Helper.CreateInstance<ChartAlgorithmBroker>()); }
			}
			public ChartBrokersContext() {
			}
		}

		/// <summary>
		/// Updates an existing business entity Chart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <param name="filterEventLogClassification">The filter event log classification.</param>
		/// <param name="filterAlarmDefinitionClassification">The filter alarm definition classification.</param>
		/// <param name="filterAzManRoles">The filter KPI az man roles.</param>
		/// <param name="meters">The store of the meters of the Chart.</param>
		/// <param name="dataseries">The store of the dataseries of the Chart.</param>
		/// <param name="calculatedseries">The store of the calculatedseries of the Chart.</param>
		/// <param name="statisticalseries">The statisticalseries.</param>
		/// <param name="chartaxis">The store of chartaxis of the Chart.</param>
		/// <param name="chartmarker">The store of chartmarker of the Chart.</param>
		/// <param name="charthistoricalanalysis">the store of ChartHistoricalAnalysis of the Chart.</param>
		/// <param name="chartfilterpset">The pset filters associated to the chart.</param>
		/// <param name="historicals">The historicals.</param>
		/// <param name="algorithms">The Algorithms.</param>
		/// <returns></returns>
		public FormResponse Chart_FormUpdate(Chart item,
			CrudStore<Location> filterSpatial,
			CrudStore<ClassificationItem> filterMeterClassification,
			CrudStore<ClassificationItem> filterEventLogClassification,
			CrudStore<ClassificationItem> filterAlarmDefinitionClassification,
			CrudStore<AuthorizationItem> filterAzManRoles,
			CrudStore<Meter> meters,
			CrudStore<DataSerie> dataseries,
			CrudStore<CalculatedSerie> calculatedseries,
			CrudStore<StatisticalSerie> statisticalseries,
			CrudStore<ChartAxis> chartaxis, CrudStore<ChartMarker> chartmarker,
			CrudStore<ChartHistoricalAnalysis> charthistoricalanalysis,
			CrudStore<FilterSpatialPset> chartfilterpset,
			CrudStore<ChartPsetAttributeHistorical> historicals,
			CrudStore<ChartAlgorithm> algorithms) {

			var chartBrokersContext = new ChartBrokersContext();
			FormResponse response = ValidateMaxMetersFormResponse(item, filterSpatial, filterMeterClassification, meters);
			if (response != null && !response.success) {
				return response;
			}

				using (var t = Helper.CreateTransactionScope()) {
					response = chartBrokersContext.ChartBroker.FormUpdate(item);

					SaveChartAdditionalContext(filterSpatial, filterMeterClassification, filterEventLogClassification, filterAlarmDefinitionClassification,
						filterAzManRoles, meters, dataseries, calculatedseries, statisticalseries, chartaxis, chartmarker, charthistoricalanalysis,
					chartfilterpset, historicals, algorithms, response, t);
				}
			return response;
			}

		/// <summary>
		/// Returns the requested chart. Unlike UpdateChart method, which updates all the Chart additional information(meters, spatial hierarchy, classification),
		/// this methos updates only the core Chart entity.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public FormResponse Chart_ChangeView(Chart item) {

			var chartBrokersContext = new ChartBrokersContext();
			FormResponse response = chartBrokersContext.ChartBroker.FormUpdate(item);
			return response;
		}

		/// <summary>
		/// Saves additional chart context to the persistence.
		/// </summary>
		/// <param name="filterSpatial"></param>
		/// <param name="filterMeterClassification"></param>
		/// <param name="filterEventLogClassification"></param>
		/// <param name="filterAlarmDefinitionClassification"></param>
		/// <param name="filterAzManRoles"></param>
		/// <param name="meters"></param>
		/// <param name="dataseries"></param>
		/// <param name="calculatedseries"></param>
		/// <param name="statisticalseries"></param>
		/// <param name="chartaxis"></param>
		/// <param name="chartmarker"></param>
		/// <param name="charthistoricalanalysis"></param>
		/// <param name="chartfilterpset"></param>
		/// <param name="historicals"></param>
		/// <param name="algorithms"></param>
		/// <param name="response"></param>
		/// <param name="t"></param>
		private void SaveChartAdditionalContext(CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification,
			CrudStore<ClassificationItem> filterEventLogClassification, CrudStore<ClassificationItem> filterAlarmDefinitionClassification, CrudStore<AuthorizationItem> filterAzManRoles,
			CrudStore<Meter> meters, CrudStore<DataSerie> dataseries, CrudStore<CalculatedSerie> calculatedseries, CrudStore<StatisticalSerie> statisticalseries, CrudStore<ChartAxis> chartaxis,
			CrudStore<ChartMarker> chartmarker, CrudStore<ChartHistoricalAnalysis> charthistoricalanalysis, CrudStore<FilterSpatialPset> chartfilterpset, CrudStore<ChartPsetAttributeHistorical> historicals,
			CrudStore<ChartAlgorithm> algorithms, FormResponse response, TransactionScope t) {

			if (response.success) {
				try {
					var chart = response.data as Chart;
					if (chart != null) {
						var chartBrokersContext = new ChartBrokersContext();
						chartBrokersContext.MeterBroker.SaveChartMeters(chart, meters);
						chartBrokersContext.DataSerieBroker.SaveChartDataSeries(chart, dataseries);
						chartBrokersContext.ChartAxisBroker.SaveChartChartAxis(chart, chartaxis);
						chartBrokersContext.ChartMarkerBroker.SaveChartChartMarkers(chart, chartmarker);
						chartBrokersContext.CalculatedSerieBroker.SaveChartCalculatedSeries(chart, calculatedseries);
						chartBrokersContext.StatisticalSerieBroker.SaveChartStatisticalSeries(chart, statisticalseries);
						chartBrokersContext.LocationBroker.SaveChartFilterSpatial(chart, filterSpatial);
						chartBrokersContext.ClassificationItemBroker.SaveChartFilterMeterClassification(chart, filterMeterClassification);
						chartBrokersContext.ClassificationItemBroker.SaveChartFilterEventLogClassification(chart, filterEventLogClassification);
						chartBrokersContext.ClassificationItemBroker.SaveChartFilterAlarmDefinitionClassification(chart,
							filterAlarmDefinitionClassification);

						chartBrokersContext.ChartHistoricalAnalysisBroker.SaveChartChartHistoricalAnalysis(chart, charthistoricalanalysis);

						//Will contain a null value when creating entity
						if (chartBrokersContext.FilterSpatialPsetBroker != null) {
							chartBrokersContext.FilterSpatialPsetBroker.SaveChartFilterSpatialPset(chart, chartfilterpset);
						}

						chartBrokersContext.ChartPsetAttributeHistoricalBroker.SaveChartChartPsetAttributeHistorical(chart, historicals);
						chartBrokersContext.AuthorizationItemBroker.SaveChartAuthorizationItem(chart, filterAzManRoles);
						chartBrokersContext.ChartAlgorithmBroker.SaveChartChartAlgorithm(chart, algorithms);
						Chart_GenerateYAxis(chart);
					}
					t.Complete();
				}
				catch (Exception ex) {
					response.success = false;
					response.msg = ex.Message;
				}
			}
		}
	
		/// <summary>
		/// Gettiing the count of the resulting meters corresponding to meters, filterSpatial and classification parameters.
		/// </summary>
		/// <param name="item">Existing chart.</param>
		/// <param name="filterSpatial">Modified filter spatial.</param>
		/// <param name="filterMeterClassification">Modified filter meter classifications.</param>
		/// <param name="meters">Modified meters.</param>
		/// <param name="loadFromPersistance">Indicator whether to load the collections from persistance.</param>
		/// <returns></returns>
		public int GetResultingMetersCount(Chart item, CrudStore<Location> filterSpatial = null, CrudStore<ClassificationItem> filterMeterClassification = null, CrudStore<Meter> meters = null,
			bool loadFromPersistance = true) {
			var brokersContext = new ChartBrokersContext();
			var paging = new PagingParameter { limit = 50 };

			var metersStore = loadFromPersistance
				? brokersContext.MeterBroker.GetListByKeyChart(item.KeyChart)
				: item.Meters;
			var resultingMeters = Helper.GetResultingEntities(meters, metersStore, p => p.KeyMeter);

			var filterSpatialStore = loadFromPersistance
				? brokersContext.LocationBroker.GetFilterSpatialStoreByKeyChart(paging, item.KeyChart).records
				: item.FilterSpatial;
			var resultingFiltersSpatial = Helper.GetResultingEntities<Location>(filterSpatial, filterSpatialStore, p => p.KeyLocation);

			var clasificationStore = loadFromPersistance
				? brokersContext.ClassificationItemBroker.GetFilterMeterClassificationStoreByKeyChart(paging, item.KeyChart).records
				: item.MetersClassifications;
			var resultingClassifications = Helper.GetResultingEntities(filterMeterClassification, clasificationStore, p => p.KeyClassificationItem);

			var list = brokersContext.MeterBroker.GetCompleteListByKeyChart(item.KeyChart, false,
				resultingFiltersSpatial,
				resultingClassifications,
				resultingMeters);

			return list != null ? list.Count : 0;
		}


		/// <summary>
		/// Chart_s the form update.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public FormResponse Chart_FormUpdate(Chart chart) {
			return Chart_FormUpdate(chart,
				chart.FilterSpatial.AsCrudStore(CrudAction.Update), chart.MetersClassifications.AsCrudStore(CrudAction.Update),
				chart.EventLogClassifications.AsCrudStore(CrudAction.Update), chart.AlarmDefinitionsClassifications.AsCrudStore(CrudAction.Update),
				chart.Roles.AsCrudStore(CrudAction.Update), chart.Meters.AsCrudStore(CrudAction.Update), chart.Series.ToList().AsCrudStore(CrudAction.Update),
				chart.CalculatedSeries.AsCrudStore(CrudAction.Update), chart.StatisticalSeries.AsCrudStore(CrudAction.Update),
				chart.Axis.Values.ToList().AsCrudStore(CrudAction.Update), chart.ChartMarkers.AsCrudStore(CrudAction.Update),
				chart.HistoricalAnalysisDefinitions.AsCrudStore(CrudAction.Update), chart.FilterSpatialPset.AsCrudStore(CrudAction.Update),
				chart.HistoricalPsets.AsCrudStore(CrudAction.Update), chart.Algorithms.AsCrudStore(CrudAction.Update));
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Chart_FormUpdateBatch(string[] keys, Chart item) {
			ChartBroker broker = Helper.CreateInstance<ChartBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Check if the Chart has a YAxis associated to it, and generates it in case the Axis doesnt exist.
		/// </summary>
		/// <param name="item">The chart item.</param>
		private void Chart_GenerateYAxis(Chart item) {
			ChartAxisBroker brokerAxis = Helper.CreateInstance<ChartAxisBroker>();
			var axisList = brokerAxis.GetListByKeyChart(item.KeyChart);

			if (axisList == null || axisList.Count == 0 || !axisList.Any(a => a.IsPrimary)) {
				var YAxis = new ChartAxis { KeyChart = item.KeyChart, Name = "Y", IsPrimary = true, LocalId = item.LocalId + "Y" };
				brokerAxis.FormCreate(YAxis);
			}

		}

		/// <summary>
		/// Gets a list for the business entity Chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Chart> Chart_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			ChartBroker broker = Helper.CreateInstance<ChartBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Return a store of available series localid for a specific Chart.
		/// </summary>
		/// <param name="Key">The Key of the Chart.</param>
		/// <param name="includeExisting">True to include existing serie, false to return only series to be created.</param>
		/// <returns></returns>
		public JsonStore<ListElement> Chart_GetAvailableDataSerieLocalId(string Key, bool includeExisting) {
			if (string.IsNullOrEmpty(Key))
				return new JsonStore<ListElement>();
			var cacheKey = ChartHelper.GetCacheKey_AvailableDataSerieLocalId(Key, includeExisting);
			var retVal = CacheService.Get(cacheKey, () => {
				var value = new List<ListElement>();
				using (var energyAggregatorServiceAgent = GetEnergyAggregatorServiceAgent()) {
					var chart = Chart_GetItem(Key, "*");

					var meters = Chart_GetMeters(Key, true);
					var meterKeys = meters.Select(m => MeterHelper.ConvertMeterKeyToInt(m.KeyMeter)).ToList();

					//var locations = Chart_GetLocations(Key, meters);
					var locationKeys = Chart_GetLocationKeys(Key, meters);

					var listLocalId = energyAggregatorServiceAgent.Chart_GetDataSerieLocalId(chart, meterKeys, locationKeys, null, includeExisting);
					value.AddRange(listLocalId);

					#region Historical Analysis

					var brokerHistoricalAnalysis = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
					var historicalAnalysisList = brokerHistoricalAnalysis.GetListByKeyChart(chart.KeyChart);
					foreach (var historicalAnalysis in historicalAnalysisList.Where(historicalAnalysis => historicalAnalysis.Enabled)) {
						ChartHelper.UpdateChartHistoricalAnalysisDates(chart, historicalAnalysis);
						listLocalId = energyAggregatorServiceAgent.Chart_GetDataSerieLocalId(chart, meterKeys, locationKeys, historicalAnalysis, includeExisting);
						value.AddRange(listLocalId);
					}

					#endregion

					return value.Count > 0 ? value : null;
				}
			}, 15);

			return retVal != null ? retVal.ToJsonStore() : new List<ListElement>().ToJsonStore();
		}

		/// <summary>
		/// Return the Chart Classification level store.
		/// </summary>
		/// <returns></returns>
		public JsonStore<SimpleListElementGeneric<int>> Chart_GetClassificationLevelStore() {
			ClassificationItemBroker classificationBroker = Helper.CreateInstance<ClassificationItemBroker>();
			int maxLevel = classificationBroker.MeterClassification_GetMaxLevel();
			const int rootMeterLevel = 1;

			var retVal = new List<SimpleListElementGeneric<int>>();
			for (int i = rootMeterLevel; i <= maxLevel; i++) {
				retVal.Add(new SimpleListElementGeneric<int> {
					Id = i.ToString(CultureInfo.InvariantCulture),
					Value = i,
					Order = i
				});
			}
			return retVal.ToJsonStore();
		}

		///// <summary>
		///// Gets a specific item for the business entity Chart.
		///// </summary>
		///// <param name="Key">The entity Key.</param>
		///// <returns></returns>
		//public Chart Chart_GetItem(string Key, params string[] fields) {
		//    var broker = Helper.CreateInstance<ChartBroker>();
		//    return broker.GetItem(Key, fields);
		//}

		/// <summary>
		/// Gets a specific item for the business entity Chart.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Chart Chart_GetItem(string Key, params string[] fields) {
			ChartBroker broker = Helper.CreateInstance<ChartBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity Chart and fetch data to create the series.
		/// </summary>
		/// <param name="key">The entity Key.</param>
		/// <param name="clearChartCache">if set to <c>true</c> [clear chart cache].</param>
		/// <param name="useCache">if set to <c>true</c> [use cache].</param>
		/// <param name="chart">The chart.</param>
		/// <returns>
		/// The Chart Item
		/// </returns>
		public Chart Chart_GetItemWithData(string key, bool clearChartCache = false, bool useCache = true, Chart chart = null) {
			var cacheKey = ChartHelper.GetCacheKey_ChartWithData(key);

			if (clearChartCache) {
				Chart_ClearCache(key);
			}

			Chart processedChart;
			if (useCache) {
				processedChart = CacheService.Get(cacheKey, () => ChartProcess(key, chart),
					chartObject => (chartObject.Series != null && !chartObject.IsEnergyAggregatorLoadingMeterData));
			}
			else {
				processedChart = ChartProcess(key, chart);
			}

			processedChart = processedChart.Clone();
			Chart_ApplySortOrder(processedChart);
			Chart_ApplyPalette(processedChart);
			Chart_UpdateAlgorithmInputValues(processedChart);

			return processedChart;
		}

		private Chart ChartProcess(string key, Chart chart = null) {
			using (var energyAggregatorServiceAgent = GetEnergyAggregatorServiceAgent()) {
				return energyAggregatorServiceAgent.Chart_Process(key, chart);
			}
		}

		/// <summary>
		/// Gets a specific item for the business entity Chart and fetch data to create the series.
		/// </summary>
		/// <param name="Keys">The keys.</param>
		/// <returns>
		/// The Chart Item
		/// </returns>
		public List<Chart> Chart_GetItemWithData(List<string> Keys) {
			var retVal = new List<Chart>();
			using (var energyAggregatorServiceAgent = GetEnergyAggregatorServiceAgent()) {
				foreach (var key in Keys) {
					var cacheKey = ChartHelper.GetCacheKey_ChartWithData(key);
					var chart = CacheService.Get(cacheKey, () => energyAggregatorServiceAgent.Chart_Process(key),
												 result => result.Series != null && !result.IsEnergyAggregatorLoadingMeterData);
					if (chart == null) continue;
					var newChart = chart.Clone();
					Chart_ApplySortOrder(newChart);
					Chart_ApplyPalette(newChart);
					Chart_UpdateAlgorithmInputValues(newChart);
					if (string.IsNullOrEmpty(newChart.KeyDynamicDisplay) == false && newChart.UseDynamicDisplay) {
						DynamicDisplay_Prepare(newChart.KeyDynamicDisplay, newChart);
					}
					retVal.Add(newChart);
				}
			}
			return retVal;
		}

		/// <summary>
		/// Applies the Chart Legend Sort Order.
		/// </summary>
		/// <param name="chart">The chart.</param>
		public void Chart_ApplySortOrder(Chart chart) {
			if (chart.Series != null) {
				var sortedSeries = DataSerieHelper.Sort(chart.Series.ToList(), chart.DisplayLegendSort, chart.LimitSeriesNumber);
				chart.Series = new DataSerieCollection(sortedSeries);
			}
			else
				chart.Series = new DataSerieCollection();
		}

		/// <summary>
		/// Applies the Chart palette to all dataseries that dont have a predefined color.
		/// </summary>
		/// <param name="chart">The chart.</param>
		public void Chart_ApplyPalette(Chart chart) {
			if (chart != null) {
				var palette = chart.Palette;
				if (string.IsNullOrEmpty(chart.KeyPalette) == false)
					palette = Palette_GetItem(chart.KeyPalette, "*");

				if (palette == null)
					palette = Palette_GetItem(Chart_GetDefaultPalette(), "*");

				if (chart.Series != null && chart.Series.Count > 0)
					Chart_ApplyPalette(palette, chart.Series.AsEnumerable());

				if (chart.HistoricalAnalysis != null && chart.HistoricalAnalysis.Count > 0)
					foreach (var ds in chart.HistoricalAnalysis.Values.Where(ds => ds != null)) {
						Chart_ApplyPalette(palette, ds.AsEnumerable());
					}
			}
		}

		/// <summary>
		/// Applies the Chart palette to all dataseries that dont have a predefined color.
		/// </summary>
		/// <param name="palette">The palette.</param>
		/// <param name="series">The list of DataSeries.</param>
		private void Chart_ApplyPalette(Palette palette, IEnumerable<DataSerie> series) {
			if (palette != null && palette.Colors != null && palette.Colors.Any()) {
				int index = 0;
				foreach (DataSerie s in series) {
					if (s.Hidden) {
						s.ColorR = s.ColorG = s.ColorB = 120;
					}
					else {
						if (!s.ColorR.HasValue || !s.ColorG.HasValue || !s.ColorB.HasValue) {
							var color = palette.Colors[index];
							s.ColorR = color.ColorR;
							s.ColorG = color.ColorG;
							s.ColorB = color.ColorB;
						}
						index++;
						if (index >= palette.Colors.Count)
							index = 0;
					}
				}
			}
		}

		/// <summary>
		/// Apply ModernUI color to a chart.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="color">The color.</param>
		/// <returns></returns>
		public Chart Chart_ApplyModernUIColor(string Key, ModernUIColor color) {
			ChartBroker broker = Helper.CreateInstance<ChartBroker>();
			var chart = broker.GetItem(Key);
			if (chart != null) {
				Chart_ClearCache(Key, chart.KeyDynamicDisplay);
				ChartHelper.ApplyModernUIStyle(chart, color);
				chart = broker.Save(chart);
			}
			return chart;
		}

		/// <summary>
		/// Get the default palette.
		/// </summary>
		/// <returns></returns>
		private string Chart_GetDefaultPalette() {
			string retVal = null;
			var brokerPalette = Helper.CreateInstance<PaletteBroker>();
			var palettes = brokerPalette.GetAll();
			if (palettes.Count > 0) {
				retVal = palettes.First().KeyPalette;
			}
			return retVal;
		}

		/// <summary>
		/// Fix some changes that could have occured in the python script.
		/// </summary>
		/// <param name="chart">The chart.</param>
		public void Chart_FixScriptChanges(Chart chart) {
			DynamicDisplayImageBroker brokerImage = new DynamicDisplayImageBroker();

			if (string.IsNullOrEmpty(chart.DynamicDisplayHeaderPicture) == false && (chart.DynamicDisplayHeaderPictureImage == null || chart.DynamicDisplayHeaderPictureImage.Image == null)) {
				chart.DynamicDisplayHeaderPictureImage = brokerImage.GetItem(chart.DynamicDisplayHeaderPicture, "*");
			}

			if (string.IsNullOrEmpty(chart.DynamicDisplayMainPicture) == false && (chart.DynamicDisplayMainPictureImage == null || chart.DynamicDisplayMainPictureImage.Image == null)) {
				chart.DynamicDisplayMainPictureImage = brokerImage.GetItem(chart.DynamicDisplayMainPicture, "*");
			}

			if (string.IsNullOrEmpty(chart.DynamicDisplayFooterPicture) == false && (chart.DynamicDisplayFooterPictureImage == null || chart.DynamicDisplayFooterPictureImage.Image == null)) {
				chart.DynamicDisplayFooterPictureImage = brokerImage.GetItem(chart.DynamicDisplayFooterPicture, "*");
			}
		}


		/// <summary>
		/// Chart_s the update script input values.
		/// </summary>
		/// <param name="chart">The chart.</param>
		private void Chart_UpdateAlgorithmInputValues(Chart chart) {
			if (chart.Algorithms == null) return;

			var broker = Helper.CreateInstance<AlgorithmInputValueBroker>();

			foreach (var chartscript in chart.Algorithms.Where(s => s.AlgorithmInputValues != null)) {
				foreach (var input in chartscript.AlgorithmInputValues) {
					if (input.IsDirty) {
						broker.Save(input);
					}
				}
			}
		}

		/// <summary>
		/// Gets the store of possible grouping localisation for Charts. 
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElementGeneric<int>> Chart_GetLocalisationStore() {
			var siteBroker = Helper.CreateInstance<SiteBroker>();

			var siteIconCls = Helper.GetAttributeValue<IconClsAttribute>(typeof(Site));
			int maxSiteLevel = siteBroker.GetMaxLevel();
			List<ListElementGeneric<int>> result = EnumExtension.GetListElementNumeric(typeof(ChartLocalisation));
			result.RemoveAll(el => el.Id.StartsWith("BySite"));
			int existingLevels = result.Count;

			//we have to inverse the site level. In the Chart aggregation the level 0 is the level just above a building. 
			//This is mandatory to group branches with different level depth.
			for (int i = 0; i <= maxSiteLevel; i++) {
				var msgcode = Helper.LocalizeText("msg_enum_axislocalisation_bysite" + i.ToString(CultureInfo.InvariantCulture), true);
				if (string.IsNullOrEmpty(msgcode)) {
					msgcode = Langue.msg_enum_axislocalisation_bysite + " " + i.ToString(CultureInfo.InvariantCulture);
				}
				result.Add(new ListElementGeneric<int> {
					Id = "BySite" + i.ToString(CultureInfo.InvariantCulture),
					MsgCode = msgcode,
					Value = existingLevels,
					IconCls = siteIconCls,
					Order = existingLevels
				});
				existingLevels += 1;
			}

			return result.ToJsonStore();
		}

		/// <summary>
		/// Return the list of Locations associated with a Chart.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		public List<Location> Chart_GetLocations(string Key, List<Meter> meters = null) {
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			List<Location> retVal = brokerLocation.GetFilterSpatialStoreByKeyChart(new PagingParameter { start = 0, limit = 0 }, Key).records;

			if (meters == null) {
				var brokerMeter = Helper.CreateInstance<MeterBroker>();
				meters = brokerMeter.GetCompleteListByKeyChart(Key, true, new string[0]);
			}
			var keys = meters.Select(m => m.KeyLocation).ToArray();
			retVal.AddRange(brokerLocation.GetList(keys));
			retVal = retVal.Distinct(loc => loc.KeyLocation).ToList();
			return retVal;
		}

		/// <summary>
		/// Return the list of Locations Keys associated with a Chart.(faster than retrieving the actual locations)
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <param name="meters">The meters.</param>
		/// <returns></returns>
		public List<string> Chart_GetLocationKeys(string Key, List<Meter> meters = null) {
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			var filter = brokerLocation.GetFilterSpatialStoreByKeyChart(new PagingParameter { start = 0, limit = 0 }, Key).records;
			var locations = filter.Select(loc => loc.KeyLocation);
			if (meters == null) {
				var brokerMeter = Helper.CreateInstance<MeterBroker>();
				meters = brokerMeter.GetCompleteListByKeyChart(Key, true, new string[0]);
			}
			var retVal = locations.Concat(meters.Select(m => m.KeyLocation));
			return retVal.Distinct().ToList();
		}

		///// <summary>
		///// Return the complete list of Meters associated with a Chart.
		///// </summary>
		///// <param name="Key">The key of the Chart.</param>
		///// <returns></returns>
		//public List<Meter> Chart_GetMeters(string Key) {
		//    return Chart_GetMeters(Key, false);
		//}

		/// <summary>
		/// Return the complete list of Meters associated with a Chart.
		/// </summary>
		/// <param name="keyChart">The key of the Chart.</param>
		/// <param name="keyMeterAndLocationOnly">if set to <c>true</c> [key meter and location only].</param>
		/// <param name="filterSpatialKeys">The filter spatial keys.</param>
		/// <param name="metersClassificationsKeys">The meters classifications keys.</param>
		/// <param name="metersKeys">The meters keys.</param>
		/// <returns></returns>
		public List<Meter> Chart_GetMeters(string keyChart, bool keyMeterAndLocationOnly = false, IEnumerable<string> filterSpatialKeys = null,
				IEnumerable<string> metersClassificationsKeys = null, IEnumerable<string> metersKeys = null) {

			var meterBroker = Helper.CreateInstance<MeterBroker>();
			var retVal = meterBroker.GetCompleteListByKeyChart(keyChart, keyMeterAndLocationOnly, filterSpatialKeys, metersClassificationsKeys, metersKeys, new string[0]);
			return retVal;
		}

		/// <summary>
		/// Return a json store of the complete list of Meters associated with a Chart.
		/// including meters associated via SpatialFilter cross SpatialFilterPset cross MeterClassification
		/// </summary>
		/// <param name="keyChart">The key of the Chart.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<Meter> Chart_GetMetersStore(string keyChart, PagingParameter paging) {
			var meterBroker = Helper.CreateInstance<MeterBroker>();
			var retVal = meterBroker.GetCompleteListByKeyChart(keyChart, false);//, "*");
			return retVal.ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Gets a json store for the business entity Chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Chart> Chart_GetStore(PagingParameter paging, PagingLocation location) {
			ChartBroker broker = Helper.CreateInstance<ChartBroker>();
			var retVal = broker.GetStore(paging, location);

			//Removed for performance reasons, now the Chart entity has a VirtualFilePath.
			//foreach (var chart in retVal.records) {
			//	broker.PopulateList(chart, new string[] { "DynamicDisplay", "VirtualFile" });
			//}
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity Chart for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmDefinition">The key AlarmDefinition.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<Chart> Chart_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			ChartBroker broker = Helper.CreateInstance<ChartBroker>();
			var retVal = broker.GetStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity Chart for a specific ChartScheduler.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChartScheduler">The key chart scheduler.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<Chart> Chart_GetStoreByKeyChartScheduler(PagingParameter paging, string KeyChartScheduler) {
			ChartBroker broker = Helper.CreateInstance<ChartBroker>();
			var retVal = broker.GetStoreByKeyChartScheduler(paging, KeyChartScheduler);
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity Chart KPI.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="KeyClassificationItemKPI">The key classification item KPI.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<Chart> Chart_GetStoreKPI(PagingParameter paging, PagingLocation location, string KeyClassificationItemKPI) {
			var retVal = new JsonStore<Chart>();
			//when we create the EBL for the EA side, since there is no reference to the FOL Role Provider in the EA web.config it fails.
			try {
				var authenticationBusinessLayer = Helper.CreateInstance<AuthenticationBusinessLayer, IAuthenticationBusinessLayer>();
				ChartBroker broker = Helper.CreateInstance<ChartBroker>();

				var roles = authenticationBusinessLayer.AuthorizationItem_GetStore(new PagingParameter(), Helper.GetCurrentUserName()).records.Where(auth => auth.ItemType == "Role").ToList();
				retVal = broker.GetStoreByKeyClassificationItemKPI(paging, KeyClassificationItemKPI, roles);

				foreach (var chart in retVal.records) {
					broker.PopulateList(chart, new[] { "DynamicDisplay", "VirtualFile" });
				}
			}
			catch (TargetInvocationException) { ;}
			return retVal;
		}

		/// <summary>
		/// Private method that returns a Tuple of the Chart with data and the stream of the dynamic display.
		/// </summary>
		/// <param name="key">The key of the Chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="chart">The Chart</param>
		/// <returns></returns>
		private Tuple<Chart, StreamResult, Padding> Chart_GetStreamBase(string key, int width, int height, Chart chart = null) {
			var chartWithData = Chart_GetItemWithData(key, useCache: chart == null, chart: chart);
			return Chart_GetStreamBase(chartWithData, width, height);
		}

		private Tuple<Chart, StreamResult, Padding> Chart_GetStreamBase(Chart chart, int width, int height, bool useCache = true) {
			Padding padding = Padding.Empty;
			StreamResult stream = null;
			if (chart.UseDynamicDisplay && !string.IsNullOrEmpty(chart.KeyDynamicDisplay)) {
				stream = DynamicDisplay_GetStreamImage(chart.KeyDynamicDisplay, width, height, out padding, chart, useCache: useCache);
			}
			return new Tuple<Chart, StreamResult, Padding>(chart, stream, padding);
		}

		/// <summary>
		/// Returns the stream of the excel chart from a chart.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <returns></returns>
		public StreamResult Chart_GetStreamExcel(string Key) {
			return Chart_GetStreamExport(Key, ExportType.Excel2007);
		}

		/// <summary>
		/// Exports the Chart as an Excel File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the Chart.</param>
		public void Chart_GetStreamExcelBegin(Guid operationId, string Key) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				StreamResult stream = Chart_GetStreamExcel(Key);
				var result = new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, stream);
				return result;
			});
		}

		/// <summary>
		/// Returns the stream of the chart export from a chart.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <param name="type">The type of export (pdf, excel, ...).</param>
		/// <param name="clearChartCache">if set to <c>true</c> [clear chart cache].</param>
		/// <returns></returns>
		private StreamResult Chart_GetStreamExport(string Key, ExportType type, bool clearChartCache = false) {
			Chart chart = Chart_GetItemWithData(Key, clearChartCache);
			if (chart != null) {
				LocalizeChartExportFields(chart);
			}
			return Chart_GetStreamExport(chart, type);
		}

		/// <summary>
		/// Localizes the chart export fields.
		/// </summary>
		/// <param name="chart">The chart.</param>
		private void LocalizeChartExportFields(Chart chart) {
			chart.Title = Helper.LocalizeInText(chart.Title, "%");
			if (chart.Series != null && chart.Series.Count > 0) {
				foreach (var serie in chart.Series.GetVisibleDataSeries()) {
					serie.Name = Helper.LocalizeInText(serie.Name, "%");
				}
			}
		}

		/// <summary>
		/// Gets chart stream export.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="type">The type.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		private StreamResult Chart_GetStreamExport(Chart chart, ExportType type, string title = null) {
			var chartStream = Chart_GetStream(chart, type);
			
			if (type!= ExportType.ImagePNG){
					if (chart.DisplayMode == ChartDisplayMode.CustomGrid) {
						var broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
						chart.ChartCustomGridCells = broker.GetPopulatedListChart(chart);
					}
			}
			switch (type){
				case ExportType.Excel2007:
				case ExportType.ExcelBasic:
				case ExportType.CSV:

					var tupleList = new List<Tuple<Chart, StreamResult>> {new Tuple<Chart, StreamResult>(chart, chartStream)};
					return ChartExcelService.RenderExcelStream(tupleList, title ?? chart.Title);

				case ExportType.ImagePNG:
					return chartStream;

				default:
					if (chart.DisplayMode == ChartDisplayMode.Html){
						return chartStream;
			}
					return ChartPdfService.RenderPdfStream(new[] {new Tuple<Chart, StreamResult>(chart, chartStream)},
						title ?? chart.Title);
		}
		}

		/// <summary>
		/// Returns the stream of the chart export from a chart.
		/// </summary>
		/// <param name="Key">The key of the chart.</param>
		/// <param name="type">The type of export (pdf, excel, ...).</param>
		/// <param name="chart">The chart.</param>
		/// <param name="clearChartCache">if set to <c>true</c> [clear chart cache].</param>
		/// <returns></returns>
		private StreamResult Chart_GetStream(string Key, ExportType type, out Chart chart, bool clearChartCache = false) {
			chart = Chart_GetItemWithData(Key, clearChartCache);
			LocalizeChartExportFields(chart);
			return Chart_GetStream(chart, type);
		}

		/// <summary>
		/// Returns the stream of the chart export from a chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="type">The type of export (pdf, excel, ...).</param>
		/// <returns></returns>
		private StreamResult Chart_GetStream(Chart chart, ExportType type) {
			Size size = ChartPdfService.GetPageDefaultSize();
			switch (type) {
				case ExportType.Excel2007:
				case ExportType.ExcelBasic:
				case ExportType.CSV:
					StreamResult imageForExcel = null;

					// On some cases we only want the data.
					if (!ChartHelper.IsGridMode(chart)) {
						imageForExcel = Chart_GetStreamImage(chart, size.Width, size.Height);
					}

					return imageForExcel;

				case ExportType.ImagePNG:
					return Chart_GetStreamImage(chart, ChartSize.DefaultImageWidth, ChartSize.DefaultImageHeight);

				case ExportType.Html:
					if (chart.DisplayMode == ChartDisplayMode.Html)
						return Chart_GetStreamHtml(chart);
					break;
				default:
					if (chart.DisplayMode == ChartDisplayMode.Html) {
						return Chart_GetStreamHtml(chart);
					}
					break;
			}
			return Chart_GetStreamImage(chart, size.Width, size.Height);
		}

		/// <summary>
		/// Returns the stream of the export file from a portalwindow.
		/// </summary>
		/// <param name="charts">The charts.</param>
		/// <param name="type">The type of export.</param>
		/// <param name="title">The title.</param>
		/// <param name="clearChartCache">if set to <c>true</c> [clear chart cache].</param>
		/// <returns></returns>
		private List<StreamResult> Chart_GetStreamExport(IEnumerable<Chart> charts, ExportType type, string title, bool clearChartCache = false) {
			var retVal = new List<StreamResult>();
			var chartStreams = new List<Tuple<Chart, StreamResult>>();
			foreach (var chart in charts) {
				if (type == ExportType.Pdf || type == ExportType.ImagePNG || type == ExportType.Html ||
					type == ExportType.ExcelBasic || type == ExportType.Excel2007) {
					Chart chartWithData;
					var chartStream = Chart_GetStream(chart.KeyChart, type, out chartWithData, true);
					if (chartWithData.DisplayMode == ChartDisplayMode.CustomGrid) {
						var broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
						chartWithData.ChartCustomGridCells = broker.GetPopulatedListChart(chartWithData);
					}
					chartStreams.Add(new Tuple<Chart, StreamResult>(chartWithData, chartStream));
				}
			}

			switch (type) {
				case ExportType.Excel2007:
				case ExportType.ExcelBasic:
				case ExportType.CSV:
					retVal.Add(ChartExcelService.RenderExcelStream(chartStreams, title));
					break;
				case ExportType.Pdf:
					retVal.Add(ChartPdfService.RenderPdfStream(chartStreams, title));
					break;

				default:
					retVal.AddRange(chartStreams.Select(tuple => tuple.Item2).ToList());
					break;
			}
			return retVal;
		}

		/// <summary>
		/// Returns the stream of the image (with no map) from a chart.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public StreamResult Chart_GetStreamImage(string Key, int width, int height) {
			StreamResult retVal = ChartHelper.Chart_GetStreamImageFromCache(Key, width, height);
			if (retVal == null) {
				var tuple = Chart_GetStreamBase(Key, width, height);
				retVal = ChartingService.RenderChartStream(tuple.Item1, width, height, tuple.Item2, tuple.Item3);
				ChartHelper.Chart_AddStreamImageToCache(Key, width, height, retVal);
			}
			return retVal;
		}

		/// <summary>
		/// Returns the stream of the image (with no map) from a chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public StreamResult Chart_GetStreamImage(Chart chart, int width, int height) {
			var tuple = Chart_GetStreamBase(chart, width, height);
			return ChartingService.RenderChartStream(chart, width, height, tuple.Item2, tuple.Item3);
		}

		/// <summary>
		/// Returns the stream of the image (with map) from a chart.
		/// </summary>
		/// <param name="guid">The guid of the image.</param>
		/// <returns></returns>
		public Stream Chart_GetStreamImageFromCache(string guid) {
			ImageMap data = CacheService.GetData(guid) as ImageMap;
			if (data != null) {
				CacheService.Remove(guid);
				return data.Base64String.GetStreamFromBase64String();
			}
			return null;
		}

		/// <summary>
		/// Returns the chart as a stream of javascript code (by default using hightcharts.com).
		/// </summary>
		/// <param name="key">The key of the Chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="chart">The Chart</param>
		/// <returns></returns>
		public StreamResult Chart_GetStreamJavascript(string key, int width, int height, Chart chart = null) {
			var tuple = Chart_GetStreamBase(key, width, height, chart);
			return ChartingService.RenderChartJavascript(tuple.Item1, width, height, tuple.Item2, tuple.Item3);
		}

		/// <summary>
		/// Returns the chart as a stream of html.
		/// </summary>
		/// <param name="key">The key of the Chart.</param>
		/// <returns></returns>
		public StreamResult Chart_GetStreamHtml(string key) {
			Chart chart = Chart_GetItemWithData(key);
			return Chart_GetStreamHtml(chart);
		}

		private static StreamResult Chart_GetStreamHtml(Chart chart) {
			string html = Chart_GetStringHtml(chart);
			var stream = StreamResultHelper.GetStreamFromHtml(html);
			stream.FileName = chart.Title;
			stream.FileExtension = "htm";

			return stream;
		}

		private static string Chart_GetStringHtml(Chart chart) {
			string html = chart.IsEnergyAggregatorLoadingMeterData
				? string.Format("<div style='width:100%;padding-top:10%;text-align:center;vertical-align:middle;font-size:larger;'>{0}</div>",
					Helper.LocalizeText(Langue.error_chart_energyaggregatorloadingmeterdata))
				: ChartingService.GetChartHtml(chart);
			return html;
		}

		/// <summary>
		/// Returns the stream of the excel chart from a chart.
		/// </summary>
		/// <param name="Key">The key of the Chart.</param>
		/// <returns></returns>
		public StreamResult Chart_GetStreamPdf(string Key) {
			return Chart_GetStreamExport(Key, ExportType.Pdf);
		}

		/// <summary>
		/// Exports the Chart as an Pdf File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the Chart.</param>
		public void Chart_GetStreamPdfBegin(Guid operationId, string Key) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				var stream = Chart_GetStreamPdf(Key);
				var result =
					new LongRunningOperationResult(
						LongRunningOperationStatus.ResultAvailable, stream);
				return result;
			});
		}

		/// <summary>
		/// Returns the stream of the image of a Micro chart.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="value">The value.</param>
		/// <param name="targetValue">The target value.</param>
		/// <param name="minValue">The min value.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public StreamResult Chart_MicroChartGetStreamImage(string text, double value, double targetValue, double? minValue, int width, int height) {
			var retVal = ChartingService.RenderMicroChart(text, value, targetValue, minValue, Color.Green, width, height);
			return retVal;
		}

		/// <summary>
		/// Preload in cache all the Charts that are used inside a PortalWindow.
		/// </summary>
		/// <param name="clearCache">if set to <c>true</c> [clear cache].</param>
		/// <param name="username">The username.</param>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <param name="KeyPortalWindow">The key portal window.</param>
		/// <returns></returns>
		public PortalTab Chart_PreLoadAll(bool clearCache = false, string username = null, string KeyPortalTab = null, string KeyPortalWindow = null) {
			#region PortalWindow retrieval
			List<PortalWindow> portalWindows = null;
			if (KeyPortalTab == null && KeyPortalWindow == null) {
				var portalWindowBroker = Helper.CreateInstance<PortalWindowBroker>();
				if (string.IsNullOrEmpty(username)) {
					portalWindows = portalWindowBroker.GetAll();
				}
				else {
					FOLMembershipUserBroker userBroker = Helper.CreateInstance<FOLMembershipUserBroker>();
					var user = userBroker.User_GetByUserName(username);
					portalWindows = portalWindowBroker.GetListByKeyCreator(user.ProviderUserKey.KeyUser);
				}
			}
			#endregion

			#region Charts retrieval
			List<string> keys;

			// This is this method return value
			// When called from LongRunningOpreation service we have to return non null value.
			// If this method runs for all tabs, we'll return dummy portal tab in order to avoid null return value.
			var portalTab = new PortalTab();
			if (KeyPortalTab == null && KeyPortalWindow == null) {
				keys = new List<string>();
				foreach (var portalWindow in portalWindows) {
					List<string> keyCharts = PortalWindow_GetChartKeys(portalWindow.KeyPortalWindow, true);
					keys.AddRange(keyCharts);
				}
			}
			else {
				if (KeyPortalTab != null) {
					var portalTabBroker = Helper.CreateInstance<PortalTabBroker>();

					portalTab = portalTabBroker.GetItem(KeyPortalTab, "*");
					keys = PortalTab_GetChartKeys(KeyPortalTab, true);
				}
				else {
					keys = PortalWindow_GetChartKeys(KeyPortalWindow, true);
				}
			}
			#endregion
			#region Chart processing
			if (clearCache) {
				foreach (var keyChart in keys) {
					var currChart = Chart_GetItem(keyChart);
					Chart_ClearCache(keyChart, currChart.KeyDynamicDisplay);
				}
			}

			Chart_GetItemWithData(keys);
			#endregion
			return portalTab;
		}

		/// <summary>
		/// Preload in cache all the Charts that are used inside a PortalWindow using a long running operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="clearCache">if set to <c>true</c> [clear cache].</param>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <param name="KeyPortalWindow">The key portal window.</param>
		public void Chart_PreLoadAllBegin(Guid operationId, bool clearCache = false, string KeyPortalTab = null, string KeyPortalWindow = null) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				var portalTab = Chart_PreLoadAll(clearCache, KeyPortalTab: KeyPortalTab, KeyPortalWindow: KeyPortalWindow);
				var result = new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, portalTab);
				return result;
			});
		}

		/// <summary>
		/// Saves a crud store for the business entity Chart.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Chart> Chart_SaveStore(CrudStore<Chart> store) {
			ChartBroker broker = Helper.CreateInstance<ChartBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity ChartAxis.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartAxis_Delete(ChartAxis item) {
			ChartAxisBroker broker = Helper.CreateInstance<ChartAxisBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartAxis and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ChartAxis_FormCreate(ChartAxis item) {
			ChartAxisBroker broker = Helper.CreateInstance<ChartAxisBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartAxis.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartAxis_FormLoad(string Key) {
			ChartAxisBroker broker = Helper.CreateInstance<ChartAxisBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ChartAxis and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ChartAxis_FormUpdate(ChartAxis item) {
			ChartAxisBroker broker = Helper.CreateInstance<ChartAxisBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartAxis_FormUpdateBatch(string[] keys, ChartAxis item) {
			ChartAxisBroker broker = Helper.CreateInstance<ChartAxisBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ChartAxis.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<ChartAxis> ChartAxis_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			ChartAxisBroker broker = Helper.CreateInstance<ChartAxisBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartAxis.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public ChartAxis ChartAxis_GetItem(string Key, params string[] fields) {
			ChartAxisBroker broker = Helper.CreateInstance<ChartAxisBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartAxis.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartAxis> ChartAxis_GetStore(PagingParameter paging, PagingLocation location) {
			ChartAxisBroker broker = Helper.CreateInstance<ChartAxisBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of chartaxis for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<ChartAxis> ChartAxis_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			ChartAxisBroker broker = Helper.CreateInstance<ChartAxisBroker>();
			return broker.GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartAxis.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartAxis> ChartAxis_SaveStore(CrudStore<ChartAxis> store) {
			ChartAxisBroker broker = Helper.CreateInstance<ChartAxisBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartCustomGridCell_Delete(ChartCustomGridCell item) {
			ChartCustomGridCellBroker broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartCustomGridCell and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ChartCustomGridCell_FormCreate(ChartCustomGridCell item) {
			ChartCustomGridCellBroker broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartCustomGridCell_FormLoad(string Key) {
			ChartCustomGridCellBroker broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ChartCustomGridCell and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ChartCustomGridCell_FormUpdate(ChartCustomGridCell item) {
			ChartCustomGridCellBroker broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartCustomGridCell_FormUpdateBatch(string[] keys, ChartCustomGridCell item) {
			ChartCustomGridCellBroker broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ChartCustomGridCell. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<ChartCustomGridCell> ChartCustomGridCell_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			ChartCustomGridCellBroker broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public ChartCustomGridCell ChartCustomGridCell_GetItem(string Key, params string[] fields) {
			ChartCustomGridCellBroker broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a list of ChartCustomGridCell for a specific chart.
		/// </summary>
		/// <param name="keyChart">The key of the chart.</param>
		/// <param name="populateValues">if set to <c>true</c> we fetch the chart with data and we populate the values.</param>
		/// <param name="chart">The chart - currently being used only for analytics</param>
		/// <returns></returns>
		public List<ChartCustomGridCell> ChartCustomGridCell_GetListByKeyChart(string keyChart, bool populateValues, Chart chart = null) {
			List<ChartCustomGridCell> retVal;
			var broker = Helper.CreateInstance<ChartCustomGridCellBroker>();

			if (populateValues) {
				var chartWithData = Chart_GetItemWithData(keyChart, false, chart == null, chart);
				retVal = broker.GetPopulatedListChart(chartWithData);
			}
			else {
				retVal = broker.GetListByKeyChart(keyChart);
			}

			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartCustomGridCell> ChartCustomGridCell_GetStore(PagingParameter paging, PagingLocation location) {
			ChartCustomGridCellBroker broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of ChartCustomGridCell for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<ChartCustomGridCell> ChartCustomGridCell_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			ChartCustomGridCellBroker broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
			return broker.GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Save a list of ChartCustomGridCell for a specific chart.
		/// </summary>
		/// <param name="cells">The cells.</param>
		/// <returns></returns>
		public bool ChartCustomGridCell_SaveList(List<ChartCustomGridCell> cells) {
			ChartCustomGridCellBroker broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
			foreach (var cell in cells) {
				broker.Save(cell);
			}
			return true;
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartCustomGridCell.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartCustomGridCell> ChartCustomGridCell_SaveStore(CrudStore<ChartCustomGridCell> store) {
			ChartCustomGridCellBroker broker = Helper.CreateInstance<ChartCustomGridCellBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartHistoricalAnalysis_Delete(ChartHistoricalAnalysis item) {
			ChartHistoricalAnalysisBroker broker = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartHistoricalAnalysis and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ChartHistoricalAnalysis_FormCreate(ChartHistoricalAnalysis item) {
			Chart_ClearCache(item.KeyChart);
			ChartHistoricalAnalysisBroker broker = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartHistoricalAnalysis_FormLoad(string Key) {
			ChartHistoricalAnalysisBroker broker = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ChartHistoricalAnalysis and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ChartHistoricalAnalysis_FormUpdate(ChartHistoricalAnalysis item) {
			Chart_ClearCache(item.KeyChart);
			ChartHistoricalAnalysisBroker broker = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartHistoricalAnalysis_FormUpdateBatch(string[] keys, ChartHistoricalAnalysis item) {
			ChartHistoricalAnalysisBroker broker = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<ChartHistoricalAnalysis> ChartHistoricalAnalysis_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			ChartHistoricalAnalysisBroker broker = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public ChartHistoricalAnalysis ChartHistoricalAnalysis_GetItem(string Key, params string[] fields) {
			ChartHistoricalAnalysisBroker broker = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartHistoricalAnalysis> ChartHistoricalAnalysis_GetStore(PagingParameter paging, PagingLocation location) {
			ChartHistoricalAnalysisBroker broker = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of ChartHistoricalAnalysis for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<ChartHistoricalAnalysis> ChartHistoricalAnalysis_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			var broker = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
			var brokerChart = Helper.CreateInstance<ChartBroker>();

			var chart = brokerChart.GetItem(KeyChart);
			var retVal = broker.GetStoreByKeyChart(paging, KeyChart);
			if (retVal.recordCount > 0) {
				foreach (var historicalAnalysis in retVal.records) {
					ChartHelper.UpdateChartHistoricalAnalysisDates(chart, historicalAnalysis);
				}
			}
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartHistoricalAnalysis.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartHistoricalAnalysis> ChartHistoricalAnalysis_SaveStore(CrudStore<ChartHistoricalAnalysis> store) {
			ChartHistoricalAnalysisBroker broker = Helper.CreateInstance<ChartHistoricalAnalysisBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity ChartMarker.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartMarker_Delete(ChartMarker item) {
			ChartMarkerBroker broker = Helper.CreateInstance<ChartMarkerBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartMarker and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ChartMarker_FormCreate(ChartMarker item) {
			ChartMarkerBroker broker = Helper.CreateInstance<ChartMarkerBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartMarker.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartMarker_FormLoad(string Key) {
			ChartMarkerBroker broker = Helper.CreateInstance<ChartMarkerBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ChartMarker and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ChartMarker_FormUpdate(ChartMarker item) {
			ChartMarkerBroker broker = Helper.CreateInstance<ChartMarkerBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartMarker_FormUpdateBatch(string[] keys, ChartMarker item) {
			ChartMarkerBroker broker = Helper.CreateInstance<ChartMarkerBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ChartMarker. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<ChartMarker> ChartMarker_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			ChartMarkerBroker broker = Helper.CreateInstance<ChartMarkerBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartMarker.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public ChartMarker ChartMarker_GetItem(string Key, params string[] fields) {
			ChartMarkerBroker broker = Helper.CreateInstance<ChartMarkerBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartMarker.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartMarker> ChartMarker_GetStore(PagingParameter paging, PagingLocation location) {
			ChartMarkerBroker broker = Helper.CreateInstance<ChartMarkerBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of ChartMarker for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<ChartMarker> ChartMarker_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			ChartMarkerBroker broker = Helper.CreateInstance<ChartMarkerBroker>();
			return broker.GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartMarker.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartMarker> ChartMarker_SaveStore(CrudStore<ChartMarker> store) {
			ChartMarkerBroker broker = Helper.CreateInstance<ChartMarkerBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets the Chart's model initial tree.
		/// </summary>
		/// <returns></returns>
		private List<TreeNode> ChartModel_GetInitialTree() {
			const string calendarIconCls = "viz-icon-small-calendar";
			const string calculationIconCls = "viz-icon-small-calculatedserie";
			const string localizationIconCls = "viz-icon-small-localization";
			const string gridIconCls = "viz-icon-small-grid";
			var chartIconCls = Helper.GetAttributeValue<IconClsAttribute>(typeof(Chart));
			var serieIconCls = Helper.GetAttributeValue<IconClsAttribute>(typeof(DataSerie));

			var formula = new TreeNode {
				Key = "FormulaParent",
				text = Langue.msg_calculatedserie_formula,
				iconCls = calculationIconCls,
				children = new List<TreeNode>
				{
					new TreeNode{ Key="Formula", text = Langue.msg_calculatedserie_formula, iconCls = calculationIconCls, leaf=true},
					new TreeNode { Key = "SEPARATOR" }
				}
			};
			foreach (AlgebricFunction func in Enum.GetValues(typeof(AlgebricFunction))) {
				if (func != AlgebricFunction.None && func != AlgebricFunction.Count && func != AlgebricFunction.Delta && func != AlgebricFunction.Percentage && func != AlgebricFunction.YTD) {
					formula.children.Add(new TreeNode { Key = func.ToString().ToLower(), text = func.GetLocalizedText(), leaf = true, iconCls = calculationIconCls });
				}
			}
			var retVal = new List<TreeNode>
							{
								new TreeNode { Key = "Chart(@Model", text = Langue.msg_chart, iconCls = chartIconCls, children = new List<TreeNode>
									{
										new TreeNode { Key = "\"Title\")", text = Langue.msg_chart_title, iconCls = chartIconCls, leaf = true }, 
										new TreeNode { Key = "SEPARATOR" },
										new TreeNode { Key = "\"Grid\")", text = Langue.msg_enum_chartdisplaymode_grid, iconCls = gridIconCls, leaf = true }, 
										new TreeNode { Key = "SEPARATOR" },
										new TreeNode { Key = "\"StartDate\")", text = Langue.msg_chart_startdate, iconCls = calendarIconCls, leaf = true }, 
										new TreeNode { Key = "\"EndDate\")" , text = Langue.msg_chart_enddate, iconCls = calendarIconCls, leaf = true }, 
										new TreeNode { Key = "\"Now\")" , text = Langue.msg_chartmodel_now, iconCls = calendarIconCls, leaf = true },
										new TreeNode { Key = "\"TimeInterval\")" , text = Langue.msg_chart_timeinterval, iconCls = calendarIconCls, leaf = true },
										new TreeNode { Key = "\"NumberOfYears\")" , text = Langue.msg_chart_timeinterval_numberofyears, iconCls = calendarIconCls, leaf = true },
										new TreeNode { Key = "\"NumberOfMonths\")" , text = Langue.msg_chart_timeinterval_numberofmonths, iconCls = calendarIconCls, leaf = true },
										new TreeNode { Key = "\"NumberOfDays\")" , text = Langue.msg_chart_timeinterval_numberofdays, iconCls = calendarIconCls, leaf = true },
										new TreeNode { Key = "\"NumberOfHours\")" , text = Langue.msg_chart_timeinterval_numberofhours, iconCls = calendarIconCls, leaf = true },
										new TreeNode { Key = "\"NumberOfMinutes\")" , text = Langue.msg_chart_timeinterval_numberofminutes, iconCls = calendarIconCls, leaf = true },
										new TreeNode { Key = "\"NumberOfSeconds\")" , text = Langue.msg_chart_timeinterval_numberofseconds, iconCls = calendarIconCls, leaf = true }
									} 
								},
								formula,
								new TreeNode { Key = "LocalizedText" , text = Langue.msg_localization, iconCls = localizationIconCls, leaf = true },
								new TreeNode { Key = "SEPARATOR" },
								new TreeNode 
									{ 
										Key = "AllSeries(@Model", text = Langue.msg_chartmodel_allseries, iconCls = serieIconCls, children = new List<TreeNode>
										{
											new TreeNode { Key = "\"MinElem\")" ,text = Langue.msg_chartmodel_minelements, leaf = true }, 
											new TreeNode { Key = "\"MaxElem\")" ,text = Langue.msg_chartmodel_maxelements, leaf = true }, 
											new TreeNode { Key = "\"AvgElem\")" ,text = Langue.msg_chartmodel_avgelements, leaf = true },
											new TreeNode { Key = "\"SumElem\")" ,text = Langue.msg_chartmodel_sumelements, leaf = true }
										} 
									}
							};
			return retVal;
		}

		/// <summary>
		/// Gets the chart tree.
		/// </summary>
		/// <param name="keyChart">The key chart.</param>
		/// <returns></returns>
		public List<TreeNode> ChartModel_GetMenuTree(string keyChart) {
			var retVal = ChartModel_GetInitialTree();
			var generalSerieTree = ChartModel_GetSerieTree();
			var dataserieIconCls = Helper.GetAttributeValue<IconClsAttribute>(typeof(DataSerie));
			if (!string.IsNullOrWhiteSpace(keyChart)) {
				var serieList = Chart_GetAvailableDataSerieLocalId(keyChart, true).records;

				if (serieList.Count > 0) {
					var seriesNameList = new TreeNode { Key = "SerieName(@Model", text = Langue.msg_chartmodel_seriename, iconCls = dataserieIconCls, children = new List<TreeNode>() };
					var i = 1;
					var rankList = new TreeNode { Key = "SerieRank(@Model", text = Langue.msg_chartmodel_serierank, iconCls = dataserieIconCls, children = new List<TreeNode>() };

					foreach (var dataSerie in serieList) {
						seriesNameList.children.Add(new TreeNode { Key = "\"" + dataSerie.Value + "\"", text = dataSerie.MsgCode, iconCls = dataSerie.IconCls, children = generalSerieTree });
						rankList.children.Add(new TreeNode { Key = i.ToString(CultureInfo.InvariantCulture), text = i.ToString(CultureInfo.InvariantCulture), iconCls = dataserieIconCls, children = generalSerieTree });
						i += 1;
					}
					retVal.Add(seriesNameList);
					retVal.Add(rankList);
				}

			}
			else {
				for (var i = 1; i <= 5; i++) {
					retVal.Add(new TreeNode { Key = "SerieRank(@Model," + i, text = i.ToString(CultureInfo.InvariantCulture), iconCls = dataserieIconCls, children = generalSerieTree });
				}
			}
			return retVal;
		}

		/// <summary>
		/// Gets the ChartModel Series tree.
		/// </summary>
		/// <returns></returns>
		private List<TreeNode> ChartModel_GetSerieTree() {
			var retVal = new List<TreeNode>
							{
								new TreeNode {Key = "\"Name\")", text = Langue.msg_name, leaf = true},
								new TreeNode {Key = "\"Sum\")", text = Langue.msg_enum_mathematicoperator_sum, leaf = true},
								new TreeNode {Key = "\"Average\")", text = Langue.msg_enum_mathematicoperator_avg, leaf = true},
								new TreeNode { Key = "SEPARATOR" },
								new TreeNode {Key = "\"Minimum\")", text = Langue.msg_enum_mathematicoperator_min, leaf = true},
								new TreeNode {Key = "\"MinimumDate\")", text =string.Format("{0} - {1}", Langue.msg_enum_mathematicoperator_min, Langue.msg_chartmodel_date), leaf = true},
								new TreeNode {Key = "\"Maximum\")", text = Langue.msg_enum_mathematicoperator_max, leaf = true},
								new TreeNode {Key = "\"MaximumDate\")", text =string.Format("{0} - {1}", Langue.msg_enum_mathematicoperator_max, Langue.msg_chartmodel_date), leaf = true},
								new TreeNode {Key = "\"First\")", text = Langue.msg_enum_mathematicoperator_first, leaf = true},
								new TreeNode {Key = "\"FirstDate\")", text =string.Format("{0} - {1}", Langue.msg_enum_mathematicoperator_first, Langue.msg_chartmodel_date), leaf = true},
								new TreeNode {Key = "\"Penultimate\")", text = Langue.msg_enum_mathematicoperator_penultimate, leaf = true},
								new TreeNode {Key = "\"PenultimateDate\")", text =string.Format("{0} - {1}", Langue.msg_enum_mathematicoperator_penultimate, Langue.msg_chartmodel_date), leaf = true},
								new TreeNode {Key = "\"Last\")", text = Langue.msg_enum_mathematicoperator_last, leaf = true},
								new TreeNode {Key = "\"LastDate\")", text =string.Format("{0} - {1}", Langue.msg_enum_mathematicoperator_last, Langue.msg_chartmodel_date), leaf = true},
								new TreeNode {Key = "\"Index\")", text = Langue.msg_enum_mathematicoperator_index, leaf = true},
								new TreeNode {Key = "\"IndexDate\")", text =string.Format("{0} - {1}", Langue.msg_enum_mathematicoperator_index, Langue.msg_chartmodel_date), leaf = true},
							  };
			return retVal;
		}

		/// <summary>
		/// Deletes an existing business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartPsetAttributeHistorical_Delete(ChartPsetAttributeHistorical item) {
			ChartPsetAttributeHistoricalBroker broker = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartPsetAttributeHistorical and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ChartPsetAttributeHistorical_FormCreate(ChartPsetAttributeHistorical item) {
			ChartPsetAttributeHistoricalBroker broker = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartPsetAttributeHistorical_FormLoad(string Key) {
			ChartPsetAttributeHistoricalBroker broker = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ChartPsetAttributeHistorical and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ChartPsetAttributeHistorical_FormUpdate(ChartPsetAttributeHistorical item) {
			ChartPsetAttributeHistoricalBroker broker = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartPsetAttributeHistorical_FormUpdateBatch(string[] keys, ChartPsetAttributeHistorical item) {
			ChartPsetAttributeHistoricalBroker broker = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ChartPsetAttributeHistorical. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<ChartPsetAttributeHistorical> ChartPsetAttributeHistorical_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			ChartPsetAttributeHistoricalBroker broker = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public ChartPsetAttributeHistorical ChartPsetAttributeHistorical_GetItem(string Key, params string[] fields) {
			ChartPsetAttributeHistoricalBroker broker = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartPsetAttributeHistorical> ChartPsetAttributeHistorical_GetStore(PagingParameter paging, PagingLocation location) {
			ChartPsetAttributeHistoricalBroker broker = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of ChartPsetAttributeHistorical for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<ChartPsetAttributeHistorical> ChartPsetAttributeHistorical_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			ChartPsetAttributeHistoricalBroker broker = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>();
			return broker.GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartPsetAttributeHistorical.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartPsetAttributeHistorical> ChartPsetAttributeHistorical_SaveStore(CrudStore<ChartPsetAttributeHistorical> store) {
			ChartPsetAttributeHistoricalBroker broker = Helper.CreateInstance<ChartPsetAttributeHistoricalBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity ChartScheduler.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartScheduler_Delete(ChartScheduler item) {
			ChartSchedulerBroker broker = Helper.CreateInstance<ChartSchedulerBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ChartScheduler and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="charts">The charts.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <returns></returns>
		public FormResponse ChartScheduler_FormCreate(ChartScheduler item, CrudStore<Chart> charts, CrudStore<PortalWindow> portalwindows) {
			if (item.StartDate != null)
				item.EndDate = item.StartDate.Value.AddMinutes(1);
			FormResponse response;

			ChartSchedulerBroker broker = Helper.CreateInstance<ChartSchedulerBroker>();
			ChartBroker brokerChart = Helper.CreateInstance<ChartBroker>();
			PortalWindowBroker brokerPortalWindow = Helper.CreateInstance<PortalWindowBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				//var ev = (CalendarEvent)item;
				CalendarHelper.UpdateEventClientDates(item);
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						ChartScheduler chartscheduler = response.data as ChartScheduler;
						if (chartscheduler != null) {
							brokerChart.SaveChartChartScheduler(chartscheduler, charts);
							brokerPortalWindow.SavePortalWindowChartScheduler(chartscheduler, portalwindows);
						}
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartScheduler.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartScheduler_FormLoad(string Key) {
			ChartSchedulerBroker broker = Helper.CreateInstance<ChartSchedulerBroker>();
			var retVal = broker.FormLoad(Key);
			return retVal;
		}

		/// <summary>
		/// Updates an existing business entity ChartScheduler and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="charts">The charts.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <returns></returns>
		public FormResponse ChartScheduler_FormUpdate(ChartScheduler item, CrudStore<Chart> charts, CrudStore<PortalWindow> portalwindows) {
			if (item.StartDate != null)
				item.EndDate = item.StartDate.Value.AddMinutes(1);
			FormResponse response;

			ChartSchedulerBroker broker = Helper.CreateInstance<ChartSchedulerBroker>();
			ChartBroker brokerChart = Helper.CreateInstance<ChartBroker>();
			PortalWindowBroker brokerPortalWindow = Helper.CreateInstance<PortalWindowBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				//var ev = (CalendarEvent)item;
				CalendarHelper.UpdateEventClientDates(item);
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						ChartScheduler chartscheduler = response.data as ChartScheduler;
						if (chartscheduler != null) {
							brokerChart.SaveChartChartScheduler(chartscheduler, charts);
							brokerPortalWindow.SavePortalWindowChartScheduler(chartscheduler, portalwindows);
						}
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartScheduler_FormUpdateBatch(string[] keys, ChartScheduler item) {
			ChartSchedulerBroker broker = Helper.CreateInstance<ChartSchedulerBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Generates the Scheduled Reports and sends them via email.
		/// </summary>
		/// <param name="KeyChartScheduler">the ChartScheduler Key.</param>
		/// <returns></returns>
		public void ChartScheduler_Generate(string KeyChartScheduler) {
			var currentCulture = Thread.CurrentThread.CurrentCulture;
			var currentUICulture = Thread.CurrentThread.CurrentUICulture;
			var currentTimeZone = TimeZoneHelper.CurrentUserTimeZone;

			try {

				ChartSchedulerBroker broker = Helper.CreateInstance<ChartSchedulerBroker>();
				ChartScheduler scheduler = broker.GetItem(KeyChartScheduler, "*");
				TimeZoneHelper.CurrentUserTimeZone = scheduler.TimeZone;

				if (!string.IsNullOrEmpty(scheduler.KeyLocalizationCulture)) {
					Thread.CurrentThread.CurrentCulture = new CultureInfo(scheduler.KeyLocalizationCulture, false);
					Thread.CurrentThread.CurrentUICulture = new CultureInfo(scheduler.KeyLocalizationCulture, false);
				}

				var attachments = new List<Attachment>();
				var errorMessage = new StringBuilder();
				const string newLine = "--------------------------------";

				if (scheduler.Charts != null) {
					foreach (Chart chart in scheduler.Charts) {
						try {
							Chart_ClearCache(chart.KeyChart, chart.KeyDynamicDisplay);
							StreamResult stream = Chart_GetStreamExport(chart.KeyChart, scheduler.ExportType, true);
							attachments.Add(new Attachment(stream.ContentStream, stream.FullFileName, stream.MimeType));
						}
						catch (Exception e) {
							errorMessage.AppendLine(chart.Title);
							errorMessage.AppendLine(newLine);
							errorMessage.AppendLine(Langue.error_msg_VizeliaException);
							errorMessage.AppendLine(newLine);
							errorMessage.AppendLine(e.ToString());
							errorMessage.AppendLine();
						}
					}
				}
				if (scheduler.PortalWindows != null) {
					foreach (PortalWindow portalWindow in scheduler.PortalWindows) {
						try {
							List<StreamResult> streams = PortalWindow_GetStreamExport(portalWindow.KeyPortalWindow, scheduler.ExportType, true);
							attachments.AddRange(streams.Select(stream => new Attachment(stream.ContentStream, stream.FullFileName, stream.MimeType)));
						}
						catch (Exception e) {
							errorMessage.AppendLine(portalWindow.Title);
							errorMessage.AppendLine(newLine);
							errorMessage.AppendLine(Langue.error_msg_VizeliaException);
							errorMessage.AppendLine(newLine);
							errorMessage.AppendLine(e.ToString());
							errorMessage.AppendLine();
						}
					}

				}
				if (errorMessage.Length > 0) {
					var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(errorMessage.ToString());
					var m = new MemoryStream(bytes);
					attachments.Add(new Attachment(m, "error.txt"));
				}

				var mailBroker = Helper.CreateInstance<MailBroker>();
				var mails = mailBroker.GetListByClassificationItemLocalId(MailBroker.const_chartscheduler);

				if (mails == null || !mails.Any()) {
					TracingService.Write(TraceEntrySeverity.Error, string.Format("Could not get mails with classification item local ID {0}, cannot send chart scheduler e-mails.", MailBroker.const_chartscheduler));
				}

				var mailMessages = MailService.ConvertMailsToMailMessages(new ModelDataChartScheduler { ChartScheduler = scheduler }, mails);
				foreach (var mailMessage in mailMessages) {
					foreach (var attachment in attachments) {
						mailMessage.Attachments.Add(attachment);
					}
					MailService.Send(mailMessage);
				}
			}
			finally {
				TimeZoneHelper.CurrentUserTimeZone = currentTimeZone;
				Thread.CurrentThread.CurrentCulture = currentCulture;
				Thread.CurrentThread.CurrentUICulture = currentUICulture;
			}
		}

		/// <summary>
		/// Generates the Scheduled Reports and sends them via email.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="KeyChartScheduler">the ChartScheduler Key.</param>
		public void ChartScheduler_GenerateBegin(Guid operationId, string KeyChartScheduler) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				ChartScheduler_Generate(KeyChartScheduler);
				var result =
					new LongRunningOperationResult(
						LongRunningOperationStatus.Completed);
				return result;
			});
		}

		/// <summary>
		/// Gets a list for the business entity ChartScheduler. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<ChartScheduler> ChartScheduler_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			ChartSchedulerBroker broker = Helper.CreateInstance<ChartSchedulerBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartScheduler.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public ChartScheduler ChartScheduler_GetItem(string Key, params string[] fields) {
			ChartSchedulerBroker broker = Helper.CreateInstance<ChartSchedulerBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartScheduler.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartScheduler> ChartScheduler_GetStore(PagingParameter paging, PagingLocation location) {
			ChartSchedulerBroker broker = Helper.CreateInstance<ChartSchedulerBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Process all the ChartScheduler and sends the one(s) that needs to be sent.
		/// </summary>
		public void ChartScheduler_Process(JobContext jobContext) {

			JobBroker jbroker = Helper.CreateInstance<JobBroker>();
			var job = jbroker.GetItem(jobContext.JobKey.Split('.')[0]);
			if (job == null)
				return;
			var schedule = job.Schedules.First();
			var broker = Helper.CreateInstance<ChartSchedulerBroker>();

			var chartSchedulers = broker.GetAll(false);
			foreach (var chartScheduler in chartSchedulers) {
				if (chartScheduler.NextOccurenceDate.HasValue && chartScheduler.Enabled) {
					var timeSpan = chartScheduler.NextOccurenceDate.Value.ToUniversalTime() - DateTime.UtcNow;
					if ((timeSpan.TotalSeconds > 0) && (timeSpan.TotalSeconds <= (m_FrequencyInSeconds[schedule.RecurrencePattern.Frequency] * schedule.RecurrencePattern.Interval))) {
						try {
							ChartScheduler_Generate(chartScheduler.KeyChartScheduler);
						}
						catch (Exception e) {
							using (TracingService.StartTracing("Chart Scheduler", chartScheduler.KeyChartScheduler)) {
								TracingService.Write(e);
							}
						}
					}
				}
			}
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartScheduler.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartScheduler> ChartScheduler_SaveStore(CrudStore<ChartScheduler> store) {
			ChartSchedulerBroker broker = Helper.CreateInstance<ChartSchedulerBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Checks the last data against frequency.
		/// </summary>
		/// <param name="AcquisitionDateTime">The acquisition date time.</param>
		/// <param name="Frequency">The frequency.</param>
		/// <returns></returns>
		private bool CheckLastDataAgainstFrequency(DateTime AcquisitionDateTime, int Frequency) {
			if ((DateTime.Now.ToUniversalTime() - AcquisitionDateTime).TotalSeconds >= (Frequency * 60 - 10)) {
				return true;
			}
			return false;
		}

		/// <summary>
		/// Return a Classification item (from hierrchy).
		/// </summary>
		/// <param name="Key">The key of the ClassificationItem.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public ClassificationItem ClassificationItem_GetItem(string Key, params string[] fields) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			var retVal = broker.GetItemFromHierarchy(Key);
			return retVal;
		}

		/// <summary>
		/// Get the classificationitem Root for meter as KeyParent / Key.
		/// </summary>
		/// <returns></returns>
		public string ClassificationItem_GetMeterFilter() {
			var retVal = "";
			var meterClassification = ClassificationItem_GetMeterRoot();
			if (meterClassification != null) {
				retVal = string.Format("{0}  / {1}", meterClassification.KeyParent, meterClassification.KeyClassificationItem);
			}
			return retVal;
		}

		/// <summary>
		/// Get the max Classification item level for Meters.
		/// </summary>
		/// <returns></returns>
		protected int ClassificationItem_GetMeterMaxLevel() {
			var retVal = 1;
			var meterClassificationFilter = ClassificationItem_GetMeterFilter();
			var classificationItemHierarchy = m_CoreBusinessLayer.ClassificationItem_GetDictionary(meterClassificationFilter);

			if (classificationItemHierarchy != null) {
				retVal = classificationItemHierarchy.Values.Select(classificationItem => classificationItem.Level).Concat(new[] { retVal }).Max();
			}
			return retVal;
		}

		/// <summary>
		/// Get the classificationitem Root of meter;
		/// </summary>
		/// <returns></returns>
		protected ClassificationItem ClassificationItem_GetMeterRoot() {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			return broker.GetCategoryRoot(ClassificationCategory.Meter);
		}

		/// <summary>
		/// Get the tree of DataAcquisition instances and endpoints.
		/// </summary>
		/// <param name="entity">The parent entity.</param>
		/// <param name="entityContainer">The entity container.</param>
		/// <returns></returns>
		public List<TreeNode> DataAcquisition_GetTree(DataAcquisitionItem entity, DataAcquisitionContainer entityContainer) {
			var retVal = new List<TreeNode>();

			if (entity == null && entityContainer == null) {
				// When does this happen?
				var dataContainerTypes = DataAcquisitionService.GetDataAcquisitionContainerTypes();

				foreach (var dataContainerType in dataContainerTypes) {
					var broker = (IBusinessEntityBroker)BrokerFactory.CreateBroker(dataContainerType);
					var containers = broker.GetAll();

					foreach (var containerObject in containers) {
						var container = (DataAcquisitionContainer)containerObject;
						var node = container.GetTree();
						node.children = DataAcquisitionService.GetHierarchy(container);
						retVal.Add(node);
					}
				}
			}
			else if (entity == null) {
				// Get all items under the given entity container.
				var broker = (IBusinessEntityBroker)BrokerFactory.CreateBroker(entityContainer.GetType());
				var container = (DataAcquisitionContainer)broker.GetItem(entityContainer.KeyDataAcquisitionContainer);
				var node = container.GetTree();
				node.children = DataAcquisitionService.GetHierarchy(container);
				retVal.Add(node);
			}
			else {
				//?
				var broker = (IBusinessEntityBroker)BrokerFactory.CreateBroker(entity.GetDataAcquisitionContainerType());
				var container = (DataAcquisitionContainer)broker.GetItem(entity.KeyDataAcquisitionContainer);
				retVal = DataAcquisitionService.GetHierarchy(container, entity);
			}

			return retVal;
		}

		/// <summary>
		/// Get the stream image of	a data acquisition endpoint.
		/// </summary>
		/// <param name="EndpointType">Type of the endpoint.</param>
		/// <param name="KeyEndpoint">The key endpoint.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public ImageMap DataAcquisitionEndpoint_BuildImage(string EndpointType, string KeyEndpoint, int width, int height) {
			ImageMap retVal = null;
			DataAcquisitionEndpoint endpoint;
			var data = DataAcquisitionEndpoint_Process(EndpointType, KeyEndpoint, out  endpoint);
			if (endpoint != null) {
				var chart = ChartHelper.GetBogusChart();
				chart.Series.Clear();
				chart.TimeInterval = AxisTimeInterval.None;
				var dataserie = new DataSerie {
					LocalId = endpoint.Name,
					Name = endpoint.Name,
					GroupOperation = MathematicOperator.AVG,
					Points = new List<DataPoint>(),
					Type = DataSerieType.Line
				};
				foreach (var d in data) {
					dataserie.Points.Add(new DataPoint {
						XDateTime = d.AcquisitionDateTime,
						YValue = d.Value
					});
				}
				chart.Series.Add(dataserie);

				retVal = ChartingService.RenderChartImageMap(chart, width, height, null, new Padding());
			}
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity DataAcquisitionEndpoint.
		/// paging is done in memory because of the multiples brokers.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<DataAcquisitionEndpoint> DataAcquisitionEndpoint_GetStore(PagingParameter paging) {
			var retVal = new List<DataAcquisitionEndpoint>();
			var dataEndpointTypes = DataAcquisitionService.GetDataAcquisitionEndpointTypes();
			foreach (var dataEndpointType in dataEndpointTypes) {
				var broker = (IBusinessEntityBroker)BrokerFactory.CreateBroker(dataEndpointType);
				var endpoints = broker.GetAll().Cast<DataAcquisitionEndpoint>();
				retVal.AddRange(endpoints);
			}
			return retVal.ToJsonStoreWithFilter(paging);
		}

		/// <summary>
		/// Process a Data Acquisition endpoint based on the type and Key.
		/// </summary>
		/// <param name="EndpointType">Type of the endpoint.</param>
		/// <param name="KeyEndpoint">The key endpoint.</param>
		/// <returns></returns>
		public List<DataAcquisitionEndpointValue> DataAcquisitionEndpoint_Process(string EndpointType, string KeyEndpoint) {
			DataAcquisitionEndpoint endpoint;
			return DataAcquisitionEndpoint_Process(EndpointType, KeyEndpoint, out endpoint);
		}

		/// <summary>
		/// Process a Data Acquisition endpoint based on the type and Key.
		/// </summary>
		/// <param name="EndpointType">Type of the endpoint.</param>
		/// <param name="KeyEndpoint">The key endpoint.</param>
		/// <param name="endpoint">The endpoint.</param>
		/// <returns></returns>
		public List<DataAcquisitionEndpointValue> DataAcquisitionEndpoint_Process(string EndpointType, string KeyEndpoint, out DataAcquisitionEndpoint endpoint) {
			var retVal = new List<DataAcquisitionEndpointValue>();
			endpoint = null;
			try {
				var endpointType = Type.GetType(EndpointType + ", Vizelia.FOL.Common");
				var endpointBroker = (IBusinessEntityBroker)BrokerFactory.CreateBroker(endpointType);
				endpoint = (DataAcquisitionEndpoint)endpointBroker.GetItem(KeyEndpoint);

				if (endpoint != null) {


					var containerBroker = (IBusinessEntityBroker)BrokerFactory.CreateBroker(endpoint.GetDataAcquisitionContainerType());
					var container = (DataAcquisitionContainer)containerBroker.GetItem(endpoint.KeyDataAcquisitionContainer);

					retVal = DataAcquisitionService.GetCurrentValue(container, endpoint);

				}
			}
			catch { }
			return retVal;
		}


		/// <summary>
		/// Gets json store of datapoint from a chart.
		/// </summary>
		/// <param name="keyChart">The KeyChart.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public JsonStore<DataPoint> DataPoint_GetStoreFromChart(string keyChart, PagingParameter paging, Chart chart = null) {
			int recordCount;
			var retval = DataPoint_GetListFromChart(keyChart, paging, out recordCount, chart);
			return retval.ToJsonStore(recordCount);
		}

		/// <summary>
		/// Gets list of datapoint from a chart.
		/// </summary>
		/// <param name="keyChart">The KeyChart.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="recordCount">The record count.</param>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public List<DataPoint> DataPoint_GetListFromChart(string keyChart, PagingParameter paging, out int recordCount, Chart chart = null) {

			paging = paging ?? new PagingParameter();

			var chartWithData = Chart_GetItemWithData(keyChart, false, chart == null, chart);
			var list = DataPoint_GetListFromChart(chartWithData, true, paging, out recordCount);

			return list;
		}


		/// <summary>
		/// Generate Bogus datapoint for performance analysis.
		/// </summary>
		/// <param name="maxCount">The max count.</param>
		/// <returns></returns>
		private List<DataPoint> DataPoint_GenerateBogus(int maxCount) {
			var r = new List<DataPoint>();
			for (var i = 0; i < maxCount; i++) {
				var serieName = "Serie " + i;
				var startDate = new DateTime(2012, 1, 1);
				for (var j = 1; j < 24; j++) {

					var p = new DataPoint() {
						YValue = 100,
						XDateTime = startDate,
						SeriesName = serieName,
						Classification = (startDate.Year == 2012) ? "Actual" : "Budget",
						Location = "Building " + i,
						Name = startDate.ToString("MMMM yy"),
						KeyDataPoint = Guid.NewGuid().ToString()
					};
					r.Add(p);
					startDate = startDate.AddMonths(1);
				}

			}
			return r;
		}

		/// <summary>
		/// Gets ths store of datapoint from a chart.
		/// </summary>
		/// <param name="chartWithData">The chart with data.</param>
		/// <param name="usePaging">if set to <c>true</c> [use paging].</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="recordCount">The record count.</param>
		/// <returns></returns>
		private List<DataPoint> DataPoint_GetListFromChart(Chart chartWithData, bool usePaging, PagingParameter paging, out int recordCount) {
			var result = new List<DataPoint>();
			if (chartWithData.Series == null) {
				recordCount = 0;
				return result;
			}

			IEnumerable<DataSerie> series =
				chartWithData.DisplayLegendSort == ChartLegendSort.ByNameDesc ?
				chartWithData.Series.AsEnumerable().OrderByDescending(s => s.Name) :
				chartWithData.Series.AsEnumerable().OrderBy(s => s.Name);

			if (chartWithData.HistoricalAnalysis != null && chartWithData.HistoricalAnalysis.Count > 0) {
				series = chartWithData.HistoricalAnalysis.Values.Aggregate(series, (current, dsc) => current.Concat(dsc.AsEnumerable()));
			}
			var maxCount = 0;
			int seriesCount = 0;
			foreach (DataSerie s in series) {
				var resultSub = new List<DataPoint>();
				if (s.Hidden == false && s.PointsWithValue.Count > 0) {
					DataPoint previousPoint = null;
					foreach (var p in s.PointsWithValue) {
						p.SeriesName = (!chartWithData.GridHideUnit && !string.IsNullOrEmpty(s.Unit)) ? string.Format("{0} {1}", s.Name, s.Unit) : s.Name;
						p.Location = !string.IsNullOrEmpty(s.LocationName) ? s.LocationName : s.Name;
						p.Classification = s.Name;
						if (chartWithData.GridAggregateByCalculatedSerieClassification && !string.IsNullOrEmpty(s.ClassificationName)) {
							if (chartWithData.UseClassificationPath && !string.IsNullOrEmpty(s.ClassificationItemLongPath))
								p.Classification = s.ClassificationItemLongPath;
							else
								p.Classification = s.ClassificationName;
						}

						//since the SeriesName is not used in the pivot grid, we use the classification to display the Unit.
						if (!string.IsNullOrEmpty(s.Unit) && !chartWithData.GridHideUnit)
							p.Classification += " " + s.Unit;

						if (string.IsNullOrEmpty(p.Name) && p.XDateTime.HasValue) {
							if (chartWithData.TimeInterval == AxisTimeInterval.Quarters) {
								p.Name = Helper.ConvertDateTimeToQuarterString(p.XDateTime.Value);
							}
							else if (chartWithData.TimeInterval == AxisTimeInterval.Semesters) {
								p.Name = Helper.ConvertDateTimeToSemesterString(p.XDateTime.Value);
							}
							else {
								p.Name = p.XDateTime.Value.ToString(chartWithData.TimeIntervalFormat);
							}
						}




						if (chartWithData.DataGridPercentage != ChartDataGridPercentage.None && previousPoint != null) {
							double delta;
							p.Tooltip = p.YValue.Value.ToString(chartWithData.NumericFormat) + " (" + ChartHelper.GetDifferenceAsPercentage(p.YValue, previousPoint.YValue, out delta) + ")";
							if (double.IsNaN(delta) == false) {
								var color = "#FF0000";
								if ((delta > 0 && chartWithData.DataGridPercentage == ChartDataGridPercentage.IncreaseInGreen) || (delta <= 0 && chartWithData.DataGridPercentage == ChartDataGridPercentage.DecreaseInGreen)) {
									color = "#00FF00";
								}
								p.Tooltip = string.Format("<font color='{0}' >{1}</font>", color, p.Tooltip);
							}
						}
						else {
							p.Tooltip = p.YValue.Value.ToString(chartWithData.NumericFormat);
						}
						previousPoint = p;
						resultSub.Add(p);
					}

					if (usePaging) {
						if (!chartWithData.GridTranspose) {
							// we need to filter each individual dataserie.
							maxCount = Math.Max(maxCount, resultSub.Count);
							result.AddRange(resultSub.Rank(paging.sort, paging.dir, paging.start, paging.limit));
						}
						else if (seriesCount >= paging.start && seriesCount < paging.start + paging.limit) {
							result.AddRange(resultSub);
						}
					}
					else {
						result.AddRange(resultSub);
					}
					seriesCount += 1;
				}
			}

			recordCount = chartWithData.GridTranspose ? seriesCount : maxCount;

			return result;
		}



		/// <summary>
		/// Clear the cache of the associcated Charts.
		/// </summary>
		/// <param name="KeyDataSerie">The key data serie.</param>
		/// <returns></returns>
		public void DataSerie_ClearCache(string KeyDataSerie) {
			DataSerieBroker broker = Helper.CreateInstance<DataSerieBroker>();
			var dataserie = broker.GetItem(KeyDataSerie);
			if (dataserie != null) {
				Chart_ClearCache(dataserie.KeyChart);
			}
		}

		/// <summary>
		/// Deletes an existing business entity DataSerie.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DataSerie_Delete(DataSerie item) {
			DataSerieBroker broker = Helper.CreateInstance<DataSerieBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DataSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DataSerie_FormCreate(DataSerie item) {
			DataSerieBroker broker = Helper.CreateInstance<DataSerieBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DataSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DataSerie_FormLoad(string Key) {
			DataSerieBroker broker = Helper.CreateInstance<DataSerieBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DataSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="ratios">The ratios.</param>
		/// <param name="colorelements">The colorelements.</param>
		/// <returns></returns>
		public FormResponse DataSerie_FormUpdate(DataSerie item, CrudStore<DataSeriePsetRatio> ratios, CrudStore<DataSerieColorElement> colorelements) {
			FormResponse response;
			var broker = Helper.CreateInstance<DataSerieBroker>();
			var brokerPsetRatio = Helper.CreateInstance<DataSeriePsetRatioBroker>();
			var brokerColorElement = Helper.CreateInstance<DataSerieColorElementBroker>();
			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						var dataserie = response.data as DataSerie;
						if (dataserie != null) {
							brokerPsetRatio.SaveDataSerieDataSeriePsetRatio(dataserie, ratios);
							brokerColorElement.SaveDataSerieDataSerieColorElement(dataserie, colorelements);
						}
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DataSerie_FormUpdateBatch(string[] keys, DataSerie item) {
			DataSerieBroker broker = Helper.CreateInstance<DataSerieBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<DataSerie> DataSerie_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			DataSerieBroker broker = Helper.CreateInstance<DataSerieBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity DataSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public DataSerie DataSerie_GetItem(string Key, params string[] fields) {
			DataSerieBroker broker = Helper.CreateInstance<DataSerieBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DataSerie> DataSerie_GetStore(PagingParameter paging, PagingLocation location) {
			DataSerieBroker broker = Helper.CreateInstance<DataSerieBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of dataserie for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<DataSerie> DataSerie_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			DataSerieBroker broker = Helper.CreateInstance<DataSerieBroker>();
			return broker.GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity DataSerie.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DataSerie> DataSerie_SaveStore(CrudStore<DataSerie> store) {
			DataSerieBroker broker = Helper.CreateInstance<DataSerieBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Toggle the visibility of a specific dataserie (and creates it of its not instanciated yet).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyDataSerie">The key data serie.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="Visible">if set to <c>true</c> [visible].</param>
		/// <param name="serieName">Name of the serie as optional parameter. If not suplied it will default to the localId</param>
		/// <returns></returns>
		public DataSerie DataSerie_ToggleVisibility(string KeyChart, string KeyDataSerie, string localId, bool Visible, string serieName = null) {
			var broker = Helper.CreateInstance<DataSerieBroker>();
			var serie = broker.GetOrCreate(KeyChart, KeyDataSerie, localId, serieName);
			serie.Hidden = !Visible;
			serie = broker.Save(serie);
			return serie;
		}

		/// <summary>
		/// Update the type of a specific dataserie (and creates it of its not instanciated yet).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyDataSerie">The key data serie.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="type">The type.</param>
		/// <param name="serieName">Name of the serie as optional parameter. If not suplied it will default to the localId</param>
		/// <returns></returns>
		public DataSerie DataSerie_TypeUpdate(string KeyChart, string KeyDataSerie, string localId, DataSerieType type, string serieName = null) {
			var broker = Helper.CreateInstance<DataSerieBroker>();
			var serie = broker.GetOrCreate(KeyChart, KeyDataSerie, localId, serieName);
			serie.Type = type;
			serie = broker.Save(serie);
			return serie;
		}



		/// <summary>
		/// Assign a secondary axis to a specific dataserie (and creates it of its not instanciated yet).
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyDataSerie">The key data serie.</param>
		/// <param name="localId">The local id.</param>
		/// <param name="serieName">Name of the serie as optional parameter. If not suplied it will default to the localId</param>
		/// <returns></returns>
		public DataSerie DataSerie_UseSecondaryAxis(string KeyChart, string KeyDataSerie, string localId,string serieName = null) {
			var serieBroker = Helper.CreateInstance<DataSerieBroker>();
			var chartAxisBroker = Helper.CreateInstance<ChartAxisBroker>();

			var axisList = chartAxisBroker.GetListByKeyChart(KeyChart);
			var serie = serieBroker.GetOrCreate(KeyChart, KeyDataSerie, localId, serieName);

			var axis = axisList.FirstOrDefault(a => !a.IsPrimary);
			if (axis == null) {
				axis = new ChartAxis {
					KeyChart = KeyChart,
					IsPrimary = false,
					Name = string.IsNullOrEmpty(serie.Unit) ? " " : serie.Unit
				};
				axis = chartAxisBroker.Save(axis);
			}
			serie.KeyYAxis = axis.KeyChartAxis;
			serie = serieBroker.Save(serie);

			return serie;
		}

		/// <summary>
		/// Deletes an existing business entity DataSerieColorElement.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DataSerieColorElement_Delete(DataSerieColorElement item) {
			DataSerie_ClearCache(item.KeyDataSerie);
			DataSerieColorElementBroker broker = Helper.CreateInstance<DataSerieColorElementBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DataSerieColorElement and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DataSerieColorElement_FormCreate(DataSerieColorElement item) {
			DataSerie_ClearCache(item.KeyDataSerie);
			DataSerieColorElementBroker broker = Helper.CreateInstance<DataSerieColorElementBroker>();
			Helper.CreateInstance<DataSerieBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DataSerieColorElement.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DataSerieColorElement_FormLoad(string Key) {
			DataSerieColorElementBroker broker = Helper.CreateInstance<DataSerieColorElementBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DataSerieColorElement and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DataSerieColorElement_FormUpdate(DataSerieColorElement item) {
			DataSerie_ClearCache(item.KeyDataSerie);
			DataSerieColorElementBroker broker = Helper.CreateInstance<DataSerieColorElementBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DataSerieColorElement_FormUpdateBatch(string[] keys, DataSerieColorElement item) {
			DataSerieColorElementBroker broker = Helper.CreateInstance<DataSerieColorElementBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DataSerieColorElement. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<DataSerieColorElement> DataSerieColorElement_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			DataSerieColorElementBroker broker = Helper.CreateInstance<DataSerieColorElementBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity DataSerieColorElement.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public DataSerieColorElement DataSerieColorElement_GetItem(string Key, params string[] fields) {
			DataSerieColorElementBroker broker = Helper.CreateInstance<DataSerieColorElementBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity DataSerieColorElement.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DataSerieColorElement> DataSerieColorElement_GetStore(PagingParameter paging, PagingLocation location) {
			DataSerieColorElementBroker broker = Helper.CreateInstance<DataSerieColorElementBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of DataSerieColorElement for a specific DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyDataSerie">The key of the dataserie.</param>
		/// <returns></returns>
		public JsonStore<DataSerieColorElement> DataSerieColorElement_GetStoreByKeyDataSerie(PagingParameter paging, string KeyDataSerie) {
			DataSerieColorElementBroker broker = Helper.CreateInstance<DataSerieColorElementBroker>();
			return broker.GetStoreByKeyDataSerie(paging, KeyDataSerie);
		}

		/// <summary>
		/// Saves a crud store for the business entity DataSerieColorElement.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DataSerieColorElement> DataSerieColorElement_SaveStore(CrudStore<DataSerieColorElement> store) {
			DataSerieColorElementBroker broker = Helper.CreateInstance<DataSerieColorElementBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DataSeriePsetRatio_Delete(DataSeriePsetRatio item) {
			DataSerie_ClearCache(item.KeyDataSerie);
			DataSeriePsetRatioBroker broker = Helper.CreateInstance<DataSeriePsetRatioBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DataSeriePsetRatio and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DataSeriePsetRatio_FormCreate(DataSeriePsetRatio item) {
			DataSerie_ClearCache(item.KeyDataSerie);
			DataSeriePsetRatioBroker broker = Helper.CreateInstance<DataSeriePsetRatioBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DataSeriePsetRatio_FormLoad(string Key) {
			DataSeriePsetRatioBroker broker = Helper.CreateInstance<DataSeriePsetRatioBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DataSeriePsetRatio and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DataSeriePsetRatio_FormUpdate(DataSeriePsetRatio item) {
			DataSerie_ClearCache(item.KeyDataSerie);
			DataSeriePsetRatioBroker broker = Helper.CreateInstance<DataSeriePsetRatioBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DataSeriePsetRatio_FormUpdateBatch(string[] keys, DataSeriePsetRatio item) {
			DataSeriePsetRatioBroker broker = Helper.CreateInstance<DataSeriePsetRatioBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DataSeriePsetRatio. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<DataSeriePsetRatio> DataSeriePsetRatio_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			DataSeriePsetRatioBroker broker = Helper.CreateInstance<DataSeriePsetRatioBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public DataSeriePsetRatio DataSeriePsetRatio_GetItem(string Key, params string[] fields) {
			DataSeriePsetRatioBroker broker = Helper.CreateInstance<DataSeriePsetRatioBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DataSeriePsetRatio> DataSeriePsetRatio_GetStore(PagingParameter paging, PagingLocation location) {
			DataSeriePsetRatioBroker broker = Helper.CreateInstance<DataSeriePsetRatioBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of DataSeriePsetRatio for a specific DataSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyDataSerie">The key of the dataserie.</param>
		/// <returns></returns>
		public JsonStore<DataSeriePsetRatio> DataSeriePsetRatio_GetStoreByKeyDataSerie(PagingParameter paging, string KeyDataSerie) {
			DataSeriePsetRatioBroker broker = Helper.CreateInstance<DataSeriePsetRatioBroker>();
			return broker.GetStoreByKeyDataSerie(paging, KeyDataSerie);
		}

		/// <summary>
		/// Saves a crud store for the business entity DataSeriePsetRatio.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DataSeriePsetRatio> DataSeriePsetRatio_SaveStore(CrudStore<DataSeriePsetRatio> store) {
			DataSeriePsetRatioBroker broker = Helper.CreateInstance<DataSeriePsetRatioBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DrawingCanvas.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DrawingCanvas_Delete(DrawingCanvas item) {
			DrawingCanvasBroker broker = Helper.CreateInstance<DrawingCanvasBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DrawingCanvas and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvas_FormCreate(DrawingCanvas item) {
			DrawingCanvasBroker broker = Helper.CreateInstance<DrawingCanvasBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DrawingCanvas.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvas_FormLoad(string Key) {
			DrawingCanvasBroker broker = Helper.CreateInstance<DrawingCanvasBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DrawingCanvas and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="charts">The charts.</param>
		/// <param name="images">The images.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvas_FormUpdate(DrawingCanvas item, CrudStore<DrawingCanvasChart> charts,
														CrudStore<DrawingCanvasImage> images) {
			FormResponse response;
			DrawingCanvasBroker broker = Helper.CreateInstance<DrawingCanvasBroker>();

			DrawingCanvasChartBroker brokerChart = Helper.CreateInstance<DrawingCanvasChartBroker>();
			DrawingCanvasImageBroker brokerImage = Helper.CreateInstance<DrawingCanvasImageBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					var drawingCanvas = (DrawingCanvas)response.data;
					try {
						brokerChart.SaveDrawingCanvasDrawingCanvasChart(drawingCanvas, charts);
						brokerImage.SaveDrawingCanvasDrawingCanvasImage(drawingCanvas, images);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvas_FormUpdateBatch(string[] keys, DrawingCanvas item) {
			DrawingCanvasBroker broker = Helper.CreateInstance<DrawingCanvasBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DrawingCanvas. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<DrawingCanvas> DrawingCanvas_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			DrawingCanvasBroker broker = Helper.CreateInstance<DrawingCanvasBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity DrawingCanvas.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public DrawingCanvas DrawingCanvas_GetItem(string Key, params string[] fields) {
			DrawingCanvasBroker broker = Helper.CreateInstance<DrawingCanvasBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DrawingCanvas> DrawingCanvas_GetStore(PagingParameter paging, PagingLocation location) {
			DrawingCanvasBroker broker = Helper.CreateInstance<DrawingCanvasBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity DrawingCanvas.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DrawingCanvas> DrawingCanvas_SaveStore(CrudStore<DrawingCanvas> store) {
			DrawingCanvasBroker broker = Helper.CreateInstance<DrawingCanvasBroker>();
			return broker.SaveStore(store);
		}



		/// <summary>
		/// Drawings the canvas_ get stream image.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="KeyLocalisationCulture">The key localisation culture.</param>
		/// <returns></returns>
		public StreamResult DrawingCanvas_GetStreamImage(string Key, int width, int height, string Creator = null, string KeyLocalisationCulture = null) {
			if (string.IsNullOrEmpty(Creator)) {
				var membershipBroker = Helper.CreateInstance<FOLMembershipUserBroker>();
				var currentUser = membershipBroker.User_GetByUserName(Helper.GetCurrentUserName());
				Creator = currentUser.ProviderUserKey.KeyUser.Substring(1);
			}
			else {
				Creator = int.Parse(Creator.Substring(1)).ToString(CultureInfo.InvariantCulture);
			}

			if (KeyLocalisationCulture != null) {
				KeyLocalisationCulture = Helper.IsValidCulture(KeyLocalisationCulture) ? KeyLocalisationCulture : "";
			}

			var url = string.Format("?xtype={0}&keyPropertyName={1}&keyPropertyValue={2}&impersonateUserKey={3}&applicationName={4}", "vizPortletDrawingCanvas", "KeyDrawingCanvas",
				Key, Creator, ContextHelper.ApplicationName);
			var retVal = ScreenshotService.GetScreenshot(url, true, width, height, ScreenshotType.PNG, MimeType.Png, KeyLocalisationCulture);

			return retVal;
		}


		/// <summary>
		/// Deletes an existing business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DrawingCanvasChart_Delete(DrawingCanvasChart item) {
			DrawingCanvasChartBroker broker = Helper.CreateInstance<DrawingCanvasChartBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DrawingCanvasChart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasChart_FormCreate(DrawingCanvasChart item) {
			DrawingCanvasChartBroker broker = Helper.CreateInstance<DrawingCanvasChartBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasChart_FormLoad(string Key) {
			DrawingCanvasChartBroker broker = Helper.CreateInstance<DrawingCanvasChartBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DrawingCanvasChart and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasChart_FormUpdate(DrawingCanvasChart item) {
			DrawingCanvasChartBroker broker = Helper.CreateInstance<DrawingCanvasChartBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasChart_FormUpdateBatch(string[] keys, DrawingCanvasChart item) {
			DrawingCanvasChartBroker broker = Helper.CreateInstance<DrawingCanvasChartBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DrawingCanvasChart. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<DrawingCanvasChart> DrawingCanvasChart_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			DrawingCanvasChartBroker broker = Helper.CreateInstance<DrawingCanvasChartBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public DrawingCanvasChart DrawingCanvasChart_GetItem(string Key, params string[] fields) {
			DrawingCanvasChartBroker broker = Helper.CreateInstance<DrawingCanvasChartBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a list of DrawingCanvasChart for a specific DrawingCanvas.
		/// </summary>
		/// <param name="KeyDrawingCanvas">The key of the DrawingCanvas.</param>
		/// <returns></returns>
		public List<DrawingCanvasChart> DrawingCanvasChart_GetListByKeyDrawingCanvas(string KeyDrawingCanvas) {
			DrawingCanvasChartBroker broker = Helper.CreateInstance<DrawingCanvasChartBroker>();
			var retVal = broker.GetListByKeyDrawingCanvas(KeyDrawingCanvas);
			//broker.PopulateList(retVal, new string[] { "*" });
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DrawingCanvasChart> DrawingCanvasChart_GetStore(PagingParameter paging, PagingLocation location) {
			DrawingCanvasChartBroker broker = Helper.CreateInstance<DrawingCanvasChartBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of DrawingCanvasChart for a specific DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyDrawingCanvas">The key of the DrawingCanvas.</param>
		/// <returns></returns>
		public JsonStore<DrawingCanvasChart> DrawingCanvasChart_GetStoreByKeyDrawingCanvas(PagingParameter paging,
																							string KeyDrawingCanvas) {
			DrawingCanvasChartBroker broker = Helper.CreateInstance<DrawingCanvasChartBroker>();
			var retVal = broker.GetStoreByKeyDrawingCanvas(paging, KeyDrawingCanvas);
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity DrawingCanvasChart.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DrawingCanvasChart> DrawingCanvasChart_SaveStore(CrudStore<DrawingCanvasChart> store) {
			DrawingCanvasChartBroker broker = Helper.CreateInstance<DrawingCanvasChartBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DrawingCanvasImage_Delete(DrawingCanvasImage item) {
			DrawingCanvasImageBroker broker = Helper.CreateInstance<DrawingCanvasImageBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DrawingCanvasImage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasImage_FormCreate(DrawingCanvasImage item) {
			DrawingCanvasImageBroker broker = Helper.CreateInstance<DrawingCanvasImageBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasImage_FormLoad(string Key) {
			DrawingCanvasImageBroker broker = Helper.CreateInstance<DrawingCanvasImageBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DrawingCanvasImage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasImage_FormUpdate(DrawingCanvasImage item) {
			DrawingCanvasImageBroker broker = Helper.CreateInstance<DrawingCanvasImageBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DrawingCanvasImage_FormUpdateBatch(string[] keys, DrawingCanvasImage item) {
			DrawingCanvasImageBroker broker = Helper.CreateInstance<DrawingCanvasImageBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DrawingCanvasImage. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<DrawingCanvasImage> DrawingCanvasImage_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			DrawingCanvasImageBroker broker = Helper.CreateInstance<DrawingCanvasImageBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public DrawingCanvasImage DrawingCanvasImage_GetItem(string Key, params string[] fields) {
			DrawingCanvasImageBroker broker = Helper.CreateInstance<DrawingCanvasImageBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a list of DrawingCanvasImage for a specific DrawingCanvas.
		/// </summary>
		/// <param name="KeyDrawingCanvas">The key of the DrawingCanvas.</param>
		/// <returns></returns>
		public List<DrawingCanvasImage> DrawingCanvasImage_GetListByKeyDrawingCanvas(string KeyDrawingCanvas) {
			DrawingCanvasImageBroker broker = Helper.CreateInstance<DrawingCanvasImageBroker>();
			return broker.GetListByKeyDrawingCanvas(KeyDrawingCanvas);
		}

		/// <summary>
		/// Gets a json store for the business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DrawingCanvasImage> DrawingCanvasImage_GetStore(PagingParameter paging, PagingLocation location) {
			DrawingCanvasImageBroker broker = Helper.CreateInstance<DrawingCanvasImageBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of DrawingCanvasImage for a specific DrawingCanvas.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyDrawingCanvas">The key of the DrawingCanvas.</param>
		/// <returns></returns>
		public JsonStore<DrawingCanvasImage> DrawingCanvasImage_GetStoreByKeyDrawingCanvas(PagingParameter paging,
																							string KeyDrawingCanvas) {
			DrawingCanvasImageBroker broker = Helper.CreateInstance<DrawingCanvasImageBroker>();
			var retVal = broker.GetStoreByKeyDrawingCanvas(paging, KeyDrawingCanvas);
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity DrawingCanvasImage.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DrawingCanvasImage> DrawingCanvasImage_SaveStore(CrudStore<DrawingCanvasImage> store) {
			DrawingCanvasImageBroker broker = Helper.CreateInstance<DrawingCanvasImageBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DynamicDisplay.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DynamicDisplay_Delete(DynamicDisplay item) {
			DynamicDisplayBroker broker = Helper.CreateInstance<DynamicDisplayBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DynamicDisplay and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplay_FormCreate(DynamicDisplay item) {
			DynamicDisplayBroker broker = Helper.CreateInstance<DynamicDisplayBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DynamicDisplay.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplay_FormLoad(string Key) {
			DynamicDisplayBroker broker = Helper.CreateInstance<DynamicDisplayBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DynamicDisplay and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplay_FormUpdate(DynamicDisplay item) {
			DynamicDisplayBroker broker = Helper.CreateInstance<DynamicDisplayBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplay_FormUpdateBatch(string[] keys, DynamicDisplay item) {
			DynamicDisplayBroker broker = Helper.CreateInstance<DynamicDisplayBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DynamicDisplay. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<DynamicDisplay> DynamicDisplay_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			DynamicDisplayBroker broker = Helper.CreateInstance<DynamicDisplayBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Generate modern UI dynamic displays.
		/// </summary>
		/// <returns></returns>
		public bool DynamicDisplay_GenerateModernUI() {
			foreach (ModernUIColor color in Enum.GetValues(typeof(ModernUIColor))) {
				var dd = ChartHelper.GenerateDynamicDisplayModernUIStyle(color, true);
				DynamicDisplay_GenerateModernUISave(dd);

				dd = ChartHelper.GenerateDynamicDisplayModernUIStyle(color, false);
				DynamicDisplay_GenerateModernUISave(dd);
			}
			Palette_GenerateModernUI();
			return true;
		}

		/// <summary>
		/// Dynamics the display_ generate modern UI save.
		/// </summary>
		/// <param name="dd">The dd.</param>
		private void DynamicDisplay_GenerateModernUISave(DynamicDisplay dd) {
			var broker = Helper.CreateInstance<DynamicDisplayBroker>();
			var brokerColor = Helper.CreateInstance<DynamicDisplayColorTemplateBroker>();
			if (dd != null) {
				if (dd.CustomColorTemplate != null) {
					var color = brokerColor.GetItemByLocalId(dd.CustomColorTemplate.LocalId);
					if (color == null) {
						dd.CustomColorTemplate = brokerColor.Save(dd.CustomColorTemplate);
					}
					else
						dd.CustomColorTemplate = color;

					dd.KeyCustomColorTemplate = dd.CustomColorTemplate.KeyDynamicDisplayColorTemplate;
				}

				var dynamicdisplay = broker.GetItemByLocalId(dd.LocalId);
				if (dynamicdisplay == null)
					broker.Save(dd);
			}
		}

		/// <summary>
		/// Gets a specific item for the business entity DynamicDisplay.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public DynamicDisplay DynamicDisplay_GetItem(string Key, params string[] fields) {
			DynamicDisplayBroker broker = Helper.CreateInstance<DynamicDisplayBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity DynamicDisplay.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DynamicDisplay> DynamicDisplay_GetStore(PagingParameter paging, PagingLocation location) {
			DynamicDisplayBroker broker = Helper.CreateInstance<DynamicDisplayBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Returns the stream of the image  from a dynamicdisplay.
		/// </summary>
		/// <param name="Key">The key of the dynamicdisplay.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="displayBogusChart">if set to <c>true</c> [display bogus chart].</param>
		/// <returns></returns>
		public StreamResult DynamicDisplay_GetStreamImage(string Key, int width, int height, bool displayBogusChart = true) {
			StreamResult retVal = ChartHelper.DynamicDisplay_GetStreamImageFromCache(Key, width, height);
			if (retVal == null) {
				Padding padding;
				retVal = DynamicDisplay_GetStreamImage(Key, width, height, out padding, null, displayBogusChart);
				ChartHelper.DynamicDisplay_AddStreamImageToCache(Key, width, height, retVal);
			}
			return retVal;
		}

		/// <summary>
		/// Returns the stream of the image  from a dynamicdisplay with the Chart padding info.
		/// </summary>
		/// <param name="Key">The Dynamic Display key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="padding">The chart padding.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="displayBogusChart">if set to <c>true</c> [display bogus chart].</param>
		/// <param name="useCache">if set to <c>true</c> uses the cached Dynamic Display</param>
		/// <returns></returns>
		public StreamResult DynamicDisplay_GetStreamImage(string Key, int width, int height, out Padding padding, Chart chart = null, bool displayBogusChart = true, bool useCache = true) {
			padding = new Padding();
			if (string.IsNullOrEmpty(Key))
				return null;
			if (chart != null && useCache) {
				var cachedTuple = ChartHelper.DynamicDisplay_GetStreamImageAndPaddingFromCache(Key, chart.KeyChart, width, height, Helper.GetCurrentUICulture());
				if (cachedTuple != null) {
					padding = cachedTuple.Item2;
					return cachedTuple.Item1;
				}
			}

			var display = DynamicDisplay_Prepare(Key, chart, useCache);

			var tuple = DynamicDisplayService.RenderDynamicDisplayStream(display, width, height);
			var retVal = tuple.Item1;
			padding = tuple.Item2;

			//Here we display a bogus chart.
			if (display.IsMainChartVisible && chart == null && tuple.Item1 != null && displayBogusChart) {
				chart = ChartHelper.GetBogusChart(display.BogusChartType, 12, string.IsNullOrEmpty(display.MainTextDefaultFontColor) ?
					(display.Type == DynamicDisplayType.Tile ? "FFFFFF" : "") : display.MainTextDefaultFontColor, display.Type == DynamicDisplayType.Tile);
				chart.UseDynamicDisplay = true;
				chart.KeyDynamicDisplay = Key;
				retVal = ChartingService.RenderChartStream(chart, width, height, tuple.Item1, padding);
			}
			//We add it to the Cache.
			if (chart != null && useCache) {
				ChartHelper.DynamicDisplay_AddStreamImageAndPaddingToCache(Key, chart.KeyChart, width, height, Helper.GetCurrentUICulture(), tuple);
			}
			return retVal;
		}

		private DynamicDisplay DynamicDisplay_Prepare(string Key, Chart chart = null, bool useCache = true) {
			#region Cache
			DynamicDisplay display;
			var cacheKey = string.Empty;
			if (chart != null && useCache) {
				cacheKey = ChartHelper.GetCacheKey_DynamicDisplayWithChart(chart.KeyChart, Key);
				display = CacheService.GetData(cacheKey) as DynamicDisplay;
				if (display != null)
					return display;
			}
			#endregion

			var broker = Helper.CreateInstance<DynamicDisplayBroker>();
			var brokerImage = Helper.CreateInstance<DynamicDisplayImageBroker>();
			display = broker.GetItem(Key, "*");
			if (display == null)
				return null;

			if (chart != null) {
				chart.DynamicDisplay = display;

				var dynamicDisplayMainText = string.IsNullOrEmpty(chart.Html) ? chart.DynamicDisplayMainText : chart.Html;
				display.MainText = ChartHelper.IsTextAreaEmpty(chart.DynamicDisplayMainText) ? display.MainText : dynamicDisplayMainText;
				display.HeaderText = ChartHelper.IsTextAreaEmpty(chart.DynamicDisplayHeaderText) ? display.HeaderText : chart.DynamicDisplayHeaderText;
				display.FooterText = ChartHelper.IsTextAreaEmpty(chart.DynamicDisplayFooterText) ? display.FooterText : chart.DynamicDisplayFooterText;

				display.MainText = Helper.LocalizeInText(display.MainText, "%");
				display.HeaderText = Helper.LocalizeInText(display.HeaderText, "%");
				display.FooterText = Helper.LocalizeInText(display.FooterText, "%");

				var templatesList = new List<string>();
				if (!string.IsNullOrWhiteSpace(display.MainText)) {
					templatesList.Add(display.MainText);
				}
				if (!string.IsNullOrWhiteSpace(display.HeaderText)) {
					templatesList.Add(display.HeaderText);
				}
				if (!string.IsNullOrWhiteSpace(display.FooterText)) {
					templatesList.Add(display.FooterText);
				}

				if (templatesList.Count > 0) {
                    // we're changing the UI culture so that the template parts of the formula will be parsed in the same culture as the non-template parts. 
                    //example: (@AllSeries(@Model,"SumElem")*0,000101). 
                    var currentUICulture = Thread.CurrentThread.CurrentUICulture;
				    var cultrureChanged = false;
                    if (!string.IsNullOrEmpty(chart.DynamicDisplayKeyLocalizationCulture)) {
                        Helper.SetUICulture(chart.DynamicDisplayKeyLocalizationCulture);
                        cultrureChanged = true;
                    }
					var result = TemplateService.ParseMany(templatesList, chart, "ChartTemplateBase");
                    if (cultrureChanged) {
                        Helper.SetUICulture(currentUICulture.Name);  
                    }
                    
					if (!string.IsNullOrWhiteSpace(display.MainText) && result.Count > 0) {
						display.MainText = result.First();
						result.Remove(display.MainText);
						display.MainText = FormulaEvaluatorService.EvaluateText(
                            display.MainText, '#', chart.NumericFormat, chart.DynamicDisplayKeyLocalizationCulture, 
							new ChartFormulaEvaluatorContext(chart));
						display.MainText = Helper.LocalizeInText(display.MainText, "%");
					}
					if (!string.IsNullOrWhiteSpace(display.HeaderText) && result.Count > 0) {
						display.HeaderText = result.First();
						result.Remove(display.HeaderText);
						display.HeaderText = FormulaEvaluatorService.EvaluateText(
                            display.HeaderText, '#', chart.NumericFormat, chart.DynamicDisplayKeyLocalizationCulture, 
							new ChartFormulaEvaluatorContext(chart));
						display.HeaderText = Helper.LocalizeInText(display.HeaderText, "%");
					}
					if (!string.IsNullOrWhiteSpace(display.FooterText) && result.Count > 0) {
						display.FooterText = result.First();
						result.Remove(display.FooterText);
						display.FooterText = FormulaEvaluatorService.EvaluateText(
                            display.FooterText, '#', chart.NumericFormat, chart.DynamicDisplayKeyLocalizationCulture, 
							new ChartFormulaEvaluatorContext(chart));
						display.FooterText = Helper.LocalizeInText(display.FooterText, "%");
					}
				}


				display.HeaderPicture = chart.DynamicDisplayHeaderPicture ?? display.HeaderPicture;
				if (string.IsNullOrEmpty(display.HeaderPicture) == false)
					display.HeaderPictureImage = brokerImage.GetItem(display.HeaderPicture, "*");
				display.MainPicture = chart.DynamicDisplayMainPicture ?? display.MainPicture;
				if (string.IsNullOrEmpty(display.MainPicture) == false)
					display.MainPictureImage = brokerImage.GetItem(display.MainPicture, "*");
				display.FooterPicture = chart.DynamicDisplayFooterPicture ?? display.FooterPicture;
				if (string.IsNullOrEmpty(display.FooterPicture) == false)
					display.FooterPictureImage = brokerImage.GetItem(display.FooterPicture, "*");

				CacheService.Add(cacheKey, display);
			}
			else {
				display.HeaderText = display.HeaderEmptyText;
				display.MainText = display.MainEmptyText;
				display.FooterText = display.FooterEmptyText;
			}
			return display;
		}

		/// <summary>
		/// Saves a crud store for the business entity DynamicDisplay.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DynamicDisplay> DynamicDisplay_SaveStore(CrudStore<DynamicDisplay> store) {
			DynamicDisplayBroker broker = Helper.CreateInstance<DynamicDisplayBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DynamicDisplayColorTemplate_Delete(DynamicDisplayColorTemplate item) {
			DynamicDisplayColorTemplateBroker broker = Helper.CreateInstance<DynamicDisplayColorTemplateBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DynamicDisplayColorTemplate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayColorTemplate_FormCreate(DynamicDisplayColorTemplate item) {
			DynamicDisplayColorTemplateBroker broker = Helper.CreateInstance<DynamicDisplayColorTemplateBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayColorTemplate_FormLoad(string Key) {
			DynamicDisplayColorTemplateBroker broker = Helper.CreateInstance<DynamicDisplayColorTemplateBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DynamicDisplayColorTemplate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayColorTemplate_FormUpdate(DynamicDisplayColorTemplate item) {
			DynamicDisplayColorTemplateBroker broker = Helper.CreateInstance<DynamicDisplayColorTemplateBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayColorTemplate_FormUpdateBatch(string[] keys, DynamicDisplayColorTemplate item) {
			DynamicDisplayColorTemplateBroker broker = Helper.CreateInstance<DynamicDisplayColorTemplateBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DynamicDisplayColorTemplate. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<DynamicDisplayColorTemplate> DynamicDisplayColorTemplate_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			var broker = Helper.CreateInstance<DynamicDisplayColorTemplateBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public DynamicDisplayColorTemplate DynamicDisplayColorTemplate_GetItem(string Key, params string[] fields) {
			DynamicDisplayColorTemplateBroker broker = Helper.CreateInstance<DynamicDisplayColorTemplateBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<DynamicDisplayColorTemplate> DynamicDisplayColorTemplate_GetStore(PagingParameter paging, PagingLocation location) {
			DynamicDisplayColorTemplateBroker broker = Helper.CreateInstance<DynamicDisplayColorTemplateBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity DynamicDisplayColorTemplate.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DynamicDisplayColorTemplate> DynamicDisplayColorTemplate_SaveStore(
			CrudStore<DynamicDisplayColorTemplate> store) {
			DynamicDisplayColorTemplateBroker broker = Helper.CreateInstance<DynamicDisplayColorTemplateBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool DynamicDisplayImage_Delete(DynamicDisplayImage item) {
			DynamicDisplayImageBroker broker = Helper.CreateInstance<DynamicDisplayImageBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity DynamicDisplayImage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayImage_FormCreate(DynamicDisplayImage item) {
			DynamicDisplayImageBroker broker = Helper.CreateInstance<DynamicDisplayImageBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayImage_FormLoad(string Key) {
			DynamicDisplayImageBroker broker = Helper.CreateInstance<DynamicDisplayImageBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity DynamicDisplayImage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayImage_FormUpdate(DynamicDisplayImage item) {
			DynamicDisplayImageBroker broker = Helper.CreateInstance<DynamicDisplayImageBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse DynamicDisplayImage_FormUpdateBatch(string[] keys, DynamicDisplayImage item) {
			DynamicDisplayImageBroker broker = Helper.CreateInstance<DynamicDisplayImageBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity DynamicDisplayImage. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<DynamicDisplayImage> DynamicDisplayImage_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			DynamicDisplayImageBroker broker = Helper.CreateInstance<DynamicDisplayImageBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public DynamicDisplayImage DynamicDisplayImage_GetItem(string Key, params string[] fields) {
			DynamicDisplayImageBroker broker = Helper.CreateInstance<DynamicDisplayImageBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="type">The type.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<DynamicDisplayImage> DynamicDisplayImage_GetStore(PagingParameter paging, PagingLocation location, DynamicDisplayImageType type) {
			DynamicDisplayImageBroker broker = Helper.CreateInstance<DynamicDisplayImageBroker>();
			return broker.GetStoreByType(paging, type);
		}

		/// <summary>
		/// Returns the stream of the image  from a dynamicdisplayimage.
		/// </summary>
		/// <param name="Key">The key of the dynamicdisplayimage.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public StreamResult DynamicDisplayImage_GetStreamImage(string Key, int width, int height) {
			DynamicDisplayImageBroker broker = Helper.CreateInstance<DynamicDisplayImageBroker>();
			DynamicDisplayImage image = broker.GetItem(Key, "*");
			var retVal = new StreamResult(image.Image.GetStream(), image.Image.DocumentType);
			using (var bitmap = new Bitmap(image.Image.GetStream())) {
				retVal = ImageHelper.CreateThumbnailImage(retVal, width > 0 ? width : bitmap.Width, height > 0 ? height : bitmap.Height);
			}
			return retVal;
		}

		/// <summary>
		/// Init the Dynamic Display images.
		/// </summary>
		private void DynamicDisplayImage_Init() {
			string path = TenantHelper.GetInitialeDynamicDisplayImagesPath();
			var brokerImage = Helper.CreateInstance<DynamicDisplayImageBroker>();

			var files = Directory.GetFiles(path, "*.png").ToList();

			foreach (var file in files) {
				try {
					using (FileStream stream = File.OpenRead(file)) {
						var finfo = new FileInfo(file);
						string guid = DocumentHelper.PrepareDocument(stream, finfo.Name, MimeType.Png);

						var image = new DynamicDisplayImage {
							KeyImage = guid,
							Name = finfo.Name.Replace(finfo.Extension, "").Replace("_300", "")
						};

						brokerImage.Save(image);
					}
				}
				catch {
					// Do nothing.
				}
			}

		}

		/// <summary>
		/// Saves a crud store for the business entity DynamicDisplayImage.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<DynamicDisplayImage> DynamicDisplayImage_SaveStore(CrudStore<DynamicDisplayImage> store) {
			DynamicDisplayImageBroker broker = Helper.CreateInstance<DynamicDisplayImageBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Copy existing business entity Chart.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy </param>
		/// <returns></returns>
		public FormResponse EnergyCertificate_Copy(string[] keys) {
			var response = new FormResponse { success = true };
			var broker = Helper.CreateInstance<EnergyCertificateBroker>();
			try {
				foreach (string key in keys) {
					var item = broker.GetItem(key, "*");
					broker.Copy(item, out response);
				}
			}
			catch (Exception ex) {
				response.success = false;
				response.msg = ex.Message;
			}
			return response;
		}

		/// <summary>
		/// Deletes an existing business entity EnergyCertificate.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool EnergyCertificate_Delete(EnergyCertificate item) {
			EnergyCertificateBroker broker = Helper.CreateInstance<EnergyCertificateBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity EnergyCertificate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="categories">The categories.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificate_FormCreate(EnergyCertificate item, CrudStore<EnergyCertificateCategory> categories) {
			FormResponse response;
			var broker = Helper.CreateInstance<EnergyCertificateBroker>();
			var brokerCategorie = Helper.CreateInstance<EnergyCertificateCategoryBroker>();

			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						brokerCategorie.SaveEnergyCertificateEnergyCertificateCategory((EnergyCertificate)response.data, categories);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity EnergyCertificate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificate_FormLoad(string Key) {
			EnergyCertificateBroker broker = Helper.CreateInstance<EnergyCertificateBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity EnergyCertificate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="categories">The categories.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificate_FormUpdate(EnergyCertificate item, CrudStore<EnergyCertificateCategory> categories) {
			FormResponse response;
			var broker = Helper.CreateInstance<EnergyCertificateBroker>();
			var brokerCategorie = Helper.CreateInstance<EnergyCertificateCategoryBroker>();

			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						brokerCategorie.SaveEnergyCertificateEnergyCertificateCategory((EnergyCertificate)response.data, categories);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificate_FormUpdateBatch(string[] keys, EnergyCertificate item) {
			EnergyCertificateBroker broker = Helper.CreateInstance<EnergyCertificateBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity EnergyCertificate. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<EnergyCertificate> EnergyCertificate_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			EnergyCertificateBroker broker = Helper.CreateInstance<EnergyCertificateBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity EnergyCertificate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public EnergyCertificate EnergyCertificate_GetItem(string Key, params string[] fields) {
			EnergyCertificateBroker broker = Helper.CreateInstance<EnergyCertificateBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity EnergyCertificate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<EnergyCertificate> EnergyCertificate_GetStore(PagingParameter paging, PagingLocation location) {
			EnergyCertificateBroker broker = Helper.CreateInstance<EnergyCertificateBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity EnergyCertificate.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<EnergyCertificate> EnergyCertificate_SaveStore(CrudStore<EnergyCertificate> store) {
			EnergyCertificateBroker broker = Helper.CreateInstance<EnergyCertificateBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool EnergyCertificateCategory_Delete(EnergyCertificateCategory item) {
			EnergyCertificateCategoryBroker broker = Helper.CreateInstance<EnergyCertificateCategoryBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity EnergyCertificateCategory and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificateCategory_FormCreate(EnergyCertificateCategory item) {
			EnergyCertificateCategoryBroker broker = Helper.CreateInstance<EnergyCertificateCategoryBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificateCategory_FormLoad(string Key) {
			EnergyCertificateCategoryBroker broker = Helper.CreateInstance<EnergyCertificateCategoryBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity EnergyCertificateCategory and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificateCategory_FormUpdate(EnergyCertificateCategory item) {
			EnergyCertificateCategoryBroker broker = Helper.CreateInstance<EnergyCertificateCategoryBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse EnergyCertificateCategory_FormUpdateBatch(string[] keys, EnergyCertificateCategory item) {
			EnergyCertificateCategoryBroker broker = Helper.CreateInstance<EnergyCertificateCategoryBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity EnergyCertificateCategory. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<EnergyCertificateCategory> EnergyCertificateCategory_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			EnergyCertificateCategoryBroker broker = Helper.CreateInstance<EnergyCertificateCategoryBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public EnergyCertificateCategory EnergyCertificateCategory_GetItem(string Key, params string[] fields) {
			EnergyCertificateCategoryBroker broker = Helper.CreateInstance<EnergyCertificateCategoryBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<EnergyCertificateCategory> EnergyCertificateCategory_GetStore(PagingParameter paging, PagingLocation location) {
			EnergyCertificateCategoryBroker broker = Helper.CreateInstance<EnergyCertificateCategoryBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of  EnergyCertificateCategory for a specific EnergyCertificate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyEnergyCertificate">The key energy certificate.</param>
		/// <returns></returns>
		public JsonStore<EnergyCertificateCategory> EnergyCertificateCategory_GetStoreByKeyEnergyCertificate(PagingParameter paging, string KeyEnergyCertificate) {
			var broker = Helper.CreateInstance<EnergyCertificateCategoryBroker>();
			return broker.GetStoreByKeyEnergyCertificate(paging, KeyEnergyCertificate);
		}

		/// <summary>
		/// Saves a crud store for the business entity EnergyCertificateCategory.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<EnergyCertificateCategory> EnergyCertificateCategory_SaveStore(CrudStore<EnergyCertificateCategory> store) {
			EnergyCertificateCategoryBroker broker = Helper.CreateInstance<EnergyCertificateCategoryBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity EventLog.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool EventLog_Delete(EventLog item) {
			EventLogBroker broker = Helper.CreateInstance<EventLogBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity EventLog and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse EventLog_FormCreate(EventLog item) {
			EventLogBroker broker = Helper.CreateInstance<EventLogBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity EventLog.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse EventLog_FormLoad(string Key) {
			EventLogBroker broker = Helper.CreateInstance<EventLogBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity EventLog and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse EventLog_FormUpdate(EventLog item) {
			EventLogBroker broker = Helper.CreateInstance<EventLogBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse EventLog_FormUpdateBatch(string[] keys, EventLog item) {
			EventLogBroker broker = Helper.CreateInstance<EventLogBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity EventLog.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<EventLog> EventLog_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			EventLogBroker broker = Helper.CreateInstance<EventLogBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity EventLog.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public EventLog EventLog_GetItem(string Key, params string[] fields) {
			EventLogBroker broker = Helper.CreateInstance<EventLogBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity EventLog.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<EventLog> EventLog_GetStore(PagingParameter paging, PagingLocation location) {
			EventLogBroker broker = Helper.CreateInstance<EventLogBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity EventLog.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<EventLog> EventLog_SaveStore(CrudStore<EventLog> store) {
			EventLogBroker broker = Helper.CreateInstance<EventLogBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a json store of the Filter AlarmDefinition Classification for a specific KeyAlarmTable.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyAlarmTable">The key KeyAlarmTable.</param>
		/// <returns></returns>
		public JsonStore<ClassificationItem> FilterAlarmDefinitionClassification_GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable) {
			var broker = Helper.CreateInstance<ClassificationItemBroker>();
			var retVal = broker.GetFilterAlarmDefinitionClassificationStoreByKeyAlarmTable(paging, KeyAlarmTable);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter AlarmDefinition Classification for a specific Chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public JsonStore<ClassificationItem> FilterAlarmDefinitionClassification_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			var broker = Helper.CreateInstance<ClassificationItemBroker>();
			var retVal = broker.GetFilterAlarmDefinitionClassificationStoreByKeyChart(paging, KeyChart);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter EventLog Classification for a specific chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public JsonStore<ClassificationItem> FilterEventLogClassification_GetStoreByKeyChart(PagingParameter paging,
																								string KeyChart) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			var retVal = broker.GetFilterEventLogClassificationStoreByKeyChart(paging, KeyChart);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter Meter Classification for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyAlarmDefinition">The key AlarmDefinition.</param>
		/// <returns></returns>
		public JsonStore<ClassificationItem> FilterMeterClassification_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			var broker = Helper.CreateInstance<ClassificationItemBroker>();
			var retVal = broker.GetFilterMeterClassificationStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter Meter Classification for a specific chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public JsonStore<ClassificationItem> FilterMeterClassification_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			var broker = Helper.CreateInstance<ClassificationItemBroker>();
			var retVal = broker.GetFilterMeterClassificationStoreByKeyChart(paging, KeyChart);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyAlarmDefinition">The key AlarmDefinition.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatial_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			var broker = Helper.CreateInstance<LocationBroker>();
			var retVal = broker.GetFilterSpatialStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific AlarmTable.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyAlarmTable">The key AlarmTable.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatial_GetStoreByKeyAlarmTable(PagingParameter paging, string KeyAlarmTable) {
			var broker = Helper.CreateInstance<LocationBroker>();
			var retVal = broker.GetFilterSpatialStoreByKeyAlarmTable(paging, KeyAlarmTable);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific chart.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatial_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			var broker = Helper.CreateInstance<LocationBroker>();
			var retVal = broker.GetFilterSpatialStoreByKeyChart(paging, KeyChart);
			return retVal;
		}

		/// <summary>
		/// Deletes an existing business entity FilterSpatialPset.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool FilterSpatialPset_Delete(FilterSpatialPset item) {
			FilterSpatialPsetBroker broker = Helper.CreateInstance<FilterSpatialPsetBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity FilterSpatialPset and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse FilterSpatialPset_FormCreate(FilterSpatialPset item) {
			FilterSpatialPsetBroker broker = Helper.CreateInstance<FilterSpatialPsetBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse FilterSpatialPset_FormLoad(string Key) {
			FilterSpatialPsetBroker broker = Helper.CreateInstance<FilterSpatialPsetBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity FilterSpatialPset and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse FilterSpatialPset_FormUpdate(FilterSpatialPset item) {
			FilterSpatialPsetBroker broker = Helper.CreateInstance<FilterSpatialPsetBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse FilterSpatialPset_FormUpdateBatch(string[] keys, FilterSpatialPset item) {
			FilterSpatialPsetBroker broker = Helper.CreateInstance<FilterSpatialPsetBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<FilterSpatialPset> FilterSpatialPset_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			FilterSpatialPsetBroker broker = Helper.CreateInstance<FilterSpatialPsetBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public FilterSpatialPset FilterSpatialPset_GetItem(string Key, params string[] fields) {
			FilterSpatialPsetBroker broker = Helper.CreateInstance<FilterSpatialPsetBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a store of Location for a specific AlarmDefinition based on the FilterSpatialPset attached to it.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmDefinition">The key of the AlarmDefinition.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatialPset_GetLocationStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			var broker = Helper.CreateInstance<LocationBroker>();
			var retVal = broker.GetFilterSpatialPsetAllPagingByKeyAlarmDefinition(paging, KeyAlarmDefinition);
			return retVal;
		}

		/// <summary>
		/// Gets a store of Location for a specific chart based on the FilterSpatialPset attached to it.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatialPset_GetLocationStoreByKeyChart(PagingParameter paging, string KeyChart) {
			var broker = Helper.CreateInstance<LocationBroker>();
			var retVal = broker.GetFilterSpatialPsetAllPagingByKeyChart(paging, KeyChart);
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<FilterSpatialPset> FilterSpatialPset_GetStore(PagingParameter paging,
																					PagingLocation location) {
			FilterSpatialPsetBroker broker = Helper.CreateInstance<FilterSpatialPsetBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of FilterSpatialPset for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmDefinition">The key of the AlarmDefinition.</param>
		/// <returns></returns>
		public JsonStore<FilterSpatialPset> FilterSpatialPset_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			var broker = Helper.CreateInstance<FilterSpatialPsetBroker>();
			return broker.GetStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
		}

		/// <summary>
		/// Gets a store of FilterSpatialPset for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<FilterSpatialPset> FilterSpatialPset_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			var broker = Helper.CreateInstance<FilterSpatialPsetBroker>();
			return broker.GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity FilterSpatialPset.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<FilterSpatialPset> FilterSpatialPset_SaveStore(CrudStore<FilterSpatialPset> store) {
			FilterSpatialPsetBroker broker = Helper.CreateInstance<FilterSpatialPsetBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a json store for the business entity FOLMembershipUser for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalTemplate">The Key PortalTemplate.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<FOLMembershipUser> FOLMembershipUser_GetStoreByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate) {
			var broker = Helper.CreateInstance<FOLMembershipUserBroker>();
			var retVal = broker.GetStoreByKeyPortalTemplate(paging, KeyPortalTemplate);
			return retVal;
		}

		/// <summary>
		/// Gets the amount of meter data items in range.
		/// </summary>
		/// <param name="start">The start.</param>
		/// <param name="end">The end.</param>
		/// <param name="frequencyNumber">The frequency number.</param>
		/// <param name="frequency">The frequency.</param>
		/// <returns></returns>
		private static int GetAmountOfMeterDataItemsInRange(DateTime start, DateTime end, int frequencyNumber, AxisTimeInterval frequency) {
			int totalSeconds = Convert.ToInt32((end - start).TotalSeconds);
			int totalCount;

			switch (frequency) {
				case AxisTimeInterval.Seconds:
					totalCount = totalSeconds / frequencyNumber;
					break;
				case AxisTimeInterval.Minutes:
					totalCount = totalSeconds / (60 * frequencyNumber);
					break;

				case AxisTimeInterval.Hours:
					totalCount = totalSeconds / (60 * 60 * frequencyNumber);
					break;

				case AxisTimeInterval.Days:
					totalCount = totalSeconds / (24 * 60 * 60 * frequencyNumber);
					break;

				case AxisTimeInterval.Weeks:
					totalCount = totalSeconds / (7 * 24 * 60 * 60 * frequencyNumber);
					break;
				case AxisTimeInterval.Months:
					totalCount = totalSeconds / (30 * 24 * 60 * 60 * frequencyNumber);
					break;

				case AxisTimeInterval.Quarters:
					totalCount = totalSeconds / (4 * 30 * 24 * 60 * 60 * frequencyNumber);
					break;
				case AxisTimeInterval.Semesters:
					totalCount = totalSeconds / (2 * 30 * 24 * 60 * 60 * frequencyNumber);
					break;

				case AxisTimeInterval.Years:
					totalCount = totalSeconds / (365 * 24 * 60 * 60 * frequencyNumber);
					break;

				default:
					throw new ArgumentOutOfRangeException("frequency");
			}

			return totalCount;
		}

		/// <summary>
		/// Gets the energy aggregator service agent.
		/// </summary>
		/// <returns></returns>
		public EnergyAggregatorServiceAgent GetEnergyAggregatorServiceAgent() {
			var machines = MachineService.GetList(const_energy_aggregator_service_reference);
			var energyAggregatorServiceAgent = new EnergyAggregatorServiceAgent(machines);

			return energyAggregatorServiceAgent;
		}

		/// <summary>
		/// Initializes the energy aggregator service agent.
		/// </summary>
		public void InitializeEnergyAggregatorServiceAgent() {

			using (var energyAggregatorServiceAgent = GetEnergyAggregatorServiceAgent()) {
				//if (!energyAggregatorServiceAgent.AreInitializedRemoteAgents()) {
				TracingService.Write(TraceEntrySeverity.Information, "Initializing remote agents...", "GetEnergyAggregatorServiceAgent", string.Empty);
				energyAggregatorServiceAgent.InitializeRemoteAgents();
				TracingService.Write(TraceEntrySeverity.Information, "Remote agents initialized.", "GetEnergyAggregatorServiceAgent", string.Empty);
				//}
			}
		}

		/// <summary>
		/// Initializes the energy aggregator service agent.
		/// </summary>
		public void InitializeEnergyAggregatorServiceAgent(List<string> tenants) {
			using (var energyAggregatorServiceAgent = GetEnergyAggregatorServiceAgent()) {
				TracingService.Write(TraceEntrySeverity.Information, "Initializing remote agents...", "GetEnergyAggregatorServiceAgent", string.Empty);
				energyAggregatorServiceAgent.InitializeRemoteAgents(tenants);
				TracingService.Write(TraceEntrySeverity.Information, "Remote agents initialized.", "GetEnergyAggregatorServiceAgent", string.Empty);
			}
		}

		/// <summary>
		/// Determines whether [is energy aggregator crud notification in progress].
		/// </summary>
		public bool IsEnergyAggregatorCrudNotificationInProgress() {
			var retVal = false;
			using (var energyAggregatorServiceAgent = GetEnergyAggregatorServiceAgent()) {
				retVal = energyAggregatorServiceAgent.IsCrudNotificationInProgress;
			}
			return retVal;
		}

		/// <summary>
		/// Initializes the current tenant with energy specific data needed to begin working with it.
		/// </summary>
		public void InitializeTenant() {
			//Init the palettes.
			Palette_InitFromProvider();
			DynamicDisplayImage_Init();
		}

		/// <summary>
		/// Return a Location item.
		/// </summary>
		/// <param name="Key">The key of the ClassificationItem.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Location Location_GetItem(string Key, params string[] fields) {
			LocationBroker broker = Helper.CreateInstance<LocationBroker>();
			var retVal = broker.GetItem(Key, fields);
			return retVal;
		}

		/// <summary>
		/// Deletes an existing business entity MachineInstance.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool MachineInstance_Delete(MachineInstance item) {
			MachineService.Delete(item);
			return true;
		}

		/// <summary>
		/// Create a new Machine Instance
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public FormResponse MachineInstance_FormCreate(MachineInstance item) {
			FormResponse response = new FormResponse { success = true };
			try {
				MachineService.Create(item);
			}
			catch (Exception ex) {

				response.success = false;
				response.msg = ex.Message;
			}
			return response;
		}

		/// <summary>
		/// Returns the list of MachineInstances.
		/// </summary>
		/// <returns></returns>
		public List<MachineInstance> MachineInstance_GetList() {
			var list = MachineService.GetList();

			using (var aggregator = new EnergyAggregatorServiceAgent(list)) {
				foreach (var machine in list.Where(machine => machine.MachinePurpose == const_energy_aggregator_service_reference)) {
					aggregator.UpdateMachineInstance(machine);
				}
			}

			return list;
		}

		/// <summary>
		/// Gets a json store for the business entity MachineInstance.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<MachineInstance> MachineInstance_GetStore(PagingParameter paging) {
			var list = MachineInstance_GetList();
			var retVal = list.ToJsonStoreWithFilter(paging);
			return retVal;
		}

		/// <summary>
		/// Build the Chart of the Machines memory usage.
		/// </summary>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public ImageMap MachineInstance_MemoryUsageBuildImage(int width, int height) {
			var chart = new Chart {
				StartDate = DateTime.Now.AddMinutes(-5),
				EndDate = DateTime.Now.AddMinutes(5),
				ChartType = ChartType.Gauge,
				GaugeType = GaugeType.Vertical,
				DisplayStartEndDate = false,
				DisplayValues = true
			};
			var mainAxis = new ChartAxis {
				KeyChartAxis = "0",
				Max = 100,
				Min = 0,
				Name = "%"
			};
			chart.Axis.Add(mainAxis.KeyChartAxis, mainAxis);
			var mainAxisMarker = new ChartMarker() {
				ColorB = 0,
				ColorR = 255,
				ColorG = 0,
				Max = 100,
				Min = 80,
				Name = " "
			};
			mainAxis.Markers.Add(mainAxisMarker);

			var machines = MachineInstance_GetList();
			foreach (var machine in machines) {
				var inMemoryObjectCount = machine.InMemoryObjectCount;
				//TODO: Remove this. (was used for demo purpose);
				if (inMemoryObjectCount == 0) {
					inMemoryObjectCount = 190000000;
				}

				var s = new DataSerie {
					LocalId = machine.KeyMachineInstance,
					Name = machine.EnvironmentName,
					Unit = "% - (" + inMemoryObjectCount.ToString("n") + " " + Langue.msg_meterdata + ")",
					KeyYAxis = mainAxis.KeyChartAxis
				};

				var p = new DataPoint {
					Name = s.Name,
					XDateTime = DateTime.Now,
					YValue = (inMemoryObjectCount / 20000000d * 1.6) / 16d * 100d
				};
				s.Points.Add(p);
				chart.Series.Add(s);
			}

			var retVal = ChartingService.RenderChartImageMap(chart, width, height, null, new Padding(5));
			return retVal;
		}

		/// <summary>
		///Get stream image of Machines memory usage from cache.
		/// </summary>
		/// <param name="guid">The GUID.</param>
		/// <returns></returns>
		public Stream MachineInstance_MemoryUsageGetStreamImageFromCache(string guid) {
			var retVal = Chart_GetStreamImageFromCache(guid);
			return retVal;
		}

		/// <summary>
		/// Restart the passed machine instance.
		/// Restart the passed machine instance.
		/// </summary>
		/// <param name="item">The item.</param>
		public void MachineInstance_Restart(MachineInstance item) {
			MachineService.Restart(item);
		}

		/// <summary>
		/// Returns if the MachineInstance service is enabled or not.
		/// </summary>
		/// <returns></returns>
		public bool MachineInstance_ServiceIsEnabled() {
			var retVal = MachineService.IsEnabled();
			return retVal;
		}

		/// <summary>
		/// Deletes an existing business entity Map.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Map_Delete(Map item) {
			MapBroker broker = Helper.CreateInstance<MapBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Map and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Map_FormCreate(Map item) {
			MapBroker broker = Helper.CreateInstance<MapBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Map.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Map_FormLoad(string Key) {
			MapBroker broker = Helper.CreateInstance<MapBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Map and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Map_FormUpdate(Map item) {
			MapBroker broker = Helper.CreateInstance<MapBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Map_FormUpdateBatch(string[] keys, Map item) {
			MapBroker broker = Helper.CreateInstance<MapBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Map. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Map> Map_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			MapBroker broker = Helper.CreateInstance<MapBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity Map.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Map Map_GetItem(string Key, params string[] fields) {
			MapBroker broker = Helper.CreateInstance<MapBroker>();
			return broker.GetItem(Key, "*");
		}

		/// <summary>
		///Return the Map Key to be used when instanciating a new Key client side.
		/// </summary>
		/// <returns></returns>
		public string Map_GetKey() {
			var retVal = MapService.GetKey();
			return retVal;
		}

		/// <summary>
		/// Get the Map Pushpins.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <returns></returns>
		public List<MapPushpin> Map_GetPushpins(string Key) {
			var retVal = new ConcurrentBag<MapPushpin>();

			var mapBroker = Helper.CreateInstance<MapBroker>();
			var locationBroker = Helper.CreateInstance<LocationBroker>();
			var psetBroker = Helper.CreateInstance<PsetBroker>();
			var alarmBroker = Helper.CreateInstance<AlarmInstanceBroker>();

			var map = mapBroker.GetItem(Key);
			Location mapLocation = null;
			if (string.IsNullOrEmpty(map.KeyLocation) == false) {
				mapLocation = locationBroker.GetItem(map.KeyLocation);
			}
			var locations = locationBroker.GetListByKeyMap(map.KeyMap);
			var locationDictionary = locations.ToDictionary(x => x.KeyLocation, x => x);

			var psetList = psetBroker.GetListByPsetNameAndAttributeName(map.PsetName, map.AttributeName, locationDictionary.Keys.ToArray());
			Tuple<Location, string>[] addressArray = new Tuple<Location, string>[psetList.Count];
			for (int i = 0; i < psetList.Count; i++) {
				var address =
						   (psetList[i].Attributes.Select(att => att.Value))
							   .FirstOrDefault();
				addressArray[i] = new Tuple<Location, string>(locationDictionary[psetList[i].KeyObject], address);
			}


			ConcurrentBag<string> errorsBag = new ConcurrentBag<string>();
			Parallel.For(0, addressArray.Length, i => {
				if (mapLocation == null || addressArray[i].Item1.KeyLocationPath == mapLocation.KeyLocationPath ||
					addressArray[i].Item1.KeyLocationPath.StartsWith(mapLocation.KeyLocationPath)) {
					if (string.IsNullOrWhiteSpace(addressArray[i].Item2) == false) {
						MapLocation geoloc = null;
						geoloc = MapService.GetLocationFromAddress(addressArray[i].Item2);
						if (geoloc != null && geoloc.IsValid) {
							var pushpin =
								new MapPushpin {
									Location = geoloc,
									Tooltip = addressArray[i].Item1.Name,
									IconCls = Helper.GetIconClsByIfcType(addressArray[i].Item1.IfcType),
									Entity = addressArray[i].Item1
								};
							if (!string.IsNullOrEmpty(map.KeyClassificationItemAlarmDefinition)) {
								var alarms = alarmBroker.GetOpenListByKeyLocation(addressArray[i].Item1.KeyLocation,
									map.KeyClassificationItemAlarmDefinition);
								pushpin.AlarmNumber = alarms.Count;
							}
							retVal.Add(pushpin);
						}
						if (geoloc != null && !geoloc.IsValid) {
							errorsBag.Add(string.Format(" Couldn't find Location: {0} ", addressArray[i].Item2));
						}
					}
				}
			});
			StringBuilder sb = new StringBuilder();
			while (!errorsBag.IsEmpty) {
				string error;
				errorsBag.TryTake(out error);
				sb.Append(error);
			}
			if (sb.Length > 0) {
				TracingService.Write(sb.ToString());
			}

			return retVal.ToList();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="operationId"></param>
		/// <param name="key"></param>
		public void Map_BeginGetPushpins(Guid operationId, string key) {
			LongRunningOperationService.Invoke(operationId, () => {
				var mapPushpinsCollection = new MapPushpinsCollection { MapPushpinsList = Map_GetPushpins(key) }; // boxing pushpins list into container that derived from BaseBusinessEntity
				return new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, mapPushpinsCollection);
			});
		}

		/// <summary>
		/// Gets a json store for the business entity Map.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Map> Map_GetStore(PagingParameter paging, PagingLocation location) {
			MapBroker broker = Helper.CreateInstance<MapBroker>();
			var retVal = broker.GetStore(paging, location);
			foreach (var map in retVal.records) {
				broker.PopulateList(map, new string[] { "*" });
			}
			return retVal;

		}

		/// <summary>
		/// Saves a crud store for the business entity Map.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Map> Map_SaveStore(CrudStore<Map> store) {
			MapBroker broker = Helper.CreateInstance<MapBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Builds the image (with map) from a single meter.
		/// </summary>
		/// <param name="Key">The key.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public ImageMap Meter_BuildImage(string Key, int width, int height) {
			using (var energyAggregatorServiceAgent = GetEnergyAggregatorServiceAgent()) {
				List<Meter> meters;
				var chart = Chart_CreateFromMeter(Key, out meters, null, false);
				chart.TimeInterval = AxisTimeInterval.Months;
				chart.DynamicTimeScaleEnabled = false;
				var locations = new List<string>();

				if (meters != null && meters.Count > 0) {
					var meter = meters[0];
					var lastMeterData = energyAggregatorServiceAgent.MeterData_GetLastFromMeter(meter);
					if (lastMeterData != null) {
						lastMeterData.AcquisitionDateTime = DateTime.SpecifyKind(lastMeterData.AcquisitionDateTime, DateTimeKind.Utc);
						chart.StartDate = lastMeterData.AcquisitionDateTime.AddMonths(-12);

						//Since virtual meters can represent data rolled up by time, 
						//the last date of the virtual meter may not be the actual date of the meters it represents
						//Additional time is added to the last value's date, so we don't miss the actual values that are rolled up by time aggregation 
						chart.EndDate = meter.IsVirtual ? lastMeterData.AcquisitionDateTime.AddMonths(12) :
														  lastMeterData.AcquisitionDateTime;
					}
					else {
						chart.StartDate = Helper.DateTimeNow.AddMonths(-12);
						chart.EndDate = Helper.DateTimeNow;
					}
					chart = energyAggregatorServiceAgent.Chart_Process(chart, meters.Select(m => MeterHelper.ConvertMeterKeyToInt(m.KeyMeter)).ToList(), locations);
				}
				Chart_ApplySortOrder(chart);
				Chart_ApplyPalette(chart);
				return ChartingService.RenderChartImageMap(chart, width, height, null, new Padding());
			}
		}

		/// <summary>
		/// Deletes an existing business entity Meter.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Meter_Delete(Meter item) {
			MeterBroker broker = Helper.CreateInstance<MeterBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Process all the meters that have an Endpoint and store the value if needed.
		/// </summary>
		public void Meter_EndpointProcess() {
			var brokerMeter = Helper.CreateInstance<MeterBroker>();
			var meters = brokerMeter.GetAllWithEndpoint();

			foreach (var meter in meters) {
				if (meter.EndpointFrequency.HasValue) {
					brokerMeter.PopulateList(meter, EntityLoadMode.Standard, "LastData");
					if (meter.LastData == null || CheckLastDataAgainstFrequency(meter.LastData.AcquisitionDateTime, meter.EndpointFrequency.Value)) {
						Meter_SaveEndpointValue(meter);
					}
				}
			}
		}

		/// <summary>
		/// Creates a new business entity Meter and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Meter_FormCreate(Meter item) {
			return Meter_FormCreate(item, null);
		}

		/// <summary>
		/// Creates a new business entity Meter and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meterOperationsWizard">The meter operations wizard.</param>
		/// <returns></returns>
		public FormResponse Meter_FormCreate(Meter item, CrudStore<Meter> meterOperationsWizard) {
			FormResponse response;
			MeterBroker broker = Helper.CreateInstance<MeterBroker>();
			MeterOperationBroker brokerMeterOperation = Helper.CreateInstance<MeterOperationBroker>();
			Meter meter = null;
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						meter = response.data as Meter;
						if (meter != null && meterOperationsWizard != null && meterOperationsWizard.IsEmpty() == false) {
							MeterOperation previousOp = null;
							int count = 1;
							foreach (var mo in meterOperationsWizard.create) {
								var localid = string.Format("{0}_{1}", (meter.LocalId ?? meter.KeyMeter), count.ToString(CultureInfo.InvariantCulture));
								if (previousOp == null) {
									var meterOperation = new MeterOperation {
										LocalId = localid,
										Factor1 = 1,
										KeyMeter1 = mo.KeyMeter,
										Order = count,
										KeyMeter = meter.KeyMeter,
									};
									previousOp = brokerMeterOperation.Save(meterOperation);
								}
								else {
									var meterOperation = new MeterOperation {
										LocalId = localid,
										Factor1 = 1,
										KeyMeterOperation1 = previousOp.KeyMeterOperation,
										Operator = ArithmeticOperator.Add,
										Factor2 = 1,
										KeyMeter2 = mo.KeyMeter,
										Order = count,
										KeyMeter = meter.KeyMeter,
									};
									previousOp = brokerMeterOperation.Save(meterOperation);
								}
								count += 1;
							}
							meter.MeterOperationTimeScaleInterval = AxisTimeInterval.None;
							meter.KeyMeterOperationResult = previousOp.KeyMeterOperation;
							broker.FormUpdate(meter);
						}
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}

			if (response.success) {
				broker.PopulateList(meter, "MeterOperations");
				ExternalCrudNotificationService.SendNotification(meter, CrudAction.Update);
			}

			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity Meter.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Meter_FormLoad(string Key) {
			var broker = Helper.CreateInstance<MeterBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Meter and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns>Updated Meter</returns>
		public FormResponse Meter_FormUpdate(Meter item) {
			return Meter_FormUpdate(item, item.MeterOperations.AsCrudStore());
		}

		/// <summary>
		/// Updates an existing business entity Meter and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meterOperations">The meter operations.</param>
		/// <returns></returns>
		public FormResponse Meter_FormUpdate(Meter item, CrudStore<MeterOperation> meterOperations) {
			FormResponse response;
			var broker = Helper.CreateInstance<MeterBroker>();
			var brokerMeterOperation = Helper.CreateInstance<MeterOperationBroker>();
			Meter meter = null;
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						meter = response.data as Meter;
						if (meter != null) {
							brokerMeterOperation.SaveMeterMeterOperations(meter, meterOperations);
						}
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			if (response.success) {
				broker.PopulateList(meter, "MeterOperations", "CalendarEventCategory");
				ExternalCrudNotificationService.SendNotification(meter, CrudAction.Update);
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Meter_FormUpdateBatch(string[] keys, Meter item) {
			MeterBroker broker = Helper.CreateInstance<MeterBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Meter. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Meter> Meter_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			MeterBroker broker = Helper.CreateInstance<MeterBroker>();
			var meters = broker.GetAll(paging, location, out total, fields: fields);
			var meterBroker = Helper.CreateInstance<MeterBroker>();
			foreach (var meter in meters) {
				if (fields != null && meter.IsVirtual && (fields.Contains("Data") || fields.Contains("LastData"))) {
					if (meter.MeterOperations == null) {
						meterBroker.PopulateList(meter, "MeterOperations");
					}
					var meterData = MeterData_GetAllFromMeter(meter, paging);
					if (fields.Contains("Data")) {
						meter.Data = meterData;
					}
					if (fields.Contains("LastData")) {
						meter.LastData = meterData != null && meterData.Any() ? meterData.Last() : null;
					}
				}
			}
			return meters;
		}

		/// <summary>
		/// Gets a dictionnary for the business entity Meter, by KeyMeter. 
		/// </summary>
		/// <returns></returns>
		public Dictionary<int, Meter> Meter_GetDictionary() {
			MeterBroker broker = Helper.CreateInstance<MeterBroker>();
			var retVal = broker.GetDictionary();
			return retVal;
		}

		/// <summary>
		/// Gets a specific item for the business entity Meter.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Meter Meter_GetItem(string Key, params string[] fields) {
			var meterBroker = Helper.CreateInstance<MeterBroker>();
			var meter = meterBroker.GetItem(Key, fields);
			if ((fields != null) && (meter != null) &&
				meter.IsVirtual && (fields.Contains("Data") || fields.Contains("LastData"))) {
				if (meter.MeterOperations == null) {
					meterBroker.PopulateList(meter, "MeterOperations");
				}
				var paging = new PagingParameter();
				var meterData = MeterData_GetAllFromMeter(meter, paging);
				if (fields.Contains("Data")) {
					meter.Data = meterData;
				}
				if (fields.Contains("LastData")) {
					meter.LastData = meterData != null && meterData.Any() ? meterData.Last() : null;
				}
			}
			return meter;
		}

		/// <summary>
		/// Gets a json store for the business entity Meter.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Meter> Meter_GetStore(PagingParameter paging, PagingLocation location) {
			MeterBroker broker = Helper.CreateInstance<MeterBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of meters for a specific AlarmDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlarmDefinition">The key of the AlarmDefinition.</param>
		/// <returns></returns>
		public JsonStore<Meter> Meter_GetStoreByKeyAlarmDefinition(PagingParameter paging, string KeyAlarmDefinition) {
			MeterBroker broker = Helper.CreateInstance<MeterBroker>();
			return broker.GetStoreByKeyAlarmDefinition(paging, KeyAlarmDefinition);
		}

		/// <summary>
		/// Gets a store of meters for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<Meter> Meter_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			MeterBroker broker = Helper.CreateInstance<MeterBroker>();
			return broker.GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Gets a store of meters for a specific meter data export task.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMeterDataExportTask">The key meter data export task.</param>
		/// <returns></returns>
		public JsonStore<Meter> Meter_GetStoreByKeyMeterDataExportTask(PagingParameter paging, string KeyMeterDataExportTask) {
			MeterBroker broker = Helper.CreateInstance<MeterBroker>();
			return broker.GetStoreByKeyMeterDataExportTask(paging, KeyMeterDataExportTask);
		}

		/// <summary>
		/// Gets a store of real meters for a specific location and classification item.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyLocation">The key of the Location.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <returns></returns>
		public JsonStore<Meter> Meter_GetStoreByLocationAndClassification(PagingParameter paging, string KeyLocation, string KeyClassificationItem) {
			var broker = Helper.CreateInstance<MeterBroker>();

			// Get all locations
			var locations = m_CoreBusinessLayer.Location_GetSpatialHierarchy(new[] { KeyLocation }, HierarchyDirection.DESC);

			List<Filter> filters = FilterHelper.GetFilter<Location>();
			// If we would like to return an empty result if no filter spatial was associated with the user, we should not check Count > 0
			if (filters != null && filters.Count > 0) {
				var query = from n in locations
							join p in filters on n.KeyLocation equals p.Key
							select n;
				locations = query.Distinct().ToList();
			}

			paging.filters = new List<GridFilter> {new GridFilter {   field = "KeyClassificationItemPath",
																	  data = new GridData {type = "string", comparison = "contains", value = KeyClassificationItem + " "}
																  },
												   new GridFilter {   field = "IsVirtual",
																	  data = new GridData {type = "boolean", comparison = "eq", value = "false"}
																  }
												  };

			var locationsString = new StringBuilder();
			foreach (var location in locations) {
				locationsString.AppendFormat("{0},", location.KeyLocation);
			}

			paging.filters.Add(
					new GridFilter {
						field = "KeyLocation",
						data = new GridData { type = "list", comparison = "eq", value = locationsString.ToString().TrimEnd(',') }
					}
				);
				int total;
			var meters = broker.GetAll(paging, PagingLocation.Database, out total, true);

			foreach (var m in meters) {
				broker.PopulateList(m, "LastData");
			}

			var retStore = meters.ToJsonStore();
			retStore.recordCount = total;
			return retStore;
		}

		/// <summary>
		/// Save the current end point value of a meter.
		/// </summary>
		/// <param name="KeyMeter">The key meter.</param>
		public bool Meter_SaveEndpointValue(string KeyMeter) {
			var brokerMeter = Helper.CreateInstance<MeterBroker>();
			var meter = brokerMeter.GetItem(KeyMeter);
			var retVal = Meter_SaveEndpointValue(meter);
			return retVal;
		}

		/// <summary>
		/// Save the current end point value of a meter.
		/// </summary>
		/// <param name="meter">The meter.</param>
		/// <returns></returns>
		private bool Meter_SaveEndpointValue(Meter meter) {
			if (!string.IsNullOrEmpty(meter.KeyEndpoint) && !string.IsNullOrEmpty(meter.EndpointType)) {
				var retVal = false;
				var list = DataAcquisitionEndpoint_Process(meter.EndpointType, meter.KeyEndpoint);
				foreach (var value in list) {
					if (!double.IsNaN(value.Value)) {
						retVal = true;
						var brokerMeterData = Helper.CreateInstance<MeterDataBroker>();

						var md = new MeterData {
							AcquisitionDateTime = value.AcquisitionDateTime,
							KeyMeter = meter.KeyMeter,
							Value = value.Value
						};

						brokerMeterData.Save(md);
					}
				}
				return retVal;
			}
			return false;
		}

		/// <summary>
		/// Saves a crud store for the business entity Meter.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Meter> Meter_SaveStore(CrudStore<Meter> store) {
			MeterBroker broker = Helper.CreateInstance<MeterBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Generates the json structure of the ClassificationItem treeview for the Meter Classification filtered by Chart or AlarmDefinition.
		/// </summary>
		/// <param name="key">The Key of the parent node.</param>
		/// <param name="entity">The parent entity.</param>
		/// <param name="keyChart">The key chart.</param>
		/// <param name="keyAlarmDefinition">The key alarm definition.</param>
		/// <param name="keyMeterValidationRule">The key meter validation rule.</param>
		/// <param name="existing">if set to <c>true</c>
		/// the method will only return MeterClassifcation for meter that are attached to the chart  or AlarmDefinition,
		/// else it will return all available MeterClassification in the spatial filter of the chart or AlarmDefinition.</param>
		/// <param name="filterSpatial">The current filter spatial of the Chart or AlarmDefinition.</param>
		/// <returns></returns>
		public List<TreeNode> MeterClassification_GetTree(string key, ClassificationItem entity, string keyChart, string keyAlarmDefinition, string keyMeterValidationRule, bool existing, CrudStore<Location> filterSpatial) {
			List<ClassificationItem> classificationItems;
			var broker = Helper.CreateInstance<ClassificationItemBroker>();

			if (string.IsNullOrEmpty(keyChart) && filterSpatial == null) {
				if (!string.IsNullOrWhiteSpace(key) && (key.IndexOf("#", StringComparison.Ordinal) < 0)) {
					return m_CoreBusinessLayer.ClassificationItem_GetTree(key, entity.Code, entity.Level, false);
				}
				classificationItems = broker.GetChildren(key);
			}
			else {
				classificationItems = broker.MeterClassification_GetChildren(key, keyChart, keyAlarmDefinition, keyMeterValidationRule, existing, filterSpatial).Distinct(new BaseBusinessEntityComparer<ClassificationItem>()).ToList();
			}

			var classificationsTree = new List<TreeNode>();

			//Apply security filters if exist
			List<Filter> filters = FilterHelper.GetFilter<ClassificationItem>();
			if (filters != null && filters.Any()) {
				var query = (from n in classificationItems
							 join p in filters.Select(filter => filter.Key)
								 on
								 n.KeyClassificationItem equals p
							 where n.Relation == null
							 select n).
					Union(
						from n in classificationItems
						where n.Relation != null
						select n
					);

				var filteredClassificationItems = query.ToList();
				if (filteredClassificationItems.Any()) {
					classificationsTree = filteredClassificationItems.GetTree();
				}
			}
			else {
				classificationsTree = classificationItems.GetTree();
			}

			return classificationsTree;
		}

		/// <summary>
		/// Deletes an existing business entity MeterData.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool MeterData_Delete(MeterData item) {
			MeterDataBroker broker = Helper.CreateInstance<MeterDataBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Delete MeterData by Date Range.
		/// </summary>
		/// <param name="KeyMeter">The key meter.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <returns></returns>
		private LongRunningOperationResult MeterData_DeleteByDateRange(string KeyMeter, DateTime StartDate, DateTime EndDate) {
			try {
				ExternalCrudNotificationService.IsAsynchronousMode = true;
				ExternalCrudNotificationService.ThrowOnNotReady();

				var response = new FormResponse { success = true };

				try {
					var broker = Helper.CreateInstance<MeterDataBroker>();
					var meterBroker = Helper.CreateInstance<MeterBroker>();
					Meter meter = meterBroker.GetItem(KeyMeter);

					// We flush every 10,000 meter data items so we won't hold too much in memory for the Energy Aggregator CRUD notifications.
					bool hasMore = true;
					long amountDeleted = 0;

					while (hasMore) {
						hasMore = broker.DeleteByDateRangeBulk(KeyMeter, StartDate, EndDate, const_meter_data_bulk_page_size);
						amountDeleted += const_meter_data_bulk_page_size;
						ExternalCrudNotificationService.FlushWaitingNotifications();

						if (meter.MeterDataCount != 0) {
							// Prevent division by zero if the meter data count was not calculated.
							LongRunningOperationService.ReportProgress((int)(amountDeleted * 100 / meter.MeterDataCount));
						}

						if (LongRunningOperationService.IsCancelationPending()) {
							hasMore = false;
						}
					}
				}
				catch (Exception ex) {
					response.success = false;
					response.msg = ex.Message;
				}

				return new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, response);
			}
			finally {
				ExternalCrudNotificationService.FlushWaitingNotifications();
			}
		}

		/// <summary>
		/// Delete MeterData by Date Range (long running start point).
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="KeyMeter">The key of the meter.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		public void MeterData_DeleteByDateRangeBegin(Guid operationId, string KeyMeter, DateTime StartDate, DateTime EndDate) {
			LongRunningOperationService.Invoke(operationId, () =>
				MeterData_DeleteByDateRange(KeyMeter, StartDate, EndDate));
		}

		/// <summary>
		/// Creates a new business entity MeterData and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse MeterData_FormCreate(MeterData item) {
			MeterDataBroker broker = Helper.CreateInstance<MeterDataBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity MeterData.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse MeterData_FormLoad(string Key) {
			MeterDataBroker broker = Helper.CreateInstance<MeterDataBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity MeterData and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse MeterData_FormUpdate(MeterData item) {
			MeterDataBroker broker = Helper.CreateInstance<MeterDataBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse MeterData_FormUpdateBatch(string[] keys, MeterData item) {
			MeterDataBroker broker = Helper.CreateInstance<MeterDataBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Generate Random MeterData process.
		/// </summary>
		/// <param name="KeyMeter">The key meter.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="FrequencyNumber">The frequency number.</param>
		/// <param name="Frequency">The frequency.</param>
		/// <param name="Minimum">The minimum.</param>
		/// <param name="Maximum">The maximum.</param>
		/// <returns></returns>
		private LongRunningOperationResult MeterData_Generate(string KeyMeter, DateTime StartDate, DateTime EndDate, int FrequencyNumber, AxisTimeInterval Frequency, double Minimum, double Maximum) {

			try {
				ExternalCrudNotificationService.IsAsynchronousMode = true;
				ExternalCrudNotificationService.ThrowOnNotReady();

				var broker = Helper.CreateInstance<MeterDataBroker>();
				var brokerMeter = Helper.CreateInstance<MeterBroker>();
				var rand = new Random();
				var response = new FormResponse { success = true };
				if (Frequency == AxisTimeInterval.None || Frequency == AxisTimeInterval.Global)
					return new LongRunningOperationResult(LongRunningOperationStatus.Exception);

				var meter = brokerMeter.GetItem(KeyMeter);
				double previousValue = 0;

				int totalCount = GetAmountOfMeterDataItemsInRange(StartDate, EndDate, FrequencyNumber, Frequency);
				int count = 0;
				var bulkData = new List<MeterData>();
				bool shouldContinue = totalCount > 0;

				while (shouldContinue) {
					var item = new MeterData {
						KeyMeter = meter.LocalId, // we set this since we use the mapping for bulk insert.
						AcquisitionDateTime = StartDate,
						Value = previousValue + rand.Next(Convert.ToInt32(Minimum), Convert.ToInt32(Maximum)),
						Validity = MeterDataValidity.Valid

					};

					if (meter.IsAccumulated) {
						previousValue = item.Value;
					}

					bulkData.Add(item);

					// Progress the start date for the next iteration.
					StartDate = AddFrequency(StartDate, FrequencyNumber, Frequency);
					shouldContinue = StartDate <= EndDate;
					count++;

					// Flush each X items in bulk, or the remaining bulk if this is the last iteration.
					if ((count % const_meter_data_bulk_page_size == 0) || (!shouldContinue && bulkData.Any())) {
						try {
							broker.CreateBulk(bulkData);
							bulkData.Clear();
							// Flush each iteration since bulk size is large.
							ExternalCrudNotificationService.FlushWaitingNotifications();
							LongRunningOperationService.ReportProgress(count * 100 / totalCount);
						}
						catch (Exception ex) {
							response.success = false;
							response.msg = ex.Message;
							break;
						}
					}

					if (LongRunningOperationService.IsCancelationPending()) {
						break;
					}
				}
				return new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, response);
			}
			finally {
				ExternalCrudNotificationService.FlushWaitingNotifications();
			}
		}

		/// <summary>
		/// Generate Random MeterData (long running start point).
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="KeyMeter">The key of the meter.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="FrequencyNumber">The frequency number.</param>
		/// <param name="Frequency">The frequency.</param>
		/// <param name="Minimum">The minimum.</param>
		/// <param name="Maximum">The maximum.</param>
		/// <returns></returns>
		public void MeterData_GenerateBegin(Guid operationId, string KeyMeter, DateTime StartDate, DateTime EndDate, int FrequencyNumber, AxisTimeInterval Frequency, double Minimum, double Maximum) {
			LongRunningOperationService.Invoke(operationId, () =>
				MeterData_Generate(KeyMeter, StartDate, EndDate, FrequencyNumber, Frequency, Minimum, Maximum));
		}

		/// <summary>
		/// Gets the complete list of MeterData  grouped by KeyMeter as int, filtered by date range.
		/// </summary>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="KeyMeters">The key meters.</param>
		/// <returns></returns>
		public ConcurrentDictionary<int, SortedList<DateTime, MD>> MeterData_GetAllByDateRange(DateTime? startDate, DateTime? endDate, string[] KeyMeters = null) {
			MeterDataBroker broker = Helper.CreateInstance<MeterDataBroker>();
			var retVal = broker.GetAllByDateRange(startDate, endDate, KeyMeters);
			return retVal;
		}

		/// <summary>
		/// Gets a specific item for the business entity MeterData.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public MeterData MeterData_GetItem(string Key, params string[] fields) {
			MeterDataBroker broker = Helper.CreateInstance<MeterDataBroker>();
			return broker.GetItem(Key, fields);
		}


		/// <summary>
		/// Export all data for a list of Meters
		/// </summary>
		/// <param name="localIds">The meter local ids.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="zip">if set to <c>true</c> [zip].</param>
		/// <returns></returns>
		public StreamResult MeterData_GetCSVExport(List<string> localIds, DateTime? startDate, DateTime? endDate, bool zip) {
			string exportCsv = MeterData_GetCSV(localIds, startDate, endDate);

			MemoryStream ms = exportCsv.GetStream();
			StreamResult retVal;

			if (!zip) {
				retVal = new StreamResult(ms, "meterdata.csv", "csv", MimeType.Csv);
			}
			else {
				var msz = CompressionHelper.Compress(new List<Tuple<string, Stream>> { new Tuple<string, Stream>("meterdata.csv", ms) }, "meterdata.zip");
				retVal = new StreamResult(msz.ToByteArray(), "meterdata.zip", "zip", MimeType.Zip);
			}

			return retVal;
		}

		/// <summary>
		/// Gets the meter data CSV as a string.
		/// </summary>
		/// <param name="localIds">The local ids.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		public string MeterData_GetCSV(IEnumerable<string> localIds, DateTime? startDate, DateTime? endDate) {
			var sb = new StringBuilder();
			var brokerMeter = Helper.CreateInstance<MeterBroker>();
			IEnumerable<Meter> meters = localIds.Select(brokerMeter.GetItemByLocalId).Where(m => m != null);

			foreach (var meter in meters) {
				string csvContent = GetMeterDataCSV(startDate, endDate, meter.KeyMeter, meter.LocalId);

				if (!string.IsNullOrEmpty(csvContent)) {
					if (sb.Length == 0) {
						// Remove entity header.
						csvContent = RemoveFirstLine(csvContent);
					}
					else {
						// Remove both entity header and column headers (they are already there from previous meters).
						csvContent = RemoveFirstLine(RemoveFirstLine(csvContent));
						// Put a line break before the next CSV is written to the String Builder.
						sb.Append("\r\n");
					}

					sb.Append(csvContent);
				}
			}

			string exportCsv = sb.ToString();

			return exportCsv;
		}

		/// <summary>
		/// Gets the meter data CSV.
		/// </summary>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="keyMeter">The key meter.</param>
		/// <param name="meterLocalId">The meter local id.</param>
		/// <returns></returns>
		private string GetMeterDataCSV(DateTime? startDate, DateTime? endDate, string keyMeter, string meterLocalId) {
			#region Paging

			var paging = new PagingParameter { dir = "ASC", sort = "AcquisitionDateTime", start = 0, limit = 0, filters = new List<GridFilter>() };
			if (startDate.HasValue) {
				paging.filters.Add(new GridFilter {
					field = "AcquisitionDateTime",
					data = new GridData {
						comparison = "gt",
						type = "date",
						value =
							startDate.Value.ToString(
								DateTimeHelper.const_date_time_format_for_filter)
					}
				});
			}
			if (endDate.HasValue) {
				paging.filters.Add(new GridFilter {
					field = "AcquisitionDateTime",
					data = new GridData {
						comparison = "lt",
						type = "date",
						value = endDate.Value.ToString(DateTimeHelper.const_date_time_format_for_filter)
					}
				});
			}

			#endregion;

			#region Store

			JsonStore<MeterData> store = MeterData_GetStoreFromMeter(keyMeter, paging, PagingLocation.Database, "UTC", false, "dd/MM/yy HH:mm:ss");

			// Transform the data received from the Energy Aggregator.
			var meterDataObjects = new List<object>();

			foreach (MeterData meterData in store.records) {
				// Manual change for export sake.
				meterData.KeyMeter = meterLocalId;
				// Add to object list since the export method accepts only a list of objects.
				meterDataObjects.Add(meterData);
			}

			#endregion

			#region CSV Content
			if (!meterDataObjects.Any()) {
				return string.Empty;
			}

			string csvContent = MappingService.Providers["CsvSerialization"].Export(meterDataObjects);
			#endregion

			return csvContent;
		}


		/// <summary>
		/// Removes the first line of the text containing multiple lines.
		/// </summary>
		/// <param name="content">The content.</param>
		/// <returns></returns>
		private string RemoveFirstLine(string content) {
			if (!string.IsNullOrEmpty(content)) {
				var index = content.IndexOf("\r\n");
				if (index > 0)
					content = content.Substring(index + 2);
			}
			return content;
		}

		/// <summary>
		/// Gets a list for the business entity Meter.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<MeterData> MeterData_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			var meterDataBroker = Helper.CreateInstance<MeterDataBroker>();
			int total;
			return meterDataBroker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a list for the business entity MeterData from existing Meter.
		/// </summary>
		/// <param name="meter">The meter.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>
		/// List of MeterData.
		/// </returns>
		public List<MeterData> MeterData_GetAllFromMeter(Meter meter, PagingParameter paging) {
			var energyAggregatorServiceAgent = GetEnergyAggregatorServiceAgent();
			var retVal = energyAggregatorServiceAgent.MeterData_GetStoreFromMeter(meter, paging, null, null, false);
			return retVal.records;
		}

		/// <summary>
		/// Gets a json store for the business entity MeterData.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<MeterData> MeterData_GetStore(PagingParameter paging, PagingLocation location) {
			MeterDataBroker broker = Helper.CreateInstance<MeterDataBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity MeterData.
		/// </summary>
		/// <param name="KeyMeter">The Key of the parent Meter.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="timeZoneId">The time zone id.</param>
		/// <param name="rawData">if set to <c>true</c> [raw data].</param>
		/// <param name="datetimeFormat">The datetime format.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<MeterData> MeterData_GetStoreFromMeter(string KeyMeter, PagingParameter paging, PagingLocation location, string timeZoneId, bool rawData, string datetimeFormat) {
			if (string.IsNullOrWhiteSpace(KeyMeter))
				return new JsonStore<MeterData>();

			datetimeFormat = DateTimeHelper.DateTimeFormat(true);

			var energyAggregatorServiceAgent = GetEnergyAggregatorServiceAgent();
			var meter = Meter_GetItem(KeyMeter, "*");
			var retVal = energyAggregatorServiceAgent.MeterData_GetStoreFromMeter(meter, paging, null, null, rawData);

			if (!string.IsNullOrEmpty(timeZoneId)) {
				var timeZone = CalendarHelper.GetTimeZoneFromId(timeZoneId);
				foreach (var md in retVal.records) {
					md.TimeZoneLocal = TimeZoneHelper.GetLocalizedDisplayName(timeZone);
					md.AcquisitionDateTimeLocal = TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(md.AcquisitionDateTime, DateTimeKind.Unspecified), timeZone).ToString(datetimeFormat);
				}
			}

			return retVal;
		}

		/// <summary>
		/// Offset MeterData by Date Range (md.Value * Factor - Offset).
		/// </summary>
		/// <param name="KeyMeter">The key meter.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="Factor">The factor.</param>
		/// <param name="Offset">The offset.</param>
		/// <returns></returns>
		private LongRunningOperationResult MeterData_OffsetByDateRange(string KeyMeter, DateTime StartDate, DateTime EndDate, double Factor, double Offset) {
			try {
				ExternalCrudNotificationService.IsAsynchronousMode = true;
				ExternalCrudNotificationService.ThrowOnNotReady();

				var broker = Helper.CreateInstance<MeterDataBroker>();
				var response = new FormResponse { success = true };
				int total;
				int count = 0;
				#region Fetch Data
				var data = broker.GetAll(new PagingParameter {
					filters = new List<GridFilter> {
						new GridFilter {
							field="KeyMeter",
							data=new GridData {
								comparison="eq",
								type="string",
								value=KeyMeter
							}
						},new GridFilter {
							field="AcquisitionDateTime",
							data=new GridData {
								comparison="gt",
								type="date",
								value=StartDate.ToUniversalTime().ToString(DateTimeHelper.const_date_time_format_for_filter)
							}
						},
						new GridFilter {
							field="AcquisitionDateTime",
							data=new GridData {
								comparison="lt",
								type="date",
								value=EndDate.ToUniversalTime().ToString(DateTimeHelper.const_date_time_format_for_filter)
							}
						}
					}
				}, PagingLocation.Database, out total);
				#endregion

				foreach (var md in data) {
					try {

						md.Value = (md.Value * Factor - Offset);
						broker.FormUpdate(md);
						count += 1;
						LongRunningOperationService.ReportProgress(count * 100 / total);

						if (LongRunningOperationService.IsCancelationPending()) {
							break;
						}
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
				return new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, response);
			}
			finally {
				ExternalCrudNotificationService.FlushWaitingNotifications();
			}
		}

		/// <summary>
		/// Offset MeterData by Date Range (long running start point).
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="KeyMeter">The key of the meter.</param>
		/// <param name="StartDate">The start date.</param>
		/// <param name="EndDate">The end date.</param>
		/// <param name="Factor">The Factor.</param>
		/// <param name="Offset">The Offset.</param>
		public void MeterData_OffsetByDateRangeBegin(Guid operationId, string KeyMeter, DateTime StartDate, DateTime EndDate, double Factor, double Offset) {
			LongRunningOperationService.Invoke(operationId, () => MeterData_OffsetByDateRange(KeyMeter, StartDate, EndDate, Factor, Offset));
		}

		/// <summary>
		/// Saves a crud store for the business entity MeterData.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<MeterData> MeterData_SaveStore(CrudStore<MeterData> store) {
			MeterDataBroker broker = Helper.CreateInstance<MeterDataBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity MeterOperation.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool MeterOperation_Delete(MeterOperation item) {
			MeterOperationBroker broker = Helper.CreateInstance<MeterOperationBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity MeterOperation and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse MeterOperation_FormCreate(MeterOperation item) {
			MeterOperationBroker broker = Helper.CreateInstance<MeterOperationBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity MeterOperation.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse MeterOperation_FormLoad(string Key) {
			MeterOperationBroker broker = Helper.CreateInstance<MeterOperationBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity MeterOperation and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse MeterOperation_FormUpdate(MeterOperation item) {
			MeterOperationBroker broker = Helper.CreateInstance<MeterOperationBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse MeterOperation_FormUpdateBatch(string[] keys, MeterOperation item) {
			MeterOperationBroker broker = Helper.CreateInstance<MeterOperationBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity MeterOperation.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<MeterOperation> MeterOperation_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			MeterOperationBroker broker = Helper.CreateInstance<MeterOperationBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity MeterOperation.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public MeterOperation MeterOperation_GetItem(string Key, params string[] fields) {
			MeterOperationBroker broker = Helper.CreateInstance<MeterOperationBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity MeterOperation.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<MeterOperation> MeterOperation_GetStore(PagingParameter paging, PagingLocation location) {
			MeterOperationBroker broker = Helper.CreateInstance<MeterOperationBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of MeterOperations for a specific meter.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMeter">The key of the meter.</param>
		/// <param name="KeyMeterOperation">The key meter operation to exclude.</param>
		/// <param name="order">The order if the current meter operation in order to filter available results. 0 to disable filtering.</param>
		/// <returns></returns>
		public JsonStore<MeterOperation> MeterOperation_GetStoreByKeyMeter(PagingParameter paging, string KeyMeter,
																			string KeyMeterOperation, int order) {
			MeterOperationBroker broker = Helper.CreateInstance<MeterOperationBroker>();
			return broker.GetStoreByKeyMeter(paging, KeyMeter, KeyMeterOperation, order);
		}

		/// <summary>
		/// Saves a crud store for the business entity MeterOperation.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<MeterOperation> MeterOperation_SaveStore(CrudStore<MeterOperation> store) {
			MeterOperationBroker broker = Helper.CreateInstance<MeterOperationBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Copy the specified palettes and colors..
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public FormResponse Palette_Copy(string[] keys) {
			FormResponse response = new FormResponse { success = false };
			var broker = Helper.CreateInstance<PaletteBroker>();

			foreach (string key in keys) {
				var item = broker.GetItem(key, "*");
				broker.Copy(item, out response);
			}
			return response;
		}

		/// <summary>
		/// Deletes an existing business entity Palette.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Palette_Delete(Palette item) {
			PaletteBroker broker = Helper.CreateInstance<PaletteBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Palette and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="palettecolor">The palettecolor crud store.</param>
		/// <returns></returns>
		public FormResponse Palette_FormCreate(Palette item, CrudStore<PaletteColor> palettecolor) {
			FormResponse response;
			PaletteBroker broker = Helper.CreateInstance<PaletteBroker>();
			PaletteColorBroker borkerColor = Helper.CreateInstance<PaletteColorBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						borkerColor.SavePalettePaletteColor((Palette)response.data, palettecolor);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity Palette.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Palette_FormLoad(string Key) {
			PaletteBroker broker = Helper.CreateInstance<PaletteBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Palette and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="palettecolor">The palettecolor crudstore.</param>
		/// <returns></returns>
		public FormResponse Palette_FormUpdate(Palette item, CrudStore<PaletteColor> palettecolor) {
			FormResponse response;
			PaletteBroker broker = Helper.CreateInstance<PaletteBroker>();
			PaletteColorBroker borkerColor = Helper.CreateInstance<PaletteColorBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						borkerColor.SavePalettePaletteColor((Palette)response.data, palettecolor);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Palette_FormUpdateBatch(string[] keys, Palette item) {
			PaletteBroker broker = Helper.CreateInstance<PaletteBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Palette. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Palette> Palette_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			PaletteBroker broker = Helper.CreateInstance<PaletteBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}


		/// <summary>
		/// Gets a specific item for the business entity Palette.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Palette Palette_GetItem(string Key, params string[] fields) {
			PaletteBroker broker = Helper.CreateInstance<PaletteBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity Palette.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Palette> Palette_GetStore(PagingParameter paging, PagingLocation location) {
			PaletteBroker broker = Helper.CreateInstance<PaletteBroker>();
			//Force in memory paging because of the Label field which doesnt exist in DB.
			location = PagingLocation.Memory;
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Returns the stream of the image that represent a palette.
		/// </summary>
		/// <param name="Key">The key of the Palette.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		/// <returns></returns>
		public StreamResult Palette_GetStreamImage(string Key, int height, int width) {
			PaletteBroker broker = Helper.CreateInstance<PaletteBroker>();
			var palette = broker.GetItem(Key, "Colors");

			BitmapWithGraphics img;
			if (palette.Colors == null || palette.Colors.Count == 0) {
				img = new BitmapWithGraphics(height, height);
			}
			else {
				int colorWidth = width / palette.Colors.Count;
				img = new BitmapWithGraphics(colorWidth * palette.Colors.Count, height);
				int xpos = 0;
				foreach (var color in palette.Colors) {
					img.DrawRectangle(xpos, 0, colorWidth, height, color.GetColor(), 0, Color.Black);
					xpos += colorWidth;
				}
			}

			var retVal = new StreamResult(img.GetStream(), palette.MsgCode, "png", MimeType.Png);
			//img.Dispose();
			return retVal;
		}

		/// <summary>
		/// Inits the available Palette list based on Palettes existing the Charting Provider.
		/// </summary>
		public void Palette_InitFromProvider() {
			var list = ChartingService.GetPalettes();
			PaletteBroker brokerPalette = Helper.CreateInstance<PaletteBroker>();
			PaletteColorBroker brokerColor = Helper.CreateInstance<PaletteColorBroker>();

			foreach (var p in list) {
				var paletteLocalId = "MSG_PALETTE_" + p.Item1.ToUpper();
				m_CoreBusinessLayer.LocalizationResource_FormCreate(new LocalizationResource() {
					Key = paletteLocalId,
					Value = p.Item1,
					Culture = "en"
				});

				//var paletteLocalId = "msg_palette_" + p.Item1.ToLower();
				var palette = brokerPalette.GetItemByLocalId(paletteLocalId);
				if (palette == null) {
					palette = new Palette {
						MsgCode = paletteLocalId,
						LocalId = paletteLocalId
					};
					palette = brokerPalette.Save(palette);
					int order = 0;
					foreach (var c in p.Item2) {
						var color = new PaletteColor {
							ColorB = c.B,
							ColorG = c.G,
							ColorR = c.R,
							KeyPalette = palette.KeyPalette,
							Order = order
						};
						brokerColor.Save(color);
						order += 1;
					}
				}
			}
			Palette_GenerateModernUI();
		}


		/// <summary>
		/// Inits the available Palette list based on Palettes existing the Charting Provider.
		/// </summary>
		public void Palette_GenerateModernUI() {
			PaletteBroker brokerPalette = Helper.CreateInstance<PaletteBroker>();
			PaletteColorBroker brokerColor = Helper.CreateInstance<PaletteColorBroker>();

			var paletteLocalId = "MSG_PALETTE_MODERNUI";
			var palette = brokerPalette.GetItemByLocalId(paletteLocalId);
			if (palette == null) {
				palette = new Palette {
					MsgCode = "msg_chart_modernui",
					LocalId = paletteLocalId
				};
				palette = brokerPalette.Save(palette);
				int order = 0;
				foreach (ModernUIColor mc in Enum.GetValues(typeof(ModernUIColor))) {
					var c = ColorHelper.GetColor(ChartHelper.GetModernUIColor(mc));
					var color = new PaletteColor {
						ColorB = c.B,
						ColorG = c.G,
						ColorR = c.R,
						KeyPalette = palette.KeyPalette,
						Order = order
					};
					brokerColor.Save(color);
					order += 1;
				}
			}
		}

		/// <summary>
		/// Saves a crud store for the business entity Palette.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Palette> Palette_SaveStore(CrudStore<Palette> store) {
			PaletteBroker broker = Helper.CreateInstance<PaletteBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity PaletteColor.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool PaletteColor_Delete(PaletteColor item) {
			PaletteColorBroker broker = Helper.CreateInstance<PaletteColorBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity PaletteColor and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse PaletteColor_FormCreate(PaletteColor item) {
			PaletteColorBroker broker = Helper.CreateInstance<PaletteColorBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity PaletteColor.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PaletteColor_FormLoad(string Key) {
			PaletteColorBroker broker = Helper.CreateInstance<PaletteColorBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PaletteColor and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse PaletteColor_FormUpdate(PaletteColor item) {
			PaletteColorBroker broker = Helper.CreateInstance<PaletteColorBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PaletteColor_FormUpdateBatch(string[] keys, PaletteColor item) {
			PaletteColorBroker broker = Helper.CreateInstance<PaletteColorBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PaletteColor. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<PaletteColor> PaletteColor_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			PaletteColorBroker broker = Helper.CreateInstance<PaletteColorBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity PaletteColor.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public PaletteColor PaletteColor_GetItem(string Key, params string[] fields) {
			PaletteColorBroker broker = Helper.CreateInstance<PaletteColorBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a list of PaletteColor for a specific Palette.
		/// </summary>
		/// <param name="KeyPalette">The key of the Palette.</param>
		/// <returns></returns>
		public List<PaletteColor> PaletteColor_GetListByKeyPalette(string KeyPalette) {
			PaletteColorBroker broker = Helper.CreateInstance<PaletteColorBroker>();
			return broker.GetListByKeyPalette(KeyPalette);
		}

		/// <summary>
		/// Gets a json store for the business entity PaletteColor.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PaletteColor> PaletteColor_GetStore(PagingParameter paging, PagingLocation location) {
			PaletteColorBroker broker = Helper.CreateInstance<PaletteColorBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of PaletteColor for a specific Palette.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPalette">The key of the Palette.</param>
		/// <returns></returns>
		public JsonStore<PaletteColor> PaletteColor_GetStoreByKeyPalette(PagingParameter paging, string KeyPalette) {
			PaletteColorBroker broker = Helper.CreateInstance<PaletteColorBroker>();
			return broker.GetStoreByKeyPalette(paging, KeyPalette);
		}

		/// <summary>
		/// Saves a crud store for the business entity PaletteColor.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PaletteColor> PaletteColor_SaveStore(CrudStore<PaletteColor> store) {
			PaletteColorBroker broker = Helper.CreateInstance<PaletteColorBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity Playlist.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Playlist_Delete(Playlist item) {
			PlaylistBroker broker = Helper.CreateInstance<PlaylistBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Playlist and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="pages">The pages.</param>
		/// <returns></returns>
		public FormResponse Playlist_FormCreate(Playlist item, CrudStore<PlaylistPage> pages) {
			FormResponse response;
			PlaylistBroker broker = Helper.CreateInstance<PlaylistBroker>();
			PlaylistPageBroker borkerColor = Helper.CreateInstance<PlaylistPageBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						borkerColor.SavePlaylistPlaylistPage((Playlist)response.data, pages);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity Playlist.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Playlist_FormLoad(string Key) {
			PlaylistBroker broker = Helper.CreateInstance<PlaylistBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Playlist and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="pages">The pages crudstore.</param>
		/// <returns></returns>
		public FormResponse Playlist_FormUpdate(Playlist item, CrudStore<PlaylistPage> pages) {
			FormResponse response;
			PlaylistBroker broker = Helper.CreateInstance<PlaylistBroker>();
			PlaylistPageBroker borkerColor = Helper.CreateInstance<PlaylistPageBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						borkerColor.SavePlaylistPlaylistPage((Playlist)response.data, pages);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Playlist_FormUpdateBatch(string[] keys, Playlist item) {
			PlaylistBroker broker = Helper.CreateInstance<PlaylistBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Generates the differents playlist as images.
		/// </summary>
		/// <param name="Keys">The keys.</param>
		public void Playlist_Generate(string[] Keys) {
			var broker = Helper.CreateInstance<PlaylistBroker>();
			var brokerPage = Helper.CreateInstance<PlaylistPageBroker>();
			var brokerDocument = Helper.CreateInstance<DocumentBroker>();

			var playlists = new List<Playlist>();
			if (Keys == null || Keys.Length == 0) {
				playlists = broker.GetAll(false);
			}
			else {
				playlists.AddRange(Keys.Select(broker.GetItem).Where(playlist => playlist != null));
			}
			int totalCount = 0;
			foreach (var playlist in playlists) {
				broker.PopulateList(playlist, "*");
				totalCount += playlist.Pages.Count;
			}

			int count = 0;
			foreach (var playlist in playlists) {
				foreach (var page in playlist.Pages) {
					if (page != null) {
						var oldDocKey = page.KeyDocument;
						var originalCulture = Helper.GetCurrentUICulture();
						try {
							Helper.SetUICulture(playlist.KeyLocalizationCulture);
							Chart_PreLoadAll(clearCache: true, KeyPortalTab: page.KeyPortalTab);
						}
						finally {
							Helper.SetUICulture(originalCulture);
						}

						var imgStream = PortalTab_GetStreamImage(page.KeyPortalTab, playlist.Width, playlist.Height, playlist.Creator,
																 playlist.KeyLocalizationCulture);
						var imgDoc = new Document(imgStream);
						imgDoc = brokerDocument.Save(imgDoc);
						if (imgDoc != null) {

							page.KeyDocument = imgDoc.KeyDocument;
							brokerPage.Save(page);

							//we delete the old document
							if (!string.IsNullOrEmpty(oldDocKey)) {
								brokerDocument.Delete(oldDocKey);
							}
						}
					}
					count += 1;
					LongRunningOperationService.ReportProgress(count * 100 / totalCount);
				}
			}
		}

		/// <summary>
		/// Generates the differents playlist as images. with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
		public void Playlist_GenerateBegin(Guid operationId, string[] Keys) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				Playlist_Generate(Keys);
				var result = new LongRunningOperationResult(LongRunningOperationStatus.Completed);
				return result;
			});
		}

		/// <summary>
		/// Gets a list for the business entity Playlist. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Playlist> Playlist_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			PlaylistBroker broker = Helper.CreateInstance<PlaylistBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity Playlist.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Playlist Playlist_GetItem(string Key, params string[] fields) {
			PlaylistBroker broker = Helper.CreateInstance<PlaylistBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Get the string representing the html code for a slideshow for a specific Playlist.
		/// </summary>
		/// <param name="KeyPlaylist">The key playlist.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public string Playlist_GetSlideshow(string KeyPlaylist, int? width = null, int? height = null) {
			var broker = Helper.CreateInstance<PlaylistBroker>();
			var brokerDocument = Helper.CreateInstance<DocumentBroker>();

			var playlist = broker.GetItem(KeyPlaylist, "*");
			var slideshowPages = new List<SlideshowPage>();

			foreach (var page in playlist.Pages) {
				if (!string.IsNullOrEmpty(page.KeyDocument)) {
					var document = brokerDocument.GetItem(page.KeyDocument);
					if (document != null) {
						slideshowPages.Add(new SlideshowPage {
							Duration = page.Duration,
							Title = page.PortalTabTitle,
							Url = "Public.svc/PlaylistPage_GetStreamImage?Key=" + page.KeyPlaylistPage,
							Width = width ?? page.PlaylistWidth,
							Height = height ?? page.PlaylistHeight
						});
					}
				}
			}

			string retVal = SlideshowService.GetSlideshow(slideshowPages, playlist.KeyLocalizationCulture);
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity Playlist.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Playlist> Playlist_GetStore(PagingParameter paging, PagingLocation location) {
			PlaylistBroker broker = Helper.CreateInstance<PlaylistBroker>();
			var retVal = broker.GetStore(paging, location);
			foreach (Playlist playlist in retVal.records)
				broker.PopulateList(playlist, "*");
			return retVal;
		}

		/// <summary>
		/// Gets the Playlist store by the job step.
		/// </summary>
		/// <param name="keyJob">The key job.</param>
		/// <param name="keyStep">The key step.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		public JsonStore<Playlist> Playlist_GetStoreByJobStep(string keyJob, string keyStep, PagingParameter paging, PagingLocation location) {
			var coreBusinessLayer = Helper.Resolve<ICoreBusinessLayer>();
			JsonStore<Playlist> children = coreBusinessLayer.GetChildEntitiesByParentJobStep<Playlist>(keyJob, keyStep, paging);

			return children;
		}

		/// <summary>
		/// Saves a crud store for the business entity Playlist.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Playlist> Playlist_SaveStore(CrudStore<Playlist> store) {
			PlaylistBroker broker = Helper.CreateInstance<PlaylistBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity PlaylistPage.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool PlaylistPage_Delete(PlaylistPage item) {
			PlaylistPageBroker broker = Helper.CreateInstance<PlaylistPageBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity PlaylistPage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse PlaylistPage_FormCreate(PlaylistPage item) {
			PlaylistPageBroker broker = Helper.CreateInstance<PlaylistPageBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity PlaylistPage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PlaylistPage_FormLoad(string Key) {
			PlaylistPageBroker broker = Helper.CreateInstance<PlaylistPageBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PlaylistPage and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse PlaylistPage_FormUpdate(PlaylistPage item) {
			PlaylistPageBroker broker = Helper.CreateInstance<PlaylistPageBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PlaylistPage_FormUpdateBatch(string[] keys, PlaylistPage item) {
			PlaylistPageBroker broker = Helper.CreateInstance<PlaylistPageBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PlaylistPage. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<PlaylistPage> PlaylistPage_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			PlaylistPageBroker broker = Helper.CreateInstance<PlaylistPageBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity PlaylistPage.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public PlaylistPage PlaylistPage_GetItem(string Key, params string[] fields) {
			PlaylistPageBroker broker = Helper.CreateInstance<PlaylistPageBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity PlaylistPage.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PlaylistPage> PlaylistPage_GetStore(PagingParameter paging, PagingLocation location) {
			PlaylistPageBroker broker = Helper.CreateInstance<PlaylistPageBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of  PlaylistPage for a specific Playlist.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPlaylist">The key of the Playlist.</param>
		/// <returns></returns>
		public JsonStore<PlaylistPage> PlaylistPage_GetStoreByKeyPlaylist(PagingParameter paging, string KeyPlaylist) {
			PlaylistPageBroker broker = Helper.CreateInstance<PlaylistPageBroker>();
			return broker.GetStoreByKeyPlaylist(paging, KeyPlaylist);
		}

		/// <summary>
		/// Returns the stream of the image from a playlist page (corresponding to the latest generation of the playlist).
		/// </summary>
		/// <param name="Key">The key of the playlist page.</param>
		/// <returns></returns>
		public StreamResult PlaylistPage_GetStreamImage(string Key) {
			StreamResult retVal = null;
			var brokerPage = Helper.CreateInstance<PlaylistPageBroker>();
			var brokerDocument = Helper.CreateInstance<DocumentBroker>();

			var page = brokerPage.GetItem(Key);
			if (page != null && !string.IsNullOrEmpty(page.KeyDocument)) {
				var document = brokerDocument.GetItem(page.KeyDocument);
				if (document != null) {
					retVal = new StreamResult(document.GetStream(), document.DocumentType);
				}
			}
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity PlaylistPage.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PlaylistPage> PlaylistPage_SaveStore(CrudStore<PlaylistPage> store) {
			PlaylistPageBroker broker = Helper.CreateInstance<PlaylistPageBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity PortalColumn.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="deleteContent">if set to <c>true</c> [delete content].</param>
		/// <returns></returns>
		public bool PortalColumn_Delete(PortalColumn item, bool deleteContent = false) {
			if (deleteContent) {
				var portlets = item.Portlets;

				foreach (var portlet in portlets) {
					var deletePortlet = true;
					if (portlet.Entity != null) {
						var content = (IPortlet)portlet.Entity;
						deletePortlet = !content.IsFavorite;
					}
					Portlet_Delete(portlet, deletePortlet);
				}
			}

			PortalColumnBroker broker = Helper.CreateInstance<PortalColumnBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity PortalColumn and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="portlet">the portlet crud store attached to this portal column.</param>
		/// <returns></returns>
		public FormResponse PortalColumn_FormCreate(PortalColumn item, CrudStore<Portlet> portlet) {
			FormResponse response;
			PortalColumnBroker broker = Helper.CreateInstance<PortalColumnBroker>();
			PortletBroker brokerPortlet = Helper.CreateInstance<PortletBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						brokerPortlet.SavePortalColumnPortlet((PortalColumn)response.data, portlet);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			broker.PopulateList((PortalColumn)response.data, "*");
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity PortalColumn.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PortalColumn_FormLoad(string Key) {
			PortalColumnBroker broker = Helper.CreateInstance<PortalColumnBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PortalColumn and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="portlet">the portlet crud store attached to this portal column.</param>
		/// <returns></returns>
		public FormResponse PortalColumn_FormUpdate(PortalColumn item, CrudStore<Portlet> portlet) {

			FormResponse response;
			PortalColumnBroker broker = Helper.CreateInstance<PortalColumnBroker>();
			PortletBroker brokerPortlet = Helper.CreateInstance<PortletBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						brokerPortlet.SavePortalColumnPortlet((PortalColumn)response.data, portlet);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			broker.PopulateList((PortalColumn)response.data, "*");
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PortalColumn_FormUpdateBatch(string[] keys, PortalColumn item) {
			PortalColumnBroker broker = Helper.CreateInstance<PortalColumnBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Generate PortalColumns from config.
		/// </summary>
		/// <param name="config">The config.</param>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <returns></returns>
		private List<PortalColumn> PortalColumn_GenerateFromConfig(PortalTabColumnConfig config, string KeyPortalTab) {
			var retVal = new List<PortalColumn>();
			switch (config) {
				case PortalTabColumnConfig.Column1_100:
					retVal.Add(new PortalColumn { Flex = 1, Order = 1, KeyPortalTab = KeyPortalTab });
					break;
				case PortalTabColumnConfig.Column2_2575:
					retVal.Add(new PortalColumn { Flex = 25, Order = 1, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 75, Order = 2, KeyPortalTab = KeyPortalTab });
					break;
				case PortalTabColumnConfig.Column2_5050:
					retVal.Add(new PortalColumn { Flex = 50, Order = 1, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 50, Order = 2, KeyPortalTab = KeyPortalTab });
					break;
				case PortalTabColumnConfig.Column2_7525:
					retVal.Add(new PortalColumn { Flex = 75, Order = 1, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 25, Order = 2, KeyPortalTab = KeyPortalTab });
					break;
				case PortalTabColumnConfig.Column3_252550:
					retVal.Add(new PortalColumn { Flex = 25, Order = 1, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 25, Order = 2, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 50, Order = 3, KeyPortalTab = KeyPortalTab });
					break;
				case PortalTabColumnConfig.Column3_255025:
					retVal.Add(new PortalColumn { Flex = 25, Order = 1, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 50, Order = 2, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 20, Order = 3, KeyPortalTab = KeyPortalTab });
					break;
				case PortalTabColumnConfig.Column3_333333:
					retVal.Add(new PortalColumn { Flex = 33, Order = 1, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 33, Order = 2, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 33, Order = 3, KeyPortalTab = KeyPortalTab });
					break;
				case PortalTabColumnConfig.Column3_502525:
					retVal.Add(new PortalColumn { Flex = 50, Order = 1, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 25, Order = 2, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 25, Order = 3, KeyPortalTab = KeyPortalTab });
					break;
				case PortalTabColumnConfig.Column4_25252525:
					retVal.Add(new PortalColumn { Flex = 25, Order = 1, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 25, Order = 2, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 25, Order = 3, KeyPortalTab = KeyPortalTab });
					retVal.Add(new PortalColumn { Flex = 25, Order = 4, KeyPortalTab = KeyPortalTab });
					break;
			}


			return retVal;

		}

		/// <summary>
		/// Gets a list for the business entity PortalColumn. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<PortalColumn> PortalColumn_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			PortalColumnBroker broker = Helper.CreateInstance<PortalColumnBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity PortalColumn.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public PortalColumn PortalColumn_GetItem(string Key, params string[] fields) {
			PortalColumnBroker broker = Helper.CreateInstance<PortalColumnBroker>();
			return broker.GetItem(Key, "*");
		}

		/// <summary>
		/// Gets a store of portalcolumn for a specific portal tab.
		/// </summary>
		/// <param name="KeyPortalTab">The key of the portal tab.</param>
		/// <returns></returns>
		public List<PortalColumn> PortalColumn_GetListByKeyPortalTab(string KeyPortalTab) {
			PortalColumnBroker broker = Helper.CreateInstance<PortalColumnBroker>();
			return broker.GetListByKeyPortalTab(KeyPortalTab);
		}

		/// <summary>
		/// Gets a json store for the business entity PortalColumn.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PortalColumn> PortalColumn_GetStore(PagingParameter paging, PagingLocation location) {
			PortalColumnBroker broker = Helper.CreateInstance<PortalColumnBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of portalcolumn for a specific portal tab.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalTab">The key of the portal tab.</param>
		/// <returns></returns>
		public JsonStore<PortalColumn> PortalColumn_GetStoreByKeyPortalTab(PagingParameter paging, string KeyPortalTab) {
			PortalColumnBroker broker = Helper.CreateInstance<PortalColumnBroker>();
			return broker.GetStoreByKeyPortalTab(paging, KeyPortalTab);
		}

		/// <summary>
		/// Saves a crud store for the business entity PortalColumn.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PortalColumn> PortalColumn_SaveStore(CrudStore<PortalColumn> store) {
			PortalColumnBroker broker = Helper.CreateInstance<PortalColumnBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Apply the filter Spatial to all the Charts contained in the portal tab.
		/// </summary>
		/// <param name="Key">The portaltab Key.</param>
		/// <param name="filterSpatial">The spatial filter.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <returns></returns>
		public FormResponse PortalTab_ApplySpatialFilter(string Key, List<BaseBusinessEntity> filterSpatial, string TimeZoneId = null) {
			List<Location> newLocations = filterSpatial.ToLocation();
			return PortalTab_ApplyTemplatePersonalization(Key, newLocations, TimeZoneId);
		}

		/// <summary>
		/// Apply the filter Spatial to all the Charts contained in the portal tab.
		/// </summary>
		/// <param name="Key">The portaltab Key.</param>
		/// <param name="filterSpatial">The spatial filter.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="listCharts">The list charts.</param>
		/// <returns></returns>
		private FormResponse PortalTab_ApplyTemplatePersonalization(string Key, List<Location> filterSpatial, string TimeZoneId = null, List<Chart> listCharts = null) {		
			var response = new FormResponse { success = true };
			var originalFilter = FilterHelper.GetAll();

			List<string> KeyCharts = PortalTab_GetChartKeys(Key);
			IEnumerable<string> KeyAlarmTables = PortalTab_GetAlarmTableKeys(Key);

			try {
				ApplyChartSpatialFilterAndTimeZone(KeyCharts, filterSpatial, TimeZoneId, listCharts);
                ApplyAlarmTableSpatialFilter(KeyAlarmTables, filterSpatial);
			}
			catch (SystemException e) {
				response.success = false;
				response.msg = e.Message;
			}
			FilterHelper.ClearFilters();
			FilterHelper.AddFilters(originalFilter);
			return response;
		}

        /// <summary>
        /// Applies spatial filters to the alarm tables.
        /// </summary>
        /// <param name="KeyAlarmTables">The key alarm tables.</param>
        /// <param name="newLocations">The new locations.</param>
        private void ApplyAlarmTableSpatialFilter(IEnumerable<string> KeyAlarmTables, List<Location> newLocations) {
            var brokerLocation = Helper.CreateInstance<LocationBroker>();
            foreach (string KeyAlarmTable in KeyAlarmTables) {
                AlarmTable alarm = AlarmTable_GetItem(KeyAlarmTable);
                List<Location> existingLocations = brokerLocation.GetFilterSpatialStoreByKeyAlarmTable(new PagingParameter(), KeyAlarmTable).records;

				var storeLocation = new CrudStore<Location> { destroy = existingLocations };
                brokerLocation.SaveAlarmTableFilterSpatial(alarm, storeLocation);
				storeLocation = new CrudStore<Location> { create = newLocations };
                brokerLocation.SaveAlarmTableFilterSpatial(alarm, storeLocation);
            }
        }

        /// <summary>
        /// Applies the chart spatial filter and time zone.
        /// </summary>
        /// <param name="KeyCharts">The key charts.</param>
        /// <param name="newLocations">The new locations.</param>
        /// <param name="TimeZoneId">The time zone identifier.</param>
		/// <param name="listCharts">The list charts.</param>
		private void ApplyChartSpatialFilterAndTimeZone(IEnumerable<string> KeyCharts, List<Location> newLocations, string TimeZoneId, List<Chart> listCharts = null) {
			var brokerChart = Helper.CreateInstance<ChartBroker>(); 
			if (listCharts == null) {
		        foreach (string KeyChart in KeyCharts) {
			        var chart = brokerChart.GetItem(KeyChart);
			        ApplyChartSpatialFilterAndTimeZone(chart, newLocations, TimeZoneId);
		        }
	        }
	        else {
		        foreach (Chart chart in listCharts) {
					ApplyChartSpatialFilterAndTimeZone(chart, newLocations, TimeZoneId);
		        }
	        }
        }

		/// <summary>
		/// Applies the chart spatial filter and time zone.
		/// </summary>
		/// <param name="newLocations"></param>
		/// <param name="TimeZoneId"></param>
		/// <param name="chart"></param>
		private void ApplyChartSpatialFilterAndTimeZone(Chart chart, List<Location> newLocations, string TimeZoneId) {
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			var brokerChart = Helper.CreateInstance<ChartBroker>();

			FilterHelper.AddFilter(new Filter(chart.KeyChart), typeof(Chart));

			if (!chart.LockFilterSpatial && newLocations != null && newLocations.Count > 0) {
				Chart_ClearCache(chart.KeyChart, chart.KeyDynamicDisplay);

				List<Location> existingLocations =
					brokerLocation.GetFilterSpatialStoreByKeyChart(new PagingParameter(), chart.KeyChart).records;
				var storeLocation = new CrudStore<Location> { destroy = existingLocations };
				brokerLocation.SaveChartFilterSpatial(chart, storeLocation);

				storeLocation = new CrudStore<Location> { create = newLocations };
				brokerLocation.SaveChartFilterSpatial(chart, storeLocation);
			}
			if (!string.IsNullOrEmpty(TimeZoneId) && chart.TimeZoneId != TimeZoneId) {
				chart.TimeZoneId = TimeZoneId;
				brokerChart.Save(chart);
			}
		}

		/// <summary>
		/// Copy existing business entity PortalTab and it s content.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy </param>
		/// <returns></returns>
		public FormResponse PortalTab_Copy(string[] keys) {
			FormResponse response = new FormResponse { success = false };
			var broker = Helper.CreateInstance<PortalTabBroker>();
			var brokerWindow = Helper.CreateInstance<PortalWindowBroker>();

			foreach (string key in keys) {
				var item = broker.GetItem(key, "*");
				var window = brokerWindow.GetItem(item.KeyPortalWindow);
				item.Order = window.PortalWindowTabNumber + 1;
				broker.Copy(item, out response);
			}
			return response;
		}

		/// <summary>
		/// Deletes an existing business entity PortalTab.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="deleteContent">if set to <c>true</c> [delete content].</param>
		/// <returns></returns>
		public bool PortalTab_Delete(PortalTab item, bool deleteContent = false) {
			if (deleteContent) {
				var portlets = item.GetPortlets();
				foreach (var portlet in portlets) {
					Portlet_Delete(portlet, true);
				}
			}
			PortalTabBroker broker = Helper.CreateInstance<PortalTabBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity PortalTab and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="portalcolumn">the portalcolumn crud store attached to this portal tab.</param>
		/// <returns></returns>
		public FormResponse PortalTab_FormCreate(PortalTab item, CrudStore<PortalColumn> portalcolumn) {
			FormResponse response;
			var broker = Helper.CreateInstance<PortalTabBroker>();
			var brokerColumn = Helper.CreateInstance<PortalColumnBroker>();

			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						var tab = (PortalTab)response.data;
						if (portalcolumn.IsEmpty()) {
							//item.Tabs.Add(tab);
							var columns = PortalColumn_GenerateFromConfig(tab.ColumnConfig, tab.KeyPortalTab);
							foreach (var column in columns) {
								brokerColumn.Save(column);
							}
						}
						else {
							brokerColumn.SavePortalTabPortalColumn(tab, portalcolumn);

						}
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			PortalTab_VerifyColumns(((PortalTab)response.data).KeyPortalTab);
			broker.PopulateList((PortalTab)response.data, "*");
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity PortalTab.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PortalTab_FormLoad(string Key) {
			PortalTabBroker broker = Helper.CreateInstance<PortalTabBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PortalTab and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="portalcolumn">the portalcolumn crud store attached to this portal tab.</param>
		/// <returns></returns>
		public FormResponse PortalTab_FormUpdate(PortalTab item, CrudStore<PortalColumn> portalcolumn) {
			FormResponse response;
			PortalTabBroker broker = Helper.CreateInstance<PortalTabBroker>();


			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
                        PortalColumnBroker brokerColumn = Helper.CreateInstance<PortalColumnBroker>();
						brokerColumn.SavePortalTabPortalColumn((PortalTab)response.data, portalcolumn);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			PortalTab_VerifyColumns(((PortalTab)response.data).KeyPortalTab);
			broker.PopulateList((PortalTab)response.data, "*");


			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PortalTab_FormUpdateBatch(string[] keys, PortalTab item) {
			PortalTabBroker broker = Helper.CreateInstance<PortalTabBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets the list of Chart's Key from a specific PortalTab
		/// </summary>
		/// <param name="KeyPortalTab">The PortalTab key.</param>
		/// <returns></returns>
		private IEnumerable<string> PortalTab_GetAlarmTableKeys(string KeyPortalTab) {
			var retVal = PortalTab_GetPortletKeys(KeyPortalTab, "Vizelia.FOL.BusinessEntities.AlarmTable");
			return retVal;

		}

		/// <summary>
		/// Gets a list for the business entity PortalTab. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<PortalTab> PortalTab_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			PortalTabBroker broker = Helper.CreateInstance<PortalTabBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets the list of Chart's Key from a specific PortalTab
		/// </summary>
		/// <param name="KeyPortalTab">The PortalTab key.</param>
		/// <param name="includeDrawingCanvas">if set to <c>true</c> [include drawing canvas].</param>
		/// <returns></returns>
		private List<string> PortalTab_GetChartKeys(string KeyPortalTab, bool includeDrawingCanvas = false) {
			var retVal = PortalTab_GetPortletKeys(KeyPortalTab, "Vizelia.FOL.BusinessEntities.Chart");

			if (includeDrawingCanvas) {
				if (retVal == null)
					retVal = new List<string>();
				var dds = PortalTab_GetPortletKeys(KeyPortalTab, "Vizelia.FOL.BusinessEntities.DrawingCanvas");
				foreach (var dd in dds) {
					var charts = DrawingCanvasChart_GetListByKeyDrawingCanvas(dd);
					retVal.AddRange(charts.Select(dcc => dcc.KeyChart));
				}
				retVal = retVal.Distinct().ToList();
			}
			return retVal;
		}

		/// <summary>
		/// Get the PortalTab total flex based on the columns type (Main,Header, Footer).
		/// </summary>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <param name="type">The type of column.</param>
		/// <returns></returns>
		public int PortalTab_GetFlex(string KeyPortalTab, PortalColumnType type) {
			var broker = Helper.CreateInstance<PortalTabBroker>();
			var tab = broker.GetItem(KeyPortalTab, "*");

			var retVal = 0;

			switch (type) {
				case PortalColumnType.Main:
					retVal = tab.TotalFlex;
					break;
				case PortalColumnType.Header:
					retVal = tab.HeaderTotalFlex;
					break;
				case PortalColumnType.Footer:
					retVal = tab.FooterTotalFlex;
					break;
			}

			return retVal;
		}

		/// <summary>
		/// Gets a specific item for the business entity PortalTab.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public PortalTab PortalTab_GetItem(string Key, params string[] fields) {
			PortalTabBroker broker = Helper.CreateInstance<PortalTabBroker>();
			if (fields == null) fields = new[] { "*" };
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a list of portaltab for a specific portal.
		/// </summary>
		/// <param name="KeyPortalWindow">The key of the portal window.</param>
		/// <returns></returns>
		public List<PortalTab> PortalTab_GetListByKeyPortalWindow(string KeyPortalWindow) {
			PortalTabBroker broker = Helper.CreateInstance<PortalTabBroker>();
			return broker.GetListByKeyPortalWindow(KeyPortalWindow);
		}

		/// <summary>
		/// Gets the list of Chart's Key from a specific PortalTab
		/// </summary>
		/// <param name="KeyPortalTab">The PortalTab key.</param>
		/// <param name="PortletType">Type of the portlet.</param>
		/// <returns></returns>
		private List<string> PortalTab_GetPortletKeys(string KeyPortalTab, string PortletType) {
			var dict = PortalTab_GetPortletKeys(KeyPortalTab);
			var entityType = Assembly.GetAssembly(typeof(BaseBusinessEntity)).GetType(PortletType);
			List<string> retVal = null;
			if (!dict.TryGetValue(entityType, out retVal))
				retVal = new List<string>();
			return retVal;
		}

		/// <summary>
		/// Gets the list of Portelts's Type and Keys from a specific PortalTab
		/// </summary>
		/// <param name="KeyPortalTab">The PortalTab key.</param>
		/// <returns></returns>
		private Dictionary<Type, List<string>> PortalTab_GetPortletKeys(string KeyPortalTab) {
			var columns = PortalColumn_GetListByKeyPortalTab(KeyPortalTab);
			var retVal = new Dictionary<Type, List<string>>();
			foreach (var c in columns) {
				var portlets = Portlet_GetListByKeyPortalColumn(c.KeyPortalColumn);
				foreach (var p in portlets) {
					var entityType = Assembly.GetAssembly(typeof(BaseBusinessEntity)).GetType(p.TypeEntity);
					retVal.AddOrMerge(entityType, p.KeyEntity);
				}
			}
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity PortalTab.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PortalTab> PortalTab_GetStore(PagingParameter paging, PagingLocation location) {
			PortalTabBroker broker = Helper.CreateInstance<PortalTabBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of portaltab for a specific portal.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalWindow">The key of the portal window.</param>
		/// <returns></returns>
		public JsonStore<PortalTab> PortalTab_GetStoreByKeyPortalWindow(PagingParameter paging, string KeyPortalWindow) {
			PortalTabBroker broker = Helper.CreateInstance<PortalTabBroker>();
			var retVal = broker.GetStoreByKeyPortalWindow(paging, KeyPortalWindow);
			//foreach (PortalTab portalTab in retVal.records)
			//	broker.PopulateList(portalTab, "VirtualFile");
			return retVal;
		}

		/// <summary>
		/// Returns the stream of the excel file from a portaltab.
		/// </summary>
		/// <param name="Key">The key of the portaltab.</param>
		/// <returns></returns>
		public StreamResult PortalTab_GetStreamExcel(string Key) {
			var retVal = PortalTab_GetStreamExport(Key, ExportType.Excel2007);
			if (retVal != null && retVal.Count > 0) {
				return retVal[0];
			}
			return null;
		}

		/// <summary>
		/// Exports the PortalTab as an Excel File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the Chart.</param>
		public void PortalTab_GetStreamExcelBegin(Guid operationId, string Key) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				var stream = PortalTab_GetStreamExcel(Key);
				var result =
					new LongRunningOperationResult(
						LongRunningOperationStatus.ResultAvailable, stream);
				return result;
			});
		}

		/// <summary>
		/// Returns the stream of the export file from a portaltab.
		/// </summary>
		/// <param name="Key">The key of the portaltab.</param>
		/// <param name="type">The type of file.</param>
		/// <returns></returns>
		private List<StreamResult> PortalTab_GetStreamExport(string Key, ExportType type) {
			PortalTabBroker broker = Helper.CreateInstance<PortalTabBroker>();
			PortalTab portalTab = broker.GetItem(Key, "*");
			portalTab.PortalWindowTitle = Helper.LocalizeInText(portalTab.PortalWindowTitle, "%");
			portalTab.Title = Helper.LocalizeInText(portalTab.Title, "%");
			List<Chart> charts = portalTab.GetCharts();

			List<StreamResult> retVal = Chart_GetStreamExport(charts, type, portalTab.FullTitle);
			return retVal;
		}

		/// <summary>
		/// Returns the stream of the image from a portaltab.
		/// </summary>
		/// <param name="key">The key of the portaltab.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="Creator">The creator.</param>
		/// <param name="KeyLocalisationCulture">The key localisation culture.</param>
		/// <param name="clearChartCache">if set to <c>true</c> [clear chart cache].</param>
		/// <returns></returns>
		public StreamResult PortalTab_GetStreamImage(string key, int width, int height, string Creator = null, string KeyLocalisationCulture = null,
			bool clearChartCache = false) {
			if (string.IsNullOrEmpty(Creator)) {
				var membershipBroker = Helper.CreateInstance<FOLMembershipUserBroker>();
				var currentUser = membershipBroker.User_GetByUserName(Helper.GetCurrentUserName());
				Creator = currentUser.ProviderUserKey.KeyUser.Substring(1);
			}
			else {
				Creator = int.Parse(Creator.Substring(1)).ToString(CultureInfo.InvariantCulture);
			}

			KeyLocalisationCulture = Helper.IsValidCulture(KeyLocalisationCulture) ? KeyLocalisationCulture : Thread.CurrentThread.CurrentUICulture.ToString();
			Chart_PreLoadAll(clearChartCache, KeyPortalTab: key);

			var url = string.Format("?xtype={0}&keyPropertyName={1}&keyPropertyValue={2}&impersonateUserKey={3}&applicationName={4}", "vizPortalTab",
				"KeyPortalTab", key, Creator, ContextHelper.ApplicationName);
			var retVal = ScreenshotService.GetScreenshot(url, true, width, height, ScreenshotType.PNG, MimeType.Png, KeyLocalisationCulture);
			return retVal;
		}

		/// <summary>
		/// Returns the stream of the excel file from a portaltab.
		/// </summary>
		/// <param name="Key">The key of the portaltab.</param>
		/// <returns></returns>
		public StreamResult PortalTab_GetStreamPdf(string Key) {
			var retVal = PortalTab_GetStreamExport(Key, ExportType.Pdf);
			if (retVal != null && retVal.Count > 0) {
				return retVal[0];
			}
			return null;
		}

		/// <summary>
		/// Exports the PortalTab as an Pdf File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the Chart.</param>
		public void PortalTab_GetStreamPdfBegin(Guid operationId, string Key) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				var stream = PortalTab_GetStreamPdf(Key);
				var result =
					new LongRunningOperationResult(
						LongRunningOperationStatus.ResultAvailable, stream);
				return result;
			});
		}

		/// <summary>
		/// Saves a crud store for the business entity PortalTab.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PortalTab> PortalTab_SaveStore(CrudStore<PortalTab> store) {
			PortalTabBroker broker = Helper.CreateInstance<PortalTabBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Verify if the PortalTab has at least one column, and creates it if it doesnt.
		/// </summary>
		/// <param name="Key">the PortalTab Key.</param>
		private void PortalTab_VerifyColumns(string Key) {
			var brokerWindow = Helper.CreateInstance<PortalWindowBroker>();
			var broker = Helper.CreateInstance<PortalTabBroker>();
			var brokerColumn = Helper.CreateInstance<PortalColumnBroker>();
			var tab = broker.GetItem(Key, "*");
			bool hasHeader = false;
			bool hasFooter = false;


			if (tab.Columns == null || tab.Columns.Count == 0) {
				var window = brokerWindow.GetItem(tab.KeyPortalWindow);
				var columns = PortalColumn_GenerateFromConfig(window.PortalTabColumnConfig, tab.KeyPortalTab);
				foreach (var column in columns) {
					brokerColumn.Save(column);
				}
			}
			else {
				foreach (var col in tab.Columns) {
					if (col.Type == PortalColumnType.Header)
						hasHeader = true;
					if (col.Type == PortalColumnType.Footer)
						hasFooter = true;
				}
			}

			if (tab.HeaderHeight.HasValue && tab.HeaderHeight.Value > 0 && !hasHeader) {
				var header = new PortalColumn { Flex = 1, Order = 1, KeyPortalTab = tab.KeyPortalTab, Type = PortalColumnType.Header };
				brokerColumn.Save(header);
			}

			if (tab.FooterHeight.HasValue && tab.FooterHeight.Value > 0 && !hasFooter) {
				var footer = new PortalColumn { Flex = 1, Order = 1, KeyPortalTab = tab.KeyPortalTab, Type = PortalColumnType.Footer };
				brokerColumn.Save(footer);
			}

		}

		/// <summary>
		/// Deletes an existing business entity PortalTemplate.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool PortalTemplate_Delete(PortalTemplate item) {
			PortalTemplateBroker broker = Helper.CreateInstance<PortalTemplateBroker>();
			PortalTemplate_DeleteAllExistingPortals(item.KeyPortalTemplate);
			return broker.Delete(item);
		}

		/// <summary>
		/// Deletes an existing business entity PortalTemplate by Key.
		/// </summary>
		/// <param name="KeyPortalTemplate">The key portal template.</param>
		/// <returns></returns>
		public bool PortalTemplate_Delete(string KeyPortalTemplate) {
			PortalTemplateBroker broker = Helper.CreateInstance<PortalTemplateBroker>();
			PortalTemplate_DeleteAllExistingPortals(KeyPortalTemplate);
			return broker.Delete(KeyPortalTemplate);
		}

		/// <summary>
		/// Deletes a list of existing business entity PortalTemplate.
		/// </summary>
		/// <param name="KeyPortalTemplates">The key portal templates.</param>
		/// <returns></returns>
		public bool PortalTemplate_Delete(string[] KeyPortalTemplates) {
			var retVal = true;


			if (KeyPortalTemplates != null) {
				foreach (var key in KeyPortalTemplates) {
					retVal = retVal && PortalTemplate_Delete(key);
				}
			}
			return retVal;
		}

		/// <summary>
		///  Delete a portaltemplate and the portalwindows to the associated users with long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
		public void PortalTemplate_DeleteBegin(Guid operationId, string[] Keys) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				PortalTemplate_Delete(Keys);
				var result = new LongRunningOperationResult(LongRunningOperationStatus.Completed);
				return result;
			});
		}

		/// <summary>
		/// Delete existing portals coming from a template for a specific user
		/// </summary>
		/// <param name="KeyUser">The key user.</param>
		/// <param name="KeyPortalTemplate">The key portal template.</param>
		private void PortalTemplate_DeleteExistingPortals(string KeyUser, string KeyPortalTemplate) {
			//we delete the existing Portal associated to this user and coming from the same template.
			var brokerPortal = Helper.CreateInstance<PortalWindowBroker>();
			List<PortalWindow> existingPortals = brokerPortal.GetListByKeyCreator(KeyUser);
		    DeleteExistingPortals(existingPortals.Where(p => p.KeyPortalTemplateOrigin == KeyPortalTemplate).ToList());
			WritePushProgressionToTrace(KeyPortalTemplate, KeyUser);
		}

        /// <summary>
        /// Deletes existing portals.
        /// </summary>
        /// <param name="Portals">The portals.</param>
        private void DeleteExistingPortals(IEnumerable<PortalWindow> Portals) {
            var brokerPortal = Helper.CreateInstance<PortalWindowBroker>();
            var originalFilter = FilterHelper.GetAll();
            foreach (var portal in Portals) {
                var fullPortal = portal;
                // if the portal window was fetched without its tabs columns and portlets.
                if (fullPortal.Tabs.Count < 1) {
                    fullPortal = brokerPortal.GetItem(portal.KeyPortalWindow, "Tabs/Columns/Portlets"); 
                }
					//in order to be able to delete the portalwindow of the specific user, we need to add it temporarly to the user.
                FilterHelper.AddFilter(new Filter(fullPortal.KeyPortalWindow), typeof(PortalWindow));
					//We do the same for all ISecurable portlets.
                var portlets = fullPortal.GetPortlets();
					foreach (var portlet in portlets) {
						var entityType = Assembly.GetAssembly(typeof(BaseBusinessEntity)).GetType(portlet.TypeEntity);
						if (Helper.IsSecurableType(entityType)) {
							FilterHelper.AddFilter(new Filter(portlet.KeyEntity), entityType);
						}
					}
                PortalWindow_Delete(fullPortal, true);
            }
            FilterHelper.ClearFilters();
            FilterHelper.AddFilters(originalFilter);
					try {
                SessionService.CreateOrSlideSession(SessionService.GetSessionID(), Helper.GetCurrentUserName(), true);
					}
					catch (SystemException e) {
						TracingService.Write(e);
					}
				}

		/// <summary>
		/// Delete all existing portals coming from a template
		/// </summary>
		/// <param name="KeyPortalTemplate">The key portal template.</param>
		private void PortalTemplate_DeleteAllExistingPortals(string KeyPortalTemplate) {
			var broker = Helper.CreateInstance<PortalTemplateBroker>();
			var template = broker.GetItem(KeyPortalTemplate, "*");

			foreach (var user in template.Users) {
				//we delete the existing Portal associated to this user and coming from the same template.
				PortalTemplate_DeleteExistingPortals(user.ProviderUserKey.KeyUser, KeyPortalTemplate);//
			}

		}

		/// <summary>
		/// Creates a new business entity PortalTemplate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <param name="users">The users.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public FormResponse PortalTemplate_FormCreate(PortalTemplate item, CrudStore<PortalWindow> portalwindows, CrudStore<FOLMembershipUser> users, CrudStore<Location> filterSpatial) {
			FormResponse response;

			PortalTemplateBroker broker = Helper.CreateInstance<PortalTemplateBroker>();
			var brokerUser = Helper.CreateInstance<FOLMembershipUserBroker>();
			PortalWindowBroker brokerPortalWindow = Helper.CreateInstance<PortalWindowBroker>();
			LocationBroker brokerLocation = Helper.CreateInstance<LocationBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						PortalTemplate portaltemplate = response.data as PortalTemplate;
						if (portaltemplate != null) {
							brokerUser.SaveFOLMembershipUserPortalTemplate(portaltemplate, users);
							brokerPortalWindow.SavePortalWindowPortalTemplate(portaltemplate, portalwindows);
							brokerLocation.SavePortalTemplateFilterSpatial(portaltemplate, filterSpatial);
						}
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity PortalTemplate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PortalTemplate_FormLoad(string Key) {
			PortalTemplateBroker broker = Helper.CreateInstance<PortalTemplateBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PortalTemplate and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="portalwindows">The portalwindows.</param>
		/// <param name="users">The users.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <returns></returns>
		public FormResponse PortalTemplate_FormUpdate(PortalTemplate item, CrudStore<PortalWindow> portalwindows, CrudStore<FOLMembershipUser> users, CrudStore<Location> filterSpatial) {
			FormResponse response;

			PortalTemplateBroker broker = Helper.CreateInstance<PortalTemplateBroker>();
			var brokerUser = Helper.CreateInstance<FOLMembershipUserBroker>();
			PortalWindowBroker brokerPortalWindow = Helper.CreateInstance<PortalWindowBroker>();
			LocationBroker brokerLocation = Helper.CreateInstance<LocationBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						PortalTemplate portaltemplate = response.data as PortalTemplate;
						if (portaltemplate != null) {
							brokerUser.SaveFOLMembershipUserPortalTemplate(portaltemplate, users);
							brokerPortalWindow.SavePortalWindowPortalTemplate(portaltemplate, portalwindows);
							brokerLocation.SavePortalTemplateFilterSpatial(portaltemplate, filterSpatial);

							//when we delete a user from a template, we want to remove the associated portals coming from the template.
							if (users != null && users.destroy != null && users.destroy.Count > 0 && string.IsNullOrEmpty(portaltemplate.KeyPortalTemplate) == false) {
								foreach (var user in users.destroy) {
									PortalTemplate_DeleteExistingPortals(user.ProviderUserKey.KeyUser, portaltemplate.KeyPortalTemplate);
								}
							}

						}
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PortalTemplate_FormUpdateBatch(string[] keys, PortalTemplate item) {
			PortalTemplateBroker broker = Helper.CreateInstance<PortalTemplateBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PortalTemplate. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<PortalTemplate> PortalTemplate_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			PortalTemplateBroker broker = Helper.CreateInstance<PortalTemplateBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity PortalTemplate.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public PortalTemplate PortalTemplate_GetItem(string Key, params string[] fields) {
			PortalTemplateBroker broker = Helper.CreateInstance<PortalTemplateBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PortalTemplate> PortalTemplate_GetStore(PagingParameter paging, PagingLocation location) {
			PortalTemplateBroker broker = Helper.CreateInstance<PortalTemplateBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets the PortalTemplate store by the job step.
		/// </summary>
		/// <param name="keyJob">The key job.</param>
		/// <param name="keyStep">The key step.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="location">The location.</param>
		/// <returns></returns>
		public JsonStore<PortalTemplate> PortalTemplate_GetStoreByJobStep(string keyJob, string keyStep, PagingParameter paging, PagingLocation location) {
			var coreBusinessLayer = Helper.Resolve<ICoreBusinessLayer>();
			JsonStore<PortalTemplate> children = coreBusinessLayer.GetChildEntitiesByParentJobStep<PortalTemplate>(keyJob, keyStep, paging);
			return children;
		}

		/// <summary>
		/// Push the modification made to the associated portalwindows to the associated users of the PortalTemplate.
		/// </summary>
		/// <param name="KeyPortalTemplates">The key portal templates array.</param>
		/// <param name="username">The username.</param>
        /// <param name="userKeys">The keys of users to filter.</param>
		/// <returns></returns>
		public bool PortalTemplate_PushUpdates(string[] KeyPortalTemplates, string username = null, string[] userKeys = null) {
		    if (KeyPortalTemplates == null || KeyPortalTemplates.Length == 0) {
		        return false;
		    }

		    if (string.IsNullOrEmpty(username)) {
		        username = Helper.GetCurrentUserName();
		    }

		    if (KeyPortalTemplates.Length > 1 && userKeys != null){
                throw new NotImplementedException();
		    }

		    var brokerLocation = Helper.CreateInstance<LocationBroker>();
            var totalPortalsToPushCount = 0; 
            int count = 0;
		    IEnumerable<PortalTemplate> templates = GetPortalTemplatesFromKeys(KeyPortalTemplates, out totalPortalsToPushCount, userKeys).ToList();
			// we still want to delete existing template portals from the users even though no portals associate with the portal template
            if (totalPortalsToPushCount == 0) {
				foreach (var template in templates) {
					foreach (var user in template.Users) {
						PortalTemplate_DeleteExistingPortals(user.ProviderUserKey.KeyUser, template.KeyPortalTemplate);
					}
				}
			}
		    if (totalPortalsToPushCount > 0) {
		        foreach (var template in templates) {
					#region Logging
					var progressTic = 1;
			        var totalPortalsPushed = 0;
			        var pushedToUsersCount = 0;
					var sbProgressStatus = new StringBuilder();
		            var sbOpeningStatus = new StringBuilder();
					sbOpeningStatus.Append("Portal list:\n");
                    foreach (var pw in template.PortalWindows) {
						sbOpeningStatus.Append(pw.Title);
						sbOpeningStatus.Append("\n");
		            }
					sbOpeningStatus.Append("User list:\n");
                    foreach (var usr in template.Users) {
						sbOpeningStatus.Append(usr.UserName);
						sbOpeningStatus.Append("\n");
                    }

					TracingService.Write(TraceEntrySeverity.Information, "Started Portal Template process. Portal count: " + template.PortalWindows.Count + 
						". User count: " + template.Users.Count + ".\n" + sbOpeningStatus, "PortalTemplate Push - " + template.Title, template.KeyPortalTemplate);
					#endregion
					List<Location> locations = brokerLocation.GetFilterSpatialStoreByKeyPortalTemplate(new PagingParameter(), template.KeyPortalTemplate).records;
		            foreach (var user in template.Users) {
		                var portalStatus = PushUpdatesToUser(username, user, template, locations, ref count);
                        var progress = count * 100 / totalPortalsToPushCount;
                        LongRunningOperationService.ReportProgress(progress);
						#region Logging
						sbProgressStatus.Append(user.UserName);
                        if (portalStatus["pushed"] != null && portalStatus["pushed"].Count != 0) {
	                        pushedToUsersCount++;
							sbProgressStatus.Append(": ");
	                        foreach (var pushedPortal in portalStatus["pushed"]) {
		                        totalPortalsPushed++;
								sbProgressStatus.Append(pushedPortal.Title);
								sbProgressStatus.Append(", ");
	                        }

							if (sbProgressStatus.ToString().Substring(sbProgressStatus.Length - 2, 2) == ", ") {
								sbProgressStatus = sbProgressStatus.Remove(sbProgressStatus.Length - 2, 2);
                            }
                        }
                        else {
							sbProgressStatus.Append(": none pushed");
                        }
						sbProgressStatus.Append("\n");

                        // each time when progress is larger than 20/40/60/80/100 write to trace
                        if (progress >= progressTic * 20) {
                            TracingService.Write(TraceEntrySeverity.Information,
											 "Portal Template progress is " + progress + "%. \n" + sbProgressStatus,
                                             "PortalTemplate Push - " + template.Title, template.KeyPortalTemplate);
                            progressTic += 1;
	                        sbProgressStatus.Clear();
						}
						#endregion
					}
					#region Logging
					TracingService.Write(TraceEntrySeverity.Information, "Ended Portal Template process. Total portals pushed: " + totalPortalsPushed +
						". Pushed to " + pushedToUsersCount + " user" + (pushedToUsersCount == 1 ? "" : "s") + ".", "PortalTemplate Push - " + template.Title, template.KeyPortalTemplate);
					#endregion
				}
		    }

		    return true;
		}

        /// <summary>
        /// Pushes the updates to user.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="user">The user.</param>
        /// <param name="template">The template.</param>
        /// <param name="locations">The locations.</param>
        /// <param name="count">The count.</param>
        /// <returns>Push Status</returns>
        private Dictionary<string, List<PortalWindow>> PushUpdatesToUser(string username, FOLMembershipUser user, PortalTemplate template, List<Location> locations, ref int count) {
            var brokerPortal = Helper.CreateInstance<PortalWindowBroker>();
            var sessionId = SessionService.GetSessionID();

	        var templatePortals = template.PortalWindows.OrderBy(p => p.DesktopShortcutIconY).ThenBy(p => p.DesktopShortcutIconX).ToList();
            var templatePortalsByKeyPortalWindow = templatePortals.ToDictionary(p => p.KeyPortalWindow, p => p);  
            var userExistingTemplatePortals = brokerPortal.GetListByKeyCreator(user.ProviderUserKey.KeyUser).Where(p => p.KeyPortalTemplateOrigin == template.KeyPortalTemplate).ToList();
            var userExistingTemplatePortalsByKeyPortalWindowOrigin = userExistingTemplatePortals.Where(p => p.KeyPortalWindowOrigin != null && p.KeyPortalWindowOrigin != "0").ToDictionary(p => p.KeyPortalWindowOrigin, p => p);

            // we want to delete as well all the user's portals from the current portal template that are no longer in the template.
            var portalsToDelete = userExistingTemplatePortals.Where(p => {
				if (p.KeyPortalWindowOrigin != null)
					return !templatePortalsByKeyPortalWindow.ContainsKey(p.KeyPortalWindowOrigin);
				else  // pre 2.11 did not have KeyPortalWindowOrigin. It deleted without regard to AuditTimeLastModified which had not been introduced yet
					return p.KeyPortalTemplateOrigin == template.KeyPortalTemplate;
			}).ToList();
            
            var portalsToCopy = new List<PortalWindow>();
            int portalXPosition = template.StartX ?? 0;
            int portalYPosition = template.StartY ?? 0;
	        foreach (var portal in templatePortals) {
                // if the pushee doesn't have the portal or if the pushee's portal modified date is different from the pusher's portal modified date.
                if (!userExistingTemplatePortalsByKeyPortalWindowOrigin.ContainsKey(portal.KeyPortalWindow) ||
                    userExistingTemplatePortalsByKeyPortalWindowOrigin[portal.KeyPortalWindow].AuditTimeLastModified != portal.AuditTimeLastModified) {

	                var portalFull = brokerPortal.GetItem(portal.KeyPortalWindow, "*");
                    if (userExistingTemplatePortalsByKeyPortalWindowOrigin.ContainsKey(portalFull.KeyPortalWindow)) {
                        portalsToDelete.Add(userExistingTemplatePortalsByKeyPortalWindowOrigin[portal.KeyPortalWindow]);
                    }
                    portalsToCopy.Add(portalFull);
                }
	            ++count;
                TryCreateOrSlideSession(sessionId, username);
	        }

            DeleteExistingPortals(portalsToDelete);
            TryCreateOrSlideSession(sessionId, username);
            CopyPortalsToUser(portalsToCopy, template, user.ProviderUserKey.KeyUser, ref portalXPosition, ref portalYPosition, locations);
            var portalStatus = new Dictionary<string, List<PortalWindow>>();
            portalStatus["all"] = templatePortals;
            portalStatus["pushed"] = portalsToCopy;
            portalStatus["notpushed"] = portalStatus["all"].Except(portalStatus["pushed"]).ToList();
            return portalStatus;
        }

        /// <summary>
        /// Copies the portals to user.
        /// </summary>
        /// <param name="portalsToCopy">The portals to copy.</param>
        /// <param name="template">The template.</param>
        /// <param name="keyUser">The key user.</param>
        /// <param name="portalXPosition">The portal x position.</param>
        /// <param name="portalYPosition">The portal y position.</param>
        /// <param name="locations">The locations.</param>
        /// <returns>List of portals</returns>
	    private List<PortalWindow> CopyPortalsToUser(IEnumerable<PortalWindow> portalsToCopy, PortalTemplate template, string keyUser, ref int portalXPosition, ref int portalYPosition, List<Location> locations) {
            var copiedPortals = new List<PortalWindow>();
	        foreach (var portal in portalsToCopy) {
                copiedPortals.Add(CopyPortal(portal, template, keyUser, ref portalXPosition, ref portalYPosition, locations));
	        }
            return copiedPortals;
	    }

	    /// <summary>
        /// Copies the portal.
        /// </summary>
        /// <param name="portalToCopy">The portal to copy.</param>
        /// <param name="template">The template.</param>
        /// <param name="KeyUser">The key user.</param>
        /// <param name="portalXPosition">The portal x position.</param>
        /// <param name="portalYPosition">The portal y position.</param>
        /// <param name="locations">The locations.</param>
        /// <returns>The copied portal</returns>
        private PortalWindow CopyPortal(PortalWindow portalToCopy, PortalTemplate template, string KeyUser, ref int portalXPosition, ref int portalYPosition, List<Location> locations) {
            var brokerPortal = Helper.CreateInstance<PortalWindowBroker>();
            portalToCopy.KeyPortalTemplateOrigin = template.KeyPortalTemplate;
            portalToCopy.KeyPortalWindowOrigin = portalToCopy.KeyPortalWindow;

            if (template.NumberOfColumns.HasValue) {
                SetPortalPosition(ref portalXPosition, ref portalYPosition, template.NumberOfColumns, portalToCopy);
            }
            // while creating new PortalWindow through copy, the orignal portal's AuditTimeLastModified will be modified before the creation.
            // Therefore we need to save the original DaTeTime before copying.
            DateTime? originalPortalDateTime = portalToCopy.AuditTimeLastModified;
            FormResponse response;
            var portalNew = brokerPortal.Copy(portalToCopy, out response, KeyUser, false);

            if (locations.Count > 0 || string.IsNullOrEmpty(template.TimeZoneId) == false) {
                foreach (var tab in portalNew.Tabs) {
                    var listCharts = new List<Chart>();
                    foreach (var portalColumn in tab.Columns) {
                        foreach (var portlet in portalColumn.Portlets) {
                            if (portlet.Entity is Chart) {
                                listCharts.Add(portlet.Entity as Chart);
                            }
                        }
                    }
                    PortalTab_ApplyTemplatePersonalization(tab.KeyPortalTab, locations, template.TimeZoneId, listCharts);
                }
            }
            brokerPortal.UpdateDateTimeModified(portalNew, originalPortalDateTime);
	        return portalNew;
        }

	    /// <summary>
	    /// Writes the push progression to trace.
	    /// </summary>
	    /// <param name="KeyPortalTemplate">The key portal template.</param>
	    /// <param name="KeyUser">The key user.</param>
	    private void WritePushProgressionToTrace(string KeyPortalTemplate, string KeyUser) {
	        var broker = Helper.CreateInstance<PortalTemplateBroker>();
	        var template = broker.GetItem(KeyPortalTemplate);
	        var brokerUser = Helper.CreateInstance<FOLMembershipUserBroker>();
	        var user = brokerUser.GetItem(KeyUser);

	        TracingService.Write(TraceEntrySeverity.Information,
	            "Existing template portal windows deleted for user : " + user.UserName,
	            "PortalTemplate Push - " + template.Title, template.KeyPortalTemplate);
	    }

        /// <summary>
        /// Sets the portal position.
        /// </summary>
        /// <param name="portalXPosition">The portal x position.</param>
        /// <param name="portalYPosition">The portal y position.</param>
        /// <param name="numberOfColumns">The number of columns.</param>
        /// <param name="portal">The portal.</param>
	    private void SetPortalPosition(ref int portalXPosition, ref int portalYPosition, int? numberOfColumns, PortalWindow portal) {
            if (portalXPosition > numberOfColumns - 1) {
                portalXPosition = 0;
                portalYPosition += 1;
            }
            portal.DesktopShortcutIconX = portalXPosition;
            portal.DesktopShortcutIconY = portalYPosition;

            ++portalXPosition;
	    }

	    /// <summary>
        /// Tries the create or slide session.
        /// </summary>
        /// <param name="sessionId">The session identifier.</param>
        /// <param name="username">The username.</param>
	    private void TryCreateOrSlideSession(string sessionId, string username) {
            try {
                SessionService.CreateOrSlideSession(sessionId, username, true);
            }
            catch (SystemException e) {
                TracingService.Write(e);
            }
        }

	    /// <summary>
        /// Gets the portal templates from keys.
        /// </summary>
        /// <param name="PortalTemplateKeys">The portal template keys.</param>
        /// <param name="totalCount">The total count.</param>
        /// <param name="userKeys">The keys of users to filter.</param>
        /// <returns></returns>
        private IEnumerable<PortalTemplate> GetPortalTemplatesFromKeys(IEnumerable<string> PortalTemplateKeys, out int totalCount, string[] userKeys = null) {
	        var retPortalTemplateList = new List<PortalTemplate>();
            var broker = Helper.CreateInstance<PortalTemplateBroker>();
	        totalCount = 0;
            foreach (var keyPortalTemplate in PortalTemplateKeys) {
                var template = broker.GetItem(keyPortalTemplate, "*");

                // filter users by user keys
                if (userKeys != null) {
                    var userKeysDictionary = userKeys.ToDictionary(x => x);
                    template.Users = template.Users.Where(x => userKeysDictionary.ContainsKey(x.KeyUser)).ToList();
                } 

                if (template.PortalWindows != null && template.Users != null)
                    totalCount += template.Users.Count * template.PortalWindows.Count;
                retPortalTemplateList.Add(template);
            }
	        return retPortalTemplateList;
	    }

	    /// <summary>
		///  Push the modification made to the associated portalwindows to the associated users of the PortalTemplaten with long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
        /// <param name="userKeys">The keys of users to filter.</param>
		public void PortalTemplate_PushUpdatesBegin(Guid operationId, string[] Keys, string[] userKeys = null) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				PortalTemplate_PushUpdates(Keys, userKeys: userKeys);
				var result = new LongRunningOperationResult(LongRunningOperationStatus.Completed);
				return result;
			});
		}

		/// <summary>
		/// Saves a crud store for the business entity PortalTemplate.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PortalTemplate> PortalTemplate_SaveStore(CrudStore<PortalTemplate> store) {
			PortalTemplateBroker broker = Helper.CreateInstance<PortalTemplateBroker>();
			if (store != null && store.destroy != null && store.destroy.Count > 0) {
				foreach (var portaltemplate in store.destroy) {
					PortalTemplate_DeleteAllExistingPortals(portaltemplate.KeyPortalTemplate);
				}
			}
			return broker.SaveStore(store);
		}

		/// <summary>
        /// Gets a list of new user keys by key portal template.
        /// </summary>
        /// <param name="keyPortalTemplate">The keyPortalTemplate.</param>
        /// <returns></returns>
        public List<string> FOLMembershipUser_GetNewUserKeysByKeyPortalTemplate(int keyPortalTemplate) {
            PortalTemplateBroker broker = Helper.CreateInstance<PortalTemplateBroker>();
            return broker.FOLMembershipUser_GetNewUserKeysByKeyPortalTemplate(keyPortalTemplate);
	    }

		/// <summary>
		/// Apply the filter Spatial to all the Charts contained  in all tabs of a portal window
		/// </summary>
		/// <param name="Key">The portaltab Key.</param>
		/// <param name="filterSpatial">The spatial filter.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_ApplySpatialFilter(string Key, List<BaseBusinessEntity> filterSpatial) {
			List<Location> newLocations = filterSpatial.ToLocation();
			return PortalWindow_ApplySpatialFilter(Key, newLocations);
		}

		/// <summary>
		/// Apply the filter Spatial to all the Charts contained in all tabs of a portal window.
		/// </summary>
		/// <param name="Key">The portaltab Key.</param>
		/// <param name="filterSpatial">The spatial filter.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_ApplySpatialFilter(string Key, List<Location> filterSpatial) {
			var formResponse = new FormResponse() { success = true, errors = new List<FormError>() };
			var tabs = PortalTab_GetListByKeyPortalWindow(Key);
			foreach (var tab in tabs) {
				var f = PortalTab_ApplyTemplatePersonalization(tab.KeyPortalTab, filterSpatial);
				if (f.success == false) {
					if (f.errors != null)
						formResponse.errors.AddRange(f.errors);
				}
			}
			return formResponse;
		}

		/// <summary>
		/// Copy existing business entity PortalWindow and it s content.
		/// </summary>
		/// <param name="keys">The list of keys of item to copy </param>
		/// <returns></returns>
		public FormResponse PortalWindow_Copy(string[] keys) {
			FormResponse response = new FormResponse { success = false };
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			foreach (string key in keys) {
				var item = broker.GetItem(key, "*");
				item.DesktopShortcutIconX = null;
				item.DesktopShortcutIconY = null;
				item.KeyPortalTemplateOrigin = null;
				item.Creator = null;
				broker.Copy(item, out response);
			}
			return response;
		}

		/// <summary>
		/// Deletes an existing business entity Portal.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="deleteContent">if set to <c>true</c> [delete content].</param>
		/// <returns></returns>
		public bool PortalWindow_Delete(PortalWindow item, bool deleteContent = false) {
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			if (deleteContent) {
				var portlets = item.GetPortlets();
				//we do this so we dont call the GetItem "*" if we dont need to.
				if (portlets.Count == 0) {
					item = broker.GetItem(item.KeyPortalWindow, "*");
					portlets = item.GetPortlets();
				}
				foreach (var portlet in portlets) {
					Portlet_Delete(portlet, true);
				}
			}
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Portal and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="portaltab">the portaltab crud store attached to this portal.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_FormCreate(PortalWindow item, CrudStore<PortalTab> portaltab) {
			FormResponse response;
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			PortalTabBroker brokerTab = Helper.CreateInstance<PortalTabBroker>();
			PortalColumnBroker brokerColumn = Helper.CreateInstance<PortalColumnBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					var window = (PortalWindow)response.data;
					try {
						if (portaltab.IsEmpty()) {
							var tab = new PortalTab {
								KeyPortalWindow = window.KeyPortalWindow,
								Order = 1,
								Title = window.Title
							};
							tab = brokerTab.Save(tab);
							//item.Tabs.Add(tab);
							var columns = PortalColumn_GenerateFromConfig(item.PortalTabColumnConfig, tab.KeyPortalTab);
							foreach (var column in columns) {
								brokerColumn.Save(column);
							}
						}
						else {
							brokerTab.SavePortalPortalTab(window, portaltab);
						}
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			broker.PopulateList((PortalWindow)response.data, "*");
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity Portal.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_FormLoad(string Key) {
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Portal and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="portaltab">the portaltab crud store attached to this portal.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_FormUpdate(PortalWindow item, CrudStore<PortalTab> portaltab) {
			FormResponse response;
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			PortalTabBroker brokerTab = Helper.CreateInstance<PortalTabBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						brokerTab.SavePortalPortalTab((PortalWindow)response.data, portaltab);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			broker.PopulateList((PortalWindow)response.data, "*");
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_FormUpdateBatch(string[] keys, PortalWindow item) {
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Portal. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<PortalWindow> PortalWindow_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets the list of Chart's Key from a specific PortalTab
		/// </summary>
		/// <param name="KeyPortalWindow">The key portal window.</param>
		/// <param name="includeDrawingCanvas">if set to <c>true</c> [include DrawingCanvas].</param>
		/// <returns></returns>
		private List<string> PortalWindow_GetChartKeys(string KeyPortalWindow, bool includeDrawingCanvas = false) {
			var retVal = PortalWindow_GetPortletKeys(KeyPortalWindow, "Vizelia.FOL.BusinessEntities.Chart");

			if (includeDrawingCanvas) {
				if (retVal == null)
					retVal = new List<string>();
				var dds = PortalWindow_GetPortletKeys(KeyPortalWindow, "Vizelia.FOL.BusinessEntities.DrawingCanvas");
				foreach (var dd in dds) {
					var charts = DrawingCanvasChart_GetListByKeyDrawingCanvas(dd);
					retVal.AddRange(charts.Select(dcc => dcc.KeyChart));
				}
				retVal = retVal.Distinct().ToList();
			}

			return retVal;
		}

		/// <summary>
		/// Gets the list of Chart's Key from a specific PortalWindow
		/// </summary>
		/// <param name="KeyPortalWindow">The key portal window.</param>
		/// <param name="PortletType">Type of the portlet.</param>
		/// <returns></returns>
		private List<string> PortalWindow_GetPortletKeys(string KeyPortalWindow, string PortletType) {
			var retVal = new List<string>();
			var tabs = PortalTab_GetListByKeyPortalWindow(KeyPortalWindow);
			foreach (var tab in tabs) {
				retVal.AddRange(PortalTab_GetPortletKeys(tab.KeyPortalTab, PortletType));
			}
			return retVal;
		}

		/// <summary>
		/// Gets the list of portlets's Keys and Types from a specific PortalWindow
		/// </summary>
		/// <param name="KeyPortalWindow">The key portal window.</param>
		/// <returns></returns>
		public Dictionary<Type, List<string>> PortalWindow_GetPortletKeys(string KeyPortalWindow) {
			Dictionary<Type, List<string>> retVal = new Dictionary<Type, List<string>>();
			var tabs = PortalTab_GetListByKeyPortalWindow(KeyPortalWindow);
			foreach (var tab in tabs) {
				retVal.Merge(PortalTab_GetPortletKeys(tab.KeyPortalTab));
			}
			return retVal;
		}

		/// <summary>
		/// Gets a specific item for the business entity Portal Window.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields"></param>
		/// <returns></returns>
		public PortalWindow PortalWindow_GetItem(string Key, params string[] fields) {
			if (fields == null || fields.Length == 0) {
				fields = new[] { "*" };
			}
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity Portal.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PortalWindow> PortalWindow_GetStore(PagingParameter paging, PagingLocation location) {
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity PortalWindow_ for a specific ChartScheduler.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChartScheduler">The key chart scheduler.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<PortalWindow> PortalWindow_GetStoreByKeyChartScheduler(PagingParameter paging, string KeyChartScheduler) {
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			var retVal = broker.GetStoreByKeyChartScheduler(paging, KeyChartScheduler);
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity PortalWindow for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalTemplate">The Key PortalTemplate.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<PortalWindow> PortalWindow_GetStoreByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate) {
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			var retVal = broker.GetStoreByKeyPortalTemplate(paging, KeyPortalTemplate);
			return retVal;
		}

		/// <summary>
		/// Returns the stream of the excel file from a portalWindow.
		/// </summary>
		/// <param name="Key">The key of the portalWindow.</param>
		/// <returns></returns>
		public StreamResult PortalWindow_GetStreamExcel(string Key) {
			var retVal = PortalWindow_GetStreamExport(Key, ExportType.Excel2007);
			if (retVal != null && retVal.Count > 0) {
				return retVal[0];
			}
			return null;
		}

		/// <summary>
		/// Exports the PortalWindow as an Excel File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the ¨PortalWindow.</param>
		public void PortalWindow_GetStreamExcelBegin(Guid operationId, string Key) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				var stream = PortalWindow_GetStreamExcel(Key);
				var result =
					new LongRunningOperationResult(
						LongRunningOperationStatus.ResultAvailable, stream);
				return result;
			});
		}

		/// <summary>
		/// Returns the stream of the export file from a portalwindow.
		/// </summary>
		/// <param name="Key">The key of the portalwindow.</param>
		/// <param name="type">The type of export.</param>
		/// <param name="clearChartCache">if set to <c>true</c> [clear chart cache].</param>
		/// <returns></returns>
		private List<StreamResult> PortalWindow_GetStreamExport(string Key, ExportType type, bool clearChartCache = false) {
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			PortalWindow portalWindow = broker.GetItem(Key, "*");
			var retVal = new List<StreamResult>();

			if (type == ExportType.Pdf && portalWindow.PrintHeight > 0 && portalWindow.PrintWidth > 0) {
				var imgs = portalWindow.Tabs.Select(
					tab => PortalTab_GetStreamImage(tab.KeyPortalTab, portalWindow.PrintWidth.Value, portalWindow.PrintHeight.Value, clearChartCache: clearChartCache)).ToList();
				retVal = new List<StreamResult>
							{
								ChartPdfService.RenderPdfImagesStream(imgs, portalWindow.PrintWidth.Value, portalWindow.PrintHeight.Value, portalWindow.Title)
							};
			}
			else {
				List<Chart> charts = portalWindow.GetCharts();
				portalWindow.Title = Helper.LocalizeInText(portalWindow.Title, "%");
				var chartstreams = Chart_GetStreamExport(charts, type, portalWindow.Title, clearChartCache);
				retVal.AddRange(chartstreams);
			}
			return retVal;
		}

		/// <summary>
		/// Returns the stream of the excel file from a portalWindow.
		/// </summary>
		/// <param name="Key">The key of the portalWindow.</param>
		/// <returns></returns>
		public StreamResult PortalWindow_GetStreamPdf(string Key) {
			var retVal = PortalWindow_GetStreamExport(Key, ExportType.Pdf);
			if (retVal != null && retVal.Count > 0) {
				return retVal[0];
			}
			return null;
		}

		/// <summary>
		/// Exports the PortalWindow as an Pdf File with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Key">The key of the ¨PortalWindow.</param>
		public void PortalWindow_GetStreamPdfBegin(Guid operationId, string Key) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				var stream = PortalWindow_GetStreamPdf(Key);
				var result = new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, stream);
				return result;
			});
		}

		/// <summary>
		/// Saves a crud store for the business entity Portal.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PortalWindow> PortalWindow_SaveStore(CrudStore<PortalWindow> store) {
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Update the PortalWindow desktop shortcut position.
		/// </summary>
		/// <param name="KeyPortalWindow">The PortalWindow key.</param>
		/// <param name="desktopShortcutIconX">The desktop shortcut icon X.</param>
		/// <param name="desktopShortcutIconY">The desktop shortcut icon Y.</param>
		/// <returns></returns>
		public FormResponse PortalWindow_UpdateDesktopShortcut(string KeyPortalWindow, int desktopShortcutIconX, int desktopShortcutIconY) {
			PortalWindowBroker broker = Helper.CreateInstance<PortalWindowBroker>();
			PortalWindow item = broker.GetItem(KeyPortalWindow);
		    FormResponse retVal;
            if (item.DesktopShortcutIconX != desktopShortcutIconX || item.DesktopShortcutIconY != desktopShortcutIconY) {
			item.DesktopShortcutIconX = desktopShortcutIconX;
			item.DesktopShortcutIconY = desktopShortcutIconY;
                retVal = broker.FormUpdate(item);
		    }
            else {
                retVal = new FormResponse {success = true};
            }

		    return retVal;
		}

		/// <summary>
		/// Copy an existing Portlet.
		/// </summary>
		/// <param name="keys">The item to copy.</param>
		/// <returns></returns>
		public FormResponse Portlet_Copy(string[] keys) {
			FormResponse response = new FormResponse { success = false };
			PortletBroker broker = Helper.CreateInstance<PortletBroker>();
			foreach (string key in keys) {
				var item = broker.GetItem(key, "*");
				item.Title = ChartHelper.Increment(item.Title);
				item.Order += 1;
				broker.Copy(item, out response);
			}
			return response;
		}

		/// <summary>
		/// Create a Portlet from a ClassificationItem (used in the drag drop of a ClassificationItem in a portal column).
		/// </summary>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <param name="KeyPortalColumn">The key portal column.</param>
		/// <param name="Order">The order.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="KeyChartToCopy"></param>
		/// <returns></returns>
		public Portlet Portlet_CreateFromClassification(string KeyClassificationItem, string KeyPortalColumn, int Order, string TimeZoneId, string KeyChartToCopy) {
			var broker = Helper.CreateInstance<PortletBroker>();
			var chart = Chart_CreateFromClassificationItem(KeyClassificationItem, TimeZoneId, KeyChartToCopy);


			var portlet = new Portlet {
				Collapsed = false,
				Entity = chart,
				Flex = 1,
				HeaderAndToolbarVisible = true,
				KeyEntity = chart.KeyChart,
				KeyPortalColumn = KeyPortalColumn,
				TypeEntity = chart.GetType().FullName,
				Order = Order,
				Title = chart.Title
			};

			portlet = broker.Save(portlet);
			broker.PopulateList(portlet, "*");

			return portlet;

		}

		/// <summary>
		/// Create a Portlet from a Location (used in the drag drop of a Location in a portal column).
		/// </summary>
		/// <param name="KeyLocation">The key location.</param>
		/// <param name="KeyPortalColumn">The key portal column.</param>
		/// <param name="Order">The order.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="KeyChartToCopy"></param>
		/// <returns></returns>
		public Portlet Portlet_CreateFromLocation(string KeyLocation, string KeyPortalColumn, int Order, string TimeZoneId, string KeyChartToCopy) {
			var broker = Helper.CreateInstance<PortletBroker>();
			var chart = Chart_CreateFromLocation(KeyLocation, TimeZoneId, KeyChartToCopy);


			var portlet = new Portlet {
				Collapsed = false,
				Entity = chart,
				Flex = 1,
				HeaderAndToolbarVisible = true,
				KeyEntity = chart.KeyChart,
				KeyPortalColumn = KeyPortalColumn,
				TypeEntity = chart.GetType().FullName,
				Order = Order,
				Title = chart.Title
			};

			portlet = broker.Save(portlet);
			broker.PopulateList(portlet, "*");

			return portlet;
		}

		/// <summary>
		/// Create a Portlet from a Meter (used in the drag drop of a meter in a portal column).
		/// </summary>
		/// <param name="KeyMeter">the Key Meter.</param>
		/// <param name="KeyPortalColumn">The key portal column.</param>
		/// <param name="Order">The order.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		/// <param name="KeyChartToCopy"></param>
		/// <returns></returns>
		public Portlet Portlet_CreateFromMeter(string KeyMeter, string KeyPortalColumn, int Order, string TimeZoneId, string KeyChartToCopy) {
			var broker = Helper.CreateInstance<PortletBroker>();
			List<Meter> meters;
			var chart = Chart_CreateFromMeter(KeyMeter, out meters, TimeZoneId, true, KeyChartToCopy);


			var portlet = new Portlet {
				Collapsed = false,
				Entity = chart,
				Flex = 1,
				HeaderAndToolbarVisible = true,
				KeyEntity = chart.KeyChart,
				KeyPortalColumn = KeyPortalColumn,
				TypeEntity = chart.GetType().FullName,
				Order = Order,
				Title = chart.Title
			};

			portlet = broker.Save(portlet);
			broker.PopulateList(portlet, "*");

			return portlet;

		}

		/// <summary>
		/// Deletes an existing business entity Portlet.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <param name="deleteContent">if set to <c>true</c> [delete content].</param>
		/// <returns></returns>
		public bool Portlet_Delete(Portlet item, bool deleteContent = false) {
			PortletBroker broker = Helper.CreateInstance<PortletBroker>();

			var retVal = false;

			if (deleteContent && !string.IsNullOrEmpty(item.KeyEntity) && !string.IsNullOrEmpty(item.TypeEntity)) {
				var entityType = Assembly.GetAssembly(typeof(BaseBusinessEntity)).GetType(item.TypeEntity);
				var brokerEntity = (IBusinessEntityBroker)BrokerFactory.CreateBroker(entityType);
				retVal = brokerEntity.Delete(item.KeyEntity);
			}

			retVal = broker.Delete(item) || retVal;

			return retVal;

		}

		/// <summary>
		/// Creates a new business entity Portlet and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Portlet_FormCreate(Portlet item) {
			var portletBroker = Helper.CreateInstance<PortletBroker>();

			if (string.IsNullOrEmpty(item.KeyEntity)) {
				Type entityType = item.GetEntityType();

				var brokerEntity = (IBusinessEntityBroker)BrokerFactory.CreateBroker(entityType);
				var entity = (BaseBusinessEntity)Activator.CreateInstance(entityType);
				((IPortlet)entity).SetTitle(item.Title);

				Chart chart = entity as Chart;
				if (chart != null)
					chart.TimeZoneId = "UTC"; // required field on Chart. "UTC" is chosen as a default. The user can fix this as they edit the chart

				FormResponse formResponse = null;
				entity = brokerEntity.Save(entity, out formResponse);
				if ((entity == null) || !formResponse.success)
					return formResponse;
				item.KeyEntity = entity.GetKey();

			}
			var response = portletBroker.FormCreate(item);
			if (response.success)
				portletBroker.PopulateList((Portlet)response.data, "*");
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity Portlet.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Portlet_FormLoad(string Key) {
			PortletBroker broker = Helper.CreateInstance<PortletBroker>();
			var retVal = broker.FormLoad(Key);
			return retVal;
		}

		/// <summary>
		/// Updates an existing business entity Portlet and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Portlet_FormUpdate(Portlet item) {
			PortletBroker broker = Helper.CreateInstance<PortletBroker>();
			FormResponse response = broker.FormUpdate(item);
			if (response.success)
				broker.PopulateList((Portlet)response.data, "*");
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Portlet_FormUpdateBatch(string[] keys, Portlet item) {
			PortletBroker broker = Helper.CreateInstance<PortletBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Portlet. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Portlet> Portlet_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			PortletBroker broker = Helper.CreateInstance<PortletBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity Portlet.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Portlet Portlet_GetItem(string Key, params string[] fields) {
			PortletBroker broker = Helper.CreateInstance<PortletBroker>();
			return broker.GetItem(Key, "*");
		}

		/// <summary>
		/// Gets a store of portalcolumn for a specific portal column.
		/// </summary>
		/// <param name="KeyPortalColumn">The key of the portal column.</param>
		/// <returns></returns>
		public List<Portlet> Portlet_GetListByKeyPortalColumn(string KeyPortalColumn) {
			PortletBroker broker = Helper.CreateInstance<PortletBroker>();
			return broker.GetListByKeyPortalColumn(KeyPortalColumn);
		}

		/// <summary>
		/// Gets a json store for the business entity Portlet.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Portlet> Portlet_GetStore(PagingParameter paging, PagingLocation location) {
			PortletBroker broker = Helper.CreateInstance<PortletBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of portlet for a specific entity.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyEntity">The key entity.</param>
		/// <param name="TypeEntity">The type entity.</param>
		/// <returns></returns>
		public JsonStore<Portlet> Portlet_GetStoreByKeyEntity(PagingParameter paging, string KeyEntity, string TypeEntity) {
			PortletBroker broker = Helper.CreateInstance<PortletBroker>();
			return broker.GetStoreByKeyEntity(paging, KeyEntity, TypeEntity);
		}

		/// <summary>
		/// Gets a store of portlet for a specific portal column.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyPortalColumn">The key of the portal column.</param>
		/// <returns></returns>
		public JsonStore<Portlet> Portlet_GetStoreByKeyPortalColumn(PagingParameter paging, string KeyPortalColumn) {
			PortletBroker broker = Helper.CreateInstance<PortletBroker>();
			return broker.GetStoreByKeyPortalColumn(paging, KeyPortalColumn);
		}

		/// <summary>
		/// Generates the json structure of the Energy available portlets.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <param name="entity">The entity.</param>
		/// <returns>
		/// a list of treenodes in json.
		/// </returns>
		public List<TreeNode> Portlet_GetTree(string Key, BaseBusinessEntity entity) {
			List<TreeNode> nodes = new List<TreeNode>();

			if (string.IsNullOrWhiteSpace(Key) || Key == "/") {
				var types = Helper.GetIPortletTypes();
				nodes.AddRange(types.Select(t => new TreeNode {
					Key = t.FullName,
					iconCls = Helper.GetAttributeValue<IconClsAttribute>(t),
					text = Langue.ResourceManager.GetString(Helper.GetAttributeValue<LocalizedTextAttribute>(t)),
					draggable = true,
					entity = (BaseBusinessEntity)Activator.CreateInstance(t),
					leaf = false,
				}));

				nodes = nodes.OrderBy(n => n.text).ToList();

				/*var meterRoot = ClassificationItem_GetMeterRoot();
				if (meterRoot != null) {
					nodes.Add(meterRoot.GetTree());
				}*/
			}
			else {
				//if (entity is ClassificationItem) {
				//	nodes.AddRange(m_CoreBusinessLayer.ClassificationItem_GetTree(Key, (ClassificationItem)entity, true, true, true));
				//}
				//else {
				var t = Assembly.GetAssembly(typeof(BaseBusinessEntity)).GetType(Key);
				if (t != null) {
					var broker = (IBusinessEntityBroker)BrokerFactory.CreateBroker(t);
					int total;
					List<BaseBusinessEntity> all = broker.GetAll(new PagingParameter {
						filters = new List<GridFilter> {
							new GridFilter {
								field = "IsFavorite",
								data = new GridData {
									type="boolean",
									value="true"
								}
							}
						}
					}, PagingLocation.Database, out total);
					if (t == typeof(FlipCard)) {
						foreach (var flipcard in all) {
							((FlipCardBroker)broker).PopulateList((FlipCard)flipcard, "*");
						}
					}

					nodes.AddRange(all.Select(portlets => ((ITreeNode)portlets).GetTree()));

					nodes = nodes.OrderBy(n => n.text).ToList();
				}
			}

			return nodes;
		}

		/// <summary>
		///Gets all the open on start up portets.
		/// </summary>
		/// <returns></returns>
		public List<BaseBusinessEntity> Portlet_GetAllOpenOnStartup() {
			var retVal = new List<BaseBusinessEntity>();

			var types = Helper.GetIPortletTypes();
			foreach (var t in types) {
				var broker = (IBusinessEntityBroker)BrokerFactory.CreateBroker(t);
				int total;
				var all = broker.GetAll(new PagingParameter {
					filters = new List<GridFilter> {
							new GridFilter {
								field = "OpenOnStartup",
								data = new GridData {
									type="boolean",
									value="true"
								}
							}
						}
				}, PagingLocation.Database, out total);
				if (t == typeof(FlipCard)) {
					var newList = all.Select(flipCard => broker.GetItem(flipCard.GetKey(), "*")).ToList();
					all.Clear();
					all.AddRange(newList);
				}
				retVal.AddRange(all);
			}
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity Portlet.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Portlet> Portlet_SaveStore(CrudStore<Portlet> store) {
			PortletBroker broker = Helper.CreateInstance<PortletBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Process all the meters that have an Endpoint and store the value if needed.
		/// </summary>
		public void PsetAttributeHistorical_EndpointProcess() {
			var broker = Helper.CreateInstance<PsetAttributeHistoricalEndpointBroker>();
			var brokerPset = Helper.CreateInstance<PsetAttributeHistoricalBroker>();
			var psets = broker.GetAll();

			foreach (var pset in from pset in psets
								 let last = brokerPset.GetLastByKeyObject(pset.KeyObject, pset.KeyPropertySingleValue)
								 where last == null ||
								 pset.EndpointFrequency.HasValue && CheckLastDataAgainstFrequency(last.AttributeAcquisitionDateTime, pset.EndpointFrequency.Value)
								 select pset) {
				PsetAttributeHistorical_SaveEndpointValue(pset);
			}
		}

		/// <summary>
		/// Save an endpoint value for the Pset Attribute historical.
		/// </summary>
		/// <param name="pset">The pset.</param>
		/// <returns></returns>
		private void PsetAttributeHistorical_SaveEndpointValue(PsetAttributeHistoricalEndpoint pset) {
			if (!string.IsNullOrEmpty(pset.KeyEndpoint) && !string.IsNullOrEmpty(pset.EndpointType)) {
				var list = DataAcquisitionEndpoint_Process(pset.EndpointType, pset.KeyEndpoint);
				foreach (var value in list) {
					if (!double.IsNaN(value.Value)) {
						var brokerPsetAttributeHistorical = Helper.CreateInstance<PsetAttributeHistoricalBroker>();

						var d = new PsetAttributeHistorical {
							AttributeAcquisitionDateTime = value.AcquisitionDateTime,
							KeyObject = pset.KeyObject,
							KeyPropertySingleValue = pset.KeyPropertySingleValue,
							AttributeValue = value.Value
						};
						brokerPsetAttributeHistorical.Save(d);
					}
				}
			}
		}

		/// <summary>
		/// Deletes an existing business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool PsetAttributeHistoricalEndpoint_Delete(PsetAttributeHistoricalEndpoint item) {
			PsetAttributeHistoricalEndpointBroker broker = Helper.CreateInstance<PsetAttributeHistoricalEndpointBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity PsetAttributeHistoricalEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistoricalEndpoint_FormCreate(PsetAttributeHistoricalEndpoint item) {
			PsetAttributeHistoricalEndpointBroker broker = Helper.CreateInstance<PsetAttributeHistoricalEndpointBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistoricalEndpoint_FormLoad(string Key) {
			PsetAttributeHistoricalEndpointBroker broker = Helper.CreateInstance<PsetAttributeHistoricalEndpointBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity PsetAttributeHistoricalEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistoricalEndpoint_FormUpdate(PsetAttributeHistoricalEndpoint item) {
			PsetAttributeHistoricalEndpointBroker broker = Helper.CreateInstance<PsetAttributeHistoricalEndpointBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse PsetAttributeHistoricalEndpoint_FormUpdateBatch(string[] keys, PsetAttributeHistoricalEndpoint item) {
			PsetAttributeHistoricalEndpointBroker broker = Helper.CreateInstance<PsetAttributeHistoricalEndpointBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity PsetAttributeHistoricalEndpoint. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<PsetAttributeHistoricalEndpoint> PsetAttributeHistoricalEndpoint_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			PsetAttributeHistoricalEndpointBroker broker = Helper.CreateInstance<PsetAttributeHistoricalEndpointBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public PsetAttributeHistoricalEndpoint PsetAttributeHistoricalEndpoint_GetItem(string Key, params string[] fields) {
			PsetAttributeHistoricalEndpointBroker broker = Helper.CreateInstance<PsetAttributeHistoricalEndpointBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		///  Get the  PsetAttributeHistoricalEndpoint for a specific Object.
		/// </summary>
		/// <param name="KeyObject">The key object.</param>
		/// <param name="KeyPropertySingleValue">The key property single value.</param>
		/// <returns></returns>
		public PsetAttributeHistoricalEndpoint PsetAttributeHistoricalEndpoint_GetItemByKeyObject(string KeyObject, string KeyPropertySingleValue) {
			PsetAttributeHistoricalEndpointBroker broker = Helper.CreateInstance<PsetAttributeHistoricalEndpointBroker>();
			return broker.GetItemByKeyObject(KeyObject, KeyPropertySingleValue);
		}

		/// <summary>
		/// Gets a json store for the business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<PsetAttributeHistoricalEndpoint> PsetAttributeHistoricalEndpoint_GetStore(PagingParameter paging, PagingLocation location) {
			PsetAttributeHistoricalEndpointBroker broker = Helper.CreateInstance<PsetAttributeHistoricalEndpointBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity PsetAttributeHistoricalEndpoint.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<PsetAttributeHistoricalEndpoint> PsetAttributeHistoricalEndpoint_SaveStore(CrudStore<PsetAttributeHistoricalEndpoint> store) {
			PsetAttributeHistoricalEndpointBroker broker = Helper.CreateInstance<PsetAttributeHistoricalEndpointBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool RESTDataAcquisitionContainer_Delete(RESTDataAcquisitionContainer item) {
			RESTDataAcquisitionContainerBroker broker = Helper.CreateInstance<RESTDataAcquisitionContainerBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity RESTDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionContainer_FormCreate(RESTDataAcquisitionContainer item) {
			RESTDataAcquisitionContainerBroker broker = Helper.CreateInstance<RESTDataAcquisitionContainerBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionContainer_FormLoad(string Key) {
			RESTDataAcquisitionContainerBroker broker = Helper.CreateInstance<RESTDataAcquisitionContainerBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity RESTDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionContainer_FormUpdate(RESTDataAcquisitionContainer item) {
			RESTDataAcquisitionContainerBroker broker = Helper.CreateInstance<RESTDataAcquisitionContainerBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionContainer_FormUpdateBatch(string[] keys, RESTDataAcquisitionContainer item) {
			RESTDataAcquisitionContainerBroker broker = Helper.CreateInstance<RESTDataAcquisitionContainerBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity RESTDataAcquisitionContainer. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<RESTDataAcquisitionContainer> RESTDataAcquisitionContainer_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			RESTDataAcquisitionContainerBroker broker = Helper.CreateInstance<RESTDataAcquisitionContainerBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public RESTDataAcquisitionContainer RESTDataAcquisitionContainer_GetItem(string Key, params string[] fields) {
			RESTDataAcquisitionContainerBroker broker = Helper.CreateInstance<RESTDataAcquisitionContainerBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<RESTDataAcquisitionContainer> RESTDataAcquisitionContainer_GetStore(PagingParameter paging, PagingLocation location) {
			RESTDataAcquisitionContainerBroker broker = Helper.CreateInstance<RESTDataAcquisitionContainerBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity RESTDataAcquisitionContainer.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<RESTDataAcquisitionContainer> RESTDataAcquisitionContainer_SaveStore(CrudStore<RESTDataAcquisitionContainer> store) {
			RESTDataAcquisitionContainerBroker broker = Helper.CreateInstance<RESTDataAcquisitionContainerBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool RESTDataAcquisitionEndpoint_Delete(RESTDataAcquisitionEndpoint item) {
			RESTDataAcquisitionEndpointBroker broker = Helper.CreateInstance<RESTDataAcquisitionEndpointBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity RESTDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionEndpoint_FormCreate(RESTDataAcquisitionEndpoint item) {
			RESTDataAcquisitionEndpointBroker broker = Helper.CreateInstance<RESTDataAcquisitionEndpointBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionEndpoint_FormLoad(string Key) {
			RESTDataAcquisitionEndpointBroker broker = Helper.CreateInstance<RESTDataAcquisitionEndpointBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity RESTDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionEndpoint_FormUpdate(RESTDataAcquisitionEndpoint item) {
			RESTDataAcquisitionEndpointBroker broker = Helper.CreateInstance<RESTDataAcquisitionEndpointBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse RESTDataAcquisitionEndpoint_FormUpdateBatch(string[] keys, RESTDataAcquisitionEndpoint item) {
			RESTDataAcquisitionEndpointBroker broker = Helper.CreateInstance<RESTDataAcquisitionEndpointBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity RESTDataAcquisitionEndpoint. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<RESTDataAcquisitionEndpoint> RESTDataAcquisitionEndpoint_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			RESTDataAcquisitionEndpointBroker broker = Helper.CreateInstance<RESTDataAcquisitionEndpointBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public RESTDataAcquisitionEndpoint RESTDataAcquisitionEndpoint_GetItem(string Key, params string[] fields) {
			RESTDataAcquisitionEndpointBroker broker = Helper.CreateInstance<RESTDataAcquisitionEndpointBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<RESTDataAcquisitionEndpoint> RESTDataAcquisitionEndpoint_GetStore(PagingParameter paging, PagingLocation location) {
			RESTDataAcquisitionEndpointBroker broker = Helper.CreateInstance<RESTDataAcquisitionEndpointBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of  RESTDataAcquisitionEndpoint by key of the container.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		public JsonStore<RESTDataAcquisitionEndpoint> RESTDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer(PagingParameter paging, string KeyDataAcquisitionContainer) {
			var broker = Helper.CreateInstance<RESTDataAcquisitionEndpointBroker>();
			var brokerContainer = Helper.CreateInstance<RESTDataAcquisitionContainerBroker>();
			var retVal = broker.GetStoreByKeyDataAcquisitionContainer(paging, KeyDataAcquisitionContainer);
			var container = brokerContainer.GetItem(KeyDataAcquisitionContainer);

			foreach (var endpoint in retVal.records) {
				try {
					var dav = DataAcquisitionService.GetCurrentValue(container, endpoint);
					if (dav.Count > 0) {
						var last = dav.OrderBy(ep => ep.AcquisitionDateTime).Last();
						endpoint.Value = last.Value;
						endpoint.ValueString = endpoint.Value.ToString(CultureInfo.InvariantCulture);
						if (string.IsNullOrEmpty(endpoint.Unit))
							endpoint.Unit = last.Unit;
					}
				}
				catch (Exception exception) {
					endpoint.ValueString = exception.Message;
					using (TracingService.StartTracing("RESTDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer", endpoint.KeyDataAcquisitionEndpoint)) {
						TracingService.Write(exception);
					}
				}
			}
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity RESTDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<RESTDataAcquisitionEndpoint> RESTDataAcquisitionEndpoint_SaveStore(CrudStore<RESTDataAcquisitionEndpoint> store) {
			RESTDataAcquisitionEndpointBroker broker = Helper.CreateInstance<RESTDataAcquisitionEndpointBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Return a store of available REST DataAcquisition Provider.
		/// </summary>
		/// <returns></returns>
		public JsonStore<ListElement> RESTDataAcquisitionProvider_GetStore() {
			var types = DataAcquisitionService.GetRESTDataAcquisitionProviders();
			var retVal = new List<ListElement>();
			foreach (var type in types) {
				retVal.Add(new ListElement {
					Id = type.Name,
					Value = Helper.GetAssemblyQualifiedNameNoVersion(type),
					MsgCode = Helper.GetLocalizedText(type)
				});
			}

			return retVal.ToJsonStore();
		}

		/// <summary>
		/// Deletes an existing business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool StruxureWareDataAcquisitionContainer_Delete(StruxureWareDataAcquisitionContainer item) {
			StruxureWareDataAcquisitionContainerBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionContainerBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity StruxureWareDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionContainer_FormCreate(StruxureWareDataAcquisitionContainer item) {
			StruxureWareDataAcquisitionContainerBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionContainerBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionContainer_FormLoad(string Key) {
			StruxureWareDataAcquisitionContainerBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionContainerBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity StruxureWareDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionContainer_FormUpdate(StruxureWareDataAcquisitionContainer item) {
			StruxureWareDataAcquisitionContainerBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionContainerBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionContainer_FormUpdateBatch(string[] keys, StruxureWareDataAcquisitionContainer item) {
			StruxureWareDataAcquisitionContainerBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionContainerBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity StruxureWareDataAcquisitionContainer. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<StruxureWareDataAcquisitionContainer> StruxureWareDataAcquisitionContainer_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			StruxureWareDataAcquisitionContainerBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionContainerBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public StruxureWareDataAcquisitionContainer StruxureWareDataAcquisitionContainer_GetItem(string Key, params string[] fields) {
			StruxureWareDataAcquisitionContainerBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionContainerBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<StruxureWareDataAcquisitionContainer> StruxureWareDataAcquisitionContainer_GetStore(PagingParameter paging, PagingLocation location) {
			StruxureWareDataAcquisitionContainerBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionContainerBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity StruxureWareDataAcquisitionContainer.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<StruxureWareDataAcquisitionContainer> StruxureWareDataAcquisitionContainer_SaveStore(CrudStore<StruxureWareDataAcquisitionContainer> store) {
			StruxureWareDataAcquisitionContainerBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionContainerBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool StruxureWareDataAcquisitionEndpoint_Delete(StruxureWareDataAcquisitionEndpoint item) {
			StruxureWareDataAcquisitionEndpointBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionEndpointBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity StruxureWareDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionEndpoint_FormCreate(StruxureWareDataAcquisitionEndpoint item) {
			StruxureWareDataAcquisitionEndpointBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionEndpointBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionEndpoint_FormLoad(string Key) {
			StruxureWareDataAcquisitionEndpointBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionEndpointBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity StruxureWareDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionEndpoint_FormUpdate(StruxureWareDataAcquisitionEndpoint item) {
			StruxureWareDataAcquisitionEndpointBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionEndpointBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse StruxureWareDataAcquisitionEndpoint_FormUpdateBatch(string[] keys, StruxureWareDataAcquisitionEndpoint item) {
			StruxureWareDataAcquisitionEndpointBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionEndpointBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity StruxureWareDataAcquisitionEndpoint. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<StruxureWareDataAcquisitionEndpoint> StruxureWareDataAcquisitionEndpoint_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			StruxureWareDataAcquisitionEndpointBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionEndpointBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public StruxureWareDataAcquisitionEndpoint StruxureWareDataAcquisitionEndpoint_GetItem(string Key, params string[] fields) {
			StruxureWareDataAcquisitionEndpointBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionEndpointBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<StruxureWareDataAcquisitionEndpoint> StruxureWareDataAcquisitionEndpoint_GetStore(PagingParameter paging, PagingLocation location) {
			StruxureWareDataAcquisitionEndpointBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionEndpointBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of  StruxureWareDataAcquisitionEndpoint by key of the container.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		public JsonStore<StruxureWareDataAcquisitionEndpoint> StruxureWareDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer(PagingParameter paging, string KeyDataAcquisitionContainer) {
			return GetStoreByKeyDataAcquisitionContainer<StruxureWareDataAcquisitionEndpoint, StruxureWareDataAcquisitionContainer>(paging, KeyDataAcquisitionContainer);
		}

		/// <summary>
		/// Gets the store of the Data Acquisition Endpoint by key data acquisition container.
		/// </summary>
		/// <typeparam name="TEndpoint">The type of the endpoint.</typeparam>
		/// <typeparam name="TContainer">The type of the container.</typeparam>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		private static JsonStore<TEndpoint> GetStoreByKeyDataAcquisitionContainer<TEndpoint, TContainer>(PagingParameter paging, string KeyDataAcquisitionContainer)
			where TEndpoint : DataAcquisitionEndpoint, new()
			where TContainer : DataAcquisitionContainer {

			var broker = (IDataAcquisitionEndpointBroker<TEndpoint>)BrokerFactory.CreateBroker(typeof(TEndpoint));
			var retVal = broker.GetStoreByKeyDataAcquisitionContainer(paging, KeyDataAcquisitionContainer);
			var brokerContainer = (IBroker<TContainer>)BrokerFactory.CreateBroker(typeof(TContainer));
			var container = brokerContainer.GetItem(KeyDataAcquisitionContainer);

			foreach (var endpoint in retVal.records) {
				try {
					var dav = DataAcquisitionService.GetCurrentValue(container, endpoint);
					if (dav.Count > 0) {
						var last = dav.OrderBy(ep => ep.AcquisitionDateTime).Last();
						endpoint.Value = last.Value;
						endpoint.ValueString = endpoint.Value.ToString(CultureInfo.InvariantCulture);
						if (string.IsNullOrEmpty(endpoint.Unit))
							endpoint.Unit = last.Unit;
					}
				}
				catch (Exception exception) {
					endpoint.ValueString = exception.Message;
					using (TracingService.StartTracing("GetStoreByKeyDataAcquisitionContainer", endpoint.KeyDataAcquisitionEndpoint)) {
						TracingService.Write(exception);
					}

				}
			}

			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity StruxureWareDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<StruxureWareDataAcquisitionEndpoint> StruxureWareDataAcquisitionEndpoint_SaveStore(CrudStore<StruxureWareDataAcquisitionEndpoint> store) {
			StruxureWareDataAcquisitionEndpointBroker broker = Helper.CreateInstance<StruxureWareDataAcquisitionEndpointBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a json store for the business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<EWSDataAcquisitionContainer> EWSDataAcquisitionContainer_GetStore(PagingParameter paging, PagingLocation location) {
			EWSDataAcquisitionContainerBroker broker = Helper.CreateInstance<EWSDataAcquisitionContainerBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<EWSDataAcquisitionContainer> EWSDataAcquisitionContainer_SaveStore(CrudStore<EWSDataAcquisitionContainer> store) {
			EWSDataAcquisitionContainerBroker broker = Helper.CreateInstance<EWSDataAcquisitionContainerBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity EWSDataAcquisitionContainer. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<EWSDataAcquisitionContainer> EWSDataAcquisitionContainer_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			EWSDataAcquisitionContainerBroker broker = Helper.CreateInstance<EWSDataAcquisitionContainerBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public EWSDataAcquisitionContainer EWSDataAcquisitionContainer_GetItem(string Key, params string[] fields) {
			EWSDataAcquisitionContainerBroker broker = Helper.CreateInstance<EWSDataAcquisitionContainerBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Loads a specific item for the business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionContainer_FormLoad(string Key) {
			EWSDataAcquisitionContainerBroker broker = Helper.CreateInstance<EWSDataAcquisitionContainerBroker>();
			return broker.FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity EWSDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionContainer_FormCreate(EWSDataAcquisitionContainer item) {
			EWSDataAcquisitionContainerBroker broker = Helper.CreateInstance<EWSDataAcquisitionContainerBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity EWSDataAcquisitionContainer and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionContainer_FormUpdate(EWSDataAcquisitionContainer item) {
			EWSDataAcquisitionContainerBroker broker = Helper.CreateInstance<EWSDataAcquisitionContainerBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionContainer_FormUpdateBatch(string[] keys, EWSDataAcquisitionContainer item) {
			EWSDataAcquisitionContainerBroker broker = Helper.CreateInstance<EWSDataAcquisitionContainerBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity EWSDataAcquisitionContainer.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool EWSDataAcquisitionContainer_Delete(EWSDataAcquisitionContainer item) {
			EWSDataAcquisitionContainerBroker broker = Helper.CreateInstance<EWSDataAcquisitionContainerBroker>();
			return broker.Delete(item);
		}


		/// <summary>
		/// Gets a json store for the business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<EWSDataAcquisitionEndpoint> EWSDataAcquisitionEndpoint_GetStore(PagingParameter paging, PagingLocation location) {
			EWSDataAcquisitionEndpointBroker broker = Helper.CreateInstance<EWSDataAcquisitionEndpointBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<EWSDataAcquisitionEndpoint> EWSDataAcquisitionEndpoint_SaveStore(CrudStore<EWSDataAcquisitionEndpoint> store) {
			EWSDataAcquisitionEndpointBroker broker = Helper.CreateInstance<EWSDataAcquisitionEndpointBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a store of  EWSDataAcquisitionEndpoint by key of the container.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyDataAcquisitionContainer">The key data acquisition container.</param>
		/// <returns></returns>
		public JsonStore<EWSDataAcquisitionEndpoint> EWSDataAcquisitionEndpoint_GetStoreByKeyDataAcquisitionContainer(PagingParameter paging, string KeyDataAcquisitionContainer) {
			return GetStoreByKeyDataAcquisitionContainer<EWSDataAcquisitionEndpoint, EWSDataAcquisitionContainer>(paging, KeyDataAcquisitionContainer);
		}

		/// <summary>
		/// Gets a list for the business entity EWSDataAcquisitionEndpoint. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<EWSDataAcquisitionEndpoint> EWSDataAcquisitionEndpoint_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			EWSDataAcquisitionEndpointBroker broker = Helper.CreateInstance<EWSDataAcquisitionEndpointBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public EWSDataAcquisitionEndpoint EWSDataAcquisitionEndpoint_GetItem(string Key, params string[] fields) {
			EWSDataAcquisitionEndpointBroker broker = Helper.CreateInstance<EWSDataAcquisitionEndpointBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Loads a specific item for the business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionEndpoint_FormLoad(string Key) {
			EWSDataAcquisitionEndpointBroker broker = Helper.CreateInstance<EWSDataAcquisitionEndpointBroker>();
			return broker.FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity EWSDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionEndpoint_FormCreate(EWSDataAcquisitionEndpoint item) {
			EWSDataAcquisitionEndpointBroker broker = Helper.CreateInstance<EWSDataAcquisitionEndpointBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity EWSDataAcquisitionEndpoint and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionEndpoint_FormUpdate(EWSDataAcquisitionEndpoint item) {
			EWSDataAcquisitionEndpointBroker broker = Helper.CreateInstance<EWSDataAcquisitionEndpointBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse EWSDataAcquisitionEndpoint_FormUpdateBatch(string[] keys, EWSDataAcquisitionEndpoint item) {
			EWSDataAcquisitionEndpointBroker broker = Helper.CreateInstance<EWSDataAcquisitionEndpointBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity EWSDataAcquisitionEndpoint.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool EWSDataAcquisitionEndpoint_Delete(EWSDataAcquisitionEndpoint item) {
			EWSDataAcquisitionEndpointBroker broker = Helper.CreateInstance<EWSDataAcquisitionEndpointBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Deletes an existing business entity VirtualFile.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool VirtualFile_Delete(VirtualFile item) {
			VirtualFileBroker broker = Helper.CreateInstance<VirtualFileBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity VirtualFile and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse VirtualFile_FormCreate(VirtualFile item) {
			VirtualFileBroker broker = Helper.CreateInstance<VirtualFileBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity VirtualFile.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse VirtualFile_FormLoad(string Key) {
			VirtualFileBroker broker = Helper.CreateInstance<VirtualFileBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity VirtualFile and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse VirtualFile_FormUpdate(VirtualFile item) {
			VirtualFileBroker broker = Helper.CreateInstance<VirtualFileBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse VirtualFile_FormUpdateBatch(string[] keys, VirtualFile item) {
			VirtualFileBroker broker = Helper.CreateInstance<VirtualFileBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity VirtualFile. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<VirtualFile> VirtualFile_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			VirtualFileBroker broker = Helper.CreateInstance<VirtualFileBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity VirtualFile.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public VirtualFile VirtualFile_GetItem(string Key, params string[] fields) {
			VirtualFileBroker broker = Helper.CreateInstance<VirtualFileBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		///Check if a VirtualFile with this path exists.
		/// </summary>
		/// <param name="path">the path.</param>
		/// <returns></returns>
		public VirtualFile VirtualFile_GetItemByPath(string path) {
			VirtualFileBroker broker = Helper.CreateInstance<VirtualFileBroker>();
			return broker.GetItemByPath(path);
		}

		/// <summary>
		/// Gets a json store for the business entity VirtualFile.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<VirtualFile> VirtualFile_GetStore(PagingParameter paging, PagingLocation location) {
			VirtualFileBroker broker = Helper.CreateInstance<VirtualFileBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Return a stream from a VirtualFile Path.
		/// </summary>
		/// <param name="direct">the path.</param>
		/// <returns></returns>
		public StreamResult VirtualFile_GetStreamImage(string direct) {

			StreamResult retVal = null;
			VirtualFile virtualfile = VirtualFile_GetItemByPath(direct);
			string htmlContent = String.Format("<html><title></title></head><body><h1>{0}</h1></body></html>", Langue.error_msg_invalidpath);

			if (virtualfile != null) {
				switch (virtualfile.EntityType) {

					case "Vizelia.FOL.BusinessEntities.Chart":
						Chart_ClearCache(virtualfile.KeyEntity);
						retVal = Chart_GetStreamImage(virtualfile.KeyEntity, virtualfile.Width, virtualfile.Height);
						break;
					case "Vizelia.FOL.BusinessEntities.PortalTab":
						retVal = PortalTab_GetStreamImage(virtualfile.KeyEntity, virtualfile.Width, virtualfile.Height, virtualfile.Creator);
						break;
					case "Vizelia.FOL.BusinessEntities.Playlist":
						htmlContent = Playlist_GetSlideshow(virtualfile.KeyEntity, virtualfile.Width, virtualfile.Height);
						break;

					case "Vizelia.FOL.BusinessEntities.Meter":
						var store = MeterData_GetStoreFromMeter(virtualfile.KeyEntity, new PagingParameter { dir = "DESC", sort = "AcquisitionDateTime", start = 0, limit = virtualfile.MaxRecordCount }, PagingLocation.Database, "UTC", false, "dd/MM/yy HH:mm:ss");
						if (store.recordCount > 0)
							htmlContent = MappingService.Providers["CsvSerialization"].Export(store.records.OfType<object>().ToList());
						else {
							htmlContent = string.Empty;
						}
						break;
				}
			}
			return retVal ?? (StreamResultHelper.GetStreamFromHtml(htmlContent));
		}

		/// <summary>
		/// Saves a crud store for the business entity VirtualFile.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<VirtualFile> VirtualFile_SaveStore(CrudStore<VirtualFile> store) {
			VirtualFileBroker broker = Helper.CreateInstance<VirtualFileBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity WeatherLocation.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool WeatherLocation_Delete(WeatherLocation item) {
			WeatherLocationBroker broker = Helper.CreateInstance<WeatherLocationBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity WeatherLocation and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse WeatherLocation_FormCreate(WeatherLocation item) {
			WeatherLocationBroker broker = Helper.CreateInstance<WeatherLocationBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity WeatherLocation.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse WeatherLocation_FormLoad(string Key) {
			WeatherLocationBroker broker = Helper.CreateInstance<WeatherLocationBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity WeatherLocation and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse WeatherLocation_FormUpdate(WeatherLocation item) {
			WeatherLocationBroker broker = Helper.CreateInstance<WeatherLocationBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse WeatherLocation_FormUpdateBatch(string[] keys, WeatherLocation item) {
			WeatherLocationBroker broker = Helper.CreateInstance<WeatherLocationBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity WeatherLocation. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<WeatherLocation> WeatherLocation_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			WeatherLocationBroker broker = Helper.CreateInstance<WeatherLocationBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Returns the 5 day Forecast for this Weather Location.
		/// </summary>
		/// <param name="KeyWeatherLocation">The WeatherLocation key.</param>
		/// <returns></returns>
		public List<WeatherForecast> WeatherLocation_GetFiveDayForecast(string KeyWeatherLocation) {
			WeatherLocationBroker broker = Helper.CreateInstance<WeatherLocationBroker>();
			WeatherLocation location = broker.GetItem(KeyWeatherLocation);

			var retVal = WeatherService.GetWeatherFiveDayForecast(location);
			return retVal;
		}

		/// <summary>
		/// Gets a specific item for the business entity WeatherLocation.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public WeatherLocation WeatherLocation_GetItem(string Key, params string[] fields) {
			WeatherLocationBroker broker = Helper.CreateInstance<WeatherLocationBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity WeatherLocation.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<WeatherLocation> WeatherLocation_GetStore(PagingParameter paging, PagingLocation location) {
			WeatherLocationBroker broker = Helper.CreateInstance<WeatherLocationBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity WeatherLocation.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<WeatherLocation> WeatherLocation_SaveStore(CrudStore<WeatherLocation> store) {
			WeatherLocationBroker broker = Helper.CreateInstance<WeatherLocationBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a json store of WeatherLocation based on a specific search.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<WeatherLocation> WeatherLocationSearch_GetStore(PagingParameter paging) {
			if (paging != null) {
				var retVal = WeatherService.GetWeatherLocation(paging.query);
				paging.query = null;
				return retVal.ToJsonStoreWithFilter(paging);
			}
			return null;
		}

		/// <summary>
		/// Deletes an existing business entity WebFrame.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool WebFrame_Delete(WebFrame item) {
			WebFrameBroker broker = Helper.CreateInstance<WebFrameBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity WebFrame and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse WebFrame_FormCreate(WebFrame item) {
			WebFrameBroker broker = Helper.CreateInstance<WebFrameBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity WebFrame.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse WebFrame_FormLoad(string Key) {
			WebFrameBroker broker = Helper.CreateInstance<WebFrameBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity WebFrame and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse WebFrame_FormUpdate(WebFrame item) {
			WebFrameBroker broker = Helper.CreateInstance<WebFrameBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse WebFrame_FormUpdateBatch(string[] keys, WebFrame item) {
			WebFrameBroker broker = Helper.CreateInstance<WebFrameBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity WebFrame. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<WebFrame> WebFrame_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			WebFrameBroker broker = Helper.CreateInstance<WebFrameBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity WebFrame.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public WebFrame WebFrame_GetItem(string Key, params string[] fields) {
			WebFrameBroker broker = Helper.CreateInstance<WebFrameBroker>();
			var retVal = broker.GetItem(Key, fields);
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity WebFrame.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<WebFrame> WebFrame_GetStore(PagingParameter paging, PagingLocation location) {
			WebFrameBroker broker = Helper.CreateInstance<WebFrameBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity WebFrame.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<WebFrame> WebFrame_SaveStore(CrudStore<WebFrame> store) {
			WebFrameBroker broker = Helper.CreateInstance<WebFrameBroker>();
			return broker.SaveStore(store);
		}





		/// <summary>
		/// Gets a json store for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<MeterValidationRule> MeterValidationRule_GetStore(PagingParameter paging, PagingLocation location) {
			MeterValidationRuleBroker broker = Helper.CreateInstance<MeterValidationRuleBroker>();
			return broker.GetStore(paging, location);
		}


		/// <summary>
		/// Gets all the enabled MeterValidationRule and process them to generate the AlarmInstances with a long running operation.
		/// </summary>
		/// <param name="operationId">The id of the operation.</param>
		/// <param name="Keys">The keys.</param>
		public void MeterValidationRule_ProcessBegin(Guid operationId, string[] Keys) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				MeterValidationRule_Process(Keys);
				var result = new LongRunningOperationResult(LongRunningOperationStatus.Completed);
				return result;
			});
		}


		/// <summary>
		/// Gets all the enabled MeterValidationRule and process them to generate the AlarmInstances.
		/// </summary>
		/// <param name="Keys">The keys.</param>
		public void MeterValidationRule_Process(string[] Keys) {
			var currentCulture = Thread.CurrentThread.CurrentCulture;
			var currentUICulture = Thread.CurrentThread.CurrentUICulture;

			var broker = Helper.CreateInstance<MeterValidationRuleBroker>();
			var brokerInstance = Helper.CreateInstance<AlarmInstanceBroker>();
			var brokerMeterData = Helper.CreateInstance<MeterDataBroker>();

			try {
				#region Getting all enabled MeterValidationRule
				var rules = new Dictionary<string, MeterValidationRule>();
				if (Keys == null || Keys.Length == 0) {
					Keys = broker.GetAll().Select(mvr => mvr.KeyMeterValidationRule).ToArray();
				}
				if (Keys.Length > 0) {
					foreach (var rule in Keys.Select(key => broker.GetItem(key, "*")).Where(rule => rule != null && rule.Enabled)) {
						rules.Add(rule.KeyMeterValidationRule, rule);
					}
				}
				#endregion
				using (var energyAggregator = GetEnergyAggregatorServiceAgent()) {
					//We generate the alarm instances.
					var dictionary = energyAggregator.MeterValidationRule_Process(rules.Values.ToList());
					var batchEmails = new Dictionary<string, List<string>>();
					var count = 0;
					var totalCount = dictionary.Sum(kvp => kvp.Value != null ? kvp.Value.Count : 0);
					foreach (var kvp in dictionary) {
						var rule = kvp.Key;
						var alarmInstances = kvp.Value;
						foreach (var instance in alarmInstances) {
							#region Culture
							if (!string.IsNullOrEmpty(rule.KeyLocalizationCulture)) {
								Thread.CurrentThread.CurrentCulture = new CultureInfo(rule.KeyLocalizationCulture, false);
								Thread.CurrentThread.CurrentUICulture = new CultureInfo(rule.KeyLocalizationCulture, false);
							}
							#endregion
							//we save or update each instance.
							var savedInstance = brokerInstance.Save(instance);
							if (savedInstance != null) {
								#region batch email generation
								savedInstance.MeterValidationRule = rule;
								var description = AlarmHelper.GetAlarmInstanceBody(savedInstance);

								//if the instance is attach to a MeterData we mark it as invalid.
								if (!string.IsNullOrEmpty(savedInstance.KeyMeterData) && savedInstance.MeterValidationRuleAlarmType != MeterValidationRuleAlarmType.Validity) {
									var meterData = brokerMeterData.GetItem(savedInstance.KeyMeterData);
									if (meterData != null && meterData.Validity == MeterDataValidity.Valid) {
										meterData.Validity = MeterDataValidity.Invalid;
										brokerMeterData.Save(meterData);
									}
								}

								//We batch the emails in order to send only one per instance of MeterValidationRUle
								if (!string.IsNullOrEmpty(rule.Emails) && savedInstance.EmailSent != true) {
									var instancesDescription = new List<string>();
									if (batchEmails.ContainsKey(rule.KeyMeterValidationRule)) {
										instancesDescription = batchEmails[rule.KeyMeterValidationRule];
										batchEmails.Remove(rule.KeyMeterValidationRule);
									}
									instancesDescription.Add(description);
									batchEmails.Add(rule.KeyMeterValidationRule, instancesDescription);
									savedInstance.EmailSent = true;
									brokerInstance.Save(savedInstance);
								}
								#endregion
							}
							count += 1;
							LongRunningOperationService.ReportProgress(count * 100 / totalCount);
						}
						#region LastProcessDateTime
						rule.LastProcessDateTime = DateTime.UtcNow;
						broker.Save(rule);
						#endregion
					}
					#region emails sending
					foreach (var kv in batchEmails) {
						var description = "";
						var rule = rules[kv.Key];

						if (!string.IsNullOrEmpty(rule.KeyLocalizationCulture)) {
							Thread.CurrentThread.CurrentCulture = new CultureInfo(rule.KeyLocalizationCulture, false);
							Thread.CurrentThread.CurrentUICulture = new CultureInfo(rule.KeyLocalizationCulture, false);
						}

						var instancesDescription = kv.Value;

						instancesDescription.ForEach(desc => description += desc + Environment.NewLine);

						var mailBroker = Helper.CreateInstance<MailBroker>();
						var mails = mailBroker.GetListByClassificationItemLocalId(MailBroker.const_metervalidationrule);
						var modelDataMeterValidationRule = new ModelDataMeterValidationRule {
							MeterValidationRule = rule,
							Description = description,
							InstanceCount = instancesDescription.Count
						};
						var mailMessages = MailService.ConvertMailsToMailMessages(modelDataMeterValidationRule, mails);
						foreach (var mailMessage in mailMessages) {
							MailService.Send(mailMessage);
						}
					}
					#endregion
				}
			}
			finally {
				Thread.CurrentThread.CurrentCulture = currentCulture;
				Thread.CurrentThread.CurrentUICulture = currentUICulture;
			}
		}

		/// <summary>
		/// Validate a new MeterData (used in the MeterData form to validate a meter data when inputted).
		/// </summary>
		/// <param name="item">The Meter Data.</param>
		/// <returns></returns>
		public List<AlarmInstance> MeterData_Validate(MeterData item) {
			var retVal = new List<AlarmInstance>();
			var rules = new List<MeterValidationRule>();

			#region Load Applicable Rules
			var brokerMeterValidationRule = Helper.CreateInstance<MeterValidationRuleBroker>();
			var Keys = brokerMeterValidationRule.GetAll().Select(mvr => mvr.KeyMeterValidationRule).ToArray();

			if (Keys.Length > 0) {
				foreach (var rule in Keys.Select(key => brokerMeterValidationRule.GetItem(key, "*")).Where(rule => rule.Enabled && rule.Meters != null)) {
					rule.Meters = rule.Meters.Where(m => m.KeyMeter == item.KeyMeter).ToList();
					//we disable the last process date time, to be sure to run the rule for the new meter data.
					rule.LastProcessDateTime = null;
					if (rule.Meters.Count > 0) {
						rules.Add(rule);
					}
				}
			}
			#endregion

			if (rules.Count > 0) {
				//first we save the new item
				FormResponse response = string.IsNullOrEmpty(item.KeyMeterData) ? MeterData_FormCreate(item) : MeterData_FormUpdate(item);
				if (response.success) {
					item = response.data as MeterData;
					if (item != null) {
						using (var energyAggregator = GetEnergyAggregatorServiceAgent()) {
							var dictionary = energyAggregator.MeterValidationRule_Process(rules);
							foreach (var kvp in dictionary) {
								var rule = kvp.Key;
								var alarmInstances = kvp.Value;
								alarmInstances.ForEach(instance => {
									if (instance.KeyMeterData == item.KeyMeterData) {
										instance.MeterValidationRule = rule;
										instance.Description = AlarmHelper.GetAlarmInstanceBody(instance);
										retVal.Add(instance);
									}
								});
							}
						}
					}
				}
				//we delete the meter data
				MeterData_Delete(item);
			}
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<MeterValidationRule> MeterValidationRule_SaveStore(CrudStore<MeterValidationRule> store) {
			MeterValidationRuleBroker broker = Helper.CreateInstance<MeterValidationRuleBroker>();
			AlarmInstanceBroker instancebroker = Helper.CreateInstance<AlarmInstanceBroker>();

			var keys = store.destroy.Where(mvr => mvr.KeyMeterValidationRule != null).Select(mvr => mvr.KeyMeterValidationRule);
			var retVal = new JsonStore<MeterValidationRule>();

			// using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions {IsolationLevel = IsolationLevel.ReadCommitted})) {
			instancebroker.DeleteLimitedNumberRowsByKeySource(const_numberofrowstodelete, keys, AlarmInstanceSource.MeterValidationRule);
			retVal = broker.SaveStore(store);
			//scope.Complete();
			//}

			return retVal;
		}

		/// <summary>
		/// Begins long running operation to Save a crud store for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="operationId"></param>
		/// <param name="store">The crud store.</param>
		public void MeterValidationRule_BeginSaveStore(Guid operationId, CrudStore<MeterValidationRule> store) {
			LongRunningOperationService.Invoke(operationId, () => {
				LongRunningOperationService.ReportProgress();
				MeterValidationRule_SaveStore(store);
				return new LongRunningOperationResult(LongRunningOperationStatus.Completed);
			});
		}

		/// <summary>
		/// Gets a list for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<MeterValidationRule> MeterValidationRule_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			MeterValidationRuleBroker broker = Helper.CreateInstance<MeterValidationRuleBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public MeterValidationRule MeterValidationRule_GetItem(string Key, params string[] fields) {
			MeterValidationRuleBroker broker = Helper.CreateInstance<MeterValidationRuleBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Loads a specific item for the business entity MeterValidationRule.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse MeterValidationRule_FormLoad(string Key) {
			MeterValidationRuleBroker broker = Helper.CreateInstance<MeterValidationRuleBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Creates a new business entity MeterValidationRule and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse MeterValidationRule_FormCreate(MeterValidationRule item) {
			return MeterValidationRule_FormCreate(item, item.Meters.AsCrudStore(), item.FilterSpatial.AsCrudStore(), item.FilterMeterClassifications.AsCrudStore());
		}

		/// <summary>
		/// Creates a new business entity MeterValidationRule and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <returns></returns>
		public FormResponse MeterValidationRule_FormCreate(MeterValidationRule item, CrudStore<Meter> meters, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification) {

			FormResponse response;

			var broker = Helper.CreateInstance<MeterValidationRuleBroker>();
			var brokerMeter = Helper.CreateInstance<MeterBroker>();
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			var brokerClassification = Helper.CreateInstance<ClassificationItemBroker>();

			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						var rule = response.data as MeterValidationRule;
						if (rule != null) {
							brokerMeter.SaveMeterValidationRuleMeters(rule, meters);
							brokerLocation.SaveMeterValidationRuleFilterSpatial(rule, filterSpatial);
							brokerClassification.SaveMeterValidationRuleFilterMeterClassification(rule, filterMeterClassification);
						}
						t.Complete();

					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Updates an existing business entity MeterValidationRule and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse MeterValidationRule_FormUpdate(MeterValidationRule item) {
			return MeterValidationRule_FormUpdate(item, item.Meters.AsCrudStore(CrudAction.Update),
				item.FilterSpatial.AsCrudStore(CrudAction.Update), item.FilterMeterClassifications.AsCrudStore(CrudAction.Update));
		}

		/// <summary>
		/// Updates an existing business entity MeterValidationRule and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="filterSpatial">The filter spatial.</param>
		/// <param name="filterMeterClassification">The filter meter classification.</param>
		/// <returns></returns>
		public FormResponse MeterValidationRule_FormUpdate(MeterValidationRule item, CrudStore<Meter> meters, CrudStore<Location> filterSpatial, CrudStore<ClassificationItem> filterMeterClassification) {

			FormResponse response;

			var broker = Helper.CreateInstance<MeterValidationRuleBroker>();
			var brokerMeter = Helper.CreateInstance<MeterBroker>();
			var brokerLocation = Helper.CreateInstance<LocationBroker>();
			var brokerClassification = Helper.CreateInstance<ClassificationItemBroker>();

			using (var t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						var rule = response.data as MeterValidationRule;
						if (rule != null) {
							brokerMeter.SaveMeterValidationRuleMeters(rule, meters);
							brokerLocation.SaveMeterValidationRuleFilterSpatial(rule, filterSpatial);
							brokerClassification.SaveMeterValidationRuleFilterMeterClassification(rule, filterMeterClassification);
						}
						t.Complete();

					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse MeterValidationRule_FormUpdateBatch(string[] keys, MeterValidationRule item) {
			MeterValidationRuleBroker broker = Helper.CreateInstance<MeterValidationRuleBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity MeterValidationRule.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool MeterValidationRule_Delete(MeterValidationRule item) {
			MeterValidationRuleBroker broker = Helper.CreateInstance<MeterValidationRuleBroker>();
			return broker.Delete(item);
		}


		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyMeterValidationRule">The key MeterValidationRule.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatial_GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule) {
			var broker = Helper.CreateInstance<LocationBroker>();
			var retVal = broker.GetFilterSpatialStoreByKeyMeterValidationRule(paging, KeyMeterValidationRule);
			return retVal;
		}


		/// <summary>
		/// Gets a json store of the Filter Spatial for a specific PortalTemplate.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyPortalTemplate">The key PortalTemplate.</param>
		/// <returns></returns>
		public JsonStore<Location> FilterSpatial_GetStoreByKeyPortalTemplate(PagingParameter paging, string KeyPortalTemplate) {
			var broker = Helper.CreateInstance<LocationBroker>();
			var retVal = broker.GetFilterSpatialStoreByKeyPortalTemplate(paging, KeyPortalTemplate);
			return retVal;
		}

		/// <summary>
		/// Gets a json store of the Filter Meter Classification for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging.</param>
		/// <param name="KeyMeterValidationRule">The key MeterValidationRule.</param>
		/// <returns></returns>
		public JsonStore<ClassificationItem> FilterMeterClassification_GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule) {
			var broker = Helper.CreateInstance<ClassificationItemBroker>();
			var retVal = broker.GetFilterMeterClassificationStoreByKeyMeterValidationRule(paging, KeyMeterValidationRule);
			return retVal;
		}

		/// <summary>
		/// Gets a store of meters for a specific MeterValidationRule.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMeterValidationRule">The key of the MeterValidationRule.</param>
		/// <returns></returns>
		public JsonStore<Meter> Meter_GetStoreByKeyMeterValidationRule(PagingParameter paging, string KeyMeterValidationRule) {
			MeterBroker broker = Helper.CreateInstance<MeterBroker>();
			return broker.GetStoreByKeyMeterValidationRule(paging, KeyMeterValidationRule);
		}

		/// <summary>
		/// Return a json store of the complete list of Meters associated with a MeterValidationRule.
		/// including meters associated via SpatialFilter cross SpatialFilterPset cross MeterClassification
		/// </summary>
		/// <param name="KeyMeterValidationRule">The key of the MeterValidationRule.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<Meter> MeterValidationRule_GetMetersStore(string KeyMeterValidationRule, PagingParameter paging) {
			var meterBroker = Helper.CreateInstance<MeterBroker>();
			var retVal = meterBroker.GetCompleteListByKeyMeterValidationRule(KeyMeterValidationRule, "*");
			return retVal.ToJsonStoreWithFilter(paging);
		}


		/// <summary>
		/// Gets a json store for the business entity ChartDrillDown.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartDrillDown> ChartDrillDown_GetStore(PagingParameter paging, PagingLocation location) {
			ChartDrillDownBroker broker = Helper.CreateInstance<ChartDrillDownBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartDrillDown.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartDrillDown> ChartDrillDown_SaveStore(CrudStore<ChartDrillDown> store) {
			ChartDrillDownBroker broker = Helper.CreateInstance<ChartDrillDownBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity ChartDrillDown. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<ChartDrillDown> ChartDrillDown_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			ChartDrillDownBroker broker = Helper.CreateInstance<ChartDrillDownBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartDrillDown.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public ChartDrillDown ChartDrillDown_GetItem(string Key, params string[] fields) {
			ChartDrillDownBroker broker = Helper.CreateInstance<ChartDrillDownBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartDrillDown.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartDrillDown_FormLoad(string Key) {
			ChartDrillDownBroker broker = Helper.CreateInstance<ChartDrillDownBroker>();
			return broker.FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity ChartDrillDown and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ChartDrillDown_FormCreate(ChartDrillDown item) {
			ChartDrillDownBroker broker = Helper.CreateInstance<ChartDrillDownBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity ChartDrillDown and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ChartDrillDown_FormUpdate(ChartDrillDown item) {
			ChartDrillDownBroker broker = Helper.CreateInstance<ChartDrillDownBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartDrillDown_FormUpdateBatch(string[] keys, ChartDrillDown item) {
			ChartDrillDownBroker broker = Helper.CreateInstance<ChartDrillDownBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity ChartDrillDown.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartDrillDown_Delete(ChartDrillDown item) {
			ChartDrillDownBroker broker = Helper.CreateInstance<ChartDrillDownBroker>();
			return broker.Delete(item);
		}


		/// <summary>
		/// Update (or Create) the ChartDrillDown after a user click on the Chart.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="XDateTime">The X date time.</param>
		/// <param name="KeyLocation">The key location.</param>
		/// <param name="KeyClassificationItem">The key classification item.</param>
		/// <param name="Type">Type of the drill down.</param>
		/// <param name="KeyPortalWindow">The key portal window.</param>
		/// <param name="KeyPortalTab">The key portal tab.</param>
		/// <returns></returns>
		public List<ChartDrillDown> ChartDrillDown_Update(string KeyChart, string XDateTime, string KeyLocation, string KeyClassificationItem, DrillDownType Type, string KeyPortalWindow, string KeyPortalTab) {
			var retVal = new List<ChartDrillDown>();
			var broker = Helper.CreateInstance<ChartDrillDownBroker>();
			var brokerChart = Helper.CreateInstance<Chart>();

			var chart = Chart_GetItem(KeyChart);
			var Charts = new List<Tuple<string, Chart>>();
			#region DrillDownMode
			switch (chart.DrillDownMode) {
				case ChartDrillDownMode.MasterWithChangePortalTab:
				case ChartDrillDownMode.MasterWithChangePortalWindow:
				case ChartDrillDownMode.Autonomous:
				case ChartDrillDownMode.Listener:
					Charts.Add(new Tuple<string, Chart>(KeyChart, chart));
					break;
			}

			List<string> keys = null;
			switch (chart.DrillDownMode) {
				case ChartDrillDownMode.MasterNoChangePortalTab:
				case ChartDrillDownMode.MasterWithChangePortalTab:
					keys = PortalTab_GetChartKeys(KeyPortalTab);
					break;

				case ChartDrillDownMode.MasterNoChangePortalWindow:
				case ChartDrillDownMode.MasterWithChangePortalWindow:
					keys = PortalWindow_GetChartKeys(KeyPortalWindow);
					break;
			}
			if (keys != null && keys.Count > 0) {
				Charts.AddRange(from key in keys
								where key != KeyChart
								let tmpChart = Chart_GetItem(key)
								where tmpChart.DrillDownMode == ChartDrillDownMode.Listener
								select new Tuple<string, Chart>(key, tmpChart));
			}
			#endregion

			foreach (var kvp in Charts) {
				#region DrillDown Get or Create
				var keyChartToDrillDown = kvp.Item1;
				var chartToDrillDown = kvp.Item2;
				var drillDown = broker.GetItemByKeyChart(keyChartToDrillDown) ?? new ChartDrillDown {
					StartDate = null,
					EndDate = null,
					Enabled = true,
					KeyChart = keyChartToDrillDown,
					Localisation = chartToDrillDown.Localisation,
					LocalisationSiteLevel = chartToDrillDown.LocalisationSiteLevel,
					TimeInterval = chartToDrillDown.TimeInterval,
					ClassificationLevel = chartToDrillDown.ClassificationLevel,
					KeyClassificationItem = "",
					KeyLocation = ""
				};

				#endregion
				#region DrillDown Update
				switch (Type) {
					case DrillDownType.StartDate:
						drillDown.StartDate = DateTime.ParseExact(XDateTime, DateTimeHelper.const_date_time_format_for_filter, CultureInfo.InvariantCulture);
						break;

					case DrillDownType.EndDate:
						drillDown.EndDate = DateTime.ParseExact(XDateTime, DateTimeHelper.const_date_time_format_for_filter, CultureInfo.InvariantCulture).AddInterval(drillDown.TimeInterval, 1);
						break;

					case DrillDownType.FocusDate:
						drillDown.StartDate = DateTime.ParseExact(XDateTime, DateTimeHelper.const_date_time_format_for_filter, CultureInfo.InvariantCulture);
						drillDown.EndDate = drillDown.StartDate.Value.AddInterval(drillDown.TimeInterval, 1);
						drillDown.TimeInterval = ChartHelper.DrillDownTimeInterval(drillDown.TimeInterval);
						break;

					case DrillDownType.DrillDownTimeInterval:
						drillDown.TimeInterval = ChartHelper.DrillDownTimeInterval(drillDown.TimeInterval);
						break;

					case DrillDownType.DrillUpTimeInterval:
						drillDown.TimeInterval = ChartHelper.DrillUpTimeInterval(drillDown.TimeInterval);
						break;

					case DrillDownType.FocusLocation:
						drillDown.KeyLocation = KeyLocation;
						break;

					case DrillDownType.DrillDownLocalisation: {
							var result = ChartHelper.DrillDownLocalisation(drillDown.Localisation, drillDown.LocalisationSiteLevel);
							drillDown.Localisation = result.Item1;
							drillDown.LocalisationSiteLevel = result.Item2;
						}
						break;

					case DrillDownType.DrillUpLocalisation: {
							var result = ChartHelper.DrillUpLocalisation(drillDown.Localisation, drillDown.LocalisationSiteLevel);
							drillDown.Localisation = result.Item1;
							drillDown.LocalisationSiteLevel = result.Item2;
						}
						break;
					case DrillDownType.FocusClassification:
						drillDown.KeyClassificationItem = KeyClassificationItem;
						break;

					case DrillDownType.DrillDownClassification:
						drillDown.ClassificationLevel += 1;
						break;

					case DrillDownType.DrillUpClassification:
						if (drillDown.ClassificationLevel > 0)
							drillDown.ClassificationLevel -= 1;
						else
							drillDown.ClassificationLevel = 0;
						break;
				}
				#endregion
				#region DrillDown Save
				drillDown = broker.Save(drillDown);
				retVal.Add(drillDown);
				#endregion
			}
			return retVal;
		}

		/// <summary>
		/// Apply the chart drill down.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="meters">The meters.</param>
		/// <param name="locations">The locations.</param>
		public void ChartDrillDown_Apply(Chart chart, List<Meter> meters, List<Location> locations) {
			if (chart.DrillDownEnabled && chart.DrillDown != null && chart.DrillDownMode != ChartDrillDownMode.Disabled) {
				var drillDown = chart.DrillDown;

				#region StartDate and EndDate
				//TODO : Check timezone
				chart.StartDate = drillDown.StartDate ?? chart.StartDate;
				chart.EndDate = drillDown.EndDate ?? chart.EndDate;
				if (drillDown.StartDate.HasValue || drillDown.EndDate.HasValue)
					chart.DynamicTimeScaleEnabled = false;
				#endregion

				#region TimeInterval
				chart.TimeInterval = drillDown.TimeInterval;
				#endregion

				#region Localisation
				chart.Localisation = drillDown.Localisation;
				chart.LocalisationSiteLevel = drillDown.LocalisationSiteLevel;

				if (string.IsNullOrEmpty(drillDown.KeyLocation) == false && meters != null) {
					var metersForDeletion = meters.Where(m => (m.KeyLocationPath + " / " + m.KeyMeter).Contains("/ " + drillDown.KeyLocation) == false);
					foreach (var meter in metersForDeletion) {
						meters.Remove(meter);
					}
				}

				if (string.IsNullOrEmpty(drillDown.KeyLocation) == false && locations != null) {
					var locationsForDeletion = locations.Where(l => l.KeyLocationPath.Contains("/ " + drillDown.KeyLocation) == false);
					foreach (var location in locationsForDeletion) {
						locations.Remove(location);
					}
				}
				#endregion

				#region Classification
				chart.ClassificationLevel = drillDown.ClassificationLevel;

				if (string.IsNullOrEmpty(drillDown.KeyClassificationItem) == false && meters != null) {
					var toDelete = new List<Meter>();
					meters.ForEach(m => {
						if (m.KeyClassificationItemPath.Contains("/ " + drillDown.KeyClassificationItem) == false)
							toDelete.Add(m);
					});
					toDelete.ForEach(m => meters.Remove(m));
				}
				#endregion
			}
		}


		/// <summary>
		/// Reset the Chart drill down.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		public void ChartDrillDown_Reset(string KeyChart) {
			var broker = Helper.CreateInstance<ChartDrillDownBroker>();
			var drillDown = broker.GetItemByKeyChart(KeyChart);

			if (drillDown != null) {
				broker.Delete(drillDown);
			}
		}


		/// <summary>
		/// Reset the Chart Calendar View Selection.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		public void ChartCalendarViewSelection_Reset(string KeyChart) {
			var broker = Helper.CreateInstance<ChartCalendarViewSelectionBroker>();
			var selections = broker.GetListByKeyChart(KeyChart);

			if (selections != null) {
				foreach (var selection in selections) {
					broker.Delete(selection);
				}
			}
		}


		/// <summary>
		/// Add a new selection to Calendar View.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		public void ChartCalendarViewSelection_Add(string KeyChart, string startDate, string endDate) {
			var broker = Helper.CreateInstance<ChartCalendarViewSelectionBroker>();

			var selection = new ChartCalendarViewSelection {
				KeyChart = KeyChart,
				StartDate = DateTime.ParseExact(startDate, DateTimeHelper.const_date_time_format_for_filter, CultureInfo.InvariantCulture),
				EndDate = DateTime.ParseExact(endDate, DateTimeHelper.const_date_time_format_for_filter, CultureInfo.InvariantCulture)
			};
			broker.Save(selection);
		}

		/// <summary>
		/// Delete a Chart Calendar View Selection.
		/// </summary>
		/// <param name="KeyChartCalendarViewSelection">The key chart calendar view selection.</param>
		public bool ChartCalendarViewSelection_Delete(string KeyChartCalendarViewSelection) {
			var broker = Helper.CreateInstance<ChartCalendarViewSelectionBroker>();
			var retVal = broker.Delete(KeyChartCalendarViewSelection);
			return retVal;
		}

		/// <summary>
		/// Deletes an existing business entity StatisticalSerie.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool StatisticalSerie_Delete(StatisticalSerie item) {
			StatisticalSerieBroker broker = Helper.CreateInstance<StatisticalSerieBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity StatisticalSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse StatisticalSerie_FormCreate(StatisticalSerie item) {
			StatisticalSerieBroker broker = Helper.CreateInstance<StatisticalSerieBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse StatisticalSerie_FormLoad(string Key) {
			StatisticalSerieBroker broker = Helper.CreateInstance<StatisticalSerieBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity StatisticalSerie and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse StatisticalSerie_FormUpdate(StatisticalSerie item) {
			StatisticalSerieBroker broker = Helper.CreateInstance<StatisticalSerieBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse StatisticalSerie_FormUpdateBatch(string[] keys, StatisticalSerie item) {
			StatisticalSerieBroker broker = Helper.CreateInstance<StatisticalSerieBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<StatisticalSerie> StatisticalSerie_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			StatisticalSerieBroker broker = Helper.CreateInstance<StatisticalSerieBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public StatisticalSerie StatisticalSerie_GetItem(string Key, params string[] fields) {
			StatisticalSerieBroker broker = Helper.CreateInstance<StatisticalSerieBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Gets a json store for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<StatisticalSerie> StatisticalSerie_GetStore(PagingParameter paging, PagingLocation location) {
			StatisticalSerieBroker broker = Helper.CreateInstance<StatisticalSerieBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a store of StatisticalSeries for a specific chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key of the chart.</param>
		/// <returns></returns>
		public JsonStore<StatisticalSerie> StatisticalSerie_GetStoreByKeyChart(PagingParameter paging, string KeyChart) {
			StatisticalSerieBroker broker = Helper.CreateInstance<StatisticalSerieBroker>();
			return broker.GetStoreByKeyChart(paging, KeyChart);
		}

		/// <summary>
		/// Saves a crud store for the business entity StatisticalSerie.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<StatisticalSerie> StatisticalSerie_SaveStore(CrudStore<StatisticalSerie> store) {
			StatisticalSerieBroker broker = Helper.CreateInstance<StatisticalSerieBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a json store for the business entity Algorithm.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Algorithm> Algorithm_GetStore(PagingParameter paging, PagingLocation location) {
			// Get persisted scripts from DB.
			var broker = Helper.CreateInstance<AlgorithmBroker>();
			var retVal = broker.GetStore(paging, location);

			foreach (Algorithm algorithm in retVal.records) {
				if (algorithm.Source != AlgorithmSource.Editable) {
					algorithm.Language = AlgorithmLanguage.NotApplicable;
				}
			}

			// Get built-in scripts.
			var types = AlgorithmHelper.GetBuiltInAlgorithms<IAnalyticsExtensibility>();

			foreach (var t in types) {
				retVal.records.Add(new Algorithm {
					Code = null,
					Description = Helper.GetLocalizedText(t),
					Language = AlgorithmLanguage.NotApplicable,
					KeyAlgorithm = Helper.GetAssemblyQualifiedNameNoVersion(t),
					Name = t.Name,
					IsNotDynamicallyCompiled = true,
					Source = AlgorithmSource.BuiltIn,
				});
			}

			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity Algorithm.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Algorithm> Algorithm_SaveStore(CrudStore<Algorithm> store) {
			AlgorithmBroker broker = Helper.CreateInstance<AlgorithmBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity Algorithm. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<Algorithm> Algorithm_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			AlgorithmBroker broker = Helper.CreateInstance<AlgorithmBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity Algorithm.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public Algorithm Algorithm_GetItem(string Key, params string[] fields) {
			AlgorithmBroker broker = Helper.CreateInstance<AlgorithmBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Loads a specific item for the business entity Algorithm.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Algorithm_FormLoad(string Key) {
			AlgorithmBroker broker = Helper.CreateInstance<AlgorithmBroker>();
			return broker.FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity Algorithm and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		public FormResponse Algorithm_FormCreate(Algorithm item, CrudStore<AlgorithmInputDefinition> inputs) {
			var broker = Helper.CreateInstance<AlgorithmBroker>();
			var brokerInput = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();
			FormResponse response;

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						brokerInput.SaveAlgorithmAlgorithmInputDefinition((Algorithm)response.data, inputs);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Updates an existing business entity Algorithm and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		public FormResponse Algorithm_FormUpdate(Algorithm item, CrudStore<AlgorithmInputDefinition> inputs) {
			var broker = Helper.CreateInstance<AlgorithmBroker>();
			var brokerInput = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();
			FormResponse response;

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						brokerInput.SaveAlgorithmAlgorithmInputDefinition((Algorithm)response.data, inputs);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Algorithm_FormUpdateBatch(string[] keys, Algorithm item) {
			AlgorithmBroker broker = Helper.CreateInstance<AlgorithmBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity Algorithm.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Algorithm_Delete(Algorithm item) {
			AlgorithmBroker broker = Helper.CreateInstance<AlgorithmBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Gets a json store for the business entity ChartAlgorithm.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ChartAlgorithm> ChartAlgorithm_GetStore(PagingParameter paging, PagingLocation location) {
			ChartAlgorithmBroker broker = Helper.CreateInstance<ChartAlgorithmBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity ChartAlgorithm.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ChartAlgorithm> ChartAlgorithm_SaveStore(CrudStore<ChartAlgorithm> store) {
			ChartAlgorithmBroker broker = Helper.CreateInstance<ChartAlgorithmBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity ChartAlgorithm. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<ChartAlgorithm> ChartAlgorithm_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			ChartAlgorithmBroker broker = Helper.CreateInstance<ChartAlgorithmBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity ChartAlgorithm.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public ChartAlgorithm ChartAlgorithm_GetItem(string Key, params string[] fields) {
			ChartAlgorithmBroker broker = Helper.CreateInstance<ChartAlgorithmBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Loads a specific item for the business entity ChartAlgorithm.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ChartAlgorithm_FormLoad(string Key) {
			ChartAlgorithmBroker broker = Helper.CreateInstance<ChartAlgorithmBroker>();
			return broker.FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity ChartAlgorithm and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		public FormResponse ChartAlgorithm_FormCreate(ChartAlgorithm item, CrudStore<AlgorithmInputValue> inputs) {
			FormResponse response;
			var broker = Helper.CreateInstance<ChartAlgorithmBroker>();
			var brokerInput = Helper.CreateInstance<AlgorithmInputValueBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						brokerInput.SaveChartAlgorithmAlgorithmInputValue((ChartAlgorithm)response.data, inputs);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Updates an existing business entity ChartAlgorithm and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="inputs">The inputs.</param>
		/// <returns></returns>
		public FormResponse ChartAlgorithm_FormUpdate(ChartAlgorithm item, CrudStore<AlgorithmInputValue> inputs) {
			FormResponse response;
			var broker = Helper.CreateInstance<ChartAlgorithmBroker>();
			var brokerInput = Helper.CreateInstance<AlgorithmInputValueBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				if (response.success) {
					try {
						brokerInput.SaveChartAlgorithmAlgorithmInputValue((ChartAlgorithm)response.data, inputs);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ChartAlgorithm_FormUpdateBatch(string[] keys, ChartAlgorithm item) {
			ChartAlgorithmBroker broker = Helper.CreateInstance<ChartAlgorithmBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity ChartAlgorithm.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ChartAlgorithm_Delete(ChartAlgorithm item) {
			ChartAlgorithmBroker broker = Helper.CreateInstance<ChartAlgorithmBroker>();
			return broker.Delete(item);
		}
		/// <summary>
		/// Get a store of Algorithm python for a specific chart.
		/// </summary>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<ChartAlgorithm> ChartAlgorithm_GetStoreByKeyChart(string KeyChart, PagingParameter paging) {
			ChartAlgorithmBroker broker = Helper.CreateInstance<ChartAlgorithmBroker>();
			var retVal = broker.GetStoreByKeyChart(paging, KeyChart);
			return retVal;
		}

		/// <summary>
		/// Returns the list of existing events title from a specific calendar category.
		/// </summary>
		/// <param name="KeyCalendarEventCategory">The key calendar event category.</param>
		/// <param name="KeyMeter">The key meter.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<SimpleListElement> Calendar_GetEventsTitle(string KeyCalendarEventCategory, string KeyMeter, PagingParameter paging) {
			var retVal = new JsonStore<SimpleListElement>();
			if (string.IsNullOrEmpty(KeyCalendarEventCategory) == false) {
				var brokerCategory = Helper.CreateInstance<CalendarEventCategoryBroker>();
				var brokerMeter = Helper.CreateInstance<MeterBroker>();

				var category = brokerCategory.GetItem(KeyCalendarEventCategory);
				var meter = brokerMeter.GetItem(KeyMeter);

				using (var energyAggregator = GetEnergyAggregatorServiceAgent()) {
					var list = energyAggregator.Calendar_GetEventsTitle(category, meter);
					retVal = list.Select(s => new SimpleListElement() {
						Id = s,
						Value = s
					}).ToList().ToJsonStoreWithFilter(paging);
				}
			}
			return retVal;
		}




		/// <summary>
		/// Gets a json store for the business entity FlipCard.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<FlipCard> FlipCard_GetStore(PagingParameter paging, PagingLocation location) {
			FlipCardBroker broker = Helper.CreateInstance<FlipCardBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity FlipCard.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<FlipCard> FlipCard_SaveStore(CrudStore<FlipCard> store) {
			FlipCardBroker broker = Helper.CreateInstance<FlipCardBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity FlipCard. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<FlipCard> FlipCard_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			FlipCardBroker broker = Helper.CreateInstance<FlipCardBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity FlipCard.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public FlipCard FlipCard_GetItem(string Key, params string[] fields) {
			FlipCardBroker broker = Helper.CreateInstance<FlipCardBroker>();
			return broker.GetItem(Key, "*");
		}

		/// <summary>
		/// Loads a specific item for the business entity FlipCard.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse FlipCard_FormLoad(string Key) {
			FlipCardBroker broker = Helper.CreateInstance<FlipCardBroker>();
			return broker.FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity FlipCard and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="cards">The cards.</param>
		/// <returns></returns>
		public FormResponse FlipCard_FormCreate(FlipCard item, CrudStore<FlipCardEntry> cards) {
			FormResponse response;
			FlipCardBroker broker = Helper.CreateInstance<FlipCardBroker>();
			FlipCardEntryBroker brokerEntry = Helper.CreateInstance<FlipCardEntryBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormCreate(item);
				if (response.success) {
					try {
						brokerEntry.SaveFlipCardFlipCardEntry((FlipCard)response.data, cards);
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Updates an existing business entity FlipCard and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="cards">The cards.</param>
		/// <returns></returns>
		public FormResponse FlipCard_FormUpdate(FlipCard item, CrudStore<FlipCardEntry> cards) {
			FormResponse response;
			FlipCardBroker broker = Helper.CreateInstance<FlipCardBroker>();
			FlipCardEntryBroker brokerEntry = Helper.CreateInstance<FlipCardEntryBroker>();

			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = broker.FormUpdate(item);
				var flipcard = (FlipCard)response.data;
				if (response.success) {
					try {
						if (cards != null && cards.create != null) {
							foreach (var card in cards.create) {
								card.KeyFlipCard = flipcard.KeyFlipCard;
								brokerEntry.Save(card);
							}
						}
						brokerEntry.SaveFlipCardFlipCardEntry(flipcard, cards);
						broker.PopulateList(flipcard, "*");
						t.Complete();
					}
					catch (Exception ex) {
						response.success = false;
						response.msg = ex.Message;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse FlipCard_FormUpdateBatch(string[] keys, FlipCard item) {
			FlipCardBroker broker = Helper.CreateInstance<FlipCardBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity FlipCard.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool FlipCard_Delete(FlipCard item) {
			FlipCardBroker broker = Helper.CreateInstance<FlipCardBroker>();
			return broker.Delete(item);
		}




		/// <summary>
		/// Gets a json store for the business entity FlipCardEntry.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<FlipCardEntry> FlipCardEntry_GetStore(PagingParameter paging, PagingLocation location) {
			FlipCardEntryBroker broker = Helper.CreateInstance<FlipCardEntryBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity FlipCardEntry.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<FlipCardEntry> FlipCardEntry_SaveStore(CrudStore<FlipCardEntry> store) {
			FlipCardEntryBroker broker = Helper.CreateInstance<FlipCardEntryBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity FlipCardEntry. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<FlipCardEntry> FlipCardEntry_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			FlipCardEntryBroker broker = Helper.CreateInstance<FlipCardEntryBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity FlipCardEntry.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public FlipCardEntry FlipCardEntry_GetItem(string Key, params string[] fields) {
			FlipCardEntryBroker broker = Helper.CreateInstance<FlipCardEntryBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Loads a specific item for the business entity FlipCardEntry.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse FlipCardEntry_FormLoad(string Key) {
			FlipCardEntryBroker broker = Helper.CreateInstance<FlipCardEntryBroker>();
			return broker.FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity FlipCardEntry and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse FlipCardEntry_FormCreate(FlipCardEntry item) {
			FlipCardEntryBroker broker = Helper.CreateInstance<FlipCardEntryBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity FlipCardEntry and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse FlipCardEntry_FormUpdate(FlipCardEntry item) {
			FlipCardEntryBroker broker = Helper.CreateInstance<FlipCardEntryBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse FlipCardEntry_FormUpdateBatch(string[] keys, FlipCardEntry item) {
			FlipCardEntryBroker broker = Helper.CreateInstance<FlipCardEntryBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity FlipCardEntry.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool FlipCardEntry_Delete(FlipCardEntry item) {
			FlipCardEntryBroker broker = Helper.CreateInstance<FlipCardEntryBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Gets a store of  FlipCardEntry for a specific FlipCard.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyFlipCard"></param>
		/// <returns></returns>
		public JsonStore<FlipCardEntry> FlipCardEntry_GetStoreByKeyFlipCard(PagingParameter paging, string KeyFlipCard) {
			FlipCardEntryBroker broker = Helper.CreateInstance<FlipCardEntryBroker>();
			return broker.GetStoreByKeyFlipCard(paging, KeyFlipCard);
		}



		/// <summary>
		/// Gets a json store for the business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<AlgorithmInputDefinition> AlgorithmInputDefinition_GetStore(PagingParameter paging, PagingLocation location) {
			AlgorithmInputDefinitionBroker broker = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AlgorithmInputDefinition> AlgorithmInputDefinition_SaveStore(CrudStore<AlgorithmInputDefinition> store) {
			AlgorithmInputDefinitionBroker broker = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity AlgorithmInputDefinition. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<AlgorithmInputDefinition> AlgorithmInputDefinition_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			AlgorithmInputDefinitionBroker broker = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public AlgorithmInputDefinition AlgorithmInputDefinition_GetItem(string Key, params string[] fields) {
			AlgorithmInputDefinitionBroker broker = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Loads a specific item for the business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputDefinition_FormLoad(string Key) {
			AlgorithmInputDefinitionBroker broker = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();
			return broker.FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity AlgorithmInputDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputDefinition_FormCreate(AlgorithmInputDefinition item) {
			AlgorithmInputDefinitionBroker broker = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity AlgorithmInputDefinition and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputDefinition_FormUpdate(AlgorithmInputDefinition item) {
			AlgorithmInputDefinitionBroker broker = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputDefinition_FormUpdateBatch(string[] keys, AlgorithmInputDefinition item) {
			AlgorithmInputDefinitionBroker broker = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity AlgorithmInputDefinition.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool AlgorithmInputDefinition_Delete(AlgorithmInputDefinition item) {
			AlgorithmInputDefinitionBroker broker = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();
			return broker.Delete(item);
		}


		/// <summary>
		/// Gets a json store for the business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<AlgorithmInputValue> AlgorithmInputValue_GetStore(PagingParameter paging, PagingLocation location) {
			AlgorithmInputValueBroker broker = Helper.CreateInstance<AlgorithmInputValueBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<AlgorithmInputValue> AlgorithmInputValue_SaveStore(CrudStore<AlgorithmInputValue> store) {
			AlgorithmInputValueBroker broker = Helper.CreateInstance<AlgorithmInputValueBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Gets a list for the business entity AlgorithmInputValue. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public List<AlgorithmInputValue> AlgorithmInputValue_GetAll(PagingParameter paging, PagingLocation location, params string[] fields) {
			int total;
			AlgorithmInputValueBroker broker = Helper.CreateInstance<AlgorithmInputValueBroker>();
			return broker.GetAll(paging, location, out total, fields: fields);
		}

		/// <summary>
		/// Gets a specific item for the business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <param name="fields">The fields.</param>
		/// <returns></returns>
		public AlgorithmInputValue AlgorithmInputValue_GetItem(string Key, params string[] fields) {
			AlgorithmInputValueBroker broker = Helper.CreateInstance<AlgorithmInputValueBroker>();
			return broker.GetItem(Key, fields);
		}

		/// <summary>
		/// Loads a specific item for the business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputValue_FormLoad(string Key) {
			AlgorithmInputValueBroker broker = Helper.CreateInstance<AlgorithmInputValueBroker>();
			return broker.FormLoad(Key);
		}


		/// <summary>
		/// Creates a new business entity AlgorithmInputValue and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputValue_FormCreate(AlgorithmInputValue item) {
			AlgorithmInputValueBroker broker = Helper.CreateInstance<AlgorithmInputValueBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Updates an existing business entity AlgorithmInputValue and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputValue_FormUpdate(AlgorithmInputValue item) {
			AlgorithmInputValueBroker broker = Helper.CreateInstance<AlgorithmInputValueBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse AlgorithmInputValue_FormUpdateBatch(string[] keys, AlgorithmInputValue item) {
			AlgorithmInputValueBroker broker = Helper.CreateInstance<AlgorithmInputValueBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Deletes an existing business entity AlgorithmInputValue.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool AlgorithmInputValue_Delete(AlgorithmInputValue item) {
			AlgorithmInputValueBroker broker = Helper.CreateInstance<AlgorithmInputValueBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Gets a json store of AlgorithmInputDefinition for a specific Algorithm.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyAlgorithm">The key script.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<AlgorithmInputDefinition> AlgorithmInputDefinition_GetStoreByKeyAlgorithm(PagingParameter paging, string KeyAlgorithm) {
			if (string.IsNullOrEmpty(KeyAlgorithm)) {
				return new JsonStore<AlgorithmInputDefinition>();
			}

			ChartAlgorithm chartAlgorithm = GetChartAlgorithm(KeyAlgorithm);

			var algorithmInputDefinitionBroker = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();
			JsonStore<AlgorithmInputDefinition> retVal = algorithmInputDefinitionBroker.GetStoreByKeyAlgorithm(paging, KeyAlgorithm);

			AlgorithmInstance<IAnalyticsExtensibility> instance = null;

			try {
				List<string> errors;
				instance = AlgorithmHelper.CreateInstance<IAnalyticsExtensibility>(chartAlgorithm, out errors);

				if (instance != null) {
					var inputs = instance.Algorithm.GetPublicInputs();
					foreach (var input in inputs) {
						retVal.records.Add(new AlgorithmInputDefinition {
							KeyAlgorithm = KeyAlgorithm,
							Name = input.Key,
							Description = input.Value,
							KeyAlgorithmInputDefinition = input.Key
						});
					}
				}
				else {
					TracingService.Write(TraceEntrySeverity.Error, string.Join(", ", errors));
				}

				return retVal;
			}
			finally {
				if (instance != null) {
					instance.Dispose();
				}
			}
		}

		private static ChartAlgorithm GetChartAlgorithm(string KeyAlgorithm) {
			ChartAlgorithm chartAlgorithm;

			var algorithmBroker = Helper.CreateInstance<AlgorithmBroker>();
			var algorithm = algorithmBroker.GetItem(KeyAlgorithm, "*");

			if (algorithm != null) {
				chartAlgorithm = new ChartAlgorithm { Algorithm = algorithm, KeyAlgorithm = KeyAlgorithm };
			}
			else {
				Type algorithmType = Type.GetType(KeyAlgorithm, false);

				if (algorithmType != null) {
					chartAlgorithm = new ChartAlgorithm { KeyAlgorithm = KeyAlgorithm };
				}
				else {
					throw new VizeliaException("Unknown KeyAlgorithm");
				}
			}
			return chartAlgorithm;
		}

		/// <summary>
		/// Gets a json store of AlgorithmInputValue for a specific Algorithm/Chart.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyChart">The key chart.</param>
		/// <param name="KeyAlgorithm">The key algorithm.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<AlgorithmInputValue> AlgorithmInputValue_GetStoreByKeyChart(PagingParameter paging, string KeyChart, string KeyAlgorithm) {
			var broker = Helper.CreateInstance<AlgorithmInputValueBroker>();
			var retVal = broker.GetStoreByKeyChart(paging, KeyChart, KeyAlgorithm);
			return retVal;
		}

		/// <summary>
		/// Gets the specific data models tree.
		/// </summary>
		/// <param name="KeyScript">The key script.</param>
		/// <returns></returns>
		public List<TreeNode> ScriptModel_GetTree(string KeyScript) {
			var retVal = m_CoreBusinessLayer.GetGeneralTypeTree(new List<string> { "Vizelia.FOL.BusinessEntities.ModelDataAlgorithm" }, null);
			if (string.IsNullOrEmpty(KeyScript) == false) {
				var brokerScript = Helper.CreateInstance<AlgorithmBroker>();
				var brokerInput = Helper.CreateInstance<AlgorithmInputDefinitionBroker>();

				var script = brokerScript.GetItem(KeyScript);
				if (script != null) {
					var inputs = brokerInput.GetListByKeyAlgorithm(KeyScript);

					retVal.AddRange(inputs.Select(input => new TreeNode {
						Key = "inputs[\"" + input.Name + "\"]",
						qtip = input.Description,
						text = input.Name,
						leaf = true,
						iconCls = "viz-icon-small-python"
					}));
				}
			}
			return retVal;
		}


		/// <summary>
		/// Get the C# class squeleton for Analytics.
		/// </summary>
		/// <param name="className">Name of the class.</param>
		/// <returns></returns>
		public string Algorithm_GetCode(string className) {
			var retVal = AlgorithmHelper.GetAnalyticsClassCode(className);
			return retVal;
		}

		/// <summary>
		/// Gets data points out of Analytics entity.
		/// </summary>
		/// <param name="analytics">The analytics.</param>
		/// <returns></returns>
		public List<DataPoint> Analytics_GetDataPoints(Analytics analytics) {
			var chart = GetAnalyticsChart(analytics.KeyAnalytics);
			chart = Analytics_ApplyOnChart(analytics, chart);

			int recordCount;
			var retVal = DataPoint_GetListFromChart(analytics.KeyAnalytics, null, out recordCount, chart);

			return retVal;
		}

		/// <summary>
		/// Gets custom grid out of Analytics entity.
		/// </summary>
		/// <param name="analytics">The analytics.</param>
		/// <returns></returns>
		public List<ChartCustomGridCell> Analytics_GetCustomGrid(Analytics analytics) {
			var chart = GetAnalyticsChart(analytics.KeyAnalytics);
			chart = Analytics_ApplyOnChart(analytics, chart);

			var retVal = ChartCustomGridCell_GetListByKeyChart(analytics.KeyAnalytics, true, chart);

			return retVal;
		}

		/// <summary>
		/// Gets HTML5 out of Analytics entity.
		/// </summary>
		/// <param name="analytics">The analytics.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public StreamResult Analytics_GetHtml5(Analytics analytics, int width, int height) {
			var chart = GetAnalyticsChart(analytics.KeyAnalytics);
			chart = Analytics_ApplyOnChart(analytics, chart);
	
			var retVal = Chart_GetStreamJavascript(analytics.KeyAnalytics, width, height, chart);
			
			return retVal;
		}

		/// <summary>
		/// Gets HTML out of Analytics entity.
		/// </summary>
		/// <param name="analytics">The analytics.</param>
		/// <returns></returns>
		public string Analytics_GetHtml(Analytics analytics) {
			var chart = GetAnalyticsChart(analytics.KeyAnalytics);
			chart = Analytics_ApplyOnChart(analytics, chart);
			
			Chart chartWithData = Chart_GetItemWithData(analytics.KeyAnalytics, false, chart == null, chart);
			string html = Chart_GetStringHtml(chartWithData);

			return html;
		}

		/// <summary>
		/// Supports keyChart= null or "" to return a default Chart instance.
		/// Throws VizeliaException with a message matching
		/// Langue.error_odata_entitynotfound_code if the key is not found.
		/// </summary>
		/// <param name="keyChart">When "" or null, it returns a default Chart.
		/// When anything else, it gets the chart associated with the key.</param>
		/// <returns></returns>
		private Chart GetAnalyticsChart(string keyChart) {
			if (String.IsNullOrEmpty(keyChart))
				return new Chart();
			var chart = Chart_GetItem(keyChart, "*");
			if (chart == null)
				throw new VizeliaException(Langue.error_odata_entitynotfound_code);
			return chart;
		}

		/// <summary>
		/// Checks if user has authorization for analytics components
		/// </summary>
		/// <param name="analytics"></param>
		private void Analytics_ApplyFilters(Analytics analytics) {
			// filters on meter and locations are performed performed in MeterBrokerDB
			if (analytics.MetersClassificationsKeys != null) {
				var classificationFilters = FilterHelper.GetFilter<ClassificationItem>();
				if (classificationFilters.Any()) {
					analytics.MetersClassificationsKeys = classificationFilters.Select(x => x.Key).Intersect(analytics.MetersClassificationsKeys).ToList();
				}
			}
		}

		private Chart Analytics_ApplyOnChart(Analytics analytics, Chart chart) {
			Analytics_ApplyFilters(analytics);

			if (analytics.FilterSpatialKeys != null || analytics.FilterSpatialLocalIds != null) {
				chart.FilterSpatial = new List<Location>();

				if (analytics.FilterSpatialKeys != null) {
					chart.FilterSpatial = Helper.CreateInstance<LocationBroker>().GetList(analytics.FilterSpatialKeys.ToArray());
				}

				if (analytics.FilterSpatialLocalIds != null) {
					var locations = Helper.CreateInstance<LocationBroker>().GetListByLocalIds(analytics.FilterSpatialLocalIds.ToArray());
					if (locations != null && locations.Count > 0) {
						var newLocations = locations.Where(p => !chart.FilterSpatial.Exists(c => c.KeyLocation.Equals(p.KeyLocation))).ToList();
						chart.FilterSpatial.AddRange(newLocations);

						if (analytics.FilterSpatialKeys == null) {
							analytics.FilterSpatialKeys = new List<string>();
						}
						analytics.FilterSpatialKeys.AddRange(newLocations.Select(p => p.KeyLocation).ToList());
					}
				}
			}
			

			chart.Meters = Chart_GetMeters(analytics.KeyAnalytics, true, analytics.FilterSpatialKeys, analytics.MetersClassificationsKeys, analytics.MetersKeys);

			// aggregation
			if (analytics.TimeInterval.HasValue) {
				chart.TimeInterval = analytics.TimeInterval.Value;
			}

			if (analytics.Localisation.HasValue) {
				chart.Localisation = analytics.Localisation.Value;
			}

			if (analytics.LocalisationSiteLevel.HasValue) {
				chart.LocalisationSiteLevel = analytics.LocalisationSiteLevel.Value;
			}

			if (analytics.ClassificationLevel.HasValue) {
				chart.ClassificationLevel = analytics.ClassificationLevel.Value;
			}
			else if (analytics.KeyAnalytics == null) {
				chart.ClassificationLevel = ClassificationItem_GetMeterMaxLevel();
			}

			if (!string.IsNullOrWhiteSpace(analytics.TimeZoneId)) {
				chart.TimeZoneId = analytics.TimeZoneId;
			}

			// date range
			if (analytics.StartDate.HasValue || analytics.EndDate.HasValue || analytics.KeyAnalytics == null) {
				chart.DynamicTimeScaleEnabled = false;
				chart.StartDate = DateTime.SpecifyKind(
					analytics.StartDate.HasValue ? analytics.StartDate.Value : DateTime.MinValue, 
					DateTimeKind.Utc);
				chart.EndDate = DateTime.SpecifyKind(
					analytics.EndDate.HasValue ? analytics.EndDate.Value : DateTime.MaxValue, 
					DateTimeKind.Utc);
			}

			if (analytics.HistoricalPsets != null) {
				chart.HistoricalPsets = analytics.HistoricalPsets;
			}

			return chart;
		}

		/// <summary>
		/// Gets the Chart's raw value.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public StreamContent Chart_GetValue(Chart chart, int width, int height) {
			var chartImage = Chart_GetStreamImage(chart.KeyChart, width, height);
			var streamContent = new StreamContent(chartImage.ContentStream);
			streamContent.Headers.ContentType = new MediaTypeHeaderValue(chartImage.MimeType);
			return streamContent;
		}

		/// <summary>
		/// Gets the Analytics raw value.
		/// </summary>
		/// <param name="analytics">The chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public StreamContent Analytics_GetValue(Analytics analytics, int width, int height) {
			var imageMap = Analytics_GetHtmlMap(analytics, width, height);
			var streamContent = GetStreamContent(imageMap.Base64String.GetStreamFromBase64String());
			return streamContent;
		}

		/// <summary>
		/// Gets the Analytics image map.
		/// </summary>
		/// <param name="analytics">The chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public ImageMap Analytics_GetHtmlMap(Analytics analytics, int width, int height) {
			var chart = GetAnalyticsChart(analytics.KeyAnalytics);
			chart = Analytics_ApplyOnChart(analytics, chart);

			var chartWithData = Chart_GetItemWithData(chart.KeyChart, false, false, chart);
			var tuple = Chart_GetStreamBase(chartWithData, width, height, false);
			var imageMap = ChartingService.RenderChartImageMap(tuple.Item1, width, height, tuple.Item2, tuple.Item3);
			return imageMap;
		}

		/// <summary>
		/// Get cached Analytics image by unique identifier.
		/// </summary>
		/// <param name="guidString">The unique identifier string.</param>
		/// <returns></returns>
		public StreamContent Analytics_GetCachedImageByGuid(string guidString) {
			var stream = Chart_GetStreamImageFromCache(guidString);
			var streamContent = GetStreamContent(stream);
			return streamContent;
		}

		private static StreamContent GetStreamContent(Stream stream) {
			var streamContent = new StreamContent(stream);
			streamContent.Headers.ContentType = new MediaTypeHeaderValue("image/png");
			return streamContent;
		}

		/// <summary>
		/// Gets the Meter's raw value.
		/// </summary>
		/// <param name="meter">The meter.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public StreamContent Meter_GetValue(Meter meter, int width, int height) {

			var meterImageMap = Meter_BuildImage(meter.KeyMeter, width, height);
			var streamContent = GetStreamContent(meterImageMap.Base64String.GetStreamFromBase64String());
			return streamContent;
		}

		/// <summary>
		/// Convert date to time zone
		/// </summary>
		/// <param name="date"></param>
		/// <param name="timeZoneId"></param>
		/// <returns></returns>
		public DateTime ConvertDateToTimeZone(DateTime date, string timeZoneId) {
			date = ChangeDateUsingServerTimeZoneOffset(date); //in case the server is not in UTC we change the time to utc.
			var timeZoneInfo = CalendarHelper.GetTimeZoneFromId(timeZoneId);
			var dt = TimeZoneInfo.ConvertTime(DateTime.SpecifyKind(date, DateTimeKind.Utc), timeZoneInfo);
			return DateTime.SpecifyKind(dt, DateTimeKind.Utc);
		}

		/// <summary>
		/// Convert Date to Utc from a date in a given time zone
		/// </summary>
		/// <param name="date"></param>
		/// <param name="timeZoneId"></param>
		/// <returns></returns>
		public DateTime ConvertDateToUtcFromTimeZone(DateTime date, string timeZoneId) {
			date = ChangeDateUsingServerTimeZoneOffset(date); //in case the server is not in UTC we change the time to utc.
			var timeZoneInfo = CalendarHelper.GetTimeZoneFromId(timeZoneId);
			return TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(date, DateTimeKind.Unspecified), timeZoneInfo);
		}

		private DateTime ChangeDateUsingServerTimeZoneOffset(DateTime date) {
			var timeZone = TimeZone.CurrentTimeZone;
			DateTime currentDate = date;
			TimeSpan currentOffset = timeZone.GetUtcOffset(currentDate);
			if (!currentOffset.TotalMinutes.Equals(0)) {
				date = date - currentOffset; //in case the server is not in UTC we change the time to utc.
			}

			return date;
		}

		/// <summary>
		/// Gets the meter data's IQuryable.
		/// </summary>
		/// <param name="meterDataRepository"></param>
		/// <returns></returns>
		public IQueryable<MeterDataEF> MeterDataEF_GetAll(IDataRepository<MeterDataEF> meterDataRepository) {
			return meterDataRepository.GetAll();
		}
	}
}

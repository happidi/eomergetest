﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web.Security;
using DDay.iCal;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.DataLayer;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Providers;
using Vizelia.FOL.WF.Common;

namespace Vizelia.FOL.BusinessLayer {
	/// <summary>
	/// Business Layer implementation for Core. 
	/// </summary>
	public class ServiceDeskBusinessLayer : IServiceDeskBusinessLayer {

		private ICoreDataAccess m_CoreDataAccess;



		/// <summary>
		/// Public Ctor.
		/// </summary>
		public ServiceDeskBusinessLayer() {
			m_CoreDataAccess = Helper.CreateInstance<CoreDataAccess, ICoreDataAccess>();
		}



		/// <summary>
		/// Deletes an existing business entity ActionRequest.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ActionRequest_Delete(ActionRequest item) {
			ActionRequestBroker broker = Helper.CreateInstance<ActionRequestBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity ActionRequest and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ActionRequest_FormCreate(ActionRequest item) {
			//ActionRequestBroker broker = Helper.CreateInstance<ActionRequestBroker>();
			//return broker.FormCreate(item);
			var localService = WorkflowExtensionService.GetExtension<IBaseLocalService<ActionRequest>>();
			var retVal = localService.RaiseEvent("", item);
			return retVal;
		}

		/// <summary>
		/// Loads a specific item for the business entity ActionRequest.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse ActionRequest_FormLoad(string Key) {
			ActionRequestBroker broker = Helper.CreateInstance<ActionRequestBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity ActionRequest and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse ActionRequest_FormUpdate(ActionRequest item) {
			ActionRequestBroker broker = Helper.CreateInstance<ActionRequestBroker>();
			FormResponse response = broker.FormUpdate(item);
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse ActionRequest_FormUpdateBatch(string[] keys, ActionRequest item) {
			ActionRequestBroker broker = Helper.CreateInstance<ActionRequestBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity ActionRequest. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<ActionRequest> ActionRequest_GetAll(PagingParameter paging, PagingLocation location) {
			int total;
			ActionRequestBroker broker = Helper.CreateInstance<ActionRequestBroker>();
			return broker.GetAll(paging, location, out total);
		}

		/// <summary>
		/// Gets a specific item for the business entity ActionRequest.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public ActionRequest ActionRequest_GetItem(string Key) {
			ActionRequestBroker broker = Helper.CreateInstance<ActionRequestBroker>();
			return broker.GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity ActionRequest.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ActionRequest> ActionRequest_GetStore(PagingParameter paging, PagingLocation location) {
			ActionRequestBroker broker = Helper.CreateInstance<ActionRequestBroker>();
			var result = broker.GetStore(paging, location);
			return result;
		}

		/// <summary>
		/// Saves a crud store for the business entity ActionRequest.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ActionRequest> ActionRequest_SaveStore(CrudStore<ActionRequest> store) {
			ActionRequestBroker broker = Helper.CreateInstance<ActionRequestBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity Approval.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Approval_Delete(Approval item) {
			ApprovalBroker broker = Helper.CreateInstance<ApprovalBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Approval and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Approval_FormCreate(Approval item) {
			ApprovalBroker broker = Helper.CreateInstance<ApprovalBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Approval.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Approval_FormLoad(string Key) {
			ApprovalBroker broker = Helper.CreateInstance<ApprovalBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Approval and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Approval_FormUpdate(Approval item) {
			ApprovalBroker broker = Helper.CreateInstance<ApprovalBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Approval_FormUpdateBatch(string[] keys, Approval item) {
			ApprovalBroker broker = Helper.CreateInstance<ApprovalBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Approval. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Approval> Approval_GetAll(PagingParameter paging, PagingLocation location) {
			int total;
			ApprovalBroker broker = Helper.CreateInstance<ApprovalBroker>();
			return broker.GetAll(paging, location, out total);
		}

		/// <summary>
		/// Gets a specific item for the business entity Approval.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Approval Approval_GetItem(string Key) {
			ApprovalBroker broker = Helper.CreateInstance<ApprovalBroker>();
			return broker.GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Approval.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Approval> Approval_GetStore(PagingParameter paging, PagingLocation location) {
			ApprovalBroker broker = Helper.CreateInstance<ApprovalBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity Approval.
		/// </summary>
		/// <param name="KeyActionRequest">The Key of the action request.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Approval> Approval_GetStoreFromActionRequest(string KeyActionRequest, PagingParameter paging, PagingLocation location) {
			if (paging.filters == null)
				paging.filters = new List<GridFilter>();

			paging.filters.Add(new GridFilter {
				data = new GridData {
					comparison = "eq",
					type = "string",
					value = KeyActionRequest
				},
				field = "KeyActionRequest"
			});
			ApprovalBroker broker = Helper.CreateInstance<ApprovalBroker>();
			//return broker.GetStore(paging, location);
			var retVal = broker.GetListByKeyActionRequest(KeyActionRequest);
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity Approval.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Approval> Approval_SaveStore(CrudStore<Approval> store) {
			ApprovalBroker broker = Helper.CreateInstance<ApprovalBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Calculates the next schedule date based on a location.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="minutes"></param>
		/// <param name="KeyLocation"></param>
		/// <returns></returns>
		public DateTime? Calendar_CalculateScheduleDate(DateTime start, double minutes, string KeyLocation) {
			iCalendarCollection calendars = new iCalendarCollection();

			List<Filter> filters = m_CoreDataAccess.GetSpatialHierarchy(new string[] { KeyLocation }, "IfcSpace",
																	 HierarchyDirection.ASC);

			foreach (Filter filter in filters) {
				string key = filter.Key;
				// retreive calendar
				string iCalData = m_CoreDataAccess.Calendar_GetFromObject(key);
				if (iCalData != null) {
					iCalendar iCal = CalendarHelper.Deserialize(iCalData);
					calendars.Add(iCal);
				}
			}
			if (calendars.Count > 0) {
				return CalendarHelper.CalculateScheduleDate(start, minutes, calendars);
			}
			return start.AddMinutes(minutes);
		}

		/// <summary>
		/// Gets a json store of ClassificationItem for a specific mail.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>A json store.</returns>
		public JsonStore<ClassificationItem> ClassificationItem_GetStoreByKeyMail(PagingParameter paging, string KeyMail) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			return broker.GetStoreByKeyMail(paging, KeyMail);
		}

		/// <summary>
		/// Gets a json store of locations for a specific mail
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Location> Location_GetStoreByKeyMail(PagingParameter paging, string KeyMail) {
			LocationBroker broker = Helper.CreateInstance<LocationBroker>();
			return broker.GetStoreByKeyMail(paging, KeyMail);
		}

		/// <summary>
		/// Deletes an existing business entity Mail.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Mail_Delete(Mail item) {
			MailBroker broker = Helper.CreateInstance<MailBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Mail and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <param name="priorities">The store of the priorities of the mail.</param>
		/// <param name="classifications">The store of the classifications for the mail.</param>
		/// <param name="locations">The store of the locations for the mail.</param>
		/// <param name="workflowActionElements">The workflow action elements.</param>
		/// <returns></returns>
		public FormResponse Mail_FormCreate(Mail item, CrudStore<Priority> priorities, CrudStore<ClassificationItem> classifications, CrudStore<Location> locations, CrudStore<WorkflowActionElement> workflowActionElements) {
			FormResponse response;

			MailBroker brokerMail = Helper.CreateInstance<MailBroker>();
			PriorityBroker brokerPriority = Helper.CreateInstance<PriorityBroker>();
			ClassificationItemBroker brokerClassification = Helper.CreateInstance<ClassificationItemBroker>();
			LocationBroker brokerLocation = Helper.CreateInstance<LocationBroker>();
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = brokerMail.FormCreate(item);
				if (response.success) {
					try {
						item = (Mail)response.data;
						brokerPriority.SaveMailPriorities(item, priorities);
						brokerClassification.SaveMailClassifications(item, classifications);
						brokerMail.SaveMailWorkflowActionElements(item, workflowActionElements);
						brokerLocation.SaveMailLocations(item, locations);
						t.Complete();
					}
					catch (Exception ex) {
						response.msg = ex.Message;
						response.success = false;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Loads a specific item for the business entity Mail.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Mail_FormLoad(string Key) {
			MailBroker broker = Helper.CreateInstance<MailBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Mail and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <param name="priorities">The store of the priorities of the mail.</param>
		/// <param name="classifications">The store of the classifications for the mail.</param>
		/// <param name="locations">The store of the locations for the mail.</param>
		/// <param name="workflowActionElements">The workflow action elements.</param>
		/// <returns></returns>
		public FormResponse Mail_FormUpdate(Mail item, CrudStore<Priority> priorities, CrudStore<ClassificationItem> classifications, CrudStore<Location> locations, CrudStore<WorkflowActionElement> workflowActionElements) {
			FormResponse response;

			MailBroker brokerMail = Helper.CreateInstance<MailBroker>();
			PriorityBroker brokerPriority = Helper.CreateInstance<PriorityBroker>();
			ClassificationItemBroker brokerClassification = Helper.CreateInstance<ClassificationItemBroker>();
			LocationBroker brokerLocation = Helper.CreateInstance<LocationBroker>();
			using (TransactionScope t = Helper.CreateTransactionScope()) {
				response = brokerMail.FormUpdate(item);
				if (response.success) {
					try {
						item = (Mail)response.data;
						brokerPriority.SaveMailPriorities(item, priorities);
						brokerClassification.SaveMailClassifications(item, classifications);
						brokerMail.SaveMailWorkflowActionElements(item, workflowActionElements);
						brokerLocation.SaveMailLocations(item, locations);
						t.Complete();
					}
					catch (Exception ex) {
						response.msg = ex.Message;
						response.success = false;
					}
				}
			}
			return response;
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Mail_FormUpdateBatch(string[] keys, Mail item) {
			MailBroker broker = Helper.CreateInstance<MailBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Mail. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Mail> Mail_GetAll(PagingParameter paging, PagingLocation location) {
			int total;
			MailBroker broker = Helper.CreateInstance<MailBroker>();
			return broker.GetAll(paging, location, out total);
		}

		/// <summary>
		/// Gets a specific item for the business entity Mail.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Mail Mail_GetItem(string Key) {
			MailBroker broker = Helper.CreateInstance<MailBroker>();
			return broker.GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Mail.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Mail> Mail_GetStore(PagingParameter paging, PagingLocation location) {
			MailBroker broker = Helper.CreateInstance<MailBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Saves a crud store for the business entity Mail.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Mail> Mail_SaveStore(CrudStore<Mail> store) {
			MailBroker broker = Helper.CreateInstance<MailBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity Priority.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Priority_Delete(Priority item) {
			PriorityBroker broker = Helper.CreateInstance<PriorityBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Priority and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Priority_FormCreate(Priority item) {
			PriorityBroker broker = Helper.CreateInstance<PriorityBroker>();
			return broker.FormCreate(item);
		}

		/// <summary>
		/// Loads a specific item for the business entity Priority.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Priority_FormLoad(string Key) {
			PriorityBroker broker = Helper.CreateInstance<PriorityBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Priority and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Priority_FormUpdate(Priority item) {
			PriorityBroker broker = Helper.CreateInstance<PriorityBroker>();
			return broker.FormUpdate(item);
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Priority_FormUpdateBatch(string[] keys, Priority item) {
			PriorityBroker broker = Helper.CreateInstance<PriorityBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Priority. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Priority> Priority_GetAll(PagingParameter paging, PagingLocation location) {
			int total;
			PriorityBroker broker = Helper.CreateInstance<PriorityBroker>();
			return broker.GetAll(paging, location, out total);
		}

		/// <summary>
		/// Gets a specific item for the business entity Priority.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Priority Priority_GetItem(string Key) {
			PriorityBroker broker = Helper.CreateInstance<PriorityBroker>();
			return broker.GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Priority.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Priority> Priority_GetStore(PagingParameter paging, PagingLocation location) {
			PriorityBroker broker = Helper.CreateInstance<PriorityBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store of priorities for a specific mail
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Priority> Priority_GetStoreByKeyMail(PagingParameter paging, string KeyMail) {
			PriorityBroker broker = Helper.CreateInstance<PriorityBroker>();
			return broker.GetStoreByKeyMail(paging, KeyMail);
		}

		/// <summary>
		/// Saves a crud store for the business entity Priority.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Priority> Priority_SaveStore(CrudStore<Priority> store) {
			PriorityBroker broker = Helper.CreateInstance<PriorityBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Deletes an existing business entity Task.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool Task_Delete(Task item) {
			TaskBroker broker = Helper.CreateInstance<TaskBroker>();
			return broker.Delete(item);
		}

		/// <summary>
		/// Creates a new business entity Task and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse Task_FormCreate(Task item) {
			//TaskBroker broker = Helper.CreateInstance<TaskBroker>();
			//return broker.FormCreate(item);
			var localService = WorkflowExtensionService.GetExtension<IBaseLocalService<Task>>();
			var retVal = localService.RaiseEvent("", item);
			return retVal;
		}

		/// <summary>
		/// Loads a specific item for the business entity Task.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public FormResponse Task_FormLoad(string Key) {
			TaskBroker broker = Helper.CreateInstance<TaskBroker>();
			return broker.FormLoad(Key);
		}

		/// <summary>
		/// Updates an existing business entity Task and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public FormResponse Task_FormUpdate(Task item) {
			TaskBroker broker = Helper.CreateInstance<TaskBroker>();
			FormResponse response = broker.FormUpdate(item);
			return response;
			/*
			FormResponse retVal = null;
			var localService = WorkflowExtensionService.GetExtension<IBaseLocalService<Task>>();
			
			if (item.State == "Completed") {
				TaskBroker broker = Helper.CreateInstance<TaskBroker>();
				retVal = broker.FormUpdate(item);
			}
			else {

				retVal = localService.RaiseEvent(item.IsCompleted ? "Complete" : !item.IsCompleted && item.State == "Assigned" ? "Reassign" : "Assign", item);
			}
			return retVal;
			*/
		}

		/// <summary>
		/// Update batch (mass update) a series of entities based on the non null value provided in item parameter.
		/// </summary>
		/// <param name="keys">The list of keys of item to batch update.</param>
		/// <param name="item">The item containing the values that should be apply to the list of keys.</param>
		/// <returns></returns>
		public FormResponse Task_FormUpdateBatch(string[] keys, Task item) {
			TaskBroker broker = Helper.CreateInstance<TaskBroker>();
			return broker.FormUpdateBatch(keys, item);
		}

		/// <summary>
		/// Gets a list for the business entity Task. 
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns></returns>
		public List<Task> Task_GetAll(PagingParameter paging, PagingLocation location) {
			int total;
			TaskBroker broker = Helper.CreateInstance<TaskBroker>();
			return broker.GetAll(paging, location, out total);
		}

		/// <summary>
		/// Gets a specific item for the business entity Task.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public Task Task_GetItem(string Key) {
			TaskBroker broker = Helper.CreateInstance<TaskBroker>();
			return broker.GetItem(Key);
		}

		/// <summary>
		/// Gets a json store for the business entity Task.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Task> Task_GetStore(PagingParameter paging, PagingLocation location) {
			TaskBroker broker = Helper.CreateInstance<TaskBroker>();
			return broker.GetStore(paging, location);
		}

		/// <summary>
		/// Gets a json store for the business entity Task from an ActionRequest.
		/// </summary>
		/// <param name="KeyActionRequest">The Key of the action request.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="location">The location parameter.</param>
		/// <returns>A json store.</returns>
		public JsonStore<Task> Task_GetStoreFromActionRequest(string KeyActionRequest, PagingParameter paging,
															  PagingLocation location) {
			TaskBroker broker = Helper.CreateInstance<TaskBroker>();

			/*
			if (paging.filters == null)
				paging.filters = new List<GridFilter>();
			paging.filters.Add(new GridFilter { data = new GridData { value = KeyActionRequest, type = "string" }, field = "KeyActionRequest" });
			*/

			//List<Task> results = broker.GetAll(paging, location, out total);
			//var retVal = broker.GetListByKeyActionRequest(KeyActionRequest);

			var retVal = broker.GetStoreByKeyActionRequest(paging, KeyActionRequest);
			return retVal;
		}

		/// <summary>
		/// Saves a crud store for the business entity Task.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<Task> Task_SaveStore(CrudStore<Task> store) {
			TaskBroker broker = Helper.CreateInstance<TaskBroker>();
			return broker.SaveStore(store);
		}

		/// <summary>
		/// Builds the image of the workflow.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public ImageMap Workflow_BuildImage(Guid instanceId, int width, int height) {
			ImageMap result = new ImageMap();
			var broker = Helper.CreateInstance<WorkflowInstanceDefinitionBroker>();
			string currentState = WorkflowHelper.GetCurrentState(instanceId);
			var workflowInstanceDefinition = broker.GetItemByInstanceId(instanceId);
			var xamlDesign = workflowInstanceDefinition.XamlDesign;
			var workflowType = Type.GetType(workflowInstanceDefinition.WorkflowAssemblyQualifiedName);
			if (workflowType != null) {
				var assembliesToLoad = workflowType.Assembly.GetReferencedAssemblies().Select(p => p.FullName).ToArray();
				var xamlFileName = workflowType.Name;
				MemoryStream stream = WorkflowViewerService.RenderWorkflowStream(xamlFileName, xamlDesign, currentState, assembliesToLoad, width, height).ContentStream;
				stream.Position = 0;
				result.Base64String = stream.GetBase64String();
				result.Guid = Guid.NewGuid().ToString();
				CacheService.Add(result.Guid, result, 2);
			}
			return result;
		}

		/// <summary>
		/// Gets all the workflows from bins.
		/// </summary>
		/// <returns></returns>
		public JsonStore<WorkflowAssemblyDefinition> Workflow_GetAllWorkflows() {
			return WorkflowHelper.GetAllWorkflows().ToJsonStore();
		}

		/// <summary>
		/// Gets the possible events that a state machine workflow instance can listen to.
		/// </summary>
		/// <param name="entity">The workflow entity.</param>
		/// <returns></returns>
		public List<WorkflowActionElement> Workflow_GetStateMachineEventsTransitions(IWorkflowEntity entity) {
			var instanceId = entity.InstanceId;
			List<WorkflowActionElement> result = new List<WorkflowActionElement>();
			var genericLocalService = typeof(BaseLocalService<>);
			var constructedLocalService = genericLocalService.MakeGenericType(new Type[] { entity.GetType() });
			var localService = WorkflowExtensionService.GetExtension(constructedLocalService);
			var workflowDefinition = (WorkflowDefinition)localService.GetType().InvokeMember("ResolveWorkflowDefinition", System.Reflection.BindingFlags.InvokeMethod, null, localService, new object[] { entity });
			var membershipUser = Membership.GetUser();
			if (membershipUser != null) {
				var retVal = WorkflowHelper.GetStateMachineEventsTransitions(instanceId, workflowDefinition, membershipUser.UserName);
				return retVal;
			}
			return result;
		}

		/// <summary>
		/// Gets all possible events from an assembly.
		/// </summary>
		/// <param name="assemblyName">The full name of the assembly.
		/// <example>Vizelia.FOL.Workflow.Services.Machine, Vizelia.FOL.WF.Workflows, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null</example>
		/// </param>
		/// <returns></returns>
		public List<WorkflowActionElement> Workflow_GetStateMachineEventsTransitionsFromAssembly(string assemblyName) {
			var retVal = WorkflowHelper.GetStateMachineEventsTransitions(assemblyName);
			return retVal;
		}

		/// <summary>
		/// Gets a distinct store of possible WorkflowActionElement given a list of KeyClassificationItem.
		/// </summary>
		/// <param name="keys">An array of KeyClassificationItem.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<WorkflowActionElement> Workflow_GetStateMachineEventsTransitionsFromClassificationItems(string[] keys) {
			ClassificationItemBroker broker = Helper.CreateInstance<ClassificationItemBroker>();
			List<WorkflowActionElement> events = new List<WorkflowActionElement>();

			if (keys == null || keys.Length <= 0) {
				var workflows = WorkflowHelper.GetAllWorkflows();
				foreach (var workflowAssemblyDefinition in workflows) {
					events.AddRange(WorkflowHelper.GetStateMachineEventsTransitions(workflowAssemblyDefinition.AssemblyQualifiedName));
				}
			}
			else
				foreach (var keyClassificationItem in keys) {
					var workflowAssemblyDefinition = broker.GetWorkflowAssemblyDefinitionByClassificationAscendant(keyClassificationItem, null);
					if (events.Any(e => e.AssemblyQualifiedName == workflowAssemblyDefinition.AssemblyQualifiedName))
						continue;
					events.AddRange(WorkflowHelper.GetStateMachineEventsTransitions(workflowAssemblyDefinition.AssemblyQualifiedName));
				}
			return events.Distinct().OrderBy(p => p.AssemblyQualifiedName).ThenBy(p => p.Label).ToList().ToJsonStore();
		}

		/// <summary>
		/// Gets a store of WorkflowActionElement for a specified Mail.
		/// </summary>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="KeyMail">The Key of the mail.</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<WorkflowActionElement> Workflow_GetStoreWorkflowActionElementByKeyMail(PagingParameter paging, string KeyMail) {
			MailBroker broker = Helper.CreateInstance<MailBroker>();
			var retVal = broker.GetStoreWorkflowActionElementByKeyMail(paging, KeyMail);
			var query = retVal.records.GroupBy(p => p.AssemblyQualifiedName);

			// we have to look into assembly to retreive MsgCode and IconCls as we don't want to store those dynamic informations in database.
			foreach (var assembly in query) {
				var events = Workflow_GetStateMachineEventsTransitionsFromAssembly(assembly.Key);
				foreach (var action in assembly) {
					var selectedEvent = events.FirstOrDefault(p => p.Id == action.Id);
					if (selectedEvent == null)
						continue;
					action.MsgCode = selectedEvent.MsgCode;
					action.IconCls = selectedEvent.IconCls;
				}

			}
			return retVal;
		}

		/// <summary>
		/// Returns the stream of the image for a worflow.
		/// </summary>
		/// <param name="instanceId">The instance id.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public StreamResult Workflow_GetStreamImage(Guid instanceId, int width, int height) {
			var broker = Helper.CreateInstance<WorkflowInstanceDefinitionBroker>();
			string currentState = WorkflowHelper.GetCurrentState(instanceId);
			var workflowInstanceDefinition = broker.GetItemByInstanceId(instanceId);
			var xamlDesign = workflowInstanceDefinition.XamlDesign;
			var workflowType = Type.GetType(workflowInstanceDefinition.WorkflowAssemblyQualifiedName);
			if (workflowType != null) {
				var assembliesToLoad = workflowType.Assembly.GetReferencedAssemblies().Select(p => p.FullName).ToArray();
				var xamlFileName = workflowType.Name;
				return WorkflowViewerService.RenderWorkflowStream(xamlFileName, xamlDesign, currentState, assembliesToLoad, width, height);

			}
			return null;


		}

		/// <summary>
		/// Returns the stream of the image for a worflow.
		/// </summary>
		/// <param name="guid">The guid of the image.</param>
		/// <returns></returns>
		public Stream Workflow_GetStreamImageFromCache(string guid) {
			object data = CacheService.GetData(guid);
			if (data != null) {
				ImageMap imageMap = (ImageMap)data;
				return imageMap.Base64String.GetStreamFromBase64String();
			}
			return null;
		}

		/// <summary>
		/// Runs workflow when raising an event on a workflow entity.
		/// </summary>
		/// <param name="entity">The workflow entity.</param>
		/// <param name="eventName">Name of the event.</param>
		/// <param name="workflowQualifiedName">Name of the workflow assembly.</param>
		/// <returns></returns>
		public FormResponse Workflow_RaiseEvent(IWorkflowEntity entity, string eventName, string workflowQualifiedName) {
			if (!string.IsNullOrWhiteSpace(workflowQualifiedName) && !Roles.IsUserInRole(workflowQualifiedName + "." + eventName)) {
				return new FormResponse { success = false, msg = "No permission for requested workflow transition" };
			}
			var retVal = WorkflowExtensionService.RaiseEvent(entity, eventName);
			return retVal;
		}

		/// <summary>
		/// Restarts the workflow.
		/// This will generate a new instanceId and also update the workflow definition.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public FormResponse Workflow_Restart(IWorkflowEntity entity) {
			var genericLocalService = typeof(BaseLocalService<>);
			var constructedLocalService = genericLocalService.MakeGenericType(new Type[] { entity.GetType() });
			var localService = WorkflowExtensionService.GetExtension(constructedLocalService);
			var setState = localService.GetType().GetMethod("SetState");
			var retVal = (FormResponse)setState.Invoke(localService, new object[] { String.Empty, entity });
			return retVal;
		}

		/// <summary>
		/// Resumes all the pending workflows.
		/// </summary>
		public void Workflow_ResumePending() {
			WorkflowHelper.ResumePending();
		}

		/// <summary>
		/// Updates the workflow definition with current state.
		/// This will generate a new instanceId.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public FormResponse Workflow_UpdateInstanceDefinition(IWorkflowEntity entity) {
			var retVal = WorkflowExtensionService.UpdateDefinition(entity);
			return retVal;
			/*
			string currentState = WorkflowHelper.GetCurrentState(instanceId);
			var localService = WorkflowExtensionService.GetExtension<IBaseLocalService<ActionRequest>>();
			var item = localService.GetItemByInstanceId(instanceId);
			var retVal = localService.SetState(currentState, item);
			return retVal;
			*/
		}
	}
}

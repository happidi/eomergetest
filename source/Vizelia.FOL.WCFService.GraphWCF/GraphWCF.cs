﻿using System.Data;
using System.ServiceModel;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.WCFService {
	/// <summary>
	/// Implementation for Graph Services.
	/// </summary>
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
	public class GraphWCF : BaseModuleWCF, IGraphWCF {

		private IGraphBusinessLayer m_GraphBusinessLayer;

		/// <summary>
		/// Ctor.
		/// </summary>
		public GraphWCF() {
			m_GraphBusinessLayer = Helper.CreateInstance<GraphBusinessLayer, IGraphBusinessLayer>();
		}




		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <param name="viewName">Name of the view.</param>
		/// <returns></returns>
		public DataSet GetView(string viewName) {
			return m_GraphBusinessLayer.GetView(viewName);

		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;

namespace Vizelia.FOL.Utilities.LogInstaller {
	/// <summary>
	/// Creates the needed event source log as they are referenced in the web.config so that EntLib can write to them.
	/// </summary>
	class Program {


		/// <summary>
		/// Main function.
		/// </summary>
		/// <param name="args">The args.</param>
		static void Main(string[] args) {



			if (args.Length == 0) {
				Console.WriteLine("usage: LogInstaller {base source file directory}");
				return;
			}

			if (!Directory.Exists(args[0])) {
				Console.WriteLine("Cannot find base source file directory {0}", args[0]);
				return;
			}
			//string path = Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory + "\\..\\Vizelia.FOL\\source\\Vizelia.FOL.Web\\web.config");
			string path = Path.GetFullPath(args[0] + "\\Vizelia.FOL.Web\\web.config");
			var config = new FileConfigurationSource(path);
			LoggingSettings loggingSettings = (LoggingSettings)config.GetSection("loggingConfiguration");
			foreach (var trace in loggingSettings.TraceListeners) {
				var propSource = trace.ElementInformation.Properties["source"];
				if (propSource == null) continue;
				var source = (string)propSource.Value;
				CreateEventLogSourceIfNotExist(source);
			}
		}


		/// <summary>
		/// Creates the event log source if not exist.
		/// </summary>
		/// <param name="source">The source.</param>
		static void CreateEventLogSourceIfNotExist(string source) {
			bool exist = false;
			exist = EventLog.SourceExists(source);
			if (!exist)
				EventLog.CreateEventSource(source, "Application");
		}
	}
}

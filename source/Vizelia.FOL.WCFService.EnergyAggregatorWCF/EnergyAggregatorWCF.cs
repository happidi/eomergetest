﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.WCFService {
	/// <summary>
	/// Implementation for MeterData in memory Services.
	/// </summary>
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
	public class EnergyAggregatorWCF : BaseWCF, IEnergyAggregatorWCF {

		private readonly IEnergyAggregatorBusinessLayer m_EnergyAggregatorBusinessLayer;

		/// <summary>
		/// Public ctor.
		/// </summary>
		public EnergyAggregatorWCF() {
			// Policy injection on the business layer instance.
			m_EnergyAggregatorBusinessLayer = Helper.CreateInstance<EnergyAggregatorBusinessLayer, IEnergyAggregatorBusinessLayer>();
		}

		/// <summary>
		/// Process all the enabled AlarmDefinition to generate the AlarmInstances.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="alarms">The alarms.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public Dictionary<AlarmDefinition, List<AlarmInstance>> AlarmDefinition_Process(EnergyAggregatorContext context, List<AlarmDefinition> alarms, string culture = null) {
			var retVal = m_EnergyAggregatorBusinessLayer.AlarmDefinition_Process(context, alarms, culture);
			return retVal;
		}

		/// <summary>
		/// Process all the enabled MeterValidationRule to generate the AlarmInstances.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="rules">The rules.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public Dictionary<MeterValidationRule, List<AlarmInstance>> MeterValidationRule_Process(EnergyAggregatorContext context, List<MeterValidationRule> rules, string culture = null) {
			var retVal = m_EnergyAggregatorBusinessLayer.MeterValidationRule_Process(context, rules, culture);
			return retVal;
		}


		/// <summary>
		/// Initializes the data.
		/// </summary>
		/// <param name="tenantDateRangeList">The tenant date range list.</param>
		public void Initialize(List<EnergyAggregatorContext> tenantDateRangeList) {
			m_EnergyAggregatorBusinessLayer.Initialize(tenantDateRangeList);
		}

		/// <summary>
		/// Determines whether this instance is initialized with Data.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns>
		///   <c>true</c> if this instance is initialized; otherwise, <c>false</c>.
		/// </returns>
		public bool IsInitialized(EnergyAggregatorContext context) {
			return m_EnergyAggregatorBusinessLayer.IsInitialized(context);
		}

		/// <summary>
		/// Determines whether the Aggregator is loading meter data.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns>
		///   <c>true</c> if [is loading meter data] [the specified application name]; otherwise, <c>false</c>.
		/// </returns>
		public bool IsLoadingMeterData(EnergyAggregatorContext context) {
			return m_EnergyAggregatorBusinessLayer.IsLoadingMeterData(context);
		}


		/// <summary>
		/// Gets the meter data count.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns></returns>
		public int GetMeterDataCount(EnergyAggregatorContext context) {
			return m_EnergyAggregatorBusinessLayer.GetMeterDataCount(context);
		}


		/// <summary>
		/// Process the Chart and adds returns the Series collecion.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="meterKeys">The meter keys.</param>
		/// <param name="locationKeys">The location keys.</param>
		/// <param name="analysis">The historical analysis.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public Chart Chart_Process(EnergyAggregatorContext context, Chart chart, List<int> meterKeys, List<string> locationKeys, ChartHistoricalAnalysis analysis, List<Filter> locationFilter, string culture = null) {
			var retVal = m_EnergyAggregatorBusinessLayer.Chart_Process(context, chart, meterKeys, locationKeys, analysis, locationFilter, culture);
			return retVal;
		}



		/// <summary>
		/// Process a collection of Charts.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="Keys">The keys.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public List<Chart> Chart_ProcessList(EnergyAggregatorContext context, List<string> Keys, List<Filter> locationFilter, string culture) {
			var retVal = m_EnergyAggregatorBusinessLayer.Chart_Process(context, Keys, locationFilter, culture);
			return retVal;
		}

		/// <summary>
		/// Process a single Chart.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="key">The key of the Chart.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public Chart Chart_ProcessFromKey(EnergyAggregatorContext context, string key, List<Filter> locationFilter, string culture, Chart chart = null) {
			var retVal = m_EnergyAggregatorBusinessLayer.Chart_Process(context, key, locationFilter, culture, chart);
			return retVal;
		}
		/// <summary>
		/// Return a list of available Series name for a specific Chart.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="meterKeys">The meter keys.</param>
		/// <param name="locationKeys">The location keys.</param>
		/// <param name="analysis">The historical analysis.</param>
		/// <param name="locationFilter">The location filter.</param>
		/// <param name="includeExisting">True to include existing serie, false to return only series to be created.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public List<ListElement> Chart_GetDataSerieLocalId(EnergyAggregatorContext context, Chart chart, List<int> meterKeys, List<string> locationKeys, ChartHistoricalAnalysis analysis, List<Filter> locationFilter, bool includeExisting, string culture = null) {
			var retVal = m_EnergyAggregatorBusinessLayer.Chart_GetDataSerieLocalId(context, chart, meterKeys, locationKeys, analysis, locationFilter, includeExisting, culture);
			return retVal;
		}

		/// <summary>
		/// Gets a json store for the business entity MeterData.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="meter">The meter.</param>
		/// <param name="paging">The paging parameter.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="rawData">if set to <c>true</c> [raw data].</param>
		/// <returns>
		/// A json store.
		/// </returns>
		public JsonStore<MeterData> MeterData_GetStoreFromMeter(EnergyAggregatorContext context, Meter meter, PagingParameter paging, DateTime? startDate, DateTime? endDate, bool rawData = false) {
			var retVal = m_EnergyAggregatorBusinessLayer.MeterData_GetStoreFromMeter(context, meter, paging, startDate, endDate, rawData);
			return retVal;
		}


		/// <summary>
		/// Get the last meter data for a specific Meter (sorted by AcquisitionDateTime).
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="meter">The meter.</param>
		/// <returns></returns>
		public MeterData MeterData_GetLastFromMeter(EnergyAggregatorContext context, Meter meter) {
			var retVal = m_EnergyAggregatorBusinessLayer.MeterData_GetLastFromMeter(context, meter);
			return retVal;
		}

		/// <summary>
		/// Update, delete or create the base business entities that are stored in memory needs.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="notifications">The notifications.</param>
		public void CrudNotification(EnergyAggregatorContext context, List<CrudInformation> notifications) {
			m_EnergyAggregatorBusinessLayer.CrudNotification(context, notifications);
		}

		/// <summary>
		/// Returns the list of existing events title from a specific calendar category.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="category">The category.</param>
		/// <param name="meter">The meter.</param>
		/// <returns></returns>
		public List<string> Calendar_GetEventsTitle(EnergyAggregatorContext context, CalendarEventCategory category, Meter meter) {
			return m_EnergyAggregatorBusinessLayer.Calendar_GetEventsTitle(context, category, meter);
		}


		/// <summary>
		/// Determines whether [is crud notification in progress].
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns>
		///   <c>true</c> if [is crud notification in progress]; otherwise, <c>false</c>.
		/// </returns>
		public bool IsCrudNotificationInProgress(EnergyAggregatorContext context) {
			return m_EnergyAggregatorBusinessLayer.IsCrudNotificationInProgress(context);
		}

	}
}

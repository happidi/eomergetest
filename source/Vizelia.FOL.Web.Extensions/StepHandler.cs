﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace Vizelia.FOL.Web.Extensions {
	/// <summary>
	/// Defines a custom handler responsible for generating a dynamic png based on step parameters: C : current, T : total.
	/// It is usually mapped in web.config to step.asmx.
	/// Call examples can be :
	/// color.asmx?C=1&T=8
	/// </summary>
	public class StepHandler : IHttpHandler {
		public bool IsReusable {
			get { return true; }
		}

		/// <summary>
		/// The function called when the handler is executing
		/// </summary>
		/// <param name="context">HttpContext object</param>
		public void ProcessRequest(HttpContext context) {
			var current = 0;
			int.TryParse(context.Request.QueryString["C"], out current);

			var total = 0;
			int.TryParse(context.Request.QueryString["T"], out total);

			const int radius = 20;
			const int padding = 5;

			const int height = radius + 2 * padding;


			var width = total * 2 * radius;
			
			using (var brushCurrent = new SolidBrush(Color.White))
			using (var brushFinished = new SolidBrush(Color.FromArgb(70, 70, 70)))
			using (var pen = new Pen(Color.Gray, 2))
			using (var image = new Bitmap(width, height))
			using (var g = Graphics.FromImage(image)) {
				g.SmoothingMode = SmoothingMode.AntiAlias;
				g.CompositingQuality = CompositingQuality.HighQuality;

				g.FillRectangle(Brushes.Transparent, 0, 0, width, height);

				int xPos = 1;
				for (int i = 0; i < total; i++) {
					if (i == current) {
						g.FillRectangle(brushCurrent, xPos, padding, radius, radius);
						//g.FillEllipse(brushCurrent, xPos, padding, radius, radius);
					}
					else if (i < current) {
						g.FillRectangle(brushFinished, xPos, padding, radius, radius);
						//g.FillEllipse(brushFinished, xPos, padding, radius, radius);
					}
					g.DrawRectangle(pen, xPos, padding, radius, radius);
					//g.DrawEllipse(pen, xPos, padding, radius, radius);
					if (i < total - 1) {
						g.DrawLine(pen, xPos + radius, padding + radius / 2, xPos + 2 * radius, padding + radius / 2);
					}
					xPos += 2 * radius;
				}
				context.Response.ContentType = "image/png";
				context.Response.AddHeader("Content-Type", "image/png");
				using (var memStream = new MemoryStream()) {
					image.Save(memStream, ImageFormat.Png);
					memStream.WriteTo(context.Response.OutputStream);
				}
			}
		}


	}
}

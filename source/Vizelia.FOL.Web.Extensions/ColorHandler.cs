﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace Vizelia.FOL.Web.Extensions {
	/// <summary>
	/// Defines a custom handler responsible for generating a dynamic png based on color parameters R, G and B ranging from 0 to 255.
	/// It is usually mapped in web.config to color.asmx.
	/// Call examples can be :
	/// color.asmx?R=255
	/// color.asmx?R=255&G=255&B=255
	/// </summary>
	public class ColorHandler : IHttpHandler {
		public bool IsReusable {
			get { return true; }
		}

		/// <summary>
		/// The function called when the handler is executing
		/// </summary>
		/// <param name="context">HttpContext object</param>
		public void ProcessRequest(HttpContext context) {
			int red = GetColorFromString(context.Request.QueryString["R"]);
			int green = GetColorFromString(context.Request.QueryString["G"]);
			int blue = GetColorFromString(context.Request.QueryString["B"]);

			bool cancel = context.Request.QueryString["cancel"] == "true";
			bool pushpin = context.Request.QueryString["pushpin"] == "true";
			bool grid = context.Request.QueryString["grid"] == "true";
			
			var width = 16;
			var height = 18;
			var radius = 14;
			var pen = Pens.Black;
			var shouldDisposePen = false;

			if (pushpin) {
				width = 26;
				height = 26;
				radius = 24;
				pen = new Pen(Color.White, 2);
				shouldDisposePen = true;
			}
			else if (grid) {
				width = 12;
				height = 12;
				radius = 11;
			}

			using (var image = new Bitmap(width, height)) 
			using (var g = Graphics.FromImage(image))
			using (var b = new SolidBrush(Color.FromArgb(red, green, blue))) {
				g.SmoothingMode = SmoothingMode.AntiAlias;
				
				g.FillRectangle(Brushes.Transparent, 0, 0, width, height);

				//g.FillRectangle(b, 2, 2, 13, 13);
				//g.DrawRectangle(Pens.Black, 1, 1, 14, 14);

				g.FillEllipse(b, 0, 0, radius, radius);
				g.DrawEllipse(pen, 0, 0, radius, radius);

				if (cancel) {
					var noImage = Image.FromFile(context.Request.PhysicalApplicationPath + "images\\cancel.png");
					g.DrawImage(noImage, 0, 0);
				}
				context.Response.ContentType = "image/png";
				context.Response.AddHeader("Content-Type", "image/png");
				context.Response.Cache.VaryByParams.IgnoreParams = false;
				context.Response.Cache.SetMaxAge(new TimeSpan(7,0,0,0));
				context.Response.Cache.SetExpires(DateTime.Now.AddDays(7));
				context.Response.Cache.SetLastModified(DateTime.Now);
				context.Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
				context.Response.Cache.SetValidUntilExpires(true);

				using (MemoryStream memStream = new MemoryStream()) {
					image.Save(memStream, ImageFormat.Png);
					memStream.WriteTo(context.Response.OutputStream);
				}
			}

			if (shouldDisposePen) pen.Dispose();
		}

		/// <summary>
		/// Converts a string to a valid color int ranging from 0 to 255.
		/// Default returned color is black
		/// </summary>
		/// <param name="sColor">a string color</param>
		/// <returns>an int ranging from 0 to 255</returns>
		private static int GetColorFromString(string sColor) {
			int result;
			const int defaultColor = 0;
			if (int.TryParse(sColor, out result)) {
				if (result >= 0 || result <= 255)
					return result;
				return defaultColor;
			}
			return defaultColor;
		}
	}
}

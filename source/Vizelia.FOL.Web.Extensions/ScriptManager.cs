﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Web.UI;
using Vizelia.FOL.Common.Localization;

namespace Vizelia.FOL.Web.Extensions
{
    /// <summary>
    /// Creates a ASP.Net control which injects scripts to the client.
    /// We currently use it only for one script file which the ASP.Net builds out of the resx file for localization.
    /// We use this class in the *.aspx files in this way:
    /// <code>
    /// <asp:ScriptManager>
    /// </asp:ScriptManager>
    /// </code>
    /// </summary>
    public class ScriptManager : System.Web.UI.ScriptManager
    {
        private const string const_script_viz_langue = "Vizelia.FOL.Common.Localization.Langue.js";

        /// <summary>
        /// Initialization.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            // This creates several localized js files called "Langue.js". Script manager of Asp.Net knows serve the localized file
            // based on the client requesting the resource. The files created from the .Net resources files(.resx) with the same name. 
            var scriptVizLangue = new ScriptReference(const_script_viz_langue, Assembly.GetAssembly(typeof(Langue)).FullName);
            Scripts.Add(scriptVizLangue);

			base.OnInit(e);
        }
    }
}

﻿//This file will be uncommented out in a future checkin, 

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;
using EventLog = System.Diagnostics.EventLog;

namespace Vizelia.FOL.Web.Extensions {
	/// <summary>
	/// A http module for handling JsonP requests.
	/// IMPORTANT: based on this example - http://bloggingabout.net/blogs/adelkhalil/archive/2009/08/14/cross-domain-jsonp-with-jquery-call-step-by-step-guide.aspx
	/// </summary>
    public class JsonPHttpModule : IHttpModule {
        private const string const_json_content_type = "application/json; charset=utf-8";

        #region IHttpModule Members
		/// <summary>
		/// Disposes of the resources (other than memory) used by the module that implements <see cref="T:System.Web.IHttpModule"/>.
		/// </summary>
        public void Dispose() {
        }

		/// <summary>
		/// Inits the specified app.
		/// </summary>
		/// <param name="app">The app.</param>
        public void Init(HttpApplication app) {
            app.BeginRequest += OnBeginRequest;
            app.ReleaseRequestState += OnReleaseRequestState;
        }
        #endregion

		/// <summary>
		/// Called when [begin request].
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void OnBeginRequest(object sender, EventArgs e) {
            HttpApplication app = (HttpApplication)sender;
            HttpRequest resquest = app.Request;
            
			if (!resquest.Url.Query.Contains("?callback")) return;
            if (string.IsNullOrEmpty(app.Context.Request.ContentType)) {
                app.Context.Request.ContentType = const_json_content_type;
            }
        }

		/// <summary>
		/// Called when [release request state].
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void OnReleaseRequestState(object sender, EventArgs e) {
            HttpApplication app = (HttpApplication)sender;
            HttpResponse response = app.Response;
            if (app.Context.Request.ContentType != const_json_content_type) return;

            response.Filter = new JsonResponseFilter(response.Filter);
        }
    }

	/// <summary>
	/// A response filter for JsonP calls
	/// </summary>
    public class JsonResponseFilter : Stream {
        private readonly Stream _responseStream;
        private long _position;

		/// <summary>
		/// Initializes a new instance of the <see cref="JsonResponseFilter"/> class.
		/// </summary>
		/// <param name="responseStream">The response stream.</param>
        public JsonResponseFilter(Stream responseStream) {
            _responseStream = responseStream;
        }

		/// <summary>
		/// When overridden in a derived class, gets a value indicating whether the current stream supports reading.
		/// </summary>
		/// <returns>true if the stream supports reading; otherwise, false.</returns>
        public override bool CanRead { get { return true; } }

		/// <summary>
		/// When overridden in a derived class, gets a value indicating whether the current stream supports seeking.
		/// </summary>
		/// <returns>true if the stream supports seeking; otherwise, false.</returns>
        public override bool CanSeek { get { return true; } }

		/// <summary>
		/// When overridden in a derived class, gets a value indicating whether the current stream supports writing.
		/// </summary>
		/// <returns>true if the stream supports writing; otherwise, false.</returns>
        public override bool CanWrite { get { return true; } }

		/// <summary>
		/// When overridden in a derived class, gets the length in bytes of the stream.
		/// </summary>
		/// <returns>A long value representing the length of the stream in bytes.</returns>
		///   
		/// <exception cref="T:System.NotSupportedException">A class derived from Stream does not support seeking. </exception>
		///   
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
        public override long Length { get { return 0; } }

		/// <summary>
		/// When overridden in a derived class, gets or sets the position within the current stream.
		/// </summary>
		/// <returns>The current position within the stream.</returns>
		///   
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		///   
		/// <exception cref="T:System.NotSupportedException">The stream does not support seeking. </exception>
		///   
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
        public override long Position { get { return _position; } set { _position = value; } }

		/// <summary>
		/// When overridden in a derived class, writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written.
		/// </summary>
		/// <param name="buffer">An array of bytes. This method copies <paramref name="count"/> bytes from <paramref name="buffer"/> to the current stream.</param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buffer"/> at which to begin copying bytes to the current stream.</param>
		/// <param name="count">The number of bytes to be written to the current stream.</param>
		/// <exception cref="T:System.ArgumentException">The sum of <paramref name="offset"/> and <paramref name="count"/> is greater than the buffer length. </exception>
		///   
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="buffer"/> is null. </exception>
		///   
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="offset"/> or <paramref name="count"/> is negative. </exception>
		///   
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		///   
		/// <exception cref="T:System.NotSupportedException">The stream does not support writing. </exception>
		///   
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
        public override void Write(byte[] buffer, int offset, int count) {
            string strBuffer = Encoding.UTF8.GetString(buffer, offset, count);
            strBuffer = AppendJsonpCallback(strBuffer, HttpContext.Current.Request);
            byte[] data = Encoding.UTF8.GetBytes(strBuffer);
            _responseStream.Write(data, 0, data.Length);
        }

        private string AppendJsonpCallback(string strBuffer, HttpRequest request) {
            //return request.Params["callback"] + "(" + strBuffer + ");";
            if (!request.Url.Query.Contains("?callback")) 
            return strBuffer;
            else {
                return request.Params["callback"] + "(" + strBuffer + ");";
            }
        }

		/// <summary>
		/// Closes the current stream and releases any resources (such as sockets and file handles) associated with the current stream.
		/// </summary>
        public override void Close() {
            _responseStream.Close();
        }

		/// <summary>
		/// When overridden in a derived class, clears all buffers for this stream and causes any buffered data to be written to the underlying device.
		/// </summary>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
        public override void Flush() {
            _responseStream.Flush();
        }

		/// <summary>
		/// When overridden in a derived class, sets the position within the current stream.
		/// </summary>
		/// <param name="offset">A byte offset relative to the <paramref name="origin"/> parameter.</param>
		/// <param name="origin">A value of type <see cref="T:System.IO.SeekOrigin"/> indicating the reference point used to obtain the new position.</param>
		/// <returns>
		/// The new position within the current stream.
		/// </returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		///   
		/// <exception cref="T:System.NotSupportedException">The stream does not support seeking, such as if the stream is constructed from a pipe or console output. </exception>
		///   
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
        public override long Seek(long offset, SeekOrigin origin) {
            return _responseStream.Seek(offset, origin);
        }

		/// <summary>
		/// Sets the length.
		/// </summary>
		/// <param name="length">The length.</param>
        public override void SetLength(long length) {
            _responseStream.SetLength(length);
        }

		/// <summary>
		/// When overridden in a derived class, reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
		/// </summary>
		/// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between <paramref name="offset"/> and (<paramref name="offset"/> + <paramref name="count"/> - 1) replaced by the bytes read from the current source.</param>
		/// <param name="offset">The zero-based byte offset in <paramref name="buffer"/> at which to begin storing the data read from the current stream.</param>
		/// <param name="count">The maximum number of bytes to be read from the current stream.</param>
		/// <returns>
		/// The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.
		/// </returns>
		/// <exception cref="T:System.ArgumentException">The sum of <paramref name="offset"/> and <paramref name="count"/> is larger than the buffer length. </exception>
		///   
		/// <exception cref="T:System.ArgumentNullException">
		///   <paramref name="buffer"/> is null. </exception>
		///   
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		///   <paramref name="offset"/> or <paramref name="count"/> is negative. </exception>
		///   
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		///   
		/// <exception cref="T:System.NotSupportedException">The stream does not support reading. </exception>
		///   
		/// <exception cref="T:System.ObjectDisposedException">Methods were called after the stream was closed. </exception>
        public override int Read(byte[] buffer, int offset, int count) {
            return _responseStream.Read(buffer, offset, count);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using System.Threading;

namespace Vizelia.FOL.Web.Extensions {

	/// <summary>
	/// The Long Polling Handler.
	/// </summary>
	public class LongPollingHandler : IHttpHandler {

		/// <summary>
		/// Gets a value indicating whether another request can use the <see cref="T:System.Web.IHttpHandler"/> instance.
		/// </summary>
		/// <returns>true if the <see cref="T:System.Web.IHttpHandler"/> instance is reusable; otherwise, false.</returns>
		public bool IsReusable {
			get { return true; }
		}

		/// <summary>
		/// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler"/> interface.
		/// </summary>
		/// <param name="context">An <see cref="T:System.Web.HttpContext"/> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
		public void ProcessRequest(HttpContext context) {
			context.Response.ContentType = MimeType.Html + ";charset=\"UTF-8\"";

			context.Response.BufferOutput = true;
			// send 256 dummy bytes.
			const string dummy = "                                                                                                                                                                                                                                                                ";
			const string script = @"<script type = 'text/javascript'>
								var data = '{0}';
							    window.parent.test(data);
							 </script>";
			context.Response.Write(dummy + "<strong>Fictious report</strong> <br>");
			for (int i = 1; i <= 100; i++) {
				int workerAvailable;
				int completionPortAvailable;
				ThreadPool.GetAvailableThreads(out workerAvailable, out completionPortAvailable);
				string data = "";
				data += string.Format(" Worker Threads Available: {0}", workerAvailable);
				data += string.Format(" Completion Port Threads Available: {0}", completionPortAvailable);
				context.Response.Write(String.Format(script, data));
				if (i == 100)
					context.Response.Write("Report complete");
				context.Response.Flush();
				Thread.Sleep(1000);
			}

			const string error = "<div>This is really the end</div>";
			context.Response.Write(error);
		}
	}
}

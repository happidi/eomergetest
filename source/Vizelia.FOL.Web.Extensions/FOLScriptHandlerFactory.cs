﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using System.Reflection;
using System.Globalization;
using System.Collections;

namespace Vizelia.FOL.Web.Extensions {
	public class FOLScriptHandlerFactory : IHttpHandlerFactory {

		public IHttpHandler GetHandler(HttpContext context, string requestType, string url, string pathTranslated) {
			return new FOLWCFClientProxyGenerator();

		}

		public void ReleaseHandler(IHttpHandler handler) {

		}
	}

	public class FOLWCFClientProxyGenerator : IHttpHandler {
		public bool IsReusable {
			get { return false; }
		}

		public void ProcessRequest(HttpContext context) {
			
			Assembly asmServiceModel = Assembly.Load("System.ServiceModel, Version=3.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
			Assembly asmWeb = Assembly.Load("System.Web, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a");

			Type ServiceParserType = asmServiceModel.GetType("System.ServiceModel.Activation.ServiceParser");
			Type ServiceBuildProviderType = asmServiceModel.GetType("System.ServiceModel.Activation.ServiceBuildProvider");
			Type VirtualPathType = asmWeb.GetType("System.Web.VirtualPath");
			// serviceBuild = new ServiceBuildProvider();
			object serviceBuild = Activator.CreateInstance(ServiceBuildProviderType);
			// serviceBuild._virtualPath = new VirtualPath(context.Request.AppRelativeCurrentExecutionPath);
			FieldInfo virtualPathField = ServiceBuildProviderType.BaseType.GetField("_virtualPath", BindingFlags.Instance | BindingFlags.NonPublic);
			object virtualPath = VirtualPathType.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic)[1].Invoke(new object[] { context.Request.AppRelativeCurrentExecutionFilePath });
			ServiceBuildProviderType.BaseType.GetField("_virtualPath" , BindingFlags.Instance | BindingFlags.NonPublic).SetValue(serviceBuild, virtualPath, BindingFlags.ExactBinding , null , null);
			// serviceBuild._referenceAssemblies = new SortedList();
			ServiceBuildProviderType.BaseType.GetField("_referencedAssemblies", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(serviceBuild, new SortedList(), BindingFlags.ExactBinding, null, null);
			// serviceBuild.EnsureParsed();
			ServiceBuildProviderType.GetMethod("EnsureParsed" , BindingFlags.Instance |  BindingFlags.NonPublic).Invoke(serviceBuild, null);
			// serviceAttributeValue = serviceBuilder.parser.serviceAttributeValue;
			object serviceParser = ServiceBuildProviderType.GetField("parser", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(serviceBuild);
			string serviceAttributeValue = (string) ServiceParserType.GetField("serviceAttributeValue" , BindingFlags.Instance | BindingFlags.NonPublic).GetValue(serviceParser);
			

			context.Response.ContentType = "application/x-javascript";
			StringBuilder stringBuilder = new StringBuilder();

			string strType = Path.GetFileNameWithoutExtension(context.Request.FilePath);
			Assembly asm = Assembly.Load("Vizelia.FOL.WCFService." + strType + "WCF");
			Type t = asm.GetType("Vizelia.FOL.WCFService." + strType + "WCF");
			Type tInterface = t.GetInterface("I" + strType + "WCF");

			stringBuilder.AppendLine("// File script for : " + tInterface.FullName);
			stringBuilder.AppendLine("Viz.Test = { __type : \"" + tInterface.FullName + "\"");
			
			foreach (MethodInfo mi in tInterface.GetMethods()) {
				StringBuilder stringBuilderCode = new StringBuilder();
				stringBuilder.AppendLine(",");
				stringBuilder.AppendLine("/**");
				stringBuilder.AppendLine("* <p>");
				stringBuilder.AppendLine("* " + mi.Name + ".");
				stringBuilder.AppendLine("* </p>");
				stringBuilder.AppendLine("* ");
				stringBuilder.AppendLine("* @param {Object}");
				stringBuilder.AppendLine("* config An object which may contain the following properties:");
				stringBuilder.AppendLine("* <ul>");
				stringBuilder.AppendLine("* <li><b>success</b> : Function (Optional)<div class=\"sub-desc\">the success callback</div></li>");
				stringBuilder.AppendLine("* <li><b>failure</b> : Function (Optional)<div class=\"sub-desc\">the failure callback</div></li>");
				stringBuilder.AppendLine("* <li><b>scope</b> : Object (Optional)<div class=\"sub-desc\">the scope passed to the callback function</div></li>");
				stringBuilderCode.AppendLine(mi.Name + " : function(config) {");
				stringBuilderCode.AppendLine("\tViz.Services.changeScope(config);");
				stringBuilderCode.Append("\tI" + strType + "WCF." + mi.Name + "(");
				
				foreach (ParameterInfo pi in mi.GetParameters()) {
					stringBuilder.AppendLine("* <li><b>"+ pi.Name +"</b> : " + pi.ParameterType.ToString() + " (Optional)<div class=\"sub-desc\">" + pi.Name + "</div></li>");
					stringBuilderCode.Append("config." + pi.Name + ","); 
				}
				stringBuilder.AppendLine("* </ul>");
				stringBuilder.AppendLine("* </p>");
				stringBuilder.AppendLine("*/");
				stringBuilderCode.Append("config.success , config.failure);");
				stringBuilderCode.AppendLine("\t}");
				stringBuilder.Append(stringBuilderCode.ToString());
				
			}
			stringBuilder.AppendLine("};");
			context.Response.Write(stringBuilder.ToString());
		}
	}


}

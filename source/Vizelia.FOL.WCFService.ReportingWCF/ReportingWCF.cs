﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.WCFService {
	/// <summary>
	/// Implementation for Reporting Services.
	/// </summary>
	[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerCall, UseSynchronizationContext = false)]
	public class ReportingWCF : BaseModuleWCF, IReportingWCF {
		private readonly IReportingBusinessLayer m_ReportingBusinessLayer;

		/// <summary>
		/// Ctor.
		/// </summary>
		public ReportingWCF() {
			// Policy injection on the business layer instance
			m_ReportingBusinessLayer = Helper.CreateInstance<ReportingBusinessLayer, IReportingBusinessLayer>();
		}

		/// <summary>
		/// Gets the report parameters.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public List<ReportParameter> GetReportParameters(string key) {
			List<ReportParameter> reportParameters = m_ReportingBusinessLayer.GetReportParameters(key);
			return reportParameters;
		}

		/// <summary>
		/// Gets the report parameter values.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <returns></returns>
		public JsonStore<ReportParameterValue> GetReportParameterValues(string key, string parameterName) {
			JsonStore<ReportParameterValue> values = m_ReportingBusinessLayer.GetReportParameterValues(key, parameterName);
			return values;
		}

		/// <summary>
		/// Creates the folder with the given name under the specified path.
		/// </summary>
		/// <param name="parentPath">The parent path.</param>
		/// <param name="folderName">Name of the folder.</param>
		public void CreateFolder(string parentPath, string folderName) {
			m_ReportingBusinessLayer.CreateFolder(parentPath, folderName);
		}

		/// <summary>
		/// Deletes the folder.
		/// </summary>
		/// <param name="path">The path.</param>
		public void DeleteFolder(string path) {
			m_ReportingBusinessLayer.DeleteFolder(path);
		}

		/// <summary>
		/// Deletes the report.
		/// </summary>
		/// <param name="path">The path.</param>
		public void DeleteReport(string path) {
			m_ReportingBusinessLayer.DeleteReport(path);
		}
		
		/// <summary>
		/// Renames or moves the item.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="newPath">The new path.</param>
		public void MoveItem(string path, string newPath) {
			m_ReportingBusinessLayer.MoveItem(path, newPath);
		}

		/// <summary>
		/// Renders a report as a stream. This is the start function
		/// Input data are sent via a form encoded as multipart/form-data.
		/// </summary>
		/// <param name="input">
		/// Contains the differents parameters needed by the service.
		/// Each parameter is read via Request.Form collection
		/// </param>
		/// <returns>The report as a stream</returns>
		public void BeginRenderReport(Stream input) {
			Guid operationId = LongRunningOperationService.GetOperationIdFromRequest();
			LongRunningOperationService.Invoke(operationId, RenderReportLongRunning);
		}

		/// <summary>
		/// Renders the report long running.
		/// </summary>
		/// <returns></returns>
		private LongRunningOperationResult RenderReportLongRunning() {
			#region Extracting parameters from Request.Form

			string serviceTypeName = HttpContext.Current.Request.Form["serviceTypeName"];
			string serviceHandler = HttpContext.Current.Request.Form["serviceHandler"];
			string columnModel = HttpContext.Current.Request.Form["columns"];
			string strExport = HttpContext.Current.Request.Form["exportType"];
			string reportTitle = HttpContext.Current.Request.Form["reportTitle"];

			string mimeType;
			string extension;

			List<Column> columns = Helper.DeserializeJson<List<Column>>(columnModel);
			// columns without dataIndex are excluded
			int index = -1;
			columns = (from c in columns
					   where String.IsNullOrEmpty(c.dataIndex) == false
					   select c).ToList();
			
			foreach (Column c in columns) {
				c.positionIndex = ++index; // positionIndex is calculated so we have uniqueness.
			}

			ExportType exportType = Helper.DeserializeJson<ExportType>(strExport);

			#endregion

			LongRunningOperationService.ReportProgress();

			#region Executing service and extracting data

			IList records;
			object result = Helper.ExecuteServiceByReflection(serviceTypeName, serviceHandler, HttpContext.Current.Request.Form);
			try {
				records = (IList)result.GetType().GetProperty("records").GetValue(result, null);
			}
			catch {
				records = (IList)result;
			}

			#endregion

			if(LongRunningOperationService.IsCancelationPending()) {
				return new LongRunningOperationResult(LongRunningOperationStatus.Canceled);
			}

			// Forwarding call to the business layer.
			MemoryStream stream = m_ReportingBusinessLayer.RenderReport(
				records,
				columns,
				reportTitle,
				exportType,
				FormatPageType.A4Landscape,
				out mimeType,
				out extension);

			#region Preparing  response content

			string reportFilename = (!string.IsNullOrEmpty(reportTitle)) ? reportTitle : "Report";
			var report = new StreamResult(stream, reportFilename, extension, mimeType);
			var operationResult = new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, report);

			#endregion

			return operationResult;
		}

		/// <summary>
		/// Generates the json structure of the Reporting treeview.
		/// </summary>
		/// <param name="Key">The Key of the parent node.</param>
		/// <returns>a list of treenodes in json.</returns>
		public List<TreeNode> Reporting_GetTree(string Key) {
			var retVal = m_ReportingBusinessLayer.Reporting_GetTree(Key);
			return retVal;
		}

		/// <summary>
		/// Renders an ActionRequest report as a stream. This is the start function
		/// Input data are sent via a form encoded as multipart/form-data.
		/// </summary>
		///<param name="input"></param>
		public void BeginRenderReportActionRequest(Stream input) {
			Guid operationId = LongRunningOperationService.GetOperationIdFromRequest();
			LongRunningOperationService.Invoke(operationId, RenderReportActionRequestLongRunning);
		}

		/// <summary>
		/// Renders the report action request long running.
		/// </summary>
		/// <returns></returns>
		private LongRunningOperationResult RenderReportActionRequestLongRunning() {
			#region Extracting parameters from Request.Form

			string keyActionRequest = HttpContext.Current.Request.Form["Key"];
			string strExport = HttpContext.Current.Request.Form["exportType"];
			string reportTitle = HttpContext.Current.Request.Form["reportTitle"];
			string strFormatPageType = HttpContext.Current.Request.Form["formatPageType"] ?? FormatPageType.A4Portrait.ParseAsInt().ToString(CultureInfo.InvariantCulture);
			string reportFile = HttpContext.Current.Request.Form["reportFile"];
			string rdlcFile = AppDomain.CurrentDomain.BaseDirectory + "rdlc" + "\\" + reportFile;

			string mimeType;
			string extension;
			ExportType exportType = Helper.DeserializeJson<ExportType>(strExport);
			FormatPageType formatPageType = Helper.DeserializeJson<FormatPageType>(strFormatPageType);

			#endregion

			LongRunningOperationService.ReportProgress();

			#region Executing service and extracting data

			var dataSources = m_ReportingBusinessLayer.PopulateModelActionRequest(keyActionRequest);

			#endregion

			// Forwarding call to the business layer.
			MemoryStream stream = m_ReportingBusinessLayer.RenderReportRdlc(
				rdlcFile,
				dataSources,
				null, 
				exportType,
				formatPageType,
				out mimeType,
				out extension);

			#region Preparing  response content

			string reportFilename = (!string.IsNullOrEmpty(reportTitle)) ? reportTitle : "Report";
			var report = new StreamResult(stream, mimeType, reportFilename, extension);
			var operationResult = new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, report);

			#endregion

			return operationResult;
		}



		/// <summary>
		/// Renders an Task report as a stream. This is the start function
		/// Input data are sent via a form encoded as multipart/form-data.
		/// </summary>
		///<param name="input"></param>
		public void BeginRenderReportTask(Stream input) {
			Guid operationId = LongRunningOperationService.GetOperationIdFromRequest();
			LongRunningOperationService.Invoke(operationId, RenderReportTaskLongRunning);
		}

		/// <summary>
		/// Renders the report task long running.
		/// </summary>
		/// <returns></returns>
		private LongRunningOperationResult RenderReportTaskLongRunning() {
			#region Extracting parameters from Request.Form

			string keyActionRequest = HttpContext.Current.Request.Form["Key"];
			string keyTask = HttpContext.Current.Request.Form["KeyTask"];
			string strExport = HttpContext.Current.Request.Form["exportType"];
			string reportTitle = HttpContext.Current.Request.Form["reportTitle"];
			string strFormatPageType = HttpContext.Current.Request.Form["formatPageType"] ?? FormatPageType.A4Portrait.ParseAsInt().ToString(CultureInfo.InvariantCulture);
			string reportFile = HttpContext.Current.Request.Form["reportFile"];
			string rdlcFile = AppDomain.CurrentDomain.BaseDirectory + "rdlc" + "\\" + reportFile;

			string mimeType;
			string extension;
			ExportType exportType = Helper.DeserializeJson<ExportType>(strExport);
			FormatPageType formatPageType = Helper.DeserializeJson<FormatPageType>(strFormatPageType);

			#endregion

			LongRunningOperationService.ReportProgress();
			#region Executing service and extracting data

			var dataSources = m_ReportingBusinessLayer.PopulateModelActionRequest(keyActionRequest);
			var taskList = ((List<Task>)dataSources["Task"]).Where(t => t.KeyTask == keyTask).ToList();
			dataSources.Remove("Task");
			dataSources.Add("Task", taskList);

			#endregion

			// Forwarding call to the business layer.
			MemoryStream stream = m_ReportingBusinessLayer.RenderReportRdlc(
				rdlcFile,
				dataSources,
				null,
				exportType,
				formatPageType,
				out mimeType,
				out extension);

			#region Preparing  response content

			string reportFilename = (!string.IsNullOrEmpty(reportTitle)) ? reportTitle : "Report";
			var report = new StreamResult(stream, mimeType, reportFilename, extension);
			var operationResult = new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, report);

			#endregion

			return operationResult;
		}


		/// <summary>
		/// Renders a custom report as a stream. This is the start function
		/// Input data are sent via a form encoded as multipart/form-data.
		/// </summary>
		///<param name="input"></param>
		public void BeginRenderReportCustom(Stream input) {
			Guid operationId = LongRunningOperationService.GetOperationIdFromRequest();
			LongRunningOperationService.Invoke(operationId, RenderReportCustomLongRunning);
		}

		/// <summary>
		/// Renders the report action request long running.
		/// </summary>
		/// <returns></returns>
		private LongRunningOperationResult RenderReportCustomLongRunning() {
			#region Extracting parameters from Request.Form

			string keyActionRequest = HttpContext.Current.Request.Form["Key"];
			string strExport = HttpContext.Current.Request.Form["exportType"];
			string reportTitle = HttpContext.Current.Request.Form["reportTitle"];
			string strFormatPageType = HttpContext.Current.Request.Form["formatPageType"] ?? FormatPageType.A4Portrait.ParseAsInt().ToString(CultureInfo.InvariantCulture);
			string reportFile = HttpContext.Current.Request.Form["reportFile"];
			string rdlcFile = AppDomain.CurrentDomain.BaseDirectory + "rdlc" + "\\" + reportFile;

			string mimeType;
			string extension;
			ExportType exportType = Helper.DeserializeJson<ExportType>(strExport);
			FormatPageType formatPageType = Helper.DeserializeJson<FormatPageType>(strFormatPageType);

			#endregion

			LongRunningOperationService.ReportProgress();

			#region Executing service and extracting data

			var dataSources = new Dictionary<string, object> {
			                                                 	{
			                                                 		"DataSet1", new List<ActionRequest> {
			                                                 		                                    	new ActionRequest {
			                                                 		                                    	                  	KeyActionRequest = "#1",
			                                                 		                                    	                  	CloseDateTime = DateTime.Now,
			                                                 		                                    	                  	Description = "test 1"
			                                                 		                                    	                  },
			                                                 		                                    	new ActionRequest {
			                                                 		                                    	                  	KeyActionRequest = "#2",
			                                                 		                                    	                  	CloseDateTime = DateTime.Now,
			                                                 		                                    	                  	Description = "test 2"
			                                                 		                                    	                  }
			                                                 		                                    }
			                                                 		}, {
			                                                 		   	"DataSet2", new List<PsetAttribute> {
			                                                 		   	                                    	new PsetAttribute {
			                                                 		   	                                    	                  	KeyObject = "#1",
			                                                 		   	                                    	                  	Value = "1",
			                                                 		   	                                    	                  	AttributeMsgCode = "prop 1"
			                                                 		   	                                    	                  },
			                                                 		   	                                    	new PsetAttribute {
			                                                 		   	                                    	                  	KeyObject = "#1",
			                                                 		   	                                    	                  	Value = "2",
			                                                 		   	                                    	                  	AttributeMsgCode = "prop 2"
			                                                 		   	                                    	                  },
			                                                 		   	                                    	new PsetAttribute {
			                                                 		   	                                    	                  	KeyObject = "#1",
			                                                 		   	                                    	                  	Value = "3",
			                                                 		   	                                    	                  	AttributeMsgCode = "prop 3"
			                                                 		   	                                    	                  },
			                                                 		   	                                    	new PsetAttribute {
			                                                 		   	                                    	                  	KeyObject = "#2",
			                                                 		   	                                    	                  	Value = "11",
			                                                 		   	                                    	                  	AttributeMsgCode = "prop 11"
			                                                 		   	                                    	                  },
			                                                 		   	                                    	new PsetAttribute {
			                                                 		   	                                    	                  	KeyObject = "#2",
			                                                 		   	                                    	                  	Value = "12",
			                                                 		   	                                    	                  	AttributeMsgCode = "prop 12"
			                                                 		   	                                    	                  },
			                                                 		   	                                    }
			                                                 		   	}
			                                                 };

			#endregion
			var reportParams = new Dictionary<string, object> {{"Logo", @"file://D:/Temp/logoPrincipal.jpg"}};

			// Forwarding call to the business layer.
			MemoryStream stream = m_ReportingBusinessLayer.RenderReportRdlc(
				rdlcFile,
				dataSources,
				reportParams,
				exportType,
				formatPageType,
				out mimeType,
				out extension);

			#region Preparing  response content

			string reportFilename = (!string.IsNullOrEmpty(reportTitle)) ? reportTitle : "Report";
			var report = new StreamResult(stream, reportFilename, extension, mimeType);
			var operationResult = new LongRunningOperationResult(LongRunningOperationStatus.ResultAvailable, report);

			#endregion

			return operationResult;
		}

		/// <summary>
		/// Saves a crud store for the business entity ReportSubscription.
		/// </summary>
		/// <param name="store">The crud store.</param>
		/// <returns>A json store containing the value after the save, or a message for each error.</returns>
		public JsonStore<ReportSubscription> ReportSubscription_SaveStore(CrudStore<ReportSubscription> store) {
			return m_ReportingBusinessLayer.ReportSubscription_SaveStore(store);
		}

		/// <summary>
		/// Creates a new business entity ReportSubscription and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public FormResponse ReportSubscription_FormCreate(ReportSubscription item) {
			return m_ReportingBusinessLayer.ReportSubscription_FormCreate(item);
		}

		/// <summary>
		/// Deletes an existing business entity ReportSubscription.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public bool ReportSubscription_Delete(ReportSubscription item) {
			return m_ReportingBusinessLayer.ReportSubscription_Delete(item);
		}

		/// <summary>
		/// Reports the subscription get all by report.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="paging">The paging.</param>
		/// <returns></returns>
		public JsonStore<ReportSubscription> ReportSubscription_GetAllByReport(string reportPath, PagingParameter paging) {
			return m_ReportingBusinessLayer.ReportSubscription_GetAllByReport(reportPath, paging);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.DataLayer.Interfaces;
using System.Data;
using System.Data.Common;
using Vizelia.FOL.BusinessEntities;
using System.Xml.Linq;
using Vizelia.FOL.Common;
using System.Web.Security;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.DataLayer {
	/// <summary>
	/// Data Layer implementation for Core. 
	/// </summary>
	public class CoreDataAccess : ICoreDataAccess {
		private IDataAccess _da;

		/// <summary>
		/// Ctor.
		/// </summary>
		public CoreDataAccess() {
			_da = new DataAccess();
		}

		/// <summary>
		/// Gets the TypeName of a specific record.
		/// </summary>
		/// <param name="Key">The Key of the record.</param>
		/// <returns>The TypeName of the record.</returns>
		public string GetTypeNameFromRecord(string Key) {
			DbCommand command = _da.GetStoredProcCommand("Core_GetTypeNameFromRecord");
			_da.AddInParameterString(command, "Key", Key);
			_da.AddOutParameterString(command, "Return");
			_da.ExecuteNonQuery(command);
			string result = Convert.ToString(_da.GetParameterValue(command, "Return"));
			return result;
		}

		/// <summary>
		/// Gets all the created entities for a specific User.
		/// </summary>
		/// <param name="KeyUser">The key of th User.</param>
		/// <param name="type">The type of entity.</param>
		/// <returns></returns>
		public List<string> GetCreatedEntity(string KeyUser, Type type) {
			DbCommand command = _da.GetStoredProcCommand("Core_EntityGetListByCreator");
			_da.AddInParameterString(command, "Creator", KeyUser);
			_da.AddInParameterString(command, "DataSrc", type.Name);
			_da.AddInParameterString(command, "Key", Helper.GetAttributeValue<KeyAttribute>(type));
			DataSet ds = _da.ExecuteDataSet(command);

			var query = from row in ds.Tables[0].AsEnumerable()
						select row.Field<object>("Key").ToString();
			return query.ToList();
		}
		#region Calendar

		/// <summary>
		/// Gets the calendar data as a string from an object (for example a location).
		/// </summary>
		/// <param name="KeyObject">The key of the object related to the calendar.</param>
		/// <returns></returns>
		public string Calendar_GetFromObject(string KeyObject) {
			string result = null;
			DbCommand command = _da.GetStoredProcCommand("ServiceDesk_CalendarGetFromObject");
			_da.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			_da.AddInParameterString(command, "KeyObject", KeyObject);
			using (IDataReader reader = _da.ExecuteReader(command)) {
				if (reader.Read()) {
					result = reader.GetValue<string>("iCalData");
				}
			}
			return result;
		}

		/// <summary>
		/// Gets the calendar data as a string from an object (for example a location).
		/// </summary>
		/// <param name="KeyObject">The key of the object related to the calendar.</param>
		/// <returns></returns>
		public string Calendar_GetTimeZoneId(string KeyObject) {
			string result = null;
			DbCommand command = _da.GetStoredProcCommand("ServiceDesk_CalendarTimeZoneGetFromObject");
			_da.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			_da.AddInParameterString(command, "KeyObject", KeyObject);
			using (IDataReader reader = _da.ExecuteReader(command)) {
				if (reader.Read()) {
					result = reader.GetValue<string>("TimeZoneId");
				}
			}
			return result;
		}

		/// <summary>
		/// Gets a dictionnary of all the calendar data as string in a dictionnary group by key object.
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, Tuple<string, string>> Calendar_GetAll() {
			var result = new Dictionary<string, Tuple<string, string>>();
			DbCommand command = _da.GetStoredProcCommand("ServiceDesk_CalendarGetAll");
			_da.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			using (IDataReader reader = _da.ExecuteReader(command)) {
				while (reader.Read()) {
					result.Add(reader.GetValue<string>("KeyObject"), new Tuple<string, string>(reader.GetValue<string>("iCalData"), reader.GetValue<string>("TimeZoneId")));
				}
			}
			return result;
		}

		/// <summary>
		/// Save a string representation of the calendar for an object (for example a location).
		/// </summary>
		/// <param name="KeyObject">The key of the object related to the calendar.</param>
		/// <param name="iCalData">The iCal data persisted as a string.</param>
		/// <param name="TimeZoneId">The time zone id.</param>
		public void Calendar_SaveForObject(string KeyObject, string iCalData, string TimeZoneId) {
			DbCommand command = _da.GetStoredProcCommand("ServiceDesk_CalendarSaveForObject");
			_da.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			_da.AddInParameterString(command, "KeyObject", KeyObject);
			_da.AddInParameterString(command, "iCalData", iCalData);
			_da.AddInParameterString(command, "TimeZoneId", TimeZoneId);
			_da.ExecuteNonQuery(command);
			ExternalCrudNotificationService.SendNotification(new iCalEntity() { KeyObject = KeyObject, iCalData = iCalData, TimeZoneId = TimeZoneId }, CrudAction.Update);

		}

		/// <summary>
		/// Deletes a calendar for an object (for example a location).
		/// </summary>
		/// <param name="KeyObject">The key of the object related to the calendar.</param>
		public void Calendar_DeleteForObject(string KeyObject) {
			DbCommand command = _da.GetStoredProcCommand("ServiceDesk_CalendarDeleteForObject");
			_da.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			_da.AddInParameterString(command, "KeyObject", KeyObject);
			_da.ExecuteNonQuery(command);
			ExternalCrudNotificationService.SendNotification(new iCalEntity() { KeyObject = KeyObject, iCalData = null }, CrudAction.Destroy);

		}
		#endregion
		#region Hierarchy

		/// <summary>
		/// Builds the spatial hierarchy from a list of Keys.
		/// The level excluded is IfcSpace.
		/// </summary>
		/// <param name="keys">The Keys.</param>
		/// <returns>The spatial hierarchy from top level to down level.</returns>
		public List<Filter> GetSpatialHierarchy(string[] keys) {
			return GetSpatialHierarchy(keys, ""); //"IfcSpace,IfcFurniture"
		}

		/// <summary>
		/// Builds the spatial hierarchy from a list of Keys.
		/// </summary>
		/// <param name="keys">The Keys.</param>
		/// <param name="levelExcluded">The ifc level to exclude. For example IfcSpace.</param>
		/// <returns>The spatial hierarchy from top level to down level.</returns>
		public List<Filter> GetSpatialHierarchy(string[] keys, string levelExcluded) {
			var allFilters = new List<Filter>();
			allFilters.AddRange(keys.Select(key => new Filter(key)));

			List<Filter> readOnly = GetSpatialHierarchy(keys, levelExcluded, HierarchyDirection.ASC);
			var readonlyFilters = readOnly.Where(f => !keys.Contains(f.Key));
			allFilters.AddRange(readonlyFilters);

			List<Filter> fullyAllowed = GetSpatialHierarchy(keys, levelExcluded, HierarchyDirection.DESC);
			var fullyAllowedFilters = fullyAllowed.Where(f => !keys.Contains(f.Key));
			allFilters.AddRange(fullyAllowedFilters);

			return allFilters;
		}

		/// <summary>
		/// Builds the spatial hierarchy from a list of Keys.
		/// </summary>
		/// <param name="keys">The Keys</param>
		/// <param name="levelExcluded">The ifc level to exclude. For example IfcSpace.</param>
		/// <param name="direction">The direction. For example ALL, ASC, DESC.</param>
		/// <returns>The spatial hierarchy from top level to down level.</returns>
		public List<Filter> GetSpatialHierarchy(string[] keys, string levelExcluded, HierarchyDirection direction) {
			DbCommand command = _da.GetStoredProcCommand("Core_HierarchySpatial");
			_da.AddInParameterXml(command, "Keys", Helper.BuildXml(keys));
			_da.AddInParameterString(command, "LevelExcluded", levelExcluded);
			_da.AddInParameterString(command, "Direction", direction.ParseAsString());
			DataSet ds = _da.ExecuteDataSet(command);
			var query = from row in ds.Tables[0].AsEnumerable()
						select new Filter(row.Field<string>("KeyChildren"), GetFilterType(direction));

			return query.ToList();
		}

		/// <summary>
		/// Sends a message to the service broker in order to refresh all the hierarchies.
		/// </summary>
		public void RefreshHierarchies() {
			DbCommand command = _da.GetStoredProcCommand("Core_ChangeHierarchy");
			_da.ExecuteNonQuery(command);
		}

		/// <summary>
		/// Returns all the cache keys that are LIKE the given key
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public List<string> DBCacheItems_GetAllLikeKey(string key) {
			DbCommand command = _da.GetStoredProcCommand("Cache_GetAllLikeKey");

			_da.AddInParameterString(command, "Key", key);
			DataSet ds = _da.ExecuteDataSet(command);
			var query = from row in ds.Tables[0].AsEnumerable()
						select row.Field<string>("Key");

			return query.ToList();
		}

		/// <summary>
		/// Gets the type of the filter.
		/// </summary>
		/// <param name="direction">The direction.</param>
		/// <returns></returns>
		private static FilterType GetFilterType(HierarchyDirection direction) {
			FilterType filterType = FilterType.Direct;
			switch (direction) {
				case HierarchyDirection.DESC:
					filterType = FilterType.Descendant;
					break;
				case HierarchyDirection.ASC:
					filterType = FilterType.Ascendant;
					break;
			}
			return filterType;
		}

		/// <summary>
		/// Builds the classification item hierarchy from a list of Keys.
		/// </summary>
		/// <param name="keys">The Keys</param>
		/// <returns>The classification item hierarchy from top level to down level.</returns>
		public List<Filter> GetClassificationItemHierarchy(string[] keys) {
			var retVal = GetClassificationItemHierarchy(keys, HierarchyDirection.ALL);
			return retVal;
		}

		/// <summary>
		/// Builds the classification item hierarchy from a list of Keys.
		/// </summary>
		/// <param name="keys">The Keys</param>
		/// <param name="direction">The direction. For example ALL, ASC, DESC.</param>
		/// <returns>The classification item hierarchy from top level to down level.</returns>
		public List<Filter> GetClassificationItemHierarchy(string[] keys, HierarchyDirection direction) {
			DbCommand command = _da.GetStoredProcCommand("Core_HierarchyClassificationItem");
			_da.AddInParameterXml(command, "Keys", Helper.BuildXml(keys));
			_da.AddInParameterString(command, "Direction", direction.ParseAsString());
			DataSet ds = _da.ExecuteDataSet(command);
			var query = from row in ds.Tables[0].AsEnumerable()
						select new Filter(row.Field<string>("KeyChildren"), GetFilterType(direction));
			return query.ToList();
		}

		/// <summary>
		/// Builds the organization hierarchy from a list of Keys.
		/// </summary>
		/// <param name="keys">The Keys</param>
		/// <returns>The organization hierarchy from top level to down level.</returns>
		public List<Filter> GetOrganizationHierarchy(string[] keys) {
			var retVal = GetOrganizationHierarchy(keys, HierarchyDirection.ALL);
			return retVal;
		}

		/// <summary>
		/// Builds the organization hierarchy from a list of Keys.
		/// </summary>
		/// <param name="keys">The Keys</param>
		/// <param name="direction">The direction. For example ALL, ASC, DESC.</param>
		/// <returns>The organization hierarchy from top level to down level.</returns>
		public List<Filter> GetOrganizationHierarchy(string[] keys, HierarchyDirection direction) {
			DbCommand command = _da.GetStoredProcCommand("Core_HierarchyOrganization");
			_da.AddInParameterXml(command, "Keys", Helper.BuildXml(keys));
			_da.AddInParameterString(command, "Direction", direction.ParseAsString());
			DataSet ds = _da.ExecuteDataSet(command);
			var query = from row in ds.Tables[0].AsEnumerable()
						select new Filter(row.Field<string>("KeyChildren"), GetFilterType(direction));
			return query.ToList();
		}

		#endregion

		/// <summary>
		/// Entities the count_ get all.
		/// </summary>
		/// <returns></returns>
		public List<ListElementGeneric<long>> EntityCount_GetAll() {

			var command = _da.GetStoredProcCommand("Core_EntityCount");
			_da.AddInParameterString(command, "Application", ContextHelper.ApplicationName);
			var ds = _da.ExecuteDataSet(command);
			var query = from row in ds.Tables[0].AsEnumerable()
						select new ListElementGeneric<long> {
							Id = row.Field<string>("Name"),
							Value = row.Field<long>("Value")
						};
			return query.ToList();

			//var retVal = new List<ListElementGeneric<int>>();
			//retVal.Add(new ListElementGeneric<int> { Id = "IfcBuilding", IconCls = "viz-icon-small-building", MsgCode = "Building", Label = "Building", Value = 13 });
			//return retVal;

		}
	}
}
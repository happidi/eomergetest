﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Expression.Controls;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using Color = System.Windows.Media.Color;
using Image = System.Windows.Controls.Image;
using Point = System.Windows.Point;
using Rectangle = System.Windows.Shapes.Rectangle;
using Size = System.Windows.Size;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a standard dynamicdisplay provider using vizelia custom implementation.
	/// </summary>
	public class VizeliaDynamicDisplayProvider : DynamicDisplayProvider {

		/// <summary>
		/// Render the dynamicdisplay image stream.
		/// </summary>
		/// <param name="display">the dynamicdisplay.</param>
		/// <param name="width">the width.</param>
		/// <param name="height">the height.</param>
		/// <returns></returns>
		public override Tuple<StreamResult, Padding> RenderDynamicDisplayStream(DynamicDisplay display, int width, int height) {
			var worker = new XamlWorker(display, width, height);
			var pngCreationThread = new Thread(() => {
				worker.GenerateImageStream();
				try {
					Dispatcher.CurrentDispatcher.InvokeShutdown();
				}
				catch { ;}
			}) { IsBackground = true };
			pngCreationThread.SetApartmentState(ApartmentState.STA);
			pngCreationThread.Start();
			pngCreationThread.Join();
			var retVal = new Tuple<StreamResult, Padding>(new StreamResult(worker.Stream, MimeType.Png), worker.ChartPadding);
			return retVal;
		}
	}

	/// <summary>
	/// Internal Class to Generate the DynamicDisplay Image Background from the Xaml template.
	/// </summary>
	internal class XamlWorker {
		private const int const_min_mainimage_width = 20;
		private const string const_defaultfontfamily = "Tahoma";
		private const string const_defaultfontcolor = "000000";
		private const string const_defaultfontsize = "4"; // multiplied by 4, therfore equals 16.


		/// <summary>
		/// private reference to the DynamicDisplay business entity.
		/// </summary>
		private DynamicDisplay m_Display;
		/// <summary>
		/// The width of the framework element to generate the image. can be different of the final height if the size is too small.
		/// </summary>
		private int m_OriginalHeight;
		/// <summary>
		/// The width of the framework element to generate the image. can be different of the final height if the size is too small.
		/// </summary>
		private int m_OriginalWidth;
		/// <summary>
		/// private reference to the DynamicDisplay final height.
		/// </summary>
		private int m_Height;
		/// <summary>
		/// private reference to the DynamicDisplay final width.
		/// </summary>
		private int m_Width;



		/// <summary>
		/// Initializes a new instance of the <see cref="XamlWorker"/> class.
		/// </summary>
		/// <param name="display">The DynamicDisplay business entity.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		public XamlWorker(DynamicDisplay display, int width, int height) {
			m_Display = display;
			m_Width = width;
			m_Height = height;
			m_OriginalWidth = m_Width;
			m_OriginalHeight = m_Height;
			if (m_Width <= 300 && m_Height <= 200) {
				m_Width = 600;
				m_Height = m_OriginalWidth > 0 ? m_Height * m_Width / m_OriginalWidth : 400;
			}
			ChartPadding = Padding.Empty;
			Stream = null;
			Errors = new List<Exception>();
			ConfigureXamlTypeMapper();

		}



		/// <summary>
		/// The resulting chart padding.
		/// </summary>
		public Padding ChartPadding { get; set; }

		/// <summary>
		/// The exception that occured in the separated Thread.
		/// </summary>
		public List<Exception> Errors { get; set; }

		/// <summary>
		/// The resulting png memory stream.
		/// </summary>
		public MemoryStream Stream { get; set; }

		/// <summary>
		/// Generate a bitmap image from a stream..
		/// </summary>
		/// <param name="stream">The stream.</param>
		/// <returns></returns>
		private BitmapImage BitmapImageFromStream(MemoryStream stream) {
			BitmapImage bi = new BitmapImage();
			bi.BeginInit();
			bi.StreamSource = stream;
			bi.CacheOption = BitmapCacheOption.OnLoad;
			bi.EndInit();
			bi.Freeze();
			stream.Close();
			return bi;
		}

		/// <summary>
		/// Calculates the chart padding final position.
		/// </summary>
		/// <param name="element">The element.</param>
		private void CalculateChartPadding(FrameworkElement element) {
			var chartImageRectangle = element.FindName("ChartImageRectangle") as Rectangle;
			if (chartImageRectangle != null) {
				var chartSize = chartImageRectangle.RenderSize;
				var relativePoint = chartImageRectangle.TransformToAncestor(element).Transform(new Point(0, 0));
				var xpos = (int)relativePoint.X;//*;
				var ypos = (int)relativePoint.Y; //*m_OriginalHeight/ m_Height;

				double ratioX = (double)m_OriginalWidth / (double)m_Width;
				double ratioY = (double)m_OriginalHeight / (double)m_Height;

				ChartPadding = new Padding((int)(xpos * ratioX), (int)(ypos * ratioY), (int)((m_Width - xpos - (int)chartSize.Width) * ratioX), (int)((m_Height - ypos - (int)chartSize.Height) * ratioY));
			}
			else {
				//ChartPadding = new Padding(m_Width, m_Height, 0, 0);
				m_Display.MainChartVisible = false;
			}
		}

		/// <summary>
		/// Configure the Xaml Type Mapper for custom drawing objects.
		/// </summary>
		private void ConfigureXamlTypeMapper() {
			try {
				var ctx = new ParserContext { XamlTypeMapper = XamlTypeMapper.DefaultMapper };
				ctx.XamlTypeMapper.AddMappingProcessingInstruction("http://schemas.microsoft.com/expression/2010/drawing", "Microsoft.Expression.Controls", typeof(Callout).Assembly.FullName);
			}
			catch (Exception e) {
				Errors.Add(e);
			}
		}

		/// <summary>
		/// Generates the image stream.
		/// </summary>
		public void GenerateImageStream() {
			try {
				var xaml = GetXamlTemplate();
				if (!string.IsNullOrEmpty(xaml)) {
					var xamlString = HandleXamlCode(xaml);
					var parsedXaml = XamlReader.Parse(xamlString);
					var element = (FrameworkElement)(parsedXaml);

					UpdateElements(element);
					SetSize(element);
					CalculateChartPadding(element);
					GenerateImageStream(element);

					//GC.Collect(0, GCCollectionMode.Forced);
					//GC.Collect(1, GCCollectionMode.Forced);
					//GC.Collect(2, GCCollectionMode.Forced);
				}
			}
			catch (Exception e) {
				Errors.Add(e);
			}
		}

		private string HandleXamlCode(string xaml) {
			if (!m_Display.MainChartVisible && m_Display.Type != DynamicDisplayType.Simple) {
				var chartPosition = xaml.IndexOf("ChartImageRectangle", StringComparison.Ordinal);
				if (chartPosition != -1) {
					var chartStartPosition = xaml.LastIndexOf('<', chartPosition);
					var chartEndPosition = xaml.IndexOf('>', chartPosition);
					xaml = xaml.Remove(chartStartPosition, chartEndPosition - chartStartPosition + 1);
				}
			}

			return xaml;
		}

		/// <summary>
		/// Generates the image stream.
		/// </summary>
		/// <param name="element">The element.</param>
		private void GenerateImageStream(FrameworkElement element) {
			MemoryStream tempStream = GetPngStream(element);
			if (m_OriginalWidth != m_Width || m_OriginalHeight != m_Height) {
				Stream = ImageHelper.CreateThumbnailImage(tempStream, m_OriginalWidth, m_OriginalHeight);
				tempStream.Dispose();
			}
			else {
				Stream = tempStream;
				Stream.Position = 0;
			}

		}

		/// <summary>
		/// Gets the PNG stream from a framework element.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <returns></returns>
		private MemoryStream GetPngStream(FrameworkElement element) {
			var renderTarget = new RenderTargetBitmap(Math.Max(30, (int)element.RenderSize.Width), Math.Max(30, (int)element.RenderSize.Height), 96, 96, PixelFormats.Pbgra32);
			var sourceBrush = new VisualBrush(element);
			var drawingVisual = new DrawingVisual();
			using (var drawingContext = drawingVisual.RenderOpen()) {
				drawingContext.DrawRectangle(sourceBrush, null, new Rect(new Point(0, 0), new Point(element.RenderSize.Width, element.RenderSize.Height)));
			}
			renderTarget.Render(drawingVisual);
			var pngEncoder = new PngBitmapEncoder();
			pngEncoder.Frames.Add(BitmapFrame.Create(renderTarget));
			var retVal = new MemoryStream();
			pngEncoder.Save(retVal);
			retVal.Position = 0;
			return retVal;
		}

		/// <summary>
		/// Gets the xaml template.
		/// </summary>
		/// <returns></returns>
		private string GetXamlTemplate() {
			string retVal = null;
			try {
				var path = string.Format("{0}xaml\\DynamicDisplay\\{1}.xaml", AppDomain.CurrentDomain.BaseDirectory, m_Display.Type);
				retVal = File.ReadAllText(path);
			}
			catch (Exception e) {
				Errors.Add(e);
			}
			return retVal;
		}

		/// <summary>
		/// Generate an Image Brush from a stream..
		/// </summary>
		/// <param name="stream">The stream.</param>
		/// <returns></returns>
		private ImageBrush ImageBrushFromStream(MemoryStream stream) {
			return new ImageBrush(BitmapImageFromStream(stream));

		}

		/// <summary>
		/// Set s the framework element final size.
		/// </summary>
		/// <param name="element"></param>
		private void SetSize(FrameworkElement element) {
			var size = new Size(m_Width, m_Height);
			element.Measure(size);
			element.Arrange(new Rect(size));
		}

		/// <summary>
		/// Updates the elements (Images, colors, text) based on the dynamic display definition.
		/// </summary>
		/// <param name="element">The element.</param>
		private void UpdateElements(FrameworkElement element) {
			// Color section
			var colorTemplate = m_Display.GetColorTemplate();
			var startColor = colorTemplate.GetStartColor();
			var endColor = colorTemplate.GetEndColor();
			var borderColor = colorTemplate.GetBorderColor();

			// Update areas section
			UpdateHeaderArea(element, startColor, borderColor, endColor);

			UpdateMainArea(element, startColor, borderColor, endColor);

			UpdateFooterArea(element, borderColor, endColor);

			UpdateForSpecificDynamicDisplay(element);
		}

		private void UpdateForSpecificDynamicDisplay(FrameworkElement element) {
			switch (m_Display.Type) {

				case DynamicDisplayType.Tile:
				case DynamicDisplayType.BottomComment:
				case DynamicDisplayType.DenseText:
					var headerImage = element.FindName("HeaderImage") as Shape;
					if (headerImage != null && headerImage.Visibility == Visibility.Visible) {
						var mainImage = element.FindName("MainImage") as Shape;
						var mainImageWidth = mainImage == null ? 0 : mainImage.Width;
						if (mainImageWidth < const_min_mainimage_width) {
							var chartImageRectangle = element.FindName("ChartImageRectangle") as Rectangle;
							if (chartImageRectangle != null) {
								var margin = chartImageRectangle.Margin;
								chartImageRectangle.Margin = new Thickness(margin.Left + (const_min_mainimage_width - mainImageWidth), margin.Top, margin.Right, margin.Bottom);
							}
						}
					}
					if (m_Display.Type == DynamicDisplayType.DenseText && m_Display.IsDisplayHeaderImage()) {
						var headerRow = element.FindName("HeaderRowDefinition") as RowDefinition;
						if (headerRow != null) {
							headerRow.MinHeight = 55;
						}
					}
					break;
				case DynamicDisplayType.RightComment:
					break;
				case DynamicDisplayType.Advice:
					break;
				case DynamicDisplayType.Simple:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		/// <summary>
		/// Updates the header area.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="borderColor">Color of the border.</param>
		/// <param name="startColor">The start color.</param>
		/// <param name="endColor">The end color.</param>
		private void UpdateHeaderArea(FrameworkElement element, System.Drawing.Color startColor, System.Drawing.Color borderColor, System.Drawing.Color endColor) {
			//Set Header Image
			var headerText = element.FindName("HeaderText") as FlowDocumentScrollViewer;
			var headerImage = element.FindName("HeaderImage") as Shape;
			if (m_Display.IsDisplayHeader() && (!string.IsNullOrWhiteSpace((m_Display.HeaderText ?? "").Replace("&nbsp;", ""))) || m_Display.IsDisplayHeaderImage()) {
				if (headerImage != null) {
					if (m_Display.IsDisplayHeaderImage()) {
						var document = m_Display.HeaderPictureImage.Image;
						var ms = document.GetStream();
						var imageBrush = ImageBrushFromStream(ms);
						headerImage.Fill = imageBrush;
						headerImage.Stroke = new SolidColorBrush(ToWindowsMediaColor(borderColor));
						var headerImageBackground = element.FindName("HeaderImageBackground") as Shape;
						if (headerImageBackground != null) {
							headerImageBackground.Fill = new SolidColorBrush(ToWindowsMediaColor(endColor));
						}
					}
					else {
						HideHeaderImage(element, headerText, headerImage, false);
					}
				}
				if (headerText != null) {
					InsertXamlFromHtml(headerText, m_Display.HeaderText, m_Display.HeaderTextDefaultFontFamily, m_Display.HeaderTextDefaultFontSize, m_Display.HeaderTextDefaultFontColor);
				}
			}
			else {
				HideHeaderImage(element, headerText, headerImage, true);
				HideHeader(element);
			}
		}

		private void HideHeader(FrameworkElement element) {
			var headerColumn = element.FindName("MainGridColDef2") as ColumnDefinition;
			if (headerColumn != null) {
				headerColumn.Width = new GridLength(0);
			}
			var headerRow = element.FindName("HeaderRowDefinition") as RowDefinition;
			if (headerRow != null) {
				headerRow.Height = new GridLength(0);
				headerRow.MinHeight = 0;
			}
		}

		private Color ToWindowsMediaColor(System.Drawing.Color color) {
			var retVal = new Color { A = color.A, B = color.B, G = color.G, R = color.R };
			return retVal;
		}

		/// <summary>
		/// Hides the header text.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="headerText">The header text.</param>
		private void HideHeaderText(FrameworkElement element, FrameworkElement headerText) {
			if (headerText != null && m_Display.Type == DynamicDisplayType.BottomComment) {
				headerText.Visibility = Visibility.Hidden;
				var mainRect = element.FindName("MainRectangle") as Rectangle;
				var externalRect = element.FindName("ExternalRectangle") as Rectangle;
				if (mainRect != null) {
					Grid.SetRow(mainRect, Grid.GetRow(mainRect) - 1);
					Grid.SetRowSpan(mainRect, Grid.GetRowSpan(mainRect) + 1);
				}
				if (externalRect != null) {
					Grid.SetRow(externalRect, Grid.GetRow(externalRect) - 1);
					Grid.SetRowSpan(externalRect, Grid.GetRowSpan(externalRect) + 1);
				}
				var mainDockPanel = element.FindName("MainDockPanel") as DockPanel;
				if (mainDockPanel != null) {
					Grid.SetRow(mainDockPanel, Grid.GetRow(mainDockPanel) - 1);
					Grid.SetRowSpan(mainDockPanel, Grid.GetRowSpan(mainDockPanel) + 1);
				}
			}
		}

		/// <summary>
		/// Hides the header image.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="headerText">The header text.</param>
		/// <param name="headerImageEllipse">The header image ellipse.</param>
		/// <param name="isHideHeaderText">if set to <c>true</c> [hides header text].</param>
		private void HideHeaderImage(FrameworkElement element, FrameworkElement headerText, Shape headerImageEllipse, bool isHideHeaderText) {
			if (headerImageEllipse != null)
				headerImageEllipse.Visibility = Visibility.Hidden;
			var headerImageBackground = element.FindName("HeaderImageBackground") as Shape;
			if (headerImageBackground != null)
				headerImageBackground.Visibility = Visibility.Hidden;
			var numColumnsLeft = 0;
			switch (m_Display.Type) {
				case DynamicDisplayType.Tile:
				case DynamicDisplayType.DenseText:
					numColumnsLeft = 2;
					break;
				case DynamicDisplayType.BottomComment:
					numColumnsLeft = 1;
					break;
			}
			if (numColumnsLeft > 0) {
				Grid.SetColumn(headerText, Grid.GetColumn(headerText) - numColumnsLeft);
				Grid.SetColumnSpan(headerText, Grid.GetColumnSpan(headerText) + numColumnsLeft);
				var mainImage = element.FindName("MainImage") as Shape;
				if (mainImage != null) {
					var margin = mainImage.Margin;
					mainImage.Margin = new Thickness(margin.Left, margin.Top - 50, margin.Right, margin.Bottom);
				}
			}
			if (string.IsNullOrWhiteSpace(m_Display.HeaderText) || isHideHeaderText) {
				HideHeaderText(element, headerText);
			}

		}

		/// <summary>
		/// Updates the footer area.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="borderColor">Color of the border.</param>
		/// <param name="endColor">The end color.</param>
		private void UpdateFooterArea(FrameworkElement element, System.Drawing.Color borderColor, System.Drawing.Color endColor) {
			var footerText = element.FindName("FooterText") as FlowDocumentScrollViewer;
			if (m_Display.IsDisplayFooter()) {
				var footerPicture = element.FindName("FooterPicture") as Rectangle;
				if (footerText != null && !string.IsNullOrWhiteSpace(m_Display.FooterText)) {
					InsertXamlFromHtml(footerText, m_Display.FooterText, m_Display.FooterTextDefaultFontFamily, m_Display.FooterTextDefaultFontSize, m_Display.FooterTextDefaultFontColor);
				}
				else {
					if (footerPicture == null || m_Display.FooterPictureImage == null) {
						HideFooter(element);
					}
				}
				var footerRect = element.FindName("FooterRectangle") as Rectangle;
				if (footerRect != null) {
					if (footerRect.Fill != null)
						footerRect.Fill = new SolidColorBrush(ToWindowsMediaColor(endColor));
					footerRect.Stroke = new SolidColorBrush(ToWindowsMediaColor(borderColor));
				}
				if (footerPicture != null)
					if (m_Display.FooterPictureImage != null) {
						var document = m_Display.FooterPictureImage.Image;
						var ms = document.GetStream();
						var imageBrush = ImageBrushFromStream(ms);
						footerPicture.Fill = imageBrush;
					}
					else {
						footerPicture.Visibility = Visibility.Hidden;
					}
			}
			else {
				HideFooter(element);
			}
		}

		private void HideFooter(FrameworkElement element) {
			var footerRowDefinition = element.FindName("FooterRowDefinition") as RowDefinition;
			if (footerRowDefinition != null) {
				footerRowDefinition.Height = new GridLength(0);
			}
		}

		/// <summary>
		/// Updates the main area.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="endColor">The end color.</param>
		/// <param name="borderColor">Color of the border.</param>
		/// <param name="startColor">The start color.</param>
		private void UpdateMainArea(FrameworkElement element, System.Drawing.Color startColor, System.Drawing.Color? borderColor, System.Drawing.Color? endColor) {
			var mainGrid = element.FindName("MainGrid") as Grid;
			if (mainGrid != null) {
				//Set Main Rectangle
				var mainRect = element.FindName("MainRectangle") as Rectangle;
				if (mainRect != null) {
					var mainRectBrush = mainRect.Fill as LinearGradientBrush;
					if (mainRectBrush != null) {
						mainRectBrush.GradientStops[0].Color = ToWindowsMediaColor(startColor);
						if (endColor.HasValue)
							mainRectBrush.GradientStops[1].Color = ToWindowsMediaColor(endColor.Value);
					}
					if (borderColor.HasValue && mainRect.Stroke != null)
						mainRect.Stroke = new SolidColorBrush(ToWindowsMediaColor(borderColor.Value));
				}
				if (m_Display.IsDisplayMainImage()) {
					//Set Main Image
					var mainImage = element.FindName("MainImage") as Image;
					var document = m_Display.MainPictureImage.Image;
					var ms = document.GetStream();
					var bitmapImage = BitmapImageFromStream(ms);
					var imageWidth = bitmapImage.Width - 100;
					var imageHeight = bitmapImage.Height - 100;
					var imageBrush = new ImageBrush(bitmapImage);

					if (mainImage != null) {
						mainImage.Source = imageBrush.ImageSource;
						if (m_Display.MainPictureSize != null) {
							mainImage.Width = Math.Max(0, m_Display.MainPictureSize.Value * m_Width / 100);
							mainImage.Height = (mainImage.Width / imageWidth) * imageHeight;
						}
					}
					else {
						var mainImageRect = element.FindName("MainImage") as Rectangle;
						if (mainImageRect != null) {
							mainImageRect.Fill = imageBrush;
							if (m_Display.MainPictureSize != null && m_Display.Type != DynamicDisplayType.Simple) {
								mainImageRect.Width = Math.Max(0, m_Display.MainPictureSize.Value * m_Width / 100);
								mainImageRect.Height = (mainImageRect.Width / imageWidth) * imageHeight;
							}
						}
					}
				}
				else {
					var mainImage = element.FindName("MainImage") as FrameworkElement;
					if (mainImage != null) {
						mainImage.Width = 0;
						mainImage.Margin = new Thickness(0);
						mainImage.Visibility = Visibility.Collapsed;
					}
				}

				if (!m_Display.MainChartVisible) {
					var columnDefinition = element.FindName("ChartAndMainTextGridColDef1") as ColumnDefinition;
					if (columnDefinition != null) {
						columnDefinition.Width = new GridLength(0);
					}
				}

				//Set Main Text
				m_Display.MainText = m_Display.MainText ?? "";
				if (!string.IsNullOrWhiteSpace(m_Display.MainText)) {
					var mainText = element.FindName("MainText") as FlowDocumentScrollViewer;
					if (mainText != null) {
						InsertXamlFromHtml(mainText, m_Display.MainText, m_Display.MainTextDefaultFontFamily, m_Display.MainTextDefaultFontSize, m_Display.MainTextDefaultFontColor);
					}
				}
				else {
					var columnDefinition = element.FindName("ChartAndMainTextGridColDef2") as ColumnDefinition;
					if (columnDefinition != null) {
						columnDefinition.Width = new GridLength(0);
					}
				}

				var fixedHeight = element.FindName("FixedHeight") as Rectangle;
				if (fixedHeight != null && fixedHeight.Height < m_Height) {
					m_Height = (int)fixedHeight.Height;
				}
			}
		}

		private void InsertXamlFromHtml(FlowDocumentScrollViewer flowDocumentScrollViewer, string htmlString, string defaultFontFamily, string defaultFontSize, string defaultFontColor) {
			htmlString = string.Format("<font color='{0}' face='{1}' size='{2}'>{3}</font>",
									   string.IsNullOrWhiteSpace(defaultFontColor) ? const_defaultfontcolor : defaultFontColor,
									   string.IsNullOrWhiteSpace(defaultFontFamily) ? const_defaultfontfamily : defaultFontFamily,
									   string.IsNullOrWhiteSpace(defaultFontSize) ? const_defaultfontsize : defaultFontSize,
									   htmlString);
			var xaml = HtmlToXamlConverter.ConvertHtmlToXaml(htmlString, true);
			var rootObject = XamlReader.Parse(xaml) as FlowDocument;
			if (rootObject != null) {
				if (flowDocumentScrollViewer.Document != null) {
					rootObject.TextAlignment = flowDocumentScrollViewer.Document.TextAlignment;
				}
				rootObject.LineHeight = 1;
				flowDocumentScrollViewer.Document = rootObject;
			}
		}
	}
}

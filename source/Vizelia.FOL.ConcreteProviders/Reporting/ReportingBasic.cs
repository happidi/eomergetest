﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using System.Collections;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Implementation of IReporting for standard csv.
	/// </summary>
	class ReportingBasic : IReporting {



		/// <summary>
		/// The list of columns of the report.
		/// </summary>
		public IList<Column> Columns {
			get;
			set;
		}

		/// <summary>
		/// Renders the report as a stream.
		/// Creates a basic CSV report that can be opened in Excel.
		/// </summary>
		/// <param name="data">The report data.</param>
		/// <param name="mimeType">The resulting mime type.</param>
		/// <param name="extension">The resulting extension of the file which content is returned as a stream.</param>
		/// <returns>The report rendered as a stream.</returns>
		public MemoryStream RenderReport(IList data, out string mimeType, out string extension) {
			mimeType = MimeType.Excel;
			extension = "csv";
			string separator = Helper.LocalizeText("langue_csv_value_separator") ?? ";";
			var builder = new StringBuilder();
			if (Columns != null) {
				var headers = (from col in Columns
							   where String.IsNullOrEmpty(col.dataIndex) == false
							   select col).ToList();

				var headerLine = String.Join(separator, headers.Select(col => FormatCSV(col.header)).ToArray());
				builder.AppendLine(headerLine);

				foreach (BaseBusinessEntity r in data) {
					string[] values = headers.Select(col => FormatCSV(r.GetPropertyValue(col.dataIndex))).ToArray();
					var dataLine = String.Join(separator, values);
					builder.AppendLine(dataLine);
				}
			}
			return builder.ToString().GetStream();
		}

		/// <summary>
		/// Formats the value to CSV.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		private static string FormatCSV(object value) {
			string stringValue;
			value = value ?? "";

			if (value is DateTime) {
				//TODO: implement CurrentUserTimeZone. As it is right now - will always return UTC
				TimeZoneInfo currentUserTimeZone = TimeZoneHelper.CurrentUserTimeZone;
				stringValue = TimeZoneInfo.ConvertTimeFromUtc((DateTime)value, currentUserTimeZone).ToString("yyyy-MM-dd HH:mm:ss");
			}
			else if (value is Enum) {
				stringValue = (value as Enum).GetLocalizedText();
			}
			else {
				stringValue = value.ToString();
			}

			return "\"" + stringValue.Replace("\"", "\"\"") + "\"";
		}

		/// <summary>
		/// The title of the report.
		/// </summary>
		public string Title {
			get;
			set;
		}

		/// <summary>
		/// Physical path for logo.
		/// Can be left blank when no logo is associated with the report.
		/// </summary>
		public string LogoPath {
			get;
			set;
		}

		/// <summary>
		/// The format of dates.
		/// </summary>
		public string FormatDate {
			get;
			set;
		}
	}
}

using System;
using System.Collections.Generic;
using System.Globalization;
using Vizelia.FOL.BusinessEntities;
using ReportDefinition = Vizelia.FOL.XSD.ReportDefinition;

namespace Vizelia.FOL.ConcreteProviders {
	class RdlTableGenerator {
		private IList<Column> m_fields;
		private string m_formatDate;
		private ExportType m_exportType;

		public RdlTableGenerator(IList<Column> fields, string formatDate, ExportType exportType) {
			m_fields = fields;
			m_formatDate = formatDate;
			m_exportType = exportType;
		}

		public ReportDefinition.TableType CreateTable() {

			ReportDefinition.TableType table = new ReportDefinition.TableType();
			table.Name = "Table1";

			table.Items = new object[] {
					RdlcGenerator.const_datasourcename,
                    CreateTableColumns(),
                    CreateHeader(),
                    CreateDetails()
                };
			table.ItemsElementName = new ReportDefinition.ItemsChoiceType21[] {
					ReportDefinition.ItemsChoiceType21.DataSetName,
					ReportDefinition.ItemsChoiceType21.TableColumns,
                    ReportDefinition.ItemsChoiceType21.Header,
                    ReportDefinition.ItemsChoiceType21.Details,
					
                };
			return table;
		}

		private ReportDefinition.HeaderType CreateHeader() {
			ReportDefinition.HeaderType header = new ReportDefinition.HeaderType();
			header.Items = new object[]
                {
                    CreateHeaderTableRows(),
					true
                };
			header.ItemsElementName = new ReportDefinition.ItemsChoiceType20[]
                {
                    ReportDefinition.ItemsChoiceType20.TableRows,
					ReportDefinition.ItemsChoiceType20.RepeatOnNewPage
                };
			return header;
		}

		private ReportDefinition.TableRowsType CreateHeaderTableRows() {
			ReportDefinition.TableRowsType headerTableRows = new ReportDefinition.TableRowsType();
			if (m_exportType == ExportType.Excel2007 || m_exportType == ExportType.ExcelBasic)
				headerTableRows.TableRow = new ReportDefinition.TableRowType[] { CreateHeaderTableRow(false) };
			else
				headerTableRows.TableRow = new ReportDefinition.TableRowType[] { CreateHeaderTableRow(true), CreateHeaderTableRow(false) };
			return headerTableRows;
		}

		private ReportDefinition.TableRowType CreateHeaderTableRow(bool empty) {
			ReportDefinition.TableRowType headerTableRow = new ReportDefinition.TableRowType();
			headerTableRow.Items = new object[] { CreateHeaderTableCells(empty), "0.20in" };
			return headerTableRow;
		}

		private ReportDefinition.TableCellsType CreateHeaderTableCells(bool empty) {
			ReportDefinition.TableCellsType headerTableCells = new ReportDefinition.TableCellsType();
			headerTableCells.TableCell = new ReportDefinition.TableCellType[m_fields.Count];
			for (int i = 0; i < m_fields.Count; i++) {
				headerTableCells.TableCell[i] = CreateHeaderTableCell(empty, m_fields[i]);
			}
			return headerTableCells;
		}

		private ReportDefinition.TableCellType CreateHeaderTableCell(bool empty, Column col) {
			ReportDefinition.TableCellType headerTableCell = new ReportDefinition.TableCellType();
			headerTableCell.Items = new object[] { CreateHeaderTableCellReportItems(empty, col) };
			return headerTableCell;
		}

		private ReportDefinition.ReportItemsType CreateHeaderTableCellReportItems(bool empty, Column col) {
			ReportDefinition.ReportItemsType headerTableCellReportItems = new ReportDefinition.ReportItemsType();
			headerTableCellReportItems.Items = new object[] { CreateHeaderTableCellTextbox(empty, col) };
			return headerTableCellReportItems;
		}

		private ReportDefinition.TextboxType CreateHeaderTableCellTextbox(bool empty, Column col) {
			ReportDefinition.TextboxType headerTableCellTextbox = new ReportDefinition.TextboxType();
			if (empty)
				headerTableCellTextbox.Name = col.uniqueName + "_blank_Header";
			else
				headerTableCellTextbox.Name = col.uniqueName + "_Header";

			if (empty) {
				headerTableCellTextbox.Items = new object[] 
					{
                    ""
                    };
				headerTableCellTextbox.ItemsElementName = new ReportDefinition.ItemsChoiceType14[] 
					{
                    ReportDefinition.ItemsChoiceType14.Value
                    };
			}
			else {
				headerTableCellTextbox.Items = new object[] 
					{
                    col.header,
                    CreateHeaderTableCellTextboxStyle(),
                    true
					};
				headerTableCellTextbox.ItemsElementName = new ReportDefinition.ItemsChoiceType14[] 
					{
                    ReportDefinition.ItemsChoiceType14.Value,
                    ReportDefinition.ItemsChoiceType14.Style,
                    ReportDefinition.ItemsChoiceType14.CanGrow
					};
			}
			return headerTableCellTextbox;
		}

		private ReportDefinition.StyleType CreateHeaderTableCellTextboxStyle() {
			ReportDefinition.StyleType headerTableCellTextboxStyle = new ReportDefinition.StyleType();
			headerTableCellTextboxStyle.Items = new object[]
                {
                    "700",
                    //"10pt",
					"ForestGreen",
					"White",
					new ReportDefinition.BorderColorStyleWidthType {
						Items = new object[] {
							"Solid"
						},
						ItemsElementName = new ReportDefinition.ItemsChoiceType3[] {
							ReportDefinition.ItemsChoiceType3.Default
						}
					},
					"2pt",
					"2pt",
					new ReportDefinition.BorderColorStyleWidthType {
						Items = new object[] {
							"Gray"
						},
						ItemsElementName = new ReportDefinition.ItemsChoiceType3[] {
							ReportDefinition.ItemsChoiceType3.Default
						}
					},
					new ReportDefinition.BorderColorStyleWidthType {
						Items = new object[] {
							"0.5pt"
						},
						ItemsElementName = new ReportDefinition.ItemsChoiceType3[] {
							ReportDefinition.ItemsChoiceType3.Default
						}
					}

                };
			headerTableCellTextboxStyle.ItemsElementName = new ReportDefinition.ItemsChoiceType5[]
                {
                    ReportDefinition.ItemsChoiceType5.FontWeight,
                    //ReportDefinition.ItemsChoiceType5.FontSize,
					ReportDefinition.ItemsChoiceType5.BackgroundColor,
					ReportDefinition.ItemsChoiceType5.Color,
					ReportDefinition.ItemsChoiceType5.BorderStyle,
					ReportDefinition.ItemsChoiceType5.PaddingLeft,
					ReportDefinition.ItemsChoiceType5.PaddingRight,
					ReportDefinition.ItemsChoiceType5.BorderColor,
					ReportDefinition.ItemsChoiceType5.BorderWidth
                };
			return headerTableCellTextboxStyle;
		}

		private ReportDefinition.DetailsType CreateDetails() {
			ReportDefinition.DetailsType details = new ReportDefinition.DetailsType();
			details.Items = new object[] { CreateTableRows() };
			return details;
		}

		private ReportDefinition.TableRowsType CreateTableRows() {
			ReportDefinition.TableRowsType tableRows = new ReportDefinition.TableRowsType();
			tableRows.TableRow = new ReportDefinition.TableRowType[] { CreateTableRow() };
			return tableRows;
		}

		private ReportDefinition.TableRowType CreateTableRow() {
			ReportDefinition.TableRowType tableRow = new ReportDefinition.TableRowType();
			tableRow.Items = new object[] { CreateTableCells(), "0.20in" };
			return tableRow;
		}

		private ReportDefinition.TableCellsType CreateTableCells() {
			ReportDefinition.TableCellsType tableCells = new ReportDefinition.TableCellsType();
			tableCells.TableCell = new ReportDefinition.TableCellType[m_fields.Count];
			for (int i = 0; i < m_fields.Count; i++) {
				tableCells.TableCell[i] = CreateTableCell(m_fields[i]);
			}
			return tableCells;
		}

		private ReportDefinition.TableCellType CreateTableCell(Column c) {
			ReportDefinition.TableCellType tableCell = new ReportDefinition.TableCellType();
			tableCell.Items = new object[] { CreateTableCellReportItems(c) };
			return tableCell;
		}

		private ReportDefinition.ReportItemsType CreateTableCellReportItems(Column c) {
			ReportDefinition.ReportItemsType reportItems = new ReportDefinition.ReportItemsType();
			reportItems.Items = new object[] { CreateTableCellTextbox(c) };
			return reportItems;
		}

		private ReportDefinition.TextboxType CreateTableCellTextbox(Column c) {
			ReportDefinition.TextboxType textbox = new ReportDefinition.TextboxType();
			textbox.Name = c.uniqueName;
			textbox.Items = new object[] 
                {
                    "=Fields!" + c.dataIndex + ".Value",
                    CreateTableCellTextboxStyle(c),
                    true
                };
			textbox.ItemsElementName = new ReportDefinition.ItemsChoiceType14[] 
                {
                    ReportDefinition.ItemsChoiceType14.Value,
                    ReportDefinition.ItemsChoiceType14.Style,
                    ReportDefinition.ItemsChoiceType14.CanGrow
                };
			return textbox;
		}

		private ReportDefinition.StyleType CreateTableCellTextboxStyle(Column c) {
			ReportDefinition.StyleType style = new ReportDefinition.StyleType();

			string formatType;
			string textAlign;

			switch (c.dataType) {

				case TypeCode.Decimal:
				case TypeCode.Double:
				case TypeCode.Single:
					formatType = "N";
					textAlign = "Right";
					break;

				case TypeCode.Int16:
				case TypeCode.Int32:
				case TypeCode.Int64:
				case TypeCode.UInt16:
				case TypeCode.UInt32:
				case TypeCode.UInt64:
					formatType = "D0";
					textAlign = "Right";
					break;

				case TypeCode.DateTime:
					formatType = m_formatDate;
					textAlign = "Right";
					break;

				default:
					formatType = "";
					textAlign = "Left";
					break;
			}


			style.Items = new object[]
                {
					"8pt",
					"Arial Unicode MS", 
					"100",
                    "=iif(RowNumber(Nothing) mod 2, \"White\", \"WhiteSmoke\")",
                    textAlign,
					new ReportDefinition.BorderColorStyleWidthType {
						Items = new object[] {
							"Solid"
						},
						ItemsElementName = new ReportDefinition.ItemsChoiceType3[] {
							ReportDefinition.ItemsChoiceType3.Default
						}
					},
					"2pt",
					"2pt",
					new ReportDefinition.BorderColorStyleWidthType {
						Items = new object[] {
							"Gray"
						},
						ItemsElementName = new ReportDefinition.ItemsChoiceType3[] {
							ReportDefinition.ItemsChoiceType3.Default
						}
					},
					new ReportDefinition.BorderColorStyleWidthType {
						Items = new object[] {
							"0.5pt"
						},
						ItemsElementName = new ReportDefinition.ItemsChoiceType3[] {
							ReportDefinition.ItemsChoiceType3.Default
						}
					}

                };
			style.ItemsElementName = new ReportDefinition.ItemsChoiceType5[]
                {
					ReportDefinition.ItemsChoiceType5.FontSize,
					ReportDefinition.ItemsChoiceType5.FontFamily,
					ReportDefinition.ItemsChoiceType5.FontWeight,
                    ReportDefinition.ItemsChoiceType5.BackgroundColor,
                    ReportDefinition.ItemsChoiceType5.TextAlign,
					ReportDefinition.ItemsChoiceType5.BorderStyle,
					ReportDefinition.ItemsChoiceType5.PaddingLeft,
					ReportDefinition.ItemsChoiceType5.PaddingRight,
					ReportDefinition.ItemsChoiceType5.BorderColor,
					ReportDefinition.ItemsChoiceType5.BorderWidth
					
                };

			if (formatType.Length > 0) {
				var items = style.Items;
				var itemsElementName = style.ItemsElementName;
				Array.Resize(ref items, items.Length + 1);
				Array.Resize(ref itemsElementName, itemsElementName.Length + 1);
				items[items.Length - 1] = formatType;
				itemsElementName[itemsElementName.Length - 1] = ReportDefinition.ItemsChoiceType5.Format;
				style.Items = items;
				style.ItemsElementName = itemsElementName;
			}

			return style;
		}

		private ReportDefinition.TableColumnsType CreateTableColumns() {
			ReportDefinition.TableColumnsType tableColumns = new ReportDefinition.TableColumnsType();
			tableColumns.TableColumn = new ReportDefinition.TableColumnType[m_fields.Count];
			for (int i = 0; i < m_fields.Count; i++) {
				tableColumns.TableColumn[i] = CreateTableColumn(m_fields[i].width);
			}
			return tableColumns;
		}

		private ReportDefinition.TableColumnType CreateTableColumn(double width) {
			ReportDefinition.TableColumnType tableColumn = new ReportDefinition.TableColumnType();
			//tableColumn.Items = new object[] { "2in" };
			NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
			tableColumn.Items = new object[] { width.ToString("N", nfi) + "in" };

			return tableColumn;
		}
	}
}

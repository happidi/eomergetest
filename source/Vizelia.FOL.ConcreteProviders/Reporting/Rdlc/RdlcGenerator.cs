using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;
using System.Text;
using System.Web.Security;
using System.Xml.Serialization;
using Microsoft.Reporting.WebForms;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using ReportDefinition = Vizelia.FOL.XSD.ReportDefinition;
using ReportParameter = Microsoft.Reporting.WebForms.ReportParameter;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Creates and renders dynamically a rdlc report 
	/// </summary>
	internal class RdlcGenerator {
		private double m_width;
		private double m_height;
		private FormatPageType m_format;
		private Dictionary<string, object> m_dataSources;


		// We force the culture to en-US, so that numerical values will always be displayed with a dot as decimal separator.
		private NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

		public const string const_datasourcename = "MyData";

		public IList<Column> Columns { get; set; }

		/// <summary>
		/// Format of the report (Landscape or Portrait).
		/// Setting this property will update Width and Height of the report.
		/// The default value is initialized as Portrait in the ctor of the class.
		/// </summary>
		public FormatPageType Format {
			get { return m_format; }
			set {
				m_format = value;
				switch (value) {
					case FormatPageType.A4Landscape:
						m_width = 11;
						m_height = 8.5;
						break;
					case FormatPageType.A4Portrait:
						m_width = 8.5;
						m_height = 11;
						break;
					case FormatPageType.A3Landscape:
						m_width = 17;
						m_height = 11;
						break;
					case FormatPageType.A3Portrait:
						m_width = 11;
						m_height = 17;
						break;
				}
			}
		}

		/// <summary>
		/// Export type of the report.
		/// The default value is initialized as PDF in the ctor of the class.
		/// </summary>
		public ExportType ExportAs { get; set; }

		/// <summary>
		/// Height (in inch) of the report.
		/// The property is automatically calculed depending on the format value of the report.
		/// </summary>
		public double Height {
			get { return m_height; }
		}

		/// <summary>
		/// Width (in inch) of the report.
		/// The property is automatically calculed depending on the format value of the report.
		/// </summary>
		public double Width {
			get { return m_width; }
		}

		/// <summary>
		/// Title of the report
		/// </summary>
		public string Title { get; set; }

		/// <summary>
		/// Path of the logo to include in the report.
		/// </summary>
		public string LogoPath { get; set; }


		/// <summary>
		/// Format of the dates.
		/// </summary>
		public string FormatDate { get; set; }

		/// <summary>
		/// Public ctor
		/// </summary>
		public RdlcGenerator() {
			Format = FormatPageType.A4Portrait;
			ExportAs = ExportType.Pdf;
			Title = "";
		}

		private ReportDefinition.Report CreateReport() {
			ReportDefinition.Report report = new ReportDefinition.Report {
				Items = new object[] {
			                                                             	                     	CreateDataSources(),
			                                                             	                     	CreateBody(),
			                                                             	                     	CreateDataSets(),
			                                                             	                     	String.Format("{0}in", (Width - 1).ToString("f", nfi)), //"6.5in",
			                                                             	                     	CreatePageHeader(),
			                                                             	                     	CreatePageFooter()
			                                                             	                     },
				ItemsElementName = new[] {
			                                                             	                         	ReportDefinition.ItemsChoiceType37.DataSources,
			                                                             	                         	ReportDefinition.ItemsChoiceType37.Body,
			                                                             	                         	ReportDefinition.ItemsChoiceType37.DataSets,
			                                                             	                         	ReportDefinition.ItemsChoiceType37.Width,
			                                                             	                         	ReportDefinition.ItemsChoiceType37.PageHeader,
			                                                             	                         	ReportDefinition.ItemsChoiceType37.PageFooter
			                                                             	                         }
			};
			return report;
		}

		private ReportDefinition.DataSourcesType CreateDataSources() {
			ReportDefinition.DataSourcesType dataSources = new ReportDefinition.DataSourcesType { DataSource = new[] { CreateDataSource() } };

			return dataSources;
		}

		private ReportDefinition.DataSourceType CreateDataSource() {
			ReportDefinition.DataSourceType dataSource = new ReportDefinition.DataSourceType { Name = "DummyDataSource", Items = new object[] { CreateConnectionProperties() } };
			return dataSource;
		}

		private ReportDefinition.ConnectionPropertiesType CreateConnectionProperties() {
			ReportDefinition.ConnectionPropertiesType connectionProperties = new ReportDefinition.ConnectionPropertiesType {
				Items = new object[] {
			                                                                                                               	                     	"",
			                                                                                                               	                     	"SQL"
			                                                                                                               	                     },
				ItemsElementName = new[] {
			                                                                                                               	                         	ReportDefinition.ItemsChoiceType.ConnectString,
			                                                                                                               	                         	ReportDefinition.ItemsChoiceType.DataProvider
			                                                                                                               	                         }
			};
			return connectionProperties;
		}

		private ReportDefinition.BodyType CreateBody() {
			ReportDefinition.BodyType body = new ReportDefinition.BodyType {
				Items = new object[] {
			                                                               	                     	CreateReportItems(),
			                                                               	                     	"1in"
			                                                               	                     },
				ItemsElementName = new[] {
			                                                               	                         	ReportDefinition.ItemsChoiceType30.ReportItems,
			                                                               	                         	ReportDefinition.ItemsChoiceType30.Height
			                                                               	                         }
			};
			return body;
		}

		private ReportDefinition.ReportItemsType CreateReportItems() {
			ReportDefinition.ReportItemsType reportItems = new ReportDefinition.ReportItemsType();
			double totalWidth = Columns.Sum(p => p.width);
			// case we have no width for columns
			if (totalWidth == 0) {
				foreach (var col in Columns) {
					col.width = 2;
				}
			}
			else {
				foreach (var col in Columns) {
					col.width = (col.width / totalWidth) * (Width - 0.4);
				}
			}
			RdlTableGenerator tableGen = new RdlTableGenerator(Columns, FormatDate, ExportAs);
			reportItems.Items = new object[] { tableGen.CreateTable() };
			return reportItems;
		}

		private ReportDefinition.DataSetsType CreateDataSets() {
			ReportDefinition.DataSetsType dataSets = new ReportDefinition.DataSetsType { DataSet = new[] { CreateDataSet() } };
			return dataSets;
		}

		private ReportDefinition.DataSetType CreateDataSet() {
			ReportDefinition.DataSetType dataSet = new ReportDefinition.DataSetType { Name = const_datasourcename, Items = new object[] { CreateQuery(), CreateFields() } };
			return dataSet;
		}

		private ReportDefinition.QueryType CreateQuery() {
			ReportDefinition.QueryType query = new ReportDefinition.QueryType {
				Items = new object[] {
			                                                                  	                     	"DummyDataSource",
			                                                                  	                     	""
			                                                                  	                     },
				ItemsElementName = new[] {
			                                                                  	                         	ReportDefinition.ItemsChoiceType2.DataSourceName,
			                                                                  	                         	ReportDefinition.ItemsChoiceType2.CommandText
			                                                                  	                         }
			};
			return query;
		}

		private ReportDefinition.FieldsType CreateFields() {
			ReportDefinition.FieldsType fields = new ReportDefinition.FieldsType();
			List<Column> list = new List<Column>(Columns);
			// we make sure the fields are unique.
			var query = (from p in list
						 select p.dataIndex).Distinct().ToList();

			fields.Field = new ReportDefinition.FieldType[query.Count];
			for (int i = 0; i < query.Count; i++) {
				fields.Field[i] = CreateField(query[i]);
			}
			return fields;
		}

		private ReportDefinition.FieldType CreateField(string dataIndex) {
			ReportDefinition.FieldType field = new ReportDefinition.FieldType { Name = dataIndex, Items = new object[] { dataIndex }, ItemsElementName = new[] { ReportDefinition.ItemsChoiceType1.DataField } };
			return field;
		}

		private ReportDefinition.PageHeaderFooterType CreatePageHeader() {
			if (ExportAs == ExportType.Excel2007)
				return null;
			ReportDefinition.PageHeaderFooterType header = new ReportDefinition.PageHeaderFooterType {
				Items = new object[] {
			                                                                                         	                     	"0.6in",
			                                                                                         	                     	CreatePageHeaderItem(),
			                                                                                         	                     	true,
			                                                                                         	                     	true,
			                                                                                         	                     	new ReportDefinition.StyleType {
			                                                                                         	                     	                               	Items = new object[] {
			                                                                                         	                     	                               	                     	new ReportDefinition.BorderColorStyleWidthType {
			                                                                                         	                     	                               	                     	                                               	Items = new object[] {"Solid"},
			                                                                                         	                     	                               	                     	                                               	ItemsElementName = new[] {ReportDefinition.ItemsChoiceType3.Bottom}
			                                                                                         	                     	                               	                     	                                               },
			                                                                                         	                     	                               	                     	new ReportDefinition.BorderColorStyleWidthType {
			                                                                                         	                     	                               	                     	                                               	Items = new object[] {"Gray"},
			                                                                                         	                     	                               	                     	                                               	ItemsElementName = new[] {ReportDefinition.ItemsChoiceType3.Default}
			                                                                                         	                     	                               	                     	                                               },
			                                                                                         	                     	                               	                     	new ReportDefinition.BorderColorStyleWidthType {
			                                                                                         	                     	                               	                     	                                               	Items = new object[] {"0.5pt"},
			                                                                                         	                     	                               	                     	                                               	ItemsElementName = new[] {ReportDefinition.ItemsChoiceType3.Default}
			                                                                                         	                     	                               	                     	                                               }
			                                                                                         	                     	                               	                     },
			                                                                                         	                     	                               	ItemsElementName = new[] {
			                                                                                         	                     	                               	                         	ReportDefinition.ItemsChoiceType5.BorderStyle,
			                                                                                         	                     	                               	                         	ReportDefinition.ItemsChoiceType5.BorderColor,
			                                                                                         	                     	                               	                         	ReportDefinition.ItemsChoiceType5.BorderWidth
			                                                                                         	                     	                               	                         }
			                                                                                         	                     	                               }
			                                                                                         	                     },
				ItemsElementName = new[] {
			                                                                                         	                         	ReportDefinition.ItemsChoiceType34.Height,
			                                                                                         	                         	ReportDefinition.ItemsChoiceType34.ReportItems,
			                                                                                         	                         	ReportDefinition.ItemsChoiceType34.PrintOnFirstPage,
			                                                                                         	                         	ReportDefinition.ItemsChoiceType34.PrintOnLastPage,
			                                                                                         	                         	ReportDefinition.ItemsChoiceType34.Style
			                                                                                         	                         }
			};

			return header;
		}

		private ReportDefinition.ReportItemsType CreatePageHeaderItem() {
			ReportDefinition.ReportItemsType reportItem = new ReportDefinition.ReportItemsType();

			ReportDefinition.ImageType image = new ReportDefinition.ImageType();
			string path = "";
			string imageType = "";

			image.Name = "image_logo_vizelia";

			
			if (!String.IsNullOrEmpty(LogoPath)) {
				var logoPathReplaced = LogoPath.Replace("{ApplicationName}", ContextHelper.ApplicationName);
				if (File.Exists(logoPathReplaced))
				{
					path = "file://" + (logoPathReplaced.Replace("\\", "/"));
					imageType = "image/" + Path.GetExtension(LogoPath);
				}
				else {
					LogoPath = Helper.GetPhysicalPath() + "images\\logo_se_180.bmp";
					path = "file://" + (LogoPath.Replace("\\", "/"));
					imageType = "image/" + Path.GetExtension(LogoPath);
				}
			}

			image.Items = new object[] {
			                           	ReportDefinition.ImageTypeSource.External,
			                           	imageType, // image/bmp
			                           	path, // file://c:/temp/logo.bmp
			                           	"0in",
			                           	String.Format("{0}in", (Width - 1.9).ToString("f", nfi)),
			                           	"1.4in",//"6cm",
			                           	ReportDefinition.ImageTypeSizing.FitProportional,

			                           	
			                           };

			image.ItemsElementName = new[] {
			                               	ReportDefinition.ItemsChoiceType15.Source,
			                               	ReportDefinition.ItemsChoiceType15.MIMEType,
			                               	ReportDefinition.ItemsChoiceType15.Value,
			                               	ReportDefinition.ItemsChoiceType15.Top,
			                               	ReportDefinition.ItemsChoiceType15.Left,
			                               	ReportDefinition.ItemsChoiceType15.Width,
			                               	ReportDefinition.ItemsChoiceType15.Sizing
			                               	
			                               };

			ReportDefinition.TextboxType textbox = new ReportDefinition.TextboxType {
				Name = "textbox_header_title",
				Items = new object[] {
			                                                                        	                     	String.IsNullOrEmpty(Title) ? " ": Title,
			                                                                        	                     	CreatePageHeaderTableCellTextboxStyle(),
			                                                                        	                     	true,
			                                                                        	                     	"0in",
			                                                                        	                     	"0in",
			                                                                        	                     	//String.Format("{0}in" , (Width-1).ToString("f",nfi)),
			                                                                        	                     	"0.3in"
			                                                                        	                     },
				ItemsElementName = new[] {
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Value,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Style,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.CanGrow,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Top,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Left,
			                                                                        	                         	//ReportDefinition.ItemsChoiceType14.Width,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Height
			                                                                        	                         }
			};
			ReportDefinition.TextboxType subTextbox = new ReportDefinition.TextboxType {
				Name = "subtextbox_header_title",
				Items = new object[] {
			                                                                        	                     	Helper.GetReportHeaderText(),
			                                                                        	                     	CreatePageHeaderTableCellSubTextboxStyle(),
			                                                                        	                     	true,
			                                                                        	                     	"0.4in",
			                                                                        	                     	"0in",
			                                                                        	                     	//String.Format("{0}in" , (Width-1).ToString("f",nfi)),
			                                                                        	                     	"0.2in"
			                                                                        	                     },
				ItemsElementName = new[] {
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Value,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Style,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.CanGrow,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Top,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Left,
			                                                                        	                         	//ReportDefinition.ItemsChoiceType14.Width,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Height
			                                                                        	                         }
			};
			reportItem.Items = !String.IsNullOrEmpty(LogoPath) ? new object[] { textbox, image, subTextbox } : new object[] { textbox, subTextbox };

			return reportItem;
		}

		private ReportDefinition.PageHeaderFooterType CreatePageFooter() {
			ReportDefinition.PageHeaderFooterType footer = new ReportDefinition.PageHeaderFooterType {
				Items = new object[] {
			                                                                                         	                     	"0.3in",
			                                                                                         	                     	CreatePageFooterItem(),
			                                                                                         	                     	true,
			                                                                                         	                     	true
			                                                                                         	                     	/*new ReportDefinition.StyleType {
			                                                                                         	                     	                               	Items = new object[] {
																																														new ReportDefinition.BorderColorStyleWidthType {
																																														                                                Items = new object[] {"Solid"},
																																														                                                ItemsElementName = new[] {ReportDefinition.ItemsChoiceType3.Top}
																																														                                               },
																																														new ReportDefinition.BorderColorStyleWidthType {
																																														                                                Items = new object[] {"Gray"},
																																														                                                ItemsElementName = new[] {ReportDefinition.ItemsChoiceType3.Default}
																																														                                               },
																																														new ReportDefinition.BorderColorStyleWidthType {
																																														                                                Items = new object[] {"0.5pt"},
																																														                                                ItemsElementName = new[] {ReportDefinition.ItemsChoiceType3.Default}
																																														                                               }
			                                                                                         	                     	                               	                     },
			                                                                                         	                     	                               	ItemsElementName = new[] {
			                                                                                         	                     	                               	                         	ReportDefinition.ItemsChoiceType5.BorderStyle,
			                                                                                         	                     	                               	                         	ReportDefinition.ItemsChoiceType5.BorderColor,
			                                                                                         	                     	                               	                         	ReportDefinition.ItemsChoiceType5.BorderWidth
			                                                                                         	                     	                               	                         }
			                                                                                         	                     	                               }
			                                                                                         	                     },*/
				},
				ItemsElementName = new[] {
			                                                                                         	                         	ReportDefinition.ItemsChoiceType34.Height,
			                                                                                         	                         	ReportDefinition.ItemsChoiceType34.ReportItems,
			                                                                                         	                         	ReportDefinition.ItemsChoiceType34.PrintOnFirstPage,
			                                                                                         	                         	ReportDefinition.ItemsChoiceType34.PrintOnLastPage,
			                                                                                         	                         //	ReportDefinition.ItemsChoiceType34.Style
			                                                                                         	                         }
			};

			return footer;
		}

		private ReportDefinition.ReportItemsType CreatePageFooterItem() {
			ReportDefinition.ReportItemsType reportItem = new ReportDefinition.ReportItemsType();

			ReportDefinition.TextboxType textbox = new ReportDefinition.TextboxType {
				Name = "textbox_footer_nbrepage",
				Items = new object[] {
			                                                                        	                     	@"=Globals!PageNumber & "" / "" &  Globals!TotalPages",
			                                                                        	                     	CreatePageFooterTableCellTextboxStyle(),
			                                                                        	                     	true,
			                                                                        	                     	"0in",
			                                                                        	                     	"0in" //String.Format("{0}in" , (Width - 0.5).ToString("f" , nfi))
			                                                                        	                     },
				ItemsElementName = new[] {
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Value,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Style,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.CanGrow,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Top,
			                                                                        	                         	ReportDefinition.ItemsChoiceType14.Left
			                                                                        	                         }
			};


			reportItem.Items = new object[] { textbox };
			return reportItem;
		}


		private ReportDefinition.StyleType CreatePageHeaderTableCellSubTextboxStyle() {
			ReportDefinition.StyleType headerTableCellTextboxStyle = new ReportDefinition.StyleType {
				Items = new object[] {
			                                                                                        	                     	"100",
			                                                                                        	                     	//"WhiteSmoke",
			                                                                                        	                     	"10pt",
			                                                                                        	                     	"Gray",
			                                                                                        	                     	"2pt",
			                                                                                        	                     	"2pt",
			                                                                                        	                     	"Left",
																																"Bottom"
			                                                                                        	                     },
				ItemsElementName = new[] {
			                                                                                        	                         	ReportDefinition.ItemsChoiceType5.FontWeight,
			                                                                                        	                         	ReportDefinition.ItemsChoiceType5.FontSize,
			                                                                                        	                         	//ReportDefinition.ItemsChoiceType5.BackgroundColor,
			                                                                                        	                         	ReportDefinition.ItemsChoiceType5.Color,
			                                                                                        	                         	ReportDefinition.ItemsChoiceType5.PaddingLeft,
			                                                                                        	                         	ReportDefinition.ItemsChoiceType5.PaddingRight,
			                                                                                        	                         	//ReportDefinition.ItemsChoiceType5.BorderStyle,
			                                                                                        	                         	//ReportDefinition.ItemsChoiceType5.BorderColor,
			                                                                                        	                         	//ReportDefinition.ItemsChoiceType5.BorderWidth,
			                                                                                        	                         	ReportDefinition.ItemsChoiceType5.TextAlign,
																																	ReportDefinition.ItemsChoiceType5.VerticalAlign

			                                                                                        	                         }
			};
			return headerTableCellTextboxStyle;
		}


		private ReportDefinition.StyleType CreatePageHeaderTableCellTextboxStyle() {
			ReportDefinition.StyleType headerTableCellTextboxStyle = new ReportDefinition.StyleType {
				Items = new object[] {
			                                                                                        	                     	"700",
			                                                                                        	                     	//"WhiteSmoke",
			                                                                                        	                     	"14pt",
			                                                                                        	                     	"Gray",
			                                                                                        	                     	"2pt",
			                                                                                        	                     	"2pt",
			                                                                                        	                     	"Left",
																																"Top"
			                                                                                        	                     },
				ItemsElementName = new[] {
			                                                                                        	                         	ReportDefinition.ItemsChoiceType5.FontWeight,
			                                                                                        	                         	ReportDefinition.ItemsChoiceType5.FontSize,
			                                                                                        	                         	//ReportDefinition.ItemsChoiceType5.BackgroundColor,
			                                                                                        	                         	ReportDefinition.ItemsChoiceType5.Color,
			                                                                                        	                         	ReportDefinition.ItemsChoiceType5.PaddingLeft,
			                                                                                        	                         	ReportDefinition.ItemsChoiceType5.PaddingRight,
			                                                                                        	                         	//ReportDefinition.ItemsChoiceType5.BorderStyle,
			                                                                                        	                         	//ReportDefinition.ItemsChoiceType5.BorderColor,
			                                                                                        	                         	//ReportDefinition.ItemsChoiceType5.BorderWidth,
			                                                                                        	                         	ReportDefinition.ItemsChoiceType5.TextAlign,
																																	ReportDefinition.ItemsChoiceType5.VerticalAlign
																																	
			                                                                                        	                         }
			};
			return headerTableCellTextboxStyle;
		}

		private ReportDefinition.StyleType CreatePageFooterTableCellTextboxStyle() {
			ReportDefinition.StyleType tableCellTextboxStyle = new ReportDefinition.StyleType {
				Items = new object[] {
			                                                                                  	                     	"100",
			                                                                                  	                     	"9pt",
			                                                                                  	                     	"2pt",
																														"Right"
			                                                                                  	                     },
				ItemsElementName = new[] {
			                                                                                  	                         	ReportDefinition.ItemsChoiceType5.FontWeight,
			                                                                                  	                         	ReportDefinition.ItemsChoiceType5.FontSize,
			                                                                                  	                         	ReportDefinition.ItemsChoiceType5.PaddingTop,
																															ReportDefinition.ItemsChoiceType5.TextAlign
			                                                                                  	                         }
			};
			return tableCellTextboxStyle;
		}

		private void WriteXml(Stream stream) {
			XmlSerializer serializer = new XmlSerializer(typeof(ReportDefinition.Report));
			serializer.Serialize(stream, CreateReport());
		}

		private byte[] RenderReport(LocalReport lr, ExportType exportType, out string extension, out string mimeType) {
			string outputFormat;
			string type;
			switch (exportType) {
				case ExportType.Pdf:
					type = "PDF";
					outputFormat = "PDF";
					break;
				case ExportType.Html:
					type = "HTML4.0";
					outputFormat = "HTML4.0";
					break;
				case ExportType.Excel2007:
					type = "EXCEL";
					outputFormat = "Excel";
					break;
				case ExportType.ImagePNG:
					type = "IMAGE";
					outputFormat = "BMP";
					break;
				default:
					throw new ArgumentException("Unrecognized type: " + exportType + ".  Type must be PDF, Excel, Image or HTML");
			}

			//The DeviceInfo settings should be changed based on the reportType
			//http://msdn2.microsoft.com/en-us/library/ms155397.aspx
			StringBuilder sb = new StringBuilder();
			sb.Append("<DeviceInfo>");
			sb.Append("<OutputFormat>");
			sb.Append(outputFormat);
			sb.Append("</OutputFormat>");

			sb.Append(String.Format("<PageWidth>{0}in</PageWidth>", Width.ToString("f", nfi)));
			sb.Append(String.Format("<PageHeight>{0}in</PageHeight>", Height.ToString("f", nfi)));
			sb.Append("<MarginTop>0.2in</MarginTop>");
			sb.Append("<MarginLeft>0.1in</MarginLeft>");
			sb.Append("<MarginRight>0.1in</MarginRight>");
			sb.Append("<MarginBottom>0.2in</MarginBottom>");
			sb.Append("</DeviceInfo>");
			string deviceInfo = sb.ToString();

			string encoding;
			Warning[] warnings;
			string[] streams;

			// Use reflection to hack the report and allow all the rendering extensions to be usable
			var m_previewService = lr.GetType().InvokeMember("m_previewService", System.Reflection.BindingFlags.GetField | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance, null, lr, null);
			IList m_ListRenderingExtensions = (IList)m_previewService.GetType().InvokeMember("ListRenderingExtensions", System.Reflection.BindingFlags.InvokeMethod | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance, null, m_previewService, null);
			foreach (var renderingExtension in m_ListRenderingExtensions) {
				renderingExtension.GetType().InvokeMember("m_isExposedExternally", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.SetField, null, renderingExtension, new object[] { true });
				renderingExtension.GetType().InvokeMember("m_isVisible", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.SetField, null, renderingExtension, new object[] { true });
			}
			
			lr.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
			////Render the report
			byte[] result = lr.Render(
				type,
				deviceInfo,
				out mimeType,
				out encoding,
				out extension,
				out streams,
				out warnings);

			return result;
		}

		/// <summary>
		/// Renders the report with data as a binary stream of byte[]
		/// </summary>
		/// <param name="data">The data of the report</param>
		/// <param name="extension">The extension of the rendered report (out param)</param>
		/// <param name="mimeType">The mime type of the rendered report (out param)</param>
		/// <returns>The binary stream of the report as a byte[]</returns>
		public MemoryStream RenderReport(object data, out string mimeType, out string extension) {
			ReportDataSource rds = new ReportDataSource {
				Name = const_datasourcename,
				Value = data
			};
			ExportType exportType = ExportAs;
			using (LocalReport lr = new LocalReport()) {
				MemoryStream streamDefinition = new MemoryStream();
				//using (MemoryStream streamDefinition = new MemoryStream()) {
				WriteXml(streamDefinition);
				streamDefinition.Position = 0;
				lr.LoadReportDefinition(streamDefinition);
				lr.DataSources.Add(rds);
				lr.EnableExternalImages = true;
				byte[] result = RenderReport(lr, exportType, out extension, out mimeType);
				MemoryStream m = new MemoryStream();
				m.Write(result, 0, result.Length);
				m.Position = 0;
				return m;
				//}
			}
		}

		/// <summary>
		/// Renders the report file.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="dataSources">The dictionary of data sources. Each entry in the dictionary should be a List of business entities.</param>
		/// <param name="reportParameters">The dictionary of report parameters. Each entry in the dictionary should be a string.</param>
		/// <param name="mimeType">Type of the MIME.</param>
		/// <param name="extension">The extension.</param>
		/// <returns></returns>
		public MemoryStream RenderReportFile(string reportPath, Dictionary<string, object> dataSources, Dictionary<string, object> reportParameters, out string mimeType, out string extension) {
			ExportType exportType = ExportAs;
			m_dataSources = dataSources;
			using (LocalReport lr = new LocalReport()) {
				lr.ReportPath = reportPath;
				lr.EnableExternalImages = true;
				lr.ShowDetailedSubreportMessages = true;
				lr.SubreportProcessing += LocalReport_SubreportProcessing;

				AddReportParameters(lr, reportParameters);
				AddDataSources(dataSources, lr);

				//lr.Refresh();
				byte[] result = RenderReport(lr, exportType, out extension, out mimeType);
				MemoryStream m = new MemoryStream();
				m.Write(result, 0, result.Length);
				m.Position = 0;
				return m;
			}
		}

		/// <summary>
		/// Adds the data sources.
		/// </summary>
		/// <param name="dataSources">The data sources.</param>
		/// <param name="lr">The report.</param>
		private void AddDataSources(Dictionary<string, object> dataSources, LocalReport lr) {
			var sources = lr.GetDataSourceNames();
			foreach (var dataSource in dataSources) {
				var index = sources.IndexOf(dataSource.Key);
				if (index > -1) {
					var rds = new ReportDataSource { Name = sources[index], Value = dataSource.Value };
					lr.DataSources.Add(rds);
				}
			}
		}

		/// <summary>
		/// Adds the report parameters.
		/// </summary>
		/// <param name="lr">The report.</param>
		/// <param name="reportParameters">The report parameters.</param>
		private void AddReportParameters(LocalReport lr, Dictionary<string, object> reportParameters) {
			List<ReportParameter> resultParams = new List<ReportParameter>();
			var parametersInfo = lr.GetParameters();
			if (reportParameters == null)
				reportParameters = new Dictionary<string, object>();
			// we are adding here the standard parameters
			AddReportParameter(reportParameters, "Logo", new Uri(LogoPath).AbsoluteUri);
			foreach (var parameterInfo in parametersInfo) {
				if (reportParameters.ContainsKey(parameterInfo.Name)) {
					resultParams.Add(new ReportParameter(parameterInfo.Name, (string)reportParameters[parameterInfo.Name]));
				}
			}
			lr.SetParameters(resultParams);
		}

		/// <summary>
		/// Adds a new parameter to the dictionary of report parameters.
		/// </summary>
		/// <param name="reportParameters">The report parameters.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <param name="parameterValue">The parameter value.</param>
		/// <param name="overrideExistingValue">if set to <c>true</c> [override existing value].</param>
		private void AddReportParameter(Dictionary<string, object> reportParameters, string parameterName, string parameterValue, bool overrideExistingValue = false) {
			if (!reportParameters.ContainsKey(parameterName))
				reportParameters.Add(parameterName, parameterValue);
			else {
				if (overrideExistingValue)
					reportParameters[parameterName] = parameterValue;
			}
		}

		/// <summary>
		/// Handles the SubreportProcessing event of the LocalReport control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Microsoft.Reporting.WebForms.SubreportProcessingEventArgs"/> instance containing the event data.</param>
		private void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e) {
			string paramName = e.Parameters[0].Name;
			string parentKeyParameter = e.Parameters[paramName].Values[0];
			string dataSourceName = e.DataSourceNames[0];
			//string subreportName = e.ReportPath;
			var data = ((IList)m_dataSources[dataSourceName]).OfType<BaseBusinessEntity>().ToList();
			e.DataSources.Add(new ReportDataSource(dataSourceName, data.Where(p => p.GetPropertyValueString(paramName, false, false) == parentKeyParameter).ToList()));
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Collections;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
    /// <summary>
    /// Implementation of IReporting for rdlc reports.
    /// </summary>
    class ReportingRdlc : IReporting {
        private RdlcGenerator rdlc;

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="exportType">The export type.</param>
        /// <param name="pageFormat">The format of the page.</param>
        public ReportingRdlc(ExportType exportType, FormatPageType pageFormat) {
            rdlc = new RdlcGenerator();
            rdlc.Format = pageFormat;
            rdlc.ExportAs = exportType;
        }


        /// <summary>
        /// The list of columns of the report.
        /// </summary>
        public IList<Column> Columns {
            get {
                return rdlc.Columns;
            }
            set {
                rdlc.Columns = value;
            }
        }

        /// <summary>
        /// Renders the report as a stream.
        /// </summary>
        /// <param name="data">The report data.</param>
        /// <param name="mimeType">The resulting mime type.</param>
        /// <param name="extension">The resulting extension of the file which content is returned as a stream.</param>
        /// <returns>The report rendered as a stream.</returns>
        public MemoryStream RenderReport(IList data, out string mimeType, out string extension) {
            DataTable dt = ConvertToDataTable(data);
            MemoryStream stream = rdlc.RenderReport(dt, out mimeType, out extension);
            return stream;
        }

        /// <summary>
        /// Converts to data table.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        private DataTable ConvertToDataTable(IList data) {
            var table = new DataTable();
            Type businessEntityType = data.GetType().GetGenericArguments().First();
            SetTableColumns(businessEntityType, table);
            var props = businessEntityType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetField | BindingFlags.GetProperty);
            var nestedObjectsPaths = Columns.Where(c => c.dataIndex.Contains(".")).Select(c => c.dataIndex).ToList();
            RemoveDotsFromColumnsNames();
            FillTable(data, props, table, nestedObjectsPaths.ToList());
            return table;
        }

        /// <summary>
        /// Removes the dots from columns names.
        /// </summary>
        private void RemoveDotsFromColumnsNames() {
            foreach (var column in Columns) {
                column.dataIndex = column.dataIndex.Replace(".", String.Empty);
            }
        }


        /// <summary>
        /// Sets the table columns.
        /// </summary>
        /// <param name="businessEntityType">Type of the business entity.</param>
        /// <param name="table">The table.</param>
        private void SetTableColumns(Type businessEntityType, DataTable table) {
            foreach (var column in Columns) {
                PropertyInfo propertyInfo = null;
                // if nested object
                if (column.dataIndex.Contains(".")) {
                    propertyInfo = GetNestedObjectPropertyInfo(businessEntityType, column.dataIndex);
                }
                else {
                    propertyInfo = businessEntityType.GetProperty(column.dataIndex);
                }
                if (propertyInfo != null) {
                    var propertyType = propertyInfo.PropertyType;

                    if (propertyType.BaseType == typeof(Enum) ||
                        (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>) &&
                         propertyType.GetGenericArguments()[0].IsEnum) ||
                        propertyType == typeof(Guid) ||
                        propertyType == typeof(TimeSpan)) {
                        table.Columns.Add(column.dataIndex.Replace(".", String.Empty), typeof(String));
                    }
                    else if (Nullable.GetUnderlyingType(propertyType) != null) {
                        table.Columns.Add(column.dataIndex.Replace(".", String.Empty), Nullable.GetUnderlyingType(propertyType));
                    }
                    else {
                        table.Columns.Add(column.dataIndex.Replace(".", String.Empty), propertyType);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the nested object property information.
        /// </summary>
        /// <param name="businessEntityType">Type of the business entity.</param>
        /// <param name="dataIndex">Index of the data.</param>
        /// <returns></returns>
        private PropertyInfo GetNestedObjectPropertyInfo(Type businessEntityType, string dataIndex) {
            var nestedObjectsNames = dataIndex.Substring(0, dataIndex.Length - (dataIndex.Length - dataIndex.LastIndexOf('.'))).Split('.');
            var propertyName = dataIndex.Substring(1 + dataIndex.LastIndexOf('.'), dataIndex.Length - dataIndex.LastIndexOf('.') - 1);
            var currentIterationObjectType = businessEntityType;

            foreach (var nestedObject in nestedObjectsNames) {
                if (currentIterationObjectType != null) {
                    var propertyInfo = currentIterationObjectType.GetProperty(nestedObject);
                    currentIterationObjectType = propertyInfo != null ? propertyInfo.PropertyType : null;
                }
            }

            return currentIterationObjectType != null ? currentIterationObjectType.GetProperty(propertyName) : null;
        }


        /// <summary>
        /// Fills the table.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="props">The props.</param>
        /// <param name="table">The table.</param>
        /// <param name="nestedObjectsPaths"></param>
        private void FillTable(IEnumerable data, PropertyInfo[] props, DataTable table, List<string> nestedObjectsPaths = null) {
            foreach (var dataItem in data) {
                DataRow row = table.NewRow();
                foreach (var propertyInfo in props) {
                    try {
                        if (table.Columns.Contains(propertyInfo.Name.Replace(".", String.Empty))) {
                            var propValue = propertyInfo.GetValue(dataItem, null);
                            AddValueToRow(propValue, row, propertyInfo.Name);
                        }
                    }
                    catch (Exception ex) {
                        TracingService.Write(TraceEntrySeverity.Error, ex.Message);
                    }
                }
                // nested objects loop. those columns requested by client to be rendered in report but ne
                if (nestedObjectsPaths != null)
                    foreach (var path in nestedObjectsPaths) {
                        if (table.Columns.Contains(path.Replace(".", String.Empty))) {
                            var value = GetNestedObjectPropertyValue(path, dataItem);
                            AddValueToRow(value, row, path.Replace(".", String.Empty));
                        }
                    }

                table.Rows.Add(row);
            }
        }

        /// <summary>
        /// Adds the value to row.
        /// </summary>
        /// <param name="propValue">The property value.</param>
        /// <param name="row">The row.</param>
        /// <param name="columnName">Name of the column.</param>
        private void AddValueToRow(object propValue, DataRow row, string columnName) {
            if (propValue != null) {
                if (propValue.GetType().IsEnum) {
                    row[columnName] = ((Enum)propValue).GetLocalizedText();
                }
                //TODO: implement CurrentUserTimeZone. As it is right now - will always return UTC
                else if (propValue is DateTime) {
                    var currentUserTimeZone = TimeZoneHelper.CurrentUserTimeZone;
                    row[columnName] = TimeZoneInfo.ConvertTimeFromUtc((DateTime)propValue, currentUserTimeZone);
                }
                else {
                    row[columnName] = propValue;
                }
            }
        }

        /// <summary>
        /// Gets the nested object property value.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="dataItem">The data item.</param>
        /// <returns></returns>
        private object GetNestedObjectPropertyValue(string path, object dataItem) {
            var pathArr = path.Split('.');
            var currentIterationObject = dataItem;
            foreach (var p in pathArr) {
                currentIterationObject = currentIterationObject.GetType().GetProperty(p).GetValue(currentIterationObject);
            }
            return currentIterationObject;
        }

        /// <summary>
        /// The title of the report.
        /// </summary>
        public string Title {
            get {
                return rdlc.Title;
            }
            set {
                rdlc.Title = value;
            }
        }

        public string LogoPath {
            get {
                return rdlc.LogoPath;
            }
            set {
                rdlc.LogoPath = value;
            }
        }


        public string FormatDate {
            get {
                return rdlc.FormatDate;
            }
            set {
                rdlc.FormatDate = value;
            }
        }
    }
}

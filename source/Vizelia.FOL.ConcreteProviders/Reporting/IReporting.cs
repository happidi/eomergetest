﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.ConcreteProviders {

    /// <summary>
    /// Inner interface for reporting.
    /// This interface is implemented for each implementation of a renderer (pdf, excel etc...).
    /// </summary>
	interface IReporting {

        /// <summary>
        /// The title of the report.
        /// </summary>
		string Title { get; set; }

        /// <summary>
        /// The list of columns of the report.
        /// </summary>
		IList<Column> Columns { get; set; }

        /// <summary>
        /// Renders the report as a stream.
        /// </summary>
        /// <param name="data">The report data.</param>
        /// <param name="mimeType">The resulting mime type.</param>
        /// <param name="extension">The resulting extension of the file which content is returned as a stream.</param>
        /// <returns>The report rendered as a stream.</returns>
		MemoryStream RenderReport(IList data ,out string mimeType , out string extension);

        /// <summary>
        /// Physical path for logo.
        /// Can be left blank when no logo is associated with the report.
        /// </summary>
        string LogoPath { get; set; }

        /// <summary>
        /// Format for dates.
        /// </summary>
        string FormatDate { get; set; }
			
	}
}

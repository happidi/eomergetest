﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using Vizelia.FOL.ServiceAgents;
using ReportParameter = Vizelia.FOL.BusinessEntities.ReportParameter;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a reporting provider.
	/// </summary>
	public class VizeliaReportingProvider : ReportingProvider {

		private const string const_attribute_logo = "logo";
		private const string const_attribute_formatdate = "formatdate";
		private const string const_attribute_reportserviceurl = "reportingServiceUrl";
		private const string const_attribute_timeout = "timeout";

		private ReportingServicesServiceAgent m_ReportServerServiceAgent;
		private Uri m_ReportServerUrl;
		private int m_Timeout;

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);
			if (String.IsNullOrEmpty(config[const_attribute_logo])) {
				throw new ProviderException(const_attribute_logo + " is missing");
			}

			Logo = config[const_attribute_logo];
			config.Remove(const_attribute_logo);

			if (String.IsNullOrEmpty(config[const_attribute_formatdate])) {
				FormatDate = DateTimeHelper.DateTimeFormat(true); // Langue.langue_dotnet_date_pattern_with_seconds; // "dd/MM/yy HH:mm:ss";
			}
			else {
				FormatDate = config[const_attribute_formatdate];
				config.Remove(const_attribute_formatdate);
			}

			string reportingServiceUrl = config[const_attribute_reportserviceurl];
			config.Remove(const_attribute_reportserviceurl);

			// The service agent that connects to the ReportServer asmx web service.
			m_ReportServerServiceAgent = new ReportingServicesServiceAgent { ReportingServiceUrl = reportingServiceUrl };

			// The base URL used for the Report Connectiong String Provider in the report viewer aspx page.
			string baseUrl = reportingServiceUrl.Substring(0, reportingServiceUrl.LastIndexOf('/'));
			m_ReportServerUrl = new Uri(baseUrl);

			string timeout = config[const_attribute_timeout];
			m_Timeout = GetTimeout(timeout);
			config.Remove(const_attribute_timeout);

			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}

		/// <summary>
		/// Gets the timeout value.
		/// </summary>
		/// <param name="timeOutString">The timeout string.</param>
		/// <returns></returns>
		private static int GetTimeout(string timeOutString) {
			int timeoutValue;

			if (!int.TryParse(timeOutString, out timeoutValue)) {
				// Assign default value - 5 minutes.
				timeoutValue = 5*60*1000;
			}

			return timeoutValue;
		}

		/// <summary>
		/// Returns the URL of the report server.
		/// </summary>
		/// <returns>
		/// A Uri object containing the URL of the report server.
		/// </returns>
		public override Uri ReportServerUrl {
			get { return m_ReportServerUrl; }
		}


		/// <summary>
		/// Gets the number of milliseconds to wait for server communications.
		/// </summary>
		/// <returns>
		/// An integer value containing the number of milliseconds to wait for server communications.
		/// </returns>
		public override int Timeout {
			get { return m_Timeout; }
		}

		/// <summary>
		/// The path of the logo
		/// </summary>
		public string Logo { set; get; }

		/// <summary>
		/// The format of the dates.
		/// </summary>
		public string FormatDate { get; set; }

		/// <summary>
		/// Renders the report as a stream.
		/// </summary>
		/// <param name="data">The report data.</param>
		/// <param name="columns">The columns of the data passed to the report.</param>
		/// <param name="title">The report's title.</param>
		/// <param name="exportType">The export type of the report.</param>
		/// <param name="pageFormat">The page format of the report.</param>
		/// <param name="mimeType">The resulting mime type.</param>
		/// <param name="extension">The resulting extension of the file which content is returned as a stream.</param>
		/// <returns>The report rendered as a stream.</returns>
		public override MemoryStream RenderReport(IList data, IList<Column> columns, string title, ExportType exportType, FormatPageType pageFormat, out string mimeType, out string extension) {
			IReporting report;
			switch (exportType) {
				case ExportType.CSV:
					report = new ReportingBasic();
					break;
				default:
					report = new ReportingRdlc(exportType, pageFormat);
					break;
			}

			report.Title = title;
			report.Columns = columns;
			report.LogoPath = GetLogoPath();
			report.FormatDate = DateTimeHelper.DateTimeFormat(true); // FormatDate;

			MemoryStream reportFile = report.RenderReport(data, out mimeType, out extension);

			return reportFile;
		}

		/// <summary>
		/// Renders the report RDLC.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="dataSources">The dictionary of data sources. Each entry in the dictionary should be a List of business entities.</param>
		/// <param name="reportParameters">The dictionary of report parameters. Each entry in the dictionary should be a string.</param>
		/// <param name="exportType">Type of the export.</param>
		/// <param name="pageFormat">The page format.</param>
		/// <param name="mimeType">Type of the MIME.</param>
		/// <param name="extension">The extension.</param>
		/// <returns></returns>
		public override MemoryStream RenderReportRdlc(string reportPath, Dictionary<string, object> dataSources, Dictionary<string, object> reportParameters, ExportType exportType, FormatPageType pageFormat, out string mimeType, out string extension) {
			var rdlc = new RdlcGenerator { Format = pageFormat, ExportAs = exportType, LogoPath = GetLogoPath() };
			MemoryStream reportFile = rdlc.RenderReportFile(reportPath, dataSources, reportParameters, out mimeType, out extension);

			return reportFile;
		}



		/// <summary>
		/// Gets the physical logo path
		/// </summary>
		/// <returns>The physical logo's path.</returns>
		private string GetLogoPath() {
			string LogoPath = HttpContext.Current.Server.MapPath(Logo);
			string path  = string.Empty;
			if (!String.IsNullOrEmpty(LogoPath)) {
				var logoPathReplaced = LogoPath.Replace("{ApplicationName}", ContextHelper.ApplicationName);
				if (File.Exists(logoPathReplaced)) {
					path = "file://" + (logoPathReplaced.Replace("\\", "/"));
					//imageType = "image/" + Path.GetExtension(LogoPath);
				}
				else {
					LogoPath = Helper.GetPhysicalPath() + "images\\logo_se_180.bmp";
					path = "file://" + (LogoPath.Replace("\\", "/"));
					//imageType = "image/" + Path.GetExtension(LogoPath);
				}
			}
			return path;
		}

		/// <summary>
		/// Report_s the get tree.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public override List<TreeNode> GetReportHierarchy(string key) {
			if (!key.StartsWith("/")) {
				throw new VizeliaException("key must start with a /");
			}

			IEnumerable<Report> reports = GetReportHierarchyForCurrentTenant(key);
			List<Report> allowedReports = GetAllowedReports(reports);
			List<TreeNode> reportTree = allowedReports.GetTree();

			return reportTree;
		}

		/// <summary>
		/// Creates the folder.
		/// </summary>
		/// <param name="path">The path.</param>
		public override void CreateFolder(string path) {
			string pathWithTenantName = AddTenantPrefix(path);
			m_ReportServerServiceAgent.CreateFolder(pathWithTenantName);
		}

		/// <summary>
		/// Deletes the item.
		/// </summary>
		/// <param name="path">The path.</param>
		public override void DeleteItem(string path) {
			ValidatePathIsNotRoot(path);

			string pathWithTenantName = AddTenantPrefix(path);
			m_ReportServerServiceAgent.DeleteItem(pathWithTenantName);
		}

		/// <summary>
		/// Renames or moves the item.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="newPath">The new path.</param>
		public override void MoveItem(string path, string newPath) {
			ValidatePathIsNotRoot(path);

			string pathWithTenantName = AddTenantPrefix(path);
			string newPathWithTenantName = AddTenantPrefix(newPath);
			m_ReportServerServiceAgent.MoveItem(pathWithTenantName, newPathWithTenantName);
		}

		/// <summary>
		/// Deletes the folder. All content will be deleted as well.
		/// </summary>
		/// <param name="path">The path.</param>
		public override void DeleteFolder(string path) {
			ValidatePathIsNotRoot(path);
			string pathWithTenantName = AddTenantPrefix(path);

			List<Report> children = m_ReportServerServiceAgent.GetChildren(pathWithTenantName, true);

			if(children.Any(r => !(ReportHelper.IsFolder(r) || ReportHelper.IsReportFile(r)))) {
				throw new VizeliaException("Cannot delete folder since it contains items hidden from the tree. Use the Reporting Services management website to investigate.");
			}

			m_ReportServerServiceAgent.DeleteFolder(pathWithTenantName);
		}

		private static void ValidatePathIsNotRoot(string path) {
			if (string.IsNullOrEmpty(path) || path.Trim().Equals("/")) {
				throw new ArgumentException("Cannot perform action on empty or root reports folder.");
			}
		}

		/// <summary>
		/// Gets the report parameters.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <returns></returns>
		public override List<ReportParameter> GetReportParameters(string reportPath) {
			string pathWithTenantName = AddTenantPrefix(reportPath);
			List<ReportParameter> reportParameters = m_ReportServerServiceAgent.GetReportParameters(pathWithTenantName);

			return reportParameters;
		}

		/// <summary>
		/// Gets the report parameter values.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="parameterName">Name of the parameter.</param>
		/// <returns></returns>
		public override JsonStore<ReportParameterValue> GetReportParameterValues(string reportPath, string parameterName) {
			string pathWithTenantName = AddTenantPrefix(reportPath);
			List<ReportParameterValue> values = m_ReportServerServiceAgent.GetReportParameterValues(pathWithTenantName, parameterName);
			JsonStore<ReportParameterValue> store = values.ToJsonStore(values.Count);

			return store;
		}


		/// <summary>
		/// Creates the subscription.
		/// </summary>
		/// <param name="subscription">The report.</param>
		public override void CreateSubscription(ReportSubscription subscription) {
			string pathWithTenantName = AddTenantPrefix(subscription.ReportPath);
			m_ReportServerServiceAgent.CreateSubscription(pathWithTenantName, subscription);
		}

		/// <summary>
		/// Deletes the subscription.
		/// </summary>
		/// <param name="keyReportSubscription">The key report subscription.</param>
		public override void DeleteSubscription(string keyReportSubscription) {
			m_ReportServerServiceAgent.DeleteSubscription(keyReportSubscription);
		}

		/// <summary>
		/// Gets the subscriptions.
		/// </summary>
		/// <param name="reportPath">The report path.</param>
		/// <param name="forKeyActor">For key actor.</param>
		/// <param name="paging">The paging.</param>
		/// <param name="totalCount">The total count.</param>
		/// <returns></returns>
		public override List<ReportSubscription> GetSubscriptions(string reportPath, string forKeyActor, PagingParameter paging, out int totalCount) {
			string pathWithTenantName = AddTenantPrefix(reportPath);
			var subscriptions = m_ReportServerServiceAgent.GetSubscriptions(pathWithTenantName, forKeyActor, paging, out totalCount);

			foreach (ReportSubscription subscription in subscriptions) {
				subscription.ReportPath = RemoveTenantPrefix(subscription.ReportPath);
			}

			return subscriptions;
		}
		
		/// <summary>
		/// Gets the report hierarchy for the current tenant.
		/// </summary>
		/// <param name="path">The path.</param>
		private IEnumerable<Report> GetReportHierarchyForCurrentTenant(string path) {
			// Tenant-awareness is done by having a folder for each tenant at the root.
			string pathWithTenantName = AddTenantPrefix(path);

			List<Report> children = m_ReportServerServiceAgent.GetChildren(pathWithTenantName, false);
			List<Report> reports = children.Where(r => ReportHelper.IsReportFile(r) || ReportHelper.IsFolder(r)).ToList();

			foreach (Report report in reports) {
				report.Path = RemoveTenantPrefix(report.Path);
			}

			return reports;
		}

		/// <summary>
		/// Gets the allowed reports by examinig the filter on reports.
		/// </summary>
		/// <param name="reports">The reports.</param>
		private static List<Report> GetAllowedReports(IEnumerable<Report> reports) {
			List<Filter> securityFilter = FilterHelper.GetFilter<Report>();
			List<Report> allowedReports;

			if (securityFilter.Count == 0) {
				allowedReports = reports.ToList();
			}
			else {
				allowedReports = reports.Where(r => ReportHelper.IsAllowed(r, securityFilter)).ToList();
			}

			return allowedReports;
		}

		/// <summary>
		/// Removes the tenant prefix from the path.
		/// </summary>
		/// <param name="path">The path.</param>
		private string RemoveTenantPrefix(string path) {
			string prefix = "/" + ContextHelper.ApplicationName;

			if (path.StartsWith(prefix)) {
				string suffix = path.Substring(prefix.Length);

				return suffix;
			}

			return path;
		}

		/// <summary>
		/// Adds the tenant prefix to the path.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <returns></returns>
		private static string AddTenantPrefix(string path) {
			string pathWithTenantName = string.Format("/{0}{1}", ContextHelper.ApplicationName, path.TrimEnd('/'));

			return pathWithTenantName;
		}
	}
}


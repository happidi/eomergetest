﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Telvent Weather service REST DataAcquisitionProvider Provider
	/// </summary>
	/// <remarks>
	/// <para>Telvent provides XML data about weather. This requests the data based on user
	/// configuration rules and converts it into DataAcquisitionEndpointValue objects.</para>
	/// </remarks>
	[LocalizedText("msg_provider_telvent")]
	public class TelventRESTDataAcquisitionProvider : RESTDataAcquisitionProvider {


		private const string const_attribute_login = "login";
		private const string const_attribute_password = "password";

		private const string const_url = "http://weather.telventdtn.com/rest-1.1/services/TDTNWeatherDataService/getWeatherData";
		private const string const_dns = "http://weather.telventdtn.com/rest-1.1/doc/";

		private const string const_url_V2 = "http://weather.telventdtn.com/rest-2.3/services/TDTNWeatherDataService/getWeatherData";
		private const string const_dns_V2 = "http://weather.telventdtn.com/rest-2.3/doc/";
		/// <summary>
		/// Gets or sets the Login.
		/// </summary>
		/// <value>
		/// The key.
		/// </value>
		protected string Login { get; set; }

		/// <summary>
		/// Gets or sets the Password.
		/// </summary>
		/// <value>
		/// The key.
		/// </value>
		protected string Password { get; set; }

		/// <summary>
		/// Used to specify what is the Location Type used to query the Telvent service
		/// </summary>
		public enum LocationType {
			/// <summary>
			/// Supply a street address
			/// </summary>
			Address,
			/// <summary>
			/// Supply a latitude and longitude
			/// </summary>
			LatLon,
			/// <summary>
			/// Supply a Station ID
			/// </summary>
			StationId
		}

		/// <summary>
		/// Data holder to hold the Location Type and details used to query the Telvent service
		/// </summary>
		public class LocationQueryData {
			/// <summary>
			/// Constructor
			/// </summary>
			public LocationQueryData() {
				Latitude = null;
				Longitude = null;
				StationLatitude = null;
				StationLongitude = null;
			}
			
			/// <summary>
			/// Selects from: Street address, Lat/Lon, and Station ID
			/// </summary>
			public LocationType Type { get; set; }

			/// <summary>
			/// When Type=LatLon, this must contain the latitude
			/// </summary>
			public double? Latitude { get; set; }

			/// <summary>
			/// When Type=LatLon, this must contain the longitude
			/// </summary>
			public double? Longitude { get; set; }

			/// <summary>
			/// When Type=StationId, this must contain the station's ID
			/// </summary>
			public string StationId { get; set; }


			/// <summary>
			/// When Type=StationId, this must contain the station's latitude
			/// </summary>
			public double? StationLatitude { get; set; }

			/// <summary>
			/// When Type=StationId, this must contain the station's longitude
			/// </summary>
			public double? StationLongitude { get; set; }

			/// <summary>
			/// When Type=Address, this must contain the street address.
			/// The format must be compatible with MapService.GetLocationFromAddress
			/// </summary>
			public string Address { get; set; }

			/// <summary>
			/// Supplies a representation of the current Type's data
			/// for display to the user.
			/// </summary>
			/// <returns></returns>
			public string GetEffectiveLocation() {
				switch (Type) {
					case LocationType.Address: {
						return Address;
					}
					case LocationType.LatLon: {
						return Latitude + ", " + Longitude;
					}
					case LocationType.StationId: {
						return StationId;
					}
					default:
						return Address;
				}
			}
		};


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_login])) {
				throw new ProviderException(const_attribute_login + " is missing");
			}
			Login = config[const_attribute_login];
			config.Remove(const_attribute_login);

			if (String.IsNullOrEmpty(config[const_attribute_password])) {
				throw new ProviderException(const_attribute_password + " is missing");
			}
			Password = config[const_attribute_password];
			config.Remove(const_attribute_password);

			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}


		/// <summary>
		/// In order to create a generic REST provider, implementor must convert the REST response to a double (meter data)
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">The endpoint.</param>
		/// <param name="RESTresponse">The REST response.</param>
		/// <returns>
		/// parsed response as double
		/// </returns>
		public override List<DataAcquisitionEndpointValue> Parse(RESTDataAcquisitionContainer container, RESTDataAcquisitionEndpoint endpoint, string RESTresponse) {
			var QueryString = endpoint.QueryString;
			var Path = endpoint.Path;

			if (string.IsNullOrEmpty(RESTresponse))
				return new List<DataAcquisitionEndpointValue>() { new DataAcquisitionEndpointValue { AcquisitionDateTime = DateTime.UtcNow, Value = double.NaN } };

			string attributeToLookFor = GetAttributeValue(Path, QueryString);
			XNamespace ns = const_dns_V2;

			var output = from a in XElement.Parse(RESTresponse).Descendants(ns + "locationResponseList")
						 from b in a.Descendants(ns + "locationResponse")
						 where b.Element(ns + attributeToLookFor) != null
						 select
							new DataAcquisitionEndpointValue {
								Value = double.Parse(b.Element(ns + attributeToLookFor).Element(ns + "value").Value, CultureInfo.InvariantCulture),
								AcquisitionDateTime = DateTime.Parse(b.Element(ns + "validDateTime").Value, CultureInfo.InvariantCulture),
								Unit = b.Element(ns + attributeToLookFor).Element(ns + "uom") != null ? b.Element(ns + attributeToLookFor).Element(ns + "uom").Value : ""
							};
			if (output.Count() == 0)
				return new List<DataAcquisitionEndpointValue>() { new DataAcquisitionEndpointValue() { AcquisitionDateTime = DateTime.Now, Value = double.NaN } };

			var retval = new List<DataAcquisitionEndpointValue>();

			if (!string.IsNullOrEmpty(endpoint.Unit)) {
				output.ToList().ForEach(v => {
					if (endpoint.Unit == "Celsius" && (v.Unit == "F" || string.IsNullOrEmpty(v.Unit))) {
						v.Value = UnitHelper.ConvertTemperature(v.Value, WeatherUnit.Fahrenheit, WeatherUnit.Celsius);
						v.Unit = Langue.msg_enum_weatherunit_celsius;
					}
					else if (endpoint.Unit == "Fahrenheit" && v.Unit == "C") {
						v.Value = UnitHelper.ConvertTemperature(v.Value, WeatherUnit.Celsius, WeatherUnit.Fahrenheit);
						v.Unit = Langue.msg_enum_weatherunit_fahrenheit;
					}
					retval.Add(v);
				});
			}
			else
				retval = output.ToList();

			retval = retval.OrderBy(a => a.AcquisitionDateTime).ToList();
			return retval;
		}

		/// <summary>
		/// Perform any other changes to the object such as property manipulation or additions that come from the various DataAcquisition Providers
		/// e.g. Return additional data that came from a web service call in addition to data stored in the data layer.
		/// </summary>
		/// <param name="container">The container</param>
		/// <returns></returns>
		public override DataAcquisitionContainer FillObject(DataAcquisitionContainer container) {
			var restContainer = (RESTDataAcquisitionContainer)container;
			//This will get Lat/Lon if the location type is Address
			var locationData = GetLocationQueryData(restContainer.CustomData, restContainer.Url);

			//Make a service call just to populate the Station Id used for this location
			var startDate = DateTime.UtcNow.AddHours(-2);
			var enddate = startDate.AddHours(1);
			var retVal = MakeRequest(startDate, enddate, locationData, TelventWeatherRequestType.HourlyObservation);
			PopulateStationInfo(restContainer, locationData, retVal);
			restContainer.CustomData = JsonConvert.SerializeObject(locationData);
			restContainer.Url = locationData.GetEffectiveLocation();
			return restContainer;
		}


		/// <summary>
		/// Executes the rest query.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">The endpoint.</param>
		/// <returns></returns>
		protected override string ExecuteRestQuery(RESTDataAcquisitionContainer container, RESTDataAcquisitionEndpoint endpoint) {
			var retVal = "";
			var customData = container.CustomData;
			var Url = container.Url;
			var Path = endpoint.Path;

			var locationData = GetLocationQueryData(customData, Url);
			

			var telventweatherrequesttype = Path.ParseAsEnum<TelventWeatherRequestType>();
			switch (telventweatherrequesttype) {
				case TelventWeatherRequestType.DailyObservation: {
						var startDate = DateTime.Now.AddDays(-2);
						var enddate = startDate.AddDays(1);
						retVal = MakeRequest(startDate, enddate, locationData, telventweatherrequesttype);
					}
					break;
				case TelventWeatherRequestType.HourlyObservation: {
						var startDate = DateTime.Now.AddHours(-2);
						var enddate = startDate.AddHours(1);
						retVal = MakeRequest(startDate, enddate, locationData, telventweatherrequesttype);
					}
					break;
				case TelventWeatherRequestType.DailyForecast:
				case TelventWeatherRequestType.HourlyForecast: {
						var startDate = DateTime.Now;
						var enddate = startDate.AddDays(15);
						retVal = MakeRequest(startDate, enddate, locationData, telventweatherrequesttype);
					}
					break;
			}
			return retVal;

		}

		/// <summary>
		/// Populates the passed locationData with Weather Station Info such as ID and Lat/Lon
		/// </summary>
		/// <param name="container"></param>
		/// <param name="locationData"></param>
		/// <param name="RESTresponse">The REST response.</param>
		private void PopulateStationInfo(RESTDataAcquisitionContainer container, LocationQueryData locationData, string RESTresponse) {
			if (string.IsNullOrEmpty(RESTresponse))
				return;
			XNamespace ns = const_dns_V2;

			var xml = XElement.Parse(RESTresponse).Descendants(ns + "locationResponseList").Descendants(ns + "locationResponse").FirstOrDefault();
			if (xml == null) {
				return;
			}
			locationData.StationId = xml.Element(ns + "stationID").Value;
			locationData.StationLatitude = double.Parse(xml.Element(ns + "stationLatitude").Value);
			locationData.StationLongitude = double.Parse(xml.Element(ns + "stationLongitude").Value);
			return;
		}

		private static LocationQueryData GetLocationQueryData(string customData, string Url) {
			if (string.IsNullOrEmpty(customData)) {
				//Use old URL property for backwards compatability
				return PopulateLatLonFromAddress(Url);
			}
			else {
				var locationData = JsonConvert.DeserializeObject<LocationQueryData>(customData);
				if (locationData.Type == LocationType.Address) {
					locationData = PopulateLatLonFromAddress(locationData.Address);
				}
				return locationData;
			}
		}

		private static LocationQueryData PopulateLatLonFromAddress(string address) {
			var locationData = new LocationQueryData();
			if (!string.IsNullOrEmpty(address)) {
				var geoLoc = MapService.GetLocationFromAddress(address);
				locationData.Type = LocationType.Address;
				locationData.Address = address;
				if (geoLoc.IsValid) {
					locationData.Latitude = geoLoc.Latitude;
					locationData.Longitude = geoLoc.Longitude;	
				}
			}
			return locationData;
		}

		private string GetAttributeValue(string Path, string QueryString) {
			var attributeValue = "";
			var telventweatherrequesttype = Path.ParseAsEnum<TelventWeatherRequestType>();
			switch (telventweatherrequesttype) {
				case TelventWeatherRequestType.DailyObservation:
					attributeValue = int.Parse(QueryString).ParseAsEnum<TelventDailyAttributes>().ParseAsString();
					break;
				case TelventWeatherRequestType.HourlyObservation:
					attributeValue = int.Parse(QueryString).ParseAsEnum<TelventHourlyAttributes>().ParseAsString();
					break;
				case TelventWeatherRequestType.DailyForecast:
					attributeValue = int.Parse(QueryString).ParseAsEnum<TelventDailyForecastAttributes>().ParseAsString();
					break;
				case TelventWeatherRequestType.HourlyForecast:
					attributeValue = int.Parse(QueryString).ParseAsEnum<TelventHourlyForecastAttributes>().ParseAsString();
					break;
			}
			return attributeValue;
		}



		/// <summary>
		/// Makes the request.
		/// </summary>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="locationData">Data holder for location query parameters</param>
		/// <param name="requestType">Type of the request.</param>
		/// <returns></returns>
		private string MakeRequest(DateTime startDate, DateTime endDate, LocationQueryData locationData, TelventWeatherRequestType requestType) {

			if (locationData.Type != LocationType.StationId) {
				if (!locationData.Latitude.HasValue || !locationData.Longitude.HasValue) {
					return null;
				}
			}
			
			var locationQueryString = locationData.Type == LocationType.StationId
				? "stationID=" + locationData.StationId.ToString(CultureInfo.InvariantCulture)
				: "latitude=" + Math.Round(locationData.Latitude.Value, 2).ToString(CultureInfo.InvariantCulture) + ",longitude=" +
				  Math.Round(locationData.Longitude.Value, 2).ToString(CultureInfo.InvariantCulture);

			var oldUiCulture = System.Threading.Thread.CurrentThread.CurrentUICulture;
			var oldCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
			System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
			System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");


			var nonce = new Random().Next(1, 10000).ToString();

			var param_values = new Dictionary<string, string>();
			string requestMessage = "dataType=" + Enum.GetName(typeof(TelventWeatherRequestType), requestType) + ",dataTypeMode=0001,startDate=" + startDate.ToString("s") + ",endDate=" + endDate.ToString("s") + "," + locationQueryString + ",";
			param_values.Add("message", requestMessage);
			param_values.Add("oauth_consumer_key", Login);
			param_values.Add("oauth_nonce", nonce);
			param_values.Add("oauth_signature_method", "HMAC-SHA1");
			param_values.Add("oauth_timestamp", ((long)GetUnixEpoch(DateTime.UtcNow)).ToString());
			param_values.Add("oauth_token", "");
			param_values.Add("oauth_version", "1.0");


			var sb = new StringBuilder();
			foreach (var param in param_values) {
				sb.Append(param.Key);
				sb.Append("=");
				sb.Append(UpperCaseUrlEncode(param.Value));
				sb.Append("&");
			}

			var message = "GET&" + UpperCaseUrlEncode(const_url_V2) + "&" + UpperCaseUrlEncode(sb.ToString().TrimEnd('&'));

			var hash = CreateHash(this.Password + "&", message);

			string finalUrl = const_url_V2 + "?" + sb.ToString() + "oauth_signature=" + UpperCaseUrlEncode(hash);
			string retval = null;
			try {
				WebClient webClient = new WebClient();
				retval = webClient.DownloadString(finalUrl);
			}
			catch {
				
			}

			System.Threading.Thread.CurrentThread.CurrentUICulture = oldUiCulture;
			System.Threading.Thread.CurrentThread.CurrentCulture = oldCulture;


			return retval;
		}


		/// <summary>
		/// Gets the unix epoch.
		/// </summary>
		/// <param name="dateTime">The date time.</param>
		/// <returns></returns>
		private static double GetUnixEpoch(DateTime dateTime) {
			var unixTime = dateTime.ToUniversalTime() -
				new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

			return unixTime.TotalSeconds;
		}
		/// <summary>
		/// Uppers the case URL encode.
		/// </summary>
		/// <param name="s">The s.</param>
		/// <returns></returns>
		private static string UpperCaseUrlEncode(string s) {
			char[] temp = HttpUtility.UrlEncode(s).ToCharArray();
			for (int i = 0; i < temp.Length - 2; i++) {
				if (temp[i] == '%') {
					temp[i + 1] = char.ToUpper(temp[i + 1]);
					temp[i + 2] = char.ToUpper(temp[i + 2]);
				}
			}
			return new string(temp);
		}


		/// <summary>
		/// Creates the hash.
		/// </summary>
		/// <param name="secret">The secret.</param>
		/// <param name="data">The data.</param>
		/// <returns></returns>
		private string CreateHash(string secret, string data) {
			var hmac = new HMACSHA1(Encoding.UTF8.GetBytes(secret));
			var hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(data));
			return Convert.ToBase64String(hash);
		}

	}
}

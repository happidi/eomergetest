﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using Vizelia.FOL.ServiceAgents;
using Vizelia.FOL.ServiceAgents.EWS;
using ContainerItemSimpleType = Vizelia.FOL.ServiceAgents.EWS.ContainerItemSimpleType;
using ValueItemType = Vizelia.FOL.ServiceAgents.EWS.ValueItemType;
using ValueItemTypeBase = Vizelia.FOL.ServiceAgents.EWS.ValueItemTypeBase;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Schneider Electric Enterprise Web Services Data Acquisition Provider.
	/// </summary>
	public class EWSDataAcquisitionProvider : DataAcquisitionProvider {

		/// <summary>
		/// Get the current value for a specific endpoint.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">the endpoint.</param>
		/// <returns></returns>
		public override List<DataAcquisitionEndpointValue> GetCurrentValue(DataAcquisitionContainer container, DataAcquisitionEndpoint endpoint) {
			var swContainer = Helper.Cast<EWSDataAcquisitionContainer>(container);
			var swEndpoint = Helper.Cast<EWSDataAcquisitionEndpoint>(endpoint);

			using (var agent = GetServiceAgent(swContainer)) {
				var endpoints = agent.GetEndpoints(new List<string> { swEndpoint.ServerLocalId });
				ValueItemType currentEndpointState = endpoints.Single();

				double value;
				// Try parsing the value, if cannot then set to NaN.
				if (string.IsNullOrWhiteSpace(currentEndpointState.Value) ||
					!double.TryParse(currentEndpointState.Value, NumberStyles.Any, CultureInfo.InvariantCulture, out value)) {
					value = double.NaN;
				}

				var endpointValue = new DataAcquisitionEndpointValue {
					Value = value,
					AcquisitionDateTime = DateTime.Now,
					Unit = currentEndpointState.Unit
				};

				return new List<DataAcquisitionEndpointValue> { endpointValue };
			}
		}

		/// <summary>
		/// Gets the hierarchy from the container.
		/// e.g. get all the meters that are connected to a specific box. or - get all the geo locations a given webservice supports
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="parentItem">The parent item.</param>
		/// <returns></returns>
		public override List<TreeNode> GetHierarchy(DataAcquisitionContainer container, DataAcquisitionItem parentItem = null) {
			var swContainer = Helper.Cast<EWSDataAcquisitionContainer>(container);

			using (var agent = GetServiceAgent(swContainer)) {

				IEnumerable<ContainerItemType> ewsContainers;

				if (parentItem != null) {
					var folder = Helper.Cast<EWSDataAcquisitionFolder>(parentItem);
					ewsContainers = agent.GetHierarchy(folder.ServerLocalId);
				}
				else {
					ewsContainers = agent.GetHierarchy();
				}


				var endpoints = new List<DataAcquisitionItem>();

				foreach (ContainerItemType ewsContainer in ewsContainers) {
					if (ewsContainer.Items != null) {
						if (ewsContainer.Items.ContainerItems != null) {
							endpoints.AddRange(ewsContainer.Items.ContainerItems.Select(c => ConvertToEWSDataAcquisitionFolder(container, c)));
						}

						if (ewsContainer.Items.ValueItems != null) {
							endpoints.AddRange(ewsContainer.Items.ValueItems.Select(v => ConvertToEWSDataAcquisitionEndpoint(container, v)));
						}
					}
				}

				return endpoints.GetTree();
			}
		}

		/// <summary>
		/// Perform any other changes to the object such as property manipulation or additions that come from the various DataAcquisition Providers
		/// e.g. Return additional data that came from a web service call in addition to data stored in the data layer.
		/// </summary>
		/// <param name="container">The container</param>
		/// <returns></returns>
		public override DataAcquisitionContainer FillObject(DataAcquisitionContainer container) {
			return container;
		}

		/// <summary>
		/// Gets the service agent.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <returns></returns>
		private static EWSServiceAgent GetServiceAgent(EWSDataAcquisitionContainer container) {
			var agent = new EWSServiceAgent(container.Url, container.Username, container.Password, container.Authentication);
			return agent;
		}

		/// <summary>
		/// Converts to EWS data acquisition endpoint.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="v">The v.</param>
		/// <returns></returns>
		private static EWSDataAcquisitionEndpoint ConvertToEWSDataAcquisitionEndpoint(DataAcquisitionContainer container, ValueItemTypeBase v) {
			return new EWSDataAcquisitionEndpoint {
				Description = v.Description,
				LocalId = container.LocalId + v.Id,
				ServerLocalId = v.Id,
				Name = v.Name,
				KeyDataAcquisitionContainer = container.KeyDataAcquisitionContainer
			};
		}

		/// <summary>
		/// Converts to EWS data acquisition folder.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="c">The c.</param>
		/// <returns></returns>
		private static EWSDataAcquisitionFolder ConvertToEWSDataAcquisitionFolder(DataAcquisitionContainer container, ContainerItemSimpleType c) {
			return new EWSDataAcquisitionFolder {
				LocalId = container.LocalId + c.Id,
				ServerLocalId = c.Id,
				Name = c.Name,
				KeyDataAcquisitionContainer = container.KeyDataAcquisitionContainer,
				Type = c.Type
			};
		}



	}
}
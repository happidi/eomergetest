﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.XPath;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Generic REST provider with XPath attributes to know where to look for the MeterId,AcquisitionDateTime and Value.
	/// </summary>
	[LocalizedText("msg_provider_xpathacquisitionprovider")]
	public class XPathRESTDataAcquisitionProvider : RESTDataAcquisitionProvider {
		/// <summary>
		/// In order to create a generic REST provider, implementor must convert the REST response to a double (meter data)
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">The endpoint.</param>
		/// <param name="RESTresponse">The REST response.</param>
		/// <returns>
		/// parsed response as double
		/// </returns>
		public override List<DataAcquisitionEndpointValue> Parse(RESTDataAcquisitionContainer container, RESTDataAcquisitionEndpoint endpoint, string RESTresponse) {
			var retVal = new List<DataAcquisitionEndpointValue>();

			var currentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
			var currentUICulture = System.Threading.Thread.CurrentThread.CurrentUICulture;

			if (!string.IsNullOrEmpty(container.KeyLocalizationCulture)) {
				System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(container.KeyLocalizationCulture, false);
				System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(container.KeyLocalizationCulture, false);
			}

			if (string.IsNullOrEmpty(RESTresponse) == false) {
				try {

					RESTresponse = RESTresponse.RemoveAllNamespaces();
					var reader = new StringReader(RESTresponse);

					var doc = new XPathDocument(reader);
					int count;
					var nav = doc.CreateNavigator();

					if (!string.IsNullOrEmpty(container.XPathAcquisitionDateTime)) {
						count = 0;
						var iterator = nav.Select(container.XPathAcquisitionDateTime);
						while (iterator.MoveNext()) {

							var point = new DataAcquisitionEndpointValue() {
								AcquisitionDateTime = DateTime.MinValue,
								Value = Double.NaN
							};
							retVal.Add(point);

							if (string.IsNullOrEmpty(container.FormatAcquisitionDateTime)) {
								container.FormatAcquisitionDateTime = DateTimeHelper.DateTimeFormat(false);
							}

							string strDateTime;
							if (string.IsNullOrEmpty(container.AttributeAcquisitionDateTime) == false)
								strDateTime = iterator.Current.GetAttribute(container.AttributeAcquisitionDateTime, "");
							else
								strDateTime = iterator.Current.Value;

							if (!string.IsNullOrEmpty(strDateTime)) {
								retVal[count].AcquisitionDateTime = DateTime.ParseExact(strDateTime, container.FormatAcquisitionDateTime, CultureInfo.CurrentCulture);
								count += 1;
							}
						}
					}
					if (!string.IsNullOrEmpty(container.XPathValue)) {
						count = 0;
						var iterator = nav.Select(container.XPathValue);
						while (iterator.MoveNext()) {
							string strValue;
							if (!string.IsNullOrEmpty(container.AttributeValue))
								strValue = iterator.Current.GetAttribute(container.AttributeValue, "");
							else
								strValue = iterator.Current.Value;

							if (!string.IsNullOrEmpty(strValue)) {
								if (retVal.Count > count) {
									retVal[count].Value = Double.Parse(strValue);
									count += 1;
								}
								else {
									var point = new DataAcquisitionEndpointValue() {
										AcquisitionDateTime = DateTime.UtcNow,
										Value = Double.Parse(strValue)
									};
									retVal.Add(point);
								}
							}
						}
					}
				}
				catch { ;}

			}
			System.Threading.Thread.CurrentThread.CurrentCulture = currentCulture;
			System.Threading.Thread.CurrentThread.CurrentUICulture = currentUICulture;

			retVal = retVal.Where(ep => double.IsNaN(ep.Value) == false && ep.AcquisitionDateTime > DateTime.MinValue && ep.AcquisitionDateTime < DateTime.MaxValue).ToList();
			return retVal;
		}


		/// <summary>
		/// Executes the rest query.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">The endpoint.</param>
		/// <returns></returns>
		protected override string ExecuteRestQuery(RESTDataAcquisitionContainer container, RESTDataAcquisitionEndpoint endpoint) {
			var url = container.Url;
			var queryString = endpoint.QueryString;
			var query = url;
			if (!string.IsNullOrEmpty(queryString)) {
				query = string.Format("{0}?{1}", url, queryString);//HttpUtility.UrlEncode(
			}
			var retVal = new WebClient().DownloadString(query);
			return retVal;
		}
	}
}

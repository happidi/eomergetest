﻿using System;
using System.Collections.Generic;
using System.Xml;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.ServiceAgents;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// WeatherDotComREST DataAcquisitionProvider Provider
	/// </summary>
	[LocalizedText("msg_provider_weatherdotcom")]
	public class WeatherDotComRESTDataAcquisitionProvider : RESTDataAcquisitionProvider {
		/// <summary>
		/// In order to create a generic REST provider, implementor must convert the REST response to a double (meter data)
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">The endpoint.</param>
		/// <param name="RESTresponse">The REST response.</param>
		/// <returns>
		/// parsed response as double
		/// </returns>
		public override List<DataAcquisitionEndpointValue> Parse(RESTDataAcquisitionContainer container, RESTDataAcquisitionEndpoint endpoint, string RESTresponse) {
			var retVal = double.NaN;
			try {
				if (!string.IsNullOrEmpty(RESTresponse)) {
					var responseXml = new XmlDocument();
					responseXml.LoadXml(RESTresponse);
					var documentUnit = WeatherDotComHelper.GetDocumentUnit(responseXml);
					var todayXml = responseXml.SelectSingleNode("//weather/cc");
					retVal = WeatherDotComHelper.GetNodeTemperature(todayXml, documentUnit, WeatherUnit.Celsius, "tmp");
				}
			}
			catch { }
			return new List<DataAcquisitionEndpointValue>() {new DataAcquisitionEndpointValue {
				AcquisitionDateTime = DateTime.UtcNow,
				Value = retVal
			}};
		}


		/// <summary>
		/// Executes the rest query.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">The endpoint.</param>
		/// <returns></returns>
		protected override string ExecuteRestQuery(RESTDataAcquisitionContainer container, RESTDataAcquisitionEndpoint endpoint) {
			var Url = container.Url;
			var QueryString = endpoint.QueryString;
			var Path = endpoint.Path;
			
			try {
				var weatherSA = new WeatherDotComServiceAgent();
				var document = weatherSA.GetWeatherFiveDayForecast(new WeatherLocation() {
					LocationId = QueryString
				});
				return document.OuterXml;
			}
			catch {
				;
			}
			return null;
		}
	}
}

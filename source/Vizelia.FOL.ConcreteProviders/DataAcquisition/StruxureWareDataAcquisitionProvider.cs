﻿using System.Collections.Generic;
using System.Linq;
//using SE.StruxureWare.Logs;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// StruxureWare DataAcquisition Provider
	/// </summary>
	public class StruxureWareDataAcquisitionProvider : DataAcquisitionProvider {

		/// <summary>
		/// Get the current value for a specific endpoint.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">the endpoint.</param>
		/// <returns></returns>
		public override List<DataAcquisitionEndpointValue> GetCurrentValue(DataAcquisitionContainer container, DataAcquisitionEndpoint endpoint) {
			return new List<DataAcquisitionEndpointValue>();
			//var swContainer = Helper.Cast<StruxureWareDataAcquisitionContainer>(container);
			//var swEndpoint = Helper.Cast<StruxureWareDataAcquisitionEndpoint>(endpoint);
			//var retval = new List<DataAcquisitionEndpointValue>();
			//try {
			//    var realtimeValue = GetRealTimeValue(swContainer.Url, swContainer.Username, swContainer.Password, swEndpoint.Path);
			//    retval.Add(new DataAcquisitionEndpointValue {
			//        AcquisitionDateTime = realtimeValue.Date.ToUniversalTime(),
			//        Value = realtimeValue.Value
			//    });
			//}
			//catch { }

			//return retval;
		}

		/// <summary>
		/// Gets the hierarchy from the container.
		/// e.g. get all the meters that are connected to a specific box. or - get all the geo locations a given webservice supports
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="parentItem">The parent item.</param>
		/// <returns></returns>
		public override List<TreeNode> GetHierarchy(DataAcquisitionContainer container, DataAcquisitionItem parentItem = null) {
			return new List<TreeNode>();
			//StruxureWareDataAcquisitionContainer swContainer = Helper.Cast<StruxureWareDataAcquisitionContainer>(container);
			//StruxureWareDataAcquisitionEndpoint swEndpoint = null;
			//if (parentItem != null)
			//    swEndpoint = Helper.Cast<StruxureWareDataAcquisitionEndpoint>(parentItem);
			//var logList = GetLogsList(swContainer.Url, swContainer.Username, swContainer.Password);

			//var retVal = logList.Select(log => new StruxureWareDataAcquisitionEndpoint {
			//    LocalId = container.LocalId + log.Path,
			//    Description = log.Description,
			//    Name = log.Name,
			//    Path = log.Path,
			//    KeyDataAcquisitionContainer = container.KeyDataAcquisitionContainer
			//}).ToList();
			//return retVal.GetTree();

		}

		/// <summary>
		/// Perform any other changes to the object such as property manipulation or additions that come from the various DataAcquisition Providers
		/// e.g. Return additional data that came from a web service call in addition to data stored in the data layer.
		/// </summary>
		/// <param name="container">The container</param>
		/// <returns></returns>
		public override DataAcquisitionContainer FillObject(DataAcquisitionContainer container) {
			return container;
		}

		///// <summary>
		///// Gets the logs list.
		///// </summary>
		///// <param name="ip">The ip.</param>
		///// <param name="login">The login.</param>
		///// <param name="password">The password.</param>
		///// <returns></returns>
		//private List<Log> GetLogsList(string ip, string login, string password) {
		//    var logcollector = GetLogCollector(ip, login, password);
		//    var list = logcollector.GetLogs(true).ToList();
		//    logcollector.SignOut();
		//    return list;
		//}


		///// <summary>
		///// Gets the real time value.
		///// </summary>
		///// <param name="ip">The ip.</param>
		///// <param name="login">The login.</param>
		///// <param name="password">The password.</param>
		///// <param name="path">The path.</param>
		///// <returns></returns>
		//private LogValue GetRealTimeValue(string ip, string login, string password, string path) {
		//    var logcollector = GetLogCollector(ip, login, password);
		//    var value = logcollector.GetLastLogValue(path);
		//    logcollector.SignOut();
		//    return value;

		//}

		///// <summary>
		///// Gets the log collector.
		///// </summary>
		///// <param name="ip">The ip.</param>
		///// <param name="login">The login.</param>
		///// <param name="password">The password.</param>
		///// <returns></returns>
		//private ILogCollector GetLogCollector(string ip, string login, string password) {
		//    var logcollector = LogCollector.Create();
		//    logcollector.SignIn(ip + "/scp", login, password, "Local");
		//    return logcollector;
		//}

	}
}

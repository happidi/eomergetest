﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// WolframAlphaREST DataAcquisitionProvider Provider
	/// </summary>
	[LocalizedText("msg_provider_wolfram")]
	public class WolframAlphaRESTDataAcquisitionProvider : RESTDataAcquisitionProvider {

		private const string const_attribute_key = "key";
		private const string const_url = "http://api.wolframalpha.com/v2/query?primary=true&input={0}&format={1}&appid={2}&podid=Result&includepodid=DecimalApproximation&includepodid=Result";
		/// <summary>
		/// Gets or sets the key.
		/// </summary>
		/// <value>
		/// The key.
		/// </value>
		protected string Key { get; set; }
		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_key])) {
				throw new ProviderException(const_attribute_key + " is missing");
			}
			Key = config[const_attribute_key];
			config.Remove(const_attribute_key);

			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}

		/// <summary>
		/// In order to create a generic REST provider, implementor must convert the REST response to a double (meter data)
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">The endpoint.</param>
		/// <param name="RESTresponse">The REST response.</param>
		/// <returns>
		/// parsed response as double
		/// </returns>
		public override List<DataAcquisitionEndpointValue> Parse(RESTDataAcquisitionContainer container, RESTDataAcquisitionEndpoint endpoint, string RESTresponse) {
			var retVal = double.NaN;
			try {
				StringReader sr = new StringReader(RESTresponse);
				XElement e = XElement.Load(sr);
				
				var results =
					e.DescendantNodes().OfType<XElement>().Where(element => element.Name == "plaintext").Select(
						element => element.Value).ToList();

				string stringThatHaveCharacters = results[0].Trim();
				Match m = Regex.Match(stringThatHaveCharacters, "\\d+\\.*\\d*");
				Double number = Double.Parse(m.Value);
				retVal = number;
			}
			catch { }

			return new List<DataAcquisitionEndpointValue>() {new DataAcquisitionEndpointValue {
				AcquisitionDateTime = DateTime.UtcNow,
				Value = retVal
			}};

		}

		/// <summary>
		/// Executes the rest query.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">The endpoint.</param>
		/// <returns></returns>
		protected override string ExecuteRestQuery(RESTDataAcquisitionContainer container, RESTDataAcquisitionEndpoint endpoint) {
			var Url = container.Url;
			var QueryString = endpoint.QueryString;
			var Path = endpoint.Path;

			string query = string.Format(const_url, HttpUtility.UrlEncode(QueryString), "plaintext", Key);
			var retVal = new WebClient().DownloadString(query);
			return retVal;
		}
	}
}

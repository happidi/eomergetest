﻿using System;
using System.Collections.Generic;
using System.Net;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// REST DataAcquisition Provider
	/// </summary>
	public abstract class RESTDataAcquisitionProvider : DataAcquisitionProvider {

		/// <summary>
		/// Get the current value for a specific endpoint.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">the endpoint.</param>
		/// <returns></returns>
		public override List<DataAcquisitionEndpointValue> GetCurrentValue(DataAcquisitionContainer container, DataAcquisitionEndpoint endpoint) {
			var restContainer = Helper.Cast<RESTDataAcquisitionContainer>(container);
			var restEndpoint = Helper.Cast<RESTDataAcquisitionEndpoint>(endpoint);
			var restResponse = this.ExecuteRestQuery(restContainer, restEndpoint);
			var retVal = this.Parse(restContainer, restEndpoint, restResponse);
			

			return retVal;
		}

		/// <summary>
		/// In order to create a generic REST provider, implementor must convert the REST response to a double (meter data)
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">The endpoint.</param>
		/// <param name="RESTresponse">The RES tresponse.</param>
		/// <returns>
		/// parsed response as double
		/// </returns>
		public abstract List<DataAcquisitionEndpointValue> Parse(RESTDataAcquisitionContainer container, RESTDataAcquisitionEndpoint endpoint, string RESTresponse);



		/// <summary>
		/// Empty list in this case.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="parentItem">The parent item.</param>
		/// <returns></returns>
		public override List<TreeNode> GetHierarchy(DataAcquisitionContainer container, DataAcquisitionItem parentItem = null) {
			return new List<TreeNode>();
		}

		/// <summary>
		/// Executes the rest query.
		/// </summary>
		/// <param name="container">The container.</param>
		/// <param name="endpoint">The endpoint.</param>
		/// <returns></returns>
		protected virtual string ExecuteRestQuery(RESTDataAcquisitionContainer container, RESTDataAcquisitionEndpoint endpoint)
		{
			var Url = container.Url;
			var Path = endpoint.Path;
			var QueryString = endpoint.QueryString;


			if (Url == null)
				Url = "";
			if (Path == null)
				Path = "";
			if (QueryString == null)
				QueryString = "";
			var fullqueryaddress = string.Format("{0}/{1}?{2}", Url.TrimEnd('/'), Path.TrimEnd('/'), QueryString.TrimStart('?'));
			var client = new WebClient();
			var retval = client.DownloadString(fullqueryaddress);
			return retval;

		}

		/// <summary>
		/// Perform any other changes to the object such as property manipulation or additions that come from the various DataAcquisition Providers
		/// e.g. Return additional data that came from a web service call in addition to data stored in the data layer.
		/// </summary>
		/// <param name="container">The container</param>
		/// <returns></returns>
		public override DataAcquisitionContainer FillObject(DataAcquisitionContainer container) {
			return container;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Text;
using System.Text.RegularExpressions;
using NSoup;
using NSoup.Safety;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// NSoup AntiXss validation concrete provider
	/// </summary>
	public class NSoupAntiXssValidationProvider : AntiXssValidationProvider {
		private const string const_attribute_autoclean = "autoClean";
		private const string const_attribute_disable = "disable";

		private Whitelist whiteList;
		private List<string> additionalTags;

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
		/// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
		///   
		/// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		///   
		/// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.</exception>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);
			if (String.IsNullOrEmpty(config[const_attribute_autoclean])) {
				throw new ProviderException(const_attribute_autoclean + " is missing");
			}
			AutoClean = bool.Parse(config[const_attribute_autoclean]);
			config.Remove(const_attribute_autoclean);

			if (!String.IsNullOrEmpty(config[const_attribute_disable])) {
				Disable = bool.Parse(config[const_attribute_disable]);
				config.Remove(const_attribute_disable);
			}

			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));

			additionalTags = new List<string>();
			BuildWhiteList();

			// Setup allowed tags from web.config
			var sb = new StringBuilder();
			sb.Append("^(");
			sb.Append(HtmlSanitizerHelper.AllowedTagsString);
			additionalTags.ForEach(tag => {
				sb.Append("|");
				sb.Append(tag);
			});
			sb.Append(")$");

			HtmlSanitizerHelper.AllowedTags = new Regex(sb.ToString());
		}

		/// <summary>
		/// Builds the nsoup white list.
		/// </summary>
		private void BuildWhiteList() {
			var nsoupConfiguration = VizeliaConfiguration.Instance.NSoupConfiguration;
			if (nsoupConfiguration != null) {
				switch (nsoupConfiguration.PreConfiguredWhiteList.Trim()) {
					case "SimpleText":
						whiteList = Whitelist.SimpleText;
						break;
					case "Basic":
						whiteList = Whitelist.Basic;
						break;
					case "BasicWithImages":
						whiteList = Whitelist.BasicWithImages;
						break;
					case "Relaxed":
						whiteList = Whitelist.Relaxed;
						break;
					default:
						whiteList = Whitelist.None;
						break;
				}

				SetManualWhiteList(nsoupConfiguration);
			}
		}

		/// <summary>
		/// Sets manualy the white list according to config section nSoupConfiguration.
		/// </summary>
		/// <param name="nsoupConfiguration">The nsoup configuration.</param>
		private void SetManualWhiteList(NSoupConfigurationElementCollection nsoupConfiguration) {
			foreach (NSoupConfigurationElement element in nsoupConfiguration) {
				AddTagsAndAttributes(element);
			}
		}

		/// <summary>
		/// Adds the tags and attributes.
		/// </summary>
		/// <param name="element">The element.</param>
		private void AddTagsAndAttributes(NSoupConfigurationElement element) {
			string[] seperatedTags = element.TagName.Trim().Split(',');
			string[] seperatedAttributes = element.Attributes.Trim().Split(',');

			foreach (var tag in seperatedTags) {
				foreach (var attribute in seperatedAttributes) {
					if (!whiteList.IsSafeTag(tag.Trim()) && !tag.ToLower().Equals(":all")) {
						whiteList.AddTags(tag.Trim());
						additionalTags.Add(tag.Trim());
					}
					whiteList.AddAttributes(tag.Trim(), attribute.Trim());
					if (attribute.ToLower().Equals(":all")) {
						whiteList.AddAttributes(tag.Trim(), "style");
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the provider should perform auto clean.
		/// </summary>
		/// <value>
		///   <c>true</c> if [auto clean]; otherwise, <c>false</c>.
		/// </value>
		public override bool AutoClean { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the provider is disabled or not
		/// </summary>
		/// <value>
		///   <c>true</c> if [disable]; otherwise, <c>false</c>.
		/// </value>
		public override bool Disable { get; set; }

		/// <summary>
		/// Determines whether the specified body HTML is valid.
		/// </summary>
		/// <param name="bodyHtml">The body HTML.</param>
		/// <returns>
		///   <c>true</c> if the specified body HTML is valid; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsValid(string bodyHtml) {
			return NSoupClient.IsValid(bodyHtml, whiteList);
		}

		/// <summary>
		/// Cleans the specified body HTML.
		/// </summary>
		/// <param name="bodyHtml">The body HTML.</param>
		/// <returns></returns>
		public override string Clean(string bodyHtml) {
			return NSoupClient.Clean(bodyHtml, whiteList);
		}

		/// <summary>
		/// Gets the additional white list tags.
		/// </summary>
		/// <returns></returns>
		public override List<string> GetAdditionalWhiteListTags() {
			return additionalTags;
		}
	}


}
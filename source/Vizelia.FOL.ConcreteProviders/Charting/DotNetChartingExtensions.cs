﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using dotnetCHARTING;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;
using DotNetChart = dotnetCHARTING.Chart;


namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Extensions Methods for the dotnetcharting component
	/// </summary>
	public static class DotNetChartingExtensions {

		/// <summary>
		/// Extension Method to add a list of Series to a SeriesCollection
		/// </summary>
		/// <param name="col">the SeriesCollection object</param>
		/// <param name="series">The List of Series to be added</param>
		public static void Add(this SeriesCollection col, List<Series> series) {
			foreach (var s in series) {
				col.Add(s);
			}
		}

	    /// <summary>
	    /// Return the Chart Image stream. Fix a bug that happens when ShaddingEffect is set on Windows 7.
	    /// </summary>
	    /// <param name="chartComp">The chart comp.</param>
	    /// <param name="keyChart">The key of the chart</param>
	    /// <param name="chartTitle">The title of the chart</param>
	    /// <returns></returns>
	    public static MemoryStream GetChartStreamFix(this DotNetChart chartComp, string keyChart, string chartTitle) {
			var retVal = chartComp.GetChartStream();
			if (retVal == null || retVal.Length == 0) {
				chartComp.ShadingEffect = false;
				retVal = chartComp.GetChartStream();
			}
		    if (retVal == null || retVal.Length == 0) {
                retVal = RenderChartWithErrorMessage(chartComp, keyChart, chartTitle);
		    }
		    return retVal;
		}

        private static MemoryStream RenderChartWithErrorMessage(DotNetChart chartComp, string keyChart, string chartTitle) { 
            chartComp.SeriesCollection.Clear(); //remove series from dotnetcharting component
            chartComp.ChartArea.SeriesCollection.Clear();

            if (chartComp.Type == dotnetCHARTING.ChartType.TreeMap) {
                chartComp.NoDataLabel = new dotnetCHARTING.Label(Helper.LocalizeText("error_chart_cantrendertreemap"), chartComp.ChartArea.Label.Font);
                TracingService.Write(TraceEntrySeverity.Error, chartTitle + " " + Helper.LocalizeText("error_chart_cantrendertreemap", new CultureInfo("en")) + " DotNetCharting Error: " + chartComp.ErrorMessage, "DotNetCharting", keyChart);
            }
            else {
                chartComp.NoDataLabel = new dotnetCHARTING.Label(Helper.LocalizeText("error_chart_cantrender"), chartComp.ChartArea.Label.Font);
                TracingService.Write(TraceEntrySeverity.Error, chartTitle + " Can't render the chart" + " DotNetCharting Error: " + chartComp.ErrorMessage, "DotNetCharting", keyChart);
            }
            var retVal = chartComp.GetChartStream();
            return retVal;
        }

	    /// <summary>
		/// Extension Method to retrieve a list of Element from a ElementCollection (useful for Linq).
		/// </summary>
		/// <param name="col">the ElementCollection object</param>
		public static List<Element> ToList(this ElementCollection col) {
			return col.Cast<Element>().ToList();
		}

		/// <summary>
		/// Extension Method to retrieve a list of Series from a SeriesCollection (useful for Linq).
		/// </summary>
		/// <param name="col">the SeriesCollection object</param>
		public static List<Series> ToList(this SeriesCollection col) {
			return col.Cast<Series>().ToList();
		}

		/// <summary>
		/// Extension Method to retrieve a list of Series from a SeriesCollection (useful for Linq).
		/// </summary>
		/// <param name="col">the SeriesCollection object</param>
		public static List<ChartArea> ToList(this ChartAreaCollection col) {
			return col.Cast<ChartArea>().ToList();
		}

		/// <summary>
		/// Gets the total elements count.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public static int GetTotalElementsCount(this DotNetChart chart) {
			return chart.SeriesCollection.Cast<Series>().Sum(s => s.Elements.Count);
		}

		/// <summary>
		/// Gets the min max.
		/// </summary>
		/// <param name="s">The s.</param>
		/// <param name="MinX">The min X.</param>
		/// <param name="MaxX">The max X.</param>
		/// <param name="MinY">The min Y.</param>
		/// <param name="MaxY">The max Y.</param>
		public static void GetMinMax(this Series s, out double MinX, out double MaxX, out double MinY, out double MaxY) {
			var minX = double.MaxValue;
			var minY = double.MaxValue;
			var maxX = double.MinValue;
			var maxY = double.MinValue;

			s.Elements.ToList().ForEach(e => {
				if (!double.IsNaN(e.YValue)) {
					minY = Math.Min((double)e.YValue, minY);
					maxY = Math.Max((double)e.YValue, maxY);
				}
				if (!double.IsNaN(e.XValue)) {
					minX = Math.Min((double)e.XValue, minX);
					maxX = Math.Max((double)e.XValue, maxX);
				}
			});

			MaxX = maxX;
			MinX = minX;
			MinY = minY;
			MaxY = maxY;
		}

		#region LimitPoints
		/// <summary>
		/// Limits the max number of points for a DataSerie (used for optimizing performances in Chart generation)
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <param name="max">The max.</param>
		/// <param name="GroupOperation">The group operation.</param>
		public static void LimitPoints(this Series serie, int max, Vizelia.FOL.BusinessEntities.MathematicOperator GroupOperation) {
			if (serie.Elements.Count > 0) {
				var pointsFinal = new ElementCollection();

				if (max > 0 && serie.Elements.Count > max && DateTime.MinValue < serie.Elements[0].XDateTime && DateTime.MaxValue > serie.Elements[0].XDateTime) {
					var j = 0;
					var groupCount = Math.Max(2, serie.Elements.Count / max);
					while (j < serie.Elements.Count) {

						DateTime startDate = serie.Elements[j].XDateTime;
						double value = serie.Elements[j].YValue;

						var ts = new TimeSpan();
						int count = 1;
						for (int i = 1; i < groupCount; i++) {
							if (j + i < serie.Elements.Count) {
								var pointCurrent = serie.Elements[j + i];
								var pointPrevious = serie.Elements[j + i - 1];
								switch (GroupOperation) {
									case Vizelia.FOL.BusinessEntities.MathematicOperator.SUM:
										if (!double.IsNaN(pointCurrent.YValue))
											value += pointCurrent.YValue;
										break;
									case Vizelia.FOL.BusinessEntities.MathematicOperator.AVG:
										if (!double.IsNaN(pointCurrent.YValue))
											value += pointCurrent.YValue;
										break;
									case Vizelia.FOL.BusinessEntities.MathematicOperator.MAX:
										if (!double.IsNaN(pointCurrent.YValue))
											value = Math.Max(value, pointCurrent.YValue);
										break;
									case Vizelia.FOL.BusinessEntities.MathematicOperator.MIN:
										if (!double.IsNaN(pointCurrent.YValue))
											value = Math.Min(value, pointCurrent.YValue);
										break;
								}
								if (pointCurrent.XDateTime != null && pointPrevious.XDateTime != null)
									ts += pointCurrent.XDateTime - pointPrevious.XDateTime;
								count++;
							}
						}
						if (GroupOperation == Vizelia.FOL.BusinessEntities.MathematicOperator.AVG)
							value = value / count;

						j += groupCount;
						var point = new Element() {
							XDateTime = startDate + new TimeSpan(ts.Ticks / count),
							YValue = value
						};
						pointsFinal.Add(point);
					}
					serie.Elements.Clear();
					serie.Elements.Add(pointsFinal);
				}
			}
		}

		/// <summary>
		/// Gets the private property value.
		/// </summary>
		/// <param name="s">The s.</param>
		/// <param name="propertyName">Name of the property.</param>
		/// <returns></returns>
		public static object GetPrivatePropertyValue(this Series s, string propertyName) {
			var seriesType = typeof(Series);
			BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Static;
			FieldInfo field = seriesType.GetField(propertyName, bindFlags);
			return field.GetValue(s);
		}
		#endregion

	}


}


﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using dotnetCHARTING;

using VizChart = Vizelia.FOL.BusinessEntities.Chart;
using ChartComponent = dotnetCHARTING.Chart;


namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// This must derive from page since the inner implementation of DotNetCharting that takes care of implementing Highcharts ( Chart.JS.Enable = true)
	/// requires that the chart be placed on a page. (better approach would be to user http handlers, but we can't due to the dotnetchart inner implementation)
	/// </summary>
	public class DotNetChartingToHighChartsPage : Page {
		/// <summary>
		/// On Load Method of the page.
		/// Creates a dotnetcharting chart and add it to the page for it to be rendered as HighCharts js.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnLoad(EventArgs e) {
			base.OnLoad(e);
			string imageToDeletePath = "";
			ContextHelper.ApplicationName = HttpContext.Current.Request["applicationName"];
			VizChart chart = (VizChart)CacheService.GetData(HttpContext.Current.Request["id"]);
			var licence = HttpContext.Current.Request["licence"];
			var tempdirectory = HttpContext.Current.Request["tempdirectory"];
			ChartComponent chartComp = DotNetChartingHelper.Chart_Create(licence, chart, 800, 600, true, tempdirectory, new Padding(), out imageToDeletePath);
			chartComp.MarginTop += 7;
			chartComp.MarginLeft += 3;
			chartComp.MarginRight += 3;
			chartComp.MarginBottom += 3;

			this.Controls.Add(chartComp);
		}
	}
}

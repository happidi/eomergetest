﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a standard charting provider using the library Visifire.
	/// </summary>
	public class VisifireChartingProvider : ChartingProvider {


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

		}



		/// <summary>
		/// Renders the chart as a stream.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		public override StreamResult RenderChartStream(Chart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Renders the chart as an image map.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		public override ImageMap RenderChartImageMap(Chart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding) {
			throw new NotImplementedException();
		}


		/// <summary>
		/// Renders the Chart as a Javascript HightCharts object.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		public override StreamResult RenderChartJavascript(Chart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Renders html chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public override string GetChartHtml(Chart chart) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Gets the palettes.
		/// </summary>
		/// <returns></returns>
		public override List<Tuple<string, List<Color>>> GetPalettes() {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Renders a micro chart.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="value">The value.</param>
		/// <param name="targetValue">The target value.</param>
		/// <param name="minValue">The min value.</param>
		/// <param name="color">The color.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public override StreamResult RenderMicroChart(string text, double value, double targetValue, double? minValue, Color color, int width, int height) {
			throw new NotImplementedException();
		}


	}
}

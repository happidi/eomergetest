﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;
using ChartComponent = dotnetCHARTING.Chart;
using VizChart = Vizelia.FOL.BusinessEntities.Chart;



namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a standard charting provider using the library DotNetCharting.
	/// </summary>
	public class DotNetChartingProvider : ChartingProvider {

		private const string const_attribute_licence = "licence";

		private const string const_attribute_highchartsurl = "highChartsUrl";

		private const string const_highchart_script_start = " new JSCharting.Chart(";

		private const string const_highchart_script_end = "JSCharting.setOptions";


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_licence])) {
				Licence = VizeliaConfiguration.Instance.Deployment.DotNetChartingLicence;
			}
			else {
				Licence = config[const_attribute_licence];
			}
			config.Remove(const_attribute_licence);

			if (String.IsNullOrEmpty(config[const_attribute_highchartsurl])) {
				throw new ProviderException(const_attribute_highchartsurl + " is missing");
			}

			HighChartsUrl = config[const_attribute_highchartsurl];
			config.Remove(const_attribute_highchartsurl);


			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));

		}

		/// <summary>
		/// The m_licence information.
		/// </summary>
		public string Licence { get; set; }

		/// <summary>
		/// Gets or sets the high charts URL.
		/// </summary>
		/// <value>
		/// The high charts URL.
		/// </value>
		public string HighChartsUrl { get; set; }

		/// <summary>
		/// Gets or sets the temp directory location.
		/// </summary>
		/// <value>
		/// The temp directory location.
		/// </value>
		public string TempDirectory {
			get { return VizeliaConfiguration.Instance.Deployment.TempDirectory; }
		}

		/// <summary>
		/// Renders the m_chart as a stream.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		public override StreamResult RenderChartStream(VizChart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding) {
			string imageToDeletePath;

			using (ChartComponent chartComp = DotNetChartingHelper.Chart_Create(this.Licence, chart, width, height, false, TempDirectory, chartPadding, out imageToDeletePath)) {
				MemoryStream stream = DotNetChartingHelper.Chart_GetStream(chartComp, chart, dynamicdisplayStream != null ? dynamicdisplayStream.ContentStream : null, chart.DynamicDisplay == null || chart.DynamicDisplay.IsMainChartVisible, chartPadding);
				DotNetChartingHelper.DeleteImageFile(imageToDeletePath);

				stream.Position = 0;
				return new StreamResult(stream, chart.Title, "png", MimeType.Png);
			}
		}


		/// <summary>
		/// Renders the m_chart as an image map.
		/// </summary>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		public override ImageMap RenderChartImageMap(VizChart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding) {
			var imageMap = new ImageMap();
			string imageToDeletePath;
			using (ChartComponent chartComp = DotNetChartingHelper.Chart_Create(this.Licence, chart, width, height, false, TempDirectory, chartPadding, out imageToDeletePath)) {
				MemoryStream stream = DotNetChartingHelper.Chart_GetStream(chartComp, chart, dynamicdisplayStream != null ? dynamicdisplayStream.ContentStream : null, chart.DynamicDisplay == null || chart.DynamicDisplay.IsMainChartVisible, chartPadding);

				stream.Position = 0;
				imageMap.Base64String = stream.GetBase64String();
				imageMap.Guid = Guid.NewGuid().ToString();
				if (chart.DynamicDisplay == null || chart.UseDynamicDisplay == false || chart.DynamicDisplay.IsMainChartVisible)
					imageMap.MapText = DotNetChartingHelper.ImageMap_GetForExtJS(chartComp);
				imageMap.MapName = chartComp.ImageMapName;

				DotNetChartingHelper.DeleteImageFile(imageToDeletePath);
				stream.Dispose();
			}
			CacheService.Add(imageMap.Guid, imageMap, 3);
			return imageMap;
		}

		/// <summary>
		/// Renders a micro m_chart.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="value">The value.</param>
		/// <param name="targetValue">The target value.</param>
		/// <param name="minValue">The min value.</param>
		/// <param name="color">The color.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <returns></returns>
		public override StreamResult RenderMicroChart(string text, double value, double targetValue, double? minValue, Color color, int width, int height) {
			string imageToDeletePath = "";
			using (ChartComponent chartComp = DotNetChartingHelper.Chart_CreateMicroChart(this.Licence, text, value, targetValue, minValue, color, width, height, TempDirectory, out imageToDeletePath)) {
				MemoryStream stream = DotNetChartingHelper.Chart_GetStream(chartComp, null, null, true, new Padding());
				DotNetChartingHelper.DeleteImageFile(imageToDeletePath);
				stream.Position = 0;
				return new StreamResult(stream, "", "png", MimeType.Png);
			}
		}



		/// <summary>
		/// Renders the Chart as a Javascript HightCharts object.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="dynamicdisplayStream">The dynamicdisplay stream.</param>
		/// <param name="chartPadding">The chart padding.</param>
		/// <returns></returns>
		public override StreamResult RenderChartJavascript(VizChart chart, int width, int height, StreamResult dynamicdisplayStream, Padding chartPadding) {

			//TO DO : Change this
			string guid = Guid.NewGuid().ToString();
			var finalHighChartsUrl = Helper.GetFrontendUrl(HighChartsUrl, VizeliaConfiguration.Instance.Deployment.FrontendUrl);
			Uri uri = new Uri(string.Format("{0}?id={1}&licence={2}&tempdirectory={3}&applicationName={4}", finalHighChartsUrl, guid, this.Licence, this.TempDirectory, ContextHelper.ApplicationName));

			var request = WebRequest.Create(uri);
			CacheService.Add(guid, chart, 10);
			var response = request.GetResponse();

			var responseStream = response.GetResponseStream();
			var streamReader = new StreamReader(responseStream);

			string body = streamReader.ReadToEnd();

			response.Close();

			body = body.Substring(body.IndexOf(const_highchart_script_start, StringComparison.Ordinal) + const_highchart_script_start.Length);
			body = body.Substring(0, body.IndexOf(const_highchart_script_end, StringComparison.Ordinal) - 2);

			var streamResult = StreamResultHelper.GetStreamFromHtml(body);
			return streamResult;
		}

		/// <summary>
		/// Gets the Chart as an html string.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		public override string GetChartHtml(VizChart chart) {
			var retVal = string.Format("<!DOCTYPE html><html><head><title>{0}</title></head><body>{1}</body></html>", chart.Title, chart.Html);
			return retVal;
		}

		/// <summary>
		/// Gets the palettes.
		/// </summary>
		/// <returns></returns>
		public override List<Tuple<string, List<Color>>> GetPalettes() {
			var retVal = new List<Tuple<string, List<Color>>>();
			using (ChartComponent chart = new ChartComponent()) {
				foreach (dotnetCHARTING.Palette p in Enum.GetValues(typeof(dotnetCHARTING.Palette))) {
					if (!p.ParseAsString().StartsWith("FiveColor")) {
						chart.PaletteName = p;
						List<Color> colors = chart.Palette.ToList();
						retVal.Add(new Tuple<string, List<Color>>(p.ParseAsString(), colors));
					}
				}
			}
			return retVal;
		}
	}
}











﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using dotnetCHARTING;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;
using DotNetChart = dotnetCHARTING.Chart;
using DotNetChartType = dotnetCHARTING.ChartType;
using DotNetElement = dotnetCHARTING.Element;
using DotNetGaugeType = dotnetCHARTING.GaugeType;
using VizChart = Vizelia.FOL.BusinessEntities.Chart;
using VizChartType = Vizelia.FOL.BusinessEntities.ChartType;
using VizGaugeType = Vizelia.FOL.BusinessEntities.GaugeType;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Helper Methods for the dotnetcharting component
	/// </summary>
	public static class DotNetChartingHelper {
		#region const
		/// <summary>
		/// Specific tag to represent a new line in the chart image map. Is replaced by a br tag when output.
		/// </summary>
		private const string imagemap_newline = " _newline ";
		#endregion
		#region Axis
		/// <summary>
		/// Extension Method to clone an existing Axis.
		/// </summary>
		/// <param name="axis">the Axis.</param>
		/// <returns></returns>
		private static Axis Axis_Clone(Axis axis) {
			Axis retVal = new Axis();

			retVal.DefaultTick.Label.Truncation.Length = axis.DefaultTick.Label.Truncation.Length;
			retVal.DefaultTick.Label.Truncation.Mode = axis.DefaultTick.Label.Truncation.Mode;
			retVal.DefaultTick.Label.Hotspot.ToolTip = axis.DefaultTick.Label.Hotspot.ToolTip;
			retVal.Scale = axis.Scale;
			retVal.GenerateElementTicks = axis.GenerateElementTicks;
			retVal.TimeInterval = axis.TimeInterval;
			retVal.FormatString = axis.FormatString;
			retVal.TimePadding = axis.TimePadding;
			retVal.Minimum = axis.Minimum;
			retVal.Maximum = axis.Maximum;
			retVal.Orientation = axis.Orientation;
			retVal.TickLabelMode = axis.TickLabelMode;
			retVal.TickLabelAngle = axis.TickLabelAngle;
			retVal.InvertScale = axis.InvertScale;
			retVal.TimeIntervalAdvanced = axis.TimeIntervalAdvanced;
			return retVal;
		}

		/// <summary>
		/// Extension method to transform ChartAxis to dotnetcharting axis
		/// </summary>
		/// <param name="chartaxis">ChartAxis</param>
		/// <returns>Axis</returns>
		private static Axis Axis_Convert(ChartAxis chartaxis) {
			Axis axis = new Axis(chartaxis.KeyChartAxis);
			axis.Label.Text = chartaxis.Name;
			if (chartaxis.Min.HasValue)
				axis.Minimum = chartaxis.Min;
			if (chartaxis.Max.HasValue)
				axis.Maximum = chartaxis.Max;

			axis.AlternateGridBackground.Visible = false;
			axis.ShowGrid = false;
			axis.Orientation = dotnetCHARTING.Orientation.Right;

			if (chartaxis.FullStacked)
				axis.Scale = Scale.FullStacked;

			if (chartaxis.Markers != null && chartaxis.Markers.Count > 0) {
				foreach (var marker in chartaxis.Markers) {
					var axismarker = ChartMarker_Convert(marker);
					if (axismarker != null)
						axis.Markers.Add(axismarker);
				}
			}

			return axis;
		}
		/// <summary>
		/// Hide an Axis.
		/// </summary>
		/// <param name="axis"></param>
		private static void Axis_Hide(Axis axis) {
			axis.Clear();
			Axis_SetColor(axis, Color.Transparent);
			axis.DefaultTick.Line.Width = 0;
			axis.Line.Width = 0;
			axis.DefaultMinorTick.Line.Width = 0;
			axis.GenerateElementTicks = false;
			axis.ShowGrid = false;
			axis.AlternateGridBackground = new Background(Color.Transparent);
		}

		/// <summary>
		/// Licence_Init the defaults Axis format
		/// </summary>
		/// <param name="chartComp">the chart</param>
		/// <param name="chart">The chart.</param>
		private static void Axis_Init(DotNetChart chartComp, VizChart chart) {
			Axis XAxis;
			Axis YAxis;
			GetAxis(chartComp, out XAxis, out YAxis);

			XAxis.DefaultTick.Label.Truncation.Length = chart.DisplayLabelTruncationLength ?? 20;
			XAxis.DefaultTick.Label.Truncation.Mode = TruncationMode.End;
			//XAxis.DefaultTick.Label.Hotspot.ToolTip = "%Value";

			if (chart.DisplayXAxisFontSize.HasValue) {
				XAxis.Label.Font = CreateSafeFont(XAxis.Label.Font.FontFamily, chart.DisplayXAxisFontSize.Value);
				XAxis.DefaultTick.Label.Font = CreateSafeFont(XAxis.DefaultTick.Label.Font.FontFamily, chart.DisplayXAxisFontSize.Value);
			}
			//disable for now as it s only useful for Historical charts and we handle it with Minimum and Maximum for now.
			//	XAxis.TimePadding = Axis_GetPadding(chart.TimeInterval);
			XAxis.TickLabelMode = TickLabelMode.Normal;

			YAxis.DefaultTick.Label.Truncation.Length = chart.DisplayLabelTruncationLength ?? 20;
			YAxis.DefaultTick.Label.Truncation.Mode = TruncationMode.End;
			YAxis.DefaultTick.Label.Hotspot.ToolTip = "%Value ";

			var timeinterval = chart.TimeInterval;
			var xAxisFormat = chart.XAxisFormat;
			var startdate = chart.StartDate;
			var enddate = chart.EndDate;
			var axis = chart.Axis;
			var displaystackseries = chart.DisplayStackSeries;
			var displaystartenddate = chart.DisplayStartEndDate;
			var displaystartenddatefontsize = chart.DisplayStartEndDateFontSize;

			if (timeinterval == AxisTimeInterval.None) {
				XAxis.Scale = Scale.Normal;
				XAxis.GenerateElementTicks = true;
			}
			else if (!ChartHelper.IsDateTime(chart)) {
				XAxis.Scale = Scale.Normal;
			}
			else {

				if (chartComp.Type == DotNetChartType.Combo || chartComp.Type == DotNetChartType.ComboHorizontal || chartComp.Type == DotNetChartType.TreeMap || chartComp.Type == DotNetChartType.Gauges) {

					if (!(timeinterval == AxisTimeInterval.Semesters || timeinterval == AxisTimeInterval.Quarters)) {
						XAxis.Scale = Scale.Time;
					}
					else {
						XAxis.DefaultTick.Label.Text = "%Name";
					}
					XAxis.TimeInterval = AxisTimeInterval_Convert(timeinterval);
					if (timeinterval == AxisTimeInterval.Weeks) {
						XAxis.TimeIntervalAdvanced.StartDayOfWeek = chart.XAxisStartDayOfWeek;
					}
					if (timeinterval == AxisTimeInterval.Semesters) {
						XAxis.TimeIntervalAdvanced.Multiplier = 2;

					}

					XAxis.FormatString = String.IsNullOrEmpty(xAxisFormat) ? timeinterval.GetFormat(true) : xAxisFormat;

					#region Axis time interval simplification
					var actualRange = ChartHelper.GetActualDateRange(chart);
					if (actualRange != null) {
						TimeSpan sp = actualRange.Item2 - actualRange.Item1;
						if (timeinterval == AxisTimeInterval.Minutes) {
							if (sp.TotalHours > 48)
								XAxis.TimeInterval = TimeInterval.Days;
							else if (sp.TotalHours > 10)
								XAxis.TimeInterval = TimeInterval.Hours;
						}
						if (timeinterval == AxisTimeInterval.Hours || timeinterval == AxisTimeInterval.Minutes) {
							if (sp.TotalDays > 90) {
								XAxis.TimeInterval = TimeInterval.Months;
							}
							else if (sp.TotalDays > 15) {
								XAxis.TimeInterval = TimeInterval.Days;
							}
						}
						XAxis.FormatString = String.IsNullOrEmpty(xAxisFormat) ? AxisTimeInterval_Convert(XAxis.TimeInterval).GetFormat(true) : xAxisFormat;
					}
					#endregion
				}
			}

			if (displaystackseries)
				YAxis.Scale = Scale.Stacked;
			else
				YAxis.Scale = Scale.Normal;

			if (chart.YAxisFullStacked)
				YAxis.Scale = Scale.FullStacked;

			if (displaystartenddate) {
				chartComp.ChartArea.Label.Text = ChartAreaLabel_GetFormat(startdate, enddate, chart.GetTimeZone());
				chartComp.ChartArea.Label.Font = CreateSafeFont(chart.DisplayFontFamily, displaystartenddatefontsize, FontStyle.Italic);
			}

			if (chartComp.Type == DotNetChartType.ComboHorizontal) {
				XAxis.InvertScale = true;
			}
			foreach (var a in axis.Values) {
				if (a.IsPrimary) {
					if (chart.ChartType == VizChartType.Gauge) {
						a.Name = String.Empty;
					}
					if (a.Min.HasValue) {
						YAxis.Minimum = a.Min.Value;
					}
					if (a.Max.HasValue) {
						YAxis.Maximum = a.Max.Value;
					}
					foreach (var marker in a.Markers) {
						YAxis.Markers.Add(ChartMarker_Convert(marker));
					}
					if (!string.IsNullOrWhiteSpace(a.Name) && a.Name != "Y") {
						YAxis.Label = new dotnetCHARTING.Label(a.Name, chartComp.DefaultElement.SmartLabel.Font, chartComp.DefaultElement.SmartLabel.Color);
					}
				}
				else {
					var newaxis = Axis_Convert(a);
					newaxis.Scale = displaystackseries ? Scale.Stacked : Scale.Normal;
					if (chart.ChartType == VizChartType.Gauge) {
						newaxis.Name = String.Empty;
					}
					chartComp.AxisCollection.Add(newaxis);
				}
			}
		}

		/// <summary>
		/// Set the Axis color.
		/// </summary>
		/// <param name="axis">the Axis.</param>
		/// <param name="color">the Color.</param>
		private static void Axis_SetColor(Axis axis, Color color) {
			axis.DefaultTick.Label.Color = color;
			axis.Line.Color = color;
			axis.DefaultTick.Line.Color = color;
		}

		/// <summary>
		/// Sets the Axis StartDate and EndDate based on the time interval and calculate the Column Width.
		/// </summary>
		/// <param name="axis">The axis.</param>
		/// <param name="timeinterval">The timeinterval.</param>
		/// <param name="startdate">The startdate.</param>
		/// <param name="enddate">The enddate.</param>
		/// <param name="timezone">The timezone.</param>
		/// <param name="setMin">if set to <c>true</c> [set min].</param>
		/// <param name="setMax">if set to <c>true</c> [set max].</param>
		private static void Axis_SetMinMax(Axis axis, AxisTimeInterval timeinterval, DateTime startdate, DateTime enddate, TimeZoneInfo timezone, bool setMin, bool setMax) {
			switch (timeinterval) {

				case AxisTimeInterval.Days: {
						if ((enddate - startdate).TotalHours > 25) {
							var minimum = new DateTime(startdate.Year, startdate.Month, startdate.Day).AddHours(-12);
							var maximum = new DateTime(enddate.Year, enddate.Month, enddate.Day).AddDays(1).AddSeconds(-1);
							if (setMin)
								axis.Minimum = minimum;
							if (setMax)
								axis.Maximum = maximum;
						}
					}
					break;
				case AxisTimeInterval.Months: {
						//var minimum = startdate.UpdateByInterval(timeinterval).AddInterval(timeinterval, -1).AddSeconds(1);
						//var maximum = enddate.AddSeconds(-1).UpdateByInterval(timeinterval).AddInterval(timeinterval, 1).AddSeconds(-1);
						var minimum = startdate.UpdateByInterval(timeinterval).AddDays(-15);
						var maximum = enddate.UpdateByInterval(timeinterval).AddDays(15);

						if (setMin)
							axis.Minimum = minimum;//DateTimeHelper.ConvertTimeFromUtc(minimum, timezone);
						if (setMax)
							axis.Maximum = maximum;//DateTimeHelper.ConvertTimeFromUtc(maximum, timezone);
					}
					break;
				case AxisTimeInterval.Hours: {
						var minimum = startdate.UpdateByInterval(timeinterval).AddHours(-1).AddSeconds(1);
						var maximum = enddate.UpdateByInterval(timeinterval).AddMinutes(-30);

						if (setMin)
							axis.Minimum = minimum.ConvertTimeFromUtc(timezone);
						if (setMax)
							axis.Maximum = maximum.ConvertTimeFromUtc(timezone);
					}
					break;
				default: {
						var minimum = startdate.UpdateByInterval(timeinterval).AddInterval(timeinterval, -1).AddSeconds(1);
						var maximum = enddate.UpdateByInterval(timeinterval).AddInterval(timeinterval, 1).AddSeconds(-1);

						if (setMin)
							axis.Minimum = minimum.ConvertTimeFromUtc(timezone);
						if (setMax)
							axis.Maximum = maximum.ConvertTimeFromUtc(timezone);
					}
					break;
			}
		}

		/// <summary>
		/// Get the Axis padding based on the timeinterval.
		/// </summary>
		/// <param name="timeInterval">The time interval.</param>
		/// <returns></returns>
		private static TimeSpan Axis_GetPadding(AxisTimeInterval timeInterval) {
			var retVal = TimeSpan.FromSeconds(0);

			switch (timeInterval) {
				case AxisTimeInterval.Years:
					retVal = TimeSpan.FromDays(365 / 2);
					break;

				case AxisTimeInterval.Quarters:
					retVal = TimeSpan.FromDays(90 / 2);
					break;
				case AxisTimeInterval.Semesters:
					retVal = TimeSpan.FromDays(180 / 2);
					break;

				case AxisTimeInterval.Months:
					retVal = TimeSpan.FromDays(30 / 2);
					break;

				case AxisTimeInterval.Weeks:
					retVal = TimeSpan.FromDays(7 / 2);
					break;

				case AxisTimeInterval.Days:
					retVal = TimeSpan.FromHours(12);
					break;

				case AxisTimeInterval.Hours:
					retVal = TimeSpan.FromMinutes(60 / 2);
					break;

				case AxisTimeInterval.Minutes:
					retVal = TimeSpan.FromSeconds(30);
					break;

				case AxisTimeInterval.Seconds:
					retVal = TimeSpan.FromSeconds(0.5);
					break;
			}
			return retVal;

		}
		/// <summary>
		/// Gets the axis.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="XAxis">The X axis.</param>
		/// <param name="YAxis">The Y axis.</param>
		private static void GetAxis(DotNetChart chartComp, out Axis XAxis, out Axis YAxis) {
			bool axisInverted = false;
			GetAxis(chartComp, out XAxis, out YAxis, out axisInverted);
		}
		/// <summary>
		/// Gets the axis.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="XAxis">The X axis.</param>
		/// <param name="YAxis">The Y axis.</param>
		/// <param name="axisInverted">if set to <c>true</c> [axis inverted].</param>
		private static void GetAxis(DotNetChart chartComp, out Axis XAxis, out Axis YAxis, out bool axisInverted) {

			if (chartComp.Type == DotNetChartType.ComboHorizontal) {
				XAxis = chartComp.YAxis;
				YAxis = chartComp.XAxis;
				axisInverted = true;
			}
			else {
				XAxis = chartComp.XAxis;
				YAxis = chartComp.YAxis;
				axisInverted = false;
			}
		}

		#endregion
		#region TimeInterval
		/// <summary>
		/// Extension method to transform AxisTimeInterval to dotnetcharting time interval
		/// </summary>
		/// <param name="timeinterval">the time interval</param>
		/// <returns>DotNetChartType</returns>
		private static TimeInterval AxisTimeInterval_Convert(AxisTimeInterval timeinterval) {
			switch (timeinterval) {
				case AxisTimeInterval.Days:
					return TimeInterval.Days;
				case AxisTimeInterval.Global:
					return TimeInterval.Years;
				case AxisTimeInterval.Hours:
					return TimeInterval.Hours;
				case AxisTimeInterval.Minutes:
					return TimeInterval.Minutes;
				case AxisTimeInterval.Months:
					return TimeInterval.Months;
				case AxisTimeInterval.None:
					return TimeInterval.None;
				case AxisTimeInterval.Quarters:
					return TimeInterval.Quarters;
				case AxisTimeInterval.Semesters:
					return TimeInterval.Quarters;
				case AxisTimeInterval.Seconds:
					return TimeInterval.Seconds;
				case AxisTimeInterval.Weeks:
					return TimeInterval.Weeks;
				case AxisTimeInterval.Years:
					return TimeInterval.Years;
			}
			return TimeInterval.None;
		}

		/// <summary>
		/// Extension method to transform TimeInterval to Axis time interval
		/// </summary>
		/// <param name="timeinterval">the time interval</param>
		/// <returns>DotNetChartType</returns>
		private static AxisTimeInterval AxisTimeInterval_Convert(TimeInterval timeinterval) {
			switch (timeinterval) {
				case TimeInterval.Days:
				case TimeInterval.Day:
					return AxisTimeInterval.Days;
				case TimeInterval.Hours:
				case TimeInterval.Hour:
					return AxisTimeInterval.Hours;
				case TimeInterval.Minutes:
				case TimeInterval.Minute:
					return AxisTimeInterval.Minutes;
				case TimeInterval.Months:
				case TimeInterval.Month:
					return AxisTimeInterval.Months;
				case TimeInterval.None:
					return AxisTimeInterval.None;
				case TimeInterval.Quarters:
				case TimeInterval.Quarter:
					return AxisTimeInterval.Quarters;
				case TimeInterval.Seconds:
				case TimeInterval.Second:
					return AxisTimeInterval.Seconds;
				case TimeInterval.Weeks:
				case TimeInterval.Week:
					return AxisTimeInterval.Weeks;
				case TimeInterval.Years:
				case TimeInterval.Year:
					return AxisTimeInterval.Years;
			}
			return AxisTimeInterval.None;
		}
		#endregion
		#region Markers
		/// <summary>
		/// Axises the X markers_ init.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="timeinterval">The timeinterval.</param>
		/// <param name="NightColorR">The night color R.</param>
		/// <param name="NightColorG">The night color G.</param>
		/// <param name="NightColorB">The night color B.</param>
		/// <param name="NightStartTime">The night start time.</param>
		/// <param name="NightEndTime">The night end time.</param>
		/// <param name="StartDayOfWeek">The start day of week.</param>
		private static void AxisXMarkers_Init(DotNetChart chartComp, AxisTimeInterval timeinterval, int? NightColorR, int? NightColorG, int? NightColorB, string NightStartTime, string NightEndTime, int StartDayOfWeek) {
			if (chartComp.Type == DotNetChartType.ComboHorizontal) {
				AxisXMarkers_Init(chartComp.YAxis, timeinterval, NightColorR, NightColorG, NightColorB, NightStartTime, NightEndTime, StartDayOfWeek);
			}
			else {
				AxisXMarkers_Init(chartComp.XAxis, timeinterval, NightColorR, NightColorG, NightColorB, NightStartTime, NightEndTime, StartDayOfWeek);
			}
		}

		/// <summary>
		/// Init the XAxis markers for Night / Week ends
		/// </summary>
		/// <param name="axis">The axis.</param>
		/// <param name="timeinterval">The timeinterval.</param>
		/// <param name="NightColorR">The night color R.</param>
		/// <param name="NightColorG">The night color G.</param>
		/// <param name="NightColorB">The night color B.</param>
		/// <param name="NightStartTime">The night start time.</param>
		/// <param name="NightEndTime">The night end time.</param>
		/// <param name="StartDayOfWeek">The start day of week.</param>
		private static void AxisXMarkers_Init(Axis axis, AxisTimeInterval timeinterval, int? NightColorR, int? NightColorG, int? NightColorB, string NightStartTime, string NightEndTime, int StartDayOfWeek) {
			if (NightColorB.HasValue && NightColorG.HasValue && NightColorR.HasValue) {
				var nightColor = Color.FromArgb(NightColorR.Value, NightColorG.Value, NightColorB.Value);
				CalendarPattern cp = null;
				#region Hours
				if (axis != null && timeinterval == AxisTimeInterval.Hours || timeinterval == AxisTimeInterval.Minutes || timeinterval == AxisTimeInterval.Seconds) {
					TimeSpan nightStartTime, nightEndTime;
					if (string.IsNullOrEmpty(NightStartTime) == false && string.IsNullOrEmpty(NightEndTime) == false && TimeSpan.TryParse(NightStartTime, out nightStartTime) && TimeSpan.TryParse(NightEndTime, out nightEndTime)) {
						var start = nightStartTime.Hours;
						var end = nightEndTime.Hours;
						string pattern = "";

						for (var i = 1; i <= 24; i++) {
							if (start < end) {
								if (i >= start && i < end)
									pattern += "1";
								else
									pattern += "0";
							}
							else if (start > end) {
								if (i < end || i >= start)
									pattern += "1";
								else
									pattern += "0";
							}
						}
						if (!string.IsNullOrEmpty(pattern) && pattern.Contains("1")) {
							cp = new CalendarPattern(TimeInterval.Hour, TimeInterval.Day, pattern);
							//This was a fix that I think gilles tried to do to make the night time look nicer for column charts,
							//however it offset the night time by half an hour which was a.inaccurate and b.really wrong for line charts.
							//cp.AdjustmentUnit = TimeInterval.Hour;
						}
					}
				}
				#endregion
				#region Days
				if (axis != null && timeinterval == AxisTimeInterval.Days) {
					switch (StartDayOfWeek) {
						//sunday
						case 0:
							cp = new CalendarPattern(TimeInterval.Day, TimeInterval.Week, "0000011");
							break;
						//monday
						case 1:
							cp = new CalendarPattern(TimeInterval.Day, TimeInterval.Week, "1000001");
							break;
						//tuesday
						case 2:
							cp = new CalendarPattern(TimeInterval.Day, TimeInterval.Week, "1100000");
							break;
						//wednesday
						case 3:
							cp = new CalendarPattern(TimeInterval.Day, TimeInterval.Week, "0110000");
							break;
						//thursday
						case 4:
							cp = new CalendarPattern(TimeInterval.Day, TimeInterval.Week, "0011000");
							break;
						//friday
						case 5:
							cp = new CalendarPattern(TimeInterval.Day, TimeInterval.Week, "0001100");
							break;
						//saturday
						case 6:
							cp = new CalendarPattern(TimeInterval.Day, TimeInterval.Week, "0000110");
							break;
					}
					cp.AdjustmentUnit = TimeInterval.Day;

				}
				#endregion
				#region Markers
				if (cp != null) {
					AxisMarker am = new AxisMarker("", new Background(Color.FromArgb(120, nightColor)), cp);
					am.Background.Transparency = 70;
					//am.CalendarPattern = cp;
					am.LegendEntry.Visible = false;
					axis.Markers.Add(am);
				}
				#endregion
			}
		}
		/// <summary>
		/// Extension method to transform a ChartMarker to dotnetcharting AxisMarker
		/// </summary>
		/// <param name="marker"></param>
		/// <returns></returns>
		private static AxisMarker ChartMarker_Convert(ChartMarker marker) {
			var color = Color.White;
			AxisMarker am = null;
			if (marker.ColorR.HasValue && marker.ColorG.HasValue && marker.ColorB.HasValue) {
				color = Color.FromArgb(marker.ColorR.Value, marker.ColorG.Value, marker.ColorB.Value);
			}
			if (marker.Min == marker.Max) {
				am = new AxisMarker(marker.Name, color, marker.Min);
			}
			else {
				am = new AxisMarker(marker.Name, new Background(color), marker.Min, marker.Max);
			}
			am.LegendEntry.Visible = false;
			am.IncludeInAxisScale = true;
			return am;
		}
		#endregion
		#region AlarmDefinition Markers
		/// <summary>
		/// Init the Alarms Definition markers.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="alarms">The alarms.</param>
		private static void AlarmDefinition_Init(DotNetChart chartComp, List<AlarmDefinition> alarms) {
			Axis XAxis;
			Axis YAxis;
			GetAxis(chartComp, out XAxis, out YAxis);
			if (alarms != null && alarms.Count > 0) {
				foreach (var alarm in alarms) {
					if (alarm.Condition != FilterCondition.Script) {
						var markers = AlarmDefinition_Convert(alarm);
						foreach (var marker in markers) {
							YAxis.Markers.Add(marker);
						}
					}					
				}

			}
		}

		/// <summary>
		/// Extension method to transform a AlarmDefinition to dotnetcharting AxisMarker
		/// </summary>
		/// <param name="alarm">The alarm.</param>
		/// <returns></returns>
		private static List<AxisMarker> AlarmDefinition_Convert(AlarmDefinition alarm) {
			var retVal = new List<AxisMarker>();
			var color = alarm.GetColor();

			if (alarm.UsePercentageDelta)
				return retVal;

			if (alarm.Threshold.HasValue && alarm.Threshold2.HasValue) {
				var min = Math.Min(alarm.Threshold.Value, alarm.Threshold2.Value);
				var max = Math.Max(alarm.Threshold.Value, alarm.Threshold2.Value);

				if (alarm.Condition == FilterCondition.Outside) {
					retVal.Add(new AxisMarker(alarm.Title, color, min));
					if (min != max)
						retVal.Add(new AxisMarker(alarm.Title, color, max));
				}
				else {
					if (min != max)
						retVal.Add(new AxisMarker(alarm.Title, color, min, max));
					else
						retVal.Add(new AxisMarker(alarm.Title, color, min));
				}
			}
			else if (alarm.Threshold.HasValue) {
				retVal.Add(new AxisMarker(alarm.Title, color, alarm.Threshold.Value));
			}
			else if (alarm.Threshold2.HasValue) {
				retVal.Add(new AxisMarker(alarm.Title, color, alarm.Threshold.Value));
			}
			retVal.ForEach(am => {
				am.LegendEntry.Visible = false;
				am.IncludeInAxisScale = true;
				am.BringToFront = true;
				am.Label.Hotspot.ToolTip = alarm.Title;
			});
			return retVal;
		}
		#endregion
		#region Background
		/// <summary>
		/// Sets the Chart's background style
		/// </summary>
		/// <param name="chartComp">Chart component</param>
		/// <param name="chart">The chart.</param>
		/// <param name="tempDirectory">The temp directory.</param>
		/// <param name="imageToDeletePath">The image to delete path.</param>
		private static void Background_Init(DotNetChart chartComp, VizChart chart, string tempDirectory, out string imageToDeletePath) {
			var startColor = Color.Transparent;
			var endColor = Color.Transparent;
			imageToDeletePath = "";
			if (!string.IsNullOrEmpty(chart.DisplayChartAreaBackgroundStart))
				startColor = ColorHelper.GetColor(chart.DisplayChartAreaBackgroundStart);
			if (!string.IsNullOrEmpty(chart.DisplayChartAreaBackgroundEnd))
				endColor = ColorHelper.GetColor(chart.DisplayChartAreaBackgroundEnd);

			chartComp.ChartArea.Background = new Background(startColor, endColor, 90);

			var bgColor = Color.Transparent;
			if (chart.UseDynamicDisplay == false && !string.IsNullOrEmpty(chart.DisplayBackground)) {
				bgColor = ColorHelper.GetColor(chart.DisplayBackground);
			}
			chartComp.Background = new Background(bgColor);


			if (chart.ChartAreaPictureImage != null) {
				var path = tempDirectory + "\\" + Guid.NewGuid().ToString() + ".png";
				var bytes = chart.ChartAreaPictureImage.Image.GetStream().ToArray();
				bytes = ImageHelper.ResizeBestFit(bytes, chartComp.Width.Value, chartComp.Height.Value, 0.7);
				ImageHelper.ConvertBytesToImageFile(bytes, path);
				chartComp.ChartArea.Background = new Background(path, BackgroundMode.Image);
				imageToDeletePath = path;
			}
			else if (startColor == Color.Transparent && endColor == Color.Transparent) {
				chartComp.ChartArea.ClearColors();
				chartComp.DefaultAxis.DefaultTick.GridLine.Visible = false;
				chartComp.DefaultAxis.DefaultTick.GridLine.Color = Color.Empty;
				chartComp.DefaultAxis.AlternateGridBackground.Color = Color.Empty;
			}
			chartComp.NoDataLabel = new dotnetCHARTING.Label(GetNoDataLabel(chart), chartComp.ChartArea.Label.Font);
		}

		/// <summary>
		/// Gets the no data label.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		private static string GetNoDataLabel(VizChart chart) {
			var sb = new StringBuilder();
			sb.AppendLine(Helper.LocalizeText("error_chart_nodata").Replace("<BR/>", Environment.NewLine));
			sb.AppendLine(ChartAreaLabel_GetFormat(chart.StartDate, chart.EndDate, chart.GetTimeZone()));
			sb.AppendLine(chart.MetersCount.ToString() + " " + Helper.LocalizeText("msg_meters"));
			return sb.ToString();
		}

		/// <summary>
		/// Sets the background transparent.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		private static void Background_SetTransparent(DotNetChart chartComp) {
			//chartComp.ChartArea.Shadow.Visible = false;
			//chartComp.ChartArea.Background = new Background(Color.Transparent);
			chartComp.TitleBox.Shadow.Visible = false;

			chartComp.DefaultAxis.Label.Shadow.Color = Color.Transparent;
			chartComp.DefaultAxis.DefaultTick.Label.Shadow.Color = Color.Transparent;
			chartComp.DefaultElement.SmartLabel.Shadow.Color = Color.Transparent;
			chartComp.DefaultSeries.LegendEntry.LabelStyle.Shadow.Color = Color.Transparent;

			chartComp.BackColor = Color.Transparent;
			chartComp.Background = new Background(Color.Transparent);
			chartComp.Background.Transparency = 100;
		}
		#endregion
		#region Chart (Create, GetStream, Init, Performance Optimize)
		/// <summary>
		/// Create a dotnetcharting component from a Chart.
		/// </summary>
		/// <param name="licence">The licence.</param>
		/// <param name="chart">The chart data.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="JSEnabled">if set to <c>true</c> [JS enabled].</param>
		/// <param name="tempDirectory">The temp directory.</param>
		/// <param name="padding">The padding.</param>
		/// <param name="imageToDeletePath">The image to delete path.</param>
		/// <returns></returns>
		public static DotNetChart Chart_Create(string licence, VizChart chart, int width, int height, bool JSEnabled, string tempDirectory, Padding padding, out string imageToDeletePath) {
			imageToDeletePath = "";
			var chartComp = new DotNetChart();
			JS_Init(chartComp, JSEnabled);
			Chart_Init(chartComp, chart, licence, width, height, tempDirectory, out imageToDeletePath);

			if (chart.IsEnergyAggregatorLoadingMeterData) {
				chartComp.NoDataLabel = new dotnetCHARTING.Label(Helper.LocalizeText("error_chart_energyaggregatorloadingmeterdata"));
			}
			else {
				//we skip this part in order not to limit the number of points
				if (chart.SpecificAnalysis != ChartSpecificAnalysis.CalendarView || !SpecificAnalysis_CalendarView_TimeInterval_Validate(chart.CalendarViewTimeInterval)) {
					if (HistoricalAnalysis_Enabled(chart) && !HistoricalAnalysis_UseTimeScale(chart))
						FillMissingValuesWithNulls(chart);
					DataSeries_Generate(chartComp, chart);
					EventLog_Generate(chartComp, chart);
					HistoricalAnalysis_Generate(chartComp, chart, padding);
					Gauges_Update(chartComp, chart);
					Pies_Update(chartComp, chart, JSEnabled);
				}
				SpecificAnalysis_Apply(chartComp, chart, width, height);
			}
			return chartComp;
		}



		/// <summary>
		/// Returns the chart's image memory stream.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="dynamicdisplayStream">the optional dynamicdisplay stream.</param>
		/// <param name="chartVisible">if set to <c>true</c> the chart will be visible, <c>false</c> to only display the dynamicdisplay.(if defined)</param>
		/// <param name="padding">The padding.</param>
		/// <returns></returns>
		public static MemoryStream Chart_GetStream(DotNetChart chartComp, VizChart chart, Stream dynamicdisplayStream, bool chartVisible, Padding padding) {
			Chart_PerformanceOptimize(chartComp);
			padding = Padding_Update(chartComp, chart, padding);
			MemoryStream retVal = null;
			if (string.IsNullOrEmpty(chart.DisplayBackground) || chart.UseDynamicDisplay)
				Background_SetTransparent(chartComp);
			InfoIcon_Init(chartComp, chart, padding);

			chartComp.MarginLeft += padding.Left;
			chartComp.MarginRight += padding.Right;
			chartComp.MarginBottom += padding.Bottom;
			chartComp.MarginTop += padding.Top;

			if (chartComp.Width.Value < 300 && chartComp.Height.Value < 300) {
				//chartComp.Dpi = 300;
			}
		    if (chartVisible || dynamicdisplayStream == null) {
                
		       // var bitMap = chartComp.GetChartBitmap();
                retVal = chartComp.GetChartStreamFix(chart.KeyChart, chart.Title);		        
		    }
				
			//useful for debug only
			//chartComp.SaveState(@"d:\temp\chart.xml");

			if (dynamicdisplayStream != null) {
				var rect = new Rectangle(0, 0, (int)chartComp.Width.Value, (int)chartComp.Height.Value);

				var bitmap = new BitmapWithGraphics(dynamicdisplayStream);
				if (chartVisible) {
					//in case of MicroChart, we dont control the MicroChart Size, so after render we get the rendered annotation size and we offset the Chart accordingly
					if (chart.SpecificAnalysis == ChartSpecificAnalysis.MicroChart && chartComp != null && chartComp.ObjectChart != null) {
						var annotationSize = ((Annotation)chartComp.ObjectChart).GetRectangle();
						int offsetX = ((int)chartComp.Width.Value - chartComp.MarginLeft - chartComp.MarginRight - annotationSize.Width) / 2;
						int offsetY = ((int)chartComp.Height.Value - chartComp.MarginTop - chartComp.MarginBottom - annotationSize.Height) / 2;

						bitmap.DrawStream(retVal, new Point(offsetX, offsetY));
					}
					else {
						bitmap.DrawStream(retVal, rect);
					}
				}
				retVal = bitmap.GetStream();

			}
			return retVal;
		}

		/// <summary>
		/// Inits the default config.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="licence">The licence.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="tempDirectory">The temp directory.</param>
		/// <param name="imageToDeletePath">The image to delete path.</param>
		private static void Chart_Init(DotNetChart chartComp, VizChart chart, string licence, int width, int height, string tempDirectory, out string imageToDeletePath) {
			imageToDeletePath = "";
			Licence_Init(chartComp, width, height, licence, Helper.GetCurrentUICulture());
			if (chart != null) {
				Fonts_Init(chartComp, chart.DisplayFontFamily, chart.DisplayFontSize, chart.DisplayFontColor);
				ChartType_Init(chartComp, chart.ChartType, chart.GaugeType);
				SerieType_Init(chartComp, chart.DataSerieType);
				Background_Init(chartComp, chart, tempDirectory, out imageToDeletePath);
				Style_Init(chartComp, chart.DisplayShaddingEffect, chart.Display3D, chart.DisplayMarkerSize, chart.DisplayLineThickness, chart.DisplayTransparency, chart.DisplayLegendVisible, chart.DisplayValues, chart.NumericFormat, chart.UseDynamicDisplay);
				Legend_Init(chartComp, chart);
				Axis_Init(chartComp, chart);
				Tooltip_Init(chartComp, chart.NumericFormat);
				AxisXMarkers_Init(chartComp, chart.TimeInterval, chart.NightColorR, chart.NightColorG, chart.NightColorB, chart.NightStartTime, chart.NightEndTime, chart.XAxisStartDayOfWeek);
				AlarmDefinition_Init(chartComp, chart.AlarmDefinitions);
			}
		}

		/// <summary>
		/// Optimize the chart performance is the number of Elements is high.
		/// (hide markers, labels etc...)
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		private static void Chart_PerformanceOptimize(DotNetChart chartComp) {
			if (chartComp.GetTotalElementsCount() > 1000) {
				chartComp.PrinterOptimizedText = false;
				chartComp.DefaultElement.SmartLabel = null;
				chartComp.DefaultElement.ToolTip = "";
				//chartComp.DefaultElement.Marker.Visible = false;
				foreach (Series s in chartComp.SeriesCollection) {
					if (s.Elements.Count >= 500) {
						s.DefaultElement.SmartLabel = null;
						s.DefaultElement.ToolTip = "";
						s.DefaultElement.Marker.Visible = false;

						foreach (Element e in s.Elements) {
							e.ToolTip = "";
							e.SmartLabel = null;
							e.Marker.Visible = false;
							e.URL = "";
						}
						var isSeriesTypeSet = (bool)s.GetPrivatePropertyValue("seriesTypeSet");
						if (s.Type.ToString() == SeriesType.Marker.ToString() || (chartComp.DefaultSeries.Type.ToString() == SeriesType.Marker.ToString() && isSeriesTypeSet == false))
							s.Type = SeriesType.Line;
					}
				}
			}
		}

		/// <summary>
		/// Fix huge values in the Chart.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		private static void Chart_HugeValueFix(DotNetChart chartComp) {
			foreach (var s in chartComp.SeriesCollection.ToList()) {
				if (s.YAxis != null) {
					var yaxis = s.YAxis;
					double minValue = double.MaxValue;
					double maxValue = double.MinValue;
					var toDelete = new List<Element>();
					foreach (Element e in s.Elements) {
						if (double.IsInfinity(e.YValue) == false) {
							minValue = Math.Min(e.YValue, minValue);
							maxValue = Math.Max(e.YValue, maxValue);
						}
						else {
							toDelete.Add(e);
						}
					}
					foreach (Element e in toDelete) {
						s.Elements.Remove(e);
					}

					if (Math.Abs(minValue) > 18300000000000000000 || Math.Abs(maxValue) > 18300000000000000000) {
						if (double.IsNaN((double)yaxis.Minimum)) {
							yaxis.Minimum = minValue;
						}
						else {
							yaxis.Minimum = Math.Min(minValue, (double)yaxis.Minimum);
						}

						if (double.IsNaN((double)yaxis.Maximum)) {
							yaxis.Maximum = maxValue;
						}
						else {
							yaxis.Maximum = Math.Max(maxValue, (double)yaxis.Maximum);
						}

					}
				}
			}
		}
		#endregion
		#region ChartArea Label
		/// <summary>
		/// Return the Chart Area Label text from a time period.
		/// </summary>
		/// <param name="startdate">the Start Date.</param>
		/// <param name="enddate">the End Date.</param>
		/// <param name="timezone">The timezone.</param>
		/// <returns></returns>
		private static string ChartAreaLabel_GetFormat(DateTime startdate, DateTime enddate, TimeZoneInfo timezone) {
			var dateRangeLabel = ChartHelper.GenerateDateRangeLabel(startdate, enddate, timezone);
			string label = string.Format("<block>{0}<block>", dateRangeLabel);

			return label;
		}
		#endregion
		#region ChartType, DataSerieType
		/// <summary>
		/// Extension method to transform ChartType to dotnetcharting chart type
		/// </summary>
		/// <param name="type">VizChartType</param>
		/// <returns>DotNetChartType</returns>
		private static DotNetChartType ChartType_Convert(VizChartType type) {
			switch (type) {
				case VizChartType.Combo:
					return DotNetChartType.Combo;
				case VizChartType.ComboHorizontal:
					return DotNetChartType.ComboHorizontal;
				case VizChartType.Donut:
					return DotNetChartType.Donut;
				case VizChartType.Gauge:
					return DotNetChartType.Gauges;
				case VizChartType.Pie:
					return DotNetChartType.Pie;
				case VizChartType.Radar:
					return DotNetChartType.Radar;
				case VizChartType.TreeMap:
					return DotNetChartType.TreeMap;
			}
			return DotNetChartType.Combo;
		}

		/// <summary>
		/// Sets the Chart's default type
		/// </summary>
		/// <param name="chartComp">Chart component</param>
		/// <param name="chartType">Type of the chart.</param>
		/// <param name="gaugeType">Type of the gauge.</param>
		private static void ChartType_Init(DotNetChart chartComp, VizChartType chartType, VizGaugeType gaugeType) {
			chartComp.Type = ChartType_Convert(chartType);
			if (chartType == VizChartType.Gauge) {
				chartComp.DefaultSeries.GaugeType = GaugeType_Convert(gaugeType);
				chartComp.DefaultSeries.GaugeBorderBox.ClearColors();
				if (gaugeType == VizGaugeType.Thermometer)
					chartComp.DefaultSeries.GaugeLinearStyle = GaugeLinearStyle.Thermometer;
			}
			if (chartType == VizChartType.TreeMap) {
				//chartComp.DefaultSeries.Box.Header.Label.Text = "%Name";
				//chartComp.DefaultSeries.Box.Header.VerticalAlignment = EdgeAlignment.Outside;
			}
		}

		/// <summary>
		/// Check if the Chart is a Gauge or a Pie or a Donut.
		/// </summary>
		/// <param name="chartComp">the chart comp.</param>
		/// <returns></returns>
		private static bool IsPieDonutGaugeRadarTreeMap(DotNetChart chartComp) {
			return chartComp.Type == DotNetChartType.Gauges || chartComp.Type == DotNetChartType.Pie || chartComp.Type == DotNetChartType.Pies || chartComp.Type == DotNetChartType.Donut || chartComp.Type == DotNetChartType.Donuts || chartComp.Type == DotNetChartType.Radar || chartComp.Type == DotNetChartType.Radars || chartComp.Type == DotNetChartType.TreeMap;
		}

		/// <summary>
		/// Extension method to transform ChartType to dotnetcharting chart type
		/// </summary>
		/// <returns>DotNetChartType</returns>
		private static SeriesType DataSerieType_Convert(DataSerieType type) {
			switch (type) {
				case DataSerieType.AreaLine:
					return SeriesType.AreaLine;
				case DataSerieType.AreaSpline:
					return SeriesType.AreaSpline;
				case DataSerieType.Column:
					return SeriesType.Column;
				case DataSerieType.Line:
					return SeriesType.Line;
				case DataSerieType.Marker:
					return SeriesType.Marker;
				case DataSerieType.Spline:
					return SeriesType.Spline;
			}
			return SeriesType.Bar;
		}

		/// <summary>
		/// Sets the Chart's default type
		/// </summary>
		/// <param name="chartComp">Chart component</param>
		/// <param name="type">the chart type</param>
		private static void SerieType_Init(DotNetChart chartComp, DataSerieType type) {
			chartComp.DefaultSeries.Type = DataSerieType_Convert(type);
		}

		#endregion

		#region DataPoint



		/// <summary>
		/// Extension method to transform DataPoint to dotnetcharting Element.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="Point">The point.</param>
		/// <param name="numericFormat">The numeric format.</param>
		/// <returns></returns>
		private static DotNetElement DataPoint_Convert(DotNetChart chartComp, DataPoint Point, string numericFormat) {
			DotNetElement retVal = null;
			if (Point.XDateTime.HasValue) {
				retVal = new DotNetElement() { XDateTime = Point.XDateTime.Value };

				Axis xAxis;
				Axis yAxis;
				GetAxis(chartComp, out xAxis, out yAxis);

				if (xAxis.TimeInterval == TimeInterval.Quarters) {
					retVal.Name = Point.Name;
					}

				if (Point.YValue.HasValue)
					retVal.YValue = Point.YValue.Value;
			}
			else if (Point.XValue.HasValue) {
				retVal = new DotNetElement() { XValue = Point.XValue.Value, YValue = Point.YValue.Value };
				if (string.IsNullOrEmpty(Point.Name) == false)
					retVal.Name = Point.Name;
			}
			else {
				retVal = new DotNetElement() { Name = Point.Name ?? "" };
				if (Point.YValue.HasValue)
					retVal.YValue = Point.YValue.Value;
			}
			if (Point.YValueStart.HasValue) {
				retVal.YValueStart = Point.YValueStart.Value;
			}
			if (Point.Color.HasValue) {
				retVal.Color = Point.Color.Value;
			}
			if (Point.OverridenYValue.HasValue) {
				retVal.BubbleSize = retVal.YValue;
				retVal.YValue = Point.OverridenYValue.Value;
			}
			if (!string.IsNullOrEmpty(Point.Tooltip)) {
				if (chartComp != null && chartComp.DefaultSeries.DefaultElement.ToolTip != null) {
					retVal.ToolTip = chartComp.DefaultSeries.DefaultElement.ToolTip + imagemap_newline;
				}
				else
					retVal.ToolTip = "";
				retVal.ToolTip += Point.Tooltip;
			}
			//fix for formatting the XValue (used for correlation by example)
			if (double.IsNaN(retVal.XValue) == false && !chartComp.JS.Enabled) {
				if (string.IsNullOrEmpty(retVal.ToolTip))
					retVal.ToolTip = chartComp.DefaultSeries.DefaultElement.ToolTip;
				retVal.ToolTip = retVal.ToolTip.Replace("%XValue", "<%XValue," + numericFormat + ">");
			}

			if (!string.IsNullOrEmpty(Point.HighlightColor)) {
				var highlightColor = ColorHelper.GetColor(Point.HighlightColor);
				switch (Point.Highlight) {
					case DataPointHighlight.Fill:
						retVal.Color = highlightColor;
						break;

					case DataPointHighlight.Border:
						retVal.Outline.Color = highlightColor;
						//retVal.Outline.Width = 2;
						break;

					case DataPointHighlight.Hatch:
						retVal.HatchColor = highlightColor;
						retVal.HatchStyle = HatchStyle.BackwardDiagonal;
						break;
				}
			}
			return retVal;
		}
		#endregion
		#region DataSerie
		/// <summary>
		/// Datas the serie_ convert.
		/// </summary>
		/// <param name="serie">The serie.</param>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="numericformat">The numericformat.</param>
		/// <param name="maxDataPoints">The max data points.</param>
		/// <param name="emptyElementDateTime">The empty element date time.</param>
		/// <param name="includePoints">if set to <c>true</c> [include points].</param>
		/// <param name="includeEmptyPoints">if set to <c>true</c> [include empty points].</param>
		/// <returns>
		/// Series.
		/// </returns>
		private static Series DataSerie_Convert(DataSerie serie, DotNetChart chartComp, string numericformat, int maxDataPoints, DateTime emptyElementDateTime, bool includePoints = true, bool includeEmptyPoints = false) {

			// We limit here the number of elements for performances purposes.
			//if (includePoints)
			//	DataSerieHelper.LimitPoints(serie, maxDataPoints);

			var newserie = new Series(DataSerieHelper.FixName(Helper.LocalizeInText(serie.Name, "%")));
			newserie.DefaultElement.CustomAttributes.Add("Unit", serie.Unit);
			newserie.LegendEntry.CustomAttributes.Add("Unit", serie.Unit);
			newserie.ParameterCollection.Add(new dotnetCHARTING.Parameter("GroupOperation", serie.GroupOperation.ParseAsString(), FieldType.Text));
			if (serie.HideLabel == true) {
				newserie.DefaultElement.ShowValue = !serie.HideLabel;
			}

			if (serie.Type != DataSerieType.None)
				newserie.Type = DataSerieType_Convert(serie.Type);
			if (serie.DisplayTransparency.HasValue)
				newserie.DefaultElement.Transparency = serie.DisplayTransparency.Value;
			if (serie.ColorR.HasValue && serie.ColorG.HasValue && serie.ColorB.HasValue) {
				newserie.DefaultElement.Color = Color.FromArgb(serie.ColorR.Value, serie.ColorG.Value, serie.ColorB.Value);
			}
			if (serie.LineWidth.HasValue) {
				newserie.Line.Width = serie.LineWidth.Value;
				newserie.DefaultElement.Outline.Width = serie.LineWidth.Value;
				if (serie.LineWidth.Value == 0)
					newserie.DefaultElement.Outline.Color = Color.Transparent;
			}

			if (serie.MarkerSize.HasValue)
				newserie.DefaultElement.Marker.Size = serie.MarkerSize.Value;

			newserie.DefaultElement.Marker.Type = MarkerType_Convert(serie.MarkerType);
			newserie.Line.DashStyle = DashStyle_Convert(serie.LineDashStyle);
			newserie.DefaultElement.Outline.DashStyle = DashStyle_Convert(serie.LineDashStyle);

			if (serie.ZIndex.HasValue)
				newserie.Position = -serie.ZIndex.Value;

			if (string.IsNullOrEmpty(serie.KeyYAxis) == false) {
				foreach (Axis axis in chartComp.AxisCollection) {
					if (axis.Name == serie.KeyYAxis) {
						if (chartComp.Type == DotNetChartType.ComboHorizontal) {
							newserie.XAxis = axis;
							newserie.YAxis = chartComp.YAxis;
							if (axis.Orientation == dotnetCHARTING.Orientation.Right) {
								axis.Orientation = dotnetCHARTING.Orientation.Top;
							}
						}
						else {
							newserie.YAxis = axis;
							newserie.XAxis = chartComp.XAxis;
						}
					}
				}
			}
			else {
				if (chartComp.Type == DotNetChartType.ComboHorizontal) {
					newserie.XAxis = chartComp.XAxis;
				}
				else {
					newserie.XAxis = chartComp.XAxis;
					newserie.YAxis = chartComp.YAxis;
				}
			}
			if (serie.DisplayLegendEntry == DataSerieLegendEntry.NONE) {
				newserie.LegendEntry.Value = "";
			}
			else {
				newserie.LegendEntry.Value = string.Format("<{0},{1}> %Unit", DataSerieLegendEntry_Convert(serie.DisplayLegendEntry), numericformat);
			}

			newserie.LegendEntry.Visible = !serie.HideLegendEntry;
			//We use a For instead of a Linq query for performances.
			if (includePoints) {
				var points = serie.PointsWithValue;
				if (includeEmptyPoints)
					points = serie.Points;
				if (points.Count > 0) {
					for (var j = 0; j < points.Count; j++) {
						if (!points[j].Hidden)
							newserie.Elements.Add(DataPoint_Convert(chartComp, points[j], numericformat));
					}
				}
				else {
					//newserie.Elements.Add(new Element() {
					//	XDateTime = emptyElementDateTime,
					//	YValue = 0
					//});
				}
			}

			newserie.LegendEntry.Hotspot.URL = string.Format("return Viz.grid.Chart.onDataSerieLegendEntryClick(this, '{0}', '{1}', '{2}','{3}',{4});", serie.KeyChart, serie.KeyDataSerie, HttpUtility.JavaScriptStringEncode(serie.LocalId), HttpUtility.JavaScriptStringEncode(serie.Name), serie.Hidden ? "false" : "true");

			if (includePoints)
				newserie.LimitPoints(maxDataPoints, serie.GroupOperation);

			return newserie;
		}

		/// <summary>
		/// Dashes the style_ convert.
		/// </summary>
		/// <param name="lineDashStyle">The line dash style.</param>
		/// <returns></returns>
		private static DashStyle DashStyle_Convert(LineDashStyle lineDashStyle) {
			var retVal = DashStyle.Solid;
			switch (lineDashStyle) {
				case LineDashStyle.Dash:
					retVal = DashStyle.Dash;
					break;
				case LineDashStyle.DashDot:
					retVal = DashStyle.DashDot;
					break;

				case LineDashStyle.DashDotDot:
					retVal = DashStyle.DashDotDot;
					break;

				case LineDashStyle.Dot:
					retVal = DashStyle.Dot;
					break;

				case LineDashStyle.Solid:
					retVal = DashStyle.Solid;
					break;

			}
			return retVal;
		}

		private static ElementMarkerType MarkerType_Convert(MarkerType markerType) {
			var retVal = ElementMarkerType.Circle;
			switch (markerType) {
				case MarkerType.ArrowDown:
					retVal = ElementMarkerType.ArrowDown;
					break;

				case MarkerType.ArrowUp:
					retVal = ElementMarkerType.ArrowUp;
					break;

				case MarkerType.Circle:
					retVal = ElementMarkerType.Circle;
					break;

				case MarkerType.Diamond:
					retVal = ElementMarkerType.Diamond;
					break;

				case MarkerType.Dividend:
					retVal = ElementMarkerType.Dividend;
					break;
				case MarkerType.Merger:
					retVal = ElementMarkerType.Merger;
					break;
				case MarkerType.None:
					retVal = ElementMarkerType.None;
					break;

				case MarkerType.ReverseSplit:
					retVal = ElementMarkerType.ReverseSplit;
					break;

				case MarkerType.Spinoff:
					retVal = ElementMarkerType.Spinoff;
					break;

				case MarkerType.Split:
					retVal = ElementMarkerType.Split;
					break;

				case MarkerType.Square:
					retVal = ElementMarkerType.Square;
					break;

				case MarkerType.Star:
					retVal = ElementMarkerType.FivePointStar;
					break;

				case MarkerType.Triangle:
					retVal = ElementMarkerType.Triangle;
					break;

				case MarkerType.TriangleUpsideDown:
					retVal = ElementMarkerType.TriangleUpsideDown;
					break;
			}
			return retVal;
		}

		/// <summary>
		/// Extension method to transform List of DataSerie to dotnetcharting SeriesCollection.
		/// </summary>
		/// <param name="series">The DataSerie List.</param>
		/// <param name="chartComp">the DotNetCharting Chart.</param>
		/// <param name="chart">The chart.</param>
		/// <returns>
		/// Series.
		/// </returns>
		private static SeriesCollection DataSeries_Convert(List<DataSerie> series, DotNetChart chartComp, VizChart chart) {
			SeriesCollection retVal = new SeriesCollection();
			if (series != null && series.Count > 0) {
				//we only convert if at least one serie has data.
				if (series.Any(s => s.HasPointsWithValue)) {
					for (var i = 0; i < series.Count; i++) {
						if (!series[i].Hidden && series[i].HasPointsWithValue) {
							var s = DataSerie_Convert(series[i], chartComp, chart.NumericFormat, chart.MaxDataPointPerDataSerie, chart.StartDate, true, HistoricalAnalysis_Enabled(chart));
							if (chart.DrillDownEnabled) {
								DrillDown_Generate(series[i], s, chart);
							}
							retVal.Add(s);
						}
					}
				}
			}
			return retVal;
		}

		/// <summary>
		/// Generes the data series.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="chart">The chart.</param>
		private static void DataSeries_Generate(DotNetChart chartComp, VizChart chart) {
			if (chart.Series != null) {
				var retVal = DataSeries_Convert(chart.Series.ToList(), chartComp, chart);
				chartComp.SeriesCollection.Add(retVal);
			}
		}

		/// <summary>
		/// Projects the series collection to unique serie.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="sc">The sc.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="JSEnabled">if set to <c>true</c> [JS enabled].</param>
		private static void ProjectSeriesCollectionToUniqueSerie(DotNetChart chartComp, SeriesCollection sc, VizChart chart, bool JSEnabled) {
			if (sc != null && sc.Count > 0) {
				if (JSEnabled) {
					chartComp.DefaultElement.SmartLabel.Text = chartComp.DefaultElement.SmartLabel.Text.Replace("%Sum", "%Value").Replace("%Unit", "").Replace("%Name", "");
					chartComp.DefaultElement.ToolTip = chartComp.DefaultElement.ToolTip.Replace("%SeriesName :", "");
				}
				Series newSeries = new Series();
				sc.ToList().ForEach(serie => {
					if (serie.Elements != null && serie.Elements.Count > 0) {
						var el = serie.Calculate(serie.Name, Calculation.Sum);
						el.SmartLabel.Text += "<%PercentOfTotal," + chart.NumericFormat + "> (" + "<%Value," + chart.NumericFormat + "> " + (serie.DefaultElement.CustomAttributes["Unit"] ?? "") + ")";
						if (JSEnabled == true)
							el.SmartLabel.Text = "%PercentOfTotal (%Value" + (serie.DefaultElement.CustomAttributes["Unit"] ?? "") + ")"; ;
						el.ToolTip = serie.Name + imagemap_newline + el.SmartLabel.Text;
						if (chart.DisplayLegendVisible == false) {
							el.SmartLabel.Text = serie.Name + Environment.NewLine + el.SmartLabel.Text;
						}
						el.Color = serie.DefaultElement.Color;
						if (JSEnabled == false) {
							el.URL = serie.Elements[0].URL;
						}
						else {
							if (string.IsNullOrEmpty(serie.Elements[0].URL) == false)
								el.URL = serie.Elements[0].URL.Replace("'", "\"");
						}
						el.LegendEntry.SortOrder = serie.LegendEntry.SortOrder;
						el.LegendEntry.Hotspot = serie.LegendEntry.Hotspot;
						var unit = serie.LegendEntry.CustomAttributes["Unit"];
						el.LegendEntry.Value = serie.LegendEntry.Value.Replace("%Unit", unit != null ? unit.ToString() : "");// = string.Format("<{0},{1}> %Unit", DataSerieLegendEntry_Convert(serie.DisplayLegendEntry), numericformat);

						newSeries.Elements.Add(el);
					}
				});
				sc.Clear();
				sc.Add(newSeries);
			}
		}

		/// <summary>
		/// Extension method to transform DataSerieLegendEntry to dotnetcharting Legend entry
		/// </summary>
		/// <returns>DotNetChartType</returns>
		private static string DataSerieLegendEntry_Convert(DataSerieLegendEntry op) {
			switch (op) {
				case DataSerieLegendEntry.AVG:
					return "%Average";
				case DataSerieLegendEntry.MAX:
					return "%High";
				case DataSerieLegendEntry.MIN:
					return "%Low";
				case DataSerieLegendEntry.SUM:
					return "%Sum";
				case DataSerieLegendEntry.NONE:
				default:
					return "";
			}
		}

		#endregion
		#region EventLog
		/// <summary>
		/// Extension method to transform a EventLog to a dotnetcharting Series.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="eventLogs">The EventLog List.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="timeZone">The time zone.</param>
		/// <returns></returns>
		private static List<Series> EventLog_Convert(DotNetChart chartComp, List<EventLog> eventLogs, DateTime? startDate, DateTime? endDate, TimeZoneInfo timeZone) {

			var eventLogAxis = new Axis();
			Axis_Hide(eventLogAxis);
			eventLogAxis.Minimum = 1;
			eventLogAxis.Maximum = 2;
			chartComp.AxisCollection.Add(eventLogAxis);

			var list = new List<Series>();
			foreach (var eventLog in eventLogs) {
				var color = Color.FromArgb(eventLog.ClassificationItemColorR, eventLog.ClassificationItemColorG, eventLog.ClassificationItemColorB);
				//we have to create a different serie for each eventlog, now that we display them as AreaLine with Startdate, Enddate
				var s = new Series(eventLog.ClassificationItemTitle) {
					Type = SeriesType.AreaLine,
					DefaultElement = { Color = color, Transparency = 40 },
					LegendEntry = { Visible = false, Marker = { Color = color }, Value = "<%ElementCount,N0>" },
					YAxis = eventLogAxis
				};

				var eventlogStartDate = eventLog.StartDate;
				var eventlogEndDate = eventLog.EndDate;

				if (startDate.HasValue && eventlogStartDate < startDate.Value)
					eventlogStartDate = startDate.Value;

				if (endDate.HasValue && eventlogEndDate > endDate.Value)
					eventlogEndDate = endDate.Value;

				s.DefaultElement.ToolTip = string.Format("%SeriesName {0} %XValue", GetImageMapNewLine(chartComp));
				s.Elements.Add(new DotNetElement {
					XDateTime = eventlogStartDate.ConvertTimeFromUtc(timeZone),
					YValue = 1.1,
					Annotation =
						new Annotation(eventLog.Name) {
							Background = { Color = s.LegendEntry.Marker.Color },
							Padding = 5,
							DefaultCorner = BoxCorner.Round,
							DynamicSize = false,
							Orientation = dotnetCHARTING.Orientation.TopLeft,
							ToolTip = eventLog.Description ?? ""
						}
				});
				//if the event has a duration we display a second point.
				if (eventlogEndDate > eventlogStartDate) {
					s.Elements.Add(new DotNetElement { XDateTime = eventlogEndDate.ConvertTimeFromUtc(timeZone).AddSeconds(-1), YValue = 1.1 });
				}
				list.Add(s);

			}
			return list;
		}

		/// <summary>
		/// Generes the event log series.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="chart">The chart.</param>
		private static void EventLog_Generate(DotNetChart chartComp, VizChart chart) {
			if (chart.EventLogs != null && chart.EventLogs.Count > 0) {
				chartComp.SeriesCollection.Add(EventLog_Convert(chartComp, chart.EventLogs, chart.StartDate, chart.EndDate, chart.GetTimeZone()));
			}
		}
		#endregion
		#region Fonts
		/// <summary>
		/// Set the Chart text font
		/// </summary>
		/// <param name="chartComp">Chart component</param>
		/// <param name="fontFamily">the font family</param>
		/// <param name="fontSize">the font size</param>
		/// <param name="fontColor">Color of the font.</param>
		private static void Fonts_Init(DotNetChart chartComp, string fontFamily, int fontSize, string fontColor) {
			Font f = CreateSafeFont(fontFamily, fontSize);
			Font fBold = CreateSafeFont(fontFamily, fontSize);

			Fonts_Init(chartComp.DefaultAxis, fontFamily, fontSize, fontColor);
			chartComp.DefaultChartArea.Label.Font = f;
			chartComp.DefaultElement.SmartLabel.Font = fBold;
			chartComp.DefaultElement.SmartLabel.Color = Color.Black;

			if (!string.IsNullOrEmpty(fontColor)) {
				var color = ColorHelper.GetColor(fontColor);
				chartComp.DefaultChartArea.Label.Color = color;
				chartComp.DefaultElement.SmartLabel.Color = color;
			}

		}

		/// <summary>
		/// Set the Chart text font
		/// </summary>
		/// <param name="chartArea">The chart area.</param>
		/// <param name="fontFamily">the font family</param>
		/// <param name="fontSize">the font size</param>
		/// <param name="fontColor">Color of the font.</param>
		private static void Fonts_Init(ChartArea chartArea, string fontFamily, int fontSize, string fontColor) {
			Font f = CreateSafeFont(fontFamily, fontSize);
			Font fBold = CreateSafeFont(fontFamily, fontSize);
			Fonts_Init(chartArea.XAxis, fontFamily, fontSize, fontColor);
			Fonts_Init(chartArea.YAxis, fontFamily, fontSize, fontColor);

			chartArea.Label.Font = f;
			chartArea.DefaultElement.SmartLabel.Font = fBold;
			chartArea.DefaultElement.SmartLabel.Color = Color.Black;

			if (!string.IsNullOrEmpty(fontColor)) {
				var color = ColorHelper.GetColor(fontColor);
				chartArea.Label.Color = color;
				chartArea.DefaultElement.SmartLabel.Color = color;
			}

		}

		/// <summary>
		/// Creates the safe font.
		/// </summary>
		/// <param name="fontFamily">The font family.</param>
		/// <param name="fontSize">Size of the font.</param>
		/// <param name="fontStyle">The font style.</param>
		/// <returns></returns>
		private static Font CreateSafeFont(string fontFamily, int fontSize, FontStyle fontStyle = FontStyle.Regular) {
			Font f = new Font("Verdana", fontSize, fontStyle, GraphicsUnit.Pixel);
			try {
				f = new Font(fontFamily, fontSize, fontStyle, GraphicsUnit.Pixel);
			}
			catch (SystemException) {
				try {
					f = new Font(fontFamily, fontSize);
				}
				catch (SystemException) {
				}
			}
			return f;
		}

		/// <summary>
		/// Creates the safe font.
		/// </summary>
		/// <param name="fontFamily">The font family.</param>
		/// <param name="fontSize">Size of the font.</param>
		/// <param name="fontStyle">The font style.</param>
		/// <returns></returns>
		private static Font CreateSafeFont(FontFamily fontFamily, int fontSize, FontStyle fontStyle = FontStyle.Regular) {
			Font f = new Font("Verdana", fontSize, fontStyle, GraphicsUnit.Pixel);
			try {
				f = new Font(fontFamily, fontSize, fontStyle, GraphicsUnit.Pixel);
			}
			catch (SystemException) {
				try {
					f = new Font(fontFamily, fontSize);
				}
				catch (SystemException) {
				}
			}
			return f;
		}

		/// <summary>
		/// Set the Chart text font
		/// </summary>
		/// <param name="Axis">The axis.</param>
		/// <param name="fontFamily">the font family</param>
		/// <param name="fontSize">the font size</param>
		/// <param name="fontColor">Color of the font.</param>
		private static void Fonts_Init(Axis Axis, string fontFamily, int fontSize, string fontColor) {
			Font f = CreateSafeFont(fontFamily, fontSize);
			if (Axis != null) {
				Axis.Label.Font = f;
				Axis.DefaultTick.Label.Font = f;
			}


			if (!string.IsNullOrEmpty(fontColor)) {
				var color = ColorHelper.GetColor(fontColor);
				if (Axis != null) {
					Axis.Label.Color = color;
					Axis.DefaultTick.Label.Color = color;
				}
			}
		}
		#endregion
		#region Pies, Gauges
		/// <summary>
		/// Update the Chart Pies.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="JSEnabled">if set to <c>true</c> [JS enabled].</param>
		private static void Pies_Update(DotNetChart chartComp, VizChart chart, bool JSEnabled) {
			if (chartComp.Type == DotNetChartType.Pie || chartComp.Type == DotNetChartType.Donut) {
				chartComp.Type = (chartComp.Type == DotNetChartType.Pie) ? DotNetChartType.Pies : DotNetChartType.Donuts;
				ProjectSeriesCollectionToUniqueSerie(chartComp, chartComp.SeriesCollection, chart, JSEnabled);
				if (chartComp.ExtraChartAreas != null && chartComp.ExtraChartAreas.Count > 0) {
					foreach (ChartArea area in chartComp.ExtraChartAreas) {
						ProjectSeriesCollectionToUniqueSerie(chartComp, area.SeriesCollection, chart, JSEnabled);
					}
				}
				if (JSEnabled)
					chartComp.SeriesCollection[0].GaugeBorderBox.Position = new Size(50, 50);
			}
		}

		/// <summary>
		/// Updates the gauges to display the correct information (to be called just before rendering).
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="chart">The chart.</param>
		private static void Gauges_Update(DotNetChart chartComp, VizChart chart) {
			if (chartComp.Type == DotNetChartType.Gauges) {
				foreach (Series s in chartComp.SeriesCollection) {
					foreach (Element e in s.Elements) {
						if (DateTime.MaxValue > e.XDateTime && DateTime.MinValue < e.XDateTime && string.IsNullOrWhiteSpace(e.Name)) {
							e.Name = e.XDateTime.ToString(chart.TimeIntervalFormat);
						}
					}
				}
			}

			if (chartComp.Type == DotNetChartType.Gauges && chart.GaugeType == VizGaugeType.IndicatorLight) {
				List<DateTime> dates = new List<DateTime>();
				foreach (Series s in chartComp.SeriesCollection) {
					foreach (Element e in s.Elements) {
						if (DateTime.MaxValue > e.XDateTime && DateTime.MinValue < e.XDateTime) {
							if (dates.Contains(e.XDateTime) == false) {
								dates.Add(e.XDateTime);
							}
						}
					}
				}
				foreach (Series s in chartComp.SeriesCollection) {
					List<DateTime> s_dates = new List<DateTime>();
					foreach (Element e in s.Elements) {
						if (DateTime.MaxValue > e.XDateTime && DateTime.MinValue < e.XDateTime) {
							if (s_dates.Contains(e.XDateTime) == false) {
								s_dates.Add(e.XDateTime);
							}
						}
					}

					IEnumerable<DateTime> missing = dates.Except(s_dates);
					foreach (var missing_date in missing) {
						var ne = new Element();
						ne.XDateTime = missing_date;
						ne.Name = ne.XDateTime.ToString(chart.TimeIntervalFormat);
						ne.YValue = double.NaN;
						s.Elements.Add(ne);
					}
					s.Sort(ElementValue.XDateTime, "ASC");
				}
			}
		}

		/// <summary>
		/// Extension method to transform ChartType to dotnetcharting chart type
		/// </summary>
		/// <param name="type">VizChartType</param>
		/// <returns>DotNetChartType</returns>
		private static DotNetGaugeType GaugeType_Convert(VizGaugeType type) {
			switch (type) {
				case VizGaugeType.Circular:
					return DotNetGaugeType.Circular;
				case VizGaugeType.Digital:
					return DotNetGaugeType.DigitalReadout;
				case VizGaugeType.Horizontal:
					return DotNetGaugeType.Horizontal;
				case VizGaugeType.IndicatorLight:
					return DotNetGaugeType.IndicatorLight;
				case VizGaugeType.Thermometer:
					return DotNetGaugeType.Vertical;
				case VizGaugeType.Vertical:
					return DotNetGaugeType.Vertical;
			}
			return DotNetGaugeType.Circular;
		}
		#endregion
		#region ImageMap
		/// <summary>
		/// Return the Imagemap new line string.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <returns></returns>
		private static string GetImageMapNewLine(DotNetChart chartComp) {
			if (chartComp.JS.Enabled)
				return "<br/>";
			else
				return imagemap_newline;
		}
		/// <summary>
		/// Get the Image Map with correct attribut for extjs qtip
		/// </summary>
		/// <param name="chartComp">Chart component</param>
		public static string ImageMap_GetForExtJS(DotNetChart chartComp) {
			var map = chartComp.ImageMapText;
			if (!string.IsNullOrEmpty(map)) {
				map = map.Replace(GetImageMapNewLine(chartComp), "<br/>").Replace(" alt=", " aalt=").Replace(" title=", " ext:qtip=").Replace("%Value", "0");
				var style = " style=\"cursor:pointer;\" ";
				map = map.Replace("href=\"return Viz.grid.Chart.onDataSerieLegendEntryClick(", style + "onclick=\"return Viz.grid.Chart.onDataSerieLegendEntryClick(");
				map = map.Replace("href=\"return Viz.grid.Chart.onCalendarViewAreaClick(", style + "onclick=\"return Viz.grid.Chart.onCalendarViewAreaClick(");
				map = map.Replace("href=\"return Viz.grid.Chart.onCalendarViewSelectionDelete(", style + "onclick=\"return Viz.grid.Chart.onCalendarViewSelectionDelete(");
				map = map.Replace("href=\"return Viz.grid.Chart.onDataPointClick(", style + "onclick=\"return Viz.grid.Chart.onDataPointClick(");
			}
			return map;
		}
		#endregion
		#region Historical Analysis
		/// <summary>
		/// Generates the historical analysis.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="padding">The padding.</param>
		private static void HistoricalAnalysis_Generate(DotNetChart chartComp, VizChart chart, Padding padding) {
			if (HistoricalAnalysis_Enabled(chart)) {
				var useTimeScale = HistoricalAnalysis_UseTimeScale(chart);

				#region Axis Init
				Axis XAxis = null;
				Axis YAxis = null;
				bool axisInverted = false;
				GetAxis(chartComp, out XAxis, out YAxis, out axisInverted);
				#endregion
				#region Main Legend section
				HistoricalAnalysis_AddMissingLegendEntry(chartComp, chart.Series.ToList(), 0);

				//YAxis.Scale = Scale.Range;
				if (!useTimeScale) {
					XAxis.Scale = Scale.Normal;
				}
				else {
					if (!axisInverted)
						Axis_SetMinMax(XAxis, chart.TimeInterval, chart.StartDate, chart.EndDate, chart.GetTimeZone(), chart.HistoricalAnalysis.Any(kvp => kvp.Key.SetXAxisMin), chart.HistoricalAnalysis.Any(kvp => kvp.Key.SetXAxisMax));
				}
				chartComp.ChartArea.GridPosition = new Point(0, 0);
				chartComp.ChartAreaLayout.Mode = ChartAreaLayoutMode.Custom;
				#endregion
				#region First  Column Entry Header
				var head = new LegendEntry(chart.EndDate.ConvertTimeFromUtc(chart.GetTimeZone()).ToString(chart.TimeInterval.GetFormat()), chart.StartDate.ConvertTimeFromUtc(chart.GetTimeZone()).ToString(chart.TimeInterval.GetFormat()), "");
				head.SortOrder = -1;
				head.HeaderMode = LegendEntryHeaderMode.StartNewColumn;
				head.DividerLine.Color = Color.Transparent;
				chartComp.LegendBox.ExtraEntries.Add(head);
				#endregion
				#region For Each HistoricalAnalysis
				var analysisCount = 1;
				foreach (var analysis in chart.HistoricalAnalysis.Keys) {
					var sortOrder = 2 * analysisCount + 1;
					#region New Column Entry Header

					head = new LegendEntry(
							analysis.EndDate.Value.ConvertTimeFromUtc(chart.GetTimeZone()).ToString(chart.TimeInterval.GetFormat()),
							analysis.StartDate.Value.ConvertTimeFromUtc(chart.GetTimeZone()).ToString(chart.TimeInterval.GetFormat()), "") {
								SortOrder = 2 * analysisCount,
								DividerLine = { Color = Color.Transparent },
								HeaderMode = LegendEntryHeaderMode.StartNewColumn
							};
					chartComp.LegendBox.ExtraEntries.Add(head);
					#endregion
					DataSerieCollection col = chart.HistoricalAnalysis[analysis];
					if (col.HasPointsWithValue) {
						#region Extra XAxis
						var extraXAxis = Axis_Clone(XAxis);
						var extraYAxis = Axis_Clone(YAxis);
						YAxis.SynchronizeScale.Add(extraYAxis);
						if (!useTimeScale) {
							extraXAxis.Scale = Scale.Normal;
						}
						else {
							if (!axisInverted)
								Axis_SetMinMax(extraXAxis, chart.TimeInterval, analysis.StartDate.Value, analysis.EndDate.Value, chart.GetTimeZone(), analysis.SetXAxisMin, analysis.SetXAxisMax);
						}
						if (!analysis.XAxisVisible)
							Axis_Hide(extraXAxis);
						#endregion
						#region Extra Series
						List<Series> extraSeries = new List<Series>();
						foreach (var originalSerie in col.ToList()) {
							if (originalSerie.LineWidth.HasValue == false) {
								originalSerie.LineWidth = 3;
								originalSerie.LineDashStyle = LineDashStyle.Dash;
								originalSerie.DisplayTransparency = 70;
							}
							var sc = DataSeries_Convert(new List<DataSerie> { originalSerie }, chartComp, chart).ToList(); ;
							if (sc != null && sc.Count > 0) {
								var s = sc[0];
								//List<Series> extraSeries = DataSeries_Convert(col.ToList(), chartComp, chart).ToList();
								//extraSeries.ForEach(s => {
								if (!axisInverted) {
									s.XAxis = extraXAxis;
									//we do this because of a Bug when not using a X Axis time interval and sharing the same Y Axis than the original series in stack mode
									if (string.IsNullOrEmpty(originalSerie.KeyYAxis))
										s.YAxis = extraYAxis;
								}
								else {
									s.YAxis = extraXAxis;
									if (string.IsNullOrEmpty(originalSerie.KeyYAxis))
										s.XAxis = extraYAxis;
								}

								s.LegendEntry.SortOrder = sortOrder;
								if (analysis.DataSerieType != DataSerieType.None)
									s.Type = DataSerieType_Convert(analysis.DataSerieType);
								//});
								extraSeries.Add(s);
							}
						}

						#endregion
						#region Extra Chart Area
						if (analysis.ExtraChartArea) {
							ChartArea extraArea = new ChartArea();

							if (chart.DisplayStartEndDate) {
								extraArea.Label.Text = ChartAreaLabel_GetFormat(analysis.StartDate.Value, analysis.EndDate.Value, chart.GetTimeZone());
							}
							extraArea.Label.Font = chartComp.ChartArea.Label.Font;
							extraArea.Background = chartComp.ChartArea.Background;
							extraArea.SeriesCollection.Add(extraSeries);

							if (!axisInverted) {
								extraArea.XAxis = extraXAxis;
								//extraArea.YAxis = extraYAxis;
								//extraArea.YAxis.SynchronizeScale.Add(chartComp.YAxis);
							}
							else {
								extraArea.YAxis = extraXAxis;
								//extraArea.XAxis = extraYAxis;
								//extraArea.XAxis.SynchronizeScale.Add(chartComp.XAxis);
							}
							extraArea.GridPosition = new Point(0, analysisCount);
							chartComp.ExtraChartAreas.Add(extraArea);
						}
						#endregion
						#region Extra Axis Style
						else {
							if (analysis.XAxisVisible)
								Axis_SetColor(extraXAxis, Color.Red);
							//weird because of bug 2680
							if (!useTimeScale) {
								extraXAxis.InvertScale = false;
							}
							extraXAxis.DefaultTick.GridLine.Color = Color.Empty;
							extraXAxis.AlternateGridBackground.Color = Color.Empty;
							extraXAxis.Position = 10;
							extraSeries.ForEach(s => {
								//s.Line.DashStyle = DashStyle.Dash;
								s.Elements.ToList().ForEach(e => {
									//e.Transparency =  90;
									e.Outline.DashStyle = s.Line.DashStyle;// DashStyle.Dash;
									//e.Outline.Width = s.Line.Width;
								});
							});
						#endregion
							chartComp.SeriesCollection.Add(extraSeries);
							Axis_Hide(extraYAxis);
						}
						analysisCount += 1;
					}
					#region Missing Legend Entry
					HistoricalAnalysis_AddMissingLegendEntry(chartComp, col.ToList(), sortOrder);
					#endregion
				}
				#endregion
				#region Legend
				if (chart.DisplayLegendVisible) {
					if (chart.LegendPosition == ChartLegendPosition.ChartTitle) {
						chartComp.TitleBox.Position = TitleBoxPosition.Left;

						var legend = chartComp.GetLegendBitmap();
						if (legend != null) {
							chartComp.LegendBox.Position = new Point(padding.Left + 10, padding.Top + 10);
							chartComp.MarginTop += 20 + chartComp.GetLegendBitmap().Height;
						}
					}
				}
				#endregion
				if (!useTimeScale) {
					var xaxisformat = String.IsNullOrEmpty(chart.XAxisFormat) ? chart.TimeInterval.GetFormat() : chart.XAxisFormat;
					ConvertToNormalScale(chartComp, xaxisformat);
				}
			}
		}

		private static bool HistoricalAnalysis_Enabled(VizChart chart) {
			return (chart.HistoricalAnalysis != null && chart.HistoricalAnalysis.Count > 0 && chart.HistoricalAnalysis.Any(kvp => kvp.Key.Enabled) && chart.SpecificAnalysis != ChartSpecificAnalysis.Correlation);
		}

		private static bool HistoricalAnalysis_UseTimeScale(VizChart chart) {
			bool useTimeScale = chart.TimeInterval == AxisTimeInterval.None;
			if (chart.ChartType == VizChartType.Radar) {
				useTimeScale = true;
			}
			if (chart.ChartType == VizChartType.Gauge) {
				useTimeScale = true;
			}
			if (chart.SpecificAnalysis == ChartSpecificAnalysis.Correlation) {
				useTimeScale = true;
			}
			return useTimeScale;
		}

		private static void ConvertToNormalScale(DotNetChart chartComp, string timeFormat) {
			foreach (var s in chartComp.SeriesCollection.ToList()) {
				foreach (var p in s.Elements.ToList()) {
					if (String.IsNullOrWhiteSpace(p.Name)) {
						p.Name = p.XDateTime.ToString(timeFormat);
					}
					p.XDateTime = DateTime.MinValue;
					if (s.DefaultElement != null && s.DefaultElement.ToolTip != null)
						p.ToolTip = s.DefaultElement.ToolTip.Replace("%XValue", p.Name);
				}
			}
			chartComp.YAxis = new Axis();
		}

		private static void FillMissingValuesWithNulls(VizChart chart) {
			if (chart.HistoricalAnalysis != null && chart.HistoricalAnalysis.Count > 0) {
				var timeZone = chart.GetTimeZone();
				foreach (var s in chart.Series.ToList()) {
					var startDate = chart.StartDate.ConvertTimeFromUtc(timeZone).UpdateByInterval(chart.TimeInterval);
					var endDate = chart.EndDate.ConvertTimeFromUtc(timeZone).UpdateByInterval(chart.TimeInterval);
					if (!chart.DynamicTimeScaleCalendarEndToNow)
						endDate = endDate.AddSeconds(-1);

					DataSerieHelper.FillMissingValues(s, chart.TimeInterval, startDate, endDate, false);
				}
				foreach (var kvp in chart.HistoricalAnalysis) {
					foreach (var s in kvp.Value.ToList()) {
						var startDate = kvp.Key.StartDate.Value.ConvertTimeFromUtc(timeZone).UpdateByInterval(chart.TimeInterval);
						var endDate = kvp.Key.EndDate.Value.ConvertTimeFromUtc(timeZone).UpdateByInterval(chart.TimeInterval);
						if (!chart.DynamicTimeScaleCalendarEndToNow)
							endDate = endDate.AddSeconds(-1);

						DataSerieHelper.FillMissingValues(s, chart.TimeInterval, startDate, endDate, false);
					}
				}
			}
		}
		/// <summary>
		/// Add missing legend entry for historical analysis.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="series">The series.</param>
		/// <param name="SortOrder">The sort order.</param>
		private static void HistoricalAnalysis_AddMissingLegendEntry(DotNetChart chartComp, List<DataSerie> series, int SortOrder) {
			foreach (var s in series) {
				//if the series has no points it hasnt been added in the legend entry but not if it s hidden
				if (!s.Hidden && !s.HasPointsWithValue) {
					chartComp.LegendBox.ExtraEntries.Add(new LegendEntry(s.Name, "") {
						SortOrder = SortOrder
					});
				}
			}
		}
		#endregion
		#region Init JS, LegendBox, TitleBox, Legend, Licence, Style, Tooltip
		/// <summary>
		/// Init the HighChart option of the Chart.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="JSEnabled">if set to <c>true</c> HighChart export is enabled.</param>
		private static void JS_Init(DotNetChart chartComp, bool JSEnabled) {
			chartComp.JS.Enabled = JSEnabled;
			chartComp.JS.Buttons.EnableExportButton = false;
			chartComp.JS.Buttons.EnablePrintButton = false;
			chartComp.JS.AxisToZoom = "x";
			chartComp.MarginBottom = chartComp.MarginTop = chartComp.MarginLeft = chartComp.MarginRight = 0;
		}

		/// <summary>
		/// Set the Chart's legend style and position
		/// </summary>
		/// <param name="chartComp">Chart component</param>
		/// <param name="chart">The chart.</param>
		private static void Legend_Init(DotNetChart chartComp, VizChart chart) {
			Font f = CreateSafeFont(chart.DisplayFontFamily, chart.DisplayLegendFontSize);
			chartComp.DefaultSeries.LegendEntry.LabelStyle.Font = f;
			chartComp.DefaultSeries.LegendEntry.LabelStyle.Truncation.Length = chart.DisplayLabelTruncationLength ?? 30;
			chartComp.DefaultSeries.LegendEntry.LabelStyle.Truncation.Mode = TruncationMode.End;

			TitleBox_Init(chartComp.TitleBox, chart.DisplayLegendVisible, f, chart.LegendPosition, chart.DisplayFontColor, chart.DisplayLegendPlain);
			LegendBox_Init(chartComp.LegendBox, chart.DisplayLegendVisible, f, chart.LegendPosition, chart.DisplayFontColor, chart.DisplayLegendPlain);
		}

		/// <summary>
		/// Set the Chart's legend style and position
		/// </summary>
		/// <param name="area">The area.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="fontSize">Size of the font.</param>
		private static void Legend_Init(ChartArea area, VizChart chart, int fontSize) {
			Font f = CreateSafeFont(chart.DisplayFontFamily, fontSize);
			area.DefaultSeries.LegendEntry.LabelStyle.Font = f;
			area.DefaultSeries.LegendEntry.LabelStyle.Truncation.Length = chart.DisplayLabelTruncationLength ?? 30;
			area.DefaultSeries.LegendEntry.LabelStyle.Truncation.Mode = TruncationMode.End;

			TitleBox_Init(area.TitleBox, chart.DisplayLegendVisible, f, chart.LegendPosition, chart.DisplayFontColor, chart.DisplayLegendPlain);
			LegendBox_Init(area.LegendBox, chart.DisplayLegendVisible, f, chart.LegendPosition, chart.DisplayFontColor, chart.DisplayLegendPlain);
		}

		/// <summary>
		/// Titles the box_ init.
		/// </summary>
		/// <param name="titleBox">The title box.</param>
		/// <param name="displayLegend">if set to <c>true</c> [display legend].</param>
		/// <param name="f">The f.</param>
		/// <param name="position">The position.</param>
		/// <param name="fontColor">Color of the font.</param>
		/// <param name="usePlainLegend">if set to <c>true</c> [use plain legend].</param>
		private static void TitleBox_Init(Box titleBox, bool displayLegend, Font f, ChartLegendPosition position, string fontColor, bool usePlainLegend) {
			switch (position) {
				case ChartLegendPosition.ChartTitle:
					titleBox.Position = TitleBoxPosition.FullWithLegend;
					break;
				default:
					titleBox.Position = TitleBoxPosition.None;
					break;
			};
			titleBox.Label.Alignment = StringAlignment.Far;
			titleBox.Visible = displayLegend;
			if (!usePlainLegend) {
				titleBox.Background = new Background(Color.White, Color.LightGray, 90);
				titleBox.CornerTopLeft = BoxCorner.Round;
				titleBox.CornerTopRight = BoxCorner.Round;
			}
			else {
				titleBox.Background = new Background(Color.Transparent);
				titleBox.CornerTopLeft = BoxCorner.Square;
				titleBox.CornerTopRight = BoxCorner.Square;
			}

			if (!displayLegend) {
				titleBox.Position = TitleBoxPosition.None;
			}
			if (f != null) {
				titleBox.Label.Font = f;
			}
		}

		/// <summary>
		/// Legends the box_ init.
		/// </summary>
		/// <param name="legendBox">The legend box.</param>
		/// <param name="displayLegend">if set to <c>true</c> [display legend].</param>
		/// <param name="f">The f.</param>
		/// <param name="position">The position.</param>
		/// <param name="fontColor">Color of the font.</param>
		/// <param name="usePlainLegend">if set to <c>true</c> [use plain legend].</param>
		private static void LegendBox_Init(LegendBox legendBox, bool displayLegend, Font f, ChartLegendPosition position, string fontColor, bool usePlainLegend) {
			legendBox.LabelStyle.Alignment = StringAlignment.Far;
			if (!usePlainLegend) {
				legendBox.Background = new Background(Color.White, Color.LightGray, 90);
			}
			else {
				legendBox.ClearColors();
				legendBox.Background = new Background(Color.Transparent);
				legendBox.DefaultEntry.SeriesType = SeriesType.Bar;
				legendBox.DefaultEntry.Marker = new ElementMarker(ElementMarkerType.Square, 100);
			}
			legendBox.DefaultCorner = BoxCorner.Square;
			legendBox.Visible = displayLegend;
			legendBox.Orientation = dotnetCHARTING.Orientation.Bottom;

			if (f != null) {
				legendBox.DefaultEntry.LabelStyle.Font = f;
				if (usePlainLegend && !string.IsNullOrEmpty(fontColor)) {
					var color = ColorHelper.GetColor(fontColor);
					legendBox.DefaultEntry.LabelStyle.Color = color;
				}
			}
			switch (position) {
				case ChartLegendPosition.Bottom:
					legendBox.Position = LegendBoxPosition.Bottom;
					break;
				case ChartLegendPosition.BottomMiddle:
					legendBox.Position = LegendBoxPosition.BottomMiddle;
					break;
				case ChartLegendPosition.ChartArea:
					legendBox.Position = LegendBoxPosition.ChartArea;

					break;
				case ChartLegendPosition.ChartTitle:
					break;
				case ChartLegendPosition.Middle:
					legendBox.Position = LegendBoxPosition.Middle;
					break;
				case ChartLegendPosition.Top:
					legendBox.Position = LegendBoxPosition.Top;
					break;
			}
		}

		/// <summary>
		/// Init the info icon.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="padding">The padding.</param>
		private static void InfoIcon_Init(DotNetChart chartComp, VizChart chart, Padding padding) {
			if (chart.InfoIconPosition != ChartInfoIconPosition.None && chart.SpecificAnalysis != ChartSpecificAnalysis.CalendarView) {
				var an = new Annotation();
				an.ClearColors();

				if (chart.LockFilterSpatial)
				{
					an.Background = new Background("~/images/locked.png");
				}
				else if (chart.DetailedStatusIconEnabled)
				{
					if (chart.Errors != null && chart.Errors.Count > 0) {
						an.Background = new Background("~/images/exception.png");
					}
					else if (chart.Warnings != null && chart.Warnings.Count > 0) {
						an.Background = new Background("~/images/warning.png");
					}
					else
					{
						an.Background = new Background("~/images/information_success.png");
					}
				}
				else
				{
					an.Background = new Background("~/images/information.png");
				}

				an.CornerBottomLeft = an.CornerBottomRight = an.CornerTopLeft = an.CornerTopRight = BoxCorner.Square;
				an.ToolTip = LegendBox_BuildMoreInfoTooltip(chart);

				switch (chart.InfoIconPosition) {
					case ChartInfoIconPosition.BottomLeft:
						an.Position = new Point(padding.Left + 2, (int)chartComp.Height.Value - 18 - padding.Bottom);
						break;

					case ChartInfoIconPosition.BottomRight:
						an.Position = new Point((int)chartComp.Width.Value - 18 - padding.Right, (int)chartComp.Height.Value - 18 - padding.Bottom);
						break;

					case ChartInfoIconPosition.TopLeft:
						an.Position = new Point(padding.Left + 2, padding.Top + 2);
						break;

					case ChartInfoIconPosition.TopRight:
						an.Position = new Point((int)chartComp.Width.Value - 18 - padding.Right, padding.Top + 2);
						break;
				}

				if (!string.IsNullOrEmpty(an.ToolTip))
					chartComp.Annotations.Add(an);
			}
		}

		/// <summary>
		/// Build thev"more info" legend entry tooltip.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		private static string LegendBox_BuildMoreInfoTooltip(VizChart chart) {
			var sb = new StringBuilder();
			int count = 0;
			int maxCount = 10;

			sb.Append("<table>");

			string meterCountWarning = chart.Warnings.FirstOrDefault(IsMeterCountWarning);
			if (!string.IsNullOrWhiteSpace(meterCountWarning)) {
				sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1}</td></tr>", "viz-icon-small-warning", meterCountWarning));
			}
			else {
				sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1} {2}</td></tr>", "viz-icon-small-meter", chart.MetersCount, Helper.LocalizeText("msg_meters")));
			}
			sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1}</td></tr>", "viz-icon-small-calendar", ChartHelper.GenerateDateRangeLabel(chart.StartDate, chart.EndDate, chart.GetTimeZone())));

			if (chart.HistoricalAnalysis != null && chart.HistoricalAnalysis.Count > 0) {
				chart.HistoricalAnalysis.Keys.ToList().ForEach(analysis => {
					sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1} : {2}</td></tr>", "viz-icon-small-chart-historicalanalysis", ChartHelper.GenerateDateRangeLabel(analysis.StartDate.Value, analysis.EndDate.Value, chart.GetTimeZone()), analysis.Name.HtmlEncode()));
				});
			}

			if (chart.FilterSpatial != null && chart.FilterSpatial.Count > 0) {
				chart.FilterSpatial.OrderBy(loc => loc.LongPath).ToList().ForEach(loc => {
					if (count < maxCount) {
						sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1}</td></tr>", loc.IconCls, loc.LongPath.HtmlEncode()));
					}
					else if (count == maxCount) {
						sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1}</td></tr>", "", "..."));
					}
					count++;
				});
			}
			count = 0;
			if (chart.MetersClassifications != null && chart.MetersClassifications.Count > 0) {
				chart.MetersClassifications.OrderBy(classif => classif.LongPath).ToList().ForEach(classif => {
					if (count < maxCount) {
						sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1}</td></tr>", "viz-icon-small-classificationitem", classif.LongPath.HtmlEncode()));
					}
					else if (count == maxCount) {
						sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1}</td></tr>", "", "..."));
					}
					count++;
				});
			}

			if (string.IsNullOrEmpty(chart.Description) == false) {
				sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1}</td></tr>", "viz-icon-small-chart", chart.Description.HtmlEncode()));
			}

			count = 0;
			if (chart.Errors != null && chart.Errors.Count > 0) {
				chart.Errors.ForEach(error => {
					if (count < maxCount) {
						sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1}</td></tr>", "viz-icon-small-exception", error.HtmlEncode()));
					}
					else if (count == maxCount) {
						sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1}</td></tr>", "", "..."));
					} 
					count++;
				});
			}

			count = 0;
			if (chart.Warnings != null && chart.Warnings.Count > 0) {
				chart.Warnings.Where(x => x!= meterCountWarning).ForEach(warning => {
					if (count < maxCount) {
						sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1}</td></tr>", "viz-icon-small-warning", warning.HtmlEncode()));
					}
					else if (count == maxCount) {
						sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>{1}</td></tr>", "", "..."));
					}
					count++;
				});
			}

			if (chart.Series != null && chart.Series.Count > 0) {
				foreach (var serie in chart.Series.GetVisibleDataSeries()) {
					if (serie.Points.Count > chart.MaxDataPointPerDataSerie) {
						sb.Append(string.Format("<tr><td width='20px'><div class='x-icon-combo-icon {0}' /></td><td>'{1}' {2}</td></tr>", "viz-icon-small-dataserie", serie.Name, Helper.LocalizeText("msg_chart_maxdatapoints_exceeded")));
					}
				}
			}
			sb.Append("</table>");
			return sb.ToString();
		}

		/// <summary>
		/// Determines if a given waring string is a waring about meters Count
		/// </summary>
		/// <param name="warning">The warning.</param>
		private static bool IsMeterCountWarning(string warning) {
			string pattern = @"\d* / \d* " + Helper.LocalizeText("msg_meters").Replace("(", @"\(").Replace(")", @"\)");
			RegexOptions regexOptions = RegexOptions.Compiled;
			Regex regex = new Regex(pattern, regexOptions);
			return regex.IsMatch(warning);
		}

		/// <summary>
		/// Init the chart component with correct dimensions, licence file, culture
		/// </summary>
		/// <param name="chartComp">Chart component</param>
		/// <param name="width">width</param>
		/// <param name="height">height</param>
		/// <param name="licencePath">licence file</param>
		/// <param name="currentUICulture">Current UI culture as 2 letter ISO</param>
		private static void Licence_Init(DotNetChart chartComp, int width, int height, string licencePath, string currentUICulture) {
			chartComp.UseFile = false;
			chartComp.Width = new Unit(width);
			chartComp.Height = new Unit(height);
			chartComp.ImageFormat = ImageFormat.Png;
			chartComp.ApplicationDNC = licencePath;
			chartComp.Debug = true;
			chartComp.Mentor = false;
			chartComp.DefaultCultureName = currentUICulture;

		}

		/// <summary>
		/// Sets the Chart's display style
		/// </summary>
		/// <param name="chartComp">Chart component</param>
		/// <param name="shaddingEffect">Chart shadding effect</param>
		/// <param name="use3d">true to display chart in 3d, false otherwise</param>
		/// <param name="markerSize">Default marker size of series</param>
		/// <param name="lineThickness">Default line thickness of series (when displayed as lines)</param>
		/// <param name="transparency">Default transparency of series</param>
		/// <param name="displayLegend">true to indicate the legend is visible, false otherwise</param>
		/// <param name="displayValues">true to display values as text on the chart</param>
		/// <param name="numericFormat">numeric format of the values</param>
		/// <param name="usedynamicdisplay">if set to <c>true</c> [usedynamicdisplay].</param>
		private static void Style_Init(DotNetChart chartComp, ChartShaddingEffect shaddingEffect, bool use3d, int markerSize, int lineThickness, int transparency, bool displayLegend, bool displayValues, string numericFormat, bool usedynamicdisplay) {
			chartComp.ShadingEffectMode = ShaddingEffect_Convert(shaddingEffect);
			chartComp.Use3D = use3d;
			chartComp.DefaultSeries.DefaultElement.Marker.Size = markerSize;
			chartComp.DefaultSeries.Line.Width = lineThickness;
			chartComp.DonutHoleSize = 50;
			chartComp.DefaultSeries.DefaultElement.ExplodeSlice = false;
			chartComp.ExplodedSliceAmount = 3;
			chartComp.RadarLabelMode = RadarLabelMode.Normal;
			chartComp.Depth = new Unit(50, UnitType.Pixel);
			chartComp.DefaultSeries.DefaultElement.Transparency = transparency;
			if (displayValues) {
				chartComp.DefaultElement.ShowValue = true;
				chartComp.DefaultElement.SmartLabel.DynamicPosition = true;


				if (chartComp.Type == DotNetChartType.Donut || chartComp.Type == DotNetChartType.Donuts || chartComp.Type == DotNetChartType.Pie || chartComp.Type == DotNetChartType.Pies) {
					var smartLabel = "<%PercentOfTotal," + numericFormat + "> (" + "<%Sum," + numericFormat + "> %Unit)";
					if (chartComp.JS.Enabled)
						smartLabel = "%PercentOfTotal (%Sum)";

					if (!displayLegend)
						smartLabel = "%Name " + smartLabel;

					chartComp.DefaultElement.SmartLabel.Text = smartLabel;
				}
				else {
					var valueString = "<%YValue," + numericFormat + ">" + " %Unit";
					if (chartComp.JS.Enabled)
						valueString = "%YValue ";

					if (displayLegend || (chartComp.Type == DotNetChartType.Gauges))
						chartComp.DefaultElement.SmartLabel.Text += valueString;
					else
						chartComp.DefaultElement.SmartLabel.Text += "%SeriesName : " + valueString;
				}
			}
			if (chartComp.Type == DotNetChartType.Gauges && chartComp.DefaultSeries.GaugeType == DotNetGaugeType.Circular) {
				if (chartComp.ChartArea.Background != null)
					chartComp.DefaultSeries.Background = new Background(chartComp.ChartArea.Background.Color, chartComp.ChartArea.Background.SecondaryColor, 90);
				if (shaddingEffect != ChartShaddingEffect.None)
					chartComp.Use3D = true;
				chartComp.ClipGauges = false;
			}
			if (IsPieDonutGaugeRadarTreeMap(chartComp) && usedynamicdisplay) {
				if (chartComp.Type == DotNetChartType.Radar) {
					//transparent doesnt work and clear colors clears all the lines of the radar
					chartComp.ChartArea.Background.Color = Color.FromArgb(10, 255, 255, 255);
					chartComp.ChartArea.Line.Color = Color.Transparent;
				}
				else {
					chartComp.ChartArea.ClearColors();
				}
			}
		}

		/// <summary>
		/// Inits the default tooltip for Series, Elements and LegendEntry
		/// </summary>
		/// <param name="chartComp">Chart component</param>
		/// <param name="numericFormat">numeric format for the tooltip</param>
		private static void Tooltip_Init(DotNetChart chartComp, string numericFormat) {
			var valueString = string.Format("<%YValue,{0}> %Unit", numericFormat);
			if (chartComp.JS.Enabled)
				valueString = "%YValue ";

			if (chartComp.YAxis != null && chartComp.YAxis.Scale == Scale.FullStacked) {
				valueString += "(%PercentOfGroup)";
			}

			if (chartComp.Type == DotNetChartType.Donut || chartComp.Type == DotNetChartType.Pie) {
				chartComp.DefaultSeries.DefaultElement.ToolTip = string.Format("%SeriesName : {0}", valueString);
			}
			else {
				Axis xAxis;
				Axis yAxis;
				GetAxis(chartComp, out xAxis, out yAxis);
				if (xAxis.TimeInterval == TimeInterval.Quarters) {
					chartComp.DefaultSeries.DefaultElement.ToolTip = string.Format("%SeriesName {1} %Name : {0}", valueString, GetImageMapNewLine(chartComp));
				}
				else {
					chartComp.DefaultSeries.DefaultElement.ToolTip = string.Format("%SeriesName {1} %XValue : {0}", valueString, GetImageMapNewLine(chartComp));
				}

			}
			chartComp.DefaultSeries.LegendEntry.ToolTip = string.Format("%SeriesName {1} %ElementCount {6} {1} {3}: <%High,{0}> %Unit {1} {2}: <%Low,{0}> %Unit {1} {4}: <%Sum,{0}> %Unit {1} {5}: <%Average,{0}> %Unit", numericFormat, GetImageMapNewLine(chartComp), Helper.LocalizeText("msg_enum_mathematicoperator_min"), Helper.LocalizeText("msg_enum_mathematicoperator_max"), Helper.LocalizeText("msg_enum_mathematicoperator_sum"), Helper.LocalizeText("msg_enum_mathematicoperator_avg"), Helper.LocalizeText("msg_datapoints"));
		}

		/// <summary>
		/// Extension method to transform ChartShaddingEffect to dotnetcharting ShaddingEffectMode
		/// </summary>
		/// <returns>DotNetChartType</returns>
		private static ShadingEffectMode ShaddingEffect_Convert(ChartShaddingEffect type) {
			switch (type) {
				case ChartShaddingEffect.None:
					return ShadingEffectMode.None;
				case ChartShaddingEffect.One:
					return ShadingEffectMode.One;
				case ChartShaddingEffect.Two:
					return ShadingEffectMode.Two;
				case ChartShaddingEffect.Three:
					return ShadingEffectMode.Three;
				case ChartShaddingEffect.Four:
					return ShadingEffectMode.Four;
				case ChartShaddingEffect.Five:
					return ShadingEffectMode.Five;
				case ChartShaddingEffect.Six:
					return ShadingEffectMode.Six;
				case ChartShaddingEffect.Seven:
					return ShadingEffectMode.Seven;
				case ChartShaddingEffect.Eight:
					return ShadingEffectMode.Eight;
			}
			return ShadingEffectMode.None;
		}

		#endregion
		#region Padding
		/// <summary>
		/// Update the chart padding based on the ChartType due to dotnetcharting quirks.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="padding">The padding.</param>
		/// <returns></returns>
		private static Padding Padding_Update(DotNetChart chartComp, VizChart chart, Padding padding) {
			if (IsPieDonutGaugeRadarTreeMap(chartComp)) {
				padding.Bottom += 5;
				padding.Left += 3;
				//if the legend is visible and the chart is in dynamic display mode and the header has an image
				if (chartComp.TitleBox.Visible == true && chart != null && chart.UseDynamicDisplay && string.IsNullOrEmpty(chart.KeyDynamicDisplay) == false) {
					padding.Left += 25;
				}
			}
			else {
				// need to find out why, we were using -5
				padding.Top -= 0;//5;
			}
			if (chartComp.TitleBox.Visible == false) {
				padding.Top += 5;
			}
			if (chartComp.Use3D) {
				padding.Top += 3;
			}
			return padding;
		}
		#endregion
		#region Specific Analysis
		/// <summary>
		/// Applies the specific analysis.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		private static void SpecificAnalysis_Apply(DotNetChart chartComp, VizChart chart, int width, int height) {
			#region Axis

			Axis XAxis;
			Axis YAxis;
			GetAxis(chartComp, out XAxis, out YAxis);

			#endregion
			#region Correlation
			if (chart.SpecificAnalysis == ChartSpecificAnalysis.Correlation) {
				chartComp.MarginTop += 5;

				var series = new DataSerieCollection();
				if (chart.Series != null)
					series.AddRange(chart.Series.ToList());

				//since we dont go through the historical analysis we have to add the historical series manually here.
				foreach (var dsc in chart.HistoricalAnalysis.Values) {
					if (dsc != null) {
						series.AddRange(dsc.ToList());
						List<Series> extraSeries = DataSeries_Convert(dsc.ToList(), chartComp, chart).ToList();
						chartComp.SeriesCollection.Add(extraSeries);
					}
				}

				var cloudSerie = series.FindByLocalId(ChartHelper.GetCorrelationCloudDataSerieLocalId());
				var lineSerie = series.FindByLocalId(ChartHelper.GetCorrelationLineDataSerieLocalId());

				var cloudSerie2 = series.FindByLocalId(ChartHelper.GetCorrelationCloudDataSerieLocalId() + "2");
				var lineSerie2 = series.FindByLocalId(ChartHelper.GetCorrelationLineDataSerieLocalId() + "2");

				if ((cloudSerie == null || lineSerie == null) && (cloudSerie2 == null || lineSerie2 == null))
					return;
				XAxis.Scale = Scale.Range;
				YAxis.Scale = Scale.Range;
				XAxis.GenerateElementTicks = false;
				XAxis.FormatString = "";
				if (!string.IsNullOrEmpty(chart.CorrelationXSerieLocalId) && !string.IsNullOrEmpty(chart.CorrelationXSerieLocalId)) {
					DataSerie dataSerie;
					if (series.TryGetDataSerie(chart.CorrelationXSerieLocalId, out dataSerie)) {
						XAxis.Label.Text = dataSerie.Name;
					}
					if (series.TryGetDataSerie(chart.CorrelationYSerieLocalId, out dataSerie)) {
						YAxis.Label.Text = dataSerie.Name;
					}
				}

				if (!string.IsNullOrEmpty(chart.CorrelationXSerieLocalId2) && !string.IsNullOrEmpty(chart.CorrelationXSerieLocalId2)) {
					DataSerie dataSerie;
					if (series.TryGetDataSerie(chart.CorrelationXSerieLocalId2, out dataSerie)) {
						XAxis.Label.Text += " " + dataSerie.Name;
					}
					if (series.TryGetDataSerie(chart.CorrelationYSerieLocalId2, out dataSerie)) {
						YAxis.Label.Text += " " + dataSerie.Name;
					}
				}

				
				foreach (Series serie in chartComp.SeriesCollection){
					if ((cloudSerie != null && serie.Name == cloudSerie.Name) || (cloudSerie2 != null && serie.Name == cloudSerie2.Name)) {
						serie.DefaultElement.Marker.Size *= 2;
						double minX, minY, maxX, maxY;

						foreach (Element element in serie.Elements){
							element.URL =
								string.Format(
									"Javascript: Viz.grid.Chart.onCorrelationCloudPointClick({0}, {1}, {2}, '{3}');",
									chart.KeyChart,
									element.XValue.ToString(CultureInfo.InvariantCulture),
									element.YValue.ToString(CultureInfo.InvariantCulture),
									(cloudSerie != null && serie.Name == cloudSerie.Name) ? cloudSerie.LocalId : ((cloudSerie2 != null && serie.Name == cloudSerie2.Name) ? cloudSerie2.LocalId : ""));
						}

						serie.GetMinMax(out minX, out maxX, out minY, out maxY);
					}
				}
			}
			#endregion
			#region HeatMap
			else if (chart.SpecificAnalysis == ChartSpecificAnalysis.HeatMap && chart.Series.Count > 0) {
				#region YAxis

				YAxis.ClearValues = true;
				var yLabels = new List<string>();
				int count = 0;
				chartComp.SeriesCollection.ToList().ForEach(s => {
					s.Elements.ToList().ForEach(e => {
						YAxis.ExtraTicks.Add(new AxisTick(
												count, s.Name));
						e.YValue = count;
					});
					yLabels.Add(s.Name);
					count++;
				});

				YAxis.DefaultTick.Label.Truncation.Length = 20;
				YAxis.Minimum = -.5;
				YAxis.Maximum = yLabels.Count - .45;
				YAxis.Scale = Scale.Stacked;
				YAxis.ShowGrid = false;
				YAxis.AlternateGridBackground = new Background(Color.Transparent);
				YAxis.Line.Visible = false;
				#endregion
				#region XAxis

				XAxis.SpacingPercentage = 0;
				XAxis.TimePadding = Axis_GetPadding(chart.TimeInterval);
				XAxis.ShowGrid = false;

				#endregion
				#region Chart
				chartComp.DefaultSeries.Type = SeriesType.Marker;
				chartComp.DefaultElement.Marker.Type = ElementMarkerType.Square;
				chartComp.DefaultElement.SmartLabel.Text = chartComp.DefaultElement.SmartLabel.Text.Replace("%YValue", "%BubbleSize");
				chartComp.DefaultSeries.DefaultElement.ToolTip = chartComp.DefaultElement.ToolTip.Replace("%YValue", "%BubbleSize");

				if (chartComp.SeriesCollection == null || chartComp.SeriesCollection.ToList() == null || chartComp.SeriesCollection.ToList().Count == 0)
					return;

				var maxNumberElements = chartComp.SeriesCollection.ToList().Max(s => s.Elements.Count);
				var squareSize = width / (maxNumberElements);
				squareSize = Math.Min(squareSize, height / chartComp.SeriesCollection.Count);
				chartComp.ChartArea.ClearColors();
				chartComp.ChartArea.Background.Visible = false;
				chartComp.SeriesCollection.ToList().ForEach(s => {
					s.Type = SeriesType.Marker;
					s.DefaultElement.Marker.Type = ElementMarkerType.Square;
					s.DefaultElement.Marker.Size = squareSize;
				});
				if (chartComp.Type == DotNetChartType.Combo) {
					var newHeight = yLabels.Count * width / (maxNumberElements) + 30;
					if (height > newHeight) {
						chartComp.MarginTop += (height - newHeight) / 2;
						chartComp.MarginBottom += chartComp.MarginTop;
					}
				}
				else {
					var newWidth = yLabels.Count * width / (maxNumberElements) + 30;
					if (width > newWidth) {
						chartComp.MarginLeft += (width - newWidth) / 2;
						chartComp.MarginRight += chartComp.MarginLeft;
					}
				}
				//we force to hide the legend
				chartComp.TitleBox.Visible = false;
				chartComp.LegendBox.Visible = false;
				#endregion
			}
			#endregion
			#region DifferenceHighlight
			else if (chart.SpecificAnalysis == ChartSpecificAnalysis.DifferenceHighlight && chart.Series.Count > 0) {
				var differenceSerie = chart.Series.FindByLocalId(ChartHelper.GetDifferenceHighlightLocalId());
				if (differenceSerie != null)
					chartComp.SeriesCollection.ToList().ForEach(s => {
						if (s.Name == differenceSerie.Name) {
							s.Line.Width = 0;
							s.Line.Visible = false;
							s.DefaultElement.Marker.Visible = false;
						}
						if (chartComp.YAxis != null)
							chartComp.YAxis.Scale = Scale.Normal;
					});
			}
			#endregion
			#region EnergyCertificate
			else if (chart.SpecificAnalysis == ChartSpecificAnalysis.EnergyCertificate) {
				var categories = chart.EnergyCertificate.Categories;
				#region Constante
				int marginTop = Math.Max(chartComp.MarginTop, 10);
				int marginBottom = Math.Max(chartComp.MarginBottom, 10);
				int marginLeft = Math.Max(chartComp.MarginLeft, 5);
				int marginRight = Math.Max(chartComp.MarginRight, 5);
				int marginElement = 10;
				Color StartColor = ColorHelper.GetColor(chart.EnergyCertificate.StartColor);
				Color StopColor = ColorHelper.GetColor(chart.EnergyCertificate.EndColor);
				Color BlueColor = Color.FromArgb(17, 145, 208);
				Color BlueColorLight = Color.FromArgb(233, 246, 252);
				#endregion
				#region Series Init
				var theData = new Series() { LegendEntry = { Visible = false }, Type = SeriesType.Marker };
				var back = new Series() { LegendEntry = { Visible = false }, Type = SeriesType.Bar };
				back.DefaultElement.Marker.Type = ElementMarkerType.None;

				var elements = new List<Element>();

				foreach (var s in chart.Series.ToList()) {
					if (!s.Hidden) {
						if (chart.EnergyCertificateDisplayElements == false) {
							var dp = DataSerieHelper.Project(s);
							var el = DataPoint_Convert(chartComp, dp, chart.NumericFormat);
							if (!string.IsNullOrEmpty(s.Unit))
								el.CustomAttributes.Add("Unit", s.Unit);
							elements.Add(el);
						}
						else {
							s.PointsWithValue.ForEach(dp => {
								var el = DataPoint_Convert(chartComp, dp, chart.NumericFormat);
								if (dp.XDateTime.HasValue)
									el.Name = s.Name + " " + dp.XDateTime.Value.ToString(chart.TimeIntervalFormat);
								if (!string.IsNullOrEmpty(s.Unit))
									el.CustomAttributes.Add("Unit", s.Unit);


								elements.Add(el);
							});
						}
					}
				}

				if (chart.EnergyCertificateTarget.HasValue) {
					elements.Add(new Element { YValue = chart.EnergyCertificateTarget.Value, Name = Helper.LocalizeText("msg_chart_energycertificate_target") });
				}
				#endregion
				#region Chart Init
				chartComp.SeriesCollection.Clear();
				chartComp.AxisCollection.Clear();
				Axis_Hide(chartComp.XAxis);
				Axis_Hide(chartComp.YAxis);
				chartComp.ChartArea.ClearColors();
				chartComp.ChartArea.Background.Color = Color.Transparent;
				chartComp.ChartArea.Background.SecondaryColor = Color.Transparent;

				chartComp.ChartArea.Line.Color = Color.Transparent;
				chartComp.ChartArea.Line.Width = 0;
				chartComp.Type = DotNetChartType.ComboHorizontal;

				chartComp.LegendBox.Visible = false;
				chartComp.TitleBox.Visible = false;
				//chartComp.DefaultSeries.Line.Width = LineWidth;
				chartComp.YAxis.DefaultTick.Label.Color = Color.Transparent;
				chartComp.XAxis.ClearValues = true;
				chartComp.YAxis.DefaultTick.Line.Color = Color.Transparent;
				chartComp.YAxis.DefaultTick.Label.Font = CreateSafeFont(chart.DisplayFontFamily, chart.DisplayFontSize * 2, FontStyle.Bold);
				chartComp.YAxis.GenerateElementTicks = false;
				chartComp.YAxis.MinimumInterval = 1;
				chartComp.YAxis.InvertScale = true;
				chartComp.YAxis.Scale = Scale.Normal;
				chartComp.DefaultElement.SmartLabel.Font = chartComp.DefaultElement.SmartLabel.Font;
				chartComp.YAxis.Line.Color = Color.Transparent;
				chartComp.XAxis.Line.Color = Color.Transparent;
				chartComp.DefaultElement.Marker.Size = chart.DisplayMarkerSize;
				chartComp.MarginTop = Math.Max(chartComp.MarginTop, 5);
				chartComp.MarginBottom = Math.Max(chartComp.MarginBottom, 5);
				chartComp.MarginLeft = Math.Max(chartComp.MarginLeft, 20);
				chartComp.MarginRight = Math.Max(chartComp.MarginRight, 20);
				#endregion
				#region DPE frame
				int categoryIndex = 0;
				categories.ForEach(category => {
					var el = new Element(category.Name) { YValue = categoryIndex + 2 };
					string tooltip = "";
					if (category.MinValue.HasValue == false || double.IsNaN(category.MinValue.Value))
						tooltip = "<" + category.MaxValue.ToString();
					else if (category.MaxValue.HasValue == false || double.IsNaN(category.MaxValue.Value))
						tooltip = ">" + category.MinValue.ToString();
					else
						tooltip = category.MinValue.ToString() + " - " + category.MaxValue.ToString();

					el.ToolTip = tooltip;
					el.ShowValue = chart.DisplayValues;
					el.SmartLabel.Text = tooltip;
					el.SmartLabel.DynamicPosition = false;
					el.XValue = categoryIndex + 1;
					el.Color = dotnetCHARTING.Internal.ChartDrawer.Common.Utils.HSVtoRGB(StartColor.GetHue() + (StopColor.GetHue() - StartColor.GetHue()) * categoryIndex / ((double)categories.Count - 1), (StartColor.GetSaturation() + (StopColor.GetSaturation() - StartColor.GetSaturation()) * categoryIndex / ((double)categories.Count - 1)), 2.0 * (StartColor.GetBrightness() + (StopColor.GetBrightness() - StartColor.GetBrightness()) * categoryIndex / ((double)categories.Count - 1)));
					back.Elements.Add(el);
					chartComp.YAxis.ExtraTicks.Add(new AxisTick(categoryIndex + 1, category.Name));
					chartComp.YAxis.ExtraTicks[categoryIndex].SetAllColors(el.Color);
					categoryIndex++;
				});
				chartComp.SeriesCollection.Add(back);
				#endregion
				#region Axis
				chartComp.XAxis.Maximum = categories.Count * 3;
				chartComp.YAxis.Minimum = -1;
				chartComp.YAxis.Maximum = categories.Count + 2;
				chartComp.XAxis.Clear();
				#endregion
				#region Draw Image
				Bitmap bmp = chartComp.GetChartBitmap();
				chartComp.FileManager.SaveImage(bmp);
				#endregion
				#region Draw Series
				int j = 0;
				foreach (var e in elements) {
					for (int i = 0; i < categories.Count; i++) {
						Element el = back[i];
						var category = categories[i];
						if (!category.MinValue.HasValue)
							category.MinValue = double.MinValue;
						if (!category.MaxValue.HasValue)
							category.MaxValue = double.MaxValue;


						int annotationHeaderY = marginTop;
						int annotationHeaderHeight = chart.DisplayFontSize * 2 + 10;
						int annotationHeaderWidth = Convert.ToInt16(chartComp.Width.Value - back[categories.Count - 1].Point.X - marginRight - marginLeft) / Math.Max(elements.Count, 1) - marginElement;
						int annotationHeaderX = Convert.ToInt16(back[categories.Count - 1].Point.X) + marginElement + j * (annotationHeaderWidth + marginElement);

						int annotationFooterHeight = chart.DisplayFontSize * 3 + 10;


						int annotationBodyX = annotationHeaderX;
						int annotationBodyY = annotationHeaderY + annotationHeaderHeight;
						int annotationBodyHeight = Convert.ToInt16(chartComp.Height.Value - marginTop - marginBottom - annotationHeaderHeight - annotationFooterHeight);
						int annotationBodyWidth = annotationHeaderWidth;

						int annotationFooterX = annotationHeaderX;
						int annotationFooterY = annotationBodyY + annotationBodyHeight;
						int annotationFooterWidth = annotationHeaderWidth;

						int annotationTextHeight = chart.DisplayFontSize * 3 + 10;
						int annotationTextWidth = annotationHeaderWidth;

						int annotationTextX = annotationHeaderX;
						int annotationTextY = Convert.ToInt16(el.Point.Y) - annotationTextHeight / 2;

						double? value = null;
						if ((double.IsNaN(e.XValue) == false && e.XValue >= category.MinValue.Value && e.XValue <= category.MaxValue.Value))
							value = e.XValue;
						if ((double.IsNaN(e.YValue) == false && e.YValue >= category.MinValue.Value && e.YValue <= category.MaxValue.Value))
							value = e.YValue;
						if (value.HasValue) {
							var valueAsText = Math.Round(e.YValue, chart.DisplayDecimalPrecision).ToString(chart.NumericFormat);
							if (e.CustomAttributes.Contains("Unit")) {
								valueAsText += " " + e.CustomAttributes["Unit"] ?? "";
							}
							//HEADER
							var a = new Annotation(e.Name) {
								ToolTip = e.Name,
								DefaultCorner = BoxCorner.Square,
								Background = new Background(BlueColor),
								CornerTopLeft = BoxCorner.Round,
								CornerTopRight = BoxCorner.Round,
								Position = new Point(annotationHeaderX, annotationHeaderY),
								Size = new Size(annotationHeaderWidth, annotationHeaderHeight),
								DynamicSize = false,
								Label = {
									Font = CreateSafeFont(chart.DisplayFontFamily, chart.DisplayFontSize * 2, FontStyle.Bold),
									Color = Color.White
								}
							};
							chartComp.Annotations.Add(a);

							//BODY
							var aBody = new Annotation {
								DefaultCorner = BoxCorner.Square,
								Background = { Color = BlueColor, Transparency = 30 },
								Position = new Point(annotationBodyX, annotationBodyY),
								Size = new Size(annotationBodyWidth, annotationBodyHeight),
								DynamicSize = false
							};
							chartComp.Annotations.Add(aBody);


							//FOOTER
							var aFooter = new Annotation(valueAsText) {
								DefaultCorner = BoxCorner.Square,
								CornerBottomLeft = BoxCorner.Round,
								CornerBottomRight = BoxCorner.Round,
								Position = new Point(annotationFooterX, annotationFooterY),
								Size = new Size(annotationFooterWidth, annotationFooterHeight),
								DynamicSize = false,
								Background = new Background(BlueColor),
								Label = {
									Font = CreateSafeFont(chart.DisplayFontFamily, chart.DisplayFontSize * 3, FontStyle.Bold),
									Color = Color.White
								}
							};
							chartComp.Annotations.Add(aFooter);

							//TEXT
							var aText = new Annotation(el.Name) {
								ToolTip = valueAsText,
								Label = {
									Font = CreateSafeFont(chart.DisplayFontFamily, chart.DisplayFontSize * 3, FontStyle.Bold),
									Color = Color.White
								},
								DefaultCorner = BoxCorner.Cut,
								Size = new Size(annotationTextWidth, annotationTextHeight),
								Position = new Point(annotationTextX, annotationTextY),
								DynamicSize = false,
								Background = {
									Color = el.Color // e.Color.IsEmpty ? Color.DarkBlue : e.Color
								}
							};
							chartComp.Annotations.Add(aText);


							j += 1;
						}
						//var am = new AxisMarker("", new Line(BlueColor, 2, DashStyle.Dash), el.XValue);
						//am.IncludeInAxisScale = true;
						//chartComp.YAxis.Markers.Add(am);
					}
				}
				#endregion
				chartComp.SeriesCollection.Add(theData);
			}
			#endregion
			#region MicroChart
			else if (chart.SpecificAnalysis == ChartSpecificAnalysis.MicroChart) {
				//chartComp.MarginTop += 10;
				var sb = new StringBuilder();
				var series = chart.Series.ToList();
				if (chart.MicroChartDisplayElements == true) {
					var categories = ChartHelper.GetCategories(chart).Select(c => c.Item1).ToList();

					sb.Append(string.Format("<block hAlign='Left'>{0}", ""));//Langue.msg_name
					foreach (string category in categories) {
						sb.Append(string.Format("<block hAlign='Center'><block fStyle='Bold' hAlign='Center'>{0}", category));
					}
					sb.Append("<HR>");

					for (var j = 0; j < chart.Series.Count; j++) {
						if (!series[j].Hidden) {
							sb.Append(string.Format("<block hAlign='Left' fStyle='Bold'>{0}", series[j].Name));

							if (!string.IsNullOrWhiteSpace(series[j].Unit)) {
								sb.Append(string.Format(" ({0})", series[j].Unit));
							}

							for (var i = 0; i < categories.Count; i++) {
								if (i < series[j].Points.Count && series[j].Points[i].YValue.HasValue) {
									var value = series[j].Points[i].YValue ?? 0;
									var targetValue = series[j].TargetElement ?? chart.MicroChartTargetElement ?? 0;
									sb.Append(string.Format("<block hAlign='Right'>{0}/{1}", value.ToString(chart.NumericFormat), targetValue.ToString(chart.NumericFormat)));
									sb.Append(GetMicroChartFormatSimple(value, targetValue, 0, series[j].Color, chart.DisplayFontSize, chart.NumericFormat, chart.DisplayFontColor, chart.DisplayShaddingEffect.ParseAsInt()));
								}
								else {
									sb.Append("<block hAlign='Right'>");
									sb.Append("<block hAlign='Right'>");
								}
							}
							if (j < chart.Series.Count - 1)
								sb.Append("<HR>");
						}
					}
				}
				else {
					foreach (DataSerie s in series) {
						if (!s.Hidden)
							sb.Append(GetMicroChartFormat(s.Name, DataSerieHelper.Project(s).YValue ?? 0, s.Target ?? chart.MicroChartTarget ?? 0, 0, s.Color, chart.DisplayFontSize, chart.NumericFormat, chart.DisplayFontColor, chart.DisplayShaddingEffect.ParseAsInt(), s.Unit) + "<hr><row>");//"<HR>"
					}
				}
				var an = new Annotation(sb.ToString());
				an.Label.Alignment = StringAlignment.Near;
				an.DynamicSize = false;
				an.Line.Visible = false;
				an.Header.Label.Text = " ";
				//an.Padding = 5;
				an.DefaultCorner = BoxCorner.Square;
				an.Header.Label.Font = CreateSafeFont(chart.DisplayFontFamily, chart.DisplayFontSize, FontStyle.Bold);
				an.Label.Font = CreateSafeFont(chart.DisplayFontFamily, chart.DisplayFontSize);
				an.Background = new Background(Color.Transparent);
				an.Background.ShadingEffectMode = ShadingEffectMode.None;
				an.Shadow.Visible = false;
				an.Shadow.Color = Color.Transparent;
				chartComp.ObjectChart = an;

			}
			#endregion
			#region CalendarView
			else if (chart.SpecificAnalysis == ChartSpecificAnalysis.CalendarView && SpecificAnalysis_CalendarView_TimeInterval_Validate(chart.CalendarViewTimeInterval)) {
				#region hasSelections
				bool hasSelections = chart.CalendarViewSelections != null && chart.CalendarViewSelections.Count > 0;
				#endregion
				#region Check CalendarViewTimeInterval
				if (chart.TimeInterval.ParseAsInt() < chart.CalendarViewTimeInterval.ParseAsInt() || chart.TimeInterval == AxisTimeInterval.Semesters) {
					chartComp.NoDataLabel = new dotnetCHARTING.Label(Helper.LocalizeText("error_chart_specificanalysis_calendarview_wrongtimeinterval"));
					return;
				}
				#endregion
				#region Start date & End Date
				var currentDate = chart.StartDate.ConvertTimeFromUtc(chart.GetTimeZone());
				var endDate = chart.EndDate.ConvertTimeFromUtc(chart.GetTimeZone());
				var currentEndDate = currentDate;


				//the date range is based on data, so it s already on the correct timezone...
				var actualDateRange = ChartHelper.GetActualDateRange(chart);
				if (actualDateRange != null) {
					currentDate = actualDateRange.Item1;
					endDate = actualDateRange.Item2;
				}

				var calendarTimeInterval = chart.CalendarViewTimeInterval;
				#endregion
				#region X and Y Axis
				YAxis = new Axis();
				YAxis.SynchronizeScale.PreserveScale = true;

				if ((chart.Axis != null) && (chart.Axis.Any())) {

					ChartAxis chartaxis = chart.Axis.First().Value;
					if (chartaxis.Markers != null && chartaxis.Markers.Count > 0) {
						foreach (var marker in chartaxis.Markers) {
							var axismarker = ChartMarker_Convert(marker);
							if (axismarker != null)
								YAxis.Markers.Add(axismarker);
						}
					}
				}
				chartComp.YAxis = YAxis;
				
				XAxis = new Axis {

					TimeInterval = AxisTimeInterval_Convert(chart.TimeInterval),
					FormatString = SpecificAnalysis_CalendarView_GetXAxisFormat(calendarTimeInterval)
				};


				if (!(chart.TimeInterval == AxisTimeInterval.Semesters || chart.TimeInterval == AxisTimeInterval.Quarters)) {
					XAxis.Scale = Scale.Time;
				}
				else {
					XAxis.DefaultTick.Label.Text = "%Name";
				}

				//Since calendar in minutes are very very dense, it messes with the xaxis. So if the chart is in minutes, we still keep the axis in hours.
				if (chart.TimeInterval == AxisTimeInterval.Minutes) {
					XAxis.TimeInterval = AxisTimeInterval_Convert(AxisTimeInterval.Hours);
				}

				if (chart.TimeInterval == AxisTimeInterval.Weeks) {
					XAxis.TimeIntervalAdvanced.StartDayOfWeek = chart.XAxisStartDayOfWeek;
				}
			
				chartComp.XAxis = XAxis;
				AxisXMarkers_Init(XAxis, chart.TimeInterval, chart.NightColorR, chart.NightColorG, chart.NightColorB, chart.NightStartTime, chart.NightEndTime, chart.XAxisStartDayOfWeek);

				#endregion
				chartComp.ChartArea.Visible = false;
				chartComp.ChartAreaLayout.Mode = ChartAreaLayoutMode.Custom;

				int count = 0;
				int countDay = 0;
				int colorCount = 0;

				chartComp.TitleBox = new Box();
				chartComp.SeriesCollection.Clear();

				chartComp.LegendBox = new LegendBox { Visible = false };

				chartComp.ChartAreaSpacing = 0;

				var subseries = new List<SpecificAnalysisCalendarViewSerie>();

				ChartArea area = null;
				while (currentDate <= endDate) {
					currentEndDate = currentDate.AddInterval(calendarTimeInterval, 1);
					#region New Area Creation

					area = new ChartArea { Background = chartComp.ChartArea.Background };
					chartComp.ExtraChartAreas.Add(area);
					XAxis = new Axis {
						TimeInterval = chartComp.XAxis.TimeInterval,
						FormatString = chartComp.XAxis.FormatString
					};


					if (!(chart.TimeInterval == AxisTimeInterval.Semesters || chart.TimeInterval == AxisTimeInterval.Quarters)) {
						XAxis.Scale = Scale.Time;
					}
					else {
						XAxis.DefaultTick.Label.Text = "%Name";
					}

					//Since calendar in minutes are very very dense, it messes with the xaxis. So if the chart is in minutes, we still keep the axis in hours.
					if (chart.TimeInterval == AxisTimeInterval.Minutes) {
						XAxis.TimeInterval = AxisTimeInterval_Convert(AxisTimeInterval.Hours);
					}

					if (chart.TimeInterval == AxisTimeInterval.Weeks) {
						XAxis.TimeIntervalAdvanced.StartDayOfWeek = chart.XAxisStartDayOfWeek;
					}

					Axis_SetMinMax(XAxis, chart.TimeInterval, currentDate, currentEndDate, TimeZoneInfo.Utc, true, true);

					AxisXMarkers_Init(XAxis, chart.TimeInterval, chart.NightColorR, chart.NightColorG, chart.NightColorB, chart.NightStartTime, chart.NightEndTime, chart.XAxisStartDayOfWeek);
					#endregion
					#region New Area Style


					if (chart.CalendarViewTimeInterval == AxisTimeInterval.Quarters) {
						area.Title = Helper.ConvertDateTimeToQuarterString(currentDate);
					}
					else {
						area.Title = currentDate.ToString(SpecificAnalysis_CalendarView_GetTitleDateFormat(calendarTimeInterval));
					}
					area.TitleBox.Label.Hotspot.ToolTip = area.Title;
					area.TitleBox.DefaultCorner = BoxCorner.Square;
					area.TitleBox.Label.Hotspot.URL = string.Format("return Viz.grid.Chart.onCalendarViewAreaClick('{0}','{1}','{2}');", chart.KeyChart, currentDate.ToString(DateTimeHelper.const_date_time_format_for_filter), currentEndDate.ToString(DateTimeHelper.const_date_time_format_for_filter));
					area.XAxis = XAxis;
					area.YAxis = YAxis;

					if (hasSelections) {
						var selections = chart.CalendarViewSelections.Where(selection => selection.StartDate == currentDate && selection.EndDate == currentEndDate).ToList();
						if (selections.Count > 0) {
							area.TitleBox.Label.Hotspot.URL = string.Format("return Viz.grid.Chart.onCalendarViewSelectionDelete('{0}','{1}');", selections[0].KeyChart, selections[0].KeyChartCalendarViewSelection);
							area.TitleBox.Background.Color = Color.Red;
						}
					}
					#endregion
					#region New DataSeries
					foreach (var s in chart.Series.ToList().Where(s=>!s.Hidden)) {
						var color = chart.Series.Count > 1 ? s.Color : SpecificAnalysis_CalendarView_GetColor(chart, colorCount);
						var newserie = SpecificAnalysis_CalendarView_CreateSerie(chartComp, chart, XAxis, chartComp.YAxis, currentDate, currentEndDate, area, s, color);
						subseries.Add(new SpecificAnalysisCalendarViewSerie() {
							Color = color,
							StartDate = currentDate,
							EndDate = currentEndDate,
							SerieName = s.Name
						});

						colorCount += 1;
					}
					SpecificAnalysis_CalendarView_EventLog_CreateSerie(chartComp, chart, currentDate, currentEndDate, area);
					#endregion
					#region GridPosition
					area.GridPosition = new Point(countDay, count / SpecificAnalysis_CalendarView_GetNumberOfChartAreaPerRow(calendarTimeInterval));

					currentDate = currentEndDate;

					count += 1;
					countDay += 1;
					if (countDay >= SpecificAnalysis_CalendarView_GetNumberOfChartAreaPerRow(calendarTimeInterval))
						countDay = 0;
					#endregion
				}
				#region Selections
				if (hasSelections) {
					var row = chartComp.ExtraChartAreas.ToList().Max(a => a.GridPosition.Y) + 1;
					var col = chartComp.ExtraChartAreas.ToList().Max(a => a.GridPosition.X) + 1;

					area = new ChartArea {Background = chartComp.ChartArea.Background};

					area.DefaultSeries.DefaultElement.Marker.Size = chart.DisplayMarkerSize * 3;
					area.DefaultSeries.Line.Width = chart.DisplayLineThickness * 3;

					area.GridPosition = new Point(col, row);
					area.HeightPercentage = 70;
					area.WidthPercentage = 80;
					//because the main Y axis is hidden when in selection mode.
					area.YAxis = new Axis();
					area.LegendBox = new LegendBox();
					area.TitleBox = new LegendBox();
					Legend_Init(area, chart, chart.DisplayFontSize * 3);

					Fonts_Init(area, chart.DisplayFontFamily, chart.DisplayFontSize * 3, chart.DisplayFontColor);
					int countNewSeries = 0;
					chart.CalendarViewSelections.ForEach(selection => {
						foreach (var s in chart.Series.ToList()) {
							XAxis = new Axis {
								Scale = Scale.Time,
								TimeInterval = chartComp.XAxis.TimeInterval,
								FormatString = String.IsNullOrEmpty(chart.XAxisFormat) ? chart.TimeInterval.GetFormat(true) : chart.XAxisFormat
							};

							Fonts_Init(XAxis, chart.DisplayFontFamily, chart.DisplayFontSize * 3, chart.DisplayFontColor);


							var previewEntries = subseries.Where(entry => entry.StartDate.ToUniversalTime() == selection.StartDate.ToUniversalTime() && entry.EndDate.ToUniversalTime() == selection.EndDate.ToUniversalTime() && entry.SerieName == s.Name).ToList();
							if (previewEntries.Count > 0) {

								var newserie = SpecificAnalysis_CalendarView_CreateSerie(chartComp, chart, XAxis, area.YAxis, selection.StartDate, selection.EndDate, area, s, previewEntries[0].Color);
								newserie.LegendEntry.Hotspot.URL = string.Format("return Viz.grid.Chart.onCalendarViewSelectionDelete('{0}','{1}');", selection.KeyChart, selection.KeyChartCalendarViewSelection);

								if (newserie.Elements.Count > 0) {
									countNewSeries += 1;
									newserie.Name += " " + selection.StartDate.ToString(SpecificAnalysis_CalendarView_GetTitleDateFormat(calendarTimeInterval));
								}
								if (countNewSeries == 1) {
									AxisXMarkers_Init(XAxis, chart.TimeInterval, chart.NightColorR, chart.NightColorG, chart.NightColorB, chart.NightStartTime, chart.NightEndTime, chart.XAxisStartDayOfWeek);
								}
								//we only show the first X Axis
								else if (countNewSeries > 1) {
									Axis_Hide(XAxis);
									if (newserie.XAxis != null) {
										Axis_Hide(newserie.XAxis);
									}

								}
							}
						}
						SpecificAnalysis_CalendarView_EventLog_CreateSerie(chartComp, chart, selection.StartDate, selection.EndDate, area);
					});
					if (area.SeriesCollection.Count > 0)
						chartComp.ExtraChartAreas.Add(area);
				}
				#endregion
			}
			#endregion
			#region LoadDurationCurve
			else if (chart.SpecificAnalysis == ChartSpecificAnalysis.LoadDurationCurve) {
				chartComp.XAxis.Scale = Scale.Normal;
				chartComp.YAxis.Scale = Scale.Normal;
				chartComp.XAxis.Minimum = 0;
				chartComp.XAxis.Maximum = 100;
				chartComp.XAxis.Name = chart.TimeInterval.GetLocalizedText();
				//chartComp.YAxis.MinimumInterval = 1;
				chartComp.XAxis.ShowGrid = false;
				chartComp.XAxis.DefaultTick.Label.Text = "<%Value,N>\\%";
				chartComp.XAxis.GenerateElementTicks = false;
				chartComp.YAxis.DefaultTick.Label.Text = "<%Value," + chart.NumericFormat + ">";
				chartComp.YAxis.GenerateElementTicks = false;

				double maxXValue = 0;
				foreach (var s in chart.Series.ToList()) {
					foreach (var p in s.PointsWithValue) {
						maxXValue = Math.Max(maxXValue, p.XValue.Value);
					}
				}

				chartComp.XAxis.Maximum = maxXValue;
			}
			#endregion
		}
		#region SpecificAnalysis_CalendarView Private functions

		private class SpecificAnalysisCalendarViewSerie {
			public DateTime StartDate { get; set; }
			public DateTime EndDate { get; set; }
			public string SerieName { get; set; }
			public Color? Color { get; set; }
		}

		/// <summary>
		/// Create Trimmed Series For Event Log in Calendar View.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="area">The area.</param>
		private static void SpecificAnalysis_CalendarView_EventLog_CreateSerie(DotNetChart chartComp, VizChart chart, DateTime startDate, DateTime endDate, ChartArea area) {
			if (chart.EventLogs != null) {
				chart.EventLogs.ForEach(eventLog => {
					if ((eventLog.StartDate >= startDate && eventLog.StartDate < endDate) || (eventLog.EndDate >= startDate && eventLog.EndDate < endDate) || (eventLog.StartDate < startDate && eventLog.EndDate > endDate)) {
						var eventLogStartDate = eventLog.StartDate;
						var eventLogEndDate = eventLog.EndDate;

						eventLog.StartDate = eventLog.StartDate > startDate ? eventLog.StartDate : startDate;
						eventLog.EndDate = eventLog.EndDate > endDate ? endDate : eventLog.EndDate;

						area.SeriesCollection.Add(EventLog_Convert(chartComp, new List<EventLog> { eventLog }, startDate, endDate, chart.GetTimeZone()));

						eventLog.StartDate = eventLogStartDate;
						eventLog.EndDate = eventLogEndDate;
					}
				});
			}
		}


		/// <summary>
		/// Specifics the color of the analysis_ calendar view_ get.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="colorIndex">Index of the color.</param>
		/// <returns></returns>
		private static Color? SpecificAnalysis_CalendarView_GetColor(VizChart chart, int colorIndex) {
			if (chart.Palette != null && chart.Palette.Colors != null && chart.Palette.Colors.Count > 0) {
				int index = colorIndex % chart.Palette.Colors.Count;
				var color = chart.Palette.Colors[index];
				return color.GetColor();
			}
			return null;
		}

		/// <summary>
		/// Create the calendar view  serie.
		/// </summary>
		/// <param name="chartComp">The chart comp.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="XAxis">The X axis.</param>
		/// <param name="YAxis">The Y axis.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="area">The area.</param>
		/// <param name="s">The s.</param>
		/// <param name="color">The color.</param>
		/// <returns></returns>
		private static Series SpecificAnalysis_CalendarView_CreateSerie(DotNetChart chartComp, VizChart chart, Axis XAxis, Axis YAxis, DateTime startDate, DateTime endDate, ChartArea area, DataSerie s, Color? color) {
			if (color.HasValue) {
				s.ColorR = color.Value.R;
				s.ColorG = color.Value.G;
				s.ColorB = color.Value.B;
			}

			var newserie = DataSerie_Convert(s, chartComp, chart.NumericFormat, chart.MaxDataPointPerDataSerie, chart.StartDate, false);

			newserie.YAxis = YAxis;
			newserie.XAxis = XAxis;

			s.PointsWithValue.ForEach(p => {
				if (p.XDateTime.HasValue && p.XDateTime.Value >= startDate && p.XDateTime.Value < endDate) {
					newserie.Elements.Add(DataPoint_Convert(chartComp, p, chart.NumericFormat));
				}
			});
			if (newserie.Elements.Count > 0)
				area.SeriesCollection.Add(newserie);

			return newserie;
		}
		/// <summary>
		/// Check if the CalendarViewTimeInterval is valid.
		/// </summary>
		/// <param name="interval">The interval.</param>
		/// <returns></returns>
		private static bool SpecificAnalysis_CalendarView_TimeInterval_Validate(AxisTimeInterval interval) {
			switch (interval) {
				case AxisTimeInterval.Quarters:
				case AxisTimeInterval.Days:
				case AxisTimeInterval.Months:
				case AxisTimeInterval.Hours:
					return true;

				default:
					return false;
			}

		}
		/// <summary>
		/// Get the number of chart area per row.
		/// </summary>
		/// <param name="calendarTimeInterval">The calendar time interval.</param>
		/// <returns></returns>
		private static int SpecificAnalysis_CalendarView_GetNumberOfChartAreaPerRow(AxisTimeInterval calendarTimeInterval) {
			switch (calendarTimeInterval) {

				case AxisTimeInterval.Hours:
					return 24;

				case AxisTimeInterval.Days:
					return 7;

				case AxisTimeInterval.Weeks:
					return 4;

				case AxisTimeInterval.Months:
					return 12;

				case AxisTimeInterval.Years:
					return 4;

			}
			return 10;
		}
		/// <summary>
		/// Get the title date format for calendar view.
		/// </summary>
		/// <param name="calendarTimeInterval">The calendar time interval.</param>
		/// <returns></returns>
		private static string SpecificAnalysis_CalendarView_GetTitleDateFormat(AxisTimeInterval calendarTimeInterval) {
			switch (calendarTimeInterval) {
				case AxisTimeInterval.Days:
					return "dddd " + AxisTimeInterval.Days.GetFormat();

				case AxisTimeInterval.Hours:
					return "ddd. MM HH:mm";
			}
			return calendarTimeInterval.GetFormat();
		}
		/// <summary>
		/// Get the X axis format  for calendar view.
		/// </summary>
		/// <param name="calendarTimeInterval">The calendar time interval.</param>
		/// <returns></returns>
		private static string SpecificAnalysis_CalendarView_GetXAxisFormat(AxisTimeInterval calendarTimeInterval) {
			switch (calendarTimeInterval) {
				case AxisTimeInterval.Days:
					return "HH:mm";

				case AxisTimeInterval.Hours:
					return "HH:mm";

				case AxisTimeInterval.Months:
					return "dd";
			}

			return calendarTimeInterval.GetFormat();

		}
		#endregion
		#endregion
		#region Micro Chart
		/// <summary>
		/// Creates the micro chart.
		/// </summary>
		/// <param name="licence">The licence.</param>
		/// <param name="text">The text.</param>
		/// <param name="value">The value.</param>
		/// <param name="targetValue">The target value.</param>
		/// <param name="minValue">The min value.</param>
		/// <param name="color">The color.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="tempDirectory">The temp directory.</param>
		/// <param name="imageToDeletePath">The image to delete path.</param>
		/// <returns></returns>
		public static DotNetChart Chart_CreateMicroChart(string licence, string text, double value, double targetValue, double? minValue, Color color, int width, int height, string tempDirectory, out string imageToDeletePath) {
			var chartComp = new DotNetChart();
			imageToDeletePath = "";
			Chart_Init(chartComp, null, licence, width, height, tempDirectory, out imageToDeletePath);
			Background_SetTransparent(chartComp);

			var format = GetMicroChartFormat(text, value, targetValue, minValue, color, 10);
			var label = new dotnetCHARTING.Label(format);
			label.Alignment = StringAlignment.Near;
			chartComp.ObjectChart = label;
			return chartComp;
		}

		/// <summary>
		/// Gets the micro chart format.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="value">The value.</param>
		/// <param name="targetValue">The target value.</param>
		/// <param name="minValue">The min value.</param>
		/// <param name="color">The color.</param>
		/// <param name="fontSize">Size of the font.</param>
		/// <param name="numericFormat">The numeric format.</param>
		/// <param name="fontColor">Color of the font.</param>
		/// <param name="shading">The shading.</param>
		/// <param name="unit">The unit.</param>
		/// <returns></returns>
		private static string GetMicroChartFormat(string text, double value, double targetValue, double? minValue, Color color, int fontSize, string numericFormat = "N0", string fontColor = "", int shading = 4, string unit = "") {
			var retVal = "";
			var finalUnit = (string.IsNullOrEmpty(unit) ? "" : " " + unit);

			if (double.IsNaN(value) == false) {
				var min = "";
				if (minValue.HasValue) {
					min = string.Format(" min='{0}' ", minValue.Value);
				}
				if (string.IsNullOrEmpty(fontColor))
					fontColor = "000000";
				retVal = string.Format("<block hAlign='Left' fColor='{7}' fSize='{8}'> {3} <row><Chart:BarGauge height='40' width='230' value='{5}' max='{6}'  {4} shading='{9}' color='{2}'><block hAlign='Right' fColor='{7}' fSize='{8}'>{0}<block fColor='{7}' fSize='{11}' hAlign='Right'>{10}", value.ToString(numericFormat) + finalUnit, targetValue.ToString(numericFormat) + finalUnit, ColorHelper.GetHTMLColor(color), text, min, value, targetValue, ColorHelper.GetHTMLColor(ColorHelper.GetColor(fontColor)), fontSize, shading, ((value / targetValue) * 100).ToString(numericFormat) + " %", fontSize * 2);//<row><block><block><block><block><block fColor='{7}' fSize='{8}' hAlign='Left'>{1}
			}
			return retVal;
		}

		/// <summary>
		/// Gets the micro chart simple.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="targetValue">The target value.</param>
		/// <param name="minValue">The min value.</param>
		/// <param name="color">The color.</param>
		/// <param name="fontSize">Size of the font.</param>
		/// <param name="numericFormat">The numeric format.</param>
		/// <param name="fontColor">Color of the font.</param>
		/// <param name="shading">The shading.</param>
		/// <returns></returns>
		private static string GetMicroChartFormatSimple(double value, double targetValue, double? minValue, Color color, int fontSize, string numericFormat = "N0", string fontColor = "", int shading = 4) {
			var retVal = "";
			if (double.IsNaN(value) == false) {
				var min = "";
				if (minValue.HasValue) {
					min = string.Format(" min='{0}' ", minValue.Value);
				}
				if (string.IsNullOrEmpty(fontColor))
					fontColor = "000000";

				retVal = string.Format("<Chart:BarGauge value='{2}' max='{3}'  {1} shading='{6}' color='{0}' width='50' >", ColorHelper.GetHTMLColor(color), min, value, targetValue, ColorHelper.GetHTMLColor(ColorHelper.GetColor(fontColor)), fontSize, shading);

			}
			return retVal;
		}
		#endregion
		#region Drill Down
		/// <summary>
		/// Generates DrillDown URL for the Chart
		/// </summary>
		/// <param name="s">The dotnet serie.</param>
		/// <param name="serie">The serie  business entity.</param>
		/// <param name="chart">The chart.</param>
		private static void DrillDown_Generate(DataSerie s, Series serie, VizChart chart) {
			if (serie.Elements != null) {
				serie.Elements.ToList().ForEach(el => {

					var datetime = "";

					var DateTimeName = "";
					if (chart.ChartType != VizChartType.Gauge && chart.ChartType != VizChartType.Donut) {
						if (el.XDateTime > DateTime.MinValue && el.XDateTime < DateTime.MaxValue) {
							datetime = el.XDateTime.ToString(DateTimeHelper.const_date_time_format_for_filter);
							DateTimeName = el.XDateTime.ToString(chart.TimeIntervalFormat);
						}
					}
					var KeyLocation = "";
					var LocationName = "";
					var ClassificationName = s.ClassificationName;
					if (chart.Localisation == ChartLocalisation.ByMeter) {
						KeyLocation = s.KeyMeter;
						LocationName = s.LocationName;
					}
					else {
						KeyLocation = s.KeyLocation ?? "";
						LocationName = s.LocationName;
					}
					el.URL = string.Format("return Viz.grid.Chart.onDataPointClick('{0}', '{1}', '{2}','{3}','{4}','{5}','{6}','{7}');", s.KeyChart, datetime, KeyLocation, s.KeyClassificationDrillDown ?? "", LocationName, ClassificationName, DateTimeName, chart.DrillDownMode.GetLocalizedText());
				});
			}
		}
		#endregion
		#region DeleteImage File
		/// <summary>
		/// Deletes the image file.
		/// </summary>
		/// <param name="imageToDeletePath">The image to delete path.</param>
		public static void DeleteImageFile(string imageToDeletePath) {
			if (string.IsNullOrEmpty(imageToDeletePath) == false && File.Exists(imageToDeletePath)) {
				try {
					File.Delete(imageToDeletePath);
				}
				catch (System.Exception) {
				}
			}
		}
		#endregion
	}
}

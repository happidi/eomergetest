using System;
using System.Collections.Generic;
using System.IO;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A base class for remote access clients.
	/// </summary>
	public abstract class BaseRemoteFileAccessClient : IRemoteFileAccessClient {
		private const string const_date_time_format = "yyyy'-'MM'-'dd'-'HH'-'mm'-'ss";
		private const string const_test_file_contents = "A test file for permissions. This file can be deleted if it was not removed automatically.";

		/// <summary>
		/// Gets or sets the file source.
		/// </summary>
		/// <value>
		/// The file source.
		/// </value>
		protected IFileSource FileSource { get; set; }

		/// <summary>
		/// Gets or sets the retry interval for remote file access actions, in seconds.
		/// </summary>
		/// <value>
		/// The retry interval.
		/// </value>
		protected int RetryInterval { get; set; }

		/// <summary>
		/// Gets or sets the maximum amount of remote file access actions retries.
		/// </summary>
		/// <value>
		/// The max retries.
		/// </value>
		protected int MaxRetries { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="BaseRemoteFileAccessClient"/> class.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval.</param>
		protected BaseRemoteFileAccessClient(IFileSource fileSource, int maxRetries, int retryInterval) {
			if (fileSource == null) {
				throw new ArgumentNullException("fileSource");
			}

			if (string.IsNullOrWhiteSpace(fileSource.Address)) {
				throw new ArgumentException("Address must have a value.");
			}

			FileSource = fileSource;
			MaxRetries = maxRetries;
			RetryInterval = retryInterval;
		}

		/// <summary>
		/// Determines whether the specified file source has files.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the specified file source has files; otherwise, <c>false</c>.
		/// </returns>
		public abstract bool HasFiles();

		/// <summary>
		/// Downloads the file from the file source to the local destination file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="localDestinationFilename">The local destination filename.</param>
		public abstract void DownloadFile(string filename, string localDestinationFilename);

		/// <summary>
		/// Deletes the specified file from the file source.
		/// </summary>
		/// <param name="filename">The filename.</param>
		public abstract void Delete(string filename);

		/// <summary>
		/// Tests the connection to the file source.
		/// When calling this method, do not call Connect() before.
		/// </summary>
		/// <returns></returns>
		public RemoteFileAccessConnectionTestResult TestConnection() {
			// This test doesn't guarantee that during the job itself we won't have any problems.
			// In fact, the permissions on the file source can change without notice any time.
			// This is just intended for the user who creates the file source to make sure they got their
			// settings correctly, so it would at least work initially.
			var testResult = new RemoteFileAccessConnectionTestResult();

			try {
				Connect();
				testResult.CanConnect = true;
				testResult.Connect = Langue.msg_mapping_task_test_action_succeeded;

				// Checking the list directory permission.
				// This permission usually doesn't change.
				HasFiles();

				string testFilename = string.Format("PermissionsTestFile {0}.tmp", DateTime.UtcNow.ToString(const_date_time_format));

				// Try creating a file and deleting it.
				TestCreateFile(testResult, testFilename);
				TestDeleteFile(testResult, testFilename);

				// Try creating a directory and deleting it.
				try {
					CreateDirectory("test_mapping");
					testResult.CanCreateDirectory = true;
					testResult.CreateDirectory = Langue.msg_mapping_task_test_action_succeeded;

					try {
						DeleteDirectory("test_mapping");
						testResult.CanDeleteDirectory = true;
						testResult.DeleteDirectory = Langue.msg_mapping_task_test_action_succeeded;
					}
					catch (Exception exception) {
						testResult.CanDeleteDirectory = false;
						testResult.DeleteDirectory = exception.Message;
					}
				}
				catch (Exception exception) {
					testResult.CanCreateDirectory = false;
					testResult.CreateDirectory = exception.Message;
				}
			}
			catch (Exception exception) {
				testResult.CanConnect = false;
				testResult.Connect = exception.Message;
			}

			return testResult;
		}

		private void TestDeleteFile(RemoteFileAccessConnectionTestResult testResult, string testFilename) {
			if (!testResult.CanCreateFile) {
				// No use to run this test.
				return;
			}

			if (Exists(testFilename)) {
				try {
					// Checking the delete permission.
					// Users who create file shares / grant FTP permissions / etc, usually forget to give this permission.
					Delete(testFilename);
					testResult.CanDeleteFile = true;
					testResult.DeleteFile = Langue.msg_mapping_task_test_action_succeeded;
				}
				catch (Exception exception) {
					testResult.CanDeleteFile = false;
					testResult.DeleteFile = exception.Message;
				}
			}
			else {
				testResult.CanDeleteFile = false;
				testResult.DeleteFile = "Cannot find expected file, possible file read permission missing.";
			}
		}

		private void TestCreateFile(RemoteFileAccessConnectionTestResult testResult, string testFilename) {
			var memoryStream = new MemoryStream();
			var writer = new StreamWriter(memoryStream);
			writer.Write(const_test_file_contents);
			writer.Flush();
			memoryStream.Seek(0, SeekOrigin.Begin);

			try {
				UploadFile(testFilename, memoryStream);
				testResult.CanCreateFile = true;
				testResult.CreateFile = Langue.msg_mapping_task_test_action_succeeded;
			}
			catch (Exception exception) {
				testResult.CanCreateFile = false;
				testResult.CreateFile = exception.Message;
			}
			finally {
				writer.Close();
				memoryStream.Close();
			}
		}

		/// <summary>
		/// Deletes the directory.
		/// </summary>
		/// <param name="path">The path.</param>
		public abstract void DeleteDirectory(string path);

		/// <summary>
		/// Returns a boolean value indicating whether the file exists or not.
		/// </summary>
		/// <param name="filename">The filename.</param>
		public abstract bool Exists(string filename);

		/// <summary>
		/// Renames the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="newFilename">The new filename.</param>
		public abstract void RenameFile(string filename, string newFilename);

		/// <summary>
		/// Connects to the file source.
		/// </summary>
		public abstract void Connect();

		/// <summary>
		/// Gets the file names sorted by date.
		/// </summary>
		/// <returns></returns>
		public abstract IEnumerable<string> GetFileNamesSortedByDate();

		/// <summary>
		/// Uploads the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="stream">The stream.</param>
		public abstract void UploadFile(string filename, Stream stream);

		/// <summary>
		/// Throws an exception indicating that the file is not found.
		/// </summary>
		/// <param name="fileSource"></param>
		/// <param name="filename">The filename.</param>
		protected static void ThrowOnFileNotFound(IFileSource fileSource, string filename) {
			throw new RemoteFileAccessException(fileSource, filename, "File not found: " + filename);
		}

		/// <summary>
		/// Retries the action if it failed with the given TException.
		/// </summary>
		/// <typeparam name="TResult">The type of the result.</typeparam>
		/// <typeparam name="TException">The type of the exception.</typeparam>
		/// <param name="action">The action.</param>
		/// <returns>The result of the action</returns>
		protected TResult RetryIfFailed<TResult, TException>(Func<TResult> action) where TException : Exception {
			TResult result = Helper.RetryIfFailed<TResult, TException>(MaxRetries, RetryInterval * 1000, action);
			return result;
		}


		/// <summary>
		/// Retries the action if it failed with the given TException.
		/// </summary>
		/// <typeparam name="TException">The type of the exception.</typeparam>
		/// <param name="action">The action.</param>
		protected void RetryIfFailed<TException>(Action action) where TException : Exception {
			RetryIfFailed<object, TException>(() => {
				action();
				return null;
			});
		}


		/// <summary>
		/// Determines whether the filename represents a text file or not.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <returns>
		///   <c>true</c> if the filename represents a text file; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsTextFile(string filename) {
			var extensionsForTextFiles = new List<string> { "csv", "txt", "log", "xml", "xsl" };
			bool isTextFileExtension = false;

			string extension = Path.GetExtension(filename);

			if (!string.IsNullOrWhiteSpace(extension)) {
				string trimmedExtension = extension.TrimStart('.');
				isTextFileExtension = extensionsForTextFiles.Contains(trimmedExtension);
			}

			return isTextFileExtension;
		}


		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public abstract void Dispose();

		/// <summary>
		/// Creates the directory.
		/// </summary>
		/// <param name="path">The path.</param>
		public abstract void CreateDirectory(string path);

		/// <summary>
		/// Deletes directories older than the specified time.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="time">The time.</param>
		public abstract void DeleteOldDirectories(string path, DateTime time);

		/// <summary>
		/// Validates the client settings.
		/// </summary>
		public abstract void Validate();
	}
}
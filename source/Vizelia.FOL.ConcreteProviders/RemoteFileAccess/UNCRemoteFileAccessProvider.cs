﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A remote file access provider for UNC paths.
	/// </summary>
	public class UNCRemoteFileAccessProvider : BaseRemoteFileAccessProvider {
		/// <summary>
		/// Gets the remote file access client.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <returns>The remote file access client for the file source.</returns>
		public override IRemoteFileAccessClient GetClient(IFileSource fileSource) {
			IRemoteFileAccessClient client = Helper.CreateInstance<UNCRemoteFileAccessClient, IRemoteFileAccessClient>(fileSource, MaxRetries, RetryInterval);

			return client;
		}
	}
}
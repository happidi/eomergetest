using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.PolicyInjection;
using Quartz.Xml;
using Rebex.IO;
using Rebex.Net;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A concrete remote file access client that accesses SFTP servers.
	/// </summary>
	public class SFTPRemoteFileAccessClient : RebexRemoteFileAccessClient {
		private Sftp m_SFTPClient;

		/// <summary>
		/// Initializes a new instance of the <see cref="SFTPRemoteFileAccessClient"/> class.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval.</param>
		/// <param name="certificateValidation">if set to <c>true</c> perform certificate validation.</param>
		public SFTPRemoteFileAccessClient(IFileSource fileSource, int maxRetries, int retryInterval, bool certificateValidation)
			: base(fileSource, maxRetries, retryInterval) {
		}

		/// <summary>
		/// Creates the SFTP client.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <returns>
		/// The SFTP client
		/// </returns>
		private Sftp CreateSFTPClient(IFileSource fileSource) {
			var sftp = new Sftp();

			try {
				if (Regex.IsMatch(fileSource.Address, const_port_specified_regex) && !fileSource.Address.ToLower().StartsWith("sftp://")) {
					// A port number was specified but the protocol prefix was not added.
					fileSource.Address = "sftp://" + fileSource.Address;
				}

				// Extract custom port number specified and connect
				Uri connectionUri;

				if (Uri.TryCreate(fileSource.Address, UriKind.Absolute, out connectionUri)) {
					var tlsParameters = new SshParameters { Compression = true};
					sftp.Connect(connectionUri.Host, connectionUri.Port, tlsParameters);
				}
				else {
					sftp.Connect(fileSource.Address);
				}

				// Login using chosen authentication method.
				SshPrivateKey key;

				switch (fileSource.SftpAuthenticationMode) {
					case SftpAuthenticationMode.Password:
						sftp.Login(fileSource.Username, fileSource.Password);
						break;
					case SftpAuthenticationMode.PrivateKey:
						// PKCS #8 or SS-Leay format RSA or DSA private key accepted here.
						key = GetPrivateKey(fileSource);
						sftp.Login(fileSource.Username, key);
						break;
					case SftpAuthenticationMode.Both:
						// PKCS #8 or SS-Leay format RSA or DSA private key accepted here.
						key = GetPrivateKey(fileSource);
						sftp.Login(fileSource.Username, fileSource.Password, key);
						break;
					default:
						throw new ArgumentOutOfRangeException("SftpAuthenticationMode");
				}

				return sftp;
			}
			catch (Exception) {
				sftp.Dispose();
				throw;
			}
		}

		/// <summary>
		/// Gets the private key.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <returns></returns>
		private SshPrivateKey GetPrivateKey(IFileSource fileSource) {
			byte[] data = Encoding.UTF8.GetBytes(fileSource.PrivateKey);
			var key = new SshPrivateKey(data, fileSource.PrivateKeyPassword);

			return key;
		}

		/// <summary>
		/// Validates the client settings.
		/// </summary>
		public override void Validate() {
			if (FileSource.SftpAuthenticationMode == SftpAuthenticationMode.PrivateKey || FileSource.SftpAuthenticationMode == SftpAuthenticationMode.Both) {
				try {
					GetPrivateKey(FileSource);
				}
				catch (Exception exception) {
					var validationResults = new ValidationResults();
					validationResults.AddResult(new ValidationResult(exception.Message, null, "PrivateKey", null, null));
					throw new ArgumentValidationException(validationResults, "item");
				}
			}
		}

		/// <summary>
		/// Downloads the file from the file source to the local destination file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="localDestinationFilename">The local destination filename.</param>
		public override void DownloadFile(string filename, string localDestinationFilename) {
			EnsureConnected();

			if (!m_SFTPClient.FileExists(filename)) {
				ThrowOnFileNotFound(FileSource, filename);
			}

			m_SFTPClient.GetFile(filename, localDestinationFilename);
		}

		/// <summary>
		/// Deletes the specified file from the file source.
		/// </summary>
		/// <param name="filename">The filename.</param>
		public override void Delete(string filename) {
			EnsureConnected();

			m_SFTPClient.DeleteFile(filename);
		}

		/// <summary>
		/// Returns a boolean value indicating whether the file exists or not.
		/// </summary>
		/// <param name="filename">The filename.</param>
		public override bool Exists(string filename) {
			EnsureConnected();

			bool exists = m_SFTPClient.FileExists(filename);

			return exists;
		}

		/// <summary>
		/// Renames the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="newFilename">The new filename.</param>
		public override void RenameFile(string filename, string newFilename) {
			EnsureConnected();

			RetryIfFailed<SftpException>(() =>
				m_SFTPClient.Rename(filename.Replace('\\', '/'), newFilename.Replace('\\', '/')));
		}

		/// <summary>
		/// Ensures that we are connected to the SFTP server.
		/// The connection may have been terminated due to a network error or an inactivity timeout.
		/// </summary>
		protected override void EnsureConnected() {
			RetryIfFailed<Exception>(() => {
				if (!IsConnected()) {
					if (m_SFTPClient != null) {
						m_SFTPClient.Dispose();
						m_SFTPClient = null;
					}

					Connect();
				}

				if (!IsConnected()) {
					throw new RemoteFileAccessException(FileSource, string.Empty, "Reconnected failed.");
				}
			});
		}

		/// <summary>
		/// Gets the file and directory list.
		/// </summary>
		/// <returns></returns>
		protected override FileSystemItemCollection GetList() {
			SftpItemCollection list = m_SFTPClient.GetList();
			return list;
		}

		/// <summary>
		/// Changes the directory.
		/// </summary>
		/// <param name="remotePath">The remote path.</param>
		protected override void ChangeDirectory(string remotePath) {
			m_SFTPClient.ChangeDirectory(remotePath);
		}

		/// <summary>
		/// Creates the directory.
		/// </summary>
		/// <param name="path">The path.</param>
		protected override void CreateDirectoryInternal(string path) {
			m_SFTPClient.CreateDirectory(path);
		}

		/// <summary>
		/// Removes the directory.
		/// </summary>
		/// <param name="remotePath">The remote path.</param>
		protected override void RemoveDirectory(string remotePath) {
			m_SFTPClient.RemoveDirectory(remotePath);
		}

		/// <summary>
		/// Deletes the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		protected override void DeleteFile(string filename) {
			m_SFTPClient.DeleteFile(filename);
		}

		/// <summary>
		/// Directories the exists.
		/// </summary>
		/// <param name="remotePath">The remote path.</param>
		/// <returns></returns>
		protected override bool DirectoryExists(string remotePath) {
			bool exists = m_SFTPClient.DirectoryExists(remotePath);
			return exists;
		}

		/// <summary>
		/// Determines whether this SFTP remote file access is connected to the SFTP server.
		/// </summary>
		/// <returns>
		///   <c>true</c> if this SFTP remote file access is connected to the SFTP server; otherwise, <c>false</c>.
		/// </returns>
		private bool IsConnected() {
			if (m_SFTPClient == null) {
				return false;
			}

			SftpConnectionState state = m_SFTPClient.GetConnectionState();

			bool connected = false;

			if (state.Connected) {
				try {
					m_SFTPClient.Session.KeepAlive(); 
					connected = true;
				}
				catch (SftpException) {
					// Do nothing. 
				}
			}
			return connected;
		}

		/// <summary>
		/// Connects to the file source.
		/// </summary>
		public override void Connect() {
			m_SFTPClient = CreateSFTPClient(FileSource);
		}
		
		/// <summary>
		/// Uploads the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="stream">The stream.</param>
		public override void UploadFile(string filename, Stream stream) {
			string remoteFilename = filename.Replace('\\', '/');

			EnsureConnected();

			RetryIfFailed<SftpException>(() => {
				using (Stream uploadStream = m_SFTPClient.GetUploadStream(remoteFilename)) {
					stream.Seek(0, SeekOrigin.Begin);
					stream.CopyTo(uploadStream);
					uploadStream.Flush();
				}
			});
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public override void Dispose() {
			if (m_SFTPClient != null) {
				m_SFTPClient.Dispose();
				m_SFTPClient = null;
			}
		}

	}
}
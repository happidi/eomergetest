using System;
using System.Collections.Generic;
using System.Linq;
using Rebex.IO;
using Rebex.Net;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A base class for Rebex Remote File Access Clients
	/// </summary>
	public abstract class RebexRemoteFileAccessClient : BaseRemoteFileAccessClient {
		/// <summary>
		/// A regex pattern for checking whether the port was specified or not.
		/// </summary>
		protected const string const_port_specified_regex = "^.*:[0-9]+$";

		/// <summary>
		/// Initializes a new instance of the <see cref="RebexRemoteFileAccessClient"/> class.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval.</param>
		protected RebexRemoteFileAccessClient(IFileSource fileSource, int maxRetries, int retryInterval) : base(fileSource, maxRetries, retryInterval) {
		}

		/// <summary>
		/// Determines whether the specified file source has files.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the specified file source has files; otherwise, <c>false</c>.
		/// </returns>
		public override bool HasFiles() {
			EnsureConnected();

			FileSystemItemCollection itemsList = GetList();
			bool hasFiles = itemsList.Any(item => item.IsFile);

			return hasFiles;
		}

		/// <summary>
		/// Creates the directory.
		/// </summary>
		/// <param name="path">The path.</param>
		public override void CreateDirectory(string path) {
			string remotePath = path.Replace('\\', '/');

			// Some FTP servers software only support creating one subdirectory at a time. We create them gradually.
			string[] directories = remotePath.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

			EnsureConnected();

			RetryIfFailed<NetworkSessionException>(() => {
			                                       	string currentDirectory = string.Empty;

			                                       	foreach (string directory in directories) {
			                                       		currentDirectory += (directory + "/");

			                                       		if (!DirectoryExists(currentDirectory)) {
			                                       			CreateDirectoryInternal(currentDirectory);
			                                       		}
			                                       	}
			                                       });
		}

		/// <summary>
		/// Deletes the directory and all of its contents.
		/// </summary>
		/// <param name="path">remote path.</param>
		public override void DeleteDirectory(string path) {
			string remotePath = path.Replace('\\', '/');

			ChangeDirectory(remotePath);
			// delete all files from the remotePath directory    
			FileSystemItemCollection files = GetList();
			for (int i = 0; i < files.Count; i++) {
				if (files[i].IsFile) {
					DeleteFile(files[i].Name);
				}
			}
			// delete all subdirectories in the remotePath directory    
			for (int i = 0; i < files.Count; i++) {
				if (files[i].IsDirectory) {
					DeleteDirectory(files[i].Name);
				}
			}

			// delete this directory    
			ChangeDirectory("..");
			RemoveDirectory(remotePath);
		}


		/// <summary>
		/// Gets the file names sorted by date.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<string> GetFileNamesSortedByDate() {
			EnsureConnected();

			FileSystemItemCollection itemsList = GetList();
			IEnumerable<FileSystemItem> fileSystemItems = itemsList.Where(item => item.IsFile);
			IEnumerable<string> filesByDate = fileSystemItems.OrderBy(item => item.LastWriteTime).Select(item => item.Name);

			return filesByDate;
		}

		/// <summary>
		/// Deletes directories older than the specified time.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="time">The time.</param>
		public override void DeleteOldDirectories(string path, DateTime time) {
			string remotePath = path.Replace('\\', '/');

			EnsureConnected();

			RetryIfFailed<NetworkSessionException>(() => {
			    if (DirectoryExists(remotePath)) {
			        try {
			            ChangeDirectory(remotePath);

			            FileSystemItemCollection items = GetList();
			            List<FileSystemItem> oldDirectories = items.Where(i => i.IsDirectory && i.LastWriteTime < time).ToList();

			            foreach (FileSystemItem oldDirectory in oldDirectories) {
			                DeleteDirectory(oldDirectory.Name);
			            }

			            ChangeDirectory("/");
			        }
			        catch {
			            try {
			                ChangeDirectory("/");
			            }
			            catch {
			                // Do nothing. 
			            }

			            throw;
			        }
			    }
			    });
		}

		/// <summary>
		/// Ensures connected.
		/// </summary>
		protected abstract void EnsureConnected();

		/// <summary>
		/// Gets the file and directory list.
		/// </summary>
		/// <returns></returns>
		protected abstract FileSystemItemCollection GetList();

		/// <summary>
		/// Changes the directory.
		/// </summary>
		/// <param name="remotePath">The remote path.</param>
		protected abstract void ChangeDirectory(string remotePath);

		/// <summary>
		/// Creates the directory.
		/// </summary>
		/// <param name="path">The path.</param>
		protected abstract void CreateDirectoryInternal(string path);

		/// <summary>
		/// Removes the directory.
		/// </summary>
		/// <param name="remotePath">The remote path.</param>
		protected abstract void RemoveDirectory(string remotePath);

		/// <summary>
		/// Deletes the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		protected abstract void DeleteFile(string filename);

		/// <summary>
		/// Directories the exists.
		/// </summary>
		/// <param name="remotePath">The remote path.</param>
		/// <returns></returns>
		protected abstract bool DirectoryExists(string remotePath);
	}
}
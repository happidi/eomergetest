﻿using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A remote file access provider for SFTP folders.
	/// </summary>
	public class SFTPRemoteFileAccessProvider : BaseRemoteFileAccessProvider {
		private bool m_CertificateValidation;
		private const string const_certificate_validation_attribute_name = "certificateValidation";


		/// <summary>
		/// Gets the remote file access client.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <returns>The remote file access client for the file source.</returns>
		public override IRemoteFileAccessClient GetClient(IFileSource fileSource) {
			IRemoteFileAccessClient client = Helper.CreateInstance<SFTPRemoteFileAccessClient, IRemoteFileAccessClient>(fileSource, MaxRetries, RetryInterval, m_CertificateValidation);

			return client;
		}

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
		/// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
		/// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		/// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.</exception>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			string certificateValidation = config[const_certificate_validation_attribute_name];

			m_CertificateValidation = true; // default. secure.

			if (!string.IsNullOrWhiteSpace(certificateValidation)) {
				// USE THIS FEATURE ONLY FOR DIAGNOSTICS OR TESTING! INSECURE IF FALSE!!!
				m_CertificateValidation = bool.Parse(certificateValidation);
			}
		}
	}
}
﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;

namespace Vizelia.FOL.ConcreteProviders {

	/// <summary>
	/// A helper class for accesing UNC paths with specific credentials.
	/// </summary>
	internal class UNCAccessWithCredentials : IDisposable {

		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		internal struct USE_INFO_2 {
			internal string ui2_local;
			internal string ui2_remote;
			internal string ui2_password;
			internal UInt32 ui2_status;
			internal UInt32 ui2_asg_type;
			internal UInt32 ui2_refcount;
			internal UInt32 ui2_usecount;
			internal string ui2_username;
			internal string ui2_domainname;
		}

		[DllImport("NetApi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
		internal static extern UInt32 NetUseAdd(
			string UncServerName,
			UInt32 Level,
			USE_INFO_2 Buf,
			out UInt32 ParmError);

		[DllImport("NetApi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
		internal static extern UInt32 NetUseDel(
			string UncServerName,
			string UseName,
			UInt32 ForceCond);

		private bool m_IsDisposed;
		private string m_UNCPath;

		/// <summary>
		/// Initializes a new instance of the <see cref="UNCAccessWithCredentials"/> class.
		/// </summary>
		/// <param name="uncPath">The unc path.</param>
		/// <param name="username">The username.</param>
		/// <param name="domain">The domain.</param>
		/// <param name="password">The password.</param>
		public UNCAccessWithCredentials(string uncPath, string username, string domain, string password) {
			Initialize(uncPath, username, domain, password);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="UNCAccessWithCredentials"/> class.
		/// </summary>
		/// <param name="uncPath">The unc path.</param>
		/// <param name="fullDomainUsername">The full domain username.</param>
		/// <param name="password">The password.</param>
		public UNCAccessWithCredentials(string uncPath, string fullDomainUsername, string password) {
			// Isolate username and domain
			if (StringContainsExactlyOneOfChar(fullDomainUsername, '\\')) {
				string[] strings = fullDomainUsername.Split(new[] { '\\' });
				Initialize(uncPath, strings[1], strings[0], password);
			}
			else if (StringContainsExactlyOneOfChar(fullDomainUsername, '@')) {
				string[] strings = fullDomainUsername.Split(new[] { '@' });
				Initialize(uncPath, strings[0], strings[1], password);
			}
			else {
				throw new ArgumentException("username must contain input in format DOMAIN\\username or username@DOMAIN");
			}
		}

		private static bool StringContainsExactlyOneOfChar(string stringToCheck, char charToCheck) {
			return stringToCheck.ToCharArray().Count(c => c.Equals(charToCheck)) == 1;
		}


		private void Initialize(string uncPath, string username, string domain, string password) {
			m_UNCPath = uncPath;

			var useinfo = new USE_INFO_2 {
				ui2_local = null,
				ui2_remote = m_UNCPath,
				ui2_username = username,
				ui2_domainname = domain,
				ui2_password = password,
				ui2_asg_type = 0xFFFFFFFF,
				ui2_usecount = 1
			};

			Connect(useinfo);
		}

		/// <summary>
		/// Connects to the UNC path.
		/// </summary>
		/// <param name="useinfo">The useinfo.</param>
		private void Connect(USE_INFO_2 useinfo) {
			uint paramErrorIndex;
			uint returncode = NetUseAdd(null, 2, useinfo, out paramErrorIndex);

			if (returncode != 0) {
				m_IsDisposed = true;
				throw new Win32Exception((int)returncode);
			}
			
		}

		public void Dispose() {
			if (!m_IsDisposed) {
				NetUseDelete();
			}
			m_IsDisposed = true;
			GC.SuppressFinalize(this);
		}


		/// <summary>
		/// Ends the connection to the remote resource 
		/// </summary>
		/// <returns>True if it succeeds.  Use LastError to get the system error code</returns>
		private void NetUseDelete() {
			uint returncode = NetUseDel(null, m_UNCPath, 2);

			if (returncode != 0) {
				throw new Win32Exception((int)returncode);
			}

			// was once a try-catch here.
			//catch {
			//	iLastError = Marshal.GetLastWin32Error();
			//}
		}

		/// <summary>
		/// Releases unmanaged resources and performs other cleanup operations before the
		/// <see cref="UNCAccessWithCredentials"/> is reclaimed by garbage collection.
		/// </summary>
		~UNCAccessWithCredentials() {
			Dispose();
		}


	}
}
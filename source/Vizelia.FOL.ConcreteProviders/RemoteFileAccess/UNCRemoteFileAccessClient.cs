using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A concrete remote file access client that accesses UNC paths.
	/// </summary>
	public class UNCRemoteFileAccessClient : BaseRemoteFileAccessClient {
		private UNCAccessWithCredentials m_UNCSession;

		/// <summary>
		/// Initializes a new instance of the <see cref="UNCRemoteFileAccessClient"/> class.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval.</param>
		public UNCRemoteFileAccessClient(IFileSource fileSource, int maxRetries, int retryInterval) : base(fileSource, maxRetries, retryInterval) {
			ValidateFileSource(fileSource);
		}


		/// <summary>
		/// Validates the file source.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		private void ValidateFileSource(IFileSource fileSource) {
			if (fileSource.FileSourceType != FileSourceType.UNC) {
				throw new ArgumentException("FileSourceType must be UNC.");
			}

			// The next two checks are to prevent providing a local path on the server and performing malicious file system actions.

			if (fileSource.Address.IndexOf("$") != -1) {
				throw new ArgumentException(@"UNC path may not contain administrative shares with a $ sign.");
			}

			if (!fileSource.Address.StartsWith(@"\\")) {
				throw new ArgumentException(@"Address must be of a UNC path starting with \\");
			}

		}

		/// <summary>
		/// Determines whether the specified file source has files.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the specified file source has files; otherwise, <c>false</c>.
		/// </returns>
		public override bool HasFiles() {
			var directoryInfo = new DirectoryInfo(FileSource.Address);
			bool hasFiles = directoryInfo.EnumerateFiles().Any();

			return hasFiles;
		}

		/// <summary>
		/// Downloads the file from the file source to the local destination file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="localDestinationFilename">The local destination filename.</param>
		public override void DownloadFile(string filename, string localDestinationFilename) {
			string fullFileName = Path.Combine(FileSource.Address, filename);
			var fileToDownload = new FileInfo(fullFileName);

			if (!fileToDownload.Exists) {
				ThrowOnFileNotFound(FileSource, fullFileName);
			}

			fileToDownload.CopyTo(localDestinationFilename);
		}


		/// <summary>
		/// Deletes the specified file from the file source.
		/// </summary>
		/// <param name="filename">The filename.</param>
		public override void Delete(string filename) {
			string fullFilePath = Path.Combine(FileSource.Address, filename);
			var file = new FileInfo(fullFilePath);
			file.Delete();
		}

		/// <summary>
		/// Deletes the directory.
		/// </summary>
		/// <param name="path">The path.</param>
		public override void DeleteDirectory(string path) {
			var directoryInfo = new DirectoryInfo(Path.Combine(FileSource.Address, path));
			directoryInfo.Delete(true);
		}

		/// <summary>
		/// Returns a boolean value indicating whether the file exists or not.
		/// </summary>
		/// <param name="filename">The filename.</param>
		public override bool Exists(string filename) {
			string fullFileName = Path.Combine(FileSource.Address, filename);
			var file = new FileInfo(fullFileName);
			return file.Exists;
		}

		/// <summary>
		/// Renames the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="newFilename">The new filename.</param>
		public override void RenameFile(string filename, string newFilename) {
			string fullFilename = Path.Combine(FileSource.Address, filename);
			var file = new FileInfo(fullFilename);

			string newFullFilename = Path.Combine(FileSource.Address, newFilename);

			RetryIfFailed<IOException>(
				() => file.MoveTo(newFullFilename));

		}

		/// <summary>
		/// Connects to the file source.
		/// </summary>
		public override void Connect() {
			// Create a new UNC authentication session that will last until this object is disposed.
			if (FileSource.UseCustomCredentials) {
				m_UNCSession = new UNCAccessWithCredentials(FileSource.Address, FileSource.Username, FileSource.Password);
			}
		}

		/// <summary>
		/// Gets the file names sorted by date.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<string> GetFileNamesSortedByDate() {
			//TODO: Make sure the whole UNC credentials thing work in windows server 2008 / R2.
			var directoryInfo = new DirectoryInfo(FileSource.Address);
			IEnumerable<string> filesByDate = directoryInfo.EnumerateFiles().OrderBy(file => file.CreationTime).Select(file => file.Name);

			return filesByDate;
		}

		/// <summary>
		/// Uploads the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="stream">The stream.</param>
		public override void UploadFile(string filename, Stream stream) {
			string combinedPath = Path.Combine(FileSource.Address, filename);

			using (var fileStream = new FileStream(combinedPath, FileMode.Create, FileAccess.Write, FileShare.None)) {
				stream.CopyTo(fileStream);
				fileStream.Flush();
			}
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public override void Dispose() {
			if(m_UNCSession != null) {
				m_UNCSession.Dispose();
			}
		}

		/// <summary>
		/// Creates the directory.
		/// </summary>
		/// <param name="path">The path.</param>
		public override void CreateDirectory(string path) {
			string combinedPath = Path.Combine(FileSource.Address, path);
			Directory.CreateDirectory(combinedPath);
		}

		/// <summary>
		/// Deletes directories older than the specified time.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="time">The time.</param>
		public override void DeleteOldDirectories(string path, DateTime time) {
			var directoryInfo = new DirectoryInfo(Path.Combine(FileSource.Address, path));

			DirectoryInfo[] subdirectories = directoryInfo.GetDirectories();
			List<DirectoryInfo> oldDirectories = subdirectories.Where(s => s.CreationTime < time).ToList();

			foreach (DirectoryInfo oldDirectory in oldDirectories) {
				oldDirectory.Delete(true);
			}
		}

		/// <summary>
		/// Validates the client settings.
		/// </summary>
		public override void Validate() {
			// Do nothing.
		}
	}
}
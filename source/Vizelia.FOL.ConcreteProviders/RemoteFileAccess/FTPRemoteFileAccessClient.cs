using System;
using System.IO;
using System.Text.RegularExpressions;
using Rebex.IO;
using Rebex.Net;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A concrete remote file access client that accesses FTP servers.
	/// </summary>
	public class FTPRemoteFileAccessClient : RebexRemoteFileAccessClient {

		private readonly bool m_PerformCertificateValidation;
		private Ftp m_FtpClient;

		/// <summary>
		/// Initializes a new instance of the <see cref="FTPRemoteFileAccessClient"/> class.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval.</param>
		/// <param name="certificateValidation">if set to <c>true</c> perform certificate validation.</param>
		public FTPRemoteFileAccessClient(IFileSource fileSource, int maxRetries, int retryInterval, bool certificateValidation)
			: base(fileSource, maxRetries, retryInterval) {
			m_PerformCertificateValidation = certificateValidation;
		}

		/// <summary>
		/// Creates the FTP client.
		/// </summary>
		/// <param name="fileSource">The file source.</param>
		/// <param name="certificateValidation">if set to <c>true</c> peform certificate validation.</param>
		/// <returns>
		/// The FTP client
		/// </returns>
		private static Ftp CreateFtpClient(IFileSource fileSource, bool certificateValidation) {
			var ftp = new Ftp();

			try {
				// Define security for the connection
				FtpSecurity security = ConvertFtpSecurity(fileSource.FtpSecurityType);
				TlsParameters tlsParameters = GetTlsParameters(fileSource.FtpSecurityType, certificateValidation);

				if (Regex.IsMatch(fileSource.Address, const_port_specified_regex) && !fileSource.Address.ToLower().StartsWith("ftp://")) {
					// A port number was specified but the protocol prefix was not added.
					fileSource.Address = "ftp://" + fileSource.Address;
				}

				Uri connectionUri;

				// Extract custom port number specified and connect
				if (Uri.TryCreate(fileSource.Address, UriKind.Absolute, out connectionUri)) {
					ftp.Connect(connectionUri.Host, connectionUri.Port, tlsParameters, security);
				}
				else {
					ftp.Connect(fileSource.Address, 21, tlsParameters, security);
				}

				// Login
				if (fileSource.UseCustomCredentials) {
					// Use provided credentials
					ftp.Login(fileSource.Username, fileSource.Password);
				}
				else {
					// Anonymous login. 
					ftp.Login(null, null);
				}

				// Switch to compressed file transfer mode if possible.
				// If the server does not support this, we will use the regular file transfer mode.
				// This is relevant to directory listing queries as well.
				ftp.TransferMode = FtpTransferMode.Zlib;

				return ftp;
			}
			catch (Exception) {
				ftp.Dispose();
				throw;
			}
		}

		/// <summary>
		/// Gets the TLS parameters for the FTP connection.
		/// </summary>
		/// <param name="ftpSecurityType">Type of the FTP security.</param>
		/// <param name="certificateValidation">if set to <c>true</c> perform certificate validation.</param>
		private static TlsParameters GetTlsParameters(FtpSecurityType ftpSecurityType, bool certificateValidation) {
			TlsParameters tlsParameters = null;

			if ((ftpSecurityType != FtpSecurityType.None) && (!certificateValidation)) {
				tlsParameters = new TlsParameters { CertificateVerifier = CertificateVerifier.AcceptAll };
			}

			return tlsParameters;
		}

		/// <summary>
		/// Converts the FTP security to a Rebex.Net.FtpSecurity instance.
		/// </summary>
		/// <param name="securityType">Type of the security.</param>
		private static FtpSecurity ConvertFtpSecurity(FtpSecurityType securityType) {
			switch (securityType) {
				case FtpSecurityType.None:
					return FtpSecurity.Unsecure;
				case FtpSecurityType.Implicit:
					return FtpSecurity.Implicit;
				case FtpSecurityType.Explicit:
					return FtpSecurity.Explicit;
				default:
					throw new ArgumentOutOfRangeException("securityType");
			}
		}


		/// <summary>
		/// Downloads the file from the file source to the local destination file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="localDestinationFilename">The local destination filename.</param>
		public override void DownloadFile(string filename, string localDestinationFilename) {
			EnsureConnected();

			if (!m_FtpClient.FileExists(filename)) {
				ThrowOnFileNotFound(FileSource, filename);
			}

			try {
				// Switch to compressed file transfer mode if possible.
				// If the server does not support this, we will use the regular file transfer mode.
				m_FtpClient.TransferMode = IsTextFile(filename) ? FtpTransferMode.Zlib : FtpTransferMode.Stream;

				m_FtpClient.GetFile(filename, localDestinationFilename);
			}
			finally {
				m_FtpClient.TransferMode = FtpTransferMode.Zlib;
			}
		}

		/// <summary>
		/// Deletes the specified file from the file source.
		/// </summary>
		/// <param name="filename">The filename.</param>
		public override void Delete(string filename) {
			EnsureConnected();

			m_FtpClient.DeleteFile(filename);
		}

		/// <summary>
		/// Returns a boolean value indicating whether the file exists or not.
		/// </summary>
		/// <param name="filename">The filename.</param>
		public override bool Exists(string filename) {
			EnsureConnected();

			bool exists = m_FtpClient.FileExists(filename);

			return exists;
		}

		/// <summary>
		/// Renames the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="newFilename">The new filename.</param>
		public override void RenameFile(string filename, string newFilename) {
			EnsureConnected();

			RetryIfFailed<NetworkSessionException>(() =>
				m_FtpClient.Rename(filename.Replace('\\', '/'), newFilename.Replace('\\', '/')));
		}

		/// <summary>
		/// Returns a bool representing whether the directory exists or not.
		/// </summary>
		/// <param name="remotePath">The remote path.</param>
		/// <returns></returns>
		protected override bool DirectoryExists(string remotePath) {
			return m_FtpClient.DirectoryExists(remotePath);
		}

		/// <summary>
		/// Removes the directory.
		/// </summary>
		/// <param name="remotePath">The remote path.</param>
		protected override void RemoveDirectory(string remotePath) {
			m_FtpClient.RemoveDirectory(remotePath);
		}

		/// <summary>
		/// Deletes the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		protected override void DeleteFile(string filename) {
			m_FtpClient.DeleteFile(filename);
		}

		/// <summary>
		/// Changes the directory.
		/// </summary>
		/// <param name="remotePath">The remote path.</param>
		protected override void ChangeDirectory(string remotePath) {
			m_FtpClient.ChangeDirectory(remotePath);
		}

		/// <summary>
		/// Creates the directory internal.
		/// </summary>
		/// <param name="path">The path.</param>
		protected override void CreateDirectoryInternal(string path) {
			m_FtpClient.CreateDirectory(path);
		}

		/// <summary>
		/// Gets the list.
		/// </summary>
		/// <returns></returns>
		protected override FileSystemItemCollection GetList() {
			FtpItemCollection list = m_FtpClient.GetList();
			return list;
		}

		/// <summary>
		/// Ensures that we are connected to the FTP server.
		/// The connection may have been terminated due to a network error or an inactivity timeout.
		/// </summary>
		protected override void EnsureConnected() {
			RetryIfFailed<Exception>(() => {
				if (!IsConnected()) {
					if (m_FtpClient != null) {
						m_FtpClient.Dispose();
						m_FtpClient = null;
					}

					Connect();
				}

				if (!IsConnected()) {
					throw new RemoteFileAccessException(FileSource, string.Empty, "Reconnected failed.");
				}
			});
		}

		/// <summary>
		/// Determines whether this FTP remote file access is connected to the FTP server.
		/// </summary>
		/// <returns>
		///   <c>true</c> if this FTP remote file access is connected to the FTP server; otherwise, <c>false</c>.
		/// </returns>
		private bool IsConnected() {
			if (m_FtpClient == null) {
				return false;
			}

			FtpConnectionState state = m_FtpClient.GetConnectionState();

			bool connected = false;

			if (state.Connected) {
				try {
					m_FtpClient.KeepAlive();
					connected = true;
				}
				catch (NetworkSessionException) {
					// Do nothing. 
				}
			}
			return connected;
		}

		/// <summary>
		/// Connects to the file source.
		/// </summary>
		public override void Connect() {
			m_FtpClient = CreateFtpClient(FileSource, m_PerformCertificateValidation);
		}

		/// <summary>
		/// Uploads the file.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="stream">The stream.</param>
		public override void UploadFile(string filename, Stream stream) {
			string remoteFilename = filename.Replace('\\', '/');

			EnsureConnected();

			RetryIfFailed<NetworkSessionException>(() => {
				using (Stream uploadStream = m_FtpClient.GetUploadStream(remoteFilename)) {
					stream.Seek(0, SeekOrigin.Begin);
					stream.CopyTo(uploadStream);
					uploadStream.Flush();
				}
			});
		}

		/// <summary>
		/// Validates the client settings.
		/// </summary>
		public override void Validate() {
			// Do nothing.
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public override void Dispose() {
			if (m_FtpClient != null) {
				m_FtpClient.Dispose();
				m_FtpClient = null;
			}
		}

	}
}
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A base class for concrete remote file access providers.
	/// </summary>
	public abstract class BaseRemoteFileAccessProvider : RemoteFileAccessProvider {

		private const string const_retry_amount_attribute_name = "retryAmount";
		private const string const_retry_interval_attribute_name = "retryInterval";
		private const int const_retry_interval_default_value = 15;
		private const int const_retry_amount_default_value = 4;

		/// <summary>
		/// Gets or sets the retry interval for remote file access actions, in seconds.
		/// </summary>
		/// <value>
		/// The retry interval in seconds.
		/// </value>
		protected int RetryInterval { get; set; }

		/// <summary>
		/// Gets or sets the maximum amount of remote file access actions retries.
		/// </summary>
		/// <value>
		/// The maximum retries.
		/// </value>
		protected int MaxRetries { get; set; }


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
		/// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
		/// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		/// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.</exception>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			// Populate the RetryInterval property.
			string retryInterval = config[const_retry_interval_attribute_name];
			RetryInterval = !string.IsNullOrWhiteSpace(retryInterval) ? int.Parse(retryInterval) : const_retry_interval_default_value;

			// Populate the MaxRetries property.
			string retryAmount = config[const_retry_amount_attribute_name];
			MaxRetries = !string.IsNullOrWhiteSpace(retryAmount) ? int.Parse(retryAmount) : const_retry_amount_default_value;
		}

	}
}
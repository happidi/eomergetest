﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using Vizelia.FOL.Practices;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A concrete provider for tracing that uses the Enterprise Library Logging Application Block to write the trace.
	/// </summary>
	public class EnterpriseLibraryTracingProvider : TracingProvider {

		private const string const_default_ent_lib_category_name = "Trace";
		private const string const_category_name_attribute_name = "categoryName";
		private const string const_stacks_dictionary_key = "enterpriseLibraryTracingProviderScopesStack";


		private string m_EnterpriseLibraryLoggingCategoryName;

		private static readonly Process m_CurrentProcess = Process.GetCurrentProcess();

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
		/// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
		/// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		/// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.</exception>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			string categoryName = config[const_category_name_attribute_name];

			m_EnterpriseLibraryLoggingCategoryName = !string.IsNullOrWhiteSpace(categoryName) ? categoryName : const_default_ent_lib_category_name;
		}

		/// <summary>
		/// Gets the trace writer for the given tracing category and related entity key.
		/// </summary>
		/// <param name="traceCategory">The trace category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		public override ITraceScope StartTracing(string traceCategory, string relatedKeyEntity) {
			EnterpriseLibraryTraceScope writer = CreateTraceWriter(traceCategory, relatedKeyEntity);

			return writer;
		}

		/// <summary>
		/// Continues tracing from other thread. Only copies the most recent tracing scope. It needs to be disposed of in the original thread.
		/// </summary>
		/// <param name="originalThreadTraceScope">The scope from other thread.</param>
		/// <returns></returns>
		public override ITraceScope ContinueTracing(ITraceScope originalThreadTraceScope) {
			var scope = originalThreadTraceScope as EnterpriseLibraryTraceScope;

			if(scope == null) {
				throw new ArgumentException("originalThreadTraceScope is not of correct type.");
			}

			EnterpriseLibraryTraceScope writer = CreateTraceWriter(scope.Category, scope.RelatedKeyEntity);

			return writer;
		}

		/// <summary>
		/// Writes the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public override void Write(string message) {
			// Use the default severity to write to the log.
			Write(TraceEntrySeverity.Information, message);
		}

		/// <summary>
		/// Writes the specified message with the given severity.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="message">The message.</param>
		public override void Write(TraceEntrySeverity severity, string message) {
			EnterpriseLibraryTraceScope currentScope = GetCurrentScope();

			string category;
			string relatedKeyEntity;

			if (currentScope != null) {
				category = currentScope.Category;
				relatedKeyEntity = currentScope.RelatedKeyEntity;
			}
			else {
				category = "No category";
				relatedKeyEntity = string.Empty;
			}

			WriteInternal(severity, message, category, relatedKeyEntity);
		}

		/// <summary>
		/// Writes the specified message with the given severity.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="message">The message.</param>
		/// <param name="category">The category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		public override void Write(TraceEntrySeverity severity, string message, string category, string relatedKeyEntity) {
			WriteInternal(severity, message, category, relatedKeyEntity);
			Flush();
		}

		/// <summary>
		/// Traces the specified trace category.
		/// </summary>
		/// <typeparam name="T">The function result type</typeparam>
		/// <param name="traceCategory">The trace category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		/// <param name="message">The message.</param>
		/// <param name="action">The action.</param>
		public override T Trace<T>(string traceCategory, string relatedKeyEntity, string message, Func<T> action) {
			Write(TraceEntrySeverity.Information, message + " started.", traceCategory, relatedKeyEntity);

			try {
				T result = action();
				Write(TraceEntrySeverity.Information, message + " ended succesfuly.", traceCategory, relatedKeyEntity);

				return result;
			}
			catch (Exception ex) {
				Write(TraceEntrySeverity.Error, message + " failed: " + ex, traceCategory, relatedKeyEntity);
				throw;
			}
		}

		/// <summary>
		/// Traces the specified action within an existing tracing scope.
		/// Documents begin, end and exception if occures.
		/// </summary>
		/// <typeparam name="T">The function result type</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="action">The action.</param>
		/// <returns></returns>
		public override T Trace<T>(string message, Func<T> action) {
			Write(TraceEntrySeverity.Information, message + " started.");

			try {
				T result = action();
				Write(TraceEntrySeverity.Information, message + " ended succesfuly.");

				return result;
			}
			catch (Exception ex) {
				Write(TraceEntrySeverity.Error, message + " failed:" + ex);
				throw;
			}
		}

		/// <summary>
		/// Writes the specified exception to the trace log.
		/// </summary>
		/// <param name="exception">The exception.</param>
		public override void Write(Exception exception) {
			Write(TraceEntrySeverity.Error, string.Format("An error has occured: {0}", exception));
		}

		/// <summary>
		/// Writes message to the log using the Enterprise Library Logger.
		/// </summary>
		/// <param name="severity">The severity.</param>
		/// <param name="message">The message.</param>
		/// <param name="category">The category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		private void WriteInternal(TraceEntrySeverity severity, string message, string category, string relatedKeyEntity) {
			string callingMethodName = GetCallingMethodName();
			string requestId = ContextHelper.RequestId;

			var properties = new Dictionary<string, object>();

			properties["context"] = new XElement("context",
												 new XElement("traceCategory", category),
												 new XElement("relatedKeyEntity", relatedKeyEntity),
												 new XElement("methodName", callingMethodName),
												 new XElement("requestId", requestId),
												 new XElement("workingSet", GetCurrentProcessWokringSetinKiloBytes()),
												 new XElement("fullMessage", message),
												 new XElement("jobRunInstanceRequestId", ContextHelper.JobRunInstanceRequestId),
												 new XElement("keyJob", ContextHelper.KeyJob));

			TraceEventType enterpriseSeverity = ConvertSeverity(severity);
			var entLibLogCategory = GetEntLibLogCategory(category);
			var logEntry = new VizeliaTraceLogEntry {
				Title = String.Empty,
				Message = message,
				Categories = new List<string> { entLibLogCategory },
				Priority = 1,
				EventId = 1000,
				Severity = enterpriseSeverity,
				ExtendedProperties = properties
			};

			Logger.Write(logEntry);
		}

		/// <summary>
		/// Gets the Enterprise Library log category from the trace category.
		/// </summary>
		/// <param name="traceCategory">The trace category.</param>
		/// <returns></returns>
		private string GetEntLibLogCategory(string traceCategory) {
			string transformedCategory = Helper.TransformLoggingCategory(traceCategory);

			if (transformedCategory == null) {
				transformedCategory = m_EnterpriseLibraryLoggingCategoryName;
			}

			return transformedCategory;
		}


		/// <summary>
		/// Gets the current process wokring setin kilo bytes.
		/// </summary>
		/// <returns></returns>
		private static long GetCurrentProcessWokringSetinKiloBytes() {
			return m_CurrentProcess.WorkingSet64 / 1024;
		}
		/// <summary>
		/// Gets the name of the calling method.
		/// </summary>
		/// <returns></returns>
		private static string GetCallingMethodName() {
            var stackTrace = new StackTrace();
			bool isHot = false; 
            // Climb up the stack trace to find the TracingService method call, and then climb up more
			// to find out the method calling the TracingService.
			for (int i = 1; i < stackTrace.FrameCount; i++) {
				StackFrame stackFrame = stackTrace.GetFrame(i);
				MethodBase method = stackFrame.GetMethod();
				if (method.DeclaringType == typeof(TracingService)) {
					isHot = true;
				}
				else {
					if (isHot) {
						return method.DeclaringType.FullName + ":" + method;
					}
				}
			}
#if DEBUG == false
            return "Release mode";
#endif
            throw new InvalidOperationException("Could not climb up the stack trace out of the TracingService.");
           
		}

		/// <summary>
		/// Converts the severity.
		/// </summary>
		/// <param name="severity">The severity.</param>
		private TraceEventType ConvertSeverity(TraceEntrySeverity severity) {
			switch (severity) {
				case TraceEntrySeverity.Information:
					return TraceEventType.Information;
				case TraceEntrySeverity.Warning:
					return TraceEventType.Warning;
				case TraceEntrySeverity.Error:
					return TraceEventType.Error;
				case TraceEntrySeverity.Verbose:
					return TraceEventType.Verbose;
				default:
					throw new ArgumentOutOfRangeException("severity");
			}
		}

		/// <summary>
		/// Flushes the log.
		/// </summary>
		public override void Flush() {
			Logger.FlushContextItems();
		}

		/// <summary>
		/// Creates the trace writer.
		/// </summary>
		/// <param name="traceCategory">The trace category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		/// <returns></returns>
		private EnterpriseLibraryTraceScope CreateTraceWriter(string traceCategory, string relatedKeyEntity) {
			var scope = new EnterpriseLibraryTraceScope(this, traceCategory, relatedKeyEntity, string.Empty);
			AddTraceScope(scope);

			return scope;
		}

		/// <summary>
		/// Gets the current scope.
		/// </summary>
		/// <returns></returns>
		private EnterpriseLibraryTraceScope GetCurrentScope() {
			Stack<EnterpriseLibraryTraceScope> stack = GetScopesStack();

			if (stack.Count == 0) {
				return null;
			}

			EnterpriseLibraryTraceScope currentScope = stack.Peek();

			return currentScope;
		}

		/// <summary>
		/// Gets the current trace scope. Returns null if no current scope exists.
		/// </summary>
		/// <returns></returns>
		public override ITraceScope GetCurrentTraceScope() {
			EnterpriseLibraryTraceScope scope = GetCurrentScope();
			return scope;
		}

		/// <summary>
		/// Adds the trace scope.
		/// </summary>
		/// <param name="scope">The scope.</param>
		private void AddTraceScope(EnterpriseLibraryTraceScope scope) {
			var stack = GetScopesStack();
			stack.Push(scope);
		}

		/// <summary>
		/// Adds the trace scope.
		/// </summary>
		/// <param name="scope">The scope.</param>
		internal void RemoveTraceScope(EnterpriseLibraryTraceScope scope) {
			var stack = GetScopesStack();

			if (stack.Count == 0) {
				throw new ArgumentException("Stack is empty, cannot remove trace scope.");
			}

			EnterpriseLibraryTraceScope stackTop = stack.Peek();

			if (stackTop != scope) {
				throw new ArgumentException(string.Format("Scope not on top of the stack. Expected category '{0}' and related key entity '{1}', got category '{2}' and related key entity '{3}'.", scope.Category, scope.RelatedKeyEntity, stackTop.Category, stackTop.RelatedKeyEntity));
			}

			stack.Pop();

			if (stack.Count == 0) {
				Flush();
			}
		}

		/// <summary>
		/// Gets the scopes stack.
		/// </summary>
		/// <returns></returns>
		private static Stack<EnterpriseLibraryTraceScope> GetScopesStack() {
			Stack<EnterpriseLibraryTraceScope> stack = ContextHelper.GetStack<EnterpriseLibraryTraceScope>(const_stacks_dictionary_key);

			return stack;
		}

	}
}

using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A concrete trace scope that uses the Enterprise Library Logging Application Block.
	/// </summary>
	internal class EnterpriseLibraryTraceScope : ITraceScope {

		private readonly EnterpriseLibraryTracingProvider m_TracingProvider;

		/// <summary>
		/// Gets or sets the request id.
		/// </summary>
		/// <value>
		/// The request id.
		/// </value>
		public string RequestId { get; set; }

		/// <summary>
		/// Gets or sets the category.
		/// </summary>
		/// <value>
		/// The category.
		/// </value>
		public string Category { get; set; }

		/// <summary>
		/// Gets or sets the related key entity.
		/// </summary>
		/// <value>
		/// The related key entity.
		/// </value>
		public string RelatedKeyEntity { get; set; }


		/// <summary>
		/// Initializes a new instance of the <see cref="EnterpriseLibraryTraceScope"/> class.
		/// </summary>
		/// <param name="tracingProvider">The tracing provider.</param>
		/// <param name="category">The category.</param>
		/// <param name="relatedKeyEntity">The related key entity.</param>
		/// <param name="requestId">The request id.</param>
		public EnterpriseLibraryTraceScope(EnterpriseLibraryTracingProvider tracingProvider, string category, string relatedKeyEntity, string requestId) {
			m_TracingProvider = tracingProvider;
			
			Category = category;
			RelatedKeyEntity = relatedKeyEntity;
			RequestId = requestId;
		}


		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		/// <filterpriority>2</filterpriority>
		public void Dispose() {
			m_TracingProvider.RemoveTraceScope(this);
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Vizelia.FOL.Providers;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.ServiceAgents;
using System.Collections.Specialized;
using System.Configuration.Provider;
using Microsoft.WindowsAzure;
using System.Threading;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a MachineProvider using Azure.
	/// </summary>
	public class AzureMachineProvider : MachineProvider {

		private const string const_attribute_subscription_id = "subscriptionid";
		private const string const_attribute_certificate_thumbprint = "certificatethumbprint";
		private const string const_attribute_storage_account_name = "storageaccountname";
		private const string const_attribute_storage_account_key = "storageaccountkey";
		private const string const_package_name = "Vizelia.FOL.Cloud.Deployment.cspkg";
		private const string const_configuration_name = "ServiceConfiguration.Cloud.cscfg";
		private const string const_attribute_enabled = "enabled";


		/// <summary>
		/// Gets or sets the subscription id.
		/// </summary>
		private string SubscriptionId { get; set; }

		/// <summary>
		/// Gets or sets the certificate thumbprint.
		/// </summary>
		private string CertificateThumbprint { get; set; }

		/// <summary>
		/// Gets or sets the name of the storage account.
		/// </summary>
		private string StorageAccountName { get; set; }

		/// <summary>
		/// Gets or sets the storage account key.
		/// </summary>
		private string StorageAccountKey { get; set; }

		/// <summary>
		/// Gets or sets the Azure machine management.
		/// </summary>
		private AzureServiceAgent m_AzureServiceAgent { get; set; }

		private CloudStorageAccount m_StorageAccount { get; set; }


		private bool Enabled { get; set; }


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, NameValueCollection config) {
			base.Initialize(name, config);
			#region attributes
			if (String.IsNullOrEmpty(config[const_attribute_subscription_id])) {
				throw new ProviderException(const_attribute_subscription_id + " is missing");
			}
			SubscriptionId = config[const_attribute_subscription_id];
			config.Remove(const_attribute_subscription_id);

			if (String.IsNullOrEmpty(config[const_attribute_certificate_thumbprint])) {
				throw new ProviderException(const_attribute_certificate_thumbprint + " is missing");
			}
			CertificateThumbprint = config[const_attribute_certificate_thumbprint];
			config.Remove(const_attribute_certificate_thumbprint);

			if (String.IsNullOrEmpty(config[const_attribute_storage_account_name])) {
				throw new ProviderException(const_attribute_storage_account_name + " is missing");
			}
			StorageAccountName = config[const_attribute_storage_account_name];
			config.Remove(const_attribute_storage_account_name);

			if (String.IsNullOrEmpty(config[const_attribute_storage_account_key])) {
				throw new ProviderException(const_attribute_storage_account_key + " is missing");
			}
			StorageAccountKey = config[const_attribute_storage_account_key];
			config.Remove(const_attribute_storage_account_key);


			if (String.IsNullOrEmpty(config[const_attribute_enabled])) {
				throw new ProviderException(const_attribute_enabled + " is missing");
			}
			Enabled = bool.Parse(config[const_attribute_enabled]);
			config.Remove(const_attribute_enabled);
			#endregion
			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
			if (Enabled) {
				m_AzureServiceAgent = new AzureServiceAgent(SubscriptionId, CertificateThumbprint);
				m_StorageAccount =
					new CloudStorageAccount(new StorageCredentialsAccountAndKey(StorageAccountName, StorageAccountKey), true);
			}
		}

		/// <summary>
		/// Returns the list of current MachineInstance.
		/// </summary>
		/// <returns></returns>
		public override List<MachineInstance> GetAllInstances() {
			if (Enabled) {
				var retVal = CacheService.Get(CacheKey.const_cache_azure_machineinstances, () => {
					var value = m_AzureServiceAgent.GetList();
					return value ?? new List<MachineInstance>();
				});
				return retVal;
			}
			return new List<MachineInstance>();

		}

		/// <summary>
		/// Gets the list of machine for a specific purpose: i.e : Vizelia.FOL.EnergyAggregatorService.
		/// </summary>
		/// <param name="purpose">The purpose.</param>
		/// <returns></returns>
		public override List<MachineInstance> GetList(string purpose) {
			var instances = this.GetAllInstances();
			var retVal = (from i in instances where i.MachinePurpose == purpose select i).ToList();
			return retVal;
		}

		/// <summary>
		/// Restarts the specified machine.
		/// </summary>
		/// <param name="machine">The machine.</param>
		public override void Restart(MachineInstance machine) {
			if (Enabled) {
				string requestId = "";
				m_AzureServiceAgent.Reboot(machine.EnvironmentName, DeploymentSlotName.production, machine.InstanceName,
										   out requestId);
			}
		}

		/// <summary>
		/// Creates a new machine instance.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override MachineInstance Create(MachineInstance item) {
			if (Enabled) {
				string requestId;
				m_AzureServiceAgent.CreateHostedService(item.EnvironmentName, item.EnvironmentName, out requestId);

				while (m_AzureServiceAgent.GetOperationStatusValue(requestId) != "Succeeded") {
					Thread.Sleep(5000);
				}


				string folderName = m_StorageAccount.BlobEndpoint.AbsoluteUri + item.MachinePurpose.ToLower().Replace(".", "") +
									item.Size.ToString().ToLower();
				string packageName = folderName + "/" + const_package_name;
				string configurationFileName = folderName + "/" + const_configuration_name;
				string configurationFileContents = GetStorageContent(configurationFileName);
				m_AzureServiceAgent.CreateDeployment(item.EnvironmentName, packageName, configurationFileContents,
													 item.MachinePurpose, DeploymentSlotName.production, out requestId);
				return item;
			}
			return null;
		}



		/// <summary>
		/// Gets the content of a file stored in the cloud.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <returns></returns>
		private string GetStorageContent(string path) {
			var webclient = new WebClient();
			var retVal = webclient.DownloadString(path);
			return retVal;
		}

		/// <summary>
		/// Deletes the specified machineinstance.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public override bool Delete(MachineInstance item) {
			if (Enabled) {
				string requestId;
				m_AzureServiceAgent.DeleteDeployment(item.EnvironmentName, DeploymentSlotName.production, out requestId);
				while (m_AzureServiceAgent.GetOperationStatusValue(requestId) != "Succeeded") {
					Thread.Sleep(5000);
				}

				m_AzureServiceAgent.DeleteHostedService(item.EnvironmentName);
				return true;
			}
			return false;
		}

		/// <summary>
		/// Determines whether this MachineInstance service is enabled.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the service is enabled; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsEnabled() {
			return Enabled;
		}
	}
}

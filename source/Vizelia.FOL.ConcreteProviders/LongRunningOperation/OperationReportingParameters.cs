using System;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A class that contains the parameters for a long-running operation reporting instance.
	/// </summary>
	internal class OperationReportingParameters {

		/// <summary>
		/// Initializes a new instance of the <see cref="OperationReportingParameters"/> class.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		public OperationReportingParameters(Guid operationId) {
			OperationId = operationId;
			ClearChildOperationParameters();
		}

		/// <summary>
		/// Gets or sets a value indicating whether this instance is prepared for child operation.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is prepared for child operation; otherwise, <c>false</c>.
		/// </value>
		public bool IsPreparedForChildOperation { get; set; }

		/// <summary>
		/// Gets or sets the operation id.
		/// </summary>
		/// <value>
		/// The operation id.
		/// </value>
		public Guid OperationId { get; set; }
		
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		public string Name { get; set; }
		
		/// <summary>
		/// Gets or sets the percent min.
		/// </summary>
		/// <value>
		/// The percent min.
		/// </value>
		public int PercentMin { get; set; }
		
		/// <summary>
		/// Gets or sets the percent max.
		/// </summary>
		/// <value>
		/// The percent max.
		/// </value>
		public int PercentMax { get; set; }

		/// <summary>
		/// Prepares for a child long-running operation.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="percentMin">The percent min.</param>
		/// <param name="percentMax">The percent max.</param>
		public void PrepareForChildOperation(string name, int percentMin, int percentMax) {
			IsPreparedForChildOperation = true;
			Name = name;
			PercentMin = percentMin;
			PercentMax = percentMax;
		}

		/// <summary>
		/// Clears the pending child long-running operation parameters.
		/// </summary>
		public void ClearChildOperationParameters() {
			IsPreparedForChildOperation = false;
			Name = string.Empty;
			PercentMin = 0;
			PercentMax = 100;
		}


		/// <summary>
		/// Adjusts the message.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <returns></returns>
		public string AdjustMessage(string message) {
			string adjustedMessage;

			if (!string.IsNullOrWhiteSpace(Name)) {
				adjustedMessage = Name + ": " + message;
			}
			else {
				adjustedMessage = message;
			}
			return adjustedMessage;
		}

		/// <summary>
		/// Adjusts the percentage to the range.
		/// </summary>
		/// <param name="percentDone">The percent done.</param>
		/// <returns></returns>
		public int AdjustToRange(int percentDone) {
			int adjusted = PercentMin + percentDone * (PercentMax - PercentMin) / 100;
			return adjusted;
		}

	}
}
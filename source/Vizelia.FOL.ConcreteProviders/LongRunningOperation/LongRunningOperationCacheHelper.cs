﻿using System;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A static helper for long polling operations.
	/// </summary>
	internal static class LongRunningOperationCacheHelper {

		private const string operation_cache_key_format = "LongRunningOperation_{0}";

		// This is an arbitraty time that represents a very long running operation in which 
		// no calls are made to the any of the ReportProgress(...) method overloads. 
		// After this time has passed, a blank status will appear in the client UI. 
		// Long-running operation server-side code should update its progress once in a while.
		private const int const_operation_cache_expiration_time = 30;

		/// <summary>
		/// Completes a long polling operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		public static void Complete(Guid operationId) {
			// Nullify the result as the operation is complete.
			UpdatePolling(operationId, LongRunningOperationStatus.Completed, null);
		}

		/// <summary>
		/// Gets the polling operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <returns>
		/// The polling operation.
		/// </returns>
		public static LongRunningOperationState GetPolling(Guid operationId) {
			var operation = LoadOperation(operationId);

			return operation;
		}

		/// <summary>
		/// Gets the result of the operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <returns>The result of the operation.</returns>
		public static object GetResult(Guid operationId) {
			var polling = GetPolling(operationId);
			return polling.Result;
		}

		/// <summary>
		/// Initializes a long polling operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="state">The state.</param>
		public static void Initialize(Guid operationId, object state) {
			UpdatePolling(operationId, LongRunningOperationStatus.Initialized);

			if (state != null) {
				var operation = LoadOperation(operationId);
				operation.ClientState = state;
				SaveOperation(operation);
			}

		}

		/// <summary>
		/// Static function that updates the polling status.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="status">The status of the operation.</param>
		/// <returns></returns>
		public static LongRunningOperationState UpdatePolling(Guid operationId, LongRunningOperationStatus status) {
			return UpdatePolling(operationId, status, null);
		}

		/// <summary>
		/// Static function that updates the polling status.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="status">The status of the operation.</param>
		/// <param name="result">The result.</param>
		/// <returns></returns>
		public static LongRunningOperationState UpdatePolling(Guid operationId, LongRunningOperationStatus status, object result) {
			string label = GenerateLabel(status);
			return UpdatePolling(operationId, 0, status, label, result);
		}

		/// <summary>
		/// Generates the label by the status.
		/// </summary>
		/// <param name="status">The status.</param>
		/// <returns></returns>
		private static string GenerateLabel(LongRunningOperationStatus status) {
			string label = string.Empty;
			try {
				label = Langue.ResourceManager.GetString("msg_pollingoperationstatus_" + status.ParseAsString().ToLower());
			}
			catch { }
			return label;
		}


		/// <summary>
		/// Static function that updates the polling.
		/// Sets the label as the Exception message.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="exception">The exception.</param>
		public static LongRunningOperationState UpdatePolling(Guid operationId, Exception exception) {
			return UpdatePolling(operationId, exception, false);
		}

		/// <summary>
		/// Static function that updates the polling.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="exception">The exception.</param>
		/// <param name="exceptionDetailsInLabel">if set to <c>true</c>, sets the label to contain full exception details.</param>
		public static LongRunningOperationState UpdatePolling(Guid operationId, Exception exception, bool exceptionDetailsInLabel) {
			string label = exceptionDetailsInLabel ? exception.ToString() : exception.Message;
			return UpdatePolling(operationId, 0, LongRunningOperationStatus.Exception, label);
		}

		/// <summary>
		/// Static function that updates the polling.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="percentDone">The percent done.</param>
		/// <param name="status">The status.</param>
		public static LongRunningOperationState UpdatePolling(Guid operationId, int percentDone, LongRunningOperationStatus status) {
			string label = GenerateLabel(status);
			return UpdatePolling(operationId, percentDone, status, label, null);
		}

		/// <summary>
		/// Static function that updates the polling.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="percentDone">The percent done.</param>
		/// <param name="status">The status.</param>
		/// <param name="label">The label.</param>
		/// <returns></returns>
		public static LongRunningOperationState UpdatePolling(Guid operationId, int percentDone, LongRunningOperationStatus status, string label) {
			return UpdatePolling(operationId, percentDone, status, label, null);
		}

		/// <summary>
		/// Static function that updates the polling status.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="percentDone">The percent done.</param>
		/// <param name="status">The status of the operation.</param>
		/// <param name="label">The label of the operation.</param>
		/// <param name="result">The result.</param>
		/// <returns></returns>
		public static LongRunningOperationState UpdatePolling(Guid operationId, int percentDone, LongRunningOperationStatus status, string label, object result) {
			ValidateOperationId(operationId);

			if (percentDone < 0)
				percentDone = 0;
			if (percentDone > 100)
				percentDone = 100;
			if (String.IsNullOrEmpty(label))
				label = string.Empty;

			var pollingStatus = LoadOperation(operationId);

			pollingStatus.PercentDone = percentDone;
			pollingStatus.Status = status;
			pollingStatus.Label = label;

			// Don't overwrite existing result with null.
			if (result != null) {
                pollingStatus.Result = result;
			}

			SaveOperation(pollingStatus);

			return pollingStatus;
		}

		/// <summary>
		/// Validates the operation id.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		private static void ValidateOperationId(Guid operationId) {
			if(operationId.Equals(Guid.Empty)) {
				throw new ArgumentException("operationId cannot be an empty Guid!");
			}
		}

		/// <summary>
		/// Updates the polling with the client request.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="clientRequest">The client request.</param>
		public static void UpdatePolling(Guid operationId, LongRunningOperationClientRequest clientRequest) {
			var operation = LoadOperation(operationId);
			operation.ClientRequest = clientRequest;
			SaveOperation(operation);
		}

		private static string GenerateCacheKey(Guid operationId) {
			string key = string.Format(operation_cache_key_format, operationId.ToString().ToLower());
			return key;
		}

		/// <summary>
		/// Loads the operation from the cache.
		/// If it doesn't exist, a new one is created with Initialized status.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <returns></returns>
		private static LongRunningOperationState LoadOperation(Guid operationId) {
			string key = GenerateCacheKey(operationId);
			LongRunningOperationState cachedObject = CacheService.GetData(key) as LongRunningOperationState;
			LongRunningOperationState operation = cachedObject ?? new LongRunningOperationState(operationId);
			return operation;
		}

		/// <summary>
		/// Saves the operation to the cache.
		/// </summary>
		/// <param name="operation">The operation.</param>
		private static void SaveOperation(LongRunningOperationState operation) {
			string key = GenerateCacheKey(operation.Id);
			CacheService.Add(key, operation, const_operation_cache_expiration_time);

  		}

	}
}

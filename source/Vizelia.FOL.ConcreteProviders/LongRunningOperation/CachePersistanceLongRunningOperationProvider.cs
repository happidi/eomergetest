﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A concrete long-running operation provider.
	/// </summary>
	public class CachePersistanceLongRunningOperationProvider : LongRunningOperationProvider {
		private const string const_show_exception_details_in_label_attribute = "showExceptionDetailsInLabel";
		private const string const_reporting_stack_key = "long_running_operation_reporting_stack";

		private bool m_ShowExceptionDetailsInLabel;

		/// <summary>
		/// Initializes a new instance of the <see cref="CachePersistanceLongRunningOperationProvider"/> class.
		/// </summary>
		public CachePersistanceLongRunningOperationProvider() {
			ClearChildOperationParameters();
		}

		/// <summary>
		/// Clears the next child operation parameters.
		/// </summary>
		public override void ClearChildOperationParameters() {
			Stack<OperationReportingParameters> stack = GetReportingStack();
			if (!stack.Any()) return;

			OperationReportingParameters parameters = stack.Peek();
			parameters.ClearChildOperationParameters();
		}

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
		/// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
		/// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		/// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.</exception>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			string value = config[const_show_exception_details_in_label_attribute];

			if (!string.IsNullOrWhiteSpace(value)) {
				m_ShowExceptionDetailsInLabel = bool.Parse(value);
			}
			else {
				m_ShowExceptionDetailsInLabel = false; // default value.
			}
		}

		/// <summary>
		/// Gets the operationId from the incoming HTTP request.
		/// </summary>
		/// <returns>The id of the operation.</returns>
		public override Guid GetOperationIdFromRequest() {
			string guidValue = HttpContext.Current.Request.Form["operationId"];
			return new Guid(guidValue);
		}

		/// <summary>
		/// Invokes the specified long running operation with the given operation ID.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="state">The state.</param>
		/// <param name="workFunction">The work function.</param>
		public override void Invoke(Guid operationId, object state, Func<LongRunningOperationResult> workFunction) {
			LongRunningOperationCacheHelper.Initialize(operationId, state);
			Stack<OperationReportingParameters> stack = GetReportingStack();

			try {
				var parameters = new OperationReportingParameters(operationId);
				stack.Push(parameters);

				var result = workFunction();
				HandleResult(operationId, result);
			}
			catch (Exception exception) {
				LongRunningOperationCacheHelper.UpdatePolling(operationId, exception, m_ShowExceptionDetailsInLabel);
				throw;
			}
			finally {
				stack.Pop();
				ClearChildOperationParameters();
			}
		}

		/// <summary>
		/// Invokes the specified long running operation with the given operation ID.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="workFunction">The work function.</param>
		public override void Invoke(Guid operationId, Func<LongRunningOperationResult> workFunction) {
			Invoke(operationId, null, workFunction);
		}

		/// <summary>
		/// Gets the operation state.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <returns></returns>
		public override LongRunningOperationState GetOperationState(Guid operationId) {
			var progress = LongRunningOperationCacheHelper.GetPolling(operationId);
			return progress;
		}

		/// <summary>
		/// Updates the operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="clientRequest">The client request.</param>
		public override void UpdateOperation(Guid operationId, LongRunningOperationClientRequest clientRequest) {
			LongRunningOperationCacheHelper.UpdatePolling(operationId, clientRequest);
		}

		/// <summary>
		/// Completes the specified operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		public override void Complete(Guid operationId) {
			LongRunningOperationCacheHelper.Complete(operationId);
		}

		/// <summary>
		/// Prepares for a child long-running operation that is about to be started.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="percentMin">The percent min.</param>
		/// <param name="percentMax">The percent max.</param>
		public override void PrepareForChildOperation(string name, int percentMin, int percentMax) {
			if (percentMin < 0) {
				throw new ArgumentOutOfRangeException("percentMin");
			}

			if (percentMax > 100) {
				throw new ArgumentOutOfRangeException("percentMax");
			}

			Stack<OperationReportingParameters> stack = GetReportingStack();
			if (!stack.Any()) return;

			OperationReportingParameters parameters = stack.Peek();
			parameters.PrepareForChildOperation(name, percentMin, percentMax);
		}

		/// <summary>
		/// Gets the reporting stack.
		/// </summary>
		/// <returns></returns>
		private Stack<OperationReportingParameters> GetReportingStack() {
			Stack<OperationReportingParameters> stack = ContextHelper.GetStack<OperationReportingParameters>(const_reporting_stack_key);

			return stack;
		}

		/// <summary>
		/// Handles the result of the long-running operation.
		/// </summary>
		/// <param name="operationId">The operation id.</param>
		/// <param name="result">The result.</param>
		private static void HandleResult(Guid operationId, LongRunningOperationResult result) {
			if (result == null) {
				throw new InvalidOperationException("Invocation result cannot be null");
			}

			// Should we email the result or store it in the session for client fetching?
			var operation = LongRunningOperationCacheHelper.GetPolling(operationId);


			if (result.Status == LongRunningOperationStatus.ResultAvailable &&
				operation.ClientRequest == LongRunningOperationClientRequest.EmailResult) {
				EmailResult(result.Output);
				LongRunningOperationCacheHelper.UpdatePolling(operationId, LongRunningOperationStatus.Completed);
			}
			else {
				LongRunningOperationCacheHelper.UpdatePolling(operationId, result.Status, result.Output);
			}
		}

		private static void EmailResult(object resultObject) {
			var result = resultObject as StreamResult;
			if (result == null) {
				throw new InvalidOperationException("Cannot send e-mail: Result must be of type StreamResult in order to be used as attachment.");
			}
			var currentUser = Helper.GetCurrentFOLMembershipUser();
			if (currentUser == null) {
				throw new InvalidOperationException("Cannot send e-mail: Cannot determine current user.");
			}

			if (string.IsNullOrEmpty(currentUser.Email)) {
				throw new InvalidOperationException("Cannot send e-mail: No email specified for current user's occupant.");
			}

			string userEmailAddress = currentUser.Email;

			EmailResult(userEmailAddress, result);
		}

		private static void EmailResult(string userEmailAddress, StreamResult result) {
			var mailBroker = Helper.CreateInstance<MailBroker>();
			var mails = mailBroker.GetListByClassificationItemLocalId(MailBroker.const_longrunningoperationresult);
			var mailMessages = MailService.ConvertMailsToMailMessages(new ModelDataLongRunningOperationResult { UserEmailAddress = userEmailAddress }, mails);
			foreach (var mailMessage in mailMessages) {
				mailMessage.Attachments.Add(new Attachment(result.ContentStream, result.FullFileName, result.MimeType));
				MailService.Send(mailMessage);
			}
		}


		/// <summary>
		/// Reports that the operation is in progress.
		/// </summary>
		public override void ReportProgress() {
			IEnumerable<OperationReportingParameters> parametersForUpdatableAncestorOperations = GetParametersForUpdatableAncestorOperations();

			foreach (var parameters in parametersForUpdatableAncestorOperations) {
				LongRunningOperationCacheHelper.UpdatePolling(parameters.OperationId, parameters.PercentMin, LongRunningOperationStatus.InProgress);
			}
		}

		/// <summary>
		/// Reports the progress.
		/// </summary>
		/// <param name="percentDone">The percent done.</param>
		public override void ReportProgress(int percentDone) {
			IEnumerable<OperationReportingParameters> parametersForUpdatableAncestorOperations = GetParametersForUpdatableAncestorOperations();
			int adjustedProgress = percentDone;

			foreach (var parameters in parametersForUpdatableAncestorOperations) {
				adjustedProgress = parameters.AdjustToRange(adjustedProgress);
				LongRunningOperationCacheHelper.UpdatePolling(parameters.OperationId, adjustedProgress, LongRunningOperationStatus.InProgress);
			}
		}

		/// <summary>
		/// Reports the progress.
		/// </summary>
		/// <param name="percentDone">The percent done.</param>
		/// <param name="message">The message.</param>
		public override void ReportProgress(int percentDone, string message) {
			IEnumerable<OperationReportingParameters> parametersForUpdatableAncestorOperations = GetParametersForUpdatableAncestorOperations();
			int adjustedProgress = percentDone;
			string adjustedMessage = message;

			foreach (var parameters in parametersForUpdatableAncestorOperations) {
				adjustedProgress = parameters.AdjustToRange(adjustedProgress);
				adjustedMessage = parameters.AdjustMessage(adjustedMessage);
				LongRunningOperationCacheHelper.UpdatePolling(parameters.OperationId, adjustedProgress, LongRunningOperationStatus.InProgress, adjustedMessage);
			}
		}

		/// <summary>
		/// Determines whether cancelation is pending for this operation or not.
		/// </summary>
		/// <returns>
		///   <c>true</c> if cancelation is pending for this operation; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsCancelationPending() {
			Stack<OperationReportingParameters> stack = GetReportingStack();
			if (stack.Count == 0) return false;

			// Cancelation is only checked at current level, not above.
			OperationReportingParameters parameters = stack.Peek();
			LongRunningOperationState state = LongRunningOperationCacheHelper.GetPolling(parameters.OperationId);
			bool isCancelationPending = state.ClientRequest == LongRunningOperationClientRequest.Cancel;

			return isCancelationPending;
		}

		/// <summary>
		/// Gets the current operation id.
		/// </summary>
		/// <returns></returns>
		public override Guid? GetCurrentOperationId() {
			Stack<OperationReportingParameters> stack = GetReportingStack();

			if (stack.Count == 0) {
				return null;
			}

			var reportingParameters = stack.Peek();

			return reportingParameters.OperationId;
		}

		/// <summary>
		/// Gets the parameters for updatable ancestor operations.
		/// These operations are either the current operation or operations up the hierarchy until we reach one that doesn't care for
		/// our updates, meaning IsPreparedForChildoperation = false.
		/// </summary>
		/// <returns></returns>
		private IEnumerable<OperationReportingParameters> GetParametersForUpdatableAncestorOperations() {
			Stack<OperationReportingParameters> stack = GetReportingStack();

			if (stack.Count == 0) {
				// In case there's no long running operation in the context, don't report progress.
				return Enumerable.Empty<OperationReportingParameters>();
			}

			OperationReportingParameters me = stack.Peek();

			IEnumerable<OperationReportingParameters> parametersForUpdatableAncestorOperations =
				stack.TakeWhile(p => p == me || p.IsPreparedForChildOperation);

			return parametersForUpdatableAncestorOperations;
		}

	}
}

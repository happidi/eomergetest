﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Linq;
using System.Web.Security;
using Microsoft.Web.Administration;
using Vizelia.FOL.Providers;
using System.Collections.Specialized;
using Vizelia.FOL.BusinessEntities;
using Site = Microsoft.Web.Administration.Site;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Provision a tenant on IIS 7.5 as a separate web site.
	/// </summary>
	public class IIS7IsolatedTenantProvisioningProvider : TenantProvisioningProvider {
		private const string const_default_tenant_identifier = "FacilityOnLine"; // this string will be replaced with the new tenant name
		private const string const_default_tenant_url = "http://localhost/Vizelia.FOL.Web.Dev"; // string will be replaced with the new tenant URL
        private const string const_default_energyaggregator_url = "http://FacilityOnLine_ea";//"net.tcp://localhost/"; // this will be replaced with the url of the ea for the tenant
		private const string const_attribute_certificate_path = "sslCertificatePath"; //e.g. "c:\cert.ssl";
		private const string const_attribute_certificate_password = "sslCertificatePassword";
		private const string const_attribute_default_customers_directory = "defaultCustomersDirectory"; // e.g. c:\v2customers
		private const string const_attribute_shared_application_pool_prefix = "sharedFrontendApplicationPoolPrefix"; // e.g. "V2SharedFrontend_";
		private const string const_attribute_isolated_application_pool_prefix = "isolatedFrontendApplicationPoolPrefix"; // e.g. "V2Frontend_";
		private const string const_attribute_energyaggregator_prefix = "energyaggregatorPrefix"; // e.g. ea_
		private const string const_attribute_energyaggregator_protocol = "energyaggregatorProtocol"; // e.g. ea_
		private const string const_attribute_frontend_binaries_zip_path = "frontendBinariesZipPath"; //e.g. c:\vizelia.fol.web.zip
		private const string const_attribute_energyaggregator_zip_path = "energyAggregatorBinariesZipPath"; //e.g. c:\vizelia.fol.energyaggregator.zip"
		private const string const_attribute_frontend_directorybase_path = "frontendDirectoryBasePath"; // e.g. "c:\v2binaries\vizelia.fol.web.dev"
		private const string const_attribute_energyaggregator_directorybase_path = "energyAggregatorDirectoryBasePath"; // e.g. "c:\v2binaries\vizelia.fol.energyaggregator"
		private const string const_attribute_frontend_shared_directories = "frontendSharedDirectories"; // e.g. bin,clientbin,flash
		private const string const_attribute_energyaggregator_shared_directories = "energyaggregatorSharedDirectories"; //e.g. bin,clientbin,flash
		private const string const_attribute_amount_of_tenants_per_applicationpool = "amountOfTenantsPerApplicationPool";

		// 

		/// <summary>
		/// Gets or sets the limit of tenants per application pool.
		/// </summary>
		/// <value>
		/// The shared tenants application pool limit.
		/// </value>
		public int SharedTenantsApplicationPoolLimit { get; set; }
		
		/// <summary>
		/// Gets or sets the frontend binaries directory.
		/// </summary>
		/// <value>
		/// The frontend binaries directory.
		/// </value>
		public string FrontendBinariesZipPath { get; set; }
		/// <summary>
		/// Gets or sets the EnergyAggregato binaries directory.
		/// </summary>
		/// <value>
		/// The EnergyAggregato binaries directory.
		/// </value>
		public string EnergyAggregatorBinariesZipPath { get; set; }
		/// <summary>
		/// Gets or sets the frontend application pool prefix.
		/// </summary>
		/// <value>
		/// The frontend application pool prefix.
		/// </value>
		public string SharedFrontendApplicationPoolPrefix { get; set; }
		/// <summary>
		/// Gets or sets the isolated frontend application pool prefix.
		/// </summary>
		/// <value>
		/// The isolated frontend application pool prefix.
		/// </value>
		public string IsolatedFrontendApplicationPoolPrefix { get; set; }
		/// <summary>
		/// Gets or sets the energy aggregator application pool prefix.
		/// </summary>
		/// <value>
		/// The aggregator application pool prefix.
		/// </value>
		public string EnergyAggregatorPrefix { get; set; }
		/// <summary>
		/// Gets or sets the SSL certificate Path
		/// </summary>
		/// <value>
		/// The SSL certificate Path.
		/// </value>
		public string SslCertificatePath { get; set; }
		/// <summary>
		/// Gets or sets the SSL certificate Password
		/// </summary>
		/// <value>
		/// The SSL certificate Password.
		/// </value>
		public string SslCertificatePassword { get; set; }
		/// <summary>
		/// Gets or sets the default customers directory.
		/// </summary>
		/// <value>
		/// The default customers directory.
		/// </value>
		public string DefaultCustomersDirectory { get; set; }
		/// <summary>
		/// Gets or sets the tenant shared directories.
		/// </summary>
		/// <value>
		/// The tenant shared directories.
		/// </value>
		public List<string> FrontendSharedDirectories { get; set; }
		/// <summary>
		/// Gets or sets the tenant shared directories.
		/// </summary>
		/// <value>
		/// The tenant shared directories.
		/// </value>
		public List<string> EnergyAggregatorSharedDirectories { get; set; }
		/// <summary>
		/// Gets or sets the frontend directory base path.
		/// </summary>
		/// <value>
		/// The frontend directory base path.
		/// </value>
		public string FrontendDirectoryBasePath { get; set; }

		/// <summary>
		/// Gets or sets the energy aggregator directory base path.
		/// </summary>
		/// <value>
		/// The energy aggregator directory base path.
		/// </value>
		public string EnergyAggregatorDirectoryBasePath { get; set; }

		/// <summary>
		/// Gets or sets the energy aggregator protocol.
		/// </summary>
		/// <value>
		/// The energy aggregator protocol.
		/// </value>
		public ServiceInvocationProtocol EnergyAggregatorProtocol { get; set; }

		private NameValueCollection _config;
		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
		/// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
		///   
		/// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		///   
		/// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.</exception>
		public override void Initialize(string name, NameValueCollection config) {
			base.Initialize(name, config);
			this._config = config;
			SslCertificatePath = GetConfig(const_attribute_certificate_path, false);
			SslCertificatePassword = GetConfig(const_attribute_certificate_password, false);
			DefaultCustomersDirectory = GetConfig(const_attribute_default_customers_directory, false);
			SharedFrontendApplicationPoolPrefix = GetConfig(const_attribute_shared_application_pool_prefix, false);
			IsolatedFrontendApplicationPoolPrefix = GetConfig(const_attribute_isolated_application_pool_prefix, false);
			FrontendBinariesZipPath = GetConfig(const_attribute_frontend_binaries_zip_path, false);
			EnergyAggregatorBinariesZipPath = GetConfig(const_attribute_energyaggregator_zip_path, false);
			EnergyAggregatorPrefix = GetConfig(const_attribute_energyaggregator_prefix, false);
			FrontendDirectoryBasePath = GetConfig(const_attribute_frontend_directorybase_path, false);
			EnergyAggregatorDirectoryBasePath = GetConfig(const_attribute_energyaggregator_directorybase_path, false);
			FrontendSharedDirectories = GetConfig(const_attribute_frontend_shared_directories, false, false) == null ? null : GetConfig(const_attribute_frontend_shared_directories, false).Split(',').ToList();
			EnergyAggregatorSharedDirectories = GetConfig(const_attribute_energyaggregator_shared_directories, false,false) == null ? null : GetConfig(const_attribute_energyaggregator_shared_directories, false) .Split(',').ToList();
			SharedTenantsApplicationPoolLimit = GetConfig(const_attribute_amount_of_tenants_per_applicationpool, false,false) == null ? 0 : Int32.Parse(GetConfig(const_attribute_amount_of_tenants_per_applicationpool, false));
			EnergyAggregatorProtocol = GetConfig(const_attribute_energyaggregator_protocol, false).ParseAsEnum<ServiceInvocationProtocol>();
			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}

		/// <summary>
		/// Gets the config.
		/// </summary>
		/// <param name="attribute">The attribute.</param>
		/// <param name="mandatory">if set to <c>true</c> [mandatory].</param>
		/// <param name="remove">if set to <c>true</c> we remove the attribute from the collection.</param>
		/// <returns></returns>
		private string GetConfig(string attribute, bool mandatory, bool remove = true) {
			if (mandatory && String.IsNullOrEmpty(_config[attribute])) {
				throw new ProviderException(attribute + " is missing");
			}
			var retval = _config[attribute];
			if (remove)
				_config.Remove(attribute);
			return retval;
		}

		/// <summary>
		/// Creates a new business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public override Tenant CreateTenant(Tenant item) {
			return item;
			/*
			Tenant energyAggregatorTenant = GetEnergyAggregatorTenant(item);
			Tenant tenant = CreateFrontend(item, energyAggregatorTenant);
			CreateEnergyAggregator(item, energyAggregatorTenant);
			return tenant;
			*/
		}

		/// <summary>
		/// Creates the frontend.
		/// </summary>
		/// <param name="frontendTenant">The frontend tenant.</param>
		/// <param name="energyAggregatorTenant">The energy aggregator tenant.</param>
		/// <param name="iisSettingsOnly">if set to <c>true</c> this method only creates IIS settings and not physical files on disk.</param>
		/// <returns></returns>
		private Tenant CreateFrontend(Tenant frontendTenant, Tenant energyAggregatorTenant, bool iisSettingsOnly = false) {
			var retval = new Tenant();

			using (ServerManager serverManager = new ServerManager()) {
				// 1. create the directory on disk
				string path = Path.Combine(DefaultCustomersDirectory, frontendTenant.Name);
				if (!iisSettingsOnly) {
					Directory.CreateDirectory(path);
				}
				// 2. create the application pool
				ApplicationPool applicationPool;
				if (frontendTenant.IsIsolated) {
					applicationPool = serverManager.ApplicationPools.Add(EncodeIsolatedApplicationPoolName(frontendTenant.Name));
					applicationPool.ManagedRuntimeVersion = "v4.0";
					applicationPool["startMode"] = "AlwaysRunning";
					serverManager.CommitChanges();
				}
				else {
					// find an available application pool
					string applicationPoolName = GetAvailableSharedApplicationPoolName();
					applicationPool = serverManager.ApplicationPools[applicationPoolName];
					applicationPool.ManagedRuntimeVersion = "v4.0";
					applicationPool["startMode"] = "AlwaysRunning";
					serverManager.CommitChanges();
				}

				// 3. create the http website
				Site site = serverManager.Sites[frontendTenant.Name];
				site = serverManager.Sites.Add(frontendTenant.Name, "http", "*:80:" + frontendTenant.Url, path);
				retval.Name = frontendTenant.Name;
				retval.Url = site.Bindings[0].Host;
				site.ApplicationDefaults.ApplicationPoolName = applicationPool.Name;
				serverManager.CommitChanges();


				// 4. create the https endpoint
				site = serverManager.Sites[frontendTenant.Name];
				X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
				store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadWrite);
				X509Certificate2 certificate = new X509Certificate2(this.SslCertificatePath, this.SslCertificatePassword);
				store.Add(certificate);
				var binding = site.Bindings.Add("*:443:" + frontendTenant.Url, certificate.GetCertHash(), store.Name);
				binding.Protocol = "https";
				store.Close();
				serverManager.CommitChanges();

				if (!iisSettingsOnly) {
					// 5. Option, copy the files and set them to non read only
					CompressionHelper.Extract(this.FrontendBinariesZipPath, path, true);
					SetNonReadOnly(path);

					// 6. Replace constant facility online with TenantName
					string webconfigPath = Path.Combine(path, "web.config");
					ReplaceTenantNameAndUrls(frontendTenant, energyAggregatorTenant, webconfigPath);


					// 7. link the shared directories
					foreach (var sharedDirectory in FrontendSharedDirectories) {
						string logicalDirectory = Path.Combine(path, sharedDirectory);
						string actualFilesDirectory = Path.Combine(this.FrontendDirectoryBasePath, sharedDirectory);
						// we create junction only if the logical directory does not exist
						if (!Directory.Exists(logicalDirectory)) {
							JunctionPoint.Create(logicalDirectory, actualFilesDirectory, true);
						}
					}
				}
				// 8. Start! Set the auto start provider
				var application = serverManager.Sites[frontendTenant.Name].Applications["/"];
				application.Attributes["serviceAutoStartEnabled"].Value = "true";
				application.Attributes["serviceAutoStartProvider"].Value = "Vizelia Frontend AutoStartProvider";
				serverManager.CommitChanges();

			}
			return retval;
		}

		/// <summary>
		/// Replaces the tenant name and urls in the web.config which its full path is specified.
		/// Replaces the tenant name, tenant URL and energy aggregator URL in the web.config file.
		/// </summary>
		/// <param name="frontendTenant">The frontend tenant.</param>
		/// <param name="energyAggregatorTenant">The energy aggregator tenant.</param>
		/// <param name="webconfigPath">The webconfig path.</param>
		private void ReplaceTenantNameAndUrls(Tenant frontendTenant, Tenant energyAggregatorTenant, string webconfigPath) {

			string energyAggregatorProtocolPrefix = "";

			switch (EnergyAggregatorProtocol) {
				case ServiceInvocationProtocol.WCFHTTP:
					energyAggregatorProtocolPrefix = "http://";
					break;
				case ServiceInvocationProtocol.WCFTCP:
					energyAggregatorProtocolPrefix = "net.tcp://";
					break;
			}


			string energyAggregatorUrl =
				(energyAggregatorTenant.Url.ToLower().StartsWith(energyAggregatorProtocolPrefix) ? "" : energyAggregatorProtocolPrefix) +
				energyAggregatorTenant.Url.TrimEnd('/') +
				(const_default_energyaggregator_url.EndsWith("/") ? "/" : "");

			string frontendUrl =
				(frontendTenant.Url.ToLower().StartsWith("http://") ? "" : "http://") +
				frontendTenant.Url.TrimEnd('/') +
				(const_default_tenant_url.EndsWith("/") ? "/" : "");

			string output = File.ReadAllText(webconfigPath)
				.Replace(const_default_energyaggregator_url, energyAggregatorUrl) // EA tenant URL
				.Replace(const_default_tenant_url, frontendUrl) // Frontend Tenant URL
				.Replace(const_default_tenant_identifier, frontendTenant.Name); // Tenant Application Name

			File.WriteAllText(webconfigPath, output);
		}

		/// <summary>
		/// Sets the path and its contents to be non-read-only recursively.
		/// </summary>
		/// <param name="path">The path.</param>
		private static void SetNonReadOnly(string path) {
			var directoryInfo = new DirectoryInfo(path);
			directoryInfo.SetNonReadonlyRecursive();
		}


		/// <summary>
		/// Creates the energy aggregator.
		/// </summary>
		/// <param name="frontendTenant">The frontend tenant.</param>
		/// <param name="eaTenant">The ea tenant.</param>
		/// <param name="iisSettingsOnly">if set to <c>true</c> [IIS settings only].</param>
		/// <returns></returns>
		private Tenant CreateEnergyAggregator(Tenant frontendTenant, Tenant eaTenant, bool iisSettingsOnly = false) {
			if (EnergyAggregatorProtocol == ServiceInvocationProtocol.InMemory)
				return new Tenant();

			var retval = new Tenant();

			using (ServerManager serverManager = new ServerManager()) {
				// 1. create the directory on disk
				string path = Path.Combine(DefaultCustomersDirectory, eaTenant.Name);
				if (!iisSettingsOnly) {
					Directory.CreateDirectory(path);
				}
				// 2. create the application pool
				ApplicationPool applicationPool = serverManager.ApplicationPools.Add(eaTenant.Name);
				applicationPool.ManagedRuntimeVersion = "v4.0";
				applicationPool.ProcessModel.IdleTimeout = TimeSpan.FromMinutes(0);
				applicationPool["startMode"] = "AlwaysRunning";
				// energy aggregator should not be recycled
				applicationPool.Recycling.PeriodicRestart.Time = TimeSpan.FromSeconds(0);
				serverManager.CommitChanges();


				// 3. create the http website
				Site site = serverManager.Sites[eaTenant.Name];
				if (EnergyAggregatorProtocol == ServiceInvocationProtocol.WCFHTTP) {
					site = serverManager.Sites.Add(eaTenant.Name, "http", "*:80:" + eaTenant.Url, path);
				}
				else if (EnergyAggregatorProtocol == ServiceInvocationProtocol.WCFTCP) {
					site = serverManager.Sites.Add(eaTenant.Name, "net.tcp", "808:" + eaTenant.Url, path);
				}
				retval.Name = eaTenant.Name;
				retval.Url = site.Bindings[0].Host;
				site.ApplicationDefaults.ApplicationPoolName = applicationPool.Name;
				serverManager.CommitChanges();
				if (EnergyAggregatorProtocol == ServiceInvocationProtocol.WCFTCP) {
					// configure allowed binding
					site = serverManager.Sites[eaTenant.Name];
					Application application = site.Applications["/"];
					application.EnabledProtocols += ",net.tcp";
				}

				// 4. create the https endpoint
				site = serverManager.Sites[eaTenant.Name];
				X509Store store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
				store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadWrite);
				X509Certificate2 certificate = new X509Certificate2(this.SslCertificatePath, this.SslCertificatePassword);
				store.Add(certificate);
				var binding = site.Bindings.Add("*:443:" + eaTenant.Url, certificate.GetCertHash(), store.Name);
				binding.Protocol = "https";
				store.Close();
				serverManager.CommitChanges();

				if (!iisSettingsOnly) {
					// 5. Option, copy the files and set them to non read only
					CompressionHelper.Extract(this.EnergyAggregatorBinariesZipPath, path, true);
					SetNonReadOnly(path);

					// 6. Replace constant facility online with TenantName
					string webconfigPath = Path.Combine(path, "web.config");
					ReplaceTenantNameAndUrls(frontendTenant, eaTenant, webconfigPath);

					// 7. link the shared directories

					foreach (var sharedDirectory in EnergyAggregatorSharedDirectories) {
						string logicalDirectory = Path.Combine(path, sharedDirectory);
						string actualFilesDirectory = Path.Combine(this.EnergyAggregatorDirectoryBasePath, sharedDirectory);

						// we need to create a junction for a physical directory that exist; this will not work
						// so we delete the physical directory.
						if (Directory.Exists(logicalDirectory)) {
							// Lets check if this exists as a junction point due to manually deleted tenant site.
							if (JunctionPoint.Exists(logicalDirectory)) {
								JunctionPoint.Delete(logicalDirectory);
							}
							else {
								Helper.DeleteRecursive(logicalDirectory);
							}
						}

						JunctionPoint.Create(logicalDirectory, actualFilesDirectory, true);
					}
				}
			}

			return retval;
		}

		/// <summary>
		/// Gets the available application pool name or create a new one if needed.
		/// </summary>
		/// <returns></returns>
		private string GetAvailableSharedApplicationPoolName() {
			string retval;
			Dictionary<string, int> applicationPoolAndNumberOfSites = new Dictionary<string, int>();
			// for each site find which application pool it uses and count it.
			using (ServerManager serverManager = new ServerManager()) {
				foreach (var site in serverManager.Sites) {
					string applicationPoolName = site.ApplicationDefaults.ApplicationPoolName;
					if (applicationPoolName.StartsWith(this.SharedFrontendApplicationPoolPrefix)) {
						if (applicationPoolAndNumberOfSites.ContainsKey(applicationPoolName)) {
							applicationPoolAndNumberOfSites[applicationPoolName]++;
						}
						else {
							applicationPoolAndNumberOfSites[applicationPoolName] = 1;
						}
					}
				}
				// count the 'empty' appliction pools
				foreach (var applicationPool in serverManager.ApplicationPools) {
					if (applicationPool.Name.StartsWith(this.SharedFrontendApplicationPoolPrefix)) {
						if (!applicationPoolAndNumberOfSites.ContainsKey(applicationPool.Name)) {
							applicationPoolAndNumberOfSites[applicationPool.Name] = 0;
						}
					}
				}
			}

			var appPoolCandidate = applicationPoolAndNumberOfSites.Where(kvp => kvp.Value < this.SharedTenantsApplicationPoolLimit).FirstOrDefault();
			// if couldn't find an open slot then get a new application name and create it.
			if (object.Equals(appPoolCandidate, default(KeyValuePair<string, int>))) {
				using (ServerManager serverManager = new ServerManager()) {
					ApplicationPool newlyCreated = serverManager.ApplicationPools.Add(GetNextApplicationPoolName());
					retval = newlyCreated.Name;
					serverManager.CommitChanges();
				}
			}
			else {
				retval = appPoolCandidate.Key;
			}
			return retval;
		}
		/// <summary>
		/// Deletes an existing business entity Tenant.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public override bool DeleteTenant(Tenant item) {
			return true;
			//var retval = DeleteFrontend(item);
			//Tenant tempEnergyTenant = this.GetEnergyAggregatorTenant(item);
			//DeleteEnergyAggregator(tempEnergyTenant);
			//return retval;
		}

		
		/// <summary>
		/// Deletes the energy aggregator.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		private bool DeleteEnergyAggregator(Tenant item) {
			using (ServerManager serverManager = new ServerManager()) {
				var site = serverManager.Sites.SingleOrDefault(s => s.Name.Equals(item.Name));

				serverManager.Sites.Remove(site);
				serverManager.CommitChanges();
				serverManager.ApplicationPools.Remove(serverManager.ApplicationPools[item.Name]);
				serverManager.CommitChanges();

				string customerDirectory = Path.Combine(this.DefaultCustomersDirectory, item.Name);

				// remove junction points
				foreach (var sharedDirectory in EnergyAggregatorSharedDirectories) {
					string junctionDirectory = Path.Combine(customerDirectory, sharedDirectory);
					if (JunctionPoint.Exists(junctionDirectory)) {
						JunctionPoint.Delete(junctionDirectory);
					}
				}

				// remove directories
				string path = Path.Combine(this.DefaultCustomersDirectory, item.Name);
				Helper.DeleteRecursive(path);

			}
			return true;
		}

		/// <summary>
		/// Deletes the frontend.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		private bool DeleteFrontend(Tenant item) {
			// delete frontend
			if (item.KeyTenant == null)
				return true;
			using (ServerManager serverManager = new ServerManager()) {
				var site = serverManager.Sites.SingleOrDefault(s => s.Name.Equals(item.KeyTenant));

				serverManager.Sites.Remove(site);
				serverManager.CommitChanges();
				// only delete application pool if the tenant is isolated
				if (item.IsIsolated) {
					serverManager.ApplicationPools.Remove(serverManager.ApplicationPools[this.IsolatedFrontendApplicationPoolPrefix + item.Name]);
					serverManager.CommitChanges();
				}

				string customerDirectory = Path.Combine(this.DefaultCustomersDirectory, item.Name);

				// remove junction points
				foreach (var sharedDirectory in FrontendSharedDirectories) {
					string junctionDirectory = Path.Combine(customerDirectory, sharedDirectory);
					if (JunctionPoint.Exists(junctionDirectory)) {
						JunctionPoint.Delete(junctionDirectory);
					}
				}

				// remove directories
				string path = Path.Combine(this.DefaultCustomersDirectory, item.Name);
				Helper.DeleteRecursive(path);

			}
			return true;
		}

		/// <summary>
		/// Gets a specific item for the business entity Tenant.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public override Tenant  GetTenant(string Key) {
			//var retval = new Tenant();
			//using (ServerManager serverManager = new ServerManager()) {
			//    var site = serverManager.Sites.SingleOrDefault(s => s.Name.Equals(Key));
			//    if (default(Site) == site)
			//        retval = new Tenant();
			//    else
			//        retval = ConvertSiteToTenant(site);
			//}
			//return retval;
			return null;
		}

		/// <summary>
		/// Gets a list for the business entity Tenant.
		/// </summary>
		public override List<Tenant> GetTenants() {
			//var retval = new List<Tenant>();
			//using (ServerManager serverManager = new ServerManager()) {
			//    Configuration config = serverManager.GetApplicationHostConfiguration();
			//    ConfigurationSection section = config.GetSection("system.applicationHost/sites");

			//    foreach (var site in serverManager.Sites) {

			//        Tenant tenant = ConvertSiteToTenant(site);
			//        if (!string.IsNullOrEmpty(tenant.Url)) {
			//            if (!tenant.Url.StartsWith(EnergyAggregatorPrefix)) {
			//                retval.Add(tenant);
			//            }
			//        }

			//    }
			//}
			//return retval;
			return Enumerable.Empty<Tenant>().ToList();
		}

		/// <summary>
		/// Updates an existing business entity Tenant and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public override Tenant UpdateTenant(Tenant item) {
			//Tenant existingTenant = GetTenant(item.KeyTenant);

			//if (existingTenant.Url != item.Url || existingTenant.HostingContainer != item.HostingContainer || existingTenant.IsIsolated != item.IsIsolated) {
			//    // delete the all tenant
			//    DeleteTenant(existingTenant);
			//    // create the new one 
			//    var retval = CreateTenant(item);
			//    return retval;
			//}
			//return existingTenant;
			return item;
		}

		/// <summary>
		/// Converts the site to tenant.
		/// </summary>
		/// <param name="site">The site.</param>
		/// <returns></returns>
		private Tenant ConvertSiteToTenant(Microsoft.Web.Administration.Site site) {
			// TODO: change this assumption to a 'harder' future assumption
			bool isIsolated = site.ApplicationDefaults.ApplicationPoolName.StartsWith(this.IsolatedFrontendApplicationPoolPrefix);

			var retval = new Tenant() {
				Name = site.Name,
				Url = site.Bindings[0].Host,
				HostingContainer = site.ApplicationDefaults.ApplicationPoolName,
				IsIsolated = isIsolated
			};

			return retval;
		}

		/// <summary>
		/// Gets the energy aggregator tenant.
		/// </summary>
		/// <param name="frontendTenant">The frontend tenant.</param>
		/// <returns></returns>
		private Tenant GetEnergyAggregatorTenant(Tenant frontendTenant) {
			bool startsWithHttp = frontendTenant.Url.ToLower().StartsWith("http://");
			string frontendUrl;

			if (startsWithHttp) {
				// Remove http:// from beginning.
				frontendUrl = frontendTenant.Url.Substring(7);
			}
			else {
				frontendUrl = frontendTenant.Url;
			}


			return new Tenant {
				Name = EnergyAggregatorPrefix + frontendTenant.Name,
				Url = (startsWithHttp ? "http://" : "") + EnergyAggregatorPrefix + frontendUrl
			};
		}

		/// <summary>
		/// Encodes the name of the single application pool.
		/// </summary>
		/// <param name="siteName">Name of the site.</param>
		/// <returns></returns>
		private string EncodeIsolatedApplicationPoolName(string siteName) {
			return this.IsolatedFrontendApplicationPoolPrefix + siteName;
		}

		/// <summary>
		/// Encodes the name of the shared application pool.
		/// </summary>
		/// <returns></returns>
		private string GetNextApplicationPoolName() {
			int max = 0;
			using (ServerManager server = new ServerManager()) {
				foreach (ApplicationPool appPool in server.ApplicationPools) {
					if (appPool.Name.StartsWith(this.SharedFrontendApplicationPoolPrefix)) {

						int temp = Int32.Parse(appPool.Name.Replace(this.SharedFrontendApplicationPoolPrefix, ""));
						if (temp > max)
							max = temp;
					}
				}
			}

			var retval = this.SharedFrontendApplicationPoolPrefix + (max + 1);
			return retval;
		}

		/// <summary>
		/// Gets or sets the MultitenantHostingMode
		/// </summary>
		/// <value>
		/// The mode.
		/// </value>
		public override MultitenantHostingMode Mode {
			get { return MultitenantHostingMode.Isolated; }
		}
	}
}

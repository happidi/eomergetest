﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Linq;
using System.Web.Security;
using Microsoft.Web.Administration;
using Vizelia.FOL.Providers;
using System.Collections.Specialized;
using Vizelia.FOL.BusinessEntities;
using Site = Microsoft.Web.Administration.Site;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {

    /// <summary>
    /// Used when the hosting environment is already set, no need to perform any special infrastructure provisioning.
    /// </summary>
	public class SharedTenantProvisioningProvider : TenantProvisioningProvider {


        /// <summary>
        /// Creates a new business entity Tenant and returns the result as a valid extjs form response.
        /// </summary>
        /// <param name="item">The item to create.</param>
        /// <returns></returns>
        public override Tenant CreateTenant(Tenant item) {
            return item;
        }

        /// <summary>
        /// Deletes an existing business entity Tenant.
        /// </summary>
        /// <param name="item">The item to delete.</param>
        /// <returns></returns>
        public override bool DeleteTenant(Tenant item) {
            return true;
        }

        /// <summary>
        /// Gets a specific item for the business entity Tenant.
        /// </summary>
        /// <param name="Key">The entity Key.</param>
        /// <returns></returns>
        public override Tenant GetTenant(string Key) {
            return null;
        }

        /// <summary>
        /// Gets a list for the business entity Tenant.
        /// </summary>
        /// <returns></returns>
        public override List<Tenant> GetTenants() {
            return Enumerable.Empty<Tenant>().ToList();
        }

        /// <summary>
        /// Updates an existing business entity Tenant and returns the result as a valid extjs form response.
        /// </summary>
        /// <param name="item">The item to update.</param>
        /// <returns></returns>
        public override Tenant UpdateTenant(Tenant item) {
            return item;
        }

        /// <summary>
        /// Gets or sets the MultitenantHostingMode
        /// </summary>
        /// <value>
        /// The mode.
        /// </value>
		public override MultitenantHostingMode Mode {
            get { return MultitenantHostingMode.Shared; }
        }
    }
}

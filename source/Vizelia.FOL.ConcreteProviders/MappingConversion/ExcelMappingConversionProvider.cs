﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using Syncfusion.XlsIO;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a Excel mapping conversion provider
	/// </summary>
	public class ExcelMappingConversionProvider : MappingConversionProvider {
		private const int const_spatial_start_line = 0;
		private const int const_meterclassification_start_line = 1;
		private const int const_meter_start_line = 1;
		private const int const_meterdata_start_line = 0;
		private const int const_metervalidationrule_start_line = 1;
		private const int const_meteroperation_start_line = 1;
		private const int const_pset_start_line = 1;
		private const int const_psetlistelement_start_line = 1;
		private const int const_psetspatial_start_line = 1;
		private const int const_pset_historical_start_line = 1;
		private const int const_equipment_start_line = 1;
		private const int const_equipmentclassification_start_line = 1;
		private const int const_alarmclassification_start_line = 1;
		private const int const_alarmdefinition_start_line = 1;
		private const int const_alarminstance_start_line = 1;
		private const int const_calendarevent_start_line = 1;
		private const int const_dataacquisitioncontainer_start_line = 0;
		private const int const_restdataacquisitioncontainer_start_line = 1;
		private const int const_dataacquisitionendpoint_start_line = 0;
		private const int const_restdataacquisitionendpoint_start_line = 1;
		private const int const_company_start_line = 0;
		private const int const_users_start_line = 1;
		private const int const_eventlogclassification_start_line = 1;
		private const int const_eventlog_start_line = 1;
		private const int const_applicationgroup_start_line = 1;
		private const int const_project_start_line = 1;
		private const int const_role_start_line = 1;
		private const int const_filterspatialpset_start_line = 1;
		private const int const_energycertificate_start_line = 1;
		private const int const_energycertificatecategory_start_line = 1;
		private const int const_chart_start_line = 1;
		private const int const_weatherlocation_start_line = 1;
		private const int const_webframe_start_line = 0;
		private const int const_map_start_line = 1;
		private const int const_drawingcanvas_start_line = 0;
		private const int const_drawingcanvaschart_start_line = 1;
		private const int const_drawingcanvasimage_start_line = 0;
		private const int const_alarmtable_start_line = 1;
		private const int const_flipcard_start_line = 0;
		private const int const_flipcardentry_start_line = 1;
		private const int const_portalwindow_start_line = 1;
		private const int const_link_start_line = 1;
		private const int const_portaltab_start_line = 0;
		private const int const_portalcolumn_start_line = 1;
		private const int const_portaltemplate_start_line = 0;
		private const int const_playlist_start_line = 0;
		private const int const_playlistpage_start_line = 0;
		private const int const_virtualfile_start_line = 1;
		private const ExcelExportDataTableOptions const_excel_export_data_table_options = ExcelExportDataTableOptions.DetectColumnTypes | ExcelExportDataTableOptions.ComputedFormulaValues | ExcelExportDataTableOptions.PreserveOleDate | ExcelExportDataTableOptions.DefaultStyleColumnTypes;
		private readonly MeterDataValidity m_DefaultMeterDataValidity = VizeliaConfiguration.Instance.Deployment.DefaultMeterDataValidity;

		private List<MappingConversionError> m_ErrorList;
		/// <summary>
		/// Gets the entities.
		/// </summary>
		/// <param name="workbook">The workbook.</param>
		/// <returns></returns>
		public List<object> GetEntities(IWorkbook workbook) {
			m_ErrorList = new List<MappingConversionError>();
			var classificationItemBroker = Helper.CreateInstance<ClassificationItemBroker>();
			ClassificationItem rootClassificationItemEquipment = classificationItemBroker.GetCategoryRoot("equipment");
			ClassificationItem rootClassificationItemMeter = classificationItemBroker.GetCategoryRoot("meter");
			ClassificationItem rootClassificationItemAlarm = classificationItemBroker.GetCategoryRoot("alarm");
			ClassificationItem rootClassificationItemEventLog = classificationItemBroker.GetCategoryRoot("eventlog");

			var spatialTuple = new Tuple<Dictionary<string, object>, List<object>>(GetSpatialEntities(workbook.Worksheets["SPATIAL"]), null);

			var equipmentClassificationItemEntities = GetClassificationItemEntities(workbook.Worksheets["EQUIPMENT CLASSIFICATION"], const_equipmentclassification_start_line, rootClassificationItemEquipment);

			AddEquipmentEntitiesData(workbook.Worksheets["EQUIPMENT"], spatialTuple);

			var meterClassificationItemEntities = GetClassificationItemEntities(workbook.Worksheets["METER CLASSIFICATION"], const_meterclassification_start_line, rootClassificationItemMeter);

			var metersWithOperationMeterList = AddMeterEntities(workbook.Worksheets["METER"], spatialTuple);

			var meterDataEntities = GetMeterDataEntities(workbook.Worksheets["METER DATA"]);

			var meterOperationEntities = GetMeterOperationEntities(workbook.Worksheets["METER OPERATION"]);

			var meterValidationRuleEntities = GetMeterValidationRuleEntities(workbook.Worksheets["METER VALIDATION RULE"]);

			var psetListElementEntities = GetPsetListElementEntities(workbook.Worksheets["PSET LIST ELEMENT"]);

			var psetDefinitionEntities = GetPsetDefinitionEntities(workbook.Worksheets["PSET"]);

			AddPsetEntitiesToSpatial(workbook.Worksheets["PSET SPATIAL"], spatialTuple.Item1, psetDefinitionEntities);

			var organizationEntities = GetOrganizationEntities(workbook.Worksheets["COMPANY"]);

			var projectEntities = GetProjectEntities(workbook.Worksheets["PROJECT"]);

			var roleEntities = GetRoleEntities(workbook.Worksheets["ROLE"]);

			var applicationGroupEntities = GetApplicationGroupEntities(workbook.Worksheets["APPLICATION GROUP"]);

			var userAndOccupantEntities = GetUserAndOccupantEntities(workbook.Worksheets["USERS"]);

			var psetAttributeHistoricalEntities = GetPsetAttributeHistoricalEntities(workbook.Worksheets["PSET HISTORICAL"]);

			var alarmClassificationItemEntities = GetClassificationItemEntities(workbook.Worksheets["ALARM CLASSIFICATION"], const_alarmclassification_start_line, rootClassificationItemAlarm);

			var alarmDefinitionEntities = GetAlarmDefinitionEntities(workbook.Worksheets["ALARM DEFINITION"]);

			var alarmInstanceEntities = GetAlarmInstanceEntities(workbook.Worksheets["ALARM INSTANCE"]);

			var filterSpatialPsetEntities = GetFilterSpatialPsetEntities(workbook.Worksheets["FILTER SPATIAL PSET"]);

			var calendarEventEntities = GetCalendarEventEntities(workbook.Worksheets["CALENDAR EVENT"]);

			var calendarEventCategoryEntities = GetCalendarEventCategoryEntities(workbook.Worksheets["EVENT CATEGORY"]);

			var eventLogClassificationItemEntities =
				GetClassificationItemEntities(workbook.Worksheets["EVENT LOG CLASSIFICATION"],
											  const_eventlogclassification_start_line,
											  rootClassificationItemEventLog);
			var eventLogEntities = GetEventLogEntities(workbook.Worksheets["EVENT LOG"]);

			var RESTDataAcquisitionContainerEntities = GetRESTDataAcquisitionContainerEntities(workbook.Worksheets["REST CONTAINER"]);

			var EWSDataAcquisitionContainerEntities = GetDataAcquisitionContainerEntities(workbook.Worksheets["EWS CONTAINER"], "EWSDataAcquisitionContainer");

			var struxureWareDataAcquisitionContainerEntities = GetDataAcquisitionContainerEntities(workbook.Worksheets["STRUXUREWARE CONTAINER"], "StruxureWareDataAcquisitionContainer");

			var RESTDataAcquisitionEndpointEntities = GetDataAcquisitionEndpointEntities(workbook.Worksheets["REST ENDPOINT"], "RESTDataAcquisitionEndpoint");

			var EWSDataAcquisitionEndpointEntities = GetDataAcquisitionEndpointEntities(workbook.Worksheets["EWS ENDPOINT"], "EWSDataAcquisitionEndpoint");

			var struxureWareDataAcquisitionEndpointEntities = GetDataAcquisitionEndpointEntities(workbook.Worksheets["STRUXUREWARE ENDPOINT"], "StruxureWareDataAcquisitionEndpoint");

			var energyCertificeateEntities = GetEnergyCertificateEntities(workbook.Worksheets["ENERGY CERTIFICATE"]);

			var energyCertificeateCategoryEntities = GetEnergyCertificateCategoryEntities(workbook.Worksheets["CERTIFICATE CATEGORY"]);

			var chartEntities = GetChartEntities(workbook.Worksheets["CHART"]);

			var weatherLocationEntities = GetWeatherLocationEntities(workbook.Worksheets["WEATHER LOCATION"]);

			var webFrameEntities = GetWebFrameEntities(workbook.Worksheets["WEB FRAME"]);

			var mapEntities = GetMapEntities(workbook.Worksheets["MAP"]);

			var drawingCanvasEntities = GetDrawingCanvasEntities(workbook.Worksheets["DRAWING CANVAS"]);

			var drawingCanvasChartEntities = GetDrawingCanvasChartEntities(workbook.Worksheets["DRAWING CANVAS CHART"]);

			var drawingCanvasImageEntities = GetDrawingCanvasImageEntities(workbook.Worksheets["DRAWING CANVAS IMAGE"]);

			var alarmTableEntities = GetAlarmTableEntities(workbook.Worksheets["ALARM TABLE"]);

			var flipCardEntities = GetFlipCardEntities(workbook.Worksheets["FLIP CARD"]);

			var flipCardEntryEntities = GetFlipCardEntryEntities(workbook.Worksheets["FLIP CARD ENTRY"]);

			var portalWindowEntities = GetPortalWindowEntities(workbook.Worksheets["PORTAL WINDOW"]);
			var linkEntities = GetLinkEntities(workbook.Worksheets["LINK"]);

			var portalTabEntities = GetPortalTabEntities(workbook.Worksheets["PORTAL TAB"]);

			var portalColumnEntities = GetPortalColumnEntities(workbook.Worksheets["PORTAL COLUMN"]);

			var portalTemplateEntities = GetPortalTemplateEntities(workbook.Worksheets["PORTAL TEMPLATE"]);

			var playlistEntities = GetPlaylistEntities(workbook.Worksheets["PLAYLIST"]);

			var playlistPageEntities = GetPlaylistPageEntities(workbook.Worksheets["PLAYLIST PAGE"]);

			var virtualFileEntities = GetVirtualFileEntities(workbook.Worksheets["URL SHORTCUT"]);

			var retVal = new List<object>();
			retVal.AddRange(m_ErrorList);
			retVal.AddRange(equipmentClassificationItemEntities.Values);
			retVal.AddRange(meterClassificationItemEntities.Values);
			retVal.AddRange(alarmClassificationItemEntities.Values);
			retVal.AddRange(eventLogClassificationItemEntities.Values);
			retVal.AddRange(RESTDataAcquisitionContainerEntities);
			retVal.AddRange(RESTDataAcquisitionEndpointEntities);
			retVal.AddRange(EWSDataAcquisitionContainerEntities);
			retVal.AddRange(EWSDataAcquisitionEndpointEntities);
			retVal.AddRange(struxureWareDataAcquisitionContainerEntities);
			retVal.AddRange(struxureWareDataAcquisitionEndpointEntities);
			retVal.AddRange(psetListElementEntities);
			retVal.AddRange(psetDefinitionEntities);
			retVal.AddRange(spatialTuple.Item1.Values);
			retVal.AddRange(meterDataEntities);
			retVal.AddRange(meterOperationEntities);
			retVal.AddRange(metersWithOperationMeterList);
			retVal.AddRange(meterValidationRuleEntities);
			if (spatialTuple.Item2 != null) retVal.AddRange(spatialTuple.Item2);
			retVal.AddRange(roleEntities);
			retVal.AddRange(organizationEntities);
			retVal.AddRange(userAndOccupantEntities);
			retVal.AddRange(psetAttributeHistoricalEntities);
			retVal.AddRange(alarmDefinitionEntities.Values);
			retVal.AddRange(alarmInstanceEntities);
			retVal.AddRange(filterSpatialPsetEntities);
			retVal.AddRange(calendarEventCategoryEntities);
			retVal.AddRange(calendarEventEntities);
			retVal.AddRange(eventLogEntities);
			retVal.AddRange(applicationGroupEntities);
			retVal.AddRange(projectEntities);
			retVal.AddRange(energyCertificeateEntities);
			retVal.AddRange(energyCertificeateCategoryEntities);
			retVal.AddRange(chartEntities);
			retVal.AddRange(weatherLocationEntities);
			retVal.AddRange(webFrameEntities);
			retVal.AddRange(mapEntities);
			retVal.AddRange(drawingCanvasEntities);
			retVal.AddRange(drawingCanvasChartEntities);
			retVal.AddRange(drawingCanvasImageEntities);
			retVal.AddRange(alarmTableEntities);
			retVal.AddRange(flipCardEntities);
			retVal.AddRange(flipCardEntryEntities);
			retVal.AddRange(portalWindowEntities);
			retVal.AddRange(portalTabEntities);
			retVal.AddRange(portalColumnEntities);
			retVal.AddRange(portalTemplateEntities);
			retVal.AddRange(playlistEntities);
			retVal.AddRange(playlistPageEntities);
			retVal.AddRange(virtualFileEntities);
			retVal.AddRange(linkEntities);
			workbook.Close();
			return retVal;
		}

		/// <summary>
		/// Gets the entities from file.
		/// </summary>
		/// <param name="filenameOrStream">The filename or stream.</param>
		/// <returns></returns>
		public override List<object> GetEntities(object filenameOrStream) {
			var excelEngine = new ExcelEngine();
			var application = GetExcelApplication(excelEngine);
			IWorkbook workbook;
			if (filenameOrStream is string)
				workbook = application.Workbooks.Open(filenameOrStream as string, ExcelParseOptions.Default, true, null);
			else if (filenameOrStream is Stream)
				workbook = application.Workbooks.Open(filenameOrStream as Stream, ExcelParseOptions.Default, true, null);
			else
				throw new ArgumentException("Wrong type, should be string or Stream", "filenameOrStream");

			var retVal = GetEntities(workbook);

			//No exception will be thrown if there are unsaved workbooks.
			excelEngine.ThrowNotSavedOnDestroy = false;
			excelEngine.Dispose();

			return retVal;
		}

		private static IApplication GetExcelApplication(ExcelEngine excelEngine) {
			IApplication application = excelEngine.Excel;
			application.DefaultVersion = ExcelVersion.Excel2010;
			return application;
		}

		private void AddPsetEntitiesToSpatial(IWorksheet sheet, Dictionary<string, object> spatialEntitiesDic, IEnumerable<object> psetDefinitionEntities) {
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_psetspatial_start_line].ItemArray.Select(x => x as string).ToList();
				var psetAttributeTypes = new Dictionary<string, string>();
				var locationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationId");
				var psetNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "PsetName");
				var ifcTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "IfcType");
				for (var i = const_psetspatial_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var keySpatial = dt.Rows[i][locationIdColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(keySpatial)) {
							break;
						}
						if (!spatialEntitiesDic.ContainsKey(keySpatial))
							throw new ExcelMappingConversionException(sheet.Name, string.Format("{0} is referenced in line {1} but does not exists in hierarchy spatial tree", keySpatial, i + 1));
						var spatialEntity = spatialEntitiesDic[keySpatial] as IPset;
						if (spatialEntity != null) {
							if (spatialEntity.PropertySetList == null)
								spatialEntity.PropertySetList = new List<Pset>();
							var psetName = dt.Rows[i][psetNameColumn.Index] as string;
							var ifcType = dt.Rows[i][ifcTypeColumn.Index] as string;
							var pset = new Pset {
								Attributes = new List<PsetAttribute>(),
								IfcType = ifcType,
								PsetName = psetName
							};
							var index = 3;
							while (index < sheet.Columns.Length) {
								var value = dt.Rows[i][index] as string;
								if (!string.IsNullOrWhiteSpace(value)) {
									var psetAttributeName = columnsDefinition[index];
									var key = string.Format("PsetName={0}PsetAttributeName={1}", psetName, psetAttributeName);
									psetAttributeTypes[key] = psetAttributeTypes.ContainsKey(key) ? psetAttributeTypes[key] : GetPsetAttributeType(key, psetDefinitionEntities);
									var conversionColumn = new ExcelMappingConversionColumn { Index = index, Name = psetAttributeName };
									switch (psetAttributeTypes[key]) {
										case "boolean":
											var boolValue = GetNullableBool(dt, i, conversionColumn);
											value = boolValue.ToString().ToLower();
											break;
										case "date":
											var date = GetNullableDateTime(dt, i, conversionColumn);
											value = date.HasValue ? date.Value.JsonDate() : null;
											break;
										case "float":
										case "int":
											value = GetNullableDouble(dt, i, conversionColumn).ToString();
											break;
									}
									pset.Attributes.Add(new PsetAttribute {
										AttributeName = psetAttributeName,
										Value = value,
										Key = psetAttributeName
									});
								}
								index++;
							}
							spatialEntity.PropertySetList.Add(pset);
						}
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
		}

		private string GetPsetAttributeType(string psetAttributeLocalId, IEnumerable<object> psetDefinitionEntities, string sheetName = "PSET SPATIAL") {
			string retVal;
			var psetAttributeDefinition = psetDefinitionEntities.FirstOrDefault(x => (x as PsetAttributeDefinition != null) && (x as PsetAttributeDefinition).LocalId == psetAttributeLocalId.Trim()) as PsetAttributeDefinition;
			if (psetAttributeDefinition != null) {
				retVal = psetAttributeDefinition.AttributeDataType;
			}
			else {
				var psetAttributeBroker = Helper.CreateInstance<PsetAttributeDefinitionBroker>();
				psetAttributeDefinition = psetAttributeBroker.GetItemByLocalId(psetAttributeLocalId);
				if (psetAttributeDefinition == null) {
					throw new ExcelMappingConversionException(sheetName, string.Format("Entity {0} does not exist", psetAttributeLocalId));
				}
				retVal = psetAttributeDefinition.AttributeDataType;
			}
			return retVal;
		}

		private IEnumerable<object> GetDataAcquisitionContainerEntities(IWorksheet sheet, string containerType) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_dataacquisitioncontainer_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var urlColumn = GetColumnByName(dt.TableName, columnsDefinition, "Url");
				var usernameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Username");
				var passwordColumn = GetColumnByName(dt.TableName, columnsDefinition, "Password");
				for (var i = const_dataacquisitioncontainer_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						object entity;
						switch (containerType) {
							case "EWSDataAcquisitionContainer":
								entity = new EWSDataAcquisitionContainer {
									LocalId = key,
									Title = dt.Rows[i][titleColumn.Index] as string,
									Url = dt.Rows[i][urlColumn.Index] as string,
									Username = dt.Rows[i][usernameColumn.Index] as string,
									Password = dt.Rows[i][passwordColumn.Index] as string
								};
								break;
							case "StruxureWareDataAcquisitionContainer":
								entity = new StruxureWareDataAcquisitionContainer {
									LocalId = key,
									Title = dt.Rows[i][titleColumn.Index] as string,
									Url = dt.Rows[i][urlColumn.Index] as string,
									Username = dt.Rows[i][usernameColumn.Index] as string,
									Password = dt.Rows[i][passwordColumn.Index] as string
								};
								break;
							default:
								throw new ArgumentException(string.Format("type \"{0}\" is not supported", containerType));
						}
						entityList.Add(entity);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetRESTDataAcquisitionContainerEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_restdataacquisitioncontainer_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var urlColumn = GetColumnByName(dt.TableName, columnsDefinition, "Url/Location");
				var providerTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "ProviderType");
				var xPathMeterIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "XPathMeterId");
				var xPathAcquisitionDateTimeColumn = GetColumnByName(dt.TableName, columnsDefinition, "XPathAcquisitionDateTime");
				var xPathValueColumn = GetColumnByName(dt.TableName, columnsDefinition, "XPathValue");
				var attributeAcquisitionDateTimeColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeAcquisitionDateTime");
				var attributeMeterIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeMeterId");
				var attributeValueColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeValue");
				var formatAcquisitionDateTimeColumn = GetColumnByName(dt.TableName, columnsDefinition, "FormatAcquisitionDateTime");
				var keyLocalizationCultureColumn = GetColumnByName(dt.TableName, columnsDefinition, "KeyLocalizationCulture");
				for (var i = const_restdataacquisitioncontainer_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						var concreteProvidersDllName = typeof(RESTDataAcquisitionProvider).Assembly.ManifestModule.Name;
						concreteProvidersDllName = concreteProvidersDllName.Remove(concreteProvidersDllName.Length - 4);
						var entity = new RESTDataAcquisitionContainer {
							LocalId = key,
							Title = dt.Rows[i][titleColumn.Index] as string,
							Url = dt.Rows[i][urlColumn.Index] as string,
							ProviderType = string.Format("{0}.{1}RESTDataAcquisitionProvider, {0}", concreteProvidersDllName, dt.Rows[i][providerTypeColumn.Index] as string),
							XPathMeterId = dt.Rows[i][xPathMeterIdColumn.Index] as string,
							XPathAcquisitionDateTime = dt.Rows[i][xPathAcquisitionDateTimeColumn.Index] as string,
							XPathValue = dt.Rows[i][xPathValueColumn.Index] as string,
							AttributeAcquisitionDateTime = dt.Rows[i][attributeAcquisitionDateTimeColumn.Index] as string,
							AttributeMeterId = dt.Rows[i][attributeMeterIdColumn.Index] as string,
							AttributeValue = dt.Rows[i][attributeValueColumn.Index] as string,
							FormatAcquisitionDateTime = dt.Rows[i][formatAcquisitionDateTimeColumn.Index] as string,
							KeyLocalizationCulture = dt.Rows[i][keyLocalizationCultureColumn.Index] as string
						};
						entityList.Add(entity);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetDataAcquisitionEndpointEntities(IWorksheet sheet, string endpointType) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var startLine = endpointType == "RESTDataAcquisitionEndpoint"
									? const_restdataacquisitionendpoint_start_line
									: const_dataacquisitionendpoint_start_line;
				var columnsDefinition = dt.Rows[startLine].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var dataAcquisitionContainerIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "DataAcquisitionContainerId");
				var pathColumn = GetColumnByName(dt.TableName, columnsDefinition, "Path");
				var nameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Name");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				var unitColumn = GetColumnByName(dt.TableName, columnsDefinition, "Unit", false);
				var queryStringColumn = GetColumnByName(dt.TableName, columnsDefinition, "QueryString", false);
				for (var i = startLine + 1; i < sheet.Rows.Length; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						object entity;
						var keyDataAcquisitionContainer = dt.Rows[i][dataAcquisitionContainerIdColumn.Index] as string;
						switch (endpointType) {
							case "EWSDataAcquisitionEndpoint":
								entity = new EWSDataAcquisitionEndpoint {
									LocalId = keyDataAcquisitionContainer + key,
									ServerLocalId = key,
									KeyDataAcquisitionContainer = keyDataAcquisitionContainer,
									Path = dt.Rows[i][pathColumn.Index] as string,
									Name = GetNotNullString(nameColumn, i, dt),
									Description = GetNotNullString(descriptionColumn, i, dt)
								};
								break;
							case "StruxureWareDataAcquisitionEndpoint":
								entity = new StruxureWareDataAcquisitionEndpoint {
									LocalId = keyDataAcquisitionContainer + key,
									ServerLocalId = key,
									KeyDataAcquisitionContainer = keyDataAcquisitionContainer,
									Path = dt.Rows[i][pathColumn.Index] as string,
									Name = GetNotNullString(nameColumn, i, dt),
									Description = GetNotNullString(descriptionColumn, i, dt)
								};
								break;
							case "RESTDataAcquisitionEndpoint":
								entity = new RESTDataAcquisitionEndpoint {
									LocalId = key,
									KeyDataAcquisitionContainer = keyDataAcquisitionContainer,
									Path = dt.Rows[i][pathColumn.Index] as string,
									Name = GetNotNullString(nameColumn, i, dt),
									Description = dt.Rows[i][descriptionColumn.Index] as string,
									Unit = dt.Rows[i][unitColumn.Index] as string,
									QueryString = dt.Rows[i][queryStringColumn.Index] as string
								};
								break;
							default:
								throw new ArgumentException(string.Format("type \"{0}\" is not supported", endpointType));
						}
						entityList.Add(entity);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetCalendarEventEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_calendarevent_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "EventId");
				var startDateColumn = GetColumnByName(dt.TableName, columnsDefinition, "StartDate");
				var endDateColumn = GetColumnByName(dt.TableName, columnsDefinition, "EndDate");
				var eventNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "EventName");
				var locationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationId");
				var locationTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationType");
				var isAllDayColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsAllDay?");
				var timeZoneIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "TimeZoneId");
				var categoryColumn = GetColumnByName(dt.TableName, columnsDefinition, "EventCategoryId");
				var factorColumn = GetColumnByName(dt.TableName, columnsDefinition, "Factor");
				var isPeriodicColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsPeriodic?");
				var frequencyColumn = GetColumnByName(dt.TableName, columnsDefinition, "Frequency");
				var intervalColumn = GetColumnByName(dt.TableName, columnsDefinition, "Interval");
				var countColumn = GetColumnByName(dt.TableName, columnsDefinition, "Count");
				var untilColumn = GetColumnByName(dt.TableName, columnsDefinition, "Until");
				var byDayColumn = GetColumnByName(dt.TableName, columnsDefinition, "ByDay");
				var byMonthDayColumn = GetColumnByName(dt.TableName, columnsDefinition, "ByMonthDay");
				var bySetPositionColumn = GetColumnByName(dt.TableName, columnsDefinition, "BySetPosition");
				var byMonthColumn = GetColumnByName(dt.TableName, columnsDefinition, "ByMonth");

				var timezones = TimeZoneInfo.GetSystemTimeZones();
				for (var i = const_calendarevent_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						var timeZoneId = dt.Rows[i][timeZoneIdColumn.Index] as string;
						var timeZone = timezones.FirstOrDefault(x => x.Id == timeZoneId);
						CalendarRecurrencePattern calendarRecurrencePattern = null;
						var byMonth = GetNullableDouble(dt, i, byMonthColumn);
						if (GetNullableBool(dt, i, isPeriodicColumn) ?? false) {
							var bySetPosition = GetNullableDouble(dt, i, bySetPositionColumn);
							calendarRecurrencePattern = new CalendarRecurrencePattern {
								Frequency = (dt.Rows[i][frequencyColumn.Index] as string).ParseAsEnum<CalendarFrequencyType>(),
								Interval = (int)GetDouble(dt, i, intervalColumn, 1),
								Count = (int)GetDouble(dt, i, countColumn, 0),
								Until = GetNullableDateTime(dt, i, untilColumn),
								ByDay = GetCalendarDaySpecifierList(dt, i, byDayColumn),
								ByMonthDay = GetIntList(dt.Rows[i][byMonthDayColumn.Index] as string),
								BySetPosition = bySetPosition.HasValue ? new List<int> { (int)bySetPosition } : null,
								ByMonth = byMonth.HasValue ? new List<int> { (int)byMonth.Value } : null
							};
							if (calendarRecurrencePattern.ByMonth != null && calendarRecurrencePattern.ByMonth.Any(x => x < 1 || x > 12)) {
								throw new ArgumentException("ByMonth values should be between 1 to 12");
							}
							if (calendarRecurrencePattern.ByMonthDay != null && calendarRecurrencePattern.ByMonthDay.Any(x => x < 1 || x > 31)) {
								throw new ArgumentException("ByMonthDay values should be between 1 to 31");
							}
							if (calendarRecurrencePattern.Count != 0 && calendarRecurrencePattern.Until.HasValue) {
								throw new ArgumentException("Both COUNT and UNTIL cannot be supplied together; they are mutually exclusive.");
							}
						}
						var entity = new CalendarEvent {
							LocalId = key,
							Title = dt.Rows[i][eventNameColumn.Index] as string,
							KeyLocation = dt.Rows[i][locationIdColumn.Index] as string,
							LocationTypeName = (dt.Rows[i][locationTypeColumn.Index] as string).ParseAsEnum<HierarchySpatialTypeName>(),
							TimeZoneId = timeZone != null ? timeZone.Id : "UTC",
							StartDate = GetNullableDateTime(dt, i, startDateColumn),
							EndDate = GetNullableDateTime(dt, i, endDateColumn),
							IsAllDay = GetBool(dt, i, isAllDayColumn),
							Category = dt.Rows[i][categoryColumn.Index] as string,
							Factor = GetNullableDouble(dt, i, factorColumn),
							RecurrencePattern = calendarRecurrencePattern
						};

						entity.TimeZoneOffset[0] = TimeZoneInfo.Local.GetUtcOffset(entity.StartDate.Value);
						entity.TimeZoneOffset[1] = TimeZoneInfo.Local.GetUtcOffset(entity.EndDate.Value);
						entity.StartDate = TimeZoneInfo.ConvertTime(entity.StartDate.Value, TimeZoneInfo.Local);
						entity.EndDate = TimeZoneInfo.ConvertTime(entity.EndDate.Value, TimeZoneInfo.Local);

						entityList.Add(entity);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private List<int> GetIntList(string listString) {
			List<int> retVal = null;
			if (!string.IsNullOrWhiteSpace(listString)) {
				var list = listString.Split(',');
				retVal = list.Select(int.Parse).ToList();
			}
			return retVal;
		}

		private List<CalendarDaySpecifier> GetCalendarDaySpecifierList(DataTable dt, int i, ExcelMappingConversionColumn idsColumn) {
			List<CalendarDaySpecifier> retVal = null;
			var ids = dt.Rows[i][idsColumn.Index] as string;
			if (!string.IsNullOrWhiteSpace(ids)) {
				retVal = ids.Split(',').Select(id => new CalendarDaySpecifier {
					DayOfWeek = (int.Parse(id) - 1).ParseAsEnum<DayOfWeek>(),
					Offset = CalendarFrequencyOccurrence.None
				}).ToList();
			}
			return retVal;
		}

		private IEnumerable<object> GetCalendarEventCategoryEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_calendarevent_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var msgCultureColumn = GetColumnByName(dt.TableName, columnsDefinition, "MsgCulture");
				var msgDisplayColumn = GetColumnByName(dt.TableName, columnsDefinition, "MsgDisplay");
				var msgCodeColumn = GetColumnByName(dt.TableName, columnsDefinition, "MsgCode");

				for (var i = const_calendarevent_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						var msgCode = dt.Rows[i][msgCodeColumn.Index] as string;
						if (!string.IsNullOrWhiteSpace(msgCode)) {
							msgCode = msgCode.Trim();
							var msgValue = dt.Rows[i][msgDisplayColumn.Index] as string;
							var localizationResource = new LocalizationResource {
								Culture = dt.Rows[i][msgCultureColumn.Index] as string,
								Key = msgCode,
								LocalId = msgCode,
								Value = msgValue
							};
							entityList.Add(localizationResource);
						}
						var rand = new Random();
						var colorId = rand.Next(1, 32);
						var rgbArray = GetRGBArray(colorId);
						var entity = new CalendarEventCategory {
							LocalId = key,
							ColorId = colorId.ToString(CultureInfo.InvariantCulture),
							ColorR = rgbArray[0],
							ColorG = rgbArray[1],
							ColorB = rgbArray[2],
							MsgCode = msgCode
						};

						entityList.Add(entity);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private int[] GetRGBArray(int colorId) {
			if (colorId < 1 || colorId > 32)
				throw new ArgumentOutOfRangeException("colorId", colorId, "Column ColorId should be between 1 to 32");

			// Color values were copied from "viz-ext-all.css" from rules starting with "x-cal-"
			var colors = new[] { "fa7166", "cf2424", "a01a1a", "7e3838", "ca7609", "f88015", "eda12a", "d5b816"
								,"e281ca", "bf53a4", "9d3283", "7a0f60", "542382", "7742a9", "8763ca", "b586e2"
								,"7399f9", "4e79e6", "2951b9", "133897", "1a5173", "1a699c", "3694b7", "64b9d9"
								,"a8c67b", "83ad47", "2e8f0c", "176413", "0f4c30", "386651", "3ea987", "7bc3b5"};
			var color = colors[colorId - 1];
			int rColor = int.Parse(color.Substring(0, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
			int gColor = int.Parse(color.Substring(2, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
			int bColor = int.Parse(color.Substring(4, 2), System.Globalization.NumberStyles.AllowHexSpecifier);
			return new[] { rColor, gColor, bColor };
		}

		private IEnumerable<object> GetAlarmInstanceEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_alarminstance_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var alarmDefinitionIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "AlarmDefinitionId");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				var statusColumn = GetColumnByName(dt.TableName, columnsDefinition, "Status");
				var instanceDateTimeColumn = GetColumnByName(dt.TableName, columnsDefinition, "InstanceDateTime");
				var valueColumn = GetColumnByName(dt.TableName, columnsDefinition, "Value");
				var meterIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "MeterId");
				var createDateTimeColumn = GetColumnByName(dt.TableName, columnsDefinition, "CreateDateTime");
				var emailSentColumn = GetColumnByName(dt.TableName, columnsDefinition, "EmailSent?");
				for (var i = const_alarminstance_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						var entity = new AlarmInstance {
							LocalId = key,
							KeyAlarmInstance = key,
							KeyAlarmDefinition = dt.Rows[i][alarmDefinitionIdColumn.Index] as string,
							Description = dt.Rows[i][descriptionColumn.Index] as string,
							Status = (dt.Rows[i][statusColumn.Index] as string).ParseAsEnum<AlarmInstanceStatus>(),
							InstanceDateTime = GetDateTime(dt, i, instanceDateTimeColumn),
							Value = GetDouble(dt, i, valueColumn),
							KeyMeter = dt.Rows[i][meterIdColumn.Index] as string,
							CreateDateTime = GetDateTime(dt, i, createDateTimeColumn),
							EmailSent = GetNullableBool(dt, i, emailSentColumn)
						};

						entityList.Add(entity);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private Dictionary<string, object> GetAlarmDefinitionEntities(IWorksheet sheet) {
			var entityDic = new Dictionary<string, object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_alarmdefinition_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var startDateColumn = GetColumnByName(dt.TableName, columnsDefinition, "StartDate");
				var endDateColumn = GetColumnByName(dt.TableName, columnsDefinition, "EndDate");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				var enabledColumn = GetColumnByName(dt.TableName, columnsDefinition, "Enabled?");
				var locationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationId");
				var locationTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationType");
				var classificationItemIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "ClassificationItemId");
				var conditionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Condition");
				var thresholdColumn = GetColumnByName(dt.TableName, columnsDefinition, "Threshold");
				var threshold2Column = GetColumnByName(dt.TableName, columnsDefinition, "Threshold2");
				var usePrecentageDeltaColumn = GetColumnByName(dt.TableName, columnsDefinition, "UsePrecentageDelta?");
				var emailsColumn = GetColumnByName(dt.TableName, columnsDefinition, "Emails");
				var calendarEventCategoryIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "CalendarEventCategoryId");
				var calendarModeColumn = GetColumnByName(dt.TableName, columnsDefinition, "CalendarMode");
				var timeIntervalColumn = GetColumnByName(dt.TableName, columnsDefinition, "TimeInterval");
				var timeZoneIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "TimeZoneId");
				var isProcessManuallyColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsProcessManually");
				var filterSpatialColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterSpatial");
				var filterChartsColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterCharts");
				var filterClassificationItemColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterMeterClassifications");
				var metersColumn = GetColumnByName(dt.TableName, columnsDefinition, "Meters");
				var timezones = TimeZoneInfo.GetSystemTimeZones();
				for (var i = const_alarmdefinition_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						var timeZoneId = dt.Rows[i][timeZoneIdColumn.Index] as string;
						var timeZone = timezones.FirstOrDefault(x => x.Id == timeZoneId);

						var entity = new AlarmDefinition {
							LocalId = key,
							KeyAlarmDefinition = key,
							Title = dt.Rows[i][titleColumn.Index] as string,
							Description = dt.Rows[i][descriptionColumn.Index] as string,
							Enabled = GetBool(dt, i, enabledColumn, true),
							KeyLocation = dt.Rows[i][locationIdColumn.Index] as string,
							LocationTypeName = (dt.Rows[i][locationTypeColumn.Index] as string).ParseAsEnum<HierarchySpatialTypeName>(),
							KeyClassificationItem = dt.Rows[i][classificationItemIdColumn.Index] as string,
							StartDate = GetNullableDateTime(dt, i, startDateColumn),
							EndDate = GetNullableDateTime(dt, i, endDateColumn),
							Condition = (dt.Rows[i][conditionColumn.Index] as string).ParseAsEnum<FilterCondition>(),
							Threshold = GetNullableDouble(dt, i, thresholdColumn),
							Threshold2 = GetNullableDouble(dt, i, threshold2Column),
							UsePercentageDelta = GetBool(dt, i, usePrecentageDeltaColumn),
							Emails = dt.Rows[i][emailsColumn.Index] as string,
							KeyCalendarEventCategory = dt.Rows[i][calendarEventCategoryIdColumn.Index] as string,
							CalendarMode = (dt.Rows[i][calendarModeColumn.Index] as string).ParseAsEnum<ChartCalendarMode>(),
							TimeInterval = (dt.Rows[i][timeIntervalColumn.Index] as string).ParseAsEnum<AxisTimeInterval>(),
							TimeZoneId = timeZone != null ? timeZone.Id : null,
							ProcessManually = GetBool(dt, i, isProcessManuallyColumn),
							FilterSpatial = GetCommaSeparetedLocationList(dt.Rows[i][filterSpatialColumn.Index] as string),
							FilterCharts = GetMappableList<Chart>(filterChartsColumn, dt, i),
							FilterMeterClassifications = GetMappableList<ClassificationItem>(filterClassificationItemColumn, dt, i),
							Meters = GetMappableList<Meter>(metersColumn, dt, i)
						};

						entityDic.Add(key, entity);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityDic;
		}

		private IEnumerable<object> GetFilterSpatialPsetEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_filterspatialpset_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var alarmDefinitionIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "AlarmDefinitionId");
				var orderColumn = GetColumnByName(dt.TableName, columnsDefinition, "Order");
				var psetNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "PsetName");
				var attributeNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeName");
				var conditionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Condition");
				var valueColumn = GetColumnByName(dt.TableName, columnsDefinition, "Value");
				var value2Column = GetColumnByName(dt.TableName, columnsDefinition, "Value2");
				var operatorColumn = GetColumnByName(dt.TableName, columnsDefinition, "Operator");
				for (var i = const_filterspatialpset_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();

						var entity = new FilterSpatialPset {
							LocalId = key,
							KeyAlarmDefinition = dt.Rows[i][alarmDefinitionIdColumn.Index] as string,
							Order = (int)GetDouble(dt, i, orderColumn),
							PsetName = dt.Rows[i][psetNameColumn.Index] as string,
							AttributeName = dt.Rows[i][attributeNameColumn.Index] as string,
							Condition = (dt.Rows[i][conditionColumn.Index] as string).ParseAsEnum<FilterCondition>(),
							Value = dt.Rows[i][valueColumn.Index] as string,
							Value2 = dt.Rows[i][value2Column.Index] as string,
							Operator = (dt.Rows[i][operatorColumn.Index] as string).ParseAsEnum<LogicalOperator>()
						};

						entityList.Add(entity);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetPsetAttributeHistoricalEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_pset_historical_start_line].ItemArray.Select(x => x as string).ToList();
				var psetNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "PsetName");
				var attributeNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeName");
				var dateTimeColumn = GetColumnByName(dt.TableName, columnsDefinition, "DateTime");
				var locationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationId");
				var locationTypeNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationTypeName");
				var valueColumn = GetColumnByName(dt.TableName, columnsDefinition, "Value");
				var commentColumn = GetColumnByName(dt.TableName, columnsDefinition, "Comment");
				for (var i = const_pset_historical_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var psetName = dt.Rows[i][psetNameColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(psetName)) {
							break;
						}
						psetName = psetName.Trim();
						var psetAttributeName = GetNotNullString(attributeNameColumn, i, dt).Trim();
						var acquisitionDataTime = GetDateTime(dt, i, dateTimeColumn);
						var locationId = GetNotNullString(locationIdColumn, i, dt).Trim();
						var locationTypeName = GetNotNullString(locationTypeNameColumn, i, dt).Trim();
						var key = string.Format("PsetName={0}PsetAttributeName={1}LocationId={2}LocationTypeName={3}AcquisitionDateTime={4}", psetName, psetAttributeName, locationId, locationTypeName, acquisitionDataTime);
						var entity = new PsetAttributeHistorical {
							KeyPsetAttributeHistorical = key,
							LocalId = key,
							KeyPropertySingleValue = string.Format("PsetName={0}PsetAttributeName={1}", psetName, psetAttributeName),
							AttributeValue = GetDouble(dt, i, valueColumn),
							AttributeComment = dt.Rows[i][commentColumn.Index] as string,
							KeyObject = locationId,
							UsageName = locationTypeName,
							AttributeAcquisitionDateTime = acquisitionDataTime
						};

						entityList.Add(entity);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private void AddEquipmentEntitiesData(IWorksheet sheet, Tuple<Dictionary<string, object>, List<object>> spatialTuple) {
			var spatialEntities = spatialTuple.Item1;
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_equipment_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var nameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Name");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				var locationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationId");
				var classificationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "ClassificationId");
				var locationTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationType");
				for (var i = const_equipment_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						var entity = new Furniture {
							LocalId = key,
							KeyFurniture = key,
							Name = dt.Rows[i][nameColumn.Index] as string,
							Description = dt.Rows[i][descriptionColumn.Index] as string,
							KeyLocation = dt.Rows[i][locationIdColumn.Index] as string,
							KeyClassificationItem = dt.Rows[i][classificationIdColumn.Index] as string,
							LocationTypeName = (dt.Rows[i][locationTypeColumn.Index] as string).ParseAsEnum<HierarchySpatialTypeName>()
						};
						if (spatialEntities.ContainsKey(key)) {
							var spatialFurniture = spatialEntities[key] as Furniture;
							if (spatialFurniture != null) {
								entity.PropertySetList = spatialFurniture.PropertySetList;
								spatialEntities[key] = entity;
							}
						}
						else {
							spatialEntities.Add(key, entity);
						}
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
		}

		private IEnumerable<object> GetUserAndOccupantEntities(IWorksheet sheet) {
			var entitiesList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_users_start_line].ItemArray.Select(x => x as string).ToList();
				var loginColumn = GetColumnByName(dt.TableName, columnsDefinition, "Login");
				var firstNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "FirstName");
				var lastNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "LastName");
				var emailColumn = GetColumnByName(dt.TableName, columnsDefinition, "Email");
				var commentColumn = GetColumnByName(dt.TableName, columnsDefinition, "Comment");
				var isLockedColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsLocked");
				var neverExpires = GetColumnByName(dt.TableName, columnsDefinition, "NeverExpires");
				var occupantLocationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "OccupantLocationId");
				var occupantLocationTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "OccupantLocationType");
				var organizationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "OrganizationId");
				var worktypeIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "WorktypeId");
				var companyIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "CompanyId");
				var timeZoneIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "TimeZoneId");
				//var userLocationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "UserLocationId");
				//var userLocationTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "UserLocationType");
				for (var i = const_users_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var key = dt.Rows[i][loginColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						var occupant = new Occupant {
							LocalId = key,
							Id = key,
							FirstName = dt.Rows[i][firstNameColumn.Index] as string,
							LastName = dt.Rows[i][lastNameColumn.Index] as string,
							KeyLocation = dt.Rows[i][occupantLocationIdColumn.Index] as string,
							LocationTypeName = (dt.Rows[i][occupantLocationTypeColumn.Index] as string).ParseAsEnum<HierarchySpatialTypeName>(),
							KeyClassificationItemOrganization = dt.Rows[i][organizationIdColumn.Index] as string,
							KeyClassificationItemWorktype = dt.Rows[i][worktypeIdColumn.Index] as string,
							KeyOrganization = GetNotNullString(companyIdColumn, i, dt)
						};
						var passwordColumn = GetColumnByName(dt.TableName, columnsDefinition, "Password");
						var isApprovedColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsApproved");
						var providerUserKey = new FOLProviderUserKey { KeyOccupant = key, NeverExpires = GetBool(dt, i, neverExpires) };

						var preferences = Helper.CreateInstance<UserPreferencesBroker>().GetItemByLocalId(key);
						if (preferences == null) {
							preferences = new UserPreferences {
								Application = ContextHelper.ApplicationName,
								LocalId = key
							};
						}
						preferences.TimeZoneId = dt.Rows[i][timeZoneIdColumn.Index] as string;

						var user = new FOLMembershipUser {
							LocalId = key,
							UserName = key,
							Password = dt.Rows[i][passwordColumn.Index] as string,
							Email = dt.Rows[i][emailColumn.Index] as string,
							IsApproved = GetBool(dt, i, isApprovedColumn),
							ProviderUserKey = providerUserKey,
							Comment = dt.Rows[i][commentColumn.Index] as string,
							IsLockedOut = GetBool(dt, i, isLockedColumn),
							ApplicationName = ContextHelper.ApplicationName,
							//Preferences = preferences,
							FirstName = dt.Rows[i][firstNameColumn.Index] as string,
							LastName = dt.Rows[i][lastNameColumn.Index] as string,
						};

						entitiesList.Add(occupant);
						entitiesList.Add(user);
						entitiesList.Add(preferences);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entitiesList;
		}

		private static string GetNotNullString(ExcelMappingConversionColumn column, int i, DataTable dt) {
			var retVal = dt.Rows[i][column.Index] as string;
			if (string.IsNullOrWhiteSpace(retVal)) {
				throw new ExcelMappingConversionException(dt.TableName, string.Format("{0} at line {1} is empty", column.Name, i + 1));
			}
			return retVal;
		}

		private IEnumerable<object> GetPsetListElementEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_psetlistelement_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "ListId");
				var msgCultureColumn = GetColumnByName(dt.TableName, columnsDefinition, "MsgCulture");
				var msgDisplayColumn = GetColumnByName(dt.TableName, columnsDefinition, "MsgDisplay");
				var msgCodeColumn = GetColumnByName(dt.TableName, columnsDefinition, "MsgCode");
				var valueColumn = GetColumnByName(dt.TableName, columnsDefinition, "Value");

				for (var i = const_psetlistelement_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						var msgCode = GetNotNullString(msgCodeColumn, i, dt).Trim();
						if (!string.IsNullOrWhiteSpace(msgCode)) {
							var msgValue = dt.Rows[i][msgDisplayColumn.Index] as string;
							var localizationResource = new LocalizationResource {
								Culture = dt.Rows[i][msgCultureColumn.Index] as string,
								Key = msgCode,
								LocalId = msgCode,
								Value = msgValue
							};
							entityList.Add(localizationResource);
						}
						var localId = string.Format("ListName={0}MsgCode={1}", key, msgCode);
						var entity = new ListElement {
							LocalId = localId,
							MsgCode = msgCode,
							Value = dt.Rows[i][valueColumn.Index] as string
						};

						entityList.Add(entity);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private List<object> GetPsetDefinitionEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			var psetAttributeList = new List<object>();
			var psetDic = new Dictionary<string, object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_pset_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var psetDescriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "PsetDescription");
				var psetMsgCodeColumn = GetColumnByName(dt.TableName, columnsDefinition, "PsetMsgCode");
				var psetMsgCultureColumn = GetColumnByName(dt.TableName, columnsDefinition, "PsetMsgCulture");
				var psetMsgDisplayColumn = GetColumnByName(dt.TableName, columnsDefinition, "PsetMsgDisplay");
				var attributeIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeId");
				var attributeMsgCodeColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeMsgCode");
				var attributeMsgCultureColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeMsgCulture");
				var attributeMsgDisplayColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeMsgDisplay");
				var attributeOrderColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeOrder");
				var attributeDefaultValueColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeDefaultValue");
				var attributeComponentColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeComponent");
				var attributeAllowBlankColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeAllowBlank");
				var attributeDataTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeDataType");
				var attributeListIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeListId");
				var attributeWidthColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeWidth");
				var attributeMaxValueColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeMaxValue");
				var attributeMinValueColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeMinValue");
				var attributeMaskColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeMask");
				var attributeValidationFunctionColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeValidationFunction");
				var typeColumn = GetColumnByName(dt.TableName, columnsDefinition, "Type");
				var hasItsOwnTab = GetColumnByName(dt.TableName, columnsDefinition, "HasItsOwnTab");
				for (var i = const_pset_start_line + 1; i < sheet.Rows.Length; i++) {
					try {
						var psetName = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(psetName)) {
							break;
						}
						psetName = psetName.Trim();
						var psetMsgCode = dt.Rows[i][psetMsgCodeColumn.Index] as string;
						var attributeName = (dt.Rows[i][attributeIdColumn.Index] as string).Trim();
						var keyPsetAttributeDefinition = string.Format("PsetName={0}PsetAttributeName={1}", psetName, attributeName);
						var attributeMsgCode = dt.Rows[i][attributeMsgCodeColumn.Index] as string;
						var psetAttributeDefenition = new PsetAttributeDefinition {
							LocalId = keyPsetAttributeDefinition,
							KeyPropertySingleValue = keyPsetAttributeDefinition,
							KeyPropertySet = psetName,
							AttributeName = attributeName,
							AttributeOrder = (int)GetDouble(dt, i, attributeOrderColumn),
							AttributeDefaultValue = dt.Rows[i][attributeDefaultValueColumn.Index] as string,
							AttributeComponent = dt.Rows[i][attributeComponentColumn.Index] as string,
							AttributeAllowBlank = GetNullableBool(dt, i, attributeAllowBlankColumn),
							AttributeMsgCode = attributeMsgCode,
							AttributeDataType = dt.Rows[i][attributeDataTypeColumn.Index] as string,
							AttributeHasTab = GetNullableBool(dt, i, hasItsOwnTab),
							AttributeList = dt.Rows[i][attributeListIdColumn.Index] as string,
							AttributeWidth = (int?)GetNullableDouble(dt, i, attributeWidthColumn),
							AttributeMaxValue = dt.Rows[i][attributeMaxValueColumn.Index] as string,
							AttributeMinValue = dt.Rows[i][attributeMinValueColumn.Index] as string,
							AttributeMask = dt.Rows[i][attributeMaskColumn.Index] as string,
							AttributeVType = dt.Rows[i][attributeValidationFunctionColumn.Index] as string
						};
						PsetDefinition psetDefinition;
						if (psetDic.ContainsKey(psetName)) {
							psetDefinition = psetDic[psetName] as PsetDefinition;
						}
						else {
							psetDefinition = new PsetDefinition {
								LocalId = psetName,
								PsetName = psetName,
								UsageName = dt.Rows[i][typeColumn.Index] as string,
								PsetDescription = dt.Rows[i][psetDescriptionColumn.Index] as string,
							};
							psetDic.Add(psetName, psetDefinition);
						}

						if (!string.IsNullOrWhiteSpace(psetMsgCode)) {
							var msgValue = dt.Rows[i][psetMsgDisplayColumn.Index] as string;
							var localizationResource = new LocalizationResource {
								Culture = dt.Rows[i][psetMsgCultureColumn.Index] as string,
								Key = psetMsgCode,
								LocalId = psetMsgCode,
								Value = msgValue
							};
							entityList.Add(localizationResource);
							if (psetDefinition != null) psetDefinition.MsgCode = psetMsgCode;
						}

						if (!string.IsNullOrWhiteSpace(attributeMsgCode)) {
							attributeMsgCode = attributeMsgCode.Trim();
							var msgValue = dt.Rows[i][attributeMsgDisplayColumn.Index] as string;
							var localizationResource = new LocalizationResource {
								Culture = dt.Rows[i][attributeMsgCultureColumn.Index] as string,
								Key = attributeMsgCode,
								LocalId = attributeMsgCode,
								Value = msgValue
							};
							entityList.Add(localizationResource);
						}
						psetAttributeList.Add(psetAttributeDefenition);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			entityList.AddRange(psetDic.Select(x => x.Value));
			entityList.AddRange(psetAttributeList);
			return entityList;
		}

		private IEnumerable<object> GetMeterDataEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_meterdata_start_line].ItemArray.Select(x => x as string).ToList();
				var addressColumn = GetColumnByName(dt.TableName, columnsDefinition, "Address");
				var timestampColumn = GetColumnByName(dt.TableName, columnsDefinition, "TimeStamp");
				var valueColumn = GetColumnByName(dt.TableName, columnsDefinition, "Value");
				for (var i = const_meterdata_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var keyMeter = dt.Rows[i][addressColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(keyMeter)) {
							break;
						}
						var meterData = new MeterData {
							KeyMeter = keyMeter,
							AcquisitionDateTime = GetDateTime(dt, i, timestampColumn),
							Value = GetDouble(dt, i, valueColumn),
							Validity = m_DefaultMeterDataValidity
						};
						entityList.Add(meterData);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private static DataTable GetDataTable(IWorksheet sheet) {
			var dataTable = sheet.ExportDataTable(sheet.UsedRange, const_excel_export_data_table_options);
			return dataTable;
		}

		private IEnumerable<object> GetMeterValidationRuleEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_metervalidationrule_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				var enabledColumn = GetColumnByName(dt.TableName, columnsDefinition, "Enabled");
				var acquisitionFrequencyColumn = GetColumnByName(dt.TableName, columnsDefinition, "AcquisitionFrequency");
				var acquisitionIntervalColumn = GetColumnByName(dt.TableName, columnsDefinition, "AcquisitionInterval");
				var delayColumn = GetColumnByName(dt.TableName, columnsDefinition, "Delay");
				var delayIntervalColumn = GetColumnByName(dt.TableName, columnsDefinition, "DelayInterval");
				var increaseOnlyColumn = GetColumnByName(dt.TableName, columnsDefinition, "IncreaseOnly");
				var spikeDetectionColumn = GetColumnByName(dt.TableName, columnsDefinition, "SpikeDetection");
				var spikeDetectionAbsThresholdColumn = GetColumnByName(dt.TableName, columnsDefinition, "SpikeDetectionAbsThreshold");
				var maxRecurringValuesColumn = GetColumnByName(dt.TableName, columnsDefinition, "MaxRecurringValues");
				var minValueColumn = GetColumnByName(dt.TableName, columnsDefinition, "MinValue");
				var maxValueColumn = GetColumnByName(dt.TableName, columnsDefinition, "MaxValue");
				var emailsColumn = GetColumnByName(dt.TableName, columnsDefinition, "Emails");
				var keyLocalizationCultureColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocalizationCultureId");
				var startDateColumn = GetColumnByName(dt.TableName, columnsDefinition, "StartDate");
				var filterSpatialColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterSpatial");
				var filterMeterClassificationsColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterMeterClassifications");
				var metersColumn = GetColumnByName(dt.TableName, columnsDefinition, "Meters");
				var endDateColumn = GetColumnByName(dt.TableName, columnsDefinition, "EndDate");
				var lastProcessDateTimeColumn = GetColumnByName(dt.TableName, columnsDefinition, "LastProcessDateTime");
				for (var i = const_metervalidationrule_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						var entity = new MeterValidationRule {
							LocalId = key,
							Title = dt.Rows[i][titleColumn.Index] as string,
							Description = dt.Rows[i][descriptionColumn.Index] as string,
							Enabled = GetBool(dt, i, enabledColumn, true),
							AcquisitionFrequency = (int?)GetNullableDouble(dt, i, acquisitionFrequencyColumn),
							AcquisitionInterval = (dt.Rows[i][acquisitionIntervalColumn.Index] as string).ParseAsEnum<AxisTimeInterval>(),
							Delay = (int?)GetNullableDouble(dt, i, delayColumn),
							DelayInterval = (dt.Rows[i][delayIntervalColumn.Index] as string).ParseAsEnum<AxisTimeInterval>(),
							IncreaseOnly = GetBool(dt, i, increaseOnlyColumn),
							SpikeDetection = GetNullableDouble(dt, i, spikeDetectionColumn),
							SpikeDetectionAbsThreshold = GetNullableDouble(dt, i, spikeDetectionAbsThresholdColumn),
							MaxRecurringValues = (int?)GetNullableDouble(dt, i, maxRecurringValuesColumn),
							MinValue = GetNullableDouble(dt, i, minValueColumn),
							MaxValue = GetNullableDouble(dt, i, maxValueColumn),
							Emails = dt.Rows[i][emailsColumn.Index] as string,
							KeyLocalizationCulture = dt.Rows[i][keyLocalizationCultureColumn.Index] as string,
							StartDate = GetNullableDateTime(dt, i, startDateColumn),
							EndDate = GetNullableDateTime(dt, i, endDateColumn),
							LastProcessDateTime = GetNullableDateTime(dt, i, lastProcessDateTimeColumn),
							FilterMeterClassifications = GetMappableList<ClassificationItem>(filterMeterClassificationsColumn, dt, i),
							Meters = GetMappableList<Meter>(metersColumn, dt, i),
							FilterSpatial = GetCommaSeparetedLocationList(dt.Rows[i][filterSpatialColumn.Index] as string)
						};
						entityList.Add(entity);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetMeterOperationEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_meteroperation_start_line].ItemArray.Select(x => x as string).ToList();
				var meterIdIndexByName = GetColumnByName(dt.TableName, columnsDefinition, "MeterId");
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var operatorColumn = GetColumnByName(dt.TableName, columnsDefinition, "Operator");
				var orderColumn = GetColumnByName(dt.TableName, columnsDefinition, "Order");
				var function1Column = GetColumnByName(dt.TableName, columnsDefinition, "Function1");
				var meterId1Column = GetColumnByName(dt.TableName, columnsDefinition, "MeterId1");
				var factor1Column = GetColumnByName(dt.TableName, columnsDefinition, "Factor1");
				var offset1Column = GetColumnByName(dt.TableName, columnsDefinition, "Offset1");
				var meterOperation1Column = GetColumnByName(dt.TableName, columnsDefinition, "MeterOperation1");
				var function2Column = GetColumnByName(dt.TableName, columnsDefinition, "Function2");
				var meterId2Column = GetColumnByName(dt.TableName, columnsDefinition, "MeterId2");
				var factor2Column = GetColumnByName(dt.TableName, columnsDefinition, "Factor2");
				var offset2Column = GetColumnByName(dt.TableName, columnsDefinition, "Offset2");
				var meterOperation2Column = GetColumnByName(dt.TableName, columnsDefinition, "MeterOperation2");
				for (var i = const_meteroperation_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var keyMeter = dt.Rows[i][meterIdIndexByName.Index] as string;
						if (string.IsNullOrWhiteSpace(keyMeter)) {
							break;
						}
						keyMeter = keyMeter.Trim();
						var localId = dt.Rows[i][idColumn.Index] as string;
						localId = localId != null ? localId.Trim() : null;
						var function1 = dt.Rows[i][function1Column.Index] as string;
						AlgebricFunction algebricFunction1;
						Enum.TryParse(function1, true, out algebricFunction1);
						var function2 = dt.Rows[i][function2Column.Index] as string;
						AlgebricFunction algebricFunction2;
						Enum.TryParse(function2, true, out algebricFunction2);
						var arithmeticOperator = dt.Rows[i][operatorColumn.Index] as string;
						ArithmeticOperator arithmetic;
						Enum.TryParse(arithmeticOperator, true, out arithmetic);
						var meterData = new MeterOperation {
							KeyMeter = keyMeter,
							Order = (int)GetDouble(dt, i, orderColumn),
							KeyMeter1 = dt.Rows[i][meterId1Column.Index] as string,
							Factor1 = GetDouble(dt, i, factor1Column),
							Function1 = algebricFunction1,
							Offset1 = GetNullableDouble(dt, i, offset1Column),
							KeyMeterOperation1 = dt.Rows[i][meterOperation1Column.Index] as string,
							Operator = arithmetic,
							KeyMeter2 = dt.Rows[i][meterId2Column.Index] as string,
							Factor2 = GetNullableDouble(dt, i, factor2Column) ?? 1,
							Function2 = algebricFunction1,
							Offset2 = GetNullableDouble(dt, i, offset2Column),
							KeyMeterOperation2 = dt.Rows[i][meterOperation2Column.Index] as string,
							LocalId = localId
						};
						entityList.Add(meterData);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> AddMeterEntities(IWorksheet sheet, Tuple<Dictionary<string, object>, List<object>> spatialTuple) {
			var spatialEntities = spatialTuple.Item1;
			var metersWithOperationMeterList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_meter_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var keyMeterOperationResultColumn = GetColumnByName(dt.TableName, columnsDefinition, "KeyMeterOperationResult");
				var nameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Name");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				var keyClassificationItemColumn = GetColumnByName(dt.TableName, columnsDefinition, "KeyClassificationItem");
				var factorColumn = GetColumnByName(dt.TableName, columnsDefinition, "Factor");
				var locationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationId");
				var locationTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationType");
				var unitInputColumn = GetColumnByName(dt.TableName, columnsDefinition, "UnitInput");
				var unitOutputColumn = GetColumnByName(dt.TableName, columnsDefinition, "UnitOutput");
				var endpointFrequencyColumn = GetColumnByName(dt.TableName, columnsDefinition, "EndpointFrequency");
				var endpointTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "EndpointType");
				var keyEndpointColumn = GetColumnByName(dt.TableName, columnsDefinition, "KeyEndpoint");
				var offsetColumn = GetColumnByName(dt.TableName, columnsDefinition, "Offset");
				var isAccumulatedColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsAccumulated");
				var isAcquisitionDateTimeEndIntervalColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsAcquisitionDateTimeEndInterval");
				var groupOperationColumn = GetColumnByName(dt.TableName, columnsDefinition, "GroupOperation");
				var meterOperationTimeScaleIntervalColumn = GetColumnByName(dt.TableName, columnsDefinition, "MeterOperationTimeScaleInterval");
				var manualInputTimeIntervalColumn = GetColumnByName(dt.TableName, columnsDefinition, "ManualInputTimeInterval");
				var frequencyInputColumn = GetColumnByName(dt.TableName, columnsDefinition, "FrequencyInput");
				var rolloverValueColumn = GetColumnByName(dt.TableName, columnsDefinition, "RolloverValue");
				var degreeDayTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "DegreeDayType");
				var degreeDayBaseColumn = GetColumnByName(dt.TableName, columnsDefinition, "DegreeDayBase");
				var timeZoneIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "TimeZoneId");
				var timezones = TimeZoneInfo.GetSystemTimeZones();
				var psetNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "PsetName");
				var attributeNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeName");
				var psetFactorOperatorColumn = GetColumnByName(dt.TableName, columnsDefinition, "PsetFactorOperator");
				for (var i = const_meter_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						var groupOperation = dt.Rows[i][groupOperationColumn.Index] as string;
						MathematicOperator groupOperationEnum;
						Enum.TryParse(groupOperation, true, out groupOperationEnum);
						var meterOperationTimeScaleInterval = dt.Rows[i][meterOperationTimeScaleIntervalColumn.Index] as string;
						AxisTimeInterval meterOperationTimeScaleIntervalEnum;
						Enum.TryParse(meterOperationTimeScaleInterval, true, out meterOperationTimeScaleIntervalEnum);
						var manualInputTimeInterval = dt.Rows[i][manualInputTimeIntervalColumn.Index] as string;
						ManualInputTimeInterval manualInputTimeIntervalEnum;
						if (string.IsNullOrEmpty(manualInputTimeInterval))
							manualInputTimeIntervalEnum = ManualInputTimeInterval.Minutes;
						else
							Enum.TryParse(manualInputTimeInterval, true, out manualInputTimeIntervalEnum);

						var keyMeterOperationResult = dt.Rows[i][keyMeterOperationResultColumn.Index] as string;
						var timeZoneId = dt.Rows[i][timeZoneIdColumn.Index] as string;
						var timeZone = timezones.FirstOrDefault(x => x.Id == timeZoneId);
						var meter = new Meter {
							LocalId = key,
							KeyMeter = key,
							Name = dt.Rows[i][nameColumn.Index] as string,
							Description = dt.Rows[i][descriptionColumn.Index] as string,
							KeyClassificationItem = dt.Rows[i][keyClassificationItemColumn.Index] as string,
							Factor = GetDouble(dt, i, factorColumn, 1),
							KeyLocation = dt.Rows[i][locationIdColumn.Index] as string,
							LocationTypeName = (dt.Rows[i][locationTypeColumn.Index] as string).ParseAsEnum<HierarchySpatialTypeName>(),
							GroupOperation = groupOperationEnum,
							UnitInput = dt.Rows[i][unitInputColumn.Index] as string,
							UnitOutput = dt.Rows[i][unitOutputColumn.Index] as string,
							EndpointFrequency = (int?)GetNullableDouble(dt, i, endpointFrequencyColumn),
							EndpointType = string.Format("Vizelia.FOL.BusinessEntities.{0}DataAcquisitionEndpoint", dt.Rows[i][endpointTypeColumn.Index] as string),
							KeyEndpoint = dt.Rows[i][keyEndpointColumn.Index] as string,
							Offset = GetNullableDouble(dt, i, offsetColumn),
							IsAccumulated = GetBool(dt, i, isAccumulatedColumn),
							MeterOperationTimeScaleInterval = meterOperationTimeScaleIntervalEnum,
							ManualInputTimeInterval = manualInputTimeIntervalEnum,
							FrequencyInput = (int?)GetNullableDouble(dt, i, frequencyInputColumn),
							IsAcquisitionDateTimeEndInterval = GetBool(dt, i, isAcquisitionDateTimeEndIntervalColumn, true),
							RolloverValue = GetNullableDouble(dt, i, rolloverValueColumn),
							DegreeDayType = (dt.Rows[i][degreeDayTypeColumn.Index] as string).ParseAsEnum<DegreeDayType>(),
							DegreeDayBase = GetNullableDouble(dt, i, degreeDayBaseColumn),
							TimeZoneId = timeZone != null ? timeZone.Id : null,
							PsetName = dt.Rows[i][psetNameColumn.Index] as string,
							AttributeName = dt.Rows[i][attributeNameColumn.Index] as string,
							PsetFactorOperator = (dt.Rows[i][psetFactorOperatorColumn.Index] as string).ParseAsEnum<ArithmeticOperator>()
						};

						if (spatialEntities.ContainsKey(key)) {
							var spatialMeter = spatialEntities[key] as Meter;
							if (spatialMeter != null) {
								meter.PropertySetList = spatialMeter.PropertySetList;
								spatialEntities[key] = meter;
							}
						}
						else {
							spatialEntities.Add(key, meter);
						}

						if (!string.IsNullOrWhiteSpace(keyMeterOperationResult)) {
							var meterWithOperationMeter = meter.Clone();
							meterWithOperationMeter.KeyMeterOperationResult = keyMeterOperationResult;
							metersWithOperationMeterList.Add(meterWithOperationMeter);
						}
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return metersWithOperationMeterList;
		}

		private IEnumerable<object> GetEventLogEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_eventlog_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var nameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Name");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				var startDateTimeColumn = GetColumnByName(dt.TableName, columnsDefinition, "StartDateTime");
				var endDateTimeColumn = GetColumnByName(dt.TableName, columnsDefinition, "EndDateTime");
				var keyClassificationItemColumn = GetColumnByName(dt.TableName, columnsDefinition, "KeyClassificationItem");
				var locationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationId");
				var locationTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationType");
				for (var i = const_eventlog_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						var eventLog = new EventLog {
							LocalId = key,
							Name = dt.Rows[i][nameColumn.Index] as string,
							Description = dt.Rows[i][descriptionColumn.Index] as string,
							StartDate = GetDateTime(dt, i, startDateTimeColumn),
							EndDate = GetDateTime(dt, i, endDateTimeColumn),
							KeyClassificationItem = dt.Rows[i][keyClassificationItemColumn.Index] as string,
							KeyLocation = dt.Rows[i][locationIdColumn.Index] as string,
							LocationTypeName =
								(dt.Rows[i][locationTypeColumn.Index] as string).ParseAsEnum<HierarchySpatialTypeName>()
						};
						entityList.Add(eventLog);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetApplicationGroupEntities(IWorksheet sheet) {
			var entityList = new List<ApplicationGroup>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_applicationgroup_start_line].ItemArray.Select(x => x as string).ToList();
				var nameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Name");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				var usersIdsColumn = GetColumnByName(dt.TableName, columnsDefinition, "UsersIds");
				var rolesIdsColumn = GetColumnByName(dt.TableName, columnsDefinition, "RolesIds");
				for (var i = const_applicationgroup_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var key = dt.Rows[i][nameColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();

						var applicationGroup = new ApplicationGroup {
							LocalId = key,
							Name = key,
							Description = dt.Rows[i][descriptionColumn.Index] as string,
							Users = GetMappableList<FOLMembershipUser>(usersIdsColumn, dt, i),
							Roles = GetMappableList<AuthorizationItem>(rolesIdsColumn, dt, i)
						};
						entityList.Add(applicationGroup);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetOrganizationEntities(IWorksheet sheet) {
			var entityList = new List<Organization>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_company_start_line].ItemArray.Select(x => x as string).ToList();
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var nameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Name");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				for (var i = const_company_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var key = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}

						var organization = new Organization() {
							LocalId = key,
							Id = key,
							Name = dt.Rows[i][nameColumn.Index] as string,
							Description = dt.Rows[i][descriptionColumn.Index] as string,
						};
						entityList.Add(organization);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private static List<TEntity> GetMappableList<TEntity>(ExcelMappingConversionColumn idsColumn, DataTable dt, int i) where TEntity : IMappableEntity, new() {
			List<TEntity> retVal = null;
			var ids = dt.Rows[i][idsColumn.Index] as string;
			if (!string.IsNullOrWhiteSpace(ids)) {
				retVal = ids.Split(',').Select(id => new TEntity { LocalId = id.Trim() }).ToList();
			}
			return retVal;
		}

		private IEnumerable<object> GetProjectEntities(IWorksheet sheet) {
			var entityList = new List<AuthorizationItem>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_project_start_line].ItemArray.Select(x => x as string).ToList();
				var parentIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "ParentId");
				var nameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Name");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				var filterSpatialColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterSpatial");
				var filterChartColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterChart");
				var filterClassificationItemColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterClassificationItem");
				var filterPortalWindowColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterPortalWindow");
				//var filterReportColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterReport");
				var filterAlarmDefinitionColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterAlarmDefinition");
				var filterDynamicDisplayColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterDynamicDisplay");
				var filterPlaylistColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterPlaylist");
				var filterChartSchedulerColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterChartScheduler");
				var filterPortalTemplateColumn = GetColumnByName(dt.TableName, columnsDefinition, "FilterPortalTemplate");
				var applicationGroupsIdsColumn = GetColumnByName(dt.TableName, columnsDefinition, "ApplicationGroupsIds");
				var usersIdsColumn = GetColumnByName(dt.TableName, columnsDefinition, "UsersIds");
				for (var i = const_project_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var key = dt.Rows[i][nameColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						var parentId = dt.Rows[i][parentIdColumn.Index] as string;
						var project = new AuthorizationItem {
							ParentId = parentId == "Root" ? null : parentId,
							Name = key,
							ItemType = "Filter",
							Description = dt.Rows[i][descriptionColumn.Index] as string,
							FilterSpatial = GetFilters<Location>(dt.Rows[i][filterSpatialColumn.Index] as string),
							FilterChart = GetFilters<Chart>(dt.Rows[i][filterChartColumn.Index] as string),
							FilterClassificationItem = GetFilters<ClassificationItem>(dt.Rows[i][filterClassificationItemColumn.Index] as string),
							FilterPortalWindow = GetFilters<PortalWindow>(dt.Rows[i][filterPortalWindowColumn.Index] as string),
							//FilterReport = GetFilters<Report>(dt.Rows[i][filterReportColumn.Index] as string),
							FilterAlarmDefinition = GetFilters<AlarmDefinition>(dt.Rows[i][filterAlarmDefinitionColumn.Index] as string),
							FilterDynamicDisplay = GetFilters<DynamicDisplay>(dt.Rows[i][filterDynamicDisplayColumn.Index] as string),
							FilterPlaylist = GetFilters<Playlist>(dt.Rows[i][filterPlaylistColumn.Index] as string),
							FilterChartScheduler = GetFilters<ChartScheduler>(dt.Rows[i][filterChartSchedulerColumn.Index] as string),
							FilterPortalTemplate = GetFilters<PortalTemplate>(dt.Rows[i][filterPortalTemplateColumn.Index] as string),
							ApplicationGroups = GetMappableList<ApplicationGroup>(applicationGroupsIdsColumn, dt, i),
							Users = GetMappableList<FOLMembershipUser>(usersIdsColumn, dt, i)
						};
						entityList.Add(project);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetRoleEntities(IWorksheet sheet) {
			var entityList = new List<AuthorizationItem>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_role_start_line].ItemArray.Select(x => x as string).ToList();
				var parentIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "ParentId");
				var nameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Name");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				for (var i = const_role_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var key = dt.Rows[i][nameColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(key)) {
							break;
						}
						key = key.Trim();
						var parentId = dt.Rows[i][parentIdColumn.Index] as string;
						var tasks = new List<AuthorizationItem>();
						var taskIndex = 1;
						var taskColumn = GetColumnByName(dt.TableName, columnsDefinition, "Task" + taskIndex, false);
						while (taskColumn.Index != -1) {
							var taskId = dt.Rows[i][taskColumn.Index] as string;
							if (!string.IsNullOrWhiteSpace(taskId)) {
								tasks.Add(new AuthorizationItem { LocalId = taskId.Trim() });
							}
							taskIndex++;
							taskColumn = GetColumnByName(dt.TableName, columnsDefinition, "Task" + taskIndex, false);
						}

						var project = new AuthorizationItem {
							ParentId = parentId == "Root" ? null : parentId,
							Name = key,
							ItemType = "Role",
							Description = dt.Rows[i][descriptionColumn.Index] as string,
							Tasks = tasks.Count == 0 ? null : tasks
						};
						entityList.Add(project);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private static List<SecurableEntity> GetFilters<TEntity>(string filtersCsv) where TEntity : BaseBusinessEntity {
			var retVal = new List<SecurableEntity>();
			if (!string.IsNullOrWhiteSpace(filtersCsv)) {
				foreach (var value in filtersCsv.Split(',')) {
					SecurableEntity securableEntity;
					var type = typeof(TEntity);
					if (type == typeof(Location)) {
						var index = value.IndexOf('_');
						var locationType = value.Substring(0, index).Trim().ParseAsEnum<HierarchySpatialTypeName>();
						var locationLocalId = value.Substring(index + 1, value.Length - index - 1).Trim();
						securableEntity = new SecurableEntity { LocalId = locationLocalId, SecurableName = locationType.ToString() };
					}
					else {
						securableEntity = new SecurableEntity { LocalId = value.Trim() };
					}
					retVal.Add(securableEntity);
				}
			}
			return retVal;
		}

		private static List<Location> GetCommaSeparetedLocationList(string filtersCsv) {
			var retVal = new List<Location>();
			if (!string.IsNullOrWhiteSpace(filtersCsv)) {
				retVal.AddRange(from value in filtersCsv.Split(',')
								let index = value.IndexOf('_')
								let locationType = value.Substring(0, index).ParseAsEnum<HierarchySpatialTypeName>()
								let locationLocalId = value.Substring(index + 1, value.Length - index - 1)
								select new Location {
									LocalId = locationLocalId.Trim(),
									TypeName = locationType
								});
			}
			return retVal.Count > 0 ? retVal : null;
		}

		private Dictionary<string, object> GetClassificationItemEntities(IWorksheet sheet, int sheetStartLine, ClassificationItem root) {
			var entityDic = new Dictionary<string, object>();
			if (sheet != null) {
				var rand = new Random();
				var dt = GetDataTable(sheet);
				for (var i = sheetStartLine; i < sheet.Rows.Length; i++) {
					try {
						if (string.IsNullOrWhiteSpace(dt.Rows[i][0] as string)) {
							break;
						}
						var index = 0;
						var parentEntity = root;
						while (index < sheet.Columns.Length && !string.IsNullOrWhiteSpace(dt.Rows[i][index] as string) &&
							   parentEntity != null) {
							var key = dt.Rows[i][index] as string;
							if (!string.IsNullOrWhiteSpace(key)) {
								key = key.Trim();
								if (!entityDic.ContainsKey(key)) {
									var title = dt.Rows[i][index + 1] as string;
									var entity = new ClassificationItem {
										KeyParent = parentEntity.LocalId,
										KeyClassificationItem = key,
										LocalId = key,
										Title = title,
										ColorR = rand.Next(0, 255),
										ColorG = rand.Next(0, 255),
										ColorB = rand.Next(0, 255)
									};
									parentEntity = entity;
									entityDic.Add(key, entity);
								}
								else {
									parentEntity = entityDic[key] as ClassificationItem;
								}
							}
							index += 2;
						}
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityDic;
		}

		private Dictionary<string, object> GetSpatialEntities(IWorksheet sheet) {
			var entityDic = new Dictionary<string, object>();

			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_spatial_start_line].ItemArray;

				for (var i = const_spatial_start_line + 2; i < sheet.Rows.Length; i++) {
					try {
						if (string.IsNullOrWhiteSpace(dt.Rows[i][0] as string)) {
							break;
						}
						var index = 0;
						BaseBusinessEntity parentEntity = new Site();
						while (index < columnsDefinition.Length) {
							if (!string.IsNullOrWhiteSpace(dt.Rows[i][index] as string)) {
								var key = dt.Rows[i][index] as string;
								var name = dt.Rows[i][index + 1] as string;
								var locationType = columnsDefinition[index] as string;
								var entity = GetSpatialEntity(key, name, parentEntity, locationType);
								parentEntity = entity;
								if (!entityDic.ContainsKey(entity.GetKey())) {
									entityDic.Add(entity.GetKey(), entity);
								}
							}
							index += 2;
						}
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityDic;
		}

		private static BaseBusinessEntity GetSpatialEntity(string key, string name, BaseEntity parentEntity, string locationType) {
			key = key.Trim();
			BaseBusinessEntity entity;
			switch (locationType) {
				case "IfcSite":
					entity = new Site {
						KeySiteParent = parentEntity.GetKey(),
						KeySite = key,
						LocalId = key,
						Name = name,
						Description = name
					};
					break;
				case "IfcBuilding":
					entity = new Building {
						KeySite = parentEntity.GetKey(),
						KeyBuilding = key,
						LocalId = key,
						Name = name,
						Description = name,
						ObjectType = "VizBuilding"
					};
					break;
				case "IfcBuildingStorey":
					entity = new BuildingStorey {
						KeyBuilding = parentEntity.GetKey(),
						KeyBuildingStorey = key,
						LocalId = key,
						Name = name,
						Description = name,
						ObjectType = "VizBuildingStorey"
					};
					break;
				case "IfcSpace":
					entity = new Space {
						KeyBuildingStorey = parentEntity.GetKey(),
						KeySpace = key,
						LocalId = key,
						Name = name,
						Description = name
					};
					break;
				case "IfcFurniture":
					entity = new Furniture {
						KeyLocation = parentEntity.GetKey(),
						KeyFurniture = key,
						LocalId = key,
						Name = name,
						Description = name
					};
					break;
				case "IfcMeter":
					entity = new Meter {
						KeyLocation = parentEntity.GetKey(),
						KeyMeter = key,
						LocalId = key,
						Name = name,
						Description = name
					};
					break;
				default:
					throw new ExcelMappingConversionException("SPATIAL", string.Format("{0} is not allowed spatial type, this is case sensitive check.", locationType));
			}
			return entity;
		}

		private static ExcelMappingConversionColumn GetColumnByName(string sheetName, List<string> columnsDefinition, string columnName, bool throwExceptionIfNotFound = true) {
			var retVal = columnsDefinition.FindIndex(x => x == columnName);
			if (retVal == -1 && throwExceptionIfNotFound)
				throw new ExcelMappingConversionException(sheetName, string.Format("Expected column {0} was not found", columnName));
			return new ExcelMappingConversionColumn { Index = retVal, Name = columnName };
		}

		private static DateTime GetDateTime(DataTable dt, int i, ExcelMappingConversionColumn column) {
			DateTime retVal;
			//dt.Columns[column.Index].DataType = typeof (DateTime);
			var dateString = dt.Rows[i][column.Index] as string;

			try {
				if (dateString != null)
					retVal = DateTime.FromOADate(double.Parse(dateString));
				else
					throw new ExcelMappingConversionException(dt.TableName, string.Format("{0} at line {1} is empty", column.Name, i + 1));
			}
			catch (Exception) {
				throw new ExcelMappingConversionException(dt.TableName, string.Format("{0} at line {1} is not a date", column.Name, i + 1));
			}

			return retVal;
		}

		private static DateTime? GetNullableDateTime(DataTable dt, int i, ExcelMappingConversionColumn column) {
			if (!string.IsNullOrWhiteSpace(dt.Rows[i][column.Index] as string)) {
				return GetDateTime(dt, i, column);
			}
			return null;
		}

		private static double GetDouble(DataTable dt, int i, ExcelMappingConversionColumn column, double? defaultValue = null) {
			double retVal;
			if (!string.IsNullOrWhiteSpace(dt.Rows[i][column.Index] as string)) {
				if (!double.TryParse(dt.Rows[i][column.Index] as string, out retVal)) {
					throw new ExcelMappingConversionException(dt.TableName, string.Format("{0} at line {1} is not a number", column.Name, i + 1));
				}
			}
			else if (defaultValue.HasValue) {
				retVal = defaultValue.Value;
			}
			else {
				throw new ExcelMappingConversionException(dt.TableName, string.Format("{0} at line {1} is mandatory", column.Name, i + 1));
			}
			return retVal;
		}

		private static double? GetNullableDouble(DataTable dt, int i, ExcelMappingConversionColumn column) {
			if (!string.IsNullOrWhiteSpace(dt.Rows[i][column.Index] as string)) {
				return GetDouble(dt, i, column);
			}
			return null;
		}

		private static bool GetBool(DataTable dt, int i, ExcelMappingConversionColumn column, bool defaultValue = false) {
			bool retVal;
			if (string.IsNullOrWhiteSpace(dt.Rows[i][column.Index] as string))
				retVal = defaultValue;
			else if (!bool.TryParse(dt.Rows[i][column.Index] as string, out retVal))
				throw new ExcelMappingConversionException(dt.TableName, string.Format("{0} at line {1} is not a boolean (true/false)", column.Name, i + 1));
			return retVal;
		}

		private static bool? GetNullableBool(DataTable dt, int i, ExcelMappingConversionColumn column) {
			if (!string.IsNullOrWhiteSpace(dt.Rows[i][column.Index] as string)) {
				return GetBool(dt, i, column);
			}
			return null;
		}

		private IEnumerable<object> GetEnergyCertificateEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_energycertificate_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var nameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Name");
				var startColorColumn = GetColumnByName(dt.TableName, columnsDefinition, "StartColor");
				var endColorColumn = GetColumnByName(dt.TableName, columnsDefinition, "EndColor");

				for (var i = const_energycertificate_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var localId = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(localId)) {
							break;
						}
						var energyCertificate = new EnergyCertificate {
							LocalId = localId,
							Name = dt.Rows[i][nameColumn.Index] as string,
							StartColor = dt.Rows[i][startColorColumn.Index] as string,
							EndColor = dt.Rows[i][endColorColumn.Index] as string
						};
						entityList.Add(energyCertificate);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetEnergyCertificateCategoryEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_energycertificatecategory_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var nameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Name");
				var keyEnergyCertificateColumn = GetColumnByName(dt.TableName, columnsDefinition, "KeyEnergyCertificate");
				var minValueColumn = GetColumnByName(dt.TableName, columnsDefinition, "MinValue");
				var maxValueColumn = GetColumnByName(dt.TableName, columnsDefinition, "MaxValue");

				for (var i = const_energycertificatecategory_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var localId = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(localId)) {
							break;
						}
						var energyCertificateCategory = new EnergyCertificateCategory {
							LocalId = localId,
							Name = dt.Rows[i][nameColumn.Index] as string,
							KeyEnergyCertificate = dt.Rows[i][keyEnergyCertificateColumn.Index] as string,
							MinValue = (int)GetDouble(dt, i, minValueColumn),
							MaxValue = (int)GetDouble(dt, i, maxValueColumn)
						};
						entityList.Add(energyCertificateCategory);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetChartEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_chart_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				var isFavoriteColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsFavorite");
				var openOnStartupColumn = GetColumnByName(dt.TableName, columnsDefinition, "OpenOnStartup");
				var localizationColumn = GetColumnByName(dt.TableName, columnsDefinition, "Localization");
				var localizationSiteLevelColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocalizationSiteLevel");
				var useSpatialPathColumn = GetColumnByName(dt.TableName, columnsDefinition, "UseSpatialPath");
				var timeIntervalColumn = GetColumnByName(dt.TableName, columnsDefinition, "TimeInterval");
				var classificationLevelColumn = GetColumnByName(dt.TableName, columnsDefinition, "ClassificationLevel");
				var useClassificationPathColumn = GetColumnByName(dt.TableName, columnsDefinition, "UseClassificationPath");
				var startDateColumn = GetColumnByName(dt.TableName, columnsDefinition, "StartDate");
				var endDateColumn = GetColumnByName(dt.TableName, columnsDefinition, "EndDate");
				var dynamicTimeScaleEnabledColumn = GetColumnByName(dt.TableName, columnsDefinition, "DynamicTimeScaleEnabled");
				var dynamicTimeScaleFrequencyColumn = GetColumnByName(dt.TableName, columnsDefinition, "DynamicTimeScaleFrequency");
				var dynamicTimeScaleIntervalColumn = GetColumnByName(dt.TableName, columnsDefinition, "DynamicTimeScaleInterval");
				var dynamicTimeScaleCalendarColumn = GetColumnByName(dt.TableName, columnsDefinition, "DynamicTimeScaleCalendar");
				var dynamicTimeScaleCompleteIntervalColumn = GetColumnByName(dt.TableName, columnsDefinition, "DynamicTimeScaleCompleteInterval");
				var dynamicTimeScaleCalendarEndToNowColumn = GetColumnByName(dt.TableName, columnsDefinition, "DynamicTimeScaleCalendarEndToNow");
				var calendarModeColumn = GetColumnByName(dt.TableName, columnsDefinition, "CalendarMode");
				var timeZoneIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "TimeZoneId");

				for (var i = const_chart_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var localId = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(localId)) {
							break;
						}
						var chart = new Chart {
							LocalId = localId,
							Title = dt.Rows[i][titleColumn.Index] as string,
							Description = dt.Rows[i][descriptionColumn.Index] as string,
							IsFavorite = GetBool(dt, i, isFavoriteColumn, true),
							OpenOnStartup = GetBool(dt, i, openOnStartupColumn, true),
							Localisation = (dt.Rows[i][localizationColumn.Index] as string).ParseAsEnum<ChartLocalisation>(),
							LocalisationSiteLevel = (int)GetDouble(dt, i, localizationSiteLevelColumn),
							UseSpatialPath = GetBool(dt, i, useSpatialPathColumn, true),
							TimeInterval = (dt.Rows[i][timeIntervalColumn.Index] as string).ParseAsEnum<AxisTimeInterval>(),
							ClassificationLevel = (int)GetDouble(dt, i, classificationLevelColumn),
							UseClassificationPath = GetBool(dt, i, useClassificationPathColumn, true),
							StartDate = GetDateTime(dt, i, startDateColumn),
							EndDate = GetDateTime(dt, i, endDateColumn),
							DynamicTimeScaleEnabled = GetBool(dt, i, dynamicTimeScaleEnabledColumn, true),
							DynamicTimeScaleFrequency = (int)GetDouble(dt, i, dynamicTimeScaleFrequencyColumn),
							DynamicTimeScaleInterval = (dt.Rows[i][dynamicTimeScaleIntervalColumn.Index] as string).ParseAsEnum<AxisTimeInterval>(),
							DynamicTimeScaleCalendar = GetBool(dt, i, dynamicTimeScaleCalendarColumn, true),
							DynamicTimeScaleCompleteInterval = GetBool(dt, i, dynamicTimeScaleCompleteIntervalColumn, true),
							DynamicTimeScaleCalendarEndToNow = GetBool(dt, i, dynamicTimeScaleCalendarEndToNowColumn, true),
							CalendarMode = (dt.Rows[i][calendarModeColumn.Index] as string).ParseAsEnum<ChartCalendarMode>(),
							TimeZoneId = dt.Rows[i][timeZoneIdColumn.Index] as string
						};
						entityList.Add(chart);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetWeatherLocationEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_weatherlocation_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var nameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Name");
				var locationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationId");
				var isFavoriteColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsFavorite");
				var openOnStartupColumn = GetColumnByName(dt.TableName, columnsDefinition, "OpenOnStartup");
				var unitColumn = GetColumnByName(dt.TableName, columnsDefinition, "Unit");
				var displayTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "DisplayType");

				for (var i = const_weatherlocation_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var keyWeatherLocation = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(keyWeatherLocation)) {
							break;
						}
						var weatherLocation = new WeatherLocation {
							LocalId = keyWeatherLocation,
							Title = dt.Rows[i][titleColumn.Index] as string,
							Name = dt.Rows[i][nameColumn.Index] as string,
							LocationId = dt.Rows[i][locationIdColumn.Index] as string,
							IsFavorite = GetBool(dt, i, isFavoriteColumn, true),
							OpenOnStartup = GetBool(dt, i, openOnStartupColumn, true),
							Unit = (dt.Rows[i][unitColumn.Index] as string).ParseAsEnum<WeatherUnit>(),
							DisplayType = (dt.Rows[i][displayTypeColumn.Index] as string).ParseAsEnum<WeatherDisplayType>()
						};
						entityList.Add(weatherLocation);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetWebFrameEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_webframe_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var urlColumn = GetColumnByName(dt.TableName, columnsDefinition, "Url");
				var bodyColumn = GetColumnByName(dt.TableName, columnsDefinition, "Body");
				var isFavoriteColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsFavorite");
				var openOnStartupColumn = GetColumnByName(dt.TableName, columnsDefinition, "OpenOnStartup");

				for (var i = const_webframe_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var keyWebFrame = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(keyWebFrame)) {
							break;
						}
						var webFrame = new WebFrame {
							LocalId = keyWebFrame,
							Title = dt.Rows[i][titleColumn.Index] as string,
							Url = dt.Rows[i][urlColumn.Index] as string,
							Body = dt.Rows[i][bodyColumn.Index] as string,
							IsFavorite = GetBool(dt, i, isFavoriteColumn, true),
							OpenOnStartup = GetBool(dt, i, openOnStartupColumn, true)
						};
						entityList.Add(webFrame);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetMapEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_map_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var mapTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "MapType");
				var psetNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "PsetName");
				var attributeNameColumn = GetColumnByName(dt.TableName, columnsDefinition, "AttributeName");
				var isFavoriteColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsFavorite");
				var openOnStartupColumn = GetColumnByName(dt.TableName, columnsDefinition, "OpenOnStartup");
				var showDashboardColumn = GetColumnByName(dt.TableName, columnsDefinition, "ShowDashboard");
				var useSpatialIconsColumn = GetColumnByName(dt.TableName, columnsDefinition, "UseSpatialIcons");
				var locationIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationId");
				var locationTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "LocationType");
				var alarmClassificationColumn = GetColumnByName(dt.TableName, columnsDefinition, "AlarmClassification");


				for (var i = const_map_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var keyMap = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(keyMap)) {
							break;
						}
						var map = new Map {
							LocalId = keyMap,
							Title = dt.Rows[i][titleColumn.Index] as string,
							MapType = (dt.Rows[i][mapTypeColumn.Index] as string).ParseAsEnum<MapType>(),
							PsetName = dt.Rows[i][psetNameColumn.Index] as string,
							AttributeName = dt.Rows[i][attributeNameColumn.Index] as string,
							IsFavorite = GetBool(dt, i, isFavoriteColumn, true),
							OpenOnStartup = GetBool(dt, i, openOnStartupColumn, true),
							ShowDashboard = GetBool(dt, i, showDashboardColumn, true),
							UseSpatialIcons = GetBool(dt, i, useSpatialIconsColumn, true),
							KeyLocation = dt.Rows[i][locationIdColumn.Index] as string,
							LocationTypeName = (dt.Rows[i][locationTypeColumn.Index] as string).ParseAsEnum<HierarchySpatialTypeName>(),
							KeyClassificationItemAlarmDefinition = dt.Rows[i][alarmClassificationColumn.Index] as string
						};
						entityList.Add(map);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetDrawingCanvasEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_drawingcanvas_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var isFavoriteColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsFavorite");
				var openOnStartupColumn = GetColumnByName(dt.TableName, columnsDefinition, "OpenOnStartup");
				var displaySlidersColumn = GetColumnByName(dt.TableName, columnsDefinition, "DisplaySliders");

				for (var i = const_drawingcanvas_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var keyDrawingCanvas = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(keyDrawingCanvas)) {
							break;
						}
						var drawingCanvas = new DrawingCanvas {
							LocalId = keyDrawingCanvas,
							Title = dt.Rows[i][titleColumn.Index] as string,
							IsFavorite = GetBool(dt, i, isFavoriteColumn, true),
							OpenOnStartup = GetBool(dt, i, openOnStartupColumn, true),
							DisplaySliders = GetBool(dt, i, displaySlidersColumn, true)
						};
						entityList.Add(drawingCanvas);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetDrawingCanvasChartEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_drawingcanvaschart_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var drawingCanvasIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "DrawingCanvasId");
				var chartIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "ChartId");
				var disableAllSliders = GetColumnByName(dt.TableName, columnsDefinition, "DisableAllSliders");
				var heightColumn = GetColumnByName(dt.TableName, columnsDefinition, "Height");
				var widthColumn = GetColumnByName(dt.TableName, columnsDefinition, "Width");
				var xColumn = GetColumnByName(dt.TableName, columnsDefinition, "X");
				var yColumn = GetColumnByName(dt.TableName, columnsDefinition, "Y");
				var zIndexColumn = GetColumnByName(dt.TableName, columnsDefinition, "ZIndex");

				for (var i = const_drawingcanvaschart_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var localId = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(localId)) {
							break;
						}
						var drawingCanvasChart = new DrawingCanvasChart {
							LocalId = localId,
							KeyDrawingCanvas = dt.Rows[i][drawingCanvasIdColumn.Index] as string,
							KeyChart = dt.Rows[i][chartIdColumn.Index] as string,
							DisableAllSliders = GetBool(dt, i, disableAllSliders, true),
							Height = (int)GetDouble(dt, i, heightColumn),
							Width = (int)GetDouble(dt, i, widthColumn),
							X = (int)GetDouble(dt, i, xColumn),
							Y = (int)GetDouble(dt, i, yColumn),
							ZIndex = (int)GetDouble(dt, i, zIndexColumn)
						};
						entityList.Add(drawingCanvasChart);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetDrawingCanvasImageEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_drawingcanvasimage_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var drawingCanvasIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "DrawingCanvasId");
				var imageIdColumn = GetColumnByName(dt.TableName, columnsDefinition, "ImageId");
				var heightColumn = GetColumnByName(dt.TableName, columnsDefinition, "Height");
				var widthColumn = GetColumnByName(dt.TableName, columnsDefinition, "Width");
				var xColumn = GetColumnByName(dt.TableName, columnsDefinition, "X");
				var yColumn = GetColumnByName(dt.TableName, columnsDefinition, "Y");
				var zIndexColumn = GetColumnByName(dt.TableName, columnsDefinition, "ZIndex");

				for (var i = const_drawingcanvasimage_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var localId = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(localId)) {
							break;
						}
						var drawingCanvasImage = new DrawingCanvasImage {
							LocalId = localId,
							KeyDrawingCanvas = dt.Rows[i][drawingCanvasIdColumn.Index] as string,
							KeyImage = dt.Rows[i][imageIdColumn.Index] as string,
							Height = (int)GetDouble(dt, i, heightColumn),
							Width = (int)GetDouble(dt, i, widthColumn),
							X = (int)GetDouble(dt, i, xColumn),
							Y = (int)GetDouble(dt, i, yColumn),
							ZIndex = (int)GetDouble(dt, i, zIndexColumn)
						};
						entityList.Add(drawingCanvasImage);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetAlarmTableEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_alarmtable_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var alarmInstanceStatusColumn = GetColumnByName(dt.TableName, columnsDefinition, "AlarmInstanceStatus");
				var isFavoriteColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsFavorite");
				var openOnStartupColumn = GetColumnByName(dt.TableName, columnsDefinition, "OpenOnStartup");
				var alarmDefinitionColumn = GetColumnByName(dt.TableName, columnsDefinition, "AlarmDefinition");

				for (var i = const_alarmtable_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var keyAlarmTable = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(keyAlarmTable)) {
							break;
						}
						var alarmTable = new AlarmTable {
							LocalId = keyAlarmTable,
							Title = dt.Rows[i][titleColumn.Index] as string,
							IsFavorite = GetBool(dt, i, isFavoriteColumn, true),
							OpenOnStartup = GetBool(dt, i, openOnStartupColumn, true),
							AlarmInstanceStatus = (dt.Rows[i][alarmInstanceStatusColumn.Index] as string).ParseAsEnum<AlarmInstanceStatus>(),
							AlarmDefinitions = GetMappableList<AlarmDefinition>(alarmDefinitionColumn, dt, i)
						};
						entityList.Add(alarmTable);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetFlipCardEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_flipcard_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var isFavoriteColumn = GetColumnByName(dt.TableName, columnsDefinition, "IsFavorite");
				var openOnStartupColumn = GetColumnByName(dt.TableName, columnsDefinition, "OpenOnStartup");
				var randomColumn = GetColumnByName(dt.TableName, columnsDefinition, "Random");
				var flipOnRefreshColumn = GetColumnByName(dt.TableName, columnsDefinition, "FlipOnRefresh");

				for (var i = const_flipcard_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var keyFlipCard = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(keyFlipCard)) {
							break;
						}
						var flipCard = new FlipCard {
							LocalId = keyFlipCard,
							Title = dt.Rows[i][titleColumn.Index] as string,
							IsFavorite = GetBool(dt, i, isFavoriteColumn, true),
							OpenOnStartup = GetBool(dt, i, openOnStartupColumn, true),
							Random = GetBool(dt, i, randomColumn, true),
							FlipOnRefresh = GetBool(dt, i, flipOnRefreshColumn, true)
						};
						entityList.Add(flipCard);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetFlipCardEntryEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_flipcardentry_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var keyFlipCardColumn = GetColumnByName(dt.TableName, columnsDefinition, "FlipCard");
				var orderColumn = GetColumnByName(dt.TableName, columnsDefinition, "Order");
				var typeEntityColumn = GetColumnByName(dt.TableName, columnsDefinition, "PortletType");
				var keyEntityColumn = GetColumnByName(dt.TableName, columnsDefinition, "PortletId");

				for (var i = const_flipcardentry_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var localId = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(localId)) {
							break;
						}
						var flipCardEntry = new FlipCardEntry {
							LocalId = localId,
							KeyFlipCard = dt.Rows[i][keyFlipCardColumn.Index] as string,
							Order = (int)GetDouble(dt, i, orderColumn, 0),
							TypeEntity = string.Concat("Vizelia.FOL.BusinessEntities.", dt.Rows[i][typeEntityColumn.Index] as string),
							KeyEntity = dt.Rows[i][keyEntityColumn.Index] as string
						};
						entityList.Add(flipCardEntry);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetPortalWindowEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_portalwindow_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var openOnStartupColumn = GetColumnByName(dt.TableName, columnsDefinition, "OpenOnStartup");
				var portalTabColumnConfigColumn = GetColumnByName(dt.TableName, columnsDefinition, "PortalTabColumnConfig");
				var displayInToolbarColumn = GetColumnByName(dt.TableName, columnsDefinition, "DisplayInToolbar");
				var preloadTabColumn = GetColumnByName(dt.TableName, columnsDefinition, "PreloadTab");
				var iconClsColumn = GetColumnByName(dt.TableName, columnsDefinition, "IconCls");

				for (var i = const_portalwindow_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var keyPortalWindow = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(keyPortalWindow)) {
							break;
						}
						var portalWindow = new PortalWindow() {
							LocalId = keyPortalWindow,
							Title = dt.Rows[i][titleColumn.Index] as string,
							OpenOnStartup = GetBool(dt, i, openOnStartupColumn, true),
							PortalTabColumnConfig = (dt.Rows[i][portalTabColumnConfigColumn.Index] as string).ParseAsEnum<PortalTabColumnConfig>(),
							DisplayInToolbar = GetBool(dt, i, displayInToolbarColumn, true),
							PreloadTab = GetBool(dt, i, preloadTabColumn, true),
							IconCls = dt.Rows[i][iconClsColumn.Index] as string,
						};
						entityList.Add(portalWindow);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}


		private IEnumerable<object> GetLinkEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_link_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var linkValueColumn = GetColumnByName(dt.TableName, columnsDefinition, "LinkValue");
				var keyImageColumn = GetColumnByName(dt.TableName, columnsDefinition, "ImageLocalId");
				var keyParentColumn = GetColumnByName(dt.TableName, columnsDefinition, "ParentLocalId");
				var msgCodeColumn = GetColumnByName(dt.TableName, columnsDefinition, "MsgCode");
				var msgDisplayColumn = GetColumnByName(dt.TableName, columnsDefinition, "MsgDisplay");
				var msgCultureColumn = GetColumnByName(dt.TableName, columnsDefinition, "MsgCulture");
				var groupNameMsgCodeColumn = GetColumnByName(dt.TableName, columnsDefinition, "GroupNameMsgCode");
				var groupNameMsgDisplayColumn = GetColumnByName(dt.TableName, columnsDefinition, "GroupNameMsgDisplay");
				var groupNameMsgCultureColumn = GetColumnByName(dt.TableName, columnsDefinition, "GroupNameMsgCulture");
				var iconClsColumn = GetColumnByName(dt.TableName, columnsDefinition, "IconCls");

				for (var i = const_link_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var keyLink = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(keyLink)) {
							break;
						}

						var msgCode = dt.Rows[i][msgCodeColumn.Index] as string;
						var groupNameMsgCode = dt.Rows[i][groupNameMsgCodeColumn.Index] as string;
						var link = new Link() {
							LocalId = keyLink,
							LinkValue = dt.Rows[i][linkValueColumn.Index] as string,
							KeyImage = dt.Rows[i][keyImageColumn.Index] as string,
							KeyParent = dt.Rows[i][keyParentColumn.Index] as string,
							MsgCode = msgCode,
							GroupNameMsgCode = groupNameMsgCode,
							IconCls = dt.Rows[i][iconClsColumn.Index] as string,
						};

						if (!string.IsNullOrWhiteSpace(msgCode)) {
							var msgValue = dt.Rows[i][msgDisplayColumn.Index] as string;
							var localizationResource = new LocalizationResource {
								Culture = dt.Rows[i][msgCultureColumn.Index] as string,
								Key = msgCode,
								LocalId = msgCode,
								Value = msgValue
							};
							entityList.Add(localizationResource);
						}

						if (!string.IsNullOrWhiteSpace(groupNameMsgCode)) {
							var msgGroupNameValue = dt.Rows[i][groupNameMsgDisplayColumn.Index] as string;
							var groupNameLocalizationResource = new LocalizationResource {
								Culture = dt.Rows[i][groupNameMsgCultureColumn.Index] as string,
								Key = groupNameMsgCode,
								LocalId = groupNameMsgCode,
								Value = msgGroupNameValue
							};
							entityList.Add(groupNameLocalizationResource);
						}

						entityList.Add(link);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetPortalTabEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_portaltab_start_line].ItemArray.Select(x => x as string).ToList();

				var portalWindowIdIndexByName = GetColumnByName(dt.TableName, columnsDefinition, "PortalWindowId");
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var columnConfigColumn = GetColumnByName(dt.TableName, columnsDefinition, "ColumnConfig");
				var footerHeightColumn = GetColumnByName(dt.TableName, columnsDefinition, "FooterHeight");
				var headerHeightColumn = GetColumnByName(dt.TableName, columnsDefinition, "HeaderHeight");
				var orderColumn = GetColumnByName(dt.TableName, columnsDefinition, "Order");
				var backgroundClsColumn = GetColumnByName(dt.TableName, columnsDefinition, "BackgroundCls");

				for (var i = const_portaltab_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var keyPortalWindow = dt.Rows[i][portalWindowIdIndexByName.Index] as string;
						var localId = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(localId)) {
							break;
						}
						if (string.IsNullOrWhiteSpace(keyPortalWindow)) {
							throw new ExcelMappingConversionException(sheet.Name, "missing field - PortalWindowId");
						}

						var portalTab = new PortalTab {
							KeyPortalWindow = keyPortalWindow,
							LocalId = localId,
							Title = dt.Rows[i][titleColumn.Index] as string,
							ColumnConfig = (dt.Rows[i][columnConfigColumn.Index] as string).ParseAsEnum<PortalTabColumnConfig>(),
							FooterHeight = (int?)GetNullableDouble(dt, i, footerHeightColumn),
							HeaderHeight = (int?)GetNullableDouble(dt, i, headerHeightColumn),
							Order = (int)GetDouble(dt, i, orderColumn),
							BackgroundCls = dt.Rows[i][backgroundClsColumn.Index] as string,
						};
						entityList.Add(portalTab);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetPortalColumnEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_portalcolumn_start_line].ItemArray.Select(x => x as string).ToList();

				var portalTabIdIndexByName = GetColumnByName(dt.TableName, columnsDefinition, "PortalTabId");
				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var typeColumn = GetColumnByName(dt.TableName, columnsDefinition, "Type");
				var orderColumn = GetColumnByName(dt.TableName, columnsDefinition, "Order");
				var flexColumn = GetColumnByName(dt.TableName, columnsDefinition, "Flex");

				for (var i = const_portalcolumn_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var keyPortalTab = dt.Rows[i][portalTabIdIndexByName.Index] as string;
						var localId = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(localId)) {
							break;
						}
						if (string.IsNullOrWhiteSpace(keyPortalTab)) {
							throw new ExcelMappingConversionException(sheet.Name, "missing field - PortalTabId");
						}

						var portalColumn = new PortalColumn {
							KeyPortalTab = keyPortalTab,
							LocalId = localId,
							Type = (dt.Rows[i][typeColumn.Index] as string).ParseAsEnum<PortalColumnType>(),
							Flex = (int)GetDouble(dt, i, flexColumn),
							Order = (int)GetDouble(dt, i, orderColumn),
						};
						entityList.Add(portalColumn);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetPortalTemplateEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_portaltemplate_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var titleColumn = GetColumnByName(dt.TableName, columnsDefinition, "Title");
				var descriptionColumn = GetColumnByName(dt.TableName, columnsDefinition, "Description");
				var usersColumn = GetColumnByName(dt.TableName, columnsDefinition, "Users");
				var portalWindowsColumn = GetColumnByName(dt.TableName, columnsDefinition, "PortalWindows");

				for (var i = const_portaltemplate_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var localId = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(localId)) {
							break;
						}

						var portalTemplate = new PortalTemplate {
							LocalId = localId,
							Title = dt.Rows[i][titleColumn.Index] as string,
							Description = dt.Rows[i][descriptionColumn.Index] as string,
							Users = GetMappableList<FOLMembershipUser>(usersColumn, dt, i),
							PortalWindows = GetMappableList<PortalWindow>(portalWindowsColumn, dt, i),
						};
						entityList.Add(portalTemplate);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetPlaylistEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_playlist_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var nameColumn = GetColumnByName(dt.TableName, columnsDefinition, "Name");
				var cultureColumn = GetColumnByName(dt.TableName, columnsDefinition, "Culture");
				var heightColumn = GetColumnByName(dt.TableName, columnsDefinition, "Height");
				var widthColumn = GetColumnByName(dt.TableName, columnsDefinition, "Width");

				for (var i = const_playlist_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var localId = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(localId)) {
							break;
						}

						var playlist = new Playlist {
							LocalId = localId,
							Name = dt.Rows[i][nameColumn.Index] as string,
							KeyLocalizationCulture = dt.Rows[i][cultureColumn.Index] as string,
							Height = (int)GetDouble(dt, i, heightColumn),
							Width = (int)GetDouble(dt, i, widthColumn)
						};
						entityList.Add(playlist);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetPlaylistPageEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_playlistpage_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var keyPlaylistColumn = GetColumnByName(dt.TableName, columnsDefinition, "Playlist");
				var keyPortalTabColumn = GetColumnByName(dt.TableName, columnsDefinition, "Tab");
				var orderColumn = GetColumnByName(dt.TableName, columnsDefinition, "Order");
				var durationColumn = GetColumnByName(dt.TableName, columnsDefinition, "Duration");

				for (var i = const_playlistpage_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var localId = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(localId)) {
							break;
						}

						var playlistPage = new PlaylistPage {
							LocalId = localId,
							KeyPlaylist = dt.Rows[i][keyPlaylistColumn.Index] as string,
							KeyPortalTab = dt.Rows[i][keyPortalTabColumn.Index] as string,
							Order = (int)GetDouble(dt, i, orderColumn),
							Duration = (int)GetDouble(dt, i, durationColumn)
						};
						entityList.Add(playlistPage);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}

		private IEnumerable<object> GetVirtualFileEntities(IWorksheet sheet) {
			var entityList = new List<object>();
			if (sheet != null) {
				var dt = GetDataTable(sheet);
				var columnsDefinition = dt.Rows[const_virtualfile_start_line].ItemArray.Select(x => x as string).ToList();

				var idColumn = GetColumnByName(dt.TableName, columnsDefinition, "Id");
				var pathColumn = GetColumnByName(dt.TableName, columnsDefinition, "Path");
				var entityTypeColumn = GetColumnByName(dt.TableName, columnsDefinition, "EntityType");
				var keyEntityColumn = GetColumnByName(dt.TableName, columnsDefinition, "KeyEntity");
				var maxRecordCountColumn = GetColumnByName(dt.TableName, columnsDefinition, "MaxRecordCount");
				var heightColumn = GetColumnByName(dt.TableName, columnsDefinition, "Height");
				var widthColumn = GetColumnByName(dt.TableName, columnsDefinition, "Width");

				for (var i = const_virtualfile_start_line + 1; i < dt.Rows.Count; i++) {
					try {
						var localId = dt.Rows[i][idColumn.Index] as string;
						if (string.IsNullOrWhiteSpace(localId)) {
							break;
						}

						var virtualFile = new VirtualFile {
							LocalId = localId,
							Path = dt.Rows[i][pathColumn.Index] as string,
							EntityType = string.Concat("Vizelia.FOL.BusinessEntities.", dt.Rows[i][entityTypeColumn.Index] as string),
							KeyEntity = dt.Rows[i][keyEntityColumn.Index] as string,
							MaxRecordCount = GetDouble(dt, i, maxRecordCountColumn) == 0 ? 1000 : (int)GetDouble(dt, i, maxRecordCountColumn),
							Height = GetDouble(dt, i, heightColumn) == 0 ? 600 : (int)GetDouble(dt, i, heightColumn, 0),
							Width = GetDouble(dt, i, widthColumn) == 0 ? 800 : (int)GetDouble(dt, i, widthColumn, 0)
						};
						entityList.Add(virtualFile);
					}
					catch (Exception e) {
						m_ErrorList.Add(new MappingConversionError { ErrorMessage = e.Message });
					}
				}
			}
			return entityList;
		}
	}

	internal struct ExcelMappingConversionColumn {
		internal int Index;
		internal string Name;
	}

	internal class ExcelMappingConversionException : ApplicationException {
		private readonly string m_Message;
		public ExcelMappingConversionException(string sheetName, string exceptionText) {
			m_Message = string.Format("Error in sheet {0}: {1}", sheetName, exceptionText);
		}

		public override string Message {
			get {
				return m_Message;
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {

	/// <summary>
	/// A mapping provider that reads XML serialized entities. 
	/// Reads the entities by deserializing the mapping entities specified in the schema attributes.
	/// </summary>
	public class XmlSerializationMappingProvider : SchemaBasedMappingProvider {
		/// <summary>
		/// The tag name of the action node in mapping file.
		/// </summary>
		private const string const_action_tagname = "action";
		/// <summary>
		/// The attribute name of the action node in mapping file.
		/// </summary>
		private const string const_action_type_attributname = "type";
		/// <summary>
		/// The name of the schema name attribute
		/// </summary>
		private const string const_schema_attribute_name = "schema";
		/// <summary>
		/// The name of the schema version attribute
		/// </summary>
		private const string const_version_attribute_name = "version";

		/// <summary>
		/// The mapping file header
		/// </summary>
		private const string const_mapping_file_header = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>\r\n<!--Mapping document-->\r\n<XML>\r\n<action type=\"update\">";

		/// <summary>
		/// The mapping file footer
		/// </summary>
		private const string const_mapping_file_footer = "</action>\r\n</XML>";

	    /// <summary>
	    /// Deserializes the entities.
	    /// </summary>
	    /// <param name="mappingFile">The mapping file.</param>
	    /// <param name="summary">The summary.</param>
	    /// <returns>
	    /// The deserialized entities.
	    /// </returns>
	    protected override IEnumerable<IMappingRecordConverter> DeserializeEntities(Stream mappingFile, IMappingSummary summary) {
			var query = from c in StreamElements(mappingFile, summary)
						select c;

			foreach (Tuple<XElement,string> o in query) {
				IMappingRecordConverter mapping = null;

				try {
					mapping = DeserializeEntity(o.Item1);
					mapping.Action = o.Item2;
				}
				catch (Exception ex) {
					summary.LogError(ex, Langue.msg_mapping_error_readingentity);
					TracingService.Write(ex);
				}

				if (mapping != null) {
					yield return mapping;
				}
			}
		}

		/// <summary>
		/// Deserializes the entity and returns its mapping entity.
		/// </summary>
		/// <param name="serializedEntity">The serialized entity.</param>
		/// <returns>A deserialized entity.</returns>
		private IMappingRecordConverter DeserializeEntity(XElement serializedEntity) {
			var typeToDeserialize = GetSerializedEntityType(serializedEntity);
			var entityXml = GetEntityXml(serializedEntity, typeToDeserialize);

			// DateTime deserialization here expects ISO-8601 input, for example 2012-09-18T10:01Z or 2012-09-18T10:01+0200.
			// If neither a UTC statement ("Z") or a timezone offset is specified, then it will parse the time as local.
			// In order to support a datetime input without timezone support that will be treated as UTC, the server must be running 
			// in UTC Timezone, so that the local time will actually be UTC.

			object mappingEntityObject = Helper.DeserializeXml(entityXml, typeToDeserialize);
			var mapping = (IMappingRecordConverter)mappingEntityObject;

			return mapping;
		}

		/// <summary>
		/// Gets the type of the serialized entity by checking the XML element name
		/// and schema attributes against CLR classes that use the MappingSchema attribute.
		/// </summary>
		/// <param name="serializedEntity">The serialized entity.</param>
		private Type GetSerializedEntityType(XElement serializedEntity) {
			// Get entity name, schema name and schema version
			string entityName = serializedEntity.Name.LocalName;
			XAttribute schemaNameAttribute = serializedEntity.Attributes().Where(a => a.Name.LocalName.ToLower().Equals(const_schema_attribute_name)).FirstOrDefault();
			XAttribute schemaVersionAttribute = serializedEntity.Attributes().Where(a => a.Name.LocalName.ToLower().Equals(const_version_attribute_name)).FirstOrDefault();

			// Use default values if these attributes are not specified.
			string schemaName = (schemaNameAttribute != null) ? schemaNameAttribute.Value : const_default_schema_name;
			string schemaVersion = (schemaVersionAttribute != null) ? schemaVersionAttribute.Value : const_default_schema_version;

			Type mappingType = GetMatchingMappingType(entityName, schemaName, schemaVersion);

			return mappingType;
		}

		/// <summary>
		/// Gets the entity XML according to the given type.
		/// </summary>
		/// <param name="serializedEntity">The serialized entity.</param>
		/// <param name="typeToDeserialize">The type to deserialize.</param>
		private static string GetEntityXml(XElement serializedEntity, Type typeToDeserialize) {
			// The entity element is abstracted using the MappingSchema approach, 
			// we need to replace it  with the concrete mapping entity we want to deserialize.

			var builder = new StringBuilder();
			foreach (var node in serializedEntity.Nodes()) {
				var nodeText = node.ToString();
				builder.Append(nodeText);
			}

			var innerXml = builder.ToString();
			string entityXml = string.Format("<{0}>{1}</{0}>", typeToDeserialize.Name, innerXml);

			return entityXml;
		}

		/// <summary>
		/// Streams the elements from the mapping file.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="summary">The summary.</param>
		/// <returns></returns>
		private static IEnumerable<Tuple<XElement, string>> StreamElements(Stream input, IMappingSummary summary)
		{
		    input.Seek(0, SeekOrigin.Begin);

			var settings = new XmlReaderSettings {IgnoreComments = true, IgnoreWhitespace = true, IgnoreProcessingInstructions = true, CloseInput = false};
			using (XmlReader reader = XmlReader.Create(input, settings)) {
				reader.MoveToContent();

				while (reader.Read()) {
					if ((reader.Name == const_action_tagname) && (reader.NodeType == XmlNodeType.Element)) {
						string action = reader.GetAttribute(const_action_type_attributname);
						int depth = reader.Depth;
						reader.Read();

						while (!reader.EOF && reader.NodeType != XmlNodeType.Element) {
							reader.Read();
						}

						while (!reader.EOF && reader.Name != action && reader.Depth > depth) {
							if (reader.NodeType == XmlNodeType.Element) {
								summary.CountRecord();
								var element = (XElement)XNode.ReadFrom(reader);
								yield return new Tuple<XElement, string>(element, action);
							}
						}
					}
				}

				reader.Close();
			}
		}

		/// <summary>
		/// Serializes the mappings.
		/// </summary>
		/// <param name="mappingInstances">The mapping instances.</param>
		/// <returns></returns>
		protected override string SerializeMappings(IEnumerable<object> mappingInstances) {
			var sb = new StringBuilder();
			sb.AppendLine(const_mapping_file_header);

			foreach (object instance in mappingInstances) {
				string serialized = SerializeMapping(instance);
				sb.Append(serialized);
			}

			sb.AppendLine(const_mapping_file_footer);

			return sb.ToString();
		}

		/// <summary>
		/// Serializes the mapping.
		/// </summary>
		/// <param name="mappingInstance">The mapping instance.</param>
		protected override string SerializeMapping(object mappingInstance) {
			string xml = Helper.SerializeXml(mappingInstance);
			XDocument document = XDocument.Parse(xml);
			var schemaAttribute = Helper.GetAttribute<MappingSchemaAttribute>(mappingInstance.GetType());
			XElement root = document.Root;

			if (root == null) {
				throw new InvalidOperationException("Cannot convert to mapping schema based XML object: No root for serialized XML document");
			}

			// Switch the concrete class name serialized into the XML root element to a schema mapping element
			root.Name = schemaAttribute.Entity;
			root.RemoveAttributes();
			root.SetAttributeValue(XName.Get(const_schema_attribute_name), schemaAttribute.Name);
			root.SetAttributeValue(XName.Get(const_version_attribute_name), schemaAttribute.Version);

			var elementXml = root.ToString();

			return elementXml;
		}

		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<string> GetAllowedMappingFileExtensions() {
			return new List<string> { "xml" };
		}

	    /// <summary>
	    /// Determines whether the provider can map the specified file or not.
	    /// </summary>
	    /// <param name="filename">The filename.</param>
	    /// <param name="mappingFile">The mapping file.</param>
	    /// <returns>
	    ///   <c>true</c> if the provider can map the specified file; otherwise, <c>false</c>.
	    /// </returns>
	    public override bool CanMapFile(string filename, Stream mappingFile) {
			// First of all lets check that the file is an XML file.
			bool isXml = base.CanMapFile(filename, mappingFile);

			if (!isXml) {
				return false;
			}

			// Now we check its header to make sure its our format of XML file.
			bool isValid = IsValidSchemaBasedMappingFile(mappingFile);

			return isValid;
		}

		/// <summary>
		/// Determines whether the specified mapping file is a valid schema based mapping file by checking its contents.
		/// </summary>
		/// <param name="mappingFile">The mapping file.</param>
		/// <returns>
		///   <c>true</c> if specified mapping file is a valid schema based mapping file; otherwise, <c>false</c>.
		/// </returns>
		protected virtual bool IsValidSchemaBasedMappingFile(Stream mappingFile) {
			mappingFile.Seek(0, SeekOrigin.Begin);

			bool isValidSchemaBasedMappingFile = false;

			try {
                // loading XDocument from the reader doesn't seem to work really, not sure why
				XDocument xDocument = XDocument.Load(mappingFile);

				if (xDocument.Root != null && xDocument.Root.Name.LocalName.ToLower().Equals("xml")) {
					XElement firstChildElement = xDocument.Root.Elements().FirstOrDefault();
					if (firstChildElement != null && firstChildElement.Name.LocalName.ToLower().Equals("action")) {
						if (IsValidIdentity(xDocument))
							isValidSchemaBasedMappingFile = true;
					}
				}
			}
			catch {
				// We failed to parse the file, it may not be XML, although its extension is.
			}

			return isValidSchemaBasedMappingFile;

		}

		/// <summary>
		/// Allows subclasses to further explore the XML to see if it is specific
		/// to that provider. For example, V & E includes a header comment
		/// with its name, allowing us to have a V & E specific provider.
		/// </summary>
		/// <remarks>
		/// <para>When CanMapFile identifies a valid file, there may be a comment line
		/// before the XML tag that uniquely identifies the source of this data.
		/// If a provider supports this, it will override and use the results to setup.</para>
		/// <para>To work, there must be a comment line as the very first line
		/// after the schema directive and it must start with "UserAgent= followed
		/// by the name/version where version is digits period digits
		/// such as "UserAgent=V and E/1.0"</para>
		/// <para>This requirement of UserAgent is because we cannot guarantee the
		/// ordering of loaded providers in the MappingService. So this base class
		/// will NOT support any XML file with the UserAgent string, allowing
		/// it's CanMapFile to be evaluated before the desired provider,
		/// but passes it along for the desired provider to see.</para>
		/// </remarks>
		/// <param name="xDocument">The XML of the file loaded as an XDocument</param>
		/// <returns>When true, the file is valid. This class always returns true.</returns>
		protected virtual bool IsValidIdentity(XDocument xDocument) {
			if (xDocument.FirstNode.NodeType == System.Xml.XmlNodeType.Comment) {
				const string const_UserAgentHeader = "UserAgent=";
				string signature = IdentitySignature();
				if (String.IsNullOrEmpty(signature)) {
					// not allowed to find a comment with UserAgent=
					Regex userAgentRE = new Regex(Regex.Escape(const_UserAgentHeader));
					return !userAgentRE.IsMatch(((XComment)xDocument.FirstNode).Value);
				}
				else {
					Regex signatureRE = new Regex(Regex.Escape(const_UserAgentHeader) + 
						Regex.Escape(signature) + @"/\d+\.\d+");
					return signatureRE.IsMatch(((XComment)xDocument.FirstNode).Value);

				}
			}
			return true;
		}

		/// <summary>
		/// Used by subclasses that need to recognize an XML mapping
		/// file differently by injecting a comment with 
		/// UserAgent=[the value returned here]/[version info].
		/// If this returns "", then no UserAgent is allowed.
		/// If the UserAgent is found when this returns "", the file
		/// should not be used by this provider, allowing the desired
		/// provider to eventually match to it.
		/// </summary>
		/// <returns></returns>
		protected virtual string IdentitySignature() {
			return String.Empty;
		}
	}
}
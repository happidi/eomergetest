﻿using System;

namespace Vizelia.FOL.ConcreteProviders {
	
	/// <summary>
	/// A class that represents an error during CSV deserialization.
	/// </summary>
	public class CsvDeserializationErrorEventArgs : EventArgs {
		/// <summary>
		/// Gets or sets the error.
		/// </summary>
		/// <value>
		/// The error.
		/// </value>
		public Exception Error { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="CsvDeserializationErrorEventArgs"/> class.
		/// </summary>
		/// <param name="exception">The exception.</param>
		public CsvDeserializationErrorEventArgs(Exception exception) {
			Error = exception;
		}
	}
}
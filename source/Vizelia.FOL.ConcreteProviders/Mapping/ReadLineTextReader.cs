using System.IO;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A TextReader class that implements itself on an inhereting TextReader that can perform ReadLine() instead of Read().
	/// The inhereting reader will read line by line, and this reader will emulate single character reads using a buffer for the line.
	/// Code taken from: http://blogs.msdn.com/b/jmstall/archive/2005/08/06/readlinetextreader.aspx
	/// </summary>
	internal abstract class ReadLineTextReader : TextReader {
		/// <summary>
		/// Reads the next character without changing the state of the reader or the character source. 
		/// Returns the next available character without actually reading it from the input stream.
		/// </summary>
		/// <returns>
		/// An integer representing the next character to be read, or -1 if no more characters are available or the stream does not support seeking.
		/// </returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.IO.TextReader"/> is closed. </exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		public override int Peek() {
			FillCharCache();
			return m_CharCache;
		}

		/// <summary>
		/// Reads the next character from the input stream and advances the character position by one character.
		/// </summary>
		/// <returns>
		/// The next character from the input stream, or -1 if no more characters are available. The default implementation returns -1.
		/// </returns>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.IO.TextReader"/> is closed. </exception>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		public override int Read() {
			FillCharCache();
			int ch = m_CharCache;
			ClearCharCache();
			return ch;
		}

		#region Character cache support
		private int m_CharCache = -2; // -2 means the cache is empty. -1 means eof.

		/// <summary>
		/// Clears the character cache.
		/// </summary>
		private void ClearCharCache() {
			m_CharCache = -2;
		}

		/// <summary>
		/// Fills the character cache.
		/// </summary>
		private void FillCharCache() {
			if (m_CharCache != -2) return; // cache is already full
			m_CharCache = GetNextCharWorker();
		}

		#endregion

		#region Worker to get next signle character from a ReadLine()-based source
		private int m_CurrentCharIndex = int.MaxValue;
		private string m_Line;
	
		/// <summary>
		/// Reads a line of characters from the current stream and returns the data as a string.
		/// </summary>
		/// <returns>
		/// The next line from the input stream, or null if all characters have been read (EOF).
		/// </returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception>
		/// <exception cref="T:System.OutOfMemoryException">There is insufficient memory to allocate a buffer for the returned string. </exception>
		/// <exception cref="T:System.ObjectDisposedException">The <see cref="T:System.IO.TextReader"/> is closed. </exception>
		/// <exception cref="T:System.ArgumentOutOfRangeException">The number of characters in the next line is larger than <see cref="F:System.Int32.MaxValue"/></exception>
		public abstract override string ReadLine();

		/// <summary>
		/// Gets the next char and advances the cursor.
		/// </summary>
		private int GetNextCharWorker() {
			// Return the current character
			if (m_Line == null) {
				m_Line = ReadLine(); // virtual
				m_CurrentCharIndex = 0;

				if (m_Line == null) {
					return -1; // EOF
				}

				m_Line += "\r\n"; // need to readd the newline that ReadLine() stripped off
			}

			char c = m_Line[m_CurrentCharIndex];
			m_CurrentCharIndex++;

			if (m_CurrentCharIndex >= m_Line.Length) {
				m_Line = null; // tell us next time around to get a new line.
			}

			return c;
		}

		#endregion
	}
}
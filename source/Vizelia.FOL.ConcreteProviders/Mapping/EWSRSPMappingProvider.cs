﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.ConcreteProviders.Mapping.Entities.EWS.RSP;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// An EWS RSP Mapping Provider.
	/// </summary>
	public class EWSRSPMappingProvider : BaseMappingProvider {
		private const string const_new_meters_site_local_id = "newMetersSiteLocalId";
		private const string const_new_meters_classification_local_id = "newMetersClassificationLocalId";
		private const string const_custom_meter_type = "Custom";
		private const string const_meter_data_valid_state = "Valid";

		private string m_NewMetersSiteLocalId;
		private string m_NewMetersClassificationItemLocalId;

		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<string> GetAllowedMappingFileExtensions() {
			return new List<string> { "xml" };
		}

		/// <summary>
		/// Exports the specified business entities.
		/// </summary>
		/// <param name="businessEntities">The business entities.</param>
		/// <returns></returns>
		public override string Export(List<object> businessEntities) {
			return string.Empty;
		}

		/// <summary>
		/// Wraps the object with thier co-responding mappings.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetMappings(List<object> entities) {
			throw new NotSupportedException();
		}

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, NameValueCollection config) {
			base.Initialize(name, config);

			m_NewMetersSiteLocalId = GetMandatoryAttribute(config, const_new_meters_site_local_id);
			m_NewMetersClassificationItemLocalId = GetMandatoryAttribute(config, const_new_meters_classification_local_id);
		}

		private static string GetMandatoryAttribute(NameValueCollection config, string attributeName) {
			string attributeValue = config[attributeName];

			if (String.IsNullOrWhiteSpace(attributeValue)) {
				throw new ArgumentException(string.Format("Missing value of {0} attribute of EWS mapping provider.", attributeName));
			}

			return attributeValue;
		}


	    /// <summary>
	    /// Deserializes the entities.
	    /// </summary>
	    /// <param name="mappingFile">The mapping file.</param>
	    /// <param name="summary">The summary.</param>
	    /// <returns>
	    /// The deserialized entities.
	    /// </returns>
	    protected override IEnumerable<IMappingRecordConverter> DeserializeEntities(Stream mappingFile, IMappingSummary summary) {
			var query = StreamElements(mappingFile, summary);

			foreach (XElement o in query) {
				IEnumerable<IMappingRecordConverter> mappings = null;

				try {
					mappings = DeserializeEntitiesFromElement(o);
				}
				catch (Exception ex) {
					summary.LogError(ex, Langue.msg_mapping_error_readingentity);
					TracingService.Write(ex);
				}

				foreach (var mapping in mappings) {
					mapping.Action = MappingConstants.const_action_type_create;
					yield return mapping;
				}
			}

		}

		/// <summary>
		/// Deserializes the entities from element.
		/// </summary>
		/// <param name="serializedEntity">The serialized entity.</param>
		/// <returns></returns>
		private IEnumerable<IMappingRecordConverter> DeserializeEntitiesFromElement(XElement serializedEntity) {
			var typeToDeserialize = GetSerializedEntityType(serializedEntity);
			string xml = GetEntityXml(serializedEntity, typeToDeserialize);

			// We expect dates in this XML format to be in ISO-8601 format and in UTC time.
			object ewsEntity = Helper.DeserializeXml(xml, typeToDeserialize);
			var converters = ConvertRspEntity(ewsEntity);

			return converters;
		}

		/// <summary>
		/// Gets the entity XML according to the given type.
		/// </summary>
		/// <param name="serializedEntity">The serialized entity.</param>
		/// <param name="typeToDeserialize">The type to deserialize.</param>
		private static string GetEntityXml(XElement serializedEntity, Type typeToDeserialize) {
			// The entity element name is not its .NET type, we need to change its element.
			var builder = new StringBuilder();

			foreach (var node in serializedEntity.Nodes()) {
				var nodeText = node.ToString();
				builder.Append(nodeText);
			}

			var innerXml = builder.ToString();
			string entityXml = string.Format("<{0}>{1}</{0}>", typeToDeserialize.Name, innerXml);

			return entityXml;
		}

		private IEnumerable<IMappingRecordConverter> ConvertRspEntity(object ewsEntity) {
			IEnumerable<IMappingRecordConverter> items;

			// Try to convert to Meter items.
			var containerItem = ewsEntity as ContainerItemType;

			if (containerItem != null) {
				items = GetMeters(containerItem);
			}
			else {
				// Try to convert to MeterData items.
				var measurements = ewsEntity as HistoryRecordsType;

				if (measurements != null) {
					items = GetMeterDatas(measurements);
				}
				else {
					items = Enumerable.Empty<IMappingRecordConverter>();
				}
			}

			return items;
		}

		private IEnumerable<IMappingRecordConverter> GetMeterDatas(HistoryRecordsType measurements) {
			string meterLocalId = measurements.ValueItemId;
			IEnumerable<DefaultMeterDataMapping> meterDatas;

			if (measurements.List != null) {
				meterDatas = measurements.List.Select(m => ConvertHistoryRecordToMeterData(m, meterLocalId));
			}
			else {
				meterDatas = Enumerable.Empty<DefaultMeterDataMapping>();
			}

			return meterDatas;
		}

		private static DefaultMeterDataMapping ConvertHistoryRecordToMeterData(HistoryRecordType measurement, string meterLocalId) {
			bool isValid = string.IsNullOrEmpty(measurement.State) || const_meter_data_valid_state.Equals(measurement.State);

			// It's better to use the mapping object than the business entity because on update it won't overwrite everything.
			var meterDataMapping = new DefaultMeterDataMapping {
				MeterLocalId = meterLocalId,
				AcquisitionDateTime = measurement.TimeStamp,
				Validity = isValid ? MeterDataValidity.Valid : MeterDataValidity.Invalid,
				Value = double.Parse(measurement.Value, CultureInfo.InvariantCulture)
			};

			return meterDataMapping;
		}

		private IEnumerable<IMappingRecordConverter> GetMeters(ContainerItemType containerItem) {
			IEnumerable<DefaultMeterMapping> meters;

			if (containerItem.Type.Equals("Equipment") && containerItem.Items != null && containerItem.Items.ValueItems != null && containerItem.Items.ValueItems.Length > 0) {
				// Build description for meter out of metadata of container item.
				string metadata = string.Empty;

				if (containerItem.Metadata != null) {
					foreach (var m in containerItem.Metadata) {
						metadata += string.Format("{0}: {1}\r\n", m.Name, m.Value);
					}
				}
				
				// Convert child value items to meters.
				meters = containerItem.Items.ValueItems.Select(v => ConvertValueItemToMeter(metadata, v));
			}
			else {
				meters = Enumerable.Empty<DefaultMeterMapping>();
			}

			return meters;
		}

		private DefaultMeterMapping ConvertValueItemToMeter(string metadata, ValueItemTypeBase valueItem) {
			// It's better to use the mapping object than the business entity because on update it won't overwrite everything.
			var meter = new RSPMeterMapping {
				LocalId = valueItem.Id,
				Name = valueItem.Name,
				LocationTypeName = HierarchySpatialTypeName.IfcSite,
				LocationLocalId = m_NewMetersSiteLocalId,
				ClassificationItemLocalId = m_NewMetersClassificationItemLocalId,
				Factor = 1,
				IsAccumulated = false, //TODO: get from metadata
				IsAcquisitionDateTimeEndInterval = true
			};

			if (valueItem.ItemsElementName != null && valueItem.ItemsElementName.Length == 2 && valueItem.Items != null && valueItem.Items.Length == 2) {
				// Add these properties to the metadata.
				for (int i = 0; i < valueItem.Items.Length; i++) {
					metadata += string.Format("{0}: {1}\r\n", valueItem.ItemsElementName[i], valueItem.Items[i]);
				}

				meter.Description = valueItem.Description + "\r\n" + metadata;

				if (valueItem.ItemsElementName[0].Equals(ItemsChoiceType.RefSpecification) && valueItem.ItemsElementName[1].Equals(ItemsChoiceType.RefId)) {
					// Some logic here, some day...
				}

				if (valueItem.ItemsElementName[0].Equals(ItemsChoiceType.Type) && valueItem.ItemsElementName[1].Equals(ItemsChoiceType.Unit)) {
					if (const_custom_meter_type.Equals(valueItem.Items[0])) {
						// Custom unit type provided.
						meter.UnitOutput = valueItem.Items[1];
					}
				}

			}

			return meter;
		}

		/// <summary>
		/// Gets the type of the serialized entity.
		/// </summary>
		/// <param name="serializedEntity">The serialized entity.</param>
		/// <returns></returns>
		private Type GetSerializedEntityType(XElement serializedEntity) {
			string typeName = string.Format("Vizelia.FOL.ConcreteProviders.Mapping.Entities.EWS.RSP.{0}Type", serializedEntity.Name.LocalName);
			Type type = typeof(EWSRSPMappingProvider).Assembly.GetType(typeName, false);

			if (type == null) {
				string message = string.Format("Could not find entity type for {0} element.", serializedEntity.Name.LocalName);
				throw new VizeliaException(message);
			}

			return type;
		}

		/// <summary>
		/// Streams the elements from the mapping file.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <param name="summary">The summary.</param>
		/// <returns></returns>
		private static IEnumerable<XElement> StreamElements(Stream input, IMappingSummary summary)
		{
		    input.Seek(0, SeekOrigin.Begin);

			var settings = new XmlReaderSettings { IgnoreComments = true, IgnoreWhitespace = true, IgnoreProcessingInstructions = true, CloseInput = false};
			using (XmlReader reader = XmlReader.Create(input, settings)) {
				while (reader.Read()) {
					if (reader.NodeType == XmlNodeType.Element) {
						int depth = reader.Depth;
						reader.Read();

						// Read 2nd level elements.
						while (!reader.EOF && reader.Depth > depth) {
							if (reader.NodeType == XmlNodeType.Element) {
								summary.CountRecord();
								var element = (XElement)XNode.ReadFrom(reader);
								yield return element;
							}
						}
					}
				}

				reader.Close();
			}
		}

		/// <summary>
		/// Gets the entity mapping texts.
		/// </summary>
		/// <param name="entityType">The entity type.</param>
		protected override string GetEntityMappingTexts(Type entityType) {
			return string.Empty;
		}

	    /// <summary>
	    /// Determines whether the provider can map the specified file or not.
	    /// </summary>
	    /// <param name="filename">The filename.</param>
	    /// <param name="mappingFile">The mapping file.</param>
	    /// <returns>
	    ///   <c>true</c> if the provider can map the specified file; otherwise, <c>false</c>.
	    /// </returns>
	    public override bool CanMapFile(string filename, Stream mappingFile) {
			// First of all lets check that the file is an XML file.
			bool isXml = base.CanMapFile(filename, mappingFile);

			if (!isXml) {
				return false;
			}

			// Now we check its header to make sure its our format of XML file.
			bool isValid = IsValidEWSMappingFile(mappingFile);

			return isValid;
		}

		/// <summary>
		/// Determines whether the specified mapping file is a valid EWS mapping file by checking its contents.
		/// </summary>
		/// <param name="mappingFile">The mapping file.</param>
		/// <returns>
		///   <c>true</c> if specified mapping file is a valid EWS mapping file; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsValidEWSMappingFile(Stream mappingFile) {
			mappingFile.Seek(0, SeekOrigin.Begin);

			bool isValid = false;

			try {
				XDocument xDocument = XDocument.Load(mappingFile);
				XElement firstElement = xDocument.Elements().FirstOrDefault();
				isValid = firstElement != null && firstElement.Name.Namespace.NamespaceName.ToLower().StartsWith("http://www.schneider-electric.com/common/rsp/");
			}
			catch {
				// We failed to parse the file, it may not be XML, although its extension is.
			}

			return isValid;
		}

	}


}
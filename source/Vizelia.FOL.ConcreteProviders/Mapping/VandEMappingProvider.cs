﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Vizelia.FOL.ConcreteProviders {

	/// <summary>
	/// Variation of XmlSerializationMappingProvider built for the 
	/// V&E brick. Always insert this provider before the XmlSerializationMappingProvider
	/// in the web.config file's mapping providers list.
	/// </summary>
	/// <remarks>
	/// <para>V&E needs to change the Validity field on existing Meter Data records
	/// to Invalid at times and the default XmlSerializationMappingProvider uses
	/// a filter to skip those records. </para>
	/// <para>V&E's mapping file includes an XML comment in the header
	/// that identifies it. The VandEMappingProvider uses that comment 
	/// to select itself with the CanMapFile method.</para>
	/// </remarks>
	public class VandEMappingProvider : XmlSerializationMappingProvider {
		/// <summary>
		/// Eliminates support of the "filter" configuration parameter.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);
			m_FilterNames = null;
		}

		/// <summary>
		/// Eliminates support of the "filter" configuration parameter.
		/// </summary>
		/// <param name="converter"></param>
		/// <returns></returns>
		protected override bool IncludeMappingRecord(BusinessEntities.IMappingRecordConverter converter) {
			return true;
		}

		private const string cVandESignature = "Schneider Electric Validation and Estimation Module";

		/// <summary>
		/// Look for the XML comment that includes "UserAgent=Schneider Electric Validation and Estimation Module/#.#"
		/// where #.# are digits representing the version.
		/// </summary>
		protected override string IdentitySignature() {
			return cVandESignature;
		}

	}
}

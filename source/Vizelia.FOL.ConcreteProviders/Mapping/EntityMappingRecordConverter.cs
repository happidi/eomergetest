﻿using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// The entity mapping record converter, which wraps a business entity.
	/// </summary>
	/// <typeparam name="TEntity">The type of the entity.</typeparam>
	internal class EntityMappingRecordConverter<TEntity> : IMappingRecordConverter where TEntity : IMappableEntity {
		private readonly TEntity m_Entity;

		/// <summary>
		/// Initializes a new instance of the <see cref="EntityMappingRecordConverter&lt;TEntity&gt;"/> class.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public EntityMappingRecordConverter(TEntity entity) {
			m_Entity = entity;
		}

		/// <summary>
		/// Gets the deserialized mappable entity.
		/// </summary>
		/// <returns></returns>
		public IMappingRecord GetMappingRecord() {
			return new OverwritingMappingRecord<TEntity>(m_Entity);
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<IMappingRecordConverter> GetChildMappings() {
			return Enumerable.Empty<IMappingRecordConverter>();
		}

		/// <summary>
		/// The action of the mapping record.
		/// </summary>
		/// <value>
		/// The action.
		/// </value>
		public string Action { get; set; }

	}
}

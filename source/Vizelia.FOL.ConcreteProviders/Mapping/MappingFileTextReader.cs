using System.IO;

namespace Vizelia.FOL.ConcreteProviders {

	/// <summary>
	/// A TextReader for flat CSV files that have mapping headers.
	/// This is a wrapper for an existing StreamReader that provides custom EOF
	/// when next mapping header appears in file.
	/// </summary>
	internal class MappingFileTextReader : ReadLineTextReader {

		private readonly StreamReader m_WrappedReader;
		private readonly string m_EOFStatement;
		private bool m_IsFragmentEOF;

		/// <summary>
		/// Gets the last line read.
		/// </summary>
		public string LastLineRead { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="MappingFileTextReader"/> class.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <param name="eofStatement">The EOF statement.</param>
		public MappingFileTextReader(StreamReader reader, string eofStatement) {
			m_WrappedReader = reader;
			m_EOFStatement = eofStatement;
		}

		/// <summary>
		/// Reads a line of characters from the current stream and returns the data as a string.
		/// </summary>
		/// <returns>
		/// The next line from the input stream, or null if all characters have been read.
		/// </returns>
		/// <exception cref="T:System.IO.IOException">An I/O error occurs. </exception><exception cref="T:System.OutOfMemoryException">There is insufficient memory to allocate a buffer for the returned string. </exception><exception cref="T:System.ObjectDisposedException">The <see cref="T:System.IO.TextReader"/> is closed. </exception><exception cref="T:System.ArgumentOutOfRangeException">The number of characters in the next line is larger than <see cref="F:System.Int32.MaxValue"/></exception><filterpriority>1</filterpriority>
		public override string ReadLine() {
			if(m_IsFragmentEOF) {
				return null;
			}

			string outputLine = null;

			// A single ReadLine call can actually read several lines if they don't contain relevant content.
			while(!m_WrappedReader.EndOfStream) {
				string line = m_WrappedReader.ReadLine();
				
				if(line == null) {
					break;
				}

				if (line.StartsWith(m_EOFStatement)) {
					// No more lines to read. End of mapping fragment or Stream EOF.
					LastLineRead = line;
					m_IsFragmentEOF = true;
					break;
				}

				if(!line.StartsWith("//") && !string.IsNullOrWhiteSpace(line)) {
					// A line with contents to read.
					outputLine = line;
					break;
				}
			}

			return outputLine;
		}

		/// <summary>
		/// Skips the fragment.
		/// </summary>
		public void SkipFragment() {
			// This will read file contents until fragment EOF is reached.
			while(Read() != -1) {
			}
		}
	}
}
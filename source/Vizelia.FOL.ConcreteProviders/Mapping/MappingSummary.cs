using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Mail;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Practices;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Represents the summary of the mapping process.
	/// </summary>
	public class MappingSummary : IMappingSummary {
		/// <summary>
		/// The name of the category for trace corresponding to the mapping header.
		/// </summary>
		private const string const_category_mappingheader = "MappingHeader";

		/// <summary>
		/// The name of the category for trace corresponding to the mapping report.
		/// </summary>
		private const string const_category_mappingreport = "MappingReport";

		/// <summary>
		/// The name of the category for trace corresponding to the mapping row.
		/// </summary>
		private const string const_category_mappingrow = "MappingRow";

		/// <summary>
		/// The name of the extended property of logEntry containing the name of the mapping file when used in conjonction with VizeliaStringBuilderTraceListener.
		/// </summary>
		private const string const_extendedproperty_filename = "filename";

		/// <summary>
		/// The name of the extended property of logEntry containing the name of the machine when used in conjonction with VizeliaStringBuilderTraceListener.
		/// </summary>
		private const string const_extendedproperty_machine_name = "machinename";

		/// <summary>
		/// The name of the extended property of logEntry containing the localid when used in conjonction with VizeliaStringBuilderTraceListener.
		/// </summary>
		private const string const_extendedproperty_localid = "localid";

		/// <summary>
		/// The name of the extended property of logEntry containing the toAddress when used in conjonction with VizeliaMailTraceListener.
		/// </summary>
		private const string const_extendedproperty_toaddress = "toAddress";

		private const string const_number_of_files_text = "Number of files:\t{0}";
		private const string const_elapsed_time_text = "Elapsed time:\t{0}";
		private const string const_number_of_records_text = "Number of records:\t{0}";
		private const string const_number_of_errors_text = "Number of errors:\t{0}";
		private const string const_number_of_create_text = "Number of create:\t{0}";
		private const string const_number_of_update_text = "Number of update:\t{0}";
		private const string const_number_of_entities_text = "Number of entities:\t{0}";
		private const string const_number_of_success_text = "Number of success:\t{0}";
		private const string const_number_of_destroy_text = "Number of destroy:\t{0}";
		private const string const_summary_remark_text = "* Some records may contain several entities within them.";
		private const string const_totals_header = "TOTALS:";
		private const string const_log_filename_date_time_format_string = "yyyy'-'MM'-'dd' 'HH'-'mm'-'ss.fffffffK";
		private const string const_number_of_skipped_text = "Number of skipped:\t{0}";
		private const string const_offsets_of_skipped_text = "Record offsets of skipped records:";
		
		private Stopwatch m_Stopwatch;
		private Stopwatch m_TotalStopwatch;

		/// <summary>
		/// Initializes a new instance of the <see cref="MappingSummary"/> class.
		/// </summary>
		public MappingSummary() {
			MappingLog = new StringBuilder();
			ShouldSendReport = true;
		}

		/// <summary>
		/// Gets the create count.
		/// </summary>
		private int m_CreateCount;

		/// <summary>
		/// Gets the total create count.
		/// </summary>
		public int TotalCreateCount { get; private set; }

		/// <summary>
		/// Gets the destory count.
		/// </summary>
		private int m_DestoryCount;

		/// <summary>
		/// Gets the total destory count.
		/// </summary>
		public int TotalDestoryCount { get; private set; }

		/// <summary>
		/// Gets the error count.
		/// </summary>
		private int m_ErrorCount;

		/// <summary>
		/// Gets the total error count.
		/// </summary>
		public int TotalErrorCount { get; private set; }

		/// <summary>
		/// Gets the row count.
		/// </summary>
		private int m_RecordCount;

		/// <summary>
		/// Gets the total record count.
		/// </summary>
		public int TotalRecordCount { get; private set; }

		/// <summary>
		/// Gets the entity count.
		/// </summary>
		private int m_EntityCount;

		/// <summary>
		/// Gets the total entity count.
		/// </summary>
		public int TotalEntityCount { get; private set; }

		/// <summary>
		/// Gets the success count.
		/// </summary>
		private int m_SuccessCount;

		/// <summary>
		/// Gets the total success count.
		/// </summary>
		public int TotalSuccessCount { get; private set; }

		/// <summary>
		/// Gets the update count.
		/// </summary>
		private int m_UpdateCount;

		/// <summary>
		/// Gets the total update count.
		/// </summary>
		public int TotalUpdateCount { get; private set; }

		/// <summary>
		/// A list of record offsets that reflect records skipped.
		/// Offset 0 is the first record. The count is the total skipped.
		/// </summary>
		private List<int> m_SkippedRecords;

		/// <summary>
		/// Gets the total skipped record count.
		/// </summary>
		public int TotalSkippedRecordCount { get; private set; }

		/// <summary>
		/// The file count.
		/// </summary>
		public int FileCount { get; private set; }

		/// <summary>
		/// Gets the total elapsed time.
		/// </summary>
		public TimeSpan TotalElapsedTime {
			get { return m_TotalStopwatch.Elapsed; }
		}

		/// <summary>
		/// Indicates whether the mapping of a file is in progress or not.
		/// </summary>
		private bool m_IsFileMappingInProgress;

		private bool m_IsMappingInProgress;

		private string m_Emails;
		/// <summary>
		/// Gets a value indicating whether this instance is cancelled.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is cancelled; otherwise, <c>false</c>.
		/// </value>
		public bool IsCancelled { get; private set; }

		/// <summary>
		/// Gets or sets the emails that the summary should be sent to.
		/// </summary>
		public string Emails {
			get {
				if (string.IsNullOrWhiteSpace(m_Emails)) {
					var currUser = Helper.GetCurrentFOLMembershipUser();
					m_Emails = currUser.Email;
				}
				return m_Emails;
			}
			set { m_Emails = value; }
		}

		/// <summary>
		/// Gets a value indicating whether this instance is mapping in progress.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is mapping in progress; otherwise, <c>false</c>.
		/// </value>
		public bool IsMappingInProgress { get { return m_IsMappingInProgress; } }

		/// <summary>
		/// Gets or sets a value indicating whether [should send report].
		/// </summary>
		/// <value>
		///   <c>true</c> if [should send report]; otherwise, <c>false</c>.
		/// </value>
		public bool ShouldSendReport { get; set; }

		/// <summary>
		/// Gets the mapping log.
		/// </summary>
		public StringBuilder MappingLog { get; private set; }

		/// <summary>
		/// Counts the entity by its action.
		/// </summary>
		/// <param name="action">The action.</param>
		public void CountEntity(string action) {
			m_EntityCount++;
			TotalEntityCount++;

			switch (action) {
				case MappingConstants.const_action_type_create:
					m_CreateCount++;
					TotalCreateCount++;
					break;
				case MappingConstants.const_action_type_destroy:
					m_DestoryCount++;
					TotalDestoryCount++;
					break;
				case MappingConstants.const_action_type_update:
					m_UpdateCount++;
					TotalUpdateCount++;
					break;
			}
		}

		/// <summary>
		/// Counts the record.
		/// </summary>
		public void CountRecord() {
			m_RecordCount++;
			TotalRecordCount++;
		}

		/// <summary>
		/// Starts the summary for the whole mapping process.
		/// </summary>
		public void Start() {
			MappingLog.Clear();
			m_TotalStopwatch = Stopwatch.StartNew();
			m_IsMappingInProgress = true;
			m_IsFileMappingInProgress = false;
			FileCount = 0;

			TotalCreateCount = 0;
			TotalDestoryCount = 0;
			TotalErrorCount = 0;
			TotalRecordCount = 0;
			TotalEntityCount = 0;
			TotalSuccessCount = 0;
			TotalUpdateCount = 0;

			LogFileHeader("None - Initializing");
		}

		/// <summary>
		/// Starts the summary for a file.
		/// </summary>
		public void StartFile(string filename) {
			if (m_IsFileMappingInProgress) {
				throw new InvalidOperationException("Summary is already started on a file.");
			}

			m_IsFileMappingInProgress = true;

			FileCount++;
			ResetFileCounters();
			LogFileHeader(filename);

			TracingService.Write("Starting mapping for file.");
		}

		/// <summary>
		/// Writes the header for the file into the mapping log.
		/// </summary>
		/// <param name="filename">The filename.</param>
		private void LogFileHeader(string filename) {
			var logEntry = new VizeliaTraceLogEntry();
			var extraProperties = new Dictionary<string, object>
			                      	{
			                      		{const_extendedproperty_filename, filename},
			                      		{const_extendedproperty_machine_name, Environment.MachineName}
			                      	};
			logEntry.ExtendedProperties = extraProperties;
			logEntry.Categories.Clear();
			logEntry.Categories.Add(const_category_mappingheader);

			Log(logEntry);
		}

		/// <summary>
		/// Resets the counters for the mapping file.
		/// </summary>
		private void ResetFileCounters() {
			m_Stopwatch = Stopwatch.StartNew();

			m_CreateCount = 0;
			m_DestoryCount = 0;
			m_ErrorCount = 0;
			m_RecordCount = 0;
			m_EntityCount = 0;
			m_SuccessCount = 0;
			m_UpdateCount = 0;
			m_SkippedRecords = null;

		}


		/// <summary>
		/// Stops the summary for the mapping file.
		/// </summary>
		public void StopFile() {
			if (!m_IsFileMappingInProgress) {
				throw new InvalidOperationException("Summary is already stopped.");
			}

			m_IsFileMappingInProgress = false;

			m_Stopwatch.Stop();

			WriteFileSummaryToLog();
		}

		/// <summary>
		/// Writes the file summary to the mapping log.
		/// </summary>
		private void WriteFileSummaryToLog() {
			StringBuilder fileSummary = new StringBuilder();

			fileSummary.AppendFormat(const_elapsed_time_text, m_Stopwatch.Elapsed);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_records_text, m_RecordCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_entities_text, m_EntityCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_success_text, m_SuccessCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_errors_text, m_ErrorCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_destroy_text, m_DestoryCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_create_text, m_CreateCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_update_text, m_UpdateCount);
			fileSummary.AppendLine();

			if (m_SkippedRecords != null) {
				fileSummary.AppendFormat(const_number_of_skipped_text, m_SkippedRecords.Count);
				fileSummary.AppendLine();
			}
			fileSummary.AppendLine(const_summary_remark_text);
			fileSummary.AppendLine();

			if (m_SkippedRecords != null) {
				fileSummary.AppendLine();
				fileSummary.AppendLine(const_offsets_of_skipped_text);
				int count = 0;
				foreach (int offset in m_SkippedRecords) {
					fileSummary.Append(offset);
					count++;
					if (count < m_SkippedRecords.Count)
						fileSummary.Append(", ");

					if (count % 10 == 0)
						fileSummary.AppendLine();
				}
				fileSummary.AppendLine();
			}
			fileSummary.AppendLine();

			TracingService.Write(TraceEntrySeverity.Verbose, 
				String.Format("Finished mapping for file. \r\n{0}", fileSummary.ToString()));

			MappingLog.Append(fileSummary.ToString());
		}


		/// <summary>
		/// Stops the summary for the whole mapping process.
		/// </summary>
		public void Stop() {
			if (!m_IsMappingInProgress) {
				return;
			}

			m_IsFileMappingInProgress = false;

			if (m_IsFileMappingInProgress) {
				StopFile();
			}

			if (!m_TotalStopwatch.IsRunning) {
				throw new InvalidOperationException("Summary is already stopped.");
			}

			m_TotalStopwatch.Stop();
			m_IsMappingInProgress = false;

			WriteTotalsToLog();

			SendReport();
		}

		/// <summary>
		/// Writes the totals to the mapping log.
		/// </summary>
		private void WriteTotalsToLog() {
			StringBuilder fileSummary = new StringBuilder();
			fileSummary.AppendLine(const_totals_header);
			fileSummary.AppendFormat(const_number_of_files_text, FileCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_elapsed_time_text, m_TotalStopwatch.Elapsed);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_records_text, TotalRecordCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_entities_text, TotalEntityCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_success_text, TotalSuccessCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_errors_text, TotalErrorCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_destroy_text, TotalDestoryCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_create_text, TotalCreateCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_update_text, TotalUpdateCount);
			fileSummary.AppendLine();
			fileSummary.AppendFormat(const_number_of_skipped_text, TotalSkippedRecordCount);
			fileSummary.AppendLine();
			fileSummary.AppendLine(const_summary_remark_text);
			fileSummary.AppendLine();

			TracingService.Write(TraceEntrySeverity.Verbose, fileSummary.ToString());
			MappingLog.Append(fileSummary.ToString());
		}

		/// <summary>
		/// Sends the report - emails it to the initiating user.
		/// </summary>
		private void SendReport() {
			if (!ShouldSendReport) return;

			//var logEntry = new VizeliaTraceLogEntry { Message = Langue.msg_mapping_report, TimeStamp = DateTime.UtcNow };

			//logEntry.Categories.Clear();
			//logEntry.Categories.Add(const_category_mappingreport);

			//logEntry.ExtendedProperties.Add(const_extendedproperty_toaddress, Emails);

			var report = MappingLog.ToString().GetStream();
			var attachment = new Attachment(report, String.Format(Langue.msg_mapping_attachment, DateTime.Now.ToString(const_log_filename_date_time_format_string)) + ".txt");
			var mailBroker = Helper.CreateInstance<MailBroker>();
			List<Mail> mails = mailBroker.GetListByClassificationItemLocalId(MailBroker.const_mappingreport);
			var mailMessages = MailService.ConvertMailsToMailMessages(new ModelDataMappingReport { MappingSummary = this, DateTime = DateTime.UtcNow }, mails);
			foreach (var mailMessage in mailMessages) {
				mailMessage.Attachments.Add(attachment);

				MailService.Send(mailMessage);
			}
			//Logger.Write(logEntry);

		}

		/// <summary>
		/// Logs the specified mapping message and attaches the builder.
		/// </summary>
		/// <param name="localid">The localid.</param>
		/// <param name="title">The title.</param>
		/// <param name="message">The message.</param>
		private void Log(string localid, string title, string message) {
			var logEntry = new VizeliaTraceLogEntry();
			var extraProperties = new Dictionary<string, object>();
			logEntry.ExtendedProperties = extraProperties;
			logEntry.ExtendedProperties.Add(const_extendedproperty_localid, localid);
			logEntry.Categories.Clear();
			logEntry.Categories.Add(const_category_mappingrow);
			logEntry.Title = title;
			logEntry.Message = message;

			Log(logEntry);
		}

		/// <summary>
		/// Logs the specified entry and attaches the builder.
		/// </summary>
		/// <param name="logEntry">The log entry.</param>
		private void Log(VizeliaTraceLogEntry logEntry) {
			// This method is a bit misleading: what actually happens here is that we 
			// use the Logging application block templates to generate the text for the log.
			// Then a custom logging handler writes the text to the attached string builder.
			logEntry.ExtendedProperties.Add(Helper.const_extendedproperty_output, MappingLog);

			// The log entry is actually not written into the log, but rather into the attached string builder!
			Logger.Write(logEntry);
		}

		/// <summary>
		/// Logs the action start.
		/// </summary>
		/// <param name="action">The action.</param>
		public void LogActionStart(string action) {
			Log(string.Empty, Langue.msg_mapping_start, "*** " + String.Format(Langue.msg_mapping_action_start, action) + " ***");
		}

		/// <summary>
		/// Logs the action end.
		/// </summary>
		/// <param name="action">The action.</param>
		public void LogActionEnd(string action) {
			Log(string.Empty, Langue.msg_mapping_end, "*** " + String.Format(Langue.msg_mapping_action_end, action) + " ***");
			MappingLog.AppendLine("");
		}

		/// <summary>
		/// Logs the success.
		/// </summary>
		/// <param name="localid">The localid.</param>
		/// <param name="message">The message.</param>
		public void LogSuccess(string localid, string message) {
			m_SuccessCount++;
			TotalSuccessCount++;

			if (VizeliaConfiguration.Instance.Mapping.LogSuccessMessages) {
				Log(localid, Langue.msg_mapping_success, message);
			}
		}

		/// <summary>
		/// Logs the error.
		/// </summary>
		/// <param name="exception">The exception.</param>
		/// <param name="logMessage">The log message.</param>
		public void LogError(Exception exception, string logMessage) {
			LogError(exception, logMessage, true);
		}

		/// <summary>
		/// Logs the error.
		/// </summary>
		/// <param name="localid">The localid.</param>
		/// <param name="logMessage">The log message.</param>
		/// <param name="shouldWriteToTrace"> </param>
		public void LogError(string localid, string logMessage, bool shouldWriteToTrace = true) {
			m_ErrorCount++;
			TotalErrorCount++;

			Log(localid, Langue.msg_mapping_error, logMessage);

			if (shouldWriteToTrace) {
				TracingService.Write(TraceEntrySeverity.Error, logMessage);
			}
		}

		/// <summary>
		/// Logs the error.
		/// </summary>
		/// <param name="exception">The exception.</param>
		/// <param name="logMessage">The log message.</param>
		/// <param name="logDetailedException">if set to <c>true</c>, logs the detailed exception and not just the message.</param>
		public void LogError(Exception exception, string logMessage, bool logDetailedException) {
			m_ErrorCount++;
			TotalErrorCount++;

			string message = logDetailedException ? exception.ToString() : exception.Message;
			Log(string.Empty, Langue.msg_mapping_error, logMessage + ": " + message);

			TracingService.Write(exception);
		}

		/// <summary>
		/// Logs the cancelation.
		/// </summary>
		/// <param name="logMessage">The log message.</param>
		public void LogCancelation(string logMessage) {
			TracingService.Write(logMessage);

			// No need to log twice.
			if (IsCancelled) return;

			Log(string.Empty, "cancelled", logMessage);
			IsCancelled = true;
		}

		/// <summary>
		/// Logs the Skipped records. Determines the record offset from
		/// the total entity count.
		/// </summary>
		public void LogSkipped() {
			if (m_SkippedRecords == null)
				m_SkippedRecords = new List<int>();
			m_SkippedRecords.Add(m_EntityCount);
			TotalSkippedRecordCount++;
		}
	}
}

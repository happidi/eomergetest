namespace Vizelia.FOL.ConcreteProviders.Mapping.Entities.EWS.RSP {
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.schneider-electric.com/common/rsp/2012/07")]
	[System.Xml.Serialization.XmlRootAttribute("Event", Namespace = "http://www.schneider-electric.com/common/rsp/2012/07", IsNullable = false)]
	public partial class AlarmEventsType {

		private string idField;

		private string sourceIDField;

		private string sourceNameField;

		private string acknowledgeableField;

		private System.DateTime timeStampOccurrenceField;

		private System.DateTime timeStampTransitionField;

		private string priorityField;

		private string stateField;

		private string typeField;

		private string messageField;

		/// <remarks/>
		public string ID {
			get {
				return this.idField;
			}
			set {
				this.idField = value;
			}
		}

		/// <remarks/>
		public string SourceID {
			get {
				return this.sourceIDField;
			}
			set {
				this.sourceIDField = value;
			}
		}

		/// <remarks/>
		public string SourceName {
			get {
				return this.sourceNameField;
			}
			set {
				this.sourceNameField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
		public string Acknowledgeable {
			get {
				return this.acknowledgeableField;
			}
			set {
				this.acknowledgeableField = value;
			}
		}

		/// <remarks/>
		public System.DateTime TimeStampOccurrence {
			get {
				return this.timeStampOccurrenceField;
			}
			set {
				this.timeStampOccurrenceField = value;
			}
		}

		/// <remarks/>
		public System.DateTime TimeStampTransition {
			get {
				return this.timeStampTransitionField;
			}
			set {
				this.timeStampTransitionField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
		public string Priority {
			get {
				return this.priorityField;
			}
			set {
				this.priorityField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
		public string State {
			get {
				return this.stateField;
			}
			set {
				this.stateField = value;
			}
		}

		/// <remarks/>
		public string Type {
			get {
				return this.typeField;
			}
			set {
				this.typeField = value;
			}
		}

		/// <remarks/>
		public string Message {
			get {
				return this.messageField;
			}
			set {
				this.messageField = value;
			}
		}
	}
}
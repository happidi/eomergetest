namespace Vizelia.FOL.ConcreteProviders.Mapping.Entities.EWS.RSP {
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.schneider-electric.com/common/rsp/2012/07", IncludeInSchema = false)]
	public enum ItemsChoiceType {

		/// <remarks/>
		RefId,

		/// <remarks/>
		RefSpecification,

		/// <remarks/>
		Type,

		/// <remarks/>
		Unit,
	}
}
namespace Vizelia.FOL.ConcreteProviders.Mapping.Entities.EWS.RSP {
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.schneider-electric.com/common/rsp/2012/07")]
	[System.Xml.Serialization.XmlRootAttribute("HierarchyChanged", Namespace = "http://www.schneider-electric.com/common/rsp/2012/07", IsNullable = false)]
	public partial class HierarchyChangedType {

		private string typeOfChangeField;

		private System.DateTime timeStampField;

		private string idField;

		/// <remarks/>
		public string TypeOfChange {
			get {
				return this.typeOfChangeField;
			}
			set {
				this.typeOfChangeField = value;
			}
		}

		/// <remarks/>
		public System.DateTime TimeStamp {
			get {
				return this.timeStampField;
			}
			set {
				this.timeStampField = value;
			}
		}

		/// <remarks/>
		public string Id {
			get {
				return this.idField;
			}
			set {
				this.idField = value;
			}
		}
	}
}
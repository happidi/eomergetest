namespace Vizelia.FOL.ConcreteProviders.Mapping.Entities.EWS.RSP {
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.schneider-electric.com/common/rsp/2012/07")]
	[System.Xml.Serialization.XmlRootAttribute("Topology", Namespace = "http://www.schneider-electric.com/common/rsp/2012/07", IsNullable = false)]
	public partial class ArrayOfContainerItemType {

		private ContainerItemType[] containerItemField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute("ContainerItem")]
		public ContainerItemType[] ContainerItem {
			get {
				return this.containerItemField;
			}
			set {
				this.containerItemField = value;
			}
		}
	}
}
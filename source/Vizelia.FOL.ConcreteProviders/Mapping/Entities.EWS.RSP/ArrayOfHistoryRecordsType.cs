namespace Vizelia.FOL.ConcreteProviders.Mapping.Entities.EWS.RSP {
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.schneider-electric.com/common/rsp/2012/07")]
	[System.Xml.Serialization.XmlRootAttribute("MeasurementRecords", Namespace = "http://www.schneider-electric.com/common/rsp/2012/07", IsNullable = false)]
	public partial class ArrayOfHistoryRecordsType {

		private HistoryRecordsType[] historyRecordsField;

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute("HistoryRecords")]
		public HistoryRecordsType[] HistoryRecords {
			get {
				return this.historyRecordsField;
			}
			set {
				this.historyRecordsField = value;
			}
		}
	}
}
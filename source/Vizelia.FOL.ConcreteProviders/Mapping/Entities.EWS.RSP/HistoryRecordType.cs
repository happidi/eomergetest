namespace Vizelia.FOL.ConcreteProviders.Mapping.Entities.EWS.RSP {
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.schneider-electric.com/common/rsp/2012/07")]
	public partial class HistoryRecordType {

		private string valueField;

		private string stateField;

		private System.DateTime timeStampField;

		/// <remarks/>
		public string Value {
			get {
				return this.valueField;
			}
			set {
				this.valueField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
		public string State {
			get {
				return this.stateField;
			}
			set {
				this.stateField = value;
			}
		}

		/// <remarks/>
		public System.DateTime TimeStamp {
			get {
				return this.timeStampField;
			}
			set {
				this.timeStampField = value;
			}
		}
	}
}
namespace Vizelia.FOL.ConcreteProviders.Mapping.Entities.EWS.RSP {
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.schneider-electric.com/common/rsp/2012/07")]
	public partial class ContainerItemTypeItems {

		private ContainerItemSimpleType[] containerItemsField;

		private ValueItemTypeBase[] valueItemsField;

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayItemAttribute("ContainerItem", IsNullable = false)]
		public ContainerItemSimpleType[] ContainerItems {
			get {
				return this.containerItemsField;
			}
			set {
				this.containerItemsField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayItemAttribute("ValueItem", IsNullable = false)]
		public ValueItemTypeBase[] ValueItems {
			get {
				return this.valueItemsField;
			}
			set {
				this.valueItemsField = value;
			}
		}
	}
}
namespace Vizelia.FOL.ConcreteProviders.Mapping.Entities.EWS.RSP {
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.schneider-electric.com/common/rsp/2012/07")]
	public partial class HistoryRecordsType {

		private string valueItemIdField;

		private HistoryRecordType[] listField;

		/// <remarks/>
		public string ValueItemId {
			get {
				return this.valueItemIdField;
			}
			set {
				this.valueItemIdField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayItemAttribute("HistoryRecord", IsNullable = false)]
		public HistoryRecordType[] List {
			get {
				return this.listField;
			}
			set {
				this.listField = value;
			}
		}
	}
}
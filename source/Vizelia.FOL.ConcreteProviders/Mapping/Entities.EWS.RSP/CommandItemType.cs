namespace Vizelia.FOL.ConcreteProviders.Mapping.Entities.EWS.RSP {
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.schneider-electric.com/common/rsp/2012/07")]
	[System.Xml.Serialization.XmlRootAttribute("CommandItem", Namespace = "http://www.schneider-electric.com/common/rsp/2012/07", IsNullable = false)]
	public partial class CommandItemType {

		private string idField;

		private string typeField;

		private string commandDataField;

		/// <remarks/>
		public string Id {
			get {
				return this.idField;
			}
			set {
				this.idField = value;
			}
		}

		/// <remarks/>
		public string Type {
			get {
				return this.typeField;
			}
			set {
				this.typeField = value;
			}
		}

		/// <remarks/>
		public string CommandData {
			get {
				return this.commandDataField;
			}
			set {
				this.commandDataField = value;
			}
		}
	}
}
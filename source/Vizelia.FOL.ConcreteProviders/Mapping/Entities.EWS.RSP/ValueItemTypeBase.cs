namespace Vizelia.FOL.ConcreteProviders.Mapping.Entities.EWS.RSP {
	/// <remarks/>
	[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.0.30319.1")]
	[System.SerializableAttribute()]
	[System.Diagnostics.DebuggerStepThroughAttribute()]
	[System.ComponentModel.DesignerCategoryAttribute("code")]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.schneider-electric.com/common/rsp/2012/07")]
	public partial class ValueItemTypeBase {

		private string idField;

		private string nameField;

		private string descriptionField;

		private string writeableField;

		private string[] itemsField;

		private ItemsChoiceType[] itemsElementNameField;

		private MetadataTypeProperty[] metadataField;

		/// <remarks/>
		public string Id {
			get {
				return this.idField;
			}
			set {
				this.idField = value;
			}
		}

		/// <remarks/>
		public string Name {
			get {
				return this.nameField;
			}
			set {
				this.nameField = value;
			}
		}

		/// <remarks/>
		public string Description {
			get {
				return this.descriptionField;
			}
			set {
				this.descriptionField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(DataType = "integer")]
		public string Writeable {
			get {
				return this.writeableField;
			}
			set {
				this.writeableField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute("RefId", typeof(string))]
		[System.Xml.Serialization.XmlElementAttribute("RefSpecification", typeof(string))]
		[System.Xml.Serialization.XmlElementAttribute("Type", typeof(string))]
		[System.Xml.Serialization.XmlElementAttribute("Unit", typeof(string))]
		[System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
		public string[] Items {
			get {
				return this.itemsField;
			}
			set {
				this.itemsField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public ItemsChoiceType[] ItemsElementName {
			get {
				return this.itemsElementNameField;
			}
			set {
				this.itemsElementNameField = value;
			}
		}

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayItemAttribute("Property", IsNullable = false)]
		public MetadataTypeProperty[] Metadata {
			get {
				return this.metadataField;
			}
			set {
				this.metadataField = value;
			}
		}
	}
}
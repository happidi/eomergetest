﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A mapping provider for importing Dynamic Display Images from within an archive file.
	/// </summary>
	public class DynamicDisplayImagesMappingProvider : BaseMappingProvider {
		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<string> GetAllowedMappingFileExtensions() {
			return new List<string> { "ddi" };
		}

		/// <summary>
		/// Exports the specified business entities.
		/// </summary>
		/// <param name="businessEntities">The business entities.</param>
		/// <returns></returns>
		public override string Export(List<object> businessEntities) {
			return string.Empty;
		}

		/// <summary>
		/// Wraps the object with thier co-responding mappings.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetMappings(List<object> entities)
		{
			throw new NotSupportedException();
		}

		/// <summary>
		/// Deserializes the entities.
		/// </summary>
		/// <param name="mappingFile">The mapping file.</param>
		/// <param name="summary">The summary.</param>
		/// <returns>
		/// The deserialized entities.
		/// </returns>
		protected override IEnumerable<IMappingRecordConverter> DeserializeEntities(Stream mappingFile, IMappingSummary summary) {
			mappingFile.Seek(0, SeekOrigin.Begin);
			// Turn each image file inside the zip to a mapping.
			IEnumerable<IMappingRecordConverter> mappings = CompressionHelper.GetFiles(mappingFile).Select(CreateMapping);

			// This is intentionally a yield return so that deferred execution will be performed 
			// and disposal of streams will happen only in the end.
			foreach (var mapping in mappings) {
				yield return mapping;
			}
		}

		/// <summary>
		/// Creates the mapping for the input file.
		/// Stores the related Document using the DocumentHelper so it will be retrieved when the DynamicDisplayImage is saved.
		/// </summary>
		/// <param name="tuple">The tuple.</param>
		private IMappingRecordConverter CreateMapping(Tuple<string, Stream> tuple) {
			using (var memoryStream = new MemoryStream()) {
				tuple.Item2.Seek(0, SeekOrigin.Begin);
				tuple.Item2.CopyTo(memoryStream);
				byte[] contents = memoryStream.ToArray();

				// Create the underlying Document entity that will be stored in the DocumentHelper.
				string filename = tuple.Item1;
				string documentType = MimeType.GetImageMimeTypeByExtension(filename);
				string documentGuid = DocumentHelper.PrepareDocument(new Document(contents, filename, documentType));

				// Create the DynamicDisplayImage entity that references to the Document.
				string documentName = Path.GetFileNameWithoutExtension(filename);
				var dynamicDisplayImageMapping = new DefaultDynamicDisplayImageMapping {
					Action = MappingConstants.const_action_type_create,
					ImageLocalId = documentGuid,
					LocalId = documentName
				};

				return dynamicDisplayImageMapping;
			}
		}

		/// <summary>
		/// Gets the entity mapping texts.
		/// </summary>
		/// <param name="entityType">The entity type.</param>
		protected override string GetEntityMappingTexts(Type entityType) {
			return string.Empty;
		}
	}
}

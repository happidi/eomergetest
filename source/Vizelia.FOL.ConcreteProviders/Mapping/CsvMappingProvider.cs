﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {

	/// <summary>
	/// A mapping provider that reads serialized entities from a flat CSV file. 
	/// Reads the entities by deserializing the mapping entities specified in the schema attributes.
	/// </summary>
	public class CsvMappingProvider : SchemaBasedMappingProvider {
		private const string const_null_expression = "nullExpression";
		private const string const_action_header = "## ACTION:";

		private string m_NullExpression;

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);
			m_NullExpression = config[const_null_expression];
		}

	    /// <summary>
	    /// Deserializes the entities.
	    /// </summary>
	    /// <param name="mappingFile">The mapping file.</param>
	    /// <param name="summary">The summary.</param>
	    /// <returns>The deserialized entities.</returns>
	    protected override IEnumerable<IMappingRecordConverter> DeserializeEntities(Stream mappingFile, IMappingSummary summary) {
			mappingFile.Seek(0, SeekOrigin.Begin);

	        using (var reader = new StreamReader(mappingFile, Encoding.UTF8, true, 1024, true))
	        {
	            string lineWithContent = ReadNextContentLine(reader);

	            if (lineWithContent == null)
	            {
	                // We reached EOF and got no content lines.
	                return new List<IMappingRecordConverter>();
	            }

	            // Read the following lines and get the mappings inside them.
	            // This will progress the stream position.
	            IEnumerable<IMappingRecordConverter> mappings = GetMappingsFromActionFragment(reader, lineWithContent,
	                summary);

	            return mappings;
	        }
	    }

	    /// <summary>
	    /// Determines whether the provider can map the specified file or not.
	    /// </summary>
	    /// <param name="filename">The filename.</param>
	    /// <param name="mappingFile">The mapping file.</param>
	    /// <returns>
	    ///   <c>true</c> if the provider can map the specified file; otherwise, <c>false</c>.
	    /// </returns>
	    public override bool CanMapFile(string filename, Stream mappingFile) {
			// First of all lets check that the file is a CSV file.
			bool isCsv = base.CanMapFile(filename, mappingFile);

			if (!isCsv) {
				return false;
			}

			mappingFile.Seek(0, SeekOrigin.Begin);
            // These values are the defaults of new StreamReader(Stream)
	        using (var reader = new StreamReader(mappingFile, Encoding.UTF8, true, 1024, true))
	        {
	            string lineWithContent = ReadNextContentLine(reader);
	            bool canMap = lineWithContent == null || lineWithContent.StartsWith(const_action_header);

	            return canMap;
	        }
	    }

		/// <summary>
		/// Reads the next content line from the stream.
		/// Performs ReadLine() until next line is not empty or remark.
		/// Progresses the stream position.
		/// </summary>
		/// <param name="streamReader">The stream reader.</param>
		/// <returns>The next line that contains content, or null if EOF encountered.</returns>
		private static string ReadNextContentLine(StreamReader streamReader) {
			string lineWithContent = null;

			while (!streamReader.EndOfStream) {
				string line = streamReader.ReadLine();

				bool isEmptyLine = string.IsNullOrWhiteSpace(line) || line.StartsWith("//");

				if (!isEmptyLine) {
					lineWithContent = line;
					break;
				}
			}
			return lineWithContent;
		}

		/// <summary>
		/// Gets the mappings from action fragment.
		/// If the fragment ends with a new action fragment header, the new action fragment will be handled too, and so on.
		/// </summary>
		/// <param name="streamReader">The stream reader.</param>
		/// <param name="lastLineRead">The last line read.</param>
		/// <param name="summary">The summary.</param>
		private IEnumerable<IMappingRecordConverter> GetMappingsFromActionFragment(StreamReader streamReader, string lastLineRead, IMappingSummary summary) {
			if (lastLineRead == null) {
				throw new ArgumentNullException("lastLineRead");
			}

			do {
				// The line must be an ACTION header.
				if (!lastLineRead.StartsWith(const_action_header)) {
					throw new ArgumentException(string.Format("Unexpected input line, must begin with ## ACTION.\r\nLine: {0}", lastLineRead));
				}

				// Handle declaration of new entity mapping in the flat file
				Match match = GetActionHeaderParameters(lastLineRead);

				// Wrap the stream in our own custom EOF reader. 
				var reader = new MappingFileTextReader(streamReader, const_action_header);

				string currentEntityAction = match.Groups["action"].Value.ToLower();
				if (MappingConstants.Actions.Contains(currentEntityAction)) {
					// The next lines actually don't do anything. The logic in method is deferred due to yield returns.
					IEnumerable<IMappingRecordConverter> deserializedMappings = DeserializeMappingsFromFragment(match, reader, summary);
					IEnumerable<IMappingRecordConverter> mappings = EnumerateFragment(deserializedMappings, reader, summary);

					foreach (var mapping in mappings) {
						mapping.Action = currentEntityAction;
						yield return mapping;
					}
				}
				else {
					throw new VizeliaException("Unknown action: " + currentEntityAction);
				}

				// The last line read may be a new ACTION header. In that case, we need to handle it.
				lastLineRead = reader.LastLineRead;

			} while (lastLineRead != null); // null means EOF reached

		}

		/// <summary>
		/// Returns an enumerable of mappings by enumerating the given mappings enumerable until the end of the 
		/// enumerable or until an exception is thrown.
		/// Either way, when the returned enumerable has no more items, then the fragment has been fully read or skipped (if an error occured).
		/// </summary>
		/// <param name="castedMappings">The casted mappings.</param>
		/// <param name="reader">The reader.</param>
		/// <param name="summary">The summary.</param>
		private static IEnumerable<IMappingRecordConverter> EnumerateFragment(IEnumerable<IMappingRecordConverter> castedMappings, MappingFileTextReader reader, IMappingSummary summary) {
			// We do an enumeration here since we want to handle exceptions that may be thrown by the MoveNext method.
			// We cannot do a try-catch with a foreach and a yield return in it since it does not compile. Instead, we do this.

			IEnumerator<IMappingRecordConverter> enumerator = castedMappings.GetEnumerator();
			bool gotNextItem;

			do {
				gotNextItem = false;
				try {
					gotNextItem = enumerator.MoveNext();
				}
				catch (Exception exception) {
					summary.LogError(exception, Langue.msg_mapping_error_skipping_fragment);
					TracingService.Write(exception);
					// This may throw too, if there still is an error with the stream or data format:
					reader.SkipFragment();
				}

				if (gotNextItem) {
					yield return enumerator.Current;
				}
			} while (gotNextItem);
		}

		/// <summary>
		/// Deserializes the mappings from fragment.
		/// Increases the summary Row Count for each read entity, even if errorous.
		/// </summary>
		/// <param name="match">The match.</param>
		/// <param name="reader">The reader.</param>
		/// <param name="summary">The summary.</param>
		/// <returns></returns>
		private IEnumerable<IMappingRecordConverter> DeserializeMappingsFromFragment(Match match, MappingFileTextReader reader, IMappingSummary summary) {
			// Extract mapping type from ACTION header.
			Type mappingType = GetMatchingMappingType(match);

			// The deserialization will progress our stream's position.
			var serializer = new CsvSerializer(m_NullExpression);
			serializer.DeserializationError += (sender, args) => {
				summary.CountRecord();
				summary.LogError(args.Error, Langue.msg_mapping_error_readingentity);
				TracingService.Write(args.Error);
			};

			IEnumerable<object> objects = serializer.DeserializeObjects(reader, mappingType);
			IEnumerable<IMappingRecordConverter> castedMappings = objects.Cast<IMappingRecordConverter>();

			foreach (IMappingRecordConverter mapping in castedMappings) {
				summary.CountRecord();
				yield return mapping;
			}
		}

		/// <summary>
		/// Parses the input line to get the action header as a Regex Match.
		/// </summary>
		/// <param name="line">The line.</param>
		/// <returns>The Regex match for the line</returns>
		private static Match GetActionHeaderParameters(string line) {
			// We are looking for a line that looks like this:
			// ## ACTION:<action> ENTITY:<entityName> SCHEMA:<schemaName> VERSION:<schemaVersion>
			// where the schema and version parts are optional.
			Match match = Regex.Match(line, @"^## ACTION:(?<action>[^:]+)+\s+ENTITY:(?<entityName>[^:]+)(\s+SCHEMA:(?<schemaName>[^:]+))?(\s+VERSION:(?<schemaVersion>[^:]+))?$", RegexOptions.RightToLeft);

			if (!match.Success || !match.Groups["entityName"].Success || !match.Groups["action"].Success) {
				throw new ArgumentException("Row begins with ## but has invalid action and entity declaration");
			}

			return match;
		}

		/// <summary>
		/// Gets the type of the matching mapping for the regular expression match.
		/// </summary>
		/// <param name="match">The match.</param>
		private Type GetMatchingMappingType(Match match) {

			string entityName = match.Groups["entityName"].Value;

			string schemaName = match.Groups["schemaName"].Success
									? match.Groups["schemaName"].Value
									: const_default_schema_name;

			string schemaVersion = match.Groups["schemaVersion"].Success
									? match.Groups["schemaVersion"].Value
									: const_default_schema_version;

			Type mappingType = GetMatchingMappingType(entityName, schemaName, schemaVersion);

			return mappingType;
		}

		/// <summary>
		/// Serializes the mappings.
		/// </summary>
		/// <param name="mappingInstances">The mapping instances.</param>
		/// <returns></returns>
		protected override string SerializeMappings(IEnumerable<object> mappingInstances) {
			// Flatten the tree of objects since in CSV there is no hierarchy like in XML.
			// Also, we serialize CrudStore objects inside the main object.
			// Then get sequences by the object type so we can have one header and then many data rows.
			List<Tuple<Type, List<IMappingRecordConverter>>> sequences =
				mappingInstances
					.Cast<IMappingRecordConverter>()
					.FlattenTree(c => c.GetChildMappings().Where(a => !IsCrudStoreMapping(a)))
					.GetSequences(b => b.GetType());


			if (!sequences.Any()) {
				throw new VizeliaException("No instances to serialize.");
			}

			var sb = new StringBuilder();
			var serializer = new CsvSerializer(m_NullExpression);

			foreach (Tuple<Type, List<IMappingRecordConverter>> sequence in sequences) {
				string header = GenerateHeaderForMapping(sequence.Item1);
				sb.Append(header);
				string serialized = serializer.SerializeObjects(sequence.Item2, sequence.Item1);
				sb.Append(serialized);
			}

			string serializeMappings = sb.ToString();
			return serializeMappings;
		}

		private bool IsCrudStoreMapping(IMappingRecordConverter mapping) {
			// Climb up the inheritance to see if the type is a Mapping<T> and get the T.
			var type = mapping.GetType();

			var toCheck = type;
			Type mappingGenericTypeArgument = null;

			while (toCheck != typeof(object)) {
				if(toCheck.IsGenericType) {
					Type typeDefinition = toCheck.GetGenericTypeDefinition();
					if (typeDefinition == typeof(Mapping<>)) {
						Type[] genericArguments = toCheck.GetGenericArguments();
						mappingGenericTypeArgument = genericArguments[0];
						break;
					}
				}

				toCheck = toCheck.BaseType;
			}

			// Check if it's a Mapping<MappingCrudStore<,>>.
			bool isCrudStoreMapping = 
				mappingGenericTypeArgument != null && 
				mappingGenericTypeArgument.IsGenericType && 
				mappingGenericTypeArgument.GetGenericTypeDefinition() == typeof(MappingCrudStore<,>);
			
			return isCrudStoreMapping;
		}

		/// <summary>
		/// Serializes the mapping.
		/// </summary>
		/// <param name="mappingInstance">The mapping instance.</param>
		protected override string SerializeMapping(object mappingInstance) {
			return SerializeMappings(new[] { mappingInstance });
		}

		private static string GenerateHeaderForMapping(Type mappingType) {
			var schemaAttribute = Helper.GetAttribute<MappingSchemaAttribute>(mappingType);
			
			string header =
				string.Format("## ACTION:UPDATE ENTITY:{0} SCHEMA:{1} VERSION:{2}\r\n",
							  schemaAttribute.Entity,
							  schemaAttribute.Name,
							  schemaAttribute.Version);

			return header;
		}

		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<string> GetAllowedMappingFileExtensions() {
			return new List<string> { "csv", "txt" };
		}
	}
}
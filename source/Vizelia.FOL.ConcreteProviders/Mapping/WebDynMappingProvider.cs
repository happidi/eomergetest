﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A mapping provider that reads serialized entities from an XML file in WebDyn format. 
	/// It supports only MeterData entities.
	/// </summary>
	public class WebDynMappingProvider : BaseMappingProvider {
		private const string const_xml_root_element_name = "wd_log";
		private const string const_meter_element_name = "log";
		private const string const_meter_data_element_name = "data";
		private const string const_acquisition_date_time_attribute_name = "date";
		private const string const_meter_local_id_attribute_name = "addr";

		// We support two formats that look the same except one has no seconds part.
		private readonly string[] m_SupportedDateFormats = new[] { "dd'/'MM'/'yyyy HH':'mm':'ss", "dd'/'MM'/'yyyy HH':'mm", "O", "yyyy'-'MM'-'dd'T'HH':'mm':'ss" };

		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<string> GetAllowedMappingFileExtensions() {
			return new List<string> { "xml" };
		}

		/// <summary>
		/// Exports the specified business entities.
		/// </summary>
		/// <param name="businessEntities">The business entities.</param>
		/// <returns></returns>
		public override string Export(List<object> businessEntities) {
			throw new NotSupportedException();
		}

		/// <summary>
		/// Wraps the object with thier co-responding mappings.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetMappings(List<object> entities) {
			throw new NotSupportedException();
		}

	    /// <summary>
	    /// Determines whether the provider can map the specified file or not.
	    /// </summary>
	    /// <param name="filename">The filename.</param>
	    /// <param name="mappingFile">The mapping file.</param>
	    /// <returns>
	    ///   <c>true</c> if the provider can map the specified file; otherwise, <c>false</c>.
	    /// </returns>
	    public override bool CanMapFile(string filename, Stream mappingFile) {
			// First of all lets check that the file is an XML file.
			bool isXml = base.CanMapFile(filename, mappingFile);

			if (!isXml) {
				return false;
			}

			// Now we check its header to make sure its our format of XML file.
			bool isValid = IsValidWebDynMappingFile(mappingFile);

			return isValid;
		}

		/// <summary>
		/// Determines whether the specified mapping file is a valid WebDyn mapping file by checking its contents.
		/// </summary>
		/// <param name="mappingFile">The mapping file.</param>
		/// <returns>
		///   <c>true</c> if specified mapping file is a valid WebDyn mapping file; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsValidWebDynMappingFile(Stream mappingFile) {
			mappingFile.Seek(0, SeekOrigin.Begin);

			bool isWebDynLogFile = false;

			try {
				XDocument xDocument = XDocument.Load(mappingFile);
				isWebDynLogFile = xDocument.Root != null && xDocument.Root.Name.LocalName.ToLower().Equals(const_xml_root_element_name);
			}
			catch {
				// We failed to parse the file, it may not be XML, although its extension is.
			}

			return isWebDynLogFile;

		}

	    /// <summary>
	    /// Deserializes the entities.
	    /// </summary>
	    /// <param name="mappingFile">The mapping file.</param>
	    /// <param name="summary">The summary.</param>
	    /// <returns>
	    /// The deserialized entities.
	    /// </returns>
	    protected override IEnumerable<IMappingRecordConverter> DeserializeEntities(Stream mappingFile, IMappingSummary summary) {
			mappingFile.Seek(0, SeekOrigin.Begin);

			var readerSettings = new XmlReaderSettings { IgnoreComments = true, IgnoreWhitespace = true, CloseInput = false};

			using (XmlReader reader = XmlReader.Create(mappingFile, readerSettings)) {
				bool foundRootElement = reader.ReadToFollowing(const_xml_root_element_name);

				if (foundRootElement) {
					bool foundMeterElement = reader.ReadToDescendant(const_meter_element_name);

					if (foundMeterElement) {
						do {
							XmlReader subtreeReader = reader.ReadSubtree();

							// This is for deffered execution purposes.
							foreach (var converter in GetMeterDatas(subtreeReader)) {
								yield return converter;
							}

						} while (reader.ReadToFollowing(const_meter_element_name));
					}
				}
			}
		}

		/// <summary>
		/// Gets the meter datas.
		/// The reader should be positioned on a 'log' element.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <returns></returns>
		private IEnumerable<IMappingRecordConverter> GetMeterDatas(XmlReader reader) {
			reader.MoveToContent();

			string meterLocalId = GetAttributeValue(const_meter_local_id_attribute_name, reader);
			bool foundDataElement = reader.ReadToDescendant(const_meter_data_element_name);

			if (foundDataElement) {
				do {
					var converter = GetMeterData(reader, meterLocalId);
					yield return converter;
				} while (reader.ReadToFollowing(const_meter_data_element_name));

			}

			// Close only the subtree-reader.
			reader.Close();
		}

		/// <summary>
		/// Gets the meter data.
		/// The reader should be positioned on a 'data' element.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <param name="meterLocalId">The meter local id.</param>
		/// <returns></returns>
		private IMappingRecordConverter GetMeterData(XmlReader reader, string meterLocalId) {
			string date = GetAttributeValue(const_acquisition_date_time_attribute_name, reader);
			DateTime acquisitionDateTime = ParseAcquisitionDateTime(date);
			string val = GetElementValue(reader);

			var meterData = new MeterData {
				AcquisitionDateTime = acquisitionDateTime,
				Value = double.Parse(val, CultureInfo.InvariantCulture),
				KeyMeter = meterLocalId
			};

			IMappingRecordConverter converter = CreateConverter(meterData);
			return converter;
		}

		/// <summary>
		/// Parses the acquisition date time by the predefined formats.
		/// </summary>
		/// <param name="date">The date.</param>
		/// <returns></returns>
		private DateTime ParseAcquisitionDateTime(string date) {
			DateTime parsedDateTime = DateTime.ParseExact(date, m_SupportedDateFormats, CultureInfo.InvariantCulture, DateTimeStyles.None);

			DateTime acquisitionDateTime;

			switch (parsedDateTime.Kind) {
				case DateTimeKind.Utc:
					acquisitionDateTime = parsedDateTime;
					break;
				case DateTimeKind.Local:
					acquisitionDateTime = parsedDateTime.ToUniversalTime();
					break;
				case DateTimeKind.Unspecified:
					acquisitionDateTime = DateTime.SpecifyKind(parsedDateTime, DateTimeKind.Utc);
					break;
				default:
					throw new ArgumentOutOfRangeException("parsedDateTime.Kind");
			}

			return acquisitionDateTime;
		}

		/// <summary>
		/// Gets the element value.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <returns></returns>
		private static string GetElementValue(XmlReader reader) {
			string parentElementName = reader.Name;

			// Skip whitespace
			if (reader.Read()) {
				reader.MoveToContent();
				if (reader.NodeType == XmlNodeType.Text) {
					string val = reader.ReadContentAsString();
					return val;
				}
			}

			throw new VizeliaException(string.Format("No content in '{0}' element.", parentElementName));
		}

		/// <summary>
		/// Gets the attribute value.
		/// Throws if the value is empty or null.
		/// </summary>
		/// <param name="attributeName">Name of the attribute.</param>
		/// <param name="reader">The reader.</param>
		/// <returns></returns>
		private static string GetAttributeValue(string attributeName, XmlReader reader) {
			string attributeValue = reader.GetAttribute(attributeName);

			if (attributeValue == null) {
				throw new VizeliaException(string.Format("No value in attribute '{0}' on element '{1}'", attributeName, reader.Name));
			}
			return attributeValue;
		}

		/// <summary>
		/// Creates the converter.
		/// </summary>
		/// <param name="meterData">The meter data.</param>
		/// <returns></returns>
		private static IMappingRecordConverter CreateConverter(MeterData meterData) {
			var converter = new EntityMappingRecordConverter<MeterData>(meterData);
			converter.Action = MappingConstants.const_action_type_create;

			return converter;
		}

		/// <summary>
		/// Gets the entity mapping texts.
		/// </summary>
		/// <param name="entityType">The entity type.</param>
		protected override string GetEntityMappingTexts(Type entityType) {
			return string.Empty;
		}
	}
}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A mapping provider that reads entities by deserializing the mapping entities specified in the schema attributes.
	/// </summary>
	public abstract class SchemaBasedMappingProvider : BaseMappingProvider {
		/// <summary>
		/// The default schema name
		/// </summary>
		protected const string const_default_schema_name = "Default";

		/// <summary>
		/// The default schema version
		/// </summary>
		protected const string const_default_schema_version = "1.0.0";


		/// <summary>
		/// Gets the matching mapping type for the given entity name, schema name and schema version.
		/// </summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="schemaName">Name of the schema.</param>
		/// <param name="schemaVersion">The schema version.</param>
		/// <returns>The matching mapping type.</returns>
		protected Type GetMatchingMappingType(string entityName, string schemaName, string schemaVersion) {
			var retval = CacheService.Get(entityName + schemaName + schemaVersion,
							 () => GetMatchingMappingTypeInternal(entityName, schemaName, schemaVersion),
							location: CacheLocation.Memory);

			return retval;
		}

		/// <summary>
		/// Gets the matching mapping type internal.
		/// </summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="schemaName">Name of the schema.</param>
		/// <param name="schemaVersion">The schema version.</param>
		/// <returns></returns>
		private Type GetMatchingMappingTypeInternal(string entityName, string schemaName, string schemaVersion) {
			Type[] commonTypes = typeof(BaseBusinessEntity).Assembly.GetTypes();
			List<Type> matchingMappingTypes =
				commonTypes.Where(t => IsMatchingMappingType(t, entityName, schemaName, schemaVersion)).ToList();

			if (matchingMappingTypes.Count == 0) {
				throw new InvalidOperationException(
					string.Format(
						"Cannot deserialize entity {0} with schema {1} and version {2}: No mapping type corresponds to it.",
						entityName, schemaName, schemaVersion));
			}
			if (matchingMappingTypes.Count > 1) {
				throw new InvalidOperationException(
					string.Format(
						"Cannot deserialize entity {0} with schema {1} and version {2}: More than one mapping type correspond to it.",
						entityName, schemaName, schemaVersion));
			}

			return matchingMappingTypes[0];
		}

		/// <summary>
		/// Determines whether the type to examine matches the given mapping parameters or not.
		/// </summary>
		/// <param name="typeToExamine">The type to examine.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="schemaName">Name of the schema.</param>
		/// <param name="schemaVersion">The schema version.</param>
		/// <returns>
		///   <c>true</c> if whether the type to examine matches the given mapping parameters; otherwise, <c>false</c>.
		/// </returns>
		private bool IsMatchingMappingType(Type typeToExamine, string entityName, string schemaName, string schemaVersion) {
			var schemaAttribute = Helper.GetAttribute<MappingSchemaAttribute>(typeToExamine);
			var interfaces = typeToExamine.GetInterfaces();

			// The current type needs to be an IMappingRecordConverter and have the same entity name, schema name and version as requested
			bool isMatching =
				interfaces.Contains(typeof(IMappingRecordConverter)) &&
				schemaAttribute != null &&
				schemaAttribute.Entity.ToLower().Equals(entityName.ToLower()) &&
				schemaAttribute.Name.ToLower().Equals(schemaName.ToLower()) &&
				schemaAttribute.Version.ToLower().Equals(schemaVersion.ToLower());

			return isMatching;

		}

		/// <summary>
		/// Gets the entity mapping texts.
		/// </summary>
		/// <param name="entityType">The entity type.</param>
		protected override string GetEntityMappingTexts(Type entityType) {
			var mappings = GetMappingTypesForEntity(entityType);
			var stringBuilder = new StringBuilder();

			foreach (var currentMapping in mappings) {
				object mappingInstance = CreatePopulatedInstance(currentMapping);
				string serializedEntity = SerializeMapping(mappingInstance);
				stringBuilder.Append(serializedEntity);
			}

			string texts = stringBuilder.ToString();
			return texts;
		}

		private List<Type> GetMappingTypesForEntity(Type entityType) {
			Type[] allTypes = typeof(BaseBusinessEntity).Assembly.GetTypes();

			var mappingsEnumerable = from t in allTypes
									 let isMappingOfGivenEntity = typeof(Mapping<>).MakeGenericType(entityType).IsAssignableFrom(t)
									 let schema = Helper.GetAttribute<MappingSchemaAttribute>(t)
									 where !t.IsAbstract && isMappingOfGivenEntity && schema != null
									 select new { MappingType = t, SchemaDescription = schema };

			var mappings = mappingsEnumerable.ToList();
			List<MappingSchemaAttribute> attributes = mappings.Select(m => m.SchemaDescription).ToList();

			if (attributes.Count != attributes.Distinct().Count()) {
				throw new InvalidOperationException(string.Format("Found more than one mapping for entity {0}", entityType.FullName));
			}

			List<Type> mappingTypesForEntity = mappings.Select(m => m.MappingType).ToList();

			return mappingTypesForEntity;
		}

		/// <summary>
		/// Creates an instance of the the given type, and populates its properties with values.
		/// </summary>
		/// <param name="typeToCreate">The mapping.</param>
		/// <returns></returns>
		private static object CreatePopulatedInstance(Type typeToCreate) {
			var genericArguments = new Type[0];
			if (typeToCreate.IsGenericType) {
				genericArguments = typeToCreate.GetGenericArguments();
			}
			var numericTypes = new[] { typeof(int), typeof(short), typeof(double), typeof(float), typeof(long), typeof(uint), typeof(ulong), typeof(ushort) };


			object newInstance;

			var isMapping = IsMapping(typeToCreate);
			if (isMapping) {
				newInstance = Activator.CreateInstance(typeToCreate);
				var properties = typeToCreate.GetProperties(BindingFlags.Public | BindingFlags.Instance);
				var assignableProperties = properties.Where(p => p.GetSetMethod() != null);

				// Fill object with property values
				foreach (var property in assignableProperties) {
					var newPropertyValue = CreatePopulatedInstance(property.PropertyType);
					property.SetValue(newInstance, newPropertyValue, null);
				}
			}
			else if (typeToCreate == typeof(string)) {
				newInstance = "string";
			}
			else if (typeToCreate.IsAssignableFrom(typeof(DateTime))) {
				newInstance = DateTime.Now;
			}
			else if (typeToCreate.IsAssignableFrom(typeof(int))) {
				newInstance = 0;
			}
			else if (
				typeToCreate.IsGenericType &&
				typeToCreate.GetGenericTypeDefinition() == typeof(Nullable<>) &&
				genericArguments.Length == 1 &&
				numericTypes.Contains(genericArguments[0])) {
				// This means it's a nullable<number> type.
				newInstance = Activator.CreateInstance(typeToCreate, 0);
			}
			else {
				newInstance = Activator.CreateInstance(typeToCreate);
				if (typeToCreate.IsGenericType && typeof(List<>).IsAssignableFrom(typeToCreate.GetGenericTypeDefinition())) {
					if (genericArguments.Length == 1) {
						// Recursive call to create the type for the list item.
						object item = CreatePopulatedInstance(genericArguments[0]);
						var list = (IList)newInstance;
						list.Add(item);
					}
				}
			}
			return newInstance;
		}

		private static bool IsMapping(Type currentType) {
			bool isMapping = false;
			while (currentType != null) {
				if (currentType.IsGenericType && currentType.GetGenericTypeDefinition() == typeof(Mapping<>)) {
					isMapping = true;
					break;
				}
				currentType = currentType.BaseType;
			}
			return isMapping;
		}

		/// <summary>
		/// Serializes the mapping.
		/// </summary>
		/// <param name="mappingInstance">The mapping instance.</param>
		protected abstract string SerializeMapping(object mappingInstance);

		/// <summary>
		/// Exports the specified business entities.
		/// </summary>
		/// <param name="entities">The business entities.</param>
		/// <returns></returns>
		public override string Export(List<object> entities) {
			var mappingInstances = GetMappings(entities);

			string serializedMappings = SerializeMappings(mappingInstances);
			return serializedMappings;
		}

		/// <summary>
		/// Wraps the object with thier co-responding mappings.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetMappings(List<object> entities) {
			if (!entities.Any()) {
				throw new ArgumentException("No entities to export.");
			}

			IEnumerable<IMappingRecordConverter> mappingInstances = entities.Select(GetMappingInstance);
			foreach (var mappingRecordConverter in mappingInstances) {
				mappingRecordConverter.Action = MappingConstants.const_action_type_create;
				yield return mappingRecordConverter;
			}
		}

		/// <summary>
		/// Gets the mapping instance for the entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		private IMappingRecordConverter GetMappingInstance(object entity) {
			Type entityType = entity.GetType();
			//Type mappingTypeForEntity = GetMappingTypeForEntity(entityType);
			Type mappingTypeForEntity = CacheService.Get(string.Format("MappingTypeForEntity:{0}", entityType.Name), () => GetMappingTypeForEntity(entityType), 0, CacheLocation.Memory);
			var instance = (IMappingRecordConverter)Activator.CreateInstance(mappingTypeForEntity, entity);

			return instance;
		}

		/// <summary>
		/// Serializes the mappings.
		/// </summary>
		/// <param name="mappingInstances">The mapping instances.</param>
		/// <returns></returns>
		protected abstract string SerializeMappings(IEnumerable<object> mappingInstances);

		private Type GetMappingTypeForEntity(Type entityType) {
			List<Type> mappingTypesForEntity = GetMappingTypesForEntity(entityType);
			//TODO: Here get schema attribute and compare version and name to requested.
			Type mappingTypeForEntity = mappingTypesForEntity.FirstOrDefault();

			if (mappingTypesForEntity == null) {
				throw new VizeliaException("Cannot find a mapping type for entity type " + entityType.Name);
			}

			return mappingTypeForEntity;
		}
	}
}
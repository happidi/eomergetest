﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A mapping provider that reads entities from Excel file. 
	/// Reads the entities by deserializing the mapping entities.
	/// </summary>
	public class ExcelMappingProvider : BaseMappingProvider {
		/// <summary>
		/// Gets the allowed mapping file extensions.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<string> GetAllowedMappingFileExtensions() {
			return new List<string> { "xlsx" };
		}

		/// <summary>
		/// Exports the specified business entities.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <returns></returns>
		public override string Export(List<object> entities) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Wraps the object with thier co-responding mappings.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <returns></returns>
		public override IEnumerable<IMappingRecordConverter> GetMappings(List<object> entities)
		{
			throw new NotSupportedException();
		}

		/// <summary>
		/// Deserializes the entities.
		/// </summary>
		/// <param name="mappingFile">The mapping file.</param>
		/// <param name="summary">The summary.</param>
		/// <returns>
		/// The deserialized entities.
		/// </returns>
		protected override IEnumerable<IMappingRecordConverter> DeserializeEntities(Stream mappingFile, IMappingSummary summary) {
			// Getting the entities from the excel file.
			List<object> entitiesList = MappingConversionService.GetEntities(mappingFile);

			var mappingRecordConverters = MappingService.Providers["XmlSerialization"].GetMappings(entitiesList);
			return mappingRecordConverters;
		}

		/// <summary>
		/// Gets the entity mapping texts.
		/// </summary>
		/// <param name="entityType">The entity type.</param>
		/// <returns></returns>
		protected override string GetEntityMappingTexts(Type entityType) {
			return String.Empty;
		}
	}
}

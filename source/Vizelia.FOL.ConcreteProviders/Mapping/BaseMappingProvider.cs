using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Localization;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A base provider for mapping.
	/// </summary>
	public abstract class BaseMappingProvider : MappingProvider {

		private const string const_filename_pattern_configuration_attribute = "filename";
		private const string const_mapping_cancelled_message = "Mapping operation cancelled by user. Created, modified or deleted entities have been permanently changed. The partial mapping done so far cannot be undone. Totals in the mapping summary represent only mapped entities until cancelation and not entire contents of the file.";
		private const string const_last_progress_report_time_key = "lastProgressReportTime";
		private const string const_filters_configuration_attribute = "filters";

		/// <summary>
		/// The interval between reports of progress of entity in seconds.
		/// There is no need to report every processed entity since it accesses the Cache databaes.
		/// The long-running operation progress is polled on an interval as well any way, so lots of these reports won't appear.
		/// </summary>
		private const int const_entity_progress_reporting_interval = 4;

		private string m_FilenamePattern;

		/// <summary>
		/// A list of names to filters that the IncludeMappingRecord uses to determine how to behave.
		/// This list is populated from the "filters" parameter in configuration settings.
		/// If that parameter is a comma delimited string, it becomes this list.
		/// If null, there are no filters.
		/// </summary>
		protected IEnumerable<string> m_FilterNames;

		/// <summary>
		/// Creates the mapping summary.
		/// </summary>
		/// <returns></returns>
		public override MappingContext CreateMappingContext(){
			return new MappingContext()
			{
				MappingSummary = new MappingSummary()
			};
		}

		/// <summary>
		/// Maps the specified mapping file.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="fileContent">Content of the file.</param>
		/// <param name="mappingContext">The mapping context.</param>
		public override void Map(string fileName, Stream fileContent, MappingContext mappingContext){

			IMappingSummary summary = mappingContext.MappingSummary;
			using (TracingService.StartTracing("Mapping", fileName)) {
				summary.StartFile(fileName);

				try {
					LongRunningOperationService.ReportProgress(0, String.Format(Langue.msg_mapping_filereceived, fileName));
					// The flattenedEntities enumerable is deffered.
					IEnumerable<IMappingRecordConverter> flattenedEntities = GetFlattenedDeserializedEntities(fileContent, summary);

					long fileLength = fileContent.Length;
					// Variables used for sequence buffering.
					Type currentEntityType = null;
					IMappingBroker currentEntityBroker = null;
					string currentAction = null;
					var entitySequenceBuffer = new List<IMappingRecord>();

					// This performs the real enumeration and reading of the input stream by the current action. 
					foreach (IMappingRecordConverter mapping in flattenedEntities) {
						if (LongRunningOperationService.IsCancelationPending()) {
							LogCancelation(summary);
							break;
						}

						summary.CountEntity(mapping.Action);

						if (!IncludeMappingRecord(mapping)) {
							summary.LogSkipped();
							continue;
						}


						IMappingRecord mappingRecord = GetMappingRecord(summary, mapping);

						if (mappingRecord != null) {
							if (mappingRecord.BusinessEntityType == typeof(MappingConversionError)) {
								summary.LogError("", ((MappingConversionError)mappingRecord.Entity).ErrorMessage);
							}
							else {
								// Check if we need to end the current sequence because we encountered a new entity type, or a new action, 
								// or because the entities buffer is full for this type.
								if ((mappingRecord.BusinessEntityType != currentEntityType || !mapping.Action.Equals(currentAction) ||
									 currentEntityBroker.MaximumBatchSize == entitySequenceBuffer.Count)) {
									// Process current sequence.
									if (entitySequenceBuffer.Any()) {
										int progressWithinFile = Math.Max(0, (int)((100.0 * fileContent.Position) / (1.0 * fileLength)) - 1);
										ProcessEntities(entitySequenceBuffer, currentAction, mappingContext, progressWithinFile, currentEntityBroker);
									}

									// Start the new sequence.
									currentEntityType = mappingRecord.BusinessEntityType;
									currentEntityBroker = CreateMappingBrokerForEntity(currentEntityType);
									currentAction = mapping.Action;
									entitySequenceBuffer = new List<IMappingRecord>();
								}

								entitySequenceBuffer.Add(mappingRecord);
							}
						}
					}

					// EOF reached, map remaining entities in the buffer.
					if (entitySequenceBuffer.Any()) {
						int progressWithinFile = Math.Max(0, (int)((100.0 * fileContent.Position) / (1.0 * fileLength)) - 1);
						ProcessEntities(entitySequenceBuffer, currentAction, mappingContext, progressWithinFile, currentEntityBroker);
					}


					LongRunningOperationService.ReportProgress(100, Langue.msg_mapping_finishing);

				}
				catch (Exception exception) {
					summary.LogError(exception, Langue.msg_mapping_error);
					throw;
				}
				finally {
					summary.StopFile();
				}
			}
		}

		private static IMappingRecord GetMappingRecord(IMappingSummary summary, IMappingRecordConverter mapping) {
			IMappingRecord mappingRecord = null;

			try {
				mappingRecord = mapping.GetMappingRecord();
			}
			catch (Exception ex) {
				summary.LogError(ex, Langue.msg_mapping_error_readingentity);
			}
			return mappingRecord;
		}

		private IEnumerable<IMappingRecordConverter> GetFlattenedDeserializedEntities(Stream fileContent, IMappingSummary summary) {
			fileContent.Seek(0, SeekOrigin.Begin);

			// This enumerable represents deferred deserialization from the input stream.
			IEnumerable<IMappingRecordConverter> deserializeEntities = DeserializeEntities(fileContent, summary);
			// This is a deferred enumerable as well.
			IEnumerable<IMappingRecordConverter> flattenedEntities = deserializeEntities.FlattenTree(GetChildMappings);
			return flattenedEntities;
		}

		/// <summary>
		/// Gets the child mappings.
		/// </summary>
		/// <param name="parent">The parent.</param>
		/// <returns></returns>
		private static IEnumerable<IMappingRecordConverter> GetChildMappings(IMappingRecordConverter parent) {
			// Since the concrete mapping providers only assign the Action to the first level objects in the file,
			// we copy the action to the child objects if it's missing.
			IEnumerable<IMappingRecordConverter> childMappings = parent.GetChildMappings();

			foreach (var childMapping in childMappings) {
				if (string.IsNullOrEmpty(childMapping.Action)) {
					childMapping.Action = parent.Action;
				}

				yield return childMapping;
			}
		}


		private static void LogCancelation(IMappingSummary summary) {
			summary.LogCancelation(const_mapping_cancelled_message);
		}

		/// <summary>
		/// Maps the entities.
		/// </summary>
		/// <param name="entities">The entities.</param>
		/// <param name="context">The context.</param>
		public override void MapEntities(List<IMappingRecordConverter> entities, MappingContext context) {
			using (TracingService.StartTracing("MapEntities", string.Empty)) {
				TracingService.Write("Starting to map entities from memory...");

				List<IMappingRecord> records = entities.Select(e => e.GetMappingRecord()).ToList();

				for (int i = 0; i < records.Count; i++) {
					int percentDone = (int)((100.0 * i) / (1.0 * records.Count));
					IMappingBroker broker = CreateMappingBrokerForEntity(records[i].BusinessEntityType);
					ProcessEntities(new List<IMappingRecord> { records[i] }, MappingConstants.const_action_type_create, context, percentDone, broker);
				}

				TracingService.Write("MapEntities complete");
			}
		}

		/// <summary>
		/// Deserializes the entities.
		/// </summary>
		/// <param name="mappingFile">The mapping file.</param>
		/// <param name="summary">The summary.</param>
		/// <returns>
		/// The deserialized entities.
		/// </returns>
		protected abstract IEnumerable<IMappingRecordConverter> DeserializeEntities(Stream mappingFile, IMappingSummary summary);

		/// <summary>
		/// Processes the entity.
		/// </summary>
		/// <param name="mappingRecords">The mapping records.</param>
		/// <param name="action">The action.</param>
		/// <param name="mappingContext">The mapping context.</param>
		/// <param name="progressToReport">The progress to report.</param>
		/// <param name="broker">The broker.</param>
		private void ProcessEntities(List<IMappingRecord> mappingRecords, string action, MappingContext mappingContext, int progressToReport, IMappingBroker broker) {
			// We assume the whole list is composed of the same type of entity.
			Type entityType = mappingRecords[0].BusinessEntityType;
			string entityName = entityType.Name;
			// Report just the first entity in the batch.
			ReportEntityProgress(progressToReport, mappingRecords[0].LocalId, entityName);

			// Do the mapping by calling the entity broker
			IEnumerable<Tuple<string, FormResponse>> responses = null;
			try {
				broker.Mapping(mappingRecords, action, mappingContext, out responses);
			}
			catch (Exception exception) {
				mappingContext.MappingSummary.LogError(exception, Langue.msg_mapping_error);
			}

			LogResponsesToSummary(action, mappingContext.MappingSummary, entityName, responses, !broker.WritesFailuresToTrace);
		}

		/// <summary>
		/// Logs the responses to summary.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="summary">The summary.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="responses">The responses.</param>
		/// <param name="shouldWriteToTrace">if set to <c>true</c> then failures should be written to the trace.</param>
		private static void LogResponsesToSummary(string action, IMappingSummary summary, string entityName, IEnumerable<Tuple<string, FormResponse>> responses, bool shouldWriteToTrace) {
			if (responses == null) return;

			foreach (var response in responses) {
				var formResponse = response.Item2;
				var localid = response.Item1;

				// Examine broker mapping success
				if (formResponse.success) {
					string message = String.Format(Langue.msg_mapping_resultentity_success, action, entityName, localid);
					summary.LogSuccess(localid, message);
				}
				else {
					string message = String.Format(Langue.msg_mapping_resultentity_error, action, entityName, localid) + ": " + formResponse.FormatError();
					summary.LogError(localid, message, shouldWriteToTrace);
				}
			}
		}

		/// <summary>
		/// Reports the entity progress.
		/// </summary>
		/// <param name="progressToReport">The progress to report.</param>
		/// <param name="localid">The localid.</param>
		/// <param name="entityName">Name of the entity.</param>
		private static void ReportEntityProgress(int progressToReport, string localid, string entityName) {
			if (ShouldReportEntityProgress()) {
				// Only report once in X time since the user polls for info in the UI every Y seconds
				string progressReportMessage = String.Format(Langue.msg_mapping_processentity, entityName, localid);
				LongRunningOperationService.ReportProgress(progressToReport, progressReportMessage);
			}
		}

		/// <summary>
		/// Returns a value indicating whether the entity progress should be reported.
		/// </summary>
		/// <returns>True if yes, False if no.</returns>
		private static bool ShouldReportEntityProgress() {
			bool shouldUpdateProgress = false;

			object lastProgressReportObject = ContextHelper.Get(const_last_progress_report_time_key);
			DateTime lastProgressReportTime;

			if (lastProgressReportObject == null) {
				lastProgressReportTime = DateTime.MinValue;
			}
			else {
				lastProgressReportTime = (DateTime)lastProgressReportObject;
			}

			if (lastProgressReportTime.AddSeconds(const_entity_progress_reporting_interval) < DateTime.Now) {
				shouldUpdateProgress = true;
				ContextHelper.Add(const_last_progress_report_time_key, DateTime.Now);
			}

			return shouldUpdateProgress;
		}

		/// <summary>
		/// Creates the mapping broker for the entity.
		/// </summary>
		/// <param name="entityType">Type of the entity.</param>
		private IMappingBroker CreateMappingBrokerForEntity(Type entityType) {
			var createdInstance = BrokerFactory.CreateBroker(entityType);
			var broker = createdInstance as IMappingBroker;

			if (broker == null) {
				throw new InvalidOperationException(string.Format("Given broker for entity {0} is not an IMappingBroker", entityType.Name));
			}
			return broker;
		}

		/// <summary>
		/// Gets all mapping objects.
		/// </summary>
		public override string GetAllMappingObjects() {
			Type[] allTypes = typeof(BaseBusinessEntity).Assembly.GetTypes();
			var entities = allTypes.Where(IsMappableObject).OrderBy(e => e.FullName);

			var stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat("Provider: {0}\r\n\r\n", GetType().FullName);

			foreach (var entity in entities) {
				stringBuilder.AppendFormat("Entity: {0}\r\n", entity.FullName);
				string entityText = GetEntityMappingTexts(entity);
				stringBuilder.Append(entityText);
				stringBuilder.Append("\r\n\r\n");
			}

			var allMappingObjects = stringBuilder.ToString();
			return allMappingObjects;
		}

		/// <summary>
		/// Determines whether the type is mappable.
		/// </summary>
		/// <param name="t">The t.</param>
		/// <returns>
		///   <c>true</c> if the type is mappable; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsMappableObject(Type t) {
			bool isMappableObject = false;

			if (t.IsClass && !t.IsAbstract && typeof(BaseBusinessEntity).IsAssignableFrom(t) && typeof(IMappableEntity).IsAssignableFrom(t)) {
				ConstructorInfo[] constructors = t.GetConstructors();
				bool hasDefaultCtor = constructors.Any(c => c.GetParameters().Length == 0);

				isMappableObject = hasDefaultCtor;
			}

			return isMappableObject;
		}

		/// <summary>
		/// Gets the entity mapping texts.
		/// </summary>
		/// <param name="entityType">The entity type.</param>
		protected abstract string GetEntityMappingTexts(Type entityType);

		/// <summary>
		/// Initializes the provider. 
		/// Supports:
		/// "filename" - establishes the filename pattern to recognize content by its file name
		/// "filters" - establishes a list of filter names used by IncludeMappingRecord. Must be comma delimited.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			if (!String.IsNullOrWhiteSpace(config[const_filename_pattern_configuration_attribute])) {
				m_FilenamePattern = config[const_filename_pattern_configuration_attribute].ToLower();
			}

			if (!String.IsNullOrWhiteSpace(config[const_filters_configuration_attribute])) {
				m_FilterNames = config[const_filters_configuration_attribute].Split(',');
			}
		}

		/// <summary>
		/// Determines whether the provider can map the specified file or not, only by its filename.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <returns>
		///   <c>true</c> if the provider can map the specified file; otherwise, <c>false</c>.
		/// </returns>
		public override bool CanMapFile(string filename) {
			if (string.IsNullOrWhiteSpace(m_FilenamePattern)) {
				throw new Exception("Provider not initialized with a pattern to match the filename by.");
			}

			bool isFilenameMatch = Helper.IsFilenameMatch(filename.ToLower(), m_FilenamePattern);

			return isFilenameMatch;
		}

		/// <summary>
		/// Determines whether the provider can map the specified file or not.
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <param name="mappingFile">The mapping file.</param>
		/// <returns>
		///   <c>true</c> if the provider can map the specified file; otherwise, <c>false</c>.
		/// </returns>
		public override bool CanMapFile(string filename, Stream mappingFile) {
			bool canMapFile = CanMapFile(filename);
			return canMapFile;
		}

		/// <summary>
		/// Allows filtering out unwanted converters. 
		/// </summary>
		/// <remarks>
		/// <para>When a record is skipped, the caller should register it within the MappingSummary
		/// using LogSkipped().</para>
		/// <para>This class is implemented to let a Mapping Provider definition include
		/// the "filters" parameter. When filters="ExcludeInvalidMeterData",
		/// it excludes meters whose Validity is Invalid.
		/// When filters="ExcludeInvalidZeroValueMeterData",
		/// it excludes meters whose Value is 0 and Validity is Invalid.
		/// The user must opt-in to use a filter.</para>
		/// </remarks>
		/// <param name="converter"></param>
		/// <returns>When true, the record should be kept. When false,
		/// it should be skipped.</returns>
		protected virtual bool IncludeMappingRecord(IMappingRecordConverter converter) {
/* This filtering system is not flexible. It requires code to be added directly here
 * instead of giving the user a way of plugging in new filters.
 * A future version could use the Provider model to let users register their list of
 * filter classes. Each registered provider has a name and those names would correspond
 * to the names in the "filters" parameter (and in m_FilterNames).
 */
			if (m_FilterNames != null) {
				foreach (string filterName in m_FilterNames) {
					switch (filterName) {
						case "ExcludeInvalidMeterData":
						/*
						 FILTER RULE: Omit MeterData when Validity is Invalid.
						 Only apply when the Value property is assigned.
						 V&E may provide change to the Validity without including a Value
						 and we want to keep those entries.
						 See User Story 6440.
						 */
							DefaultMeterDataMapping mdMapping = converter as DefaultMeterDataMapping;
							if (mdMapping != null) {
								if (mdMapping.HasPropertyAssigned("Value"))
									if (mdMapping.Validity == MeterDataValidity.Invalid)
										return false;

							}
							break;
						case "ExcludeInvalidZeroValueMeterData":
							/*
							 FILTER RULE: Omit MeterData when Value is 0.0 and Validity is Invalid.
							 Only apply when the Value property is assigned.
							 V&E may provide change to the Validity without including a Value
							 and we want to keep those entries.
							 This does not match the requirements of User Story 6440, but is here
							 in case those requirements negatively impact users.
							 */
							mdMapping = converter as DefaultMeterDataMapping;
							if (mdMapping != null) {

								if (mdMapping.HasPropertyAssigned("Value"))
									if (mdMapping.Value == 0.0 && mdMapping.Validity == MeterDataValidity.Invalid)
										return false;
							}
							break;
					}
				}
			}
			return true;
		}
	}
}

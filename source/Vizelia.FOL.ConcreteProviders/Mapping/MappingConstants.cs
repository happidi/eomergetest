﻿namespace Vizelia.FOL.ConcreteProviders {

	/// <summary>
	/// A static helper for Mapping.
	/// </summary>
	internal static class MappingConstants {

		/// <summary>
		/// The actions array
		/// </summary>
		public static readonly string[] Actions = new[] { const_action_type_destroy, const_action_type_create, const_action_type_update };

		/// <summary>
		/// The value of attribute type for action node corresponding to a create.
		/// </summary>
		public const string const_action_type_create = "create";

		/// <summary>
		/// The value of attribute type for action node corresponding to a destroy.
		/// </summary>
		public const string const_action_type_destroy = "destroy";

		/// <summary>
		/// The value of attribute type for action node corresponding to an update.
		/// </summary>
		public const string const_action_type_update = "update";

		/// <summary>
		/// Gets the action multiplier.
		/// </summary>
		public static double ActionMultiplier {
			get {
				double actionMultiplier = (1 / (1.0 * Actions.Length));
				return actionMultiplier;
			}
		}
	}
}

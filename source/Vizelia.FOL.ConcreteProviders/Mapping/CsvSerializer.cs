﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using Kent.Boogaart.KBCsv;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessEntities.Mapping;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {

	/// <summary>
	/// A CSV Serializer that serializes primitive members mostly
	/// </summary>
	public class CsvSerializer {
		private readonly string m_NullExpression;

		/// <summary>
		/// Initializes a new instance of the <see cref="CsvSerializer"/> class.
		/// </summary>
		/// <param name="nullExpression">The null expression.</param>
		public CsvSerializer(string nullExpression) {
			m_NullExpression = nullExpression;
		}

		/// <summary>
		/// The default flat file field delimiter.
		/// </summary>
		private const char const_default_flatfile_seperator_character = ',';

		/// <summary>
		/// An event that is fired when a deserialization error occures.
		/// </summary>
		public event EventHandler<CsvDeserializationErrorEventArgs> DeserializationError;
	
		/// <summary>
		/// Serializes a single object.
		/// </summary>
		/// <param name="objectToSerialize">The object to serialize.</param>
		/// <returns>The serialized object</returns>
		public string SerializeObject(object objectToSerialize) {
			// Create parameter for method
			Type listType = typeof(List<>).MakeGenericType(objectToSerialize.GetType());
			var list = (IList)Activator.CreateInstance(listType);
			list.Add(objectToSerialize);

			// Invoke method using reflection since type is dynamic
			MethodInfo serializeObjectsGenericMethod = typeof(CsvSerializer).GetMethod("SerializeObjects", BindingFlags.Instance | BindingFlags.Public);
			MethodInfo serializeObjectsMethod = serializeObjectsGenericMethod.MakeGenericMethod(objectToSerialize.GetType());
			object result = serializeObjectsMethod.Invoke(this, new object[] { list });
			string serializeSingleObject = (string)result;

			return serializeSingleObject;
		}

		/// <summary>
		/// Serializes the objects.
		/// </summary>
		/// <param name="objectsToSerialize">The objects to serialize.</param>
		/// <returns>The serialized objects</returns>
		public string SerializeObjects<T>(IEnumerable<T> objectsToSerialize) {
			Type typeToSerialize = typeof (T);
			string serializeObjects = SerializeObjects(objectsToSerialize.OfType<object>(), typeToSerialize);

			return serializeObjects;
		}

		/// <summary>
		/// Serializes the objects.
		/// </summary>
		/// <param name="objectsToSerialize">The objects to serialize.</param>
		/// <param name="objectsType">The type of objects to serialize.</param>
		/// <returns></returns>
		public string SerializeObjects(IEnumerable<object> objectsToSerialize, Type objectsType) {
			List<PropertyInfo> serializableProperties = GetSerializableProperties(objectsType);

			string[] propertyNames = serializableProperties.Select(p => p.Name).ToArray();
			var headerRecord = new HeaderRecord(propertyNames);
			var memoryStream = new MemoryStream();

			using (var writer = new CsvWriter(memoryStream)) {
				// Write header row
				writer.WriteHeaderRecord(headerRecord);

				// Convert each object into a row in the CSV
				var dataRecords = from currentObject in objectsToSerialize
				                  let currentObjectValues = GetPropertyValues(serializableProperties, currentObject)
				                  let record = new DataRecord(headerRecord, currentObjectValues)
				                  select record;

				List<DataRecord> recordsToWrite = dataRecords.ToList();
				writer.WriteDataRecords(recordsToWrite);

				// Get the string of serialized objects from the stream
				writer.Flush();
				memoryStream.Seek(0, SeekOrigin.Begin);

				using (var reader = new StreamReader(memoryStream)) {
					string serializedObjects = reader.ReadToEnd();

					// Trim newline characters in end of CSV.
					serializedObjects = serializedObjects.TrimEnd('\r', '\n');

					return serializedObjects;
				}
			}
		}


		/// <summary>
		/// Gets the property values of an object.
		/// </summary>
		/// <param name="serializableProperties">The serializable properties.</param>
		/// <param name="objectToSerialize">The object to serialize.</param>
		private string[] GetPropertyValues(IEnumerable<PropertyInfo> serializableProperties, object objectToSerialize) {
			var values = from p in serializableProperties
			             let value = GetPropertyValue(p, objectToSerialize)
			             let outputValue = (value == null) ? null : value.ToString()
			             select outputValue;

			string[] valuesArray = values.ToArray();

			return valuesArray;
		}

		private object GetPropertyValue(PropertyInfo property, object objectToSerialize) {
			object propertyValue = property.GetValue(objectToSerialize, null);

			if(propertyValue == null) {
				propertyValue = m_NullExpression;
			}
			else if (property.PropertyType == typeof(DateTime)) {
				// Special handling for DateTime as it needs formatting
				var dateTime = (DateTime)propertyValue;
				propertyValue = XmlConvert.ToString(dateTime, XmlDateTimeSerializationMode.RoundtripKind);
			}
			else if (property.PropertyType == typeof(DateTime?)) {
				// And the same for Nullable DateTime
				var nullableDateTime = (DateTime?)propertyValue;
				propertyValue = XmlConvert.ToString(nullableDateTime.Value, XmlDateTimeSerializationMode.RoundtripKind);
			}
			else if (property.PropertyType == typeof(int)) {
				var v = (int) propertyValue;
				propertyValue = v.ToString(CultureInfo.InvariantCulture);
			}
			else if (property.PropertyType == typeof(double)) {
				var v = (double)propertyValue;
				propertyValue = v.ToString(CultureInfo.InvariantCulture);
			}
			else if (property.PropertyType == typeof(float)) {
				var v = (float)propertyValue;
				propertyValue = v.ToString(CultureInfo.InvariantCulture);
			}
			else if (property.PropertyType == typeof(uint)) {
				var v = (uint)propertyValue;
				propertyValue = v.ToString(CultureInfo.InvariantCulture);
			}
			else if (property.PropertyType == typeof(short)) {
				var v = (short)propertyValue;
				propertyValue = v.ToString(CultureInfo.InvariantCulture);
			}
			else if (property.PropertyType == typeof(ushort)) {
				var v = (ushort)propertyValue;
				propertyValue = v.ToString(CultureInfo.InvariantCulture);
			}
			else if (property.PropertyType == typeof(long)) {
				var v = (long)propertyValue;
				propertyValue = v.ToString(CultureInfo.InvariantCulture);
			}
			else if (property.PropertyType == typeof(ulong)) {
				var v = (ulong)propertyValue;
				propertyValue = v.ToString(CultureInfo.InvariantCulture);
			}
			else if (property.PropertyType == typeof(decimal)) {
				var v = (decimal)propertyValue;
				propertyValue = v.ToString(CultureInfo.InvariantCulture);
			}
			
			return propertyValue;
		}

		
		/// <summary>
		/// Deserializes a single object from the stream by the CSV header row.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stream">The stream.</param>
		public object DeserializeObject<T>(Stream stream) {
			object deserializeObject = DeserializeObject(stream, typeof(T));

			return deserializeObject;
		}

		/// <summary>
		/// Deserializes the objects in a stream by the CSV header row.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stream">The stream.</param>
		public IEnumerable<T> DeserializeObjects<T>(Stream stream) {
			IEnumerable<object> deserializeObjects = DeserializeObjects(stream, typeof(T), const_default_flatfile_seperator_character);
			IEnumerable<T> objects = deserializeObjects.Cast<T>();

			return objects;
		}

		/// <summary>
		/// Deserializes a single object from the stream by the CSV header row.
		/// </summary>
		/// <param name="stream">The stream.</param>
		/// <param name="type">The type.</param>
		public object DeserializeObject(Stream stream, Type type) {
			var objects = DeserializeObjects(stream, type, const_default_flatfile_seperator_character);

			return objects.First();
		}

		/// <summary>
		/// Deserializes the objects in a stream by the CSV header row.
		/// </summary>
		/// <param name="stream">The stream.</param>
		/// <param name="type">The type.</param>
		public IEnumerable<object> DeserializeObjects(Stream stream, Type type) {
			var objects = DeserializeObjects(stream, type, const_default_flatfile_seperator_character);

			return objects;
		}

		/// <summary>
		/// Deserializes the objects in a stream by the CSV header row.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <param name="type">The type.</param>
		public IEnumerable<object> DeserializeObjects(TextReader reader, Type type) {
			var objects = DeserializeObjects(reader, type, const_default_flatfile_seperator_character);

			return objects;
		}

		/// <summary>
		/// Deserializes the objects.
		/// </summary>
		/// <param name="textReader">The text reader.</param>
		/// <param name="type">The type.</param>
		/// <param name="valueSeparator">The value separator.</param>
		/// <returns></returns>
		private IEnumerable<object> DeserializeObjects(TextReader textReader, Type type, char valueSeparator) {
			using (var reader = new CsvReader(textReader)) {
				IEnumerable<object> objects = DeserializeObjects(type, valueSeparator, reader);

				// We need to yield return here so the CsvReader won't be disposed until done deserializing.
				foreach (object currentObject in objects) {
					yield return currentObject;
				}
			}
		}

		/// <summary>
		/// Deserializes the objects in a stream by the CSV header row.
		/// </summary>
		/// <param name="stream">The stream.</param>
		/// <param name="type">The type.</param>
		/// <param name="valueSeparator">The value separator.</param>
		public IEnumerable<object> DeserializeObjects(Stream stream, Type type, char valueSeparator) {
			using (var reader = new CsvReader(stream)) {
				IEnumerable<object> objects = DeserializeObjects(type, valueSeparator, reader);

				// We need to yield return here so the CsvReader won't be disposed until done deserializing.
				foreach (object currentObject in objects) {
					yield return currentObject;
				}
			}
		}

		/// <summary>
		/// Deserializes the objects.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="valueSeparator">The value separator.</param>
		/// <param name="reader">The reader.</param>
		private IEnumerable<object> DeserializeObjects(Type type, char valueSeparator, CsvReader reader) {
			List<PropertyInfo> serializableProperties = GetSerializableProperties(type);
			reader.ValueSeparator = valueSeparator;
			reader.ReadHeaderRecord();

			foreach (DataRecord record in reader.DataRecords) {
				object deserializedObject = DeserializeObject(type, record, serializableProperties);
				if (deserializedObject != null) {
					yield return deserializedObject;
				}
			}

		}

		/// <summary>
		/// Deserializes the object from a single data record.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="record">The record.</param>
		/// <param name="serializableProperties">The serializable properties.</param>
		/// <returns></returns>
		private object DeserializeObject(Type type, DataRecord record, List<PropertyInfo> serializableProperties) {
			try {
				object deserializedInstance = Activator.CreateInstance(type);
				PopulatePrimitiveProperties(deserializedInstance, record, serializableProperties);
				PopulateDynamicProperties(deserializedInstance, record, type, serializableProperties);

				return deserializedInstance;
			}
			catch(Exception exception) {
				// Raise an event instead of throwing the exception since we want to try deserialize other data records too.
				if (DeserializationError != null) {
					var eventArgs = new CsvDeserializationErrorEventArgs(exception);
					DeserializationError(this, eventArgs);
				}

				return null;
			}
		}

		/// <summary>
		/// Populates the dynamic properties, which are properties that are not primitives and cannot be assigned trivially.
		/// These properties will be assigned by the logic of the deserialized instance itself.
		/// </summary>
		/// <param name="deserializedInstance">The deserialized instance.</param>
		/// <param name="record">The record.</param>
		/// <param name="type">The type.</param>
		/// <param name="serializableProperties">The serializable properties.</param>
		private void PopulateDynamicProperties(object deserializedInstance, DataRecord record, Type type, IEnumerable<PropertyInfo> serializableProperties) {
			// Get properties that appear in the CSV file but not on the entity.
			List<string> propertyNames = serializableProperties.Select(p => p.Name).ToList();
			IEnumerable<string> dynamicPropertyNames = record.HeaderRecord.Values.Except(propertyNames).ToList();

			if (!dynamicPropertyNames.Any()) {
				// Nothing to do.
				return;
			}

			var csvSerializableInstance = deserializedInstance as ICsvSerializable;
			if (csvSerializableInstance == null) {
				throw new ArgumentException(
					string.Format(
						"Error deserializing object of type {0} since it is not ICsvSerializable but it contains the following dynamic properties: {1}.",
						type.FullName, string.Join(",", dynamicPropertyNames.ToArray())));
			}

			foreach (string dynamicPropertyName in dynamicPropertyNames) {
				string value = record.GetValueOrNull(dynamicPropertyName);
				if (value != null) {
					csvSerializableInstance.Set(dynamicPropertyName, value);
				}
			}
		}

		/// <summary>
		/// Populates the primitive properties into the deserialized instance.
		/// </summary>
		/// <param name="deserializedInstance">The deserialized instance.</param>
		/// <param name="record">The record.</param>
		/// <param name="serializableProperties">The serializable properties.</param>
		private void PopulatePrimitiveProperties(object deserializedInstance, DataRecord record, IEnumerable<PropertyInfo> serializableProperties) {
			foreach (PropertyInfo property in serializableProperties) {
				string value = record.GetValueOrNull(property.Name);
				if (value != null) {
					// Assign value to property
					object newValue = ConvertValueToType(value, property.PropertyType);
					property.SetValue(deserializedInstance, newValue, null);
				}
			}
		}
		
		private object ConvertValueToType(string value, Type propertyType) {
			object newValue = null;
			var parseSuccess = true;

			if (m_NullExpression.Equals(value)) {
				 newValue = null;
			}
			else if(propertyType == typeof(string)) {
				newValue = value;
			}
			else if (propertyType == typeof(char)) {
				char newValueLocal;
				parseSuccess = char.TryParse(value, out newValueLocal);
				if (parseSuccess) {
					newValue = newValueLocal;
				}				
			}
			else if (propertyType == typeof(bool)) {
				bool newValueLocal;
				parseSuccess = bool.TryParse(value, out newValueLocal);
				if (parseSuccess) {
					newValue = newValueLocal;
				}		
			}
			else if (propertyType == typeof(int)) {
				int newValueLocal;
				parseSuccess = int.TryParse(value, out newValueLocal);
				if (parseSuccess) {
					newValue = newValueLocal;
				}		
			}
			else if (propertyType == typeof(double)) {
				double newValueLocal;
				parseSuccess = double.TryParse(value, out newValueLocal);
				if (parseSuccess) {
					newValue = newValueLocal;
				}		
			}
			else if (propertyType == typeof(float)) {
				float newValueLocal;
				parseSuccess = float.TryParse(value, out newValueLocal);
				if (parseSuccess) {
					newValue = newValueLocal;
				}		
			}
			else if (propertyType == typeof(uint)) {
				uint newValueLocal;
				parseSuccess = uint.TryParse(value, out newValueLocal);
				if (parseSuccess) {
					newValue = newValueLocal;
				}		
			}
			else if (propertyType == typeof(short)) {
				short newValueLocal;
				parseSuccess = short.TryParse(value, out newValueLocal);
				if (parseSuccess) {
					newValue = newValueLocal;
				}		
			}
			else if (propertyType == typeof(ushort)) {
				ushort newValueLocal;
				parseSuccess = ushort.TryParse(value, out newValueLocal);
				if (parseSuccess) {
					newValue = newValueLocal;
				}		
			}
			else if (propertyType == typeof(long)) {
				long newValueLocal;
				parseSuccess = long.TryParse(value, out newValueLocal);
				if (parseSuccess) {
					newValue = newValueLocal;
				}		
			}
			else if (propertyType == typeof(ulong)) {
				ulong newValueLocal;
				parseSuccess = ulong.TryParse(value, out newValueLocal);
				if (parseSuccess) {
					newValue = newValueLocal;
				}		
			}
			else if (propertyType == typeof(decimal)) {
				decimal newValueLocal;
				parseSuccess = decimal.TryParse(value, out newValueLocal);
				if (parseSuccess) {
					newValue = newValueLocal;
				}		
			}
			else if (propertyType == typeof(Guid)) {
				Guid newValueLocal;
				parseSuccess = Guid.TryParse(value, out newValueLocal);
				if (parseSuccess) {
					newValue = newValueLocal;
				}		
			}
			else if (propertyType == typeof(DateTime)) {
				try {
					DateTime dateTime = XmlConvert.ToDateTime(value, XmlDateTimeSerializationMode.RoundtripKind);
					newValue = dateTime;
				}
				catch (Exception) {
					parseSuccess = false;
				}
			}
			else if (propertyType.IsEnum) {
				try {
					newValue = Enum.Parse(propertyType, value);
					if (!Enum.IsDefined(propertyType, newValue)) {
						parseSuccess = false;
					}
				}
				catch (Exception) {
					parseSuccess = false;
				}
			}
			else if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(DefaultMappingCrudStoreMapping<,>)) {
				try {
					string[] localIds = value.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
					newValue = Activator.CreateInstance(propertyType, new object[] { localIds });
				}
				catch (Exception) {
					parseSuccess = false;
				}
			}
			else if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>)) {
				try {
					Type valueTypeInsideNullable = propertyType.GetGenericArguments()[0];
					newValue = ConvertValueToType(value, valueTypeInsideNullable);
				}
				catch (Exception) {
					parseSuccess = false;
				}
			}
			else {
				throw new ArgumentException("Unknown property type");
			}

			if (!parseSuccess) {
				throw new ArgumentException("Cannot convert " + (string.IsNullOrEmpty(value) ? "(empty string)" : value) + " to type " + propertyType.Name);
			}

			return newValue;
		}

		/// <summary>
		/// Gets the serializable properties of the given type.
		/// </summary>
		/// <param name="type">The type.</param>
		private static List<PropertyInfo> GetSerializableProperties(Type type) {
			PropertyInfo[] allProperties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

			List<PropertyInfo> relevantProperties =
				allProperties.Where(IsCsvSerializableProperty).ToList();

			return relevantProperties;
		}

		/// <summary>
		/// Determines whether the property is of a type that is CSV serializable or not.
		/// </summary>
		/// <param name="property">The property.</param>
		/// <returns>
		///   <c>true</c> if the property is of a type that is CSV serializable; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsCsvSerializableProperty(PropertyInfo property) {
			bool isAllowedType = IsAllowedType(property.PropertyType);

			// Do not serialize out the "Action" property. It is used only in import.
			bool isMappingActionProperty =
				property.Name.Equals("Action") &&
				property.DeclaringType != null &&
				property.DeclaringType.IsGenericType &&
				property.DeclaringType.GetGenericTypeDefinition() == typeof (Mapping<>);

			bool isCsvSerializableProperty =
				isAllowedType &&
				!isMappingActionProperty &&
				property.GetSetMethod() != null &&
				property.GetGetMethod() != null;

			return isCsvSerializableProperty;
		}

		/// <summary>
		/// Determines whether a type is allowed for flat file serialization or not.
		/// </summary>
		/// <param name="propertyType">Type of the property.</param>
		/// <returns>
		///   <c>true</c> if the type is allowed for flat file serialization; otherwise, <c>false</c>.
		/// </returns>
		private static bool IsAllowedType(Type propertyType) {
			// We allow only string, DateTime, enums, primitives and Nullable<T> where T is an allowed type.
			// Also, we allow DefaultMappingCrudStoreMapping<,> since it will be inputted as a CSV string.
			// You cannot represent complex types in a CSV file.
			bool isAllowedType;

			if (propertyType.IsGenericType) {
				Type[] genericArguments = propertyType.GetGenericArguments();
				Type genericTypeDefinition = propertyType.GetGenericTypeDefinition();

				isAllowedType =
					(genericTypeDefinition == typeof(DefaultMappingCrudStoreMapping<,>)) ||
					(genericTypeDefinition == typeof(Nullable<>) && IsAllowedType(genericArguments[0]));
			}
			else {
				isAllowedType = propertyType.IsPrimitive ||
								propertyType == typeof(string) ||
								propertyType == typeof(DateTime) ||
								propertyType.IsEnum;
			}

			return isAllowedType;
		}
	}

}
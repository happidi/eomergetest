using Vizelia.FOL.BusinessEntities.Mapping;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A deserialized mappable entity that indicates that every field has been assigned to, thus always
	/// overwriting property values of existing entities.
	/// </summary>
	public class OverwritingMappingRecord<TEntity> : MappingRecord<TEntity> where TEntity : IMappableEntity {

		/// <summary>
		/// Initializes a new instance of the <see cref="OverwritingMappingRecord&lt;TEntity&gt;"/> class.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public OverwritingMappingRecord(TEntity entity)
			: base(entity) {
			// Do nothing.
		}

		/// <summary>
		/// Determines whether the given field has been assigned with a value or not.
		/// </summary>
		/// <param name="propertyName">Name of the field.</param>
		/// <returns>
		///   <c>true</c> if the given field has been assigned with a value; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsPropertyAssigned(string propertyName) {
			return true;
		}

		/// <summary>
		/// Marks the property as assigned.
		/// </summary>
		/// <param name="name">The property name.</param>
		protected override void MarkPropertyAsAssigned(string name) {
			// Do nothing.
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;
using System.IO;
using System.Configuration.Provider;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a of a WorkflowViewer provider for WF 4.
	/// </summary>
	public class WF4WorkflowViewerProvider : WorkflowViewerProvider {

		private const string const_attribute_tempdirectory = "tempDirectory";
		private const string const_attribute_xamlfolder = "xamlFolder";
		private const string const_attribute_exename = "exeName";
		private const string const_attribute_htmlcolorcurrentstate = "htmlColorCurrentState";
		private const string const_attribute_enablelocalization = "enableLocalization";

		const string const_namespace_activities = "http://schemas.microsoft.com/netfx/2009/xaml/activities";
		const string const_namespace_vizelia = "clr-namespace:Vizelia.FOL.WF.Activities;assembly=Vizelia.FOL.WF.Activities";


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_tempdirectory])) {
				TempDirectory = VizeliaConfiguration.Instance.Deployment.TempDirectory;
			}
			else {
				TempDirectory = config[const_attribute_tempdirectory];
				if (!Directory.Exists(TempDirectory))
					throw new ProviderException(const_attribute_tempdirectory + " does not exist");
				TempDirectory = Path.GetFullPath(TempDirectory);
			}
			config.Remove(const_attribute_tempdirectory);

			if (String.IsNullOrEmpty(config[const_attribute_xamlfolder])) {
				throw new ProviderException(const_attribute_xamlfolder + " is missing");
			}
			XamlFolder = config[const_attribute_xamlfolder];
			config.Remove(const_attribute_xamlfolder);

			if (String.IsNullOrEmpty(config[const_attribute_exename])) {
				throw new ProviderException(const_attribute_exename + " is missing");
			}
			ExeName = config[const_attribute_exename];
			config.Remove(const_attribute_exename);

			// set a default value for HtmlColorCurrentState;
			HtmlColorCurrentState = ColorTranslator.ToHtml(Color.Yellow);
			if (!String.IsNullOrEmpty(config[const_attribute_htmlcolorcurrentstate])) {
				try {
					var value = config[const_attribute_htmlcolorcurrentstate];
					var color = ColorTranslator.FromHtml(value);
					HtmlColorCurrentState = value;
				}
				catch {
					throw new ProviderException(const_attribute_htmlcolorcurrentstate + " has an invalid value");
				}

				config.Remove(const_attribute_htmlcolorcurrentstate);
			}

			if (String.IsNullOrEmpty(config[const_attribute_enablelocalization])) {
				throw new ProviderException(const_attribute_enablelocalization + " is missing");
			}
			try {
				EnableLocalization = Convert.ToBoolean(config[const_attribute_enablelocalization]);
			}
			catch {
				throw new ProviderException(const_attribute_enablelocalization + " has an invalid value");
			}
			config.Remove(const_attribute_enablelocalization);


			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}

		/// <summary>
		/// Gets or sets a value indicating whether the viewer should enable localization of the workflow image.
		/// </summary>
		public bool EnableLocalization { get; set; }

		/// <summary>
		/// Gets or sets the temp directory location.
		/// </summary>
		/// <value>
		/// The temp directory location.
		/// </value>
		public string TempDirectory { get; set; }

		/// <summary>
		/// Gets or sets the name of the folder where xaml workflow files are stored under the web application.(Example xaml)
		/// </summary>
		/// <value>
		/// The xaml directory name.
		/// </value>
		public string XamlFolder { get; set; }

		/// <summary>
		/// Gets or sets the name of the exe responsible for generating the image.
		/// </summary>
		/// <value>
		/// The name of the exe.
		/// </value>
		public string ExeName { get; set; }


		/// <summary>
		/// Gets or sets the color (html format) that the current state should be hilighted with.
		/// </summary>
		public string HtmlColorCurrentState { get; set; }

		/// <summary>
		/// Renders the workflow as an image stream.
		/// </summary>
		/// <param name="xamlFileName">The xaml file name (no extension, no path).</param>
		/// <param name="xamlDesign">The xaml design.</param>
		/// <param name="currentState">The current state if the workflow.</param>
		/// <param name="assembliesToLoad">The list of the assemblies to load.</param>
		/// <param name="width">The width of the image.</param>
		/// <param name="height">The height of the image.</param>
		/// <returns></returns>
		public override StreamResult RenderWorkflowStream(string xamlFileName, string xamlDesign, string currentState, string[] assembliesToLoad, int width, int height) {
			var xamlFile = AppDomain.CurrentDomain.BaseDirectory + XamlFolder + "\\" + xamlFileName + ".xaml";
			string resultFile = Path.Combine(TempDirectory, "workflowImage" + Guid.NewGuid() + ".png");

			string exeFile = AppDomain.CurrentDomain.BaseDirectory + "bin\\" + ExeName;

			// logic when localization is enabled.
			if (EnableLocalization) {
				xamlDesign = LocalizeXaml(xamlDesign);
				var localizedState = Helper.LocalizeText("msg_workflow_state_" + currentState.ToLower(), true);
				if (localizedState != null)
					currentState = localizedState;
			}

			var arg = new WorkflowViewerArgument {
				XamlFile = xamlFile,
				XamlDesign = LocalizeXaml(xamlDesign),
				AssembliesToLoad = assembliesToLoad,
				CurrentState = currentState,
				Width = width,
				Height = height,
				ResultFile = resultFile,
				HtmlColorCurrentState = HtmlColorCurrentState
			};

			string arguments = Helper.SerializeXml(arg);
			arguments = arguments.Replace("\"", "\"\"");
			arguments = ConvertToArgument(arguments);

			using (Process p = new Process {
				StartInfo = {
					FileName = exeFile,
					Arguments = arguments
				}
			}) {
				p.Start();
				p.StartInfo.CreateNoWindow = false;
				p.WaitForExit();
			}
			MemoryStream memoryStream = ImageHelper.ConvertImageFileToMemoryStream(resultFile, true);
			var retVal = new StreamResult(memoryStream, MimeType.Png);
			return retVal;

		}

		/// <summary>
		/// Localizes the xaml.
		/// </summary>
		/// <param name="xaml">The xaml.</param>
		/// <returns></returns>
		private string LocalizeXaml(string xaml) {
			var xmlDoc = XDocument.Load(new StringReader(xaml));
			XNamespace xmlnsActivities = const_namespace_activities;
			XNamespace xmlnsVizelia = const_namespace_vizelia;
			var states = (from l in xmlDoc.Descendants(xmlnsActivities + "State") select l);
			var eventHandlerActivities = (from l in xmlDoc.Descendants(xmlnsVizelia + "EventHandlerActivity") select l);

			foreach (var state in states) {
				var xAttributeDisplayName = state.Attribute("DisplayName");
				if (xAttributeDisplayName == null) continue;
				var translatedValue = Helper.LocalizeText("msg_workflow_state_" + xAttributeDisplayName.Value.ToLower(), true);
				if (translatedValue != null)
					xAttributeDisplayName.SetValue(translatedValue);
			}

			foreach (var eventHandlerActivity in eventHandlerActivities) {
				var xAttributeMsgCode = eventHandlerActivity.Attribute("MsgCode");
				if (xAttributeMsgCode == null) continue;
				string msgCode = xAttributeMsgCode.Value;
				var transition = eventHandlerActivity.Ancestors(xmlnsActivities + "Transition").FirstOrDefault();
				if (transition == null) continue;
				XAttribute xAttributeDisplayName = transition.Attribute("DisplayName");
				if (xAttributeDisplayName == null) continue;
				var translatedValue = Helper.LocalizeText(msgCode, true);
				if (translatedValue != null)
					xAttributeDisplayName.SetValue(translatedValue);
			}
			var result = xmlDoc.ToString(SaveOptions.None);

			return result;
		}


		/// <summary>
		/// Converts to argument.
		/// </summary>
		/// <param name="strArg">The string argument.</param>
		/// <returns></returns>
		private static string ConvertToArgument(string strArg) {
			return "\"" + strArg + "\" ";
		}




		/// <summary>
		/// Gets the xaml folder.
		/// </summary>
		/// <returns></returns>
		public override string GetXamlFolder() {
			return this.XamlFolder;
		}
	}
}

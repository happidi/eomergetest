﻿using System;
using System.Data;
using System.Linq;
using RazorEngine.Templating;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// 
	/// </summary>
	public class ChartTemplateBase : TemplateBaseProvider {
		/// <summary>
		/// Gets the template.
		/// </summary>
		/// <returns></returns>
		public override Type GetTemplateBaseType() {
			var retVal = typeof(ChartRazorTemplateBase<>);
			return retVal;
		}

	}

	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class ChartRazorTemplateBase<T> : TemplateBase<T> {
		/// <summary>
		/// Gets wanted property by the Serie name.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="serieLocalId">The serie local id.</param>
		/// <param name="wantedProperty">The wanted property.</param>
		/// <param name="dateOrIndexFormat">The date or index format.</param>
		/// <param name="dateFormat">The date format.</param>
		/// <returns></returns>
		public string SerieName(T chart, string serieLocalId, string wantedProperty, string dateOrIndexFormat = null, string dateFormat = null) {
			var retVal = "";
			var chartEntity = chart as Chart;
			if (chartEntity != null) {
				DataSerie serie;
				if (chartEntity.Series.TryGetDataSerie(serieLocalId, out serie))
					retVal = GetSerieProperty(chartEntity, serie, wantedProperty, dateOrIndexFormat, dateFormat);
				else if (chartEntity.HistoricalAnalysis != null) {
					var serieFound = false;
					var i = 0;
					var historicalAnalysisDataSeries = chartEntity.HistoricalAnalysis.Values.ToList();
					while (!serieFound && i < chartEntity.HistoricalAnalysis.Count) {
						if (historicalAnalysisDataSeries[i].TryGetDataSerie(serieLocalId, out serie)) {
							serieFound = true;
							retVal = GetSerieProperty(chartEntity, serie, wantedProperty, dateOrIndexFormat, dateFormat);
						}
						i++;
					}
				}
			}
			return retVal;
		}
		/// <summary>
		/// Gets wanted property by the Serie rank.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="serieRank">The serie rank.</param>
		/// <param name="wantedProperty">The wanted property.</param>
		/// <param name="dateOrIndexFormat">The date or index format.</param>
		/// <param name="dateFormat">The date format.</param>
		/// <returns></returns>
		public string SerieRank(T chart, int serieRank, string wantedProperty, string dateOrIndexFormat = null, string dateFormat = null) {
			var retVal = "";

			var chartEntity = chart as Chart;
			if (chartEntity != null && chartEntity.Series != null) {
				var series = chartEntity.Series.GetVisibleDataSeries().ToList();
				foreach (var kvp in chartEntity.HistoricalAnalysis.Where(kvp => kvp.Value != null)) {
					series.AddRange(kvp.Value.GetVisibleDataSeries());
				}
				if (series.Count >= serieRank) {
					DataSerie serie = series[serieRank - 1];
					retVal = GetSerieProperty(chartEntity, serie, wantedProperty, dateOrIndexFormat, dateFormat);
				}
			}
			return retVal;
		}

		/// <summary>
		/// Gets the specified property of the chart.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="wantedProperty">The wanted property.</param>
		/// <param name="dateFormat">The date format.</param>
		/// <returns></returns>
		public string Chart(T chart, string wantedProperty, string dateFormat = null) {
			var retVal = "";
			var chartEntity = chart as Chart;
			if (chartEntity != null) {
				if (dateFormat == null) {
					dateFormat = chartEntity.TimeInterval.GetFormat();
				}
				var startDate = chartEntity.StartDate;
				var endDate = chartEntity.EndDate.AddMilliseconds(-1);
				switch (wantedProperty) {
					case "Title": {
							retVal = chartEntity.Title;
							break;
						}
					case "Grid": {
							DataTable dataTable;

							if (chartEntity.GridTranspose) {
								dataTable = ChartHelper.ToDataTableTransposedPaged(chartEntity, false, 0, 50);
							}
							else {
								dataTable = ChartHelper.ToDataTablePaged(chartEntity, false, 0, 50);
							}

							retVal = ChartHelper.ToHtml(dataTable);
							break;
						}
					case "StartDate": {
							retVal = chartEntity.StartDate.ConvertTimeFromUtc(chartEntity.GetTimeZone()).ToString(dateFormat);
							break;
						}
					case "EndDate": {
							retVal = chartEntity.EndDate.ConvertTimeFromUtc(chartEntity.GetTimeZone()).ToString(dateFormat);
							break;
						}
					case "Now": {
							retVal = Helper.DateTimeUtcNow.ConvertTimeFromUtc(chartEntity.GetTimeZone()).ToString(dateFormat);
							break;
						}
					case "TimeInterval": {
							var msgCode = EnumExtension.GetEnumValueLocalizedText(chartEntity.TimeInterval.GetType(), chartEntity.TimeInterval.ParseAsInt());
							retVal = Helper.LocalizeText(msgCode);
							break;
						}
					case "NumberOfYears":
						retVal = (endDate.Year - startDate.Year).ToString(chartEntity.NumericFormat);
						break;
					case "NumberOfMonths":
						retVal = ((endDate.Year - startDate.Year) * 12 + endDate.Month - startDate.Month + 1).ToString(chartEntity.NumericFormat);
						break;
					case "NumberOfDays":
						retVal = (endDate - startDate).TotalDays.ToString(chartEntity.NumericFormat);
						break;
					case "NumberOfHours":
						retVal = (endDate - startDate).TotalHours.ToString(chartEntity.NumericFormat);
						break;
					case "NumberOfMinutes":
						retVal = (endDate - startDate).TotalMinutes.ToString(chartEntity.NumericFormat);
						break;
					case "NumberOfSeconds":
						retVal = (endDate - startDate).TotalSeconds.ToString(chartEntity.NumericFormat);
						break;
				}
			}
			return retVal;
		}

		/// <summary>
		/// Gets the wanted property from all the series.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <param name="wantedProperty">The wanted property.</param>
		/// <returns></returns>
		public string AllSeries(T chart, string wantedProperty) {
			var retVal = "";
			var chartEntity = chart as Chart;
			if (chartEntity != null) {
				switch (wantedProperty) {
					case "MinElem": {
							retVal = ChartHelper.GetMinElement(chartEntity).ToString(chartEntity.NumericFormat);
							break;
						}
					case "MaxElem": {
							retVal = ChartHelper.GetMaxElement(chartEntity).ToString(chartEntity.NumericFormat);
							break;
						}
					case "AvgElem": {
							retVal = ChartHelper.GetAvgElement(chartEntity).ToString(chartEntity.NumericFormat);
							break;
						}
					case "SumElem": {
							retVal = ChartHelper.GetSumElement(chartEntity).ToString(chartEntity.NumericFormat);
							break;
						}
				}
			}
			return retVal;
		}

		private string GetSerieProperty(Chart chart, DataSerie serie, string wantedProperty, string dateOrIndexFormat = null, string dateFormat = null) {
			var retVal = "";

			dateFormat = dateFormat ?? DateTimeHelper.DateTimeFormat(false);
			switch (wantedProperty) {
				case "Name": {
						retVal = serie.Name;
						break;
					}
				case "Sum": {
						if (serie.HasPointsWithValue)
							retVal = DataSerieHelper.Sum(serie).ToString(chart.NumericFormat);
						break;
					}
				case "Average": {
						if (serie.HasPointsWithValue)
							retVal = DataSerieHelper.Avg(serie).ToString(chart.NumericFormat);
						break;
					}
				case "Minimum": {
						if (serie.HasPointsWithValue)
							retVal = DataSerieHelper.Min(serie).ToString(chart.NumericFormat);
						break;
					}
				case "Maximum": {
						if (serie.HasPointsWithValue)
							retVal = DataSerieHelper.Max(serie).ToString(chart.NumericFormat);
						break;
					}
				case "First": {
						if (serie.HasPointsWithValue)
							retVal = DataSerieHelper.First(serie).ToString(chart.NumericFormat);
						break;
					}
				case "Penultimate": {
						if (serie.HasPointsWithValue)
							retVal = DataSerieHelper.Penultimate(serie).ToString(chart.NumericFormat);
						break;
					}
				case "Last": {
						if (serie.HasPointsWithValue)
							retVal = DataSerieHelper.Last(serie).ToString(chart.NumericFormat);
						break;
					}

				case "Index": {
						if (serie.HasPointsWithValue) {
							int index;
							if (int.TryParse(dateOrIndexFormat, out index)) {
								retVal = DataSerieHelper.Index(serie, index).ToString(chart.NumericFormat);
							}
						}
						break;
					}

				case "MinimumDate": {
						if (serie.HasPointsWithValue)
							retVal = DataSerieHelper.MinDate(serie).ToString(dateOrIndexFormat ?? DateTimeHelper.DateTimeFormat(false));
						break;
					}
				case "MaximumDate": {
						if (serie.HasPointsWithValue)
							retVal = DataSerieHelper.MaxDate(serie).ToString(dateOrIndexFormat ?? DateTimeHelper.DateTimeFormat(false));
						break;
					}
				case "FirstDate": {
						if (serie.HasPointsWithValue)
							retVal = DataSerieHelper.FirstDate(serie).ToString(dateOrIndexFormat ?? DateTimeHelper.DateTimeFormat(false));
						break;
					}
				case "PenultimateDate": {
						if (serie.HasPointsWithValue)
							retVal = DataSerieHelper.PenultimateDate(serie).ToString(dateOrIndexFormat ?? DateTimeHelper.DateTimeFormat(false));
						break;
					}
				case "LastDate": {
						if (serie.HasPointsWithValue)
							retVal = DataSerieHelper.LastDate(serie).ToString(dateOrIndexFormat ?? DateTimeHelper.DateTimeFormat(false));
						break;
					}

				case "IndexDate": {
						if (serie.HasPointsWithValue) {
							int index;
							if (int.TryParse(dateOrIndexFormat, out index)) {
								retVal = DataSerieHelper.IndexDate(serie, index).ToString(dateFormat ?? DateTimeHelper.DateTimeFormat(false));
							}
						}
						break;
					}

			}
			return retVal;
		}

		/// <summary>
		/// Helps provide error handling for other functions that report errors as the empty string.
		/// When they have an error, this returns a value of "0".
		/// </summary>
		/// <remarks>
		/// <para>Pass in the result of other functions, like @SerieName, @SerieRank,
		/// @AllSerie, or @Chart, especially when the value is used inside of a formula. 
		/// When the result is a number,
		/// it returns that value unchanged. When it is not a number, such as
		/// the empty string, it returns "0".</para>
		/// <example>
		/// #@ErrorCorrectToZero(@SerieName("SITE1", "Max"))*2#
		/// </example>
		/// </remarks>
		/// <param name="original">The text to determine if it is a number. 
		/// It can be null.</param>
		/// <returns>The original value when its a number and "0" otherwise.</returns>
		public string ErrorCorrectToZero(string original) {
			if (!String.IsNullOrWhiteSpace(original) && 
				original.IsDouble(System.Threading.Thread.CurrentThread.CurrentUICulture.NumberFormat))
				return original;
			return "0";
		}

		/// <summary>
		/// Helps provide error handling for other functions that report errors as the empty string.
		/// When they have an error, this returns a value of "1".
		/// </summary>
		/// <remarks>
		/// <para>Pass in the result of other functions, like @SerieName, @SerieRank,
		/// @AllSerie, or @Chart, especially when the value is used inside of a formula. 
		/// When the result is a number,
		/// it returns that value unchanged. When it is not a number, such as
		/// the empty string, it returns "1".</para>
		/// <example>
		/// #@ErrorCorrectToOne(@SerieName("SITE1", "Max"))*2#
		/// </example>
		/// </remarks>
		/// <param name="original">The text to determine if it is a number. 
		/// It can be null.</param>
		/// <returns>The original value when its a number and "0" otherwise.</returns>
		public string ErrorCorrectToOne(string original) {
			if (!String.IsNullOrWhiteSpace(original) &&
				original.IsDouble(System.Threading.Thread.CurrentThread.CurrentUICulture.NumberFormat))
				return original;
			return "1";
		}
	}
}
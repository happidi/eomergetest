﻿using System;
using System.Collections.Generic;
using System.Linq;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a standard template provider using the engine Razor.
	/// </summary>
	public class RazorTemplateProvider : TemplateProvider {

		private const int const_max_parse_length = 100000;
		/// <summary>
		/// Parse template relying provided data and template base type.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="template">The template.</param>
		/// <param name="data">The data model.</param>
		/// <param name="templateBaseName">Name of the template base.</param>
		/// <returns>
		/// The result string.
		/// </returns>
		public override string Parse<T>(string template, T data, string templateBaseName) {
			var templateBaseType = TemplateBaseService.GetTemplateBaseType(templateBaseName);
			var decodedTemplate = template.HtmlDecode();
			var result = template;
		    if (template.Contains('@')) {
                var originalCulture = Helper.GetCurrentCulture();
		        try {
		            Helper.SetCulture(Helper.GetCurrentUICulture());
                    var config = new TemplateServiceConfiguration {
                        BaseTemplateType = templateBaseType,
                        EncodedStringFactory = new RawStringFactory(),
                        Namespaces = new HashSet<string>(new[] { "Vizelia.FOL.Common.Localization", "Vizelia.FOL.Common" })
                    };

		            Razor.SetTemplateService(new RazorEngine.Templating.TemplateService(config));

		            result = Razor.Parse(decodedTemplate, data);
		        }
		        catch (Exception ex) {
		            result = ex.Message;
		            TracingService.Write(TraceEntrySeverity.Error, ex.Message, "RazorTemplateProvider", templateBaseName);
		        }
		        finally {
		            Helper.SetCulture(originalCulture);
		        }
		    }

			return result;
		}

		/// <summary>
		/// Parse list of templates.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="templatesList">The templates list.</param>
		/// <param name="data">The data.</param>
		/// <param name="templateBaseName">The base template name.</param>
		/// <returns>Result list of strings</returns>
		public override List<string> ParseMany<T>(IEnumerable<string> templatesList, T data, string templateBaseName) {
			var retVal = string.Empty;
			// We add special chars delimiter to the templates.
			var delimiter = " " + Guid.NewGuid() + " ";
			var aggregatedTemplates = templatesList.Aggregate("", (current, s) => current + (s + delimiter));

			// Due to Razor load test, we can't send strings that are bigger than const_max_parse_length
			while (aggregatedTemplates.Length > 0) {
				var index = aggregatedTemplates.IndexOf(delimiter, Math.Min(const_max_parse_length, aggregatedTemplates.Length), StringComparison.Ordinal);
				// If not found than we need to take it all
				index = index < 0 ? aggregatedTemplates.Length : index;
				var chunkTemplates = aggregatedTemplates.Substring(0, index);
				// Parse it as a whole once for better performance.
				retVal += Parse(chunkTemplates, data, templateBaseName);
				aggregatedTemplates = aggregatedTemplates.Substring(index);
			}
			// Then return the split of the parsed template.
			return retVal.Split(new[] { delimiter }, StringSplitOptions.None).ToList();
		}
	}
}
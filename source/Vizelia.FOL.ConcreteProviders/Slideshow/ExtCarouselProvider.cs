﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Mootools implementation of SlideshowProvider
	/// </summary>
	class ExtCarouselProvider : SlideshowProvider {

		private const string const_slideshow_directory = "../../slideshow/extcarousel";

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);
			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}

		/// <summary>
		/// Get a string result which contains the Slideshow code for the list of pages provided.
		/// </summary>
		/// <param name="pages">The pages.</param>
		/// <param name="keyCulture">The key culture.</param>
		/// <returns></returns>
		public override string GetSlideshow(List<SlideshowPage> pages, string keyCulture) {


			var retVal = "<!DOCTYPE html PUBLIC \" -//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"> <html xmlns=\"http://www.w3.org/1999/xhtml\"> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">";

			retVal += GetHeaderStart();
			retVal += GetCss();
			retVal += GetHeaderEnd();
			retVal += GetScripts(pages, keyCulture);
			retVal += GetHeaderEnd();
			retVal += GetBody();

			retVal += "</html>";
			return retVal;
		}


		/// <summary>
		/// Gets the header start.
		/// </summary>
		/// <returns></returns>
		private string GetHeaderStart() {
			var retVal = "<head><title></title>" + Environment.NewLine;
			return retVal;
		}

		/// <summary>
		/// Gets the header end.
		/// </summary>
		/// <returns></returns>
		private string GetHeaderEnd() {
			var retVal = "</head>" + Environment.NewLine;
			return retVal;
		}

		/// <summary>
		/// Gets the CSS.
		/// </summary>
		/// <returns></returns>
		private string GetCss() {
			var retVal = string.Format("<link rel=\"stylesheet\" href=\"{0}/css/carousel.css\">", const_slideshow_directory) + Environment.NewLine;
			return retVal;

		}

		/// <summary>
		/// Gets the scripts.
		/// </summary>
		/// <param name="pages">The pages.</param>
		/// <param name="keyCulture">The key culture.</param>
		/// <returns></returns>
		private string GetScripts(List<SlideshowPage> pages, string keyCulture) {
			var retVal = string.Format("<script src=\"{0}/js/ext-core-debug.js\"></script>", const_slideshow_directory) + Environment.NewLine;
			retVal += string.Format("<script src=\"{0}/js/carousel.js\"></script>", const_slideshow_directory) + Environment.NewLine;
			retVal += "<style type=\"text/css\"> html, body { overflow: hidden; } </style>" + Environment.NewLine;

			retVal += "<script>";
			retVal += " var carousel;";
			retVal += "  Ext.onReady(function() {";
			retVal += "    carousel = new Ext.ux.Carousel('slideshow', { ";
			retVal += "        itemSelector: 'img',";
			retVal += "        interval: 1,";
			retVal += "        autoPlay: false,";
			retVal += "        showPlayButton: true,";
			retVal += "        pauseOnNavigate: true,";
			retVal += "        freezeOnHover: false,";
			retVal += "		  hideNavigation:false,";
			//retVal += "        transitionType: 'fade',";
			retVal += "        navigationOnClick: true  ";
			retVal += "    });";
			retVal += "    setTimeout(function() {carousel.play();}, 1000)";
			retVal += "})";
			retVal += "</script>";

			retVal += " <style>";
			retVal += "div.item {padding: 20px;}";
			retVal += "</style>";


			retVal += "<body style=\"overflow:hidden;margin:0px;padding:0px;background-color:rgb(0,0,0);\">" + Environment.NewLine;
			if (pages != null && pages.Count > 0) {
				retVal += "<div id='slideshow' style='padding: 5px; height:" + pages[0].Height + "px;width:" + pages[0].Width + "px; background-color: #E6E6E0'>";
				pages.ForEach(p => {
					var title = GetLocalizedTitle(keyCulture, p.Title);
					retVal += "<img style='width:" + p.Width + "px;height:" + p.Height + "px;' src='../../" + p.Url + "&applicationName=" + ContextHelper.ApplicationName + "' title='" + title.HtmlEncode() + "'  delay='" + (p.Duration * 1000).ToString() + "' /> " + Environment.NewLine;
				});
				retVal += "</div>";
			}
			retVal += "</body>" + Environment.NewLine;
			return retVal;
		}

		/// <summary>
		/// Gets the body.
		/// </summary>
		/// <returns></returns>
		private string GetBody() {
			return "";
		}
	}
}

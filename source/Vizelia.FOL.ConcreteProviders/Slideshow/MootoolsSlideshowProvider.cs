﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Mootools implementation of SlideshowProvider
	/// </summary>
	class MootoolsSlideshowProvider : SlideshowProvider {

		private const string const_slideshow_directory = "../../slideshow/mootools";

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);
			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}

		/// <summary>
		/// Get a string result which contains the Slideshow code for the list of pages provided.
		/// </summary>
		/// <param name="pages">The pages.</param>
		/// <param name="keyCulture">The key culture.</param>
		/// <returns></returns>
		public override string GetSlideshow(List<SlideshowPage> pages, string keyCulture) {


			var retVal = "<!DOCTYPE html PUBLIC \" -//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\"> <html xmlns=\"http://www.w3.org/1999/xhtml\"> ";

			retVal += GetHeaderStart();
			retVal += GetCss();
			retVal += GetHeaderEnd();
			retVal += GetScripts(pages, keyCulture);
			retVal += GetHeaderEnd();
			retVal += GetBody();

			retVal += "</html>";
			return retVal;
		}


		/// <summary>
		/// Gets the header start.
		/// </summary>
		/// <returns></returns>
		private string GetHeaderStart() {
			var retVal = "<head><title></title>" + Environment.NewLine;
			return retVal;
		}

		/// <summary>
		/// Gets the header end.
		/// </summary>
		/// <returns></returns>
		private string GetHeaderEnd() {
			var retVal = "</head>" + Environment.NewLine;
			return retVal;
		}

		/// <summary>
		/// Gets the CSS.
		/// </summary>
		/// <returns></returns>
		private string GetCss() {
			var retVal = string.Format("<link rel=\"stylesheet\" href=\"{0}/css/slideshow.css\">", const_slideshow_directory) + Environment.NewLine;
			return retVal;

		}

		/// <summary>
		/// Gets the scripts.
		/// </summary>
		/// <param name="pages">The pages.</param>
		/// <param name="keyCulture">The key culture.</param>
		/// <returns></returns>
		private string GetScripts(List<SlideshowPage> pages, string keyCulture) {
			var retVal = string.Format("<script src=\"{0}/js/mootools.js\"></script>", const_slideshow_directory) + Environment.NewLine;
			retVal += string.Format("<script src=\"{0}/js/slideshow.js\"></script>", const_slideshow_directory) + Environment.NewLine;
			retVal += string.Format("<script src=\"{0}/js/slideshow.push.js\"></script>", const_slideshow_directory) + Environment.NewLine;
			retVal += "<style type=\"text/css\">.slideshow {  height: 100%;  width: 100%;} html, body { overflow: hidden; } </style>" + Environment.NewLine;
			retVal += "<script> window.addEvent('domready', function(){ ";
			retVal += "var data= {";
			pages.ForEach(p => {
				var title = GetLocalizedTitle(keyCulture, p.Title);
				retVal += "'" + p.Url + "&applicationName=" + ContextHelper.ApplicationName + "' : { caption : '" + title.HtmlEncode() + "', delays:" + (p.Duration * 1000).ToString() + " }, " + Environment.NewLine;
			});
			retVal.Remove(retVal.LastIndexOf(','));
			retVal += "};";

			retVal += "var theWidth, theHeight;" + Environment.NewLine;
			retVal += "if (window.innerWidth) {theWidth=window.innerWidth;} else if (document.documentElement && document.documentElement.clientWidth) {theWidth=document.documentElement.clientWidth;}else if (document.body) {theWidth=document.body.clientWidth;} if (window.innerHeight) {theHeight=window.innerHeight;}else if (document.documentElement && document.documentElement.clientHeight) {theHeight=document.documentElement.clientHeight;}else if (document.body) {theHeight=document.body.clientHeight;};" + Environment.NewLine;
			retVal += "var myShow = new Slideshow.Push('show', data, {controller: false, height: theHeight, width: theWidth, hu: '../../', thumbnails: false, transition: 'back:in:out',delay:10000,resize :'length',center:false});" + Environment.NewLine;
			retVal += "setTimeout('window.location=window.location.href',1000*60*10);});" + Environment.NewLine;
			retVal += "</script>";
			return retVal;
		}

		/// <summary>
		/// Gets the body.
		/// </summary>
		/// <returns></returns>
		private string GetBody() {
			var retVal = "<body style=\"overflow:hidden;margin:0px;padding:0px;background-color:rgb(0,0,0);\">" + Environment.NewLine;
			retVal += " <div id=\"show\" class=\"slideshow\">" + Environment.NewLine;
			retVal += "</div>" + Environment.NewLine;
			retVal += "</body>" + Environment.NewLine;
			return retVal;
		}
	}
}

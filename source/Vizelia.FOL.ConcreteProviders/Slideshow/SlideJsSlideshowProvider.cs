﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Slidejs implementation of SlideshowProvider
	/// </summary>
	class SlideJsSlideshowProvider : SlideshowProvider {

		private const string const_slideshow_directory = "../../slideshow/slidejs";

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);
			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}

		/// <summary>
		/// Get a string result which contains the Slideshow code for the list of pages provided.
		/// </summary>
		/// <param name="pages">The pages.</param>
		/// <param name="keyCulture">The key culture.</param>
		/// <returns></returns>
		public override string GetSlideshow(List<SlideshowPage> pages, string keyCulture) {
			var retVal = "<html>";

			retVal += GetHeaderStart();
			retVal += GetCss();
			retVal += GetHeaderEnd();
			retVal += GetScripts();
			retVal += GetHeaderEnd();
			retVal += GetBody(pages, keyCulture);

			retVal += "</html>";
			return retVal;
		}


		/// <summary>
		/// Gets the header start.
		/// </summary>
		/// <returns></returns>
		private string GetHeaderStart() {
			var retVal = "<head><title></title>" + Environment.NewLine;
			return retVal;
		}

		/// <summary>
		/// Gets the header end.
		/// </summary>
		/// <returns></returns>
		private string GetHeaderEnd() {
			var retVal = "</head>" + Environment.NewLine;
			return retVal;
		}

		/// <summary>
		/// Gets the CSS.
		/// </summary>
		/// <returns></returns>
		private string GetCss() {
			var retVal = string.Format("<link rel=\"stylesheet\" href=\"{0}/css/global.css\">", const_slideshow_directory) + Environment.NewLine;
			return retVal;

		}

		/// <summary>
		/// Gets the scripts.
		/// </summary>
		/// <returns></returns>
		private string GetScripts() {
			var retVal = "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js\"></script>" + Environment.NewLine;
			retVal += string.Format("<script src=\"{0}/js/slides.jquery.js\"></script>", const_slideshow_directory) + Environment.NewLine;
			retVal += "<script>$(function(){$(\"#slides\").slides({responsive: true, pagination: false}); $(\"#slides\").slides(\"play\");});</script>" + Environment.NewLine;
			return retVal;
		}


		/// <summary>
		/// Gets the body.
		/// </summary>
		/// <param name="pages">The pages.</param>
		/// <param name="keyCulture">The key culture.</param>
		/// <returns></returns>
		private string GetBody(List<SlideshowPage> pages, string keyCulture) {
			var retVal = "<body><div id=\"container\"><div id=\"slides\">" + Environment.NewLine;
			pages.ForEach(p => {
				var title = GetLocalizedTitle(keyCulture, p.Title);
				retVal += string.Format("<img src=\"../../{1}\" width=\"{2}\" height=\"{3}\" alt=\"{0}\" /> ", title.HtmlEncode(), p.Url + "&applicationName=" + ContextHelper.ApplicationName, p.Width, p.Height) + Environment.NewLine;
			});
			retVal += string.Format("</div></div></body>", const_slideshow_directory) + Environment.NewLine;
			return retVal;
		}
	}
}

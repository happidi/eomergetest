using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A base mail provider implementation.
	/// </summary>
	public abstract class BaseMailProvider : MailProvider {
		private const string const_attribute_maximum_total_attachments_size = "maxTotalAttachmentSizeBytes";
		private const string const_attribute_maximum_single_attachment_size = "maxSingleAttachmentSizeBytes";
		private const string const_archive_file_name = "Attachments.zip";

		private static readonly string m_LogFilePrefix = Membership.ApplicationName + " Mail ";
		private static readonly object m_LockObject = new object();

		/// <summary>
		/// Gets or sets the maximum size of the total attachments in bytes.
		/// </summary>
		/// <value>
		/// The maximum size of the total attachments in bytes.
		/// </value>
		public int MaximumTotalAttachmentsSize { get; set; }

		/// <summary>
		/// Gets or sets the maximum size of the single attachment in bytes.
		/// </summary>
		/// <value>
		/// The maximum size of the single attachment in bytes.
		/// </value>
		public int MaximumSingleAttachmentSize { get; set; }

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The friendly name of the provider.</param>
		/// <param name="config">A collection of the name/value pairs representing the provider-specific attributes specified in the configuration for this provider.</param>
		/// <exception cref="T:System.ArgumentNullException">The name of the provider is null.</exception>
		/// <exception cref="T:System.ArgumentException">The name of the provider has a length of zero.</exception>
		/// <exception cref="T:System.InvalidOperationException">An attempt is made to call <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)"/> on a provider after the provider has already been initialized.</exception>
		public override void Initialize(string name, NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_maximum_total_attachments_size])) {
				throw new ProviderException(const_attribute_maximum_total_attachments_size + " is missing");
			}

			MaximumTotalAttachmentsSize = int.Parse(config[const_attribute_maximum_total_attachments_size]);
			config.Remove(const_attribute_maximum_total_attachments_size);


			if (String.IsNullOrEmpty(config[const_attribute_maximum_single_attachment_size])) {
				throw new ProviderException(const_attribute_maximum_single_attachment_size + " is missing");
			}

			MaximumSingleAttachmentSize = int.Parse(config[const_attribute_maximum_single_attachment_size]);
			config.Remove(const_attribute_maximum_single_attachment_size);

		}

		/// <summary>
		/// Sends a mail message.
		/// </summary>
		/// <param name="message">The mail message</param>
		public sealed override void Send(MailMessage message) {
			CompressBigAttachments(message);
			SendInternal(message);
		}

		/// <summary>
		/// Compresses big attachments into ZIP files.
		/// </summary>
		/// <param name="message">The message.</param>
		private void CompressBigAttachments(MailMessage message) {
			if(!message.Attachments.Any()) return;

			bool isTooBig =
				message.Attachments.Any(a => a.ContentStream.Length > MaximumSingleAttachmentSize) ||
				message.Attachments.Sum(a => a.ContentStream.Length) > MaximumTotalAttachmentsSize;

			SetUniqueNames(message);

			if(isTooBig) {
				// For a single file, call the zip with the same name but with "zip" extension.
				// For multiple files, call the zip "Attachments.zip".
				string archiveFileName = message.Attachments.Count == 1
											? Path.ChangeExtension(message.Attachments[0].Name, "zip")
											: const_archive_file_name;

				var filesWithNames = message.Attachments.Select(a => new Tuple<string, Stream>(a.Name, a.ContentStream));
				Stream compressedFile = CompressionHelper.Compress(filesWithNames, archiveFileName);
				message.Attachments.Clear();
				message.Attachments.Add(new Attachment(compressedFile, archiveFileName, MimeType.Zip));
			}
		}

		/// <summary>
		/// Set unique names for strings. Example:
		/// file.txt 
		/// file (1).txt 
		/// file (2).txt
		/// </summary>
		/// <param name="message"></param>
		private static void SetUniqueNames(MailMessage message) {
			var names = new Dictionary<string, int>();
			foreach (var attachment in message.Attachments) {
				if (names.ContainsKey(attachment.Name)) {
					var originalName = attachment.Name;
					var fileNumber = names[attachment.Name];
					var extension = Path.GetExtension(attachment.Name);
					var newName = attachment.Name.Substring(0, attachment.Name.LastIndexOf(extension, StringComparison.InvariantCulture));
					newName += " (" + fileNumber + ")" + extension;
					attachment.Name = newName;

					// add count to basic name
					names[originalName] = names[originalName] + 1;

					// add new name to list
					names.Add(attachment.Name, 1);
				}
				else {
					names.Add(attachment.Name, 1);
				}
			}
		}

		/// <summary>
		/// A concrete implementation of sending the message.
		/// </summary>
		/// <param name="message">The message.</param>
		protected abstract void SendInternal(MailMessage message);

		/// <summary>
		/// Logs the message success.
		/// </summary>
		/// <param name="messageSummary">The message summary.</param>
		protected static void LogMessageSuccess(string messageSummary) {
			string logRow = "SUCCESS\t" + messageSummary;
			WriteToMailLog(logRow);
		}

		/// <summary>
		/// Logs the message failure.
		/// </summary>
		/// <param name="messageSummary">The message summary.</param>
		/// <param name="exception">The exception.</param>
		protected static void LogMessageFailure(string messageSummary, Exception exception) {
			string logRow = "FAILURE\t" + messageSummary + "\tError details:" + exception;
			WriteToMailLog(logRow);
		}

		private static void WriteToMailLog(string logRow) {
			// Don't log if not requested to.
			if (string.IsNullOrWhiteSpace(VizeliaConfiguration.Instance.Mail.LogPath)) return;

			try {
				lock (m_LockObject) {
					Directory.CreateDirectory(VizeliaConfiguration.Instance.Mail.LogPath);

					// We use daily log files.
					string filePath = Path.Combine(VizeliaConfiguration.Instance.Mail.LogPath,
												   m_LogFilePrefix + DateTime.Today.ToString("yyyy'-'MM'-'dd") + ".log");

					using (var fileStream = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)) {
						using (var writer = new StreamWriter(fileStream, Encoding.UTF8)) {
							writer.WriteLine(DateTime.Now.ToString("u") + "\t" + logRow);
							writer.Flush();
						}
					}

					PurgeOldLogs();
				}
			}
			catch (Exception e) {
				try {
					TracingService.Write(e);
				}
				catch (Exception) {
					// Do nothing. We don't want to crash a background thread if this is running in it.
				}
				
			}
		}

		private static void PurgeOldLogs() {
			var logDirectory = new DirectoryInfo(VizeliaConfiguration.Instance.Mail.LogPath);
			int daysToKeepOldFiles = -1 * VizeliaConfiguration.Instance.Mail.DaysToKeepLogFiles;

			IEnumerable<FileInfo> oldFiles =
				logDirectory.GetFiles(m_LogFilePrefix + "*.log").Where(f => f.LastWriteTime < DateTime.Now.AddDays(daysToKeepOldFiles));

			foreach (var file in oldFiles) {
				file.Delete();
			}
		}

	}
}

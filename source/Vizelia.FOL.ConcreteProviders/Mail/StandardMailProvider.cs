﻿using System;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Extensions;
using System.Configuration.Provider;
using System.Net.Mail;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a standard mail provider using the SmtpClient.
	/// </summary>
	public class StandardMailProvider : BaseMailProvider {
		private const string const_attribute_async = "async";

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_async])) {
				throw new ProviderException(const_attribute_async + " is missing");
			}
			Async = Convert.ToBoolean(config[const_attribute_async]);
			config.Remove(const_attribute_async);

			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}

		/// <summary>
		/// True if the mail should be sent asynchronously, false otherwise.
		/// Default is false.
		/// </summary>
		public bool Async { get; set; }

		/// <summary>
		/// A concrete implementation of sending the message.
		/// </summary>
		/// <param name="message">The message.</param>
		protected override void SendInternal(MailMessage message) {
			var client = new SmtpClient();
			string messageSummary = message.CreateMessageSummary();

			if (Async) {
				try {
					client.SendCompleted += OnSendCompleted;
					client.SendAsync(message, new Tuple<string, string>(ContextHelper.ApplicationName, messageSummary));
				}
				catch (Exception ex) {
					LogMessageFailure(messageSummary, ex);
				}
			}
			else {
				try {
					client.Send(message);
					LogMessageSuccess(messageSummary);
				}
				catch (Exception ex) {
					LogMessageFailure(messageSummary, ex);
				}
			}
		}

		private static void OnSendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e) {
			var state = (Tuple<string, string>)e.UserState;
			string messageSummary = state.Item2;

			// Restore tenant context since this is a seperate thread invoked by SmtpClient.
			TenantHelper.RunInDifferentTenantContext(state.Item1, () => {
				if (e.Error == null) {
					LogMessageSuccess(messageSummary);
				}
				else {
					LogMessageFailure(messageSummary, e.Error);
				}
			});
		}

	}
}

﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Reflection;
using System.Threading.Tasks;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Vizelia.FOL.Common.Extensions;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of the Amazon SES mail service.
	/// </summary>
	public class AmazonSESMailProvider : BaseMailProvider {
		private const string const_attribute_awsAccessKey = "awsAccessKey";
		private const string const_attribute_awsSecretKey = "awsSecretKey";
		private const string const_attribute_async = "async";

		/// <summary>
		/// Gets or sets the aws access key.
		/// </summary>
		/// <value>
		/// The aws access key.
		/// </value>
		public string AwsAccessKey { get; set; }

		/// <summary>
		/// Gets or sets the aws secret key.
		/// </summary>
		/// <value>
		/// The aws secret key.
		/// </value>
		public string AwsSecretKey { get; set; }

		/// <summary>
		/// True if the mail should be sent asynchronously, false otherwise.
		/// Default is false.
		/// </summary>
		public bool Async { get; set; }

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_async])) {
				throw new ProviderException(const_attribute_async + " is missing");
			}
			Async = Convert.ToBoolean(config[const_attribute_async]);
			config.Remove(const_attribute_async);


			if (String.IsNullOrEmpty(config[const_attribute_awsAccessKey])) {
				throw new ProviderException(const_attribute_awsAccessKey + " is missing");
			}
			AwsAccessKey = config[const_attribute_awsAccessKey];
			config.Remove(const_attribute_awsAccessKey);

			if (String.IsNullOrEmpty(config[const_attribute_awsSecretKey])) {
				throw new ProviderException(const_attribute_awsSecretKey + " is missing");
			}
			AwsSecretKey = config[const_attribute_awsSecretKey];
			config.Remove(const_attribute_awsSecretKey);

			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));
		}

		/// <summary>
		/// A concrete implementation of sending the message.
		/// </summary>
		/// <param name="message">The message.</param>
		protected override void SendInternal(MailMessage message) {
			if (Async) {
				Task.Factory.StartNew(() => SendMessage(message));
			}
			else {
				SendMessage(message);
			}
		}

		private void SendMessage(MailMessage message) {
			string messageSummary = message.CreateMessageSummary();

			try {
				var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
				string fromEmail = string.IsNullOrEmpty(message.From.Address) ? smtpSection.From : message.From.Address;
				var rawMessage = new RawMessage();

				using (MemoryStream memoryStream = ConvertMailMessageToMemoryStream(message)) {
					rawMessage.WithData(memoryStream);
				}

				var request = new SendRawEmailRequest();
				request.WithRawMessage(rawMessage);
				//request.WithDestinations(message.To.Select(p => String.Format("\"{0}\" <{1}>", p.DisplayName, p.Address)).ToList());
				request.WithSource(fromEmail);

				var client = new AmazonSimpleEmailServiceClient(AwsAccessKey, AwsSecretKey);

				client.SendRawEmail(request);
				LogMessageSuccess(messageSummary);
			}
			catch (Exception ex) {
				LogMessageFailure(messageSummary, ex);
			}
		}


		/// <summary>
		/// Sends - OLD VERSION DO NOT USE. 
		/// This method does not support attachment or priorities.
		/// </summary>
		/// <param name="message">The message.</param>
		public void SendOLD(MailMessage message) {
			// We read the smtp section of web.config
			var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
			// We will use the default "From" address from the web.config file if no "From" specified on object.
			string fromEmail = string.IsNullOrEmpty(message.From.Address) ? smtpSection.From : message.From.Address;
			Body body = message.IsBodyHtml ? new Body().WithHtml(new Content(message.Body)) : new Body().WithText(new Content(message.Body));

			SendEmailRequest req = new SendEmailRequest()
				.WithDestination(new Destination {
					ToAddresses = message.To.Select(p => String.Format("\"{0}\" <{1}>", p.DisplayName, p.Address)).ToList(),
					BccAddresses = message.Bcc.Select(p => String.Format("\"{0}\" <{1}>", p.DisplayName, p.Address)).ToList(),
					CcAddresses = message.CC.Select(p => String.Format("\"{0}\" <{1}>", p.DisplayName, p.Address)).ToList()
				})

				.WithSource(fromEmail)
				.WithReturnPath(fromEmail)
				.WithMessage(new Message(new Content(message.Subject), body));

			var client = new AmazonSimpleEmailServiceClient(AwsAccessKey, AwsSecretKey);
			client.SendEmail(req);
		}

		/// <summary>
		/// Converts the mail message to memory stream.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <returns></returns>
		private static MemoryStream ConvertMailMessageToMemoryStream(MailMessage message) {
			Assembly assembly = typeof(SmtpClient).Assembly;
			Type mailWriterType = assembly.GetType("System.Net.Mail.MailWriter");
			MemoryStream fileStream = new MemoryStream();
			ConstructorInfo mailWriterContructor = mailWriterType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { typeof(Stream) }, null);
			object mailWriter = mailWriterContructor.Invoke(new object[] { fileStream });
			MethodInfo sendMethod = typeof(MailMessage).GetMethod("Send", BindingFlags.Instance | BindingFlags.NonPublic);
			sendMethod.Invoke(message, BindingFlags.Instance | BindingFlags.NonPublic, null, new[] { mailWriter, true }, null);
			MethodInfo closeMethod = mailWriter.GetType().GetMethod("Close", BindingFlags.Instance | BindingFlags.NonPublic);
			closeMethod.Invoke(mailWriter, BindingFlags.Instance | BindingFlags.NonPublic, null, new object[] { }, null);
			return fileStream;
		}



	}
}
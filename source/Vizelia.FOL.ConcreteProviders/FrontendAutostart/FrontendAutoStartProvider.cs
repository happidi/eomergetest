﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Threading;
using System.Web.Hosting;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;
using Task = System.Threading.Tasks.Task;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// 
	/// </summary>
	public class FrontendAutoStartProvider : IProcessHostPreloadClient {
		/// <summary>
		/// Provides initialization data that is required in order to preload the application.
		/// </summary>
		/// <param name="parameters">Data to initialize the application.</param>
		public void Preload(string[] parameters) {
			try {
				TracingService.Write(TraceEntrySeverity.Information, "FrontendAutoStartProvider starting...",
									 "FrontendAutoStartProvider", string.Empty);

				var energyAggregatorInitializationTask = new Task(Helper.StartFrontend);
				Thread.Sleep(10000);
				energyAggregatorInitializationTask.Start();


				TracingService.Write(TraceEntrySeverity.Information, "FrontendAutoStartProvider done",
									 "FrontendAutoStartProvider", string.Empty);


			}
			catch (Exception exception) {
				TracingService.Write(TraceEntrySeverity.Error, "FrontendAutoStartProvider error: " + exception, "FrontendAutoStartProvider", string.Empty);
				throw;
			}
		}

	}
}

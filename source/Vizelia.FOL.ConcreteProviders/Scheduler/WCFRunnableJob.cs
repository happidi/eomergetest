﻿using System;
using System.Collections.Concurrent;
using System.Reflection;
using System.ServiceModel;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Common;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;
using Vizelia.FOL.ServiceAgents;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.ConcreteProviders {

	/// <summary>
	/// WCFRunnableJob
	/// </summary>
	public class WCFRunnableJob : IRunnableJob {
		private const string const_protocol_detection_category = "SchedulerProtocolDetection";
		private static readonly ConcurrentDictionary<string, string> SitesProtocols = new ConcurrentDictionary<string, string>();

		/// <summary>
		/// Runs the job with the specified context.
		/// </summary>
		/// <param name="context">The context.</param>
		public void Run(JobContext context) {
			//Trust all certificates
			System.Net.ServicePointManager.ServerCertificateValidationCallback =
			    ((sender, certificate, chain, sslPolicyErrors) => true);


			TenantHelper.RunInDifferentTenantContext(context.ApplicationName,
				() => TracingService.Trace("Job invocation", context.MethodQualifiedName, "Job invocation",
					() => RunJob(context)));
		}

		/// <summary>
		/// Runs the job.
		/// </summary>
		/// <param name="context">The context.</param>
		private void RunJob(JobContext context) {
			string username = context.ApplicationName + "\\" + VizeliaConfiguration.Instance.Authentication.SchedulerUsername;
			string password = VizeliaConfiguration.Instance.Authentication.SchedulerPassword;
			string tenantUrl = GetTenantUrl(context.ApplicationName);

			MethodInfo mi = typeof(IJobWCF).GetMethod(context.MethodName);
			var action = new Action<IJobWCF>(j => mi.Invoke(j, new object[] { context }));
			tenantUrl = tenantUrl.TrimEnd('/');

			bool isAdded = false;
			string protocol = SitesProtocols.GetOrAdd(tenantUrl, s => {
			                                                     	isAdded = true;
			                                                     	return "http";
			                                                     });

			bool shouldWriteSuccess = isAdded; // Depends on if this is the first run or there has been a change in protocols.

			Exception exception = CrossTenantServiceCall.Call(tenantUrl, username, password, action, protocol, false);

			if (exception == null) {
				SitesProtocols[tenantUrl] = protocol;
			}
			else {
				shouldWriteSuccess = false;

				if (exception is EndpointNotFoundException || exception is CommunicationException) {
					string fallbackProtocol = SwitchToFallbackProtocol(protocol, tenantUrl);

					// Recall the service using fallback protocol.
					Exception fallbackException = CrossTenantServiceCall.Call(tenantUrl, username, password, action, fallbackProtocol, false);

					if (fallbackException == null) {
						shouldWriteSuccess = true;
					}
					else {
						TracingService.Write(TraceEntrySeverity.Error,
							string.Format(
								"Scheduler could not invoke job using neither HTTP or HTTPS protocols. Please make sure front-end website is running and configured correctly.\r\n\r\n{0} Error: {1}.\r\n\r\n{2} Error: {3}",
								protocol.ToUpper(), exception, fallbackProtocol.ToUpper(), fallbackException), const_protocol_detection_category, string.Empty);
					}
				}
				else {
					// An exception from the server-side, not related to WCF or network. Application logic. Write it.
					TracingService.Write(exception);
				}
			}

			if (shouldWriteSuccess) {
				TracingService.Write(TraceEntrySeverity.Information, string.Format("Scheduler has detected tenant {0} should be invoked using {1} protocol.", context.ApplicationName, SitesProtocols[tenantUrl].ToUpper()), const_protocol_detection_category, string.Empty);
			}
		}

		private static string SwitchToFallbackProtocol(string protocol, string tenantUrl) {
			var fallbackProtocol = protocol.ToLower().Equals("http") ? "https" : "http";
			SitesProtocols[tenantUrl] = fallbackProtocol;
			//var oldUrl = ContextHelper.Url;
			//ContextHelper.Url = protocol + "://" + tenantUrl;
			//TracingService.Write(TraceEntrySeverity.Information, string.Format("Fallback to {0} protocol", fallbackProtocol.ToUpper()),
			//                     "Job invocation", string.Empty);
			//ContextHelper.Url = oldUrl;

			return fallbackProtocol;
		}


		/// <summary>
		/// Gets the tenant URL.
		/// </summary>
		/// <param name="tenantName">Name of the tenant.</param>
		/// <returns></returns>
		private string GetTenantUrl(string tenantName) {
			var tenantBroker = new TenantBroker();
			var tenant = tenantBroker.GetItem(tenantName);

			if (tenant == null) {
				throw new VizeliaException(string.Format("Unable to get tenant URL for tenant \"{0}\": Tenant not found.", tenantName));
			}

			if (string.IsNullOrEmpty(tenant.Url)) {
				throw new VizeliaException(string.Format("Unable to get tenant URL for tenant \"{0}\": Found empty URL.", tenantName));
			}

			return tenant.Url;
		}

	}
}

﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Web.Hosting;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;
using Task = System.Threading.Tasks.Task;

namespace Vizelia.FOL.ConcreteProviders  {
	/// <summary>
	/// 
	/// </summary>
	public class QuartzSchedulerAutoStartProvider : IProcessHostPreloadClient {

		/// <summary>
		/// Provides initialization data that is required in order to preload the application.
		/// </summary>
		/// <param name="parameters">Data to initialize the application.</param>
		public void Preload(string[] parameters) {
			try {
				TracingService.Write(TraceEntrySeverity.Information, "QuartzSchedulerAutoStartProvider starting...", "QuartzSchedulerAutoStartProvider", string.Empty);

				var task = new Task(() => {
				                    	try {
				                    		SchedulerService.Start();
				                    	}
				                    	catch (Exception exception) {
											// We don't want the background thread to crash.
				                    		TracingService.Write(TraceEntrySeverity.Error, "QuartzSchedulerAutoStartProvider error: " + exception, "QuartzSchedulerAutoStartProvider", string.Empty);
				                    	}
				                    });

				Thread.Sleep(10000);
				task.Start();

				TracingService.Write(TraceEntrySeverity.Information, "QuartzSchedulerAutoStartProvider done", "QuartzSchedulerAutoStartProvider", string.Empty);
			}
			catch (Exception exception) {
				TracingService.Write(TraceEntrySeverity.Error, "QuartzSchedulerAutoStartProvider error: " + exception, "QuartzSchedulerAutoStartProvider", string.Empty);
				throw;
			}
		}
	}
}

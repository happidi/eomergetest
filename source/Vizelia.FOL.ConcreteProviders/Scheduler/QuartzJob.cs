﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A quartz job.
	/// </summary>
	public class QuartzJob : IJob {

		/// <summary>
		/// The key for storing the list of schedules.
		/// </summary>
		public const string const_vizelia_schedule_list = "vizelia_schedule_list";

		/// <summary>
		/// The key for storing the list of steps.
		/// </summary>
		public const string const_vizelia_step_list = "vizelia_step_list";

		/// <summary>
		/// Called by the <see cref="T:Quartz.IScheduler"/> when a <see cref="T:Quartz.ITrigger"/>
		/// fires that is associated with the <see cref="T:Quartz.IJob"/>.
		/// </summary>
		/// <param name="context">The execution context.</param>
		public void Execute(IJobExecutionContext context) {
			string applicationName = QuartzSchedulerProvider.DecodeJobApplicationName(context.JobDetail.Key.Group);
			if (context.MergedJobDataMap.ContainsKey(QuartzSchedulerProvider.const_is_paused)) {
				return;
			}

			List<Step> steps = GetSteps(context);
			ContextHelper.JobRunInstanceRequestId = Guid.NewGuid().ToString();
			ContextHelper.KeyJob = context.JobDetail.Key.ToString();

			if (steps != null) {
				foreach (var step in steps) {
					Type jobType = Type.GetType(step.ExecutorType);

					if (jobType != null) {
						var runnableJob = Activator.CreateInstance(jobType) as IRunnableJob;

						if (runnableJob != null)
							runnableJob.Run(new JobContext {
								ApplicationName = applicationName,
								CallerKey = step.KeyStep,
								CallerName = step.Name,
								MethodName = step.MethodName,
								MethodQualifiedName = step.MethodQualifiedName,
								Keys = step.ModelDataKeys,
								JobKey = context.JobDetail.Key.ToString(),
								JobRunInstanceRequestId = ContextHelper.JobRunInstanceRequestId
							});
					}
				}
			}
		}

		private static List<Step> GetSteps(IJobExecutionContext context) {
			List<Step> steps = null;

			// Deserialize steps
			try {
				steps = ((List<string>)context.MergedJobDataMap[const_vizelia_step_list]).Select(stp => Helper.DeserializeJson<Step>(stp)).ToList();
			}
			catch (Exception exceptionJson) {
				TracingService.Write(TraceEntrySeverity.Warning, "Cannot deserialize steps, trying older implementation. Open and re-save this job to fix this. Error detais: " + exceptionJson);

				try {
					steps = (List<Step>)context.MergedJobDataMap[const_vizelia_step_list];
				}
				catch (Exception exceptionOld) {
					TracingService.Write(TraceEntrySeverity.Warning, "Cannot deserialize steps even with older implementation. Error detais: " + exceptionOld);
				}
			}
			return steps;
		}
	}
}

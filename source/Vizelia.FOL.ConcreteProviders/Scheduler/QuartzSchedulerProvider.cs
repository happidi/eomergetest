﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using Quartz.Impl.Triggers;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Providers;
using System.Configuration.Provider;
using System.Web;
using System.Net.Mail;
using Vizelia.FOL.Common;
using System.Reflection;
using System.Windows.Markup;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Threading;
using System.Windows.Shapes;
using System.Windows.Controls;
using Microsoft.Expression.Controls;
using System.Collections.Specialized;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Matchers;
using System.Web.Security;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a standard job provider using quartz custom implementation.
	/// </summary>
	public class QuartzSchedulerProvider : SchedulerProvider {
		private IScheduler _scheduler;
		/// <summary>
		/// This key will be in the job data map if the job is paused.
		/// </summary>
		public static string const_is_paused = "IsPaused";

		private const string const_attribute_job_store_type = "jobStore_type";
		private const string const_attribute_job_store_driver_delegate_type = "jobStore_driverDelegateType";
		private const string const_attribute_job_store_table_prefix = "jobStore_tablePrefix";
		private const string const_attribute_job_store_data_source = "jobStore_dataSource";
		private const string const_attribute_database_instancename = "databaseInstanceName";
		private const string const_attribute_data_source_provider = "dataSource_provider";
		private const string const_attribute_job_store_use_properties = "jobStore_useProperties";
		private const string const_attribute_thread_pool_thread_priority = "threadPool_threadPriority";

		/// <summary>
		/// The prefix used to store jobs in Quartz database in order to allow system jobs for example not visible to users.
		/// </summary>
		private const string const_user_job_prefix = "viz_user_job";


		/// <summary>
		/// Encodes the job key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		private string EncodeJobKey(string key) {
			return const_user_job_prefix + "_" + ContextHelper.ApplicationName + "_" + key;
		}

		/// <summary>
		/// Decodes the job key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		private string DecodeJobKey(string key) {
			var result = key.Split(new string[] { EncodeJobKey("") }, StringSplitOptions.None);
			return result.Length == 1 ? result[1] : null;
		}

		/// <summary>
		/// Decodes the name of the job application from a job key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public static string DecodeJobApplicationName(string key) {
			string retVal = key;
			retVal = retVal.Substring((const_user_job_prefix + "_").Length);
			retVal = retVal.Substring(0, retVal.Length - 36 - 1);
			return retVal;
		}

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_job_store_type])) {
				var type = Type.GetType("Quartz.Impl.AdoJobStore.JobStoreTX, Quartz");
				if (type != null) JobStoreType = type.FullName;
			}
			else
				JobStoreType = config[const_attribute_job_store_type];

			config.Remove(const_attribute_job_store_type);

			if (String.IsNullOrEmpty(config[const_attribute_job_store_driver_delegate_type])) {
				JobStoreDriverDelegateType = "Quartz.Impl.AdoJobStore.SqlServerDelegate, Quartz";
			}
			else
				JobStoreDriverDelegateType = config[const_attribute_job_store_driver_delegate_type];
			config.Remove(const_attribute_job_store_driver_delegate_type);

			if (String.IsNullOrEmpty(config[const_attribute_job_store_table_prefix])) {
				JobStoreTablePrefix = "QRTZ_";
			}
			else
				JobStoreTablePrefix = config[const_attribute_job_store_table_prefix];
			config.Remove(const_attribute_job_store_table_prefix);

			if (String.IsNullOrEmpty(config[const_attribute_job_store_data_source])) {
				JobStoreDataSource = "myDS";
			}
			else
				JobStoreDataSource = config[const_attribute_job_store_data_source];
			config.Remove(const_attribute_job_store_data_source);

			if (String.IsNullOrEmpty(config[const_attribute_database_instancename])) {
				throw new ProviderException(const_attribute_database_instancename + " is missing");
			}
			DataSourceConnection = ConfigurationManager.ConnectionStrings[config[const_attribute_database_instancename]].ConnectionString;
			config.Remove(const_attribute_database_instancename);


			if (String.IsNullOrEmpty(config[const_attribute_data_source_provider])) {
				DataSourceProvider = "SqlServer-20";
			}
			else
				DataSourceProvider = config[const_attribute_data_source_provider];
			config.Remove(const_attribute_data_source_provider);


			if (String.IsNullOrEmpty(config[const_attribute_job_store_use_properties])) {
				JobStoreUseProperties = false;
			}
			else
				JobStoreUseProperties = Convert.ToBoolean(config[const_attribute_job_store_use_properties]);
			config.Remove(const_attribute_job_store_use_properties);

			if (String.IsNullOrEmpty(config[const_attribute_thread_pool_thread_priority])) {
				ThreadPoolPriority = "Normal";
			}
			else
				ThreadPoolPriority = config[const_attribute_thread_pool_thread_priority];
			config.Remove(const_attribute_thread_pool_thread_priority);


			if (config.Count > 0)
				throw new ProviderException(String.Format("attribute {0} is unauthorized", config.GetKey(0)));


			InitQuartz();

		}

		/// <summary>
		/// Inits the quartz.
		/// </summary>
		private void InitQuartz() {
			NameValueCollection properties = new NameValueCollection();
			properties["quartz.jobStore.type"] = JobStoreType;
			properties["quartz.jobStore.driverDelegateType"] = JobStoreDriverDelegateType;
			properties["quartz.jobStore.tablePrefix"] = JobStoreTablePrefix;
			properties["quartz.jobStore.dataSource"] = JobStoreDataSource;
			properties["quartz.dataSource.myDS.connectionString"] = DataSourceConnection;
			properties["quartz.dataSource.myDS.provider"] = DataSourceProvider;
			properties["quartz.jobStore.useProperties"] = JobStoreUseProperties ? "true" : "false";
			//properties["quartz.scheduler.instanceName"] = ContextHelper.ApplicationName;
			//properties["quartz.threadPool.threadPriority"] = ThreadPoolPriority;

			var schedFact = new StdSchedulerFactory(properties);
			_scheduler = schedFact.GetScheduler();
		}

	

		/// <summary>
		/// Starts this instance.
		/// </summary>
		public override void Start() {
			_scheduler.Start();
		}

		
		/// <summary>
		/// Gets or sets the thread pool priority.
		/// </summary>
		/// <value>
		/// The thread pool priority.
		/// </value>
		public string ThreadPoolPriority { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [job store user properties].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [job store user properties]; otherwise, <c>false</c>.
		/// </value>
		public bool JobStoreUseProperties { get; set; }

		/// <summary>
		/// Gets or sets the data source provider.
		/// </summary>
		/// <value>
		/// The data source provider.
		/// </value>
		public string DataSourceProvider { get; set; }

		/// <summary>
		/// Gets or sets the data source connection.
		/// </summary>
		/// <value>
		/// The data source connection.
		/// </value>
		public string DataSourceConnection { get; set; }

		/// <summary>
		/// Gets or sets the job store data source.
		/// </summary>
		/// <value>
		/// The job store data source.
		/// </value>
		public string JobStoreDataSource { get; set; }

		/// <summary>
		/// Gets or sets the job store table prefix.
		/// </summary>
		/// <value>
		/// The job store table prefix.
		/// </value>
		public string JobStoreTablePrefix { get; set; }

		/// <summary>
		/// Gets or sets the type of the job store driver delegate.
		/// </summary>
		/// <value>
		/// The type of the job store driver delegate.
		/// </value>
		public string JobStoreDriverDelegateType { get; set; }

		/// <summary>
		/// Gets or sets the type of the job store.
		/// </summary>
		/// <value>
		/// The type of the job store.
		/// </value>
		public string JobStoreType { get; set; }

		/// <summary>
		/// Creates a new business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to create.</param>
		/// <returns></returns>
		public override Job CreateJob(Job item) {
			var guid = Guid.NewGuid().ToString();
			item.KeyJob = EncodeJobKey(guid);

			foreach (var schedule in item.Schedules) {
				schedule.KeySchedule = Guid.NewGuid().ToString();

				if (schedule.StartDate.HasValue && schedule.StartDate.Value.Kind != DateTimeKind.Utc) {
					CalendarHelper.UpdateEventClientDates(schedule);
				}
			}

			foreach (var step in item.Steps) {
				step.KeyStep = string.IsNullOrWhiteSpace(step.KeyStep) ? Guid.NewGuid().ToString() : step.KeyStep;
			}

			IDictionary<string, object> jobParameters = new Dictionary<string, object>();
			jobParameters.Add(QuartzJob.const_vizelia_schedule_list, item.Schedules.Select(s=>Helper.SerializeJson(s)).ToList());
			jobParameters.Add(QuartzJob.const_vizelia_step_list, item.Steps.Select(s => Helper.SerializeJson(s)).ToList());


			var jobDetail = JobBuilder.Create<QuartzJob>()
				.WithDescription(item.Description)
				.WithIdentity(item.Name, item.KeyJob)
				.UsingJobData(new JobDataMap(jobParameters))
				.StoreDurably(true)
				.Build();

			IDictionary<IJobDetail, IList<ITrigger>> jobs = new Dictionary<IJobDetail, IList<ITrigger>>();

			jobs.Add(jobDetail, item.Schedules.Select(CreateTriggerFromSchedule).ToList());

			_scheduler.ScheduleJobs(jobs, true);

			return item;
		}

		/// <summary>
		/// Creates the trigger from schedule.
		/// </summary>
		/// <param name="schedule">The schedule.</param>
		/// <returns></returns>
		private ITrigger CreateTriggerFromSchedule(Schedule schedule) {
			ITrigger trigger = null;
			if (schedule.RecurrencePattern == null) {
				var offset = new DateTimeOffset(schedule.StartDate.Value);
				trigger = TriggerBuilder.Create().StartAt(offset).Build();
			}
			else {
				DateTime startDateUTC;
				if (schedule.StartDate != null)
					startDateUTC = schedule.StartDate.Value;
				else
					startDateUTC = DateTime.UtcNow;

				DateTime? endDateUTC = schedule.RecurrencePattern.Until;
				if(endDateUTC != null)
					endDateUTC = CalendarHelper.ConvertClientDateToUTC(endDateUTC.Value, schedule.TimeZoneId);

				if ((schedule.RecurrencePattern.Frequency == CalendarFrequencyType.Monthly) ||
					(schedule.RecurrencePattern.Frequency == CalendarFrequencyType.Yearly) ||
					(schedule.RecurrencePattern.Frequency == CalendarFrequencyType.Weekly)) {
						trigger = TriggerBuilder.Create().StartAt(startDateUTC).EndAt(endDateUTC).WithCronSchedule(schedule.ToCron()).Build();
				}
				int interval = schedule.RecurrencePattern.Interval;
				if (schedule.RecurrencePattern.Frequency == CalendarFrequencyType.Hourly) {
					trigger = TriggerBuilder.Create().StartAt(startDateUTC).EndAt(endDateUTC).WithSchedule(CalendarIntervalScheduleBuilder.Create()
																	.WithIntervalInHours(interval)).Build();
				}
				if (schedule.RecurrencePattern.Frequency == CalendarFrequencyType.Minutely) {
					trigger = TriggerBuilder.Create().StartAt(startDateUTC).EndAt(endDateUTC).WithSchedule(CalendarIntervalScheduleBuilder.Create()
																	.WithIntervalInMinutes(interval)).Build();
				}
			}
			return trigger;
		}

		/// <summary>
		/// Deletes an existing business entity Job.
		/// </summary>
		/// <param name="item">The item to delete.</param>
		/// <returns></returns>
		public override bool DeleteJob(Job item) {
			var jobsToDelete = _scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(item.KeyJob));
			DeleteStepsLogAndHistory(jobsToDelete);
			_scheduler.DeleteJobs(jobsToDelete.ToList());
			return true;
		}

		private void DeleteStepsLogAndHistory(IEnumerable<JobKey> jobsToDelete, List<string> exceptKeySteps = null) {
			// Currently disabled. These trace and log items will by purged using the Purge job.
			return;
			/*
			if(exceptKeySteps == null) {
				exceptKeySteps = new List<string>();
			}

			var broker = Helper.CreateInstance<LoggingBroker>();

			foreach (var jobKey in jobsToDelete) {
				var jobDetail = _scheduler.GetJobDetail(jobKey);
				var job = ConvertToBusinessEntity(jobDetail);

				
				var deletedSteps = job.Steps.Select(s => s.KeyStep).Except(exceptKeySteps);

				foreach (var step in deletedSteps) {
					broker.DeleteByCallerKey(step);
				}
			}*/
		}


		/// <summary>
		/// Deletes an existing business entity Job.
		/// </summary>
		/// <param name="newJob">The item to delete.</param>
		/// <returns></returns>
		private void DeleteJobForUpdate(Job newJob) {
			var existingJobsToDelete = _scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(newJob.KeyJob));

			// Get the keys of the new job's steps and delete the existing job's steps that were removed.
			var newJobKeySteps = newJob.Steps.Select(s => s.KeyStep).ToList();
			DeleteStepsLogAndHistory(existingJobsToDelete, newJobKeySteps);

			_scheduler.DeleteJobs(existingJobsToDelete.ToList());
		}


		/// <summary>
		/// Deletes all jobs.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		public override void DeleteAllJobs(string applicationName) {
			GetJobs(applicationName).ForEach(job => DeleteJob(job));
		}

		/// <summary>
		/// Gets a specific item for the business entity Job.
		/// </summary>
		/// <param name="Key">The entity Key.</param>
		/// <returns></returns>
		public override Job GetJob(string Key) {
			var jobKey = _scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(Key)).FirstOrDefault();
			if (jobKey != null) {
				var jobDetail = _scheduler.GetJobDetail(jobKey);
				return ConvertToBusinessEntity(jobDetail);
			}
			return null;
		}

		/// <summary>
		/// Gets a list for the business entity Job.
		/// </summary>
		/// <returns></returns>
		public override List<Job> GetJobs() {
			var jobKeys = _scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupStartsWith(EncodeJobKey("")));
			var retVal = new List<Job>();
			foreach (var jobKey in jobKeys) {
				try {
					var detail = _scheduler.GetJobDetail(jobKey);
					var job = ConvertToBusinessEntity(detail);
					retVal.Add(job);
				}
				catch (System.Exception e) {
					TracingService.Write(e);
				}
			}
			return retVal;
		}

		/// <summary>
		/// Gets the complete list of jobs for a specific application name.
		/// </summary>
		/// <param name="applicationName">The application name.</param>
		/// <returns></returns>
		private List<Job> GetJobs(string applicationName) {
			if (String.IsNullOrEmpty(applicationName))
				throw new ArgumentException("Invalid argument", "applicationName");
			var jobKeys = _scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupStartsWith(const_user_job_prefix + "_" + applicationName + "_"));
			var retVal = jobKeys.Select(jobKey => ConvertToBusinessEntity(_scheduler.GetJobDetail(jobKey))).ToList();
			return retVal;
		}

		/// <summary>
		/// Converts to business entity.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <returns></returns>
		private Job ConvertToBusinessEntity(IJobDetail job) {
			string keyJob = job.Key.Group;
			try {
				var steps = (List<string>)job.JobDataMap[QuartzJob.const_vizelia_step_list];
				var schedules = (List<string>)job.JobDataMap[QuartzJob.const_vizelia_schedule_list];
				var entity = new Job {
					KeyJob = keyJob,
					Name = job.Key.Name,
					Description = job.Description,
					Schedules = schedules.Select(s =>Helper.DeserializeJson<Schedule>(s)).ToList(),
					Steps = steps.Select(stp => Helper.DeserializeJson<Step>(stp)).ToList(),
					IsPaused  = job.JobDataMap.ContainsKey(const_is_paused)
				};

				foreach (var step in entity.Steps) {
					step.KeyJob = keyJob;
				}

				entity.Schedules = entity.Schedules.Select(FixTimeZone).ToList();

				return entity;
			}
			catch (Exception) {

				var oldSteps = (List<Step>)job.JobDataMap[QuartzJob.const_vizelia_step_list];
				foreach (var step in oldSteps) {
					step.KeyJob = keyJob;
				}
				var entity = new Job {
					KeyJob = keyJob,
					Name = job.Key.Name,
					Description = job.Description,
					Schedules = (List<Schedule>)job.JobDataMap[QuartzJob.const_vizelia_schedule_list],
					Steps = oldSteps
				};
				UpdateJob(entity);
				return entity;
			}
			
			
		}

		/// <summary>
		/// Fixes the time zone of the schedule.
		/// </summary>
		/// <param name="schedule">The s.</param>
		/// <returns></returns>
		private static Schedule FixTimeZone(Schedule schedule) {
			// This is a hack to change the timezone going out to the client to reflect 
			// the user's chosen time.
			schedule.TimeZoneOffset[0] = schedule.TimeZone.GetUtcOffset(schedule.StartDate.Value);
			schedule.TimeZoneOffset[1] = schedule.TimeZoneOffset[0];
			if(schedule.RecurrencePattern != null && schedule.RecurrencePattern.Until !=null)
				schedule.TimeZoneOffset[2] = schedule.TimeZone.GetUtcOffset(schedule.RecurrencePattern.Until.Value);
			return schedule;
		}

		/// <summary>
		/// Converts to quartz.
		/// </summary>
		/// <param name="ev">The ev.</param>
		/// <returns></returns>
		private static Trigger ConvertToQuartz(CalendarEvent ev) {
			return new Trigger();
		}

		/// <summary>
		/// Runs the job.
		/// </summary>
		/// <param name="item">The item.</param>
		public override void ExecuteJob(Job item) {
			JobKey jk = new JobKey(item.Name, item.KeyJob);
			_scheduler.TriggerJob(jk);

		}

		/// <summary>
		/// Resumes the item.
		/// </summary>
		/// <param name="item">The item.</param>
		public override void ResumeItem(Job item) {
			JobKey jobKey = new JobKey(item.Name, item.KeyJob);
			var jobDetail = _scheduler.GetJobDetail(jobKey);
			if (jobDetail.JobDataMap.ContainsKey(const_is_paused)) {
				jobDetail.JobDataMap.Remove(const_is_paused);
				_scheduler.AddJob(jobDetail, true);
			}
		}

		/// <summary>
		/// Pauses the item.
		/// </summary>
		/// <param name="item">The item.</param>
		public override void PauseItem(Job item) {
			JobKey jobKey = new JobKey(item.Name, item.KeyJob);
			var jobDetail = _scheduler.GetJobDetail(jobKey);
			if (!jobDetail.JobDataMap.ContainsKey(const_is_paused)) {
				jobDetail.JobDataMap.Add(const_is_paused, true);
				_scheduler.AddJob(jobDetail, true);
			}
		}

		/// <summary>
		/// Updates an existing business entity Job and returns the result as a valid extjs form response.
		/// </summary>
		/// <param name="item">The item to update.</param>
		/// <returns></returns>
		public override Job UpdateJob(Job item) {
			DeleteJobForUpdate(item);
			CreateJob(item);
			return item;
		}
	}




}












﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.BackingStoreImplementations;
using Microsoft.Practices.EnterpriseLibrary.Caching.Database;
using Microsoft.Practices.EnterpriseLibrary.Caching.Database.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A concrete implementation of EntLib DataBackingStore in order to use the dynamic applicationName.
	/// </summary>
	[ConfigurationElementType(typeof(VizeliaDataCacheStorageData))]
	public class VizeliaCacheDataBackingStore : DataBackingStore {


		private readonly Database database;
		private readonly IStorageEncryptionProvider encryptionProvider;
		private string partitionName {
			get { return ContextHelper.ApplicationName; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="VizeliaCacheDataBackingStore"/> class.
		/// </summary>
		/// <param name="database">The database.</param>
		/// <param name="databasePartitionName">Name of the database partition.</param>
		/// <param name="encryptionProvider">The encryption provider.</param>
		public VizeliaCacheDataBackingStore(Database database, string databasePartitionName, IStorageEncryptionProvider encryptionProvider)
			: base(database, ContextHelper.ApplicationName, encryptionProvider) {
			this.database = database;
			this.encryptionProvider = encryptionProvider;
		}

		/// <summary>
		/// Adds the new item.
		/// </summary>
		/// <param name="storageKey">The storage key.</param>
		/// <param name="newItem">The new item.</param>
		protected override void AddNewItem(int storageKey, CacheItem newItem) {
			Helper.StartNewThread(() => {
				string key = newItem.Key;
				byte[] array = SerializationUtility.ToBytes(newItem.Value);
				if (this.encryptionProvider != null) {
					array = this.encryptionProvider.Encrypt(array);
				}
				byte[] value = SerializationUtility.ToBytes(newItem.GetExpirations());
				byte[] value2 = SerializationUtility.ToBytes(newItem.RefreshAction);
				CacheItemPriority scavengingPriority = newItem.ScavengingPriority;
				DateTime lastAccessedTime = newItem.LastAccessedTime;
				DbCommand storedProcCommand = this.database.GetStoredProcCommand("AddItem");
				this.database.AddInParameter(storedProcCommand, "@partitionName", DbType.String, this.partitionName);
				this.database.AddInParameter(storedProcCommand, "@storageKey", DbType.Int32, storageKey);
				this.database.AddInParameter(storedProcCommand, "@key", DbType.String, key);
				this.database.AddInParameter(storedProcCommand, "@value", DbType.Binary, array);
				this.database.AddInParameter(storedProcCommand, "@expirations", DbType.Binary, value);
				this.database.AddInParameter(storedProcCommand, "@refreshAction", DbType.Binary, value2);
				this.database.AddInParameter(storedProcCommand, "@scavengingPriority", DbType.Int32, scavengingPriority);
				this.database.AddInParameter(storedProcCommand, "@lastAccessedTime", DbType.DateTime, lastAccessedTime);
				this.database.ExecuteNonQuery(storedProcCommand);
			});
		}

		/// <summary>
		/// Flushes this instance.
		/// </summary>
		public override void Flush() {
			DbCommand storedProcCommand = this.database.GetStoredProcCommand("Flush");
			this.database.AddInParameter(storedProcCommand, "@partitionName", DbType.String, this.partitionName);
			this.database.ExecuteNonQuery(storedProcCommand);
		}

		/// <summary>
		/// Gets the count.
		/// </summary>
		public override int Count {
			get {
				DbCommand storedProcCommand = this.database.GetStoredProcCommand("GetItemCount");
				this.database.AddInParameter(storedProcCommand, "@partitionName", DbType.String, this.partitionName);
				return (int)this.database.ExecuteScalar(storedProcCommand);
			}
		}

		/// <summary>
		/// Loads the data from store.
		/// </summary>
		/// <returns></returns>
		protected override Hashtable LoadDataFromStore() {
			DbCommand storedProcCommand = this.database.GetStoredProcCommand("LoadItems");
			this.database.AddInParameter(storedProcCommand, "@partitionName", DbType.String, this.partitionName);
			DataSet dataSet = this.database.ExecuteDataSet(storedProcCommand);
			Hashtable hashtable = new Hashtable();
			foreach (DataRow dataToLoad in dataSet.Tables[0].Rows) {
				CacheItem cacheItem = this.CreateCacheItem(dataToLoad);
				hashtable.Add(cacheItem.Key, cacheItem);
			}
			return hashtable;
		}

		/// <summary>
		/// Creates the cache item.
		/// </summary>
		/// <param name="dataToLoad">The data to load.</param>
		/// <returns></returns>
		private CacheItem CreateCacheItem(DataRow dataToLoad) {
			string key = (string)dataToLoad["Key"];
			object value = this.DeserializeValue(dataToLoad);
			CacheItemPriority scavengingPriority = (CacheItemPriority)dataToLoad["ScavengingPriority"];
			object obj = this.DeserializeObject(dataToLoad, "RefreshAction");
			object obj2 = SerializationUtility.ToObject((byte[])dataToLoad["Expirations"]);
			object obj3 = (DateTime)dataToLoad["LastAccessedTime"];
			return new CacheItem((DateTime)obj3, key, value, scavengingPriority, (ICacheItemRefreshAction)obj, (ICacheItemExpiration[])obj2);
		}

		/// <summary>
		/// Deserializes the value.
		/// </summary>
		/// <param name="dataToLoad">The data to load.</param>
		/// <returns></returns>
		private object DeserializeValue(DataRow dataToLoad) {
			object obj = dataToLoad["Value"];
			if (obj == DBNull.Value) {
				obj = null;
			}
			else {
				byte[] array = (byte[])obj;
				if (this.encryptionProvider != null) {
					array = this.encryptionProvider.Decrypt(array);
				}
				obj = SerializationUtility.ToObject(array);
			}
			return obj;
		}

		/// <summary>
		/// Deserializes the object.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="columnName">Name of the column.</param>
		/// <returns></returns>
		private object DeserializeObject(DataRow data, string columnName) {
			object obj = data[columnName];
			object result;
			if (obj == DBNull.Value) {
				result = null;
			}
			else {
				result = SerializationUtility.ToObject((byte[])obj);
			}
			return result;
		}

		/// <summary>
		/// Removes the specified storage key.
		/// </summary>
		/// <param name="storageKey">The storage key.</param>
		protected override void Remove(int storageKey) {
			DbCommand storedProcCommand = this.database.GetStoredProcCommand("RemoveItem");
			this.database.AddInParameter(storedProcCommand, "@partitionName", DbType.String, this.partitionName);
			this.database.AddInParameter(storedProcCommand, "@storageKey", DbType.Int32, storageKey);
			this.database.ExecuteNonQuery(storedProcCommand);
		}


		/// <summary>
		/// Updates the last accessed time.
		/// </summary>
		/// <param name="storageKey">The storage key.</param>
		/// <param name="lastAccessedTime">The last accessed time.</param>
		protected override void UpdateLastAccessedTime(int storageKey, DateTime lastAccessedTime) {
			Helper.StartNewThread(() => {
				DbCommand storedProcCommand = this.database.GetStoredProcCommand("UpdateLastAccessedTime");
				this.database.AddInParameter(storedProcCommand, "@partitionName", DbType.String, this.partitionName);
				this.database.AddInParameter(storedProcCommand, "@lastAccessedTime", DbType.DateTime, lastAccessedTime);
				this.database.AddInParameter(storedProcCommand, "@storageKey", DbType.Int32, storageKey);
				this.database.ExecuteNonQuery(storedProcCommand);
			});
		}

	}
}

﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Linq;
using BookSleeve;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// 
	/// </summary>
	public class RedisCacheProvider : CacheProvider {

		private const string const_attribute_redis_host = "redisHost";
		private const string const_attribute_redis_port = "redisPort";
		private const string const_attribute_redis_database = "redisDatabase";
		private const string const_attribute_redis_password = "redisPassword";

		private static Object _redisConnectionLock = new Object();
		private static RedisConnection _redisClient;

		/// <summary>
		/// Gets or sets the redis host.
		/// </summary>
		/// <value>
		/// The redis host.
		/// </value>
		public string RedisHost { get; set; }


		/// <summary>
		/// Gets or sets the redis port.
		/// </summary>
		/// <value>
		/// The redis port.
		/// </value>
		public int RedisPort { get; set; }

		/// <summary>
		/// Gets or sets the redis database.
		/// </summary>
		/// <value>
		/// The redis database.
		/// </value>
		public int RedisDatabase { get; set; }

		/// <summary>
		/// Gets or sets the redis password.
		/// </summary>
		/// <value>
		/// The redis password.
		/// </value>
		public string RedisPassword { get; set; }

		/// <summary>
		/// Gets the redis client.
		/// </summary>
		private RedisConnection RedisClient {
			get {
				if (_redisClient != null && (_redisClient.State == RedisConnectionBase.ConnectionState.Open || _redisClient.State == RedisConnectionBase.ConnectionState.Opening)) return _redisClient;
				lock (_redisConnectionLock) {
					if (_redisClient != null && (_redisClient.State == RedisConnectionBase.ConnectionState.Open || _redisClient.State == RedisConnectionBase.ConnectionState.Opening)) return _redisClient;
					TracingService.Write(TraceEntrySeverity.Information, "Redis connection is opening");
					_redisClient = new RedisConnection(RedisHost, RedisPort, password: RedisPassword);
					_redisClient.Closed += (sender, e) => TracingService.Write(TraceEntrySeverity.Warning, "Redis connection was closed");
					_redisClient.Open();
				}
				return _redisClient;
			}
		}


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			if (String.IsNullOrEmpty(config[const_attribute_redis_host])) {
				throw new ProviderException(const_attribute_redis_host + " is missing");
			}
			RedisHost = config[const_attribute_redis_host];
			config.Remove(const_attribute_redis_host);

			if (String.IsNullOrEmpty(config[const_attribute_redis_port])) {
				throw new ProviderException(const_attribute_redis_port + " is missing");
			}
			RedisPort = int.Parse(config[const_attribute_redis_port]);
			config.Remove(const_attribute_redis_port);

			if (String.IsNullOrEmpty(config[const_attribute_redis_database])) {
				throw new ProviderException(const_attribute_redis_database + " is missing");
			}
			RedisDatabase = int.Parse(config[const_attribute_redis_database]);
			config.Remove(const_attribute_redis_database);

			if (!String.IsNullOrEmpty(config[const_attribute_redis_password])) {
				RedisPassword = (config[const_attribute_redis_password]);
				config.Remove(const_attribute_redis_password);
			}
			else {
				RedisPassword = null;
			}
		}


		/// <summary>
		/// Adds new CacheItem to cache.
		/// If another item already exists with the same key, that item is removed before the new item is added.
		/// If any failure occurs during this process, the cache will not contain the item being added.
		/// Items added with this method will be not expire, and will have a Normal Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority priority.
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		/// <param name="expirationInMinutes">Expires after the given number of minutes. O indicates that the cache should never expired.</param>
		public override void Add(string key, object value, double expirationInMinutes) {
			key = EncodeCacheKey(key);
			if (expirationInMinutes <= 0)
				RedisClient.Strings.Set(RedisDatabase, key, value.ToByteArray()).Wait();
			else
				RedisClient.Strings.Set(RedisDatabase, key, value.ToByteArray(), (long)expirationInMinutes * 60).Wait();
		}


		/// <summary>
		/// Adds new CacheItem to cache.
		/// If another item already exists with the same key, that item is removed before the new item is added.
		/// If any failure occurs during this process, the cache will not contain the item being added.
		/// Items added with this method will be not expire, and will have a Normal Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority priority.
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		public override void Add(string key, object value) {
			key = EncodeCacheKey(key);
			RedisClient.Strings.Set(RedisDatabase, key, value.ToByteArray()).Wait();
		}


		/// <summary>
		/// Returns the value associated with the given key.
		/// </summary>
		/// <param name="key">Key of item to return from cache.</param>
		/// <returns></returns>
		public override object GetData(string key) {
			key = EncodeCacheKey(key);
			var method = RedisClient.Strings.Get(RedisDatabase, key);
			method.Wait();
			if (method.Result == null)
				return null;
			var retVal = method.Result.Deserialize<object>();
			return retVal;
		}


		/// <summary>
		/// Removes the given item from the cache.
		/// If no item exists with that key, this method does nothing.
		/// </summary>
		/// <param name="key">Key of item to remove from cache.</param>
		public override void Remove(string key) {
			key = EncodeCacheKey(key);
			RedisClient.Keys.Remove(RedisDatabase, key).Wait();
		}

		/// <summary>
		/// Locks the specified action.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="lockName">Name of the lock.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval in milliseconds.</param>
		/// <param name="async">if set to <c>true</c> [async].</param>
		public override void Lock(Action action, string lockName = null, int maxRetries = 3, int retryInterval = 100, bool async = false) {
			if (async)
				Helper.StartNewThread(() => InternalLock(action, lockName, maxRetries, retryInterval));
			else
				InternalLock(action, lockName, maxRetries, retryInterval);
		}

		/// <summary>
		/// Internal function performing the lock.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="lockName">Name of the lock.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval in milliseconds.</param>
		private void InternalLock(Action action, string lockName, int maxRetries, int retryInterval) {
			lockName = lockName ?? "lock:" + ContextHelper.ApplicationName;
			Helper.RetryIfFalse(maxRetries, retryInterval, () => {
				var retVal = RedisClient.Strings.TakeLock(RedisDatabase, lockName, "1", 10).Result;
				if (retVal) {
					try {
						action();
					}
					catch (Exception ex) {
						TracingService.Write(TraceEntrySeverity.Error, "error internalLock " + lockName + ex.Message);
					}
					finally {
						RedisClient.Strings.ReleaseLock(RedisDatabase, lockName);
					}
				}
				else
					TracingService.Write(TraceEntrySeverity.Warning, "warning internalLock " + lockName);
				return retVal;
			});
		}

		/// <summary>
		/// Gets the existing items.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public override IDictionary<string, object> GetData(List<string> keys) {
			var keysArray = keys.Select(EncodeCacheKey).ToArray();
			var values = RedisClient.Strings.Get(RedisDatabase, keysArray).Result;
			Dictionary<string, object> retVal = new Dictionary<string, object>();
			for (int i = 0; i < values.Length; i++) {
				if (values[i] != null)
					retVal.Add(DecodeCacheKey(keysArray[i]), values[i].Deserialize<object>());
			}
			return retVal;
		}




	}
}

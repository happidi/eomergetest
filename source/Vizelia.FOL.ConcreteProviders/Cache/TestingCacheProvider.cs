﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Enyim.Caching;
using Enyim.Caching.Memcached;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Implements cache provider for tests.
	/// </summary>
	public class TestingCacheProvider : CacheProvider {

		private static Dictionary<string, object> m_Cache; 

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			m_Cache = new Dictionary<string, object>();
		}


		/// <summary>
		/// Initializes the <see cref="TestingCacheProvider"/> class.
		/// </summary>
		static TestingCacheProvider() {
			//try {
			//    InitCacheManager();
			//}
			//catch { }
		}



		/// <summary>
		/// Inits the cache manager.
		/// </summary>
		static void InitCacheManager() {
			// configuration fetched from web.config
//			m_cacheClient = new MemcachedClient();
		}



		/// <summary>
		/// Adds new CacheItem to cache.
		/// If another item already exists with the same key, that item is removed before the new item is added.
		/// If any failure occurs during this process, the cache will not contain the item being added.
		/// Items added with this method will be not expire, and will have a Normal Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority priority.
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		public override void Add(string key, object value) {
			key = EncodeCacheKey(key);
			//m_cacheClient.Store(StoreMode.Set, key, value);
			if (m_Cache.ContainsKey(key))
				m_Cache[key] = value;
			else {
				m_Cache.Add(key, value);	
			}
			
		}

		/// <summary>
		/// Adds new CacheItem to cache.
		/// If another item already exists with the same key, that item is removed before the new item is added.
		/// If any failure occurs during this process, the cache will not contain the item being added.
		/// Items added with this method will be not expire, and will have a Normal Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority priority.
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		/// <param name="expirationInMinutes">Expires after the given number of minutes. O indicates that the cache should never expired.</param>
		public override void Add(string key, object value, double expirationInMinutes) {
			Add(key,value);
		}


		/// <summary>
		/// Returns the value associated with the given key.
		/// </summary>
		/// <param name="key">Key of item to return from cache.</param>
		/// <returns></returns>
		public override object GetData(string key) {
			key = EncodeCacheKey(key);
			object value = null;
			m_Cache.TryGetValue(key,out value);
			//var retval = m_cacheClient.Get(key);
			return value;
		}

		
		

		

		/// <summary>
		/// Locks the specified action.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="lockName">Name of the lock.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval in milliseconds.</param>
		/// <param name="async">if set to <c>true</c> [async].</param>
		public override void Lock(Action action, string lockName = null, int maxRetries = 3, int retryInterval = 100, bool async = false) {
			//throw new NotImplementedException();
		}

		/// <summary>
		/// Gets the existing items.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public override IDictionary<string, object> GetData(List<string> keys) {
			if (keys != null && keys.Count > 0) {
				var retVal = keys.Select(k => new KeyValuePair<string,object>(EncodeCacheKey(k),GetData(EncodeCacheKey(k))));//m_cacheClient.Get(keys.Select(EncodeCacheKey));
				return retVal.Where(p=>p.Value != null).ToDictionary(p => DecodeCacheKey(p.Key), p => p.Value);
			}
			return new Dictionary<string, object>();
		}

		/// <summary>
		/// Adds a new item to the memory cache.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="expirationInMinutes">The expiration in minutes.</param>
		public override void MemoryCacheAdd(string key, object value, double expirationInMinutes = 1000) {
			Add(key,value);
		}

		/// <summary>
		/// Returns the value associated with the given key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public override object MemoryCacheGetData(string key) {
			return GetData(key);
		}

		/// <summary>
		/// Removes the given item from the memory cache.
		/// </summary>
		/// <param name="key">The key.</param>
		public override void MemoryCacheRemove(string key) {
			Remove(key);
		}
		/// <summary>
		/// Removes the given item from the cache. 
		/// If no item exists with that key, this method does nothing.
		/// </summary>
		/// <param name="key">Key of item to remove from cache.</param>
		public override void Remove(string key) {
			key = EncodeCacheKey(key);
			//m_cacheClient.Remove(key);
			m_Cache.Remove(key);
		}

		/// <summary>
		/// Clear the Cache
		/// </summary>
		public static void ClearCache(){
			m_Cache.Clear();
		}
	}
}

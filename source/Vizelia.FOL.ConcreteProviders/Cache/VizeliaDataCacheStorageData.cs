﻿using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.BackingStoreImplementations;
using Microsoft.Practices.EnterpriseLibrary.Caching.Database.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ContainerModel;
using Microsoft.Practices.EnterpriseLibrary.Data;


namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A custom implementation of DataCacheStorageData
	/// </summary>
	public class VizeliaDataCacheStorageData : DataCacheStorageData {

		/// <summary>
		/// Gets the registrations.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<TypeRegistration> GetRegistrations() {
			yield return new TypeRegistration<IBackingStore>(
				() => new VizeliaCacheDataBackingStore(Container.Resolved<Database>(DatabaseInstanceName),
											PartitionName,
											Container.ResolvedIfNotNull<IStorageEncryptionProvider>(StorageEncryption))) {
												Name = this.Name
											};
		}


	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Enterprise Library Cache Provider
	/// </summary>
	public class EnterpriseLibraryCacheProvider : CacheProvider {

		/// <summary>
		/// The cache manager from entlib.
		/// </summary>
		private static ICacheManager m_CacheManager;

		private readonly object generallock = new object();
		private Dictionary<string, object> lockdictionary = new Dictionary<string, object>();


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="config"></param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			if (m_CacheManager == null) {
				InitCacheManager();
			}
		}

		/// <summary>
		/// Initializes the <see cref="EnterpriseLibraryCacheProvider"/> class.
		/// </summary>
		static EnterpriseLibraryCacheProvider() {
			try {
				InitCacheManager();
			}
			catch { }
		}

		/// <summary>
		/// Inits the cache manager.
		/// </summary>
		static void InitCacheManager() {
			m_CacheManager = CacheFactory.GetCacheManager();
		}



		/// <summary>
		/// Adds new CacheItem to cache. 
		/// If another item already exists with the same key, that item is removed before the new item is added. 
		/// If any failure occurs during this process, the cache will not contain the item being added. 
		/// Items added with this method will be not expire, and will have a Normal Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority priority.
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		public override void Add(string key, object value) {
			m_CacheManager.Add(key, value);
		}

		/// <summary>
		/// Adds new CacheItem to cache. 
		/// If another item already exists with the same key, that item is removed before the new item is added. 
		/// If any failure occurs during this process, the cache will not contain the item being added. 
		/// Items added with this method will be not expire, and will have a Normal Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority priority.
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		/// <param name="expirationInMinutes">Expires after the given number of minutes. O indicates that the cache should never expired.</param>
		public override void Add(string key, object value, double expirationInMinutes) {
			//new TimeSpan(0, 5, 0)
			if (expirationInMinutes <= 0)
				Add(key, value);
			else
				m_CacheManager.Add(key, value, CacheItemPriority.NotRemovable, null, new AbsoluteTime(TimeSpan.FromMinutes(expirationInMinutes)));
		}



		/// <summary>
		/// Returns the value associated with the given key.
		/// </summary>
		/// <param name="key">Key of item to return from cache.</param>
		/// <returns></returns>
		public override object GetData(string key) {
			return m_CacheManager.GetData(key);
		}


		/// <summary>
		/// Removes the given item from the cache. 
		/// If no item exists with that key, this method does nothing.
		/// </summary>
		/// <param name="key">Key of item to remove from cache.</param>
		public override void Remove(string key) {
			m_CacheManager.Remove(key);
		}


		/// <summary>
		/// Gets the existing items.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public override IDictionary<string, object> GetData(List<string> keys) {
			var result = new Dictionary<string, object>();
			foreach (var key in keys) {
				var obj = GetData(key);
				if (obj != null)
					result.Add(key, obj);
			}
			return result;
		}


		/// <summary>
		/// Locks the specified action.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="lockName">Name of the lock.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval in milliseconds.</param>
		/// <param name="async">if set to <c>true</c> [async].</param>
		public override void Lock(Action action, string lockName = null, int maxRetries = 3, int retryInterval = 100, bool async = false) {
			if (async)
				Helper.StartNewThread(() => InternalLock(action, lockName));
			else
				InternalLock(action, lockName);
		}

		/// <summary>
		/// Internal lock function.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="lockName">Name of the lock.</param>
		private void InternalLock(Action action, string lockName) {
			lockName = lockName ?? "lock:" + ContextHelper.ApplicationName;

			lock (generallock) {
				if (!lockdictionary.ContainsKey(lockName))
					lockdictionary.Add(lockName, new object());
			}
			lock (lockdictionary[lockName]) {
				action();
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Web;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// HttpRuntime Cache Provider.
	/// Note this cache is isolated to each application pool, meaning 
	/// it is NOT shared between front-ends and energy aggregators.
	/// </summary>
	public class HttpRuntimeCacheProvider : CacheProvider {

		private readonly object generallock = new object();
		private Dictionary<string, object> lockdictionary = new Dictionary<string, object>();

		/// <summary>
		/// Adds new CacheItem to cache. 
		/// If another item already exists with the same key, that item is removed before the new item is added. 
		/// If any failure occurs during this process, the cache will not contain the item being added. 
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		public override void Add(string key, object value) {
			if (value == null)
				return;
			string cacheKey = EncodeCacheKey(key);
			HttpRuntime.Cache[cacheKey] = value;
		}

		/// <summary>
		/// Adds new CacheItem to cache. 
		/// If another item already exists with the same key, that item is removed before the new item is added. 
		/// If any failure occurs during this process, the cache will not contain the item being added. 
		/// </summary>
		/// <param name="key">Identifier for the item cache.</param>
		/// <param name="value">Value to be stored in cache. May be null.</param>
		/// <param name="expirationInMinutes">Expires after the given number of minutes. O indicates that the cache should never expired.</param>
		public override void Add(string key, object value, double expirationInMinutes) {
			if (value == null)
				return;

			if (expirationInMinutes <= 0) {
				Add(key, value);
			}
			else {
				string cacheKey = EncodeCacheKey(key);
				HttpRuntime.Cache.Insert(cacheKey, value, null, DateTime.Now.AddMinutes(expirationInMinutes), System.Web.Caching.Cache.NoSlidingExpiration, System.Web.Caching.CacheItemPriority.Normal, null);
			}
		}



		/// <summary>
		/// Returns the value associated with the given key.
		/// </summary>
		/// <param name="key">Key of item to return from cache.</param>
		/// <returns></returns>
		public override object GetData(string key) {
			string cacheKey = EncodeCacheKey(key);
			object data = HttpRuntime.Cache[cacheKey];
			return data;
		}


		/// <summary>
		/// Removes the given item from the cache. 
		/// If no item exists with that key, this method does nothing.
		/// </summary>
		/// <param name="key">Key of item to remove from cache.</param>
		public override void Remove(string key) {
			string cacheKey = EncodeCacheKey(key);
			HttpRuntime.Cache.Remove(cacheKey);
		}


		/// <summary>
		/// Locks the specified action.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="lockName">Name of the lock.</param>
		/// <param name="maxRetries">The max retries.</param>
		/// <param name="retryInterval">The retry interval in milliseconds.</param>
		/// <param name="async">if set to <c>true</c> [async].</param>
		public override void Lock(Action action, string lockName = null, int maxRetries = 3, int retryInterval = 100, bool async = false) {
			if (async)
				Helper.StartNewThread(() => InternalLock(action, lockName));
			else
				InternalLock(action, lockName);
		}

		/// <summary>
		/// Gets the existing items.
		/// </summary>
		/// <param name="keys">The keys.</param>
		/// <returns></returns>
		public override IDictionary<string, object> GetData(List<string> keys) {
			var result = new Dictionary<string, object>();
			foreach (var key in keys) {
				var obj = GetData(key);
				if (obj != null)
					result.Add(key, obj);
			}
			return result;
		}

		/// <summary>
		/// Internals the lock.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="lockName">Name of the lock.</param>
		private void InternalLock(Action action, string lockName) {
			lockName = lockName ?? "lock:" + ContextHelper.ApplicationName;

			lock (generallock) {
				if (!lockdictionary.ContainsKey(lockName))
					lockdictionary.Add(lockName, new object());
			}
			lock (lockdictionary[lockName]) {
				action();
			}
		}


	}
}

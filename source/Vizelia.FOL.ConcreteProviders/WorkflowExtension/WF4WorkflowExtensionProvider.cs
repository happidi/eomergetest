﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using RazorEngine;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common.Configuration;
using Vizelia.FOL.Providers;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using Vizelia.FOL.WF.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a WorkflowExtension provider for WF 4.
	/// </summary>
	public class WF4WorkflowExtensionProvider : WorkflowExtensionProvider {

		private Dictionary<Type, List<object>> _extensions;
		private object _extensionsLock;


		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);

			this._extensionsLock = new object();
			this._extensions = new Dictionary<Type, List<object>>();

			foreach (WorkflowExtensionElement extension in VizeliaConfiguration.Instance.WorkflowExtension) {
				this.AddExtensionFromSettings(extension);
			}
		}

		/// <summary>
		/// Adds the extension from settings.
		/// </summary>
		/// <param name="extension">The extension.</param>
		private void AddExtensionFromSettings(WorkflowExtensionElement extension) {
			object extensionInstance = null;
			Type type = Type.GetType(extension.Type, true);
			extensionInstance = Activator.CreateInstance(type);
			this.AddExtensionImpl(extensionInstance);
		}

		/// <summary>
		/// Adds the extension implementation.
		/// </summary>
		/// <param name="extension">The extension.</param>
		private void AddExtensionImpl(object extension) {
			lock (this._extensionsLock) {
				if (this.GetAllExtensions(extension.GetType()).Contains(extension)) {
					throw new InvalidOperationException("Cannot add the same service more than once");
				}	
				Type baseType = extension.GetType();
				foreach (Type type2 in baseType.GetInterfaces()) {
					List<object> list;
					if (this._extensions.ContainsKey(type2)) {
						list = this._extensions[type2];
					}
					else {
						list = new List<object>();
						this._extensions.Add(type2, list);
					}
					list.Add(extension);
				}
				while (baseType != null) {
					List<object> list2 = null;
					if (this._extensions.ContainsKey(baseType)) {
						list2 = this._extensions[baseType];
					}
					else {
						list2 = new List<object>();
						this._extensions.Add(baseType, list2);
					}
					list2.Add(extension);
					baseType = baseType.BaseType;
				}
			}
		}

		/// <summary>
		/// Gets all extensions.
		/// </summary>
		/// <param name="extensionType">Type of the extension.</param>
		/// <returns></returns>
		private ReadOnlyCollection<object> GetAllExtensions(Type extensionType) {
			if (extensionType == null) {
				throw new ArgumentNullException("extensionType");
			}
			lock (this._extensionsLock) {
				List<object> list = new List<object>();
				if (this._extensions.ContainsKey(extensionType)) {
					list.AddRange(this._extensions[extensionType]);
				}
				return new ReadOnlyCollection<object>(list);
			}
		}

 

 

		/// <summary>
		/// Gets the extension.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public override T GetExtension<T>() {
			return (T)this.GetExtension(typeof(T));

		}

		/// <summary>
		/// Gets the extension.
		/// </summary>
		/// <param name="extensionType">Type of the extension.</param>
		/// <returns></returns>
		public override object GetExtension(Type extensionType) {
			if (extensionType == null) {
				throw new ArgumentNullException("extensionType");
			}

			lock (this._extensionsLock) {
				object obj2 = null;
				if (this._extensions.ContainsKey(extensionType)) {
					List<object> list = this._extensions[extensionType];
					if (list.Count > 1) {
						throw new InvalidOperationException(String.Format("More than one runtime extension exists of type '{0}'.", extensionType.ToString()));
					}
					if (list.Count == 1) {
						obj2 = list[0];
					}
				}
				return obj2;
			}
		}

		/// <summary>
		/// Raises the event.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="eventName">Name of the event.</param>
		/// <returns></returns>
		public override FormResponse RaiseEvent(IWorkflowEntity entity, string eventName) {
			FormResponse retVal = null;
			var genericLocalService = typeof(BaseLocalService<>);
			var constructedLocalService = genericLocalService.MakeGenericType(new Type[] { entity.GetType() });
			var localService = WorkflowExtensionService.GetExtension(constructedLocalService);
			var raiseEvent = localService.GetType().GetMethod("RaiseEvent");
			retVal = (FormResponse)raiseEvent.Invoke(localService, new object[] { eventName, entity });
			return retVal;
		}

		/// <summary>
		/// Updates the definition of the workflow.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public override FormResponse UpdateDefinition(IWorkflowEntity entity) {
			var genericLocalService = typeof(BaseLocalService<>);
			var constructedLocalService = genericLocalService.MakeGenericType(new Type[] { entity.GetType() });
			var localService = WorkflowExtensionService.GetExtension(constructedLocalService);
			var getItemByInstanceId = localService.GetType().GetMethod("GetItemByInstanceId");
			var item = (IWorkflowEntity)getItemByInstanceId.Invoke(localService, new object[] {entity.InstanceId});
			var setState = localService.GetType().GetMethod("SetState");
			var retVal = (FormResponse)setState.Invoke(localService, new object[] { item.State, item });
			return retVal;
		}
	}


}











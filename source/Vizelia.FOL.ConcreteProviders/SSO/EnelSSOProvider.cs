﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Security;
using Vizelia.FOL.Providers;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.ServiceAgents;
using System.Collections.Specialized;
using System.Configuration.Provider;
using Microsoft.WindowsAzure;
using System.Threading;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a MachineProvider using Azure.
	/// </summary>
	public class EnelSSOProvider : BaseSSOProvider {
		/// <summary>
		/// Validates a username out of the secret secret.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <returns>
		/// True if validation succeeded, false otherwise
		/// </returns>
		public override UserDetails Validate(out string applicationName) {
			applicationName = ContextHelper.ApplicationName;
			ServicePointManager.ServerCertificateValidationCallback += ((sender, certificate, chain, sslPolicyErrors) => true); 
			try {
				using (var serviceAgent = new EnelSSOValidationServiceAgent()) {

					var token = HttpContext.Current.Request["tokenId"];
					{
						var userId = HttpContext.Current.Request["userId"];

						var response = serviceAgent.Validate(token, userId, string.Empty);
						var responseParts = response.Split(';');
						if (responseParts[0] == "OK") {
							return new UserDetails(true, userId);
						}
						else {
							var userDetails = new UserDetails(false, string.Empty);
							if (responseParts[1] != "null")
								userDetails.ErrorRedirect = responseParts[1];
							return userDetails;
						}
					}
				}

			}
			catch (Exception) {
				return new UserDetails(false, string.Empty);
			}

		}
	}
}
﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Security;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;
using DotNetOpenAuth.OpenId.RelyingParty;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Providers;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.ServiceAgents;
using System.Collections.Specialized;
using System.Configuration.Provider;
using Microsoft.WindowsAzure;
using System.Threading;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a OpenId Provider
	/// </summary>
	public class OpenIdProvider : BaseSSOProvider {
		/// <summary>
		/// Validates a username out of the secret secret.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <returns>
		/// True if validation succeeded, false otherwise
		/// </returns>
		public override UserDetails Validate(out string applicationName) {
			applicationName = ApplicationName;
			OpenIdRelyingParty rp = new OpenIdRelyingParty();
			var r = rp.GetResponse();
			if (r != null) {
				switch (r.Status) {
					case AuthenticationStatus.Authenticated:
						var properties = r.GetExtension<FetchResponse>();

						var username = GetUsernameFromProperties(properties);
						var userDetails = new UserDetails(true, username);
						FillUserDetails(userDetails, properties);
						return userDetails;


					case AuthenticationStatus.Canceled:
						break;
					case AuthenticationStatus.Failed:
						break;
				}
			}
			else {
				OpenIdLogin();
			}
			return new UserDetails(false,string.Empty);

		}

		/// <summary>
		/// Fills the user details.
		/// </summary>
		/// <param name="userDetails">The user details.</param>
		/// <param name="properties">The properties.</param>
		private void FillUserDetails(UserDetails userDetails, FetchResponse properties) {

			var firstName = properties.Attributes.FirstOrDefault(a => a.TypeUri == WellKnownAttributes.Name.First);
			var lastName = properties.Attributes.FirstOrDefault(a => a.TypeUri == WellKnownAttributes.Name.Last);
			var email = properties.Attributes.First(a => a.TypeUri == WellKnownAttributes.Contact.Email);
			var fullName = properties.Attributes.FirstOrDefault(a => a.TypeUri == WellKnownAttributes.Name.FullName);
			string firstNameValue = string.Empty;
			string lastNameValue = string.Empty;
			if ((firstName != null) && (lastName != null)) {
				firstNameValue = firstName.Values.First();
				lastNameValue = lastName.Values.First();
			}
			else if (fullName != null) {
				firstNameValue = fullName.Values.First().Split(' ')[0];
				lastNameValue = fullName.Values.First().Split(' ')[1];
			}

			userDetails.FirstName = firstNameValue;
			userDetails.LastName = lastNameValue;
			if (email != null) {
				userDetails.Email = email.Values.FirstOrDefault();
			}
		}


		/// <summary>
		/// Gets the username from properties.
		/// </summary>
		/// <param name="properties">The properties.</param>
		/// <returns></returns>
		public virtual string GetUsernameFromProperties(FetchResponse properties) {
			return properties.Attributes.First(a => a.TypeUri == IdPropertyPath).Values.First();

		}


		/// <summary>
		/// logins through the open id provider.
		/// </summary>
		private void OpenIdLogin() {
			string discoveryUri = OpenIdUrl;
			OpenIdRelyingParty openid = new OpenIdRelyingParty();
			var realm = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "");
			var returnToUrl = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
			var req = openid.CreateRequest(discoveryUri, realm, returnToUrl);
			var fetchRequest = new FetchRequest();
			fetchRequest.Attributes.AddRequired(WellKnownAttributes.Contact.Email);
			fetchRequest.Attributes.AddRequired(WellKnownAttributes.Name.First);
			fetchRequest.Attributes.AddRequired(WellKnownAttributes.Name.Last);
			fetchRequest.Attributes.AddRequired(WellKnownAttributes.Name.FullName);
			req.AddExtension(fetchRequest);
			req.RedirectToProvider();
		}
	}
}

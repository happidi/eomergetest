﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Web;
using Vizelia.FOL.Providers;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.ServiceAgents;
using System.Collections.Specialized;
using System.Configuration.Provider;
using Microsoft.WindowsAzure;
using System.Threading;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a MachineProvider using Azure.
	/// </summary>
	public class WindowsAuthenticationSSOProvider : BaseSSOProvider {
		/// <summary>
		/// Validates a username out of the secret secret.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <returns>
		/// True if validation succeeded, false otherwise
		/// </returns>
		public override UserDetails Validate(out string applicationName) {
			applicationName = ApplicationName;
			
			if ((HttpContext.Current.User.Identity is WindowsIdentity) && (HttpContext.Current.User.Identity.IsAuthenticated)) {
			 return new UserDetails(true, HttpContext.Current.User.Identity.Name.Split('\\')[1]);
			}

			return  new UserDetails(false,string.Empty);
		}
	}
}

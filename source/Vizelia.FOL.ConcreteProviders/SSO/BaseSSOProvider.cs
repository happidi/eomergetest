﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Interfaces;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A base class for sso providers - implements Create and Get User
	/// </summary>
	public abstract class BaseSSOProvider : SSOProvider {
		/// <summary>
		/// Gets the user.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="userSuffix">The suffix attached to the username.</param>
		/// <returns></returns>
		public override FOLMembershipUser GetUser(string username, string userSuffix) {
			var authenticationBusinessLayer = Helper.Resolve<IAuthenticationBusinessLayer>();
			var user = authenticationBusinessLayer.User_GetByUserName(username +  userSuffix); 
			return user;
		}


		/// <summary>
		/// Creates the user.
		/// </summary>
		/// <param name="userDetails">The user details.</param>
		/// <param name="userSuffix">The suffix attached to the username.</param>
		/// <returns></returns>
		public override string CreateUser(UserDetails userDetails, string userSuffix) {
			var membershipProvider = (IFOLSqlMembershipProvider)Membership.Provider;

			var coreBusinessLayer = Helper.Resolve<ICoreBusinessLayer>();

			string userName = userDetails.Username + userSuffix;
			string password =  Membership.GeneratePassword(Membership.MinRequiredPasswordLength,
															  Membership.MinRequiredNonAlphanumericCharacters);

			MembershipCreateStatus status;
			var occupant = coreBusinessLayer.Occupant_CreateFromUser(userDetails.LastName,userDetails.FirstName, userName, "OPENID_");
			membershipProvider.CreateUser(userName, password, userDetails.Email, null, null, false, new FOLProviderUserKey() { KeyOccupant = occupant.KeyOccupant }, out status);
			return userName;
		}

	}
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Security;
using Vizelia.FOL.Providers;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.ServiceAgents;
using System.Collections.Specialized;
using System.Configuration.Provider;
using Microsoft.WindowsAzure;
using System.Threading;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a MachineProvider using Azure.
	/// </summary>
	public class UserPasswordSSOProvider : BaseSSOProvider {
		/// <summary>
		/// Validates a username out of the secret secret.
		/// </summary>
		/// <param name="applicationName">Name of the application.</param>
		/// <returns>
		/// True if validation succeeded, false otherwise
		/// </returns>
		public override UserDetails Validate(out string applicationName) {
			try {
				var applicationanduser = HttpContext.Current.Request["Username"];
				var username = applicationanduser.Split('\\')[1];
				applicationName = applicationanduser.Split('\\')[0];
				var password = HttpContext.Current.Request["Password"];
				var membershipProvider = (IFOLSqlMembershipProvider)Membership.Provider;
                ContextHelper.ApplicationName = applicationName;
				var isValidated=  membershipProvider.ValidateUser(username, password);
				return new UserDetails(isValidated,username);

			}
			catch (Exception) {
				applicationName = ApplicationName;
				return new UserDetails(false,string.Empty);
			}
		}
	}
}

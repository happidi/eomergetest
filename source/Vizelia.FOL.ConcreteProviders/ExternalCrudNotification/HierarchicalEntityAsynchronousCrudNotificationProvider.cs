﻿using Vizelia.FOL.DataLayer;
using Vizelia.FOL.DataLayer.Interfaces;
using Vizelia.FOL.Providers;
using Vizelia.FOL.Common;
namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// A entity CRUD notification provider for Hierarchical entities that calls the refreshing of their views when they are changed.
	/// </summary>
	public class HierarchicalEntityAsynchronousCrudNotificationProvider : ExternalCrudNotificationProvider {

		/// <summary>
		/// Flushes the waiting notifications.
		/// </summary>
		public override void FlushWaitingNotifications() {
			// Broker that notify the service broker
			ICoreDataAccess coreDataAccess = Helper.CreateInstance<CoreDataAccess, ICoreDataAccess>();
			coreDataAccess.RefreshHierarchies();
		}

		/// <summary>
		/// Determines whether the CRUD notification provider is ready to receive notifications.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the CRUD notification provider is ready to receive notifications; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsReadyForNotifications() {
			return true; // I was born ready.
		}

		/// <summary>
		/// Send a notification to an external crud notification service.
		/// </summary>
		/// <param name="notifications">a list of entity and the crud operation that was performed</param>
		public override void SendNotification(System.Collections.Generic.List<BusinessEntities.CrudInformation> notifications) {

		}
	}
}
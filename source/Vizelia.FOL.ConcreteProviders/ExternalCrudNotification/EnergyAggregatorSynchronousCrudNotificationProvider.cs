﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Providers;
using Vizelia.FOL.ServiceAgents;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {

	/// <summary>
	/// Implements external crud notification behavior by sending a message to a WCF service
	/// </summary>
	public class EnergyAggregatorSynchronousCrudNotificationProvider : EnergyAggregatorCrudNotificationProvider {

		/// <summary>
		/// Send a notification update, delete or create the base business entities that are stored in memory.
		/// </summary>
		/// <param name="notifications">The notifications.</param>
		public override void SendNotification(List<CrudInformation> notifications) {
			try {
				List<CrudInformation> energyAggregatorNotifyableEntities =
					notifications.Where(n => n.Entity is IEnergyAggregatorNotifyableEntity).ToList();

				if (energyAggregatorNotifyableEntities.Count == 0)
					return;

				using (var energyAggregatorServiceAgent = GetEnergyAggregatorServiceAgent()) {
					energyAggregatorServiceAgent.CrudNotification(energyAggregatorNotifyableEntities);
				}
			}
			catch (Exception ex) {
				TracingService.Write(ex);
				throw;
			}
		}

		/// <summary>
		/// Flushes the waiting notifications.
		/// </summary>
		public override void FlushWaitingNotifications() {
			// Do nothing.
		}
	}
}

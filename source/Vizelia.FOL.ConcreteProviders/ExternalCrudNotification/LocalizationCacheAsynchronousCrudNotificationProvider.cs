﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Implements external crud notification behavior by sending a message to a WCF service
	/// </summary>
	public class LocalizationCacheAsynchronousCrudNotificationProvider : ExternalCrudNotificationProvider {
		/// <summary>
		/// use to keep track of crud notification keys in the cache
		/// </summary>
		private const string const_should_refresh_localization_cache = "should_refresh_localization_cache";

		private bool ShouldRefreshCache {
			get {
				var shouldRefresh = ContextHelper.Get(const_should_refresh_localization_cache) as string;
				return (shouldRefresh == bool.TrueString);
			}
			set {
				ContextHelper.Add(const_should_refresh_localization_cache, value.ToString(CultureInfo.InvariantCulture));
			}
		}

		/// <summary>
		/// Determines whether the CRUD notification provider is ready to receive notifications.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the CRUD notification provider is ready to receive notifications; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsReadyForNotifications() {
			return true;
		}

		/// <summary>
		/// Send a notification update, delete or create the base business entities that are stored in memory.
		/// </summary>
		/// <param name="notifications">The notifications.</param> 
		public override void SendNotification(List<CrudInformation> notifications) {
			if (ShouldRefreshCache)
				return;
			// push in order
			foreach (CrudInformation t in notifications) {
				if (t.Entity is LocalizationResource) {
					ShouldRefreshCache = true;
					break;
				}
			}
		}

		/// <summary>
		/// Flushes the waiting notifications.
		/// </summary>
		public override void FlushWaitingNotifications() {
			try {
				if (ShouldRefreshCache) {
					CacheService.Remove(CacheKey.const_cache_localization);
				}
			}
			catch (Exception ex) {
				TracingService.Write(ex);
				throw;
			}
			finally {
				ShouldRefreshCache = false;
			}
		}
	}
}

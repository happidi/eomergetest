using Vizelia.FOL.Providers;
using Vizelia.FOL.ServiceAgents;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Implements external crud notification behavior by sending a message to a WCF service - base class for Energy Aggregator.
	/// </summary>
	public abstract class EnergyAggregatorCrudNotificationProvider : ExternalCrudNotificationProvider {
		/// <summary>
		/// Gets the energy aggregator service agent.
		/// </summary>
		/// <returns></returns>
		protected EnergyAggregatorServiceAgent GetEnergyAggregatorServiceAgent() {
			var machines = MachineService.GetList();
			var energyAggregatorServiceAgent = new EnergyAggregatorServiceAgent(machines);
			return energyAggregatorServiceAgent;
		}

		/// <summary>
		/// Determines whether the CRUD notification provider is ready to receive notifications.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the CRUD notification provider is ready to receive notifications; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsReadyForNotifications() {
			using (var serviceAgent = GetEnergyAggregatorServiceAgent()) {
				bool isReady = serviceAgent.IsReadyForNotifications;
				return isReady;
			}
		}
	}
}
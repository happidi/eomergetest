﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.BusinessLayer.Broker;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Implements external crud notification behavior by sending a message to a WCF service
	/// </summary>
	public class EnergyAggregatorAsynchronousCrudNotificationProvider : EnergyAggregatorCrudNotificationProvider {

		/// <summary>
		/// use to keep track of crud notification keys in the cache
		/// </summary>
		public const string const_awaiting_publishing_queue_key = "awaitingPublishingQueueKey";

		private Queue<CrudInformation> awaitingPublishing {
			get {
				Queue<CrudInformation> queue = ContextHelper.Get(const_awaiting_publishing_queue_key) as Queue<CrudInformation>;
				if (queue == null) {
					queue = new Queue<CrudInformation>();
					ContextHelper.Add(const_awaiting_publishing_queue_key, queue);
				}
				return queue;
			}
			set {
				ContextHelper.Add(const_awaiting_publishing_queue_key, value);
			}
		}

		/// <summary>
		/// Send a notification update, delete or create the base business entities that are stored in memory.
		/// </summary>
		/// <param name="notifications">The notifications.</param> 
		public override void SendNotification(List<CrudInformation> notifications) {
			// push in order
			foreach (CrudInformation t in notifications.Where(t => t.Entity is IEnergyAggregatorNotifyableEntity)) {
				// imitate the behavior in Energy BL
				if (t.Entity is Meter) {
					var meter = t.Entity as Meter;
					var broker = Helper.CreateInstance<MeterBroker>();
					broker.PopulateList(meter, "MeterOperations", "CalendarEventCategory");
				}
				awaitingPublishing.Enqueue(t);
			}
		}

		/// <summary>
		/// Flushes the waiting notifications.
		/// </summary>
		public override void FlushWaitingNotifications() {
			try {
				// Optimization so we won't do all the WCF overhead and eventually not send anything.
				if (awaitingPublishing.Count == 0) return;

				using (var serviceAgent = GetEnergyAggregatorServiceAgent()) {
					List<CrudInformation> objectsToNotifyAbout = awaitingPublishing.ToList();
					serviceAgent.CrudNotification(objectsToNotifyAbout);
				}

				awaitingPublishing.Clear();
			}
			catch (Exception ex) {
				TracingService.Write(ex);
				throw;
			}
		}
	}
}

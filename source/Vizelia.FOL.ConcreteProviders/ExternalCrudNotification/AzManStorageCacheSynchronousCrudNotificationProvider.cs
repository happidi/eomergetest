﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Security;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Implements external crud notification behavior by sending a message to a WCF service
	/// </summary>
	public class AzManStorageCacheSynchronousCrudNotificationProvider : ExternalCrudNotificationProvider {
		/// <summary>
		/// Determines whether the CRUD notification provider is ready to receive notifications.
		/// </summary>
		/// <returns>
		///   <c>true</c> if the CRUD notification provider is ready to receive notifications; otherwise, <c>false</c>.
		/// </returns>
		public override bool IsReadyForNotifications() {
			return true;
		}

		/// <summary>
		/// Send a notification update, delete or create the base business entities that are stored in memory.
		/// </summary>
		/// <param name="notifications">The notifications.</param> 
		public override void SendNotification(List<CrudInformation> notifications) {
			foreach (var t in notifications) {
				if (t.Entity is AuthorizationItem || t.Entity is ApplicationGroup || t.Entity is FOLMembershipUser) {
					CacheService.Remove(CacheKey.const_cache_localization);
					FlushWaitingNotifications();
				}
			}
			
		}

		/// <summary>
		/// Flushes the waiting notifications.
		/// </summary>
		public override void FlushWaitingNotifications() {
			try {
				var providerRole = (IFOLRoleProvider)Roles.Provider;
				providerRole.InvalidateStorageCache();
			}
			catch (Exception ex) {
				TracingService.Write(ex);
				throw;
			}
		}
	}
}

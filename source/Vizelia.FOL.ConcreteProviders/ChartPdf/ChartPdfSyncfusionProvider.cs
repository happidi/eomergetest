﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Windows.Threading;
using Syncfusion.HtmlConverter;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Grid;
using Syncfusion.Pdf.HtmlToPdf;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;
using VizTuple = System.Tuple<Vizelia.FOL.BusinessEntities.Chart, Vizelia.FOL.BusinessEntities.StreamResult>;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of a standard charting provider using the library Visifire.
	/// </summary>
	public class ChartPdfSyncfusionProvider : ChartPdfProvider {
		/// <summary>
		/// A stream represnting the Vizelia Logo
		/// </summary>
		private Stream m_VizeliaLogo;
		/// <summary>
		/// The starting height for the header
		/// </summary>
		private int m_HeaderStartingHeight = 2;

		/// <summary>
		/// The minimum height of the pdf grid row 
		/// </summary>
		private int const_pdfGrid_row_minimum_height = 15; 

		const float const_pdfgrid_border_width = 0.5F;

		/// <summary>
		/// Initializes a new instance of the <see cref="ChartPdfSyncfusionProvider"/> class.
		/// </summary>
		public ChartPdfSyncfusionProvider() {
			m_VizeliaLogo = GetVizeliaLogo();
		}

		/// <summary>
		/// Initializes the provider.
		/// </summary>
		/// <param name="name">The provider name.</param>
		/// <param name="config">The collection of config attributes.</param>
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config) {
			base.Initialize(name, config);
		}


		/// <summary>
		/// Return the default pdf page size in pixel.
		/// </summary>
		/// <returns></returns>
		public override Size GetPageDefaultSize() {
			PdfUnitConvertor con = new PdfUnitConvertor();
			SizeF size = GetPageDefaultSizeInPoint(null);
			int width = (int)con.ConvertToPixels(size.Width * 0.7f, PdfGraphicsUnit.Point) * 2;
			int height = (int)con.ConvertToPixels(size.Height * 0.7f, PdfGraphicsUnit.Point) * 2;

			return new Size(width, height);
		}

		/// <summary>
		/// Return the default pdf page in points.
		/// </summary>
		/// <param name="page"></param>
		/// <returns></returns>
		private SizeF GetPageDefaultSizeInPoint(PdfPage page) {
			SizeF retVal;
			if (page == null) {
				PdfDocument pdfDoc = CreateNewDocument(string.Empty);
				page = pdfDoc.Pages.Add();
				retVal = page.GetClientSize();
				pdfDoc.Close(true);
			}
			else {
				retVal = page.GetClientSize();
			}
			return retVal;
		}

		/// <summary>
		/// Renders the chart as an excel spreadsheet stream.
		/// </summary>
		/// <param name="chartsWithImage">The charts with image.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		public override StreamResult RenderPdfStream(IEnumerable<VizTuple> chartsWithImage, string title) {
			PdfDocument pdfDoc = CreateNewDocument(title);
			PdfUnitConvertor con = new PdfUnitConvertor();

			foreach (VizTuple chartWithImage in chartsWithImage) {
				var chart = chartWithImage.Item1;
				var stream = chartWithImage.Item2;

				try {

					if (ChartHelper.HasData(chart) || (stream != null && stream.ContentStream != null && stream.ContentStream.Length > 0)) {
						PdfPage page = null;
						
						if (stream.ContentStream != null && stream.ContentStream.Length > 0 
							&& chart.DisplayMode != ChartDisplayMode.Grid
							&& chart.DisplayMode != ChartDisplayMode.CustomGrid) {
							page = pdfDoc.Pages.Add();
							PdfGraphics g = page.Graphics;
							if (stream.MimeType == MimeType.Html) {
								var htmlConertionThread = new Thread(() => {
									InternalHtmlToPdfStream(stream.ContentStream.GetString(), page);
									try {
										Dispatcher.CurrentDispatcher.InvokeShutdown();
									}
									catch { ;}
								}) { IsBackground = true };

								htmlConertionThread.SetApartmentState(ApartmentState.STA);
								htmlConertionThread.Start();
								htmlConertionThread.Join();
							}
							else {
								PdfBitmap image = new PdfBitmap(stream.ContentStream);
								var widthInPoint = con.ConvertFromPixels(image.Width / 0.7f, PdfGraphicsUnit.Point) / 2;
								var heightInPoint = con.ConvertFromPixels(image.Height / 0.7f, PdfGraphicsUnit.Point) / 2;
								var clientSize = page.GetClientSize();

								if (widthInPoint > clientSize.Width || heightInPoint > clientSize.Height) {
									var ratio = widthInPoint / clientSize.Width;
									ratio = Math.Max(ratio, heightInPoint / clientSize.Height);

									widthInPoint = widthInPoint / ratio;
									heightInPoint = heightInPoint / ratio;
								}

								g.DrawImage(image, 0, 0, widthInPoint, heightInPoint);
							}
						}

						//Add Grid to the PDF
						if (ChartHelper.HasData(chart) && !chart.HideDatagridInExport && chart.DisplayMode != ChartDisplayMode.CustomGrid) {
							bool shouldShowPercentage = GetShouldShowPercentageFromChart(chart);

							int start = 0;
							int pageSize = int.MaxValue;
							var dataTable = ChartHelper.ToDataTablePaged(chart, shouldShowPercentage, start, pageSize);
							if (dataTable.Rows.Count != 0) {
								page = pdfDoc.Pages.Add();
								PdfGrid table = new PdfGrid { DataSource = dataTable };

								SetTableStyle(table, chart.DataGridPercentage);
								table.Style.Font = GetFontForLocal();

								PdfGridLayoutFormat format = new PdfGridLayoutFormat();
								format.Layout = PdfLayoutType.Paginate;
								format.Break = PdfLayoutBreakType.FitElement;
								table.RepeatHeader = true;

								table.Draw(page, new PointF(0, 0), format);
							}

							// HistoricalAnalysis
							foreach (var historicalAnalysis in chart.HistoricalAnalysis.Values) {
								start = 0;
								pageSize = int.MaxValue;
								dataTable = ChartHelper.ToDataTablePaged(chart, shouldShowPercentage, start, pageSize, dataSerieCollection: historicalAnalysis);
								if (dataTable.Rows.Count != 0) {
									page = pdfDoc.Pages.Add();
									PdfGrid table = new PdfGrid { DataSource = dataTable };

									SetTableStyle(table, chart.DataGridPercentage);
									table.Style.Font = GetFontForLocal();

									PdfGridLayoutFormat format = new PdfGridLayoutFormat();
									format.Layout = PdfLayoutType.Paginate;
									format.Break = PdfLayoutBreakType.FitElement;
									table.RepeatHeader = true;
									
									table.Draw(page, new PointF(0, 0), format);
								}
							}
						}

						// Add Custom Grid to the PDF
						if (ChartHelper.HasData(chart) && chart.DisplayMode == ChartDisplayMode.CustomGrid){
							
							page = pdfDoc.Pages.Add();

							PdfGrid customGridPdfGrid = CreatePdfGridForCustomGrid(chart);

							var format = new PdfGridLayoutFormat {
								Layout = PdfLayoutType.Paginate,
								Break = PdfLayoutBreakType.FitElement
							};

							customGridPdfGrid.Draw(page, new PointF(0, 0), format);
						}
					}
				}
				catch (SystemException e) {
					TracingService.Write(e);
				}
			}
			pdfDoc.PageSettings.Orientation = PdfPageOrientation.Landscape;
			MemoryStream ms = new MemoryStream();
			pdfDoc.Save(ms);
			pdfDoc.Close(true);
			ms.Position = 0;
			var filename = TimeZoneHelper.GetFilenameTimestamped(title);
			var retVal = new StreamResult(ms, filename, "pdf", MimeType.Pdf);
			return retVal;
		}


		/// <summary>
		/// Creates the PDF grid for Chart's custom grid data.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		private PdfGrid CreatePdfGridForCustomGrid(Chart chart){
			var customGridPdfGrid = new PdfGrid();
			customGridPdfGrid.Columns.Add(chart.CustomGridColumnCount);

			//Create a dictionary that holds the data of the custom grid for fast data retrieval
			var chartCustomGridDictionary = new Dictionary<Tuple<int, int>, ChartCustomGridCell>();
			foreach (var cell in chart.ChartCustomGridCells) {
				var cellIndex = new Tuple<int, int>(cell.RowIndex, cell.ColumnIndex);
				chartCustomGridDictionary[cellIndex] = cell;
			}

			// Fill the data of the pdf grid
			for (int rowIndex = 0; rowIndex < chart.CustomGridRowCount; rowIndex++) {
				PdfGridRow pdfGridRow = customGridPdfGrid.Rows.Add();
				for (int colIndex = 0; colIndex < chart.CustomGridColumnCount; colIndex++) {

					ChartCustomGridCell customGridCell;
					if (chartCustomGridDictionary.TryGetValue(new Tuple<int, int>(rowIndex, colIndex), out customGridCell)) {

						pdfGridRow.Cells[colIndex].Value = customGridCell.Value.StripTagsCharArray();
						pdfGridRow.Cells[colIndex].StringFormat.LineAlignment = PdfVerticalAlignment.Middle;
					}
				}
				SetPdfGridMinimumHeightForEmptyRow(pdfGridRow);
			}

			customGridPdfGrid.Style.Font = GetFontForLocal();
			customGridPdfGrid.Style.CellPadding = new PdfPaddings(3, 3, 0, 0);
			ColorTableRowsAndBorder(customGridPdfGrid, const_pdfgrid_border_width);

			return customGridPdfGrid;
		}


		/// <summary>
		/// Sets the minimum height for the pdf grid empty row.
		/// If the height is not set, then empty rows are collapsed in the pdf
		/// </summary>
		private void SetPdfGridMinimumHeightForEmptyRow(PdfGridRow pdfGridRow){
			bool rowHasValues = false;

			for (int i = 0; i < pdfGridRow.Cells.Count; i++){
				if (pdfGridRow.Cells[i].Value != null && !string.IsNullOrWhiteSpace(pdfGridRow.Cells[i].Value.ToString())){
					rowHasValues = true;
					break;
				}
			}
			if (!rowHasValues){
				pdfGridRow.Height = const_pdfGrid_row_minimum_height;
			}
		}


		/// <summary>
		/// Renders the PDF images stream.
		/// </summary>
		/// <param name="images">The images.</param>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="title">The title.</param>
		/// <returns></returns>
		public override StreamResult RenderPdfImagesStream(List<StreamResult> images, int width, int height, string title) {
			PdfDocument pdfDoc = new PdfDocument { PageSettings = { Orientation = PdfPageOrientation.Landscape } };
			pdfDoc.PageSettings.SetMargins(0, 0, 0, 0);

			PdfUnitConvertor con = new PdfUnitConvertor();

			var widthInPoint = con.ConvertFromPixels(width / 0.7f, PdfGraphicsUnit.Point) / 2;
			var heightInPoint = con.ConvertFromPixels(height / 0.7f, PdfGraphicsUnit.Point) / 2;


			pdfDoc.PageSettings.Width = widthInPoint;
			pdfDoc.PageSettings.Height = heightInPoint;

			foreach (var stream in images) {
				PdfBitmap image = new PdfBitmap(stream.ContentStream);
				PdfPage page = pdfDoc.Pages.Add();
				PdfGraphics g = page.Graphics;
				g.DrawImage(image, 0, 0, widthInPoint, heightInPoint);
			}

			MemoryStream ms = new MemoryStream();
			pdfDoc.Save(ms);
			pdfDoc.Close(true);
			ms.Position = 0;
			var filename = TimeZoneHelper.GetFilenameTimestamped(title);
			var retVal = new StreamResult(ms, filename, "pdf", MimeType.Pdf);
			return retVal;

		}

		private void InternalHtmlToPdfStream(string html, PdfPage page) {
			using (var htmlConverter = new HtmlConverter()) {
				var size = GetPageDefaultSizeInPoint(page);
				HtmlToPdfResult result = htmlConverter.Convert(html, "", ImageType.Metafile, (int)size.Width, (int)size.Height, AspectRatio.FitPageSize);

				if (result != null) {
					PdfMetafile mf = new PdfMetafile(result.RenderedImage as Metafile);
					var format = new PdfMetafileLayoutFormat { Break = PdfLayoutBreakType.FitPage, Layout = PdfLayoutType.Paginate };
					//Draws the image.
					mf.Draw(page, new PointF(0, 0), format);
				}
			}
		}

		/// <summary>
		/// Gets the font for local.
		/// </summary>
		/// <param name="size">The size.</param>
		/// <param name="style">The style.</param>
		/// <returns></returns>
		private PdfFont GetFontForLocal(int size = 8, PdfFontStyle style = PdfFontStyle.Regular) {
			//Here to support chinese fonts as well
			var currentCultureShort = Helper.GetCurrentCultureShort();
			if (currentCultureShort == "zh") {
				return new PdfCjkStandardFont(PdfCjkFontFamily.MonotypeHeiMedium, size, style);
			}
			else {
				return new PdfTrueTypeFont(new Font("Arial Unicode MS", size, style.ParseAsInt().ParseAsEnum<FontStyle>()), true);
			}

		}

		/// <summary>
		/// Sets the table style.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="chartDataGridPercentage">The chart data grid percentage.</param>
		private void SetTableStyle(PdfGrid table, ChartDataGridPercentage chartDataGridPercentage) {

			SetTableHeaderStyle(table, const_pdfgrid_border_width);
			ColorTableRowsAndBorder(table, const_pdfgrid_border_width);

			if (chartDataGridPercentage != ChartDataGridPercentage.None) {
				ColorDeltaValues(table, chartDataGridPercentage);
			}

			table.Style.CellPadding = new PdfPaddings(3, 3, 6, 0);

		}

		/// <summary>
		/// Colors the table rows and border.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="borderWidth">Width of the border.</param>
		private void ColorTableRowsAndBorder(PdfGrid table, float borderWidth) {
			var cellBorders = new PdfBorders() {
				Top = new PdfPen(Color.Gray, borderWidth / 2),
				Bottom = new PdfPen(Color.Gray, borderWidth / 2),
				Left = new PdfPen(Color.Gray, borderWidth),
				Right = new PdfPen(Color.Gray, borderWidth)
			};
			for (int i = 0; i < table.Rows.Count; i = i + 2) {
				table.Rows[i].Style.BackgroundBrush = new PdfSolidBrush(new PdfColor(DefaultGridColor));
				for (int j = 0; j < table.Columns.Count; j++) {
					table.Rows[i].Cells[j].Style.Borders = cellBorders;
					table.Rows[i].Cells[j].StringFormat.RightToLeft = IsCurrentCultureRTL();
					//If we want to control the alignment
					//if (j != 0) {
					//    table.Rows[i].Cells[j].StringFormat.Alignment = GetCurrentAligment();
					//}
				}
			}

			for (int i = 1; i < table.Rows.Count; i = i + 2) {
				table.Rows[i].Style.BackgroundBrush = new PdfSolidBrush(new PdfColor(AlternateGridColor));
				for (int j = 0; j < table.Columns.Count; j++) {
					table.Rows[i].Cells[j].Style.Borders = cellBorders;
					table.Rows[i].Cells[j].StringFormat.RightToLeft = IsCurrentCultureRTL();
					//If we want to control the alignment
					//if (j != 0) {
					//    table.Rows[i].Cells[j].StringFormat.Alignment = GetCurrentAligment();
					//}
				}
			}

		}

		/// <summary>
		/// Sets the table header style.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="borderWidth">Width of the border.</param>
		private void SetTableHeaderStyle(PdfGrid table, float borderWidth) {
			table.Headers[0].Style.BackgroundBrush = new PdfSolidBrush(GridHeaderColor);
			table.Headers[0].Style.TextBrush = new PdfSolidBrush(GridHeaderFontColor);
			table.Headers[0].Style.Font = GetFontForLocal(10);
			//Only the date has a header, the rest are the names from the series
			table.Headers[0].Cells[0].StringFormat.RightToLeft = IsCurrentCultureRTL();
			for (int i = 0; i < table.Headers[0].Cells.Count; i++) {
				table.Headers[0].Cells[i].StringFormat.Alignment = PdfTextAlignment.Center;
				table.Headers[0].Cells[i].Style.Borders.All = new PdfPen(Color.Gray, borderWidth);
				table.Headers[0].Cells[i].Style.Borders.Bottom = new PdfPen(Color.Gray, borderWidth / 2);


			}
		}



		/// <summary>
		/// Colors the delta values.
		/// </summary>
		/// <param name="table">The table.</param>
		/// <param name="chartDataGridPercentage">The chart data grid percentage.</param>
		private static void ColorDeltaValues(PdfGrid table, ChartDataGridPercentage chartDataGridPercentage) {
			Color decreaseColor;
			Color increaseColor;
			if (chartDataGridPercentage == ChartDataGridPercentage.DecreaseInGreen) {
				decreaseColor = Color.Green;
				increaseColor = Color.Red;
			}
			else {
				decreaseColor = Color.Red;
				increaseColor = Color.Green;
			}
			for (int i = 1; i < table.Rows.Count; i = i + 1) {
				for (int j = 1; j < table.Columns.Count; j++) {
					string cellString = table.Rows[i].Cells[j].Value as string;
					if ((cellString != null) && (cellString.Contains("%"))) {
						var splitCell = cellString.Split('(');

						if (splitCell.Length > 1) {
							bool isNegative = splitCell[1].StartsWith("-");
							if (isNegative)
								table.Rows[i].Cells[j].Style.TextBrush = new PdfSolidBrush(decreaseColor);
							else {
								table.Rows[i].Cells[j].Style.TextBrush = new PdfSolidBrush(increaseColor);
							}
						}
					}
				}
			}
		}


		/// <summary>
		/// Gets if chart should show percentage.
		/// </summary>
		/// <param name="chart">The chart.</param>
		/// <returns></returns>
		private bool GetShouldShowPercentageFromChart(Chart chart) {
			return chart.DataGridPercentage != ChartDataGridPercentage.None;
		}


		/// <summary>
		/// Create a new Pdf Document with Header and Footer
		/// </summary>
		/// <returns></returns>
		private PdfDocument CreateNewDocument(string title) {

			float HeaderHeight = 50;
			float FooterHeight = 30;

			PdfDocument retVal = new PdfDocument();
			//does this help?
			retVal.PageSettings.Orientation = PdfPageOrientation.Landscape;
			retVal.PageSettings.SetMargins(10, 10, 10, 10);

			float ClientWidth = retVal.PageSettings.Width - retVal.PageSettings.Margins.Left - retVal.PageSettings.Margins.Right;

			retVal.Template.Top = CreatePageHeader(ClientWidth, HeaderHeight, title);
			retVal.Template.Bottom = CreateFooter(ClientWidth, FooterHeight);

			return retVal;
		}

		/// <summary>
		/// Create the document Header.
		/// </summary>
		/// <param name="width">the page client width.</param>
		/// <param name="height">the header height.</param>
		/// <param name="title">The title of the page.</param>
		/// <returns></returns>
		private PdfPageTemplateElement CreatePageHeader(float width, float height, string title) {
			RectangleF rect = new RectangleF(0, 0, width, height + 30);
			PdfPageTemplateElement header = new PdfPageTemplateElement(rect);

			int lengthOfLogoPictures = 100;
			int lengthOfGradient = Convert.ToInt32(width) - (lengthOfLogoPictures * 2);

			DrawHeaderTitle(height, title, header, lengthOfLogoPictures, lengthOfGradient);
			DrawTenantLogo(width, height, lengthOfLogoPictures, header);

			header.Graphics.DrawLine(new PdfPen(Color.Gray, 0.5F), 5, height + 5, width, height + 5);
			//header.Graphics.DrawLine(new PdfPen(Color.LightGray, 0.5F), width - lengthOfLogoPictures - 3, m_HeaderStartingHeight, width - lengthOfLogoPictures - 3, height + 5);
			return header;

		}

		/// <summary>
		/// Draws the header title.
		/// </summary>
		/// <param name="height">The height.</param>
		/// <param name="title">The title.</param>
		/// <param name="header">The header.</param>
		/// <param name="lengthOfLogoPictures">The length of logo pictures.</param>
		/// <param name="lengthOfGradient">The length of gradient.</param>
		private void DrawHeaderTitle(float height, string title, PdfPageTemplateElement header, int lengthOfLogoPictures,
									 int lengthOfGradient) {
			RectangleF textRect = new RectangleF(5, 5, lengthOfGradient, height / 2);
			int fontSize = 14;
			PdfBrush brush = new PdfSolidBrush(PdfHeaderFontColor);
			PdfFont font = GetFontForLocal(fontSize, PdfFontStyle.Bold);

			PdfStringFormat format = new PdfStringFormat(PdfTextAlignment.Left, PdfVerticalAlignment.Top);
			format.RightToLeft = IsCurrentCultureRTL();
			format.Alignment = GetCurrentAligment();

			header.Graphics.DrawString(title, font, brush, textRect, format);

			var additionalDataString = Helper.GetReportHeaderText();
			PdfFont additionalDatafont = GetFontForLocal(10);
			PdfBrush additionalDatabrush = new PdfSolidBrush(Color.Gray);
			PdfStringFormat additionalDataformat = new PdfStringFormat(PdfTextAlignment.Left, PdfVerticalAlignment.Middle);
			additionalDataformat.RightToLeft = IsCurrentCultureRTL();
			additionalDataformat.Alignment = GetCurrentAligment();
			var additionalDataRect = new RectangleF(5, (height / 2) - 1, lengthOfGradient, height);

			header.Graphics.DrawString(additionalDataString, additionalDatafont, additionalDatabrush, additionalDataRect, additionalDataformat);

		}

		/// <summary>
		/// Gets the current aligment.
		/// </summary>
		/// <returns></returns>
		private PdfTextAlignment GetCurrentAligment() {
			return IsCurrentCultureRTL() ? PdfTextAlignment.Right : PdfTextAlignment.Left;
		}

		/// <summary>
		/// Determines if the current culture needs to be right to left
		/// </summary>
		/// <returns>
		///   <c>true</c> if the current culture needs to be right to left otherwise, <c>false</c>.
		/// </returns>
		private bool IsCurrentCultureRTL() {

			return RtlCultures.Contains(Helper.GetCurrentCultureShort());
		}

		/// <summary>
		/// Draws the background gradient.
		/// </summary>
		/// <param name="height">The height.</param>
		/// <param name="header">The header.</param>
		/// <param name="lengthOfGradient">The length of gradient.</param>
		/// <param name="lengthOfLogoPictures">The length of logo pictures.</param>
		private void DrawGradient(float height, PdfPageTemplateElement header, int lengthOfGradient, int lengthOfLogoPictures) {
			PdfBrush gradientBrush = new PdfLinearGradientBrush(
				new RectangleF(lengthOfLogoPictures, m_HeaderStartingHeight, lengthOfGradient, height), new PdfColor(PdfHeaderGradientTop),
				new PdfColor(PdfHeaderGradientBottom), 90);
			header.Graphics.DrawRectangle(gradientBrush, lengthOfLogoPictures, m_HeaderStartingHeight, lengthOfGradient, height);
		}

		/// <summary>
		/// Draws the tenant logo.
		/// </summary>
		/// <param name="width">The width.</param>
		/// <param name="height">The height.</param>
		/// <param name="lengthOfLogoPictures">The length of logo pictures.</param>
		/// <param name="header">The header.</param>
		private void DrawTenantLogo(float width, float height, int lengthOfLogoPictures, PdfPageTemplateElement header) {
			string keyTenant = string.Empty;
			using (var tenantLogo = new PdfBitmap(TenantHelper.GetCurrentTenantLogoPath(keyTenant))) {
				header.Graphics.DrawImage(tenantLogo, width - lengthOfLogoPictures, m_HeaderStartingHeight, lengthOfLogoPictures, height);
			}
		}

		/// <summary>
		/// Create the document Footer.
		/// </summary>
		/// <param name="width">the page client width.</param>
		/// <param name="height">the footer height.</param>
		/// <returns></returns>
		private PdfPageTemplateElement CreateFooter(float width, float height) {
			PdfBrush brush = new PdfSolidBrush(Color.Black);
			PdfFont font = GetFontForLocal(10, PdfFontStyle.Bold);


			RectangleF rect = new RectangleF(0, 0, width, height);
			PdfPageTemplateElement footer = new PdfPageTemplateElement(rect);
			PdfPageNumberField pageNumber = new PdfPageNumberField(font, brush);
			PdfPageCountField count = new PdfPageCountField(font, brush);
			PdfCompositeField compositeField = new PdfCompositeField(font, brush, "{0} / {1}", pageNumber, count);
			compositeField.Bounds = footer.Bounds;
			compositeField.Draw(footer.Graphics, new PointF(width - 20, height / 2));

			return footer;
		}
	}




}

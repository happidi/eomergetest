﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vizelia.FOL.Common;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Concrete implementation of SqlProfileProvider
	/// </summary>
	public class FOLSqlProfilerProvider : System.Web.Profile.SqlProfileProvider {
		/// <summary>
		/// Gets or sets the name of the application for which to store and retrieve profile information.
		/// </summary>
		/// <returns>The name of the application for which to store and retrieve profile information. The default is the <see cref="P:System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath"/> value.</returns>
		///   
		/// <exception cref="T:System.Web.HttpException">An attempt was made to set the <see cref="P:System.Web.Profile.SqlProfileProvider.ApplicationName"/> property by a caller that does not have <see cref="F:System.Web.AspNetHostingPermissionLevel.High"/> ASP.NET hosting permission.</exception>
		///   
		/// <exception cref="T:System.Configuration.Provider.ProviderException">An attempt was made to set the <see cref="P:System.Web.Profile.SqlProfileProvider.ApplicationName"/> property to a string that is longer than 256 characters.</exception>
		public override string ApplicationName {
			get {
				string retval = ContextHelper.ApplicationName;
				return retval;
			}
			set {

			}
		}
	}
}
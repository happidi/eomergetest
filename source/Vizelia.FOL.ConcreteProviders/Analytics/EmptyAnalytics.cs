﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.Text;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Utilities.WCFClient.CookieManager;
using Vizelia.FOL.WCFService.Contracts;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Empty Analytics example
	/// </summary>
	[LocalizedText("Empty Analytics example ")]
	public class EmptyAnalytics : IAnalyticsExtensibility {
		/// <summary>
		/// Executes the specified chart.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="chart">The chart.</param>
		/// <param name="series">The series.</param>
		/// <param name="inputs">The inputs.</param>
		public Tuple<Chart, List<DataSerie>, Dictionary<string, string>> Execute(EnergyAggregatorContext context, Chart chart, List<DataSerie> series, Dictionary<string, string> inputs) {

			ChannelFactory<IPublicWCF> publicFactory = null;
			ChannelFactory<IEnergyWCF> serviceChannelFactory = null;
			IPublicWCF publicProxy = null;

			// Resolve Tenant Endpoint
			var tenantUrl = "FacilityOnLine/";
			var protocol = "http";
			Uri baseUri = new Uri((tenantUrl.ToLower().StartsWith(protocol + "://") ? string.Empty : protocol + "://") + tenantUrl.TrimEnd('/') + '/');
			Uri publicServiceUri = new Uri(baseUri, "Public.svc/soap");
			var cookieManager = CookieManagerEndpointBehavior.CreateNew();
			publicFactory = new ChannelFactory<IPublicWCF>("PublicProxy_" + protocol.ToLower(), new EndpointAddress(publicServiceUri));
			publicFactory.Endpoint.Behaviors.Add(cookieManager);

			// Log in to the tenant, if given login details. Otherwise work unauthenticated.
			var username = "Admin";
			var password = "vizelia!!";
			if (username != null && password != null) {
				publicProxy = publicFactory.CreateChannel(new EndpointAddress(publicServiceUri));
				publicProxy.Login_Response(username, password, true, null);
			}

			//// Invoke remote service
			Uri serviceUri = new Uri(baseUri, "Energy.svc/soap");
			serviceChannelFactory = new ChannelFactory<IEnergyWCF>("EnergyProxy_" + protocol.ToLower(), new EndpointAddress(serviceUri));
			serviceChannelFactory.Endpoint.Behaviors.Add(cookieManager);
			IEnergyWCF energyClient = serviceChannelFactory.CreateChannel(new EndpointAddress(serviceUri));
			if (energyClient != null) {
				//var meters = energyClient.Meter_GetStore(new PagingParameter { limit = 50, start = 0 }, PagingLocation.Database);
				string localId = "Meter" + Guid.NewGuid();
				energyClient.Meter_FormCreate(new Meter { LocalId = localId, Name = localId, KeyLocation = "#87236", KeyClassificationItem = "#92673" }, null);
			}

			/*
			HttpWebRequest request = CreateWebRequest("http://facilityonline/Public.svc/Login_Response");
			string json = "{\"userName\":\"Admin\",\"password\":\"vizelia!!\",\"createPersistentCookie\":false,\"provider\":\"FOLSqlMembership\"}";
			SetData(request.GetRequestStream(), json);
			HttpWebResponse response = (HttpWebResponse)request.GetResponse();

			HttpWebRequest energyRequest = CreateWebRequest("http://facilityonline/Energy.svc/Meter_FormCreate");
			energyRequest.CookieContainer.Add(response.Cookies[0]);
			energyRequest.Headers.Add("PAGEID", response.Headers["PAGEID"]);

			string meterId = "NewMeter" + Guid.NewGuid();
			string createMeterJsonPost = "{\"item\":{\"KeyMeter\":null,\"LocalId\":\"" + meterId + "\",\"Name\":\"" + meterId + "\",\"Description\":null,\"KeyLocation\":\"#87236\",\"KeyClassificationItem\":\"#92673\",\"GroupOperation\":0,\"Factor\":1,\"undefined\":[\"Offset:\",\"Unit input:\",\"Unit output:\",\"Operator:\"],\"Offset\":0,\"UnitInput\":null,\"UnitOutput\":null,\"PsetFactorOperator\":3,\"IsAccumulated\":false,\"IsAcquisitionDateTimeEndInterval\":true,\"EndpointFrequency\":null,\"CalibrationDateTime\":null,\"CalibrationValue\":null,\"RolloverValue\":null,\"AcceptMobileInput\":false,\"DegreeDayType\":0,\"DegreeDayBase\":null,\"MeterOperationTimeScaleInterval\":0,\"TimeZoneId\":\"Israel Standard Time\",\"CalendarMode\":0,\"PropertySetList\":[]},\"meterOperationsWizard\":{}}";
			SetData(energyRequest.GetRequestStream(), createMeterJsonPost);


			var energyResponse = energyRequest.GetResponse();
			Stream responseStream = energyResponse.GetResponseStream();
			StreamReader streamReader = new StreamReader(responseStream);
			*/
			var existingSeries = chart.Series;

			DataSerie s1;
			if (existingSeries.ContainsLocalid("Serie1")) {
				s1 = existingSeries.FindByLocalId("Serie1");
				if (s1.Type == DataSerieType.None)
					s1.Type = DataSerieType.Marker;
			}
			else {
				s1 = new DataSerie();
				s1.LocalId = s1.Name = "Serie1";
				s1.Type = DataSerieType.Marker;
				s1.DisplayLegendEntry = DataSerieLegendEntry.NONE;
				s1.ColorR = 0;
				s1.ColorG = 0;
				s1.ColorB = 255;
				s1.MarkerSize = 20;
				s1.MarkerType = MarkerType.Circle;
			}
			s1.Points = new List<DataPoint>();
			for (var i = 0; i < 10; i++) {
				s1.Points.Add(new DataPoint {
					XValue = i,
					YValue = i,
					Name = ""
				});
			}

			series.Clear();
			series.Add(s1);

			return new Tuple<Chart, List<DataSerie>, Dictionary<string, string>>(chart, series, null);
		}


		private static void SetData(Stream requestStream, string json) {
			byte[] postBytes = Encoding.UTF8.GetBytes(json);
			requestStream.Write(postBytes, 0, postBytes.Length);
			requestStream.Close();
		}

		private static HttpWebRequest CreateWebRequest(string uri) {
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
			request.ContentType = "application/json; charset=utf-8";
			request.Method = "POST";
			request.CookieContainer = new CookieContainer();
			return request;
		}

		/// <summary>
		/// Gets the public inputs.
		/// </summary>
		/// <returns></returns>
		public Dictionary<string, string> GetPublicInputs() {
			return new Dictionary<string, string>();
		}

		/// <summary>
		/// Gets the data series local id.
		/// </summary>
		/// <returns></returns>
		public List<ListElement> GetDataSeriesLocalId() {
			return new List<ListElement>() {
				GetListElementFromLocalId("Serie1")
			};
		}

		/// <summary>
		/// Gets the list element from local id.
		/// </summary>
		/// <param name="localId">The local id.</param>
		/// <returns></returns>
		private ListElement GetListElementFromLocalId(string localId) {
			return new ListElement {
				Id = localId,
				MsgCode = localId,
				Value = localId,
				IconCls = Helper.GetAttributeValue<IconClsAttribute>(typeof(DataSerie))
			};
		}
	}
}

﻿using System.Collections.Generic;
using System.Reflection;
using Vizelia.FOL.Providers;
using Vizelia.FOL.BusinessEntities;

namespace Vizelia.FOL.ConcreteProviders {

	/// <summary>
	/// A localization provider that gets the string from the db.
	/// </summary>
	public class DBResourceProvider : LocalizationProvider {
		private static readonly object m_PopulateCacheLock = new object();
		private static readonly object m_UpdateCacheLock = new object();

		/// <summary>
		/// Gets the localized text.
		/// </summary>
		/// <param name="msgCode">The message code.</param>
		/// <param name="cultureCode">The culture code.</param>
		/// <returns></returns>
		public override string GetLocalizedText(string msgCode, string cultureCode) {
			var localizationCache = GetLocalizationCache(cultureCode);

			Dictionary<string, string> messages = localizationCache[cultureCode];
			string localizedText = null;

			if (messages.ContainsKey(msgCode))
				localizedText = messages[msgCode];
			
			return localizedText;
		}

		/// <summary>
		/// Gets the localization cache with the dictionary populated for the requested culture.
		/// </summary>
		/// <param name="cultureCode">The culture code.</param>
		/// <returns></returns>
		private static Dictionary<string, Dictionary<string, string>> GetLocalizationCache(string cultureCode) {
			var localizationCache = GetOrCreateLocalizationCache();

			if (!localizationCache.ContainsKey(cultureCode)) {
				lock (m_PopulateCacheLock) {
					localizationCache = GetOrCreateLocalizationCache();
					
					if (!localizationCache.ContainsKey(cultureCode)) {
						localizationCache.Add(cultureCode, BuildDictionaryFromDatabase(cultureCode));
						CacheService.Add(CacheKey.const_cache_localization, localizationCache);
					}
				}
			}

			return localizationCache;
		}

		/// <summary>
		/// Gets the or create localization cache.
		/// </summary>
		/// <returns></returns>
		private static Dictionary<string, Dictionary<string, string>> GetOrCreateLocalizationCache() {
			var localizationCache = CacheService.Get(CacheKey.const_cache_localization,
			                                         () => new Dictionary<string, Dictionary<string, string>>(), 0);

			return localizationCache;
		}


		/// <summary>
		/// Adds the localized text to cache.
		/// </summary>
		/// <param name="msgCode">The MSG code.</param>
		/// <param name="cultureCode">The culture code.</param>
		/// <param name="value">The value.</param>
		public override void AddLocalizedTextToCache(string msgCode, string cultureCode, string value) {
			lock(m_UpdateCacheLock) {
				var localizationCache = GetLocalizationCache(cultureCode);

				localizationCache[cultureCode][msgCode] = value;
				CacheService.Add(CacheKey.const_cache_localization, localizationCache);
			}
		}

		/// <summary>
		/// Removes the localized text from cache.
		/// </summary>
		/// <param name="msgCode">The MSG code.</param>
		/// <param name="cultureCode">The culture code.</param>
		public override void RemoveLocalizedTextFromCache(string msgCode, string cultureCode) {
			lock (m_UpdateCacheLock) {
				var localizationCache = GetLocalizationCache(cultureCode);

				if (localizationCache[cultureCode].Remove(msgCode)) {
					// Only update the cache if removed.
					CacheService.Add(CacheKey.const_cache_localization, localizationCache);
				}
			}
		}


		/// <summary>
		/// Builds the dictionary from database.
		/// </summary>
		/// <param name="cultureCode">The cultureCode.</param>
		/// <returns></returns>
		private static Dictionary<string, string> BuildDictionaryFromDatabase(string cultureCode) {
			var assembly = Assembly.Load("Vizelia.FOL.Common.Localization.ResourceManager");
			var method = assembly.GetType("Vizelia.FOL.Common.Localization.DatabaseResourceHelper").GetMethod("GetMessages");
			Dictionary<KeyValuePair<string, string>, string> results = method.Invoke(null, new object[] { cultureCode }) as Dictionary<KeyValuePair<string, string>, string>;
			Dictionary<string, string> messages = new Dictionary<string, string>();
			foreach (KeyValuePair<KeyValuePair<string, string>, string> keyValuePair in results) {
				messages.Add(keyValuePair.Key.Key, keyValuePair.Value);
			}
			return messages;
		}

	}
}

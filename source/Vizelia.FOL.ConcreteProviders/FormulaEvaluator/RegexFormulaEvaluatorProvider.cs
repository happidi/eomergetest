﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vizelia.FOL.BusinessEntities;
using Vizelia.FOL.Common;
using Vizelia.FOL.Providers;

namespace Vizelia.FOL.ConcreteProviders {
	/// <summary>
	/// Regex Formula Evaluator Provider
	/// </summary>
	public class RegexFormulaEvaluatorProvider : Vizelia.FOL.Providers.FormulaEvaluatorProvider {
		/// <summary>
		/// Evaluates the specified calculated serie.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="calcul">The calculated serie.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		/// <returns></returns>
		public override DataSerie Evaluate(List<DataSerie> series, CalculatedSerie calcul, IFormulaEvaluatorContext context = null) {
			var helper = new RegexFormulaEvaluatorHelper(series, context);
			var retVal = helper.Evaluate(calcul);
			return retVal;

		}

		/// <summary>
		/// Evaluates the specified calculated series.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="calculatedSeries">The calculated series.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		/// <returns></returns>
		public override List<Tuple<DataSerie, CalculatedSerie, Exception>> Evaluate(
			List<DataSerie> series, List<CalculatedSerie> calculatedSeries, IFormulaEvaluatorContext context = null) {
			#region Grouping by Order
			var sortedList = new SortedDictionary<int, List<CalculatedSerie>>();
			foreach (var calculatedSerie in calculatedSeries) {
				List<CalculatedSerie> list;
				if (!sortedList.TryGetValue(calculatedSerie.Order, out list)) {
					list = new List<CalculatedSerie>();
					sortedList.Add(calculatedSerie.Order, list);
				}
				list.Add(calculatedSerie);
			}
			#endregion

			var retVal = new List<Tuple<DataSerie, CalculatedSerie, Exception>>();

			var helper = new RegexFormulaEvaluatorHelper(series, context);
			foreach (var kvp in sortedList) {
				foreach (var calculatedSerie in kvp.Value) {
					Exception error = null;
					DataSerie dataSerie = null;
					try {
						dataSerie = helper.Evaluate(calculatedSerie);
					}
					catch (Exception e) {
					//NOTE: Exceptions are almost always handled by the Evaluate method. If an error is found,
					// it returns dataSeries = null and updates the errors within the context object.
					// As of 2.11, the Exception element of the Tuple is not being consumed elsewhere in EO.
						error = e;
					}
					if (dataSerie != null) {
						var result = new Tuple<DataSerie, CalculatedSerie, Exception>(dataSerie, calculatedSerie, error);
						retVal.Add(result);
					}
				}
			}
			return retVal.ToList();
		}

		/// <summary>
		/// Evaluates the specified formula.
		/// </summary>
		/// <param name="formula">The formula.</param>
		/// <param name="KeyLocalizationCulture">The key localization culture.</param>
		/// <param name="context">If supplied and an evaluation error occurs, an error message will be added.</param>
		/// <returns></returns>
		public override double Evaluate(string formula, string KeyLocalizationCulture = null, IFormulaEvaluatorContext context = null) {
			var helper = new RegexFormulaEvaluatorHelper(context);
			var retVal = helper.Evaluate(formula, KeyLocalizationCulture);
			return retVal;
		}

		/// <summary>
		/// Validates the specified calculated serie.
		/// </summary>
		/// <param name="series">The series.</param>
		/// <param name="calcul">The calculated serie.</param>
		/// <returns></returns>
		public override bool Validate(List<DataSerie> series, CalculatedSerie calcul) {
			return true;
		}

		/// <summary>
		/// Validates the specified formula.
		/// </summary>
		/// <param name="formula">The formula.</param>
		/// <returns></returns>
		public override bool Validate(string formula) {
			return true;
		}


	}
}
